<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>MYR BackEnd: custom application to group the main tabs for MyRenault administrators</description>
    <formFactors>Large</formFactors>
    <label>MyRenault Back End</label>
    <tab>Synchro_ATC__c</tab>
    <tab>standard-Account</tab>
    <tab>VRE_VehRel__c</tab>
    <tab>VEH_Veh__c</tab>
    <tab>Vehicle_Configuration__c</tab>
    <tab>Logger__c</tab>
    <tab>Customer_Message__c</tab>
    <tab>Customer_Message_Settings__c</tab>
    <tab>Customer_Message_Templates__c</tab>
    <tab>Myr_Administration</tab>
    <tab>MLC_Molicar__c</tab>
    <tab>FaturaDealer2__c</tab>
    <tab>SurveySent__c</tab>
</CustomApplication>
