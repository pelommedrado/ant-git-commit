<apex:page controller="DashboardLeadController" showHeader="true" sidebar="true">
    <apex:stylesheet value="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <apex:stylesheet value="{!URLFOR($Resource.inputStyle)}" />
    <script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.3/jquery.mask.min.js"></script>
    
     <apex:stylesheet value="{!URLFOR($Resource.bootstrap_icons, 'bootstrap_icons/bootstrap.css')}" />
    <script>
    j$ = $.noConflict();
    j$(document).ready( function( $ ) {
        
        j$('[id$=vehicleOwnerId]').is(':checked')  ? 
            j$( '.vehicleOwnerInfo' ).prop( 'disabled', false ) : j$( '.vehicleOwnerInfo' ).prop( 'disabled', true );
        
        j$( '[id$=nextActSession]' ).hide();
        
        configDate();
        maskInput();
    });
    
    j$(document).on('change', 'select[id$=statusId], select[id$=subjectId]', function(){
        var selected = j$( 'select[id$=statusId] option:selected' ).val();
        var subject = j$( 'select[id$=subjectId] option:selected' ).val();
        var subjectsAvailable = 'Confirm Test Drive, Confirm Visit, Contact';
        
        if(selected == 'Completed' && subjectsAvailable.indexOf(subject) >= 0){
            j$( '[id$=nextActSession]' ).slideDown();
        } else {
            j$( '[id$=nextActSession]' ).slideUp();
            
        }
    });
    
    j$(document).on('change', '[id$=vehicleOwnerId]', function() {
        j$( this ).is( ':checked' )  ? j$( '.vehicleOwnerInfo' ).prop( 'disabled', false ) : clearVehicleOwnerInfo();
    });
    
    function setReminder(actDateId, actHourId, reminderHourId) {
        var day = j$( 'input[id$=' + actDateId + ']' ).val().split("/");
        var hour = j$( 'select[id$=' + actHourId + '] option:selected' ).val().split(":");
        var reminderDatetime = new Date(day[2], day[1] -1, day[0], hour[0], hour[1] - 15, 0, 0);
        
        j$( 'span[id$=' + reminderHourId + ']' ).text(j$.format.date(reminderDatetime, 'dd/MM/yyyy HH:mm'));
    }
    function maskInput() {
    	j$('input[id$=actHour]').mask('99:99', { placeholder: "__:__" } );
    }
    function clearVehicleOwnerInfo() {
        j$( '.vehicleOwnerInfo' ).prop( 'disabled', true );
        j$( '.vehicleOwnerInfo' ).val( '' );
    }
    function configDate() {
        j$('.datepicker').datepicker({
            "showAnim": "slideDown",
            "dateFormat": "dd/mm/yy"
        });
        j$('.datepicker').mask('99/99/9999', { placeholder: "  /  /" }); 
    }
    </script>
    
    <apex:form >
        <apex:pageBlock title="Prospecção de Lead" mode="edit">
            
            <apex:pageBlockButtons >
                <apex:commandButton value="Salvar" action="{!save}" reRender="error" status="status" />
                <apex:commandButton value="Cancelar" action="{!cancelar}" immediate="true" reRender="error" />&nbsp;
                <apex:actionStatus id="status">
                    <apex:facet name="start" >
                        <apex:outputPanel >
                            <apex:image url="/img/loading.gif"/>&nbsp;
                            <apex:outputLabel value="{!$Label.VFP21_Loading}"/>
                        </apex:outputPanel>
                    </apex:facet>
                </apex:actionStatus>
            </apex:pageBlockButtons>
            
            <apex:pageMessages id="error"/>
            
            <apex:pageBlockSection title="Informações do Cliente" collapsible="false" columns="2">               
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Nome: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"/>
                        <apex:inputText value="{!l.FirstName}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Telefone: "/>
                    <apex:inputText value="{!l.HomePhone__c}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Sobrenome: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"/>
                        <apex:inputText value="{!l.LastName}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Celular: "/>
                    <apex:inputText value="{!l.MobilePhone}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="CPF: "/>
                    <apex:inputText value="{!l.CPF_CNPJ__c}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Email: "/>
                    <apex:inputText value="{!l.Email}" />
                </apex:pageBlockSectionItem>           
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Informações de Interese" collapsible="false" columns="2">
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Veículo de Interesse: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"/>
                    	<apex:inputField value="{!l.VehicleOfInterest__c}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Possuí veículo: "/>
                    <apex:inputCheckbox value="{!isVehicleOwner}" id="vehicleOwnerId" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Marca: "/>
                    <apex:actionRegion >
                        <apex:selectList value="{!selectedBrand}" size="1" styleClass="vehicleOwnerInfo">
                            <apex:selectOptions value="{!brands}" />
                            <apex:actionSupport event="onchange" action="{!getModelDetails}"
                                                rerender="modelList" status="brandstatus"/>
                        </apex:selectList>
                        
                        <apex:actionStatus id="brandstatus">
                            <apex:facet name="start" >
                                <apex:outputPanel >
                                    <apex:image url="/img/loading.gif"/>&nbsp;
                                    <apex:outputLabel value="{!$Label.VFP21_Loading}"/>
                                </apex:outputPanel>
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:actionRegion>
                    
                </apex:pageBlockSectionItem>
               
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Modelo: "/>
                    <apex:actionRegion >
                        <apex:selectList value="{!selectedModel}" size="1" styleClass="vehicleOwnerInfo"
                                         id="modelList" >
                            <apex:selectOptions value="{!models}" />
                        </apex:selectList>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem />
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection id="infoActv" title="Informações da Atividade" collapsible="false" columns="2">
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Responsável: "/>
                    <apex:outputText value="{!act.owner}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Data: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />
                        <apex:inputText value="{!act.activityDate}" id="actDate" styleClass="datepicker"
                                        onchange="setReminder('actDate', 'actHour', 'reminderHour')" 
                                        disabled="{!disableHora}"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Atividade: "/>
                    <apex:outputText value="{!act.subject}"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Hora: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />
                        <apex:inputText value="{!act.activityHour}" id="actHour" size="5"
                                        onchange="setReminder('actDate', 'actHour', 'reminderHour')"
                                        disabled="{!disableHora}"/>
                        <!--apex:selectList value="{!act.activityHour}" size="1" id="actHour" styleClass="timepicker"
                                         onchange="setReminder('actDate', 'actHour', 'reminderHour')"
                                         disabled="{!disableHora}">
                            <apex:selectOption itemLabel="--Nenhum--" itemValue=""/>
                            <apex:selectOptions value="{!hourList}" />
                        </apex:selectList-->
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Status: "/>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />
                        <apex:actionRegion >
                            <apex:selectList value="{!statusLead}" size="1" id="statusId">
                                <apex:selectOption itemValue="" itemLabel="--Nenhum--"/>
                                <apex:selectOptions value="{!statusLeadList}" />
                                <apex:actionSupport action="{!changeStatus}" event="onchange"
                                                    status="statusLeadLoad" reRender="infoActv" oncomplete="configDate();" />
                            </apex:selectList>
                            
                            <apex:actionStatus id="statusLeadLoad">
                                <apex:facet name="start" >
                                    <apex:outputPanel >
                                        <apex:image url="/img/loading.gif"/>&nbsp;
                                        <apex:outputLabel value="{!$Label.VFP21_Loading}"/>
                                    </apex:outputPanel>
                                </apex:facet>
                            </apex:actionStatus>
                        </apex:actionRegion>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Lembrete: "/>
                    <apex:outputPanel id="reminderPanel">
                        <apex:inputCheckbox value="{!act.reminder}" disabled="{!disableHora}"/>
                        <apex:outputText id="reminderHour" value="{0,date,dd/MM/yyyy HH:mm}" >
                            <apex:param value="{!act.reminderDatetime}"/>
                        </apex:outputText>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Motivo: "/>
                    <apex:selectList value="{!motivo}" id="reasonId" size="1"
                                     disabled="{!disableMotivo}" styleClass="act-reason">
                        <apex:selectOption itemValue="" itemLabel="-- Nenhum --"/>
                        <apex:selectOptions value="{!motivosList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Comentários: "/>
                    <apex:inputTextarea value="{!act.description}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Previsão próxima compra: "/>
                    <apex:selectList value="{!previsao}" id="previsaoId" size="1"
                                     disabled="{!disablePrevisao}">
                        <apex:selectOption itemValue="" itemLabel="-- Nenhum --"/>
                        <apex:selectOptions value="{!previsaoList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
            </apex:pageBlockSection>
            
            <!--apex:pageBlockSection title="Próxima Atividade" id="nextActSession" collapsible="false" columns="2">
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Responsável: "/>
                    <apex:outputText value="{!u.Name}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Data: "/>
                    <apex:outputPanel >
                        <apex:inputText value="{!nextAct.activityDate}" id="nextActDate" styleClass="datepicker" onchange="setReminder('nextActDate', 'nextActHour', 'nextReminderHour')" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Atividade: "/>
                    <apex:selectList value="{!nextAct.subject}" size="1">
                        <apex:selectOptions value="{!subjectList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Hora: "/>
                    <apex:selectList value="{!nextAct.activityHour}" size="1" id="nextActHour" styleClass="timepicker" onchange="setReminder('nextActDate', 'nextActHour', 'nextReminderHour')">
                        <apex:selectOptions value="{!hourList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Status: "/>
                    <apex:selectList value="{!nextAct.status}" size="1" id="nextStatusId" onchange="showReason('nextStatusId', 'nextReasonId')" >
                        <apex:selectOptions value="{!statusList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Lembrete: "/>
                    <apex:outputPanel >
                        <apex:inputCheckbox value="{!nextAct.reminder}" />
                        <apex:outputText id="nextReminderHour" value="{0,date,dd/MM/yyyy HH:mm}" >
                            <apex:param value="{!nextAct.reminderDatetime}"/>
                        </apex:outputText>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Motivo: "/>
                    <apex:selectList value="{!nextAct.reason}" id="nextReasonId" disabled="true" size="1">
                        <apex:selectOptions value="{!reasonList}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Comentários: "/>
                    <apex:inputTextarea value="{!nextAct.description}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Criar próxima atividade: "/>
                    <apex:outputPanel >
                        <apex:inputCheckbox value="{!createNext}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
            </apex:pageBlockSection-->
            
        </apex:pageBlock>
    </apex:form>
    
</apex:page>