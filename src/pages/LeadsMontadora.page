<apex:page controller="LeadsMontadoraController" sidebar="false" >
  
  <script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
  <script src="{!$Resource.jqdatatables}"></script>
  <script>
    var oppIdMap = {};
    function removeId (oppId){
      if( oppIdMap.hasOwnProperty( oppId ) ) delete oppIdMap[oppId];
    }
    function addId (oppId, ownerId){
      oppIdMap[oppId] = ownerId;
    }
    function selectChange (oppId, ownerId){
      var checkbox = document.getElementById( 'check' + oppId );
      if( ownerId == '' ){
        checkbox.checked = false;
        removeId( oppId );
      }else{
        checkbox.checked = true;
        addId( oppId, ownerId );
      }
    }
    j$ = $.noConflict();
    jQuery(document).ready( function( $ ){
      formatTable();
    } );
    function formatTable(){
      j$('.list').dataTable( {
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false
      } );
      j$('.last').removeClass('last');
      oppIdList = [];
    }
    function disableBtn (button){
      j$(button).removeClass( 'btn' );
      j$(button).addClass( 'btnDisabled' );
      j$(button).prop( 'disabled', true );
    }
    function enableBtn (button){
      j$(button).addClass( 'btn' );
      j$(button).removeClass( 'btnDisabled' );
      j$(button).prop( 'disabled', false );
    }
  </script>
  
  <style>
    .message{
      font-size: 18px;
      padding: 10px;
    }
    .pull-r{
      float: right;
    }
    .pull-l{
      float: left;
    }
    .centeredLink{
      display: inline-block;
    }
    #tableControls{
      text-align: center;
      width: 100%;
      margin: 13px 0;
    }
    .mini-padding{
      padding: 8px 1% 0px 2%;
    }
    th.headerRow div:after{
      content:url({!$Resource.tablesorticon});
    }
  </style>
  
  <apex:pageMessage strength="0" summary="{!$Label.Hello}, {!$User.FirstName}! <br/>{!welcomeMessage}" severity="warning" escape="false" rendered="{!messages.Show_welcome_message__c}"/>
  <apex:form >
  
    <apex:actionFunction action="{!forward}" name="forward" rerender="PB, pageMessage" oncomplete="oppIdMap = {}; formatTable(); enableBtn(document.getElementById('forwardButton'));">
      <apex:param value="" assignTo="{!oppIdListAsString}" name="selectedOppIdList"/>
    </apex:actionFunction>
  
    <apex:pageblock title="{!$Label.VFP24_Lead_Management}" id="PB">
      
      <apex:pageMessages id="pageMessage"/>
      
      <div id="tableControls">
       
        <apex:outputpanel rendered="{!oppList.size > 0}">
          <button id="forwardButton" class="btn pull-l" 
            onclick="
              if( Object.keys( oppIdMap ).length != 0 ){
                disableBtn(this); 
                forward( JSON.stringify( oppIdMap ) );
              }
              return false;">
              {!$Label.VFP24_Forward_Button}
          </button>
        </apex:outputpanel>
      
        <div class="centeredLink">
          <apex:outputLink value="{!$Label.VFP24_ManagerLeadBox_centeredLink}">{!$Label.VFP24_Subscribe_Leads}
          </apex:outputLink>
        </div>

        <div class="pull-r">
          <apex:selectList size="1" value="{!dealerFilter}">
            <apex:actionSupport event="onchange" action="{!loadOppMap}" rerender="PB"/>
            <apex:selectOptions value="{!dealerSelectList}"/>
          </apex:selectList>
        </div>
      
        <div class="pull-r">
          <apex:selectList size="1" value="{!campaignFilter}">
            <apex:actionSupport event="onchange" action="{!loadOppMap}" rerender="PB"/>
            <apex:selectOptions value="{!campaignSelectList}"/>
          </apex:selectList>
        </div>
      
      </div>
      
      <apex:pageblockTable value="{!oppList}" var="opp" rendered="{!oppList.size > 0}">
        <apex:column headerValue="{!$Label.VFP24_Forward_Button}">
          <input type="checkbox" disabled="disabled" id="check{!opp.oppId}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.Name.label}">
          <apex:outputLink value="/{!opp.oppId}">{!opp.oppName}</apex:outputLink>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.CreatedDate.label}" 
          value="{!opp.oppCreateDate}"/>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.VehicleInterest__c.label}"  
          value="{!opp.oppVehicleInterest}"/>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.OpportunitySubSource__c.label}"
          value="{!opp.oppOpportunitySubSource}"/>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.Campaign_Name__c.label}"  
          value="{!opp.oppCampaignName}"/>
        <apex:column headerValue="{!$ObjectType.Opportunity.fields.Dealer__c.label}"  
          value="{!opp.oppDealer}"/>
        <apex:column headerValue="{!$Label.VFP24_Owner}">
          <apex:selectList size="1" onchange="selectChange( '{!opp.oppId}', this.options[this.selectedIndex].value )">
            <apex:selectOptions value="{!opp.dealerContacts}"/>
          </apex:selectList>
        </apex:column>
      </apex:pageblockTable>
      
      <i><apex:outputText value="{!$Label.VFP20_Desktop_No_Records}" rendered="{!oppList.size == 0}" styleClass="mini-padding"/></i>
      
    </apex:pageblock>
  </apex:form>
</apex:page>