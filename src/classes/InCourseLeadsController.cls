public without sharing class InCourseLeadsController {
    
    public List<CampaignMember> campMemberList{get;set;}
    public String CPF{get;set;}
    public String tpVoucher{get;set;}
    
    public InCourseLeadsController(){
        CPF = ApexPages.currentPage().getParameters().get('CPF');
        tpVoucher = ApexPages.currentPage().getParameters().get('tipoVoucher');
        campMemberLoad();
    }
    
    public List<CampaignMember> lsCampMember{
        get{
            return campMemberList;
        }
    }
    
    public void campMemberLoad(){
        
        String stage = ApexPages.currentPage().getParameters().get('stage');
        
        // recupera os leads com CPF passado e relacionados a uma campanha ativa
        List<CampaignMember> lsCampMember = Database.query( 
            'SELECT Id, CampaignId, Campaign.Name, Campaign.Refundable__c, Lead.Name, Lead.CPF_CNPJ__c, ' +
            'Lead.Voucher_Protocol__c, Campaign.Description, Campaign.EndDate ' +
            'FROM CampaignMember WHERE Lead.CPF_CNPJ__c = \''+ CPF + 
            '\' AND Lead.Status = \'To Rescue\' AND Campaign.IsActive = TRUE AND Campaign.Status = \'In Progress\' ' +
            (stage != NULL && stage.equalsIgnoreCase('In Attendance') ? 
             'AND Campaign.Billing_Required__c = FALSE AND Campaign.Refundable__c = FALSE ' : '') +
            'AND Lead.RecordTypeId = \'' + Utils.getRecordTypeId('Lead', 'Voucher') + '\''
        );
        
        system.debug('lsCampMember: '+lsCampMember);
        
        campMemberList = new List<CampaignMember>();
        campMemberList.addAll(lsCampMember);
    }
    
    
    public void localizaVoucher(){
            String oppCPF = ApexPages.currentPage().getParameters().get('CPF');
            String stage = ApexPages.currentPage().getParameters().get('stage');
            String voucherOuCPF = ApexPages.currentPage().getParameters().get('valor');
            
            // recupera os leads com valor passado (CPF ou Cod Promocional)
            List<CampaignMember> lsCampMember = Database.query( 
                'SELECT Id, CampaignId, Campaign.Name, Campaign.Refundable__c, Lead.Name, Lead.CPF_CNPJ__c, ' +
                'Lead.Voucher_Protocol__c, Campaign.Description, Campaign.EndDate ' +
                'FROM CampaignMember WHERE ' + 
                (oppCPF != NULL && !oppCPF.equalsIgnoreCase(voucherOuCPF) ? 'Campaign.Available_to_Third_Parties__c = TRUE AND ' : '') +
                '(Lead.CPF_CNPJ__c = \''+ voucherOuCPF + '\' OR Lead.Voucher_Protocol__c = \'' + voucherOuCPF + '\') ' +
                (stage != NULL && stage.equalsIgnoreCase('In Attendance') ? 
                 'AND Campaign.Billing_Required__c = FALSE AND Campaign.Refundable__c = FALSE ' : '') +
                'AND Lead.Status = \'To Rescue\' AND Campaign.IsActive = TRUE AND Campaign.Status = \'In Progress\' ' +
                'AND Lead.RecordTypeId = \'' + Utils.getRecordTypeId('Lead', 'Voucher') + '\''
            );
            
            system.debug('lsCampMember: '+lsCampMember);
            
            campMemberList = new List<CampaignMember>();
            campMemberList.addAll(lsCampMember);
    }
    
    public PageReference resgatar(){
        
        String oppId = ApexPages.currentPage().getParameters().get('Id');
        String chassi = ApexPages.currentPage().getParameters().get('chassi');
        String cmId = ApexPages.currentPage().getParameters().get('cmId');
        String NF = ApexPages.currentPage().getParameters().get('nf');
        
        system.debug('oppId: '+oppId+' cmId: '+cmId+' chassi: '+chassi+' NF: '+NF);
        
        Lead lead = [SELECT Id FROM Lead WHERE Id IN (SELECT LeadId FROM CampaignMember WHERE Id =: cmId) LIMIT 1];
        
        system.debug('lead: '+lead);
        
        Lead newLead = new Lead();
        newLead.Id = lead.Id;
        newLead.Status = 'Rescued';
        newLead.Related_Opportunity__c = oppId;
        newLead.VIN__c = chassi != NULL && chassi != '' ? chassi : NULL;
        newLead.Vehicle_Invoice__c = NF != NULL && NF != '' ? NF : NULL;
        Update newLead;
        
        PageReference pg = new PageReference('/apex/PromotionalActions');
        return pg;
    }
    
    public PageReference loadPagePromotionalActions(){
        PageReference pg = new PageReference('/apex/PromotionalActions');
        return pg;
    }
    
    public PageReference loadVoucherRescuedPassPage(){
        String cmId = ApexPages.currentPage().getParameters().get('cmId');
        PageReference pg = new PageReference('/apex/VoucherRescuedPassing?cmId='+cmId);
        return pg;
    }
    
    public PageReference loadGetVoucherConsult(){
        System.debug('Entrou no redirecionamento');
        String cmId = ApexPages.currentPage().getParameters().get('cmId');
        System.debug('#### cmId:' + cmId);
        
        CampaignMember cm = [SELECT Id, Lead.CPF_CNPJ__c
                             FROM CampaignMember
                             WHERE Id =: cmId];
        
        System.debug('CPF: ' + cm.Lead.CPF_CNPJ__c);
    	
    
        PageReference pg = new PageReference('/apex/getvoucher?CPF='+cm.Lead.CPF_CNPJ__c);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference loadPagePesquisSfa(){
        String cmId = ApexPages.currentPage().getParameters().get('cmId');
        PageReference pg = new PageReference('/apex/PesquisaSfa2?cmId='+cmId);
        pg.setRedirect(true);
        return pg;
    }
    
}