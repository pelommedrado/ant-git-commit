@isTest(seealldata=true)
public with sharing class TestVFC05_EditPageRedirect
 
{
    static testMethod void testTestVFC05_EditPageRedirect()
    
    {
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        
        Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
        
        Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        
        Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
        insert c;
      
         Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
        
         //Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='TECHNICAL' limit 1].Id;
        Goodwill__c g = new Goodwill__c (case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        insert g;
        
        Test.StartTest();
      
         ApexPages.currentPage().getParameters().put('Id',g.Id);
         ApexPages.currentPage().getParameters().put('retURL',c.id);
         ApexPages.StandardController controller=new ApexPages.StandardController(g);
         VFC05_EditPageRedirect tst=new VFC05_EditPageRedirect(controller);
         tst.redirectToPage();
     
          Id RecordId1= [select Id from RecordType where sObjectType='Goodwill__c' and Name='COMMERCIAL' limit 1].Id;
           Goodwill__c g1 = new Goodwill__c (case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId1,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        insert g1;
        
      
      
         ApexPages.currentPage().getParameters().put('Id',g1.Id);
         ApexPages.currentPage().getParameters().put('retURL',c.id);
         ApexPages.StandardController controller1=new ApexPages.StandardController(g1);
         VFC05_EditPageRedirect tst1=new VFC05_EditPageRedirect(controller1);
         tst1.redirectToPage();
         Id RecordId2= [select Id from RecordType where sObjectType='Goodwill__c' and Name='TECHNICAL' limit 1].Id;
           Goodwill__c g2 = new Goodwill__c (case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId2,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        insert g2;
        
      
      
         ApexPages.currentPage().getParameters().put('Id',g2.Id);
         ApexPages.currentPage().getParameters().put('retURL',c.id);
         ApexPages.StandardController controller2=new ApexPages.StandardController(g2);
         VFC05_EditPageRedirect tst2=new VFC05_EditPageRedirect(controller2);
         tst2.redirectToPage();
         Test.StopTest();
    }
 }