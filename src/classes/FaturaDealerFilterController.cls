public class FaturaDealerFilterController {
    
    public static final Id validRecordTypeId 	= Utils.getRecordTypeId('FaturaDealer__c', 'Valid');
    public static final Id invalidRecordTypeId 	= Utils.getRecordTypeId('FaturaDealer__c', 'Invalid');
    
    public FaturaDealer__c fDealer 	{ get;set; }
    public String situacao 			{ get;set; }
    
    public List<FaturaDealer__c> fatList { get; set; }
    
    private Date dataNfIni { get; set; }
    private Date dataNfFim { get; set; }
    private Date dataEnvIni { get; set; }
    private Date dataEnvFim { get; set; }
    
    public List<SelectOption> regionalList { 
        get {
            List<SelectOption> temp = new List<SelectOption>(); 
            temp.add(new SelectOption('R1', 'R1'));
            temp.add(new SelectOption('R2', 'R2'));
            temp.add(new SelectOption('R3', 'R3'));
            temp.add(new SelectOption('R4', 'R4'));
            temp.add(new SelectOption('R5', 'R5'));
            temp.add(new SelectOption('R6', 'R6'));
            return temp;
        }
    }
    
    public List<SelectOption> setorList { 
        get {
            List<SelectOption> temp = new List<SelectOption>(); 
            temp.add(new SelectOption('S1', 'S1'));
            temp.add(new SelectOption('S2', 'S2'));
            temp.add(new SelectOption('S3', 'S3'));
            temp.add(new SelectOption('S4', 'S4'));
            temp.add(new SelectOption('S5', 'S5'));
            temp.add(new SelectOption('S6', 'S6'));
            temp.add(new SelectOption('S7', 'S7'));
            
            return temp;
        }
    }
    
    public List<SelectOption> tipoRegList { 
        get {
            List<SelectOption> temp = new List<SelectOption>(); 
            temp.add(new SelectOption('10', '10 – Registro de NF'));
            temp.add(new SelectOption('99', '99 – Informações de Entrega'));
            return temp;
        }
    }
    
    public List<SelectOption> codList { 
        get { 
            List<SelectOption> temp = new List<SelectOption>(); 
            temp.add(new SelectOption('19', '19 - Passagem Oficina'));
            temp.add(new SelectOption('21', '21 - Vendas Diretas'));
            temp.add(new SelectOption('22', '22 - Vendas E-commerce'));
            temp.add(new SelectOption('25', '25 - Venda de Veículo Novo'));
            temp.add(new SelectOption('26', '26 - Passagem Oficina Outras Marcas'));
            temp.add(new SelectOption('44', '44 - Seminovos'));
            temp.add(new SelectOption('66', '66 - Vendas Peças e Acessórios'));
            temp.add(new SelectOption('67', '67 - Outras Notas Fiscais'));
            temp.add(new SelectOption('98', '98 - Atualização de Cadastro'));
            return temp;
        } 
    }
    
    public List<SelectOption> sitOptList { 
        get {
            List<SelectOption> temp = new List<SelectOption>();
            temp.add(new SelectOption(validRecordTypeId, 'Válido'));
            temp.add(new SelectOption(invalidRecordTypeId, 'Inválido'));
            return temp;
        } 
    }
    public FaturaDealerFilterController() {
        limparFiltro();
    }
    
    public PageReference limparFiltro() {
        fatList = new List<FaturaDealer__c>();
        fDealer = new FaturaDealer__c();
        fDealer.NFBIREMI_Mirror__r = new Account();
        return null;
    }
    
    public PageReference filtrarFat() {
        dataNfIni = fDealer.NFDTAORI_Mirror__c;
        dataNfFim = fDealer.NFDTANFI_Mirror__c;
        
        if(dataNfIni != null) {
            Integer d = dataNfIni.daysBetween(Datetime.now().date());
            if(d > 214) {
                Apexpages.addMessage(new Apexpages.Message( 
                    ApexPages.Severity.WARNING, 
                    'A data mínima para início da análise é 01/' + Datetime.now().addMonths(-6).format('MM/YYYY') ));
                return null;
            }
        }
        
        if(dataNfFim != null) {
            Integer d = dataNfFim.daysBetween(Datetime.now().date());
            if(d < 0) {
                Apexpages.addMessage(new Apexpages.Message( 
                    ApexPages.Severity.WARNING, 
                    'A data máxima para término da análise é ' + Datetime.now().format('dd/MM/YYYY')));
            }
        }
        
        String query = montarQueryFiltro();
        System.debug('Query:' + query);
        this.fatList = Database.Query(query);
        
        return null;
    }
    
    public PageReference exportarFat() {
        System.debug('Exportando Fatura situacao: ' + situacao);
        
        if(String.isEmpty(situacao)) {
            return null;
        }
        
        try {
            String query = montarQueryExportar();
            System.debug('Query Exportar:' + query);
            
            List<FaturaDealer__c> faturas =  Database.Query(query);
            
            String body = null;
            PageReference pageRef = null;
            
            if(situacao.equals(validRecordTypeId)) {
                body = gerarBodyValido(faturas);
                
            } else if(situacao.equals(invalidRecordTypeId)) {
                body = gerarBodyInvalido(faturas);
            }
            
            String fileName = 'Fatura';
            Id folderId = [SELECT Id FROM Folder WHERE Name = 'Shared Documents'].Id;
            DELETE [SELECT Id FROM Document WHERE Name =: fileName];
            
            Document dn = new Document(
                Name = fileName,
                FolderId = folderId,
                Body = Blob.valueOf(body),
                ContentType = 'text/csv;charset=UTF-8;',
                Type = 'csv'
            );
            
            INSERT dn;
            
            PageReference pg = new PageReference('/servlet/servlet.FileDownload?file=' + dn.Id);
            pg.setRedirect(true);
            return pg;
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
            Apexpages.addMessage(new Apexpages.Message( 
                ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    private String montarQueryFiltro() {
        dataNfIni = fDealer.NFDTAORI_Mirror__c;
        dataNfFim = fDealer.NFDTANFI_Mirror__c;
        dataEnvIni = fDealer.NFDTAENT_Mirror__c;
        dataEnvFim = fDealer.NFDTASAI_Mirror__c;
        
        return 
            'SELECT Id, '   + 
            'Name,'                 + 
            'Error_Messages__c,'    +
            'NFTIPREG__c,'          +
            'NFBIRENV__c,'          +
            'NFBIREMI__c,'          +
            'NFCODOPR__c,'          + 
            'NFTIPONF__c,'          +
            'NFCHASSI__c,'          +
            'NFANOFAB__c,'          +
            'NFANOMOD__c,'          +
            'NFCPFCGC__c,'          +
            'NFEMAILS__c,'          +
            'NFDDDRES__c,'          +   
            'NFTELRES__c,'          +
            'NFDDDCEL__c,'          + 
            'NFTELCEL__c,'          +
            'NFNRONFI__c,'			+
            'NFDTANFI_Mirror__c,'   +
            'NFDTASIS_Mirror__c '   +
            'FROM FaturaDealer__c ' +
            
            'WHERE Id != null AND Reprocessed__c != true ' +
            (String.isNotBlank(fDealer.NFBIRENV__c)  ? 'AND NFBIRENV__c LIKE \'%' 	+ fDealer.NFBIRENV__c  + '%\'' : '' )  +
            (String.isNotBlank(fDealer.NFBIREMI__c)  ? 'AND NFBIREMI__c LIKE \'%' 	+ fDealer.NFBIREMI__c  + '%\'' : '' )  +
            (String.isNotBlank(fDealer.NFTIPREG__c)  ? 'AND NFTIPREG__c = \''  		+ fDealer.NFTIPREG__c  + '\' ' : '' )  +
            (String.isNotBlank(fDealer.NFCODOPR__c)  ? 'AND NFCODOPR__c = \''  		+ fDealer.NFCODOPR__c  + '\' ' : '' )  +
            (String.isNotBlank(situacao)    ? 'AND RecordTypeId = \'' + situacao    + '\' ' : '' )  +
            
            (String.isNotBlank(fDealer.NFBIREMI_Mirror__r.NameZone__c)  ? 'AND NFBIREMI_Mirror__r.NameZone__c = \''  		+ fDealer.NFBIREMI_Mirror__r.NameZone__c  + '\' ' : '' )  +
            (String.isNotBlank(fDealer.NFBIREMI_Mirror__r.RegionalSector__c)  ? 'AND NFBIREMI_Mirror__r.RegionalSector__c = \''  		+ fDealer.NFBIREMI_Mirror__r.RegionalSector__c  + '\' ' : '' )  +
            
            (dataNfIni != null && dataNfFim != null     ? 
             'AND NFDTANFI_Mirror__c >=: dataNfIni AND NFDTANFI_Mirror__c <=: dataNfFim ' : '' )  +
            (dataEnvIni != null && dataEnvFim != null   ? 
             'AND NFDTASIS_Mirror__c >=: dataEnvIni AND NFDTASIS_Mirror__c <=: dataEnvFim ' : '' )    +
            ' LIMIT 700';
    }
    
    private String montarQueryExportar() {
        Date dataNfIni = fDealer.NFDTAORI_Mirror__c;
        Date dataNfFim = fDealer.NFDTANFI_Mirror__c;
        
        Date dataEnvIni = fDealer.NFDTAENT_Mirror__c;
        Date dataEnvFim = fDealer.NFDTASAI_Mirror__c;
        
        return 'SELECT Id, ' +  
            'Error_Messages__c,'  + 'NFBIRENV_Mirror__c,' +
            'NFBIREMI_Mirror__c,' + 'NFCODOPR_Mirror__c,' +
            'NFTIPONF_Mirror__c,' + 'NFDTANFI_Mirror__c,' +
            'NFDTANAS_Mirror__c,' + 'NFCPFCGC_Mirror__c,' +
            'NFEMAILS_Mirror__c,' + 'NFDDDRES_Mirror__c,' +
            'NFTELRES_Mirror__c,' + 'NFDDDCEL_Mirror__c,' +
            'NFTELCEL_Mirror__c,' + 'NFCHASSI_Mirror__c,' +
            'NFANOFAB_Mirror__c,' + 'NFANOMOD_Mirror__c,' +
            'NFDTAENT_Mirror__c,' + 'NFDTASAI_Mirror__c,' +
            'NFDTAORI_Mirror__c,' + 'NFDTAECL_Mirror__c,' +
            'NFDTAPVE_Mirror__c,' + 'NFDTASIS_Mirror__c,' +
            'NFTIPREG__c,' + 'NFBIRENV__c,' + 'NFBIREMI__c,' + 'NFSEQUEN__c,' +
            'NFNROREG__c,' + 'NFBANDEI__c,' + 'NFCODOPR__c,' + 'NFCODFIS__c,' +
            'NFTIPONF__c,' + 'NFNRONFI__c,' + 'NFSERNFI__c,' + 'NFDTANFI__c,' +
            'NFNOMCLI__c,' + 'NFTIPVIA__c,' + 'NFNOMVIA__c,' + 'NFNROVIA__c,' +
            'NFCPLEND__c,' + 'NFBAIRRO__c,' + 'NFCIDADE__c,' + 'NFNROCEP__c,' +
            'NFESTADO__c,' + 'NFPAISRE__c,' + 'NFESTCIV__c,' + 'NFSEXOMF__c,' +
            'NFDTANAS__c,' + 'NFTIPCLI__c,' + 'NFCPFCGC__c,' + 'NFINSEST__c,' +
            'NFPROFIS__c,' + 'NFEMAILS__c,' + 'NFDDDRES__c,' + 'NFTELRES__c,' +
            'NFDDDCOM__c,' + 'NFTELCOM__c,' + 'NFDDDCEL__c,' + 'NFTELCEL__c,' +
            'NFDDDFAX__c,' + 'NFNROFAX__c,' + 'NFNROFIL__c,' + 'NFHOBBYS__c,' +
            'NFFAISAL__c,' + 'NFGRAINS__c,' + 'NFCONCLI__c,' + 'NFRAMATV__c,' +
            'NFNROEFE__c,' + 'NFFATURA__c,' + 'NFTIPFRT__c,' + 'NFNROFRT__c,' +
            'NFPERFRT__c,' + 'NFANOCON__c,' + 'NFMOLICA__c,' + 'NFCHASSI__c,' +
            'NFPLACAS__c,' + 'NFANOFAB__c,' + 'NFANOMOD__c,' + 'NFIDFCOR__c,' +
            'NFCPFVEN__c,' + 'NFNOVUSA__c,' + 'NFSEGURO__c,' + 'NFQUILOM__c,' +
            'NFFORPAG__c,' + 'NFPRZFIN__c,' + 'NFNOMARE__c,' + 'NFVEITRO__c,' +
            'NFMOLTRO__c,' + 'NFANOTRO__c,' + 'NFEQUIPE__c,' + 'NFNRONOS__c,' +
            'NFDTAENT__c,' + 'NFDTASAI__c,' + 'NFQTITENS__c,' +
            'NFNROORI__c,' + 'NFSERORI__c,' + 'NFDTAORI__c,' + 'NFDTAECL__c,' +
            'NFBIRECL__c,' + 'NFDTAPVE__c,' + 'NFFILLER__c,' + 'TELNOVDG__c,' +
            'NFDTASIS__c ' +
            'FROM FaturaDealer__c ' +
            
            'WHERE Id != null AND Reprocessed__c != true ' +
            (String.isNotBlank(fDealer.NFBIRENV__c)  ? 'AND NFBIRENV__c LIKE \'%' 	+ fDealer.NFBIRENV__c  + '%\'' : '' )  +
            (String.isNotBlank(fDealer.NFBIREMI__c)  ? 'AND NFBIREMI__c LIKE \'%' 	+ fDealer.NFBIREMI__c  + '%\'' : '' )  +
            (String.isNotBlank(fDealer.NFTIPREG__c)  ? 'AND NFTIPREG__c = \''  		+ fDealer.NFTIPREG__c  + '\' ' : '' )  +
            (String.isNotBlank(fDealer.NFCODOPR__c)  ? 'AND NFCODOPR__c = \''  		+ fDealer.NFCODOPR__c  + '\' ' : '' )  +
            (String.isNotBlank(situacao)    ? 'AND RecordTypeId = \'' + situacao    + '\' ' : '' )  +
            
            (String.isNotBlank(fDealer.NFBIREMI_Mirror__r.NameZone__c)  ? 'AND NFBIREMI_Mirror__r.NameZone__c = \''  		+ fDealer.NFBIREMI_Mirror__r.NameZone__c  + '\' ' : '' )  +
            (String.isNotBlank(fDealer.NFBIREMI_Mirror__r.RegionalSector__c)  ? 'AND NFBIREMI_Mirror__r.RegionalSector__c = \''  		+ fDealer.NFBIREMI_Mirror__r.RegionalSector__c  + '\' ' : '' )  +
            
            
            (dataNfIni != null && dataNfFim != null     ? 
             'AND NFDTANFI_Mirror__c >=: dataNfIni AND NFDTANFI_Mirror__c <=: dataNfFim ' : '' )  +
            (dataEnvIni != null && dataEnvFim != null   ? 
             'AND NFDTASIS_Mirror__c >=: dataEnvIni AND NFDTASIS_Mirror__c <=: dataEnvFim ' : '' )    +
            ' LIMIT 1500';
    }
    
    private List<String> gerarListaCampoValido() {
        List<String> fieldList = new List<String>();
        
        fieldList.add(FaturaDealer__c.NFTIPREG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIRENV_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIREMI_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEQUEN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROREG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBANDEI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCODOPR_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCODFIS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPONF_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNRONFI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSERNFI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTANFI_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPLEND__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBAIRRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCIDADE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROCEP__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFESTADO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPAISRE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFESTCIV__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEXOMF__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTANAS_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPFCGC_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFINSEST__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPROFIS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFEMAILS_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDRES_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELRES_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDCOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELCOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDCEL_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELCEL_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDFAX__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFAX__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFIL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFHOBBYS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFAISAL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFGRAINS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCONCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFRAMATV__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROEFE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFATURA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPERFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOCON__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFMOLICA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCHASSI_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPLACAS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOFAB_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOMOD_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFIDFCOR__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPFVEN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOVUSA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEGURO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFQUILOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFORPAG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPRZFIN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMARE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFVEITRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFMOLTRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOTRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFEQUIPE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNRONOS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAENT_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTASAI_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFQTITENS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROORI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSERORI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAORI_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAECL_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIRECL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAPVE_Mirror__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFILLER__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.TELNOVDG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTASIS_Mirror__c.getDescribe().getLabel());
        
        return fieldList;
    }
    
    private String gerarBodyValido(List<FaturaDealer__c> fatList) {
        
        List<String> fieldsList = gerarListaCampoValido();
        List<String> fieldsHeaderList = new List<String>();
        for(String stg : fieldsList) {
            fieldsHeaderList.add(format(stg));
        }
        
        String result = criarLine(fieldsHeaderList);
        
        for(FaturaDealer__c fat : fatList) {
            List<String> fieldList = new List<String>();            
            fieldList.add(format(fat.NFTIPREG__c));
            fieldList.add(format(fat.NFBIRENV_Mirror__c));
            fieldList.add(format(fat.NFBIREMI_Mirror__c));
            fieldList.add(format(fat.NFSEQUEN__c));
            fieldList.add(format(fat.NFNROREG__c));
            fieldList.add(format(fat.NFBANDEI__c));
            fieldList.add(format(fat.NFCODOPR_Mirror__c));
            fieldList.add(format(fat.NFCODFIS__c));
            fieldList.add(format(fat.NFTIPONF_Mirror__c));
            fieldList.add(format(fat.NFNRONFI__c));
            fieldList.add(format(fat.NFSERNFI__c));
            fieldList.add(format(fat.NFDTANFI_Mirror__c));
            fieldList.add(format(fat.NFNOMCLI__c));
            fieldList.add(format(fat.NFTIPVIA__c));
            fieldList.add(format(fat.NFNOMVIA__c));
            fieldList.add(format(fat.NFNROVIA__c));
            fieldList.add(format(fat.NFCPLEND__c));
            fieldList.add(format(fat.NFBAIRRO__c));
            fieldList.add(format(fat.NFCIDADE__c));
            fieldList.add(format(fat.NFNROCEP__c));
            fieldList.add(format(fat.NFESTADO__c));
            fieldList.add(format(fat.NFPAISRE__c));
            fieldList.add(format(fat.NFESTCIV__c));
            fieldList.add(format(fat.NFSEXOMF__c));
            fieldList.add(format(fat.NFDTANAS_Mirror__c));
            fieldList.add(format(fat.NFTIPCLI__c));
            fieldList.add(format(fat.NFCPFCGC_Mirror__c));
            fieldList.add(format(fat.NFINSEST__c));
            fieldList.add(format(fat.NFPROFIS__c));
            fieldList.add(format(fat.NFEMAILS_Mirror__c));
            fieldList.add(format(fat.NFDDDRES_Mirror__c));
            fieldList.add(format(fat.NFTELRES_Mirror__c));
            fieldList.add(format(fat.NFDDDCOM__c));
            fieldList.add(format(fat.NFTELCOM__c));
            fieldList.add(format(fat.NFDDDCEL_Mirror__c));
            fieldList.add(format(fat.NFTELCEL_Mirror__c));
            fieldList.add(format(fat.NFDDDFAX__c));
            fieldList.add(format(fat.NFNROFAX__c));
            fieldList.add(format(fat.NFNROFIL__c));
            fieldList.add(format(fat.NFHOBBYS__c));
            fieldList.add(format(fat.NFFAISAL__c));
            fieldList.add(format(fat.NFGRAINS__c));
            fieldList.add(format(fat.NFCONCLI__c));
            fieldList.add(format(fat.NFRAMATV__c));
            fieldList.add(format(fat.NFNROEFE__c));
            fieldList.add(format(fat.NFFATURA__c));
            fieldList.add(format(fat.NFTIPFRT__c));
            fieldList.add(format(fat.NFNROFRT__c));
            fieldList.add(format(fat.NFPERFRT__c));
            fieldList.add(format(fat.NFANOCON__c));
            fieldList.add(format(fat.NFMOLICA__c));
            fieldList.add(format(fat.NFCHASSI_Mirror__c));
            fieldList.add(format(fat.NFPLACAS__c));
            fieldList.add(format(fat.NFANOFAB_Mirror__c));
            fieldList.add(format(fat.NFANOMOD_Mirror__c));
            fieldList.add(format(fat.NFIDFCOR__c));
            fieldList.add(format(fat.NFCPFVEN__c));
            fieldList.add(format(fat.NFNOVUSA__c));
            fieldList.add(format(fat.NFSEGURO__c));
            fieldList.add(format(fat.NFQUILOM__c));
            fieldList.add(format(fat.NFFORPAG__c));
            fieldList.add(format(fat.NFPRZFIN__c));
            fieldList.add(format(fat.NFNOMARE__c));
            fieldList.add(format(fat.NFVEITRO__c));
            fieldList.add(format(fat.NFMOLTRO__c));
            fieldList.add(format(fat.NFANOTRO__c));
            fieldList.add(format(fat.NFEQUIPE__c));
            fieldList.add(format(fat.NFNRONOS__c));
            fieldList.add(format(fat.NFDTAENT_Mirror__c));
            fieldList.add(format(fat.NFDTASAI_Mirror__c));
            fieldList.add(format(fat.NFQTITENS__c));
            fieldList.add(format(fat.NFNROORI__c));
            fieldList.add(format(fat.NFSERORI__c));
            fieldList.add(format(fat.NFDTAORI_Mirror__c));
            fieldList.add(format(fat.NFDTAECL_Mirror__c));
            fieldList.add(format(fat.NFBIRECL__c));
            fieldList.add(format(fat.NFDTAPVE_Mirror__c));
            fieldList.add(format(fat.NFFILLER__c));
            fieldList.add(format(fat.TELNOVDG__c));
            fieldList.add(format(fat.NFDTASIS_Mirror__c));
            
            result += criarLine(fieldList);
        }
        return result;
    }
    
    private List<String> gerarListaCampoInvalido() {
        List<String> fieldList = new List<String>();
        
        fieldList.add(FaturaDealer__c.NFTIPREG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIRENV__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIREMI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEQUEN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROREG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBANDEI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCODOPR__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCODFIS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPONF__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNRONFI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSERNFI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTANFI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROVIA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPLEND__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBAIRRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCIDADE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROCEP__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFESTADO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPAISRE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFESTCIV__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEXOMF__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTANAS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPFCGC__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFINSEST__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPROFIS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFEMAILS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDRES__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELRES__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDCOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELCOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDCEL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTELCEL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDDDFAX__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFAX__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFIL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFHOBBYS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFAISAL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFGRAINS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCONCLI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFRAMATV__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROEFE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFATURA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFTIPFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPERFRT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOCON__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFMOLICA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCHASSI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPLACAS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOFAB__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOMOD__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFIDFCOR__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFCPFVEN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOVUSA__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSEGURO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFQUILOM__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFORPAG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFPRZFIN__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNOMARE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFVEITRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFMOLTRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFANOTRO__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFEQUIPE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNRONOS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAENT__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTASAI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFQTITENS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFNROORI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFSERORI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAORI__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAECL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFBIRECL__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTAPVE__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFFILLER__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.TELNOVDG__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.NFDTASIS__c.getDescribe().getLabel());
        fieldList.add(FaturaDealer__c.Error_Messages__c.getDescribe().getLabel());
        
        return fieldList;
    }
    
    private String gerarBodyInvalido(List<FaturaDealer__c> fatList) {
        
        List<String> fieldsList = gerarListaCampoInvalido();
        List<String> fieldsHeaderList = new List<String>();
        for(String stg : fieldsList) {
            fieldsHeaderList.add(format(stg));
        }
        
        String result = criarLine(fieldsHeaderList);
        
        for(FaturaDealer__c fat : fatList) {
            List<String> fieldList = new List<String>();
            fieldList.add(format(fat.NFTIPREG__c));
            fieldList.add(format(fat.NFBIRENV__c));
            fieldList.add(format(fat.NFBIREMI__c));
            fieldList.add(format(fat.NFSEQUEN__c));
            fieldList.add(format(fat.NFNROREG__c));
            fieldList.add(format(fat.NFBANDEI__c));
            fieldList.add(format(fat.NFCODOPR__c));
            fieldList.add(format(fat.NFCODFIS__c));
            fieldList.add(format(fat.NFTIPONF__c));
            fieldList.add(format(fat.NFNRONFI__c));
            fieldList.add(format(fat.NFSERNFI__c));
            fieldList.add(format(fat.NFDTANFI__c));
            fieldList.add(format(fat.NFNOMCLI__c));
            fieldList.add(format(fat.NFTIPVIA__c));
            fieldList.add(format(fat.NFNOMVIA__c));
            fieldList.add(format(fat.NFNROVIA__c));
            fieldList.add(format(fat.NFCPLEND__c));
            fieldList.add(format(fat.NFBAIRRO__c));
            fieldList.add(format(fat.NFCIDADE__c));
            fieldList.add(format(fat.NFNROCEP__c));
            fieldList.add(format(fat.NFESTADO__c));
            fieldList.add(format(fat.NFPAISRE__c));
            fieldList.add(format(fat.NFESTCIV__c));
            fieldList.add(format(fat.NFSEXOMF__c));
            fieldList.add(format(fat.NFDTANAS__c));
            fieldList.add(format(fat.NFTIPCLI__c));
            fieldList.add(format(fat.NFCPFCGC__c));
            fieldList.add(format(fat.NFINSEST__c));
            fieldList.add(format(fat.NFPROFIS__c));
            fieldList.add(format(fat.NFEMAILS__c));
            fieldList.add(format(fat.NFDDDRES__c));
            fieldList.add(format(fat.NFTELRES__c));
            fieldList.add(format(fat.NFDDDCOM__c));
            fieldList.add(format(fat.NFTELCOM__c));
            fieldList.add(format(fat.NFDDDCEL__c));
            fieldList.add(format(fat.NFTELCEL__c));
            fieldList.add(format(fat.NFDDDFAX__c));
            fieldList.add(format(fat.NFNROFAX__c));
            fieldList.add(format(fat.NFNROFIL__c));
            fieldList.add(format(fat.NFHOBBYS__c));
            fieldList.add(format(fat.NFFAISAL__c));
            fieldList.add(format(fat.NFGRAINS__c));
            fieldList.add(format(fat.NFCONCLI__c));
            fieldList.add(format(fat.NFRAMATV__c));
            fieldList.add(format(fat.NFNROEFE__c));
            fieldList.add(format(fat.NFFATURA__c));
            fieldList.add(format(fat.NFTIPFRT__c));
            fieldList.add(format(fat.NFNROFRT__c));
            fieldList.add(format(fat.NFPERFRT__c));
            fieldList.add(format(fat.NFANOCON__c));
            fieldList.add(format(fat.NFMOLICA__c));
            fieldList.add(format(fat.NFCHASSI__c));
            fieldList.add(format(fat.NFPLACAS__c));
            fieldList.add(format(fat.NFANOFAB__c));
            fieldList.add(format(fat.NFANOMOD__c));
            fieldList.add(format(fat.NFIDFCOR__c));
            fieldList.add(format(fat.NFCPFVEN__c));
            fieldList.add(format(fat.NFNOVUSA__c));
            fieldList.add(format(fat.NFSEGURO__c));
            fieldList.add(format(fat.NFQUILOM__c));
            fieldList.add(format(fat.NFFORPAG__c));
            fieldList.add(format(fat.NFPRZFIN__c));
            fieldList.add(format(fat.NFNOMARE__c));
            fieldList.add(format(fat.NFVEITRO__c));
            fieldList.add(format(fat.NFMOLTRO__c));
            fieldList.add(format(fat.NFANOTRO__c));
            fieldList.add(format(fat.NFEQUIPE__c));
            fieldList.add(format(fat.NFNRONOS__c));
            fieldList.add(format(fat.NFDTAENT__c));
            fieldList.add(format(fat.NFDTASAI__c));
            fieldList.add(format(fat.NFQTITENS__c));
            fieldList.add(format(fat.NFNROORI__c));
            fieldList.add(format(fat.NFSERORI__c));
            fieldList.add(format(fat.NFDTAORI__c));
            fieldList.add(format(fat.NFDTAECL__c));
            fieldList.add(format(fat.NFBIRECL__c));
            fieldList.add(format(fat.NFDTAPVE__c));
            fieldList.add(format(fat.NFFILLER__c));
            fieldList.add(format(fat.TELNOVDG__c));
            fieldList.add(format(fat.NFDTASIS__c));
            fieldList.add(format(fat.Error_Messages__c));
            
            result += criarLine(fieldList);
        }
        return result;
    }
    
    private String criarLine(List<String> fieldsList) {
        return String.join(fieldsList, ';') + '\r\n';
    }
    
    private String format(String s) {
        return s != null ? '"' + s + '"' : '""';
    }
    
    private String format(Date d)    {
        return d != null ? '"' + d.format().escapeCsv() + '"' : '""';
    }
    
    private String format(Decimal d, Integer scale) {
        return d != null ? String.valueOf(d.setScale(scale)) : '';
    }
    
    private String format(Decimal d) {
        return format(d, 2);
    }
}