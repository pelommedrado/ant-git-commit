/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class QueueItemRequest {
    @InvocableVariable(label='Scheduled Item Id' description='Identifier of scheduled item.' required=true)
    global Id ScheduledItemId;
    global QueueItemRequest() {

    }
}
