@isTest
private class VFC145_OfferWizardControllerTest {
    
    static testMethod void deveCriarVFC145_OfferWizardController() {
        User manager;
        System.runAs(new User(Id = UserInfo.getUserId())){
            manager = criarUser();
        }
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            
            String cdc = clt.CDC;
            System.assert(cdc != null);
            
            String options = clt.options;
            System.assert(options != null);
            
            String pref = clt.groupOfferIdPrefix;
            System.assert(pref != null);
            
            String st = clt.stateSelectOptionString;
            System.assert(st != null);
        }
    }
    
    static testMethod void deveLoadMilesimes(){
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        
        List< String > milesimeList = VFC145_OfferWizardController.loadMilesimes(model.Id);
        System.assert(!milesimeList.isEmpty());
    }
    
    static testMethod void deveLoadVersions() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        
        List< PVVersion__c > versionList = VFC145_OfferWizardController.loadVersions(model.Id, milesime.Milesime__c);
        System.assert(!versionList.isEmpty());
    }
    
    static testMethod void deveLoadOptionals() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        Map<String, List<Optional__c >> optMap = 
            VFC145_OfferWizardController.loadOptionals(version.Id);
        System.assert(!optMap.isEmpty());
    }
    
    static testMethod void deveLoadCommercialActions() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        PVCommercial_Action__c commAction = new PVCommercial_Action__c(
            Model__c = model.Id,
            Start_Date__c = System.today().addDays( -5 ),
            End_Date__c = System.today().addDays( 5 ),
            Name = 'Commercial Action',
            Status__c = 'Active'
        );
        Database.insert( commAction );
        
        List< PVCommercial_Action__c > commList = 
            VFC145_OfferWizardController.loadCommercialActions(model.Id, System.today(), System.today());
        System.assert(!commList.isEmpty());
    }
    
    static testMethod void deveLoadCallOffers() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        PVCommercial_Action__c commAction = criarPVCommercial(model);
        
        PVCall_Offer__c callOffer = devePVCallOffer(commAction);
        
        List< PVCall_Offer__c >  callList = 
            VFC145_OfferWizardController.loadCallOffers(
                commAction.Id, version.Id ,System.today(), System.today() );
        //System.assert(!callList.isEmpty());
    }
    
    static testmethod void deveLoadCallOfferDetail() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        PVCommercial_Action__c commAction = criarPVCommercial(model);
        
        PVCall_Offer__c callOffer = devePVCallOffer(commAction);
        
        PVCall_Offer__c db = 
            VFC145_OfferWizardController.loadCallOfferDetail( callOffer.Id );
        System.assert(db != null);
    }
    
    static testMethod void deveLoadOptionalFeatures() {
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        Map<String, Object > mapObj = 
            VFC145_OfferWizardController.loadOptionalFeatures( optList[0].Id );
        System.assert(!mapObj.isEmpty());
    }
    
    
    static testMethod void deveSelectRegional() {
        
        User manager = criarUser();
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            List<String> regionalList = clt.regionalSelectOptionList;
            
            List< String > selectRegional  = 
                VFC145_OfferWizardController.selectRegional(regionalList);
            
            System.assert(!selectRegional.isEmpty());
        }
    }
    
    static testMethod void deveSelectState() {
        User manager = criarUser();
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            List<String> regionalList = clt.regionalSelectOptionList;
            List<String> stateList = clt.stateSelectOptionList;
            
            List< String > selectState  = 
                VFC145_OfferWizardController.selectState(regionalList, stateList);
            
            //System.assert(!selectState.isEmpty());
        } 
    }
    
    static testMethod void deveSelectCity() {
        User manager = criarUser();
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            List<String> regionalList = clt.regionalSelectOptionList;
            List<String> stateList = clt.stateSelectOptionList;
            
            List< String > cityList  = 
                VFC145_OfferWizardController.selectState(regionalList, stateList);
            
            List< Account > accountList = 
                VFC145_OfferWizardController.selectCity(regionalList, stateList, cityList);
            //System.assert(!accountList.isEmpty());
        } 
    }
    
    static testMethod void deveLoadTvSignal() {
        User manager = criarUser();
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            
            List<String> stateList = clt.stateSelectOptionList;
            
            List< String > tvSignal  = 
                VFC145_OfferWizardController.loadTvSignal(stateList);
            
            //System.assert(!tvSignal.isEmpty());
        } 
    }
    
    
    static testMethod void deveSelectTvSignal() {
        User manager;
        System.runAs(new User(Id = UserInfo.getUserId())){
            manager = criarUser();
        }
        
        System.runAs(manager) {
            VFC145_OfferWizardController clt = new VFC145_OfferWizardController();
            
            List<String> stateList = clt.stateSelectOptionList;
            
            List< String > tvSignalList  = 
                VFC145_OfferWizardController.loadTvSignal(stateList);
            
            Map< String, List< Object > > mapaList = 
                VFC145_OfferWizardController.selectTvSignal(tvSignalList);
            System.assert(!mapaList.isEmpty());
        } 
    }
    
    static testMethod void deveSave() {
        Account acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Praça',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '123ABC123',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        PVCommercial_Action__c commAction = criarPVCommercial(model);
        
        PVCall_Offer__c callOffer = devePVCallOffer(commAction);
        
        Group_Offer__c groupOffer = new Group_Offer__c(
            Type_of_Offer__c = 'Cooperada',
            Number_Offer__c = '1',
            Type_of_Action__c = 'Internet',
            Date_Start_Offer__c = System.today().addDays( -1 ),
            Date_End_Offer__c = System.today().addDays( 1 )
        );
        
        
        Offer_Item__c pricingTemplate = new Offer_Item__c(
            Name = 'Pricing Template', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Pricing_Template'),
            Status__c = 'Active',
            Code__c = 'Pricing Template'
        );
        Database.insert(pricingTemplate);
        
        Offer_Item__c featuredInCampaign = new Offer_Item__c(
            Name = 'Featured in Campaign', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Featured_in_Campaign'),
            Status__c = 'Active',
            End_Date__c = System.today(),
            Start_Date__c = System.today(),
            Code__c = 'Featured in Campaign'
        );
        Database.insert(featuredInCampaign);
        
        Offer_Item__c featuredInOffer = new Offer_Item__c(
            Name = 'Featured In Offer', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Featured_in_Offer'),
            Status__c = 'Active',
            Code__c = 'Featured In Offer'
        );
        Database.insert(featuredInOffer);
        
        Offer_Item__c offerHeader = new Offer_Item__c(
            Name = 'Offer Header', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Offer_Header'),
            Status__c = 'Active',
            Code__c = 'Offer Header'
        );
        Database.insert(offerHeader);
        
        Offer_Item__c stamp = new Offer_Item__c(
            Name = 'Stamp', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Stamp'),
            Status__c = 'Active',
            Code__c = 'Stamp'
        );
        Database.insert(stamp);
        
        
        Offer__c offer = new Offer__c( 
            From_To_Offer__c = false,
            hasUpgrade__c = false,
            Offer_to_Internet__c = true,
            Total_Inventory_Vehicle__c = 3,
            Model__c = model.Id,
            Version__c = version.Id,
            Optional__c = optList[0].Id,
            Painting__c = optList[1].Id,
            Featured_Product_Text__c = 'air conditioning;steering;electric Lock',
            Condition__c = 'Financiado',
            Commercial_Action__c = commAction.Id,
            ValueTo__c = 40060,
            Minimum_Input__c = 75,
            Number_of_Installments__c = 36,
            Monthly_Tax__c = 0,
            Coefficient__c = 0.02861,
            Entry_Value__c = 30045,
            Installment_Value__c = 305.01,
            Stamp__c = 'Marketing of factory;Big marketing in factory',
            Offer_Start_Date_Website__c = System.today().addDays( -1 ),
            Offer_End_Date_Website__c = System.today().addDays( 1 ),
            Pricing_Template__c = 'Sight',
            Featured_In_Offer__c = 'Complete',
            Pricing_Template_Lookup__c = pricingTemplate.Id,
            Featured_in_Offer_Lookup__c = featuredInOffer.Id,
            Offer_Header__c = offerHeader.Id,
            Stamp_Lookup__c = Stamp.Id,
            Featured_in_Campaign__c = featuredInCampaign.Id
        );
        
        Group_Offer__c fauxGroupOffer = new Group_Offer__c(
            Date_Start_Offer__c = System.today().addDays( -1 ),
            Date_End_Offer__c = System.today().addDays( 1 )
        );
        
        Map< String, Object > moreInfo = new Map< String, Object >{
            'regional' => 'R2',
                'state' => 'Estado',
                'city' => 'Cidade',
                'siteDealerAccs' => acc.Id,
                'dealerAccs' => acc.Id,
                'PVCVal' => 0,
                'PVRVal' => 0
                };
                    
                    
                    Map< String, Object > saveResponseMap = 
                    VFC145_OfferWizardController.save( offer, fauxGroupOffer, moreInfo );
        
        System.assert((Boolean)saveResponseMap.get('success'), (String)saveResponseMap.get('errorMsg'));
        
        VFC145_OfferWizardController.loadGroupOffer((Id)saveResponseMap.get('groupOfferId'));
    }
    
    static testMethod void deveLoadModel() {
        Model__c model = criarModel();
        PVCommercial_Action__c commAction = criarPVCommercial(model);
        
        List< VFC145_OfferWizardController.SObjectWrapper > objWra = 
            VFC145_OfferWizardController.loadModels('Cooperada');
    }
    
    static testMethod void testLoadDealerAccountsDetail(){
        User manager = criarUser();
        
        List<Account> accList = [SELECT Id, Name FROM Account];
        List<Id> accIdList = new List<Id>();
        
        for(Account a : accList){
            accIdList.add(a.Id);
        }
        
        List<Account> accReturnList = VFC145_OfferWizardController.loadDealerAccountsDetail(accIdList);
        
        System.assertEquals(accList[0].Name, accReturnList[0].Name);
    }
    
    static testMethod void testLoadCoefficient(){
        PV_FinancingConditions__c fc = new PV_FinancingConditions__c(Coefficient__c = 4, 
                                                                     Month__c = 3, 
                                                                     Tax__c = 10.5, 
                                                                     Status__c = 'ACTIVE');
        Insert fc;
        
        PV_FinancingConditions__c PV_fc = VFC145_OfferWizardController.loadCoefficient(3, 10.5);
        Decimal Coefficient = PV_fc.Coefficient__c;
        
        System.assertEquals(4, Coefficient);
    }
    
    private static Model__c criarModel() {
        Model__c model = new Model__c(
            Id_Milesime__c = 'BACy',
            Market__c = 'Bresil',
            Model_Spec_Code__c = 'L4M',
            Phase__c = '1',
            Status__c = 'Active',
            Model_PK__c = 'L4M-1'
        );
        insert model ;
        
        return model;
    }
    
    private static PV_ModelMilesime__c criarMilesime (Model__c model){
        PV_ModelMilesime__c modelMilesime = new PV_ModelMilesime__c(
            Model__c = model.Id,
            Milesime__c = '2015/2016',
            ENS__c = 'ENS',
            Milesime_PK__c = model.Model_PK__c + 'ENS',
            Status__c = 'Active',
            Name = model.Name + ' 2015/2016'
        );
        
        insert modelMilesime;
        
        return modelMilesime;
    }
    
    private static PVVersion__c criarPVVersion (PV_ModelMilesime__c modelMilesime) {
        PVVersion__c version = new PVVersion__c(
            Milesime__c = modelMilesime.Id, 
            Model__c = modelMilesime.Model__c, 
            Version_Id_Spec_Code__c = 'ABC', 
            Price__c = 100000, 
            PVC_Maximo__c = 1.0, 
            PVR_Minimo__c = 1.0
        );
        Database.insert( version );
        
        return version;
    }
    
    private static List<Optional__c> criarOptional(PVVersion__c version) {
        List<Optional__c> optionalList = new List<Optional__c>();
        
        Optional__c o = new Optional__c(
            Name = 'Optional', 
            Version__c = version.Id, 
            Type__c = 'Item', 
            Featured_in_Product__c = 'air conditioning;steering;electric Lock'
        );
        optionalList.add(o);
        
        Optional__c o1 = new Optional__c(
            Name = 'Paint', 
            Version__c = version.Id, 
            Type__c = 'Paint'
        );
        optionalList.add(o1);
        
        Database.insert( optionalList );
        
        return optionalList;
    }
    
    private static PVCommercial_Action__c criarPVCommercial(Model__c model) {
        PVCommercial_Action__c commAction = new PVCommercial_Action__c(
            Model__c = model.Id,
            Start_Date__c = System.today().addDays( -5 ),
            End_Date__c = System.today().addDays( 5 ),
            Name = 'Commercial Action',
            Status__c = 'Active'
        );
        
        Database.insert( commAction );
        
        return commAction;
    }
    
    private static PVCall_Offer__c devePVCallOffer(PVCommercial_Action__c commAction) {
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
            Commercial_Action__c = commAction.Id,
            Call_Offer_Start_Date__c = system.today().addDays( -3 ),
            Call_Offer_End_Date__c = System.today().addDays( -3 )
        );
        Database.insert( callOffer );
        return callOffer;
    }
    
    public static testMethod void deveClone() {
        Account acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Praça',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '123ABC123',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        Model__c model = criarModel();
        PV_ModelMilesime__c milesime = criarMilesime(model);
        PVVersion__c version = criarPVVersion(milesime);
        List<Optional__c> optList = criarOptional(version);
        
        PVCommercial_Action__c commAction = criarPVCommercial(model);
        
        PVCall_Offer__c callOffer = devePVCallOffer(commAction);
        
        Group_Offer__c groupOffer = new Group_Offer__c(
            Type_of_Offer__c = 'Cooperada',
            Number_Offer__c = '1',
            Type_of_Action__c = 'Internet',
            Date_Start_Offer__c = System.today().addDays( -1 ),
            Date_End_Offer__c = System.today().addDays( 1 )
        );
        
        Database.insert(groupOffer);
        
        
        Offer_Item__c pricingTemplate = new Offer_Item__c(
            Name = 'Pricing Template', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Pricing_Template'),
            Status__c = 'Active',
            Code__c = 'Pricing Template'
        );
        Database.insert(pricingTemplate);
        
        Offer_Item__c featuredInCampaign = new Offer_Item__c(
            Name = 'Featured in Campaign', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Featured_in_Campaign'),
            Status__c = 'Active',
            End_Date__c = System.today(),
            Start_Date__c = System.today(),
            Code__c = 'Featured in Campaign'
        );
        Database.insert(featuredInCampaign);
        
        Offer_Item__c featuredInOffer = new Offer_Item__c(
            Name = 'Featured In Offer', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Featured_in_Offer'),
            Status__c = 'Active',
            Code__c = 'Featured In Offer'
        );
        Database.insert(featuredInOffer);
        
        Offer_Item__c offerHeader = new Offer_Item__c(
            Name = 'Offer Header', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Offer_Header'),
            Status__c = 'Active',
            Code__c = 'Offer Header'
        );
        Database.insert(offerHeader);
        
        Offer_Item__c stamp = new Offer_Item__c(
            Name = 'Stamp', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Stamp'),
            Status__c = 'Active',
            Code__c = 'Stamp'
        );
        Database.insert(stamp);
        
        
        Offer__c offer = new Offer__c( 
            From_To_Offer__c = false,
            hasUpgrade__c = false,
            Offer_to_Internet__c = true,
            Total_Inventory_Vehicle__c = 3,
            Model__c = model.Id,
            Version__c = version.Id,
            Optional__c = optList[0].Id,
            Painting__c = optList[1].Id,
            Featured_Product_Text__c = 'air conditioning;steering;electric Lock',
            Condition__c = 'Financiado',
            Commercial_Action__c = commAction.Id,
            ValueTo__c = 40060,
            Minimum_Input__c = 75,
            Number_of_Installments__c = 36,
            Monthly_Tax__c = 0,
            Coefficient__c = 0.02861,
            Entry_Value__c = 30045,
            Installment_Value__c = 305.01,
            Stamp__c = 'Marketing of factory;Big marketing in factory',
            Offer_Start_Date_Website__c = System.today().addDays( -1 ),
            Offer_End_Date_Website__c = System.today().addDays( 1 ),
            Pricing_Template__c = 'Sight',
            Featured_In_Offer__c = 'Complete',
            Pricing_Template_Lookup__c = pricingTemplate.Id,
            Featured_in_Offer_Lookup__c = featuredInOffer.Id,
            Offer_Header__c = offerHeader.Id,
            Stamp_Lookup__c = Stamp.Id,
            Group_Offer__c = groupOffer.id,
            Featured_in_Campaign__c = featuredInCampaign.Id
        );
        
        Database.insert(offer);
        
        Offer__c fromScript = new Offer__c( 
            From_To_Offer__c = false,
            hasUpgrade__c = false,
            Offer_to_Internet__c = true,
            Total_Inventory_Vehicle__c = 3,
            Condition__c = 'Financiado',
            Commercial_Action__c = commAction.Id,
            ValueTo__c = 40060,
            Minimum_Input__c = 75,
            Number_of_Installments__c = 36,
            Monthly_Tax__c = 0,
            Coefficient__c = 0.02861,
            Entry_Value__c = 30045,
            Installment_Value__c = 305.01,
            Stamp__c = 'Marketing of factory;Big marketing in factory',
            Offer_Start_Date_Website__c = System.today().addDays( -1 ),
            Offer_End_Date_Website__c = System.today().addDays( 1 ),
            Pricing_Template__c = 'Sight',
            Featured_In_Offer__c = 'Complete',
            Pricing_Template_Lookup__c = pricingTemplate.Id,
            Featured_in_Offer_Lookup__c = featuredInOffer.Id,
            Offer_Header__c = offerHeader.Id,
            Stamp_Lookup__c = Stamp.Id,
            Featured_in_Campaign__c = featuredInCampaign.Id
        );
        
        Group_Offer__c fauxGroupOffer = new Group_Offer__c(
            Date_Start_Offer__c = System.today().addDays( -1 ),
            Date_End_Offer__c = System.today().addDays( 1 )
        );
                    
        
        PageReference pg = VFC145_OfferWizardController.getCurrOffer( fromScript, offer.id);
        
        VFC145_OfferWizardController n = new VFC145_OfferWizardController();
        n.getDealersNames(offer.id);
        n.getOfferInfo(offer.Id);
        //Offer__c newOffer = VFC145_OfferWizardController().getOfferInfo( offer.id );
        
        
    }
    
    static User criarUser() {
        User admin = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User manager = new User();
        
        System.runAs(admin){
            Account acc = new Account(
                Name = 'DealerAcc', 
                Country__c = 'Brazil',
                ShippingCity = 'Cidade', 
                ShippingState = 'Estado', 
                NameZone__c = 'R2', 
                TV_Signal__c = 'Pra?a',
                Active_PV__c = true,
                isDealerActive__c = true,
                PV_Cooperative__c = true,
                PV_NotCooperative__c = true, 
                IDBIR__c= '123ABC123',
                Status__c = 'Active',
                RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
            );
            Database.insert( acc );
            
            Contact cont = new Contact(
                RecordTypeId ='012D0000000KApJIAW',
                LastName = 'teste',
                // Name = 'teste',
                AccountId = acc.Id,
                CPF__c = '898.612.386-03',
                Email='teste2@teste.com.br',
                Phone = '1122334455'
            );
            Database.insert( cont );
            
            manager = new User(
                FirstName = 'Test',
                LastName = 'User',
                Email = 'test@org.com',
                Username = 'test@org1.com',
                Alias = 'tes',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles',
                CommunityNickname = 'testing',
                ProfileId = '00eD0000001PnJi',
                ContactId = cont.Id,
                BIR__c ='123ABC123'
            );
            Database.insert(manager);
        }
        
        
        return manager;
    }
}