/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class UpdateITRMessageAction {
    global UpdateITRMessageAction() {

    }
    @InvocableMethod(label='Update ITR Message' description='Updates the given entries in ITR_Message__c')
    global static List<LiveText.ActionResult> updateITRMessages(List<LiveText.UpdateInboundItem> items) {
        return null;
    }
}
