/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class SendSMSToCampaignBatch implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful {
    global SendSMSToCampaignBatch(String liveTextNumberId, String message, String campaignId, List<String> sendToStatuses, String statusAfterSend, String duplicatesStatus, String statusAfterResponse, String statusAfterOptOut, String storageId) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
