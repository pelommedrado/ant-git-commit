public class PassingPurchaseServico {
    
    private static final String PERMISSAO_GRUPO  = 'BR_Passing_and_Purchase_Order_Group';
    private static final String PERMISSAO_DEALER = 'BR_Passing_and_Purchase_Order_Dealer';
    
    public static final String ONTEM = 'ONTEM';
    public static final String HOJE  = 'HOJE';
    
    private String statusDia = ONTEM;
    
    public PassingPurchaseServico(String status) {
        if(status != null) {
            this.statusDia = status;    
        }      
    }
    
    public void executar() {
        
        //obter os ids das contas que nao realizaram pedido
        Set<Id> setIds = obterContaSemPedido();
        Set<Id> temp = obterContaDraft();
        setIds.addAll(temp);
        
        System.debug('setIds:' + setIds.size());
        
        notificarUsuarios(setIds);       
        notificarGrupo(setIds);
    }
    
    private List<Account> obterTodasContaAtiva() {
        Id recordTypeId = [
            SELECT id 
            FROM RecordType 
            WHERE developerName = 'Network_Site_Acc' 
            LIMIT 1 
        ].id;
        
        List<Account> accList = [
            SELECT Id
            FROM Account
            WHERE Active_to_PPO__c = true AND Dealer_Status__c = 'Active' AND RecordTypeId =: recordTypeId
        ];
        
        return accList;
    }
    
    private Set<Id> obterContaDraft() {
        List<Account> accList = obterTodasContaAtiva();
        
        List<PassingAndPurchaseOrder__c> ppList = null;
        if(statusDia.equalsIgnoreCase(ONTEM)) {
            
            ppList = [
                SELECT Id, Account__r.Id
                FROM PassingAndPurchaseOrder__c
                WHERE Date__c = YESTERDAY AND Account__c in: accList AND Status__c = 'Draft'
            ];     
            
        } else if(statusDia.equalsIgnoreCase(HOJE)) {
            ppList = [
                SELECT Id, Account__r.Id
                FROM PassingAndPurchaseOrder__c
                WHERE Date__c = TODAY AND Account__c in: accList AND Status__c = 'Draft'
            ]; 
        }
        
        Set<Id> setAccIds = new Set<Id>();
        for(PassingAndPurchaseOrder__c p : ppList) {
            setAccIds.add(p.Account__r.Id);
        }
        return setAccIds;
    }
    
    private Set<Id> obterContaSemPedido() {
        List<Account> accList = obterTodasContaAtiva();
        
        System.debug('accList:' + accList.size());
        
        Set<Id> setIds = new Set<Id>();
        for(Account ac : accList) {
            setIds.add(ac.Id);
        }
        
        System.debug('Select PassingAndPurchaseOrder:' + statusDia);
        List<PassingAndPurchaseOrder__c> ppList = null;
        if(statusDia.equalsIgnoreCase(ONTEM)) {
            
            ppList = [
                SELECT Id, Account__r.Id
                FROM PassingAndPurchaseOrder__c
                WHERE Date__c = YESTERDAY AND Account__c in: accList 
            ];     
            
        } else if(statusDia.equalsIgnoreCase(HOJE)) {
            ppList = [
                SELECT Id, Account__r.Id
                FROM PassingAndPurchaseOrder__c
                WHERE Date__c = TODAY AND Account__c in: accList 
            ]; 
        }
        
        System.debug('ppList:' + ppList.size());
        
        for(PassingAndPurchaseOrder__c p : ppList) {
            setIds.remove(p.Account__r.Id);
        }
        
        return setIds;
    }
    
    private String getTexto() {
        if(statusDia.equalsIgnoreCase(ONTEM)) {
        return 'Bom dia! \n' + 
            'Informamos que até o momento não foi detectado o envio da declaração de Pedidos/Passagens/Test Drive para a BIR @bir, na aplicação Pedidos Online (do Renault + Cliente) para a data de ontem. \n' +
            'Lembramos que esta informação deve ser enviada impreterívelmente até as 9h de hoje. \n' + 
            'Não esqueça que os registros que já foram lançados como "Rascunho" precisam ter seu status alterado para "Enviado". \n\n' + 
            'Atenciosamente, Renault do Brasil';
             
        }
            
        return 'Boa Noite! \n' + 
                'Informamos que até o momento não foi detectado o envio da declaração de Pedidos/Passagens/Test Drive para a BIR @bir, na aplicação Pedidos Online (do Renault + Cliente) para a data de hoje.\n' +
                'Lembramos que esta informação deve ser enviada impreterívelmente no primeiro horário de amanhã.\n' + 
                'Não esqueça que os registros que já foram lançados como "Rascunho" precisam ter seu status alterado para "Enviado". \n\n' + 
                'Atenciosamente, Renault do Brasil ';
        
    }
    
    private void notificarUsuarios(Set<Id> setIds) {
        List<User> userListSend  = new List<User>();
        List<User> userList = obterUsuariosConta(setIds);
        for(User us: userList) {
            
            Set<String> permissionSets = new Set<String>();
            for(PermissionSetAssignment permission : us.PermissionSetAssignments) 
                permissionSets.add(permission.PermissionSet.Name);
            
            if(permissionSets.contains(PERMISSAO_GRUPO) || permissionSets.contains(PERMISSAO_DEALER)) {
                userListSend.add(us);   
            }
        }
        
        System.debug('userList:' + userList.size());
        System.debug('userListSend:' + userListSend.size());
        
        for(User us: userListSend) {
            System.debug('user Send:' + us);
            System.debug('user Send Conta:' + us.Contact.Account);
        }
        
        String texto = getTexto();
        
        for(User user : userListSend) {
            String textoEmail = texto.replaceAll('@bir', user.Contact.Account.IDBIR__c);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setToAddresses(new String[] { user.Email });
            mail.setReplyTo('batch@acme.com');
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Pedidos/Passagens/Test Drive');
            mail.setPlainTextBody(textoEmail);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
        }
    }
    
    private void notificarGrupo(Set<Id> setIds) {
        System.debug('Iniciando a notificacao do grupo...');
        
        List<Account> accAlert = [
            SELECT Id, ParentId, IDBIR__c
            FROM Account
            WHERE Id in:setIds 
        ];
        
        System.debug('accAlert:' + accAlert.size());
        
        Set<Id> idsAccPai = new Set<Id>();
        for(Account ac : accAlert) {
            idsAccPai.add(ac.ParentId);
        }
        
        List<Account> accFilha = [
            SELECT Id, IDBir__c, ParentId
            FROM Account
            WHERE ParentId in: idsAccPai
        ];
        
        System.debug('accFilha:' + accFilha.size());
        
        Set<Id> accIdsFilha = new Set<Id>();
        for(Account ac : accFilha) {
            //if(!setIds.contains(ac.Id)) {
                accIdsFilha.add(ac.Id);     
            //}
        }
        System.debug('accIdsFilha:' + accIdsFilha.size());
        
        List<User> userList = obterUsuariosConta(accIdsFilha);
        
        System.debug('userList:' + userList.size());
        
        List<User> userListGroup = new List<User>();
        for(User us: userList) {
            
            Set<String> permissionSets = new Set<String>();
            for(PermissionSetAssignment permission : us.PermissionSetAssignments) 
                permissionSets.add(permission.PermissionSet.Name);
            
            if(permissionSets.contains(PERMISSAO_GRUPO)) {
                userListGroup.add(us);
            }
        }
        
        String texto = getTexto();
        
        List<Messaging.SingleEmailMessage> listEmail = new List<Messaging.SingleEmailMessage>();
        for(Account ac : accAlert) {
            for(User user : userListGroup) {
                if(user.Contact.Account.ParentId == ac.ParentId) {
                    System.debug('Send user:' + user);
                    System.debug('Send acc:' + ac);
                    String textoEmail = texto.replaceAll('@bir', ac.IDBIR__c);
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                    mail.setToAddresses(new String[] { user.Email });
                    mail.setReplyTo('batch@acme.com');
                    mail.setSenderDisplayName('Batch Processing');
                    mail.setSubject('Pedidos/Passagens/Test Drive');
                    mail.setPlainTextBody(textoEmail);
                    
                    listEmail.add(mail);
                    
                }
            }
        }
        
        Messaging.sendEmail(listEmail);
    }
    
    private List<User> obterUsuariosConta(Set<Id> setAccIds) {
        List<String> permi = new List<String> { PERMISSAO_GRUPO, PERMISSAO_DEALER };
            List<User> userList = [
                SELECT Id, 
                Email,
                Contact.AccountId,
                Contact.Account.IDBIR__c,
                Contact.Account.ParentId,
                Contact.Account.Name, (
                    SELECT PermissionSet.Name
                    FROM PermissionSetAssignments
                    WHERE PermissionSet.Name in: permi
                )
                FROM User 
                WHERE Contact.AccountId in: setAccIds
            ];
        return userList;
    }
}