/**
*   Class   -   VFC84_QuoteLineItemBO
*   Author  -   Christian Ranha
*   Date    -   24/03/2013
*   #01 <ChristianRanha> <24/03/2013>
*   BO class for Quote object to logical function.
**/
public with sharing class VFC84_QuoteLineItemBO {

    private static final VFC84_QuoteLineItemBO instance = new VFC84_QuoteLineItemBO();
        
    /*private constructor to prevent the creation of instances of this class*/
    private VFC84_QuoteLineItemBO()
    {
    }

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC84_QuoteLineItemBO getInstance()
    {
        return instance;
    }
    
    /**
    * Obtêm os itens do orçamento pelo id do orçamento.
    * @param quoteId O id do orçamento.
    * @param setDeveloperName Um set contendo os valores de tipos de registros.
    * @return Uma lista de objetos QuoteLineItem.
    * @author Christian Ranha.
    */
    public List<QuoteLineItem> getQuoteLineItem(String quoteId, Set<String> setDeveloperName)
    {
    	List<QuoteLineItem> lstSObjQuoteLineItem = null;
    	
    	/*obtêm os itens de orçamento a partir do id do orçamento e tipo de registro*/
    	lstSObjQuoteLineItem = VFC36_QuoteLineItemDAO.getInstance().getQuoteLineItemByQuoteId(quoteId, setDeveloperName);
    	
    	return lstSObjQuoteLineItem;
    }

    /**
    * Exclui item do orçamento pelo id.
    * @param quoteLineItemId O id do item do orçamento.
    * @author Christian Ranha.
    */
    public void deleteQuoteLineItem(String quoteLineItemId)
    {
    	try
    	{
	    	VFC36_QuoteLineItemDAO.getInstance().deleteData(quoteLineItemId); 
    	}
    	catch(DMLException ex)
    	{
    		throw new VFC90_DeleteQuoteLineItemException(ex, ex.getDMLMessage(0));
    	}
    }

    /**
    * Atualiza itens de orçamento.
    * @param lstSObjQuoteLineItem Uma lista de itens de orçamento.
    * @author Christian Ranha.
    */
    public void updateQuoteLineItem(List<QuoteLineItem> lstSObjQuoteLineItem)
    {
    	try
    	{
	    	VFC36_QuoteLineItemDAO.getInstance().updateData(lstSObjQuoteLineItem); 
    	}
    	catch(DMLException ex)
    	{
    		throw new VFC93_UpdateQuoteLineItemException(ex, ex.getDMLMessage(0));
    	}
    }
    
    /**
    * 
    */
    public Map<String, QuoteLineItem> mapByQuoteId(String developerName, Set<String> lstSObjQuoteLineItem)
    {
    	List<QuoteLineItem> lstSObjQuoteLineItemAux = null;
    	Map<String, QuoteLineItem> mapQuoteLineItem = new Map<String, QuoteLineItem>();
    	
		lstSObjQuoteLineItemAux = VFC36_QuoteLineItemDAO.getInstance().fetchQuoteLineItemByQuoteId(developerName , lstSObjQuoteLineItem);
			
		//Adiona ao mapa
		for(QuoteLineItem auxListQuote : lstSObjQuoteLineItemAux)
		{
			mapQuoteLineItem.put(auxListQuote.QuoteId, auxListQuote);
		} 
		
		return  mapQuoteLineItem;	
    }    
}