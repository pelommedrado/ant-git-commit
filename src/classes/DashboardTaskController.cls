public with sharing class DashboardTaskController {

    private static final DashModalServico dashModal = new DashModalServico();
    private static final DashLembreteServico dashLembrete = new DashLembreteServico();

    public static final String ABA_TASK  = 'task';
    public static final String ABA_QUOTE = 'quote';
    public static final String ABA_LEAD  = 'lead';
    public static final String ABA_QUEUE = 'queue';

    public Boolean isSeller      { get { return ProfileUtils.isSeller;  } }
    public Boolean isManager     { get { return ProfileUtils.isManager; } }
    public Boolean isManagerBdc  { get { return ProfileUtils.isManagerBdc; } }
    public Boolean isManagerExec { get { return ProfileUtils.isManagerExec; } }

    public String stringOwner { get;set; }
    public List<SelectOption> selectOwner { get; set;}

    public String stringDealer { get; set; }
    public List<SelectOption> selectDealer { get; set;}

    public String stringSeller { get;set; }
    public List<SelectOption> selectSeller { get;set; }

    public Boolean isBdcSelected {get;set;}

    public PageReference changeDealer() {
        PageReference pg = new PageReference(
            '/apex/DashboardTask?dealer=' + stringDealer +
            '&aba=' + this.selectAba + '&seller=' +
            (String.isEmpty(stringSeller) ? '' : stringSeller));
        pg.setRedirect(true);
        return pg;
    }

    public PageReference changeSeller() {
        PageReference pg = new PageReference(
            '/apex/DashboardTask?dealer=' + stringDealer +
            '&aba=' + this.selectAba + '&seller=' +
            (String.isEmpty(stringSeller) ? '' : stringSeller));
        pg.setRedirect(true);
        return pg;
    }

    public Date dataAtual {  get { return Date.today();  } }
    public Task periodo 	  { get;set; }
    public String textSearch  { get;set; }
    public String orderSelect { get;set; }

    public List<OportunidadeItem> oppList	{ get;set; }
    public List<DashLembreteServico.Lembrete> resultAtividade 	  { get;set; }
    public List<DashLembreteServico.Lembrete> resultQueue         { get;set; }

    public List<DashLembreteServico.Lembrete> lembreteList 		  { get;set; }
    public List<Quote> quoteList			  { get;set; }

    public Integer numContato { get;set; }
    public Integer objContato 	  { get; set; }
    public Integer objFaturamento { get; set; }

    public Integer totalOpp 		{ get;set; }
    public Integer totalOppFaturada { get;set; }
    public Integer conversao 		{ get;set; }

    public Integer numSolicitacao 	{ get;set; }
    public Integer numTaskAtrasada  { get;set; }
    public Integer numTaskHoje      { get;set; }
    public Integer numLeadMontadora { get;set; }

    public String selectAba 		{ get;set; }

    public Integer oppIden { get;set; }
    public Integer oppAnda { get;set; }
    public Integer oppPreO { get;set; }
    public Integer oppFatu { get;set; }
    public Integer oppPerd { get;set; }

    public DashModalServico.ModalDetalhe modalDet 		{ get;set; }
    public List<LeadProspreccao> leadProsList { get;set; }

    public String dataIniString {get;set;}
    public String dataFimString {get;set;}

    public Account accDealer {
        get {
            List<Contact> cttList = [ SELECT Id, AccountId FROM Contact WHERE Id =: user.ContactId ];

            if(cttList.isEmpty()) {
                return null;
            }
            return [
                SELECT Id, Name, ParentId
                FROM Account WHERE Id =: cttList.get(0).AccountId
            ];
        }
    }

    public User user {
        get { return [ SELECT Id, Name, ContactId FROM User WHERE Id =: UserInfo.getUserId()]; }
    }

    public List<String> monthList {
        get {
            List<String> stgMes = new List<String>();

            Schema.DescribeFieldResult fieldResult =
                Monthly_Goal_Group__c.Month__c.getDescribe();

            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry f : ple) {
                stgMes.add(f.getValue());
            }
            return stgMes;
        }
    }

    public List<SelectOption> temperaturaList {
        get { return  Utils.getPicklistValues('Opportunity', 'temperature__c'); }
    }

    public List<SelectOption> itemModelo {
        get { return DashboardUtil.modeloSelectOption(); }
    }

    public List<SelectOption> itemAtividade {
        get { return DashboardUtil.atividadeSelectOption(isBdcSelected); }
    }

    public DashboardTaskController() {
        this.selectAba = ABA_TASK;
        this.periodo = new Task();
        this.periodo.ActivityDate = null;

        this.oppList  = new List<OportunidadeItem>();
        this.leadProsList = new List<LeadProspreccao>();
        this.orderSelect = '0';

        this.isBdcSelected = false;

        String paramAba = ApexPages.currentPage().getParameters().get('aba');
        if(!String.isEmpty(paramAba)) {
            this.selectAba = paramAba;
        }

        init();
        initLembrete();
        initPainel();
        selectTableMain();
        buscarLead();
        //initAtividades();
    }

    public void init() {
        String paramDealer = ApexPages.currentPage().getParameters().get('dealer');
        String paramSeller = ApexPages.currentPage().getParameters().get('seller');
        String paramProfile = ApexPages.currentPage().getParameters().get('profile');

        System.debug('init()');
        System.debug('paramDealer: ' + paramDealer);
        System.debug('paramSeller: ' + paramSeller);

        this.selectDealer = new List<SelectOption>();
        this.selectSeller = new List<SelectOption>();

        if(isManagerExec) {
            List<Account> accL = [
                SELECT Id, Name FROM Account
                WHERE ParentId =: accDealer.ParentId
                AND RecordTypeId =: Utils.getRecordTypeId('Account', 'Network_Site_Acc')
            ];

            for(Account a: accL) {
                selectDealer.add(new SelectOption(a.Id, a.Name));
            }

        } else {
            selectDealer.add(new SelectOption(accDealer.Id, accDealer.Name));
        }

        if(selectDealer.isEmpty()) {
            return;
        }

        this.stringDealer = (String.isEmpty(paramDealer) ?
                             selectDealer.get(0).getValue() : paramDealer);

        //if(isManagerBdc){
        //   selectSeller.add( new SelectOption('BDC', 'BDC'));
        //}

        if(isManager || isManagerExec) {
            List<User> sellerList = DashboardUtil.obterSeller(stringDealer);
            for(User u : sellerList) {
                selectSeller.add(new SelectOption(u.Id, u.Name));
            }

        } else if(isSeller) {
            selectSeller.add(new SelectOption(user.Id, user.Name));

        }

        if(paramProfile == 'BDC'){
            isBdcSelected = true;
            this.stringSeller = user.Id;
        }

        if(!String.isEmpty(paramSeller) /*&& paramSeller != 'BDC'*/) {
            this.stringSeller = paramSeller;
            isBdcSelected = false;

        } else if(isSeller && !isManager) {
            this.stringSeller = user.Id;
        }
    }

    public void initPainel() {
        System.debug('initPainel()');

        List<Id> allSellers = obterOwner();
        final Datetime ini = DataUtils.dataToDatetimeIniMes(dataAtual);
        final Datetime fim = DataUtils.dataToDatetimeFimMes(dataAtual);

        System.debug('StringDealer: ' + stringDealer);
        System.debug('StringSeller: ' + stringSeller);
        System.debug('Data ini: ' + ini);
        System.debug('Data fim: ' + fim);

        this.numContato = 0;
        List<TST_Transaction__c> oppTransList = [
            SELECT Id FROM TST_Transaction__c
            WHERE Opportunity_Status__c = 'In Attendance' AND
            CreatedDate >=: ini AND CreatedDate <=: fim
            AND Opportunity__r.OwnerId IN: allSellers
        ];
        System.debug('Trans Opportunidade: ' + oppTransList.size());

        List<TST_Transaction__c> leadTransList = [
            SELECT Id FROM TST_Transaction__c
            WHERE CreatedDate >=: ini AND CreatedDate <=: fim
            AND Lead__r.OwnerId IN: allSellers AND isLeadContact__c = true
            AND Lead__r.IsConverted = false
        ];
        System.debug('Trans Lead: ' + leadTransList.size());
        this.numContato = oppTransList.size() + leadTransList.size();

        String query = 'SELECT Id FROM Quote WHERE Status =  \'Billed\' ' +
            'AND LastModifiedDate >=: ini AND LastModifiedDate <=: fim '   +
            (String.isEmpty(stringSeller) ? 'AND Opportunity.Dealer__c =: stringDealer' :
             ' AND Opportunity.OwnerId =: stringSeller ') ;

        this.quoteList = Database.query(query);
        initConversao();
        initObjetivos();
    }

    private void initObjetivos() {
        System.debug('initObjetivos()');

        AggregateResult[] objTotalResults = null;
        this.objContato = 0;

        if(String.isEmpty(stringSeller)) {
            objTotalResults = [
                SELECT SUM(General_Objetive__c) cou
                FROM Monthly_Goal_Dealer__c
                WHERE Dealer__c =: stringDealer AND
                Monthly_Goal_Group__r.Month__c =: monthList.get(dataAtual.month()-1)
            ];

        } else {
            objTotalResults = [
                SELECT SUM(General_Objetive__c) cou
                FROM Monthly_Goal_Seller__c
                WHERE Seller__c =: stringSeller AND
                Monthly_Goal_Dealer__r.Monthly_Goal_Group__r.Month__c =: monthList.get(dataAtual.month()-1)
            ];
        }

        this.objFaturamento = Integer.valueOf(objTotalResults[0].get('cou'));

        System.debug('objFaturamento: ' + objFaturamento);

        if(this.objFaturamento == null || this.objFaturamento == 0) {
            this.objFaturamento = 0;
            return;
        }

        if(conversao > 0 && objFaturamento > 0) {
            final Double conver = Double.valueOf(conversao);
            final Double taxaConversao = (conver/Double.valueOf(100));
            System.debug('conversao: ' + conversao);
             System.debug('taxaConversao: ' + taxaConversao);
            this.objContato = Integer.valueOf(objFaturamento/taxaConversao);
        }
    }

    private void initConversao() {
        System.debug('initConversao()');

        this.conversao = 0;
        AggregateResult[] totalOppResults = null;
        AggregateResult[] totalOppFatResults = null;
        final Datetime ini = DataUtils.dataToDatetimeIniMes(dataAtual);
        final Datetime fim = DataUtils.dataToDatetimeFimMes(dataAtual);

        if(String.isEmpty(stringSeller)) {
            totalOppResults = [
                SELECT COUNT(Id) cou FROM Opportunity
                WHERE Dealer__c =: stringDealer
                AND LastModifiedDate >= LAST_N_DAYS:30
                //AND LastModifiedDate >=: ini AND LastModifiedDate <=: fim
            ];

        } else {
            totalOppResults = [
                SELECT COUNT(Id) cou FROM Opportunity
                WHERE OwnerId =: stringSeller
                AND LastModifiedDate >= LAST_N_DAYS:30
                //AND LastModifiedDate >=: ini AND LastModifiedDate <=: fim
            ];
        }
        this.totalOpp = Integer.valueOf(totalOppResults[0].get('cou'));

        if(String.isEmpty(stringSeller)) {
            totalOppFatResults = [
                SELECT COUNT(Id) cou FROM Opportunity
                WHERE Dealer__c =: stringDealer AND StageName = 'Billed'
                AND LastModifiedDate >= LAST_N_DAYS:30
                //AND LastModifiedDate >=: ini AND LastModifiedDate <=: fim
            ];

        } else {
            totalOppFatResults = [
                SELECT COUNT(Id) cou FROM Opportunity
                WHERE OwnerId =: stringSeller AND StageName = 'Billed'
                AND LastModifiedDate >= LAST_N_DAYS:30
                //AND LastModifiedDate >=: ini AND LastModifiedDate <=: fim
            ];
        }
        this.totalOppFaturada = Integer.valueOf(totalOppFatResults[0].get('cou'));

        System.debug('totalOpp: ' + totalOpp);
        System.debug('totalOppFaturada: ' + totalOppFaturada);
        if(totalOpp > 0 && totalOppFaturada > 0) {
            Decimal a = Decimal.valueOf(totalOppFaturada);
            Decimal b = Decimal.valueOf(totalOpp);
            this.conversao = Integer.valueOf((a/b)*100);
        }
    }

    public void initLembrete() {
        System.debug('initLembrete()');
        this.lembreteList = dashLembrete.obterLembrete(user.Id);
        //this.numTaskAtrasada = 0;
    }

    public void cancelarLembrete() {
        String param = ApexPages.currentPage().getParameters().get('taskId');
        List<Task> tList = [ SELECT Id FROM Task WHERE Id =: param ];

        if(!tList.isEmpty()) {
            Task tk = tList.get(0);
            tk.IsReminderSet = false;

            UPDATE tk;
        }

        List<Event> eList = [ SELECT Id FROM Event WHERE Id =: param ];

        if(!eList.isEmpty()) {
            Event et = eList.get(0);
            et.IsReminderSet = false;

            UPDATE et;
        }

        initLembrete();
    }

    public void selectTableMain() {
        System.debug('selectTableMain()');

        String param = ApexPages.currentPage()
            .getParameters().get('table');

        System.debug('Param: ' + param);

        if(!String.isEmpty(param)) {
            this.selectAba = param;
        }
        this.paramStatus = '';

        if(this.selectAba.equals(ABA_QUOTE)) {
            initNegociacao();

        } else if(this.selectAba.equals(ABA_TASK)) {
            initAtividades();

        } else if(this.selectAba.equals(ABA_LEAD)) {
            buscarLead();

        } else if(this.selectAba.equals(ABA_QUEUE)) {
            buscarTaskBDC();
        }
    }

    public void initAtividades() {
        System.debug('initAtividades()');
        AggregateResult[] totalOppResults = [
            SELECT COUNT(Id) cou FROM opportunity
            WHERE OpportunitySource__c <> 'NETWORK'
            AND OpportunitySource__c <> 'DEALER'
            /*AND OwnerId =:  user.Id*/  AND Submitted__c = false AND StageName = 'Identified'
        ];
        this.numLeadMontadora = Integer.valueOf(totalOppResults[0].get('cou'));

        totalOppResults = [
            SELECT COUNT(Id) cou FROM Opportunity
            WHERE Ownership_requested_by_ID__c <> NULL
        ];
        this.numSolicitacao = Integer.valueOf(totalOppResults[0].get('cou'));

        List<Id> allSellers = obterOwner();

        System.debug('allSellers:' + allSellers);

        final Datetime ini = DataUtils.dataToDatetimeIni(dataAtual);
        final Datetime fim = DataUtils.dataToDatetimeFim(dataAtual);

        totalOppResults = [
            SELECT COUNT(Id) cou FROM Task
            WHERE OwnerId IN:(allSellers) AND WhatId != NULL AND ActivityDatetime__c != NULL
            AND ActivityDatetime__c >=: ini AND ActivityDatetime__c <=: fim
            AND Status != 'Canceled'
            AND Status != 'Completed' /*AND IsReminderSet = true*/
        ];
        Integer temp =  Integer.valueOf(totalOppResults[0].get('cou'));

        totalOppResults = [
            SELECT  COUNT(Id) cou FROM Event
            WHERE OwnerId IN:(allSellers) AND WhatId != NULL AND ActivityDatetime__c != NULL
            AND ActivityDatetime__c >=: ini AND ActivityDatetime__c <=: fim
            AND Status__c != 'Canceled'
            AND Status__c != 'Completed' /*AND IsReminderSet = true */
        ];

        numTaskHoje = temp + Integer.valueOf(totalOppResults[0].get('cou'));

        totalOppResults = [
            SELECT COUNT(Id) cou FROM Task
            WHERE OwnerId IN:(allSellers) AND WhatId != NULL AND ActivityDatetime__c != NULL
            AND ActivityDatetime__c <=: Datetime.now()
            AND Status != 'Canceled'
            AND Status != 'Completed'/*AND IsReminderSet = true */
        ];
        temp =  Integer.valueOf(totalOppResults[0].get('cou'));

        totalOppResults = [
            SELECT  COUNT(Id) cou FROM Event
            WHERE OwnerId IN:(allSellers) AND WhatId != NULL AND ActivityDatetime__c != NULL
            /*AND ActivityDatetime__c >=: ini*/ AND ActivityDatetime__c <=: Datetime.now()
            AND Status__c != 'Canceled'
            AND Status__c != 'Completed' /*AND IsReminderSet = true*/
        ];
        numTaskAtrasada = temp + Integer.valueOf(totalOppResults[0].get('cou'));

        buscarTask();
    }

    public void initNegociacao() {
        this.oppIden = 0;
        this.oppAnda = 0;
        this.oppPreO = 0;
        this.oppFatu = 0;
        this.oppPerd = 0;

        final DashboardOpportunityService countService = new DashboardOpportunityService();
        countService.allSellers = obterOwner();
        this.oppPreO = countService.countMonth('Order');
        this.oppFatu = countService.countMonth('Billed');
        this.oppIden = countService.countLastDays(new List<String>{ 'Identified' });
        this.oppAnda = countService.count(new List<String>{ 'In Attendance', 'Test Drive', 'Quote' });
        this.oppPerd = countService.countLastDays(new List<String>{ 'Lost' });
        buscarOpp();
    }

    public String paramSubSource { get;set; }
    public String paramDetail 	 { get;set; }
    public String paramVehicle 	 { get;set; }
    public String paramSubject 	 { get;set; }
    public String paramOppSource { get;set; }

    public String paramStatus 	 { get;set; }

    public void buscarTask() {
        System.debug('buscarTask()');
        this.resultAtividade = new List<DashLembreteServico.Lembrete>();

        final Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
        if(this.selectAba.equals(ABA_QUOTE)) {
          buscarOpp();
          for(OportunidadeItem oppItem : this.oppList) {
              oppMap.put(oppItem.opp.Id, oppItem.opp);
          }
        } else if(this.selectAba.equals(ABA_TASK)) {
          final List<Id> allSellers = obterOwner();
          DashboardOpportunityService queryService = new DashboardOpportunityService();
          queryService.action = this.selectAba;
          queryService.paramVehicle = paramVehicle;
          queryService.paramStatus = paramStatus;
          queryService.paramOppSource = paramOppSource;
          queryService.allSellers = allSellers;
          queryService.isData = false;
          //queryService.dataIniString = dataIniString;
          //queryService.dataFimString = dataFimString;
          queryService.textSearch = textSearch;
          queryService.orderSelect = '';
          final List<Opportunity> oList = queryService.buscarOpp();
          for(Opportunity o : oList) {
              oppMap.put(o.Id, o);
          }
        }

        final DashboardTaskService queryService = new DashboardTaskService();
        queryService.paramSubject = paramSubject;
        queryService.paramStatus = paramStatus;
        queryService.allSellers = obterOwner();
        queryService.oppList = oppMap.values();
        queryService.dataIniString = dataIniString;
        queryService.dataFimString = dataFimString;
        //queryService.isData = this.selectAba.equals(ABA_TASK);

        final List<Task> taskList = queryService.buscarTaskList();
        for(Task task : taskList) {
          final Opportunity opp = oppMap.get(task.WhatId);
          DashLembreteServico.Lembrete lem = dashLembrete.createLembrete(task, opp);
          this.resultAtividade.add(lem);
        }
        final List<Event> eventList = queryService.buscarEventList();
        for(Event event: eventList) {
          final Opportunity opp = oppMap.get(event.WhatId);
          DashLembreteServico.Lembrete lem = dashLembrete.createLembrete(event, opp);
          this.resultAtividade.add(lem);
        }

        this.resultAtividade = dashLembrete.sortLembrete(
          this.resultAtividade, Integer.valueOf(orderSelect));
    }

    public void buscarTaskBDC() {
        System.debug('buscarTaskBDC()');

        List<String> paramSubjectList = DashboardUtil.paramFilter(paramSubject);

        Map<String,String> mapSubject = new Map<string,String>();

        //String valueOfChaseOpp 			= '';
        //String valueOfRescueOpp 		= '';
        //String valueOfConfDeliveryOpp 	= '';

        for(String value: paramSubjectList) {
            if(value == 'Persecution') {
                mapSubject.put(value,'ChaseOpportunity__c = true ');
            }
            if(value == 'Rescue') {
                mapSubject.put(value,'RescueOpportunity__c = true ');
            }
            if(value == 'Delivery Confirmation') {
                mapSubject.put(value,'ConfirmDeliveryOpportunity__c = true ');
            }
        }

        String queryWhere = '';
        if(paramSubjectList.isEmpty()) {
            queryWhere += '( ChaseOpportunity__c = true OR ' +
              'RescueOpportunity__c = true OR ' +
              'ConfirmDeliveryOpportunity__c = true)';
        }

        if(!paramSubjectList.isEmpty() && paramSubjectList.size() != 1) {
            queryWhere = '( ';
        }
        for(Integer i=0; i < paramSubjectList.size(); i++) {
            if(!paramSubjectList.isEmpty() && paramSubjectList.size() == 1) {
                queryWhere += mapSubject.get(paramSubjectList[i]);
            } else {
                queryWhere += mapSubject.get(paramSubjectList[i]) + 'OR ';
            }
        }

        // remove ultimo "OR" e acresecenta o ")"
        if(!paramSubjectList.isEmpty() && paramSubjectList.size() != 1) {
            queryWhere = queryWhere.substring(0, queryWhere.length() - 3) + ') ';
        }
        String query = 'SELECT Id, Account.Name, Name, Owner.Name, VehicleInterest__c, toLabel(StageName), ' +
            ' toLabel(OpportunitySource__c), toLabel(OpportunitySubSource__c), CreatedDate, LastModifiedDate, ' +
            ' RescueOpportunity__c , ConfirmDeliveryOpportunity__c , ChaseOpportunity__c ' +
            ' FROM Opportunity WHERE Accept__c != \'Yes Accept\' AND Dealer__c =: stringDealer AND ' + queryWhere +
            (orderSelect == '0' ? 'ORDER BY CreatedDate ASC' : ' ORDER BY CreatedDate DESC') +
            ' LIMIT 999';

        System.debug('***QUERY: '+query);

        List<Opportunity> oppsBDCList = Database.query(query);
        this.resultQueue = new List<DashLembreteServico.Lembrete>();

        for(Opportunity oppt: oppsBDCList) {
          final DashLembreteServico.Lembrete lembrete = dashLembrete.createLembrete(oppt);
          lembrete.opp  = oppt;
          lembrete.veiculo  = oppt.VehicleInterest__c;
          lembrete.ownerName  = 'BDC';
          lembrete.descricao  = 'Entrar em contato com cliente para realizar ' + lembrete.assunto + '.';

          if(!String.isEmpty(oppt.AccountId)) {
              lembrete.NomeCliente = oppt.Account.Name;
          }
          resultQueue.add(lembrete);
        }
    }

    public void buscarLead() {
        System.debug('buscarLead()');

        this.leadProsList = new List<LeadProspreccao>();

        String queryWhere = '';
        if(!String.isEmpty(textSearch)) {
            queryWhere += 'AND (FirstName LIKE \'%'   + textSearch + '%\' OR ' +
                ' LastName LIKE \'%'  	 + textSearch + '%\' OR ' +
                ' CPF_CNPJ__c LIKE \'%'  + textSearch + '%\' OR ' +
                ' Email LIKE \'%' 		 + textSearch + '%\' OR ' +
                ' HomePhone__c LIKE \'%' + textSearch + '%\' OR ' +
                ' MobilePhone LIKE \'%'  + textSearch + '%\')';
        }

        List<Id> idOwnerList = obterOwner();

        String query = 'SELECT Id, OwnerId, Owner.Name, MobilePhone, HomePhone__c, toLabel(Status),' 	+
            'Email, FirstName, LastName, CPF_CNPJ__c, CreatedDate, LastModifiedDate, '  +
            'Description, VehicleOfInterest__c ' 							+
            'FROM Lead WHERE OwnerId IN: idOwnerList AND Status != \'Not Interested\' ' +
            (String.isEmpty(queryWhere) ? '' : queryWhere);

        System.debug('Query: ' + query);

        List<Lead> leadList = Database.query(query);

        Map<Id, Lead> mapLead = new Map<Id, Lead>();
        for(Lead le : leadList) {
            mapLead.put(le.Id, le);
        }
        List<Id> allSellers = obterOwner();
        query = 'SELECT Id, Status, OwnerId, Owner.Name, Account.Name, ' +
            ' Subject, Description, ReminderDateTime, WhatId, WhoId ' +
            'FROM Task WHERE (WhoId IN: leadList) AND OwnerId IN: allSellers ' +
            (orderSelect == '0' ? ' ORDER BY CreatedDate ASC' : ' ORDER BY CreatedDate DESC') +
            ' LIMIT 999';

        final List<Task> taskList = Database.query(query);

        for(Task t : taskList) {
            final LeadProspreccao lp = new LeadProspreccao();
            lp.lead = mapLead.get(t.WhoId);
            lp.task = t;
            //lp.task.ReminderDateTime = DataUtils.getUserDatetime(t.ReminderDateTime);
            this.leadProsList.add(lp);
        }
    }

    public void buscarOpp() {
      final List<Id> allSellers = obterOwner();
      DashboardOpportunityService queryService = new DashboardOpportunityService();
      queryService.action = this.selectAba;
      queryService.paramVehicle = paramVehicle;
      queryService.paramStatus = paramStatus;
      queryService.paramOppSource = paramOppSource;
      queryService.dataIniString = dataIniString;
      queryService.dataFimString = dataFimString;
      queryService.textSearch = textSearch;
      queryService.orderSelect = orderSelect;
      queryService.allSellers = allSellers;
      //queryService.isData = this.selectAba.equals(ABA_QUOTE);

      this.oppList.clear();
      final List<Opportunity> allOppResultList = queryService.buscarOpp();
      for(Opportunity op: allOppResultList) {
        OportunidadeItem oI = new OportunidadeItem();
        oI.opp = op;
        oI.createdDate = DataUtils.getUserDatetime(op.CreatedDate);
        oI.updatedDate = DataUtils.getUserDatetime(op.LastModifiedDate);
        this.oppList.add(oI);
      }
    }

    public void sortSelect() {
        System.debug('selectAba: ' + selectAba);

        if(this.selectAba.equals(ABA_QUOTE)) {
            buscarOpp();
        } else if(this.selectAba.equals(ABA_LEAD)) {
            buscarLead();
        } else if(this.selectAba.equals(ABA_TASK)) {
            buscarTask();
        } else if(this.selectAba.equals(ABA_QUEUE)) {
            buscarTaskBDC();
        }
    }

    public void abrirModalTask() {
        System.debug('abrirModal()');
        Id taskId = ApexPages.currentPage().getParameters().get('taskId');

        if(String.isEmpty(taskId)) { return; }

        final DashModalServico.ModalDetalhe modal = new DashModalServico.ModalDetalhe();
		    modal.modo = 'Task';
        String whatId = null;

        final Task task = dashModal.obterTask(taskId);
        final Event event = dashModal.obterEvent(taskId);
        if(task != null) {
            whatId = task.WhatId;
            dashModal.configModalTask(modal, task);

        } else if(event != null) {
            whatId = event.WhatId;
            dashModal.configModalEvent(modal, event);
        }

        final Opportunity opp = dashModal.obterOpp(whatId);
        dashModal.configModalOpp(modal, opp);
        System.debug('dashModal ' + modal);
        this.modalDet = modal;
    }

    public void abrirModalOpp() {
        System.debug('abrirModalOpp()');

        Id oppId = ApexPages.currentPage().getParameters().get('oppId');

        if(String.isEmpty(oppId)) { return; }

        final DashModalServico.ModalDetalhe modal = new DashModalServico.ModalDetalhe();

        final Opportunity opp = dashModal.obterOpp(oppId);
        dashModal.configModalOpp(modal, opp);
        modal.isTask = false;
        this.modalDet = modal;
    }

    public void abrirModalBDC() {
        System.debug('abrirModalBDC()');

        Id oppId = ApexPages.currentPage().getParameters().get('oppId');
        String assunto = ApexPages.currentPage().getParameters().get('actSubj');

        System.debug('oppId: ' + oppId);
        System.debug('assunto: ' + assunto);

        if(String.isEmpty(oppId)) { return; }

        final DashModalServico.ModalDetalhe modal = new DashModalServico.ModalDetalhe();

        final Opportunity opp = dashModal.obterOpp(oppId);
        dashModal.configModalBDC(modal, opp);
        modal.isTask                        = true;
        this.modalDet                       = modal;
        this.modalDet.isPreTask				= true;
        this.modalDet.actv                  = DashboardUtil.traduzirSubject(assunto);
        this.modalDet.subject				= assunto;
        this.modalDet.comentarioActv        =
            'Entrar em contato com cliente para realizar ' + this.modalDet.actv + '.';
    }

    public void abrirModalLead() {
        System.debug('abrirModalLead()');

        Id taskId = ApexPages.currentPage().getParameters().get('taskId');

        if(String.isEmpty(taskId)) { return; }

        final DashModalServico.ModalDetalhe modal = new DashModalServico.ModalDetalhe();

        final Task task = dashModal.obterTask(taskId);
        dashModal.configModalTask(modal, task);

        final Lead lead = dashModal.obterLead (task.WhoId);
        dashModal.configModalLead(modal, lead);
        this.modalDet = modal;
    }

    public PageReference abrirLembrete() {
        System.debug('abrirLembrete()');

        Id lemId = ApexPages.currentPage().getParameters().get('lemId');
        String lead = ApexPages.currentPage().getParameters().get('lead');
        PageReference pg = null;
        if(Boolean.valueOf(lead)) {
            pg = new PageReference(
                '/apex/DashboardLead?actId=' + lemId);
        } else {
            pg = new PageReference(
                '/apex/DashboardAtividade?actId=' + lemId);
        }
        pg.setRedirect(true);
        return pg;
    }

    public void abrirModalOnwer() {
      System.debug('abrirModalOnwer()');
      this.selectOwner = new List<SelectOption>();
      final Opportunity opp =  this.modalDet.opp;
      final List<User> userList = DashboardUtil.obterSeller(opp.Dealer__c);
      for(User user: userList) {
        this.selectOwner.add(new SelectOption(user.Id, user.Name));
      }
    }

    public void salvarOwner() {
        System.debug('salvarOwner()');
        String ownerId = ApexPages.currentPage().getParameters().get('ownerId');
        if(String.isEmpty(ownerId)) {
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, 'É necessário selecionar um vendedor'));
            return;
        }
        final Opportunity opp =  new Opportunity(Id=this.modalDet.opp.Id);
        opp.OwnerId = ownerId;
        final List<Task> taskList = [
            SELECT Id FROM Task WHERE Status IN('Not Started', 'In Progress')
            AND WhatId =: opp.Id
        ];
        for(Task task : taskList) {
            task.OwnerId = ownerId;
        }
        final List<Event> eventList = [
            SELECT Id FROM Event WHERE Status__c IN('Not Started', 'In Progress')
            AND WhatId =: opp.Id
        ];
        for(Event event : eventList) {
            event.OwnerId = ownerId;
        }
        try {
            UPDATE opp;
            UPDATE taskList;
            UPDATE eventList;

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }

    public PageReference modalEditar() {
        PageReference pg = null;

        if(this.selectAba.equals(ABA_QUOTE)) {

        } else if(this.selectAba.equals(ABA_TASK)) {
            pg = new PageReference(
                '/apex/DashboardAtividade?actId=' + modalDet.taskId);
        } else if(this.selectAba.equals(ABA_LEAD)) {
              pg = new PageReference(
                '/apex/DashboardLead?actId=' + modalDet.taskId);
        }

        pg.setRedirect(true);
        return pg;
    }

    public PageReference modalNova() {
        PageReference pg = new PageReference(
            '/apex/DashboardAtividade?oppId=' + modalDet.opp.Id);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference visualizarOpp() {
        PageReference pg = new PageReference(
            '/' + modalDet.opp.Id);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference atenderOpp() {
        PageReference pg = new PageReference(
            '/apex/OportunidadeButtonAtend?scontrolCaching=1&id=' + modalDet.opp.Id);
        System.debug('pg: ' + pg);
        pg.setRedirect(true);

        return pg;
    }

    public PageReference retornarOpp() {
        final PageReference pg = new PageReference(
            '/apex/OportunidadeButtonReturn?id=' + modalDet.opp.Id);
        System.debug('pg: ' + pg);
        pg.setRedirect(true);

        return pg;
    }

    public PageReference aceitarBDC() {
        System.debug('aceitarBDC()');
        System.debug('modalDet.subject: ' + modalDet.subject);
        System.debug('modalDet.oppp: ' + modalDet.opp);

        if(String.isEmpty(modalDet.subject)) {
            return null;
        }

        try {
            final Task newTask = ActivityManagerBo.createBdcTask(modalDet.opp.Id, modalDet.subject);
            System.debug('newTask:' + newTask);
            PageReference pg = new PageReference(
                '/apex/DashboardAtividade?actid=' + newTask.Id);
            pg.setRedirect(true);
            return pg;

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
        }
    }

    private List<Id> obterOwner() {
      List<Id> idList = new List<Id>();
      if(String.isEmpty(stringSeller)) {
        List<User> uL = DashboardUtil.obterSeller(stringDealer);
        for(User u : uL) {
            idList.add(u.Id);
        }
      } else {
        idList.add(stringSeller);
      }
      return idList;
    }

    public PageReference newLead(){
        return new PageReference(
            '/00Q/e?retURL=%2F00Q%2Fo&RecordType=' +
            Utils.getRecordTypeId('Lead', 'DVR') + '&ent=Lead' );
    }
    public class LeadProspreccao {
        public Lead lead { get;set; }
        public Task task { get;set; }
    }
    public class OportunidadeItem {
        public Opportunity opp { get;set; }
        public Datetime createdDate { get;set; }
        public Datetime updatedDate { get;set; }
    }
}