@isTest
public class Rforce_StringBuffer_Test {

/**
 * @author Venkatesh Kumar
 * @date 13/12/2015
 * @description This method is test the String buffer class.
 */

static TestMethod void testStringBuffer() {
    system.debug('StringBuffer_Test : testStringBuffer ======  ' );
    Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
    insert ctr;
    User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
    System.runAs(usr) {
    Test.startTest();
    
    
    Rforce_StringBuffer_CLS stringBuffer = new Rforce_StringBuffer_CLS();
    stringBuffer.toStr(); 
    Rforce_StringBuffer_CLS stringBufferdecimal = new Rforce_StringBuffer_CLS(0.002);
    Rforce_StringBuffer_CLS stringBufferL = new Rforce_StringBuffer_CLS(100000L);
    Rforce_StringBuffer_CLS stringBufferdec = new Rforce_StringBuffer_CLS(123.4);
    Rforce_StringBuffer_CLS stringBufferno = new Rforce_StringBuffer_CLS(2147483);
    Rforce_StringBuffer_CLS stringBufferdbool = new Rforce_StringBuffer_CLS(true);
    Rforce_StringBuffer_CLS stringBufferId = new Rforce_StringBuffer_CLS(00300000003);
      
    String theString = 'Rforce';
    System.debug('stringBuffer ----------------------------------------->'+stringBuffer );

    stringBuffer.append('rforce');
    stringBuffer.append(0.002);
    stringBuffer.append(100000L);
    stringBuffer.append(123.4);
    stringBuffer.append(2147483);
    stringBuffer.append('2011-01-10');
    stringBuffer.append(true);
    stringBuffer.append(00300000003);

    Test.stopTest();
    }
    }
    }