/**
	Class   -   VFC28_TestDriveDAO_Test
    Author  -   Ramesh prabu
    Date    -   16/11/2012
    
    #01 <Ramesh prabu> <16/11/2012>
        Created this class using test for VFC28_TestDriveDAO.
**/
@isTest
private class VFC28_TestDriveDAO_Test
{
    static testMethod void VFC28_TestDriveDAO_Test()
    {
    	List<VEH_Veh__c> lstVehs = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
       	List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
       	List<TDV_TestDriveVehicle__c> lstTestDriveVehicle =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
        List<TDV_TestDrive__c> lstTestDrives = VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDrive_RecordsInsertion(lstDealerRecords[0], lstTestDriveVehicle[0]);
        
        Date DateOfBooking = Date.today();
        Date startDateOfBooking = Date.today();
        Date endDateOfBooking = Date.today();

        String model = lstVehs[0].Model__c;
        List<TDV_TestDrive__c> lstTestDrivesRec = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsingCriteria(lstDealerRecords[0].Id, model, DateOfBooking);
        List<TDV_TestDrive__c> lstTestDrivesRecRange = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsingCriteriaByRange(lstDealerRecords[0].Id, model, startDateOfBooking, endDateOfBooking);
        List<TDV_TestDrive__c> lstTestDriveRecord = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsingTestDriveVehId(lstTestDrives[0].TestDriveVehicle__c,DateOfBooking); 
    }
}