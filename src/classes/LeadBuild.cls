@IsTest
public class LeadBuild {
    
    public static LeadBuild instance = null;
    
    public static LeadBuild getInstance() {
        if(instance == null) {
            instance = new LeadBuild();
        }
        return instance;
    }
    
    private LeadBuild() {
    }
    
    public Lead createLead(Id accountId) {
        final Id recordType = Utils.getRecordTypeId('Lead','DVR');
        
        final Lead lead = new Lead();
        lead.FirstName 			 = 'Lead';
        lead.LastName 			 = 'Test';
        lead.CPF_CNPJ__c 		 = '59512193299';
        lead.PromoCode__c 		 = 'test';
        lead.Email 				 = 'testeamil@email.com.br';
        lead.Phone 				 = '12452365';
        lead.DealerOfInterest__c = accountId;
        lead.IdDealer__c 		 = accountId;
        lead.RecordTypeId 		 = recordType;
        lead.CRV_CurrentVehicle_WebToLead__c = 'teste';
        return lead;
    }
}