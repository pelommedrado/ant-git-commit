/**
* Classe que representa os dados da página de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC54_TestDriveVO 
{
	/*atributos utilizados somente para controle interno, não são exibidos na tela 
	 (usados para guardar variáveis para posterior utilização, retornar listas de outros objetos)*/
	public String id {get;set;}
	public String opportunityId {get;set;}
	public String dealerId {get;set;}
	public List<TDV_TestDrive__c> lstSObjTestDriveAgenda {get;set;}
	public List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicleAvailable{get;set;}
	public List<Product2> lstSObjModel {get;set;}
	
	/*atributos referentes a seção informações do test drive*/
	public String name {get;set;}
	public String sellerName {get;set;}
	public String opportunityName {get;set;}
	public TDV_TestDrive__c sObjTestDrive {get;set;}
	public String fuelOutputLevel {get;set;}
	public String status {get;set;}
	public String reasonCancellation {get;set;}
	public String vehicleInterest {get;set;}
	public String outputKM {get;set;}
	public String returnKM {get;set;}
	public String fuelReturnLevel {get;set;}
    public String motor { get;set; }
    public String cambio { get;set; }
    
	/*atributos referentes a seção informações do cliente*/
	public String customerName {get;set;}
	public String currentVehicle {get;set;}
	public String persLandline {get;set;}
	public Integer yearCurrentVehicle {get;set;}
	public String persMobPhone {get;set;}
	
	/*atributos referentes a seção agenda de veículos*/
	public String testDriveVehicleId {get;set;}
	public String plaque {get;set;}
	public String model {get;set;}
	public String version {get;set;}
	public String color {get;set;}
	public DateTime dateBooking {get;set;}
	public String dateBookingFmt {get;set;}
	public Date dateBookingNavigation {get;set;}
	public String dateBookingNavigationFmt {get;set;}
	public String selectedTime {get;set;}
	public String vehicleScheduled {get;set;}
	
	/*atributos referentes a seção avaliação do test drive*/
	public String design {get;set;}
	public String internalSpace {get;set;}
	public String handling {get;set;}
	public String security {get;set;}
	public String performance {get;set;}
	public String panelCommands {get;set;}
	public String equipmentLevel {get;set;}
	
	/*atributo referente a seção interesse após test drive*/
	public String buyingInterestAfterTestDrive {get;set;}
	
	/*atributo referente a seção comentários e sugestões*/
	public String comments {get;set;}
	
	/*atributo referente ao motivo de cancelamento da opportunidade*/
	public String reasonLossCancellationOpportunity {get;set;}
	
	/*picklists que serão exibidos na tela*/
	public List<SelectOption> lstSelOptionStatus {get;set;}
	public List<SelectOption> lstSelOptionReasonCancellation {get;set;}
	public List<SelectOption> lstSelOptionVehicleInterest {get;set;}
	public List<SelectOption> lstSelOptionFuelOutputLevel {get;set;}
	public List<SelectOption> lstSelOptionFuelReturnLevel {get;set;}
	public List<SelectOption> lstSelOptionVehiclesAvailable {get;set;}
	public List<SelectOption> lstSelOptionReasonLossCancellationOpp {get;set;}
	
	/*flags que indicam se os controles estão ou não habilitados/visíveis*/
	public Boolean statusDisabled {get;set;}
	public Boolean reasonCancellationDisabled {get;set;}
	public Boolean disabledFields {get;set;}
	public Boolean navigationAgendaVisible {get;set;}
	public Boolean commentsDisabled {get;set;}
	public Boolean btnCompleteTestDriveDisabled {get;set;}
	public Boolean btnCancelOpportunityDisabled {get;set;}
	public Boolean confirmationGenerationQuoteVisible {get;set;}
	public Boolean reasonCancellationPopUpVisible {get;set;}
	public Boolean btnGenerateQuoteDisabled {get;set;}
	
	/*variável que armazena o título do pop up de geração de orçamento*/
	public String titlePopUpGenerationQuote {get;set;}
	
	/*flag que indica se o test drive já foi atualizado (salvo)*/
	public Boolean updatedTestDrive {get;set;}
	
	/*matriz que contém a agenda do veículo (foi criado uma lista de listas para poder exibir a agenda na tela em formato tabular - matriz)*/
	public List<List<VFC64_ScheduleTestDriveVO>> lstAgendaVehicle {get;set;}
	
	/*lista que contém os documentos RVA*/
	public List<VFC108_DocumentationRVAVO> lstDocumentationRVA {get;set;}
			
	public VFC54_TestDriveVO()
	{
		this.initialize(); 
	}
	
	private void initialize()
	{
		this.disabledFields = false;	
		this.statusDisabled = false;
		this.commentsDisabled = false;
		this.confirmationGenerationQuoteVisible = false;
		this.reasonCancellationPopUpVisible = false;
		this.updatedTestDrive = false;
		this.btnCompleteTestDriveDisabled = false;
		this.navigationAgendaVisible = true;
		this.reasonCancellationDisabled = true;		
		this.btnCancelOpportunityDisabled = true;	
		this.btnGenerateQuoteDisabled = true;		
		this.sObjTestDrive = new TDV_TestDrive__c();
	}
	
}