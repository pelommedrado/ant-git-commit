/** Handle a change of language. Must be called using Rest Api
    @author Donatien Veron (AtoS)
    @date March 17, 2016
**/
@RestResource(urlMapping='/Myr_ManageSetting_WS/*')

global without sharing class Myr_ManageSetting_WS {

	global enum TEST_FAILURE_SIMULATION {FAKEL}
	private class Myr_ManageSetting_WS_Exception extends Exception {}
    
	//Patch Language of user
	@HttpPatch 
    global static Myr_ManageSetting_WS_Response Patch_Language() {
		P_L_Answer L_R = new P_L_Answer();
        L_R.Language = RestContext.request.params.get('LanguageLocaleKey');
		L_R.Begin_Time=DateTime.now().getTime();
		L_R.Error_Level=INDUS_Logger_CLS.ErrorLevel.Critical.name();
		
        if(String.isBlank(L_R.Language)){
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS501;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS501_Msg;
			return P_L_LogAndAnswer (L_R);
		}

        if(L_R.Language.length()>5){
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS502;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS502_Msg;
			return P_L_LogAndAnswer (L_R);
		}
		        
        List<User> List_Users = [select Id, accountId, languageLocaleKey, profileId from User where Id=:UserInfo.getUserId()];

		if (List_Users.size()<1){
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS503;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS503_Msg; 
			return P_L_LogAndAnswer (L_R);
		}
		
		User u = List_Users[0];
		L_R.UserId=u.Id;
		L_R.AccountId=u.accountId;

		if( ! String.isBlank(u.profileId) && u.profileId!=Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id){
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS504;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS504_Msg;
			return P_L_LogAndAnswer (L_R);
		}

		L_R.Error_Level=INDUS_Logger_CLS.ErrorLevel.Info.name();
		if(!String.isBlank(u.languageLocaleKey) && u.languageLocaleKey.equalsIgnoreCase(L_R.Language)){
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS001;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS001_Msg;
			return P_L_LogAndAnswer (L_R);
		}
        
        try{
			//To test general fail
			if( Test.isRunningTest() && TEST_FAILURE_SIMULATION.FAKEL.name().equalsIgnoreCase(L_R.Language) ) {
				throw new Myr_ManageSetting_WS_Exception();
			}
            update u;
        } catch (Exception e) {
			L_R.Code=System.Label.Myr_ManageSetting_WS07MS505;
			L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS505_Msg;
			L_R.Error_Level=INDUS_Logger_CLS.ErrorLevel.Critical.name();
			return P_L_LogAndAnswer (L_R);
        }   
        L_R.Code=System.Label.Myr_ManageSetting_WS07MS000;
		L_R.MessageLabel=System.Label.Myr_ManageSetting_WS07MS000_Msg;
		L_R.Error_Level=INDUS_Logger_CLS.ErrorLevel.Info.name();
		return P_L_LogAndAnswer (L_R);
    }

	private static Myr_ManageSetting_WS_Response P_L_LogAndAnswer(P_L_Answer L_R) {
		try{	        
			log(Myr_ManageSetting_WS.class.getName() + L_R.Begin_Time, L_R.Code, L_R.MessageLabel, L_R.Error_Level, L_R.AccountId, L_R.Begin_Time, 'Language = ' + L_R.Language, L_R.UserId);
		}catch (Exception e){
			//
		}
		Myr_ManageSetting_WS_Response response = new Myr_ManageSetting_WS_Response();
		response.Code = L_R.Code;
		response.MessageLabel = L_R.MessageLabel;
		return response;
	}

	global class Myr_ManageSetting_WS_Response {
		WebService String Code;
		WebService String MessageLabel;
	}

	@future
	private static void log(String logRunId, String MessageCode, String MessageLabel, String ErrorLevel, String myId, Long Begin_Time, String inputs, String UserId) {
		Id id = INDUS_Logger_CLS.addGroupedLogFull(logRunId, null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_ManageSetting_WS', 'Myr_ManageSetting_WS Output', MessageCode,  MessageLabel, ErrorLevel, myId, null, null, Begin_Time, DateTime.now().getTime(), 'Inputs : <' + inputs + '>, UserId : <' + UserId);
	}

	public class P_L_Answer {
		public String Language;
		public Long Begin_Time;
		public String Code;
		public String MessageLabel;
		public String Error_Level;
		public String UserId;
		public String AccountId;
	}

}