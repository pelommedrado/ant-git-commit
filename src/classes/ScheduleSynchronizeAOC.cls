global class ScheduleSynchronizeAOC implements Schedulable, Database.Batchable< Map< String, Object > >, Database.AllowsCallouts {
    
    public Map<String,List<Object>> versionSpecCodShowableSpecCodes = new Map<String,List<Object>>();
    
    private class fieldReadException extends Exception{}
    
    private class Request{
        
        private Map< String, Object > res;
        
        public Request (String endpoint){
            this.res = sendRequest( endpoint );  
        }
        
        public Object get (String field){
            return getFromMap( this.res, field );
        }
        
        public void debug(){
            System.debug( '\n@@@\n' + JSON.serializePretty( this.res ) );
        }
    }
    
    private static final String BASEURL = 'http://br.co.rplug.renault.com/';
    
    private static Map< String, Object > sendRequest (String endpoint){
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint( endpoint );
        httpReq.setMethod( 'GET' );
        httpReq.setHeader( 'Accept', 'application/json' );
        
        HttpResponse httpRes = new Http().send( httpReq );
        system.debug('***httpRes: '+httpRes.getBody());
        return (Map< String, Object >)JSON.deserializeUntyped( httpRes.getBody() );
    }
    
    private static Object getFromMap (Object target, String field){
        if( String.isBlank( field ) ) return target;
        
        List< String > fieldList = field.split( '\\.' );
        try{
            return getFromMap( fieldList[0].isNumeric() ?
                              ((List< Object >)target)[Integer.valueOf( fieldList[0] )] :
                              ((Map< String, Object >)target).get( fieldList[0] ),
                              String.join( sliceStart( fieldList, 1 ), '.' ) );
        }catch( System.NullPointerException e ){
            throw new fieldReadException( 'error-handling|field: ' + field + ' target: ' + JSON.serializePretty( target ) );
        }
        return null;
    }
    
    private static List< String > sliceStart (List< String > target, Integer newStart){
        if( target.isEmpty() ) return target;
        List< String > copy = new List< String >();
        for( Integer i=newStart;i<target.size();i++ ) copy.add( target[i] );
        return copy;
    }
    
    private static String fullYear (String year){
        System.assert( year.length() == 2 || year.length() == 4 );
        return (year.length() == 2 ? '20' : '') + year;
    }
    
    private String processMilesime (List<Object> target){
        for(Object obj: target){
            Matcher m = Pattern.compile( 'NFG\\$(\\d{2}(?:\\d{2})?)/(\\d{2}(?:\\d{2})?)$' ).matcher( (String)obj );
            if( !m.find() ) continue;
            return fullYear( m.group( 1 ) ) + '/' + fullYear( m.group( 2 ) );
        }    
        return '';
    }
    
    // presGroups = doc.presentationData.marketingModelPresentation.marketingModelPresentation.versionsPresentation
    private  List< PVVersion__c > processPresentationGroup (Map<String,Object> versCatMap, Map<String,Object> semiClairMap, 
                                                            Map< String, Object > presGroup, Model__c model){
        List< PVVersion__c > versionList = new List< PVVersion__c >();
                                                                
                                                                system.debug('###PRESGROUP: '+presGroup);
                                                                
        String ENS = (String)getFromMap( presGroup, 'presentationGroup.key' );
        
        Map<String, Id> modelMilesimePKMap = new Map<String, Id>();
        
        for( Object item : (List< Object >)getFromMap( presGroup, 'presentationGroup.items' ) )
            if( ((Map< String, Object >)item).containsKey( 'presentationGroup' ) )
            versionList.addAll( processPresentationGroup( (Map<String,Object>) versCatMap, (Map<String,Object>) semiClairMap,
                                                         (Map< String, Object >)item, model ) );
        else if( ((Map< String, Object >)item).containsKey( 'versionItem' ) ){
            
            String milesime = model.Model_Spec_Code__c == 'MAH' ? '2017/2018' :
                processMilesime( (List<Object>)getFromMap( item, 'versionItem.showableSpecCodes' ) );
	    if( String.isEmpty( milesime ) ) continue;
            
            String modelMilesimePK = model.Model_PK__c + ENS + milesime;
            
            if( !modelMilesimePKMap.containsKey(modelMilesimePK) ){
                PV_ModelMilesime__c modelMilesime = new PV_ModelMilesime__c(
                    Model__c = model.Id, 
                    ENS__c = ENS,
                    Milesime_PK__c = modelMilesimePK,
                    Status__c = 'Active',
                    Milesime__c = milesime,
                    Name = model.Name + ' ' + milesime
                );
                Database.upsert( modelMilesime, PV_ModelMilesime__c.fields.Milesime_PK__c );
                modelMilesimePKMap.put(modelMilesimePK, modelMilesime.Id);
            }
            
            String versionIdSpecCode = (String)getFromMap( item, 'versionItem.versionIdSpecCode' );
            
            // obtem todas as categorias da versao para extrair o semiclair
            Map<String,Object> categVersao = (Map<String,Object>)getFromMap(versCatMap.get(versionIdSpecCode), 'categories.mapRepresentation.map');
            
            // extrai o semiclair da versão
            String semiclair;
            for(Object obj: categVersao.keySet()){
                String cat = (String)obj;
                if(semiClairMap.containsKey(cat) && categVersao.get(cat) == 'SYSTEMATIC'){
                    semiclair = cat;
                }
            }
            
            versionList.add( new PVVersion__c(
                Model__c = model.Id,
                Name = (String)getFromMap( item, 'versionItem.label.pt' ),
                Milesime__c =  modelMilesimePKMap.get(modelMilesimePK),
                Version_Id_Spec_Code__c = versionIdSpecCode,
                PVC_Maximo__c = 100,
                PVR_Minimo__c = 100,
                Version_PK__c = modelMilesimePK + versionIdSpecCode,
                Semiclair__c = semiclair
            ) );
            
            versionSpecCodShowableSpecCodes.put(versionIdSpecCode,(List<Object>)getFromMap( item, 'versionItem.showableSpecCodes' )); 
            
        }  
        else throw new fieldReadException( 'error-handling| CAN\'T PROCESS: ' + JSON.serializePretty( item ) );
        
        return versionList;
    }
    
    // priceInfo = doc.pricesData.pricesList.priceList.0.versionPriceList.versionIdSpecCode(in option relationship).optionsPrices.mapRepresentation.map
    // dictionary = doc.presentationData.marketingModelPresentation.marketingModelPresentation.asMap.mapRepresentation.map
    private List< Optional__c > extractOptionals (Map<String,Object> versCatMap, Map<String,String> catMap, PVVersion__c version, 
                                                  Map< String, Object > priceInfo, Map< String, Object > dictionary){      
        List< Optional__c > optionalList = new List< Optional__c >();
        Set<String> optionalPK = new Set<String>();                                                    
        
        // obtem todas as categorias da versao para extrair os opcionais que podem ou não existir para esta versao
        Map<String,Object> categVersao = 
            (Map<String,Object>)getFromMap(versCatMap.get(version.Version_Id_Spec_Code__c), 'categories.mapRepresentation.map'); 
         
        for( String optionalCode : catMap.keySet() ){
            
            // verifica se este opcional existe para esta versao
            if( !optionalPK.contains(version.Version_PK__c + optionalCode) && categVersao.get(optionalCode) != 'FORBIDDEN' ){
            
                try{
                    String description;
                    
                    try{
                        description = (String)getFromMap( dictionary, optionalCode + '.presentationItem.label.pt' );
                        if( description.length() > 80 ) description = description.substring( 0 , 80 );
                    }catch (Exception e){
                        description = optionalCode;
                    }
                    
                    optionalList.add( new Optional__c(
                        Optional_Code__c = optionalCode,
                        Amount__c = (Decimal)priceInfo.get( optionalCode ),
                        Name = description,
                        Version__c = version.Id,
                        Type__c = catMap.get(optionalCode),
                        Optional_PK__c = version.Version_PK__c + optionalCode
                    ) );
                
                optionalPK.add(version.Version_PK__c + optionalCode);
                
                }catch( fieldReadException e ){
                    throw new fieldReadException( 'Error extracting optional: ' + optionalCode + ' with original message "' +
                                                 e.getMessage() + '"\n' + JSON.serializePretty( optionalList ) );
                }
                
            }
        }
                                                      
        for( String optionalCode : new List< String >( priceInfo.keyset() ) ){
            
            // verifica se este opcional existe para esta versao
            if( !optionalPK.contains(version.Version_PK__c + optionalCode) && categVersao.get(optionalCode) != 'FORBIDDEN' ){
                
                try{
                    String description;
                    
                    try{
                        description = (String)getFromMap( dictionary, optionalCode + '.presentationItem.label.pt' );
                        if( description.length() > 80 ) description = description.substring( 0 , 80 );
                    }catch (Exception e){
                        description = optionalCode;
                    }
                    
                    optionalList.add( new Optional__c(
                        Optional_Code__c = optionalCode,
                        Amount__c = (Decimal)priceInfo.get( optionalCode ),
                        Name = description,
                        Version__c = version.Id,
                        Type__c = (optionalCode.startsWith('PK') || optionalCode.contains('PRLEX1')) 
                        	&& !optionalCode.contains('PKRODA') && !optionalCode.contains('PKBR01') ? 'Pack' : 'Item',
                        Optional_PK__c = version.Version_PK__c + optionalCode
                    ) );
                    
                    optionalPK.add(version.Version_PK__c + optionalCode);
                    
                }catch( fieldReadException e ){
                    throw new fieldReadException( 'Error extracting optional: ' + optionalCode + ' with original message "' +
                                                 e.getMessage() + '"\n' + JSON.serializePretty( optionalList ) );
                }
            }
        }
        
        system.debug('***optionalList: '+optionalList);
        return optionalList;
    }
    
    // presGroups = doc.presentationData.marketingModelPresentation.marketingModelPresentation.versionsPresentation
    private  List< PVVersion__c > extractVersions (Map<String,Object> versCatMap, Map<String,Object> semiClairMap, 
                                                   List< Object > presGroups, Model__c model){
        List< PVVersion__c > versionList = new List< PVVersion__c >();
        for( Object presGroup : presGroups )
            versionList.addAll( processPresentationGroup( (Map<String,Object>) versCatMap, (Map<String,Object>) semiClairMap,
                                                         (Map< String, Object >)presGroup, model) );
        return versionList;
    }
    
    // docData = docs (http://br.co.rplug.renault.com/docs)
    private void processModel (Map< String, Object > docData){
        Request doc = new Request( (String)getFromMap( docData, 'doc' ) );
        doc.debug();
        
        Request semiClairs = new Request( (String)doc.get( 'diversityData.localSemiClair' ) );
        system.debug('***semiClairs: '+JSON.serializePretty(semiClairs));
        
        Request specCat = new Request( (String)doc.get( 'diversityData.specificationCategories' ) );
        system.debug('***specCat: '+JSON.serializePretty(specCat));
        
        Request versCat = new Request( (String)doc.get( 'diversityData.versionsCategories' ) );
        system.debug('***versCat: '+JSON.serializePretty(versCat));
        
        Request pres = new Request( (String)doc.get( 'presentationData.marketingModelPresentation' ) );
        system.debug('***pres: '+JSON.serializePretty(pres));
        
        Request priceList = new Request( (String)doc.get( 'pricesData.pricesList' ) );
        priceList.debug();
        
        String idMilesime = ((String)doc.get( 'requestedURI' )).removeStart( BASEURL + 'doc/' );
        
        Model__c model = new Model__c(
            Name = (String)pres.get( 'marketingModelPresentation.label.pt' ),
            Program__c = (String)doc.get( 'docId.program' ),
            Phase__c = (String)doc.get( 'docId.phase' ),
            Market__c = (String)doc.get( 'docId.market' ),
            Model_Spec_Code__c   = (String)doc.get( 'docId.modelSpecCode' ),
            Model_PK__c = (String)doc.get( 'docId.modelSpecCode' ) + '-' + (String)doc.get( 'docId.phase' ),
            Id_Milesime__c = idMilesime,
            Semiclair__c = (String)doc.get( 'docId.localSemiclair' ),
            TypeVehicle__c = (String)doc.get( 'docId.commercialKind' ),
            Status_SFA__c = 'Active'
        );
        
        System.debug( '@@@| Model: ' + JSON.serializePretty( model ) );
        Database.upsert( model, Model__c.fields.Model_PK__c );
        
        // extrair todas as categorias do modelo
        Map<String,String> catMap = new Map<String,String>();
        for( Object node : (List< Object >)specCat.get( 'specCat' ) ){
            
            // chave de cores
            if((String)getFromMap( node, '@type' ) == 'colorSpecCat'){
                for(Object obj: (List<Object>)getFromMap( node, 'spec' ))
                    catMap.put((String)getFromMap( obj, 'specCode' ),'Cor');
            }
            
            // chave de pintura
            if((String)getFromMap( node, '@type' ) == 'colorQualitySpecCat'){
                for(Object obj: (List<Object>)getFromMap( node, 'spec' ))
                    catMap.put((String)getFromMap( obj, 'specCode' ),'Pintura');
            }
            
            // chave de harmonia
            if((String)getFromMap( node, '@type' ) == 'colorThemeSpecCat'){
                for(Object obj: (List<Object>)getFromMap( node, 'spec' ))
                    catMap.put((String)getFromMap( obj, 'specCode' ),'Harmonia');
            }
            
            // chave de estofados
            if((String)getFromMap( node, '@type' ) == 'trimSpecCat'){
                for(Object obj: (List<Object>)getFromMap( node, 'spec' ))
                    catMap.put((String)getFromMap( obj, 'specCode' ),'Trim');
            }
        }
        
        System.debug( '@@@| catMap: ' + JSON.serializePretty( catMap ) );
        
        // mapa de todos semiclaros do modelo
        Map< String, Object > semiClairMap = (Map< String, Object >)semiClairs.get( 'localSemiClair.mapRepresentation.map' );
        
        System.debug( '@@@| semiClairMap: ' + JSON.serializePretty( semiClairMap ) );
        
        // mapa de preço por versão
        Map< String, Object > priceMap = new Map< String, Object >();
        for( Object node : (List< Object >)priceList.get( 'priceList.0.versionPriceList' ) )
            priceMap.put( (String)getFromMap( node, 'versionIdSpecCode' ), node );
        
        System.debug( '@@@| Pricemap: ' + JSON.serializePretty( priceMap ) );
        
        // mapa das categorias por versão
        Map< String, Object > versCatMap = new Map< String, Object >();
        for( Object node : (List< Object >)versCat.get( 'versionCategories' ) )
            versCatMap.put( (String)getFromMap( node, 'versionIdSpecCode' ), node );
        
        System.debug( '@@@| versCatMap: ' + JSON.serializePretty( versCatMap ) );
        
        List< PVVersion__c > versionList = 
            extractVersions( versCatMap, semiClairMap, (List< Object >)pres.get( 'marketingModelPresentation.versionsPresentation' ), model );
        
        Map< String, Object > dictionary =
            (Map< String, Object >)pres.get( 'marketingModelPresentation.asMap.mapRepresentation.map' );
        System.debug( '@@@| Dictionary: ' + JSON.serializePretty( dictionary ) );
        
        for( PVVersion__c version : versionList )
            version.Price__c = (Decimal)getFromMap( priceMap.get( version.Version_Id_Spec_Code__c ), 'price' );
        
        System.debug( '@@@| Versions: ' + JSON.serializePretty( versionList ) );
        Database.upsert( versionList, PVVersion__c.fields.Version_PK__c );
        
        List< Optional__c > optionalList = new List< Optional__c >();
        
        for( PVVersion__c version : versionList )
            optionalList.addAll( extractOptionals(
                versCatMap,
                catMap,
                version,
                (Map< String, Object >)getFromMap( priceMap, version.Version_Id_Spec_Code__c + '.optionsPrices.mapRepresentation.map' ),
                dictionary
            ) );
        
        System.debug( '@@@| Optionals: ' + JSON.serializePretty( optionalList ) );
        Database.upsert( optionalList, Optional__c.fields.Optional_PK__c );
        
    }
    
    //Schedulable method
    public void execute (SchedulableContext sc) {
        Database.executeBatch( this, 1 );
    }
    
    //Batchable methods
    public List< Map< String, Object > > start (Database.BatchableContext bc){
        Request root = new Request( BASEURL + 'docs' );
        
        List< PV_Integration_Aoc__c > AOCIntegration = [
            select ByPass_Aoc__c 
            from PV_Integration_Aoc__c 
            order by CreatedDate desc
            limit 1
        ];
        
        String docgroup = ((String)root.get( 'docgroup' )).removeStart( BASEURL + 'doc/' );
        
        List< Map< String, Object > > retList = new List< Map< String, Object > >();
        
        //quando for rodar este serviço mais que uma vez ao dia, descomentar a linha abaixo
        //for( Object node : (List< Object >)root.get( 'docs' ) ) retList.add( (Map< String, Object >)node );
        
        if( AOCIntegration.size() == 0 || AOCIntegration[0].ByPass_Aoc__c != docgroup ){
            Database.insert( new PV_Integration_Aoc__c( ByPass_Aoc__c = docgroup ) );
            for( Object node : (List< Object >)root.get( 'docs' ) ) retList.add( (Map< String, Object >)node );
        }
        
        return retList;
        
    }
    
    public void execute (Database.BatchableContext bc, List< Map< String, Object > > scope){
        System.assertEquals( 1, scope.size(), 'Batch size must be exactly 1' );
        processModel( scope[0] );
    }
    
    public void finish (Database.BatchableContext bc){}
    
}