/**
*	Class	-	VFC33_TaskDAO
*	Author	-	Surseh Babu
*	Date	-	06/12/2012
*
*	#01 <Suresh Babu> <06/12/2012>
*		DAO class for Task object to Query records.
**/
public with sharing class VFC33_TaskDAO extends VFC01_SObjectDAO
{
	private static final VFC33_TaskDAO instance = new VFC33_TaskDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC33_TaskDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC33_TaskDAO getInstance()
    {
        return instance;
    }
    
    /**
    * Localiza uma determinada tarefa na base de dados.
    * @param id O Id da tarefa.
    * @return Um objeto Task ou null caso a tarefa não exista.
    */
    public Task findById(String id)
    {
    	Task sObjTask = null;
    	
    	try
    	{
    		sObjTask = [SELECT Id, 
    		                   WhatId
    		            FROM Task
    		            WHERE Id =: id];
    	}
    	catch(QueryException ex)
    	{
    		
    	}
    	
    	return sObjTask;
    }
    
    public List<Task> findByOpportunityId(Set<String> setOpportunityId, String status)
    {
    	List<Task> lstSObjTask = [SELECT Id
    	                          FROM Task
    	                          WHERE WhatId IN : setOpportunityId
    	                          AND Status != : status];
    	
    	return lstSObjTask;
    	
    }
    
    public List<Task> findOpenRecurrencebyOpportunityId(String opportunityId)
    {
    	List<Task> lstSObjTask = [SELECT Id
    	                          FROM Task
    	                          WHERE WhatId = :opportunityId
    	                          AND IsRecurrence = true
    	                          AND IsClosed = false ];
    	
    	System.debug('>>>>>>>>>>>>>>>>>>lstSObjTask: ' + lstSObjTask );
    	
    	return lstSObjTask;
    	
    }
    
}