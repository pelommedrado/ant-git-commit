global class WSC08_RetailPlatform{
    
    private static Datetime now(){
        Datetime now = System.now();
        return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
    }
    
    private static String buildMessage (Boolean success, String message, List< Map< String, Object > > response){
        return Json.SerializePretty( new Map< String, Object >{
            'success' => success,
                'timestamp' => now(),
                'message' => message,
                'data' => response
                } );
    }
    
    static webservice String getOffersMock (Datetime lastUpdateTime){
        List< Map< String, Object > > offerWrapperList = new List< Map< String, Object > >();
        offerWrapperList.addAll( new List< Map< String, Object > >{
            new Map< String, Object >{
                'OfferId' => '00X000000000000XXX',
                    'DealersIds' => '00Y000000000000XXX,00Y000000000000XXX,00Y000000000000XXX',
                    'OfferStartDate' => System.now().addMonths( -1 ),
                    'OfferEndDate' => System.now().addMonths( 1 ),
                    'VehicleCodeAOC' => 'ABC',
                    'VehicleVersionAOC' => 'VEC016_BRES',
                    'IdMilesimeAOC' => 'BABi',
                    'NumberOfInstallments' => 36,
                    'MonthlyTax' => 0.99,
                    'ValueFrom' => 65350.00,
                    'ValueTo' => 65350.00,
                    'ValueToOffer' => true,
                    'EntryValue' => 65350.00,
                    'InstallmentValue' => 65350.00,
                    'TotalInventoryVehicle' => 1000,
                    'PricingTemplate' => 'PricingTemplate',
                    'FeaturedInOffer' => 'Featured in offer',
                    'FeaturedInProduct' => '\'Ar condicionado\', \'Dire??o Hidr?ulica\', \'Media Nav\'',
                    'ProductAdvantages' => new Map< String, String >{
                        'Technology' => 'Texto da tecnologia',
                            'Power' => 'Maior pot?ncia'
                            },
                                'Stamp' => '\'Mega B?nus\',\'24hs de Validade\'',
                                'FeaturedPicture' => 'Front',
                                'IsUpgrade' => true,
                                'TypeOfUpgrade' => 'Engine',
                                'OfferUpgradeId' => '00X000000000000XXX',
                                'OfferUpgradeDescription' => 'Com apenas mais R$70,00 na parcela, voc? leva c?mbio automatico',
                                'LegalText' => 'Utilizado como base o texto jur?dico recebido com exemplo',
                                'Status' => 'active',
                                'offerType' => 'Cooperada',
                                'merchandisingCode' => 'CBBR08694'
                                },
                                    new Map< String, Object >{
                                        'OfferId' => '00X000000000001XXX',
                                            'DealersIds' => '00Y000000000000XXX,00Y000000000000XXX,00Y000000000000XXX',
                                            'OfferStartDate' => System.now().addMonths( -2 ),
                                            'OfferEndDate' => System.now().addMonths( 2 ),
                                            'VehicleCodeAOC' => 'CBA',
                                            'VehicleVersionAOC' => 'VEC016_BRES',
                                            'IdMilesimeAOC' => 'BABi',
                                            'NumberOfInstallments' => 36,
                                            'MonthlyTax' => 0.99,
                                            'ValueFrom' => 65355.00,
                                            'ValueTo' => 65355.00,
                                            'ValueToOffer' => false,
                                            'EntryValue' => 65355.00,
                                            'InstallmentValue' => 65355.00,
                                            'DailyInstallment' => 2178.50,
                    						'ActiveDailyInstallment' => true,
                                            'TotalInventoryVehicle' => 100,
                                            'PricingTemplate' => 'PricingTemplate',
                                            'FeaturedInOffer' => 'Featured in offer',
                                            'FeaturedInProduct' => '\'Ar condicionado\', \'Dire??o Hidr?ulica\', \'Media Nav\'',
                                            'ProductAdvantages' => new Map< String, String >{
                                                'Technology' => 'Texto da tecnologia',
                                                    'Power' => 'Maior pot?ncia'
                                                    },
                                                        'Stamp' => '\'Mega B?nus\',\'24hs de Validade\'',
                                                        'FeaturedPicture' => 'Front',
                                                        'IsUpgrade' => true,
                                                        'TypeOfUpgrade' => 'Engine',
                                                        'OfferUpgradeId' => '00X000000000001XXX',
                                                        'OfferUpgradeDescription' => 'Com apenas mais R$50,00 na parcela, voc? leva c?mbio automatico',
                                                        'LegalText' => 'Utilizado como base o texto jur?dico recebido com exemplo',
                                                        'Status' => 'inactive',
                                                        'offerType' => 'Cooperada',
                                                        'merchandisingCode' => 'CBBR08695'
                                                        }
        } );
        
        return buildMessage( true, 'OK', offerWrapperList );
    }
    
    static webservice String getOffers(Datetime lastUpdateTime){
        List< Map< String, Object > > offerWrapperList = new List< Map< String, Object > >();
        for( Offer__c offer : WSC08_RetailPlatform_QueryWrapper.queryOffers( lastUpdateTime )  ){
            Map< String, Object > advantageMap = new Map< String, Object >();
            if( offer.Version__r.Comfort__c  && String.isNotBlank( offer.Version__r.Confort_Description__c ) )
                advantageMap.put( 'Comfort', offer.Version__r.Confort_Description__c );
            if( offer.Version__r.Performance__c  && String.isNotBlank( offer.Version__r.Performance_Description__c ) )
                advantageMap.put( 'Performance', offer.Version__r.Performance_Description__c );
            if( offer.Version__r.Power__c  && String.isNotBlank( offer.Version__r.Power_Description__c ) )
                advantageMap.put( 'Power', offer.Version__r.Power_Description__c );
            if( offer.Version__r.Security__c  && String.isNotBlank( offer.Version__r.Security_Description__c ) )
                advantageMap.put( 'Security', offer.Version__r.Security_Description__c );
            if( offer.Version__r.Technology__c  && String.isNotBlank( offer.Version__r.Technology_Description__c ) )
                advantageMap.put( 'Technology', offer.Version__r.Technology_Description__c );
            if( offer.Version__r.Volume__c  && String.isNotBlank( offer.Version__r.Description_Volume__c ) )
                advantageMap.put( 'Volume', offer.Version__r.Description_Volume__c );
            
            /*Matcher m = Pattern.compile( '(\\d{2}(?:\\d{2})?)/(\\d{2}(?:\\d{2})?)$' ).matcher( offer.Version__r.Name );
            if( !m.find() ) continue;
            String milesime =
                (m.group( 1 ).length() == 2 ? '20' : '') + m.group( 1 ) + '/' + (m.group( 2 ).length() == 2 ? '20' : '') + m.group( 2 );*/
            
            offerWrapperList.add( new Map< String, Object >{
                'OfferHeader' => offer.Offer_Header__r.Code__c,
                    'OfferId' => offer.Id,
                    'OfferUpgradeStatus' => ((offer.hasUpgrade__c) ? (((offer.Offer_Upgrade__r.Status__c == 'ACTIVE')&&(offer.Offer_Upgrade__r.Stage__c == 'Approved')) ? 'ACTIVE':null):null)   ,
                    'OfferStartDate' => offer.Offer_Start_Date_Website__c,
                    'OfferEndDate' => offer.Offer_End_Date_Website__c,
                    'VehicleCodeAOC' => offer.Model__r.Model_Spec_Code__c,
                    'VehicleVersionAOC' => offer.Version__r.Version_Id_Spec_Code__c + ';' + offer.Model__r.Model_Spec_Code__c + ';' + offer.Version__r.Milesime__r.Milesime__c,
                    //'VehicleVersionAOC' => offer.Version__r.Version_Id_Spec_Code__c,
                    'IdMilesimeAOC' => offer.Model__r.Id_Milesime__c,
                    'NumberOfInstallments' => offer.Number_Of_Installments__c,
                    'MonthlyTax' => offer.Monthly_Tax__c,
                    'ValueFrom' => offer.Value_From__c, 
                    'ValueTo' => offer.ValueTo__c,
                    'FromToOffer' => offer.From_To_Offer__c,
                    'EntryValue' => offer.Entry_Value__c,
                    'InstallmentValue' => offer.Installment_Value__c,
					'DailyInstallment' => offer.ParcelaDia__c,
					'ActiveDailyInstallment' => offer.ativarParcelaDia__c,
                    'TotalInventoryVehicle' => offer.Total_Inventory_Vehicle__c,
                    'PricingTemplate' => offer.Pricing_Template_Lookup__r.Code__c,
                    'FeaturedInOffer' => /*(String.isEmpty(offer.Featured_In_Offer_Lookup__c) && offer.Dealers_Offers__r.size() == 1) ?
                    offer.Dealers_Offers__r[0].Dealer__r.Name :*/
                offer.Featured_In_Offer_Lookup__r.Code__c,
                    'FeaturedInCampaign' => (offer.Dealer_Offer__c ? offer.Created_by_Dealer__c : offer.Featured_in_Campaign__r.Code__c),
                    'FeaturedInProduct' => offer.Featured_Product_Text__c,
                    'ProductAdvantages' => advantageMap,
                    'Stamp' => (offer.Stamp_Lookup__r.Code__c + (String.isEmpty(offer.Stamp_Lookup_2__r.Code__c) ? '' : ';' + offer.Stamp_Lookup_2__r.Code__c) ),
                    'FeaturedPicture' => offer.Version__r.FeaturedPicture__c,
                    'IsUpgrade' => offer.hasUpgrade__c,
                    'TypeOfUpgrade' => offer.Type_Of_Upgrade__c,
                    'OfferUpgradeId' => offer.Offer_Upgrade__c,
                    'OfferUpgradeDescription' => offer.Offer_Upgrade_Description__c,
                    'LegalText' => offer.Legal_Text_VU_Manual__c != null ? offer.Legal_Text_VU_Manual__c : offer.Legal_Text__c,
                    //'LegalText' => offer.Legal_Text__c,
                    'Status' => offer.Status__c,
                    'OfferType' => (offer.National_Offer__c ? offer.Group_Offer__r.Type_of_Offer__c + ';NATIONAL' : offer.Group_Offer__r.Type_of_Offer__c),
                     //'OfferType' => offer.Group_Offer__r.Type_of_Offer__c,
                    'MerchandisingCode' => offer.Mercadoria__r.Name,
                    'Optional' => offer.Optional__r.Name,
                    'Painting' => offer.Painting__r.Name,
                    'DealersIds' => getFieldListAsString( offer.Dealers_Offers__r, 'Dealer__c' )
                    } );
        }
        
        return buildMessage( true, 'OK', offerWrapperList );
    }
    
    static webservice String getDealers (Datetime lastUpdateTime){
        List< Map< String, Object > > accWrapperList = new List< Map< String, Object > >();
        for( Account acc : WSC08_RetailPlatform_QueryWrapper.queryDealers( lastUpdateTime ) ){
            accWrapperList.add( new Map< String, Object >{
                'AccountId' => acc.Id,
                    'Name' => acc.Name,
                    'Street' => acc.ShippingStreet,
                    'City' => acc.ShippingCity,
                    'PostalCode' => acc.ShippingPostalCode,
                    'State' => acc.ShippingState,
                    'Country' => acc.ShippingCountry,
                    'Status__c' => acc.Status__c,
                    'IDBIR__c' => acc.IDBIR__c,
                    'Latitude__c' => acc.Latitude__c,
                    'Longitude__c' => acc.Longitude__c,
                    //'Latitude__c' => Double.valueOf( acc.Latitude__c.replaceAll( '[^\\x00-\\x7F]', '' ).replace( ',', '.' ) ),
                    //'Longitude__c' => Double.valueOf( acc.Longitude__c.replaceAll( '[^\\x00-\\x7F]', '' ).replace( ',', '.' ) ),
                    'Phone' => acc.Phone
                    } );
        }
        return buildMessage( true, 'OK', accWrapperList );
    }
    
    private static String getFieldListAsString (List< SObject > targetList, String fieldName){
        List< String > fieldList = new List< String >();
        for( SObject target : targetList ) fieldList.add( String.valueOf( target.get( fieldName ) ) );
        return String.join( fieldList, ',' );
    }
    
}