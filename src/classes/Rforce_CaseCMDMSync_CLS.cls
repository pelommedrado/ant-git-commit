/**
 * @Project : Rforce
 * @Class Name : Rforce_CaseCMDMSync_CLS
 * @TestClass Name : Rforce_CaseCMDMSync_Test
 * @Author: RNTBCI(HQREQ 7929)
 * @Date: 29/06/2017
 * @Description: This class is used to handle Case and synchronisation CMDM.

 */
public class Rforce_CaseCMDMSync_CLS {  
    
    public static void updateAccountStopComRCFromToCaseBeforeInsert(List<Case> lstCase) {
        
        Set<Id> accIds = new Set<Id>();
        Map<Id,Case> accasmap = new Map<Id,Case>();
        Map<Id,Case> accNewmap = new Map<Id,Case>();
        Map<Id,Case> accOldmap = new Map<Id,Case>();
        List<Account> acList = new List<Account>();
        List<Account> acNewList = new List<Account>();
        for(Case cs :lstCase) {
            System.debug('### Rforce_CaseCMDMSync_CLS : Case Type -> ' + cs.Type + ' and Account Id -> ' + cs.AccountId);
            if(cs.Type == system.label.CAS_Type_Complaint && cs.AccountId != null && cs.Status !='Draft') {
                accasmap.put(cs.AccountId, cs);                 
            }
            System.debug('### Rforce_CaseCMDMSync_CLS : Account id  -> '+ accIds);
            System.debug('### Rforce_CaseCMDMSync_CLS : CaseList -> '+ lstCase);       
        }

        system.debug('### Rforce_CaseCMDMSync_CLS : Accids that has to be updated - accasmap -> ' + accasmap);
        system.debug('### Rforce_CaseCMDMSync_CLS : Accids that has to be updated - accasmap -> ' + accasmap.keyset());
        if(accasmap != null) {
            /* if this is the first open complant, we set the stopcom RC */
            for(List<Case> cs : [SELECT Id, AccountId from Case where AccountId IN : accasmap.keyset() and type = 'Complaint' and status !='Draft' and status != 'Closed' ]) {
                System.debug('### Rforce_CaseCMDMSync_CLS : CS Size ->222 ' + cs.size());
                
                /* each account that's already associated with an open complaint needs no changes - removed from the list */
                for(Case caseStat : cs) {
                    accasmap.remove(caseStat.AccountId);
                }
                
                /* we modify all the remaining accounts */
                for(Account a:[SELECT Id,StopComRCFrom__c, StopComRCTo__c, CreatedDate  FROM Account WHERE Id IN:accasmap.keyset()]) { 
                    a.StopComRCTo__C =  null;
                    a.StopComRCFrom__c = Datetime.now(); 
                    acList.add(a);
                }
                              
            } try {
                update acList;
                System.debug('### Rforce_CaseCMDMSync_CLS : updateAccountStopComRCFromTo acList -> ' + acList);
            } catch(Exception e) {
                System.debug('### Rforce_CaseCMDMSync_CLS :updateAccountStopComRCFromTo -> ' + e);
            }
        }     
    }
    //Update StopRCcomFrom and To
    public static void updateAccstopcomRC(list <Case> listOldCas, list <Case> listNewCas, String action) {
        Map<Id,Case> accasmap = new Map<Id,Case>();
        Map<Id,Case> caseValues=new Map<Id,Case>();
        Map<Id,Datetime> accFromDateMap = new Map<Id, Datetime>();
        List<Account> aclist = new List<Account>();
        system.debug('### Rforce_CaseCMDMSync_CLS :updateAccstopcomRC');
        if (action == 'delete') {
            for(Case c:listOldCas) {
                /* we only consider deleting of open complaints as they only can impact the stopcomRC value */
                if (c.AccountId != null && c.type == 'Complaint' && c.status !='Draft' && c.status != 'Closed') {
                    accasmap.put(c.AccountId,c);
                    caseValues.put(c.Id,c);
                }
            } 
        } else {
            for(Case c:listOldCas) {
                /* we consider open complaints that were modified as they can impact the stopcomRC value */
                if (c.AccountId != null && c.type == 'Complaint' && c.status !='Draft' && c.status != 'Closed') {
                    accasmap.put(c.AccountId,c);
                    caseValues.put(c.Id,c);
                }
            }
            
            for (Case c:listNewCas) {
                /* we consider cases that were modified to become open complaints as they can impact the stopcomRC value */
                if (c.AccountId != null && c.type == 'Complaint' && c.status !='Draft' && c.status != 'Closed') {
                    accasmap.put(c.AccountId,c);
                    caseValues.put(c.Id,c);
                }
            } 
        }

        system.debug('### Rforce_CaseCMDMSync_CLS :updateAccstopcomDelete Accids that has to be updated - accasmap -> ' + accasmap);
        system.debug('#######size of the Account and cases'+accasmap.size());
        system.debug('######### Case Values size : '+caseValues);
        if(accasmap != null) {
            /* searching for remaining open complaints */
            for(List<Case> cs : [SELECT Id , CaseNumber, status, CreatedDate,ClosedDate,AccountId FROM Case WHERE AccountId IN : accasmap.keyset() and type = 'Complaint' and status !='Draft' and status != 'Closed' ORDER BY AccountId ASC, CreatedDate ASC]) {
                System.debug('### Rforce_CaseCMDMSync_CLS : CSSS Delete-> ' + cs.size());
                
                if(cs.size() == 0) {
                    /* no remaining open complaints for the accounts, set the TO date to close the stopcom RC */
                    for(Account a:[SELECT Id,StopComRCFrom__c, StopComRCTo__c, CreatedDate  FROM Account WHERE Id IN:accasmap.keyset()]){ 
                        a.StopComRCTo__c = Datetime.now();
                        acList.add(a);
                    }
                }
                else {
                    /* there are still remaining open complaints for the accounts, set new stopcom RC values */
                    Id currentAcc = null;
                    for(Case caseList : cs) {
                        /* for each account, we get the first CreatedDate from the list (smallest) */
                        if(caseList.AccountId != currentAcc) {
                            currentAcc = caseList.AccountId;
                            accFromDateMap.put(caseList.AccountId, caseList.CreatedDate);
                        }
                    }
                    
                    System.debug('### Rforce_CaseCMDMSync_CLS : map of accounts to update -> ' + accFromDateMap);    
                    for(Account a:[SELECT Id,StopComRCFrom__c, StopComRCTo__c, CreatedDate FROM Account WHERE Id IN : accFromDateMap.keyset()]){
                        /* if there are remaining open complaints and stopcom RC was closed, we open it */
                        if (a.StopComRCTo__c != null) {
                            a.StopComRCFrom__c = Datetime.now();
                            a.StopComRCTo__c = null;
                        }
                        /* otherwise, if the stopcom RC was open, we leave it as-is */
                        acList.add(a); 
                    }
                }
            }
        }       
        
        try {
            update aclist;
        } catch(Exception e) {
            system.debug('### Rforce_CaseCMDMSync_CLS : Exception e -> ' + e);
        }    
    }        
                   
}