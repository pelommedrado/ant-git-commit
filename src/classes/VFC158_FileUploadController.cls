public with sharing class VFC158_FileUploadController implements FileUploaderCaller{

  public Id parentId{
    get{
      return ApexPages.currentPage().getParameters().get( 'pid' );
    }
  }

  public void fileUploaderCallback (FileUploaderController target){
    System.debug( target );
  }
    
  public void registerController (FileUploaderController target){
        System.debug( target );
  }
 
  public FileUploaderCaller self {get;set;}

  public VFC158_FileUploadController(ApexPages.StandardController stdController) {
    this.self = this;
  }

}