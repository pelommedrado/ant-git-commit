@isTest
public class createDealerGroupTest {
    
    public static TestMethod void testCriarGrupos(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User communityUser;
        Account dealer;
                
        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {
            
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            //insere conta do portal
            dealer = moc.criaAccountDealer();
            insert(dealer);
            
            //insere contato associado a conta do portal
            Contact contact = moc.criaContato();
            contact.AccountId = dealer.Id;
            insert(contact);
            
            //cria usuario do portal
            communityUser = moc.criaUser();
            communityUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            communityUser.ContactId = contact.Id;
            insert(communityUser);  
        }
    }

}