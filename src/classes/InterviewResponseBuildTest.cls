@isTest
public with sharing class InterviewResponseBuildTest {
	@TestSetup static void setup(){
		Account newAcc = new Account();
		newAcc.Name = 'accTeste';
		newAcc.Phone = '12345678';
		Database.insert(newAcc);

		Contact newContact = new Contact();
		newContact.LastName = 'testeContact';
		newContact.AccountId = newAcc.Id;
		Database.insert(newContact);

		Interview__c newInterview = new Interview__c();
		newInterview.Name = 'Pesquisa PV';
		Database.insert(newInterview);
	}
	
	@isTest static void shouldGetInterviewResponse() {
		Account acc 			= [SELECT Id FROM Account Limit 1];
		Contact contact 		= [SELECT Id FROM Contact Limit 1];
		Interview__c interview 	= [SELECT Id FROM Interview__c Limit 1];

		InterviewResponseBuild instance = InterviewResponseBuild.getInstance();
		InterviewResponse__c interviewRes = instance.newInterviewResp(acc.Id, contact.Id, interview.Id);
		Database.insert(interviewRes);

		InterviewResponse__c check = InterviewResponseDAO.getInterviewResById(interviewRes.Id);

		System.assertEquals('test', check.ReasonReturn__c);
		System.assertEquals('test', check.TestDriveOffered__c);
		System.assertEquals('test', check.CustomerExperience__c);
		System.assertEquals('test', check.DealerGetInTouchAfterService__c);
		System.assertEquals('test', check.DealerGetInTouchAfterService__c);
		System.assertEquals('test', check.VehicleDeliveriedDateCombined__c);
		System.assertEquals('test', check.CustomerReturnedRenaultRepairShop__c);
		System.assertEquals('www.test.com.br', check.AnswerLink__c);
		System.assertEquals('Facebook', check.ContactChannel__c);
		System.assertEquals('Facebook', check.Origin__c);
		System.assertEquals(System.Today(), check.AnswerDate__c);
		System.assertEquals(0, check.ReccomendDealerNote__c);
		

		System.assertEquals(acc.Id, check.AccountId__c);
		System.assertEquals(contact.Id, check.ContactId__c);
		System.assertEquals(interview.Id, check.InterviewId__c);


	}
}