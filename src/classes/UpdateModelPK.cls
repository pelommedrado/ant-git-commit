global class UpdateModelPK implements Database.Batchable<sObject> {

    global UpdateModelPK() {
        
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC){
        return Database.getQueryLocator([
            select Id, Model_Spec_Code__c, Phase__c
            from Model__c
        ] );
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.assertEquals( scope.size(), 1 );
        Model__c model = (Model__c)scope[0];

        model.Model_PK__c = model.Model_Spec_Code__c + '-' + model.Phase__c;

        Database.update( model );



        List< PV_ModelMilesime__c > milesimeList = [
            select Id, ENS__c, Milesime_PK__c 
            from PV_ModelMilesime__c 
            where Model__c = :model.Id
        ];
        //Map< Id, PV_ModelMilesime__c > milesimeMap = new Map< Id, PV_ModelMilesime__c >( milesimeList );

        for( PV_ModelMilesime__c milesime : milesimeList ) milesime.Milesime_PK__c = model.Model_PK__c + milesime.ENS__c;

        Database.update( milesimeList );


        List< PVVersion__c > versionList = [
            select Id, Version_Id_Spec_Code__c, Version_PK__c, Milesime__r.Milesime_PK__c
            from PVVersion__c
            where Milesime__c in :milesimeList
        ];
        //Map< Id, PVVersion__c > versionMap = new Map< Id, PVVersion__c >( versionList );

        for( PVVersion__c version : versionList ) version.Version_PK__c = version.Milesime__r.Milesime_PK__c + version.Version_Id_Spec_Code__c;

        Database.update( versionList );



        List< Optional__c > optionalList = [
            select Id, Optional_Code__c, Optional_PK__c, Version__r.Version_PK__c
            from Optional__c
            where Version__c in :versionList
        ];

        for( Optional__c optional : optionalList ) optional.Optional_PK__c = optional.Version__r.Version_PK__c + optional.Optional_Code__c;

        Database.update( optionalList );

        //Map< Id, Optional__c > optionalMap = new Map< Id, Optional__c >( optionalList );
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}