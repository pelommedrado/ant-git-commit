@isTest
public class TRG_MYR_MyrEvent_afterDeleteTest {

    @isTest static void myUnitTest(){
        
        MyrEvent__c event = new MyrEvent__c();
        event.Title__c = 'Event_1';
        event.Event_Type__c = 'News';
        event.Description_PT_Rich__c = 'Description of Event_1';
        event.Date_Start__c = system.today()+1;
        event.Date_End__c = system.today()+3;
        event.IsActive__c = true;
        event.Timeline_Publication__c = true;
        event.Seg_Type__c='For All Users';
        event.Description_PT_Rich__c = 'Description of Event ';
        event.Description_Summary_PT__c = 'Description of Event ';
        insert event;
        
        delete event;
    }
}