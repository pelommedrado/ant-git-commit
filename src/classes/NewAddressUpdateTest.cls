@isTest
private class NewAddressUpdateTest {
	
	private static testMethod void testOne(){

		test.startTest();

		MyOwnCreation moc = new MyOwnCreation();

		Account acc = moc.criaPersAccount();
		acc.ShippingPostalCode = '09820220';
		acc.BillingPostalCode = '09820220';
		insert acc;

		Contact cnt = moc.criaContato();
		cnt.MailingPostalCode = '09820220';
		cnt.OtherPostalCode = '09820220';
		insert cnt;

		Lead ld = moc.CriaLead();
		ld.PostalCode = '09820220';
		insert ld;

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('correiostest');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');
        
        Test.setMock(HttpCalloutMock.class, mock);

        Database.executeBatch(new NewAddressUpdate());

        test.stopTest();

	}
	
}