public with sharing class FileUploaderController {

  public Id attId{
    get;
    set{
      System.debug( '@@@ ATTACHMENT ID SETTER @@@ ' + this.attId );
      if( String.isNotBlank( value ) && this.attId != value ){
        this.attId = value;
        Attachment att = [select Id, ParentId, Body, Description, Name from Attachment where Id = :this.attId];
        this.parentId = att.ParentId;
        this.decodedImage = att.Body;
        this.description = att.Description;
        this.name = att.Name;
      } else this.attId = value;
    }
  }
  public Id parentId{get;set;}
  public String errorMsg{get;set;}
  public String step{get;set;}
  public String description{get;set;}
  public String name{get;set;}
  public String type{
    get;
    set{
      System.debug( '@@@ TYPE SETTER @@@ ' + value );
      this.type = value;
    }
  }
  public Boolean strict{get;set;}
  public FileUploaderCaller externalController{
    get;
    set{
      this.externalController = value;
      this.externalController.registerController( this );
    }
  }
  public Decimal megabyteLimit{get;set;}
  public String contentType{get;set;}
  public transient Blob decodedImage{get;set;}
  public String encodedImage{
    get{
      return '';
    }
    set{
      String encoded = value;
      if( String.isNotBlank( encoded ) )
        decodedImage = EncodingUtil.base64Decode( encoded.substringAfter( ',' ) );
    }
  }
  public String communityPath{get;set;}

  public String baseURL{
    get{
      return URL.getCurrentRequestUrl().toExternalForm().substringBefore( '/apex' );
    }
  }

  public String attURL{
    get{
      //return String.isNotBlank( attId ) ? URL.getSalesforceBaseUrl().toExternalForm() + (String.isNotBlank( communityPath ) ? communityPath : '') + '/servlet/servlet.FileDownload?file=' + attId : null;
      return String.isNotBlank( this.attId ) ? this.baseURL + '/servlet/servlet.FileDownload?file=' + attId : null;
    }
  }

  public String fileExtension{
    get{
      return String.isNotEmpty( name ) && name.contains( '.' ) ? name.substringAfterLast( '.' ) : '';
    }
  }

  public Boolean isImg{
    get{
      return new Set< String >{'jpg', 'jpeg', 'png', 'bmp'}.contains( fileExtension.toLowerCase() );
    }
  }
  public Boolean isPDF{
    get{
      return fileExtension.equalsIgnoreCase( 'pdf' );
    }
  }

  public String debug{
    get{
      return JSON.serializePretty( new Map< String, Object >{
        'attId' => attId,
        'parentId' => parentId,
        'type' => type
      } );
    }
  }

  public FileUploaderController () {
  }

  public void validate(){

    if( String.isEmpty( parentId ) ){
      errorMsg = 'É necessário especificar um registro pai.';
      return;
    }

    if(
      !parentId.getSobjectType().getDescribe().getName().endsWith( '__c' ) && 
      !(new Set< String >{'Account', 'Asset', 'Campaign', 'Case', 'Contact', 'Contract', 'EmailMessage',
      'EmailTemplate', 'Event', 'Lead', 'Opportunity', 'Product2', 'Solution', 'Task'}.contains( parentId.getSobjectType().getDescribe().getName() ))
    ){
      errorMsg = 'Tipo inválido para registro pai.';
      return;
    }

    String parentObjectName = parentId.getSobjectType().getDescribe().getName();
    List< SObject > parentList = Database.query( 'select Id from ' + parentObjectName + ' where Id = :parentId' );

    if( parentList.size() == 0 ){
      errorMsg = 'O registro pai não existe, foi excluído ou seu Id está incorreto.';
      return;
    }
	
    if( String.isNotEmpty( name ) ){
      
      if( name.contains( '\\' ))
        name = name.substringAfterLast( '\\' );
      else if( name.contains( '/' ) )
        name = name.substringAfterLast( '/' );
		
        system.debug('***** FileUploaderController.fileExtension: type '+type +' **** this.fileExtension '+this.fileExtension);
      if( String.isNotEmpty( type ) ){
        Set< String > allowedExtensions = new Set< String >( type.split( ';' ) );
        if( !allowedExtensions.contains( this.fileExtension ) ){
          if( this.strict && !name.contains( '.' ) ){
            errorMsg = 'Arquivos sem extensão não são permitidos.';
            return;
          }
          if( name.contains( '.' ) ){
            errorMsg = 'Formato inválido. Arquivos permitidos: ' + String.join( new List< String >( allowedExtensions ),  ' / ' );
            return;
          }
        }
      }
    }
      
    if(fileExtension.equalsIgnoreCase( 'pdf' )){
        contentType = 'application/pdf';
        
    } else {
        Set< String > extension = new Set< String >{'jpg', 'jpeg', 'png', 'bmp'};
        if(extension.contains( fileExtension.toLowerCase())) 
            contentType = 'image/'+fileExtension;
    }
      system.debug('#### contentType FILE: '+contentType);

    if( megabyteLimit != null && megabyteLimit > 0.0 ){
      Decimal byteLimit = megabyteLimit * 1024 * 1024; //megabyte -> byte
      if( Decimal.valueOf( decodedImage.size() ) > byteLimit ){
        errorMsg = 'Arquivo muito grande. Limite de ' + megabyteLimit + ' mB.';
        return;
      }
    }

    if( String.isEmpty( name ) ){
      SObject parent = parentList[0];
      Integer attachmentCount = [select COUNT() from Attachment where ParentId = :parentId];

      name = parentObjectName + ' - Attachment #' + String.valueOf( attachmentCount );
    }

    Attachment att = new Attachment( 
      ParentId = parentId, 
      Id = attId, 
      Body = decodedImage, 
      Description = description,
      Name = name,
      ContentType = contentType  
    );

    System.Savepoint sp = Database.setSavepoint();
    try{
      Database.upsert( att );
      attId = att.Id;
      errorMsg = null;
      if( externalController != null ) externalController.fileUploaderCallback( this );
    }catch( Exception e ){
      Database.rollback( sp );
      errorMsg = Utils.parseErrorMessage( e );    
    }
  }

  public void reset(){
    attId = null;
    errorMsg = null;
    description = null;
    name = null;
  }

  public void reset(Boolean resetPId){
    if( resetPId ) parentId = null;
    reset();
  }

}