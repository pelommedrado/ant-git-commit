@isTest(SeeAllData=true)

public class Rforce_MDM_WS_TEST implements HttpCalloutMock{
 
 private HttpResponse resp;

  public Rforce_MDM_WS_TEST(String testBody) {
       resp = new HttpResponse();
       resp.setBody(testBody);
       resp.setStatusCode(200);
  }
 
  public HTTPResponse respond(HTTPRequest req) {
        return resp;
  }
  
   static testMethod void RforceMDMWSTest() {
   
   String formattedtext='<ns2:SearchPartyResponse><ns2:PartyOverview><ns2:PartyId>30380033</ns2:PartyId><ns2:PartyType>I</ns2:PartyType><ns2:MatchScore>100</ns2:MatchScore><ns2:FirstName>BERNARD</ns2:FirstName><ns2:LastName>ROCHIER</ns2:LastName><ns2:Sex>M</ns2:Sex><ns2:Language>FRE</ns2:Language><ns2:AddressLine1>10 IMPASSE DES LAVANDIERES</ns2:AddressLine1><ns2:City>MEYSSE</ns2:City><ns2:Postcode>07400</ns2:Postcode><ns2:Country>FRA</ns2:Country><ns2:FixedPhoneNumber>+33 4 75 52 95 73</ns2:FixedPhoneNumber></ns2:PartyOverview><ns2:ResponseMessage>:: Search Party OK, number of records : 1</ns2:ResponseMessage></ns2:SearchPartyResponse><ns2:GetPartyResponse><ns2:Party><ns2:PartyType>I</ns2:PartyType><ns2:PartyStatus>A</ns2:PartyStatus><ns2:FirstName1>BERNARD</ns2:FirstName1><ns2:LastName1>ROCHIER</ns2:LastName1><ns2:FullName>BERNARD ROCHIER</ns2:FullName><ns2:YearOfBirth>1940</ns2:YearOfBirth><ns2:Civility>M</ns2:Civility><ns2:Sex>M</ns2:Sex><ns2:Language>FRE</ns2:Language><ns2:RenaultGroupStaff>N</ns2:RenaultGroupStaff><ns2:OccupationalCategoryCode>M1</ns2:OccupationalCategoryCode><ns2:OccupationalCategoryDescription>RETRAITE - SANS PROFESS.</ns2:OccupationalCategoryDescription><ns2:Address><ns2:AddressType>MAIN</ns2:AddressType><ns2:IrisCode>071570000</ns2:IrisCode><ns2:AddressLine1>10 IMPASSE DES LAVANDIERES</ns2:AddressLine1><ns2:City>MEYSSE</ns2:City><ns2:Postcode>07400</ns2:Postcode><ns2:Country>FRA</ns2:Country><ns2:StartDate>2007-01-04+01:00</ns2:StartDate></ns2:Address><ns2:PhoneNumber><ns2:PhoneNumberType>LAND</ns2:PhoneNumberType><ns2:PhoneNumberValue>+33 4 75 52 95 73</ns2:PhoneNumberValue><ns2:LastUpdateDate>2014-10-12+02:00</ns2:LastUpdateDate></ns2:PhoneNumber><ns2:PartyIdentification><ns2:IdentificationType>AAA</ns2:IdentificationType></ns2:PartyIdentification><ns2:PartyIdentification><ns2:IdentificationType>BCP</ns2:IdentificationType><ns2:IdentificationValue>18298155</ns2:IdentificationValue></ns2:PartyIdentification><ns2:PartyIdentification><ns2:IdentificationType>MDMID</ns2:IdentificationType><ns2:IdentificationValue>15516993</ns2:IdentificationValue></ns2:PartyIdentification><ns2:PartyIdentification><ns2:IdentificationType>MDMID</ns2:IdentificationType><ns2:IdentificationValue>30380033</ns2:IdentificationValue></ns2:PartyIdentification><ns2:Vehicle><ns2:VehicleType>VP</ns2:VehicleType><ns2:VehicleIdentificationNumber>VF1JMS50635714628</ns2:VehicleIdentificationNumber><ns2:Brand>RENAULT</ns2:Brand><ns2:BrandLabel>RENAULT</ns2:BrandLabel><ns2:Model>MJ22</ns2:Model><ns2:ModelLabel>SCENIC II</ns2:ModelLabel><ns2:MINEType>5516A879</ns2:MINEType><ns2:CartecId>66453</ns2:CartecId><ns2:RegistrationDate>2006-04-01+02:00</ns2:RegistrationDate><ns2:CurrentMileage>0</ns2:CurrentMileage><ns2:EndOfLife>N</ns2:EndOfLife><ns2:PreviousRegistrationNumber>2307XG26</ns2:PreviousRegistrationNumber><ns2:PartyVehicleType>OWN</ns2:PartyVehicleType><ns2:PurchaseDate>2007-01-04+01:00</ns2:PurchaseDate><ns2:SellingDealerR1>00000043</ns2:SellingDealerR1><ns2:AfterSalesDealerCode>00060901</ns2:AfterSalesDealerCode><ns2:RegistrationNumber>2307XG26</ns2:RegistrationNumber><ns2:ValidRegistrationNumber>Y</ns2:ValidRegistrationNumber><ns2:PurchaseMileage>0</ns2:PurchaseMileage><ns2:NewVehicle>N</ns2:NewVehicle><ns2:StartDate>2007-01-04+01:00</ns2:StartDate></ns2:Vehicle></ns2:Party><ns2:ResponseMessage>:: Get Party OK ::</ns2:ResponseMessage></ns2:GetPartyResponse>'; 
    
    
    HttpCalloutMock mock = new Rforce_MDM_WS_TEST(formattedtext);
    Test.setMock(HttpCalloutMock.class, mock);
    Rforce_MDM_WS mdmWS = new Rforce_MDM_WS();
    
    List<Rforce_MDMPartyOverview_CLS> mdmxmlDataList = new List<Rforce_MDMPartyOverview_CLS>();
    List<Rforce_MDMVehicle_CLS> mdmVehicleDataList = new List<Rforce_MDMVehicle_CLS>();
    Rforce_MDMPartyOverview_CLS mdmPartyOverviewClass = new Rforce_MDMPartyOverview_CLS();
    
    Rforce_MDMSearchParty_CLS mdmSearcyParty = new Rforce_MDMSearchParty_CLS('VF1JMS50635714628','','FRA','BERNARD','ROCHIER','','','','','','');
    Integer intReponse=mdmWS.getSearchPartyResponse(mdmSearcyParty);
    
    mdmVehicleDataList=mdmWS.getVehiclesDetails('30380033');
    mdmxmlDataList=mdmWS.getAllParty(mdmSearcyParty);
    mdmPartyOverviewClass=mdmWS.getVehicleBasedOnParty('30380033');
    String accoutID=mdmWS.createAccount('30380033');
       
   }
}