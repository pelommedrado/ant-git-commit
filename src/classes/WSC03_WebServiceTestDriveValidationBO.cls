public with sharing class WSC03_WebServiceTestDriveValidationBO
{
	// Validate accountId parameter
	public static String validateAccountId(String accountId)
	{
		/* Check Account Id was null */
        if (accountId == null || accountId == '') {
            return 'Account Id is a required field and can’t be empty.';
        }
        /*  check account Id does not exceed 18 characters */
        else if (accountId.length() > 18) {
            return 'Account Id is too long: maximum size 18 characters.';
        }
        /*  check account Id does not less than 15 characters */
        else if (accountId.length() < 15) {
        	return 'Account Id is too small: minimum size 15 characters.';
        }
        /*  check account Id does not equal to 16 and 17 characters */
        else if (accountId.length() == 16 || accountId.length() == 17) {
        	return 'Account Id is not match: minimum size 15 characters and maximum size 18 characters.';
        }
		return null;
	}


	// Verify if accountId exists in Salesforce
	public static String checkAccountExistsInSalesforce(String accountId)
	{
		Account acc = null;
		try {
			acc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(accountId);
		}
		catch (Exception e) {
			// ignore
		}

		if (acc == null)
			return 'Account Id not found.';
		else
			return null;
	}


	// Check vehicle model was null
	public static String checkVehicleModel(String vehicleModel)
	{
        if (vehicleModel == null || vehicleModel == '') {
            return 'Vehicle model is a required field and can’t be empty.';
        }

        /*  check vehicle model does not exceed 30 characters */
        if (vehicleModel.length() > 30) {
			return 'Vehicle model too long: maximum size 30 characters.';
		}

        List<VEH_Veh__c> vehicleRecords = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingModelName(vehicleModel);
        if (vehicleRecords.isEmpty()) {
            return 'Vehicle model not found.';
        }
        return null;
	}

	// Check date of Booking
	public static String checkDate(String dateOfBooking)
	{
        if (dateOfBooking == null || dateOfBooking == '') {
            return 'Date of booking is a required field and can’t be empty.';
        }
        else {
            String patternForDate = '(((19|20)\\d\\d)\\/(0?[1-9]|1[012])\\/(0?[1-9]|[12][0-9]|3[01]))';
            Pattern isDateFormat = Pattern.compile(patternForDate);
            Matcher dateMatcher = isDateFormat.matcher(dateOfBooking);

            if (!dateMatcher.Matches()) {
                return 'Date of booking is invalid. Use format: YYYY/MM/DD.';
            }
        }
        return null;
	}

	// Check date of Booking
	public static String checkDateTime(String dateOfBooking)
	{
        if (dateOfBooking == null || dateOfBooking == '') {
            return 'Date of booking is a required field and can’t be empty.';
        }
        else {
            String patternForDate = '(((19|20)\\d\\d)\\/(0?[1-9]|1[012])\\/(0?[1-9]|[12][0-9]|3[01]))\\s([01]?[0-9]|2?[0-3])\\:([012345]?[0-9])';
            Pattern isDateFormat = Pattern.compile(patternForDate);
            Matcher dateMatcher = isDateFormat.matcher(dateOfBooking);

            if (!dateMatcher.Matches()) {
                return 'Date of booking is invalid. Use format: YYYY/MM/DD HH:MI.';
            }
        }
        return null;
	}

	// Check start and end date of Booking
	public static String checkDatesOfBooking(String startDateOfBooking, String endDateOfBooking)
	{
		// For startDateOfBooking
        if (startDateOfBooking == null || startDateOfBooking == '') {
            return 'Start Date of booking is a required field and can’t be empty.';
        }
        else {
            String patternForDate = '((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])';
            Pattern isDateFormat = Pattern.compile(patternForDate);
            Matcher dateMatcher = isDateFormat.matcher(startDateOfBooking);
            
            if (!dateMatcher.Matches()) {
                return 'Start Date of booking is invalid. Use format: YYYY/MM/DD.';
            }
        }
        
        // For endDateOfBooking
        if (endDateOfBooking == null || endDateOfBooking == '') {
            return 'End Date of booking is a required field and can’t be empty.';
        }
        else {
            String patternForDate = '((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])';
            Pattern isDateFormat = Pattern.compile(patternForDate);
            Matcher dateMatcher = isDateFormat.matcher(endDateOfBooking);
            
            if (!dateMatcher.Matches()) {
                return 'End Date of booking is invalid. Use format: YYYY/MM/DD.';
            }
        }
        
        // Check if start is less than end 
        Date startBookingDate = VFC69_Utility.convertStringToDate(startDateOfBooking);
        Date endBookingDate = VFC69_Utility.convertStringToDate(endDateOfBooking);
        
        if (startBookingDate > endBookingDate) {
        	return 'start date of booking should be less tean end date of booking.';
        }
        return null;
	}


	// Check town is null
	public static String checkTown(String town)
	{
		if (town == null || town == '') {
        	return 'Address town is a required field and can’t be empty.';
        }
        else if (town.length() > 30){
			return 'Address town too long: maximum size 30 characters.';
        }
        return null;
	}
	
	
	// Check state code is null
	public static String checkStateCode(String town, String stateCode)
	{
        if (stateCode == null || stateCode == '') {
        	return 'Address state code is a required field and can’t be empty.';
        }
        else if (stateCode.length() > 2) {
            return 'Address state code too long: maximum size 2 characters.';
        }

        if (town != '' && stateCode != '')
        {
            List<Account> lstAccountRecords = VFC12_AccountDAO.getInstance().fetchAccountsUsingCityAndState(town, stateCode);

            //  Check lstAccountRecords list was empty
            if  (lstAccountRecords.isEmpty()) {
                return 'No dealers available for this address.';
            }
        }
        return null;
	}

	// Check first Name is null
	public static String checkFirstName(String firstName)
	{
        if (firstName == null || firstName == '') {
            return 'Customer First Name is a required field and can’t be empty.';
        }
        else if (firstName.length() > 40) {
            return 'Customer First Name too long: maximum size 40 characters.';
        }
        return null;
	}
	
	// Check Last Name is null
	public static String checkLastName(String lastName)
	{
        if (lastName == null || lastName == '') {
            return 'Customer Last Name is a required field and can’t be empty.';
        }
        else if (lastName.length() > 80) {
			return 'Customer Last Name too long: maximum size 80 characters.';
        }
        return null;
	}
	
	// Check Email is null
	public static String checkEmail(String email)
	{
        if (email == null || email == '') {
            return 'Customer Email is a required field and can’t be empty.';
        }
        else if (email.length() > 80){
			return 'Customer Email is too long: maximum size 80 characters.';
        }
        else
        {
            // Check email pattern should matches the standard email structure.
            String patternForEmail = '[a-z0-9!#$%&'+'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'+'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?';
            Pattern isEmailFormat = Pattern.compile(patternForEmail);
            Matcher emailMatcher = isEmailFormat.matcher(email);
            if (!emailMatcher.Matches()) {
                return 'Customer Email is invalid: o proper email format is required.';
            }
        }
        return null;
	}

	// Check PostalCode is null
	public static String checkPostalCode(String postalCode)
	{
        if (postalCode == null || postalCode == '') {
            return 'Customer Postal Code is a required field and can’t be empty.';
        }
        else if (postalCode.length() > 9) {
            return 'Customer Postal Code is too long: maximum size 9 characters.';
        }
        else
        {
            // Check Number field pattern should matche only numbers
            Pattern isNumbers = Pattern.compile('^[0-9]+$');
            Matcher postalCodeMatcher = isNumbers.matcher(postalCode);
            if (!postalCodeMatcher.Matches()) {
                return 'Customer Postal Code is invalid: use only numbers.';
            }
        }
     	return null;
	}

	// Check if cpf is null or blank
	public static String checkCPF(String cpf)
	{
        if (cpf == null || cpf == '') {
            return 'Customer CPF is a required field and can’t be empty.';
        }
        else if (cpf.length() > 11)
        {
                return 'Customer CPF is too long: maximum size 11 characters.';
        }
        else
        {
            // Check cpf pattern should matches the standard email structure.
            String patternForCPF = '\\d{11}';
            Pattern isCPFFormat = Pattern.compile(patternForCPF);
            Matcher cpfMatcher = isCPFFormat.matcher(cpf);
            if (!cpfMatcher.Matches()) {
                return 'Customer CPF is invalid: use only numbers without . or -.';
            }
        }
		return null;
	}

	// Check if phone is null or blank
	public static String checkPhone(String phone)
	{
        if (phone == null || phone == '') {
            return 'Customer phone is a required field and can’t be empty.';
        }
        else
        {
            String numberFormat = '\\d{6,11}';
            Pattern isCustomerPhone = Pattern.compile(numberFormat);
            Matcher customerPhoneMatcher = isCustomerPhone.matcher(phone);
            if (!customerPhoneMatcher.Matches()) {
                return 'Customer phone is invalid. Use only numbers.';
            }
        }
		return null;
	}

	// Check if the date of booking didn't passed
	public static String checkDateOfBookingDidntPassed(Datetime dateTimeForBooking)
	{
        if (dateTimeForBooking <= Datetime.now()) {
        	return 'The date of booking can not be less or equals to now.';
        }
        return null;
	}

	// Check if the spot is available
	public static String checkIfSpotIsAvailable(List<Integer> lstWorkingHours, String vehicleModel, 
		Datetime dateTimeForBooking, String accountId, List<String> lstTimeFormated)
	{
        List<TDV_TestDrive__c> lstTestDriveRecords = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsing_DateAndTime(accountId, vehicleModel, dateTimeForBooking);
		
        String currentBookingTime = lstTimeFormated[0] + lstTimeFormated[1];
		
        if (Integer.valueOf(currentBookingTime) < lstWorkingHours[0] || Integer.valueOf(currentBookingTime) > lstWorkingHours[1]) {
            return 'Period not available for reservations.';
        }
		
        if (!lstTestDriveRecords.isEmpty()) {
            return 'Period already reserved.';
        }
        return null;
	}
}