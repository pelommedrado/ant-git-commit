global class PassingPurchaseOntemSched implements Schedulable {
    global void execute(SchedulableContext sc) {
        System.debug('Iniciando a execucao...');
        PassingPurchaseServico pServ = new PassingPurchaseServico('Ontem');
        pServ.executar();
    }
}