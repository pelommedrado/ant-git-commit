public class LMT_OrigensAccount {
    
    public static void convertLeadWithAccount(List<Account> accountList) {
        System.debug('convertLeadWithAccount()');
        
        //get Record Default Country of User
        User u = [
            SELECT Id,RecordDefaultCountry__c 
            FROM User WHERE Id =: UserInfo.getUserId()
        ];
        
        if(u.RecordDefaultCountry__c == null || !u.RecordDefaultCountry__c.equalsIgnoreCase('Brazil')) {
            return;
        }
        
        //recupera o tipo de registro de conta PF e PJ
        Id accountPF = Utils.getRecordTypeId('Account','Personal_Acc');
        Id accountPJ = Utils.getRecordTypeId('Account','Company_Acc');
        
        //recupera Id do proprietário de webToLead e emailToLead para não converte-los 
        //(A conversão destes é pela classe VFC131)
        Id webToLeadOwner  	= [SELECT Id FROM User WHERE Name = 'Renault do Brasil'].Id;
        Id emailToLeadOwner = [SELECT Id FROM User WHERE Name = 'Renault do Brasil Rede'].Id;
        
        //Lista de contas PF, PJ que não são criadas por webToOpportunity, 
		//emailServices e que não tenham um lead associado já convertido.*/
        List<Account> lsAccount = new List<Account>();
        
        if(!usuarioContemConjPermisssaoLeadsMontadora()){
            for(Account conta: accountList) {
                if( (conta.RecordTypeId.equals(accountPF) || conta.RecordTypeId.equals(accountPJ) ) &&
                   !conta.OwnerId.equals(webToLeadOwner) && !conta.OwnerId.equals(emailToLeadOwner) && 
                   conta.LeadConverted__c == false){
                       lsAccount.add(conta);
                   }
            }
        }
        
        //set para obter todos os CPFs da trigger
        Set<String> setCPF = new Set<String>();
        if(!lsAccount.isEmpty())
            setCPF = ObterCPF(lsAccount);
        
        //map para obter todos os leads mais antigos com base nos CPF's
        map<String,Id> mapLeads = new map<String,Id>();
        if(!setCPF.isEmpty())
            mapLeads = ObterLeadsExistentes(setCPF);
        
        //Lead a converter
        Lead lead = new Lead();
        
        //Mapeamento da conta com origens e conversão de lead
        for(account conta: lsAccount) {
            
            Id leadId;
            if(!mapLeads.isEmpty())
                leadId = mapLeads.get(conta.CustomerIdentificationNbr__c);
            
            if(leadId != null) {
                lead = VFC09_LeadDAO.getInstance().findLeadBYLeadId(leadId);
                
                //Preenche as origens da conta com o Lead mais antigo na base.
                if(trigger.isBefore) {
                    mappingAccountFROMLead(conta,lead);
                }
                
                //Converte o Lead.
                if(trigger.isAfter){
                    System.Debug('$$$ IsConverted: ' + lead.IsConverted);
                    System.Debug('$$$ SFAConverted__c: ' + lead.SFAConverted__c);
                    
                    if(lead.IsConverted == false && lead.SFAConverted__c == false) {
                        convertLead(conta.Id, lead);
                    }
                    
                }
            }
        }
        
    }
    
    public static void mappingAccountFROMLead(Account a, Lead l){
        a.Source__c = l.LeadSource;
        a.AccSubSource__c = l.SubSource__c;
        a.Detail__c = l.Detail__c;
        a.Sub_Detail__c = l.Sub_Detail__c;
        if(l.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))){
            a.Cliente_DVE__c = true;
            if(l.Type_DVE__c != null && !l.Type_DVE__c.equals(''))
                a.Type_DVE__c = l.Type_DVE__c;
        }
    }
    
    public static void convertLead(Id accountId, Lead l) {
        System.debug('convertLead() ');
        
        System.debug('AccountId: ' + accountId);
        System.debug('Conversao de Lead: ' + l);
        
        final Id leadVendas = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
        final Id leadServic = Utils.getRecordTypeId('Lead','Service');
        
        final Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity( true );
        lc.setLeadId(l.Id);
        
        if(l.RecordTypeId.equals(leadVendas)) {
            lc.setConvertedStatus('Qualified');
            
        } else if(l.RecordTypeId.equals(leadServic)) {
            lc.setConvertedStatus('Performed');
            
        } else {
            lc.setConvertedStatus('Hot Lead');
            
        }
        
        // if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (l.Owner.type == 'Queue') {
            lc.setOwnerId( Userinfo.getUserId() );
        }
        
        Database.Leadconvertresult successLeadConversion = Database.convertLead(lc);
        System.debug('Success Lead Conversion : ' + successLeadConversion);
        System.debug('Account Id gerado ' + successLeadConversion.getAccountId());
    }
    
    public static Map<String, Id> obterLeadsExistentes(Set<String> CPF) {
        System.debug('obterLeadsExistentes()');
        
        final Id serviceLead = Utils.getRecordTypeId('Lead', 'Service');
        final Id voucherLead = Utils.getRecordTypeId('Lead', 'Voucher');
        
        List<Lead> lsLead = [
            SELECT Id, CPF_CNPJ__c FROM lead 
            WHERE CPF_CNPJ__c in :CPF AND RecordTypeId !=: serviceLead 
            AND RecordTypeId !=: voucherLead 
            ORDER BY CreatedDate DESC
        ];
        
        Map<String, Id> mapCpfLead = new Map<String, Id>();
        for(Lead l : lsLead) {
            mapCpfLead.put(l.CPF_CNPJ__c, l.Id);
        }
        return mapCpfLead;
    }
    
    public static Set<String> ObterCPF(List<Account> lsConta){
        Set<String> CPF = new Set<String>();
        for(Account c : lsConta) {
            if(c.CustomerIdentificationNbr__c != null)
                CPF.add(c.CustomerIdentificationNbr__c);
        }
        return CPF;
    }
    
    public static Boolean usuarioContemConjPermisssaoLeadsMontadora() {
        System.debug('usuarioContemConjPermisssaoLeadsMontadora()');
        List<PermissionSetAssignment> atribuidosList = [
            SELECT Id FROM PermissionSetAssignment 
            WHERE AssigneeId =: UserInfo.getUserId() AND 
            PermissionSetId IN (
                SELECT Id FROM PermissionSet WHERE Name = 'BR_Leads_Montadora'
            ) limit 1
        ];
        // Para usuario admin - classe de teste
        if(UserInfo.getProfileId().equalsIgnoreCase(Utils.getSystemAdminProfileId())) {
            return true;
        }   
        return atribuidosList.size() > 0;
    }
}