/*****************************************************************************************
    Name            : Rforce_CommercialOffers_WS
    Description     : Generate the request with required Parameters for the webservice and return the request to the calling function..
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Imnplemented By : Ashok Muntha

    Description     : Commercial Offer V3 changes as a part of 16.1 release
    Release Number  : 16.1
    Modified Date   : 08/12/2015
    Modified By     : Venkatesh Kumar
 
******************************************************************************************/

global class Rforce_CommercialOffers_WS {
    static HttpRequest initialize(String input) { 
        system.debug('CLASS:RFORCE_CommercialOffers_WS-->METHOD:init()');
        HttpRequest request = new HttpRequest();
   
        try{
          request.setMethod(Label.ACC_SETMETHOD);
          if(input.equalsIgnoreCase('pull')){
            request.setEndpoint(Label.ACC_CommercialPullEndPoint);
          }
          else{
            request.setEndpoint(Label.ACC_CommercialPushEndPoint);
          }
          request.setClientCertificateName(System.label.clientcertnamex);
          request.setTimeout(Integer.ValueOf(Label.ACC_TIMEOUT)); 
         // request.setHeader(Label.ACC_ContentType, 'text/xml; charset=utf-8');
          request.setHeader(Label.ACC_ContentType, 'application/soap+xml; charset=utf-8');
          
        }catch(Exception ex) {
              system.debug('Rforce_CommercialOffers_WS-->METHOD:init Exception : :'+ ex.getMessage());
        }  
        return request;
}

/**
 * @author Rajavel
 * @date 08/10/2015
 * @description This method is used to push the offer related values to adobe from the Send to Adobe button using Java Script.
 * @Param httpRequest to formulate the HTTP request
 * @Param offerByEmailOrVin information on the type of Offer by VIN or Email.
 * @Param proposeRequest intializes the values from Rforce_CommercialOffersPropose_CLS.
 * @return httpRequest
 */
 
public HttpRequest addRequestDetails(HttpRequest httpRequest, String offerByEmailOrVin, Rforce_CommercialOffersPropose_CLS proposeRequest) { 
    system.debug('CLASS:RFORCE_CommercialOffers_WS-->METHOD:init()');
    String requestStr;
    String strOfferByEmail = 'getEmailOffers';
    String strOfferByVin = 'getVinOffers';
      
   
    try{
        if(offerByEmailOrVin.equalsIgnoreCase(strOfferByEmail)){
            system.debug('Inside Email'+proposeRequest.strEmailAndCountry);
            httpRequest.setHeader('SOAPAction', 'ren:heliosProposeEmail#Propose');
            requestStr = ''                
                 +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:ren:heliosProposeEmail">'
                 +'<soapenv:Header/>'
                 +'<soapenv:Body>'
                 +'<urn:Propose>'
                 +'<urn:sessiontoken>'+Label.Rforce_Comm_Credentials+'</urn:sessiontoken>'
                 +'<urn:targetId>'+proposeRequest.strEmailAndCountry+'</urn:targetId>'                 
                 +'<urn:maxCount>10</urn:maxCount>'
                 +'<urn:uuid></urn:uuid>'
                 +'<urn:nlid></urn:nlid>'
                 +'<urn:noProp></urn:noProp>'
                 +'<urn:categories></urn:categories>'
                 +'<urn:themes></urn:themes>'
                 +'<urn:context>'
                 +'</urn:context>'
                 +'</urn:Propose>'
                 +'</soapenv:Body>'
                 +'</soapenv:Envelope>';
                  system.debug('requestStr strOfferByEmail..::'+ requestStr);
           }else if(offerByEmailOrVin.equalsIgnoreCase(strOfferByVin)){
               system.debug('Inside Vin'+proposeRequest.strVinAndCountry);     
                 httpRequest.setHeader('SOAPAction', 'ren:heliosProposeVIN#Propose'); 
             requestStr = '' 
                 +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:ren:heliosProposeVIN">'
                 +'<soapenv:Header/>'
                 +'<soapenv:Body>'
                 +'<urn:Propose>'
                 +'<urn:sessiontoken>'+Label.Rforce_Comm_Credentials+'</urn:sessiontoken>'
                 +'<urn:targetId>'+proposeRequest.strVinAndCountry+'</urn:targetId>'
                 +'<urn:maxCount>10</urn:maxCount>'
                 +'<urn:uuid></urn:uuid>'
                 +'<urn:nlid></urn:nlid>'
                 +'<urn:noProp></urn:noProp>'
                 +'<urn:categories></urn:categories>'
                 +'<urn:themes></urn:themes>'
                 +'<urn:context>'
                 +'</urn:context>'
                 +'</urn:Propose>'
                 +'</soapenv:Body>'
                 +'</soapenv:Envelope>'; 
                 system.debug('requestStr strOfferByVin..::'+ requestStr);  
           }            
          
        httpRequest.setHeader('Content-Length', String.valueOf(requestStr.length()));
        httpRequest.setBody(requestStr);
          
        }catch(Exception ex) {
              system.debug('Rforce_CommercialOffers_WS-->METHOD:init Exception : :'+ ex.getMessage());
        } 
     return httpRequest;
   }
   
static String callOfferService(HttpRequest httpRequest) { 
    String strOfferResponse='';
    try {
        Http http = new Http();
        HTTPResponse response = http.send(httpRequest);
        strOfferResponse = response.getBody();
        system.debug('@@@@ strOfferResponse is..::'+ strOfferResponse);
    }catch(CalloutException ex) {
        system.debug ('Rforce_CommercialOffers_WS : getResponse, CALLOUT EXCEPTION'+ex.getMessage());
    }catch (Exception e) {
        system.debug ('Rforce_CommercialOffers_WS : getResponse, UNKNOWN EXCEPTION'+e.getMessage());
    }
  
       return strOfferResponse;
   }
   
/**
 * @author Venkatesh Kumar
 * @date 08/12/2015
 * @description Added the extra field values that needs to be transfered to Adobe as a part of Commercial Offer V3 changes.
 * @Param httpRequest to formulate the HTTP request
 * @Param strEmailId Email Id information is received from Push Offer class.
 * @Param strImageUrl Image Url is received from Push Offer class.
 * @Param strEventType Event Type Informations is received from Push Offer class.
 * @Param OfferDetails Offer details the values is received from Push Offer class..
 * @return httpRequest needs to be passed as a response
 */

static HttpRequest pushOfferRequest(HttpRequest httpRequest, String strEmailId, String strImageUrl, String strEventType, Rforce_CommercialOfferDetails_CLS list_AdobeDetails, String strExternalId) { 
   
  system.debug('### Rforce_CommercialOffers_WS : pushOfferRequest()');
  system.debug('### Rforce_CommercialOffers_WS : strEmailId -> ' + strEmailId);
  system.debug('### Rforce_CommercialOffers_WS : strImageUrl -> ' + strImageUrl);
  system.debug('### Rforce_CommercialOffers_WS : strEventType -> ' + strEventType);     
  system.debug('### Rforce_CommercialOffers_WS : list_AdobeDetails -> ' + list_AdobeDetails);
  system.debug('### Rforce_CommercialOffers_WS : strExternalId -> ' + strExternalId);
  
    String strRequest;         
    String acctFirstName;
    String acctLastName;
    String ComofferTitle;    
    String ComofferSalutation;   
    String acctCompanyID;   
    String acctCustomerIdentificationNumber;   
    String acctPhonenumber;    
    String venhVIN;    
    String venhVehicleModel;
    String venhYearModel;
    String ComOfferMarketingDesc;
    String CommOfferName;
    String strEligibility;
    String strAccountID;
    String strOfferLabel;
    Date CommofferValidityDate;
    String strLegalConditions;
    String requestStr;
    String strOffercode;

    acctFirstName = list_AdobeDetails.strAccFirstName;
    acctLastName = list_AdobeDetails.strAccLastName;
    ComofferTitle = list_AdobeDetails.strAccOfferTitle;
    ComofferSalutation = list_AdobeDetails.strAccOfferSalutation;
    acctCompanyID = list_AdobeDetails.strAcctCompanyID;
    acctCustomerIdentificationNumber = list_AdobeDetails.strAcctCustomerIdentificationNumber;
    acctPhonenumber = list_AdobeDetails.strAcctPhonenumber;
    venhVIN = list_AdobeDetails.strVenhVIN;
    venhVehicleModel = list_AdobeDetails.strVenhVehicleModel;
    venhYearModel = list_AdobeDetails.strVenhYearModel;
    ComOfferMarketingDesc = list_AdobeDetails.strComofferMarketingDesc;
    CommOfferName = list_AdobeDetails.strCommofferName;
    CommofferValidityDate = list_AdobeDetails.dteCommofferValidityDate;
    strEligibility = list_AdobeDetails.strEligibility;
    strLegalConditions = list_AdobeDetails.strLegalConditions;
    strAccountID = list_AdobeDetails.strAccountID;
    strOfferLabel = list_AdobeDetails.strOfferLabel;
    strOffercode = list_AdobeDetails.strOffercode;

    
    system.debug('### Rforce_CommercialOffers_WS : acctFirstName -> ' + acctFirstName);
    system.debug('### Rforce_CommercialOffers_WS : acctLastName -> ' + acctLastName);
    system.debug('### Rforce_CommercialOffers_WS : ComofferTitle -> ' + ComofferTitle);
    system.debug('### Rforce_CommercialOffers_WS : ComofferSalutation -> ' + ComofferSalutation);
    system.debug('### Rforce_CommercialOffers_WS : acctCompanyID -> ' + acctCompanyID);
    system.debug('### Rforce_CommercialOffers_WS : acctCustomerIdentificationNumber -> ' + acctCustomerIdentificationNumber);
    system.debug('### Rforce_CommercialOffers_WS : acctPhonenumber -> ' + acctPhonenumber);
    system.debug('### Rforce_CommercialOffers_WS : venhVIN -> ' + venhVIN);
    system.debug('### Rforce_CommercialOffers_WS : venhYearModel -> ' + venhYearModel);
    system.debug('### Rforce_CommercialOffers_WS : venhVehicleModel -> ' + venhVehicleModel);
    system.debug('### Rforce_CommercialOffers_WS : ComOfferMarketingDesc -> ' + ComOfferMarketingDesc);
    system.debug('### Rforce_CommercialOffers_WS : CommOfferName -> ' + CommOfferName);
    system.debug('### Rforce_CommercialOffers_WS : CommofferValidityDate -> ' + CommofferValidityDate);
    system.debug('### Rforce_CommercialOffers_WS : strEligibility -> ' + strEligibility);
    system.debug('### Rforce_CommercialOffers_WS : strOfferLabel -> ' + strOfferLabel);
      
      try{
         //system.debug('Inside Email'+proposeRequest.strEmailAndCountry);
         httpRequest.setHeader('SOAPAction', 'nms:rtEvent#PushEvent');
         requestStr = ''
                 
            +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:nms:rtEvent">'
            +'<soapenv:Body>'
            +'<urn:PushEvent>'
            +'<urn:sessiontoken>'+Label.Rforce_CommMessage_Credentials+'</urn:sessiontoken>'
            +'<urn:domEvent>'
            +'<rtEvent type="'+strEventType+'" email="'+strEmailId+'" origin="" wishedChannel="0" externalId="'+strExternalId+'" language="" country="" emailFormat="2" mobilePhone="">'
               +'<ctx>'
               +'<Firstname>'+acctFirstName+'</Firstname>'
               +'<Lastname>'+acctLastName+'</Lastname>'
               +'<Title>'+ComofferTitle+'</Title>'
               +'<Salutation>'+ComofferSalutation+'</Salutation>'
               +'<CompanyID>'+acctCompanyID+'</CompanyID>'
               +'<AccountID>'+strAccountID+'</AccountID>'
               +'<CustomerIdentificationNumber>'+acctCustomerIdentificationNumber+'</CustomerIdentificationNumber>'
               +'<Phonenumber>'+acctPhonenumber+'</Phonenumber>'
               +'<VIN>'+venhVIN+'</VIN>'
               +'<VehicleModel>'+venhVehicleModel+'</VehicleModel>'
               +'<YearModel>'+venhYearModel+'</YearModel>'
               +'<LabelOffer>'+CommOfferName+'</LabelOffer>'
               +'<ValidityDate>'+CommofferValidityDate+'</ValidityDate>'
               +'<MarketingDesc>'+ComOfferMarketingDesc+'</MarketingDesc>'
               +'<LegalConditions>'+strLegalConditions+'</LegalConditions>'
               +'<Eligibility>'+strEligibility+'</Eligibility>'                           
               +'<imageUrl>'+strImageUrl+'</imageUrl>'
               +'<Offerlabel>'+strOfferLabel+'</Offerlabel>'
               +'<Offercode>'+strOffercode+'</Offercode>'
               +'</ctx>'
            +'</rtEvent>'
            +'</urn:domEvent>'
            +'</urn:PushEvent>'
            +'</soapenv:Body>'
            +'</soapenv:Envelope>';
            system.debug('requestStr strOfferByEmail..::'+ requestStr);
            
           httpRequest.setHeader('Content-Length', String.valueOf(requestStr.length()));
           httpRequest.setBody(requestStr);
          
        }catch(Exception ex) {
              system.debug('Rforce_CommercialOffers_WS-->METHOD:pushOfferRequest : :'+ ex.getMessage());
        } 
        
     return httpRequest;
}
   

 /*
  @description: Return the response based on the Method parameter.
 */   

public String getCommercialOffersBasedOnEmailOrVin(String offerByEmailOrVin, Rforce_CommercialOffersPropose_CLS proposeRequest){
 system.debug('CLASS:RFORCE_CommercialOffers_WS-->METHOD:getResponse()');
 HttpRequest httpRequest=initialize('pull');
 HttpRequest hRequest = addRequestDetails(httpRequest,offerByEmailOrVin, proposeRequest);
 return callOfferService(hRequest);
  }

webservice static String pushOffer(String strEmailId, String strImageUrl,String strEventType,Rforce_CommercialOfferDetails_CLS OfferDetails, String strExternalId){
     
 system.debug('CLASS:RFORCE_CommercialOffers_WS-->METHOD:pushOffer()');
 system.debug('### Rforce_CommercialOffers_WS : strEmailId -> ' + strEmailId);
 system.debug('### Rforce_CommercialOffers_WS : strImageUrl -> ' + strImageUrl);
 system.debug('### Rforce_CommercialOffers_WS : strExternalId -> ' + strExternalId );
 
 HttpRequest httpRequest=initialize('push');
 HttpRequest hRequest = pushOfferRequest(httpRequest, strEmailId, strImageUrl,strEventType,OfferDetails,strExternalId);
 return callOfferService(hRequest);
  
  }
}