/**
* Classe responsável por fazer o papel de business delegate da página de orquestração de oportunidade.
* @author Felipe Jesus Silva.
*/
public class VFC51_OppOrchestrationBusinessDelegate {
	private static final VFC51_OppOrchestrationBusinessDelegate instance = 
        new VFC51_OppOrchestrationBusinessDelegate();
	
	/**
	 * Construtor privado para impedir a criação de instancias dessa classe.
	 */
	private VFC51_OppOrchestrationBusinessDelegate() {
	}
	
	/**
     * Método responsável por prover a instância dessa classe.
     */  
    public static VFC51_OppOrchestrationBusinessDelegate getInstance() {
        return instance;
    }
	
	public VFC53_OpportunityDestinationType getOpportunityDestination(String opportunityId) {
		Opportunity sObjOpportunity = null;
		VFC53_OpportunityDestinationType opportunityDestination = null;
		
		sObjOpportunity = VFC47_OpportunityBO.getInstance().findById(opportunityId);
			
        if(sObjOpportunity.StageName.equals('Identified') ||
          sObjOpportunity.StageName.equals('In Attendance')) {
            opportunityDestination = VFC53_OpportunityDestinationType.QUALIFYING_ACCOUNT;
            
        } else if(sObjOpportunity.StageName.equals('Test Drive')) {
            opportunityDestination = VFC53_OpportunityDestinationType.TEST_DRIVE;
            
        } else if((sObjOpportunity.StageName.equals('Quote'))  || 
                  (sObjOpportunity.StageName.equals('Lost'))   || 
                  (sObjOpportunity.StageName.equals('Billed')) ||
                  (sObjOpportunity.StageName.equals('Order'))) {
                      opportunityDestination = VFC53_OpportunityDestinationType.QUOTE;
                  }
				
		return opportunityDestination;  
	}
	
}