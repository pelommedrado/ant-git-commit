@isTest
public class Account_TriggerTest {
    
    public static testMethod void myTest(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Case caso = moc.criaCaso();
        Insert caso;
        
        caso = [select CaseNumber from Case where id =: caso.Id];
        
        Account persAcc = moc.criaPersAccount();
        persAcc.CaseNumber__c = caso.CaseNumber;
        Insert persAcc;
        
    }
    
    public static testMethod void myTest2(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        CS04_MYR_Settings__c s = new CS04_MYR_Settings__c();
        s.Myr_AC_Activated__c = true;
        s.Myr_Personal_Account_Record_Type__c = 'test';
        s.Myr_Profile_Helios_ReadOnly__c = 'test';
        insert s;
        
        Account acc = moc.criaAccountDealer();
        insert acc;
        
    }
    
}