global class SCC_TD_Scan_WS {
	
	// ---------------------------------------------
    // scanPages() method
    // ---------------------------------------------        	
	webservice static string scanPages() {
		map<string, string> mapInfos = new map<string, string>();
		
		// "System Overview" page
		PageReference pRef1 = new PageReference('/setup/systemOverview.apexp');
		pRef1.setRedirect(true);
		
		// "Storage Usage" page
		PageReference pRef2 = new PageReference('/setup/org/orgstorageusage.jsp?id=' + UserInfo.getOrganizationId());
		pRef2.setRedirect(true);
        
		string sContent1 = null;
		string sContent2 = null;
        
		if (!system.Test.isRunningTest()) {
			sContent1 = pRef1.getContent().toString();
			sContent2 = pRef2.getContent().toString();
        }
		else {
			sContent1 = '<div class="num" id="usage_block_schema_num_3"><span><a>48.5&nbsp;GB</a></span>';
			sContent1 += '<div class="num" id="usage_block_api_num_1"><span>345,497</span>';
			sContent1 += '<div class="num" id="usage_block_apex_num_4"><span>20.5%</span>';
			
			sContent2 = '<th scope="row" class=" dataCell  ">Data Storage</th><td class=" dataCell  numericalColumn">41%</td></tr>';
			sContent2 += '<th scope="row" class=" dataCell  ">File Storage</th><td class=" dataCell  numericalColumn">7%</td></tr>';
			sContent2 += '<span  class="001">Accounts</span></th><td class=" dataCell  numericalColumn">2,357,789</td>';
		}

		list<cElement> lElements = new list<cElement>();
		lElements.add(new cElement(1, 'STORAGE_USAGE', '<div class="num" id="usage_block_schema_num_3">', '</a>'));
		lElements.add(new cElement(1, 'API_USAGE', '<div class="num" id="usage_block_api_num_1">', '</span>'));
		lElements.add(new cElement(1, 'CODE_USED', '<div class="num" id="usage_block_apex_num_4">', '</span>'));
		
		lElements.add(new cElement(2, 'DATA_STORAGE_USED', '<th scope="row" class=" dataCell  ">Data Storage</th>', '</td></tr>'));
		lElements.add(new cElement(2, 'FILE_STORAGE_USED', '<th scope="row" class=" dataCell  ">File Storage</th>', '</td></tr>'));
		lElements.add(new cElement(2, 'NB_ACCOUNTS', '<span  class="001">', '</td>'));
		lElements.add(new cElement(2, 'NB_CONTACTS', '<span  class="003">', '</td>'));
		lElements.add(new cElement(2, 'NB_CASES', '<span  class="500">', '</td>'));
		lElements.add(new cElement(2, 'NB_LEADS', '<span  class="00Q">', '</td>'));		
		lElements.add(new cElement(2, 'NB_OPPS', '<span  class="006">', '</td>'));
		lElements.add(new cElement(2, 'NB_TASKS', '<span  class="00T">', '</td>'));

		for (cElement oElement:lElements) {
			system.debug('### SCC_TD_Scan_WS - scanPages() - oElement.sPropertyName: ' + oElement.sPropertyName);
			
			string sContent = (oElement.iType == 1)?sContent1:sContent2;
			integer i = 0;
			integer j = 0;
			string sValue;
			
			i = sContent.indexOf(oElement.sStart);
			if (i > -1) {
				j = sContent.indexOf(oElement.sStop, i);
				
				if (j > -1) {
					string sSubString = sContent.subString(i, j);
					sSubString = sSubString.replace(oElement.sStart, '');
					
					i = sSubString.lastIndexOf('>');
					
					system.debug('### SCC_TD_Scan_WS - scanPages() - sSubString: ' + sSubString);
					//sValue = sSubString.subString(i + 1).replace('&nbsp;', '').replaceAll('[^\\w]', '');
					sValue = sSubString.subString(i + 1).replaceAll('&nbsp;', '').replaceAll(' ', '').replaceAll(',', '');
					
					system.debug('### SCC_TD_Scan_WS - scanPages() - sValue: ' + sValue);
        
					// All storage values are recovered in TB
					// All percentage values are recovered in %
					if (sValue.endsWith('KB')) {
						sValue = sValue.substring(0, sValue.length() - 2);
						sValue = Decimal.valueOf(sValue).divide(1000000000, 9).toPlainString();
					}
					else if (sValue.endsWith('MB')) {
						sValue = sValue.substring(0, sValue.length() - 2);
						sValue = Decimal.valueOf(sValue).divide(1000000, 9).toPlainString();
					}
					else if (sValue.endsWith('GB')) {
						sValue = sValue.substring(0, sValue.length() - 2);
						sValue = Decimal.valueOf(sValue).divide(1000, 9).toPlainString();
					}
					else if (sValue.endsWith('TB')) {
						sValue = sValue.substring(0, sValue.length() - 2);
						sValue = Decimal.valueOf(sValue).toPlainString();
					}					
					else if (sValue.endsWith('%')) {
						sValue = sValue.substring(0, sValue.length() - 1);
						sValue = String.valueOf(Decimal.valueOf(sValue));
					}

					mapInfos.put(oElement.sPropertyName, sValue);
					
					system.debug('### SCC_TD_Scan_WS - scanPages() - oElement.sPropertyName: ' + oElement.sPropertyName + ' - sValue: ' + sValue);
				}
			}
		}
		
		system.debug('### SCC_TD_Scan_WS - scanPages() - mapInfos: ' + mapInfos);
        
		return JSON.serializePretty(mapInfos);
	}

	// ---------------------------------------------
    // cElement class
    // ---------------------------------------------        
	private class cElement {
		integer iType;
		string sPropertyName;
		string sStart;
		string sStop;
        
		cElement(Integer iType, string sPropertyName, string sStart, string sStop) {
			this.iType = iType;
			this.sPropertyName = sPropertyName;
			this.sStart = sStart;
			this.sStop = sStop;
		}
	}
}