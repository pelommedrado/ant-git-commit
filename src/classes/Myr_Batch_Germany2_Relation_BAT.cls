/*****************************************************************************************************
  Purge of notifications of german myRenault accounts
******************************************************************************************************
31 Mar 2016 : Creation 
******************************************************************************************************/
global class Myr_Batch_Germany2_Relation_BAT extends INDUS_Batch_AbstractBatch_CLS { 
    
	String Account_Ids='';
	List<Myr_Batch_Germany2_ToolBox.TB_Answer> TB_Acc;
	Global String Global_Query='';
	global Myr_Batch_Germany2_Relation_BAT() {
		String RetentionDay = Country_Info__c.getInstance('Germany2').Myr_Deleting_Retention__c;
		String restriction='';
		//Account_Ids
		String query = 'Select Id from Account where country__c=\'Germany2\' and ((';
		query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MyD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay;
		query += ') OR (';
		query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myd_Status__c=null';
		query += ') OR (';
		query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myr_Status__c=null';
		query += '))';
		query += Myr_Batch_Germany2_ToolBox.Add_Governor_Limit();
		System.debug('Myr_Batch_Germany2_ToolBox.Get_Account_Ids : query=' + query);
		List<Account> L_A = Database.query(query);

		for (Account a : L_A) {
			if (!restriction.equalsIgnoreCase('')){
				restriction+='\', \'';
			}
			restriction+=a.Id;
		}
		Global_Query='select Id from VRE_VehRel__c where account__c in (\'' + restriction + '\')';
	}  

	global Database.QueryLocator start(Database.BatchableContext info){
		System.debug('Myr_Batch_Germany2_Relation_BAT.start : ' + Global_Query);
        return Database.getQueryLocator(Global_Query); 
    }
   
    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> scope){
		System.debug('Myr_Batch_Germany2_Relation_BAT.doExecute' + scope);
        return new DatabaseAction(scope, Action.ACTION_DELETE, false);
    }   
      
    global override void doFinish(Database.BatchableContext info){
		System.debug('Myr_Batch_Germany2_Relation_BAT.doFinish'+TB_Acc);
    } 
     
    global override String getClassName(){
        return Myr_Batch_Germany2_Relation_BAT.class.GetName();
    }
}