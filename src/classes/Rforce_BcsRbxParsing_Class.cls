/**
  * Class Name : Rforce_BcsRbxParsing_Class
  * Descripion : This is a Parserclass which will be used to fetch the values from BCS andSIC Webservices.
  * Date : 10-Apr-2015
  * Author:Vetrivel S
  * HQREQ-01155 
  * Description :BCS WS Mapping
**/


public class Rforce_BcsRbxParsing_Class {

   public List<Rforce_BcsRbxResponse_Class> parseXML(String xml) 
    {
      Integer start=xml.indexOf('<response>'); 
      Integer endIndex=xml.indexOf('</response>');  
      String formattedxml=xml.SubString(start,endIndex+11);    
      List<Rforce_BcsRbxResponse_Class> bcsxmlDataList=new List<Rforce_BcsRbxResponse_Class>();
      List<Rforce_BcsRbxResponse_Class.Vehicle> vehicleDataList=new List<Rforce_BcsRbxResponse_Class.Vehicle>();

      Integer count=1;

     Dom.Document doc = new Dom.Document();
     doc.load(formattedxml);
     Dom.XMLNode rootElement = doc.getRootElement();


    for (Dom.XmlNode assets : rootElement.getChildElements()) {
 
      if(assets.getname()=='clientList')
      {
        System.debug('Parsing the '+count+'Client >>>>>>');
        count++;
        System.debug('assets.getChildElements()>>>>>>>>>>>'+assets.getChildElements());
        
        Rforce_BcsRbxResponse_Class bcsxmlData=new Rforce_BcsRbxResponse_Class();
  
       for (Dom.XmlNode child : assets.getChildElements()) {
    
           if (child.getname() == 'idClient') {
               bcsxmlData.IdClient=child.getText();
           } 
           if (child.getname() == 'lastName') {
               bcsxmlData.LastName=child.getText();
           } 
           if (child.getname() == 'title') {
               bcsxmlData.Title=child.getText();
           } 
           if (child.getname() == 'lang') { 
               bcsxmlData.Lang=child.getText(); 
           } 
           if (child.getname() == 'firstName') {     
               bcsxmlData.FirstName=child.getText();        
           } 
           
           // Added for BCS New Mapping on 30thMarch2015
           
           if (child.getname() == 'middleName') {     
               bcsxmlData.middleName=child.getText();        
           }   
           
           if (child.getname() == 'ident1') {     
               bcsxmlData.localCustomerID1=child.getText();        
           }  
           if (child.getname() == 'ident2') {     
               bcsxmlData.localCustomerID2=child.getText();        
           }   
           if (child.getname() == 'birthDay') {     
               bcsxmlData.birthDate=child.getText();        
           } 
           if (child.getname() == 'sex') {     
               bcsxmlData.sex=child.getText();        
           }
           if (child.getname() == 'idMyr') {     
               bcsxmlData.myRenaultID=child.getText();        
           }
           if (child.getname() == 'typeperson') {     
               bcsxmlData.customerType=child.getText();        
           }
           
           
                   
       ///BCScommAgreement List
       if (child.getname() == 'BCScommAgreement') {

          for (dom.XmlNode BCScommAgrechild: child.getchildren() ) {
      
                if (BCScommAgrechild.getname() == 'Global.Comm.Agreement') {
                    bcsxmlData.GlobalCommAgreement=BCScommAgrechild.getText();   
                }
                if (BCScommAgrechild.getname() == ' Preferred.Communication.Method') {
                    bcsxmlData.preferredCommunication=BCScommAgrechild.getText();   
                }
                if (BCScommAgrechild.getname() == 'Post.Comm.Agreement') {
                    system.debug('Posttttttttt'+BCScommAgrechild.getText());
                    bcsxmlData.PostCommAgreement=BCScommAgrechild.getText();
                }
                if (BCScommAgrechild.getname() == 'Tel.Comm.Agreement') {
                    system.debug('Telllllllllll'+BCScommAgrechild.getText());
                    bcsxmlData.TelCommAgreemen=BCScommAgrechild.getText();
                }
                if (BCScommAgrechild.getname() == 'SMS.Comm.Agreement') {
                    system.debug('SMSsssssssssssssssss'+BCScommAgrechild.getText());
                    bcsxmlData.SMSCommAgreement=BCScommAgrechild.getText();
                }
                if (BCScommAgrechild.getname() == 'Fax.Comm.Agreement') {
                    system.debug('Faxxxxxxxxxxxxxxxxxxxxxx'+BCScommAgrechild.getText());
                    bcsxmlData.FaxCommAgreement=BCScommAgrechild.getText();
                }
                if (BCScommAgrechild.getname() == 'Email.Comm.Agreement') {
                    system.debug('Emailllllllll'+BCScommAgrechild.getText());
                    bcsxmlData.EmailCommAgreement=BCScommAgrechild.getText();
                }
            } 
      
       }     
       if (child.getname() == 'contact') {

            for (dom.XmlNode contactchild: child.getchildren() ) {
                  if (contactchild.getname() == 'phoneNum1') {
                      bcsxmlData.phoneNum1=contactchild.getText();
                  }
                  if (contactchild.getname() == 'phoneNum2') {
                     bcsxmlData.phoneNum2=contactchild.getText();
                  }
                  if (contactchild.getname() == 'phoneNum3') {
                     bcsxmlData.phoneNum3=contactchild.getText();
                  }   
                  if (contactchild.getname() == 'phoneCode1') {
                      bcsxmlData.phoneCode1=contactchild.getText();
                  }
                  if (contactchild.getname() == 'phoneCode2') {
                     bcsxmlData.phoneCode2=contactchild.getText();
                  }
                  if (contactchild.getname() == 'phoneCode3') {
                     bcsxmlData.phoneCode3=contactchild.getText();
                  }   
                  if (contactchild.getname() == 'email') {
                     bcsxmlData.email=contactchild.getText();
                  }
              }
       }

       if (child.getname() == 'address') {
        
          for (dom.XmlNode addresschild: child.getchildren() ) {
              
              if (addresschild.getname() == 'strName') {
                  system.debug('strNameeeeeee'+addresschild.getText());
                  bcsxmlData.StrName=addresschild.getText();
              }
              if (addresschild.getname() == 'strNum') {
                  system.debug('strNum'+addresschild.getText());
                  bcsxmlData.strNumber=addresschild.getText();
              } 
               
              if (addresschild.getname() == 'compl1') {
                  bcsxmlData.Compl1=addresschild.getText();
              }
              
              if (addresschild.getname() == 'compl2') {
                  bcsxmlData.Compl2=addresschild.getText();
              }
              
              if (addresschild.getname() == 'compl3') {
                  bcsxmlData.Compl3=addresschild.getText();
              }
              
              if (addresschild.getname() == 'strType') {
                  bcsxmlData.streetType=addresschild.getText();
              }
              
              if (addresschild.getname() == 'countryCode') {
                  system.debug('countryCodeeeeeeeeee'+addresschild.getText());
                  bcsxmlData.CountryCode=addresschild.getText();
              } 
                
              if (addresschild.getname() == 'zip') {
                  system.debug('zippppppppppp'+addresschild.getText());
                  bcsxmlData.Zip=addresschild.getText();
              }  
              
              if (addresschild.getname() == 'city') {
                  system.debug('cityyyyyyyyyyyyyy'+addresschild.getText());
                  bcsxmlData.City=addresschild.getText();
              } 
              
              if (addresschild.getname() == 'areaCode') {
                  system.debug('areaCode'+addresschild.getText());
                  bcsxmlData.areaCode=addresschild.getText();
              }             
            
           }
         }
          //typeperson
         if (child.getname() == 'typeperson') {
          system.debug('typepersonnnnnn'+child.getText());
          bcsxmlData.typeperson=child.getText();
         }

         //vehicleList
         // vehicleDataList=new List<Rforce_BcsRbxResponse_Class.Vehicle>();
          
          if (child.getname() == 'vehicleList') {
            
            Rforce_BcsRbxResponse_Class.Vehicle vehicleData=new Rforce_BcsRbxResponse_Class.Vehicle();

            for (dom.XmlNode vehicleListchild: child.getchildren() ) {
                       
                       if (vehicleListchild.getname() == 'vin') {
                            system.debug('vinnnnnnnnnn'+vehicleListchild.getText());
                            vehicleData.vin=vehicleListchild.getText();
                        }
                        
                        vehicleData.IdClient=bcsxmlData.IdClient;
                        
                        if (vehicleListchild.getname() == 'brandCode') {
                            system.debug('brandCodeeeeeeeeee'+vehicleListchild.getText());
                            vehicleData.brandCode=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'modelCode') {
                            system.debug('modelCodeeeeeeeeeee'+vehicleListchild.getText());
                            vehicleData.modelCode=vehicleListchild.getText();
                        } 
                        if (vehicleListchild.getname() == 'modelLabel') {
                            system.debug('modelLabellllllllllll'+vehicleListchild.getText());
                            vehicleData.modelLabel=vehicleListchild.getText();
                        } 
                        if (vehicleListchild.getname() == 'versionLabel') {
                            system.debug('versionLabelllllllllllll'+vehicleListchild.getText());
                            vehicleData.versionLabel=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'registrationDate') {
                            system.debug('lastRegistrationDate> ::'+vehicleListchild.getText());
                            vehicleData.lastRegistrationDate=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'firstRegistrationDate') {
                            system.debug('firstRegistrationDateeeeeeeeeeeeee'+vehicleListchild.getText());
                            vehicleData.firstRegistrationDate=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'registration') {
                            system.debug('registrationnnnnnnnnnnnn'+vehicleListchild.getText());
                            vehicleData.registration=vehicleListchild.getText();
                        } 
                        if (vehicleListchild.getname() == 'new') {
                            system.debug('new ::'+vehicleListchild.getText());
                            vehicleData.vehicleType=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'possessionBegin') {
                            system.debug('possessionBeginnnnnnnnnnnnnnnnn'+vehicleListchild.getText());
                            vehicleData.possessionBegin=vehicleListchild.getText();
                        } 
                        if (vehicleListchild.getname() == 'possessionEnd') {
                            system.debug('possessionEnd'+vehicleListchild.getText());
                            vehicleData.possessionEnd=vehicleListchild.getText();
                        } 
                  }
                  
                  vehicleDataList.add(vehicleData);
          }
          
          bcsxmlData.vcle=vehicleDataList;
      }
      bcsxmlDataList.add(bcsxmlData);
    }
  }
     return bcsxmlDataList;
 }
}