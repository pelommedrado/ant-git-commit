/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto VehicleBooking__c.
* @author Felipe Jesus Silva.
*/
public without sharing class VFC86_VehicleBookingBO 
{
	private static final VFC86_VehicleBookingBO instance = new VFC86_VehicleBookingBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC86_VehicleBookingBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC86_VehicleBookingBO getInstance()
	{
		return instance;
	}
	
	public void insertVehicleBooking(VehicleBooking__c sObjVehicleBooking)
	{
		try
		{									
			VFC85_VehicleBookingDAO.getInstance().insertData(sObjVehicleBooking);		
		}
		catch(DMLException ex)
		{
			throw new VFC88_CreateVehicleBookingException(ex, ex.getDMLMessage(0));
		}					
	}
	
	public void updateVehicleBooking(VehicleBooking__c sObjVehicleBooking)
	{
		try
		{									
			VFC85_VehicleBookingDAO.getInstance().updateData(sObjVehicleBooking);		
		}
		catch(DMLException ex)
		{
			//TODO: criar exception de atualização
			throw new VFC88_CreateVehicleBookingException(ex, ex.getDMLMessage(0));
		}					
	}
		
	public Map<String, VehicleBooking__c> getMappingVehicleBookingActivesByVehicles(Set<String> setVehicleId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = null;
		Map<String, VehicleBooking__c> mapSObjVehicleBooking = new Map<String, VehicleBooking__c>();
		
		/*faz a busca por vehicle bookings ativos para os veículos do set(essa busca retornará no máximo um registro de vehicle booking por veículo)*/
		lstSObjVehicleBooking = VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(setVehicleId, 'Active');
		
		/*se nenhum registro foi encontrado, aborta o processo*/
		if(lstSObjVehicleBooking.isEmpty())
		{
			throw new VFC89_NoDataFoundException('Nenhuma Reserva Ativa foi encontrada para o Veículo que está no Orçamento. Verifique se a mesma não foi Cancelada pelo Gerente.');
		}
		
		/*percorre a lista para montar o mapeamento dos vehicle bookings por id do veículo*/
		for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBooking)
		{
			mapSObjVehicleBooking.put(sObjVehicleBooking.Vehicle__c, sObjVehicleBooking);
		}
		
		return mapSObjVehicleBooking;		
	}
	
	public Map<String, VehicleBooking__c> getMappingVehicleBookingActivesByQuotes(Set<String> setQuoteId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = this.getVehicleBookingActivesByQuotes(setQuoteId);
		
		Map<String, VehicleBooking__c> mapSObjVehicleBooking = new Map<String, VehicleBooking__c>();
		
		/*percorre a lista para montar o mapeamento dos vehicle bookings por id do veículo*/
		for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBooking)
		{
			mapSObjVehicleBooking.put(sObjVehicleBooking.Quote__c, sObjVehicleBooking);
		}
		
		return mapSObjVehicleBooking;		
	}

	public Map<String, VehicleBooking__c> getMappingVehicleBookingActivesOrClosedByQuotes(Set<String> setQuoteId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = this.getVehicleBookingActivesOrClosedByQuotes(setQuoteId);
		
		Map<String, VehicleBooking__c> mapSObjVehicleBooking = new Map<String, VehicleBooking__c>();
		
		/*percorre a lista para montar o mapeamento dos vehicle bookings por id do veículo*/
		for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBooking)
		{
			mapSObjVehicleBooking.put(sObjVehicleBooking.Quote__c, sObjVehicleBooking);
		}
		
		return mapSObjVehicleBooking;		
	}
	
	public List<VehicleBooking__c> getVehicleBookingActivesByQuotes(Set<String> setQuoteId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = null;
		
		/*faz a busca por vehicle bookings ativos para os veículos do set(essa busca retornará no máximo um registro de vehicle booking por orçamento)*/
		lstSObjVehicleBooking = VFC85_VehicleBookingDAO.getInstance().findByQuoteAndStatus(setQuoteId, 'Active');
		
		/*se nenhum registro foi encontrado, aborta o processo*/
		if(lstSObjVehicleBooking.isEmpty())
		{
			throw new VFC89_NoDataFoundException('Nenhuma Reserva Ativa foi encontrada para o Veículo que está no Orçamento. Verifique se a mesma não foi Cancelada pelo Gerente.');
		}
		
		return lstSObjVehicleBooking;		
	}
	
	public List<VehicleBooking__c> getVehicleBookingActivesOrClosedByQuotes(Set<String> setQuoteId)
	{	
		List<VehicleBooking__c> resultList = new List<VehicleBooking__c>();
		List<VehicleBooking__c> lstSObjVehicleBooking = null;
		List<VehicleBooking__c> lstSObjVehicleBookingCanceled = null;
		
		/*faz a busca por vehicle bookings ativos para os veículos do set(essa busca retornará no máximo um registro de vehicle booking por orçamento)*/
		lstSObjVehicleBooking = VFC85_VehicleBookingDAO.getInstance().findByQuoteAndStatus(setQuoteId, 'Active');
		lstSObjVehicleBookingCanceled = VFC85_VehicleBookingDAO.getInstance().findByQuoteAndStatus(setQuoteId, 'Closed');

		System.debug('***lstSObjVehicleBooking' + lstSObjVehicleBooking);
		System.debug('***lstSObjVehicleBookingCanceled' + lstSObjVehicleBookingCanceled);
		
		resultList.addAll(lstSObjVehicleBooking);
		resultList.addAll(lstSObjVehicleBookingCanceled);

		System.debug('***resultList' + resultList);
				
		/*se nenhum registro foi encontrado, aborta o processo*/
		if(resultList.isEmpty())
		{
			throw new VFC89_NoDataFoundException('Nenhuma Reserva Ativa foi encontrada para o Veículo que está no Orçamento. Verifique se a mesma não foi Cancelada pelo Gerente.');
		}
		
		return resultList;		
	}
	
	public VehicleBooking__c getVehicleBookingActiveByQuote(String quoteId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = null;
		VehicleBooking__c sObjVehicleBooking = null;
		
		/*faz a busca pelo vehiclebooking ativo desse orçamento (essa busca retornará no máximo um registro)*/
		lstSObjVehicleBooking = VFC85_VehicleBookingDAO.getInstance().findByQuoteAndStatus(quoteId, 'Active');
		
		/*se nenhum registro foi encontrado, aborta o processo*/
		if(lstSObjVehicleBooking.isEmpty())
		{
			throw new VFC89_NoDataFoundException('Nenhuma Reserva Ativa foi encontrada para o Veículo que está no Orçamento. Verifique se a mesma não foi Cancelada pelo Gerente.');
		}
		
		sObjVehicleBooking = lstSObjVehicleBooking.get(0);
		
		return sObjVehicleBooking;
	}
	
	public void updateVehicleBookingsFromQuotesForCanceledStatus(Set<String> setQuoteId)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = this.getVehicleBookingActivesByQuotes(setQuoteId);
		//Set<Id> lstIdVehicles = new Set<Id>();
        
		for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBooking)
		{
            //lstIdVehicles.add(sObjVehicleBooking.Vehicle__c);
			sObjVehicleBooking.Status__c = 'Canceled';
		}
        
        /*List<VEH_Veh__c> lstVehicles = [Select Id, Status__c From VEH_Veh__c where Id in:lstIdVehicles];
        for(VEH_Veh__c v: lstVehicles){
            if(v.Status__c == 'Billed')
                v.Status__c = 'Stock';
        }*/
        
		
		this.updateVehicleBookings(lstSObjVehicleBooking);
        //update lstVehicles;
	}
	
	public void updateVehicleBookings(List<VehicleBooking__c> lstSObjVehicleBooking)
	{
		try
		{
			System.debug('updateVehicleBookings -> lstSObjVehicleBooking'+lstSObjVehicleBooking);
			//System.debug('updateVehicleBookings -> lstSObjVehicleBooking Result'+VFC85_VehicleBookingDAO.getInstance().updateData(lstSObjVehicleBooking));
			VFC85_VehicleBookingDAO.getInstance().updateData(lstSObjVehicleBooking);
		}
		catch(DMLException ex)
		{
			//TODO: criar exception de atualização
			throw new VFC88_CreateVehicleBookingException(ex, ex.getDMLMessage(0));
		}			
	}
}