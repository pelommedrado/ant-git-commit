/**
*	Class   -   VFC36_QuoteLineItemDAO
*   Author  -   SureshBabu
*   Date    -   13/01/2013
*    
*   #01 <Suresh Babu> <13/01/2013>
*      Created this Class to handle records from QuoteLineItem related Queries.
*/
public with sharing class VFC36_QuoteLineItemDAO extends VFC01_SObjectDAO {
	private static final VFC36_QuoteLineItemDAO instance = new VFC36_QuoteLineItemDAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC36_QuoteLineItemDAO(){}

    /**
    * Method responsible for providing the instance of this class.. 
    */  
    public static VFC36_QuoteLineItemDAO getInstance(){
        return instance;
    }
    
    /** 
	* This Method was used to get QuotelineItem Records based on Quote Ids. 
	* @param setQuoteIds		-	This field using get QuoteLineItems details using Quote Id.
    * @return lstQuoteLineItems	-	fetch and return the result in lstQuoteRecords.
    */
    public List<QuoteLineItem> fetchQuoteLineItems_UsingQuoteIds(Set<Id> setQuoteIds)
    {
    	List<QuoteLineItem> lstQuoteLineItems = 
    		[SELECT Id, QuoteId, Description, PricebookEntry.Id, Vehicle__c, Vehicle__r.Name, PricebookEntry.Pricebook2Id, 
    				PricebookEntry.Product2Id, PricebookEntry.UnitPrice, PricebookEntry.Product2.Description, 
    				PricebookEntry.Product2.RecordTypeId
			   FROM QuoteLineItem
			  WHERE QuoteId IN : setQuoteIds];
		return lstQuoteLineItems;
    }
    
    /** 
	* This Method was used to get QuotelineItem Records based on Quote Ids. 
	* @param setQuoteIds		-	This field using get QuoteLineItems details using Quote Id.
    * @return lstQuoteLineItems	-	fetch and return the result in lstQuoteRecords.
    */
    public QuoteLineItem getQuoteLineItemById(String quoteLineItemId){
        
    	List<QuoteLineItem> quoteLineItem = [SELECT 
    			Id, 
    			QuoteId, 
    			Description
    		FROM 
				QuoteLineItem
			WHERE
				Id =: quoteLineItemId];
                
        if(!quoteLineItem.isEmpty()){
            return quoteLineItem[0];
        }        
                
		return null;
		
    }
    
    /** 
	* This Method was used to get QuotelineItem Records based on Quote Ids. 
	* @param quoteId			-	This field using get QuoteLineItems details using Quote Id.
	* @param setDeveloperName	-	This field using get QuoteLineItems details using Developer Name .
    * @return lstQuoteLineItems	-	fetch and return the result in lstQuoteRecords.
    */
    public List<QuoteLineItem> getQuoteLineItemByQuoteId(String quoteId, Set<String> setDeveloperName){
    	return [SELECT 
					Id, 
					QuoteId, 
					Description,
					UnitPrice, 
					PricebookEntry.Id,
					Vehicle__r.Id, 
					Vehicle__r.Name,
					Vehicle__r.Status__c,
                    PricebookEntry.Pricebook2Id, 
                    PricebookEntry.Product2Id, 
                    PricebookEntry.UnitPrice,
                    PricebookEntry.Product2.Name, 
                    PricebookEntry.Product2.Description,
                    PricebookEntry.Product2.RecordTypeId
	    		FROM 
					QuoteLineItem
				WHERE
					QuoteId =: quoteId
					And PricebookEntry.Product2.RecordType.DeveloperName IN: setDeveloperName
					order by Description, Vehicle__r.Name];
    }
    
    
    public List<QuoteLineItem> fetchQuoteLineItemByQuoteId(String developerName, Set<String> setQuote)
    {
    	return [Select 
    				id , 
    				PricebookEntry.Name , 
    				QuoteId
    			From 
    				QuoteLineItem 
    			Where 
    				QuoteId IN : setQuote 
    			And 
    				PricebookEntry.Product2.RecordType.DeveloperName = :developerName  ];	
    }
}