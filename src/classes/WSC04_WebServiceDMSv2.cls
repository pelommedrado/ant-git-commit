global class WSC04_WebServiceDMSv2 {
    
    webservice static List<VeiculoEstoque> updateStock(Integer numberBIR, List<VeiculoEstoque> veiculoList) {
        System.debug('Update Stock...');
        System.debug('numberBIR: '      + numberBIR);
        System.debug('veiculoList: '    + veiculoList);
        
        WSC04_WebServiceDMSServico wsService = new WSC04_WebServiceDMSServico();
        wsService.validarVeiculo(veiculoList);
        
        for(VeiculoEstoque vei : veiculoList) {
            if(!String.isEmpty(vei.errorMessage)) {
                return veiculoList;
            }
        }
        
        wsService.atualizacaoContinua(numberBIR, veiculoList);
        
        /*for(VeiculoEstoque vei : veiculoList) {
            if(String.isEmpty(vei.errorMessage)) {
                vei.errorMessage = 'Integration Succeeded!!!';
            }
        }*/
        
        return veiculoList;   
    }
    
    global class VeiculoEstoque {
        
        webservice Boolean  fullIntegration      { get;set; }
        webservice String   version              { get;set; }   // required: Versão [Text:255]
        webservice String   VIN                  { get;set; }   // required: Chassi [Text:17]
        webservice String   model                { get;set; }   // required: Modelo [Text:100]
        webservice Integer  manufacturingYear    { get;set; }   // required: Ano Fabricação [4]
        webservice Integer  modelYear            { get;set; }   // required: Ano Modelo [4]
        webservice String   color                { get;set; }   // required: Cor [Text:100]
        webservice String   optionals            { get;set; }   // required: Opcionais [Text:400]
        webservice Date     entryInventoryDate   { get;set; }   // required: Data Entrada no Estoque
        webservice Double   price                { get;set; }   // required: Preço [10,2]
        webservice Datetime lastUpdateDate       { get;set; }   // required: Data Última Atualização
        webservice String   status               { get;set; }   // required: Status : EN: [Available / To be billed / Reserved / Billed]
        webservice String   stage                { get;set; }   // required: Stage : EN: [IN TRANSIT / STOCK]
        webservice Date     costumerDeliveryDate { get;set; }   // 
        webservice String   errorMessage         { get;set; }   // optional
    }
}