/**
* @author: Rajavel Baskaran
* @date : 02-10-2015
* @description: This class is used to Create/Reset/Send Activation Link of MyR/MyD customer.
* @createAccount()
* @resetPassword()
* @resendActivationEmail()
* @initialise()
*
* @Modified by : Ramamoorthy Dakshinamoorthy(for Version 3)
* @date : 02-02-2016
* 
* @Modified by : Ramamoorthy Dakshinamoorthy(for Version 4)
* @date : 07-03-2016
*/
  public with sharing class Rforce_MyRMyD_VFCx {
  
    public Account oAccount;
    public boolean bIsCreateMyRButtonShown{set;get;}
    public boolean bIsCreateMyDButtonShown{set;get;}
    public boolean bIsResetMyRButtonShown{set;get;}
    public boolean bIsResetMyDButtonShown{set;get;}
    public boolean bIsReactiveMyRButtonShown{set;get;}
    public boolean bIsReactiveMyDButtonShown{set;get;}
    public String sEmail {get; set;}
    public String sBrandName {get; set;}
    public String sMyRStatus {get; set;}
    public String sMyDStatus {get; set;}    
    public String sMyRUserName {get;set;}
    public String sMyDUserName {get;set;}
    public User oUsr {get;set;}
    public boolean bIsDisplayPage{set;get;}   
    
    // US ID-->2601,2600 ; Feature ID -->1528     
    public boolean bIsRefreshPage{set;get;}
    public String sMatchingEmail {get; set;}
    public String sMatchingBrandName {get; set;}
    public String sErrorCode{get;set;}
    public String sAccId {get;set;}
    //Standard Salesforce Constructor which holds Account values available in Form
    public Rforce_MyRMyD_VFCx(ApexPages.StandardController controller) {
    Account oAccountDetails = (Account)controller.getRecord();
    
    //Queried AccountBrand__c also for US ID-->2601,2600 ; Feature ID -->1528
    try{
            oUsr = [SELECT  Id,RecordDefaultCountry__c FROM User WHERE Id = : UserInfo.getUserId()];
              if ( oUsr.RecordDefaultCountry__c ==System.Label.Rforce_Germany){
                    bIsDisplayPage = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,System.Label.Rforce_GermanyUnavailability));    
              } else{bIsDisplayPage=true;}
            this.oAccount = [select id,PersEmailAddress__c,AccSubSource__c,Account_Sub_Sub_Source__c,AccountSource,MYR_Status__c,MYD_Status__c,MyRenaultID__c,MyDaciaID__c,AccountBrand__c from Account where id = :oAccountDetails.id];           
            sMatchingBrandName = oAccount.AccountBrand__c;
            sMatchingEmail = oAccount.PersEmailAddress__c;
            sAccId = oAccount.Id;
            initialise();                  
        }
    catch(DMLException e){
        System.debug('Exception-->Rforce_MyRMyD_VFCx :'+e.getMessage());
    }
   }
        
/**
* @author: Rajavel Baskaran
* @date: 02-10-2015
* @description:Create MyR/MyD account to the customer by updating the MyR_Status__c / MyD_Status__c by 
               preregistered, AccountSource=SRC,SUbsource=ADOBE_CAMPAIGN,SUBSUBSOURCE=MYR
* @return: void

* @Modified by : Ramamoorthy Dakshinamoorthy(for Version 3)
* @date : 02-02-2016
* @description : Set the MyRUserName/MyDUserName and MyRStatus/MyDStatus according to the condition specified
* @Modified by : Ramamoorthy Dakshinamoorthy(for Version 4)
* @date : 07-03-2016
* @description : Check whether the personal email address is used in anyother MyR/MyD account
*/
  
  public void createAccount() {

    oAccount.AccountSource = System.Label.Rforce_ACC_Source;
    oAccount.AccSubSource__c = System.Label.Rforce_ACC_Sub_Source;          
    oAccount.Account_Sub_Sub_Source__c = System.Label.Rforce_ACC_MYR_SubSub_Source;
    
      if (sMatchingBrandName ==null || sMatchingBrandName == System.Label.Rforce_Nissan || sMatchingBrandName == System.Label.Rforce_RenaultDacia) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Rforce_ErrorBrandRequired));    
        }
        if(sMatchingEmail == null && oAccount.MyRenaultID__c==null && oAccount.MyDaciaID__c==null){  
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Error_Email_Required));
        }
    //Added for // US ID-->2601,2600 ; Feature ID -->1528 
    else if(((sMatchingBrandName != System.Label.Rforce_RenaultDacia ) && (sMatchingBrandName != System.Label.Rforce_Nissan )&& (sMatchingBrandName != null)))
    {
    Myr_ManageAccount_MatchingKeys_Cls myrManageAccount = new Myr_ManageAccount_MatchingKeys_Cls(sMatchingBrandName , sMatchingEmail,sAccId);
    Myr_ManageAccount_MatchingKeys_Cls.Response myrManageAccountResponse = myrManageAccount.checkEmailAvailability();
    system.debug('ErrorCode : ' + myrManageAccountResponse.ErrorCode);
    sErrorCode = myrManageAccountResponse.ErrorCode;
             
       if (sErrorCode == system.label.Myr_ManageAccount_WS01MS520) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Rforce_EmailDuplicateCreatedAccount));
        }         
        else if (sErrorCode == system.label.Myr_ManageAccount_WS01MS521) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Rforce_EmailDuplicateActivatedAccount));
        }
        else if (sErrorCode == system.label.Myr_ManageAccount_WS01MS530) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,System.Label.Rforce_EmailDuplicatebyAccountID));
        }       
        else if((sErrorCode!=system.label.Myr_ManageAccount_WS01MS520||sErrorCode !=system.label.Myr_ManageAccount_WS01MS521||sErrorCode !=system.label.Myr_ManageAccount_WS01MS530)){
          if(sBrandName==System.Label.Rforce_Renault && (oAccount.MYR_Status__c== null || oAccount.MYR_Status__c== '' || oAccount.MYR_Status__c == System.Label.Rforce_MyRMyDStatus_Deleted)){  
                system.debug('Inside Renault');
                if(oAccount.MyDaciaID__c == null && sMatchingEmail!=null){
                       oAccount.MYR_Status__c = System.Label.Rforce_MYRMYD_Status;
                       oAccount.MyRenaultID__c = oAccount.PersEmailAddress__c;                              
                }
                else if(oAccount.MyDaciaID__c != null) {
                          oAccount.MYR_Status__c = System.Label.Rforce_MYRMYD_Status;
                          oAccount.MyRenaultID__c = oAccount.MyDaciaID__c;                                    
               }
          }
          if(sBrandName==System.Label.Rforce_Dacia && (oAccount.MYD_Status__c== null || oAccount.MYD_Status__c== '' || oAccount.MYD_Status__c == System.Label.Rforce_MyRMyDStatus_Deleted)){ 
              system.debug('Inside Dacia');
                if(oAccount.MyRenaultID__c==null && sMatchingEmail!=null){
                          oAccount.MYD_Status__c = System.Label.Rforce_MYRMYD_Status;
                          oAccount.MyDaciaID__c = oAccount.PersEmailAddress__c;                           
                }
                else if(oAccount.MyRenaultID__c !=null){
                          oAccount.MYD_Status__c = System.Label.Rforce_MYRMYD_Status;
                          oAccount.MyDaciaID__c =  oAccount.MyRenaultID__c;                             
                } 
         }
    bIsRefreshPage=true;
    update oAccount;
    }
    } 
 }        
/**
* @author: Rajavel Baskaran
* @date: 02-10-2015
* @description:Reset the Gigya password of the given Personal Account
* @return: void
*/ 
 
  public void resetPassword(){
  System.debug('Inside resetPassword');
       Myr_GigyaFunctions_Cls myrGigyaFunction =  new Myr_GigyaFunctions_Cls();
       Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response myrResonse = myrGigyaFunction.resetPassword(oAccount.id,sBrandName);    
  ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info,myrResonse.msg));
  }

/**
* @author: Rajavel Baskaran
* @date: 02-10-2015
* @description:Resend the activation mail of the given Personal Account
* @return: void.
*/

  public void resendActivationEmail(){
  System.debug('Inside resendActivationEmail');
      Myr_GigyaFunctions_Cls myrGigyaFunction =  new Myr_GigyaFunctions_Cls();
      Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response myrResonse = myrGigyaFunction.resendActivationEmail(oAccount.id,sBrandName);
   ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info,myrResonse.msg));
  }
  
/**
* @author: Ramamoorthy Dakshinamoorthy
* @date: 07-03-2016
* @description:To set the button visibility according to the MyR/MyD status
* @return: void.
*/ 
 public void initialise() {
 System.debug('initialise'); 
    if(oAccount.MYR_Status__c!=System.Label.Rforce_MYRMYD_Status && oAccount.MYR_Status__c!=System.Label.Rforce_MyRMyDStatus_Activated && oAccount.MYR_Status__c!=System.Label.Rforce_MyRMyDStatus_Created && (oAccount.MYR_Status__c==null||oAccount.MYR_Status__c=='')){
        bIsCreateMyRButtonShown= false;
    }
    else{
        bIsCreateMyRButtonShown= true;
    }       
    if(oAccount.MYR_Status__c==System.Label.Rforce_MyRMyDStatus_Activated){
        bIsResetMyRButtonShown = false;
    }
    else{
        bIsResetMyRButtonShown= true;
    }     
    if(oAccount.MYR_Status__c==System.Label.Rforce_MyRMyDStatus_Created){
        bIsReactiveMyRButtonShown = false;
    }
    else {
        bIsReactiveMyRButtonShown = true;
    }
    if(oAccount.MYD_Status__c!=System.Label.Rforce_MYRMYD_Status && oAccount.MYD_Status__c!=System.Label.Rforce_MyRMyDStatus_Activated && oAccount.MYD_Status__c!=System.Label.Rforce_MyRMyDStatus_Created && (oAccount.MYD_Status__c==null || oAccount.MYD_Status__c=='')){
        bIsCreateMyDButtonShown= false;
    }
    else{
        bIsCreateMyDButtonShown = true;
    }
    if(oAccount.MYD_Status__c==System.Label.Rforce_MyRMyDStatus_Activated) {
        bIsResetMyDButtonShown = false;
    }
    else{
        bIsResetMyDButtonShown = true;
    }
    if(oAccount.MYD_Status__c==System.Label.Rforce_MyRMyDStatus_Created){
        bIsReactiveMyDButtonShown = false;
    }
    else{
        bIsReactiveMyDButtonShown = true;
    }     
} 
}