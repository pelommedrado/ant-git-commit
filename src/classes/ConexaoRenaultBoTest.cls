@isTest
private class ConexaoRenaultBoTest {


	@isTest
	static void itShouldExecuteFatura2() {

		FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c 	= 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
    fatura.Customer_Type__c 		= 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1199098876';
		fatura.Phone_3__c = '1120987761';
		fatura.External_Key__c = '123';
		fatura.Delivery_Date_Mirror__c = System.today();
		fatura.Status__c = 'Valid';
		fatura.Invoice_Date_Mirror__c = System.today();
		Database.insert(fatura);

		final List<FaturaDealer2__c> faturaList = [
			SELECT Id, Invoice_Date_Mirror__c, VN_Survey_Date__c
			FROM FaturaDealer2__c
			WHERE Status__c = 'Valid' AND Customer_Type__c = 'F'
				AND Invoice_Type__c = 'New Vehicle'
				AND Invoice_Date_Mirror__c != NULL AND VN_Survey_Date__c = NULL
		];

		ConexaoRenaultBo.getInstance().executeFatura2(faturaList);
		final FaturaDealer2__c f = faturaList.get(0);
		System.assert(f.VN_Survey_Date__c != null);
	}

	@isTest
	static void itShouldExecuteFatura() {
		FaturaDealer__c fat = new FaturaDealer__c();
    fat.NFTIPREG__c = '10';
    fat.NFBIRENV__c = '07600999';
    fat.NFBIREMI__c = '07600999';
    fat.NFSEQUEN__c = '2780';
    fat.NFNROREG__c = '00029';

    fat.NFBANDEI__c = 'REN';
    fat.NFCODOPR__c = '25';
    fat.NFCODFIS__c = '5403  VN';
    fat.NFTIPONF__c = 'VN';
    fat.NFNRONFI__c = '00073295';
    fat.NFCHASSI__c = '11111111111111911';

    fat.NFSERNFI__c = 'U';
    fat.NFDTANFI__c = '20150827';
    fat.NFNOMCLI__c = 'ITAMAR JOSE VIEIRA FERNANDES';
    fat.NFTIPVIA__c = 'RUA';
    fat.NFNOMVIA__c = 'Manoel Mancellos Moura';

    fat.NFNROVIA__c = '681';
    fat.NFCPLEND__c = 'AP 103';
    fat.NFBAIRRO__c = 'Canasvieiras';
    fat.NFCIDADE__c = 'FLORIANÓPOLIS';
    fat.NFNROCEP__c = '88054030';

    fat.NFESTADO__c = 'SC';
    fat.NFPAISRE__c = '00';
    fat.NFESTCIV__c = '';
    fat.NFSEXOMF__c = 'M';
    fat.NFDTANAS__c = '195908';
    fat.NFTIPCLI__c = 'F';
    fat.NFCPFCGC__c = '37970380972';

    fat.NFEMAILS__c = 'pelommedrado@gmail.com';
    fat.NFDDDRES__c = '11';
    fat.NFTELRES__c = '77778888';
    fat.NFDDDCEL__c = '11';
    fat.NFTELCEL__c = '99996666';
    fat.NFANOFAB__c = '2000';
    fat.NFANOMOD__c = '2000';
		fat.NFCODOPR_Mirror__c = '19';
		fat.NFDTANFI_Mirror__c = System.today();
		fat.RecordTypeId = FaturaDealerServico.VALID;

		INSERT fat;

		final List<FaturaDealer__c> faturaList = [
			SELECT Id, NFDTANFI_Mirror__c, PV_Survey_Date__c
			FROM FaturaDealer__c
			WHERE NFTIPREG__c = '10' AND NFCODOPR_Mirror__c = '19'
				AND NFTIPCLI__c = 'F' AND NFDTANFI_Mirror__c != NULL
				AND PV_Survey_Date__c = NULL
				AND RecordTypeId =: FaturaDealerServico.VALID
		];

		ConexaoRenaultBo.getInstance().executeFatura(faturaList);
		final FaturaDealer__c f = faturaList.get(0);
		System.assert(f.PV_Survey_Date__c != null);
	}
}