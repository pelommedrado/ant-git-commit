public with sharing class Rforce_ContractProperties {
    
    /* public String lastName {get;set;}
    public String firstName {get;set;}
    public String address {get;set;}
    public String idContractCard {get;set;}    
    public String country {get;set;}
    public String status {get;set;}
    public String kmMaxSubscription {get;set;} */
    
    
    public String idContract {get;set;}
    public String strType {get;set;}
    public String strTechLevel {get;set;}
    public String strServiceLevel {get;set;}
    public String strSubsDate {get;set;}
    public String strUpdDate {get;set;}
    public String strStatus {get;set;}
    public String strProductCode {get;set;}
    public String strContractValidityDate {get;set;}
    public String strContractValidityKM {get;set;}
    
}