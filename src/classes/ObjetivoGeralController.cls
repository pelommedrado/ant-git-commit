public class ObjetivoGeralController extends ObjetivoAbstractController {
    /*public List<String> modeloList			{ 
        get {
            List<String> tempList = new List<String>();
            tempList.add('CLIO');
            tempList.add('DUSTER');
            tempList.add('DUSTER/ORCH');
            tempList.add('FLUENCE');
            tempList.add('KANGOO');
            tempList.add('LOGAN');
            tempList.add('MASTER');
            tempList.add('SANDERO');
            tempList.add('SANDERO/RS');
            tempList.add('SANDERO/STEPWAY');
            return tempList;
        } 
    }*/
    
    public ObjetivoGeralController() {
        init();
        updateData();
        validar();
    }
    
    public override void updateMgd() {
        validar();
    }
    
    public void distribuirValores() {
        if(this.mgd.isEmpty()) {
            return;
        }
        
        Integer va = (Integer) Math.floor(this.mgg.General_Objetive__c / this.mgd.size());
        Integer to = 0;
        
        for(Monthly_Goal_Dealer__c d: this.mgd) {
            d.General_Objetive__c = va;
            to += va;
        }
        
        Integer resto = (Integer) this.mgg.General_Objetive__c - to;
        if(resto > 0) {
            Monthly_Goal_Dealer__c d =
                this.mgd.get(this.mgd.size()-1);
            d.General_Objetive__c += resto;
        }
        
        distribuirValoresModelo();        
        
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.Confirm, 
            'Valores distribuidos com sucesso. '));
    }
    
    public void distribuirValoresModelo() {
        if(this.mgd.isEmpty()) {
            return;
        }
        
        String param = Apexpages.currentPage().getParameters().get('acc');
        System.debug('param: ' + param);
        //Integer totalModelo = 10;
        
        Decimal newTotal = 0;
        for(Monthly_Goal_Dealer__c d: this.mgd) {
            if(!String.isEmpty(param) && d.Dealer__c != param) {
                continue;
            }
            
            d.Clio_Proposal__c 	 		  = calc(d.General_Objetive__c, this.mgg.Clio_Proposal_percent__c);
            d.Duster_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Duster_Proposal_percent__c);
            d.Duster_Oroch_Proposal__c 	  = calc(d.General_Objetive__c, this.mgg.Duster_Oroch_Proposal_percent__c);
            d.Fluence_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Fluence_Proposal_percent__c);
            d.Kangoo_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Kangoo_Proposal_percent__c);
            d.Logan_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Logan_Proposal_percent__c);
            d.Master_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Master_Proposal_percent__c);
            d.Sandero_Proposal__c 		  = calc(d.General_Objetive__c, this.mgg.Sandero_Proposal_percent__c);
            d.Sandero_RS_Proposal__c 	  = calc(d.General_Objetive__c, this.mgg.Sandero_RS_Proposal_percent__c);
            d.Sandero_Stepway_Proposal__c = calc(d.General_Objetive__c, this.mgg.Sandero_Stepway_Proposal_percent__c);
            
            newTotal = 0;
            newTotal += d.Clio_Proposal__c;
            newTotal += d.Duster_Proposal__c;
            newTotal += d.Duster_Oroch_Proposal__c;
            newTotal += d.Fluence_Proposal__c;
            newTotal += d.Kangoo_Proposal__c;
            newTotal += d.Logan_Proposal__c;
            newTotal += d.Master_Proposal__c;
            newTotal += d.Sandero_Proposal__c;
            newTotal += d.Sandero_RS_Proposal__c;
            newTotal += d.Sandero_Stepway_Proposal__c;
            
            d.General_Objetive__c = newTotal;
        }
        validar();
    }
    
    public void salvar() {
        if(this.mgd.isEmpty()) {
            return;
        }
        
        Savepoint sp = Database.setSavepoint();
        try {         
            if(!validar()) {
                return;
            }
            
            UPDATE this.mgg;
            
            UPDATE this.mgd;
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.Confirm, 
                'Alterações salvas com sucesso. '));
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex.getMessage());
            System.debug('LINE: ' + ex.getLineNumber());
            
            Database.rollback( sp );
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'Ex: ' + ex.getMessage() ) );
            
        }
    }
    
    private Boolean validar() {
        this.mgg.Clio_Proposal__c 			= 0;
        this.mgg.Duster_Proposal__c 		= 0;
        this.mgg.Duster_Oroch_Proposal__c 	= 0;
        this.mgg.Fluence_Proposal__c		= 0;
        this.mgg.Kangoo_Proposal__c			= 0;
        this.mgg.Logan_Proposal__c			= 0;
        this.mgg.Master_Proposal__c			= 0;
        this.mgg.Sandero_Proposal__c		= 0;
        this.mgg.Sandero_RS_Proposal__c		= 0;
        this.mgg.Sandero_Stepway_Proposal__c= 0;
        
        this.total = 0;
        
        for(Monthly_Goal_Dealer__c d: this.mgd) {
            this.total += d.General_Objetive__c;
            this.mgg.Clio_Proposal__c 			+= d.Clio_Proposal__c;
            this.mgg.Duster_Proposal__c 		+= d.Duster_Proposal__c;
            this.mgg.Duster_Oroch_Proposal__c 	+= d.Duster_Oroch_Proposal__c;
            this.mgg.Fluence_Proposal__c 		+= d.Fluence_Proposal__c;
            this.mgg.Kangoo_Proposal__c 		+= d.Kangoo_Proposal__c;
            this.mgg.Logan_Proposal__c 			+= d.Logan_Proposal__c;
            this.mgg.Master_Proposal__c 		+= d.Master_Proposal__c;
            this.mgg.Sandero_Proposal__c 		+= d.Sandero_Proposal__c;
            this.mgg.Sandero_RS_Proposal__c 	+= d.Sandero_RS_Proposal__c;
            this.mgg.Sandero_Stepway_Proposal__c += d.Sandero_Stepway_Proposal__c;
        }
        
        if(this.total < ((Integer) this.mgg.General_Objetive__c) ) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'O valor distribuido ' + total + ' é menor que o valor proposto ' +  this.mgg.General_Objetive__c) );
            return false;
        }
        
        return true;
    }
    
    public PageReference atribuirSeller() {
        PageReference page = new PageReference(
            '/ObjetivoSeller?mes=' + this.mgg.Month__c + '&ano=' + this.mgg.Year__c);
        return page;
    }
}