public with sharing class VFC75_AccessoryDAO extends VFC01_SObjectDAO{
	
	private static final VFC75_AccessoryDAO instance = new VFC75_AccessoryDAO();

    /*private constructor to prevent the creation of instances of this class*/
    private VFC75_AccessoryDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC75_AccessoryDAO getInstance(){
        return instance;
    }
    
     public void insertData(QuoteLineItem sObjQuoteLineItem)
    {
    	super.insertData(sObjQuoteLineItem);
    }
    
     public void updateData(QuoteLineItem sObjQuoteLineItem)
    {
    	super.updateData(sObjQuoteLineItem);
    }
    
     public void insertData(PricebookEntry sObjPricebookEntry)
    {
    	super.insertData(sObjPricebookEntry);
    }
    
    public PricebookEntry getPricebookEntry(String productId, String priceBookId){
    	
    	PricebookEntry sObjPricebookEntry = [
	    	SELECT 
	    		IsActive, 
	    		CreatedById, 
	    		CurrencyIsoCode, 
	    		ProductCode, 
	    		LastModifiedDate, 
	    		CreatedDate, 
	    		IsDeleted, 
	    		Id, 
	    		Pricebook2Id, 
	    		Product2Id, 
	    		SystemModstamp, 
	    		Name, 
	    		UnitPrice, 
	    		UseStandardPrice, 
	    		LastModifiedById 
	    	FROM 
	    		PricebookEntry
	    	WHERE
	    		Pricebook2Id =: priceBookId AND
	    		Product2Id =: productId 
	    	limit 1];
    	
    	
    	return sObjPricebookEntry;
    	
    }
    
    public PricebookEntry getPricebookEntryByProduc(String productId){
    	
    	PricebookEntry sObjPricebookEntry = [
	    	SELECT 
	    		IsActive, 
	    		CreatedById, 
	    		CurrencyIsoCode, 
	    		ProductCode, 
	    		LastModifiedDate, 
	    		CreatedDate, 
	    		IsDeleted, 
	    		Id, 
	    		Pricebook2Id, 
	    		Product2Id, 
	    		SystemModstamp, 
	    		Name, 
	    		UnitPrice, 
	    		UseStandardPrice, 
	    		LastModifiedById 
	    	FROM 
	    		PricebookEntry
	    	WHERE
	    		Product2Id =: productId 
	    	limit 1];
    	
    	
    	return sObjPricebookEntry;
    	
    }    
	

}