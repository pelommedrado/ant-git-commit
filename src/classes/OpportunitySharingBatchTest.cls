@isTest
private class OpportunitySharingBatchTest {

	private static testMethod void test() {
        Account newAcc = MyOwnCreation.getInstance().criaAccountDealer();
        newAcc.ShippingStreet = 'Rua X';
        insert newAcc;
    
        
        Opportunity newOpp   = MyOwnCreation.getInstance().criaOpportunity();
        newOpp.AccountId     = newAcc.Id;
        newOpp.Dealer__c     = newAcc.Id;
        insert newOpp; 
        
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(newOpp);
        
        OpportunitySharingBatch batch = new OpportunitySharingBatch(lstOpp);
        Database.executeBatch(batch);

        
	}

}