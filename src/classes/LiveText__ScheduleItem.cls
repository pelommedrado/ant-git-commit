/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class ScheduleItem {
    @InvocableVariable(label='Bypass Opt-in' description='If checked, this message will be sent without verification of opt in.' required=false)
    global Boolean BypassOptinCheck;
    @InvocableVariable(label='Enforce Object Presence' description='If checked and the object specified by ObjectId is is deleted prior to send, the send will fail.' required=false)
    global Boolean EnforceObjectPresence;
    @InvocableVariable(label='LiveText Number' description='Phone number to send the message from.' required=false)
    global String LiveTextNumber;
    @InvocableVariable(label='Message' description='Message to send.' required=true)
    global String Message;
    @InvocableVariable(label='Message Source' description='Optional message source (ITR, etc)' required=false)
    global String MessageSource;
    @InvocableVariable(label='Object Id' description='Identifier for object that triggered this scheduled item.' required=true)
    global Id ObjectId;
    @InvocableVariable(label='Reference Id' description='Reference identifier for this scheduled item.' required=true)
    global String ReferenceId;
    @InvocableVariable(label='Schedule Date' description='Date and time to send the message.' required=false)
    global Datetime ScheduleDate;
    @InvocableVariable(label='To' description='Phone number to send the message to.' required=false)
    global String ToPhoneNumber;
    global ScheduleItem() {

    }
}
