public class VFC64_QuoteDetailsController 
{
    private String opportunityId;
    private String quoteId;
    private List<String> brandsDetailList;
    private List<String> modelDetailList;
    public VFC61_QuoteDetailsVO quoteDetailsVO {get;set;}
    public String selectedBrand {get;set;}
    public String selectedModel {get;set;}
                
    public VFC64_QuoteDetailsController (ApexPages.StandardController controller)
    {
        System.debug('VFC64_QuoteDetailsController:');
        this.quoteId = controller.getId();
        this.opportunityId = Apexpages.currentPage().getParameters().get('opportunityId');       
    }

     public PageReference initialize()
     {
         System.debug('this.quoteId :' + this.quoteId);
         System.debug('this.opportunityId :' + this.opportunityId);
         
         this.quoteDetailsVO = VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, this.opportunityId);
         
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> quoteDetailsVO > '+this.quoteDetailsVO);
        
        if(this.quoteDetailsVO.sObjQuote.UsedVehicle__r != null)
        {
            selectedBrand = this.quoteDetailsVO.sObjQuote.UsedVehicle__r.Brand__c;
            selectedModel = this.quoteDetailsVO.sObjQuote.UsedVehicle__r.Model__c;
        }
         
        this.getBrandDetails();
        this.getModelDetails();        
        this.buildPicklists(); 
                       
        return null;
     }
        
     /**
     * Constrói os picklists da tela.
     */
     private void buildPicklists()
     {       
        this.quoteDetailsVO.lstSelOptionPaymentMethods = VFC49_PickListUtil.buildPickList(Quote.PaymentMethods__c);
     }
     
    /**
     * Metodo responsavel por salvar o orcamento
     */
     public PageReference updateQuoteDetails()
     {
        List<MLC_Molicar__c> lstMolicarDetId = null;
        PageReference pageRef = null;
        ApexPages.Message message = null;
        
        if(quoteDetailsVO.sObjQuote.ExpirationDate == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Informe a data de validade.'));
               return null;
        }
                
        try
        {
            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
            
            if(!lstMolicarDetID.isEmpty())
            {
                quoteDetailsVO.sObjQuote.UsedVehicle__c = lstMolicarDetID[0].Id;            
            }
                
            VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);                                    
            pageRef = new PageReference('/apex/VFP11_Quote?Id=' + this.quoteDetailsVO.opportunityId);     
        }
        catch(Exception ex)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                        
            ApexPages.addMessage(message);
        }
              
        return pageRef;
      }
        
      /**
      * Trata a ação da exlusão de algum item do orçamento.
      */     
      public void deleteQuoteLineItem()
      {
         VFC83_QuoteLineItemVO quoteLineItemVOAux = null;
         ApexPages.Message message = null;
         String quoteLineItemId = ApexPages.currentPage().getParameters().get('quoteLineItemId');
                
         /*obtém da lista de itens o item que corresponde ao veículo (caso ele exista)*/
         quoteLineItemVOAux = this.getQuoteLineItemVehicle();
                
         try
         {               
            VFC65_QuoteDetailsBusinessDelegate.getInstance().deleteQuoteLineItem(this.quoteDetailsVO, quoteLineItemId);
                        
            /*se o item que foi deletado é o veículo, seta o flag da reserva para false*/
            if((quoteLineItemVOAux != null) && (quoteLineItemVOAux.id.equals(quoteLineItemId)))
            {
                this.quoteDetailsVO.vehicleWasReserved = false;
            }                                       
         }
         catch(VFC90_DeleteQuoteLineItemException ex)
         {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                        
            ApexPages.addMessage(message);
          }
      }
        
        /**
        * Trata a ação do botão reservar veículo.
        */
        public void reserveVehicle()
        {
            ApexPages.Message message = null;
            VFC83_QuoteLineItemVO quoteLineItemVOAux = null;


            if(quoteDetailsVO.sObjQuote.ExpirationDate == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Informe a data de validade.'));
                   return;
            }
    
          
            if(this.quoteDetailsVO.vehicleWasReserved)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Você já reservou o veículo.');
                this.getQuoteLineItemVehicle().isReserved = true;
            }
            else
            {
                /*obtém da lista de itens o item que corresponde ao veículo (caso ele exista)*/
                quoteLineItemVOAux = this.getQuoteLineItemVehicle();
                        
                /*verifica se não foi encontrado um item de cotação que é veículo*/
                if(quoteLineItemVOAux == null)
                {
                    message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Deve ser inserido um veículo com chassi no orçamento para realizar a reserva.');
                }
                else
                {
                    /*seta no vo principal o id do veículo que será reservado*/
                    this.quoteDetailsVO.idReservedVehicle = quoteLineItemVOAux.vehicleId;
                                
                    try
                    {
                        /*faz a reserva e obtém o id do vehicleBooking criado*/
                        this.quoteDetailsVO.vehicleBookingId = VFC65_QuoteDetailsBusinessDelegate.getInstance().reserveVehicle(this.quoteDetailsVO);
                                        
                        /*sinaliza que a reserva foi feita*/
                        this.quoteDetailsVO.vehicleWasReserved = true;
                        
                        /*seta o flag do item (veículo) para reservado caso o usuário não tenha o feito*/
                        quoteLineItemVOAux.isReserved = true;
                                        
                        message = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Veículo reservado.');
                    }
                    catch(VFC88_CreateVehicleBookingException ex)
                    {
                        message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                    }       
                }                       
            }
                
            ApexPages.addMessage(message);                  
        }
        
         /**
        * Retorna o item de cotação que é um veículo.
        * @return Se encontrado, retorna o item correspondente ao veículo, caso contrário, retorna null.
        */
        private VFC83_QuoteLineItemVO getQuoteLineItemVehicle()
        {
            VFC83_QuoteLineItemVO quoteLineItemVOAux = null;
                
            /*percorre a lista para procurar o item da cotação que é um veículo*/
            for(VFC83_QuoteLineItemVO quoteLineItemVO : this.quoteDetailsVO.lstQuoteLineItemVO)
            {
                /*se o id do veículo estiver preenchido, indica que o item foi encontrado*/
                if(String.isNotEmpty(quoteLineItemVO.vehicleId))
                {
                    quoteLineItemVOAux = quoteLineItemVO;
                                
                    break;
                }
            }
                
            return quoteLineItemVOAux;
        }
        
        /**
        * Trata a ação do botão Gerar Pedido.
        */
        public PageReference generatePreOrder()
        {
            PageReference pageRef = null;
            ApexPages.Message message = null;
            VFC83_QuoteLineItemVO quoteLineItemVOAux = null;
        	List<MLC_Molicar__c> lstMolicarDetId = null;

            if(quoteDetailsVO.sObjQuote.ExpirationDate == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Informe a data de validade.'));
                   return null;
            }
    
            /*obtém da lista de itens o item que corresponde ao veículo (caso ele exista)*/
            quoteLineItemVOAux = this.getQuoteLineItemVehicle();
                    
            /*verifica se não foi encontrado um item de cotação que é veículo*/
            if(quoteLineItemVOAux == null)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Deve ser inserido um veículo com chassi no orçamento para realizar a reserva.');
                ApexPages.addMessage(message);
            } 
            else 
            {               
                try
                {


		            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
		            
		            if(!lstMolicarDetID.isEmpty())
		            {
		                quoteDetailsVO.sObjQuote.UsedVehicle__c = lstMolicarDetID[0].Id;            
		            }


                    VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);
    
                    
                    pageRef = new PageReference('/apex/VFP15_SendToRequest?Id=' + this.quoteId);                          
                }
                catch(Exception ex)
                {
                    message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                            
                    ApexPages.addMessage(message);
                }
            }
                
            return pageRef;        
        }
        
        /**
        *Geração de PDF
        */
        public void generatePDF()
        {
              
        }

    	/**
    	 * Esse metodo e responsavel por cancelar a reserva criar pelo codigo
    	 * da renault (mesmo quando o orcamento e cancelado)
    	 */
        private void cancelarReservaVeiculo() {
            
            this.quoteDetailsVO = 
                VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, this.opportunityId);
            
            System.debug('quoteDetailsVO.vehicleBookingId' + quoteDetailsVO.vehicleBookingId);
            
            if(quoteDetailsVO.vehicleBookingId == null) {
                return;
            }
            VehicleBooking__c vbk = [ SELECT Id, Status__c
                                     FROM VehicleBooking__c
                                     WHERE Id =: quoteDetailsVO.vehicleBookingId];
            vbk.Status__c = 'Canceled';
            
            update vbk;
        }
    
        public PageReference cancelQuotation()
        {   
            //a data de validade nao existe?
            if(quoteDetailsVO.sObjQuote.ExpirationDate ==  null) {
                quoteDetailsVO.sObjQuote.ExpirationDate = Date.today();
            }
            
            quoteDetailsVO.status = 'Canceled';
            updateQuoteDetails();
            
            cancelarReservaVeiculo();
            
            return new PageReference('/apex/VFP11_Quote?Id=' + this.quoteDetailsVo.opportunityId);       
        }
                
        public Boolean getIsReadOnly()
        {
            return ((quoteDetailsVO.status == 'Canceled')  ||
                    (quoteDetailsVO.oppStatus == 'Lost')   ||
                    (quoteDetailsVO.oppStatus == 'Billed') ||
                    (quoteDetailsVO.oppStatus == 'Order'));
        }
		
		public void checkCDC()
        {
            quoteDetailsVO.lsgFinancing = false;
        }
        
        public void checkLSG()
        {
            quoteDetailsVO.cdcCFinancing = false;
        }
        
        /*      
        * get Brand values from Molicar object and add those values to visualforce page picklist.
        * @return option - return the result and to store values form record                     
        */
        public List<Selectoption> getBrands()
        {
                /* initialize the SelectOption to store values form record */
                List<SelectOption> option = new List<SelectOption>();
                /* Add none value to the picklist option */
                option.add(new SelectOption('', ''));
                
                /* check returned brands list was not empty */
                if(!brandsDetailList.isEmpty())
                {
                        // Hard coded top 10 list
                        option.add(new SelectOption('RENAULT', 'RENAULT'));
                        option.add(new SelectOption('CHEVROLET', 'CHEVROLET'));
                        option.add(new SelectOption('CITROEN', 'CITROEN'));
                        option.add(new SelectOption('FIAT', 'FIAT'));
                        option.add(new SelectOption('FORD', 'FORD'));
                        option.add(new SelectOption('GMC', 'GMC'));
                        option.add(new SelectOption('HONDA', 'HONDA'));
                        option.add(new SelectOption('HYUNDAI', 'HYUNDAI'));
                        option.add(new SelectOption('PEUGEOT', 'PEUGEOT'));
                        option.add(new SelectOption('TOYOTA', 'TOYOTA'));
                        option.add(new SelectOption('VOLKSWAGEN', 'VOLKSWAGEN'));

                        /* Iterate the Brand list to get sigle values */
                        for(String molicar : brandsDetailList){
                                /* add iterated value to the option */
                                option.add(new SelectOption(molicar, molicar));
                        }
                }
                /* Return Option */
                return option;
        }
        
        /*      
        * get Model values from Molicar object 
        * @return option - return the molicar  model result and to store values form record
        */
        public List<Selectoption> getModels()
        {
                /* Initialize the selectOption to store options */
                List<SelectOption> option = new List<SelectOption>();
                /* Add none values to the option to show in picklist. */
                option.add(new SelectOption('', ''));
                /* check Brand name must not be null and the model names list also not equalto null*/
                if((selectedBrand != null || selectedBrand != '') && modelDetailList != null)
                {
                        /* check model details list not equalto empty */
                        if(!modelDetailList.isEmpty())
                        {
                                /* Iterate Model details list to sigle record */
                                for(String model : this.modelDetailList)
                                {
                                        /* Add iterared values to the option */
                                        option.add(new SelectOption(model, model));
                                }
                        }
                }
                return option;
        }
        
        /* 
        * get Brand  values from Molicar object using selected brand 
        * @return - dont have any value return 
        */
        public void getBrandDetails(){
                brandsDetailList = new List<String>();
                /* get Brand values from Molicar object */
                AggregateResult[] agrMolicarBrandDetails = VFC11_MolicarDAO.getInstance().findMolicarBrand();
                /* check agrMolicarBrandDetails was not empty */
                if(!agrMolicarBrandDetails.isEmpty())
                {
                        /* iterate agrMolicarBrandDetails list */
                        for(AggregateResult Brand : agrMolicarBrandDetails)
                        {
                                /* To avoid duplication add Brand values to set list of Brands. */
                                        brandsDetailList.add(String.valueOf(Brand.get('Brand__c')));
                                }
                        }
                        brandsDetailList.sort();
                }

                /* 
        * get Model values from Molicar object using selected Id 
        * @return - dont have any value return 
        */
        public void getModelDetails(){
                
                this.modelDetailList = new List<String>();
                /* create the new list of Molicar object */
                List<MLC_Molicar__c> lstMolicarModels = new List<MLC_Molicar__c>();
                /* get Brand values from Molicar object using selected Brand Id*/
                lstMolicarModels = VFC11_MolicarDAO.getInstance().findMolicarByBrand(this.selectedBrand);
                /*check lstMolicarModels was not empty */
                if(!lstMolicarModels.isEmpty())
                {
                        for(MLC_Molicar__c molicar : lstMolicarModels)
                        {
                                /* add the molicar model values in list */
                                this.modelDetailList.add(molicar.Model__c);
                        }
                }
        }

        /* 
        * 
        */ 
        public PageReference getValueTraded()
        {
            PageReference pageRef = null;
            ApexPages.Message message = null;
            Decimal unitPrice = 0.0;
            List<VFC83_QuoteLineItemVO>  lstQuoteLineItem = new List<VFC83_QuoteLineItemVO>();
            
            for(VFC83_QuoteLineItemVO quoteLineItem : this.quoteDetailsVO.lstQuoteLineItemVO)
            {
                unitPrice = VFC69_Utility.convertPrice(quoteLineItem.unitPriceMask);

                if(unitPrice != quoteLineItem.unitPrice)
                {
                    lstQuoteLineItem.add(quoteLineItem);                    
                }
            }
            
            try
            {
                VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteItemsDetails(lstQuoteLineItem); 
                initialize();
                
                System.debug('>>>> quoteDetailsVo.totalPrice'+quoteDetailsVo.totalPrice);                     
            }
            catch(Exception ex)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());     
                ApexPages.addMessage(message);
            }                               

            return null;    
        } 
        
        /* 
        * 
        */ 
        public PageReference redirectToNewAccessory()
        {
        	ApexPages.Message message = null;
        	List<MLC_Molicar__c> lstMolicarDetId = null;
        	
            try
            {
	            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
	            
	            if(!lstMolicarDetID.isEmpty())
	            {
	                quoteDetailsVO.sObjQuote.UsedVehicle__c = lstMolicarDetID[0].Id;            
	            }
	                        	
                VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);
   				 
                return new PageReference('/apex/VFP17_AddAccessory?Id='+quoteDetailsVO.Id); 
                 
            }
            catch(Exception ex)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                        
                ApexPages.addMessage(message);
            }  
                      	
        	return null;
        }

        /* 
        * 
        */         
        public PageReference redirectToNewVehico()
        {
        	ApexPages.Message message = null;
        	List<MLC_Molicar__c> lstMolicarDetId = null;
        	
            try
            {
	            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
	            
	            if(!lstMolicarDetID.isEmpty())
	            {
	                quoteDetailsVO.sObjQuote.UsedVehicle__c = lstMolicarDetID[0].Id;            
	            }
	                        	
                VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);
   
                return new PageReference('/apex/VFP16_NewVehicle?Id='+quoteDetailsVO.Id);                   
            }
            catch(Exception ex)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                        
                ApexPages.addMessage(message);
            }  
                      	
        	return null;	
        } 

        /* 
        * 
        */         
        public PageReference redirectToNewAccessoryEdit()
        {
        	ApexPages.Message message = null;
        	List<MLC_Molicar__c> lstMolicarDetId = null;
        	String quoteLineItem = Apexpages.currentPage().getParameters().get('quoteLineItem');
        	
            try
            {
	            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
	            
	            if(!lstMolicarDetID.isEmpty())
	            {
	                quoteDetailsVO.sObjQuote.UsedVehicle__c = lstMolicarDetID[0].Id;            
	            }  
	                      	
                VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);
   
                return new PageReference('/apex/VFP17_AddAccessory?Id='+quoteDetailsVO.Id+'&quoteLineItemId='+quoteLineItem+'&action=edit');                    
            }
            catch(Exception ex)
            {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                        
                ApexPages.addMessage(message);
            }  
                      	
        	return null;	
        }                              
}