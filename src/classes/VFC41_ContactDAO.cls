/**
* Classe responsável pela manipulação dos dados do objeto Contact.
* @author Felipe Jesus Silva.
*/
public class VFC41_ContactDAO extends VFC01_SObjectDAO
{
	private static final VFC41_ContactDAO instance = new VFC41_ContactDAO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC41_ContactDAO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC41_ContactDAO getInstance()
	{
		return instance;
	}
	
	/**
	* Localiza contatos na base de dados mediante ids de contas.
	* @param setAccountId Um set contendo os ids das contas.
	* @return Uma lista de contatos.
	*/
	public List<Contact> findByAccountId(Set<String> setAccountId)
	{
		List<Contact> lstSObjContact = [SELECT AccountId, 
											   Id
		                                FROM Contact
		                                WHERE AccountId IN : setAccountId];
		                                
		return lstSObjContact;
	}
	
	/**
	* Localiza contatos na base de dados mediante o id da conta e o tipo de contato.
	* @param accountId O id da conta.
	* @param contactType O tipo de contato.
	* @return Uma lista de contatos ordenados pelo campo Name.
	*/
	public List<Contact> findByAccountIdAndContactType(String accountId, String contactType)
	{
		List<Contact> lstSObjContact = [SELECT Id,
		                                       Name
		                                FROM Contact
		                                WHERE AccountId =: accountId
		                                AND ContactType__c =: contactType];
		                                
		return lstSObjContact;
	}
}