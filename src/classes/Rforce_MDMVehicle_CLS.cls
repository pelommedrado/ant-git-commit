/**
 * @author: Vetrivel Sundararajan
 * @date: 07/08/2014
 * @description: This apex class is a POJO class which will contains the Vehicle values of Search Party.
*/

public class Rforce_MDMVehicle_CLS {
    
    public String vin {get;set;}
    public String IdClient{get;set;}
    public String brandCode {get;set;}
    public String modelCode {get;set;}
    public String modelLabel {get;set;}
    public String versionLabel {get;set;}
    public String firstRegistrationDate{get;set;}
    public String registration{get;set;}
    public String possessionBegin{get;set;}
    public String vehicleType{get;set;}
    public String startDate{get;set;}
    public String endDate{get;set;}
    public String firstName{get;set;}
    public String lastName{get;set;}  
 }