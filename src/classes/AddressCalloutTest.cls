@isTest(SeeAllData=true)
private class AddressCalloutTest {
	
	static testMethod void unitTest04() {

        test.startTest();

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('correiostest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController([SELECT Id FROM Quote LIMIT 1]);
        
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);

        ct.initializeQuote();
        ct.quoteVO.accountZipCode = '09820220';
        ct.getAddressGivenZipCode();

        ct.quoteVO.billPJZipCode = '09820220';
        ct.getAddressGivenZipCodeCNPJ();

        ct.quoteVO.billZipCode = '09820220';
        ct.getAddressGivenZipCodeCPF();
        
        ct.getStates();
        ct.useExistBillingAccount = true;
        ct.useAccountInfo();

        test.stopTest();
    }
    
    static testMethod void testVFC19() {

        test.startTest();

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('correiostest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController([SELECT Id FROM Account LIMIT 1]);
        
        VFC19_AccountCustomerActivity ct = new VFC19_AccountCustomerActivity(stdCont);

        ct.objCustomerActivityEntityForm.leadInfo.PostalCode__c = '09820220';
        ct.getAddressGivenZipCode();

        ct.objCustomerActivityEntityForm.leadInfo.RefAddressPostalCode__c = '09820220';
        ct.getAddressGivenZipCodeRef();

        test.stopTest();
    }
    
    static testMethod void testVFC04() {

        test.startTest();

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('correiostest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController([SELECT Id FROM Lead LIMIT 1]);
        
        VFC04_CustomerActivity_v2 ct = new VFC04_CustomerActivity_v2(stdCont);

        ct.objCustomerActivityEntityForm.leadRecord.PostalCode__c = '09820220';
        ct.getAddressGivenZipCode();

        ct.objCustomerActivityEntityForm.leadRecord.RefAddressPostalCode__c = '09820220';
        ct.getAddressGivenZipCodeRef();

        test.stopTest();
    }
	
}