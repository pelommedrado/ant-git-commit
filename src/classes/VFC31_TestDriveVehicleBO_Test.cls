/**
*	Class	-	VFC31_TestDriveVehicleBO_Test
*	Author	-	RameshPrabu
*	Date	-	16/11/2012
*
*	#01 <RameshPrabu> <16/11/2012>
*		This test class called for VFC31_TestDriveVehicleBO
**/
@isTest 
public with sharing class VFC31_TestDriveVehicleBO_Test {
	static testMethod void VFC31_TestDriveVehicleBO_Test() {
		List<VEH_Veh__c> lstVehs = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
       	List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
       	List<TDV_TestDriveVehicle__c> lstTestDriveVehicle =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
       	try{
       		List<TDV_TestDriveVehicle__c> lstTestDriveVehicle1 =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
       	}catch(System.DMLException e){
       		String errorMessage = Label.ERRTDVMessage1;
          	System.assert(e.getMessage().contains(errorMessage),e.getMessage());
      	}
       	List<TDV_TestDriveVehicle__c> lstUpdateTestDriveVeh = new List<TDV_TestDriveVehicle__c>();
       	for(TDV_TestDriveVehicle__c updateTestDriveVeh : lstTestDriveVehicle){
       		updateTestDriveVeh.AgendaOpeningDate__c = system.today()+ 31;
    		updateTestDriveVeh.AgendaClosingDate__c = system.today() + 60;
    		lstUpdateTestDriveVeh.add(updateTestDriveVeh);
       	}
       	try{
       		VFC27_TestDriveVehicleDAO.getInstance().updateData(lstUpdateTestDriveVeh);
       	}catch(System.DMLException e){
       		String errorMessage = Label.ERRTDVMessage1;
          	System.assert(e.getMessage().contains(errorMessage),e.getMessage());
      	}
       	
	}
}