/**
* Classe que representa exceções geradas na deleção de item do orçamento.
* @author Christian Ranha.
*/
public with sharing class VFC90_DeleteQuoteLineItemException extends Exception {

	public VFC90_DeleteQuoteLineItemException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}