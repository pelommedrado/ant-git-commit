public class VeiculoRecallListController {
    
    public List<Recall_Relation__c> recallList  { get;set; }
    public String acaoId 					{ get;set; }
    
    public VeiculoRecallListController(ApexPages.StandardController stdController) {
        acaoId = stdController.getId();
        atualizar();
    }
    
    public void atualizar() {
		
        String veiculoId = acaoId;
        
        List<Case> caseList = [
            SELECT ID, VIN__c
            FROM Case
            WHERE Id=:acaoId
        ];
        
        if(!caseList.isEmpty()) {
            Case caso = caseList.get(0);
            
            System.debug('Caso: ' + caso);
            
            if(!String.isEmpty(caso.VIN__c)) {
                
                VEH_Veh__c veiculo = [
                    SELECT ID
                    FROM VEH_Veh__c
                    WHERE Id =: caso.VIN__c
                ];
                
                if(veiculo != null) {
                    veiculoId = veiculo.Id;    
                }
                
            }
        }
        
        recallList = [
            SELECT Id, Technical_Reasons__c, Solution__c, Risk__c, Recall_Name__c, Status__c
            FROM Recall_Relation__c
            WHERE Vehicle__c =: veiculoId
        ];
    }
    
    public PageReference clienteInformado() {
        
        String Iid = Apexpages.currentPage().getParameters().get('Id');
        String recallId = Apexpages.currentPage().getParameters().get('recallId');
        
        System.debug('Iid: ' + Iid);
        System.debug('recallId: ' + recallId);
        
        if(!String.isEmpty(recallId)) {
            
            Recall_Relation__c recal = new Recall_Relation__c(Id= recallId);
            recal.Status__c = 'Yes';
            
            UPDATE recal;
        }
        
        atualizar();
        
        return null;
    }
}