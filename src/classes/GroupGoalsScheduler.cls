global class GroupGoalsScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc){
        GroupGoalsBatch ggb = new GroupGoalsBatch();
        Database.executeBatch(ggb);
        
        /*
         * GroupGoalsScheduler ggs = new GroupGoalsScheduler();
		 * System.schedule('Group Goal Scheduler', 0 0 1 1 * ?, ggs);
         * 
         * */
        
    }

}