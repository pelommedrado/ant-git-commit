/**
*	Class	-	VFC29_ContactDAO
*	Author	-	Surseh Babu
*	Date	-	30/10/2012
*
*	#01 <Suresh Babu> <30/10/2012>
*		DAO class for Test Drive object to Query records.
**/
public with sharing class VFC29_ContactDAO extends VFC01_SObjectDAO{
	private static final VFC29_ContactDAO instance = new VFC29_ContactDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC29_ContactDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC29_ContactDAO getInstance(){
        return instance;
    }
    
    /**
    * 	This Method was used to get Product Record 
    * 	@return	lstContactRecords - fetch and return the List of Contact Records from Contact object.
    */
    public List<Contact> fetchContactRecordsUsingCriteria(){
    	List<Contact> lstContactRecords = null;
    	
    	lstContactRecords = [SELECT 
    							AccountId, Id,  Name, ContactType__c  
    						FROM 
    							Contact
    						WHERE
    							ContactType__c IN  ('Seller' , 'Receptionist')
    						];
		return lstContactRecords;
    }
    
    /**
    * 	This Method was used to get Contact Record 
    * 	@param	AccountId	-	accountId to criteria for query.
    * 	@return	contactRecord - fetch and return the Contact Record using Account Id.
    */
    public Contact fetchContactUsingAccountId( Id AccountId ){
    	Contact contactRecord = new Contact();
    	
    	contactRecord = [SELECT 
    						c.AccountId, c.Id, c.Name
    					FROM
    						Contact c
    					WHERE
    						c.AccountId =: AccountId
    						limit 1
    					];
    	return contactRecord;
    }
    
    /**
    * 	This Method was used to get Contact Record 
    * 	@param	lstContactRecords	-	list of Dealer Ids to fetch contacts.
    * 	@return	lstContactRecords - fetch and return the Contact Records using Account Ids.
    */
    public List<Contact> fetchContactRecords_UsingDealers( List<Id> AccountIDs ){
    	List<Contact> lstContactRecords = new List<Contact>();
    	
    	lstContactRecords = [SELECT 
    								c.AccountId, c.Id, c.Name, c.Email, c.Phone, c.CPF__c
    							FROM
    								Contact c
    							WHERE
    								c.AccountId IN : AccountIDs
    						];
    	return lstContactRecords;
    }
}