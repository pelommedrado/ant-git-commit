/*****************************************************************************************
    Name            : Rforce_CommercialOffersParsing_CLS
    Description     : This apex class is to parse the Commercial offers response xml and return as List to calling function.
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Imnplemented By : Ashok Muntha
 
******************************************************************************************/
public class Rforce_CommercialOffersParsing_CLS {


 public List<Rforce_CommercialOffersResponse_CLS> parseCommercialOffers(String strXML) {

    system.debug('## Inside parseCommercialOffers method ##');

    String strDesMkt='';
    String strDeskEli='';
    String strDeskLeg='';
    String strPictureGen='';
    String strPictureDetail='';
    
    String strPropID='';
    String strPropOfferID='';
    String strPropOfferSpaceID='';
    
    String strPropWeight='';
    Integer intPropWeight;
    String strPropRank='';
    String strPropCode='';
    String strPropLabel='';
    String strPropStartDate='';
    String strPropEndDate='';
    String strValidityDate='';
    String strUsedOffer='';
    String strBrandListID='';
    
    String strCategoryType='';
    String strContextWeight='';
    String strNewOffer='';
     
    Rforce_CommercialOffersResponse_CLS commercialOffersObj;
    List<Rforce_CommercialOffersResponse_CLS> commercialOffers = new List<Rforce_CommercialOffersResponse_CLS>();
   
   /*   
    Rforce_CommercialOffers_WS obj = new Rforce_CommercialOffers_WS();
    Rforce_CommercialOffersPropose_CLS obj1 = new Rforce_CommercialOffersPropose_CLS();
    strXML = obj.getResponse('getEmailOffers',obj1);
     
    system.debug('## value for strXML is..::'+ strXML);
   */  
   
  try {
     system.debug('## Using with Dom.Document ##');
     
     Rforce_CustomXMLDom parser=new Rforce_CustomXMLDom();
     parser.parseFromString(strXML);
    system.debug('xmllllllllllll'+ strXML);
     
     Rforce_CustomXMLDom.Element propositionsElement=parser.getElementByTagname('propositions');
     List<Rforce_CustomXMLDom.Element> propositionElements=propositionsElement.getElementsByTagName('proposition');
     
     if (propositionElements !=null && propositionElements.size() > 0) {
         
         for (Integer i=0; i<propositionElements.size(); i++) {
             
             //commercialOffersObj = new Rforce_CommercialOffersResponse_CLS();
             
             strPropID            =    propositionElements[i].getAttribute('id')==null ? '':propositionElements[i].getAttribute('id');
             strPropOfferID       =    propositionElements[i].getAttribute('offer-id') == null ? '':propositionElements[i].getAttribute('offer-id');
             strPropOfferSpaceID  =    propositionElements[i].getAttribute('offerSpace-id') == null ? '':propositionElements[i].getAttribute('offerSpace-id');
             strPropWeight        =    propositionElements[i].getAttribute('weight') == null ? '': propositionElements[i].getAttribute('weight');
             if(strPropWeight != null){
             intPropWeight        =    Integer.valueOf(strPropWeight);
             }
             strPropRank          =    propositionElements[i].getAttribute('rank') == null ? '': propositionElements[i].getAttribute('rank');
             strPropCode          =    propositionElements[i].getAttribute('code') == null ? '': propositionElements[i].getAttribute('code');
             strPropLabel         =    propositionElements[i].getAttribute('label') == null ? '': propositionElements[i].getAttribute('label');
             strPropStartDate     =    propositionElements[i].getAttribute('startDate') == null ? '': propositionElements[i].getAttribute('startDate');
             strPropEndDate       =    propositionElements[i].getAttribute('endDate') == null ? '': propositionElements[i].getAttribute('endDate');
             strValidityDate      =    propositionElements[i].getAttribute('validityDate') == null ? '': propositionElements[i].getAttribute('validityDate');
             strUsedOffer         =    propositionElements[i].getAttribute('usedOffer') == null ? '': propositionElements[i].getAttribute('usedOffer');
             strBrandListID       =    propositionElements[i].getAttribute('idBrandList_off') == null ? '': propositionElements[i].getAttribute('idBrandList_off');
         
             strDesMkt     = propositionElements[i].getElementByTagname('desMkt').nodevalue;
             strDeskEli    = propositionElements[i].getElementByTagname('deskEli').nodevalue;
             strDeskLeg    = propositionElements[i].getElementByTagname('deskLeg').nodevalue;
             strPictureGen = propositionElements[i].getElementByTagname('pictureGen').nodevalue; 
             strPictureDetail=propositionElements[i].getElementByTagname('pictureDetail').nodevalue;  
             
             strNewOffer   = propositionElements[i].getElementByTagname('newOffer').nodevalue;  
             
             strPropStartDate    =    dateFormat(strPropStartDate);
             strPropEndDate      =    dateFormat(strPropEndDate);         
             strValidityDate     =    dateFormat(strValidityDate); 
             
             /*
             commercialOffersObj.strPropositionID=strPropostionID;
             commercialOffersObj.strPropositionOfferID =strPropositionOfferID;
             commercialOffersObj.strPropositionOfferSpaceID=strPropositionOfferSpaceID;
             commercialOffersObj.strPropositionWeight=strPropositionWeight;
             commercialOffersObj.strPropositionRank=strPropositionRank;
             commercialOffersObj.strPropositionCode=strPropositionCode;
             commercialOffersObj.strPropositionLabel=strPropositionLabel;
             commercialOffersObj.strPropositionStartDate=strPropositionStartDate;
             commercialOffersObj.strPropositionEndDate=strPropositionEndDate;
             commercialOffersObj.strValidityDate=strValidityDate;
             commercialOffersObj.strUsedOffer=strUsedOffer;
             commercialOffersObj.strBrandListID=strBrandListID;
             
             //commercialOffersObj.strDesMkt=strDesMkt;
             //commercialOffersObj.strDeskEli=strDeskEli;
             //commercialOffersObj.strDeskLeg=strDeskLeg;
             //commercialOffersObj.strPictureGen=strPictureGen;
             //commercialOffersObj.strPictureDetail=strPictureDetail;
             //commercialOffersObj.strNewOffer=strNewOffer;
             
             
             if (strPictureDetail!=null && strPictureDetail.length() > 0) {
                 commercialOffers.add(commercialOffersObj);
             } 
             */
             
             commercialOffersObj = new Rforce_CommercialOffersResponse_CLS(strDesMkt,strDeskEli,strDeskLeg,strPictureGen,strPictureDetail,strNewOffer,
                                         strPropID,strPropOfferID,strPropOfferSpaceID,strPropWeight,strPropRank,strPropCode,strPropLabel,strPropStartDate,strPropEndDate,strValidityDate,strUsedOffer,strBrandListID,intPropWeight);
             
              if (strPictureDetail!=null && strPictureDetail.length() > 0) {
                 commercialOffers.add(commercialOffersObj);
             } 
         }
     }
     
    }catch (Exception ex) {
          system.debug('Rforce_CommercialOffersParsing_CLS-->METHOD:parseCommercialOffers() Exception ::'+ ex.getMessage());
   }  
          system.debug('## commercialOffers is..::'+ commercialOffers);
          return commercialOffers;
    }
    
 
 public String parsePushEvents(String strXML) {
 
     system.debug('## Inside parsePushEvents ##');
     String strPushReponseID='';    
     
     try {
         Rforce_CustomXMLDom parser = new Rforce_CustomXMLDom();
         parser.parseFromString(strXML);               
         strPushReponseID=parser.getValueByTagName('plId');
     
     }catch (Exception ex) {
             system.debug('Rforce_CommercialOffersParsing_CLS-->METHOD:parsePushEvents() Exception ::'+ ex.getMessage());
     }
       return strPushReponseID;
 }
    
    public String dateFormat(String strDate) {
        
        String dobDateFormat='';               
        String newDateFormat='';
        
        if (strDate !=null && strDate.length() >0) {
            Date dt = Date.valueOf(strDate); 
            dobDateFormat  = String.valueOf(dt);                   
            DateTime dtTime= Date.valueOf(dobDateFormat);                   
            newDateFormat  = dtTime.format('dd/MM/yyyy');   
        }
        return newDateFormat;
    }


     
 }