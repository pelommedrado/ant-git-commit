// Test Class for LeadToOpportunity - Reena Jeyapal - 01/Nov/2013
@isTest
public with sharing class TestLeadToOpportunity_Extn   
{
    static testMethod void testLeadOpp_Extn()
    {
  
    try{
    
      Id RecTypenw = [select Id from RecordType where sObjectType='Account' and Name='Network Site Account' limit 1].Id;
     Account Acc = new Account(Name='Test1',IDBIR__c='234234',Phone='1000',RecordTypeId=RecTypenw, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
     Id LeadRecordId= [select Id from RecordType where sObjectType='Lead' and Name='Lead Core RecType'].Id;
     
    
     Id oppRecordId= [select Id from RecordType where sObjectType='opportunity' and Name='CCBIC Core'].Id;
      Test.startTest(); 
    Lead le = new Lead(firstname='Reena',lastname='jeyapal',VehicleOfInterest__c='Sandero',DealerOfInterest__c=Acc.Id,TypeOfInterest__c='VO',IntentToPurchaseNewVehicle__c='2 Months',SubType_Of_Interest__c='After Sales Request',Status='Identified',RecordTypeId=LeadRecordId);
    insert le;
    opportunity opp=new Opportunity(name='ree',StageName='Identified',AccountId=Acc.Id,CloseDate=date.parse('02/04/2013'),RecordTypeId=oppRecordId);
    insert opp;
     ApexPages.currentPage().getParameters().put('Criteria','LMT');
     ApexPages.currentPage().getParameters().put('Id',le.Id);
     ApexPages.StandardController controller1=new ApexPages.StandardController(le);
    LeadToOpportunity_Extn tst1=new LeadToOpportunity_Extn(controller1);
    tst1.opportunityRedirectPage();
    System.debug('le**********'+le);
    System.debug('opp**********'+opp);
    le.Converted__c=false;
    le.Status='Portfolio';
    le.Sent_To_BCS__c=true;
    update le;
   
      ApexPages.currentPage().getParameters().put('Criteria','BCS');
    ApexPages.StandardController controller2=new ApexPages.StandardController(le);
    LeadToOpportunity_Extn tst2=new LeadToOpportunity_Extn(controller2);
    tst2.opportunityRedirectPage();
    }
    catch(Exception e){}
    Test.StopTest();
    
    }
    }