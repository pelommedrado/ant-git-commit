/**
 * Test Class for Account Archive Cases
 */
@isTest
private class Rforce_AccountAttributes_Test {

    static User getEnvSetup(){
        // Get the context of a user
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
          User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
          LocaleSidKey='fr', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com');   
          return u;
    }
 
    
        static testMethod void test_getArchives(){
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR',Country_Code_3L__c = 'FRA', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
         System.runAs(usr) {
         
        Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
//        Account acc = new Account(name='1 TEMPS DARET', ShippingCity='MARSEILLE 02', RecordTypeId = RTID_COMPANY,ShippingPostalCode='13002',Bcs_Id__c='123', ProfEmailAddress__c='tester@tester.fr');
 Account acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '+123456', HomePhone__c='+23456',RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', Bcs_Id__c='123',ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Country__c='France');
      
        insert acc;
        
        PageReference pageRef = Page.Rforce_AccountAttributes;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('IdCase','1-244507361');
        pageRef.getParameters().put('country','FRA');
        pageRef.getParameters().put('attachmentName','S_ACTIVITY_ATT_1-41KY13_1-IBAL8.SAF');
        pageRef.getParameters().put('contentType','docx');
        
        // Get ready to run the methods
        Rforce_AccountAttributes accController = new Rforce_AccountAttributes(new ApexPages.StandardController(acc));
        
        accController.sToParse ='<test><SerialNumber2>123</SerialNumber2></test>';
         //vehController.toParse='<SiebelMessage><FirstName>AA</FirstName><ListOfAction><Action><Type2>AA</Type2><SerialNumber2>123</SerialNumber2></Action></ListOfAction></SiebelMessage>';
        Test.startTest();
        Test.setMock(WebServiceMock.class, new Rforce_RcArch_WebServiceMock_Test());       
        

        accController.IdCase='123';
        accController.getArchives();
        accController.getCaseDetailXML();
        accController.getCaseDetails();
        accController.getAttachements();
        
        accController.getAttachementData();
  
        PageReference FinalPage=accController.OpenXMLPage();
        System.assertEquals('1 TEMPS DARET',accController.getAccount());
        Test.stopTest();
        }
    }
 
  static testMethod void test_dacia_getArchives(){
Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR',Country_Code_3L__c = 'FRA', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
         System.runAs(usr) {
          
        Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
//        Account acc = new Account(name='1 TEMPS DARET', ShippingCity='MARSEILLE 02', RecordTypeId = RTID_COMPANY,ShippingPostalCode='13002',Bcs_Id__c='123', ProfEmailAddress__c='tester@tester.fr');
 Account acc = new Account(FirstName = 'Test1', LastName = 'Acc', ProfLandLine__c = '+00234567', HomePhone__c='+23456',RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', Bcs_Id_Dacia__c='1243',BillingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', BillingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Country__c='France');
      
        insert acc;
        
        PageReference pageRef = Page.Rforce_AccountAttributes;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('IdCase','1-244507361');
        pageRef.getParameters().put('country','FRA');
        pageRef.getParameters().put('attachmentName','S_ACTIVITY_ATT_1-41KY13_1-IBAL8.SAF');
        pageRef.getParameters().put('contentType','docx');
        
        // Get ready to run the methods
        Rforce_AccountAttributes accController = new Rforce_AccountAttributes(new ApexPages.StandardController(acc));
        
        accController.sToParse ='<test><SerialNumber2>123</SerialNumber2></test>';
         //vehController.toParse='<SiebelMessage><FirstName>AA</FirstName><ListOfAction><Action><Type2>AA</Type2><SerialNumber2>123</SerialNumber2></Action></ListOfAction></SiebelMessage>';
        Test.startTest();
        Test.setMock(WebServiceMock.class, new Rforce_RcArch_WebServiceMock_Test());
        
       

        accController.IdCase='123';
        accController.getArchives();
        accController.getCaseDetailXML();
        accController.getCaseDetails();
        accController.getAttachements();
        
        accController.getAttachementData();
  
        PageReference FinalPage=accController.OpenXMLPage();
        System.assertEquals('1 TEMPS DARET',accController.getAccount());
        Test.stopTest();
        }
    }
static testMethod void test_acc_getArchives(){
Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR',Country_Code_3L__c = 'FRA', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
       
       User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
         System.runAs(usr) {
         
         Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='CORE_ACC_Company_Account_RecType' limit 1].Id;
    Account acc = new Account(Name='Test1',ProfMobPhone__c='+23456',RecordTypeId=RTID_COMPANY,Bcs_Id__c='12345',ProfEmailAddress__c = 'addr1@mail.com',Country__c='France');
    insert acc;
        PageReference pageRef = Page.Rforce_AccountAttributes;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('IdCase','1-244507361');
        pageRef.getParameters().put('country','FRA');
        pageRef.getParameters().put('attachmentName','S_ACTIVITY_ATT_1-41KY13_1-IBAL8.SAF');
        pageRef.getParameters().put('contentType','docx');
        
        // Get ready to run the methods
        Rforce_AccountAttributes accController = new Rforce_AccountAttributes(new ApexPages.StandardController(acc));
        
        accController.sToParse ='<test><SerialNumber2>123</SerialNumber2></test>';       
        Test.startTest();
        Test.setMock(WebServiceMock.class, new Rforce_RcArch_WebServiceMock_Test());     
        
        accController.IdCase='123';
        accController.getArchives();
        accController.getCaseDetailXML();
        accController.getCaseDetails();
        accController.getAttachements();        
        accController.getAttachementData();  
        PageReference FinalPage=accController.OpenXMLPage();
        System.assertEquals('1 TEMPS DARET',accController.getAccount());
        Test.stopTest();
        }
    }
 
 static testMethod void test_Comp_getArchives(){
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR',Country_Code_3L__c = 'FRA', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
       
         User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
         System.runAs(usr) {
             
             Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='CORE_ACC_Company_Account_RecType' limit 1].Id;
             Account acc = new Account(Name='Test1',PersMobPhone__c='+23456',RecordTypeId=RTID_COMPANY,Bcs_Id_Dacia__c='1243',ProfEmailAddress__c = 'addr1@mail.com',Country__c='France');
             insert acc;
            PageReference pageRef = Page.Rforce_AccountAttributes;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('IdCase','1-244507361');
            pageRef.getParameters().put('country','FRA');
            pageRef.getParameters().put('attachmentName','S_ACTIVITY_ATT_1-41KY13_1-IBAL8.SAF');
            pageRef.getParameters().put('contentType','docx');
            
            // Get ready to run the methods
            Rforce_AccountAttributes accController = new Rforce_AccountAttributes(new ApexPages.StandardController(acc));        
            accController.sToParse ='<test><SerialNumber2>123</SerialNumber2></test>'; 
            Test.startTest();
            Test.setMock(WebServiceMock.class, new Rforce_RcArch_WebServiceMock_Test());
            
            accController.IdCase='123';
            accController.getArchives();
            accController.getCaseDetailXML();
            accController.getCaseDetails();
            accController.getAttachements();        
            accController.getAttachementData();  
            PageReference FinalPage=accController.OpenXMLPage();
            System.assertEquals('1 TEMPS DARET',accController.getAccount());
            Test.stopTest();
        }
        }
 
}