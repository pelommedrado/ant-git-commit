@isTest
private class Test_VFC04_VehicleData {

    @isTest static void test_method_one() {

        Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'Company_Acc' limit 1].Id;

        User usr = new User (LastName = 'Rotondo', alias = 'lro', Email = 'lrotondo@rotondo.com', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com', BypassVR__c = true, BypassWF__c = true);

        Account Acc = new Account(Name = 'Test1', Phone = '1000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
        Contact Con =
            new Contact(FirstName = 'Mandatory', LastName = 'Test Contact', Salutation = 'Mr.', ProEmail__c = 'lro@lro.com', AccountId = Acc.Id);
        insert Con;

        VEH_Veh__c v = new VEH_Veh__c (Name = '45678934567123456', VehicleBrand__c = 'Renault', DeliveryDate__c = date.parse('04/06/2010'));
        insert v;

        System.runAs(usr) {
            Test.startTest();
            ApexPages.StandardController controller1 = new ApexPages.StandardController(v);

            VFC04_VehicleData vd2 = new VFC04_VehicleData(controller1);

            vd2.getVehicleData();
            vd2.getVin();
            Test.stopTest();

        }
}
 


    }