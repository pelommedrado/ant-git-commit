/**
* @author Vinoth Baskaran(RNTBCI)
* @date 28/07/2015
* @description This Class is an Custom Soap service and it will create case and attachement accordding to the user input.
*/
global class Rforce_W2CService_CLS{

/**
* @author Vinoth Baskaran
* @date 28/07/2015
* @description It accepts the Casedetails from the external service.
* @param case Details
* @return String value according to request
*/
    public static boolean createCase(Rforce_W2CSchema_CLS.Casedetails caseDetails,Case caseDetailMapping){
    boolean bCaseStatus;   
      //VOCD
        if(caseDetails.SubSource ==System.label.Rforce_CAS_VOC_Dealer){ 
        bCaseStatus = Rforce_CreateCaseAttach_VOCD_CLS.createCase(caseDetails,caseDetailMapping);
        } 
        //VOCC
        if(caseDetailMapping.CaseSubSource__c ==System.label.RForce_VOC_Call_Center){ 
        bCaseStatus = Rforce_CreateCaseAttach_VOCC_CLS.createCase(caseDetails,caseDetailMapping);
        }                                       
        //HELIOS
        if(caseDetails.origin !='' && caseDetails.origin !=System.label.Rforce_CAS_VOCD_Origin){        
        bCaseStatus =Rforce_CreateCaseAttach_Helios_CLS.createCase(caseDetails,caseDetailMapping);                    
        } 
    return bCaseStatus;
    }   
    public static String getResponse(List<Attachment> attchmentList,Case caseDetailMapping,Integer iAttachCount){
    String sReturnValue;
        //Invoking VOCD Class to Get the attachmetn Response for both VOCD and VOCC
        if((caseDetailMapping.CaseSubSource__c ==System.label.Rforce_CAS_VOC_Dealer || caseDetailMapping.CaseSubSource__c==System.label.RForce_VOC_Call_Center) && attchmentList.size() >0)
        {
        sReturnValue= Rforce_CreateCaseAttach_VOCD_CLS.getResponse(attchmentList,caseDetailMapping,iAttachCount);   
        }  
        /*
         //Invoking VOCC Class to Get the attachmetn Response
        if(caseDetailMapping.CaseSubSource__c ==System.label.RForce_VOC_Call_Center && attchmentList.size() >0)
        {
        sReturnValue= Rforce_CreateCaseAttach_VOCD_CLS.getResponse(attchmentList,caseDetailMapping,iAttachCount);   
        } 
         */     
        //Invoking Helios Class to Get the attachmetn Response
        if(caseDetailMapping.origin != '' && caseDetailMapping.origin !=System.label.Rforce_CAS_VOCD_Origin && attchmentList.size()>0){          
        sReturnValue= Rforce_CreateCaseAttach_Helios_CLS.getResponse(attchmentList,caseDetailMapping,iAttachCount);   
        }     
    return sReturnValue;
    }     
        public static String getResponseWithoutAttach(Case caseDetailMapping,List<Attachment> attchmentList){
    String sReturnValue;
    
        //Invoking VOCD Class to Get the attachmetn Response for both VOCD and VOCC
        if((caseDetailMapping.CaseSubSource__c ==System.label.Rforce_CAS_VOC_Dealer || caseDetailMapping.CaseSubSource__c ==System.label.RForce_VOC_Call_Center) && attchmentList.size()==0)
        {
         sReturnValue= Rforce_CreateCaseAttach_VOCD_CLS.getResponseWithoutAttach(caseDetailMapping); 
        }  /* 
        //Invoking VOCD Class to Get the attachmetn Response
        if(caseDetailMapping.CaseSubSource__c ==System.label.RForce_VOC_Call_Center  && attchmentList.size()==0)
        {
         sReturnValue= Rforce_CreateCaseAttach_VOCD_CLS.getResponseWithoutAttach(caseDetailMapping); 
        }    
        */  
        //Invoking Helios Class to Get the attachmetn Response
        
        if(caseDetailMapping.origin !='' && caseDetailMapping.origin !=System.label.Rforce_CAS_VOCD_Origin && attchmentList.size()==0){          
        sReturnValue= Rforce_CreateCaseAttach_Helios_CLS.getResponseWithoutAttach(caseDetailMapping);   
        }     
    return sReturnValue;
    }  
    
        
}