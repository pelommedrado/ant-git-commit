global class DistrinetLogicCodeBatch implements Database.Batchable<sObject> {
	
	String query;
	String emailBody;
	public Boolean sendEmail;
	
	global DistrinetLogicCodeBatch() {
		
	}
	
	public List<DistrinetLogicCode__c> initialLogicCodeLst;
	
	global Iterable<SObject> start(Database.BatchableContext BC) {
		
		List<DistrinetLogicCode__c> ccoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> afcList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> dsfList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> acoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> mcoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> cl1List = new List<DistrinetLogicCode__c>();
		
		List<DistrinetLogicCode__c> finalLogicCodeLst = new List<DistrinetLogicCode__c>();
		
		String status = 'Not Integrated';
		
		query =  'SELECT ' +
				 'Id, ' +
				 'PC_LogicCode__c, ' +
				 'PC_CustomerNumberInternalSedre__c, ' +
				 'PC_ChassisNumberCurrent__c, ' +
				 'PC_SemiclairModel__c, ' +
				 'PC_SemiclairVersion__c, ' +
				 'PC_SemiclairOptional__c, ' +
				 'PC_ColorBoxCriterion__c, ' +
				 'PC_SemiclairUpholstered__c, ' +
				 'PC_SemiclairHarmony__c, ' +
				 'PC_SemiclairColor__c, ' +
				 'PC_FirstName__c, ' +
				 'PC_PersonalOrCompanyClientName__c, ' +
				 'PC_CustomerCompany__c, ' +
				 'PC_WayNome__c, ' +
				 'PC_City__c, ' +
				 'PC_State__c, ' +
				 'PC_PersonalPhone__c, ' +
				 'PC_MobilePhone__c, ' +
				 'PC_ErrorMessage__c, ' +
				 'PC_OrderNumberSedre__c, ' +
				 'PC_OwnerAccountControl__c, ' +
				 'PC_ExternalCustomerCodeSedre__c, ' +
				 'PC_DmsOrderNumber__c ' +
				 'FROM DistrinetLogicCode__c ' +
				 'WHERE Status__c =: status';
				 
		initialLogicCodeLst = Database.query(query);
		
		for(DistrinetLogicCode__c logicCode : initialLogicCodeLst){

			if(logicCode.PC_LogicCode__c == 'CCO')
				ccoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'MCO')
				mcoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'AFC')
				afcList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'DSF')
				dsfList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'ACO')
				acoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'CL1'){
				cl1List.add(logicCode);
			}
		}
		
		finalLogicCodeLst.addAll(ccoList);
		finalLogicCodeLst.addAll(afcList);
		finalLogicCodeLst.addAll(dsfList);
		finalLogicCodeLst.addAll(acoList);
		finalLogicCodeLst.addAll(mcoList);
		finalLogicCodeLst.addAll(cl1List);
				
		return finalLogicCodeLst; //Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<DistrinetLogicCode__c> logicCodeLst) {
   		
   		List<DistrinetLogicCode__c> ccoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> afcList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> dsfList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> acoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> mcoList = new List<DistrinetLogicCode__c>();
		List<DistrinetLogicCode__c> cl1List = new List<DistrinetLogicCode__c>();
		
		Map<Id, Quote> ccoListQuote = new Map<Id, Quote>();
		Map<Id, Quote> afcListQuote = new Map<Id, Quote>();
		Map<Id, Quote> dsfListQuote = new Map<Id, Quote>();
		Map<Id, Quote> acoListQuote = new Map<Id, Quote>();
		Map<Id, Quote> mcoListQuote = new Map<Id, Quote>();
		Map<Id, Quote> cl1ListQuote = new Map<Id, Quote>();
	
		Map<Id, Quote> mapQuoteToUpdate = new Map<Id, Quote>();
		List<Quote> lstQuoteToUpdate = new List<Quote>();
		
		Set<String> quoteNbrSet = new Set<String>();
		
		for(DistrinetLogicCode__c logicCode : logicCodeLst){
			//logicCode.Status__c = 'Integrated';
			quoteNbrSet.add(logicCode.PC_DmsOrderNumber__c);
		}
		
		for(DistrinetLogicCode__c logicCode : logicCodeLst){

			if(logicCode.PC_LogicCode__c == 'CCO')
				ccoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'MCO')
				mcoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'AFC')
				afcList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'DSF')
				dsfList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'ACO')
				acoList.add(logicCode);
			if(logicCode.PC_LogicCode__c == 'CL1'){
				cl1List.add(logicCode);
			}
		}
		
		//CCO, MCO, AFC, DSF, ACO, CL1
		
		
		if(!ccoList.isEmpty()){
			ccoListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(ccoList, quoteNbrSet, false);
			mapQuoteToUpdate.putAll(ccoListQuote);
			lstQuoteToUpdate.addAll(ccoListQuote.values());
		}
		if(!mcoList.isEmpty()){
			mcoListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(mcoList, quoteNbrSet, false);
			mapQuoteToUpdate.putAll(mcoListQuote);
			lstQuoteToUpdate.addAll(mcoListQuote.values());
		}
		if(!afcList.isEmpty()){
			afcListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(afcList, quoteNbrSet, false);
			mapQuoteToUpdate.putAll(afcListQuote);
			lstQuoteToUpdate.addAll(afcListQuote.values());
		}
		if(!dsfList.isEmpty()){
			dsfListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(dsfList, quoteNbrSet, false);
			mapQuoteToUpdate.putAll(dsfListQuote); 
			lstQuoteToUpdate.addAll(dsfListQuote.values());
		}
		if(!acoList.isEmpty()){
			acoListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(acoList, quoteNbrSet, false);
			mapQuoteToUpdate.putAll(acoListQuote);
			lstQuoteToUpdate.addAll(acoListQuote.values());
		}
		if(!cl1List.isEmpty()){
			cl1ListQuote = DistrinetLogicCodeUtils.getInstance().mapToQuoteDistrinet(cl1List, quoteNbrSet, true);
			//lstQuoteToUpdate.addAll(cl1ListQuote);
		}
		
		try{
		
			System.debug('@@@ lstQuoteToUpdate: ' + lstQuoteToUpdate);
		
			//Uspsert CCO, AFC, DSF, ACO, MCO
			Schema.SObjectField fieldId = Quote.Fields.External_Id__c;
			Database.UpsertResult[] sr = Database.upsert(lstQuoteToUpdate, fieldId, false);
			
			//Upsert CL1
			Database.UpsertResult[] sr2 = Database.upsert(cl1ListQuote.values(), false);
			
			
			Map<String, Quote> quoteError = new Map<String, Quote>();
			emailBody = 'Erros de integração com Distrinet: \n\n';
			sendEmail = false;
			
			for(Database.UpsertResult s : sr){
				if(!s.isSuccess()){
					
					System.debug('@@ debug dos primeiros!: ' + s.getErrors());
					System.debug('@@ mapQuoteToUpdate.get(s.getId()): ' + mapQuoteToUpdate.get(s.getId()));
				
					if(mapQuoteToUpdate.get(s.getId()) != null){
					
						Quote quote = new Quote();
						quote.Id = s.getId();
						quote.External_Id__c = mapQuoteToUpdate.get(s.getId()).PC_DmsOrderNumber__c;
						quote.PC_ErrorMessage__c = String.valueOf(s.getErrors()[0]);
						quoteError.put(quote.External_Id__c, quote);
						
						emailBody += 'Cotação: ' + s.getId() + '\nErro: ' + s.getErrors() + '\n\n';
						sendEmail = true;
					
					}
					
				}
				
			}
			
			for(Database.UpsertResult s : sr2){
				if(!s.isSuccess()){
					
					System.debug('@@ debug do CL1!');
				
					Quote quote = new Quote();
					quote.Id = s.getId();
					quote.External_Id__c = cl1ListQuote.get(s.getId()).PC_DmsOrderNumber__c;
					quote.PC_ErrorMessage__c = String.valueOf(s.getErrors()[0]);
					quoteError.put(quote.External_Id__c, quote);
					
					emailBody += 'Cotação: ' + s.getId() + '\nErro: ' + s.getErrors() + '\n\n';
					sendEmail = true;
					
				}
			}
			
			if(sendEmail){
				sendEmail(new String[] { 'landrade@kolekto.com.br', 'marcelino@kolekto.com.br' }, emailBody, 'Problemas de Integração com DISTRINET');
			}
			
			updateLogicCodeStatus(logicCodeLst, quoteError);
			Database.Update(quoteError.values());
		
		} catch(Exception e){
			sendEmail(new String[] { 'landrade@kolekto.com.br', 'marcelino@kolekto.com.br' }, 'Erro: ' + e.getMessage() + '\n' + e.getStackTraceString(), 'Erro no Batch de DISTRINET');
		}
	
	}
	
	global void finish(Database.BatchableContext BC) {
	}
	
	public void sendEmail(List<String> toAddresses, String body, String subject){
		
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.toAddresses = toAddresses;
		message.subject = subject;
		message.plainTextBody = body;
		
		Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
		
	}
	
	public void updateLogicCodeStatus(List<DistrinetLogicCode__c> logicCodeLst, Map<String, Quote> quoteError){
		
		for(DistrinetLogicCode__c logicCode : logicCodeLst){
			
			if(String.isNotEmpty(logicCode.PC_DmsOrderNumber__c) && quoteError.get(logicCode.PC_DmsOrderNumber__c) == null){
				logicCode.Status__c = 'Integrated';
			
			} else if(String.isEmpty(logicCode.PC_DmsOrderNumber__c) || quoteError.get(logicCode.PC_DmsOrderNumber__c) != null){
				logicCode.Status__c = 'Invalid';
			}
			
		}
		
		Database.Update(logicCodeLst);
		
	}
	 
}