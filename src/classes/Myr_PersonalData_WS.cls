/** SDP / 25.08.2014 
  * Apex Class which encapsulates the Personal data call
  **/
public with sharing class Myr_PersonalData_WS {
  
  /** Type of the personal data source (used in webservice response ) **/
  public enum DataSourceType {BCS, SIC, MDM}
  
  /** MDM interrogation mode: 
    MYRENAULT mode returns only one customer with its associated vehicles 
    NORMAL mode returns up to 20 customer without the vehicles. Then the getVehicles method should be called after 
  **/
  public enum CALL_MODE {MYRENAULT, RFORCE} 
  
  /** Datasource determined thanks to the getDataSource function and CountryInfo Custom Setting **/
  public class DataSource {
    DataSourceType mDSType;
    String mCountry; 
  }
  
  /** Possible modes for Personal Data Webservices **/
  public enum PDATA_MODE {
    MODE_0, //default mode
    MODE_1,
    MODE_2,
    MODE_3,
    MODE_4, //workshops description without bills || To be treated thanks to a transcoding table
    MODE_5,
    MODE_6,
    MODE_7, //dedicated Myr mode ?
    MODE_8 //Garanty extension and maintenance contracts
  }  
  
  /** Defining the parameters to be used for requesting the webservice **/ 
  public class RequestParameters {
  	public PDATA_MODE mode {get; set;}
    public User user {get; set;}
    public String vin {get;set;}
    public String registration {get;set;}
    public String country {get;set;}
    public String lastname {get;set;}
    public String firstname {get;set;}
    public String brand {get;set;}
    public String email {get;set;}
    public String city {get;set;}
    public String zip {get;set;}
    public String idClient {get;set;}	  //RBX or SIC identification number
    public String ident1 {get;set;}        //Customer Identification Number (e.g. fiscal code for Italy)
    public String strLocalID {get;set;}      //MDM
    public String strFixeLandLine {get;set;}    //MDM
    public String strMobile {get;set;}      //MDM
    public String strPartyID {get;set;}      //MDM
    public CALL_MODE callMode {get; set;}      //Call Mode :MyRenault or Rforce or something else (settings are different)
    public DataSourceType mDataSourceType;    //Test.isRunningTest purposes
    public Integer nbReplies {get; set;}
    public String demander {get; set;}
	public String user_x {get;set;}			//username of the salesforce user calling personal data => required by MDM
    //@default constructor
    public RequestParameters() {
    	mode = PDATA_MODE.MODE_7;
    	nbReplies = 10;
    	demander = 'PRODCCB';
    }
  }



	/** Use the implementation made by the Transverse Team
		@description Set the proper DataSource depending on the given country 
		@params String, the country
		@return the DataSource to use
	**/
	public static TRVRequestParametersWebServices.RequestParameters getProperDataSource(String country) {
		
		TRVRequestParametersWebServices.RequestParameters trvParams = new TRVRequestParametersWebServices.RequestParameters();
		trvParams.technicalCountry = country;
		trvParams.country = country;
		system.debug('### Myr_PersonalData_WS - <getProperDataSource> - Before get country info, trvParams=' + trvParams);
		trvParams = TRVWebservicesManager.getcountryInfo(trvParams);
		system.debug('### Myr_PersonalData_WS - <getProperDataSource> - trvParams.country=' + trvParams.country);
		return trvParams;
	}

	/** @return the answer of Bcs / Rbx webservice **/
	public static TRVResponseWebservices getPersonalData(TRVRequestParametersWebServices.RequestParameters params ) { 
		//Get the proper datasource from transverse team
		TRVRequestParametersWebServices.RequestParameters datasource = getProperDataSource(params.country);
		if( String.isBlank(datasource.mDataSourceType) ) return null;		
		//set the user_x => required by MDM
		params.user_x = UserInfo.getUsername();
		system.debug('### Myr_PersonalData_WS - <getPersonalData> - params.country=' + params.country + ', params.user_x=' + params.user_x);



		TRVResponseWebservices personalDataAnswer = new TRVResponseWebservices();
		
		//call the proper webservice
		if( datasource.mDataSourceType.equalsIgnoreCase('bcs') ){
			system.debug('#### Myr_PersonalData_WS - <getPersonalData> - Call BCS');
			
			//TRVWebservicesManager bcs = new TRVWebservicesManager();
			//set the proper country code
			
			if( Test.isRunningTest() ) 
			{
				system.debug('### Myr_PersonalDataSIC_Cls - <getPersonaData_BCS> - BCS is running test');
				
				//CHECK COUNTRY CODE
				/* SDP, 16.06.2015 / Useless as the check in test class is made on current user.RecordDefaultCountry (asynchronous method) and not on
				the user that triggers the asynchronous method (system.runAs) ....
				params = TRVWebservicesManager.getcountryInfo(params);
				String countryCode = params.country;
				system.debug('##Ludo country code : ' + params.country);
				List<Country_Info__c> countryInfo = [SELECT Id, Name FROM Country_Info__c WHERE Country_Code_2L__c = :countryCode];
				system.debug('### getPersonalData Myr_PersonalDataSIC_Cls - Nb of Countries found: CountryCode=' + countryCode + ', countries=' + countryInfo);
				system.assertEquals( 1, countryInfo.size());
				**/
				//CHECK COUNTRY CODE
				//AsyncTRVwsdlBcs mock = new AsyncTRVwsdlBcs();
				//Prepare the workaround due to the bug System.CalloutException: You have uncommitted work pending. Please commit or rollback before calling out in Winter16
		    	//concatenate the fieldsinto the test dedicated fields
		    	//Order is : lastname;firstname;email;custIdentNumber;purchaseNature;vehicleenddaterelation
		    	//impacted classes are SIC MDM and BCS
		    	
		    	system.debug('##Ludo balise code 1');
		    	CS04_MYR_Settings__c myrSettings = CS04_MYR_Settings__c.getInstance();
		    	if( !String.isBlank(myrSettings.MYR_Test_DataSource__c) ) 
		    	{
			    	List<String> myrparams = myrSettings.MYR_Test_DataSource__c.split(';');
					AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].LastName = (myrparams.size() > 0) ? myrparams[0] : '';
					AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].FirstName = (myrparams.size() > 1) ? myrparams[1] : '';
					AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].contact.email = (myrparams.size() > 2) ? myrparams[2] : '';
					if( myrparams .size() > 3 && !String.isBlank(myrparams[3])) {
						AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].ident1 = myrparams[3];	
					}
					if( myrparams .size() > 4 && !String.isBlank(myrparams[4])) {
						AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].vehicleList[0].purchaseNature = myrparams[4];	
					}
					if( myrparams .size() > 5 && !String.isBlank(myrparams[5])) {
						AsyncTRVwsdlBcs.bcsResponse.getCustDataResponse.response.clientList[0].vehicleList[0].possessionEnd = myrparams[5];	
					}
		    	}
		    	
		    	//AsyncTRVwsdlBcs trvwsdl = new AsyncTRVwsdlBcs(); 
		    	//AsyncTRVwsdlBcs.bcsResponse = mock.bcsResponse;
		    	system.debug('##Ludo balise code 2');
				return TRVWebservicesBCS.mapPersonalDataFields(AsyncTRVwsdlBcs.bcsResponse, params);  	
				
			}
		
		
			params.country = datasource.country;
			
			personalDataAnswer = TRVWebservicesManager.requestSearchParty(params);
			
		} else if ( datasource.mDataSourceType.equalsIgnoreCase('rbx') || datasource.mDataSourceType.equalsIgnoreCase('sic') ) {
    		
    		system.debug('#### Myr_PersonalData_WS - <getPersonalData> - Call RBX');
    		//TRVWebservicesManager sic = new TRVWebservicesManager(); 
			//set the proper country code
	
				TRVwsdlRbx.GetCustDataResponse responseSIC = null;
				if( Test.isRunningTest() ) {
					system.debug('### Myr_PersonalData_WS - <getPersonaData_SIC> - RBX is running test');
					//dont' forget to set the datasourceorigijn otherwise bcs_id or bcs_id_dacia are not filled. That's really complicated :-(
					params.DataSourceOrigin = datasource.DataSourceOrigin;
					params.country2L = datasource.country2L;
					system.debug('### Myr_PersonalData_WS - <getPersonaData_SIC> - DataSourceOrigin: '+params.DataSourceOrigin);
					//CHECK COUNTRY CODE
					/* SDP, 16.06.2015 / Useless as the check in test class is made on current user.RecordDefaultCountry (asynchronous method) and not on
					the user that triggers the asynchronous method (system.runAs) ....
					Real problem is on Russia when trying to test Poland for instance.
					We get the countrycode from Russia with the following function ==> BCS ==> Country Code 2L and not from Poland
					And so we try to find Country Info with Country Code 3L whereas we got a country code 2L !
					params = TRVWebservicesManager.getcountryInfo(params);
					String countryCode = params.country;
					system.debug('##Ludo country code : ' + params.country);
					List<Country_Info__c> countryInfo = [SELECT Id, Name FROM Country_Info__c WHERE Country_Code_3L__c = :countryCode];
					system.debug('### Myr_PersonalDataSIC_Cls - Nb of Countries found: CountryCode=' + countryCode + ', countries=' + countryInfo);
					system.assertEquals( 1, countryInfo.size());
					*/
					//CHECK COUNTRY CODE
					AsyncTRVwsdlRbx mock = new AsyncTRVwsdlRbx();
					//Prepare the workaround due to the bug System.CalloutException: You have uncommitted work pending. Please commit or rollback before calling out in Winter16
			    	//concatenate the fieldsinto the test dedicated fields
			    	//impacted classes are SIC MDM and BCS
			    	//Order is : lastname;firstname;email;custIdentNumber;purchaseNature;vehicleenddaterelation
			    	CS04_MYR_Settings__c myrSettings = CS04_MYR_Settings__c.getInstance();
			    	if( !String.isBlank(myrSettings.MYR_Test_DataSource__c) ) {
				    	List<String> myrparams = myrSettings.MYR_Test_DataSource__c.split(';');
				    	system.debug('##Ludo name rbx mock : ' + mock.sicResponse.response.clientList[0].LastName);
						mock.sicResponse.response.clientList[0].LastName = (myrparams.size() > 0) ? myrparams[0] : ''; 
						mock.sicResponse.response.clientList[0].FirstName = (myrparams.size() > 1) ? myrparams[1] : ''; 
						mock.sicResponse.response.clientList[0].contact.email = (myrparams.size() > 2) ? myrparams[2] : '';
						if( myrparams.size() > 3 && !String.isBlank(myrparams[3])) {
							mock.sicResponse.response.clientList[0].ident1 = myrparams[3];	
						}
						if( myrparams.size() > 4 && !String.isBlank(myrparams[4])) {
							mock.sicResponse.response.clientList[0].vehicleList[0].purchaseNature = myrparams[4];	
						}
						if( myrparams.size() > 5 && !String.isBlank(myrparams[5])) {
							mock.sicResponse.response.clientList[0].vehicleList[0].possessionEnd = myrparams[5];	
						}
			    	}
					//responseSIC = mock.sicResponse.response;
					system.debug('##Ludo rbx mock : ' + TRVWebservicesRBX.mapPersonalDataFields(mock.sicResponse.response, params));
					return TRVWebservicesRBX.mapPersonalDataFields(mock.sicResponse.response, params);  	 
				}
					
			
				params.country = datasource.country;
				personalDataAnswer = TRVWebservicesManager.requestSearchParty(params);			
			
			
			
			
		} else if ( datasource.mDataSourceType.equalsIgnoreCase('mdm') ) {
			// SDP / 25.11.2015 / ----- Use the Transverse team implementation
			// Waiting for the final use of this implementation for RBX/BCS I was forced to develope a mapping between
			// the response format of the transverse team and ours.
			//Set the specific demander for MDM
			system.debug('#### Myr_PersonalData_WS - <getPersonalData> - Call MDM');
			params.demander = CS04_MYR_Settings__c.getInstance().Myr_MDM_Env__c + 'MYR';
			
			system.debug('### Myr_PersonalDataMDM_Cls Concat MyrPersonalData - <callMDM> - params=' + params);
			
			personalDataAnswer = TRVWebservicesManager.requestSearchParty(params);
			
			system.debug('### Myr_PersonalDataMDM_Cls Concat MyrPersonalData  - <callMDM> - SearchPArty, number of clients found: ' 
			+ ((personalDataAnswer != null && personalDataAnswer.response != null && personalDataAnswer.response.infoClients != null)?String.valueOf(personalDataAnswer.response.infoClients.size()):'0'));
			
			if( personalDataAnswer != null && personalDataAnswer.response != null && personalDataAnswer.response.infoClients != null && personalDataAnswer.response.infoClients.size() == 1) {
				system.debug('### Myr_PersonalDataMDM_Cls - <callMDM> - only 1 response found, call GetParty');
				//ok we found only one reponse :-D, we can continue by getting the vehicles
				params.strPartyID = personalDataAnswer.response.infoClients[0].strPartyID;
				
				personalDataAnswer = TRVWebservicesManager.requestGetParty(params);
				//mdmResponse = mapTRVMDMResponse(trvDetailsResponse);
			}
			
			//personalDataAnswer = Myr_PersonalDataMDM_Cls.callMDM(params);
			
			system.debug('#### Myr_PersonalData_WS - <getPersonalData> - personalDataAnswer=' + personalDataAnswer);
		}
		return personalDataAnswer;
	}	 
}