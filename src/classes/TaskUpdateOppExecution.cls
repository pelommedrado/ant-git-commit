public class TaskUpdateOppExecution {

    /*public void execute(List<Task> taskList) {
        Set<String> whatIDs = new Set<String>();
        for (Task t : taskList) {
            whatIDs.add(t.whatID);
        }

        List<Opportunity> oppList = [
            SELECT Id
            FROM Opportunity
            WHERE Id =: whatIDs
        ];

        if(!oppList.isEmpty()) {
            update oppList;
        }
    }*/

    public void atualizarStageOpp(List<Task> taskList) {
        Set<String> whatIDs = new Set<String>();
        for (Task t : taskList) {
            whatIDs.add(t.whatID);
        }

        List<Opportunity> oppList = [
            SELECT Id, StageName
            FROM Opportunity
            WHERE Id =: whatIDs
            AND RecordTypeId =: Utils.getRecordTypeId('Opportunity', 'DVR') AND StageName = 'Identified'
        ];

        if(oppList.isEmpty()) {
            return;
        }

        for(Opportunity op: oppList) {
            op.StageName = 'In Attendance';
        }

        try {
            Database.update(oppList);

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
        }
    }
}