/* This Page is used to redirect to the Goodwill Edit Page - Reena Jeyapal */
public with sharing class VFC05_EditPageRedirect {

    
    Id GudwillId= System.currentPageReference().getParameters().get('Id');
    String retURL=System.currentPageReference().getParameters().get('retURL');
    public VFC05_EditPageRedirect(ApexPages.StandardController controller) {
    }
    
    
    //This method is used to redirect to the Goodwill Edit Page based on Record Type
    public PageReference redirectToPage(){
        PageReference EduListPage;
        String selectedRecordType=[select RecordTypeId from Goodwill__c where Id=:GudwillId].RecordTypeId;
        if(selectedRecordType =='012D0000000KBFGIA4'){ 
            EduListPage = new PageReference('/apex/VFP03_FTSGoodwill_Edit?Id='+GudwillId+'&retURL='+retURL);
            EduListPage.setRedirect(true);
            return EduListPage;
          }
        else if (selectedRecordType =='012D0000000KBFBIA4'){
           return new PageReference('/'+GudwillId+ '/e?retURL='+ retURL +'&nooverride=1');
        }
        else{
           return new PageReference('/'+GudwillId+ '/e?retURL='+ retURL +'&nooverride=1');
        
        }
   }

}