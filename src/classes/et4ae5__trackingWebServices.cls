/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class trackingWebServices {
    global trackingWebServices() {

    }
    webService static String buttonResponse(String sendDefId) {
        return null;
    }
    webService static void cancelSendDef(String sendDefId) {

    }
    webService static void cancelSmsDef(String smsDefId) {

    }
    webService static void immediateTracking(String sendDefId, String evalString) {

    }
}
