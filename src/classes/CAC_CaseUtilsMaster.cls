public abstract without sharing class CAC_CaseUtilsMaster {
	
    public Datetime now(){
        Datetime now = System.now();
        return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
    }
    
    protected User userInfos;
    protected final List<Case> lsCase;
    protected final Map<Id,Profile> mapProfile;
    protected final Map<Id,Case> mapOldCase;
    protected Map<Id,RecordType> mapAvailableRecordTypeToUser;
    
    public CAC_CaseUtilsMaster(){}
    public CAC_CaseUtilsMaster(List<Case> caseList, Map<Id,Case>oldCase){
        Id idUser = UserInfo.getUserId();
        userInfos = [select Id, isCac__c, roleInCac__c from User where id =: idUser];
        this.lsCase = caseList;
        this.mapOldCase = oldCase;
        this.mapAvailableRecordTypeToUser = getAvailableRecordType();
    }
    
    public Map<Id,RecordType> getAvailableRecordType(){
        Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>();
        Set<String> lsDeveloperName = new Set<String>();
        
        for(cacweb_Mapping__c custonSetCac : cacweb_Mapping__c.getAll().values())
            lsDeveloperName.add(custonSetCac.Record_Type_Name__c);
        
        return new Map<Id,RecordType>([select Id, SObjectType, DeveloperName, Name from RecordType where DeveloperName in:(lsDeveloperName)]);
    }
    
    //insere a data de expiração
    public dateTime corretDayReturn(long hour , integer startHourSac , integer finishHourSac){
      	System.debug('#### hour '+hour);
        System.debug('#### startHourSac '+startHourSac);
        System.debug('#### finishHourSac '+finishHourSac);
        
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'Brazil'];
        long horario = hour*60*60*1000;//hora>minutos>segundos>milesimos

        System.debug('#### horario '+horario);
        DateTime dataCorreta = BusinessHours.add(bh.Id, system.now(),horario);
        System.debug('#### dataCorreta '+dataCorreta);
        
        return dataCorreta;
        
    }
    
    public integer dayControl(integer day, List<String>lsDays){
        if(lsDays.get(day).equals('Sun') )
            return 2;
        if(lsDays.get(day).equals('Sat'))
            return 3;
        return 1;
    }
    
    
    //get ExpiredDate by Subject
    public Map<String,cacweb_settingTime__c> getExpiredDate(){
        Map<String,cacweb_settingTime__c> mapSettingTime = new Map<String,cacweb_settingTime__c>();
        for(cacweb_settingTime__c settingTime: cacweb_settingTime__c.getAll().values()){
            mapSettingTime.put(settingTime.Subject__c, settingTime);
        }
        return mapSettingTime;
    }
    
}