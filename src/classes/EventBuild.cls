@IsTest
public class EventBuild {

    public static EventBuild instance = null;

    public static EventBuild getInstance() {
        if(instance == null) {
            instance = new EventBuild();
        }
        return instance;
    }

    private EventBuild() {
    }

    public Event createEvent(Id userId, Id oppId) {
        final Event event = createEvent(userId);
        event.WhatId = oppId;
        return event;
    }

    public Event createEvent(Id userId) {
        final Datetime dat = Datetime.now();
        final Event event = new Event();
        event.OwnerId 			= userId;
        event.Subject 			= 'Subject';
        event.StartDateTime 	= dat.addMinutes(10);
        event.EndDateTime		= dat.addMinutes(20);
        event.ActivityDatetime__c 	= Datetime.now();
        event.DurationInMinutes = 10;
        event.ReminderDateTime 	= dat;
        event.IsReminderSet 	= true;
        event.Status__c 		= 'Active';
        return event;
    }

    public List<Event> createListEvent(Id userId, Id oppId, Integer size) {
        List<Event> eventList = new List<Event>();
        for(Integer i = 0; i < size; i++) {
          final Event event = createEvent(userId, oppId);
          event.Subject 			= 'Event'+i;
          event.StartDateTime 	= Datetime.now().addMinutes(i+1);
          event.EndDateTime		= Datetime.now().addMinutes(i+2);
          event.DurationInMinutes = 1;
          event.ActivityDatetime__c 	= Datetime.now();
          event.ReminderDateTime 	= Datetime.now();
          event.IsReminderSet 	= true;
          event.Status__c 		= 'Not Started';
          eventList.add(event);
        }
        return eventList;
    }
}