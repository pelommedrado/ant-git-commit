@isTest
public class LMT_CampaignsMembersLoadControllerTest {
    
    public static testMethod void myTest(){
        
        Id invalid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Invalid');
        Id unprocessed = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Unprocessed');
        Id valid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Valid');
        
        Campaign c = new Campaign(
            Status = 'Planned',
            PAActiveShareAllowed__c = true,
            Name='teste',
            DBMCampaignCode__c='Cod',
            WebToOpportunity__c = false );
        Insert c;
        
        Lead l = new Lead(
            FirstName = 'Nome',
            LastName = 'Sobrenome',
            CPF_CNPJ__c='72413718362');
        Insert l;
        
        CampaignMember cm = new CampaignMember(LeadId = l.Id,CampaignId = c.Id);
        Insert cm;
        
        Document d = new Document(Name = Label.rotuloCSV,FolderId=[select Id from Folder where Name = 'Shared Documents' limit 1].Id);
        Insert d;
        
        InterfaceCampaignMembers__c icm = new InterfaceCampaignMembers__c(
            CampaignCode__c = 'Cod',
            CPF_CNPJ__c = '72413718363',
            LastName__c = 'Sobrenome',
            ErrorMessage__c = 'erro',
            RecordTypeId = invalid
        );
        Insert icm;
        
        List<InterfaceCampaignMembers__c> lsIcm = new List<InterfaceCampaignMembers__c>();
        lsIcm.add(icm);
        PageReference pageRef = Page.LMT_CampaignsMembersLoad;
	    Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('param', cm.Id);
        LMT_CampaignsMembersLoadController controller = new LMT_CampaignsMembersLoadController();
         
        //testa Variaveis
        controller.Campanha = c.Id;
        
        //testa setCampaign
        controller.setCampaign();
        
        //testa links
        icm.RecordTypeId = unprocessed;
        Update icm;
        String unprocess = controller.getUnproccess();
        system.assertEquals('1', unprocess);
        icm.RecordTypeId = valid;
        Update icm;
        String valids = controller.getValids();
        system.assertEquals('1', valids);
        icm.RecordTypeId = invalid;
        Update icm;
        String invalids = controller.getInvalids();
        system.assertEquals('1', invalids);
        
        //testa metodos de limpar
        controller.limpaInvalidos();
        controller.limpaValidos();
        
        //testa metodo de reprocessar
        controller.reprocess();
        
        //testa metodo getCampaignsName
        List<SelectOption> options = new List<SelectOption>();
        options = controller.getCampaignsName();
        system.assertEquals(c.Id, options.get(0).getValue());
        
        //testa metodo getListCampaignMembers
        List<CampaignMember> lsCM = new List<CampaignMember>();
        lsCM = controller.getListCampaignMembers();
        system.assertEquals(cm.Id, lsCM.get(0).Id);

        //testa metodo de execução de Bacth
        controller.executeBatch();
        
        //testa metodo de CSV
        Id csvId = controller.getCsvPadraoId();
        system.assertEquals(d.Id, csvId);
        
        //testa metodo de CSV sem doc
        Delete d;
        Id csvIdDel = controller.getCsvPadraoId();
        system.assertEquals(d.Id, csvId);
        
        //testa metodo principal
        controller.importCSVFile(lsIcm);
        
        //Testa LastName
        system.assertEquals(true, controller.isValidLastName('Campo', icm ));
        icm.LastName__c = null;
        system.assertEquals(null, icm.LastName__c);
        system.assertEquals(false, controller.isValidLastName('Campo', icm ));
        
        //testa metodo duplicatedMemberInList
        controller.duplicatedMemberInList(icm);
        
        //testa metodo existMemberCampaing
        Set<String> setCpfsCodCampanhasDoBD = new Set<String>();
        setCpfsCodCampanhasDoBD.add('72413718363Cod');
        system.assertEquals(true, controller.existMemberCampaing(setCpfsCodCampanhasDoBD, icm));
        
        //testa metodo isValidoDecimal
        system.assertEquals(true, controller.isValidoDecimal('Campo','12345',icm));
        system.assertEquals(false, controller.isValidoDecimal('Campo','nao decimal',icm));
        
        //testa metodo isValidaDate
        system.assertEquals(true, controller.isValidaDate('Campo','1985-05-12',icm) );
        system.assertEquals(false, controller.isValidaDate('Campo','12/05/1985',icm) );
        
        //testa metodo datatime 
        system.assertEquals(true, controller.isValidaDateTime('Campo','1985-05-12 00:00:00',icm) );
        system.assertEquals(false, controller.isValidaDateTime('Campo','12/05/1985 00:00:00',icm) );
        
        //teste success delets
        controller.deleteMembers();
        controller.deleteFew();
        
        //teste failed delets
        c.WebToOpportunity__c = true;
        update c;
        controller.deleteMembers();
        controller.deleteFew();
        
        //testa relatorios
        controller.getRelatorioInvalidId();
        controller.getRelatorioUnproccessedId();
        controller.getRelatorioValidId();
       
    }
    
    @isTest(SeeAllData='true')
    public static void testAsyncReportWithTestData() {
        
        LMT_CampaignsMembersLoadController controller = new LMT_CampaignsMembersLoadController();
        
        //testa relatorios
        controller.getRelatorioInvalidId();
        controller.getRelatorioUnproccessedId();
        controller.getRelatorioValidId();
    }

}