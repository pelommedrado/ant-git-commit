/*  Desactivation of myRenault access (Webservice) 
    Error codes :   OK              : WS03MS000 
                    OK WITH WARNING : WS03MS001 -> WS03MS500
                    CRITICAL ERROR  : WS03MS501 -> WS03MS999
*************************************************************************************************************
2014 Aug Creation
2015 May Assesment SalesForce and improve logging system
2015 Sep ATC synchronization
2015 Nov R16.01  9.1 ChocolateCamelBaby, D. Veron (AtoS) : User's login and account's personal email become independent
2016 Mar R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
2016 Jul R16.10 11.1 BlackEagle, D. Veron (AtoS) US-3655 : Account desactivation implies associated customer messages are set to status="deleted"
2016 Oct R16.11 Amber Fox, S. Ducamp (AtoS) F1855: added an exception when trying to deactivate the user to test a behaviour within changeEmail
*************************************************************************************************************/
global without sharing class Myr_UserDeActivation_WS { 

	public enum TEST_EXCEPTION {DEACTIVATE_EXCP}
	public class Myr_UserDeActivation_Exception extends Exception {}

    //WebService used to activate a user on a given account
    //@param accountSfdcId, the SFDC id of the personal account on which we want activate the user
    //@return a msg and the activated user
    
    WebService static Myr_UserDeActivation_WS_Response deActivateUser(String accountId, String accountBrand) {
        User u;
		Account acc;
		List<User> listUsers;
		List<Account> listAcc;
		Long BeginTime = DateTime.now().getTime();
        String logRunId = Myr_UserActivation_WS.class.getName() + DateTime.now().getTime();
        String Inputs='accountId='+accountId+', brand='+accountBrand;
		System.debug('deActivateUser:Inputs=<'+Inputs+'>');
          
        try { 
            //Check mandatory fields value 
            String fieldsLabel = missingMandatory(accountId);
            if (fieldsLabel != ''){
				return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS504, system.Label.Myr_UserDeActivation_WS03MS504_Msg + ' ' + fieldsLabel, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs);
            } 
             
            //Check brand parameter
            if( !String.isBlank(accountBrand) && !accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name()) && !accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name())) {
				return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS505, system.Label.Myr_UserDeActivation_WS03MS505_Msg + accountBrand, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs); 
            }
            if( String.isBlank(accountBrand) ) {
              accountBrand = Myr_MyRenaultTools.Brand.Renault.name();
            }
            
            //Check the account information and update them
            listAcc = [SELECT Id, MyRenaultID__c, MyDaciaID__c, MYR_Status__c, MYD_Status__c FROM Account WHERE Id = :accountId ];
            if( listAcc.size() == 0 ) {
              return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS501, system.Label.Myr_UserDeActivation_WS03MS501_Msg, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs);
            }
            acc = listAcc[0]; 
            
            if( !String.isBlank(accountBrand) && accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name())) {
				ATC_synchronization(accountId);
            }
          
            //HQREQ-03957 // if( isBrandActive(acc, brand) ) {
            if( accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name())) {
                acc.MYR_Status__c = system.Label.Myr_Status_Deleted; // not for update, just for later user
            } else if ( accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name()) ) {
                acc.MYD_Status__c = system.Label.Myd_Status_Deleted; // not for update, just for later user
            }
            //HQREQ-03957 // } //otherwise nothing to do: brand is already deactivated. Check after to see if the user has to be removed
            
            //If one of the 2 brands is activated, we can return OK otherwise we have to deactivate the user
            if( isBrandActive(acc, Myr_MyRenaultTools.Brand.Renault.name()) || isBrandActive(acc, Myr_MyRenaultTools.Brand.Dacia.name()) ) {
                //one of the 2 marks is active we can deactivate the account in a syncrhonous way
                cleanAccount(acc.Id, accountBrand, logRunId, BeginTime, Inputs);
                return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS000, system.Label.Myr_UserDeActivation_WS03MS000_Msg, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Info, BeginTime, inputs);
            } else {
                //otherwise we have not update the account asynchronously as we will have to update the user.
                cleanAccountFuture(acc.Id, accountBrand, logRunId, Inputs);
            } 
           
            //The 2 brands are inactive (deleted status), so we have to deactivate the salesforce user
            listUsers = [Select ProfileId, Profile.Name, IsActive From User WHERE AccountId=:accountId];
            if(listUsers.size()==0) {
                //HQREQ-03957: now this is a OK status and the account status is modified to deleted for the given brand
                //return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS501, system.Label.Myr_UserDeActivation_WS03MS501_Msg, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Critical);
                return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS000, system.Label.Myr_UserDeActivation_WS03MS000_Msg, logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Info, BeginTime, inputs);
            } else {
                u = listUsers[0];
                if ( u.profileId != Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id ){
                    return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS503, system.Label.Myr_UserDeActivation_WS03MS503_Msg.replace('{0}', u.profile.name), logRunId, 'userId='+u.Id, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs);
                } else {
                    if (!u.IsActive){
                        return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS001, system.Label.Myr_UserDeActivation_WS03MS001_Msg, logRunId, 'userId='+u.Id, INDUS_Logger_CLS.ErrorLevel.Info, BeginTime, inputs);
                    } else {
                        try {
                            u.username = Myr_Users_Utilities_Class.generateCancelUsername(accountId);//empty the username for later re-use (SDP: 12.02.2015)
							//SDP / 06.10.2016 / throw an exception to test the behaviour of the changeEmail
							if( Test.isRunningTest() && 
								(!String.isBlank(acc.MyRenaultID__c) && acc.MyRenaultId__c.startsWithIgnoreCase( TEST_EXCEPTION.DEACTIVATE_EXCP.name() )
								|| !String.isBlank(acc.MyDaciaId__c) && acc.MyDaciaId__c.startsWithIgnoreCase( TEST_EXCEPTION.DEACTIVATE_EXCP.name() ) ) ) 
							{
								//Generates a bad username
								u.username = 'bad_username';
							}
                            u.isActive=false;
							//u.isPortalEnabled = false;
                            update u;
                            return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS000, system.Label.Myr_UserDeActivation_WS03MS000_Msg, logRunId, 'userId='+u.Id, INDUS_Logger_CLS.ErrorLevel.Info, BeginTime, inputs);
                        } catch (Exception e) {
                            return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS502, system.Label.Myr_UserDeActivation_WS03MS502_Msg + e.getMessage(), logRunId, 'userId='+u.Id, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs);
                        }
                    }
                }
            }
        } catch (Exception e) {
            return prepareResponse(system.Label.Myr_UserDeActivation_WS03MS502, system.Label.Myr_UserDeActivation_WS03MS502_Msg + e.getMessage(), logRunId, 'accountId='+accountId, INDUS_Logger_CLS.ErrorLevel.Critical, BeginTime, inputs);
        }
    } 
        
    //@return the response in case of a bad situation
    private static Myr_UserDeActivation_WS_Response prepareResponse(String code, String message, String logRunId, String logRecord, INDUS_Logger_CLS.ErrorLevel logErrLvl, Long BeginTime, String inputs) {    
        addLog(logRunId, 'DeActivateUser', logRecord, code, message, logErrLvl.name(), BeginTime, inputs);
        Myr_UserDeActivation_WS_Response response = new Myr_UserDeActivation_WS_Response();
        response.info = new Myr_UserDeActivation_WS_Response_Msg();
        response.info.code = code;
        response.info.message = message;
        return response;
    }
  
    //Global format of the response
    global class Myr_UserDeActivation_WS_Response {
        WebService Myr_UserDeActivation_WS_Response_Msg info;
    }
  
    global class Myr_UserDeActivation_WS_Response_Msg {
        WebService String code;
        WebService String message;
    }
    
    static String missingMandatory(String accountId) {
        String str='';
        if( String.isBlank(accountId) ){
            str += 'accountId';
        }
        return str;
    }
      
    //Check if the account is active for the given brand
    static Boolean isBrandActive(Account acc, String brand) {
        return (brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name()) 
                && (acc.MYR_Status__c == system.Label.Myr_Status_Created || acc.MYR_Status__c == system.Label.Myr_Status_Activated))
          || (brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name()) 
                && (acc.MYD_Status__c == system.Label.Myd_Status_Created || acc.MYD_Status__c == system.Label.Myd_Status_Activated));
    }
      
    //Clean the account fields linked to the Brand. Done in a future method as the user is updated online in the webservice (avoid MIXED_DML_EXCEPTION)
    @future
    global static void cleanAccountFuture(String accountId, String brand, String logRunId, String Inputs) {
        Long BeginTime = DateTime.now().getTime();
        cleanAccount(accountId, brand, logRunId, BeginTime, Inputs);
    }
      
    //Clean the account fields linked to the Brand.
    global static void cleanAccount(String accountId, String brand, String logRunId, Long BeginTime, String Inputs) {
        try {
            if( brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name())) {
                cleanRenaultAccount(accountId);
            } else if ( brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name())) {
                cleanDaciaAccount(accountId);  
            }
            addSyncLog(logRunId, 'DeActivateUser', accountId, 'OK', 'Clean Account OK', INDUS_Logger_CLS.ErrorLevel.Info.name(), BeginTime, Inputs);
        }catch (Exception e) {
            addSyncLog(logRunId, 'DeActivateUser', accountId, 'Exception', 'Problem encountered when cleaning asynchronously the account: ' + e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Critical.name(), BeginTime, Inputs);
        }
        
        if (System.isFuture()){
			Delete_Customer_Message(accountId, brand, logRunId, BeginTime, Inputs);
        }else{
            Delete_Customer_Message_Future(accountId, brand, logRunId, BeginTime, Inputs);
        }
    }
    
    @future
    global static void Delete_Customer_Message_Future(String Account_Id, String Brand_Name, String logRunId, Long BeginTime, String Inputs) {
        Delete_Customer_Message(Account_Id, Brand_Name, logRunId, BeginTime, Inputs);
    } 

	//For all the customer message associated to the account, set the status to "deleted"
    global static void Delete_Customer_Message(String Account_Id, String Brand_Name, String logRunId, Long BeginTime, String Inputs) {
        List<Customer_Message__c> Customer_List;
		addSyncLog(logRunId, 'DeActivateUser', Account_Id, 'Begin', 'Delete notifications associated to the accountId=<' + Account_Id + '>', INDUS_Logger_CLS.ErrorLevel.Info.name(), BeginTime, Inputs);
         
        try {
			Customer_List = [select Status__c from Customer_Message__c where account__c=:Account_Id and Brand__c=:Brand_Name];
			for(Customer_Message__c C_M : Customer_List){
				C_M.Status__c=system.Label.Myr_Customer_Message_Status_Deleted; 
			} 
            update Customer_List; 
            addSyncLog(logRunId, 'DeActivateUser', Account_Id, 'OK', 'Delete_Account_Notifications.Delete_notifications_associated_to_the_accountId=<' + Account_Id + '>_Status_OK', INDUS_Logger_CLS.ErrorLevel.Info.name(), BeginTime, Inputs);
        }catch (Exception e) {
            addSyncLog(logRunId, 'DeActivateUser', Account_Id, 'Exception', 'Problem_encountered_when_deleting_asynchronously_the_notifications:<' + e.getMessage() + '>', INDUS_Logger_CLS.ErrorLevel.Critical.name(), BeginTime, Inputs);
        }
    }
      
    //Clean the account fields linked to Dacia.
    global static void cleanDaciaAccount(String accountId) {
        List<Account> listAcc = [SELECT Id, MYD_Status__c FROM Account WHERE Id = :accountId];
        if( listAcc.size() > 0 ) {
          listAcc[0].MYD_Status__c = system.Label.Myd_Status_Deleted;
          listAcc[0].MyDaciaID__c  = '';
        }
        update listAcc;
    }
      
    //Clean the account fields linked to Renault. Done in a future method as the user is updated online in the webservice (avoid MIXED_DML_EXCEPTION)
    global static void cleanRenaultAccount(String accountId) {
        List<Account> listAcc = [SELECT Id, MYR_Status__c FROM Account WHERE Id = :accountId];
        if( listAcc.size() > 0 ) {
          listAcc[0].MYR_Status__c = system.Label.Myr_Status_Deleted;
          listAcc[0].MyRenaultID__c  = '';
        }
        update listAcc;
    }
      
    private static void addSyncLog(String runLogId, String myMethod, String myId, String exceptType, String myMessage, String myLevel, Long BeginTime, String inputs) {
        Id id = INDUS_Logger_CLS.addGroupedLogFull(runLogId, null, INDUS_Logger_CLS.ProjectName.MYR, Myr_UserDeActivation_WS.class.getName(), myMethod, exceptType, myMessage, INDUS_Logger_CLS.ErrorLevel.Info.name(), myId, null, null, BeginTime, DateTime.now().getTime(), 'Inputs: <' + inputs + '>');
    }

    //Add the logs for this webservice in future mode: avoid the mixed_dml_exception as we could have updated the username of the user
    @future 
    private static void addLog(String runLogId, String myMethod, String myId, String exceptType, String myMessage, String myLevel, Long BeginTime, String inputs) {
        Id id = INDUS_Logger_CLS.addGroupedLogFull(runLogId, null, INDUS_Logger_CLS.ProjectName.MYR, Myr_UserDeActivation_WS.class.getName(), myMethod, exceptType, myMessage, INDUS_Logger_CLS.ErrorLevel.Info.name(), myId, null, null, BeginTime, DateTime.now().getTime(), 'Inputs: <' + inputs + '>');
    }
  
    //ATC synchronization
    @future (callout=true)
    private static void ATC_synchronization(Id Account_Id) {
        Myr_Synchro_ATC.Change_Myr_Status(Account_Id, 'Delete_On_Account');
    }
}