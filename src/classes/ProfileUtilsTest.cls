@IsTest
private class ProfileUtilsTest {
    
    private static User user;
    private static Account accDealer;
    
    static {
        User usr = [Select id from User where Id = :UserInfo.getUserId()];

        //necessario usuario possuir papel
        UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
        usr.UserRoleId = r.Id;
        Update usr;

        System.RunAs(usr) {
            //Test.startTest();
            
            accDealer = MyOwnCreation.getInstance().criaAccountDealer();
            INSERT accDealer;
            
            Contact ctt = new Contact(
                CPF__c 		=  '44476157114',
                Email 		= 'manager1@org1.com',
                FirstName 	= 'User',
                LastName 	= 'Manager1',
                MobilePhone = '11223344556',
                AccountId 	= accDealer.Id
            );
            
            INSERT ctt;
            
            user = new User(
                ContactId 			= ctt.Id,
                ProfileId 			= Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName 			= 'User',
                LastName 			= 'Manager1',
                Username 			= 'manager1@org1.com.teste',
                Email 				= 'manager1@org1.com',
                Alias 				= 'ma1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c 				= '123456',
                EmailEncodingKey	='UTF-8',
                LanguageLocaleKey	='en_US',
                LocaleSidKey		='en_US',
                TimeZoneSidKey		='America/Los_Angeles'
            );
            
            INSERT user;
            //Test.stopTest();
        }
    }
    
    static testMethod void testSeller() {
        insertPermission(user, ProfileUtils.permissionSeller);     
        System.RunAs(user) {
            System.assertEquals(true, ProfileUtils.isSeller);
        }
    }
    
    static testMethod void testManager() {
        insertPermission(user, ProfileUtils.permissionDealer);
        System.RunAs(user) {
            System.assertEquals(true, ProfileUtils.isManager);
        }
    }
    
    static testMethod void testManagerBdc() {
        insertPermission(user, ProfileUtils.permissionDealer);
        insertPermission(user, ProfileUtils.permissionBdc);
        
        System.RunAs(user) {
            System.assertEquals(true, ProfileUtils.isManagerBdc);
        }
    }
    
    static testMethod void testManagerExec() {
        insertPermission(user, ProfileUtils.permissionExec);
        System.RunAs(user) {
            System.assertEquals(true, ProfileUtils.isManagerExec);
        }
    }
    
    static void insertPermission(User u, String namePermission) {
        PermissionSet ps = [ SELECT ID From PermissionSet WHERE Name =: namePermission ];
        PermissionSetAssignment psA = 
            new PermissionSetAssignment(AssigneeId = u.id, PermissionSetId = ps.Id );       
        INSERT psA;    
    }
    
    static testMethod void deveVerificarSeUserTemPermissaoSeller() {
        insertPermission(user, ProfileUtils.permissionSeller);
        System.RunAs(user) {
            final List<String> permissionNameList = new List<String>();
            permissionNameList.add(ProfileUtils.permissionSeller);
            permissionNameList.add(ProfileUtils.permissionDealer);
            permissionNameList.add(ProfileUtils.permissionExec);
            permissionNameList.add(ProfileUtils.permissionBdc);
            final Boolean isPermission = 
                ProfileUtils.hasPermission(user.Id, permissionNameList);
            System.assertEquals(true, isPermission);
        }
    }
    static testMethod void deveVerificarSeUserNaoTemPermissao() {
        insertPermission(user, ProfileUtils.permissionSeller);
        System.RunAs(user) {
            final List<String> permissionNameList = new List<String>();
            permissionNameList.add(ProfileUtils.permissionExec);
            permissionNameList.add(ProfileUtils.permissionDealer);
            permissionNameList.add(ProfileUtils.permissionBdc);
            final Boolean isPermission = 
                ProfileUtils.hasPermission(user.Id, permissionNameList);
            System.assertEquals(false, isPermission);
        }
    }
    static testMethod void deveVerificarSeUserTemPermissaoSellerSet() {
        insertPermission(user, ProfileUtils.permissionSeller);
        System.RunAs(user) {
            Set<Id> allSellerPermission = 
                ProfileUtils.userPermission(ProfileUtils.permissionSeller);
            System.assertEquals(true, allSellerPermission.contains(user.Id));
        }
    }
}