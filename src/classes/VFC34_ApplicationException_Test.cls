/**
	Class   -   VFC34_ApplicationException_Test
    Author  -   Suresh Babu
    Date    -   13/12/2012
    
    #01 <RameshPrabu> <13/12/2012>
        Created this class using test for VFC34_ApplicationException.
**/
@isTest
private class VFC34_ApplicationException_Test {

    static testMethod void myUnitTest() {
    	try{
			String errorMessage = 'ERROR FOUND'; 
			throw new VFC34_ApplicationException(errorMessage);
		}
		catch( VFC34_ApplicationException e ){
			String expectedExceptionThrown =  e.getMessage();
			System.AssertEquals(expectedExceptionThrown, 'ERROR FOUND');
		} 
    }
    
    static testmethod void myUnitTest2(){
    	VFC34_ApplicationException excep = new VFC34_ApplicationException();
    	excep.exceptionHandling();
    }
}