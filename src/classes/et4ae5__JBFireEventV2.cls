/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class JBFireEventV2 implements Process.Plugin {
    global JBFireEventV2() {

    }
    global Process.PluginDescribeResult describe() {
        return null;
    }
    global Process.PluginResult invoke(Process.PluginRequest request) {
        return null;
    }
}
