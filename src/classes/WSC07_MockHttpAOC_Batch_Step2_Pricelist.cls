/**
*	Class	-	WSC07_MockHttpResponseGenerator
*	Author	-	Rameshprabu
*	Date	-	05/04/2013
*
*	#01 <Rameshprabu> <05/04/2013>
*		This class was used to test Http callout.
**/
@isTest
global class WSC07_MockHttpAOC_Batch_Step2_Pricelist implements HttpCalloutMock {
	/* This webservice method  called */
	global HTTPResponse respond(HTTPRequest req) {
		/* System assert check get method */
        System.assertEquals('GET', req.getMethod());
        /* initialize http response object */
        HttpResponse res = new HttpResponse();
        /* set header in http response */
        res.setHeader('Content-Type', 'application/json');
        /* set body in http response */
        res.setBody('{"requestedURI":"http://br.co.rplug.renault.com/pricelist/BABi","priceList":[{"priceType":{"reference":"PVCTTC","currency":"BRL"},"versionPriceList":[{"versionIdSpecCode":"VEC162_BRES","price":38270.0,"optionsPrices":{"mapRepresentation":{"map":{"DPSEC":0.0,"PK35LA":2090.0,"QPA$MV":910.0},"metadata":{"key":"specCode","value":"price"}}}}]}]}');
        /* set statuscode in http response */
        res.setStatusCode(200);
        return res;
	}



}