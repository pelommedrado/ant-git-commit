@isTest
public class Myr_Synchro_ATC_Run_ENH_Test { 
	private static testMethod void responseStatusMessage_Test1(){
		String msgValInXML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><type><responseCode>11</responseCode><responseDiag>CSDB-0011 : Vehicle not found {0}</responseDiag><value>KO</value></type>';
		Myr_Synchro_ATC_Run_ENH.responseStatusMessage(msgValInXML);
	}
	private static testMethod void responseStatusMessage_Test2(){
		String msgValInXML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><type><responseCode>11</responseCode><value>KO</value></type>';
		Myr_Synchro_ATC_Run_ENH.responseStatusMessage(msgValInXML);
	}
	private static testmethod void Myr_Call_Visibilty_Fn01_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.MYR_Status = 'Created';
		P_H.img_Transparent = 'testResourcePathForImage';
		Myr_Synchro_ATC_Run.Line_Handler myH = new Myr_Synchro_ATC_Run.Line_Handler();
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn01(P_H,myH);
	}
	private static testMethod void Myr_Call_Visibilty_Fn02_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.MYR_Status = 'Created';
		P_H.img_Transparent = 'testResourcePathForImage';
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn02(P_H);
	}
	private static testMethod void Myr_Call_Visibilty_Fn03_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.MYR_Status = 'Created';
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn03(P_H);
	}
	private static testMethod void Myr_Call_Visibilty_Fn04_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.MYR_Status = 'Created';
		P_H.img_Transparent = 'testResourcePathForImage';
		Myr_Synchro_ATC_Run.Line_Handler myH = new Myr_Synchro_ATC_Run.Line_Handler();
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn04(P_H,myH);
	}
	private static testMethod void Myr_Call_Visibilty_Fn05_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.CSDB_Account_Activated = true;
		P_H.Buttn_Acc_Lbl = System.Label.Myr_Synchro_ATC_Acc_Button + ' - Update';
		P_H.img_Transparent = 'testResourcePathForImage';
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn05(P_H);
	}
	private static testMethod void Myr_Call_Visibilty_Fn06_Test(){
		Myr_Synchro_ATC_Run.Page_Handler P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.Buttn_Acc_Rnd = false;
		P_H.Buttn_Veh_Rnd = false;
		P_H.Buttn_Acc_Lbl = System.Label.Myr_Synchro_ATC_Acc_Button + ' - Update';
		Myr_Synchro_ATC_Run_ENH.Myr_Call_Visibilty_Fn06(P_H);
	}
}