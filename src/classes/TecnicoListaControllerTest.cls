@isTest
private class TecnicoListaControllerTest {

    public static Account acc { get;set; }
    public static Contact cont { get;set; }
    
    static testMethod void unitTest01() {
        User user = criarUser();
        System.runAs(user) {
            TecnicoListaController ct = new TecnicoListaController();
            ct.novoContato();
            ct.editarContato();
        }
    }
    
    
    static User criarUser() {
        acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '79856523',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_Service__c = 'Available'
        );
        
        Database.insert( cont );
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='79856523',
            isCac__c = true
            
        );
        
        return manager;
    }
}