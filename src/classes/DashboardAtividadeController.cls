public class DashboardAtividadeController extends AtividadeAbstract {

    public Opportunity opp 			{ get;set; }
    public Account acc 				{ get;set; }
    public ActivityMap act	  		{ get;set; }
    public ActivityMap nextAct		{ get;set; }
    public Boolean isReabrir 		{ get;set; }
    public Boolean isOpenNewTask 	{ get;set; }
    public Boolean disabMotivo 		{ get;set; }
    public Boolean proximaTask 		{ get;set; }
    public Boolean questionTask 	{ get;set; }
    public Boolean newTask 			{ get;set; }

    public Boolean showConfigHora {
        get {
            if(newTask) {
                return true;
            }
            if(!String.isEmpty(act.status) && act.status.equalsIgnoreCase('Extended')) {
                return true;
            }
            return false;
        }
    }

    public List<SelectOption> motivoList { get;set; }

    public List<SelectOption> motivoCancelaList {
        get {
            List<SelectOption> opLi = new List<SelectOption>();
            opLi.add(new SelectOption('Bought another brand', 'Comprou outra marca'));
            opLi.add(new SelectOption('Bought at another Renault dealer', 'Comprou em outra concessionária Renault'));
            opLi.add(new SelectOption('Withdrawal', 'Desistência'));
            opLi.add(new SelectOption('Financing not approved', 'Financiamento não aprovado'));
            opLi.add(new SelectOption('Not Show Room', 'Não é Show Room'));
            opLi.add(new SelectOption('purchase postponed', 'Compra adiada'));
            opLi.add(new SelectOption('After Sale', 'Fluxo Pós Venda'));
            if(ProfileUtils.isManagerBdc) {
                opLi.add(new SelectOption('Used Vehicle Evaluation', 'Avaliação de Veículo usado'));
                opLi.add(new SelectOption('Poor care', 'Mal atendimento'));
            }
            return opLi;
        }
    }

    public List<SelectOption> motivoFaltou {
        get {
            return VFC49_PickListUtil.buildPickList(Event.OppLossReason__c, '');
        }
    }

    public List<SelectOption> motivoProrrogaList {
        get {
            List<SelectOption> opLi = new List<SelectOption>();
            opLi.add(new SelectOption('Didn\'t answer the phone', 'Não atendeu o telefone'));
            opLi.add(new SelectOption('Requested return at another time', 'Solicitou retorno em outro horário'));
            opLi.add(new SelectOption('Customer Absent', 'Cliente ausente'));
            return opLi;
        }
    }

    public List<SelectOption> optionConfirm {
        get {
            return VFC49_PickListUtil.buildPickList(
                Task.vehicleDeliveredCombinedDate__c, '');
        }
    }

    public List<SelectOption> statusList {
        get { return DashboardAtividadeService.optionActivity(act.subject, false); }
    }

    public List<SelectOption> statusNextList {
        get { return DashboardAtividadeService.optionActivity(nextAct.subject, true); }
    }

    public List<SelectOption> motivoTestDrive {
        get {
            return VFC49_PickListUtil.buildPickList(
                Event.WhyTestDriveNotPerformed__c, '');
        }
    }

    public DashboardAtividadeController() {
        this.isTask 	 = false;
        this.isEvent 	 = false;
        this.disabMotivo = true;
        this.newTask 	 = false;
        this.proximaTask = false;
        this.questionTask = false;
        this.isReabrir = false;
        this.isOpenNewTask = false;

        this.motivoList  = new List<SelectOption>();

        Id oppId = ApexPages.currentPage().getParameters().get('oppId');
        Id actId = ApexPages.currentPage().getParameters().get('actId');

        if(!String.isEmpty(oppId)) {
            this.opp = getOpp(oppId);
            this.acc = ( this.opp != null ? getAcc(this.opp.AccountId) : null );
        }

        this.act = ( actId != null && !String.isEmpty(actId) ? getAct(actId) : null );
        if(act == null) {
            act = createActv(opp);
            this.newTask = true;

        } else {
            this.opp = getOpp(act.whatId);
            this.acc = ( this.opp != null ? getAcc(this.opp.AccountId) : null );
        }

        this.nextAct = createActv(opp);

        //SET VALORES PARA VARIÁVEIS DA PÁGINA
        this.isVehicleOwner = ( this.opp != null && this.opp.isVehicleOwner__c != null && this.opp.isVehicleOwner__c.equals('Y') ? true : false  );
        this.selectedBrand  = ( this.acc != null && this.acc.CurrentVehicle_BR__c != null ? this.acc.CurrentVehicle_BR__r.Brand__c : null);
        this.selectedModel  = ( this.acc != null && this.acc.CurrentVehicle_BR__c != null ? this.acc.CurrentVehicle_BR__r.Model__c : null);

        getModelDetails();
    }

    private ActivityMap createActv(Opportunity opp) {
        ActivityMap actTemp = new ActivityMap();
        actTemp.reminderDatetime = System.now().addMinutes(-15);
        actTemp.reminder = true;
        actTemp.status = 'In Progress';
        actTemp.owner = u.Name;
        actTemp.whatId = (opp != null ? opp.Id : null);
        return actTemp;
    }

    public Opportunity getOpp(Id oppId) {
        if(String.isEmpty(oppId)) {
            return null;
        }

        List<Opportunity> oppList = [
            SELECT Name, StageName, AccountId, CreatedDate, Approach__c,
            VehicleInterest__c, toLabel(OpportunitySource__c),
            toLabel(OpportunitySubSource__c),
            toLabel(Detail__c), Description, IsVehicleOwner__c, temperature__c
            FROM Opportunity WHERE id=: oppId
        ];
        if(oppList.isEmpty()) {
            return null;
        }
        return oppList.get(0);
    }

    public Account getAcc(Id accId) {
        if(String.isEmpty(accId)) {
            return null;
        }
        List<Account> accList = [
            SELECT Id, FirstName, LastName, CustomerIdentificationNbr__c,
             PersMobPhone__c, PersEmailAddress__c, PersLandline__c,
            CurrentVehicle_BR__c, CurrentVehicle_BR__r.Brand__c,
            CurrentVehicle_BR__r.Model__c
            FROM Account WHERE Id =: accId
        ];
        if(accList.isEmpty()) {
            return null;
        }
        return accList.get(0);
    }

    public PageReference save() {
        System.debug('save()');

        String toRedirectActual = null;
        String toRedirectNext 	= null;

        if(!isValid()) {
            System.debug('Invalid');
            return null;
        }

        try {
            String show = Label.swayShowNextAct;
            this.createNext = show.containsIgnoreCase(act.subject);

            if(isReabrir || this.opp.StageName.equalsIgnoreCase('Identified')) {
             	this.opp.StageName = 'In Attendance';
            }

            this.opp.isVehicleOwner__c = getVehicleOwner();
            this.opp.Approach__c = getApproach();
            this.opp.StageName = getStageName();
            this.opp.IntenttoPurchaseNewVehicle__c = previsao;
            Database.update(this.opp);

            if(isValidBrandModal()) {
                this.acc.CurrentVehicle_BR__c = VFC11_MolicarDAO.getInstance()
                    .findMolicarByBrandAndModel(selectedBrand, selectedModel)[0].Id;
            }
            Database.update(this.acc);

            verificarNumTentativa();

            toRedirectActual = saveRecord(this.act);
            System.debug('createNext' + createNext);
            toRedirectNext   = ( createNext && !String.isEmpty(this.nextAct.subject) ?
                                saveRecord(this.nextAct) : null );

            System.debug('toRedirectNext: ' + toRedirectNext);

            PageReference pg = new PageReference('/apex/' +
                (toRedirectNext != null ? toRedirectNext : toRedirectActual));
            pg.setRedirect(true);

            return ( toRedirectActual == null && toRedirectNext == null ? null : pg );

        } catch(Exception ex) {
            System.debug('Ex: ' + ex.getLineNumber());

            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
        }

        return null;
    }

    private Boolean isValid() {
        if(isInvalidContact()) {
            Apexpages.addMessage( new Apexpages.Message(
               ApexPages.Severity.ERROR, Label.ERRPageErrorMessage
               + ' ' + Label.ACCEmail + ' ' + Label.UTIL_OR
               + ' ' + Label.ACCHomePhone  + ' ' + Label.UTIL_OR
               + ' ' + Label.LDDCellular ) );
            return false;
        }

        if(isInvalidField()) {
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, 'Os campos de Nome, Sobrenome,'
                + ' Data, Hora, Atividade e Status são obrigatórios.'));
            return false;
        }

        if(isInvalidDate()) {
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, 'A data/hora informada não pode ser anterior a atual'));
            return false;
        }

        if(isRequiredReason()) {
            ApexPages.addMessage( new ApexPages.Message(
               ApexPages.Severity.ERROR, 'Para atividades canceladas'
               + ' ou prorrogadas é necessário selecionar um motivo.'));
            return false;
        }

        String show = Label.swayShowNextAct;

        if(isInvalidFieldNext()
            && (show.containsIgnoreCase(act.subject) || isOpenNewTask)
            && act.status == 'Completed') {
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR, 'Os campos de Data, Hora,'
                    + ' Atividade e Status da PRÓXIMA ATIVIDADE são obrigatórios.'));
            return false;
        }

        if(!String.isEmpty(nextAct.activityDate) && !String.isEmpty(nextAct.activityHour)) {
            if(!isDataFutura(nextAct.activityDate, nextAct.activityHour)) {
                ApexPages.addMessage( new ApexPages.Message(
                    ApexPages.Severity.ERROR,
                    'A data/hora informada não pode ser anterior a atual'));
                return false;
            }
        }
        return true;
    }

    private Boolean isInvalidContact() {
        return
            String.isBlank(acc.PersLandline__c) &&
            String.isBlank(acc.PersMobPhone__c) &&
            String.isBlank(acc.PersEmailAddress__c);
    }

    private Boolean isInvalidField() {
        return String.isBlank(act.activityDate)
            || String.isBlank(act.activityHour)
            || String.isBlank(act.status)
            || String.isBlank(act.subject)
            || String.isBlank(acc.FirstName)
            || String.isBlank(acc.LastName);
    }

    private Boolean isInvalidDate() {
        return showConfigHora && !isDataFutura(act.activityDate, act.activityHour);
    }

    private Boolean isRequiredReason() {
        return (act.status == 'Canceled' || act.status == 'Extended')
            && String.isEmpty(act.reason);
    }

    private Boolean isInvalidFieldNext() {
        return String.isBlank(nextAct.activityDate)
            || String.isBlank(nextAct.activityHour)
            || String.isBlank(nextAct.status)
            || String.isBlank(nextAct.subject);
    }

    private String getVehicleOwner() {
        return this.isVehicleOwner ? 'Y' : 'N';
    }

    private String getApproach() {
        return this.selectedBrand == 'RENAULT' ? 'Loyalty' : 'Acquisition';
    }

    private String getStageName() {
        return this.act.status == 'Canceled'
            && this.act.reason != 'Not Show Room' ? 'Lost' : this.opp.StageName;
    }

    private Boolean isValidBrandModal() {
        return !String.isEmpty(selectedBrand)
            && !String.isEmpty(selectedModel);
    }

    public void selectTemperatura() {
        String temp = ApexPages.currentPage().getParameters().get('temp');
        if(!String.isEmpty(temp)) {
            opp.temperature__c = temp;
        }
    }

    public void changeStatus() {
        this.disabMotivo = true;
        this.proximaTask = false;
        this.questionTask = false;
        if(String.isEmpty(act.status)) {
            return;
        }

        if(act.status.equalsIgnoreCase('Canceled')) {
            motivoList = motivoCancelaList;
            disabMotivo = false;

        } else if(act.status.equalsIgnoreCase('Extended')) {
            motivoList = motivoProrrogaList;
            disabMotivo = false;

        } else if(act.status.equalsIgnoreCase('Completed')) {

            if(act.subject.equalsIgnoreCase('Confirm Test Drive') ||
               act.subject.equalsIgnoreCase('Confirm Visit') /*||
               act.subject.equalsIgnoreCase('Contact')*/) {
                   this.proximaTask = true;
               }

            if(act.subject.equalsIgnoreCase('Rescue') ||
               act.subject.equalsIgnoreCase('Persecution') ||
               act.subject.equalsIgnoreCase('Delivery Confirmation')) {
                   this.questionTask = true;
                   if(isOpenNewTask) {
                       this.proximaTask = true;
                   }
               }
        }
    }

    public void chanceNextStatus() {
        DashboardAtividadeService.optionActivity(nextAct.subject, true);
    }

    /*public List<SelectOption> statusSelectOption(ActivityMap acM, Boolean isNext) {
        final List<SelectOption> optionList =
            DashboardAtividadeService.optionActivity(acM.subject,);

        if(optionList.isEmpty()) {
            return optionList;
        }
        if(isNext) {
            optionList.add(new SelectOption('In Progress', 'Pendente'));
        }
        return optionList;
    }*/

    /*public List<SelectOption> statusSelectOption(ActivityMap acM, Boolean isNext) {
        final List<SelectOption> optionList = new List<SelectOption>();

        if(String.isEmpty(acM.subject)) {
            //optionList.add(new SelectOption('In Progress', 'Pendente'));
            return optionList;
        }

        if(isNext) {
            optionList.add(new SelectOption('In Progress', 'Pendente'));
        }

        if(isSubject(acM, ActivityManagerBo.confirmTestDrive)) {
            optionConfirmTestDrive(optionList);
        }

        if(acM.subject.equalsIgnoreCase('Confirm Test Drive')) {
            optionList.add(new SelectOption('Completed', 'Test Drive Confirmado'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Test Drive Prorrogado'));

        } else if(acM.subject.equalsIgnoreCase('Perform Test Drive')) {
            optionList.add(new SelectOption('Completed', 'Test Drive Realizado'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Test drive Prorrogado'));

        } else if(acM.subject.equalsIgnoreCase('Confirm Visit')) {
            optionList.add(new SelectOption('Completed', 'Vista Confirmada'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Visita Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Perform Visit')) {
            optionList.add(new SelectOption('Completed', 'Visita Realizada'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Visita Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Customer Service')) {
            optionList.add(new SelectOption('Completed', 'Atividade Realizada'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Atividade Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Persecution')) {
            optionList.add(new SelectOption('Completed', 'Perseguição Realizada'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Atividade Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Rescue')) {
            optionList.add(new SelectOption('Completed', 'Oportunidade Resgatada'));
            optionList.add(new SelectOption('Extended', 'Atividade Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Delivery Confirmation')) {
            optionList.add(new SelectOption('Completed', 'Atividade Realizada'));
            optionList.add(new SelectOption('Extended', 'Atividade Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Prospection Attendance')) {

        } else if(acM.subject.equalsIgnoreCase('Contact')) {
            optionList.add(new SelectOption('Completed', 'Atividade Realizada'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));
            optionList.add(new SelectOption('Extended', 'Atividade Prorrogada'));

        } else if(acM.subject.equalsIgnoreCase('Negotiation')) {
            optionList.add(new SelectOption('Completed', 'Seguir Negociação'));
            optionList.add(new SelectOption('Canceled', 'Oportunidade Perdida'));

        } else if(acM.subject.equalsIgnoreCase('Pre-Order')) {

        }
        return optionList;
    }

    private Boolean isSubject(ActivityMap activityMap, String subject) {
        return activityMap.subject.equalsIgnoreCase(subject);
    }*/

    private void verificarNumTentativa() {
        final Set<String> valueSet = new Set<String>();
        valueSet.add('Rescue');
        valueSet.add('Persecution');

        if(this.act.status.equalsIgnoreCase('Extended') && valueSet.contains(this.act.subject)) {
            if(this.act.numTentativa == null) {
                this.act.numTentativa = 0;
            }
            this.act.numTentativa++;
            if(this.act.numTentativa >=3) {
                this.act.Status = 'Canceled';
            }
        }
    }

    private Boolean isDataFutura(String data, String hora) {
        final Date dataObj = DataUtils.parseDDmmYYYY(data);
        final Time horaObj = DataUtils.parseHHmm(hora);
        //final Time ti = horaObj.addMinutes(-15);
        final Datetime dataHora = Datetime.newInstance(dataObj, horaObj);
        return dataHora > Datetime.now();
    }

    public PageReference cancelar() {
        final Set<String> valueSet = new Set<String>();
        valueSet.add('Rescue');
        valueSet.add('Persecution');
        valueSet.add('Delivery Confirmation');

        if(valueSet.contains(this.act.subject)) {
            final PageReference pg = new PageReference(
                '/apex/DashboardTask?profile=BDC');
            pg.setRedirect(true);
            return pg;
        }

        PageReference pg = new PageReference('/apex/DashboardTask');
        pg.setRedirect(true);
        return pg;
    }
}