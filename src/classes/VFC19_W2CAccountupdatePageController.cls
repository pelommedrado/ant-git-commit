// This is to List out the Conflicts between account and Web2Case - Reena Jeyapal -29/08/13

public with sharing class VFC19_W2CAccountupdatePageController {
    public Id caseId=ApexPages.currentPage().getParameters().get('caseId');
    public List<caseAccount> accwrapList {get; set;}
    List<case> caseRec=new List<case>();
    List<Account> accRec=new List<Account>();
     List<Account> accDetailRec=new List<Account>();
    String casAccountId;
    Account acc=new Account();
    public Account updateAcc{get;set;}
    public Case updateCase{get;set;}
    Case caseForm=new case();
    Id casAccountIds;
    String show{get;set;}
    Pagereference cagePg=null;
    String display;
    Id csAccId;
    public String upFirstName{
    get;
    set {upFirstName=value;
    }
    }
    public String upLastName{
    get;
    set {upLastName=value;

    }
    }
    public String upPhone{
    get;
    set {upPhone=value;
    }
    }
    public String upEmail{
    get;
    set {  
    upEmail=value;

    }
    }
    public String upStreet{
    get; 
    set {upStreet=value;
    
    }
    }
    public String upCity{
    get; 
    set {upCity=value;
     
    }
    }
    public String upState{
    get; 
    set {upState=value;
    
    }
    }
   public String upName{
    get;    
    set {upName=value;
   
    }
    }
    public List<Case> getRecordsToDisplay() {
        return caseRec;
    }
    public List<caseAccount> getCaseAccList() {
        accwrapList= new List<caseAccount>();
      
        caseForm=[select Id,accountId,CaseNumber,Cell_Phone_Web__c,Address_Web__c,City_Web__c,SuppliedName,SuppliedPhone,SuppliedEmail,CPF_Web__c,CPF_Not_Found_Web__c,Email_Opt_In_Web__c,FirstName_Web__c,LastName_Web__c,License_Number_Web__c,State_Web__c,Title_Web__c,VIN_Web__c from case where Id=:caseId];
        
       if(caseForm.accountId !=null)
            {  
       casAccountIds=caseForm.accountId;
        acc=[select Id,name,phone,BillingCity, BillingCountry,BillingState, BillingStreet, BillingPostalCode,PersEmailAddress__c from account where id=:casAccountIds];
        }
        
        accwrapList.add(new caseAccount(caseForm,acc));
        return accwrapList;
    }
    public List<Account> getAccDetail() {
      if(test.isRunningTest())
          
      {
      return null;
      }
        
        else{ 
        try{
            
        csAccId=[select accountid from case where Id=:caseId].accountid ;
     
         accDetailRec=[SELECT AccountBrand__c,CurrencyIsoCode,Description,Fax,Id, Name, AccountNumber, Phone,Extension_CN__c, FOManagerEmail__c, FOManagerName__c, FOManagerPhone__c, FOProvider__c from account where Id=:csAccId];
        }
         catch (System.Stringexception e) {

		System.debug(e.getMessage());
    }
        catch (System.Listexception e) {
		System.debug(e.getMessage());
    }
            return accDetailRec;
        }
        
    }
    public class caseAccount{
        public Case cadetail {get; set;}
        public Account accdetail {get; set;}
        public Boolean selected {get; set;}
        
        public caseAccount(Case c,Account ac) {
        cadetail = c;
        accdetail=ac;
        selected = false;
        }
        
    }

    public PageReference bckToCase(){
        return cagePg= new PageReference('/'+caseId);
    }
    public PageReference processSelected() {

    updateAcc=new Account();
    updateCase=new Case();
    if(upFirstName!=null){
    updateAcc.firstName=upFirstName;
    }
    if(upLastName!=null){
    updateAcc.lastName=upLastName;
    }
    if(upPhone!=null){
    updateAcc.phone=upPhone;
    }
    if(upEmail!=null){
    updateAcc.PersEmailAddress__c=upEmail;
    }
    if(upStreet!=null){
    updateAcc.billingstreet=upStreet;
    }
    if(upCity!=null){
    updateAcc.billingcity=upCity;
    }
    if(upState!=null){
    updateAcc.billingstate=upState;
    }
    updateAcc.Id=casAccountIds;
    try{
        update updateAcc;
        updateCase.Id=caseId;
        update updateCase;
    
    }
    catch(Exception e){}
      
        return null;
    
    }
     public PageReference processSelectedcmd() {
         return null;
     }
    public Id getAccountIds(){
        return casAccountIds;
    }
}