/**
	Class   -   VFC15_RelationshipDecisionTreeDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC15_RelationshipDecisionTreeDAO.
**/

@isTest
public with sharing class VFC15_RelationshipDecisionTreeDAO_Test {
	
	 static testMethod void VFC15_RelationshipDecisionTreeDAO_Test() {
	 	 
		String AddressOK = 'Y';
		String Birthdate = 'Y';
		String EmailOK = 'Y';
		String InformationUpdatedInTheLast5Years = 'Y';
		String ParticipateOnOPA = 'N';
		String PhoneOK = 'Y';
		List<RDT_RelationshipDecisionTree__c> lstRelationshipTreeInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRelationshipDecisionTreeObject();
		
		Test.startTest(); 
		//List<RDT_RelationshipDecisionTree__c> lstRelationshipTree = VFC15_RelationshipDecisionTreeDAO.getInstance().findRelationshipDecisionTree(AddressOK, Birthdate, EmailOK, InformationUpdatedInTheLast5Years, ParticipateOnOPA, PhoneOK);
		List<RDT_RelationshipDecisionTree__c> lstRelationshipTree = VFC15_RelationshipDecisionTreeDAO.getInstance().findRelationshipDecisionTree(lstRelationshipTreeInsert[0].AddressOK__c, lstRelationshipTreeInsert[0].Birthdate__c, lstRelationshipTreeInsert[0].EmailOK__c, lstRelationshipTreeInsert[0].InformationUpdatedInTheLast5Years__c, lstRelationshipTreeInsert[0].ParticipateOnOPA__c, lstRelationshipTreeInsert[0].PhoneOK__c);
		system.debug('lstRelationshipTree...' +lstRelationshipTree);
		Test.stopTest();
		system.assert(lstRelationshipTree.size() > 0);
		
		
	 }
}