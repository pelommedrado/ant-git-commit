/** MyRBackend Apex Test class used to test non regression access for MyRenault users. 
	
	@author S. Ducamp
	@version 1.0
	@date 22.07.2015
 */
@isTest
private class Myr_SecurityCheck_Test {
	
	@testSetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		Myr_Datasets_Test.prepareCustomerMessageSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
	}
	
	//TEST: Check that a community user has only access to their vehicle relations
    static testMethod void test_Community_AccessVehRelations() {
    	//Prepare a list of accounts
        List<Account> listAccUsers = Myr_Datasets_Test.insertPersonalAccounts( 5, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
        //Prepare a list of associated vehicles
        List<VEH_Veh__c> listVehicles = Myr_Datasets_Test.getVehicles('TSTSECURIT', 15, true);
        List<VRE_VehRel__c> listRelations = new List<VRE_VehRel__c>();
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[0].Id, VIN__c=listVehicles[0].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[1].Id, VIN__c=listVehicles[1].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[1].Id, VIN__c=listVehicles[2].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[2].Id, VIN__c=listVehicles[3].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[2].Id, VIN__c=listVehicles[4].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[2].Id, VIN__c=listVehicles[5].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[3].Id, VIN__c=listVehicles[6].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[3].Id, VIN__c=listVehicles[7].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[3].Id, VIN__c=listVehicles[8].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[3].Id, VIN__c=listVehicles[9].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[4].Id, VIN__c=listVehicles[10].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[4].Id, VIN__c=listVehicles[11].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[4].Id, VIN__c=listVehicles[12].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[4].Id, VIN__c=listVehicles[13].Id));
        listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[4].Id, VIN__c=listVehicles[14].Id));
        insert listRelations;
        
        //CHECK USER 1 - ASSOCIATED TO THE ACCOUNT 1
        User usr1 = [SELECT Id FROM User WHERE AccountId = :listAccUsers[0].Id];
        system.runAs(usr1) {
        	List<VRE_VehRel__c> listRelUsers = [SELECT Id FROM VRE_VehRel__c]; //should only access to its vehicule thanks to the sharing set on community
        	system.assertEquals(1, listRelUsers.size()); 
        	system.assertEquals(listRelations[0].Id, listRelUsers[0].Id);
        }
    }
    
    //TEST: Check that a community user has only access to their customer messages
    static testMethod void test_Community_AccessCustomerMessages() {
    	//Prepare a list of accounts
        List<Account> listAccUsers = Myr_Datasets_Test.insertPersonalAccounts( 5, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
        //take a template with no merge option
        Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
        system.AssertNotEquals(null, template);
        system.AssertEquals(null, template.MessageType__r.MergeAction__c);
        //Prepare a list of messages
        List<Customer_Message__c> listMessages = new List<Customer_Message__c>();
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[0].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') ); 
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[1].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[1].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[2].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[2].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[2].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[3].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[3].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[3].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[3].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[4].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[4].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[4].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[4].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        listMessages.add( new Customer_Message__c( Account__c=listAccUsers[4].Id, Template__c=template.Id, Channel__c='sms', Title__c='fake title', Summary__c='fake summary', Body__c='fake body') );
        insert listMessages;
        
        //CHECK USER 2: SHOULD SEE ONLY 2 MESSAGES
        User usr2 = [SELECT Id FROM User WHERE AccountId = :listAccUsers[1].Id];
        system.runAs( usr2 ) {
        	List<Customer_Message__c> messagesUsr2 = [SELECT Id FROM Customer_Message__c];
        	system.assertEquals(2, messagesUsr2.size());
        }
    }
    
    //TEST: Check that a customer message technical user has access to the CreateCustMessage WS
    static testMethod void test_CustMessage_AccessWS() {
    	//The goal is not to test the webservice itself, just the access to it to possible regressions
    	User custMsgUser = Myr_Datasets_Test.getCustMsgUser('Myr', 'SecurityCheck', false);
    	
    	//====> ACCESS TO CREATECUSTMESSAGES
    	system.runAs(custMsgUser) {
    		Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{} );
    		system.assertNotEquals(null, response);
    		system.assertNotEquals(null, response.info);
  			system.assertNotEquals(null, response.info.code);
  			system.assertNotEquals(null, response.info.message);
  			system.assertEquals('WS06MS501', response.info.code);
  			system.assertEquals('Global Failure: No messages to treat (input list is empty)', response.info.message);
    	}
    }
    
    //TEST: Check that a customer message technical user has access to the Account/User WS
    static testMethod void test_Tek_AccessWS() {
    	//The goal is not to test the webservice themselves, just the access to it to possible regressions
    	User userTek = Myr_Datasets_Test.getTechnicalUser('France');
    	    	
    	//====> ACCESS TO CHECKUSERNAME
    	system.runAs(userTek) {
    		Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = Myr_CheckUsername_WS.isAvailable('test@test.fr', 'Renault');
    		system.assertNotEquals(null, response);
        	system.assertEquals('WS05MS502', response.info.code);
        	system.assertEquals(true, response.info.message.contains('has not the proper format'));
        }
    	
    	//====> ACCESS TO MANAGEACCOUNT
    	system.runAs(userTek) {
    		Myr_ManageAccount_WS.Myr_ManageAccount_WS_Response response = Myr_ManageAccount_WS.manageAccount('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
    		system.assertNotEquals(null,response);
    		system.assertEquals(null,response.accountSfdcId);
    		system.assertEquals('WS01MS501',response.info.code);
    		system.assertEquals(true,response.info.message.contains('Value(s) missing : Parameter(s)'));
    	}
    	
    	//====> ACCESS TO CHANGEEMAIL
    	system.runAs(userTek) {
        	Myr_ChangeEmail_WS.Myr_ChangeEmail_WS_Response response = Myr_ChangeEmail_WS.changeEmail('','');
        	system.assertNotEquals(null, response);
        	system.assertEquals('WS04MS501', response.info.code);
        	system.assertEquals(true, response.info.message.contains('Value(s) missing : Parameter(s)'));
        	system.assertEquals(true, response.info.message.contains('accountSfdcId'));
        	system.assertEquals(true, response.info.message.contains('emailAddress'));
        }
    	
    	//====> ACCESS TO USERACTIVATION
        system.runAs(userTek) {
            Myr_UserActivation_WS.Myr_UserActivation_WS_Response response = Myr_UserActivation_WS.activateUser('', '', '', '', '', '', '');
        	system.assertNotEquals(null, response); 
        	system.assertEquals('WS02MS501', response.info.code);
        	system.assertEquals(true, response.info.message.contains('Value(s) missing : Parameter(s)'));
        }
    	
    	//====> ACCESS TO USERDEACTIVATION
      	system.runAs(userTek) {
            Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = Myr_UserDeActivation_WS.deActivateUser('', '');
        	system.assertNotEquals(null, response);
        	system.assertEquals('WS03MS504', response.info.code);
        	system.assertEquals(true, response.info.message.contains('Value missing : Parameter'));
        }
    }
    
    //TEST: Check that a dealer user has access to the ManageAccount and CheckUsername WS 
    static testMethod void test_Dealer_AccessWS() {
    	//The goal is not to test the webservice themselves, just the access to it to possible regressions
    	User userDealer = Myr_Datasets_Test.getDealerUser('Myr', 'TestDealerUser', 'France');
    	    	
    	//====> ACCESS TO CHECKUSERNAME
    	system.runAs(userDealer) {
    		Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = Myr_CheckUsername_WS.isAvailable('test@test.fr', 'Renault');
    		system.assertNotEquals(null, response);
        	system.assertEquals('WS05MS502', response.info.code);
        	system.assertEquals(true, response.info.message.contains('has not the proper format'));
        }
    	
    	//====> ACCESS TO MANAGEACCOUNT
    	system.runAs(userDealer) {
    		Myr_ManageAccount_WS.Myr_ManageAccount_WS_Response response = Myr_ManageAccount_WS.manageAccount('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
    		system.assertNotEquals(null,response);
    		system.assertEquals(null,response.accountSfdcId);
    		system.assertEquals('WS01MS501',response.info.code);
    		system.assertEquals(true,response.info.message.contains('Value(s) missing : Parameter(s)'));
    	}

		//====> CHECK ACCOUNT CREATION USING MANAGEACCOUNT
		String firstname = 'Roger';
		String lastname = 'Dubranchu';
		String email = 'roger.dubranchu@yopmail.com';
		system.runAs(userDealer) {
			Myr_ManageAccount_WS.Myr_ManageAccount_WS_Response response = Myr_ManageAccount_WS.manageAccount('Renault','France','MYR','STANDARD','','PARIS','1 avenue','90000','','',email,'012345678','',firstname,'',lastname,'','','','','','Y','','','','','','','',null,null);
			system.assertNotEquals(null,response);
			system.assertEquals('WS01MS000', response.info.code);
    		system.assertNotEquals(null,response.accountSfdcId);
		}

		//====> CHECK ACCOUNT CREATION USING CREATEACCOUNT
		Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question req = new Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question();
		req.country='France';
		req.accountSource='MYR';
		req.accountSubSource='STANDARD';
		req.emailAddress='albert.seoual@yopmail.com';
		req.firstname='Albert';
		req.lastname='Semoule';
		req.accountBrand='Renault';
		system.runAs( userDealer ) {
    		Myr_CreateAccount_WS.Myr_CreateAccount_WS_Response response = Myr_CreateAccount_WS.createAccount(req);
			system.assertNotEquals(null, response);
			system.assertEquals('WS01MS000',response.info.code);
			system.assertNotEquals(null, response.accountSfdcId);
		}
    }

	//Community user: right to modify his ownr profile (just some fields not all)
	static testMethod void test_Community_ProfileModification() {
		//Prepare a list of accounts
        Account acc = Myr_Datasets_Test.insertPersonalAccounts( 1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS )[0];
		User communityUsr = [SELECT Id FROM User Where AccountId=:acc.Id];
		//Connect as a community user and try to modify the profile
		system.runAs( communityUsr ) {
			Account communityAcc = [SELECT Id, Firstname, Lastname, BillingStreet, BillingCity, MyRenaulTID__c, HomePhone__c
								, MYR_Dealer_1_BIR__c, MYR_Dealer_2_BIR__c, Salutation_Digital__c
								, ComAgreemt__c, PersEmail__pc
							FROM Account];
			communityAcc.Firstname = 'New Lastname community';
			communityAcc.Lastname = 'New Firstname Community';
			communityAcc.BillingStreet = 'New Street Community';
			communityAcc.BillingCity = 'New City Community';
			communityAcc.MyRenaulTID__c = 'new.community@email.com';
			communityAcc.MYR_Dealer_1_BIR__c = '12345678';
			communityAcc.MYR_Dealer_2_BIR__c = '87654321';
			communityAcc.Salutation_Digital__c = 'Mr.';
			communityAcc.PersEmail__pc = 'Yes';
			update communityAcc;
		}
		//Check the result
		acc = [SELECT Id, Firstname, Lastname, BillingStreet, BillingCity, MyRenaulTID__c, HomePhone__c
								, MYR_Dealer_1_BIR__c, MYR_Dealer_2_BIR__c, Salutation_Digital__c
								, ComAgreemt__c, PersEmail__pc
							FROM Account];
		system.assertEquals( 'New Lastname community', acc.Firstname);
		system.assertEquals( 'New Firstname Community',	acc.Lastname);
		system.assertEquals( 'New Street Community', acc.BillingStreet);
		system.assertEquals( 'New City Community', acc.BillingCity);
		system.assertEquals( 'new.community@email.com',	acc.MyRenaulTID__c);
		system.assertEquals( '12345678', acc.MYR_Dealer_1_BIR__c);
		system.assertEquals( '87654321', acc.MYR_Dealer_2_BIR__c);
		system.assertEquals( 'Mr.',	acc.Salutation_Digital__c);
		system.assertEquals( 'Yes',	acc.PersEmail__pc);

	}
	
	//Community user: right to add a vehicle and the corresponding relation
	static testMethod void test_Community_AddVehicle() {
		//Prepare a list of accounts
        Account acc = Myr_Datasets_Test.insertPersonalAccounts( 1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS )[0];
		User communityUsr = [SELECT Id FROM User Where AccountId=:acc.Id];
		//Connect as a community user and try to add the vehicle
		system.runAs( communityUsr ) {
			Account communityAcc = [SELECT Id FROM Account];
			VEH_Veh__c veh = new VEH_Veh__c( Name = 'VF4567654567898RF');
			insert veh;
			VRE_VehRel__c vre = new VRE_VehRel__c( Account__c = communityAcc.Id, VIN__c = veh.Id, TypeRelation__c='Owner');
			insert vre;
		}
		List<VEH_Veh__c> listNewVeh = [SELECT Id, Data_Completion_SLK_Needed__c, Data_Completion_Eticom_Needed__c, Data_Completion_BVM_Needed__c FROM VEH_Veh__c WHERE Name='VF4567654567898RF'];
		system.assertEquals( 1, listNewVeh.size() );
		system.assertEquals( true, listNewVeh[0].Data_Completion_SLK_Needed__c );
		system.assertEquals( true, listNewVeh[0].Data_Completion_Eticom_Needed__c );
		system.assertEquals( true, listNewVeh[0].Data_Completion_BVM_Needed__c );
		List<VRE_VehRel__c> listNewRelation = [SELECT Id FROM VRE_VehRel__c WHERE Account__c = :acc.Id AND VIN__c = :listNewVeh[0].Id];
		system.AssertEquals( 1, listNewRelation.size() );
	}
    
}