global class AccountCaseCorrectionBatch implements Database.Batchable<sObject> {
	
	String query;
	Id recType = Utils.getRecordTypeId('Account', 'Personal_Acc');
	DateTime daysBefore = System.today().addDays(-(Integer.ValueOf(Label.DaysBeforeModify)));
	
	global AccountCaseCorrectionBatch() {

		this.query 	= 'SELECT Id, AccountId, Type, Status, CreatedDate, ClosedDate '
					+ 'FROM Case '
					+ 'WHERE Case.Account.RecordTypeId =: recType AND LastModifiedDate >=: daysBefore';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		AccountCaseCorrection instance = new AccountCaseCorrection(scope);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}