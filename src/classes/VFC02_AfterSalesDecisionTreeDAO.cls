/**
*	Class   -   VFC02_AfterSalesDecisionTreeDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*    
*   #01 <RameshPrabu> <31/08/2012>
*        Created this Class to handle record from After Sales Decision Tree related Queries.
*/
public with sharing class VFC02_AfterSalesDecisionTreeDAO {
	private static final VFC02_AfterSalesDecisionTreeDAO instance = new VFC02_AfterSalesDecisionTreeDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC02_AfterSalesDecisionTreeDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC02_AfterSalesDecisionTreeDAO getInstance(){
        return instance;
    }
    
    /**
	* This Method was used to get After Sales Decision Record based on some lead fields
 	* @param currentMilage - This field using from current milage of lead object             
    * @param afterSalesOffer This field using from after sales affer of lead object
    * @param rfv This field using from rfv of lead object
    * @return lstAfterSales - fetch and return the result in lstAfterSales list
    */
    public List<AST_AfterSalesDecisionTree__c> findAfterSalesDecisionTree(Double currentMilage, Boolean afterSalesOffer, String rfv){
        List<AST_AfterSalesDecisionTree__c> lstAfterSales = null;
        
        lstAfterSales = [select 
        					Id, 
        					Name, 
        					CurrentMilage__c, 
        					Recommendation1__c, 
        					Recommendation2__c 
        				from 
        					AST_AfterSalesDecisionTree__c
                		where  
                			AfterSalesOffer__c = :afterSalesOffer 
                			And RFV__c = :rfv
                			And (FromMileage__c <= :currentMilage and ToMileage__c >= :currentMilage)];
        
        return lstAfterSales;
    }
    
    /**
	* This Method was used to get After Sales Decision Record based on some lead fields	             
    * 	@param afterSalesOffer This field using from after sales affer of lead object
    *	@param rfv This field using from rfv of lead object
    *	@return lstAfterSales - fetch and return the result in lstAfterSales list
    */
    public List<AST_AfterSalesDecisionTree__c> findAfterSalesDecisionTreeWithNullCurrentMilage( Boolean afterSalesOffer, String rfv ){
        List<AST_AfterSalesDecisionTree__c> lstAfterSales = null;
        
        lstAfterSales = [select 
        					Id, 
        					Name, 
        					CurrentMilage__c, 
        					Recommendation1__c, 
        					Recommendation2__c 
        				from 
        					AST_AfterSalesDecisionTree__c
                		where  
                			AfterSalesOffer__c = :afterSalesOffer 
                			AND RFV__c = :rfv
                			AND FromMileage__c = null
	               			AND ToMileage__c = null];
        
        return lstAfterSales;
    }
}