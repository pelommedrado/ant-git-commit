/**
* Classe controladora da página de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC55_TestDriveController 
{
    private String opportunityId;
    public VFC54_TestDriveVO testDriveVO {get;set;}
    
    public VFC55_TestDriveController(ApexPages.StandardController controller) 
    {
        this.opportunityId = controller.getId();
    }
    
    public PageReference initialize()
    {
        this.testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().getTestDriveByOpportunity(this.opportunityId);
        
        this.initializeTestDrive();
            
        this.verifyStatus();
        
        return null;
    }
    
    private void initializeTestDrive()
    {
        this.testDriveVO.lstAgendaVehicle = new List<List<VFC64_ScheduleTestDriveVO>>();
        
        this.testDriveVO.lstSelOptionReasonCancellation = VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.ReasonCancellation__c, '');
        this.testDriveVO.lstSelOptionFuelOutputLevel = VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.FuelOutputLevel__c, '');
        this.testDriveVO.lstSelOptionFuelReturnLevel = VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.FuelReturnLevel__c, '');
        
        /*constroi o picklist de status*/
        this.buildPicklistStatus();
        
        /*constroi o picklist de veículos de interesse*/
        this.buildPicklistVehiclesInterest();   
        
        /*constroi o picklist de veículos disponíveis*/
        this.buildPicklistVehiclesAvailable();
            
        /*constroi a agenda do primeiro veículo disponível*/
        this.buildAgendaVehicle();
    }
    
    /**
    * Cria o picklist de status.
    * Inicialmente só 2 opções estarão disponíveis: a primeira será o valor do campo Status e a segunda a opção Não Realizado.
    */
    private void buildPicklistStatus()
    {
        Set<String> setValues = new Set<String>{this.testDriveVO.status, 'Not Performed','Performed'};
        
        this.testDriveVO.lstSelOptionStatus = VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.Status__c, setValues);
    }
    
    private void buildPicklistVehiclesInterest()
    {
        List<SelectOption> lstSelOption = new List<SelectOption>();
        
        for(Product2 sObjModel : this.testDriveVO.lstSObjModel)
        {
            lstSelOption.add(new SelectOption(sObjModel.Name, sObjModel.Name));
        }
        
        this.testDriveVO.lstSelOptionVehicleInterest = lstSelOption;
        
        /*essa lista não é mais necessária*/
        this.testDriveVO.lstSObjModel = null;       
    }
    
    private void buildPicklistVehiclesAvailable()
    {
        List<SelectOption> lstSelOption = new List<SelectOption>();
        
        if(this.testDriveVO.lstSObjTestDriveVehicleAvailable != null)
        {
            for(TDV_TestDriveVehicle__c sObjTestDriveVehicle : this.testDriveVO.lstSObjTestDriveVehicleAvailable)
            {
                if(String.isNotEmpty(sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c))
                {
                    lstSelOption.add(new SelectOption(sObjTestDriveVehicle.Id, sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c));
                }           
            }
            
            /*essa lista não é mais necessária*/
            this.testDriveVO.lstSObjTestDriveVehicleAvailable = null;
        }
        
        this.testDriveVO.lstSelOptionVehiclesAvailable = lstSelOption;                  
    }
    
    public void verifyStatus()
    {
        /*verifica se o status do test drive está confirmado*/
        if(this.testDriveVO.status.equals('Confirmed'))
        {           
            /*seta as informações da confirmação*/
            this.testDriveVO.dateBookingFmt = this.testDriveVO.dateBooking.format() + 'hs';     
            this.testDriveVO.vehicleScheduled = this.testDriveVO.model + ' ' + this.testDriveVO.plaque;
        }
    }
        
    /**
    * Trata o evento onchange do campo status.
    * Quando a opção selecionada for Not Performed, somente o campo motivo cancelamento ficará habilitado, os demais serão bloqueados.
    */
    public void processStatusChange()
    {
        if(this.testDriveVO.status.equals('Not Performed'))
        {
            this.testDriveVO.reasonCancellationDisabled = false;
            this.testDriveVO.disabledFields = true;
            this.testDriveVO.sObjTestDrive.DepartureTime__c = null;
            this.testDriveVO.sObjTestDrive.ReturnTime__c = null;
            this.testDriveVO.reasonCancellation = '';       
            this.testDriveVO.outputKM = ''; 
            this.testDriveVO.returnKM = '';
            this.testDriveVO.fuelReturnLevel = '';
            this.testDriveVO.fuelOutputLevel = '';      
            this.testDriveVO.design = '';
            this.testDriveVO.internalSpace = '';
            this.testDriveVO.handling = '';
            this.testDriveVO.security = '';
            this.testDriveVO.performance = '';
            this.testDriveVO.panelCommands = '';
            this.testDriveVO.equipmentLevel = '';               
            this.testDriveVO.buyingInterestAfterTestDrive = ''; 
            
            /*marca os horários disponíveis da agenda como bloqueados*/
            this.markAvailableAsBlocked();
        }
        else
        {
            this.testDriveVO.reasonCancellationDisabled = true;
            this.testDriveVO.disabledFields = false;
            this.testDriveVO.reasonCancellation = '';
            
            /*marca os horários bloqueados da agenda como disponíveis*/
            this.markBlockedAsAvailable();                      
        }   
    }
    
    /**
    * Trata o evento onchange do campo modelo desejado (veículo de interesse).
    */
    public void processVehicleInterestChange()
    {
        VFC54_TestDriveVO testDriveVOResponse = VFC56_TestDriveBusinessDelegate.getInstance().getVehiclesAvailable(this.testDriveVO);
        
        /*armazena a lista de veículos disponíveis no VO principal*/
        this.testDriveVO.lstSObjTestDriveVehicleAvailable = testDriveVOResponse.lstSObjTestDriveVehicleAvailable;
            
        /*constroi o picklist de veículos disponíveis*/
        this.buildPicklistVehiclesAvailable();
        
        /*seta os detalhes do primeiro veículo disponível*/
        this.testDriveVO.model = testDriveVOResponse.model;
        this.testDriveVO.version = testDriveVOResponse.version;
        this.testDriveVO.color = testDriveVOResponse.color;
        this.testDriveVO.plaque = testDriveVOResponse.plaque;
        this.testDriveVO.dateBookingNavigation = testDriveVOResponse.dateBookingNavigation;
        this.testDriveVO.selectedTime = '';
        
        /*armazena a agenda do primeiro veículo no VO principal*/
        this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
        
        /*constroi a agenda do primeiro veículo disponível*/
        this.buildAgendaVehicle();
        
        /*armazena a lista de documentos RVA*/
        this.testDriveVO.lstDocumentationRVA = testDriveVOResponse.lstDocumentationRVA;                 
    }
    
    /**
    * Trata o evento onchange do campo veículos disponíveis.
    */
    public void processVehicleAvailableChange()
    {
        VFC54_TestDriveVO testDriveVOResponse = null;
        
        /*seta a data atual no atributo dateBooking para obter a agenda do dia inicialmente*/
        this.testDriveVO.dateBookingNavigation = Date.today();
            
        /*obtém os detalhes do veículo*/
        testDriveVOResponse = VFC56_TestDriveBusinessDelegate.getInstance().getVehicleDetails(this.testDriveVO);
        
        /*seta os detalhes do veículo selecionado*/ 
        this.testDriveVO.model = testDriveVOResponse.model;
        this.testDriveVO.version = testDriveVOResponse.version;
        this.testDriveVO.color = testDriveVOResponse.color;
        this.testDriveVO.plaque = testDriveVOResponse.plaque;
        this.testDriveVO.selectedTime = '';
        
        /*seta a agenda do veículo no VO principal*/
        this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
        
        /*constroi a agenda do veículo selecionado*/    
        this.buildAgendaVehicle();
        
    }
    
    public void getAgendaNextDay()
    {
        VFC54_TestDriveVO testDriveVOResponse = null;
        
        /*incrementa um dia no datebooking para obter a agenda do dia seguinte*/
        this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation + 1;
        
        /*obtém a agenda do dia seguinte*/
        testDriveVOResponse = VFC56_TestDriveBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
        
        /*seta a agenda do veículo no VO principal*/
        this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
        
        /*constroi a agenda*/
        this.buildAgendaVehicle();
    }
    
    public void getAgendaPreviousDay()
    {
        VFC54_TestDriveVO testDriveVOResponse = null;
        
        /*decrementa um dia no datebooking para obter a agenda de um dia atrás*/
        this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation - 1;
        
        /*obtém a agenda de um dia atrás*/
        testDriveVOResponse = VFC56_TestDriveBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
        
        /*seta a agenda do veículo no VO principal*/
        this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
        
        /*constroi a agenda*/
        this.buildAgendaVehicle();      
    }
    
    private void buildAgendaVehicle()
    {
        VFC64_ScheduleTestDriveVO scheduleVO = null; 
        String hourAux = null;
        String minuteAux = null;
        TDV_TestDrive__c sObjTestDriveAux = null;
        Map<String, TDV_TestDrive__c> mapTestDrive = null;
        Date currentDate = null;
        DateTime startDate = null;
        DateTime endDate = null;
        List<VFC64_ScheduleTestDriveVO> lstAux = null;
        
        this.testDriveVO.lstAgendaVehicle.clear();
        
        if(this.testDriveVO.lstSObjTestDriveAgenda != null)
        {
            mapTestDrive = new Map<String, TDV_TestDrive__c>();
            currentDate = Date.today();
            startDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 8, 0, 0);      
            endDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 20, 0, 0);
            lstAux = new List<VFC64_ScheduleTestDriveVO>();
            
            for(TDV_TestDrive__c sObjTestDrive : this.testDriveVO.lstSObjTestDriveAgenda)
            {
                hourAux = sObjTestDrive.DateBooking__c.hour().format();
                minuteAux = + sObjTestDrive.DateBooking__c.minute().format();
            
                mapTestDrive.put(hourAux + minuteAux, sObjTestDrive);
            }
            
            while(startDate <= endDate)
            {
                scheduleVO = new VFC64_ScheduleTestDriveVO();
                        
                hourAux = startDate.hour() < 10 ? ('0' + startDate.hour()) : (startDate.hour().format());
            
                minuteAux = startDate.minute() < 10 ? ('0' + startDate.minute()) : (startDate.minute().format());
            
                scheduleVO.hour = hourAux + ':' + minuteAux;
                    
                hourAux = startDate.hour().format();
                minuteAux = startDate.minute().format();
            
                /*obtém do map um test drive com esse horário*/
                sObjTestDriveAux = mapTestDrive.get(hourAux + minuteAux);
            
                /*verifica se foi retornado null, o que indica que esse horário está disponível*/
                if(sObjTestDriveAux == null)
                {
                    scheduleVO.schedulingStatus = 'available';
                }
                else
                {
                    if(sObjTestDriveAux.Id == this.testDriveVO.id)
                    {                   
                        scheduleVO.schedulingStatus = 'confirmed';
                    }
                    else
                    {
                        scheduleVO.schedulingStatus = 'scheduled';
                    }
                }
                    
                lstAux.add(scheduleVO);
            
                if(lstAux.size() == 5)
                {       
                    this.testDriveVO.lstAgendaVehicle.add(lstAux);
                
                    lstAux = new List<VFC64_ScheduleTestDriveVO>();
                }
                            
                startDate = startDate.addMinutes(30);   
            }
        
            this.testDriveVO.dateBookingNavigationFmt = this.testDriveVO.dateBookingNavigation.format();
            
            /*essa lista não é mais necessária*/
            this.testDriveVO.lstSObjTestDriveAgenda = null;
        }               
    }
    
    public void confirmSchedulingTestDrive()
    {
        String currentStatus = null;    
        DateTime currentDateBooking = null; 
        String[] arrSelectedTime = null;
        Integer hour = null;
        Integer minutes = null;
        String strMessage = null;
        ApexPages.Message message = null;
        
        /*armazena o status atual do test drive*/
        currentStatus = this.testDriveVO.status;
        
        /*armazena a data de agendamento atual*/
        currentDateBooking = this.testDriveVO.dateBooking;
        
        /*altera o status do test drive para confirmado*/
        this.testDriveVO.status = 'Confirmed';
        
        /*faz o split do horário no formato HH:mm para a hora e os minutos separados*/          
        arrSelectedTime = this.testDriveVO.selectedTime.split(':');
            
        /*converte a hora e os minutos para inteiro*/
        hour = Integer.valueOf(arrSelectedTime[0]);
        minutes = Integer.valueOf(arrSelectedTime[1]);
            
        /*cria o datebooking completo*/
        this.testDriveVO.dateBooking = DateTime.newInstance(this.testDriveVO.dateBookingNavigation.year(), this.testDriveVO.dateBookingNavigation.month(), 
                                                           this.testDriveVO.dateBookingNavigation.day(), hour,
                                                           minutes, 0); 
                                                                                                                                                                                                                                                            
        try
        {
            VFC56_TestDriveBusinessDelegate.getInstance().updateTestDrive(this.testDriveVO);
            
            this.testDriveVO.dateBookingFmt = this.testDriveVO.dateBooking.format() + 'hs';
            
            this.testDriveVO.vehicleScheduled = this.testDriveVO.model + ' ' + this.testDriveVO.plaque;
            
            /*marca o horário na lista como confirmado*/
            this.markAsConfirmed();
            
            /*constroi novamente o picklist de status*/
            this.buildPicklistStatus();
        }
        catch(VFC59_UpdateTestDriveException ex)
        {       
            /*volta para o status que estava*/
            this.testDriveVO.status = currentStatus;
            
            /*volta para a data de agendamento que estava*/
            this.testDriveVO.dateBooking = currentDateBooking;
            
            strMessage = 'Não foi possível confirmar o agendamento do Test Drive. Motivo: \n' + ex.getMessage();
                    
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            
            ApexPages.addMessage(message);
        }
            
    }
    
    private void markAsConfirmed()
    {
        for(List<VFC64_ScheduleTestDriveVO> lstAux : this.testDriveVO.lstAgendaVehicle)
        {
            for(VFC64_ScheduleTestDriveVO scheduleVO : lstAux)
            {               
                if(scheduleVO.hour.equals(testDriveVO.selectedTime))
                {
                    scheduleVO.schedulingStatus = 'confirmed';
                }       
                
                else if(scheduleVO.schedulingStatus.equals('confirmed'))
                {
                    scheduleVO.schedulingStatus = 'available';
                }                   
            }
        }
    }
    
    private void markAvailableAsBlocked()
    {
        /*torna os links de navegação da agenda*/
        this.testDriveVO.navigationAgendaVisible = false;
        
        for(List<VFC64_ScheduleTestDriveVO> lstAux : this.testDriveVO.lstAgendaVehicle)
        {
            for(VFC64_ScheduleTestDriveVO scheduleVO : lstAux)
            {                               
                if(scheduleVO.schedulingStatus.equals('available'))
                {
                    scheduleVO.schedulingStatus = 'blocked';
                }                   
            }
        }
    }
    
    private void markBlockedAsAvailable()
    {
        /*torna os links de navegação da agenda*/
        this.testDriveVO.navigationAgendaVisible = true;
        
        for(List<VFC64_ScheduleTestDriveVO> lstAux : this.testDriveVO.lstAgendaVehicle)
        {
            for(VFC64_ScheduleTestDriveVO scheduleVO : lstAux)
            {                               
                if(scheduleVO.schedulingStatus.equals('blocked'))
                {
                    scheduleVO.schedulingStatus = 'available';
                }                   
            }
        }
    }
    
    /**
    * Trata a ação do botão concluir test drive.
    */
    public void completeTestDrive()
    {
        String currentStatus = null;
        String strMessage = null;
        ApexPages.Message message = null;
        
        if(this.validateFields())
        {
            /*armazena o status atual do test drive*/
            currentStatus = this.testDriveVO.status;
            
            /*se o status do test drive for confirmado, altera para realizado*/
            if(this.testDriveVO.status.equals('Confirmed'))
            {
                this.testDriveVO.status = 'Performed';
                
            }else if(testDriveVO.sObjTestDrive.DepartureTime__c != null && !this.testDriveVO.status.equals('Not Performed'))
            {
                this.testDriveVO.status = 'Performed';
            }
            
            try
            {
                            
                VFC56_TestDriveBusinessDelegate.getInstance().updateTestDrive(this.testDriveVO);
                
                /*sinaliza que o test drive foi atualizado*/
                this.testDriveVO.updatedTestDrive = true;
                                    
                strMessage = 'Test Drive salvo com sucesso.';
                
                message = new ApexPages.Message(ApexPages.Severity.CONFIRM, strMessage);
                            
                if(this.testDriveVO.status.equals('Performed'))
                {
                    this.buildPicklistStatus();
                }
                
                /*desabilita os campos da tela*/
                this.disableAllFields();
            }
            catch(VFC59_UpdateTestDriveException ex)
            {
                /*como houve erro na atualização do test drive, volta para o status que estava*/
                this.testDriveVO.status = currentStatus;
                
                strMessage = 'Não foi possível salvar o Test Drive. Motivo: \n' + ex.getMessage();
                    
                message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            }
            
            ApexPages.addMessage(message);
        }       
    }
    
    /**
    * Trata a ação do botão Novo Test Drive.
    */
    public void performNewTestDrive()
    {
        /*verifica se o test drive atual já foi atualizado (salvo)*/
        if(this.testDriveVO.updatedTestDrive)
        {
            /*cria um novo test drive*/
            this.createNewTestDrive();
        }   
        else
        {
            /*atualiza o test drive atual e cria um novo*/
            this.updateTestDriveAndCreateNew();
        }
    }
    
    private void createNewTestDrive()
    {
        String strMessage = null;
        ApexPages.Message message = null;
        
        try
        { 
            /*cria um novo test drive para a mesma oportunidade*/
            this.testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().createNewTestDrive(this.testDriveVO.opportunityId);
            
            /*inicializa o test drive*/ 
            this.initializeTestDrive(); 
        }
        catch(VFC58_CreateTestDriveException ex)
        {
            strMessage = 'Não foi possível criar o novo Test Drive. Motivo: \n' + ex.getMessage();
                
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                
            ApexPages.addMessage(message);
        }
    }
    
    private void updateTestDriveAndCreateNew()
    {
        String currentName = null;
        String currentStatus = null;
        String strMessage = null;
        ApexPages.Message message = null;
        
        if(this.validateFields())
        {
            /*guarda o nome do test drive atual*/
            currentName = this.testDriveVO.name;
            
            /*guarda o status do test drive atual*/
            currentStatus = this.testDriveVO.status;
            
            /*se o status do test drive for confirmado, altera para realizado*/
            if(this.testDriveVO.status.equals('Confirmed'))
            {
                this.testDriveVO.status = 'Performed';
            }
                
            try
            {                                                   
                /*atualiza o test drive atual e obtém o novo test drive criado*/
                this.testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().updateTestDriveAndCreateNew(this.testDriveVO);
                    
                /*inicializa o test drive*/ 
                this.initializeTestDrive();
                    
                strMessage = 'O Test Drive ' + currentName + ' foi salvo com sucesso e um novo Test Drive foi criado.';
                    
                message = new ApexPages.Message(ApexPages.Severity.CONFIRM, strMessage);    
            }
            catch(VFC59_UpdateTestDriveException ex)
            {
                /*como houve erro na atualização do test drive, volta para o status que estava*/
                this.testDriveVO.status = currentStatus;
                
                strMessage = 'Não foi possível salvar o Test Drive ' + currentName + '. Motivo: \n' + ex.getMessage();
                    
                message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            }
            catch(VFC58_CreateTestDriveException ex)
            {
                /*sinaliza que o test drive atual foi atualizado (somente o novo que não foi criado)*/
                this.testDriveVO.updatedTestDrive = true;
                    
                /*desabilita os campos da tela*/
                this.disableAllFields();
                    
                strMessage = 'O Test Drive ' + currentName + ' foi salvo com sucesso, mas não foi possível criar o novo Test Drive. Motivo: \n' + ex.getMessage();
                    
                message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            }
                
            ApexPages.addMessage(message);
        }
    }
    
    /**
    * Valida os campos da tela de acordo com o valor do campo status.
    * @return true se todos os campos estiverem preenchidos, caso contrário, retorna false.
    */
    private Boolean validateFields()
    {
        Boolean validationOK = null;
        
        if(this.testDriveVO.status.equals('Not Performed'))
        {
            validationOK = this.validateFieldsForCancellationTestDrive();
        }
        else
        {
            validationOK = this.validateFieldsForCompleteTestDrive();
        }
        
        return validationOK;
    }
    
    /**
    * Valida os campos da tela para o cenário de cancelamento de test drive.
    * @return true se todos os campos estiverem devidamente preenchidos, caso contrário, retorna false.
    */
    private Boolean validateFieldsForCancellationTestDrive()
    {
        Apexpages.Message message = null;
        Boolean fieldValidated = true;
        
        if(String.isEmpty(this.testDriveVO.reasonCancellation))
        {               
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Motivo Cancelamento.');
            
            ApexPages.addMessage(message);
            
            fieldValidated = false;
        }
        
        return fieldValidated;
    }
        
    /**
    * Valida os campos da tela para o cenário de conclusão de test drive.
    * @return true se todos os campos estiverem devidamente preenchidos, caso contrário, retorna false.
    */
    private Boolean validateFieldsForCompleteTestDrive()
    {
        Apexpages.Message message = null;
        Boolean fieldsValidated = true;
        
        if(this.testDriveVO.sObjTestDrive.DepartureTime__c == null)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Horário de Saída.');
        }
        
        else if(String.isEmpty(this.testDriveVO.outputKM))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo KM de Saída.');
        }
        
        else if(!this.testDriveVO.outputKM.isNumeric())
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'O campo KM de Saída deve conter somente números.');
        }
        
        else if(String.isEmpty(this.testDriveVO.fuelOutputLevel))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Nível Combustível Saída.');
        }
        
        else if(this.testDriveVO.sObjTestDrive.ReturnTime__c == null)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Horário de Retorno.');
        }
            
        else if(this.testDriveVO.sObjTestDrive.ReturnTime__c <= this.testDriveVO.sObjTestDrive.DepartureTime__c)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'O Horário de Retorno deve ser maior que o Horário de Saída.');
        }
                
        else if(String.isEmpty(this.testDriveVO.returnKM))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo KM de Retorno.');
        }
        
        else if(!this.testDriveVO.returnKM.isNumeric())
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'O campo KM de Retorno deve conter somente números.');
        }
        
        else if((Integer.valueOf(this.testDriveVO.returnKM)) <= (Integer.valueOf(this.testDriveVO.outputKM)))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'A KM de Retorno deve ser maior que a KM de Saída.');
        }
                                                    
        else if(String.isEmpty(this.testDriveVO.fuelReturnLevel))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Nível Combustível Retorno.');
        }
        
        //else if(this.testDriveVO.status.equals('Open'))
        //{
        //    message = new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário confirmar um horário para o Test Drive.');
        //}
            
        else if(String.isEmpty(this.testDriveVO.design))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Design.');
        }   
            
        else if(String.isEmpty(this.testDriveVO.internalSpace))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Espaço Interno.');
        }   
            
        else if(String.isEmpty(this.testDriveVO.handling))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Dirigibilidade (Estabilidade e Conforto).');
        }   
            
        else if(String.isEmpty(this.testDriveVO.security))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Segurança.');
        }
            
        else if(String.isEmpty(this.testDriveVO.performance))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Desempenho.');
        }   
            
        else if(String.isEmpty(this.testDriveVO.panelCommands))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Painel e Comandos.');
        }   
            
        else if(String.isEmpty(this.testDriveVO.equipmentLevel))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Nível de Equipamentos.');
        }
            
        else if(String.isEmpty(this.testDriveVO.buyingInterestAfterTestDrive))
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha o campo Interesse Após Test Drive.');
        }           
                
        if(message != null)
        {
            ApexPages.addMessage(message);
            
            fieldsValidated = false;
        }
        
        return fieldsValidated;
    }
    
    /**
    * Desabilita os campos da tela.
    */
    private void disableAllFields()
    {
        this.testDriveVO.statusDisabled = true;
        this.testDriveVO.disabledFields = true;     
        this.testDriveVO.reasonCancellationDisabled = true;
        this.testDriveVO.commentsDisabled = true;
        this.testDriveVO.btnCompleteTestDriveDisabled = true;
        this.markAvailableAsBlocked();
    }
    
    /**
    * Trata a ação do botão principal de geração de orçamento.
    */
    public PageReference verifyGenerateQuote()
    {
        PageReference pageRef = null;
        String strMessage = null;
        ApexPages.Message message = null;
        
        if(this.testDriveVO.updatedTestDrive)
        {
            try
            {
                VFC56_TestDriveBusinessDelegate.getInstance().updateOpportunityForQuoteStage(this.testDriveVO.opportunityId);
                
                pageRef = new PageReference('/apex/VFP11_Quote?Id=' + this.opportunityId);
            }
            catch(Exception ex)
            {
                /*TODO: verificar tratamento dessa exceção*/
                
                strMessage = 'Não foi possível alterar a oportunidade para a fase de orçamento. Motivo: \n' + ex.getMessage();
                            
                message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);
            
                ApexPages.addMessage(message);
            }
        }
        else
        {
            this.testDriveVO.titlePopUpGenerationQuote = 'Geração de Orçamento';
            this.testDriveVO.confirmationGenerationQuoteVisible = true;
            this.testDriveVO.reasonCancellationPopUpVisible = false;
        }
        
        return pageRef;
    }
    
    public void confirmGenerationQuote()
    {
        this.testDriveVO.titlePopUpGenerationQuote = 'Cancelamento de Test Drive';
        this.testDriveVO.confirmationGenerationQuoteVisible = false;
        this.testDriveVO.reasonCancellationPopUpVisible = true;
        this.testDriveVO.reasonCancellation = '';
        this.testDriveVO.btnGenerateQuoteDisabled = true;
    }
    
    public void processReasonCancellationTestDriveChange()
    {
        this.testDriveVO.btnGenerateQuoteDisabled = String.isEmpty(this.testDriveVO.reasonCancellation);
    }
    
    /**
    * Trata a ação do botão Gerar Orçamento (Pop-Up);
    */
    public PageReference generateQuote()
    {
        PageReference pageRef = null;
        String strMessage = null;
        ApexPages.Message message = null;
        
        try
        {
            VFC56_TestDriveBusinessDelegate.getInstance().cancelTestDriveAndUpdateOpportunityForQuoteStage(this.testDriveVO);
            
            pageRef = new PageReference('/apex/VFP11_Quote?Id=' + this.opportunityId);
        }
        catch(Exception ex)
        {
            /*TODO: verificar tratamento dessa exceção*/
            
            strMessage = 'Não foi possível alterar a oportunidade para a fase de orçamento. Motivo: \n' + ex.getMessage();
                        
            message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);
            
            ApexPages.addMessage(message);
        }
        
        return pageRef;
    }
    
    /**
    * Trata a ação do botão cancelar oportunidade.
    * Constroi o picklist com as opções de cancelamento da oportunidade para que o pop-up possa ser aberto.
    */
    public void openPopUpCancellation()
    {
        this.testDriveVO.reasonLossCancellationOpportunity = '';
        this.testDriveVO.btnCancelOpportunityDisabled = true;   
        
        /*verifica se o picklist ainda não foi construído*/
        if(this.testDriveVO.lstSelOptionReasonLossCancellationOpp == null)
        {
            /*cria o picklist que exibe as opções de cancelamento da oportunidade*/
            this.testDriveVO.lstSelOptionReasonLossCancellationOpp = VFC49_PickListUtil.buildPickList(Opportunity.ReasonLossCancellation__c, '');
        }
    }
    
    /**
    * Trata o evento onchange do campo razão de cancelamento da oportunidade (Pop-Up de cancelamento).
    */
    public void processReasonCancellationChange()
    {
        this.testDriveVO.btnCancelOpportunityDisabled = String.isEmpty(this.testDriveVO.reasonLossCancellationOpportunity);
    }
    
    /**
    * Trata a ação do botão cancelar oportunidade.
    */
    public PageReference cancelOpportunity()
    {
        PageReference pageRef = null; 
        String strMessage = null;
        ApexPages.Message message = null;
        
        try 
        {
            VFC56_TestDriveBusinessDelegate.getInstance().cancelOpportunity(this.testDriveVO);
            
            pageRef = new PageReference('/home/home.jsp'); 
        }
        catch(Exception ex)
        {
            /*TODO: verificar tratamento dessa exceção*/
            
            strMessage = 'Não foi possível cancelar a oportunidade. Motivo: \n' + ex.getMessage();
            
            message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);
            
            ApexPages.addMessage(message);
        }
        
        return pageRef;
    }
    
}