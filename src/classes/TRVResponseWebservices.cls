public with sharing class TRVResponseWebservices 
{
/** @author Ludovic Marboutie
  * @date 17/09/2015
  * Apex class repsenting a response type of a personal data webservice (SIC, BCS, MDM)
  **/
  
  		public PersonalDataResponse response {get;set;}
  		/** @constructor **/
		public TRVResponseWebservices() {
			InfoMsg info = new InfoMsg();
			response = new PersonalDataResponse();
			response.info = info; 
		}
  		
  		public class InfoMsg {
			public String codRetour {get;set;}
			public String msgRetour {get;set;}
		}
  
  		/** General Response of the WebService **/
		public class PersonalDataResponse {  
			public InfoMsg info {get;set;} 
			public String dataSource {get;set;}
			public String rawResponse {get; set;}
			public String rawQuery {get; set;}
			public Integer searchPartyResponse {get;set;}	//MDM
			public List<InfoClient> infoClients {get;set;}
			public PersonalDataResponse() 
			{
				infoClients = new List<InfoClient>();
			}
		}


		public class InfoClient {
			
	        //public String IdClient {get;set;}					//BCS/SIC
	        public String strPartyID {get;set;} 				//SF : TechAccExternalID__c/Bcs_Id__c/Bcs_Id_Dacia__c/Bcs_Id_Nissan__c/PeopleId_CN__c 	| MDM : PartyId 	| BCS : IdClient | RBX : IdClient
	        public String strAccountType{get;set;}				//SF : RecordTypeID 				| MDM : PartyType 						| BCS : typeperson 				| RBX : typeperson
	        public String FirstName1 {get;set;}					//SF : FirstName 					| MDM : FirstName1 						| BCS : firstname 				| RBX : firstname
	        public String LastName1 {get;set;}					//SF : LastName 					| MDM : LastName1 						| BCS : Lastname 				| RBX : lastname
	        public String FirstName2 {get;set;}					//SF : SecondFirstName__c 			| MDM : FistName2 						| BCS :  						| RBX : 
	        public String LastName2 {get;set;}					//SF : SecondSurname__c 			| MDM : LastName2						| BCS : middleName  			| RBX : middleName
	        public String name {get;set;}						//SF : name 						| MDM : OrganisationName				| BCS : lastName  				| RBX : 
	        public String strDOB {get;set;}						//SF : PersonBirthDate 				| MDM : DateOfBirth 					| BCS : birthDay  				| RBX : birthDay
	        public String title {get;set;}						//SF : title__c 					| MDM : title 							| BCS :   						| RBX : 
	        public String civility {get;set;}					//SF : Salutation 					| MDM : Civility 						| BCS : title  					| RBX : title
	        public String sex{get;set;}							//SF : sex__c 						| MDM : Sex 							| BCS : sex  					| RBX : sex
	        public String marital{get;set;}						//SF : MaritalStatus__c				| MDM : MaritalStatus 					| BCS : 	  					| RBX : 
	        public String Lang {get;set;}						//SF : Language__c					| MDM : Language	 					| BCS : lang  					| RBX : lang
	        public String renaultEmployee {get;set;}			//SF : Renault_Employee__c 			| MDM : RenaultGroupStaff 				| BCS : 	 					| RBX : 
	        public String nbrChildrenHome {get;set;}			//SF : nbrChildrenHome__c			| MDM : NumberOfChildrenAtHome 			| BCS : 	 					| RBX :
	        public String deceased {get;set;}					//SF : Deceased__c					| MDM : Deceased						| BCS : 	 					| RBX :
	        public String preferredMedia{get;set;}				//SF : PreferedMedia__c				| MDM : PreferredMedia					| BCS : Preferred.Communication.Method	 | RBX :
	        public String OccupationalCategoryCodeP {get;set;}	//SF : JobClass__c					| MDM : OccupationalCategoryCode		| BCS : 						| RBX :
	        public String OccupationalCategoryCode {get;set;}	//SF : Industry						| MDM : OccupationalCategoryCode		| BCS : 						| RBX :
	        public String OccupationalCategoryDescription {get;set;}//SF : 							| MDM : OccupationalCategoryDescription	| BCS : 						| RBX :
	        public String partySegment {get;set;}				//SF : SpecialCustmr__c				| MDM : PartySegment					| BCS : 						| RBX :
	        public String commercialName {get;set;}				//SF : UsualName__c					| MDM : CommercialName					| BCS : 						| RBX :
	        public Integer numberOfEmployees {get;set;}			//SF : NumberOfEmployees			| MDM : NumberOfEmployees				| BCS : 						| RBX :
	        public String financialStatus {get;set;}			//SF : Financial_Status__c			| MDM : FinancialStatus					| BCS : 						| RBX :
	        public String codeNAF {get;set;}					//SF : Code_NAF__c					| MDM : CompanyActivityDescription		| BCS : 						| RBX :
	        public String legalNature {get;set;}				//SF : legalForm__c					| MDM : legalNature						| BCS : 						| RBX :
	        public String partySub {get;set;}					//SF : Sub__c						| MDM : PartySubType					| BCS : 						| RBX :
	        public String idMyr {get;set;}					    //SF : My_Renault_ID_Account_number__c| MDM : 					| BCS : idMyr					| RBX : idMyr
	        
	        
	        //block adress
	        public String addressLine1 {get;set;}				//SF : BillingStreet				| MDM : AddressLine1					| BCS : strName 				| RBX : strName
	        public String addressLine2 {get;set;}				//SF : BillingStreet				| MDM : AddressLine2					| BCS : compl2	 				| RBX : compl2
	        public String strNum {get;set;}						//SF : BillingStreet				| MDM : 								| BCS : strNum	 				| RBX : strNum
	        public String strType {get;set;}					//SF : BillingStreet				| MDM : 								| BCS : StrType	 				| RBX : strType/strTypeLabel
	        public String compl1{get;set;}						//SF : BillingStreet				| MDM : AddressLine3					| BCS : compl1	 				| RBX : compl1
	        public String compl3{get;set;}						//SF : BillingStreet				| MDM : AddressLine4					| BCS : 		 				| RBX : compl3
	        public String city {get;set;}						//SF : BillingCity					| MDM : City							| BCS : city 					| RBX : city
	        public String region {get;set;}						//SF : BillingState					| MDM : Region							| BCS : areaCode 				| RBX : areaLabel/areaCode
	        public String zip {get;set;}						//SF : BillingPostalCode			| MDM : PostCode						| BCS : zip 					| RBX : zip
	        public String CountryCode {get;set;}				//SF : BillingCountry				| MDM : Country							| BCS : countryCode 			| RBX : countryCode
	        
	        public String PersonOtherStreet {get;set;}			//SF : PersonOtherStreet			| MDM : AddressLine1/2/3/4				| BCS :  						| RBX : 
	        public String PersonOtherCity {get;set;}			//SF : PersonOtherCity				| MDM : City							| BCS :  						| RBX : 
	        public String PersonOtherState {get;set;}			//SF : PersonOtherState				| MDM : Region							| BCS :  						| RBX : 
	        public String PersonOtherPostalCode {get;set;}		//SF : PersonOtherPostalCode		| MDM : PostCode						| BCS :  						| RBX : 
	        public String PersonOtherCountry {get;set;}			//SF : PersonOtherCountry			| MDM : Country							| BCS :  						| RBX : 
	        
	        
	        
	        public String email {get;set;}						//SF : PersEmailAddress__c			| MDM : ElectronicAddressValue			| BCS : email					| RBX : email
	        public String emailPro{get;set;}					//SF : ProfEmailAddress__c			| MDM : ElectronicAddressValue			| BCS : BCSemailPro				| RBX : 
	        
			public String strFixeLandLine{get;set;}				//SF : HomePhone__c 				| MDM : PhoneNumberValue				| BCS : phoneNum1				| RBX : phonecode1+phoneNum1
	        public String strFixeLandLinePro{get;set;}			//SF : ProfLandline__c 				| MDM : 								| BCS : phoneNum3				| RBX : phonecode2+phoneNum2
	        public String strMobile{get;set;}					//SF : PersMobPhone__c 				| MDM : PhoneNumberValue				| BCS : phoneNum2				| RBX : phonecode3+phoneNum3
			public String strMobilePro{get;set;}				//SF : ProfMobPhone__c 				| MDM : PhoneNumberValue				| BCS : 				| RBX : 

			public String prefDealer{get;set;}					//SF : PrefdDealer__c 				| MDM : DealerCode						| BCS : 						| RBX : 
	        public String prefDealerDacia{get;set;}				//SF : PrefdDealerDacia__c			| MDM : DealerCode						| BCS : 						| RBX : 
	       
	        public String companyID{get;set;}					//SF : CompanyID__c 				| MDM : IdentificationValue				| BCS : ident1					| RBX : ident1
	        public String secondCompanyID{get;set;}				//SF : Second_account_id__c 		| MDM : IdentificationValue				| BCS : ident2					| RBX : ident2
	       
	       	public String localCustomerID1{get;set;}			//SF : CustomerIdentificationNbr__c | MDM : IdentificationValue				| BCS : ident1					| RBX : ident1
	        public String localCustomerID2{get;set;}			//SF : CustomerIdentificationNbr2__c| MDM : IdentificationValue				| BCS : ident2					| RBX : ident2
	       
	        public String Siren{get;set;}						//SF : 								| MDM : Siren							| BCS : ident2					| RBX : ident2
	        public String Siret{get;set;}						//SF : 								| MDM : Siret							| BCS : ident1					| RBX : ident
	        
	        public String GlobalCommAgreement {get;set;} 		//SF : ComAgreemt__c				| MDM : 								| BCS : Global.Comm.Agreement	| RBX : 
	        public String TelCommAgreement {get;set;}			//SF : PersMobiPhone__pc			| MDM : CommunicationAgreement			| BCS : Tel.Comm.Agreement		| RBX :
	        public Date TelCommAgreementDate {get;set;}			//SF : PersMobiPhoneDate__pc		| MDM : UpdatedDate						| BCS : 						| RBX :
	        public String EmailCommAgreement {get;set;}			//SF : PersEmail__pc				| MDM : CommunicationAgreement			| BCS : Email.Comm.Agreement	| RBX :
	        public Date EmailCommAgreementDate {get;set;}		//SF : PersEmailDate__pc			| MDM : UpdatedDate						| BCS : 						| RBX :
	        public String SMSCommAgreement {get;set;}			//SF : SMS__pc						| MDM : CommunicationAgreement			| BCS : SMS.Comm.Agreement		| RBX :
	        public Date SMSCommAgreementDate {get;set;}			//SF : SMSDate__pc					| MDM : UpdatedDate						| BCS : 						| RBX :
	        public String PostCommAgreement {get;set;}			//SF : Adress__pc					| MDM : CommunicationAgreement			| BCS : Post.Comm.Agreement		| RBX :
	        public Date PostCommAgreementDate {get;set;}		//SF : AdressDate__pc				| MDM : UpdatedDate						| BCS : 						| RBX :
	        public String stopComFlag {get;set;}				//SF : StopComFlag__c				| MDM : StopCommunication				| BCS :						 	| RBX :
	        public Date stopComFlagDate {get;set;}				//SF : StopComDate__c				| MDM : UpdatedDate						| BCS :						 	| RBX :
	        public String stopComSMDFlag {get;set;}				//SF : StopComSMDFlag__c			| MDM : StopCommunication				| BCS :						 	| RBX :
	        public Date stopComSMDFlagDate {get;set;}			//SF : StopComSMDDate__c			| MDM : UpdatedDate						| BCS :						 	| RBX :
	        public String StopCommOrigin {get;set;}				//SF : StopComOrigin__c, 			| MDM : StopCommOrigin					| BCS :						 	| RBX :
	        public String StopCommSubOrigin {get;set;}			//SF : StopComSubOrigin__c			| MDM : StopCommSubOrigin				| BCS :						 	| RBX :
	        
	        public String accountBrand {get;set;}				//SF : AccountBrand__c				| MDM : 								| BCS :						 	| RBX :	
	      	public String soc {get;set;}						//SF : 								| MDM : 								| BCS :	soc					 	| RBX :
	      	
	      	
	      	//technical field  
	      	public string datasource{get;set;}
	      	public string DataSourceOrigin{get;set;} // origin of datasource
	      	public string countryCode2 {get;set;} //for id RBX 	
	      	
	      	//Country
	      	public string countrySF{get;set;}					//SF : country__c 
	      	public string AdriaticCountrySF{get;set;}			//SF : Adriatic_country__c 
	      	public string MiDCECountrySF{get;set;}				//SF : MIDCE_Country__c
	      	public string NRCountrySF{get;set;}					//SF : NR_Country__c
	      	public string PlCountrySF{get;set;}					//SF : PL_Country__c
	      	public string UkIeCountrySF{get;set;}				//SF : UK_IE_country__c 
	        
	        public List<TRVResponseWebservices.Vehicle> vcle {get;set;}
	        public List<TRVResponseWebservices.Contract> cont {get;set;}
		}

        public class Vehicle{
        	
        	public String vin {get;set;}						//SF : Name							| MDM : VehicleIdentificationNumber		| BCS :	vin					 	| RBX : vin
          	public String vinExternal{get;set;}					//SF : Tech_VINExternalID__c		| MDM : VehicleIdentificationNumber		| BCS :	vin					 	| RBX : vin
          	public String brandCode {get;set;}					//SF : VehicleBrand__c				| MDM : Brand							| BCS :	brandCode				| RBX : brandCode
          	public String model {get;set;}						//SF : Model__c						| MDM : Model							| BCS :							| RBX :
          	public String modelCode {get;set;}					//SF : Model_Code2__c				| MDM : Model							| BCS :	modelCode				| RBX : modelCode
			public String modelLabel {get;set;}					//SF : Commercial_Model__c			| MDM : modelLabel						| BCS :	modelLabel				| RBX :          	
          	public String versionLabel {get;set;}				//SF : Version__c					| MDM : 								| BCS :	versionLabel			| RBX : versionLabel
          	public String deliveryDate {get;set;}				//SF : DeliveryDate__c				| MDM : DeliveryDate					| BCS :							| RBX : 
          	public String firstRegistrationDate {get;set;}		//SF : FirstRegistrDate__c			| MDM : firstRegistrationDate			| BCS :	firstRegistrationDate	| RBX : firstRegistrationDate
          	public String lastRegistrationDate {get;set;}		//SF : LastRegistrDate__c			| MDM :	RegistrationDate	 			| BCS :	registrationDate		| RBX : registrationDate
          	public String registrationNumber {get;set;}			//SF : VehicleRegistrNbr__c			| MDM :	RegistrationNumber 				| BCS :	registration			| RBX : registration
          	public String technicalControlDate {get;set;}		//SF : Technical_Control_Date__c	| MDM :	 								| BCS :	technicalInspectionDate	| RBX : technicalInspectionDate
          	
          	public String dataCompletionBVM {get;set;}			//SF : Data_Completion_BVM_Needed__c| MDM :	 								| BCS :							| RBX : 
          	public String dataCompletionETICOM {get;set;}		//SF : Data_Completion_ETICOM_Needed__c	| MDM :								| BCS :							| RBX : 
          	public String dataCompletionSLK {get;set;}			//SF : Data_Completion_SLK_Needed__c| MDM :	 								| BCS :							| RBX : 
          	public String dataBVMUpto {get;set;}				//SF : BVM_data_Upto_date__c		| MDM :	 								| BCS :							| RBX : 
          	
          	//Vehicle Relation 
          	public String vehicleType{get;set;}					//SF : TypeRelation__c				| MDM :	PartyVehicleType				| BCS : purchaseNature			| RBX : purchaseNature
          	public String possessionBegin {get;set;}			//SF : StartDateRelation__c			| MDM :	StartDate						| BCS :	possessionBegin			| RBX : possessionBegin
	        public String possessionEnd {get;set;}				//SF : EndDateRelation__c			| MDM :	EndDate 						| BCS :	possessionEnd			| RBX : possessionEnd
			public String vnvo {get;set;}						//SF : VNVO__c						| MDM : NewVehicle						| BCS :	new						| RBX : new
			public String garageStatus {get;set;}				//SF : My_Garage_Status__c			| MDM :	 								| BCS :							| RBX : 
			public String status {get;set;}						//SF : Status__c					| MDM :	 								| BCS :							| RBX :  
          	
          	public String IdClient {get;set;}
			
			//List of contracts
			public List<TRVResponseWebservices.Contract> cont {get;set;}
    	}
    	
    	public class Contract
    	{
          	public String idContrat {get;set;}					//SF : 								| MDM : Number_x						| BCS :						 	| RBX : idContrat
	        public String status {get;set;}						//SF : 								| MDM : Status							| BCS :						 	| RBX : status
	        public String type_x {get;set;}						//SF : 								| MDM : Category						| BCS :						 	| RBX : type_x
	        public String vin {get;set;}						//SF : 								| MDM : vin					 			| BCS :						 	| RBX : 
	        public String SubCategory {get;set;}				//SF : 								| MDM : SubCategory						| BCS :						 	| RBX : 
	        public String Offer {get;set;}						//SF : 								| MDM : Offer							| BCS :						 	| RBX : 
	        public String BeginningDate {get;set;}				//SF : 								| MDM : BeginningDate					| BCS :						 	| RBX : 
	        public String Duration {get;set;}					//SF : 								| MDM : Duration						| BCS :						 	| RBX : 
	        public String techLevel {get;set;}					//SF : 								| MDM : 								| BCS :						 	| RBX : techLevel
	        public String serviceLevel {get;set;}				//SF : 								| MDM : 								| BCS :						 	| RBX : serviceLevel
	        public String productLabel {get;set;}				//SF : 								| MDM : 								| BCS :						 	| RBX : productLabel
	        public String initKm {get;set;}						//SF : 								| MDM : 								| BCS :						 	| RBX : initKm
	        public String maxSubsKm {get;set;}					//SF : 								| MDM : 								| BCS :						 	| RBX : maxSubsKm
	        public String subsDate {get;set;}					//SF : 								| MDM : 								| BCS :						 	| RBX : subsDate
	        public String initContractDate {get;set;}			//SF : 								| MDM : 								| BCS :						 	| RBX : initContractDate
	        public String endContractDate {get;set;}			//SF : 								| MDM : 								| BCS :						 	| RBX : endContractDate
	        public String updDate {get;set;}					//SF : 								| MDM : 								| BCS :						 	| RBX : updDate
	        public String idMandator {get;set;}					//SF : 								| MDM : 								| BCS :						 	| RBX : idMandator
	        public String idCard {get;set;}						//SF : 								| MDM : 								| BCS :						 	| RBX : idCard
                    	
    	}
}