public with sharing class VFC144_ManagerLeadBoxController {
  
  public VFP24_LeadBoxMessages__c messages{get;set;}
  private String region{
    get{
      try{
        return [select NameZone__c from Account where Id = :accDealerId].NameZone__c;
      }catch( Exception e ){
        return 'R1';
      }
    }
  }
  
  public String welcomeMessage{
    get{
      try{
        return (String)messages.get( 'Welcome_message_' + region + '_' + Userinfo.getLanguage() + '__c' );
      }catch( Exception e ){
        return messages.Welcome_message_R1_pt_BR__c;
      }
    }
  }
  
  public List< SelectOption > campaignSelectList{
    get{
      List< SelectOption > campaignSelectList = new List< SelectOption >();
      Set< String > campaignNameSet = new Set< String >();
      
      String userBIR = [select BIR__c from User where Id = :Userinfo.getUserId()].BIR__c;
      accDealerId = [
      select Id 
      from Account
      where RecordType.DeveloperName = 'Network_Site_Acc'
        and IDBIR__c = :userBIR
      ].Id;
      
      campaignSelectList.add( new SelectOption( '', Label.VFP24_All_Campaign ) );
      for( Opportunity opp : [select Campaign_Name__c from Opportunity where Campaign_Name__c != null and Dealer__c =:accDealerId] )
        campaignNameSet.add( opp.Campaign_Name__c );

        system.debug('$$$ campaignNameSet: '+campaignNameSet);
      
      List< String > campaignNameList = new List< String >( campaignNameSet );
      campaignNameList.sort();
      
      for( String campaignName : campaignNameList )
        campaignSelectList.add( new SelectOption( campaignName, campaignName ) );
        
      return campaignSelectList;
    }
  }
  
  public List< SelectOption > sellerSelectList{
    get{
      List< Contact > contactList = VFC29_ContactDAO.getInstance().fetchContactRecordsUsingCriteria();
      List< Id > contactIdList = new List< Id >();
      for( Contact c : contactList ) contactIdList.add( c.Id );
      
      List< User > userList = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId( contactIdList );
      String BIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).BIR__c;
      List< SelectOption > sellerSelectList = new List< SelectOption >();
      
      sellerSelectList.add( new SelectOption( Userinfo.getUserId(), '--' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName() + '--' ) );
      for( User u : userList )
        if( !u.DVESeller_BR__c && u.BIR__c == BIR )
          sellerSelectList.add( new SelectOption( u.Id, u.Name ) );
      
      return sellerSelectList;
    }
  }
  
  public List< Opportunity > oppList{
    get{
      return oppMap.values();
    }
  }
  
  private Map< Id, Opportunity > oppMap{get;set;}
  
  private Map< Id, Id > oppOwnerMap{get;set;}
  
  public String oppIdListAsString{
    get{
      return JSON.Serialize( selectedOppIdList );
    }
    set{
      //selectedOppIdList = (List< Id >)Json.Deserialize( value, List< Id >.class );
      oppOwnerMap = (Map< Id, Id >)Json.Deserialize( value, Map< Id, Id >.class );
    }
  }
  private List< Id > selectedOppIdList{get;set;}
  
  private Id accDealerId{get;set;}
  
  public String campaignFilter{get;set;}
  
  public VFC144_ManagerLeadBoxController(){
    messages = VFP24_LeadBoxMessages__c.getInstance( Userinfo.getProfileId() );
    String userBIR = [select BIR__c from User where Id = :Userinfo.getUserId()].BIR__c;
    system.debug('$$$ userBIR: '+userBIR);
    accDealerId = [
      select Id 
      from Account
      where RecordType.DeveloperName = 'Network_Site_Acc'
        and IDBIR__c = :userBIR
      ].Id;
    system.debug('$$$ accDealerId: '+accDealerId);
    loadOppMap();
    
    selectedOppIdList = new List< Id >();
    campaignFilter = '';
  }
  
  public void loadOppMap(){
    oppMap = new Map< Id, Opportunity >( (List< Opportunity >)Database.query( 
      'select Id, Name, CreatedDate, Vehicle_of_Interest__c, VehicleInterest__c, OpportunitySubSource__c,Campaign_Name__c, OwnerId ' +
      'from Opportunity ' +
      'where OpportunitySource__c <> \'NETWORK\' ' +
      	'AND OpportunitySource__c <> \'DEALER\' ' +
        //'and OwnerId = \'' + Userinfo.getUserId() + '\' ' +
        'AND Submitted__c = false ' +
        'and StageName = \'Identified\' ' +
      (!String.isBlank( campaignFilter ) ? 'and Campaign_Name__c = \'' + campaignFilter + '\' ' : '') +
      'and Dealer__c = \'' + accDealerId + '\' ' +
      'order by CreatedDate asc'
    ) );
    system.debug('$$$ oppMap'+oppMap);
  }
  
  public void forward(){
    try{
      List< Opportunity > modifiedOppList = new List< Opportunity >();
      /*for( Id oppId : selectedOppIdList )
        modifiedOppList.add( oppMap.get( oppId ) );*/
      for( Id oppId : new List< Id >( oppOwnerMap.keySet() ) )
        modifiedOppList.add( new Opportunity( Id = oppId, OwnerId = (Id)oppOwnerMap.get( oppId ), Submitted__c = true, OpportunityRedirectedSeller__c = true ));
      System.debug( '@@@ BEFORE UPDATE: ' + Json.SerializePretty( modifiedOppList ) );
      Database.update( modifiedOppList );
      System.debug( '@@@ AFTER UPDATE: ' + Json.SerializePretty( modifiedOppList ) );
    } catch( Exception e ){
      Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
    }
    loadOppMap();
  }
}