public with sharing class TRVRequestParametersWebServices 
{
	public class RequestParameters {
		public User user {get; set;}
		public String vin{get;set;}
		public String registration{get;set;}
		public String country{get;set;}			//use by getParty too //code iso
		public String country2L{get;set;}	    // for ID BCS, set by manager
		public String lastname{get;set;}
		public String firstname{get;set;}
		public String brand{get;set;}
		public String email{get;set;}
		public String city{get;set;}
		public String zip{get;set;}
		
		public string username{get;set;}
		public String idClient{get;set;}
		public String ident1{get;set;}				//All
		public String strLocalID{get;set;}			//MDM
		public String strFixeLandLine{get;set;}		//MDM
    	public String strMobile{get;set;}			//MDM
    	
    	public String BusinessId1{get;set;}			//MDM
    	public String BusinessId2{get;set;}			//MDM
    	public String PersonalId1{get;set;}			//MDM
    	public String PersonalId2{get;set;}			//MDM
    	
		public String partyType {get; set;}
    	
    	//technicals
    	public String strPartyID{get;set;}			//MDM //use by getParty too
    	public decimal callMode {get; set;}			//call MODE for customers client
    	public string DataSourceOrigin{get; set;}	// Origin datasource 
    	public String mDataSourceType;		// mdm, bcs, Sic private TRV 
    	public string demander{get;set;}    //use by getParty too
		public string user_x{get;set;}      //use by getParty too
		public string technicalCountry{get;set;} //for manage multi Country 
		public boolean logger{get;set;} //insert log or not, true for log false for not log
	
		//Country
      	public string countrySF{get;set;}					//SF : country__c 
      	public string AdriaticCountrySF{get;set;}			//SF : Adriatic_country__c 
      	public string MiDCECountrySF{get;set;}				//SF : MIDCE_Country__c
      	public string NRCountrySF{get;set;}					//SF : NR_Country__c
      	public string PlCountrySF{get;set;}					//SF : PL_Country__c
      	public string UkIeCountrySF{get;set;}				//SF : UK_IE_country__c 
		
		public string nbReplies{get;set;}      //BCS Only
	}


}