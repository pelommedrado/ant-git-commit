/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class QueueMessageAction {
    global QueueMessageAction() {

    }
    @InvocableMethod(label='Queue Messages' description='Queues the given messages for delivery and returns the status of each item.')
    global static List<LiveText.ActionResult> queueMessages(List<LiveText.QueueItemRequest> items) {
        return null;
    }
}
