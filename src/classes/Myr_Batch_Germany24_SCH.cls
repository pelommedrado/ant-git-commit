/* Purge of accounts
  *************************************************************************************************************
  18 Mar 2016 : Creation - purge of german accounts
  *************************************************************************************************************/
global class Myr_Batch_Germany24_SCH implements Schedulable {
	global void execute(SchedulableContext sc) {
 
		//Release and anonymize community users
		Database.executebatch(new Myr_Batch_Germany2_User_BAT());  
	}
}