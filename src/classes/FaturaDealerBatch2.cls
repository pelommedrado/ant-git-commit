global class FaturaDealerBatch2 implements Schedulable, Database.Batchable<sObject> {

    String query;

    global FaturaDealerBatch2() {
        this.query  = 'SELECT Id, Region__c, Sector__c, BIR_Group_Issuer__c,'
                    + ' Group_Name_Issuer__c, BIR_NFE_Issuer__c, BIR_Name_NFE_Issuer__c,'
                    + ' Shipping_BIR__c, Invoice_Type__c, VIN__c, Integration_Protocol__c,'
                    + ' Sequential_File__c, Invoice_Number__c, Invoice_Serie__c,'
                    + ' Invoice_Data__c, Delivery_Date__c, Customer_Name__c, Address__c,'
                    + ' Number__c, Address_Complement__c, Neighborhood__c, City__c,'
                    + ' Postal_Code__c, State__c, Birth_Date__c, Customer_Type__c, Customer_Email_Mirror__c,'
                    + ' Customer_Identification_Number__c, Customer_Email__c, DDD1__c,'
                    + ' Phone_1__c, Manufacturing_Year__c, Model_Year__c, Payment_Type__c,'
                    + ' DDD2__c, Phone_2__c, DDD3__c, Phone_3__c, Error_Messages__c,'
                    + ' RecordTypeId, Integration_Date__c, CNPJ_Issuer_Register__c,  CNPJ_Matrix_Register__c, '
                    + ' External_Key__c, Status__c FROM FaturaDealer2__c '
                    + ' WHERE Status__c = \'Unprocessed\' ORDER BY Invoice_Data__c ASC NULLS LAST';
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<FaturaDealer2__c> scope) {
        System.debug('execute: ' + scope.size());
        //FaturaDealerServico2 servico = new FaturaDealerServico2();
        //FaturaDealer2Service servico = new FaturaDealer2Service();
        //servico.startValidation(scope);

        System.debug(scope);
        try {
            FaturaNotaService servicoNota = new FaturaNotaService(scope);
            servicoNota.startValidation();
        } 
        catch(Exception ex) {
    		System.debug('Ex:' + ex);
    	}

        try {
            FaturaClienteService servicoNota = new FaturaClienteService(scope);
            servicoNota.startValidation();
        } 
        catch(Exception ex) {
    		System.debug('Ex:' + ex);
    	}
    }
    global void finish(Database.BatchableContext BC) {
    
    }

    global void execute(SchedulableContext sc) {
  	     System.debug('Iniciando a execucao do batch');
        FaturaDealerBatch2 fatBatch = new FaturaDealerBatch2();
        Database.executeBatch(fatBatch);
    }
}