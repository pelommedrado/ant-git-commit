public class DataUtils {

    public static Time parseHHmm(String hora) {
        final String[] horaSplit = hora.split(':');
        final Integer hh = Integer.valueOf(horaSplit[0]);
        final Integer mm = Integer.valueOf(horaSplit[1]);
        return Time.newInstance(hh, mm, 0, 0);
    }

    /*public static Datetime parseDDMMYYYYHHmmss(String format) {
        final List<String> token = format.split(' ');
        if(token.size() != 2) {
            return null;
        }
        final String[] dataSplit = token[0].split('/');
        final Integer dia = Integer.valueOf(dataSplit[0]);
        final Integer mes = Integer.valueOf(dataSplit[1]);
        final Integer ano = Integer.valueOf(dataSplit[2]);

        final String[] horaSplit = token[1].split(':');
        final Integer hh = Integer.valueOf(horaSplit[0]);
        final Integer mm = Integer.valueOf(horaSplit[1]);
        final Integer ss = Integer.valueOf(horaSplit[2]);

        return Datetime.newInstance(ano, mes, dia, hh, mm, ss);
    }*/

    public static Date parseDDmmYYYY(String data) {
      if(String.isEmpty(data)) {
        return null;
      }
      final String[] dataSplit = data.split('/');
      return Date.newInstance(
          Integer.valueOf(dataSplit[2]),
          Integer.valueOf(dataSplit[1]),
          Integer.valueOf(dataSplit[0]));
    }

    public static Date toDate(String data){
        if(String.isEmpty(data)) {
        return null;
      }
      final String[] dataSplit = data.split('/');
      return Date.newInstance(
          Integer.valueOf(dataSplit[0]),
          Integer.valueOf(dataSplit[1]),
          Integer.valueOf(dataSplit[2]));
    }

    public static Datetime dataToDatetimeFimMes(Date data) {
        Integer numberOfDays =
            Date.daysInMonth(data.year(), data.month());

        Datetime fim = Datetime.newInstance(
            data.year(),
            data.month(),
            numberOfDays, 23, 59, 59);
        return fim;
    }

    public static Datetime dataToDatetimeIniMes(Date data) {
        Datetime ini = Datetime.newInstance(
            data.year(),
            data.month(),
            1, 0, 0, 0);
        return ini;
    }

    public static Datetime dataToDatetimeFim(Date data) {
        Integer numberOfDays =
            Date.daysInMonth(data.year(), data.month());

        Datetime fim = Datetime.newInstance(
            data.year(),
            data.month(),
            data.day(), 23, 59, 59);
        return fim;
    }

    public static Datetime dataToDatetimeIni(Date data) {
        Datetime ini = Datetime.newInstance(
            data.year(),
            data.month(),
            data.day(), 0, 0, 0);
        return ini;
    }

    public static Datetime getUserDatetime(Datetime dt) {
        Integer d = UserInfo.getTimeZone().getOffset(dt);
        return dt.addSeconds( d/ 1000);
    }

    public class DiffData {
        public Long tIni { get;set; }
        public Long tFim { get;set; }
        public Long diffSeconds { get;set; }
        public Long diffMinutes { get;set; }
        public Long diffHours 	{ get;set; }
        public Long diffDays 	{ get;set; }

        public DiffData(Datetime ini, Datetime fim) {
            this.tIni 		 = ini.getTime();
            this.tFim 		 = fim.getTime();//DataUtils.getUserDatetime(Datetime.now()).getTime();

            Long diff 		 = tFim - tIni;

            this.diffSeconds = Math.abs( Math.mod( diff / (1000			 	  ), 60));
            this.diffMinutes = Math.abs( Math.mod( diff / (60 * 1000	 	  ), 60));
            this.diffHours   = Math.abs( Math.mod( diff / (60 * 60 * 1000	  ), 24));
            this.diffDays    = diff / (24 * 60 * 60 * 1000);
        }

        public String format() {

            if(diffDays == 0 && diffHours == 0 && diffMinutes == 0){
                return '0';
            }

            return
                (diffDays > 0 ? diffDays + ' dia(s) ' : ' ') 	+
                (diffHours > 0 ? diffHours + ' hora(s) ' : ' ') +
                (diffMinutes > 0 ? diffMinutes + ' minuto(s) ' : ' ');
        }
    }

    public static List<Holiday> holidayRecurrenceList() {
      return [
        SELECT h.RecurrenceStartDate, h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h
        WHERE h.IsRecurrence = true
      ];
    }

    public static Integer calcWorkingDaysBetweenTwoDates(Date date1, Date date2, List<Holiday> holidayList) {
        System.debug('calcWorkingDaysBetweenTwoDates()' + date1 + ' ' + date2);
        final Integer allDaysBetween = date1.daysBetween(date2);

        Integer allWorkingDays = 0;
        for(Integer k = 0; k < allDaysBetween ;k++ ) {
            if(isWorkingDay(date1.addDays(k), holidayList)) {
                allWorkingDays++;
            }
        }

        return allWorkingDays;
    }

    public static Date nextWorkingDay(Date data, Integer next, List<Holiday> holidayList) {
      System.debug('nextWorkingDay()' + data + ' ' + next);
  		Date finalDate = data;
  		final Integer direction = next < 0 ? -1 : 1;

  		do {
  			finalDate = finalDate.addDays(direction);
  			if(isWorkingDay(finalDate, holidayList)) {
  				next -= direction;
  			}
  		} while(next != 0);

  		return finalDate;
  	}

    public static Boolean isWorkingDay(Date currentDate, List<Holiday> holidays) {
  		System.debug('isWorkingDay()' + currentDate);

  		if(isWeekendDay(currentDate)) {
  			return false;
  		}

  		if(isHolidayListContains(holidays, currentDate)) {
  			return false;
  		}

  		return true;
    }

    public static Boolean isHolidayListContains(List<Holiday> holidays, Date data) {
  		System.debug('isHolidayListContains()');

  		for(Holiday hDay : holidays) {
        if(isHolidayEqualsDate(hDay, data)) {
  				return true;
        }
      }
  		return false;
  	}

  	public static Boolean isHolidayEqualsDate(Holiday hDay, Date data) {
  		System.debug('isHolidayEqualsDate()' + data);
  		System.debug('Holiday:' + hDay);
  		if(hDay.ActivityDate.month() != data.month()) {
  			return false;
  		}
  		if(hDay.ActivityDate.day() != data.day()) {
  			return false;
  		}
  		return true;
  		//return data.daysBetween(hDay.ActivityDate) == 0;
  	}

    public static Boolean isWeekendDay(Date data) {
  		System.debug('isWeekendDay() '+ data);
      Boolean result = false;
      //Recover the day of the week
      final Date startOfWeek   = data.toStartOfWeek();
      final Integer dayOfWeek  = startOfWeek.daysBetween(data);
  		return dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
    }
}