/*  Test class of Myr_CheckUsername_WS web service
*************************************************************************************************************
13.02.2014  : Creation
19.09.2015  : New message codes : WS05MS001, WS05MS506
*************************************************************************************************************/
@isTest
private class Myr_CheckUsername_WS_Test {
	//check the logs contained in the logger__c 
	private static void checkLogs(String inputs, String output) {
  		//Exception_Type__c & Message__c & Record__c are not filterable as they are long text area
	  	List<Logger__c> logs = [SELECT Id, RunId__c, Error_Level__c, Exception_Type__c, Function__c, Message__c, Project_Name__c, Record__c FROM Logger__c ];
	  	Boolean entry = false;
	  	Boolean code = false;
	  	system.debug('### checkLogs - inputs=' + inputs);
		system.debug('### checkLogs - output=' + output);
   		for( Logger__c log : logs ) {
			system.debug('### checkLogs - log=' + log);
			if( !String.isBlank(log.Record__c) && log.Record__c.equalsIgnoreCase(inputs)) {
				entry = true;
			} 
			if( !String.isBlank(log.Exception_Type__c) && log.Exception_Type__c.equalsIgnoreCase(output) ) {
				code = true;
			}
   		}
		system.debug('### checkLogs - entry=' + entry);
		system.debug('### checkLogs - code=' + code);
   		system.assertEquals(true, entry && code);
   		delete logs;
  	}
    
    @testSetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    }

    //Test Username parameter
    static testMethod void testBadUsername() {
        //Prepare the custom setting
        String prefix =  CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = null;  
        //Custom settings & technical user
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        //test bad format 
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable('test@test.fr', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        String inputs = 'Inputs: <username=test@test.fr, brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable('test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username=test, brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable('test@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username=test@test, brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(prefix+'test@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username='+prefix+'test@test'+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format (space)
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(prefix+' test@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username='+prefix+' test@test'+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format (space)
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(prefix+'$test@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username='+prefix+'$test@test'+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format (space)
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(prefix+'étest@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username='+prefix+'étest@test'+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test bad format (space)
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(prefix+'^test@test', 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS502', response.info.code);
        system.assertEquals(true, response.info.message.contains('has not the proper format'));
        inputs = 'Inputs: <username='+prefix+'^test@test'+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
         //test proper format but too long
        String usn = prefix+'rhelios_serjhkfgsdjkfwsdjkfd567dklfhsduklhvqsdklfhvsdjkfh@sdkfjqsdkfjsdvfklhsdvnklfvhsdfvksd.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(usn, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS507', response.info.code);
        system.assertEquals(true, response.info.message.contains('Value(s) too long : Parameter(s) Username'));
        inputs = 'Inputs: <username='+usn+', brand=Renault>';
        checkLogs(inputs, response.info.code);
    }
    
    static testMethod void testBadBrand() {
        //Prepare the custom setting
        String prefix =  CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = null;
        //Custom settings & technical user
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        String mail = prefix + 'test.killer@kmail.vdf';
        //test bad brand 
        system.runAs(techUser) {
               response = Myr_CheckUsername_WS.isAvailable(mail,'Tricot') ;
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS004', response.info.code);
        system.assertEquals(true, response.info.message.contains('Brand accepts only the values Renault, Dacia and <null>'));
        String inputs = 'Inputs: <username='+mail+', brand=Tricot>';
        checkLogs(inputs, response.info.code);
        
        //test good brand 
        system.runAs(techUser) {
               response = Myr_CheckUsername_WS.isAvailable(mail,'DaCiA') ;
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals(true, response.info.message.contains('is available'));
        inputs = 'Inputs: <username='+mail+', brand=DaCiA>';
        checkLogs(inputs, response.info.code);
        
        //test bad brand 
        system.runAs(techUser) {
               response = Myr_CheckUsername_WS.isAvailable(mail,' DaCiA') ;
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS004', response.info.code);
        system.assertEquals(true, response.info.message.contains('Brand accepts only the values Renault, Dacia and <null>'));
        inputs = 'Inputs: <username='+mail+', brand= DaCiA>';
        checkLogs(inputs, response.info.code);
        
        //test good brand 
        system.runAs(techUser) {
               response = Myr_CheckUsername_WS.isAvailable(mail,'RENAULT') ;
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals(true, response.info.message.contains('is available'));
        inputs = 'Inputs: <username='+mail+', brand=RENAULT>';
        checkLogs(inputs, response.info.code);
        
        //test good brand 
        system.runAs(techUser) {
               response = Myr_CheckUsername_WS.isAvailable(mail,null) ;
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals(true, response.info.message.contains('is available'));  
        inputs = 'Inputs: <username='+mail+', brand=null>';
        checkLogs(inputs, response.info.code);   
    }
    
    //Test username is not available because of existing user with another profile
    static testMethod void testUnavailable() { 
        //Prepare the custom setting
        String prefix =  CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = null;
        //Custom settings & technical user
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
		Test.startTest();
		system.runAs( Myr_Datasets_Test.getSysAdminUser() ) {
			techUser.Username = prefix + techUser.Username;
			update techUser;
		}
		Test.stopTest();

        //Insert a HeliosCommunity user
        //List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        User techUsr = [SELECT Id, Username FROM User WHERE Id = :techUser.Id];
        String usrnm = techUsr.Username;
        //Launch the tests 
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(usrnm, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS505', response.info.code);
        system.assertEquals('Username <'+usrnm+'> is not available: already created in another organization', response.info.message);
        
        String inputs = 'Inputs: <username='+usrnm+', brand=Renault>';
        checkLogs(inputs, response.info.code);
    }
    

    //Test Availability cross brands
    static testMethod void testAvailableCrossBrands() {
        //Prepare the custom setting
        String prefix =  CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = null;
        //Custom settings & technical user
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        //Insert a HeliosCommunity user
        List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        //modify the brand
        Account acc = [SELECT Id, MYR_Status__c, MYD_Status__c FROM Account WHERE Id = :listAcc[0].Id][0];
        User usr = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id][0];
        String mail = usr.username;
        //Test 1: Renault active and ask for Dacia: available !
        acc.MYR_Status__c = 'Activated';
        acc.MYD_Status__c = 'Deleted';
        update acc;
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Dacia');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS001', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: user exists with this username but the given brand is not activated', response.info.message);
        String inputs = 'Inputs: <username='+mail+', brand=Dacia>';
        checkLogs(inputs, response.info.code);
        
        //Test 2: Dacia active and ask for Renault: available !
        acc.MYR_Status__c = 'Deleted';
        acc.MYD_Status__c = 'Activated';
        update acc;
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS001', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: user exists with this username but the given brand is not activated', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //Test 3: Dacia active and ask for Renault (by default): available !
        acc.MYR_Status__c = 'Deleted';
        acc.MYD_Status__c = 'Activated';
        update acc;
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, null);
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS001', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: user exists with this username but the given brand is not activated', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=null>';
        checkLogs(inputs, response.info.code);
        
        //Test 4: Dacia active and ask for Dacia: not available !
        acc.MYR_Status__c = 'Deleted';
        acc.MYD_Status__c = 'Activated';
        update acc;
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Dacia');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS506', response.info.code);
        system.assertEquals('Username <'+mail+'> is not available: activated for the given Brand', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Dacia>';
        checkLogs(inputs, response.info.code);
        
         //Test 5: Renault active and ask for Renault: not available !
        acc.MYR_Status__c = 'Activated';
        acc.MYD_Status__c = 'Deleted';
        update acc;
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS506', response.info.code);
        system.assertEquals('Username <'+mail+'> is not available: activated for the given Brand', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
    } 
    
    //Test username is available
    static testMethod void testAvailable() { 
        //Prepare the custom setting
        String prefix =  CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        Myr_CheckUsername_WS.Myr_CheckUsername_WS_Response response = null;
        //Custom settings & technical user
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        //normal test
        String mail = prefix + 'testkiller@kmail.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: no existing user with this username', response.info.message);
        String inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test with -
        mail = prefix + 'test-killer@kmail.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: no existing user with this username', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test with _
        mail = prefix + 'test_killer@kmail.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: no existing user with this username', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test with .
        mail = prefix + 'test.killer@kmail.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: no existing user with this username', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
        
        //test with numbers
        mail = prefix + 'test.killer45@kmail.vdf';
        system.runAs(techUser) {
            response = Myr_CheckUsername_WS.isAvailable(mail, 'Renault');
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS05MS000', response.info.code);
        system.assertEquals('Username <'+mail+'> is available: no existing user with this username', response.info.message);
        inputs = 'Inputs: <username='+mail+', brand=Renault>';
        checkLogs(inputs, response.info.code);
    }
    
    // Test the brands into Myr_Renault_Tools
     static testMethod void testCheckBrands() {
        system.assertEquals(true, Myr_MyRenaultTools.checkRenaultBrand('Renault'));
        system.assertEquals(true, Myr_MyRenaultTools.checkRenaultBrand('RENAULT'));
        system.assertEquals(true, Myr_MyRenaultTools.checkRenaultBrand('REnauLT'));
        system.assertEquals(false, Myr_MyRenaultTools.checkRenaultBrand(' REnauLT'));
        system.assertEquals(false, Myr_MyRenaultTools.checkRenaultBrand('REnauLT '));
        system.assertEquals(true, Myr_MyRenaultTools.checkDaciaBrand('Dacia'));
        system.assertEquals(true, Myr_MyRenaultTools.checkDaciaBrand('DACIA'));
        system.assertEquals(true, Myr_MyRenaultTools.checkDaciaBrand('DAcIa'));
        system.assertEquals(false, Myr_MyRenaultTools.checkDaciaBrand(' DAcIa'));
        system.assertEquals(false, Myr_MyRenaultTools.checkDaciaBrand('DAcIa '));
     }
}