public with sharing class IntegrationDistrinet {

	private List<VRE_VehRel__c> vehicleRelationList { get;set; }
	private Set<Id> vehicleIdSet  { get;set; }
	private Set<Id> dealerIdSet  	{ get;set; }

	private Set<Id> groupIdSet { get;set; }

  public IntegrationDistrinet(List<VRE_VehRel__c> vehicleRelationList) {
		this.vehicleRelationList = vehicleRelationList;
		this.vehicleIdSet = new Set<Id>();
		this.dealerIdSet = new Set<Id>();
		this.groupIdSet = new Set<Id>();
		init();
	}

	private void init() {
		System.debug('init()' + vehicleRelationList.size());

		for(VRE_VehRel__c vr : vehicleRelationList) {
			if(vr.Status__c == 'Active') {

				if(!String.isEmpty(vr.VIN__c)) {
					vehicleIdSet.add(vr.VIN__c);
				}

				if(!String.isEmpty(vr.Account__c)) {
					dealerIdSet.add(vr.Account__c);
				}
			}
		}

		if(!dealerIdSet.isEmpty()) {
			this.groupIdSet = obterGroupDealer();
		}
	}

	private Set<Id> obterGroupDealer() {
		System.debug('obterGroupDealer()');

		groupIdSet = new Set<Id>();
		final List<Account> accGroupList = [
			SELECT Id, ParentId FROM Account WHERE Id IN: this.dealerIdSet
		];
		for(Account ac : accGroupList) {
			if(ac.ParentId != null) {
				groupIdSet.add(ac.ParentId);
			}
		}
		return groupIdSet;
	}

	public void execute() {
		System.debug('execute()');

		// if(this.groupIdSet.isEmpty()) {
		// 	System.debug('Nenhum group dealer encontrado!');
		// 	return;
		// }

		final List<VehicleBooking__c> vbkList = [
				SELECT Id, Vehicle__c, Quote__r.Opportunity.Dealer__c
				FROM VehicleBooking__c
				WHERE Vehicle__c IN: this.vehicleIdSet
					AND Quote__r.Opportunity.Dealer__r.ParentId IN: groupIdSet
					AND Quote__r.Status = 'Vehicle allocated'
					AND Status__c = 'Active'
		];

		final List<Quote> quoteList = new List<Quote>();

		for(VehicleBooking__c vbk: vbkList) {
			quoteList.add( new Quote(
				Id = vbk.Quote__c, Status = 'Pre Order Requested' ));
		}

		final List<Database.SaveResult> results = Database.update(quoteList, false);
    for(Integer i = 0; i < results.size(); i++) {
      if(!results[i].isSuccess()) {
      	quoteList[i].addError('Erro: '+ String.valueOf( results[i].getErrors() ) );
      }
    }

	}
}