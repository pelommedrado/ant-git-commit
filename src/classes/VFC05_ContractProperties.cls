public with sharing class VFC05_ContractProperties {
	public String idContract {get;set;}
	public String lastName {get;set;}
	public String firstName {get;set;}
	public String address {get;set;}
	public String idContractCard {get;set;}
	public String productLabel {get;set;}
	public String country {get;set;}
	public String status {get;set;}
	public String kmMaxSubscription {get;set;}
}