@isTest
public class VFC156_PV_VersionCallOffer_Util_Test {
    
    static testMethod void deveInsertVersionCallOffer() {
        Model__c model = criarModel();
        PVVersion__c version = criarVersion(model);
        PVCommercial_Action__c commercialAction = criarPvCommercial(model);
        
        PVCall_Offer__c callOffer = criarPVCall_Offer(commercialAction);
        
        List<PVCall_Offer__c> clOf = new List<PVCall_Offer__c>();
        clOf.add(callOffer);
        
        VFC156_PV_VersionCallOffer_Util vfc156 = new VFC156_PV_VersionCallOffer_Util();
        vfc156.insertVersionCallOffer(clOf);
    }
    
    private static Model__c criarModel() {
        Model__c model = new Model__c(
            //   ENS__c = 'abc',
            name = 'ab',
            Market__c = 'abc',
            Status__c = 'Active',
            Model_Spec_Code__c = 'ABC',
            Phase__c = '1',
            Model_PK__c = 'ABC-1'
        );
        
        Database.insert( model );
        
        return model;
    }
    
    private static PVVersion__c criarVersion(Model__c model) {
        PVVersion__c versao = new PVVersion__c(
            name = 'teste',
            Model__c = model.id,
            Version_Id_Spec_Code__c = 'VEC162_BRES',
            Price__c = 100.00,
            PVC_Maximo__c = 100,
            PVR_Minimo__c = 100    
        );
        
        Database.insert(versao);
        
        return versao;
    }
    
    private static PVCommercial_Action__c criarPvCommercial(Model__c model) {
        PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
            Model__c = model.Id,
            Start_Date__c = System.today().addMonths( -1 ),
            End_Date__c = System.today().addMonths( 1 ),
            Type_of_Action__c = 'abc',
            Status__c = 'Active'
        );
        Database.insert( commercialAction );
        return commercialAction;
    }
    
    private static PVCall_Offer__c criarPVCall_Offer(PVCommercial_Action__c commercialAction) {
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
            Commercial_Action__c = commercialAction.Id,
            Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
            Call_Offer_End_Date__c = System.today().addMonths( 1 ),
            Minimum_Entry__c = 100.0,
            Period_in_Months__c = 5, 
            Month_Rate__c = 0.99
        );
        Database.insert( callOffer );
        return callOffer;
    }
    
    /**static testMethod void myUnitTest() {
        
         Model__c model = new Model__c(
           //   ENS__c = 'abc',
              name = 'ab',
              Market__c = 'abc',
              Status__c = 'Active'
        );
        Database.insert( model );
        
        PVVersion__c versao = new PVVersion__c(
        	name = 'teste',
            Model__c = model.id,
            Version_Id_Spec_Code__c = 'VEC162_BRES',
            Price__c  =100.00,
            PVC_Maximo__c=100,
            PVR_Minimo__c = 100
            
        );
        Database.insert(versao);
        
        PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
              Model__c = model.Id,
              Start_Date__c = System.today().addMonths( -1 ),
              End_Date__c = System.today().addMonths( 1 ),
              Type_of_Action__c = 'abc',
              Status__c = 'Active'
        );
        Database.insert( commercialAction );
        
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
              Commercial_Action__c = commercialAction.Id,
              Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
              Call_Offer_End_Date__c = System.today().addMonths( 1 ),
              Minimum_Entry__c = 100.0,
              Period_in_Months__c = 5, 
              Month_Rate__c = 0.99
        );
        Database.insert( callOffer );
        
        //VersionCallOffer__c version = new VersionCallOffer__c(
        // 			Call_Offer__c = callOffer.Id, 
        //            Version__c = versao.Id,
        //           Status__c = 'Active'
        //);
        //Database.insert(version);
        
        model.name= 'abcd';
        Database.update(model);
        
        //commercialAction.Model__c = model.name;
        // update commercialAction;
        //List<PVCall_Offer__c> clOf = new List<PVCall_Offer__c> ();
        //clOf.add(callOffer);
        //VFC156_PV_VersionCallOffer_Util vfc156 = new VFC156_PV_VersionCallOffer_Util();
        //vfc156.insertVersionCallOffer(clOf);
        // delete [select id from VersionCallOffer__c where Call_Offer__c =: calloffer.id];
        
    }**/
}