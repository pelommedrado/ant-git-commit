public with sharing class VFC141_RMCNew_Opportunity_Controller {
    
    public Account acc {get;set;}
    public Contact cnt {get;set;}
    public Opportunity opp {get;set;}
    
    public String companyName { get;set; }
    public String oppRecTypeVendaDireta {get;set;}
    
    public Boolean isReceptionist{get;set;}
    public Boolean isSeller{get;set;}
    
    public String dealerBIR {get;set;}
    
    public Id leadId{get;set;}

    public Id dvrRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','DVR');
        }
    }

    public Id dveRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','DVE');
        }
    }

    public Id smnRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','Seminovos');
        }
    }
    
    public static final Map< String, List< String > > searchFieldMap = new Map< String, List< String > >{
        'Lead' => new List< String >{'Email', 'FirstName', 'LastName', 'Phone', 'BusinessPhone__c', 'CPF_CNPJ__c', 'HomePhone__c', 'Mobile_Phone__c'},
        'Account' => new List< String >{'CustomerIdentificationNbr__c', 'FirstName', 'LastName', 'HOMEPHONE__c', 'Phone', 'PersMobPhone__c', 'PersLandline__c', 'ProfLandline__c', 'Email__c', 'PersonEmail' },
        'Opportunity' => new List< String >{'Name'},
        'Case' => new List< String >{ 'CaseNumber' }
    };
    public static final Map< String, List< String > > returnFieldMap = new Map< String, List< String > >{
        'Lead' => new List< String >{'FirstName', 'LastName', 'CreatedDate', 'toLabel( LeadSource )', 'SubSource__c', 'VehicleOfInterestLookup__r.Name WHERE isConverted = false'},
        'Account' => new List< String >{'FirstName', 'LastName', 'HOMEPHONE__c', 'Phone', 'PersMobPhone__c', 'ShippingCity', 'ShippingPostalCode'},
        'Opportunity' => new List< String >{'Name', 'toLabel( StageName )', 'CreatedDate', 'CloseDate', 'OpportunitySource__c', 'OpportunitySubSource__c', 'Campaign_Name__c', 'Owner.Name'},
        'Case' => new List< String >{ 'CaseNumber', 'CreatedDate', 'toLabel( Type )', 'Subtype__c', 'Subject', 'toLabel( Status )'}
    };
    public String fieldMapping{
        get{
            return Json.Serialize(VFC141_RMCNew_Opportunity_Controller.returnFieldMap);
        }
    }
    
    public Id campaignId{
        get{
            return opp.CampaignId;
        }
        set{
            opp.CampaignId = value;
        }
    }
    
    /*
     * 
     * by @Lucas Andrade - [ kolekto ] - 27/09/2016
     * Nova funcionalidade de visão GRUPO na criação de novo passante por recepcionista
     * 
     * */
    
    public Boolean isExecutive{
        get{
            
            List< PermissionSetAssignment > psets = [SELECT AssigneeId FROM PermissionSetAssignment WHERE
                                                     PermissionSetId IN (SELECT Id FROM PermissionSet WHERE 
                                                                         Name = 'BR_SFA_Dealer_Executive')];
            
            Set < Id > dealers = new Set < Id >();
            for(PermissionSetAssignment p : psets) dealers.add(p.AssigneeId);
            
            if(dealers.contains(UserInfo.getUserId())){
                return true;
            } else { return false; }
        }
    }
    
    public List< SelectOption > dealerSelectList{
        get{
            User u = [SELECT Id, Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId
                      FROM User WHERE Id =: UserInfo.getUserId()];
            
            List< Account > accs = [SELECT Id, Name, IDBIR__c
                               		FROM Account
                               		WHERE RecordTypeId =: Utils.getRecordTypeId('Account', 'Network_Site_Acc')
                               		AND isDealerActive__c = true
                               		AND ParentId =: u.Contact.Account.ParentId ORDER BY Name ASC];
            
            
            List< SelectOption > dealerSelectList = new List< SelectOption >();
            dealerSelectList.add( new SelectOption( '', '--' + Label.OPPNone + '--' ) );
            for(Account a : accs){
                dealerSelectList.add( new SelectOption(a.IDBIR__c, a.Name) );
            }
            
            return dealerSelectList;
        }
    }
    
    /*
     * 
     * 
     * Fim de alterações por @Lucas Andrade
     * 
     * */
    
    public List< SelectOption > campaignSelectList{
        get{
            Id accDealerId;
            try{
                accDealerId = [select Contact.AccountId from User where Id = :Userinfo.getUserId()].Contact.AccountId;
            }catch( Exception e ){
                accDealerId = null;
            }
            return VFC120_CampaignService.fetchCampaignList( 'Dealer', null, null, accDealerId );
        }
    }
    
    public Id sellerId{
        get{
            return opp.OwnerId;
        }
        set{
            opp.OwnerId = value;
        }
    }
    
    @testVisible
    private String sellerName{
        get{
            return sellerId != null ? [select Name from User where Id = :sellerId limit 1].Name : '';
        }
    }
    
    public String accountPrefix{
        get{
            return Account.SObjectType.getDescribe().getKeyPrefix();
        }
    }
    
    public List< SelectOption > sellerSelectList{
        get{
        
            system.debug('dealerBIR: ' + this.dealerBIR);
            List< PermissionSetAssignment > psets = [
                SELECT AssigneeId 
                FROM PermissionSetAssignment 
                WHERE PermissionSetId IN (
                        SELECT Id FROM PermissionSet WHERE (
                            Name = 'BR_SFA_Seller' OR Name = 'BR_SFA_Hostess' OR Name = 'BR_SFA_Seller_Agenda'
                        )
                    )
                ];
                
            Set < Id > sellers = new Set < Id >();
            for ( PermissionSetAssignment p : psets ) sellers.add(p.AssigneeId);
            
            System.debug('######## sellers: ' + sellers);
            
            List< Contact > contactList = VFC29_ContactDAO.getInstance().fetchContactRecordsUsingCriteria();
            List< Id > contactIdList = new List< Id >();
            for( Contact c : contactList ) contactIdList.add( c.Id );
            
            List< User > userList = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId( contactIdList );
            
            String BIR;
            
            if(isExecutive && this.dealerBIR != null){
                BIR = this.dealerBIR;
            } else {
                BIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).Contact.Account.IDBIR__c;
            }
            
            
            List< SelectOption > sellerSelectList = new List< SelectOption >();
            
            System.debug('######## userList: ' + userList);
            System.debug('######## BIR: ' + BIR);
            
            
            sellerSelectList.add( new SelectOption( '', '--' + Label.OPPNone + '--' ) );
            for( User u : userList ){
                if( sellers.contains(u.Id) && u.Contact.Account.IDBIR__c == BIR ){
                 System.debug('### u.Id ' + u.Id + ' u.Contact.Account.IDBIR__c: ' + u.Contact.Account.IDBIR__c);
                 sellerSelectList.add( new SelectOption( u.Id, u.Name ) );
                }
            }
            return sellerSelectList;
        }
    }
    
    public VFC141_RMCNew_Opportunity_Controller (){
        Set<String> permissionByUserList = new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());
        isReceptionist = isSeller = false;
        
        if( UserInfo.getProfileId() == Utils.getProfileId('SFA - Receptionist') || (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess')))
            isReceptionist = true;
        	
        else if( UserInfo.getProfileId() == Utils.getProfileId('SFA - Seller') || (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Seller')))
            isSeller = true;
        	
        if(!Test.isRunningTest()) System.assert( isReceptionist || isSeller, Label.VFP20_Unauthorized_access);
        
        dealerBIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).Contact.Account.IDBIR__c;
        
        clearObjects();
    }
    
    public void clearObjects(){ 
        acc = new Account();
        cnt = new Contact();
        opp = new Opportunity();
        
        companyName = null;
    	oppRecTypeVendaDireta = null;
        
        sellerId = Userinfo.getUserId();
    }
    
    public Boolean isCpfValid(String cpf) {
        if(cpf == null) return false;
        
        Pattern MyPattern = Pattern.compile('[0-9]{3}\\.?[0-9]{3}\\.?[0-9]{3}\\-?[0-9]{2}');
        Matcher MyMatcher = MyPattern.matcher(cpf);
        return MyMatcher.matches();
    }
    
    public Pagereference save() {
        
        if( String.isBlank( acc.PersEmailAddress__c ) && String.isBlank( acc.PersMobPhone__c ) && String.isBlank( acc.PersLandline__c ) ) {
               Apexpages.addMessage( new Apexpages.Message( 
                   ApexPages.Severity.ERROR, Label.ERRPageErrorMessage + ' ' + Label.ACCEmail + ' ' + 
                   Label.UTIL_OR + ' ' + Label.ACCHomePhone + ' ' + Label.UTIL_OR + ' ' + Label.LDDCellular ) );
               return null;
           }
           
       if(String.isNotBlank(acc.CustomerIdentificationNbr__c)) {
            if(!Sfa2Utils.isCpfValido(acc.CustomerIdentificationNbr__c) ) {
                Apexpages.addMessage(new Apexpages.Message( 
                    ApexPages.Severity.ERROR, 'CPF Inválido!'));
                return null;
            }
            
        } /*else {
            if(!String.isEmpty(opp.SourceMedia__c) && 
               !opp.SourceMedia__c.equalsIgnoreCase('Telephone')) {
                   Apexpages.addMessage(new Apexpages.Message( 
                       ApexPages.Severity.ERROR, 'O campo CPF é obrigatório!'));
                   return null;
               }
        }*/
        
        if(String.isBlank(opp.RecordTypeId)) {
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR,
                'Preencha o tipo de negociação!'));
                
            return null;
        }
        
        if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')) {
            if(String.isBlank(oppRecTypeVendaDireta) && String.isBlank(acc.Type_DVE__c)) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR,
                    'Preencha os dados do cliente na seção de Vendas Especiais.'));
                return null;
            }
            if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa') && String.isBlank(companyName)) {
                Apexpages.addMessage(new Apexpages.Message(
                                         ApexPages.Severity.ERROR,
                                         'Preencha o nome da empresa!'));
                return null;
            }
        }
        
        acc.CustomerIdentificationNbr__c = limparFormatacaoCpfCnpj(acc.CustomerIdentificationNbr__c);
        
        // Begin transaction
        Savepoint sp = Database.setSavepoint();
        
        try {
            String BIR = Test.isRunningTest() ? '123456' : [select BIR__c from User where Id = :sellerId].BIR__c;
            Id dealerId = [select Id from Account where IDBIR__c = :BIR limit 1].Id;
            
            /*--Start Code-- Modificado por @Edvaldo - kolekto --*/
            
            List<Lead> lsLead;
            Account account;
            List<Contact> lsContact;
            Lead leadOld;
            Lead lead;
            
            //check if there's an account with CPF equals the CPF field of form.
            if(acc.CustomerIdentificationNbr__c != null)
                account =  VFC12_AccountDAO.getInstance().fetchAccountUsingCPF(acc.CustomerIdentificationNbr__c);
            
            //check if there's leads that have CPF equals the CPF field of form.
            if(acc.CustomerIdentificationNbr__c != null)
                lsLead = [select Id, IsConverted, CPF_CNPJ__c, FirstName, LastName, LeadSource, SubSource__c, Detail__c,
                          Sub_Detail__c, CreatedDate from lead where 
                          CPF_CNPJ__c = :acc.CustomerIdentificationNbr__c Order by CreatedDate ASC];
            
            //get first lead inserted in DataBase.
            if(lsLead != null && !lsLead.isEmpty()){
                leadOld = lsLead.get(0);
                leadOld = VFC09_LeadDAO.getInstance().findLeadByLeadId(leadOld.Id);
            }
            
            //update the Lead with contact information of SFA
            if(leadOld != null && leadOld.IsConverted == false) updateLeadFromSFA(leadOld);
            
            // insert  Account informations.
            //acc.FirstName = cnt.FirstName;
            //acc.LastName = cnt.LastName;
            
            if(!String.isBlank(acc.RecordTypeId)) {
                
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'DVE')){
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                }
            }
            
            acc.VehicleInterest_BR__c = opp.Vehicle_Of_Interest__c;
            
            //insert or update account
            if( account != null ){
                acc.Id = account.Id;
                
                if( account.RecordTypeId == Utils.getRecordTypeId('Account', 'Company_Acc') || Test.isRunningTest()){
                    acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    acc.Phone = acc.PersLandline__c;
                    acc.Cliente_DVE__c = true;
                    
                    cnt.PersEmail__c = acc.PersEmailAddress__c;
                    cnt.HomePhone = acc.PersLandline__c;
                    cnt.PersMobPhone__c = acc.PersMobPhone__c;
                    
                    if(companyName != null){
                        acc.Name = companyName;
                    }
                    
                    //check if there's an contact in company account with this name
                    lsContact = [SELECT Id, FirstName, LastName
                                FROM Contact
                                WHERE FirstName = :cnt.FirstName
                                AND LastName = :cnt.LastName
                                ORDER BY CreatedDate ASC];
                    
                    //update or insert the contact within existing account
                    if(!lsContact.isEmpty() && lsContact != null){
                        cnt.Id = lsContact[0].Id;
                    }
                    
                }
                
                if(leadOld != null && leadOld.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))){
                    acc.Cliente_DVE__c = true;
                    if(leadOld.Type_DVE__c != null && !leadOld.Type_DVE__c.equals(''))
                        acc.Type_DVE__c = leadOld.Type_DVE__c;
                }
                Database.update(acc);
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.upsert(cnt);
                }
            }
            if( account == null && leadOld != null){
                
                if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')){
                    
                    acc.Cliente_DVE__c = true;
                    
                    if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                        
                        cnt.RecordTypeId = Utils.getRecordTypeId('Contact', 'Network_Ctc');
                        cnt.PersEmail__c = acc.PersEmailAddress__c;
                        cnt.HomePhone = acc.PersLandline__c;
                        cnt.PersMobPhone__c = acc.PersMobPhone__c;
                        
                        acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    	acc.Phone = acc.PersLandline__c;
                    	acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                        acc.Name = companyName;
                        
                    } else{
                        acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                        acc.FirstName = cnt.FirstName;
            			acc.LastName = cnt.LastName;
                    }                    
                    
                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                    acc.FirstName = cnt.FirstName;
            		acc.LastName = cnt.LastName;
                }
                
                acc.Source__c = leadOld.LeadSource;
                acc.AccSubSource__c = leadOld.SubSource__c; 
                acc.Detail__c = leadOld.Detail__c;
                acc.Sub_Detail__c = leadOld.Sub_Detail__c;
                
                Database.Upsert( acc );
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.insert(cnt);
                }
            }
            if( account == null && leadOld == null ){
                
                if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')){
                    
                    acc.Cliente_DVE__c = true;
                    
                    if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                        
                        cnt.RecordTypeId = Utils.getRecordTypeId('Contact', 'Network_Ctc');
                        cnt.PersEmail__c = acc.PersEmailAddress__c;
                    	cnt.HomePhone = acc.PersLandline__c;
                    	cnt.PersMobPhone__c = acc.PersMobPhone__c;
                        
                    	acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                        acc.Name = companyName;
                        acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    	acc.Phone = acc.PersLandline__c;
                    	
                        
                        
                    } else{
                        acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                        acc.FirstName = cnt.FirstName;
            			acc.LastName = cnt.LastName;
                    }                    
                    
                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                    acc.FirstName = cnt.FirstName;
            		acc.LastName = cnt.LastName;
                }
                
                acc.Source__c = 'DEALER';
                acc.AccSubSource__c = 'PASSING';
                acc.Detail__c = 'NEW VEHICLE';
                if(!Test.isRunningTest()) Database.Upsert( acc );
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.insert(cnt);
                }
                
            }
            
            //get lead that is entering
            if(leadId != null) lead = VFC09_LeadDAO.getInstance().findLeadByLeadId(leadId);
            
            //convert lead that was created before the lead that is entering.
            if(LeadOld != null && LeadOld.IsConverted == false) convertLead(acc.Id, leadOld);
            
            //convert the new lead
            if(lead != null && leadOld != NULL && lead.IsConverted == false && lead.Id != leadOld.Id) convertLead(acc.Id, lead);
            
            //insert opportunity
            if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')) opp.RecordTypeId = oppRecTypeVendaDireta;
            
            System.debug('RECORD TYPE DA OPP ANTES DE INSERIR: ' + opp.RecordType.Name);
            insertOpportunity(dealerId);
            
            /*--Finish Code-- Modificado por @Edvaldo - kolekto --*/
            
            //coloca o Lead em quarentena - by @Felipe Livino - kolekto
            if(leadId != null) retailPlataform(leadId);
            
        }catch( Exception e ){
            Database.rollback(sp);
            String message = e.getMessage();
            Apexpages.addMessage( new Apexpages.Message(
                ApexPages.Severity.ERROR,
                message.contains( '_EXCEPTION, ' ) ? message.substringAfter( '_EXCEPTION, ' ) :
                message.contains( 'DUPLICATE_VALUE' ) && message.contains( 'CustomerIdentificationNbr__c' ) ? Label.VFP23_NewOpportunity_DuplicateId :
                message
            ) );
            return null;
        }
        
        if( isSeller ) {
            
            Pagereference newPage;
            
            if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Seminovos')
              || opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')
              || opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa_PF')){
                
                newPage = new PageReference('/' + opp.Id);
            } else{
            
            newPage = Page.VFP06_QualifyingAccount;
            newPage.getParameters().put( 'Id', opp.Id );
                
            }
            return newPage;
        }
        
        Apexpages.addMessage( new Apexpages.Message( 
            ApexPages.Severity.Confirm, 'Negociação encaminhada para ' + sellerName ) );
        clearObjects();
        return null;
    }
    
    /*--Start Code-- Métodos criados por @Edvaldo - kolekto --*/
    
    //--Start Code--  modified by Felipe Livino {kolekto} || Retail Plataform
    public void retailPlataform(Id leadId){
        List<Quarantine__c> listaQuarentena = [select id from Quarantine__c where Lead__c =: leadId];
        System.debug('###### listaQuarentena '+listaQuarentena);
        List<Quarantine__c> listaUpdateQuarentena = new List<Quarantine__c>();
        for(Quarantine__c quarentena : listaQuarentena){
            quarentena.Account__c = acc.Id;
            quarentena.Opportunity__c = opp.id;
            listaUpdateQuarentena.add(quarentena);
        }
        Database.update (listaUpdateQuarentena);
        System.debug('###### listaUpdateQuarentena '+listaUpdateQuarentena);
    }
    //--Finish Code--  modified by Felipe Livino {kolekto} || Retail Plataform
    
    public void insertOpportunity(Id dealerId){
        if ( opp.Vehicle_Of_Interest__c != null ){
            String vehicle = opp.Vehicle_Of_Interest__c;
            
            List< Product2 > product = VFC22_ProductDAO.getInstance().findProductRecordsbyName( vehicle );
            
            if (!product.isEmpty()){
                PricebookEntry pbEntry = VFC24_PriceBookDAO.getInstance().findPricebyProductId( product[0].Id );
                if ( pbEntry != null )
                    opp.Amount = pbEntry.UnitPrice;
            }
        }
        opp.Name = Label.OPPName + ' ' + '(' + cnt.FirstName + ' ' + cnt.LastName + ')';
        opp.AccountId = acc.Id;
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today() + 30;
        opp.OpportunitySource__c = 'DEALER';
        opp.OpportunitySubSource__c = 'PASSING';
        opp.Detail__c = 'NEW VEHICLE';
        opp.Dealer__c = dealerId;
        Database.insert( opp );
    }
    
    public void updateLeadFromSFA(lead l){
        l.HomePhone__c = acc.PersLandline__c;
        l.Email = acc.PersEmailAddress__c;
        l.MobilePhone = acc.PersMobPhone__c;
        l.OwnerId = userinfo.getUserId();
        l.SFAConverted__c = true;
        Database.update(l);
        system.debug('***** Lead= '+l);
    } 
    
    public void convertLead(Id accountId, Lead l){
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity( true );
        lc.setLeadId(l.Id);
        if(l.RecordTypeId == Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))
            lc.setConvertedStatus('Qualified');
        else
            lc.setConvertedStatus('Hot Lead');
        
        // if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (l.Owner.type == 'Queue')
            lc.setOwnerId( Userinfo.getUserId() );
        
        Database.Leadconvertresult successLeadConversion;
        if(!Test.isRunningTest()) successLeadConversion = Database.convertLead(lc);
        Id oppId = successLeadConversion != null ? successLeadConversion.getOpportunityId() : null;
    }

    /*--Finish Code-- Métodos criados por @Edvaldo - kolekto --*/
    
    public void selectAccount(){
        Set< String > accFieldSet = new Set< String >( (List< String >)VFC141_RMCNew_Opportunity_Controller.returnFieldMap.get( 'Account' ) );
        accFieldSet.addAll( (List< String >)VFC141_RMCNew_Opportunity_Controller.searchFieldMap.get( 'Account' ) );
        acc = Database.query( 'select ' + String.join( new List< String >( accFieldSet ), ', ' ) + 
                             ',Type_DVE__c,IsPersonAccount, PersEmailAddress__c from Account where Id = \'' + acc.Id + '\' limit 1' );
        cnt.FirstName = acc.FirstName;
        cnt.LastName = acc.LastName;
        sellerId = Userinfo.getUserId();
    }
    
    public void selectLead(){
        Lead baseLead = [select Id, FirstName, LastName, LeadSource, SubSource__c, Detail__c, Sub_Detail__c
                         from Lead where Id = :leadId];
        
        cnt.FirstName = baseLead.FirstName;
        cnt.LastName = baseLead.LastName;
        sellerId = Userinfo.getUserId();
    }
    
    @testVisible
    private class QueryWrapper{
        public String SObjectName{get;set;}
        private List< String > returnFieldList;
        private String searchStr;
        public String queryStr{
            get{
                return 'find \'' + this.searchStr + '\' in all fields returning ' + this.SObjectName + '(Id, ' + String.join( returnFieldList, ', ' ) + ' limit 20)';
            }
        }
        
        public QueryWrapper (String searchStr, String SObjectName){
            this.searchStr = searchStr;
            this.SObjectName = SObjectName;
            this.returnFieldList = (List< String >)VFC141_RMCNew_Opportunity_Controller.returnFieldMap.get( this.SObjectName );
            System.assert( returnFieldList != null );
        }
    }	
    
    @RemoteAction
    public static SObject[] queryObject (String searchStr, String SObjectName){
        QueryWrapper queryObj = new QueryWrapper( searchStr, SObjectName );
        system.debug('###########'+queryObj.queryStr);
        return Search.query( queryObj.queryStr )[0];
    }
    
    /**
    * Remover a formatacao do CPF ou CNPJ
    */
    public static String limparFormatacaoCpfCnpj(String param) {
        if(param == null) return param;
        return param.replaceAll('[/\\.-]', '');
    }
}