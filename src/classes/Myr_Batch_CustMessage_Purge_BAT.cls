/** Purge of Customer_Message
  @author Sebastien Ducamp
  @version 1.0
  @date 23.07.2015
  A batch is scheduled every day to remove the customer messages answering to the following criterias: 
   - Customer_Message__c.Status__c = deleted AND StatusUpdateDate__c > Now() – 1 mois
   - Customer_Message__c.ExpirationDate__c < today
**/
global class Myr_Batch_CustMessage_Purge_BAT extends INDUS_Batch_AbstractBatch_CLS {
  
  //TODO: add schedulable class
  
  /** Inner custom exception class **/
  public class Myr_Batch_CustMessage_Purge_BAT_Exception extends Exception {}
  
  global enum TEST_FAILURE {FAIL_START_TEST}
  @TestVisible private TEST_FAILURE TestFailureSimulation;

  //Deleted retention tag to replace
  private static final String DELETED_RETENTION = '<DELETED_RETENTION>';
  //Retention History to check value in test methods
  @TestVisible private Integer m_retention;
  //Parent Log Id
  private Id m_ParentLog_Id;
  
  @TestVisible private String m_query = 'SELECT Id FROM Customer_Message__c WHERE'
    + ' (Status__c=\'' + system.Label.Customer_Message_Deleted + '\' AND StatusUpdateDate__c <= LAST_N_DAYS:'+DELETED_RETENTION+')'
    + ' OR ( ExpirationDate__c < TODAY)';  
  
    /** Empry constructor **/
    global Myr_Batch_CustMessage_Purge_BAT() {
        system.debug('### Myr_Batch_CustMessage_Purge_BAT - <constructor> - BEGIN');
        m_retention = Integer.valueOf(CS04_MYR_Settings__c.getInstance().Myr_Customer_Message_PurgeRetention__c);
        if( m_retention == null ) {
            m_retention = 30;
        }
        m_query = m_query.replace(DELETED_RETENTION, String.valueOf(m_retention));
        system.debug('### Myr_Batch_CustMessage_Purge_BAT - <constructor> - END query='+m_query);
    }
  
    /** Start function **/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('### Myr_Batch_CustMessage_Purge_BAT - <start> - BEGIN, query=' + m_query);
        try {
            if( Test.isRunningTest() && TEST_FAILURE.FAIL_START_TEST == TestFailureSimulation) {
                throw new Myr_Batch_CustMessage_Purge_BAT_Exception('Simulate Failure');
            }
            m_ParentLog_Id = INDUS_Logger_CLS.addLog(INDUS_Logger_CLS.ProjectName.MYR, Myr_Batch_CustMessage_Purge_BAT.class.getName(), 'start', '', '', m_query, INDUS_Logger_CLS.ErrorLevel.Info);
        } catch (Exception e) {
            //continue without blocking exception
        }
    
        try {
            Database.QueryLocator ql = Database.getQueryLocator(m_query);
            return ql;
        } catch (Exception e) {
            String message = 'FATAL ERROR: query is mal-formatted, message=' + e.getMessage();
            system.debug('### Myr_Batch_CustMessage_Purge_Batch - <start> - ' + message);
            INDUS_Logger_CLS.addLogChild(m_ParentLog_Id, INDUS_Logger_CLS.ProjectName.MYR, Myr_Batch_CustMessage_Purge_BAT.class.getName(), 'start', '', e.getTypeName(), message, INDUS_Logger_CLS.ErrorLevel.Critical);
            throw new Myr_Batch_CustMessage_Purge_BAT_Exception(Myr_Batch_CustMessage_Purge_BAT.class.getName() +': unable to initialize the query locator='+e.getMessage());
        }
    }
  
    /** Execute the batch : delete the given scope **/
    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> scope){
        system.debug('### Myr_Batch_CustMessage_Purge_Batch - <execute> - delete scope='+scope);
        return new DatabaseAction(scope, Action.ACTION_DELETE, false);
    }
    
    /** Finalize the batch: log the results **/
    global override void doFinish(Database.BatchableContext info){ 
        system.debug('### Myr_Batch_CustMessage_Purge_Batch - <execute> - END');
    }

    /** @returns the name of the current class **/
    global override String getClassName(){
        return Myr_Batch_CustMessage_Purge_BAT.class.GetName();
    }
}