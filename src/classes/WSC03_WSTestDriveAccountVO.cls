/**
*   WebService Class    -   WSC03_WSTestDriveAccountVO
*   Author              -   Suresh Babu
*   Date                -   28/11/2012
*   
*   #01 <Suresh Babu> <28/11/2012>
*       Account Value Object class to return Dealer Account values to web service.
**/
global class WSC03_WSTestDriveAccountVO {
	webservice String accountId {get;set;}
	webservice String accountName {get;set;}
	webservice String accountStreet {get;set;}
	webservice String accountCity {get;set;}
	webservice String accountState {get;set;}
	webservice String accountPostalCode {get;set;}
	webservice String accountCountry {get;set;}
	webservice Integer accountSFDCDeployed {get;set;}
	webservice String errorMessage {get;set;}
	
	global WSC03_WSTestDriveAccountVO(){} 
}