global class BatchExpiredOpportunity implements Database.Batchable<SObject>, Schedulable  {
    
    global String queryString;

    global void execute(SchedulableContext BC) {
        BatchExpiredOpportunity expired = new BatchExpiredOpportunity();
        String sch = ' 0 30 9 * * ?';
        system.schedule('Expired Opportunity', sch, expired);
    }
    
    global Database.querylocator start(Database.BatchableContext BC) {
		Database.querylocator objQL; 
        
        Id recId = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
    
        queryString = 'SELECT Id, Name, DealerDirectorFstName__c,IDBIR__c, DealerDirectorPhone__c, DealerManagerFstName__c, DealerManagerPhone__c, DealerOwner__c, OwnerPhone__c  FROM Account WHERE DealershipStatus__c = \'Active \'  AND Account.Country__c = \'Brazil\' AND RecordTypeId = \''+ String.escapeSingleQuotes(recId)+'\'';
        System.debug('queryString&&'+queryString);
    	return Database.getQueryLocator(queryString);
    }
	
    global void execute(Database.BatchableContext BC, List<Account> scope) {
	    
	    if(!test.isRunningTest()){
	        WSMarketingCloud.getConfiguration();
	        WSMarketingCloud.login();
	    }
	    map<Id,Integer> countOpp = new Map<Id,Integer>();
	    
	    AggregateResult[] lstOpp = [SELECT  COUNT(Id) opps,
	                                        AccountId
	                                    FROM Opportunity 
	                                    WHERE SLA_Dealer_Expired__c  = true
	                                    AND  Vehicle_Of_Interest__c = 'CAPTUR'
	                                    AND StageName = 'Identified'
	                                    AND AccountId IN: scope
	                                    GROUP BY AccountId];
	    
	    for(AggregateResult ar : lstOpp){
           countOpp.put((Id)ar.get('AccountId'), (Integer)ar.get('opps'));
        }
	    
	    Map<String, Integer> mapOpp = new Map<String, Integer>();   
       
        for(Account itemAcc : scope){
            Integer oppExpired = countOpp.get(itemAcc.Id);
             if(!test.isRunningTest()){
                WSMarketingCloud.sendSMS(itemAcc, oppExpired);    
             }
        }
    }
    
    global void finish(Database.BatchableContext  BC) {
       
        
    }
    
}