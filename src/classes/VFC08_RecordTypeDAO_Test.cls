/**
	Class   -   VFC08_RecordTypeDAO_Test
    Author  -   Ramesh Prabu
    Date    -   05/10/2012
    
    #01 <Suresh Babu> <05/10/2012>
        Created this Class to handle test class for VFC08_RecordTypeDAO.
**/
@isTest
private class VFC08_RecordTypeDAO_Test {

    static testMethod void VFC08_RecordTypeDAOTest() {
        // TO DO: implement unit test
        String developerName = 'RCM_CommercialRecommendation';
        test.startTest();
		RecordType recordTypeId =  VFC08_RecordTypeDAO.getInstance().findRecordTypeByDeveloperName(developerName);
		Test.stopTest();
		System.assert(recordTypeId != null);
    }
}