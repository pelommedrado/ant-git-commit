@isTest
private class TestExceptionClass {

   	static testMethod void testVFC115_CreateOpportunityException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC115_CreateOpportunityException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC88_CreateVehicleBookingException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC88_CreateVehicleBookingException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC89_NoDataFoundException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		VFC89_NoDataFoundException ex = new VFC89_NoDataFoundException();
        		ex.exceptionHandling();
        		throw new VFC89_NoDataFoundException('Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC90_DeleteQuoteLineItemException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC90_DeleteQuoteLineItemException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC91_UpdateOpportunityException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC91_UpdateOpportunityException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC92_UpdateQuoteException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC92_UpdateQuoteException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC93_UpdateQuoteLineItemException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC93_UpdateQuoteLineItemException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC93_UpdateVehicleException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC93_UpdateVehicleException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }
    
    static testMethod void testVFC82_CreateQuoteException() {
        
        try{
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }catch(Exception e){
        	try{
        		throw new VFC82_CreateQuoteException(e,'Mensagem');
        	}catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        	
        }
        
    }	
    
    
    static testMethod void testVFC58_CreateTestDriveException()
    {
        try {
        	VFC113_AccountBO.getInstance().getById('invalidaid');
        }
        catch(Exception e){
        	try {
        		throw new VFC58_CreateTestDriveException(e,'Mensagem');
        	}
        	catch(Exception ex){
        		System.assert(e.getMessage().contains('Invalid id'));
        	}
        }
    }

}