/** Mock WebService for MDM
  * Used for test purposes 
  **/
@isTest
global class Myr_PersonalDataMDM00_WS_MK implements WebServiceMock {
	
	private Boolean MYRENAULT_MODE;

	/** @constructor **/	
	global Myr_PersonalDataMDM00_WS_MK() {
		MYRENAULT_MODE = false;
	}
	
	/** @constructor **/	
	global Myr_PersonalDataMDM00_WS_MK(Boolean MyRenaultMode) {
		MYRENAULT_MODE = MyRenaultMode;
	}

	/** Simulates inovkation **/
	global void doInvoke( Object stub, Object request, Map<String, Object> response,
           					String endpoint, String soapAction, String requestName,
           					String responseNS, String responseName, String responseType) {
    
    	system.debug('#### Myr_PersonalDataMDM00_WS_MK - doInvoke - BEGIN request=' + request);
    	if( requestName == 'SearchPartyRequest' ) {
    		response.put( 'response_x', buildSearchPartyResponse() );
    	} else if ( requestName == 'GetPartyRequest' ) {
    		response.put( 'response_x', buildDetailedResponse() );
    	}
    	system.debug('#### Myr_PersonalDataMDM00_WS_MK - doInvoke - END');       		           	
    }
    
    /** Build a response with several persons **/
    public Myr_PersonalDataMDM00_WS.SearchPartyResponse_element buildSearchPartyResponse() {
    	Myr_PersonalDataMDM00_WS.SearchPartyResponse_element resp = new Myr_PersonalDataMDM00_WS.SearchPartyResponse_element();
    	resp.ResponseMessage = 'Search Party OK';
    	List<Myr_PersonalDataMDM00_WS.PartyOverview> listParty = new List<Myr_PersonalDataMDM00_WS.PartyOverview>();
    	//In MyRenault mode, returns only one response
    	//response 1
    	Myr_PersonalDataMDM00_WS.PartyOverview party1 = new Myr_PersonalDataMDM00_WS.PartyOverview();
    	party1.PartyId = '345678';
        party1.PartyType = '';
        party1.MatchScore = 99;
        party1.FirstName = 'JONATHAN';
        party1.LastName = 'LAMBERT';
        party1.DateOfBirth = Date.newInstance(1980,1,1);
        party1.Sex = '1';
        party1.Language = 'FR';
        party1.AddressLine1 = '71 RUE DES AMANDIERS';
        party1.AddressLine2 = '';
        party1.City = 'PARIS';
        party1.Region = '';
        party1.Postcode = '75000';
        party1.Country = 'FRANCE';
        party1.FixedPhoneNumber = new List<String>();
        party1.FixedPhoneNumber.add('+33 3 29 66 53 42');
        party1.MobilePhoneNumber = new List<String>();
        party1.MobilePhoneNumber.add('+33 6 85 29 09 96');
        party1.EmailAddress = new List<String>();
        party1.EmailAddress.add('mag.perrin21@free.fr');
        listParty.add(party1);
        if( !MYRENAULT_MODE ) {
	    	//response 2
	    	Myr_PersonalDataMDM00_WS.PartyOverview party2 = new Myr_PersonalDataMDM00_WS.PartyOverview();
	    	party2.PartyId = '1082019';
	        party2.PartyType = '';
	        party2.MatchScore = 90;
	        party2.FirstName = 'JONATHAN';
	        party2.LastName = 'LAMBERT';
	        party2.DateOfBirth = Date.newInstance(1980,1,1);
	        party2.Sex = '1';
	        party2.Language = 'FR';
	        party2.AddressLine1 = '4 IMPASSE SAMUEL DE CHAMPLAIN';
	        party2.AddressLine2 = '';
	        party2.City = 'PARIS';
	        party2.Region = '';
	        party2.Postcode = '75000';
	        party2.Country = 'FRANCE';
	        party2.FixedPhoneNumber = new List<String>();
	        party2.FixedPhoneNumber.add('+33 3 29 66 53 42');
	        party2.MobilePhoneNumber = new List<String>();
	        party2.MobilePhoneNumber.add('+33 6 85 29 09 96');
	        party2.EmailAddress = new List<String>();
	        party2.EmailAddress.add('mag.perrin21@free.fr');
	        listParty.add(party2);
        }
        resp.PartyOverview = listParty;
        return resp;
    }
    
    /** Build the detailed response for one person **/
    public  Myr_PersonalDataMDM00_WS.GetPartyResponse_element buildDetailedResponse() {
    	Myr_PersonalDataMDM00_WS.Party party = new Myr_PersonalDataMDM00_WS.Party();
		party.PartyType = 'I';
		party.PartySubType = 'AC';
		party.PartyStatus = 'A';
		party.FirstName1 = 'JONATHAN';
		party.LastName1 = 'LAMBERT';
		party.FullName = 'JONATHAN LAMBERT';
		party.DateOfBirth = Date.newInstance(1979, 1, 1);
		party.Civility = 'M';
		party.Sex = 'M';
		party.Language = 'FRE';
		party.OccupationalCategoryCode = 'EMPLOYE';
		party.OccupationalCategoryDescription = 'B1';
		party.PartySegment = 'OTHER';
		party.ElectronicAddress = new List<Myr_PersonalDataMDM00_WS.ElectronicAddress>();
		Myr_PersonalDataMDM00_WS.ElectronicAddress email = new Myr_PersonalDataMDM00_WS.ElectronicAddress();
		email.ElectronicAddressValue = 'mag.perrin21@free.fr';
		party.ElectronicAddress.add(email);
		party.PhoneNumber = new List<Myr_PersonalDataMDM00_WS.PhoneNumber>();
		Myr_PersonalDataMDM00_WS.PhoneNumber land = new Myr_PersonalDataMDM00_WS.PhoneNumber();
		land.PhoneNumberType = 'LAND';
		land.PhoneNumberValue = '+33 3 29 66 53 42';
		land.Valid = 'Y';
		land.LastUpdateDate = Date.newInstance(2014, 7, 29);
		party.PhoneNumber.add(land);
		Myr_PersonalDataMDM00_WS.PhoneNumber mobile = new Myr_PersonalDataMDM00_WS.PhoneNumber();
		mobile.PhoneNumberType = 'MOBILE';
		mobile.PhoneNumberValue = '+33 6 85 29 09 96';
		mobile.Valid = 'Y';
		mobile.LastUpdateDate = Date.newInstance(2014, 7, 29);		
		party.PhoneNumber.add(mobile);
		Myr_PersonalDataMDM00_WS.Vehicle veh = new Myr_PersonalDataMDM00_WS.Vehicle();		
		veh.VehicleType = 'VP';
		veh.VehicleIdentificationNumber = 'VF1BMSE0637005985';
		veh.Brand = 'DACIA';
		veh.BrandLabel = 'DACIA';
		veh.Model = 'TW2';
		veh.ModelLabel = 'TWINGO II';
		veh.CartecId = '63468';
		veh.RegistrationDate = Date.newInstance(2006, 12, 12);
		veh.CurrentMileage = 0;
		veh.CurrentMileageDate = Date.newInstance(1900, 1, 1);
		veh.PreviousRegistrationNumber = '7938VH88';
		veh.PartyVehicleType = 'OWN';
		veh.DeliveryDate = Date.newInstance(2006, 12, 12);
		veh.OrderDate = Date.newInstance(2007, 1, 2);
		veh.PurchaseDate = Date.newInstance(2007, 1, 2);
		veh.RegistrationNumber = '7938VH88';
		veh.PurchaseMileage = 0;
		veh.NewVehicle = 'Y';
		veh.StartDate = Date.newInstance(2014, 7, 29);
		party.Vehicle = new List<Myr_PersonalDataMDM00_WS.Vehicle>();
		party.Vehicle.add(veh);
		Myr_PersonalDataMDM00_WS.GetPartyResponse_element mdmResponse = new Myr_PersonalDataMDM00_WS.GetPartyResponse_element();
		mdmResponse.ResponseMessage = ':: Get Party OK ::';
		mdmResponse.Party = party;
		return mdmResponse;
    }
}