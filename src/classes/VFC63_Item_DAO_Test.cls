/**
	Class   -   VFC63_Item_DAO_Test
    Author  -   Subbarao
    Date    -   13/03/2013
    
    #01 <Subbarao> <13/03/2013>
        Created this class using test for VFC63_Item_DAO.
**/
@isTest 
private class VFC63_Item_DAO_Test
{
	static testMethod void VFC63_Item_DAO_Test()
	{
		// Prepare test data
		Id recordTypeColorId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Color'].Id;
		Id recordTypeOptionId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Option'].Id;
		
		Item__c item01 = new Item__c(Name = 'Color_01', 
								     Key__c = 'COLOR-001', 
								     Label__c = 'Blue',
								     recordTypeId = recordTypeColorId);
		insert item01;
		Item__c item02 = new Item__c(Name = 'Option_01', 
									 Key__c = 'OPTION-001',
									 Label__c = 'FREIOS ABS',
									 recordTypeId = recordTypeOptionId);
		insert item02;

        Item__c Itemslst = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordtoItem();

        // Start test
        Test.startTest();
        
        // Test 01
        List<Item__c> fetchedItems = VFC63_Item_DAO.getInstance().fetchAll_Item_Records();
        
        // Test 02
        VFC63_Item_DAO.getInstance().fetch_Items_Map_Name();
        
        // Test 03
        Map<String, Item__c> test03 = VFC63_Item_DAO.getInstance().fetch_Color_Items_Map_Name();

        // Test 04
        Map<String, Item__c> test04 = VFC63_Item_DAO.getInstance().fetch_Option_Items_Map_Name();

        // Stop test
        Test.stopTest();
        
        // Validade test
        system.assert(fetchedItems.size()>0);
	} 
}