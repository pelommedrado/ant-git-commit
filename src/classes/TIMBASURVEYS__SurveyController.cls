/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class SurveyController {
    @Deprecated
    global static void insertRecipientForContact(String TScontactId, String TSleadId, String TSuserRecId, String TSpAccId, String wfContactId, String wfUserId, String wfLeadId, String wfAccountId, String FromWorkflow, Boolean AllowSurvey, String SurveyId) {

    }
}
