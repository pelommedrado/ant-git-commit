/**
 * This class tests only the case where there ar absolutely no global settings for Gigya => No CS_Gigya_Settings
 * This can not been tested within Myr_GigyaFunctions_Test because we initialize the settings
 * in testsetup function, after that event if we try to delete the custom settings, it's impossible
 * to get a null setings.
 *
 * @author S. Ducamp
 * @date 05.10.2015
 * 
 * @version 1.0: test missing global setting
 */
@isTest
private class Myr_GigyaCaller_Test { 
	
	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		Myr_Datasets_Test.prepareGigyaCustomSettings([select IsSandbox from Organization].IsSandbox);
	}
		
	//Return the Gigya_Datacenter_NoProd__c or Gigya_Datacenter__c
	private static String getGigyaDatacenter(Country_Info__c country) {
		if ([select IsSandbox from Organization].IsSandbox){
			return country.Gigya_Datacenter_NoProd__c;
		} else {
			return country.Gigya_Datacenter__c;
		}		
	}

    //KO: No Gigya settings into CS_Gigya_Settings__c
    static testMethod void test_settings_NoGigyaSettings() {
		CS_Gigya_Settings__c csGigya = [SELECT Id FROM CS_Gigya_Settings__c];
		delete csGigya;
    	//test
    	Boolean catchedException = false;
    	try {
    		Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls('Italy', 'Renault',Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
    	} catch (Myr_GigyaCaller_Cls.MYR_GigyaCaller_Exception e) {
    		system.assertEquals(system.Label.GIGYA_GigyaCaller_NoGigyaSettings, e.getMessage());
    		catchedException = true;
    	} 
    	system.assertEquals(true, catchedException);
    }
    
    //KO: Settings into CS_Gigya_Settings__c and fake Brand
    static testMethod void test_settings_FakeBrand() {
    	//insert the Gigya Settings
    	CS_Gigya_Settings__c gigSettings = new CS_Gigya_Settings__c();
		gigSettings.Url_Token__c = 'https://socialize.gigya.com/socialize.getToken?';
		gigSettings.Url_Reset_Password__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resetPassword?';
		gigSettings.Url_Resend_Activation_Email__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resendVerificationCode?';
		gigSettings.Url_Account_Search__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.search?';
		insert gigSettings;
    	//test
    	Boolean catchedException = false;
    	try {
    		Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls('Italy', 'TrucMuche', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
    	} catch (Myr_GigyaCaller_Cls.MYR_GigyaCaller_Exception e) {
			String message = String.format(system.Label.GIGYA_GigyaCaller_CountryNoBrand, new List<String>{'Italy', 'TrucMuche', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD.name()});
    		system.assertEquals(e.getMessage(), message);
    		catchedException = true;
    	} 
    	system.assertEquals(true, catchedException);
    }
    
    //KO: Settings into CS_Gigya_Settings__c and fake Brand
    static testMethod void test_settings_FakeCountry() {
    	//insert the Gigya Settings
    	CS_Gigya_Settings__c gigSettings = new CS_Gigya_Settings__c();
		gigSettings.Url_Token__c = 'https://socialize.gigya.com/socialize.getToken?';
		gigSettings.Url_Reset_Password__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resetPassword?';
		gigSettings.Url_Resend_Activation_Email__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resendVerificationCode?';
		gigSettings.Url_Account_Search__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.search?';
		insert gigSettings;
    	//test
    	Boolean catchedException = false;
    	try {
    		Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls('BizarreLand', 'TrucMuche', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
    	} catch (Myr_GigyaCaller_Cls.MYR_GigyaCaller_Exception e) {
    		system.assertEquals(system.Label.GIGYA_GigyaCaller_NoCountry.replace('{0}', 'BizarreLand'), e.getMessage());
    		catchedException = true;
    	}
    	system.assertEquals(true, catchedException);
    }

	//Check behaviour when theyr are not datacenter id filled
	static testmethod void test_NoDataCenter() {
		//prepare a custom setting without datacenter id
		Country_Info__c setRomania = new Country_Info__c( Name='Romania', 
			Language__c = 'Romanian',
			Gigya_Datacenter_NoProd__c = '',
			Gigya_REN_K_NoProd__c = LMT_Crypto_CLS.encode('gigya:daciakey', system.Label.Myr_Gigya_Key)
		);
		insert setRomania;
		//Trigger the test
		Boolean catchedException = false;
    	try {
    		Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls('Romania', 'Renault', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
    	} catch (Myr_GigyaCaller_Cls.MYR_GigyaCaller_Exception e) {
			String message = String.format( system.Label.GIGYA_GigyaCaller_NoDataCenterId, new List<String> { 'Romania', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD.name() } );
    		system.assertEquals(message, e.getMessage());
    		catchedException = true;
    	}
    	system.assertEquals(true, catchedException);
	}

	//Check behaviour when theyr are not datacenter id filled
	static testmethod void test_NoApiKey() {
		//prepare a custom setting without datacenter id
		Country_Info__c setRomania = new Country_Info__c( Name='Romania', 
			Language__c = 'Romanian',
			Gigya_Datacenter_NoProd__c = 'us1',
			Gigya_REN_K_NoProd__c = ''
		);
		insert setRomania;
		//Trigger the test
		Boolean catchedException = false;
    	try {
    		Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls('Romania', 'Renault', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
    	} catch (Myr_GigyaCaller_Cls.MYR_GigyaCaller_Exception e) {
			String message = String.format( system.Label.GIGYA_GigyaCaller_CountryNoBrand, new List<String> { 'Romania', 'Renault', Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD.name() } );
    		system.assertEquals(message, e.getMessage());
    		catchedException = true;
    	}
    	system.assertEquals(true, catchedException);
	}

	//Check the Gigya query as this could not be checked using Mock
	static testmethod void test_CheckGigyaQuery() {
		String url = 'fakeUrl';
		String token = 'token';
		String datacenterId = 'eu1';
		String accId = '001b000003ACIFvAAP'; 
		Myr_GigyaCaller_Cls.SearchAccountRequest caller = new Myr_GigyaCaller_Cls.SearchAccountRequest(url, token, datacenterId, accId);
		String queryOk = 'query=select+UID%2C+isRegistered%2C+isActive%2C+isVerified%2C+profile.firstname%2C+profile.lastname%2C+profile.email%2C+loginIDs.username%2C+loginIDs.emails%2C+loginIDs.unverifiedEmails%2C+emails.verified%2C+emails.unverified%2C+registered%2C+created%2C+lastLogin%2C+data.accountSfdcActivated%2C+data.newEmail+from+accounts+where+data.accountSfdcId+%3D+%27'+accId+'%27';
		String generatedQuery = caller.generateHttpQuery();
		system.debug('query OK   =' + queryOk );
		system.debug('query Gigya=' + generatedQuery );
		system.assertEquals( queryOk, generatedQuery );
	}

	//Test method to increase code coverage of the gigya parser
	static testMethod void test_GetResults() {
		Country_Info__c infoItaly = Country_Info__c.getInstance('Italy');
		//Prepare the mock for results
		Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(infoItaly));
        mock.setToken_OK();
        mock.setAccSch_OK(); //response OK on the 1st call
		Test.setMock( HttpCalloutMock.class, mock ); 

		//Call to get results
		Test.startTest();
		Myr_GigyaCaller_Cls caller;
		if([select IsSandbox from Organization].IsSandbox){
			caller = new Myr_GigyaCaller_Cls('Italy', Myr_MyRenaultTools.Brand.Renault.name(),Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);
		} else {
			caller = new Myr_GigyaCaller_Cls('Italy', Myr_MyRenaultTools.Brand.Renault.name(),Myr_GigyaCaller_Cls.ENVIRONMENT.PROD);		
		}
		Myr_GigyaParser_Cls.GigyaAccountParser foundGigya = caller.searchAccount( '0017E0000074LDo' );
		Test.stopTest();

		//Check some values in the results
		system.assertEquals('31f50cda43114fdf8e8b50f92083a74f', foundGigya.results[0].UID );
		system.assertEquals(false, foundGigya.results[0].isVerified );
		system.assertEquals(true, foundGigya.results[0].isRegistered );
		system.assertEquals(true, String.valueOf(foundGigya.results[0].created).contains('2015-08-26') );
		system.assertEquals(null, foundGigya.results[0].lastLogin );
		system.assertEquals('{}', String.valueOf(foundGigya.results[0].loginIDs.emails) );
		system.assertEquals('{testing_register3@mailinator.com}', String.valueOf(foundGigya.results[0].loginIDs.unverifiedEmails) );
		system.assertEquals('{}', String.valueOf(foundGigya.results[0].emails.verified) );
		system.assertEquals('{testing_register3@mailinator.com}', String.valueOf(foundGigya.results[0].emails.unverified) );
		system.assertEquals( 'testing_register3@mailinator.com', foundGigya.results[0].profile.email );
		system.assertEquals( null, foundGigya.results[0].profile.firstName );
		system.assertEquals( null, foundGigya.results[0].profile.lastName );
		system.assertEquals( null, foundGigya.results[0].profile.age );
		system.assertEquals( null, foundGigya.results[0].profile.gender );
		system.assertEquals( null, foundGigya.results[0].profile.country );
	}
}