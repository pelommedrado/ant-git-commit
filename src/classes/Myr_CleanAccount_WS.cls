/* Clean of a myRenault access (Webservice) 
   Error codes : OK              : WS10MS000 
                 OK WITH WARNING : WS10MS001 -> WS10MS500
                 CRITICAL ERROR  : WS10MS501 -> WS10MS999
*********************************************************************************************************
2016 Dec : R17.01 v13.0 AmberFoxBaby, D. Veron (AtoS) : Creation
*********************************************************************************************************/
global without sharing class Myr_CleanAccount_WS {
	private static Long Begin_Time = DateTime.now().getTime();
	private static String logRunId = Myr_CleanAccount_WS.class.getName() + Begin_Time;
	private static String Prefix = '####' + Myr_CleanAccount_WS.class.getName() + '.';
	
    WebService static Myr_CleanAccount_WS_Response CleanAccount(Myr_CleanAccount_WS_Question q) {
		Myr_CleanAccount_WS_Response rep;
		Handler_CleanAccount H_C=new Handler_CleanAccount(q);
		
	  	rep=H_C.Check_Inputs();
		if (rep!=null){
			return rep; 
		}

		rep=H_C.Database_Query();
		if (rep!=null){
			return rep;
		}

		rep=H_C.Clean_Account();
		if (rep!=null){
			return rep;
		}
		
		return H_C.Delete_Account();
    }

	public class Handler_CleanAccount {
		public String accId;
		public String accBrand;
		public Account Acc;
		public Exception Except;
		private Map<String, String> Lbl_Msg;

		//Constructor
		public Handler_CleanAccount(Myr_CleanAccount_WS_Question q) {
			System.debug(Prefix+'Handler_CleanAccount:BEGIN');
			Lbl_Msg = new Map<String, String>();
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS000, Label.Myr_CleanAccount_WS10MS000_Msg);
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS501, Label.Myr_CleanAccount_WS10MS501_Msg);
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS502, Label.Myr_CleanAccount_WS10MS502_Msg);
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS503, Label.Myr_CleanAccount_WS10MS503_Msg);
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS504, Label.Myr_CleanAccount_WS10MS504_Msg);
			Lbl_Msg.put(Label.Myr_CleanAccount_WS10MS999, Label.Myr_CleanAccount_WS10MS999_Msg);
		
			accId=removeSpace(q.accountId);
			accBrand=removeSpace(q.accountBrand);
		}

		//Checks
		public Myr_CleanAccount_WS_Response Check_Inputs() {
			String Empty_Param;
			String Too_Long_Param;

			System.debug(Prefix+'Check_Inputs:BEGIN');

			Empty_Param=Missing_Mandatory_Parameter();
			if (Empty_Param != null){
				return Build_Resp(Label.Myr_CleanAccount_WS10MS501, Empty_Param);
			}
        
			Too_Long_Param=Too_Long_Param_Value();
			if (Too_Long_Param != null){
				return Build_Resp(Label.Myr_CleanAccount_WS10MS502, Too_Long_Param);
			}
        
			if(!String.isBlank(accBrand) && 
			   !accBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name()) && 
			   !accBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name())
			){
				return Build_Resp(Label.Myr_CleanAccount_WS10MS503, accBrand);
			}
			return null;
		}

		//Missing mandatory parameter
		public String Missing_Mandatory_Parameter() {
			String str=null;
			if(String.isBlank(accId)){
				str=' accountId';
			}
			if(String.isBlank(accBrand)){
				if(str!=null){
					str+=', accountBrand';
				}else{
					str=' accountBrand';
				}
			}
			return str;
		}

		//Too long value parameters
		public String Too_Long_Param_Value() {
			String str;
			if(accId!=null && accId.length()>18){
				str=' accountId';
			}
			if(accBrand!=null && accBrand.length()>50){
				if(str!=null){
					str+=', accountBrand';
				}else{
					str=' accountBrand';
				}
			}
			return str;
		}

		//Database query
		public Myr_CleanAccount_WS_Response Database_Query() {
			List<Account> Acc_Lst;
			
			System.debug(Prefix+'Database_Query:BEGIN');
			//Inputs are fine, so search of an account into the database
			Acc_Lst=[SELECT MYR_Status__c
						  , MYD_Status__c
						  , MyRenaultID__c
						  , MyDaciaID__c
						  , FirstName
						  , LastName
					 FROM Account 
					 WHERE Id = :accId];

			if(Acc_Lst.size()==0){//No account found
				return Build_Resp(Label.Myr_CleanAccount_WS10MS504, '<' + accId + '>');
			}else{
				Acc=Acc_Lst[0];
				return null;
			}
		}

		//Account cleaning
		public Myr_CleanAccount_WS_Response Clean_Account() {
			if(accBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name())){
				Acc.MyRenaultID__c='';
				Acc.Myr_Status__c='';
			}else if(accBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name())){
				Acc.MyDaciaID__c='';
				Acc.Myd_Status__c='';
			}
			try{
				if( Test.isRunningTest() && Acc!=null && Acc.FirstName.equalsIgnoreCase('Test_Exception') ) {
					throw new Myr_CleanAccount_WS_Exception('Exception during update');
				}
				update Acc; 
				return Build_Resp(Label.Myr_CleanAccount_WS10MS000, null); 
			} catch (Exception e) {
				Except=e;
				return null;
			}
		}

		//Account delete
		public Myr_CleanAccount_WS_Response Delete_Account() {
			System.debug(Prefix+'Delete_Account:BEGIN');
			try {
				if( Test.isRunningTest() && Acc!=null && Acc.LastName.equalsIgnoreCase('Test_Exception') ) {
					throw new Myr_CleanAccount_WS_Exception('Exception during delete');
				}
				delete Acc;
				return Build_Resp(Label.Myr_CleanAccount_WS10MS000, null);
			} catch (Exception e) {
				return Build_Resp(Label.Myr_CleanAccount_WS10MS999, Except.getMessage() + e.getMessage());
			}
		}

		//Build a response and log
		public Myr_CleanAccount_WS_Response Build_Resp(String Code, String Msg) {
			Myr_CleanAccount_WS_Response resp;
			String Err_Lvl;
			String Msg_Completed;

			if(Code.equalsIgnoreCase(Label.Myr_CleanAccount_WS10MS000)){
				Err_Lvl=INDUS_Logger_CLS.ErrorLevel.Info.name();
			}else{
				Err_Lvl=INDUS_Logger_CLS.ErrorLevel.Critical.name();
			}	
			Msg_Completed=Lbl_Msg.get(Code) + ' ' + Msg;
			
			Send_Log(logRunId, Code, Msg_Completed + ', Inputs=accountId=<' + accId + '>, accountBrand=<' + accBrand + '>', Err_Lvl, accId, Begin_Time, DateTime.now().getTime());

			resp = new Myr_CleanAccount_WS_Response();
			resp.info = new Myr_CleanAccount_WS_Response_Msg();
			resp.info.code = Code;
			resp.info.label = Msg_Completed;
			return resp;
		}
   	}

	@future
	public static void Send_Log(String Log_Run_Id, String Code_Err, String Msg_Err, String Lvl_Err, String Account_Id, Long BeginTime, Long EndTime) {
		Id My_Id;
		List<account> L_A;
		String AccId;

		L_A=[select Id from Account where Id=:Account_Id];
		if(L_A.size()==1){
			AccId=Account_Id;
		}
		
		//log
		My_Id=INDUS_Logger_CLS.addGroupedLogFull(
				  Log_Run_Id
				, null
				, INDUS_Logger_CLS.ProjectName.MYR
				, Myr_CleanAccount_WS.class.getName()
				, 'CleanAccount'
				, Code_Err
				, Msg_Err
				, Lvl_Err
				, AccId
				, null
				, null
				, BeginTime
				, EndTime
				, null);
	}

	// All the fields in entry of createAccount should be trimmed 
	private static String removeSpace(String str) {
		if (str != null && !String.isBlank(str)){ 
			str = str.trim();
		}
		return str; 
	}
	
	//Inner classes
    global class Myr_CleanAccount_WS_Response {
        WebService Myr_CleanAccount_WS_Response_Msg info;
    }

    global class Myr_CleanAccount_WS_Response_Msg {
        WebService String code;
        WebService String label; 
    }

	global class Myr_CleanAccount_WS_Question {
		WebService String accountBrand;
		WebService String accountId;
   	}

	public class Myr_CleanAccount_WS_Exception extends Exception {
	}
}