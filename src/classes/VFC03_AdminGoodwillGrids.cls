public class VFC03_AdminGoodwillGrids 
{
    public Goodwill_Grid_Line__c gwParams {get;set;}
    public list<GoodwillGridLineWrapper> gwGridLinesWrapper {get;set;}
    public boolean createMode {get;set;}
    private list<Goodwill_Grid_Line__c> gwGridLines;
    private static final Integer maxColumns = 61;
    private static final Integer maxLines = 27;
    
    //GGA
    public Goodwill_Grid_Line__c tmp {get;set;}    

    public VFC03_AdminGoodwillGrids()
    {
        gwParams = new Goodwill_Grid_Line__c();
        gwGridLinesWrapper = new list<GoodwillGridLineWrapper>();
        gwGridLines = new list<Goodwill_Grid_Line__c>();
        createMode = false;
        tmp = new Goodwill_Grid_Line__c ();
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillParamSelection);
        ApexPages.addMessage(myMsg);
    } 
    
    public void paramSelected()
    {
        createMode = false;
        
        if (gwParams.Country__c == null
            || gwParams.VehicleBrand__c == null
            || gwParams.Organ__c == null)
        {
            gwGridLinesWrapper.clear();
            gwGridLines.clear();
             
            gwParams.Age_interval__c = null;
            gwParams.Max_Age__c = null;
            gwParams.Mileage_Interval__c = null;
            gwParams.Max_Mileage__c = null;
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillParamSelection);
            ApexPages.addMessage(myMsg);
        }
        else
        {
            String organ = gwParams.Organ__c.split(';',-1)[0];
            gwGridLinesWrapper.clear();
            gwGridLines.clear();
            
            gwGridLines = [Select Valid_from__c, Valid_to__c, Additional_Information__c, Age_interval__c, Max_Age__c, Mileage_Interval__c, Max_Mileage__c, Row_Number__c, f1__c, f2__c, f3__c, f4__c, f5__c, f6__c, f7__c, f8__c, f9__c, f10__c, f11__c, f12__c, f13__c, f14__c, f15__c, f16__c, f17__c, f18__c, f19__c, f20__c, f21__c, f22__c, f23__c, f24__c, f25__c, f26__c, f27__c, f28__c, f29__c, f30__c, f31__c, f32__c, f33__c, f34__c, f35__c, f36__c, f37__c, f38__c, f39__c, f40__c, f41__c, f42__c, f43__c, f44__c, f45__c, f46__c, f47__c, f48__c, f49__c, f50__c, f51__c, f52__c, f53__c, f54__c, f55__c, f56__c, f57__c, f58__c, f59__c, f60__c, f61__c from Goodwill_Grid_Line__c where Country__c = :gwParams.Country__c AND VehicleBrand__c = :gwParams.VehicleBrand__c AND Organ__c = :organ order by Row_Number__c];
            
            if (gwGridLines.size() > 0)
            {
                gwParams.Age_interval__c = gwGridLines.get(0).Age_interval__c;
                gwParams.Max_Age__c = gwGridLines.get(0).Max_Age__c;
                gwParams.Mileage_Interval__c = gwGridLines.get(0).Mileage_Interval__c;
                gwParams.Max_Mileage__c = gwGridLines.get(0).Max_Mileage__c;
                currentNotesID = gwGridLines[0].ID;
            }
            else
            {
                gwParams.Age_interval__c = null;
                gwParams.Max_Age__c = null;
                gwParams.Mileage_Interval__c = null;
                gwParams.Max_Mileage__c = null;
            }
            
            Integer rowNum = 1;
            for (Goodwill_Grid_Line__c g : gwGridLines)
            {
                gwGridLinesWrapper.add(new GoodwillGridLineWrapper(rowNum, g));
                rowNum++;
            }
        }
    }
    
    public void createGrid()
    {
        if (!(parametersAreFilledIn()))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.GoodwillFillParameters);
            ApexPages.addMessage(myMsg);
        }
        else if (!(parametersIntLessMax())){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.GoodwillIntervalValid);
            ApexPages.addMessage(myMsg);
        }
        else if (gwParams.Max_Age__c/gwParams.Age_interval__c > maxLines || gwParams.Max_Mileage__c/gwParams.Mileage_Interval__c > maxColumns)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.GoodwillGridTooLarge);
            ApexPages.addMessage(myMsg);
        }
        else
        {
            createMode = true;
            
            Integer numberOfRows = Integer.valueOf(gwParams.Max_Mileage__c/gwParams.Mileage_Interval__c);
            
            for (Integer i = 0; i < numberOfRows; i++)
                gwGridLinesWrapper.add(new GoodwillGridLineWrapper(i+1, new Goodwill_Grid_Line__c()));
        }
    //  Save(); // Make sure we have a grid to act on when we recreate.
    }
    
    public void save()
    {
        // To fix a save problem when someone changes the interval or max parameters we must delete the grid and recreate it
        // This change means that the update code never runs, performance should also be monitored as it is typically worse in
        // sandboxes than in live.
        try{
            deleteGrid();
        }
        catch(ListException ex){
            // do nothing, there isn't anything to delete
        }
        createMode = true;
        
        list<Goodwill_Grid_Line__c> gwToInsertUpdate = new list<Goodwill_Grid_Line__c>();
        Goodwill_Grid_Line__c ggl = new Goodwill_Grid_Line__c();
        
        String[] organs = gwParams.Organ__c.split(';',-1);
        if(organs.size() > 1){
            // Delete any existing entries before we overwrite them
            Goodwill_Grid_Line__c[] linesToDelete = [SELECT ID FROM Goodwill_Grid_Line__c WHERE Country__c = :gwParams.Country__c AND VehicleBrand__c = :gwParams.VehicleBrand__c AND Organ__c IN :organs];
            delete linesToDelete;
            createMode = true;
        }
        for (String organitem : gwParams.Organ__c.split(';',-1)){
            Integer rowNum = 1;
            for (GoodwillGridLineWrapper g : gwGridLinesWrapper)
            {   
                
                if (createMode)
                {
                    if (!(parametersAreFilledIn()))
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.GoodwillFillParameters);
                        ApexPages.addMessage(myMsg);
                    }
                    else
                    {
                        ggl = g.gwGrid.clone(false,true,false,false);
                        ggl.Row_Number__c = rowNum;
                        rowNum++;
                        ggl.Country__c = gwParams.Country__c;
                        ggl.VehicleBrand__c = gwParams.VehicleBrand__c;
                        ggl.Organ__c = organitem;
                        ggl.Age_interval__c = gwParams.Age_interval__c;
                        ggl.Max_Age__c = gwParams.Max_Age__c;
                        ggl.Mileage_Interval__c = gwParams.Mileage_Interval__c;
                        ggl.Max_Mileage__c = gwParams.Max_Mileage__c;
                        //system.debug('RV:' + g);
                        g.gwGrid = ggl;
                        system.debug('RVg.gwGrid:-----' + g.gwGrid + '------');
                        system.debug('RVggl:-----' + g.gwGrid + '------');
                        system.debug('id:' + ggl.id);
                        
                    }
                }
                
                fillEmptyFields(g.gwGrid);
                
                gwToInsertUpdate.add(g.gwGrid);
                system.debug('------------------' + gwToInsertUpdate);
                system.debug('------------------');
                
            }
        }
            
        if (gwToInsertUpdate.size() > 0)
        {
            try
            {
                if (createMode)
                {
                    insert gwToInsertUpdate;
                    createMode = false;
                    currentNotesID = gwToInsertUpdate[0].ID;
                }
                else
                    update gwToInsertUpdate;
                    currentNotesID = gwToInsertUpdate[0].ID;
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, System.label.GoodwillUpdateSucceed);
                ApexPages.addMessage(myMsg);
            }
            catch(DMLException e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillUpdateFailed);
                ApexPages.addMessage(myMsg);
            }
        }
        
    }
    
    public void deleteGridAndClearParam()
    {
        deleteGrid();
        clearParameters();
        
    }
    
    public void deleteGrid()
    {
        list<Goodwill_Grid_Line__c> gwTodelete = new list<Goodwill_Grid_Line__c>();
        String[] organs;
        
        if (gwParams.Organ__c <> null && gwParams.VehicleBrand__c <> null && gwParams.Country__c <> null){
            organs = gwParams.Organ__c.split(';',-1);
      
            
            if(organs.size() > 1){
                // Delete any existing entries before we overwrite them
                gwTodelete = [SELECT ID FROM Goodwill_Grid_Line__c WHERE Country__c = :gwParams.Country__c AND VehicleBrand__c = :gwParams.VehicleBrand__c AND Organ__c IN :organs];
            } else {
                for (GoodwillGridLineWrapper g : gwGridLinesWrapper)
                    gwTodelete.add(g.gwGrid);
            }
            
            try
            {
                delete gwTodelete;
            }
            catch(DMLException e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwilldeleteFailed);
                ApexPages.addMessage(myMsg);
            }
        }
    }
    
    public void recreate()
    {
        if (!createMode)
            deleteGrid();
        
        gwGridLinesWrapper.clear();
        gwGridLines.clear();
        createMode = true;
        
        createGrid();
    }
    
    public void clearParameters()
    {
        gwGridLinesWrapper.clear();
        gwGridLines.clear();
        gwParams = new Goodwill_Grid_Line__c();
        createMode = false;
    }
    
    public boolean parametersAreFilledIn()
    {
        return !(gwParams.Age_interval__c == null 
            || gwParams.Max_Age__c == null 
            || gwParams.Mileage_Interval__c == null 
            || gwParams.Max_Mileage__c == null);
            
    }
    
    public boolean parametersIntLessMax(){
        return !(gwParams.Age_interval__c > gwParams.Max_Age__c
            || gwParams.Mileage_Interval__c > gwParams.Max_Mileage__c);
    }
    
    public void fillEmptyFields(Goodwill_Grid_Line__c g)
    {
        if (g.f1__c == null)
            g.f1__c = 0;
        if (g.f2__c == null)
            g.f2__c = 0;
        if (g.f3__c == null)
            g.f3__c = 0;
        if (g.f4__c == null)
            g.f4__c = 0;
        if (g.f5__c == null)
            g.f5__c = 0;
        if (g.f6__c == null)
            g.f6__c = 0;
        if (g.f7__c == null)
            g.f7__c = 0;
        if (g.f8__c == null)
            g.f8__c = 0;
        if (g.f9__c == null)
            g.f9__c = 0;
        if (g.f10__c == null)
            g.f10__c = 0;
        if (g.f11__c == null)
            g.f11__c = 0;
        if (g.f12__c == null)
            g.f12__c = 0;
        if (g.f13__c == null)
            g.f13__c = 0;
        if (g.f14__c == null)
            g.f14__c = 0;
        if (g.f15__c == null)
            g.f15__c = 0;
        if (g.f16__c == null)
            g.f16__c = 0;
        if (g.f17__c == null)
            g.f17__c = 0;
        if (g.f18__c == null)
            g.f18__c = 0;
        if (g.f19__c == null)
            g.f19__c = 0;
        if (g.f20__c == null)
            g.f20__c = 0;
        if (g.f21__c == null)
            g.f21__c = 0;
        if (g.f22__c == null)
            g.f22__c = 0;
        if (g.f23__c == null)
            g.f23__c = 0;
        if (g.f24__c == null)
            g.f24__c = 0;
        if (g.f25__c == null)
            g.f25__c = 0;
        if (g.f26__c == null)
            g.f26__c = 0;
        if (g.f27__c == null)
            g.f27__c = 0;
        if (g.f28__c == null)
            g.f28__c = 0;
        if (g.f29__c == null)
            g.f29__c = 0;
        if (g.f30__c == null)
            g.f30__c = 0;
        if (g.f31__c == null)
            g.f31__c = 0;
        if (g.f32__c == null)
            g.f32__c = 0;
        if (g.f33__c == null)
            g.f33__c = 0;
        if (g.f34__c == null)
            g.f34__c = 0;
        if (g.f35__c == null)
            g.f35__c = 0;
        if (g.f36__c == null)
            g.f36__c = 0;
        if (g.f37__c == null)
            g.f37__c = 0;
        if (g.f38__c == null)
            g.f38__c = 0;
        if (g.f39__c == null)
            g.f39__c = 0;
        if (g.f40__c == null)
            g.f40__c = 0;
        if (g.f41__c == null)
            g.f41__c = 0;
        if (g.f42__c == null)
            g.f42__c = 0;
        if (g.f43__c == null)
            g.f43__c = 0;
        if (g.f44__c == null)
            g.f44__c = 0;
        if (g.f45__c == null)
            g.f45__c = 0;
        if (g.f46__c == null)
            g.f46__c = 0;
        if (g.f47__c == null)
            g.f47__c = 0;
        if (g.f48__c == null)
            g.f48__c = 0;
        if (g.f49__c == null)
            g.f49__c = 0;
        if (g.f50__c == null)
            g.f50__c = 0;
        if (g.f51__c == null)
            g.f51__c = 0;
        if (g.f52__c == null)
            g.f52__c = 0;
        if (g.f53__c == null)
            g.f53__c = 0;
        if (g.f54__c == null)
            g.f54__c = 0;
        if (g.f55__c == null)
            g.f55__c = 0;
        if (g.f56__c == null)
            g.f56__c = 0;
        if (g.f57__c == null)
            g.f57__c = 0;
        if (g.f58__c == null)
            g.f58__c = 0;
        if (g.f59__c == null)
            g.f59__c = 0;
        if (g.f60__c == null)
            g.f60__c = 0;
        if (g.f61__c == null)
            g.f61__c = 0;
    }
    
    public class GoodwillGridLineWrapper
    {
        public Goodwill_Grid_Line__c gwGrid {get;set;}
        public Integer rowNumber {get; set;}

        public GoodwillGridLineWrapper(Integer rn, Goodwill_Grid_Line__c gw)
        {
            rowNumber = rn;
            gwGrid = gw;
        }      
    }
    
    public ID currentNotesID {get;set;}
    
    /*
    public SelectOption[] getselectListCountries(){
        SelectOption[] s = new SelectOption[]{};
        ID userid = UserInfo.getUserId();
        User u = [SELECT id, country, FROM User WHERE ID = :userid];

        // Administrative option so someone can see all the entries.
        if(u.Country == 'GRIDADMINISTRATOR'){
            s.add(new SelectOption('--None--', '--None--'));
            for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
                s.add(new SelectOption(p.getValue(), p.getValue()));
            }
        } else {
            for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
                // This just makes sure that the country entered in the user is actually one listed in the picklist
                if(u.Country == p.getValue()){
                    s.add(new SelectOption(u.Country, u.Country));
                }
            }
        }
        return s;
    }*/
    
   
    public SelectOption[] getselectListCountries(){
        SelectOption[] s = new SelectOption[]{};
        ID userid = UserInfo.getUserId();
        User u = [SELECT id, GridCountry__c FROM User WHERE ID = :userid];

        // Administrative option so someone can see all the entries.
        if(u.GridCountry__c == 'GRIDADMINISTRATOR'){
            s.add(new SelectOption('--None--', '--None--'));
            for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
                s.add(new SelectOption(p.getValue(), p.getValue()));
            }
        } else {
        
           // Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
           // List<Schema.PicklistEntry>  Goodwill_Grid_Line__c.Country__c = field_map.get(field_name).getDescribe().getPickLis​tValues();
            for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
                // This just makes sure that the country entered in the user is actually one listed in the picklist
               // if(u.DefaultCountry__c == p.getValue()){
                if (u.GridCountry__c != null && u.GridCountry__c.contains(p.getValue())){

                    s.add(new SelectOption(p.getValue(), p.getValue()));
                      
            }
          }    
         }         
        return s;
    }
    
}