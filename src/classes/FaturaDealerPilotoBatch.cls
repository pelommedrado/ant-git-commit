global class FaturaDealerPilotoBatch implements Database.Batchable<sObject>, Schedulable {

    private List<String> birList = new List<String> { 
        '07600100', '07600331', '07600358', '07600360', '07600361', '07600595', '07600672', '07600728',
            '07600013', '07600213', '07600312', '07600526', '07600686', '07600681', 
            '07600017', '07600304', '07600495', '07600570', '07600790', '07600835', 
            '07600108', '07600604', '07600715', '07600802'
            };
    
    private String queryDefault = 
                'SELECT Id, Error_Messages__c, RecordTypeId, ' 		+ 
                ' NFBANDEI__c, NFCODOPR__c, NFTIPONF__c, NFTIPREG__c, NFBIRENV__c, NFBIREMI__c,' 	+ 
                ' NFDTANFI__c, NFDTANAS__c, NFTIPCLI__c, NFCPFCGC__c, NFNRONFI__c, '				+
                ' NFEMAILS__c, NFDDDRES__c, NFTELRES__c, NFDDDCEL__c, NFTELCEL__c, '				+
                ' NFCHASSI__c, NFANOFAB__c, NFANOMOD__c, NFDTASIS__c, Count__c,'					+
                
                ' NFBIRENV_Mirror__c, NFBIREMI_Mirror__c, NFCODOPR_Mirror__c, NFTIPONF_Mirror__c,'  +
                ' NFCPFCGC_Mirror__c, NFEMAILS_Mirror__c, NFDDDRES_Mirror__c, NFDDDCEL_Mirror__c,'	+
                ' NFTELCEL_Mirror__c, NFTELRES_Mirror__c, NFANOFAB_Mirror__c, NFANOMOD_Mirror__c,'  +
                ' NFCHASSI_Mirror__c, NFDTANFI_Mirror__c, NFDTASIS_Mirror__c '						+
                ' FROM FaturaDealer__c'																+
                ' WHERE RecordTypeId = \'' + FaturaDealerServico.UNPROCESSED + '\' ' + montarQueryIn();
    
    private String montarQueryIn() {
        String inStg = ' AND NFBIREMI__c in(';
        
        for(String stg : birList) {
            if(birList.get(birList.size() - 1).equals(stg)) {
                inStg += '\'' + stg + '\')';
                
            } else {
                inStg += '\'' + stg + '\',';
                
            }
        }
        
        return inStg;
    }
    
    global void execute(SchedulableContext sc) {
        System.debug('Iniciando a execucao do batch');
        System.debug('Query: ' + queryDefault);
        
        FaturaDealerPilotoBatch fatBatch = new FaturaDealerPilotoBatch();
        Database.executeBatch(fatBatch, 5);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryDefault);
    }
    
    global void execute(Database.BatchableContext BC, List<FaturaDealer__c> scope) {
        FaturaDealerServico servico = new FaturaDealerServico();
        servico.iniciarValidacao(scope);
    }
    
    global void finish(Database.BatchableContext BC) {
    }
}