@isTest
public with sharing class TST_TransactionBuild {

	public static TST_Transaction__c createTransaction(Opportunity opp) {
      final TST_Transaction__c tst = new TST_Transaction__c();
      tst.Opportunity_Last_Status__c = opp.StageName;
      tst.Opportunity__c = opp.Id;
      tst.Opportunity_Status__c = opp.StageName;
      tst.VehicleOfInterest__c = opp.VehicleInterest__c;
      return tst;
    }
}