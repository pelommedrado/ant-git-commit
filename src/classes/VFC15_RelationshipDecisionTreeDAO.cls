/**
*	Class   -   VFC15_RelationshipDecisionTreeDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*    
*   #01 <RameshPrabu> <31/08/2012>
*        Created this Class to handle record from Relationship Decision Tree related Queries.
*/
public with sharing class VFC15_RelationshipDecisionTreeDAO {
	
	private static final VFC15_RelationshipDecisionTreeDAO instance = new VFC15_RelationshipDecisionTreeDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC15_RelationshipDecisionTreeDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC15_RelationshipDecisionTreeDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get Relationship Decision Tree Record based on some lead fields
    * @param addressOK - This field using from addressOK of lead object             
    * @param birthdate - This field using from birthdate  of lead object
    * @param emailOK - This field using from emailOK of lead object
    * @param informationUpdatedInTheLast5Years - This field using from informationUpdatedInTheLast5Years of lead object
    * @param participateOnOPA - This field using from participateOnOPA of lead object
    * @param phoneOK - This field using from phoneOK of lead object
    * @return lstRelationship - fetch and return the result in lstCustomerTree list
    */
    public List<RDT_RelationshipDecisionTree__c> findRelationshipDecisionTree(String addressOK, String birthdate, String emailOK, String informationUpdatedInTheLast5Years, String participateOnOPA, String phoneOK){
        List<RDT_RelationshipDecisionTree__c> lstRelationship = null;
        system.debug('addressOK....' +addressOK);
        system.debug('birthdate....' +birthdate);
        system.debug('emailOK....' +emailOK);
        system.debug('informationUpdatedInTheLast5Years....' +informationUpdatedInTheLast5Years);
        system.debug('participateOnOPA....' +participateOnOPA);
        system.debug('phoneOK....' +phoneOK);    
        lstRelationship = [select
	        					Id, 
	        					Name, 
	        					Recommendation1__c, 
	        					Recommendation2__c, 
	        					Recommendation3__c, 
	        					Recommendation4__c
	        				from
	        					RDT_RelationshipDecisionTree__c
	                		where
	                			AddressOK__c =: addressOK 
	                			and Birthdate__c =: birthdate 
	                			and EmailOK__c =: emailOK
	                			and InformationUpdatedInTheLast5Years__c =: informationUpdatedInTheLast5Years
	                			and ParticipateOnOPA__c =: participateOnOPA 
	                			and PhoneOK__c =:phoneOK];
		system.debug('Relationship in DAO....' +lstRelationship);
		return lstRelationship;
    }
}