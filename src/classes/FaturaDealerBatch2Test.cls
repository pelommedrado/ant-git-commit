@isTest
private class FaturaDealerBatch2Test {
	@isTest
	static void itShould() {
		final FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
    fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.Delivery_Date__c = '2017-05-15T10:01:24';
		fatura.RecordTypeId = FaturaDealer2Utils.RECORDTYPEID_INVOICE;
		Database.insert(fatura);

		final FaturaDealer2__c fatura2 = new FaturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '44542350746';
		fatura2.Customer_Type__c = 'F';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '1127381122';
		fatura2.Phone_2__c = '1299098876';
		fatura2.External_Key__c = '1004';
		fatura2.RecordTypeId = FaturaDealer2Utils.RECORDTYPEID_REGISTER;
		fatura2.Status__c = 'Unprocessed';
		fatura2.Delivery_Date__c = '2017-05-15T10:01:24';
		fatura2.Invoice_Data__c = '2017-05-15T09:57:52';
		Database.insert(fatura2);

		Test.startTest();
		Database.executeBatch(new FaturaDealerBatch2());
		Test.stopTest();

		final List<FaturaDealer2__c> faturaList = [
			SELECT Id, Status__c FROM FaturaDealer2__c WHERE Status__c != 'Unprocessed'
		];

		System.assertEquals(faturaList.isEmpty(), false);
		System.assertEquals(faturaList.get(0).Status__c, 'Valid');

		final List<Account> accList = [
			SELECT Id, RecordTypeId FROM Account WHERE CustomerIdentificationNbr__c = '44542350746'
		];
		System.assertEquals(accList.isEmpty(), false);
		System.assert(accList.get(0).RecordTypeId == Utils.getRecordTypeId('Account', 'Personal_Acc'));
	}

    @isTest
    public static void itShouldSchedule() {
    	Test.StartTest();
        FaturaDealerBatch2 sh1 = new FaturaDealerBatch2();
        String sch = '0 0 23 * * ?';
        System.schedule('Test check', sch, sh1);
    	Test.stopTest();
    }
}