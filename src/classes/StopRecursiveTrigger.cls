/*****************************************************************************************
    Name    : StopRecursiveTrigger
    Desc    : This is to stop recursive behaviour of triggers
    Approach: Static Boolean Variable
                
                                                
 Modification Log : 
---------------------------------------------------------------------------------------
Developer                       Date                Description
---------------------------------------------------------------------------------------
Praveen Ravula                  10/21/2013           Created 

******************************************************************************************/


public class StopRecursiveTrigger { 
   public static boolean firstRun = false; 

   public static boolean hasAlreadyDone(){ 
    return firstRun;
   }

   public static void setAlreadyDone() {
    firstRun = true;  
   }

   public static void forceResetAlreadyDone() {
    firstRun = false; 
   }
}