@isTest
private class FaturaDealer2ControllerTest {

	static User userRun;

	static {
		userRun = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
	}

	@TestSetup static void setup() {
		FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c 	= '51820533000150';
		fatura.VIN__c 								= '3425GSVFAGS234637';
		fatura.Invoice_Type__c 						= 'New Vehicle';
		fatura.Manufacturing_Year__c 				= '1994';
		fatura.Customer_Type__c 					= 'J';
		fatura.Model_Year__c 						= '1994';
		fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
		fatura.DDD1__c 								= '11';
		fatura.Phone_1__c 							= '27381122';
		fatura.DDD2__c 								= '12';
		fatura.Phone_2__c 							= '99098876';
		fatura.DDD3__c 								= '13';
		fatura.Phone_3__c 							= '20987761';
		fatura.External_Key__c 						= '123';
		fatura.Delivery_Date_Mirror__c 				=  System.today();
		fatura.Integration_Date_Mirror__c 			= Date.ValueOf('2017-01-01');
		fatura.Invoice_Date_Mirror__c 				= Date.ValueOf('2017-01-01');
		fatura.RecordTypeId 						= FaturaDealer2Utils.RECORDTYPEID_INVOICE;
		fatura.Status__c 							= 'Invalid';

		Database.insert(fatura);
		

		FaturaDealer2__c fatura2 = new FaturaDealer2__c();
		fatura2.Customer_Identification_Number__c 	= '46843566634';
		fatura2.VIN__c 								= '1234ABCDEFG123456';
		fatura2.Invoice_Type__c 					= 'New Vehicle';
		fatura2.Manufacturing_Year__c 				= '1994';
        fatura2.Customer_Type__c 					= 'F';
		fatura2.Model_Year__c 						= '1994';
		fatura2.Customer_Email__c 					= 'dadf@sdfdas.com';
		fatura2.DDD1__c 							= '11';
		fatura2.Phone_1__c 							= '27381122';
		fatura2.DDD2__c 							= '12';
		fatura2.Phone_2__c 							= '99098876';
		fatura2.DDD3__c 							= '13';
		fatura2.Phone_3__c 							= '20987761';
		fatura2.External_Key__c 					= '456';
		fatura2.Delivery_Date_Mirror__c 			=  Date.ValueOf('1900-01-01');
		fatura2.Status__c 							= 'Invalid';
		fatura2.RecordTypeId 						= FaturaDealer2Utils.RECORDTYPEID_REGISTER;

		Database.insert(fatura2);
	}

	@isTest static void shouldTestTypeChange() {
		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			System.assertEquals(true, controller.typeSwitch);
			controller.typeChange();
			System.assertEquals(false, controller.typeSwitch);
		}
		Test.stopTest();
	}

	@isTest static void shouldGetCsvInvoiceContent() {
		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			controller.typeSwitch = true;
			PageReference check = controller.searchAllInvoice();
			System.assertEquals('System.PageReference[/servlet/servlet.FileDownload?file', String.valueOf(check).substringBefore('='));
		}
		Test.stopTest();
	}

	@isTest static void shouldGetCsvRegisterContent() {
		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			controller.typeSwitch = false;
			PageReference check = controller.searchAllInvoice();
			System.assertEquals('System.PageReference[/servlet/servlet.FileDownload?file', String.valueOf(check).substringBefore('='));
		}
		Test.stopTest();
	}

	@isTest static void shouldGetCsvContentWithBothDates() {
		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			controller.typeSwitch 	= true;
			List<String> ontem 		= String.valueOf(System.today().addDays(-1)).substringBefore(' ').split('-');
			List<String> hoje 		= String.valueOf(System.today()).substringBefore(' ').split('-');
			controller.sendFrom 	= ontem[2]+'/'+ontem[1]+'/'+ontem[0];
			controller.sendTo 		= hoje[2]+'/'+hoje[1]+'/'+hoje[0];
			controller.invoiceFrom 	= ontem[2]+'/'+ontem[1]+'/'+ontem[0];
			controller.invoiceTo 	= hoje[2]+'/'+hoje[1]+'/'+hoje[0];
			PageReference check 	= controller.searchAllInvoice();
			System.assertEquals('System.PageReference[/servlet/servlet.FileDownload?file', String.valueOf(check).substringBefore('='));
		}
		Test.stopTest();
	}

	@isTest static void shouldFillInvoiceList() {
		Test.startTest();
		System.RunAs(userRun) {
			List<String> today = String.valueOf(System.today()).split('-');
			List<String> tomorrow = String.valueOf(System.today().addDays(1)).split('-');

			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			controller.typeSwitch 	= true;
			controller.invoiceFrom 	= '01/12/2016';
			controller.invoiceTo 	= '02/01/2017';
			controller.sendFrom 	= today[2]+'/'+today[1]+'/'+today[0];
			controller.sendTo 		= tomorrow[2]+'/'+tomorrow[1]+'/'+tomorrow[0];	
			System.debug('!!!'+controller.sendFrom);
			controller.searchInvoice();
			System.assertEquals(false, controller.invoiceList.isEmpty());
			System.assertEquals(1, controller.invoiceList.size());
			System.assertEquals('3425GSVFAGS234637', controller.invoiceList[0].VIN__c);
		}
		Test.stopTest();
	}

	@isTest static void shouldFillInvoiceListWithOneInvoice() {

		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			controller.typeSwitch 	= false;
			controller.customerIdentification = '46843566634';
			controller.searchInvoice();

			System.assertEquals(false, controller.invoiceList.isEmpty());
			System.assertEquals(1, controller.invoiceList.size());
			System.assertEquals('1234ABCDEFG123456', controller.invoiceList[0].VIN__c);
		}
		Test.stopTest();
	}

	/*@isTest static void shouldNotFillInvoiceListBecauseOfInvoiceDateFrom() {
		FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c 	= '51820533000150';
		fatura.VIN__c 								= '3425GSVFAGS234637';
		fatura.Invoice_Type__c 						= 'New Vehicle';
		fatura.Manufacturing_Year__c 				= '1994';
        fatura.Customer_Type__c 					= 'J';
		fatura.Model_Year__c 						= '1994';
		fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
		fatura.DDD1__c 								= '11';
		fatura.Phone_1__c 							= '27381122';
		fatura.DDD2__c 								= '12';
		fatura.Phone_2__c 							= '99098876';
		fatura.DDD3__c 								= '13';
		fatura.Phone_3__c 							= '20987761';
		fatura.External_Key__c 						= '123';
		fatura.Delivery_Date_Mirror__c 				=  System.today();
		fatura.Status__c 							= 'Invalid';
		Database.insert(fatura);

		FaturaDealer2Controller controller = new FaturaDealer2Controller();
		controller.invoiceFrom = '01/01/1999';
		controller.searchInvoice();
		System.assertEquals('(ApexPages.Message["A data mínima para início da análise ', String.valueOf(ApexPages.getMessages()).substringBeforeLast('é'));
		System.assertEquals(true, controller.invoiceList.isEmpty());
		System.assertEquals(0, controller.invoiceList.size());
	}*/

	@isTest static void shouldGiveInvoiceDateToException() {
		Test.startTest();
		System.RunAs(userRun) {
			FaturaDealer2Controller controller = new FaturaDealer2Controller();
			List<String> amanha 	= String.valueOf(System.today().addDays(1)).substringBefore(' ').split('-');
			controller.invoiceTo 	= amanha[2]+'/'+amanha[1]+'/'+amanha[0];
			controller.typeSwitch 	= true;
			controller.searchInvoice();
			System.assertEquals('(ApexPages.Message["A data máxima para término da análise ', String.valueOf(ApexPages.getMessages()).substringBeforeLast('é'));
		}
		Test.stopTest();
	}

	@isTest static void shouldTestFormat(){
		FaturaDealer2Controller controller = new FaturaDealer2Controller();

		System.debug(controller.format(System.today()));
		System.debug(controller.format('Teste'));
		System.debug(controller.format(100.40));
		System.debug(controller.format(100.40, 3));
	}
}