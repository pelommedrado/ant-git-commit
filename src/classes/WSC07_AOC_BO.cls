public class WSC07_AOC_BO
{
	private static final WSC07_AOC_BO instance = new WSC07_AOC_BO();

    private WSC07_AOC_BO(){}

    public static WSC07_AOC_BO getInstance(){
        return instance;
    }

	
	private static final Decimal NO_PRICE = 888888888;


	// Create / update a PricebookEntry record that will determine the model's
	// price. The price will be the smallest version price available.
	public void updateModePricesWithTheSmallestVersionPrice()
	{
		Map<Id, Id> mapModelId = new Map<Id, Id>();
		Map<Id, Decimal> mapModelPrice = new Map<Id, Decimal>();
		for (Product2 version : [SELECT Id, Name, Version__c, Model__c
							  	   FROM Product2
								  WHERE RecordType.DeveloperName = 'PDT_ModelVersion'
								    AND IsActive = True
								    AND CurrencyIsoCode = 'BRL'])
		{
			mapModelId.put(version.Id, version.Model__c);
			
			if (!mapModelPrice.containsKey(version.Model__c)) {
				mapModelPrice.put(version.Model__c, NO_PRICE);
			}
		}


		Id priceBookId;
		
		// Get all version prices given their Ids and find the smaller
		for (PricebookEntry price : [SELECT Id, Pricebook2Id, Product2Id, UnitPrice
									   FROM PricebookEntry
									  WHERE IsActive = True
										AND CurrencyIsoCode = 'BRL'
										AND Product2Id in :mapModelId.keySet()])
		{
			Id versionId = price.Product2Id;
			Id modelId = mapModelId.get(versionId);

			if (mapModelPrice.containsKey(modelId))
			{
				Decimal modelPriceBRL = mapModelPrice.get(modelId);
				if (price.UnitPrice != null && price.UnitPrice <  modelPriceBRL)
				{
					// A new smaller price for a model's version was found.
					// Use it as the default price of model.
					mapModelPrice.put(modelId, price.UnitPrice);
				}
				
			}
		
			// I'm assuming that all prices will share the same default priceBook Id
			priceBookId = price.Pricebook2Id;
		}
	
	
		// Build a map with PriceBookEntry for model prices to be updated
		Map<Id, PricebookEntry> mapModelPriceBook = new Map<Id, PricebookEntry>();
		for (PricebookEntry pbe : [SELECT Id,Pricebook2Id, Product2Id, UnitPrice
								     FROM PricebookEntry
								    WHERE IsActive = True
								 	  AND CurrencyIsoCode = 'BRL'
								 	  AND Product2Id in :mapModelPrice.keySet()])
		{
			mapModelPriceBook.put(pbe.Product2Id, pbe);
		}
		
	
		// List of Price Book Entry to insert and update model prices
		List<PricebookEntry> lstToInsert = new List<PricebookEntry>();
		List<PricebookEntry> lstToUpdate = new List<PricebookEntry>();
	
		// Update the smallest price for each model
		for (Id modelId : mapModelPrice.keySet())
		{
			Decimal price = mapModelPrice.get(modelId);
			system.debug('*** modelId['+modelId+']='+price);
			
			// Check if we have a record for a priceBookEntry for that model
			if (mapModelPriceBook.containsKey(modelId))
			{
				PricebookEntry pbe = mapModelPriceBook.get(modelId);
				pbe.UnitPrice = price;
				lstToUpdate.add(pbe);	
			}
			else
			{
				// Create a new record to store the price for the model
				PricebookEntry pbe = new PricebookEntry(UnitPrice = price,
														CurrencyIsoCode = 'BRL',
														Pricebook2Id = priceBookId,
														Product2Id = modelId,
														IsActive = True);
				lstToInsert.add(pbe);
			}
		}

		// Save the prices
		insert lstToInsert;
		update lstToUpdate;
	}

}