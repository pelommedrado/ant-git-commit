/**
* @author sebastien.ducamp@atos.net.hqmyrdev
* @description  Apex class dedicated to update the MyRenault accounts with the personal data inputs
* 
*   2016 August, Author=S. Ducamp (AtoS) : R16.10 F1854-US3654 - Add the possibility 
*										 to use get vehicle from rubics in synchronous mode
* 
*/
public class Myr_ManageAccount_PersDataUpdate_Cls {  

	public static final String LOG_PERSDATAPREFIX = Myr_ManageAccount_PersDataUpdate_Cls.class.getName() + ': ';
	public static final String LOG_FUNCTION = LOG_PERSDATAPREFIX + 'Search';
	public static final String LOG_SEARCHTRY = LOG_PERSDATAPREFIX + 'Search Try';
	public static final String LOG_SEARCHOK = LOG_PERSDATAPREFIX + 'Search OK';
	public static final String LOG_SEARCHKO = LOG_PERSDATAPREFIX + 'Search KO';
	public static final String LOG_SEARCHEXCEPTION = LOG_PERSDATAPREFIX + 'Search Exception';
	public static final String LOG_ACC_UPDOK = 	LOG_PERSDATAPREFIX + 'Account Update OK';
	public static final String LOG_ACC_UPDEXCEPTION = LOG_PERSDATAPREFIX + 'Account Update Exception';
	public static final String LOG_VEHVRE_UPDOK = LOG_PERSDATAPREFIX + 'Vehicles and Relations Upsert OK';
	public static final String LOG_VEHVRE_UPDEXCEPTION = LOG_PERSDATAPREFIX + 'Vehicles and Relations Exception';

	//detyect login mode to apply specific business need on veh relation
	public Boolean LoginMode = false;

	private String mAccountId;
	@TestVisible private String mAccountBrand;
	private String mVin;
	private Myr_ManageAccount_PersDataCall_Cls mPersDataCaller;
	private TRVResponseWebservices mPDataResponse;
	private TRVResponseWebservices.InfoClient mClient;
	private Myr_PersonalData_WS.DataSourceType mDataSource;
	@TestVisible private Account mAccount;
	private Set<String> mAccountFields = Schema.SObjectType.Account.fields.getMap().keySet();
	@TestVisible private Country_Info__c mCountryInfo;
	private Id mParentLogId;
	private String mExecutionContext; //used to prefix the message in the logs to identify by which the process, this update has been required

	/* Inner exception class **/
	public class ManageAccountPersDataupdateException extends Exception {}

	/** @constructor **/
	public Myr_ManageAccount_PersDataUpdate_Cls(String accoundId, String accoundBrand, Id parentLogId, String context) {
		try { 
		//Account informations
		mAccountId = accoundId;
		mAccountBrand = accoundBrand;
		mParentLogId = parentLogId;
		mExecutionContext = (String.isBlank(context)?Myr_ManageAccount_PersDataUpdate_Cls.class.getName():context);
		mAccount = [SELECT 
            Firstname, lastName, Language__c, SecondSurname__c, Salutation, Salutation_Digital__c, Title__c, 
            ownerId, PersonBirthDate, Sex__c, MyTC_Acceptance__c, Country__c,
			MyRenaultID__c, MyDaciaID__c, CustomerIdentificationNbr__c, CustomerIdentificationNbr2__c
			, Bcs_Id__c, Bcs_Id_Dacia__c, Tech_ACCExternalID__c
			, My_Renault_ID_Account_number__c, PeopleId_CN__c,
            BillingStreet, BillingCity, BillingPostalCode, BillingCountry, BillingState,
            MYR_Dealer_1_BIR__c, MYR_Dealer_2_BIR__c, MYR_Dealer_3_BIR__c, MYR_Dealer_4_BIR__c, MYR_Dealer_5_BIR__c,   
            PersLandline__c, HomePhone__c, ProfLandLine__c, PersMobPhone__c, PersEmailAddress__c,
            MYR_Status__c, MYD_Status__c,
            Address__pc, AddressDate__pc, PersMobiPhone__pc, PersMobiPhoneDate__pc, SMS__pc, SMSDate__pc, PersEmail__pc, PersEmailDate__pc, ComAgreemt__c
			FROM Account WHERE Id=:accoundId];
		mCountryInfo = Country_Info__c.getInstance(mAccount.Country__c);
		}catch (Exception e) {
			throw new ManageAccountPersDataupdateException('Personal Data response not acceptable to upgrade MyRenault accounts: ' + e.getMEssage());
		}
	}

	/* Set the vin for the Personal Data callout **/
	public void setVin( String vin ) {
		mVin = vin;
	}

	/* Determines which id to use to search within Personal Data .
		Algorithm o determine the right id is the following one:
		1. Use BcsId__c if the brand is Renault, BcsIdDacia__c if brand is Dacia. If the corresponding field is empty, use the other one
		2. If the webservice to call is RBX/SIC, try to remove the prefix (country_code_2L), if this prefix does not match, use the full id
		3. If the webservice to call is BCS, use the Id as it is stored
		4. If the webservice to call is MDM, use the field TechACCExternalId__c.
	*/
	@TestVisible private String determinePersonalId() {
		system.debug('### Myr_ManageAccount_PersDataUpdate_Cls - <determinePersonalId> - BEGIN ');
		String persId = null;
		//Determine which webservice is called
		String bcsRbx = 'sic|bcs|rbx';
		String rbx = 'sic|rbx';
		String mdm = 'mdm';
		if( mCountryInfo != null && !String.isBlank(mCountryInfo.DataSource__c) ) {
			if( bcsRbx.containsIgnoreCase( mCountryInfo.DataSource__c ) ) {
				//determines the id
				if( Myr_MyRenaultTools.checkRenaultBrand(mAccountBrand) ) {
					persId = mAccount.Bcs_Id__c;
					if( String.isBlank( persId ) ) {
						persId = mAccount.Bcs_Id_Dacia__c;
					}
				} else if( Myr_MyRenaultTools.checkDaciaBrand(mAccountBrand) ) {
					persId = mAccount.Bcs_Id_Dacia__c;
					if( String.isBlank( persId ) ) {
						persId = mAccount.Bcs_Id__c;
					}
				}
				//if the webservice is rbx/sic, try to remove the prefix (country_code 2L)
				if( rbx.containsIgnoreCase(mCountryInfo.DataSource__c) && !String.isBlank(persId) ) {
					persId = persId.removeStartIgnoreCase( mCountryInfo.Country_Code_2L__c );
				}
			} else if ( mdm.equalsIgnoreCase(mCountryInfo.DataSource__c) ) {
				persId = mAccount.Tech_ACCExternalID__c;
			}
		}
		system.debug('### Myr_ManageAccount_PersDataUpdate_Cls - <determinePersonalId> - PersId='+persId);
		return persId;
	}

	/* Default public function to call personal data **/
	public TRVResponseWebservices callPersonalData() {
		return innerCallPersonalData( null );
	}

	/* Call Personal Data with a list of keys instead of using the country configuration for personal data matching keys **/
	public TRVResponseWebservices callPersonalData( List<String> listMatchingKeys ) {
		LoginMode = true; //this constructor is ued only by login handler
		return innerCallPersonalData( listMatchingKeys );
	} 

	/** Call Personal Data before trying to update informations **/
	private TRVResponseWebservices innerCallPersonalData( List<String> listMatchingKeys ) {
		Long beginT = system.now().getTime();
		system.debug('### Myr_ManageAccount_PersDataUpdate_Cls - <callPersData> - BEGIN');
		if( String.isBlank(mAccountBrand) || String.isBlank(mAccount.Country__c) ) {
			String msg = 'Parameters brand or country could ne be empty before performing personal data callout: brand='+mAccountBrand+', country='+mAccount.Country__c;
			log( '', 'Wrong Parameters', msg, beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Warn);
		}
		try { //Ludo 
			//Prepare the parameters
			Myr_ManageAccount_PersDataCall_Cls.PersDataParameters reqParams = new Myr_ManageAccount_PersDataCall_Cls.PersDataParameters();
			reqParams.lastName = mAccount.Lastname;
			reqParams.firstname = mAccount.Firstname;
			if ( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mAccountBrand) ) {
				reqParams.email = mAccount.MyRenaultID__c;
			} else if ( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mAccountBrand) ) {
				reqParams.email = mAccount.MyDaciaID__c;
			}
			reqParams.customerIdentificationNbr = mAccount.CustomerIdentificationNbr__c;
			reqParams.vin = mVin;
			reqParams.city = mAccount.BillingCity;
			reqParams.brand = mAccountBrand;
			reqParams.country = mAccount.Country__c;
			reqParams.renaultPersDataId = determinePersonalId();
			if( !String.isBlank(mAccount.HomePhone__c) ) {
				reqParams.phoneNumber = mAccount.HomePhone__c;
			} else if ( !String.isBlank(mAccount.PersMobPhone__c) ) {
				reqParams.phoneNumber = mAccount.PersMobPhone__c;
			}

			//Call the webservice
			mPersDataCaller = new Myr_ManageAccount_PersDataCall_Cls(mAccountBrand, mAccount.Country__c);
			if( null != listMatchingKeys ) {
				mPersDataCaller.specifyMatchingKeyOnTheFly( listMatchingKeys );
			}
			mPDataResponse = mPersDataCaller.findCustomer(reqParams);
			//log the tries
			for( Myr_ManageAccount_PersDataCall_Cls.CallTry call : mPersDataCaller.getCaller().callTries ) {
				String msg = 'QUERY=' + call.rawQuery + '<br/>' + 'RESPONSE=' + call.rawResponse;
				log('',LOG_SEARCHTRY, msg, beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Info); 
            }
			if( mPDataResponse != null && mPDataResponse.response != null && mPDataResponse.response.infoclients != null && mPDataResponse.response.infoclients.size() == 1) {
				mClient = mPDataResponse.response.infoclients[0];
				//mDataSource = mPDataResponse.response.dataSource;
				log('', LOG_SEARCHKO, 'Key='+mPersDataCaller.getSuccessKey()+', Response='+mPDataResponse, beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Info);
			} else   {
				log('', LOG_SEARCHKO, 'No matching response found', beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Info);
			}
			system.debug('##Ludo mresponse : ' + mPDataResponse);
			return mPDataResponse;
		} catch (Exception e) { 
			log('', LOG_SEARCHEXCEPTION, e.getMessage() + ', trace=' + e.getStackTraceString(), beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Warn);
        }
		return null;
	}

	/** Update the accounts with personal data responses**/
	public void updateAccounts() {
		//nothing to be done
		if( mClient == null ) return;
		Long beginT = system.now().getTime();
		
		try { 

			/* Save Customer Ids before checking them */
			String mdmId = mAccount.Tech_ACCExternalID__c;
			String bcsId = mAccount.Bcs_Id__c;
			String bcsDaciaId = mAccount.Bcs_Id_Dacia__c;
		
			/* save communication aggreement */
			string PostCommAgreementOld = mAccount.Address__pc;
			Date PostCommAgreementDateOld = mAccount.AddressDate__pc;
			string TelCommAgreementOld = mAccount.PersMobiPhone__pc;
			Date TelCommAgreementDateOld = mAccount.PersMobiPhoneDate__pc;
			string SMSCommAgreementOld = mAccount.SMS__pc;
			Date SMSCommAgreementDateOld = mAccount.SMSDate__pc;
			string EmailCommAgreementOld = mAccount.PersEmail__pc;
			Date EmailCommAgreementDateOld = mAccount.PersEmailDate__pc;
			string GlobalCommAgreementOld = mAccount.ComAgreemt__c;
				
			system.debug('### mAcount before: ' + mAccount );
			mclient = formatresponse(mclient, mAccount);
			mAccount = TRVUtils.mappingAccount(mclient, mAccount);
			system.debug('### mAcount after: ' + mAccount );
				
			mAccount.Address__pc = PostCommAgreementOld;
			mAccount.AddressDate__pc = PostCommAgreementDateOld;
			mAccount.PersMobiPhone__pc = TelCommAgreementOld;
			mAccount.PersMobiPhoneDate__pc = TelCommAgreementDateOld;
			mAccount.SMS__pc = SMSCommAgreementOld;
			mAccount.SMSDate__pc = SMSCommAgreementDateOld;
			mAccount.PersEmail__pc = EmailCommAgreementOld;
			mAccount.PersEmailDate__pc = EmailCommAgreementDateOld;
			mAccount.ComAgreemt__c = GlobalCommAgreementOld;
				
		
			// ----- CUSTOMER IDS --- check the external ids to avoid any problem of dup when trying to insert an
			//external id that is already used b y another personal account.
			if( !String.isBlank(mAccount.Tech_ACCExternalID__c) && !mAccount.Tech_ACCExternalID__c.equalsIgnoreCase( mdmId ) ) {
				//oups, account has been filled with a new mdm id. Check it before updating account to avoid any update exception
				List<Account> listMdmIds = [SELECT Id FROM Account WHERE Tech_ACCExternalID__c = :mAccount.Tech_ACCExternalID__c AND Id <> :mAccount.Id];
				if( listMdmIds.size() > 0 ) {
					//found another account with this id, set the current value to blank
					mAccount.Tech_ACCExternalID__c = null;
				}
			}
			if( !String.isBlank(mAccount.Bcs_Id__c) && !mAccount.Bcs_Id__c.equalsIgnoreCase( bcsId ) ) {
				//oups, account has been filled with a new mdm id. Check it before updating account to avoid any update exception
				List<Account> listBcsIds = [SELECT Id FROM Account WHERE Bcs_Id__c = :mAccount.Bcs_Id__c AND Id <> :mAccount.Id];
				if( listBcsIds.size() > 0 ) {
					//found another account with this id, set the current value to blank
					mAccount.Bcs_Id__c = null;
				}
			}
			if( !String.isBlank(mAccount.Bcs_Id_Dacia__c) && !mAccount.Bcs_Id_Dacia__c.equalsIgnoreCase( bcsDaciaId ) ) {
				//oups, account has been filled with a new mdm id. Check it before updating account to avoid any update exception
				List<Account> listBcsDaciaIds = [SELECT Id FROM Account WHERE Bcs_Id_Dacia__c = :mAccount.Bcs_Id_Dacia__c AND Id <> :mAccount.Id];
				if( listBcsDaciaIds.size() > 0 ) {
					//found another account with this id, set the current value to blank
					mAccount.Bcs_Id_Dacia__c = null;
				}
			}
        
			//Finally update the acount;
			update mAccount;
			log('', LOG_ACC_UPDOK, '' , beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Info);
		} catch (Exception e) {
			log('', LOG_ACC_UPDEXCEPTION, e.getMessage() + ', trace=' + e.getStackTraceString() , beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Critical);
		}
	}


	private TRVResponseWebservices.InfoClient formatresponse(TRVResponseWebservices.InfoClient rformat, account a)
	{
		
		if (String.isEmpty(a.FirstName) == false)
			rformat.FirstName1 = a.FirstName;
		/*if (String.isEmpty(a.SecondFirstName__c) == false)
			rformat.FirstName2 = a.SecondFirstName__c;*/
		if (String.isEmpty(a.LastName) == false)
			rformat.LastName1 = a.LastName;
		if (String.isEmpty(a.SecondSurname__c) == false)
			rformat.LastName2 = a.SecondSurname__c;
		if (String.isEmpty(a.Language__c) == false)
			rformat.Lang = a.Language__c;
		if (String.isEmpty(a.Salutation) == false)
			rformat.civility = a.Salutation;
		if (String.isEmpty(a.Title__c) == false)
			rformat.title = a.Title__c;
		if (a.PersonBirthdate != null)
			rformat.strDOB = String.valueof(a.PersonBirthdate);
		if (String.isEmpty(a.Sex__c) == false)
			rformat.sex = a.Sex__c;
		if (String.isEmpty(a.BillingCountry) == false) 
			rformat.CountryCode =  a.BillingCountry;
		if (String.isEmpty(a.CustomerIdentificationNbr__c) == false)
			rformat.localCustomerID1 = a.CustomerIdentificationNbr__c;
		if (String.isEmpty(a.CustomerIdentificationNbr2__c) == false)
			rformat.localCustomerID2 = a.CustomerIdentificationNbr2__c;
		
		if (String.isEmpty(a.BillingPostalCode) == false)
			rformat.zip = a.BillingPostalCode;
		if (String.isEmpty(a.BillingCity) == false)
			rformat.City = a.BillingCity;
		if (String.isEmpty(a.BillingState) == false)
			rformat.region = a.BillingState;
		if (String.isEmpty(a.BillingCountry) == false)
			rformat.CountryCode = a.BillingCountry;
		if (String.isEmpty(a.BillingStreet) == false)
		{
			rformat.StrNum ='';
			rformat.strType='';
			rformat.addressLine2='';
			rformat.Compl1='';
			rformat.Compl1='';
			rformat.addressLine1 = a.BillingStreet;
		}
		if (String.isEmpty(a.HomePhone__c) == false)
			rformat.strFixeLandLine = a.HomePhone__c;
		if (String.isEmpty(a.ProfLandline__c) == false)
			rformat.strFixeLandLinePro = a.ProfLandline__c;
		if (String.isEmpty(a.PersMobPhone__c) == false)
			rformat.strMobile = a.PersMobPhone__c;
		if (String.isEmpty(a.PersEmailAddress__c) == false)
			rformat.email = a.PersEmailAddress__c;
			

		return rformat;		
	}
	
	/** Update the vehicles **/
	public void updateVehicles() {
		system.debug('### Myr_ManageAccount_PersDataUpdate_Cls - <updateVehicles>');	
		//Quit the function: nothing to do
		if( mClient == null ) return;
		if( mClient.vcle == null ) return;
		if( mClient.vcle.size() <= 0 ) return;
		Long beginT = system.now().getTime();

		Map<String, VEH_Veh__c> mapVINVehId = new Map<String, VEH_Veh__c>();
		List<TRVResponseWebservices.Vehicle> vehListPersData = mClient.vcle;
		List<String> myVINList= new List<String>();
		List<VEH_Veh__c> newV = new List<VEH_Veh__c>();

		String renault = Myr_MyRenaultTools.Brand.Renault.name();
		String dacia = Myr_MyRenaultTools.Brand.Dacia.name();
		system.debug('##Ludo Debut vehicule mYr'); 
		try{
			
			//Prepare the list of existing vehicle
			for( TRVResponseWebservices.Vehicle v: vehListPersData ){
				myVINList.add(v.VIN);
			}
			List<VEH_Veh__c> myVList = [SELECT Id, Name, VehicleBrand__c, VehicleRegistrNbr__c, Model_Code2__c, Technical_Control_Date__c,
										Commercial_Model__c, Version__c, FirstRegistrDate__c, LastRegistrDate__c , DeliveryDate__c
										FROM VEH_Veh__c WHERE Name in :myVINList];
			
			for( VEH_Veh__c veh : myVList ) {
				mapVINVehId.put( veh.Name, veh );
			}
        		system.debug('##Ludo Debut vehicule mYr 2'); 
			// -----------------------------------------------------------------------------------------------------------------
			// ------------- VEHICLES ------------------------------------------------------------------------------------------
			for( TRVResponseWebservices.Vehicle v: vehListPersData ){
				system.debug('### Myr_ManageAccount - <Manage_Vehicle> - Vehicle ' + v.vin + ', brand=' + v.brandCode);
				//SDP - 29.01.2016
				 //If the vehicle retreived from PERsonal Data is not a Renault or a Dacia, *    
				//the vehicle is not taken into account 
				if( String.isBlank(v.brandCode) ||
					(!String.isBlank(v.brandCode) && !v.brandCode.equalsIgnoreCase(renault) && !v.brandCode.equalsIgnoreCase(dacia) ) ) {
						system.debug('### Myr_ManageAccount - <Manage_Vehicle> - Vehicle ' + v.vin + ' rejected, brand=' + v.brandCode);
						continue; //Skip the currrent vehicle
				}

				//Either we get the vin, either we create a new vehicle
				VEH_Veh__c myV = new VEH_Veh__c();
				
				myV = TRVUtils.mappingVehicle(v, myV);

				if( mapVINVehId.containsKey(v.VIN) ) {
					myV = mapVINVehId.get(v.VIN);
				} else {
					myV.Data_Completion_BVM_Needed__c = true;
					myV.Data_Completion_ETICOM_Needed__c = true;
					myV.Data_Completion_SLK_Needed__c = true;
				}
				
				newV.add( myV );
				system.debug('##Ludo myV : ' + myV);
			}
        
			//Upsert vehicles, then continue with the relations
            upsert newV;
            
			//Added just inserted vehicles in the map to be able to retrieve them for the relations
			mapVINVehId = new Map<String, VEH_Veh__c>();
			for( VEH_Veh__c veh : newV ) {
				mapVINVehId.put( veh.Name, veh );
			}
        
			// -----------------------------------------------------------------------------------------------------------------
			// ------------- VEHICLE RELATIONS ---------------------------------------------------------------------------------  

			List<VRE_VehRel__c> newVR = new List<VRE_VehRel__c>();
			List<VRE_VehRel__c> myVRList = [SELECT Id, Account__c, Vin__r.Name, VNVO__c, TypeRelation__c, StartDateRelation__c, EndDateRelation__c, My_Garage_Status__c
											, VIN__r.VehicleBrand__c //F1854 / US3654
											FROM VRE_VehRel__c WHERE Vin__r.Name in :myVINList];
			Map<String, List<VRE_VehRel__c>> mapVINVehRel = new Map<String, List<VRE_VehRel__c>>(); 
			for( VRE_VehRel__c vre : myVRList ) {
				if( mapVINVehRel.containsKey( vre.VIN__r.Name ) ) {
					mapVINVehRel.get( vre.VIN__r.Name ).add(vre);
				} else {
					mapVINVehRel.put( vre.VIN__r.Name, new List<VRE_VehRel__c>{vre});
				}
			}

			for( TRVResponseWebservices.Vehicle v: vehListPersData ){
				
				//SDP - 29.01.2016
				//If the vehicle retreived from PERsonal Data is not a Renault or a Dacia, *    
				//the vehicle is not taken into account 	
				if( String.isBlank(v.brandCode) ||
					(!String.isBlank(v.brandCode) && !v.brandCode.equalsIgnoreCase(renault) && !v.brandCode.equalsIgnoreCase(dacia) ) ) {
					  system.debug('### Myr_ManageAccount - <Manage_Vehicle> - Vehicle ' + v.vin + ' rejected, brand=' + v.brandCode);
					  continue; //Skip the currrent vehicle
				}
				system.debug('>>>>> v.brandCode='+v.brandCode);
         
				//Either we get the relation, either we create a new vehicle
				VEH_Veh__c veh = mapVINVehId.get( v.vin );
				if( null == veh ) continue;
				List<VRE_VehRel__c> listRelations = mapVINVehRel.get( v.Vin );

				VRE_VehRel__c vehRel = new VRE_VehRel__c();
				vehRel = TRVUtils.mappingVehicleRelations(v,vehRel,mAccount,veh);
				System.debug('vehRel.Status__c=' + vehRel.Status__c + '' + vehRel.My_Garage_Status__c);
				if( null == listRelations ) {
					newVR.add(vehRel); 
				} else {
					System.debug('vehRel.Status__c2222');
					Boolean LinkedToThisAccount = false;
					for( VRE_VehRel__c vre : listRelations ) {
						if( vre.Account__c == mAccount.Id ) {
							System.debug('vehRel.Status__c3333');
							vehRel.Id = vre.Id;
							newVR.add(vehRel);
							LinkedToThisAccount = true;
						}
					}
					if( !LoginMode && !LinkedToThisAccount) {
						System.debug('vehRel.Status__c4444');
						newVR.add(vehRel);
					}
				} 

			}       
			//Upsert relations
			upsert newVR;

			//Then second update for MyGarageStatus. It is done in 2 phases
			//because the link between the relation and the vehicle for the new relationsdoes not exist
			//and so it is not possible to retrieve the vehicle brand through the link
			//F1854 / US3654 Set the my garage status
			Myr_ManageAccount_VehGarageStatus relationsUpdater = new Myr_ManageAccount_VehGarageStatus( mAccountId );
			relationsUpdater.setGaragetStatusAndCommit();

			for (VRE_VehRel__c vr : newVR){
				System.debug('### TESTS22 : vr=<'+ vr + '> My_Garage_Status__c=<' + vr.My_Garage_Status__c + '>');
			}
			
            log('', LOG_VEHVRE_UPDOK, '' , beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Info);
        } catch (Exception e) {
            log('', LOG_VEHVRE_UPDEXCEPTION, e.getMessage() + ', trace=' + e.getStackTraceString() , beginT, system.now().getTime(), INDUS_Logger_CLS.ErrorLevel.Critical);
        }
    }

	/**
	* @description Generates a log
	* @param record, the record (generally the id of the salesforce record)
	* @param ExceptType, String representing the exception type
	* @param Message, String for thez message that should be displayed
	* @param lvl, INDUS_Logger_CLS.ErrorLevel, the error level
	*/
	private void log( String record, String ExceptType, String Message, Long beginT, Long endT, INDUS_Logger_CLS.ErrorLevel lvl) {
		system.debug('### Myr_ManageAccount_PersDataUpdate_Cls - ExceptType='+ExceptType+' - Message='+Message+' - errorLevel=' + lvl);
		Id idLog = INDUS_Logger_CLS.addGroupedLogFull (
				null,													//runId
				mParentLogId,											//parentLogId
				INDUS_Logger_CLS.ProjectName.MYR,						//Project name
				mExecutionContext,										//apex class that is in this case the contect
				LOG_FUNCTION,						//function
				ExceptType,												//exception type
				Message,												//message
				lvl.name(),												//error level
				mAccountId,												//Account id
				null,													//Relation Id
				null,													//vehicle id
				beginT,													//Begin Time
				endT,													//End_Time
				Record													//details on the record
			);
		if( null == mParentLogId ) {
			mParentLogId = idLog;
		}
	}

}