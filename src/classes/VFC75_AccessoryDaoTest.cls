@isTest
private class VFC75_AccessoryDaoTest {
    
    static testMethod void test() {
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        Insert dealer;
        
        Account dealer2 = moc.criaAccountDealer();
        dealer2.IDBIR__c = '1234567';
        dealer2.ParentId = dealer.Id;
        Insert dealer2;
        
        Account a = moc.criaPersAccount();
        Insert a;
        
        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = a.Id;
        opp.Dealer__c = dealer2.Id;
        Insert opp;
        
        Product2 prd = moc.criaProduct2();
        Insert prd;
        
        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;
        
        Quote q = moc.criaQuote();
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb.Id;
        Insert q;
        
        Product2 prd2 = moc.criaProduct2();
        prd2.ModelSpecCode__c = 'Name';
        prd2.Model__c = prd.Id;
        Insert prd2;
        
        VEH_Veh__c v = moc.criaVeiculo();
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        vr.Account__c = dealer2.Id;
        Insert vr;
        
        PricebookEntry pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = pb.Id;
        pe.Product2Id = prd.Id;
        
        VFC75_AccessoryDAO.getInstance().insertData(pe);
        //Insert pe;
        
        VFC75_AccessoryDAO.getInstance().getPricebookEntryByProduc(prd.Id);
        PricebookEntry pbe = VFC75_AccessoryDAO.getInstance().getPricebookEntry(prd.id, pb.Id);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.quoteId = q.Id;
        quoteLineItem.PricebookEntryId = pbe.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 10;
        quoteLineItem.Vehicle__c = v.Id;
        
        VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem); 
        VFC75_AccessoryDAO.getInstance().updateData(quoteLineItem);
    }
}