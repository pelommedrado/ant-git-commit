@isTest
public class RFORCE_sendSmsControllerTest {
	
    public static Case caso;
    
    static{
        createData(false);
        VEH_Veh__c vehicle = [select Id from VEH_Veh__c limit 1];
        Account customerAccount = [SELECT Id FROM Account where RecordType.DeveloperName = 'Personal_Acc' limit 1];
        caso = new Case(
        	AccountId = customerAccount.Id,
            VIN_Web__c  = vehicle.Id,
            Status = 'New',
            Origin = 'RENAULT SITE',
            CaseBrand__c = 'Renault',
            Description = 'test',
            Cell_Phone_Web__c = '1111111111',
            FirstName_Web__c = 'fistName',
            LastName_Web__c = 'LastName'
        );
        Database.insert(caso);
        
        marketing_cloud_configuration__c mc = new marketing_cloud_configuration__c();
        mc.Name = 'default';
        mc.messageId__c = 'test';
        mc.clientSecret__c = 'test';
        mc.clientSecret__c = 'test';
        insert mc;
        
    }
    
    
    static testMethod void validateFieldsList(){
        Id systemAdiminProfileId = Utils.getSystemAdminProfileId();
        System.runAs(new User(Id = UserInfo.getUserId())){
            Test.startTest();
            PageReference pageRef = Page.RFORCE_sendSms;
            Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('header', 'false');
			ApexPages.currentPage().getParameters().put('id', caso.Id);
            
            RFORCE_sendSmsController controller = new RFORCE_sendSmsController();
            List<FieldsToPutOnSMS__mdt> fieldsList = controller.fieldsList;
            System.assert(!fieldsList.isEmpty());
            
            String metadataString = controller.fieldsListToString;
            System.assert(String.isNotEmpty(metadataString));
            
            String querySring = controller.queryFieldsCaseInfo;
            System.assert(String.isNotEmpty(querySring));
            
            String token = RFORCE_sendSmsController.login();
            System.assertEquals('Success', token);
            
            RFORCE_sendSmsController.sendSMS('11977751839','test',caso.Id);
            Test.stopTest();
        }
    }
    
    
    public static void createData(boolean createUser){

		Id dealerRecordTypeId = Utils.getRecordTypeId('Account','Network_Site_Acc');
		Id contactRecordTypeId = Utils.getRecordTypeId('Contact','Network_Ctc');
		Id gerenteProfileId = Utils.getProfileId('BR - Renault + Cliente Manager');
		Id sellerProfileId = Utils.getProfileId('BR - Renault + Cliente');

		Id parentId = criaConcessionariaGrupo(dealerRecordTypeId);

		Account dealer = criaConcessionaria(parentId, dealerRecordTypeId);
        List<VEH_Veh__c> veiculoList = criarVeiculos();

		criarRelacaoPosse(dealer.Id, veiculoList);
        createCustomerAccount();

	}
    
    
    public static User criaUsuariosAdmin(String firstName, String lastName, String email){
        
        String nameSandbox = UserInfo.getUserName().substringAfterLast('.');
        
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Username = email + '.' + nameSandbox,
            Email = email,
            Alias = firstName.substring(0,3),
            RecordDefaultCountry__c = 'Brazil',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            TimeZoneSidKey='America/Sao_Paulo',
            ProfileId = Utils.getSystemAdminProfileId(),
            DefaultCurrencyIsoCode = 'BRL'
        );
        
        Database.DMLOptions dmlo = new Database.DMLOptions();
        dmlo.EmailHeader.triggerUserEmail = true;
        dmlo.EmailHeader.triggerAutoResponseEmail= true;
        u.setOptions(dmlo);
        
        return u;
        
    }

	public static void criarRelacaoPosse(Id accountId, List<VEH_Veh__c> veiculoList){

		List<VRE_VehRel__c> veiculoRelList = new List<VRE_VehRel__c>();

		for(Integer i=0; i < 10; i++){

			VRE_VehRel__c veiculoRel = new VRE_VehRel__c(
				Account__c = accountId,
				VIN__c = veiculoList[i].Id
			);

			veiculoRelList.add(veiculoRel);

		}

		Insert veiculoRelList;

	}

	public static List<VEH_Veh__c> criarVeiculos(){

		List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();
		List<String> modelos = new List<String>{'CLIO','SANDERO','DUSTER','LOGAN','CAPTUR','FLUENCE','MASTER','KANGOO','CLIO','SANDERO'};

		for(Integer i=0; i < 10; i++){

			VEH_Veh__c veiculo = new VEH_Veh__c(
				Name = '3425GSVFAGS2346' +  String.valueOf( 37 + i ),
				Version__c = 'Authentic 1.0',
				Price__c = 35000,
				Model__c = modelos[i],
				Is_Available__c = true,
				Status__c = 'Stock',
				Color__c = 'Prata'
			);

			veiculoList.add(veiculo);

		}

		Insert veiculoList;

		return veiculoList;

	}

	public static Id obterConjPermissao(String name){

		List<PermissionSet> conjPerms = [SELECT Id FROM PermissionSet WHERE Name =: name];

		Id conjPermsId = conjPerms.get(0).Id != null ? conjPerms.get(0).Id : null;

		return conjPermsId;

	}

	public static void criarPermissao(Id userId, Id permSetId){

		PermissionSetAssignment psa = new PermissionSetAssignment(
			AssigneeId = userId,
			PermissionSetId = permSetId
		);
		Insert psa;

	}

	public static Id criaConcessionariaGrupo(Id dealerRecordTypeId){

		Account dealer = new Account(
			Name = 'Concessionaria Grupo',
			RecordTypeId = dealerRecordTypeId,
			IDBIR__c = '123451'
		);

		Insert dealer;

		return dealer.Id;

	}

	public static Account criaConcessionaria(Id parentId, Id dealerRecordTypeId){

		Account dealer = new Account(
			Name = 'Concessionaria',
			RecordTypeId = dealerRecordTypeId,
			IDBIR__c = '12341',
			ParentId = parentId,
			NameZone__c = 'R1'
		);

		Insert dealer;

		return dealer;

	}

	public static Contact criaContatoConcessionaria(Id accountId, Id contactRecordTypeId, String firstName){

		Contact dealerContact = new Contact(
			FirstName = firstName,
			LastName = 'Teste ' ,
			RecordTypeId = contactRecordTypeId,
			AccountId = accountId
		);

		Insert dealerContact;

		return dealerContact;

	}

	@Future
	public static void criaUsuarioComunidade(Id contactId, String bir, String firstName, Id profileId, String conjPerm){

		System.debug('firstName: '+firstName);

		System.debug('firstName.substring(): ' + firstName.substring( firstName.length() - 4,firstName.length() ));


        User u = new User(
            FirstName = firstName,
            LastName = 'Test',
            Username = 'usuario'+ firstName.replace(' ','') +'@test.com',
            Email = 'usuario' + firstName.replace(' ','') + '@test.com',
            Alias = firstName.substring(firstName.length() - 4,firstName.length()),
            RecordDefaultCountry__c = 'Brazil',
            BIR__c = bir,
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            ProfileId = profileId,
            ContactId = contactId,
            TimeZoneSidKey='America/Sao_Paulo',
            DefaultCurrencyIsoCode = 'BRL'
        );

        Insert u;

		criarPermissao(u.Id, obterConjPermissao(conjPerm));
    }
    
    public static void createCustomerAccount(){
		Id recordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
        Account conta = new  Account();
        conta.recordTypeId = recordTypeId;
        conta.FirstName = 'first Name';
        conta.LastName = 'lastName';
        conta.AccountBrand__c = 'Renault';
        conta.Sex__c = 'Man';
        conta.CustomerIdentificationNbr__c = '49188563499';
        conta.Language__c = 'Portuguese';
        conta.MaritalStatus__c = '2-SINGLE';
        conta.NbrChildrenHome__c = '0';
        conta.NbrAdultsHome__c = '0';
        conta.CustmrStatus__c = 'Customer';
        conta.SpecialCustmr__c = 'VIP - VIP';
        conta.Deceased__c = 'No';
        conta.ComAgreemt__c = 'No';
        conta.PersMobPhone__c = '41991688074';
        conta.PersEmailAddress__c = 'felipe@kolekto.com.br';
        conta.ProEmailAddress__pc = 'felipe@kolekto.com.br';
        conta.ProfMobiPhone__pc = '41991688074';
        conta.Phone = '1122334455';
        Database.insert(conta);
        
    }
}