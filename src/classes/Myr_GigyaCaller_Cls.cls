/** Gigya class that makes the callout to the Gigya referentials

	Please find hereunder the references in the Gigya documentation:
	 - token:                  http://developers.gigya.com/display/GD/socialize.getToken+REST
	 - resend activation mail: http://developers.gigya.com/display/GD/accounts.resendVerificationCode+REST
	 - reset password:         http://developers.gigya.com/display/GD/accounts.resetPassword+REST
	 - account search:         http://developers.gigya.com/display/GD/accounts.search+REST
	 - change email:		   http://developers.gigya.com/display/GD/accounts.setAccountInfo+REST
	 - delete account:		   http://developers.gigya.com/display/GD/accounts.deleteAccount+REST

	@author S. Ducamp
	@date 29.09.2015
	
	@version 1.0 initial version
	@version 1.1 add change email$
	@version 16.11 (SDP): introduce the force new Login Ids (call nomailverification Gigya API and call a setAccountInfo with addLoginEmails filled with the new Email
**/
public with sharing class Myr_GigyaCaller_Cls {
	
	//Determine the couple of keys to use
	public class GigyaIdsCouple {
		public String field;
		public String datacenter;
		public GigyaIdsCouple( String field, String datacenter ) {
			this.field = field;
			this.datacenter = datacenter;
		}
	}
	public static final Map<String, Map<ENVIRONMENT, GigyaIdsCouple>> MAP_BRAND_ENV_FIELDS = new Map<String, Map<ENVIRONMENT, GigyaIdsCouple>> {
		'RENAULT' => new Map<ENVIRONMENT, GigyaIdsCouple> {
			  ENVIRONMENT.PROD => new GigyaIdsCouple('gigya_ren_k__c', 'gigya_datacenter__c')
			, ENVIRONMENT.NOPROD => new GigyaIdsCouple('gigya_ren_k_noprod__c', 'gigya_datacenter_noprod__c')
			, ENVIRONMENT.NOVERIFEMAIL_PROD => new GigyaIdsCouple('gigya_ren_noemailverif_k__c', 'gigya_datacenter__c')
			, ENVIRONMENT.NOVERIFEMAIL_NOPROD => new GigyaIdsCouple('gigya_ren_noemailverif_k_noprod__c', 'gigya_datacenter_noprod__c')
		},
		'DACIA' => new Map<ENVIRONMENT, GigyaIdsCouple> {
			  ENVIRONMENT.PROD => new GigyaIdsCouple('gigya_dac_k__c', 'gigya_datacenter__c')
			, ENVIRONMENT.NOPROD => new GigyaIdsCouple('gigya_dac_k_noprod__c', 'gigya_datacenter_noprod__c')
			, ENVIRONMENT.NOVERIFEMAIL_PROD => new GigyaIdsCouple('gigya_dac_noemailverif_k__c', 'gigya_datacenter__c')
			, ENVIRONMENT.NOVERIFEMAIL_NOPROD => new GigyaIdsCouple('gigya_dac_noemailverif_k_noprod__c', 'gigya_datacenter_noprod__c')
		}
	};

    /** Gigya settings **/
    private CS_Gigya_Settings__c gigyaSettings;
    private Country_Info__c countrySettings;
    /** private members **/
    private String mToken;
    private String mCountry;
    private String mBrand;
    private String gigyaKey; //corresponds to encrypted(APIKey:SecretKey)
    private String gigyaDatacenterId;
	public enum ENVIRONMENT { PROD, NOPROD, NOVERIFEMAIL_PROD, NOVERIFEMAIL_NOPROD }

    /** Exception class **/
    public class MYR_GigyaCaller_Exception extends Exception {}


    /** @constructor
    	@throw MYR_Gigya_Caller_Exception
    **/
    public Myr_GigyaCaller_Cls(String country, String brand, ENVIRONMENT env) {
    	system.debug('#### MYR_Gigya_Caller_Cls - <constructor> - Get The Token for Country=' + country + ' and brand=' + brand + ' and ENVIRONMENT=' + env);
    	mBrand = brand;
    	mCountry = country;
		    	
    	//get and check the general Gigya Settings
    	system.debug('#### MYR_Gigya_Caller_Cls - <constructor> - Get the Gigya settings');
        gigyaSettings = CS_Gigya_Settings__c.getInstance();
        //it is possible that the custom setting returned is not null but blank ....
        if( gigyaSettings == null || (gigyaSettings != null && String.isBlank(gigyaSettings.Name)) ) { 
        	throw new MYR_GigyaCaller_Exception(system.Label.GIGYA_GigyaCaller_NoGigyaSettings);
        }
        
        //get and check the country Settings
        system.debug('#### MYR_Gigya_Caller_Cls - <constructor> - Get the Country Info settings');
        countrySettings = Country_Info__c.getInstance(country);
        if( countrySettings == null ) { 
			String message = String.format(system.Label.GIGYA_GigyaCaller_NoCountry, new List<String>{country});
        	throw new MYR_GigyaCaller_Exception(message);
        }
		//Get the right couple to use 
		if( !MAP_BRAND_ENV_FIELDS.containsKey(brand.toUpperCase()) ) {
			String message = String.format(system.Label.GIGYA_GigyaCaller_CountryNoBrand, new List<String>{country,brand,env.name()});
			throw new MYR_GigyaCaller_Exception(message);
		}
		GigyaIdsCouple gigCouple = MAP_BRAND_ENV_FIELDS.get(brand.toUpperCase()).get(env);
		//control the couple
		if( String.isBlank( (String) countrySettings.get(gigCouple.datacenter) ) ) {
			String message = String.format(system.Label.GIGYA_GigyaCaller_NoDataCenterId, new List<String>{country, env.name()});
			throw new MYR_GigyaCaller_Exception(message);
		} else {
			gigyaDatacenterId = (String) countrySettings.get( gigCouple.datacenter );
		}
		if( String.isBlank( (String) countrySettings.get(gigCouple.field) ) ) {
			String message = String.format(system.Label.GIGYA_GigyaCaller_CountryNoBrand, new List<String>{country,brand,env.name()});
			throw new MYR_GigyaCaller_Exception(message);
		} else {
			gigyaKey = (String) countrySettings.get( gigCouple.field );
		}        
        //request the token
		requestToken();
    }
    
    /** @return the Token retrieved at the construction of the Gigya instance **/
    public String getToken() {
        return mToken;
    }
   
    /** get the token to request the Gigya REST API **/
    private void requestToken() {
    	system.debug('#### MYR_Gigya_Caller_Cls - <requestToken> - BEGIN');
    	try {
	        GetTokenRequest lTokenRequest = new GetTokenRequest(gigyaSettings.Url_Token__c, gigyaKey);
	        //Send the request 
	        HTTPResponse response = lTokenRequest.sendRequest();
	        //Parse the response
	        if( response != null && response.getBody() != '') {
	            JSONParser parser = JSON.createParser( response.getBody() );
	            parser.nextToken();
	            Myr_GigyaParser_Cls.TokenParser tokenParser = (Myr_GigyaParser_Cls.TokenParser) parser.readValueAs(Myr_GigyaParser_Cls.TokenParser.class);
	            mToken = tokenParser.access_token;
	            system.debug('#### MYR_Gigya_Caller_Cls - <requestToken> - access_token: ' + mToken);
	        } else {
	            throw new MYR_GigyaCaller_Exception(system.Label.GIGYA_GigyaCaller_ResponseEmpty);
	        } 
    	} catch (Exception e) {
    		throw new MYR_GigyaCaller_Exception( system.Label.GIGYA_GigyaCaller_TokenProblem + e.getMessage() );
    	}
    }
    
    /** Parse the HTTP response of Gigya to return an internal object
    	@param HTTResponse, the response to parse 
    	@return Myr_GigyaParser_Cls.GigyaAccountParser, the parsed response
    	@throws MYR_GigyaCaller_Exception 
    **/
    private Myr_GigyaParser_Cls.GigyaAccountParser parseGigyaResponse(HTTPResponse response) {
    	system.debug('### Myr_GigyaCaller_Cls - <parseGigyaResponse> - response=' + String.ValueOf(response));
    	Myr_GigyaParser_Cls.GigyaAccountParser gigParser = null;
    	if( response != null && response.getStatusCode() == 200 && response.getBody() != '') {
    		JSONParser parser = JSON.createParser( response.getBody() );
            parser.nextToken();
            gigParser = (Myr_GigyaParser_Cls.GigyaAccountParser) parser.readValueAs(Myr_GigyaParser_Cls.GigyaAccountParser.class);
		} else {
			throw new MYR_GigyaCaller_Exception( system.Label.GIGYA_GigyaCaller_ResponseEmpty );
		}
		return gigParser;
    }
    
    /** Search the given account within Gigya
    	@param the account salesforce Id
    	@return the Gigya structure
    	@throws MYR_GigyaCaller_Exception
    **/ 
    public Myr_GigyaParser_Cls.GigyaAccountParser searchAccount(String accountSfdcId) {
    	try {
    		SearchAccountRequest sAccReq = new SearchAccountRequest(gigyaSettings.Url_Account_Search__c, mToken, gigyaDatacenterId, accountSfdcId);
    		//Send the query
    		HTTPResponse response = sAccReq.sendRequest();
    		//Parse the response
    		return parseGigyaResponse(response);
    	} catch (Exception e) {
			system.debug('#### MYR_Gigya_CLS - <searchAccount> - exception: ' + e.getMessage());
			system.debug('#### MYR_Gigya_CLS - <searchAccount> - exception: ' + e.getStackTraceString());
    		throw new MYR_GigyaCaller_Exception( e.getMessage() );
    	} 
    }
    
    /** Reset the password for the given loginsIds 
    	@param the email specified in loginsIds.emails
    	@return the status of the response
    **/
    public Myr_GigyaParser_Cls.GigyaAccountParser resetPassword( String loginIDs ) {
		loginIDs = EncodingUtil.urlEncode( loginIDs, 'UTF-8' );
    	MainGigyaRequest gigyaReq = new MainGigyaRequest(gigyaSettings.Url_Reset_Password__c, mToken, gigyaDatacenterId, 'loginID='+loginIDs, '', GIGYAMODE.GET);
    	//Send the query
    	HTTPResponse response = gigyaReq.sendRequest();
    	//Parse the response
    	return parseGigyaResponse(response);
    } 
    
    /** Resend the Activation Mail 
    	@param String, the Gigya UID
    	@param String, the email to use to resend the activation email
    	@return the status of the response
    **/
    public Myr_GigyaParser_Cls.GigyaAccountParser resendActivationMail( String uid, String email ) {
		email = EncodingUtil.urlEncode( email, 'UTF-8' );
    	MainGigyaRequest gigyaReq = new MainGigyaRequest(gigyaSettings.Url_Resend_Activation_Email__c, mToken, gigyaDatacenterId, 'uid='+uid+'&email='+email, '', GIGYAMODE.GET );
    	//Send the query
    	HTTPResponse response = gigyaReq.sendRequest();
    	//Parse the response
    	return parseGigyaResponse(response); 
    }
	
	/** Change the email on Gigya
	* @param uid 
	* @param newEmail 
	* @return Myr_GigyaParser_Cls.GigyaAccountParser 
	*
	* Sample:
	* POST /accounts.setAccountInfo HTTP/1.1
	* Host: accounts.eu1.gigya.com
	* Authorization: OAuth AT3_49AFB14F8ABA51E5F08B37FD002CBCF6_Jbln2RJFz1KMSfEHL7LTCwQuXu4__PDuM_GH0LIzsgA7vtcThqkC1Pt9P31Miyui6vng3O3TThX-yDVl-MrMpA==
	* Cache-Control: no-cache
	* Content-Type: application/x-www-form-urlencoded
	* UID=e1195a1238884672bd3da52ae45eb213&data=%7B%22newEmail%22%3A%22test.postsplitfront2%40yopmail.com%22%7D&addLoginEmails=test.postsplitfront2%40yopmail.com 
	*/
	public Myr_GigyaParser_Cls.GigyaAccountParser changeGigyaEmail( String uid, String newEmail) {
		//Prepare the query
		String data = EncodingUtil.urlEncode('{"newEmail":"'+newEmail+'"}', 'UTF-8');
		String httpRequest = 'UID='+uid+'&data='+data+'&addLoginEmails='+EncodingUtil.urlEncode(newEmail,'UTF-8');
		MainGigyaRequest gigyaReq = new MainGigyaRequest(gigyaSettings.Url_Set_Account_Info__c, mToken, gigyaDatacenterId, '', httpRequest, GIGYAMODE.POST);
		//send the query
		HTTPResponse response = gigyaReq.sendRequest();
		//and parse the response
		return parseGigyaResponse(response); 
	}

	/** Force Email on Gigya
		This funciton should be called only with noemailverification Gigya API, otherwise the new login
		won't be validated by default and the customer will receive an activation email.
		This function should be called only in the context of a MyRenault changeEmail.
	* @param uid 
	* @param accId, the account salesforce id to push within gigya
	* @param newLogin 
	* @return Myr_GigyaParser_Cls.GigyaAccountParser 
	*/
	public Myr_GigyaParser_Cls.GigyaAccountParser forceGigyaEmail( String uid, String accId, String newEmail, String oldEmail) {
		system.assertEquals( true, !String.isBlank(accId) && accId.length()==18);
		//Prepare the query
		String data = EncodingUtil.urlEncode('{"accountSfdcId":"'+accId+'"}', 'UTF-8');
		String httpRequest='';
		String body = 'UID='+uid;
		body += '&addLoginEmails=' + EncodingUtil.urlEncode( newEmail, 'UTF-8' );
		body += '&removeLoginEmails=' + EncodingUtil.urlEncode( oldEmail, 'UTF-8' );
		body += '&isVerified=true';
		body += '&profile='+EncodingUtil.urlEncode( '{"email":"'+newEmail+'"}', 'UTF-8' );
		body += '&data='+EncodingUtil.urlEncode( '{"accountSfdcId":"'+accId+'"}', 'UTF-8' );
		MainGigyaRequest gigyaReq = new MainGigyaRequest(gigyaSettings.Url_Set_Account_Info__c, mToken, gigyaDatacenterId, httpRequest, body, GIGYAMODE.POST);
		//send the query
		HTTPResponse response = gigyaReq.sendRequest();
		//and parse the response
		return parseGigyaResponse(response); 
	}

	/** Delete the given Gigya UID
      @param String, the Gigya UID
      @return the status of the response
    **/
	public Myr_GigyaParser_Cls.GigyaAccountParser deleteGigyaAccounts( String uid ) {
		MainGigyaRequest gigyaReq = new MainGigyaRequest(gigyaSettings.Url_Delete_Account__c, mToken, gigyaDatacenterId, '', 'UID='+uid, GIGYAMODE.POST);
		//Send the query
		HTTPResponse response = gigyaReq.sendRequest();
		//Parse the response
		return parseGigyaResponse(response); 
    }
    
    /** ---------------------------------------------------------------------------------------- 
        Http Request tools
    **/
    
    /** Interface defining the classes to call Gigya **/
    public interface IRequestGigya {
        String generateHttpQuery();
    }
    
    /** Abstract class to call Gigya. 
    	The inheriting classes have only to implement checkParameters and generateHttpQuery 
    **/
	public enum GIGYAMODE { GET, POST }
    private abstract class ARequestGigya implements IRequestGigya {

		
        
        public String mUrl;
        public String mToken;
        public String mDataCenterId;
		public GIGYAMODE mMode;

        abstract public String generateHttpQuery();
		abstract public String generateBody();
        abstract HttpRequest setToken(HttpRequest iHttpReq);
        
        /** Official SendRequest method for each case **/
        public HTTPResponse sendRequest() {
            //Prepare the request
            HttpRequest req = new HttpRequest();
            req.setMethod(mMode.name());
            req.setEndpoint( mUrl + generateHttpQuery() ); //TODO should be in parameters
            req.setTimeout(120000);
            req.setHeader('Accept', 'application/json');
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
			String body = generateBody();
			if( GIGYAMODE.POST == mMode ) {
				body = (body==null)?'':body;
				req.setHeader('Content-Length', String.valueOf(body.length()));
			}
			req.setBody( body );
             
            //Authentication Header: either login/password in case of token, either token in case of other request
            req = setToken(req);
            
            //Http request
            Http http = new Http();
            HTTPResponse response = null;
            try {
                //REQUEST: execute the call there and display dbug messages
                system.debug('|GIGYA| REQUEST TOKEN = ' + req.getHeader('Authorization'));
                system.debug('|GIGYA| REQUEST       = ' + req.toString());
				system.debug('|GIGYA| REQUEST BODY  = ' + req.getBody());
                response = http.send(req);  
                if( response != null ) {
                    system.debug('|GIGYA| RESPONSE STATUS      = ' + response.getStatus());
                    system.debug('|GIGYA| RESPONSE STATUS_CODE = ' + response.getStatusCode());
					system.debug('|GIGYA| RESPONSE BODY        = ' + response.getBody());
                    if( response.getStatusCode() != 200) {
                        throw new MYR_GigyaCaller_Exception(system.Label.GIGYA_GigyaCaller_Error + ' ' + response.getStatusCode() + ' ' + response.getStatus() );
                    }
                    //RESPONSE: see the response there and display debug messages
                    String strBody = response.getBody();
                    system.debug('|GIGYA| RESPONSE = ' + strBody );
                }
                return response;
            } catch (Exception e ) {
                system.debug('#### MYR_Gigya_CLS - <sendRequest> - exception: ' + e.getMessage());
				system.debug('#### MYR_Gigya_CLS - <sendRequest> - exception: ' + e.getStackTraceString());
                throw new MYR_GigyaCaller_Exception( e.getMessage());
            }
        }
    }
    
    /** Class used to get the Gigya Token **/
    private class GetTokenRequest extends ARequestGigya { 
        private String mGigyaKey; //corresponds to encrypted(APIKey:SecretKey)
        //@constructor
        public GetTokenRequest(String url, String gigya_key) {
            mUrl = url;
            mGigyaKey = gigya_key;
			mMode = GIGYAMODE.GET;
        }
        //generate the query
        public override String generateHttpQuery() {
            return 'grant_type=none';
        }
		//generate the body (empty)
		public override String generateBody() {
			return '';
		}
        //set the specific token in the header for the given query
        public override HttpRequest setToken(HttpRequest iHttpReq) {
        	String key = LMT_Crypto_CLS.decode(mGigyaKey, system.Label.Myr_Gigya_Key);
            String lEncodedAuthent = EncodingUtil.base64Encode(Blob.valueOf(key));
            iHttpReq.setHeader('Authorization', 'Basic ' + lEncodedAuthent);
            system.debug('##### MYR_Gigya_CLS.GetTokenRequest.setToken Authorization=' + 'Basic ' + lEncodedAuthent);
            return iHttpReq;
        }
    } 
    
    /** Abstract class used to centralize the token setting for the main queries **/
    private abstract class AMainRequestGigya extends ARequestGigya {
		
    	//@constructor
    	public AMainRequestGigya(String url, String token, String dataCenterId, GIGYAMODE iMode) {
    		mUrl = url;
    		mToken = token;
    		mDataCenterId = dataCenterId;
			mMode = iMode;
    	}
        //set the token and replace the datacenterid on the fly
        public override HttpRequest setToken(HttpRequest iHttpReq) {
            iHttpReq.setHeader( 'Authorization', 'OAuth ' + mToken );
            String endPoint = iHttpReq.getEndPoint();
            iHttpReq.setEndPoint(endPoint.replace('<Data_Center_ID>', mDataCenterId));
            return iHttpReq;
        }
    }
    
    /** Search Account class for Gigya **/
    @TestVisible private class SearchAccountRequest extends AMainRequestGigya {
        public String mAccountSfdcId;
        //@constructor
        public SearchAccountRequest(String url, String token, String dataCenterId, String accountSfdcId) {
            super(url, token, dataCenterId, GIGYAMODE.GET);
            mAccountSfdcId = accountSfdcId;
        } 
        //generate the query
        public override String generateHttpQuery() {
        	ID sfdcId18char = mAccountSfdcId; 
            String query = '';
            query += 'select UID, isRegistered, isActive, isVerified, profile.firstname, profile.lastname, profile.email, ';
            query += 'loginIDs.username, loginIDs.emails, loginIDs.unverifiedEmails, emails.verified, emails.unverified, ';
            query += 'registered, created, lastLogin, data.accountSfdcActivated, data.newEmail '; 
            query += 'from accounts ';
            query += 'where data.accountSfdcId = \'' + sfdcId18char + '\'';
            return 'query=' + EncodingUtil.urlEncode(query, 'UTF-8'); 
        }
		//generate the body (empty)
		public override String generateBody() {
			return '';
		}
    }
    
    /** General class for all the Gigya functions **/
    public class MainGigyaRequest extends AMainRequestGigya {
        public String mParameter;
		public String mBody;
        //@constructor
        public MainGigyaRequest(String url, String token, String dataCenterId, String parameter, String body, GIGYAMODE iMode) {
        	super(url, token, dataCenterId, iMode);
            mParameter = parameter;
			mBody = body;
        }
        //generate the query
        public override String generateHttpQuery() {
            return mParameter;
        }

		public override String generateBody() {
			return mBody;
		}
    }
 
}