public with sharing class TRVUtils 
{
    public static Id PERSONAL_ID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
    public static Id COMPANY_ID  = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Company_Account_RecType' limit 1].Id;
    public static User u = [Select Id, RecordDefaultCountry__c, UserType__c from user where Id = :UserInfo.getUserId()];
    public static Id createAccountWSPage(TRVResponseWebservices.InfoClient response)
    {
        
        if (response != null)
        {
            //String strURL = URL.getSalesforceBaseUrl().toExternalForm();
            
            //check if account already exists 
                List<Account> accs = new List<Account>();
                List<VEH_Veh__c> lstVehAll = new List<VEH_Veh__c>();
                //List<VEH_Veh__c> lstVehAllAll = new List<VEH_Veh__c>();
                List<VEH_Veh__c> lstVeh = new List<VEH_Veh__c>();
                List<VRE_VehRel__c> lstVehRelation = new List<VRE_VehRel__c>();
                
                Account acct   = new Account();
                VEH_Veh__c vehicle = new VEH_Veh__c(); 
                VRE_VehRel__c vehRelation = new VRE_VehRel__c();
                
                Map<String,VEH_Veh__c> vinnumbers=new Map<String,VEH_Veh__c>();
                Map<String,Id> allvinmap = new Map<String,Id>();
                
                String ePartyID ='';
                
                //prepare ID and request for check if account existing 
                if (String.isEmpty(response.strpartyID) == false && response.datasource =='MDM')
                {
                    ePartyID ='MDM-'+ response.strPartyID;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Tech_ACCExternalID__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Tech_ACCExternalID__c = :ePartyID LIMIT 1];
                }
                else if (String.isEmpty(response.strpartyID) == false && u.RecordDefaultCountry__c == 'Austria' || u.RecordDefaultCountry__c == 'Switzerland')
                {
	                ePartyId = response.strpartyID;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where PeopleId_CN__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where PeopleId_CN__c = :ePartyID LIMIT 1];
	                   
                }
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='BCS')  || (response.DataSourceOrigin=='BCS'))  && response.accountBrand =='RENAULT')
                {
                    ePartyId = response.strpartyID;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id__c = :ePartyID LIMIT 1];
                }
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='BCS')  || (response.DataSourceOrigin=='BCS'))  && response.accountBrand =='DACIA')
                {
                    ePartyId = response.strPartyId;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id_Dacia__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id_Dacia__c = :ePartyID LIMIT 1];
                } 
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='BCS')  || (response.DataSourceOrigin=='BCS')) && response.accountBrand =='NISSAN')
                {
                    ePartyId = response.strPartyId;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id_Nissan__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id_Nissan__c = :ePartyID LIMIT 1];
                } 
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='RBX') || (response.datasource =='SIC')) && response.accountBrand =='RENAULT' && response.DataSourceOrigin=='SIC')
                {
                    ePartyId = response.countryCode2 + response.strPartyId;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id__c = :ePartyID LIMIT 1];
                } 
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='RBX') || (response.datasource =='SIC'))   && response.accountBrand =='DACIA' && response.DataSourceOrigin=='SIC')
                {
                    ePartyId = response.countryCode2 + response.strPartyId;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id_Dacia__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id_Dacia__c = :ePartyID LIMIT 1];
                } 
                else if (String.isEmpty(response.strpartyID) == false && ((response.datasource =='RBX') || (response.datasource =='SIC'))  && response.accountBrand =='NISSAN' && response.DataSourceOrigin=='SIC')
                {
                    ePartyId =response.countryCode2 + response.strPartyId;
                    if((response.strAccountType=='Personal' || response.strAccountType=='P') && String.isEmpty(ePartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Bcs_Id_Dacia__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(ePartyID) == false) //company
                        accs = [Select Id, Name from Account where Bcs_Id_Nissan__c = :ePartyID LIMIT 1];
                } 
                
                if (String.isEmpty(ePartyId) == false)
                    ePartyId = ePartyId.trim();
                    
                
                
                //####################### CREATE ACCOUNT    
                
                //system.debug('##Ludo response type : ' + response.strAccountType);
                //system.debug('##Ludo response strpartyID : ' + response.strPartyID);
                //system.debug('##Ludo string empty ? : ' + String.isEmpty(mdmPartyID));
                
                
            /*  if (response.datasource == 'MDM') 
                    if(response.strAccountType=='Personal' && String.isEmpty(mdmPartyID) == false)
                        accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Tech_ACCExternalID__c = :ePartyID LIMIT 1];
                    else if(String.isEmpty(mdmPartyID) == false) //company
                        accs = [Select Id, Name from Account where Tech_ACCExternalID__c = :mdmPartyID LIMIT 1];
                
                */

                
                if(accs != null && accs.size() != 0) //else new account
                    acct = accs[0];

                system.debug('##Ludo accs : ' + accs);
                system.debug('##Ludo mdmPartyId : ' + ePartyID);    
                system.debug('##Ludo accs size..::'+ accs.size());
                
                acct = mappingAccount(response, acct); 
                
                try
                {
                    if(accs.size() != 0)
                        update acct;
                    if(accs.size() == 0)
                        insert acct;
                }
                catch(DMLException dmlEx)
                {
                    system.debug('DMLException occured While Inserting/Updating the Account :'+ dmlEx.getMessage());
                    return null; 
                }
                catch(Exception ex)
                { 
                  	system.debug('##Ludo Flag passage catch Exception : ' + ex.getMessage());
                    system.debug('Exception occured While Inserting/Updating the Account :'+ ex.getMessage());
                    return null; 
                }

                //####################### CREATE Vehicle of Account
                if (response.vcle == null)
                    return acct.Id;     
                for (TRVResponseWebservices.Vehicle v : response.vcle) //map vehicle 
                {
                    //system.debug('##Ludo list vehicule : ' + response.vcle);
                    vehicle = new VEH_Veh__c(); 
                    vehicle = mappingVehicle(v,vehicle);
                    
                    if (vehicle != null)
                    {
                        system.debug('##Ludo vehicle.name : ' + vehicle.name);
                        system.debug('##Ludo vehicle.name : ' + vehicle);
                        lstVehAll.add(vehicle);
                        vinnumbers.put(vehicle.name,vehicle);
                    }
                }
                    system.debug('##Ludo flag after parse vehicule : ');
                

                for(VEH_Veh__c veh: [Select Id,Name from VEH_Veh__c WHERE Name IN : vinnumbers.keyset()])
                {
                    vinnumbers.remove(veh.Name );
                }
                    
                for(VEH_Veh__c veh : lstVehAll)
                {
                    VEH_Veh__c vh= vinnumbers.get(veh.name);
                    if(vh!=null)
                        lstVeh.add(vh);
                }
                try
                {
                    system.debug('##Ludo test insert vehicle : ' + lstVeh); 
                    insert lstVeh;
                }
                catch(DMLException dmlEx)
                {
                    system.debug('DMLException occured While Inserting/Updating the Vehicle List :'+ dmlEx.getMessage());
                }
                catch(Exception ex) 
                {
                    system.debug('Exception occured While Inserting the Vehicles List :'+ ex.getMessage());
                }
                //system.debug('##Ludo map of vehiles to be inserted-->'+vinnumbers);
                //system.debug('##Ludo CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount():vehList:'+vehList);
                //system.debug('##Ludo  Available VIN in allVehicleSet ##'+ allVehicleSet);
                //system.debug('##Ludo  All VehicleList ##'+ allVehicleList);
                
                //####################### CREATE relation Vehicle
                // Adding all the Vin Number and ID to check in Vehicle Relations
                
                //system.debug('##Ludo  All VehicleList map ##'+ lstVehAllAll);
                //system.debug('##Ludo  All VehicleList ##'+ lstVehAll);
               // for (VEH_Veh__c veh : lstVeh) //map vehicle relation 
                
                list<String> lstVehID = new list<String>(); 
                for (VEH_Veh__c veh : lstVehAll) //retrive id vehicle
                    lstVehID.add(veh.name); 
                list<VEH_Veh__c> lstVehAllAll = [Select id, name from VEH_Veh__c where name in :lstVehID]; 
                list<VRE_VehRel__c> lstVehRelAllAll = [Select id, vin__r.name, account__r.Tech_ACCExternalID__c from VRE_VehRel__c where account__r.Id = :acct.Id]; 
                
                //system.debug('##Ludo test listall  : ' + lstVehAllAll);
                //for (VEH_Veh__c veh : lstVehAllAll) //map vehicle relation 
                //for (VEH_Veh__c veh : lstVeh) //map vehicle relation
                
                Boolean delet = false;
                system.debug('##Ludo  All VehicleList map ## '+ lstVehAllAll);
                system.debug('##Ludo  All VehicleList relation map ## '+ lstVehRelAllAll);
                list<VEH_Veh__c> lstVehFinal = new list<VEH_Veh__c>();
                for (VEH_Veh__c veh : lstVehAllAll)
                {
                    delet = false;
                    for(VRE_VehRel__c vehRel : lstVehRelAllAll)
                    {
                        if(veh.name == vehRel.vin__r.name)
                        {
                            //lstVehAllAll.remove(veh);
                            delet = true;
                        }
                    }
                    if (delet == false)
                    {
                        lstVehFinal.add(veh);
                        delet = true;
                    }
                    
                }
                system.debug('##Ludo  All VehicleList Final : '+ lstVehFinal);
                
                for (VEH_Veh__c veh : lstVehFinal) //map vehicle relation 
                {
                    for (TRVResponseWebservices.Vehicle v : response.vcle) //map vehicle 
                    {
                        if (veh.name == v.vin) //if vehicle list vin is like to vehicle ws 
                        {
                            system.debug('##Ludo test name/vin : ' + veh.name + ' / ' + v.vin);
                            system.debug('##Ludo test v.ID  : ' + veh.Id);
                            system.debug('##Ludo test name/vin brand : ' +  v.brandCode);
                            vehRelation = new VRE_VehRel__c();  
                            vehRelation = mappingVehicleRelations(v,vehRelation,acct,veh); 
                            
                            lstVehRelation.add(vehRelation);
                        } 
                    }
                    
                }
                try
                {
                    system.debug('##Ludo vehrelation : ' + lstVehRelation);
                    insert lstVehRelation;
                }
                catch(DMLException dmlEx)
                {
                    system.debug('DMLException occured While Inserting/Updating the VehicleRelations :'+ dmlEx.getMessage());
                }
                catch(Exception ex) 
                {
                    system.debug('Exception occured While Inserting/Updating the VehicleRelations :'+ ex);
                }
            return acct.Id;                         
        }
        return null; 
    }


    public static list<Account> createAccountWS(list<TRVResponseWebservices.InfoClient> response, boolean isInsert)
    {
        
        if (response.size() > 0)
        {
            //String strURL = URL.getSalesforceBaseUrl().toExternalForm();
            
            list<Account> lstAcc = new list <Account>();
            Account a = new Account(); 
            
            //system.debug('## strURL is..::' + strURL);
            
            for (TRVResponseWebservices.InfoClient r : response) //bulkification
            {
                a = new Account(); 
                a = mappingAccount(r,a); 
                lstAcc.add(a); //insert account to list  
            } 
            if (isInsert)
                insert lstAcc; 
            return lstAcc;
        }
        return null; 
    }

    public static Account mappingAccount(TRVResponseWebservices.InfoClient r, Account a)
    {

        if (r.strAccountType == 'P' || r.strAccountType == 'Personal') //Personnal account
                {
                    a.RecordTypeId = PERSONAL_ID;
                   
                    if (String.isEmpty(r.FirstName1) == false)
                        a.FirstName = r.FirstName1;
                    else 
                        a.FirstName = '.';
                    
                    if (String.isEmpty(r.FirstName2) == false)
                        a.SecondFirstName__c = r.FirstName2; 
                    if (String.isEmpty(r.LastName1) == false)
                        a.LastName = r.LastName1;
                    if (String.isEmpty(r.LastName2) == false) 
                        a.SecondSurname__c = r.LastName2;
                    if (String.isEmpty(r.localCustomerID1) == false) 
                        a.CustomerIdentificationNbr__c = r.localCustomerID1;
                    if (String.isEmpty(r.localCustomerID2) == false)
                        a.CustomerIdentificationNbr2__c  = r.localCustomerID2;
                    
                    if (String.isEmpty(r.PostCommAgreement) == false) 
						a.Address__pc = r.PostCommAgreement;

                    if (r.PostCommAgreementDate != null)
                        a.AddressDate__pc = r.PostCommAgreementDate;
                    
                    if (String.isEmpty(r.TelCommAgreement) == false) 
                      a.PersMobiPhone__pc = r.TelCommAgreement;
                        
                    if (r.TelCommAgreementDate != null) 
                        a.PersMobiPhoneDate__pc = r.TelCommAgreementDate;
                   
                    if (String.isEmpty(r.SMSCommAgreement) == false) 
                        a.SMS__pc = r.SMSCommAgreement;

                    if (r.SMSCommAgreementDate != null) 
                        a.SMSDate__pc= r.SMSCommAgreementDate;
                    
                    if (String.isEmpty(r.EmailCommAgreement) == false) 
                        a.PersEmail__pc = r.EmailCommAgreement;
                       
                    if (r.EmailCommAgreementDate != null) 
                        a.PersEmailDate__pc = r.EmailCommAgreementDate;
                    
                    if (String.isEmpty(r.OccupationalCategoryCodeP) == false)
                        a.JobClass__c = r.OccupationalCategoryCodeP;
                    

                   
                    if (String.isEmpty(r.PersonOtherStreet) == false)
                        a.PersonOtherStreet = r.PersonOtherStreet; 
                    if (String.isEmpty(r.PersonOtherCity) == false)
                        a.PersonOtherCity = r.PersonOtherCity;
                    if (String.isEmpty(r.PersonOtherState) == false)
                        a.PersonOtherState = r.PersonOtherState;
                    if (String.isEmpty(r.PersonOtherPostalCode) == false)
                        a.PersonOtherPostalCode = r.PersonOtherPostalCode;
                    if (String.isEmpty(r.PersonOtherCountry) == false)
                        a.PersonOtherCountry = r.PersonOtherCountry;    
                        
					if(String.isEmpty(r.civility) == false)
						a.Salutation = r.civility; 
                    if (String.isNotBlank(r.strDOB)) 
						a.PersonBirthdate  = Date.valueof(r.strDOB);
					if (String.isEmpty(r.title) == false) 
						a.Title__c = r.Title;
					if (String.isEmpty(r.sex) == false) 
						a.Sex__c= r.sex;
					if (String.isEmpty(r.localCustomerID1) == false) 
						a.CustomerIdentificationNbr__c = r.localCustomerID1;
					if (String.isEmpty(r.localCustomerID2) == false)
						a.CustomerIdentificationNbr2__c = r.localCustomerID2;  
                    
                    
                
                } 
                else 
                {
                    a.RecordTypeId = COMPANY_ID;
                    if (String.isEmpty(r.name) == false) 
                        a.name = r.name;
                    //if (String.isEmpty(r.email) == false )    
                    //  a.ProfEmailAddress__c = r.email;
                    if (String.isEmpty(r.OccupationalCategoryCode) == false)
                        a.Industry = r.OccupationalCategoryCode; 
                    if (String.isEmpty(r.commercialName) == false)
                        a.UsualName__c = r.commercialName;
					if (String.isEmpty(r.companyID) == false)
                      a.CompanyID__c = r.companyID; 
					if (String.isEmpty(r.secondCompanyID) == false)
                      a.Second_account_id__c = r.secondCompanyID; 
                }
                
                if (String.isEmpty(r.email) == false ) 
                        a.PersEmailAddress__c = r.email;
                if (String.isEmpty(r.emailPro) == false ) 
                        a.ProfEmailAddress__c = r.emailPro;
                        

				if (String.isEmpty(r.strMobile) == false && r.strMobile != '') 
					a.PersMobPhone__c = r.strMobile.replace(' ','');
				if (String.isEmpty(r.strFixeLandLinePro) == false && r.strFixeLandLinePro != '') 
					a.ProfLandline__c = r.strFixeLandLinePro.replace(' ','');
				if (String.isEmpty(r.strFixeLandLine) == false) 
                    a.Phone = r.strFixeLandLine.replace(' ','');
                if (String.isEmpty(r.strFixeLandLine) == false) 
                    a.HomePhone__c = r.strFixeLandLine.replace(' ',''); 
				if (String.isEmpty(r.strMobilePro) == false) 
                    a.ProfMobPhone__c = r.strMobilePro.replace(' ','');   
				 

                //a.AccountBrand__c=Label.ACC_MDM; //TODO Ludo check 
                a.AccountBrand__c = r.accountBrand; 
                
                system.debug('##Ludo r.accountBrand mapping : ' + r.accountBrand);
                system.debug('##Ludo r.datasource mapping : ' + r.datasource);
                system.debug('##Ludo r.countryCode2L mapping rbx : ' + r.countryCode2);
                system.debug('##Ludo DataSource origin : ' + r.DataSourceOrigin);
                system.debug('##Ludo DataSource  : ' + r.datasource);
                
                if (String.isEmpty(r.strpartyID) == false && r.datasource =='MDM')
                    a.Tech_ACCExternalID__c = 'MDM-' + r.strPartyID; 
                else if (String.isEmpty(r.strPartyID) == false && u.RecordDefaultCountry__c == 'Austria')
                    a.PeopleId_CN__c = r.strPartyID;   //todo check Ludo
                else if (String.isEmpty(r.strPartyID) == false && u.RecordDefaultCountry__c == 'Switzerland')
                    a.PeopleId_CN__c = r.strPartyID;   //todo check Ludo 
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='BCS') || (r.DataSourceOrigin=='BCS')) && r.accountBrand =='RENAULT')
                    a.Bcs_Id__c = r.strPartyId; 
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='BCS') || (r.DataSourceOrigin=='BCS'))  && r.accountBrand =='DACIA')
                    a.Bcs_Id_Dacia__c = r.strPartyId; 
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='BCS') || (r.DataSourceOrigin=='BCS')) && r.accountBrand =='NISSAN')
                    a.Bcs_Id_Nissan__c = r.strPartyId; 
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='RBX') || (r.datasource =='SIC')) && r.accountBrand =='RENAULT' && r.DataSourceOrigin=='SIC')
                    a.Bcs_Id__c = r.countryCode2 + r.strPartyId;
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='RBX') || (r.datasource =='SIC')) && r.accountBrand =='DACIA'  && r.DataSourceOrigin=='SIC')
                    a.Bcs_Id_Dacia__c = r.countryCode2 + r.strPartyId;
                else if (String.isEmpty(r.strpartyID) == false && ((r.datasource =='RBX') || (r.datasource =='SIC')) && r.accountBrand =='NISSAN' && r.DataSourceOrigin=='SIC')
                    a.Bcs_Id_Nissan__c = r.countryCode2 + r.strPartyId;
                
                system.debug('##Ludo Tech_ACCExternalID__c mapping sortie : ' + a.Tech_ACCExternalID__c);
                system.debug('##Ludo Bcs_Id__c mapping sortie : ' + a.Bcs_Id__c);
                system.debug('##Ludo Bcs_Id_Nissan__c mapping sortie : ' + a.Bcs_Id_Nissan__c);
                system.debug('##Ludo Bcs_Id_Dacia__c mapping sortie : ' + a.Bcs_Id_Dacia__c);
                
                
                if (String.isEmpty(r.partySub) == false)
                    a.Sub__c = r.partySub; 
             
                
                if(r.NumberOfEmployees != null)
                    a.NumberOfEmployees = r.NumberOfEmployees; 
                
                //system.debug('##Ludo : flag passage prefdealer : ' + r.prefDealer);
                //system.debug('##Ludo : flag passage brand account : ' + r.accountBrand);
                if (String.isEmpty(r.prefDealer) == false) /*&& r.accountBrand == 'RENAULT'*/
                    a.PrefdDealer__c = r.prefDealer;
                else if (String.isEmpty(r.prefDealerDacia) == false) /*&& r.accountBrand == 'DACIA'*/
                    a.PrefdDealerDacia__c = r.prefDealerDacia;
                //system.debug('##Ludo : flag passage account 1');
                
                if(!String.isEmpty(r.partySegment))
                    a.SpecialCustmr__c = r.partySegment; 
                
                if(String.isEmpty(r.codeNAF) == false)
                    a.Code_NAF__c = r.codeNAF; 
                    
                if(String.isEmpty(r.financialStatus) == false)
                    a.Financial_Status__c = r.financialStatus; 
                
                if(String.isEmpty(r.legalNature) == false)
                    a.legalForm__c = r.legalNature; 
               
                if (String.isEmpty(r.Lang) == false) 
                    a.Language__c = r.Lang;
                
                if(String.isEmpty(r.marital) == false)
                    a.MaritalStatus__c = r.marital; 
                
                if(String.isEmpty(r.renaultEmployee) == false && (r.renaultEmployee.equalsIgnoreCase('Y')))
                    a.Renault_Employee__c = true; 
                else if (String.isEmpty(r.renaultEmployee) == false && (r.renaultEmployee.equalsIgnoreCase('N')))
                    a.Renault_Employee__c = false;  
               
                if(String.isEmpty(r.nbrChildrenHome) == false)
                    a.nbrChildrenHome__c = r.nbrChildrenHome; 
                
                if(String.isEmpty(r.deceased) == false)
                    if (r.deceased.equalsIgnoreCase('Y'))
                        a.Deceased__c = 'Yes'; 
                    else
                        a.Deceased__c = 'No'; 
                
                if (String.isEmpty(r.GlobalCommAgreement) == false) 
                    a.ComAgreemt__c= r.GlobalCommAgreement;
                
                if (String.isEmpty(r.preferredMedia) == false) 
                    a.PreferedMedia__c= r.preferredMedia;
                
                String CompleteStreet = '';
                

                
                if (String.isEmpty(r.StrNum) == false) 
                    CompleteStreet = r.StrNum;
                if (String.isEmpty(r.strType) == false ) 
                    CompleteStreet = CompleteStreet + '\n' + r.strType;
                if (String.isEmpty(r.addressLine1) == false) 
                    CompleteStreet = CompleteStreet + '\n' + r.addressLine1;
				if (String.isEmpty(r.addressLine2) == false ) 
                    CompleteStreet = CompleteStreet + '\n' + r.addressLine2;
                if (String.isEmpty(r.Compl1) == false) 
                    CompleteStreet = CompleteStreet + '\n' + r.Compl1;
                if (String.isEmpty(r.Compl3) == false ) 
                    CompleteStreet = CompleteStreet + '\n' + r.Compl3;
                

                a.BillingStreet = CompleteStreet;
                
                if (String.isEmpty(r.zip) == false) 
                    a.BillingPostalCode = r.zip;
                if (String.isEmpty(r.City) == false) 
                    a.BillingCity = r.City;
                if (String.isEmpty(r.region) == false) 
                    a.BillingState = r.region;

                if (String.isEmpty(r.stopComFlag) == false) 
                    a.StopComFlag__c = r.stopComFlag;
                if (r.stopComFlagDate != null) 
                    a.StopComDate__c = r.stopComFlagDate;
                if (String.isEmpty(r.stopComSMDFlag) == false) 
                    a.StopComSMDFlag__c = r.stopComSMDFlag;
                if (r.stopComSMDFlagDate != null) 
                    a.StopComSMDDate__c = r.stopComSMDFlagDate;
                    
                if (String.isEmpty(r.StopCommOrigin) == false)
                	a.StopComOrigin__c = r.StopCommOrigin;
                if (String.isEmpty(r.StopCommSubOrigin) == false)
                    a.StopComSubOrigin__c = r.StopCommSubOrigin;
        				
                   
                
                //Country SF    
                /*if (String.isEmpty(r.CountryCode) == false) 
                {
                    a.BillingCountry = r.CountryCode;
                    if (r.CountryCode.equalsIgnoreCase('HR') || r.CountryCode.equalsIgnoreCase('BA')) 
                    {
                       a.Adriatic_country__c='Croatia';
                       a.Language__c='Croatian';
                    }
                    else if (r.CountryCode.equalsIgnoreCase('RS')) 
                    {
                       a.Adriatic_country__c='Serbia';
                       a.Language__c='Serbian'; 
                    }
                    else if (r.CountryCode.equalsIgnoreCase('276') || r.CountryCode.equalsIgnoreCase('705')) 
                    {
                       a.Adriatic_country__c='Slovenia';
                       a.Language__c='Slovene'; 
                    }
                } */
                
                
                if (String.isEmpty(r.CountryCode) == false) 
                    a.BillingCountry = r.CountryCode;
                
                
                
                if (String.isEmpty(r.countrySF) == false)
                { 
                    a.country__c = r.countrySF;
                }
                if (String.isEmpty(r.AdriaticCountrySF) == false) 
                {
                    a.Adriatic_country__c = r.AdriaticCountrySF;
                    a.country__c = r.AdriaticCountrySF;
                }
                if (String.isEmpty(r.MiDCECountrySF) == false) 
                {
                    a.MIDCE_Country__c = r.MiDCECountrySF;
                    a.country__c = r.MiDCECountrySF;
                }
                if (String.isEmpty(r.NRCountrySF) == false) 
                {
                    a.NR_Country__c = r.NRCountrySF;
                    a.country__c = r.NRCountrySF;
                }
                if (String.isEmpty(r.PlCountrySF) == false) 
                { 
                    a.PL_Country__c = r.PlCountrySF;
                    a.country__c = r.PlCountrySF;
                }
                if (String.isEmpty(r.UkIeCountrySF) == false) 
                { 
                    a.UK_IE_country__c = r.UkIeCountrySF;
                    a.country__c = r.UkIeCountrySF;
                }
        
        
            return a;
    }
    public static VEH_Veh__c mappingVehicle(TRVResponseWebservices.Vehicle r, VEH_Veh__c v)
    {
                
        v.Name = r.vin; //required
        
        if (String.isEmpty(r.vinExternal) == false)
            v.Tech_VINExternalID__c = r.vinExternal;
        if (String.isEmpty(r.model) == false)
            v.model__c = r.model;  
        if (String.isEmpty(r.brandCode) == false)
            v.VehicleBrand__c = r.brandCode; 
        if (String.isEmpty(r.modelCode) == false)
            v.Model_Code2__c = r.modelCode;
        if (String.isEmpty(r.modelLabel) == false)
            v.Commercial_Model__c = r.modelLabel; 
        
        if (String.isEmpty(r.versionLabel) == false)
        	v.Version__c = r.versionLabel; 
        
        if(String.isEmpty(r.deliveryDate) == false)
            v.DeliveryDate__c = date.valueOf(r.deliveryDate); 
        
        if(String.isEmpty(r.firstRegistrationDate) == false)
            v.FirstRegistrDate__c = date.parse(r.firstRegistrationDate); 
            
            
        if(String.isEmpty(r.lastRegistrationDate) == false)
            v.LastRegistrDate__c = date.parse(r.lastRegistrationDate); 
        
        if (String.isEmpty(r.registrationNumber) == false)
            v.VehicleRegistrNbr__c = r.registrationNumber; 
        
        if (String.isEmpty(r.brandCode) == false)
            if (r.brandCode.equalsIgnoreCase('Renault') || r.brandCode.equalsIgnoreCase('Dacia')) 
                return v; 
        //TODO ludo try catch go error 
        system.debug('##Ludo test brand : ' + r.brandCode);
        return null; //fail brand  
    }
    public static VRE_VehRel__c mappingVehicleRelations(TRVResponseWebservices.Vehicle r, VRE_VehRel__c vr, Account a, VEH_Veh__c v)
    {
        //Map<String, MDMVehicleRelationType__c> mdmVehicleRelationType= MDMVehicleRelationType__c.getAll();
        //List<String> vehicleRelationsNames = new List<String>();
         
        //vr.VIN__c = r.vin; 
        vr.VIN__c = v.Id;
        vr.Account__c = a.Id; 
        if (String.isEmpty(r.vnvo) == false && (r.vnvo.equals('N') || r.vnvo.equals('VN')))
            vr.VNVO__c = Label.ACC_New;
        else if (String.isEmpty(r.vnvo) == false && (r.vnvo.equals('O') || r.vnvo.equals('VO')))
            vr.VNVO__c = Label.ACC_SecondHand;
        
                
        //vehicleRelationsNames.addAll(mdmVehicleRelationType.keySet());
        //vehicleRelationsNames.sort();
        
        //for (String vehicleName : vehicleRelationsNames) 
        //{
        //    MDMVehicleRelationType__c vehicleRelationValue = mdmVehicleRelationType.get(vehicleName);
        //    if (vehicleRelationValue.Name.equalsIgnoreCase(r.vehicleType)) 
                //vr.TypeRelation__c= vehicleRelationValue.VehicleRelationType__c;
        //}
        if (String.isEmpty(r.vehicleType) == false)
            vr.TypeRelation__c = r.vehicleType;
        
        if (String.isEmpty(r.possessionBegin) == false)
            vr.StartDateRelation__c = date.parse(r.possessionBegin); 
        
        if (String.isEmpty(r.possessionEnd) == false)
            {
            	vr.EndDateRelation__c = date.parse(r.possessionEnd);
            	vr.Status__c = 'Inactive'; 
            }
        else 
        	{
        		vr.Status__c = 'Active';
        	}
        if (String.isEmpty(r.vin) == false)
            return vr;
        return null;
        
    }

}