/** Apex class used to centralize the manage of the field My_Garage_Status
	This class has been initialized with the feature1854/US3654
@author S�bastien Ducamp
@date 12.08.2016
@version 16.10: initial revision - F1854 - US3654
**/

public class Myr_ManageAccount_VehGarageStatus { 

	/** Salesforce Id of the account supervised **/
	private Id mAccountId;
	/** List of the Vehicle Relations to potentially update **/
	private List<ModifRemationsFollowUp> mListRelations;
	/** List of the relations that have been modified **/
	private List<Boolean> mListModified;
	/** List of vehicles linked to the account supervised **/
	private Map<Id, List<VRE_VehRel__c>> mMapsOtherConfirmedRelations;
	

	/** @constructor with Account **/
	public Myr_ManageAccount_VehGarageStatus( String accountId ) {
		mAccountId = accountId;
		List<VRE_VehRel__c> listRelations = [SELECT Id, VIN__r.Name, VIN__c, Account__c, My_Garage_Status__c, VIN__r.VehicleBrand__c
											FROM VRE_VehRel__c WHERE Account__c=:accountId];
		innerConstructor( accountId, listRelations );
	}

	/** Function used by construtor to initialize the variables **/
	private void innerConstructor( String accountId, List<VRE_VehRel__c> listRelations ) {
		mAccountId = accountId;
		mListRelations = new List<ModifRemationsFollowUp>();
		for( VRE_VehRel__c vre : listRelations ) {
			mListRelations.add( new ModifRemationsFollowUp(vre) );
		}
	}

	/** Get the relations between the vehicle and other account than the current one **/
	private void prepareOtherRelations() {
		if( mListRelations == null || (mListRelations != null && mListRelations.size() == 0) ) return;
		//prepare the list of vins
		mMapsOtherConfirmedRelations = new Map<Id, List<VRE_VehRel__c>>();
		for( ModifRemationsFollowUp rel : mListRelations ) {
			if( !mMapsOtherConfirmedRelations.containsKey( rel.relation.VIN__c ) ) {
				mMapsOtherConfirmedRelations.put( rel.relation.VIN__c, new List<VRE_VehRel__c>() );
			}
		}
		//query the confirmed other relations
		//Status is not used directly in the query so that to be sure to used only indexed fields in
		//the WHERE clause
		List<VRE_VehRel__c> listOtherRelations = [SELECT Id, VIN__r.Name, VIN__c, My_Garage_Status__c FROM VRE_VehRel__c 
												  WHERE VIN__c IN :mMapsOtherConfirmedRelations.keySet()
												  AND Account__c <> :mAccountId];
		for( VRE_VehRel__c vre : listOtherRelations ) {
			if( system.Label.Myr_GarageStatus_Confirmed.equalsIgnoreCase( vre.My_Garage_Status__c ) ) {
				mMapsOtherConfirmedRelations.get( vre.VIN__c ).add( vre );
			}
		}
	}

	/** @return true if the current vehicle is already affected, i.e. confirmed, to another account **/
	private Boolean isVinAlreadyLinkedToOtherAccount( Id vinId ) {
		return mMapsOtherConfirmedRelations.get(vinId).size() > 0;
	}

	/** @return true if the current account has a MyRenault/MyDacia account, i.e. Myx_Status__c in Preregistered, Activated, Created **/
	private Boolean hasHeliosAccount(String status) {
		return ( system.Label.Myr_Status_Created.equalsIgnoreCase( status )
				|| system.Label.Myr_Status_Preregistrered.equalsIgnoreCase( status )
				|| system.Label.Myr_Status_Activated.equalsIgnoreCase( status ) );
	}

	/** Update the relations in application of the document EB_SFDC_MyGarageStatus_Improvement_EN-v1.2.docx
		The relations are treated if they are at the unconfirmed or null status.
		THE MODIFICATIONS ARE COMMITTED IN THIS FUNCTION
	**/
	public void setGaragetStatusAndCommit() { 
		system.debug('### Myr_ManageAccount_VehGarageStatus - <setGaragetStatusAndCommit> - BEGIN');
		setGaragetStatus();
		//Prepare the list of relations to really update
		List<VRE_VehRel__c> listRelations = new List<VRE_VehRel__c>();
		for( ModifRemationsFollowUp vre : mListRelations ) {
			if( vre.modified ) {
				listRelations.add( vre.relation );
			}
		}
		//update the database if necessary
		if( listRelations.size() > 0 ) {
			update listRelations;
		}
		system.debug('### Myr_ManageAccount_VehGarageStatus - <setGaragetStatusAndCommit> - END');
	}

	/** Prepare the list of relations and return them **/
	private List<VRE_VehRel__c> listRelations() {
		List<VRE_VehRel__c> listRelations = new List<VRE_VehRel__c>();
		for( ModifRemationsFollowUp vre : mListRelations ) {
			listRelations.add( vre.relation );
		}
		return listRelations;
	}

	/** Update the relations in application of the document EB_SFDC_MyGarageStatus_Improvement_EN-v1.2.docx
		The relations are treated if they are at the unconfirmed or null status
		@return ist<VRE_VehRel__c>, the list of relations modified
		THE MODIFICATIONS ARE NOT COMMITTED IN THIS FUNCTION
		**/
	public List<VRE_VehRel__c> setGaragetStatus() {
		system.debug('### Myr_ManageAccount_VehGarageStatus - <setGaragetStatus> - BEGIN');
		//If the current account has not a myDacia or a MyRenault account, this is useless to continue ....
		Account currentAcc = [SELECT Id, MyR_Status__c, MyD_Status__c FROM Account WHERE Id=:mAccountId];
		Boolean isMyRenault = hasHeliosAccount( currentAcc.MyR_Status__c );
		Boolean isMyDacia = hasHeliosAccount( currentAcc.MyD_Status__c );
		if( !isMyRenault && !isMyDacia ) return listRelations();
		//prepare the other relations potentially linked to the vehicles linked to the supervised accounts
		prepareOtherRelations();
		//Transverse the relations and modify them in application of the feature
		for( ModifRemationsFollowUp vre : mListRelations ) {
			if( String.isBlank( vre.relation.My_Garage_Status__c ) 
				|| ( !String.isBlank( vre.relation.My_Garage_Status__c ) 
						&& system.Label.Myr_GarageStatus_UnConfirmed.equalsIgnoreCase( vre.relation.My_Garage_Status__c ) ) ) 
			{
				if( !isVinAlreadyLinkedToOtherAccount( vre.relation.VIN__c ) ) {
					if( Myr_MyRenaultTools.checkRenaultBrand(vre.relation.VIN__r.VehicleBrand__c) ) {
						system.debug('>>>>> renault vehicle');
						//vehicle is a renault
						if( !isMyRenault && isMyDacia ) {
							vre.relation.My_Garage_Status__c = system.Label.Myr_GarageStatus_UnConfirmed;
						} else {
							vre.relation.My_Garage_Status__c = system.Label.Myr_GarageStatus_Confirmed;
						}
						vre.modified = true;
					} else 	if( Myr_MyRenaultTools.checkDaciaBrand(vre.relation.VIN__r.VehicleBrand__c) ) {
						system.debug('>>>>> dacia vehicle');
						//vehicle is a dacia
						if( isMyRenault && !isMyDacia ) {
							vre.relation.My_Garage_Status__c = system.Label.Myr_GarageStatus_UnConfirmed;
						} else {
							vre.relation.My_Garage_Status__c = system.Label.Myr_GarageStatus_Confirmed;
						}
						vre.modified = true;
					}
					//cases already treated above: 
					//   - if the vehicle is already affected to customer A, set MyGarageStatus to 
					//     “confirmed” if different from “taken” or “rejected”
					//	 - if the vehicle is already affected to another customer, do not touch anything
				}
			}
		} //end of the loop for( VRE_VehRel__c vre : mListRelations ) {
		system.debug('### Myr_ManageAccount_VehGarageStatus - <setGaragetStatus> - END');
		//Prepare the response
		return listRelations();
	}


	// --------- INNER CLASS ------------
	// the goal of this class is to update only the modified relations and not the other ones ....
	public class ModifRemationsFollowUp {
		public VRE_VehRel__c relation;
		public Boolean modified;
		//@inner constructor
		public ModifRemationsFollowUp( VRE_VehRel__c vre ) {
			relation = vre;
			modified = false;
		}
	}

}