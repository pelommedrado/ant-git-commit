@isTest
public class OpportunityTransactionTest {
    
    public static testMethod void testaMudaStage1(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account a = moc.criaPersAccount();
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Identified';
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        insert opp;
        
        opp.StageName = 'In Attendence';
        update opp;        
        
        test.stopTest();
        
    }
    
    public static testMethod void testaMudaStage2(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account a = moc.criaPersAccount();
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'In Attendence';
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        insert opp;
        
        TST_Transaction__c tst = new TST_Transaction__c();
        tst.Opportunity__c = opp.id;
        insert tst;
        
        opp.StageName = 'Test Drive';
        update opp;        
        
        test.stopTest();
        
    }
    
    public static testMethod void testaMudaDelivered(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account a = moc.criaPersAccount();
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Identified';
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        insert opp;
        
        VEH_Veh__c veh = moc.criaVeiculo();
        insert veh;
        
        Pricebook2 pc2 = moc.criaStdPricebook2();
        upsert pc2;
        
        Product2 pd2 = moc.criaProduct2();
        insert pd2;
        
        PricebookEntry pb = moc.criaPricebookEntry();
        pb.Pricebook2Id = pc2.id;
        pb.Product2Id = pd2.id;
        insert pb;
        
        Quote q = moc.criaQuote();
        q.Pricebook2Id = pc2.Id;
        q.OpportunityId = opp.Id;
        insert q;
        
        QuoteLineItem ql = moc.criaQuoteLineItem();
        ql.PricebookEntryId = pb.Id;
        ql.QuoteId = q.id;
        ql.Vehicle__c = veh.Id;
        ql.Quantity = 1;
        ql.UnitPrice = 35000;
        insert ql;
        
        veh.DeliveryDate__c = System.today();
        update veh;
        
        
        test.stopTest();
        
    }
    
    public static testMethod void testaMudaLicense(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account a = moc.criaPersAccount();
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'In Attendence';
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        insert opp;
        
        TST_Transaction__c tst = new TST_Transaction__c();
        tst.Opportunity__c = opp.id;
        insert tst;
        
        VEH_Veh__c veh = moc.criaVeiculo();
        insert veh;
        
        Pricebook2 pc2 = moc.criaStdPricebook2();
        upsert pc2;
        
        Product2 pd2 = moc.criaProduct2();
        insert pd2;
        
        PricebookEntry pb = moc.criaPricebookEntry();
        pb.Pricebook2Id = pc2.id;
        pb.Product2Id = pd2.id;
        insert pb;
        
        Quote q = moc.criaQuote();
        q.Pricebook2Id = pc2.Id;
        q.OpportunityId = opp.Id;
        insert q;
        
        QuoteLineItem ql = moc.criaQuoteLineItem();
        ql.PricebookEntryId = pb.Id;
        ql.QuoteId = q.id;
        ql.Vehicle__c = veh.Id;
        ql.Quantity = 1;
        ql.UnitPrice = 35000;
        insert ql;
        
        veh.VehicleRegistrNbr__c = '876254';
        update veh;
        
        
        test.stopTest();
        
    }
    
    public static testMethod void test1(){
        
        Test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account a = moc.criaPersAccount();
        insert a;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'In Attendence';
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        opp.CreatedDate = System.today();
        insert opp;
        
        VEH_Veh__c veh = moc.criaVeiculo();
        Insert veh;
        
        List<VEH_Veh__c> veiList = new List<VEH_Veh__c>();
        veiList.add(veh);
        
        TST_Transaction__c tst = new TST_Transaction__c();
        Insert tst;
        
        Map<Id, TST_Transaction__c> tstOld = new Map<Id, TST_Transaction__c>();
        tstOld.put(tst.Id, tst);
        
        //OpportunityTransaction oppt = new OpportunityTransaction();
        
        OpportunityTransaction.createInAttendance(opp, opp);
        OpportunityTransaction.createAfterInAttendance(opp, opp);
        OpportunityTransaction.createTransactionFromVeh(veiList, veiList);
        OpportunityTransaction.setTransactionFromVehicle(veh, veh, opp, tstOld);
        
        Test.stopTest();
        
    }

}