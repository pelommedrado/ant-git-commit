public with sharing class VFC128_CampaignMemberDAO
{
	private static final VFC128_CampaignMemberDAO instance = new VFC128_CampaignMemberDAO();
    
	private VFC128_CampaignMemberDAO(){}

    public static VFC128_CampaignMemberDAO getInstance(){
        return instance;
    }

    
	/**
     * Return a CampaignMember record given the campaign Id and lead Id
     */
    public CampaignMember fetchLeadCampaignMemberOfThisCampaign(Id campaingId, Id leadId)
    {
    	try
    	{
    		CampaignMember cm = [SELECT Id, CampaignId, Status, Source__c, Transferred__c, LeadId
								   FROM CampaignMember
								  WHERE CampaignId = :campaingId
								    AND LeadId = :leadId
							   order by CreatedDate desc
								  limit 1];
    		return cm;
    	}
    	catch (Exception e) {
    		return null;
    	}
    }
    
    /**
     * Return a CampaignMember record given the campaign Id and contact Id
     */
    public CampaignMember fetchContactCampaignMemberOfThisCampaign(Id campaingId, Id contactId)
    {
    	try
    	{
    		CampaignMember cm = [SELECT Id, CampaignId, Status, Source__c, Transferred__c, ContactId
								   FROM CampaignMember
								  WHERE CampaignId = :campaingId
								    AND ContactId = :contactId
							   order by CreatedDate desc
								  limit 1];
    		return cm;
    	}
    	catch (Exception e) {
    		return null;
    	}
    }
    
    /**
     * Fetch campaign for heating lead picklist: 4th rule
     */
    public List<CampaignMember> fetchLeadCampaignMemberFor4thRule(Set<Id> setCampaignId, Id leadId)
    {
    	List<CampaignMember> lstCampaignMember = 
    		[SELECT Id, CampaignId, campaign.Name
			   FROM CampaignMember
			  WHERE CampaignId IN :setCampaignId
			    AND LeadId = :leadId];
    	return lstCampaignMember;
    }
    
    /**
     * Fetch campaign for heating account picklist: 4th rule
     */
    public List<CampaignMember> fetchContactCampaignMemberFor4thRule(Set<Id> setCampaignId, Id contactId)
    {
    	List<CampaignMember> lstCampaignMember = 
    		[SELECT Id, CampaignId, campaign.Name
			   FROM CampaignMember
			  WHERE CampaignId IN :setCampaignId
			    AND ContactId = :contactId];
    	return lstCampaignMember;
    }
    
    /**
     * Check for a lead/contact if he belongs to another campaignMember for another 
     * brother campaign (same parent campaign)
     */
    public Boolean checkIftCampaignMemberShouldBeTransferred(Id campaignParentId, Id campaignId, Id leadId, Id contactId)
	{
	    List<CampaignMember> lstCampaignMember = null;
	    
	    if (leadId != null) {
	    	lstCampaignMember = [SELECT Id, Campaign.Id, Campaign.ParentId, LeadId, ContactId, Transferred__c 
							       FROM CampaignMember
							      WHERE Transferred__c = false
							        AND Campaign.ParentId = :campaignParentId
							        AND Campaign.Id <> :campaignId
					 			    AND LeadId = :leadId];
	    }
	    else {
	    	lstCampaignMember = [SELECT Id, Campaign.Id, Campaign.ParentId, LeadId, ContactId, Transferred__c 
							       FROM CampaignMember
							      WHERE Transferred__c = false
							        AND Campaign.ParentId = :campaignParentId
							        AND Campaign.Id <> :campaignId
					 			    AND ContactId = :contactId];
	    }

		if (lstCampaignMember.isEmpty())
			return false;
		else
			return true;
	}
	
	/**
	 * Return a set of CM's given a set of campaign's Ids and another set of lead's Ids.
	 */
	public Set<String> fetchCMByLeadIdAndCampaignId(Set<Id> setCampaignId, Set<Id>setLeadId)
	{
		Set<String> setCM = new Set<String>();

		List<CampaignMember> lstCampaignMember = 
			[SELECT Id, CampaignId, LeadId 
		       FROM CampaignMember
		      WHERE Campaign.Id IN :setCampaignId
			    AND LeadId IN :setLeadId];
		
		for (CampaignMember cm : lstCampaignMember) {
			setCM.add(cm.CampaignId + '-' + cm.LeadId);
		}

		return setCM;
	}
}