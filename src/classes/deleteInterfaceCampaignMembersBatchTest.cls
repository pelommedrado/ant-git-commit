@isTest
private class deleteInterfaceCampaignMembersBatchTest {
    
    static testmethod void test() {
        
        // The query used by the batch job.
        String query = 'SELECT Id FROM InterfaceCampaignMembers__c';
        
        // Create some test CampaignMember and lead items to be delete by the batch job.
        List<InterfaceCampaignMembers__c> icmList = new List<InterfaceCampaignMembers__c>();
        
        for (Integer i=0;i<10;i++) {
            InterfaceCampaignMembers__c icm = new InterfaceCampaignMembers__c();          
            icmList.add(icm);
        }
        insert icmList;
        
        Test.startTest();
        deleteInterfaceCampaignMembersBatch batch = new deleteInterfaceCampaignMembersBatch(query);
        Database.executeBatch(batch);
        Test.stopTest();
        
        // Verifica se os Leads foram excluidos
        Integer i = [SELECT COUNT() FROM InterfaceCampaignMembers__c];
        System.assertEquals(0, i);
    }
}