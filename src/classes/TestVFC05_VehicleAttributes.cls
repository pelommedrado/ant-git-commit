@isTest
public class TestVFC05_VehicleAttributes {

    static User getEnvSetup(){
        // Get the context of a user
          Profile p = [SELECT Id FROM Profile WHERE Name='CCBIC - Agent Front Office']; 
          User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
          LocaleSidKey='fr', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com',
          Country = 'GRIDADMINISTRATOR');

         VFC05_LanguageMapping__c mycs = VFC05_LanguageMapping__c.getValues('Short_Code__c');
          
          if(mycs == null) {
            mycs = new VFC05_LanguageMapping__c(Name= 'French');
            mycs.Short_Code__c = 'FR';
            insert mycs;
          }
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            system.debug('Userlanguage: ' + u.LanguageLocaleKey);
          return u;
    }

static testMethod void testVehicleAttributes(){
    
        // Set up the envioronment 
        User u = getEnvSetup();
     //   User u = getEnvSetup();
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Visit the page so we also run the constructor code
        PageReference pageRef = Page.VFP05_VehicleAttributes;
        Test.setCurrentPage(pageRef);
    
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
      Test.startTest();          
        

      System.runAs(u) {

            // Test the methods in the VehicleAttributes class in test mode.
            vehController.WarrantyDataSource.Test = true;
            vehController.MaintDataSource.Test = true;
            vehController.OTSDataSource.Test = true;
            vehController.ContractDataSource.Test = true;
            vehController.WarrantyHistoryDataSource.Test = true;
            vehController.WarrantyHistoryDetailDataSource.Test = true;

            vehController.getWarrantyData();
            // Test it populated wpList
            System.assert(vehController.wpList.size() > 0);
            vehController.getOTSData();
            System.assert(vehController.otsPropList.size() > 0);
            vehController.getMaintenanceData();
            // Test the maint title has something in it, this also tests findMaintLabel()
            System.assert(vehController.maint.MaintTitle1 != null);
            vehController.getContractData();
            System.assert(vehController.contracts.size() > 0);
            vehController.getWarrantyHistoryData();
            System.assert(vehController.whList.size() > 0);
            vehController.getWarrantyHistoryDetailData();
            System.assert(vehController.whd.PandO.size() > 0);

            // Test the methods in the VehicleAttributes class not in test mode which will try to make the WS call.
            vehController.WarrantyDataSource.Test = false;
            vehController.MaintDataSource.Test = false;
            vehController.OTSDataSource.Test = false;
            vehController.ContractDataSource.Test = false;
            vehController.WarrantyHistoryDataSource.Test = false;
            vehController.WarrantyHistoryDetailDataSource.Test = false;
            
            vehController.getWarrantyData();
            // Test it populated wpList
            System.assert(vehController.wpList.size() > 0);
            vehController.getOTSData();
            System.assert(vehController.otsPropList.size() > 0);
            vehController.getMaintenanceData();
            // Test the maint title has something in it, this also tests findMaintLabel()
            System.assert(vehController.maint.MaintTitle1 != null);
            vehController.getContractData();
            System.assert(vehController.contracts.size() > 0);
            vehController.getWarrantyHistoryData();
            System.assert(vehController.whList.size() > 0);
            vehController.getWarrantyHistoryDetailData();
            System.assert(vehController.whd.PandO.size() > 0);
            
            // test showDetail property;
            system.assertNotEquals(vehController.showDetail, null);
            vehController.showDetail = 2;
            system.assertEquals(vehController.showDetail, 2);   
        
    }
   Test.stopTest();
 }
 
    static testMethod void testUtils_OTSDataSource(){
    
        // Set up the envioronment
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse results;
        Test.startTest();
           // Test the methods in the VehicleAttributes class
            vehController.OTSDataSource.Test = true;
            //results = vehController.OTSDataSource.getOTSData(VehController);
             vehController.getOTSData();
            vehController.OTSDataSource.Test = false;
            results = vehController.OTSDataSource.getOTSData(VehController);
            
        Test.stopTest();
    }
    
     static testMethod void testUtils_OTSDataSourceWS(){
    
        WS02_otsIcmApvBserviceRenault.ApvGetListOts sample = new WS02_otsIcmApvBserviceRenault.ApvGetListOts();
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse test = new WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse();
        WS02_otsIcmApvBserviceRenault.OtsInfoMsg msg = new WS02_otsIcmApvBserviceRenault.OtsInfoMsg();
        
        WS01_ApvGetDetVehXml.DetVehCritere crit = new WS01_ApvGetDetVehXml.DetVehCritere();
    }
 
    static testMethod void testUtils_MaintDataSource(){

        // Set up the envioronment
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse results;
        Test.startTest();
            // Test the methods in the VehicleAttributes class
            vehController.MaintDataSource.Test = true;
            results = vehController.MaintDataSource.getMaintenanceData(VehController);
            vehController.MaintDataSource.Test = false;
            results = vehController.MaintDataSource.getMaintenanceData(VehController);
            
        Test.stopTest();
    }
   
    static testMethod void testUtils_WarrantyDataSource(){

        // Set up the envioronment
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS06_servicesBcsDfr.ListDetWarrantyCheck results;
        Test.startTest();
            // Test the methods in the VehicleAttributes class
            vehController.WarrantyDataSource.Test = true;
            results = vehController.WarrantyDataSource.getWarrantyData(VehController);
            vehController.WarrantyDataSource.Test = false;
            results = vehController.WarrantyDataSource.getWarrantyData(VehController);
            
        Test.stopTest();
    }
    
    static testMethod void testUtils_ContractDataSource(){

        // Set up the envioronment
        User u = getEnvSetup();
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS04_CustdataCrmBserviceRenault.GetCustDataResponse results;
        Test.startTest();
        System.Runas(u){
            // Test the methods in the VehicleAttributes class
            vehController.ContractDataSource.Test = true;
            system.debug(u.Name);
            results = vehController.ContractDataSource.getContractData(VehController);
            vehController.ContractDataSource.Test = false;
            results = vehController.ContractDataSource.getContractData(VehController);
        }    
        Test.stopTest();
    }
    
        static testMethod void testUtils_WarrantyHistoryDataSource(){

        // Set up the envioronment
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS05_ApvGetDonIran1.ApvGetDonIran1Response results;
        Test.startTest();
            // Test the methods in the VehicleAttributes class
            vehController.WarrantyHistoryDataSource.Test = true;
            results = vehController.WarrantyHistoryDataSource.getWarrantyHistoryData(VehController);
            vehController.WarrantyHistoryDataSource.Test = false;
            results = vehController.WarrantyHistoryDataSource.getWarrantyHistoryData(VehController);
            
        Test.stopTest();
    }
    
    static testMethod void testUtils_WarrantyHistoryDetailDataSource(){

        // Set up the envioronment
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response results;
        Test.startTest();
            // Test the methods in the VehicleAttributes class
            vehController.WarrantyHistoryDetailDataSource.Test = true;
            vehController.WarrantyHistoryDetailDataSource.modulo = 'ABC';
            vehController.WarrantyHistoryDetailDataSource.numInt = 'ABC';
            results = vehController.WarrantyHistoryDetailDataSource.getWarrantyHistoryDetailData(VehController);
            vehController.WarrantyHistoryDetailDataSource.Test = false;
            results = vehController.WarrantyHistoryDetailDataSource.getWarrantyHistoryDetailData(VehController);
            
        Test.stopTest();
    }

    static testMethod void testWS02_otsIcmApvBserviceRenault(){
        
        Test.startTest();
            WS02_otsIcmApvBserviceRenault.SiUnavailableException e = new WS02_otsIcmApvBserviceRenault.SiUnavailableException();
            WS02_otsIcmApvBserviceRenault.NoDataFoundException e1 = new WS02_otsIcmApvBserviceRenault.NoDataFoundException();
            WS02_otsIcmApvBserviceRenault.BadRequestException e2 = new WS02_otsIcmApvBserviceRenault.BadRequestException();
            WS02_otsIcmApvBserviceRenault.SiErrorException e3 = new WS02_otsIcmApvBserviceRenault.SiErrorException();
        Test.stopTest();
    }
   
    static testMethod void testWS03_fullRepApvBserviceRenault(){
        
        Test.startTest();
            WS03_fullRepApvBserviceRenault.SiUnavailableException e = new WS03_fullRepApvBserviceRenault.SiUnavailableException();
            WS03_fullRepApvBserviceRenault.NoDataFoundException e1 = new WS03_fullRepApvBserviceRenault.NoDataFoundException();
            WS03_fullRepApvBserviceRenault.BadRequestException e2 = new WS03_fullRepApvBserviceRenault.BadRequestException();
            WS03_fullRepApvBserviceRenault.SiErrorException e3 = new WS03_fullRepApvBserviceRenault.SiErrorException();
            
            // No idea what all the rest of these classes do, assume not needed.
            WS03_fullRepApvBserviceRenault.PgmEntPrec e4 = new WS03_fullRepApvBserviceRenault.PgmEntPrec();
            WS03_fullRepApvBserviceRenault.PgmEntInfoSpecial e5 = new WS03_fullRepApvBserviceRenault.PgmEntInfoSpecial();
            WS03_fullRepApvBserviceRenault.PgmEntCapMoy e6 = new WS03_fullRepApvBserviceRenault.PgmEntCapMoy();
            WS03_fullRepApvBserviceRenault.PgmEntRefro e7 = new WS03_fullRepApvBserviceRenault.PgmEntRefro ();
            WS03_fullRepApvBserviceRenault.PgmEntFrein e8 = new WS03_fullRepApvBserviceRenault.PgmEntFrein();
            WS03_fullRepApvBserviceRenault.PgmEntHuile e9 = new WS03_fullRepApvBserviceRenault.PgmEntHuile();
            WS03_fullRepApvBserviceRenault.PgmEntRev e10 = new WS03_fullRepApvBserviceRenault.PgmEntRev();
        Test.stopTest();
    }
 
  
    
    static testMethod void testWS04_CustdataCrmBserviceRenault(){
        
        Test.startTest();
            WS04_CustdataCrmBserviceRenault.Survey e = new WS04_CustdataCrmBserviceRenault.Survey();
            WS04_CustdataCrmBserviceRenault.Contact e1 = new WS04_CustdataCrmBserviceRenault.Contact();
            WS04_CustdataCrmBserviceRenault.LogsCustData e2 = new WS04_CustdataCrmBserviceRenault.LogsCustData();
            WS04_CustdataCrmBserviceRenault.GetCustDataError e3 = new WS04_CustdataCrmBserviceRenault.GetCustDataError();
            WS04_CustdataCrmBserviceRenault.Dealer e4 = new WS04_CustdataCrmBserviceRenault.Dealer();
            WS04_CustdataCrmBserviceRenault.Desc_x e5 = new WS04_CustdataCrmBserviceRenault.Desc_x();
            WS04_CustdataCrmBserviceRenault.wsInfos e6 = new WS04_CustdataCrmBserviceRenault.wsInfos();
            WS04_CustdataCrmBserviceRenault.WorkShop e7 = new WS04_CustdataCrmBserviceRenault.WorkShop();
           
                         
        Test.stopTest();
    }
    
      public String getCountryInfo()
    {
    return 'PTB';
    } 
    
    
        static testMethod void testWS05getArchives(){

        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        
        vehController.toParse='<test><SerialNumber2>123</SerialNumber2></test>';
        
         Test.setMock(WebServiceMock.class, new Test_WS_RcArch_WebServiceMock());
       
        Test.startTest();

        vehController.IdCase='123';
        vehController.getArchives();
        vehController.getCaseDetailXML();
        vehController.getCaseDetails();
        vehController.getAttachements();
        vehController.getAttachementData();
        PageReference FinalPage=vehController.OpenXMLPage();
        System.assertEquals('VF1BGRG0633285766',vehController.getVin());
        Test.stopTest();
    }
 
}