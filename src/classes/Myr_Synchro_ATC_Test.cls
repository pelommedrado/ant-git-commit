/*****************************************************************************************
 Name    : Myr_Synchro_ATC_Test                               Creation date : 10 July 2015
 Desc    : Synchronizations from Salesforce towards CSDB web services 
 Author  : Donatien Veron (AtoS)
 Project : myRenault
******************************************************************************************
25 March 2016 : Additional tests dedicated to common methods Get_Basic_Authent & End_Point_To_Call 
******************************************************************************************/
@isTest
public class Myr_Synchro_ATC_Test{ 
    
    @testsetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('UK', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('UK', null, true) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, true) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    }
    
    private static final Set<String> objectFields = Schema.SObjectType.User.fields.getMap().keySet();
    
    private static User applyPatchWFBrazil(User usr) {
        //PATCH SDP due to a new production workflow !!!
        // => bypass the workflow to avoid a MIXED_DML_ACTION
                if( MYR_Datasets_Test.isBrazilOrg() ) {
                    system.debug('### Myr_Datasets_Test  - <insertPersonalAccounts> - brazil org');
                    if(objectFields.contains('iscac__c')) {
                        usr.put('iscac__c', true);
                        system.debug('### Myr_Datasets_Test  - <insertPersonalAccounts> - is CAC :'+  usr.get('iscac__c'));
                    }
                }
                //PATCH SDP due to a new production workflow !!!
        return usr;
    }
    
    static testMethod void test010()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Update_On_Account_ChangeEmail');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
    
    static testMethod void test020()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_KO));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Update_On_Account_ChangeEmail');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
   
    static testMethod void test030()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_KO));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Delete_On_Account');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(1, ls.size());
    }
    
    static testMethod void test040()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Activation_On_Account');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(1, ls.size());
    }
    
    static testMethod void test050()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUserWithRole('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
		Account acc = new Account();
		system.runAs( techUser ) {
			acc = Myr_Datasets_Test.insertPersonalAccountAndUser('my firstname', 'my lastname', '89900', 'donatien.veron@atos.net.test050wxc', 'firstname.lastname@atos.net', '', 'USER_ACTIF', '', 'UK', 'Rennes', 'my street', '022355' ,'');
			acc.MyR_Status__c = 'Activated'; 
			update acc;
		} 
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault', rlinkEligibility__c='Y');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=acc.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
            Myr_Synchro_ATC.Change_Myr_Status(acc.Id, 'Activation_On_Account');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(2, ls.size());
    }
   
    static testMethod void test060()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.Change_Email(a1.Id, 'donatien.veron@atos.net', 'donatien.veron@atos.net.end');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
    
    
    static testMethod void test070() 
    {
        String Compile_Body = Myr_Synchro_ATC.Compile_Body('Update_On_Account_ChangeEmail:new.new@atos.net', 'old.old@atos.net', 'UK', 'the Lastname', 'the Firstname', 'Mr', '02298333', '06298333', 'Street', 'Postal Code', 'City');
        system.assertEquals('<customer><email>old.old@atos.net</email><newEmail>new.new@atos.net</newEmail><portalCountryCode>UK</portalCountryCode><lastName>the Lastname</lastName><civility>1</civility><firstName>the Firstname</firstName><homePhone>02298333</homePhone><mobilePhone>06298333</mobilePhone><address>Street</address><zipCode>Postal Code</zipCode><city>City</city><optInATCProfile>00000</optInATCProfile><type>HELIOS</type></customer>', Compile_Body);
    }
    
    //Test Add_VIN
    static testMethod void test080()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;
        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName, country__c from Account where FirstName = 'my firstname' ];
        
        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rheliosDDDD_' + a1.PersEmailAddress__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.PersEmailAddress__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId; 
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;
        
        List<User> checklistu = [select Id from User where LastName ='my lastname' ];
        system.assertEquals(1,checklistu.size());       
        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id, Name, Model__c from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);  
        insert lr;
        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c]; 
        
        Test.startTest();
        system.runAs( techUser ) {
            Myr_Synchro_ATC.VIN_Call_Result vin_Result = new Myr_Synchro_ATC.VIN_Call_Result(); 
            Myr_Synchro_ATC.Add_VIN(a1.PersEmailAddress__c, Country_Info__c.getInstance(a1.country__c).Country_Code_2L_ISO__c, v.Model__c, 'https://' + CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint__c + 'csdb/vin/' + v.Name, a1.Id, v.Id, vr.Id, 'Update_On_Account_ChangeEmail', 'PUT', 200, 'POST', vin_Result);
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(1, ls.size());
    }
    
    
    static testMethod void test100()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUserWithRole('Italy');
        Id Account_Id = null;
        Id Vehicle_Id = null;
        Id Relation_Vehicle_Id = null;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street'
			, Billingcountry='England', Country__c='Italy'
            , PersLandline__c='022355', BillingPostalCode='89900', MyRenaultID__c='dev.test@atos.net', MyR_Status__c='Activated');
        la.add(a1);

        system.runAs( techUser ) {
            insert la;
            a1 = [select Id, PersonContactId, MyRenaultID__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        }

        List<User> lu =new List<User>();
        User u1 = new User();
        u1.Username = 'rhelios_' + a1.MyRenaultID__c;
        u1.firstname = a1.firstname;
        u1.lastname = a1.lastname;
        u1.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(a1.firstname, a1.lastname);
        u1.isActive = true;
        u1.Email = a1.MyRenaultID__c;
        u1.Alias = a1.FirstName.left(1) + a1.LastName.left(3); 
        u1.TimeZoneSidKey = 'GMT';
        u1.LocaleSidKey = 'fr_FR_EURO';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'fr';
        u1.RecordDefaultCountry__c='France';
        u1.ProfileId = Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id;
        u1.ContactId = a1.PersonContactId;
        u1 = applyPatchWFBrazil(u1);
        lu.add(u1);
        insert lu;

        system.runAs( techUser ) {
            List<User> checklistu = [select Id from User where LastName ='my lastname' ];
            system.assertEquals(1,checklistu.size());
        
            VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
            insert v;
            v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
            List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
            VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
            lr.add(r1);
            insert lr;
            VRE_VehRel__c vr = [select Id from VRE_VehRel__c];
         
            Test.startTest();
            Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Update_On_Account');
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Account__c, Action_On_Object__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Relation_Vehicle__c, Status__c, VIN__r.Name, VIN_Http_Method__c, VIN_Question__c, VIN_Response__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        
        system.assertEquals(1, ls.size());
        system.assertEquals('Update_On_Account', ls[0].Action_On_Object__c);
        system.assertEquals('PUT', ls[0].Customer_Http_Method__c);
        system.assertEquals('<customer><email>dev.test@atos.net</email><portalCountryCode>'+Country_Info__c.getInstance('Italy').Country_Code_2L_ISO__c+'</portalCountryCode><lastName>my lastname</lastName><civility></civility><firstName>my firstname</firstName><homePhone></homePhone><mobilePhone></mobilePhone><address>my street</address><zipCode>89900</zipCode><city>Rennes</city><optInATCProfile>00000</optInATCProfile><type>HELIOS</type></customer>', ls[0].Customer_Question__c);
        system.assertEquals('VF17R5A0H48447489',ls[0].VIN__r.Name);   
    }

	static testMethod void test110()
    {
		String Basic_Authent = Myr_Synchro_ATC.Get_Basic_Authent();
        if ([select IsSandbox from Organization].IsSandbox){
			system.assertEquals('Basic ' + EncodingUtil.base64Encode(Blob.valueOf(CS04_MYR_Settings__c.getInstance().Myr_ATC_Basic_Authorization_NoProd__c)), Basic_Authent);
		}else{
			system.assertEquals('Basic ' + EncodingUtil.base64Encode(Blob.valueOf(CS04_MYR_Settings__c.getInstance().Myr_ATC_Basic_Authorization__c)), Basic_Authent);
		}
    }
	
	

	static testMethod void test120()  
    {
		String Url_Returned = Myr_Synchro_ATC.End_Point_To_Call('DELETE_MODE');
		if ([select IsSandbox from Organization].IsSandbox){
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint_NoProd__c+'csdb/vins/',Url_Returned);
		}else{
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint__c+'csdb/vins/',Url_Returned);
		}

		Url_Returned = Myr_Synchro_ATC.End_Point_To_Call('CUSTOMER');
		if ([select IsSandbox from Organization].IsSandbox){
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint_NoProd__c+'csdb/customer/',Url_Returned);
		}else{
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint__c+'csdb/customer/',Url_Returned);
		}

		Url_Returned = Myr_Synchro_ATC.End_Point_To_Call('VIN');
		if ([select IsSandbox from Organization].IsSandbox){
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint_NoProd__c+'csdb/customer/vin/',Url_Returned);
		}else{
			system.assertEquals('https://'+CS04_MYR_Settings__c.getInstance().Myr_ATC_EndPoint__c+'csdb/customer/vin/',Url_Returned);
		}
    }

	static testMethod void test130(){
        System.assertEquals( '', Myr_Synchro_ATC.Conversion_Civility(''));
        System.assertEquals('1', Myr_Synchro_ATC.Conversion_Civility('Mr'));
        System.assertEquals('2', Myr_Synchro_ATC.Conversion_Civility('Ms'));
		System.assertEquals('2', Myr_Synchro_ATC.Conversion_Civility('Miss'));
		System.assertEquals('2', Myr_Synchro_ATC.Conversion_Civility('Mrs'));  
		System.assertEquals('1', Myr_Synchro_ATC.Conversion_Civility('Exotic_Salutation_Digitale'));
    }

	// TEST the ATC Synchro 'Delete_On_Vehicle' 
	static testMethod void test140()
    {
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');       
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_KO));   
		
		// ADD ACCOUNT     
        List<Account> la =new List<Account>();
        Account a1 = new Account(RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType()
            , FirstName = 'my firstname', LastName ='my lastname', BillingCity='Rennes', BillingStreet='my street', Billingcountry='England', Country__c='UK'
            , PersLandline__c='022355', BillingPostalCode='89900', PersEmailAddress__c='donatien.veron@atos.net', MyR_Status__c='Activated');
        la.add(a1);
        insert la;        
        a1 = [select Id, PersonContactId, PersEmailAddress__c, FirstName, LastName from Account where FirstName = 'my firstname' ];
        
		// ADD VEHICULE        
        VEH_Veh__c v  = new VEH_Veh__c (Name='VF17R5A0H48447489', VehicleRegistrNbr__c='1234', VehicleBrand__c='Renault');
        insert v;
        v = [select Id from VEH_Veh__c where Name='VF17R5A0H48447489'];
        
		// ADD VEHICULE RELATION 
        List<VRE_VehRel__c> lr =new List<VRE_VehRel__c>();
        VRE_VehRel__c r1 = new VRE_VehRel__c(vin__c=v.Id, account__c=a1.Id, My_Garage_Status__c='confirmed');
        lr.add(r1);
        insert lr;        
        VRE_VehRel__c vr = [select Id from VRE_VehRel__c];

		// TEST THE SYNCHRO ATC
		List<Synchro_ATC__c> ls1 = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls1.size());
        
        Test.startTest();
        system.runAs( techUser ) {
                Myr_Synchro_ATC.send_Request_Create_Customer(a1.Id, v.Id,vr.Id, 'Delete_On_Vehicule');
        }
        Test.stopTest();
        
        List<Synchro_ATC__c> ls = [select Account__c, VIN__c, Relation_Vehicle__c, Action_On_Object__c from Synchro_ATC__c];
	    Synchro_ATC__c s0 = ls[0];
		system.assertEquals(1, ls.size());
	    system.assertEquals(a1.Id, s0.Account__c);
		system.assertEquals(v.Id, s0.VIN__c);
		system.assertEquals(vr.Id, s0.Relation_Vehicle__c);
		system.assertEquals('Delete_On_Vehicule', s0.Action_On_Object__c);	
    }
}