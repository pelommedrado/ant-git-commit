@isTest
private class TestVFC11_SearchVehicleFromAccountToCase {
    
	static testMethod void VFC11_SearchVehicleFromAccountToCase() {
        
    	Test.startTest();
	    PageReference PageRef = Page.success;
        
	    VFC11_SearchVehicleFromAccountToCase svfatc = new VFC11_SearchVehicleFromAccountToCase();
	    WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse ws = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
	    WS01_ApvGetDetVehXml.DetVeh d = new WS01_ApvGetDetVehXml.DetVeh();
        d.tvv 				= 'tvv';
        d.libCarrosserie 	= 'libCarrosserie';
        d.dateTcmFab		= '23/02/1987';
        d.dateLiv			= '23/02/1987';
        d.indMot			= 'indMot';
        d.NMot				= 'NMot';
        d.typeMot			= 'typeMot';
        d.indBoi			= 'indBoi';
        d.NBoi				= 'NBoi';
        d.typeBoi			= 'typeBoi';
        d.libModel			= 'libModel';
        d.marqCom			= 'marqCom';
        d.NFab				= 'NFab';
        d.version			= 'version';
        
	    ws.detVeh = d;
	    
        svfatc.VIN = 'VIN';
        svfatc.VRN = 'VRN';
        
	    PageRef = svfatc.searchVehicle();
	    pageRef = svfatc.createVehicle(ws);
	    ws = svfatc.searchBVM();
        
        PageReference pRef = svfatc.selectVehicle();
        PageReference pRef1 = svfatc.skip();
        PageReference pRef2 = svfatc.cancel();
        
	    Test.stopTest();	    
    }    
}