/**
 * Class "copied" from VFC48_QualifyingAccountBusinessDelegate but using without sharing rule.
 * Only the needed methods were copied.
 */ 
public without sharing class VFC133_QualifyingAccountBOWOS
{
	private static final VFC133_QualifyingAccountBOWOS instance = new VFC133_QualifyingAccountBOWOS();
	
	private VFC133_QualifyingAccountBOWOS() {}
	  
	public static VFC133_QualifyingAccountBOWOS getInstance() {
		return instance;
	}
	
	
	/**
	 * Method used by VFP06_QualifyingAccount page to fetch Opportunities and Campaigns records.
	 * It will call an existing method but enforcing the rule "without sharing" to bypass
	 * the organization sharing for the object Campaign.
	 * All methods called further, must not declare with or without sharing, so the configuration here,
	 * in this class, will be used.
	 */
	public VFC46_QualifyingAccountVO getAccountByOpportunityId(String opportunityId)
	{
		return  VFC48_QualifyingAccountBusinessDelegate.getInstance().getAccountByOpportunityId(opportunityId);
	}
}