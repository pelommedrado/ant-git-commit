public with sharing class VFC137_ZipCodeService
{
	/**
	 * Returns a zipCodeBase object given a zip code number.
	 */
	public static ZipCodeBase__c getAddressGivenZipCode(String zipCodeNumber)
	{	
		if (zipCodeNumber != null && zipCodeNumber != '') {
			ZipCodeBase__c zipCode = VFC136_ZipCodeDAO.getInstance().searchZipCodeByNumber(zipCodeNumber);
			return zipCode;
		}
		else
			return null;
	} 
}