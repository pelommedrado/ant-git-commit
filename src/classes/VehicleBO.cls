public with sharing class VehicleBO {

    public static final String SAP_SOURCE = 'SAP';

    private static VehicleBO classInstance;

    static {
      classInstance = new VehicleBO();
    }

    private VehicleBO() {}

    public static VehicleBO getInstance() {
      return classInstance;
    }

    public void actionCancelAll(final List<VEH_Veh__c> vehicleList, final Map<Id, VEH_Veh__c> oldMap) {
        System.debug('VehicleBO.actionCancelAll() vehicleList: ' + vehicleList.size() );
        final List<Id> vehicleCancelList = new List<Id>();
        for(VEH_Veh__c vehicle: vehicleList) {
            final VEH_Veh__c old = oldMap.get(vehicle.Id);
            if(isChangeStatus(vehicle, old) && isStatus(old, 'Billed')) {
              vehicleCancelList.add(vehicle.Id);
            }
        }
        if(vehicleCancelList.isEmpty()) {
          return;
        }

        System.debug('update vehicleCancelList: ' + vehicleCancelList);
        final List<Quote> quoteBilledList = [
            SELECT Id, Status, OpportunityId FROM Quote
            WHERE Id IN (SELECT QuoteId FROM QuoteLineItem WHERE Vehicle__c IN: vehicleCancelList)
            AND Status = 'Billed'
        ];
        if(quoteBilledList.isEmpty()) {
          return;
        }
        final List<Opportunity> oppQuoteList = new List<Opportunity>();
        for(Quote quote : quoteBilledList) {
            quote.Status = 'Canceled';
            oppQuoteList.add(new Opportunity(Id=quote.OpportunityId, StageName= 'Lost'));
        }

        Savepoint sp1 = Database.setSavepoint();

        try {
            System.debug('update quoteBilledList: ' + quoteBilledList);
            Database.update(quoteBilledList);

            System.debug('update oppQuoteList: ' + oppQuoteList);
            Database.update(oppQuoteList);
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            Database.rollback(sp1);

        }
    }

    private Boolean isChangeStatus(VEH_Veh__c vehicle, VEH_Veh__c old) {
      return vehicle.Status__c != old.Status__c;
    }

    private Boolean isStatus(VEH_Veh__c vehicle, String status) {
      if(String.isEmpty(vehicle.Status__c)) {
        return false;
      }
      return vehicle.Status__c.equals(status);
    }

    public static Boolean isInterfaceSAP(final List<VEH_Veh__c> vehicleList) {
      return SAP_SOURCE.equalsIgnoreCase(vehicleList.get(0).VehicleSource__c);
    }

}