@isTest
private class TestAP01Case 
{
	static testMethod void AP01Case_TEST() 
	{
    	list<Case> listeCases = new list<Case>();
    	Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        
     	Account Acc2 = new Account(Name='Test1',Phone='1000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
     	insert Acc2;    
     	Contact Con = new Contact(FirstName='Test FirstName', LastName='Test Contact',Salutation='Mr.',ProEmail__c ='lro@lro.com',AccountId=Acc2.Id);
     	insert Con;
      
    	list<VEH_Veh__c> vehiculesToUpdate1 = new list<VEH_Veh__c>();
    	VEH_Veh__c Vehicule = new VEH_Veh__c (Name = '21342312323123456',  VehicleBrand__c = 'Renault', KmCheck__c=100 , KmCheckDate__c = date.today());
   
    	insert Vehicule;

    	Case Case1 = new Case  (Origin='Mobile',Type='Information Request',SubType__c= 'Booking', Status='Open',Priority='Urgent',
                        		Description='Description', From__c='Customer',Kilometer__c =1000000, AccountId=Acc2.Id, ContactId=Con.Id, 
                        		VIN__c = Vehicule.Id); 
                        
    	Case Case2  = new Case  (Origin='Mobile',Type='Information Request',Status='Open',Priority='Urgent',
                        		 Description='Description', From__c='Customer',Kilometer__c =10000000,SubType__c= 'Booking', ContactId=Con.Id, AccountId=Acc2.Id, 
                        		 VIN__c = Vehicule.Id);
                        
       	Test.startTest();
       
       	insert Case1;
       	insert Case2;
       	Case2.Kilometer__c = 2000000000;
       	Update(Case2);
                      
        Test.stopTest();
	}
}