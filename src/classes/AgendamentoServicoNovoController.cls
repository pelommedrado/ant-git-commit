public class AgendamentoServicoNovoController {
    
    public Account acc { get;set; }
    public Opportunity opp { get;set; }
    
    public VEH_Veh__c veiculo       { get;set; }
    public VRE_VehRel__c relVeiculo { get;set; }
    
    public List<SelectOption> tipoRel       { get; set; }
    
    public String textSearch        { get;set; }
    public String tipoRelText       { get;set; }
    
    public AgendamentoServicoNovoController() {
        
        String accId = 
            ApexPages.currentPage().getParameters().get('Id');
        
        this.acc = [
            SELECT Id, isPersonAccount, LastName, 
            FirstName, PersonHomePhone, PersMobPhone__c,
            CustomerIdentificationNbr__c,PersonEmail ,IDBIR__c
            FROM Account
            WHERE Id =: accId
        ];
        
        this.opp = new Opportunity();
        this.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'Service');
        
        this.tipoRel = new List<SelectOption>();
        this.tipoRel.add(new SelectOption('Owner', 'Proprietário'));
        this.tipoRel.add(new SelectOption('User', 'Usuário'));
    }
    
    private void buscarVeiculo(String texto) {
        if(!String.isEmpty(texto)) {
            List<VEH_Veh__c> relList = [
                SELECT Id, VehicleRegistrNbr__c, Name, Model__c, Version__c
                FROM VEH_Veh__c 
                WHERE VehicleRegistrNbr__c =: texto OR Name =: texto
                LIMIT 1
            ];
            
            if(!relList.isEmpty()) {
                this.veiculo = relList.get(0);    
                this.opp.VehicleRegistrNbr__c = veiculo.VehicleRegistrNbr__c;
                
            }
            
            this.relVeiculo = new VRE_VehRel__c();
        }
    }
    
    public PageReference buscarVeiculo() {
        buscarVeiculo(textSearch);
        return null;
    }
    
    public PageReference cancelar() {
        PageReference pg = new PageReference('/' + acc.Id);
        return pg;
    }
    
    public PageReference salvar() {
        if(!validar()) {
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            
            Lead lead = new Lead();
            lead.OwnerId = UserInfo.getUserId();
            lead.Status             = 'Identified';
            lead.FirstName          = acc.FirstName;
            lead.LastName           = acc.LastName;
            lead.CPF_CNPJ__c        = acc.CustomerIdentificationNbr__c;
            lead.Email              = acc.PersonEmail;
            lead.Home_Phone_Web__c  = acc.PersonHomePhone;
            lead.Mobile_Phone__c    = acc.PersMobPhone__c;
            
            lead.BIR_Dealer_of_Interest__c = acc.IDBIR__c;
            lead.CurrencyIsoCode    = 'BRL';
            lead.LeadSource         = 'CSC';
            lead.SubSource__c       = 'RECEPTIVE PHONE';
            lead.Detail__c          = 'AFTER SALES';
            lead.RecordTypeId       = Utils.getRecordTypeId('Lead', 'Service');
            lead.VehicleRegistrNbr__c = opp.VehicleRegistrNbr__c;
            
            lead.ServiceType__c             = opp.ServiceType__c;
            lead.ProposalScheduleDate__c    = opp.ProposalScheduleDate__c;
            lead.ProposalScheduleHour__c    = opp.ProposalScheduleHour__c;
            lead.ScheduledPeriod__c         = opp.ScheduledPeriod__c;
            lead.Description                = opp.Description;
            
            System.debug('Lead: ' + lead);
            
            Account accBd = [
                SELECT Id, CustomerIdentificationNbr__c
                FROM Account
                WHERE Id =: acc.Id
            ];
            
            if(accBd.CustomerIdentificationNbr__c == null ) {
                accBd.PersonHomePhone = lead.Home_Phone_Web__c;
                accBd.PersMobPhone__c = lead.Mobile_Phone__c;
                accBd.CustomerIdentificationNbr__c = lead.CPF_CNPJ__c;
                accBd.PersonEmail = lead.Email;
                update accBd;
            }
            
            Database.insert(lead);
            
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead, null);
            
            Opportunity op = [
                SELECT Id
                FROM Opportunity 
                WHERE LeadId__c =: lead.Id
            ];
            
            //opp.Dealer__c = ac.Id;
            opp.Id = op.Id;
            
            update opp;
            
            System.debug('Opp: ' + opp);
            
            upsert acc;
            
            if(relVeiculo == null || relVeiculo.VIN__r.Id != veiculo.Id) {
                
                VRE_VehRel__c tem =  [
                    SELECT Id, TypeRelation__c, VIN__r.Id 
                    FROM VRE_VehRel__c
                    WHERE VIN__r.Id =: this.veiculo.Id AND Account__c =: acc.Id
                ];
                
                if(tem == null) {
                    relVeiculo = new VRE_VehRel__c();
                    relVeiculo.VIN__c = veiculo.Id;
                    relVeiculo.Account__c = acc.Id;
                } else {
                    relVeiculo = tem;
                    
                }
                
                opp.VehicleRegistrNbr__c = veiculo.VehicleRegistrNbr__c;
            }
            
            relVeiculo.TypeRelation__c = tipoRelText;
            
            opp.StageName = 'Pre Scheduled';
            upsert opp;
            upsert relVeiculo;
            
            PageReference pg = new PageReference('/' + opp.Id);
            return pg;
            
        } catch(Exception ex) {
            Database.rollback(sp);
            
            System.debug('Ex: ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
            return null;
        }
        
        return null; 
    }
    
    private Boolean validar() {
        if(String.isBlank(acc.FirstName) || String.isBlank(acc.LastName)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar o Nome é o Sobrenome'));
            return false;  
        }
        
        if(String.isBlank(acc.CustomerIdentificationNbr__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar o CPF'));
            return false; 
        }
        
        if(String.isNotBlank(acc.CustomerIdentificationNbr__c)) {
            if(!Sfa2Utils.isCpfValido(acc.CustomerIdentificationNbr__c) ) {
                Apexpages.addMessage(new Apexpages.Message( 
                    ApexPages.Severity.ERROR, 
                    'É necessário informado é invalido CPF'));
                
                return false; 
            }
        }
        
        if(String.isBlank(acc.PersonEmail)  && 
           String.isBlank(acc.PersMobPhone__c )     && 
           String.isBlank(acc.PersonHomePhone ) ) {
               ApexPages.addMessage(
                   new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar Telefone ou Celular ou Email'));
               
               return false; 
           }
        
        if(opp.ProposalScheduleDate__c== null) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar \'Data Solicitação\''));
            return false;  
        }
        
        if(String.isEmpty(opp.ProposalScheduleHour__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar \'Hora Solicitação\''));
            return false; 
        }
        
        if(String.isEmpty(opp.ScheduledPeriod__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar \'Periodo\''));
            return false; 
        }
        
        
        if(String.isEmpty(opp.Dealer__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar \'Concessionária\''));
            return false; 
        }
        
        
        if(String.isEmpty(opp.ServiceType__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar \'Tipo Serviço\''));
            return false; 
        }
        
        if(veiculo == null) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'O veículo informado não existe em nossa base. Insira-o para seguir com o atendimento.'));
            return false; 
        }
        
        if(String.isEmpty(tipoRelText)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 
                                      'É necessário selecionar \'Tipo Relacionamento\'.'));
            return false;
        }
        return true;
    }
    
}