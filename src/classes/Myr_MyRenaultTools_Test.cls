/** Apex class used to test Myr_CustMessage_Cls 
    
    @author S. Ducamp
    @version 1.0
    @date 10.02.2016
    
	These functions should be added later in this test class
	 - buildSelectAllQuery : covered today by Myr_CreateCustMessage_Cls_Test
	 - replaceAccentsAndVowels : covered today by Myr_ManageAccount_MatchingKeys_Test
 */
@isTest 
private class Myr_MyRenaultTools_Test { 

	static testMethod void test_authorizedBrand() {
		system.assertEquals( true, Myr_MyRenaultTools.authorizedBrand('Renault') );
		system.assertEquals( true,  Myr_MyRenaultTools.authorizedBrand('Dacia' ));
		system.assertEquals( true,  Myr_MyRenaultTools.authorizedBrand('ReNault' ));
		system.assertEquals( true,  Myr_MyRenaultTools.authorizedBrand('DaciA' ));
		system.assertEquals( false,  Myr_MyRenaultTools.authorizedBrand('Renault '));
		system.assertEquals( false,  Myr_MyRenaultTools.authorizedBrand('Truc'));
	}

	static testMethod void test_removeSpaces() {
		system.AssertEquals('trucchose', Myr_MyRenaultTools.removeAllSpaces('  truc  chose     '));
		system.AssertEquals('bidule', Myr_MyRenaultTools.removeAllSpaces('b    i du      l         e'));
	}

	static testMethod void test_checkDacia() {
		system.AssertEquals(true, Myr_MyRenaultTools.checkDaciaBrand('DaCiA'));
		system.AssertEquals(false, Myr_MyRenaultTools.checkDaciaBrand(' DaCiA'));
		system.AssertEquals(false, Myr_MyRenaultTools.checkDaciaBrand('Renault'));
	}

	static testMethod void test_checkRenault() {
		system.AssertEquals(true, Myr_MyRenaultTools.checkRenaultBrand('ReNaUlt'));
		system.AssertEquals(false, Myr_MyRenaultTools.checkRenaultBrand('Renault '));
		system.AssertEquals(false, Myr_MyRenaultTools.checkRenaultBrand('Dacia'));
	}

	static testMethod void test_idField() {
		system.AssertEquals('myrenaultid__c', Myr_MyRenaultTools.getIdField('ReNaUlt'));
		system.AssertEquals('', Myr_MyRenaultTools.getIdField('Renault '));
		system.AssertEquals('mydaciaid__c', Myr_MyRenaultTools.getIdField('Dacia'));
		system.AssertEquals('', Myr_MyRenaultTools.getIdField('Dacia '));
		system.AssertEquals('mydaciaid__c', Myr_MyRenaultTools.getIdField('DaCiA'));
	}

	static testMethod void test_statusField() {
		system.AssertEquals('myr_status__c', Myr_MyRenaultTools.getStatusField('ReNaUlt'));
		system.AssertEquals('', Myr_MyRenaultTools.getStatusField('Renault '));
		system.AssertEquals('myd_status__c', Myr_MyRenaultTools.getStatusField('Dacia'));
		system.AssertEquals('', Myr_MyRenaultTools.getStatusField('Dacia '));
		system.AssertEquals('myd_status__c', Myr_MyRenaultTools.getStatusField('DaCiA'));
	}

	//Test the email pattern
	static testmethod void checkEmailPattern() {
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test.test.test'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test@test@test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test@test.fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test@test.test.test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('32@test.fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('32@32.fr'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('32@32'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test.Fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test.Fr.com'));
	}

	//test select all
	static testMethod void testSelectAll() {
		Myr_MyRenaultTools.SelectAll selectAccount = Myr_MyRenaultTools.buildSelectAllQuery(Account.class.getName(), null);
		system.assertNotEquals( null, selectAccount );
		system.assertEquals( 'Account', selectAccount.sObjectName );
		system.assertEquals( true, selectAccount.Query.containsIgnoreCase('FROM Account') );
		system.assertEquals( true, selectAccount.Fields.size() > 0 );

		Myr_MyRenaultTools.SelectAll selectAccountMyr = Myr_MyRenaultTools.buildSelectAllQuery(Account.class.getName(), 'Myr');
		system.assertNotEquals( null, selectAccountMyr );
		system.assertEquals( 'Account', selectAccountMyr.sObjectName );
		system.assertEquals( true, selectAccountMyr.Query.containsIgnoreCase('FROM Account') );
		system.assertEquals( true, selectAccountMyr.Fields.size() > 0 );
		system.assertEquals( true, selectAccountMyr.Fields.size() < selectAccount.Fields.size() );

		Myr_MyRenaultTools.SelectAll selectVeh= Myr_MyRenaultTools.buildSelectAllQuery(VEH_Veh__c.class.getName(), null);
		system.assertNotEquals( null, selectVeh );
		system.assertEquals( 'VEH_Veh__c', selectVeh.sObjectName );
		system.assertEquals( true, selectVeh.Query.containsIgnoreCase('FROM VEH_Veh__c') );
		system.assertEquals( true, selectVeh.Fields.size() > 0 );

		Myr_MyRenaultTools.SelectAll selectTruc= Myr_MyRenaultTools.buildSelectAllQuery('Truc', null);
		system.assertNotEquals( null, selectTruc );
		system.assertEquals( 'Truc', selectTruc.sObjectName );
		system.assertEquals( null, selectTruc.Query );
		system.assertEquals( 0, selectTruc.Fields.size() );
	}
}