/*  Purge of Synchro_ATC
*************************************************************************************************************
25 August 2014  : Creation
*************************************************************************************************************/
@isTest
private class Myr_Batch_SynchroATC_Purge_BAT_TEST {
  @testSetup static void setCustomSettings() {
      Myr_Datasets_Test.prepareRequiredCustomSettings();
      Myr_Datasets_Test.prepareCustomerMessageSettings();
      List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts( 5, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
    }
 
  
    static testMethod void test_010 () {
    //--BEFORE PURGE
    Synchro_ATC__c s  = new Synchro_ATC__c(Action_On_Object__c='Update_On_Account', Customer_Http_Method__c='POST', Customer_Question__c='the question', Customer_Response__c=null, Customer_Response_Http_Status_Code__c  =null, Logger__c=null, Relation_Vehicle__c=null, Status__c='OK', VIN__c=null, VIN_Http_Method__c='POST', VIN_Question__c=null, VIN_Response__c=null, VIN_Response_Http_Status_Code__c=null);
    insert s;
    List<Synchro_ATC__c> ls = [ SELECT Id FROM Synchro_ATC__c ];
        System.assertEquals(1, ls.size());    
        
        
        CS04_MYR_Settings__c setting = [select Myr_ATC_Batch_KO__c, Myr_ATC_Batch_OK__c from CS04_MYR_Settings__c];
      setting.Myr_ATC_Batch_KO__c='0';
      setting.Myr_ATC_Batch_OK__c='0';
      update setting;
            
        //---LAUNCH THE PURGE
        Myr_Batch_SynchroATC_Purge_BAT batch = new Myr_Batch_SynchroATC_Purge_BAT();
        Test.startTest();
        Database.executeBatch(batch, 200);
        Test.stopTest();
        
        //---CONTROL AFTER THE PURGE
        ls = [ SELECT Id FROM Synchro_ATC__c ];
    System.assertEquals(0, ls.size());
  }
}