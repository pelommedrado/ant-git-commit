public class WSC08_RetailPlatform_QueryWrapper {

    public static List< Offer__c > queryOffers(Datetime lastUpdateTime) {
        final List<String> userBlackList = Label.WSC08_RetailPlatform_BlackList.split('@');
        return [
            select Id,
            Dealer_Offer__c,
            Legal_Text_VU__c,
            Legal_Text_VU_2__c,
            Legal_Text_VU_Manual__c,
            Stage__c,
            Model__r.Name,
            Offer_Upgrade__r.Stage__c,
            Model__r.Model_Spec_Code__c,
            Offer_Start_Date_Website__c,
            Offer_End_Date_Website__c,
            Version__r.Version_Id_Spec_Code__c,
            Model__r.Id_Milesime__c,
            Number_Of_Installments__c,
            Monthly_Tax__c,
            Value_From__c,
            ValueTo__c,
            From_To_Offer__c,
            Entry_Value__c,
            Installment_Value__c,
            Featured_in_Campaign__r.Code__c,
            Featured_in_Offer_Lookup__r.Code__c,
            Offer_Header__r.Code__c,
            Total_Inventory_Vehicle__c,
            Pricing_Template__c,
            Featured_In_Offer__c,
            Optional__r.Name,
            Painting__r.Name,
            Featured_Product_Text__c,
            Stamp__c,
            Version__r.FeaturedPicture__c,
            hasUpgrade__c,
            Type_Of_Upgrade__c,
            Offer_Upgrade_Description__c,
            Group_Offer__r.Type_of_Offer__c,
            Mercadoria__r.Name,
            Legal_Text__c,
            Status__c,
            Pricing_Template_Lookup__r.Code__c,
            Stamp_Lookup__r.Code__c,
            Stamp_Lookup_2__r.Code__c,
            Created_by_Dealer__c,
            National_Offer__c, ParcelaDia__c, ativarParcelaDia__c,
            Version__r.Milesime__r.Milesime__c,
            Version__r.Comfort__c, Version__r.Confort_Description__c,
            Version__r.Performance__c, Version__r.Performance_Description__c,
            Version__r.Power__c, Version__r.Power_Description__c,
            Version__r.Security__c, Version__r.Security_Description__c,
            Version__r.Technology__c,Offer_Upgrade__r.Status__c,
            Version__r.Technology_Description__c, Version__r.Volume__c,
            Version__r.Description_Volume__c, Version__r.Name, Offer_Upgrade__c, (
              select Dealer__c, Dealer__r.Name
              from Dealers_Offers__r where Status__c = 'Active'
            )
            from Offer__c
            where LastModifiedDate > :lastUpdateTime
            and Offer_To_Internet__c = true
            and Stage__c = 'Approved'
            and LastModifiedById NOT IN: userBlackList
            //and Status__c = 'ACTIVE'
        ];
    }

  public static List< Account > queryDealers(Datetime lastUpdateTime){
    return [
      select Id, Name, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry, Status__c, IDBIR__c, Latitude__c, Longitude__c, Phone
      from Account
      where RecordType.DeveloperName = 'Network_Site_Acc'
        and Country__c = 'Brazil'
        and LastModifiedDate > :lastUpdateTime
        and Active_PV__c = true
    ];
  }
}