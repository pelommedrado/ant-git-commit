/**
* @author RNTBCI(Sumanth Yeluri)
* @date 16/07/2015
* @description This class contains the functionalies for Rforce on atttachment
*/
public with sharing class Rforce_AttachmentUtils_CLS{
/**
* @author RNTBCI(Sumanth Yeluri)
* @date 16/07/2015
* @description Activity Management on Attachments:Task and emails are sent when an attachment is Insert by Dealer or SRC user for the cases created by dealer 
* @param list of attachments 
* @param current user details 
*/
    public static void activitymanagementonattachment(List<Attachment> lattachmentlist,User u){
        //Create a task to SRC user when a attachment is inserted by dealer or IKR User
        Set<Id> scaseid =new Set<Id>();
        Set<Id> groupid =new Set<Id>();
        List<Case> lcaselist=new List<Case>();
        List<Case> lgroupcaselist=new List<Case>();
        List<Task> lSRCtasklist=new List<Task>();
        String IKRUserId = System.Label.IKRUserId;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Will excute only for community dealers and IKR users 
        if(u.Profile.Name==system.label.USR_Id_CoreServiceProfile || u.Id==IKRUserId ){
            //Getting list of attachments on case
            for(Attachment att : lattachmentlist){
                if(att.ParentId.getSobjectType() == Case.SobjectType){
                    scaseid.add(att.ParentId);    
                }
            }
            //Creating task only if the case is owned by user and email owned by group
            system.debug('-->scaseid'+scaseid);
            if(scaseid!=null){
                try{
                    lcaselist=[Select Id,OwnerId,ContactId,Casenumber,CountryCase__c from Case where Id IN: scaseid];
                    system.debug('-->lcaselist'+lcaselist);
                    for(Case c : lcaselist){
                        String ownerid=c.OwnerId;
                        system.debug('-->c'+c);
                        if(ownerid.SubString(0,3)!='00G'){
                            Task t=new Task(WhoId=c.ContactId,IsReminderSet = true,WhatId=c.id,Type=system.label.TSK_Type_Attachment_Dealer_to_SRC,Subject=system.label.TSK_Type_Attachment_Dealer_to_SRC,ActivityDate=system.today(),Priority=system.label.TSK_Priority_Normal,Status=system.label.TSK_Status_ToDo,ReminderDateTime=system.now().addHours(2),OwnerId=c.OwnerId);                                              
                            lSRCtasklist.add(t);
                        } 
                        else if(ownerid.SubString(0,3)=='00G'){
                            groupid.add(c.OwnerId);
                            lgroupcaselist.add(c); 
                        }   
                    }
                    if(lgroupcaselist!=null){
                        Map<Id,Group> mcasegroup = new Map<Id,Group>([Select Id,Email from Group where Id IN : groupid]);
                        system.debug('mcasegroup -->'+mcasegroup );
                        for(Case c : lgroupcaselist){
                            String queueemail=mcasegroup.get(c.OwnerId).Email;
                            system.debug('queueemail-->'+queueemail);
                            if(queueemail!=null){
                                List<String> EmailList = new List<String>();
                                EmailList.add(queueemail);
                                mail.setToAddresses(EmailList);
                                mail.setSubject(system.label.ATT_NewAttachmentonCase + c.CaseNumber);
                                mail.setUseSignature(false);
                                string msg = system.label.ATT_NewAttachmentonCase + c.CaseNumber +' '+system.label.ATT_NewAttachmentBy+' '+ userInfo.getName();
                               /* if(c.CountryCase__c==system.label.Acc_FRCountry){
                                    msg = 'Une nouvelle pièce jointe a été ajoutée au dossier :' + c.CaseNumber + ' par ' + userInfo.getName();
                                    mail.setSubject('Nouvelle pièce jointe ajoutée au dossier :' + c.CaseNumber);
                                }*/
                                mail.setHtmlBody(msg);
                                // Send the email
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                                  
                            }                         
                        }
                    }
                    insert lSRCtasklist; 
                    system.debug('-->lSRCtasklist'+lSRCtasklist);                        
                }catch(Exception e){
                    system.debug(e);
                }
            }    
        }
        //Send an email to Dealer when a attachment is inserted by SRC agent
        //Getting the list of attachment on cases
        if(u.Profile.Name!=system.label.USR_Id_CoreServiceProfile){
            for(Attachment att : lattachmentlist){
                if(att.ParentId.getSobjectType() == Case.SobjectType){
                    scaseid.add(att.ParentId); 
                }
            }
            //Sending email to dealer contact on case
            if(scaseid!=null){
                try{
                    lcaselist=[Select Id,OwnerId,ContactId,EmailContactDealer__c,CreatedById,DealerContactLN__c,CaseNumber from Case where Id IN: scaseid];
                    for(Case c : lcaselist){
                        String ownerid=c.OwnerId;
                        if(c.EmailContactDealer__c!=null){
                            List<String> EmailList = new List<String>();
                            EmailList.add(c.EmailContactDealer__c);
                            mail.setToAddresses(EmailList);
                            mail.setSubject(system.label.ATT_NewAttachmentonCase + c.CaseNumber);
                            mail.setUseSignature(false);
                            string msg = system.label.ATT_NewAttachmentonCase + c.CaseNumber +' '+system.label.ATT_NewAttachmentBy +' '+userInfo.getName();
                            mail.setHtmlBody(msg);
                            // Send the email
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                                    
                        }    
                    }                        
                }catch(Exception e){
                    system.debug(e);
                }
            }    
        }                
    } 
/**
* @author RNTBCI(Sumanth Yeluri)
* @date 16/07/2015
* @description When email with multiple attachments is sent by customer on a closed case a new related case is created and
               these attachment are attached to the related case.
* @param list of attachments 
* @param current user details 
*/
    public static void attachmenton_email_for_a_closedcase(List<Attachment> lattachmentlist){        
        Map<Id,EmailMessage> memgidemg = new Map<Id,EmailMessage>();
        List<Case> lcaselist= new List<Case>();
        List<Attachment> lemailattachments = new List<Attachment>();        
        List<EmailMessage> lemailmessages = new List<EmailMessage>();
        Set<Id> sattachmentparentid= new Set<Id>();
        Set<Id> semailparentid= new Set<Id>();
        System.debug('Attachment Size-->'+lattachmentlist.size());
        //Get the list of attachments on EmailMessges
        for(Attachment at: lattachmentlist) {
            String prntid=at.ParentId;
            if(prntid.SubString(0,3)=='02s'){
                lemailattachments.add(at);
                sattachmentparentid.add(at.parentid);
            }
        }
        system.debug('lemailattachments-->##'+lemailattachments.size()); 
        system.debug('sattachmentparentid-->##'+sattachmentparentid);        
        if(sattachmentparentid!=null){
            try{
               //Checking whether the parent of email message is case
                lemailmessages=[Select ParentId from EmailMessage where ID IN : sattachmentparentid];   
                if(lemailmessages!=null){
                    for(Emailmessage eg : lemailmessages){
                        if(eg.ParentId.getSobjectType() == Case.SobjectType){
                            semailparentid.add(eg.ParentId);
                            memgidemg.put(eg.Id,eg);    
                        }
                    }
                    system.debug('memgidemg-->##'+memgidemg);
                    //Quering the cases which are closed
                    Map<Id,Case> mCases= new Map<Id,Case> ([Select Id,Status,(Select Id from Cases order by CreatedDate DESC) from Case where Id IN : semailparentid AND Status='Closed']);
                    system.debug('mCases-->##'+mCases);
                    if(mCases!=null){
                        for(Attachment a  : lemailattachments){
                            Emailmessage e=memgidemg.get(a.ParentId);
                            system.debug('e-->##'+e);
                            if(e!=null){
                                Case c=mCases.get(e.ParentId);
                                system.debug('c-->##'+c);
                                system.debug('c.Cases[0].Id-->##'+c.Cases[0].Id);
                                if(c.Id!=null)
                                    a.ParentId=c.Cases[0].Id;    
                                
                            }    
                        }
                    }
                }
            }catch(Exception e){
                system.debug(e);
            }
        }   
    }                      
}