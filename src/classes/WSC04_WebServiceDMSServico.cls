public class WSC04_WebServiceDMSServico {
    
    public void atualizacaoContinua(Integer numBIR, List<WSC04_WebServiceDMSv2.VeiculoEstoque> veiculoList) {
        Map<String, WSC04_WebServiceDMSv2.VeiculoEstoque> mapW =
            new Map<String, WSC04_WebServiceDMSv2.VeiculoEstoque>();
        
        for(WSC04_WebServiceDMSv2.VeiculoEstoque w : veiculoList) {
            if(w.status != 'Reserved') {
                mapW.put(w.VIN, w);
                
            } else {
                w.errorMessage = 'E113 - A vehicle status can not be updated' +
                    ' to Booking by this service. Ask the dealer to first create a Quote on SFA.;';
            }
        }
       
        /*List<VEH_Veh__c> veiListDb = [
            SELECT Id, Name FROM VEH_Veh__c 
            WHERE Name IN: (mapW.keySet()) AND Status__c = 'Booking'
        ];
        
        for(VEH_Veh__c v: veiListDb) {
            WSC04_WebServiceDMSv2.VeiculoEstoque w = mapW.get(v.Name);
            mapW.remove(v.Name);
        }*/
        
        atualizarDados(numBIR, mapW);
    }
    
    private void atualizarDados(Integer numBIR, Map<String, WSC04_WebServiceDMSv2.VeiculoEstoque> mapW) {
        System.debug('atualizarDados: ' + numBIR + ' ' + mapW);
        List<Account> accBirList = obterContaBir(numBIR);
        
        if(accBirList.isEmpty()) {
            System.debug('BIR nao encontrada');
            
            for(WSC04_WebServiceDMSv2.VeiculoEstoque w : mapW.values()) {
                w.errorMessage = 'BIR nao encontrada :' + numBIR;
            }
            return;
        }
        
        //mapa com os veiculos que solicitam update
        Map<String, VEH_Veh__c> veiMap = new Map<String, VEH_Veh__c>();
        for(WSC04_WebServiceDMSv2.VeiculoEstoque vei : mapW.values()) {
            VEH_Veh__c veiculo = new VEH_Veh__c();
            veiculo.Received_By_Stock_Full__c = vei.fullIntegration;
            veiculo.Name 			= vei.VIN;
            veiculo.Model__c		= vei.model;
            veiculo.Version__c		= vei.version;
            veiculo.DateofManu__c	= vei.entryInventoryDate;
            veiculo.ModelYear__c 	= vei.modelYear;
            veiculo.Color__c		= vei.color;
            veiculo.Optional__c		= vei.optionals;
            veiculo.Price__c		= vei.price;
            veiculo.LastModifiedDate= vei.lastUpdateDate;
            veiculo.Status__c		= vei.status;
            veiculo.State__c		= vei.stage;
            veiculo.DeliveryDate__c	= vei.CostumerDeliveryDate;
            //armazenamento temporario
            veiculo.FirstRegistrDate__c = vei.entryInventoryDate;
            
            veiMap.put(vei.VIN, veiculo);
        }
        
        System.debug('Vei Map: ' + veiMap);
        tratarExistenciaVeiculo(veiMap, mapW);
        System.debug('Vei Map: ' + veiMap);
        
        List<VRE_VehRel__c> veiRelList = 
            desativarOutrosRelVeiculo(accBirList.get(0), veiMap.keySet());
        System.debug('veiRelList: ' + veiRelList);
        
        List<VEH_Veh__c> vList = new List<VEH_Veh__c>();
        for(VEH_Veh__c vei : veiMap.values()) {
            
            VEH_Veh__c veiculo = new VEH_Veh__c();
            veiculo.Id = vei.Id;
            veiculo.Received_By_Stock_Full__c = vei.Received_By_Stock_Full__c;
            veiculo.Name 			= vei.Name;
            veiculo.Model__c		= vei.Model__c;
            veiculo.Version__c		= vei.Version__c;
            veiculo.DateofManu__c	= vei.DateofManu__c;
            veiculo.ModelYear__c 	= vei.ModelYear__c;
            veiculo.Color__c		= vei.Color__c;
            veiculo.Optional__c		= vei.Optional__c;
            veiculo.Price__c		= vei.Price__c;
            veiculo.Status__c		= vei.Status__c;
            veiculo.State__c		= vei.State__c;
            veiculo.DeliveryDate__c	= vei.DeliveryDate__c;     
            vList.add(veiculo);
        }
        
        Savepoint sp = Database.setSavepoint();
        try {
            
            UPSERT vList;
            //atualizar relacionamento antigo
            UPDATE veiRelList;
            
            List<VRE_VehRel__c> newVeiRel = 
                ativarMeusRelVeiculo(accBirList.get(0), vList);
            System.debug('newVeiRel: ' + newVeiRel);
            
            //inserir novo relacionamento
            UPSERT newVeiRel;
            
        } catch(Exception ex) {
            Database.rollback(sp);
            
            System.debug('Ex: ' + ex);
            
            for(WSC04_WebServiceDMSv2.VeiculoEstoque w : mapW.values()) {
                w.errorMessage = ex.getMessage();
            }
            
        }
    }
    
    private void tratarExistenciaVeiculo(Map<String, VEH_Veh__c> veiMap, Map<String, WSC04_WebServiceDMSv2.VeiculoEstoque> mapW) {
        System.debug('tratarExistenciaVeiculo : ' + veiMap);
        
        List<VEH_Veh__c> veiListDb = [
            SELECT Id, Name, Status__c, LastModifiedDate FROM VEH_Veh__c 
            WHERE Name IN: (veiMap.keySet())
        ];
        
        if(veiListDb.isEmpty()) {
            return;
        }
        
        for(VEH_Veh__c veiDb : veiListDb) {
            VEH_Veh__c vei = veiMap.get(veiDb.Name);
            
            System.debug('vei>' + vei + ' veiDb ' + veiDb);
            // Convert the date from DB to user GMT
            Datetime dtDB = Datetime.newInstanceGMT(
                veiDb.LastModifiedDate.year(), veiDb.LastModifiedDate.month(),
                veiDb.LastModifiedDate.day(), veiDb.LastModifiedDate.hour(), veiDb.LastModifiedDate.minute(),
                veiDb.LastModifiedDate.second());
            
            if (vei.LastModifiedDate <= dtDB) {
                //vehicleVO.error = true;
                mapW.get(vei.Name).errorMessage = 'B203 - Vehicle not updated: lastModifiedDate is before than the record at Salesforce.';
                veiMap.remove(vei.Name);
            }
            
            if(vei.Received_By_Stock_Full__c && veiDb.Status__c == 'Booking') {
                if(vei.Status__c == 'Billed'){
                    vei.Status__c = 'Billed';
                } else {
                    vei.Status__c = 'Booking';
                }
            
            } 
            if(!vei.Received_By_Stock_Full__c && veiDb.Status__c == 'Booking') {
                 //vehicleVO.error = true;
                mapW.get(vei.Name).errorMessage = 'E112 - Reserved vehicles can not have their status changed by this service.' +
                ' Ask the dealer to first cancel the reservation, on SFA.';
                veiMap.remove(vei.Name);
            }
            vei.Id = veiDb.Id;
        }
    }
    
    private List<VRE_VehRel__c> ativarMeusRelVeiculo(Account acc, List<VEH_Veh__c> veiList) {
        System.debug('ativarMeusRelVeiculo : ' + acc + ' ' + veiList);
        
        Map<String, VRE_VehRel__c> mapRel = new Map<String, VRE_VehRel__c>();
        for(VEH_Veh__c v : veiList) {
            VRE_VehRel__c veiRel = new VRE_VehRel__c();
            veiRel.Status__c = 'Active';
            veiRel.Account__c = acc.Id;
            veiRel.DateOfEntryInStock__c = v.FirstRegistrDate__c;
            veiRel.VIN__c = v.Id;
            
            //remover armazenamento temporario
            v.FirstRegistrDate__c = null;
            mapRel.put(v.Name, veiRel);
        }
        
        List<VRE_VehRel__c> veiRelDb = [
            SELECT Id, VIN__r.Name
            FROM VRE_VehRel__c
            WHERE Account__c =: acc.Id AND VIN__c IN: veiList
        ];
        
        for(VRE_VehRel__c vRdb: veiRelDb) {
            VRE_VehRel__c rel = mapRel.get(vRdb.VIN__r.Name);
            rel.Id = vRdb.Id;
        }
        
        return mapRel.values();
    }
    
    private List<VRE_VehRel__c> desativarOutrosRelVeiculo(Account acc, Set<String> vinList) {
        System.debug('desativarOutrosRelVeiculo : ' + acc + ' ' + vinList);
        System.debug('Tipo de registro esperado: ' + Utils.getRecordTypeId('Account', 'Network_Site_Acc'));
        
        List<VRE_VehRel__c> veiRelList = [
            SELECT ID, Account__r.Id, Status__c
            FROM VRE_VehRel__c
            WHERE Status__c = 'Active' AND VIN__r.Name IN: (vinList)
            AND Account__r.RecordTypeId =: Utils.getRecordTypeId('Account', 'Network_Site_Acc')
            AND Account__r.Id !=: acc.Id
        ];
        
        System.debug('veiRelList no método: ' + veiRelList);
        
        
        for(VRE_VehRel__c vR : veiRelList) {
            vR.Status__c = 'Inactive';
        }
        
        return veiRelList;
    }
    
    private List<Account> obterContaBir(Integer bir) {
        if(bir < 1) {
            return new List<Account>();
        }
        
        return [
            SELECT Id, Name, IDBIR__c, WebServiceVersion__c, NewWebServiceAddress__c
            FROM Account 
            WHERE IDBIR__c =: String.valueOf(bir)
            OR IDBIR__c =: '0' + String.valueOf(bir)
            ORDER BY IDBIR__c desc 
            LIMIT 1
        ];
    }
    
    public void validarVeiculo(List<WSC04_WebServiceDMSv2.VeiculoEstoque> veiculoList) {
        System.debug('Iniciando validacao de veiculos: ' + veiculoList.size());
        
        for(WSC04_WebServiceDMSv2.VeiculoEstoque vei : veiculoList) {
            System.debug('Veiculo: ' + vei);
            
            campoObrigatorio(vei);
            
            validaChassi(vei);
            
            validaQuant(vei);
            
            validaStatus(vei);
            
            validaStage(vei);
            
            validaStageStatus(vei);
            
            validaCostumerDeliveryDate(vei);
            
            validaAno(vei);
        }
    }
    
    private void validaAno(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        if(vei.manufacturingYear != null) {
            if(vei.manufacturingYear < 2000 || vei.manufacturingYear > 3000) {
                vei.errorMessage = 'E109 - manufacturingYear field must be between 2000 and 3000;';
            }
        }
        
        if(vei.modelYear != null) {
            if(vei.modelYear < 2000 || vei.modelYear > 3000) {
                vei.errorMessage = 'E109 - modelYear field must be between 2000 and 3000;';
            }
        }
    }
    
    private void validaStageStatus(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        if(vei.stage != null && vei.stage.toLowerCase().equals('in transit')) {
            if(vei.status != null && 
               (vei.status.toLowerCase().equals('billed') || vei.status.toLowerCase().equals('immobilized'))) {
                   vei.errorMessage = 'E108 - To update the VIN state to “In Transit”, VIN status can not be Billed or Immobilized;';
               }
        }
    }
    
    private void validaCostumerDeliveryDate(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        if(vei.CostumerDeliveryDate != null && vei.status.toLowerCase() != 'billed') {
            vei.errorMessage = 'E107 - To update the field CostumerDeliveryDate, VIN status must be Billed;';
        }
    }
    
    private void validaStage(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        Map<String, String> mapa = new Map<String, String>();
        mapa.put('in transit',  'in transit');
        mapa.put('stock',       'stock');
        
        if(vei.stage != null) {
            String valor = mapa.get(vei.stage.toLowerCase());
            if(valor == null) {
                vei.errorMessage = 'E106 - State field must belong to a value of this list: [In Transit / Stock];';
                
            }
        }
    }
    
    private void validaStatus(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        Map<String, String> mapa = new Map<String, String>();
        mapa.put('available',           'available');
        mapa.put('quality blockade',    'quality blockade');
        mapa.put('comercial blockade',  'comercial blockade');
        mapa.put('billed',              'billed');
        mapa.put('immobilized',         'immobilized');
        
        if(vei.status != null) {
            String valor = mapa.get(vei.status.toLowerCase());
            
            if(vei.fullIntegration != null && vei.fullIntegration && valor == null) {
                if(vei.status.toLowerCase() == 'booking') {
                    vei.status = 'Comercial Blockade';
                    return; 
                }
                
            } else if(vei.fullIntegration != null && !vei.fullIntegration && valor != null) {
                return;
                
            } 
            
            if(valor == null) {
                vei.errorMessage = 'E105 - Status field must belong to a value of this list: [Available / Quality Blockade / Comercial Blockade / Billed / Immobilized];';
            }
        }
    }
    
    private void validaQuant(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        if(vei.model != null && vei.model.length() > 30) {
            vei.errorMessage = 'E104 - model too long: maximum size 30 characters;';  
        }
        
        if(vei.version != null && vei.version.length() > 50) {
            vei.errorMessage = 'E104 - version too long: maximum size 50 characters;';  
        }
        
        if(vei.manufacturingYear != null && String.valueOf(vei.manufacturingYear).length() > 4) {
            vei.errorMessage = 'E104 - manufacturingYear too long: maximum size 4 characters;';  
        }
        
        if(vei.modelYear != null && String.valueOf(vei.modelYear).length() > 4) {
            vei.errorMessage = 'E104 - modelYear too long: maximum size 4 characters;';  
        }
        
        if(vei.color != null && vei.color.length() > 30) {
            vei.errorMessage = 'E104 - color too long: maximum size 30 characters;';  
        }
        
        if(vei.optionals != null && vei.optionals.length() > 100) {
            vei.errorMessage = 'E104 - optionals too long: maximum size 100 characters;';  
        }
    }
    
    private void validaChassi(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        if(vei.VIN != null && vei.VIN.length() != 17) {
            if(vei.errorMessage != null) {
                vei.errorMessage = vei.errorMessage + '\nE102 - VIN field must contain exactly 17 characters;';
            } else {
                vei.errorMessage = '\nE102 - VIN field must contain exactly 17 characters;';
            }
        }
    }
    
    private void campoObrigatorio(WSC04_WebServiceDMSv2.VeiculoEstoque vei) {
        
        if(vei.fullIntegration == null) {
            addMsgCampoObrigatorio(vei, 'fullIntegration');
        }
        
        if(String.isEmpty(vei.VIN)) {
            addMsgCampoObrigatorio(vei, 'VIN');
        }
        
        if(String.isEmpty(vei.model)) {
            addMsgCampoObrigatorio(vei, 'model');
        }
        
        if(String.isEmpty(vei.version)) {
            addMsgCampoObrigatorio(vei, 'version');
        }
        
        if(vei.manufacturingYear == null) {
            addMsgCampoObrigatorio(vei, 'manufacturingYear');
        }
        
        if(vei.modelYear == null) {
            addMsgCampoObrigatorio(vei, 'modelYear');
        }
        
        if(String.isEmpty(vei.color)) {
            addMsgCampoObrigatorio(vei, 'color');
        }
        
        if(String.isEmpty(vei.optionals)) {
            addMsgCampoObrigatorio(vei, 'optionals');
        }
        
        if(vei.entryInventoryDate == null) {
            addMsgCampoObrigatorio(vei, 'entryInventoryDate');
        }
        
        if(vei.price == null) {
            addMsgCampoObrigatorio(vei, 'price');
        }
        
        if(vei.lastUpdateDate == null) {
            addMsgCampoObrigatorio(vei, 'lastUpdateDate');
        }
        
        if(String.isEmpty(vei.status)) {
            addMsgCampoObrigatorio(vei, 'status');
        }
        
        if(String.isEmpty(vei.stage)) {
            addMsgCampoObrigatorio(vei, 'stage');
        }
        
        /*if(vei.costumerDeliveryDate == null) {
        addMsgCampoObrigatorio(vei, 'costumerDeliveryDate');
        }*/
    }
    
    private void addMsgCampoObrigatorio(WSC04_WebServiceDMSv2.VeiculoEstoque vei , String campo) {
        String msg = 'E100 - Field "CAMPO" is required and cannot be empty;';
        
        if( vei.errorMessage == null ) {
            vei.errorMessage = msg.replace('CAMPO', 'costumerDeliveryDate');    
            
        } else {
            vei.errorMessage = vei.errorMessage + '\n' + msg.replace('CAMPO', campo); 
            
        }
    }
}