@isTest
private class WSC07_AOC_Batch_Step1_Test
{
   static testmethod void myUnitTest1()
   {
		// Prepare test data
        List<Product2> lstProducts= VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
        system.assert(lstProducts.size() > 0);
        WSC07_AOC_Batch_Step1 aocBatchStep1 = new WSC07_AOC_Batch_Step1();
        Database.BatchableContext BC;
 
 		// Start test
        Test.startTest();
 
        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpResponseGenerator());
		aocBatchStep1.start(BC);

    	// Sotop test   
        Test.stopTest();
	}

    static testmethod void myUnitTest2()
    {
    	// Prepare test data
        List<CS_Models__c> lstCSModels = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertCSModels();
        system.assert(lstCSModels.size() > 0);
        WSC07_AOC_Batch_Step1 aocBatchStep2 = new WSC07_AOC_Batch_Step1();
        Database.BatchableContext BC;

		// Start test
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpAOC_Batch_Step1());
        aocBatchStep2.start(BC);
        aocBatchStep2.execute(BC, lstCSModels);
        aocBatchStep2.finish(BC);
        aocBatchStep2.Test_WrapperValues();
        WSC07_AOC_Batch_Step1.Wrapper_Label wrapperlabel = new WSC07_AOC_Batch_Step1.Wrapper_Label();
        wrapperlabel.pt = 'Test';
        WSC07_AOC_Batch_Step1.Wrapper_Presentation wrapperPresentation = new WSC07_AOC_Batch_Step1.Wrapper_Presentation(wrapperlabel,'Test1');
        
        // Stop test
        Test.stopTest();
    }
}