@isTest
public class VFC129_UTIL_TestData
{

	/**
     * Load some profiles Ids
     */
    public Map<String, Id> loadProfiles()
    {
        Map<String, Id> mapProfiles = new Map<String, Id>();
        for (Profile prof : [SELECT Id, Name 
                             FROM Profile
                             WHERE Name in ('Usuário padrão',
                                            'Administrador do sistema')])
        {
            if (prof.Name == 'Usuário padrão')
                mapProfiles.put('Usuario padrao', prof.Id);
            else
                mapProfiles.put(prof.Name , prof.Id);
        }
        return mapProfiles;
    }
    
    /**
     * Load some profiles Ids
     */
    public Map<String, Id> loadUserRoles()
    {
        Map<String, Id> mapRoles = new Map<String, Id>();
        for (UserRole role : [SELECT Id, Name 
                                FROM UserRole
                               WHERE Name in ('Presidente')])
        {
			mapRoles.put(role.Name , role.Id);
        }
        return mapRoles;
    }

 	/**
 	 * Build a Lead record
 	 */
	public static Lead createLead()
	{
		Lead lead = new Lead(FirstName = 'John',
							 LastName = 'Doe');
		return lead;
	}

 	/**
	 * Build a Campaign record.
	 */
	public static Campaign createCampaignParent(String DBMCode)
	{
		// Get all recordTypes Ids necessary, where the key is the developer name
        Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
            getRecordTypeIdByDevNameSet(new Set<String>{'ParentCampaign', 'SingleCampaign'});

		Campaign campaign = new Campaign(Name = 'Test Campaign #01',
		  								 RecordTypeId = mapRecordTypes.get('ParentCampaign'),
										 DBMCampaignCode__c = DBMCode,
        								 isActive = true, 
        								 Status = 'In Progress', 
        								 StartDate = Date.today()-30, 
        								 EndDate = Date.today()+30, 
        								 DealerInclusionAllowed__c = false,
        								 DealerShareAllowed__c = false, 
        								 ManualInclusionAllowed__c = true, 
        								 PAActiveShareAllowed__c = true);
		return campaign;
	}
	
	/**
	 * Build a Campaign record.
	 */
	public static Campaign createCampaignSingle(String DBMCode, Id accountDealerId, Id campaignParentId)
	{
		// Get all recordTypes Ids necessary, where the key is the developer name
        Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
            getRecordTypeIdByDevNameSet(new Set<String>{'ParentCampaign', 'SingleCampaign'});

		Campaign campaign = new Campaign(Name = 'Test Campaign #01',
		  								 RecordTypeId = mapRecordTypes.get('SingleCampaign'),
										 DBMCampaignCode__c = DBMCode,
        								 isActive = true,  
        								 Status = 'In Progress', 
        								 StartDate = Date.today()-30, 
        								 EndDate = Date.today()+30, 
        								 DealerInclusionAllowed__c = false,
        								 DealerShareAllowed__c = false, 
        								 ManualInclusionAllowed__c = true, 
        								 PAActiveShareAllowed__c = true);
		if (campaignParentId != null)
			campaign.ParentId = campaignParentId;
		if (accountDealerId != null)
			campaign.Dealer__c = accountDealerId;
		return campaign;
	}

	/**
	 * Build a Campaign record for DL validation.
	 */
	public static Campaign createCampaignForDLValidation()
	{
		// Get all recordTypes Ids necessary, where the key is the developer name
        Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
            getRecordTypeIdByDevNameSet(new Set<String>{'SingleCampaign'});

		Campaign campaign = new Campaign(Name = 'Test Campaign #01',
		  								 RecordTypeId = mapRecordTypes.get('SingleCampaign'),
        								 isActive = true,  
        								 Status = 'In Progress', 
        								 StartDate = Date.today()-30, 
        								 EndDate = Date.today()+30, 
        								 DealerInclusionAllowed__c = true,
        								 DealerShareAllowed__c = true, 
        								 ManualInclusionAllowed__c = true, 
        								 PAActiveShareAllowed__c = true);
		return campaign;
	}

 	/**
	 * Build a CampaignMember record.
	 */
	public static CampaignMember createCampaignMember(Id campaignId, Id leadId, Id contactId)
	{
		CampaignMember cm = new CampaignMember(CampaignId = campaignId,
											   LeadId = leadId,
											   ContactId = contactId,
											   Status = '', 
											   Source__c = '', 
											   Transferred__c = false); 
		return cm;
    }

	/**
	 * Build a Account record with recordtype.
	 */
	public static Account createAccountDealer()
	{
		RecordType recType = VFC08_RecordTypeDAO.getInstance().
			findRecordTypeByDeveloperName('Network_Site_Acc');
		
		Account account = new Account(Name = 'Dealer Test #01',
									  RecordTypeId = recType.Id,
                                     IDBIR__c = '1000'); 
		return account;
    }
    
    /**
	 * Build a Account record with recordtype.
	 */
	public static Account createAccountPersonal()
	{
		RecordType recType = VFC08_RecordTypeDAO.getInstance().
			findRecordTypeByDeveloperName('Personal_Acc');
		
		Account account = new Account(FirstName = 'John',
									  LastName = 'Doe',
									  HomePhone__c = '90901010',
									  ShippingCity = 'Rio de Janeiro', 
									  ShippingCountry = 'Brasil', 
									  ShippingState = 'RJ',  
									  ShippingStreet = 'Av. das américas, 1000, Barra da Tijuca', 
									  RecordTypeId = recType.Id); 
		return account;
    }
 
    /**
     * Build a DBM_Import__c record.
     */
    public static DBM_Import__c createDBMImport()
    {
    	DBM_Import__c dbmImport = new DBM_Import__c();
    	return dbmImport;
    }
    
    /**
     * Build a ZipCodeBase__c record.
     */
    public static ZipCodeBase__c createZipCodeBase(String zipCode)
    {
    	ZipCodeBase__c zip = new ZipCodeBase__c(Name = zipCode,
    										    Street__c = 'Av. Paulista', 
    										    Numerical_Range__c = '1 - 100', 
    										    Neighborhood__c = 'Centro',
    						  					City__c = 'São Paulo',
    						  					State__c = 'SP');
    	return zip;
    }
}