/**
	Class   -   VFC21_VehicleDAO_Test
    Author  -   RameshPrabu
    Date    -   30/10/2012
    
    #01 <RameshPrabu> <30/10/2012>
        Created this class using test for VFC21_VehicleDAO.
**/
@isTest 
private class VFC21_VehicleDAO_Test {

    static testMethod void VFC21_VehicleDAO_Test() {
        // TO DO: implement unit test
       
     	Test.startTest();
     		//List<VEH_Veh__c> lstVehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehicleObjects();
    		
    		List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
    		List<VRE_VehRel__c> lstVehicleRelations = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehRelObjects(lstDealerRecords);

    		List<VEH_Veh__c> lstVehicles = VFC21_VehicleDAO.getInstance().fetchVehicleRecords();
    		
    		//fetchVehicleRecordsUsingModelName
    		
    		String model = 'Fluence';
    		List<VEH_Veh__c> lstVehicleUsingModel = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingModelName(model);
    		// public Map<String, VEH_Veh__c> fetchVehicleUsingVIN( Set<String> setVIN ){
    		Set<String> setVIN = new Set<String>();
    		List<Id> lstVREIDs = new List<Id>();
    		
    		for( VEH_Veh__c vre : lstVehicles ){
    			lstVREIDs.add( vre.Id );
    			setVIN.add( vre.Name);
    		}
    		
    		Map<String, VEH_Veh__c> mapVehicleIDs = VFC21_VehicleDAO.getInstance().fetchVehicleUsingVIN(setVIN);
    		
    		//fetchVehicleRecordsUsingVehicleRelationIDs
    		
    		List<VEH_Veh__c> lstVREUsingIds = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingVehicleRelationIDs(lstVREIDs);
    		
    		
    		
     	Test.stopTest();
        system.debug('lead Details...' +lstVehicles);
		System.assert(lstVehicles.size() > 0);
    }
}