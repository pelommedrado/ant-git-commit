public class createDealerGroup{

    public static void create(List<User> lsUser){
        
        /**** Cria grupo publico para grupos de concesionaria para papel de executivos do portal ****/
        List<User> usersExecutive = getPapelUsuario(lsUser,'Executive');       
        Set<String> nomeGrupos = criaNomesGrupos(usersExecutive,'ExecutiveUsers');
        Set<String> existingGroup = getGruposExistentes(nomeGrupos);
        criaGrupoSeNaoExistir(existingGroup,nomeGrupos);

        // recupera todos os grupos existentes e recem criados
        List<Group> lsNewGroups = [SELECT Id FROM Group WHERE Name IN: nomeGrupos];
        Set<Id> userRoleIds = getPapeis(usersExecutive);
        Map<Id,Id> mapGroupPortalRole = getGrupoPapelEstaComoMembro(userRoleIds);
        putUsuarioNoGrupo(usersExecutive,lsNewGroups,mapGroupPortalRole);
        
        
        
        /**** Cria grupo publico para concessionarias para todos os papeis do portal ****/
        List<User> allUsers = getPapelUsuario(lsUser,null);
        Set<String> grupoAllDealers = criaNomesGrupos(allUsers,'AllDealerUsers');
        Set<String> existingGroupAllDealer = getGruposExistentes(grupoAllDealers);
        criaGrupoSeNaoExistir(existingGroupAllDealer,grupoAllDealers);
        
        // recupera todos os grupos existentes e recem criados
        List<Group> lsNewGroupsAllDealers = [SELECT Id FROM Group WHERE Name IN: grupoAllDealers];       
        Set<Id> userRoleId = getPapeis(allUsers);  
        Map<Id,Id> mapGrupoPapelPortal = getGrupoPapelEstaComoMembro(userRoleId);
        putUsuarioNoGrupo(allUsers,lsNewGroupsAllDealers,mapGrupoPapelPortal);
        
        
        
        /**** Cria grupo publico para grupos de concesionaria para todos papeis do portal ****/
        List<User> usuarios = getPapelUsuario(lsUser,null);
        Set<String> allGroupUsers = criaNomesGrupos(usuarios,'AllGroupUsers');
        Set<String> existingGroupAllGroupUsers = getGruposExistentes(allGroupUsers);
        criaGrupoSeNaoExistir(existingGroupAllGroupUsers,allGroupUsers);
        
        // recupera todos os grupos existentes e recem criados
        List<Group> lsNewGroupsAllGroupUsers = [SELECT Id FROM Group WHERE Name IN: allGroupUsers];       
        Set<Id> papelUser = getPapeis(allUsers);  
        Map<Id,Id> mapGroupPapelPortal = getGrupoPapelEstaComoMembro(papelUser);
        putUsuarioNoGrupo(allUsers,lsNewGroupsAllGroupUsers,mapGroupPapelPortal);
        
    }
    
    public static void putUsuarioNoGrupo(List<User> usersExecutive,List<Group> lsNewGroups, Map<Id,Id> mapGroupPortalRole){
        //coloca o papel do usuario dentro de seu respectivo grupo
        List<GroupMember> newGroupMember = new List<GroupMember>();
        for(User u: usersExecutive){
            for(Group g: lsNewGroups){
                GroupMember gm = new GroupMember();
                gm.GroupId = g.Id;
                gm.UserOrGroupId = mapGroupPortalRole.get(u.UserRoleId);
                newGroupMember.add(gm);
            }
        }
        List<Database.UpsertResult> results = Database.Upsert(newGroupMember,false);
        for(Database.UpsertResult r: results){
            if(!r.isSuccess()){
                system.debug('DML ERROR: '+r.getErrors());
            }
        }
    }
    
    public static Map<Id,Id> getGrupoPapelEstaComoMembro(Set<Id> userRoleIds){
        
        // recupera Id do grupo relacionado ao papel e cria mapa de Id<Papel> com Id<grupo relacionado ao papel>
        List<Group> grupoRelacionado = [SELECT Id, RelatedId FROM Group WHERE RelatedId IN: userRoleIds];
        Map<Id,Id> mapGroupPortalRole = new Map<Id,Id>();
        for(Group g: grupoRelacionado){
            mapGroupPortalRole.put(g.RelatedId, g.Id);
        }
        
        // recupera o grupo em que o papel está como membro (GroupId)
        List<GroupMember> lsGroupMember = [SELECT GroupId,UserOrGroupId FROM GroupMember 
                                           WHERE UserOrGroupId IN: grupoRelacionado];
        Set<Id> setRolePortal = new Set<Id>();
        for(GroupMember g: lsGroupMember){
            setRolePortal.add(g.UserOrGroupId);
        }
        return mapGroupPortalRole;
    }
    
    public static Set<Id> getPapeis(List<User> usersExecutive){
        Set<Id> userRoleIds = new Set<Id>();
        for(User u: usersExecutive){
            // armazena todos os papeis dos usuarios
            userRoleIds.add(u.UserRoleId);
        }
        return userRoleIds; 
    }
    
    public static Set<String> criaNomesGrupos(List<User> usersExecutive, String nomeGrupo){
        Set<String> nomeCompletoGrupo = new Set<String>();
        for(User u: usersExecutive){
            // armazena nome do grupo para verificar se iremos ou não criar um novo
            if(nomeGrupo.equals('ExecutiveUsers')){
                nomeCompletoGrupo.add('BR - ' + u.Contact.Account.ParentId + ' - ' + nomeGrupo);
            }
            if(nomeGrupo.equals('AllDealerUsers')){
                nomeCompletoGrupo.add('BR - '+ u.AccountId +' - ' + nomeGrupo);
            }
            if(nomeGrupo.equals('AllGroupUsers')){
                nomeCompletoGrupo.add('BR - ' + u.Contact.Account.ParentId + ' - ' + nomeGrupo);
            }
        }
        return nomeCompletoGrupo;  
    }
    
    public static Set<String> getGruposExistentes(Set<String> nomeGrupo){
        // recupera todos os grupos dos grupos de concessionarias existentes
        List<Group> lsGroup = [SELECT Id, Name FROM Group WHERE Name IN: nomeGrupo];
        Set<String> existingGroup = new Set<String>();
        for(Group g: lsGroup){
            existingGroup.add(g.Name);
        }
        return existingGroup;
    }
    
    public static void criaGrupoSeNaoExistir(Set<String> existingGroup, Set<String> nomeGrupo){
        //cria o grupo caso não exista
        List<Group> insertGroup = new List<Group>();
        for(String g: nomeGrupo){
            if(!existingGroup.contains(g)){
                Group gr = new Group();
                gr.Name = g;
                insertGroup.add(gr);
            }
        }
        List<Database.SaveResult> results = Database.Insert(insertGroup,false);
        for(Database.SaveResult r: results){
            if(!r.isSuccess()){
                system.debug('DML ERROR: '+r.getErrors());
            }
        }
    }
    
    public static List<User> getPapelUsuario(List<User> lsUser, String papelPortal){
        // filtra apenas os usuarios com papeis da comunidade
        List<User> users = new List<User>();
        if(papelPortal != null){
        	users = [SELECT Id, UserRoleId, AccountId, Contact.Account.ParentId 
                                     FROM User WHERE Id IN: lsUser AND UserRole.PortalRole =: papelPortal];
        }else{
            users = [SELECT Id, UserRoleId, AccountId, Contact.Account.ParentId 
                                     FROM User WHERE Id IN: lsUser AND UserRole.PortalType =: 'Partner'];
        }
        
        return users;
    }
}