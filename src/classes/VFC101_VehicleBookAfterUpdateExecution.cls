/**
* Classe que será disparada pela trigger do objeto VehicleBooking__c após a atualização dos registros.
* @author Felipe Jesus Silva.
*/
public class VFC101_VehicleBookAfterUpdateExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{	
		List<VehicleBooking__c> lstSObjCurrentVehicleBooking = (List<VehicleBooking__c>) lstNewtData;
		Map<Id, VehicleBooking__c> mapSObjOldVehicleBooking = (Map<Id, VehicleBooking__c>) mapOldData;
		
		this.checkStatus(lstSObjCurrentVehicleBooking, mapSObjOldVehicleBooking);
	}
	
	private void checkStatus(List<VehicleBooking__c> lstSObjCurrentVehicleBooking, Map<Id, VehicleBooking__c> mapSObjOldVehicleBooking)
	{	
        Account acc;
        User u = Sfa2Utils.obterUsuarioLogado();
        if(u.ContactId != null){
            acc = Sfa2Utils.obterContaUserComunidade();
        }
        
		VehicleBooking__c sObjOldVehicleBooking = null;
		VEH_Veh__c sObjVehicle = null;
		List<VEH_Veh__c> lstSObjVehicle = new List<VEH_Veh__c>();
        
		
		for(VehicleBooking__c sObjCurrentVehicleBooking : lstSObjCurrentVehicleBooking)
		{		
			sObjOldVehicleBooking = mapSObjOldVehicleBooking.get(sObjCurrentVehicleBooking.Id);
			System.debug('** flag no vehicle booking: ' + sObjCurrentVehicleBooking.UsedInNewStock__c );
            
			if((String.isNotEmpty(sObjCurrentVehicleBooking.Status__c)) && 
			   (sObjCurrentVehicleBooking.Status__c != sObjOldVehicleBooking.Status__c) &&
			   (sObjCurrentVehicleBooking.Status__c != 'Active'))
			{
				sObjVehicle = new VEH_Veh__c(Id = sObjCurrentVehicleBooking.Vehicle__c);
				
				if(sObjCurrentVehicleBooking.Status__c == 'Canceled' || sObjCurrentVehicleBooking.Status__c == 'Expired')
				{
                    
                    //lstIdVehicleToStock.add(sObjCurrentVehicleBooking.Vehicle__c);
					// Ao encerrar uma reserva o status do veículo se torna disponível.
                    if(u.ContactId != null){
                    	if(acc.new_stock_version__c == true){
							sObjVehicle.Status__c = 'Available';
                    		system.debug('*** STATUS DO VEÍCULO: ' + sObjVehicle.Status__c);
                    	}
                    } else if(sObjCurrentVehicleBooking.UsedInNewStock__c == true){
                            sObjVehicle.Status__c = 'Available';
                    }
				}
				else
				{
					sObjVehicle.Status__c = 'Billed';
				}
				
				//Ajuste inserido no dia 09/08/2013 - Solicitado por Hugo Medrado
				sObjVehicle.Booking_User__c = null;
				
				lstSObjVehicle.add(sObjVehicle);
                system.debug('*** OBJETO PARA ATUALIZAR: ' + sObjVehicle);
                
			}			
		}
        
        
        /*List<VEH_Veh__c> lstSObjVehicleToStock = [Select Id, Status__c From VEH_Veh__c where Id in:lstIdVehicleToStock];
        for(VEH_Veh__c v:lstSObjVehicleToStock){
            if(v.Status__c == 'Billed')
                v.Status__c = 'Stock';
            
        }*/
        
        if(!lstSObjVehicle.isEmpty())
		{
			VFC65_VehicleBO.getInstance().updateVehicles(lstSObjVehicle);
		}
			
        /*if(!lstSObjVehicleToStock.isEmpty()){
            VFC65_VehicleBO.getInstance().updateVehicles(lstSObjVehicleToStock);
        }*/
	}
	
}