@isTest
public class TestVFC10_CaseSuggestedKnowledgeClass {
    
    static testMethod void testVFC10_CaseSuggestedKnowledge() {
        
        User usr = new User (
            LastName='Rotondo', 
            alias='lro',
            Email='lrotondo@rotondo.com',
            BypassVR__c=true,
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            RecordDefaultCountry__c = 'Brazil',
            ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', 
            UserName='lrotondo@lrotondo.com');
        
        System.runAs(usr) {
            
            Id RTID_COMPANY = [
                SELECT Id 
                FROM RecordType 
                WHERE sObjectType='Account' AND DeveloperName='Company_Acc' LIMIT 1
            ].Id;
            
            Account MyAccount = new Account(
                Name='Test1',
                Phone='1000',
                RecordTypeId = RTID_COMPANY, 
                ProfEmailAddress__c = 'addr1@mail.com', 
                ShippingCity = 'Paris', 
                ShippingCountry = 'France', 
                ShippingState = 'IDF', 
                ShippingPostalCode = '75013', 
                ShippingStreet = 'my street',
            	OwnerId = usr.Id);
            insert MyAccount;
            
            Product2 MyProduct = new Product2(Name = 'Name', Family = 'Family');
            insert MyProduct;
            
            Asset MyAsset = new Asset(Name = 'Name', AccountId = MyAccount.Id, Product2id = MyProduct.Id);
            insert MyAsset;
            
            Case oCase = new Case(Origin='Phone',AccountId=MyAccount.Id,AssetId=MyAsset.Id);
            database.insert(oCase);
            
            String caseId; 
            caseId = oCase.Id;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id', caseId);
            ApexPages.currentPage().getParameters().put('subject', 'Test subject');
            ApexPages.StandardController con = new ApexPages.StandardController(new Case());
            VFC10_CaseSuggestedKnowledgeClass ext = new VFC10_CaseSuggestedKnowledgeClass(con);
            ext.getCase();
            ext.getcatFilter();
            ext.attachToCase();
            Test.stopTest();
            System.debug('TESTS ENDED');
        }
    }
}