/**
	Class   -   VFC65_Pricebook2_DAO_Test
    Author  -   Subbarao
    Date    -   13/03/2013
    
    #01 <Subbarao> <13/03/2013>
        Created this class using test for VFC65_Pricebook2_DAO.
**/
@isTest
private class VFC65_Pricebook2_DAO_Test {
	
	static testmethod void VFC65_Pricebook2_DAO_Test(){
		
		Pricebook2 priceBook = VFC03_InsertSObjectsRecordsForTestClass.getInstance().priceBook_RecordInsertion();
		Test.startTest();		
		Pricebook2 priceBookfetched = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();	
	    system.assert(priceBookfetched == null);	
		Test.stopTest();
	}

}