public class VFC04_VehicleData  {

    public Utils_VehicleDataSource DataSource; // Data source to access the Web Service
    
    private VEH_Veh__c vehicle;

    /*=======================================
      CONSTRUCTOR
    =======================================*/     
    public VFC04_VehicleData(ApexPages.StandardController controller) { 
        DataSource = new Utils_VehicleDataSource();
        VEH_Veh__c tmp= (VEH_Veh__c)controller.getRecord();
        //System.debug('### => vehicle.id : ' + vehicle.id);        
        vehicle = [select id, name from VEH_Veh__c 
                       where id = :tmp.id];        
        DataSource.test = false; //dummy implementation used       
    }
    
    public PageReference getVehicleData() {
try {    
         WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse  results = DataSource.getVehicleData(this);
         
        if (results==null) {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));

        } else {
        
         //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results not null'));
            WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg = results.detVehInfoMsg;
             WS01_ApvGetDetVehXml.DetVeh detVeh = results.DetVeh;
        if (detVehInfoMsg != null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + detVehInfoMsg.codeRetour));
        }
        if (detVeh != null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'libModel : ' + detVeh.libModel));
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'libConst : ' + detVeh.libConst));
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'dateTcmFab : ' + detVeh.dateTcmFab));
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'dateLiv : ' + detVeh.dateLiv));


    
                                     
        }

}
        } catch (Exception CalloutException) {
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'error'));
         
}
         return null;
    }    

    public String getVin() {
        return vehicle.name;

    }
}