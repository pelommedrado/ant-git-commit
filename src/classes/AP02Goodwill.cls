public with sharing class AP02Goodwill 
{
    public static void updateGoodwill(list<Goodwill__c> listeGoodwill)
    {
        for (Goodwill__c g : listeGoodwill)
            g.SRCPartRate__c = getGoodwillPercentage(g.Country__c, g.Brand__c, g.Organ__c, Integer.valueOf(g.VehicleAgeInMonth__c), Integer.valueOf(g.Kilometer__c));
    }
    
    public static Integer getGoodwillPercentage(String country, String brand, String organ, Integer age, Integer mileage)
    {
        Integer gwPercentage;
        
        if (country == null || country == ''
            || brand == null || brand == ''
            || organ == null || organ == ''
            || age == null
            || mileage == null)
        {
            gwPercentage = 0;
        }
        else
        {        
            List<Goodwill_Grid_Line__c> gwGridLines = [Select Age_interval__c, Max_Age__c, Mileage_Interval__c, Max_Mileage__c, Row_Number__c, f1__c, f2__c, f3__c, f4__c, f5__c, f6__c, f7__c, f8__c, f9__c, f10__c, f11__c, f12__c, f13__c, f14__c, f15__c, f16__c, f17__c, f18__c, f19__c, f20__c, f21__c, f22__c, f23__c, f24__c, f25__c, f26__c, f27__c, f28__c, f29__c, f30__c, f31__c, f32__c, f33__c, f34__c, f35__c, f36__c, f37__c, f38__c, f39__c, f40__c, f41__c, f42__c, f43__c, f44__c, f45__c, f46__c, f47__c, f48__c, f49__c, f50__c, f51__c, f52__c, f53__c, f54__c, f55__c, f56__c, f57__c, f58__c, f59__c, f60__c, f61__c from Goodwill_Grid_Line__c where Country__c = :country AND VehicleBrand__c = :brand AND Organ__c = :organ order by Row_Number__c];
            
           System.debug('gwGridLines.size() >>>>>>>>>>>>'+gwGridLines.size());
           
            if (gwGridLines.size() == 0 || age > gwGridLines.get(0).Max_Age__c || mileage > gwGridLines.get(0).Max_Mileage__c)
            { 
                gwPercentage = 0;
            }
            else
            {
                Integer mileageLine = Integer.valueOf(mileage / gwGridLines.get(0).Mileage_Interval__c)+1;
                Integer ageColumn = Integer.valueOf(age / gwGridLines.get(0).Age_interval__c)+1;
                gwPercentage = 0;
                
                for (Goodwill_Grid_Line__c gw : gwGridLines)
                { 
                
                
                    if (gw.Row_Number__c == mileageLine) 
                        gwPercentage = Integer.valueOf(gw.get('f'+ageColumn+'__c')); 
                        
                   
                }
            }
        }
        
        System.debug('###### gwPercentage ######'+gwPercentage);
        return gwPercentage;
    }
}