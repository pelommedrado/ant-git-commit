@IsTest
private class DataUtilsTest {

    private static User user;
    private static Account accDealer;
    private static Holiday hol;

    static {
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            //Test.startTest();

            accDealer = MyOwnCreation.getInstance().criaAccountDealer();
            INSERT accDealer;

            Contact ctt = new Contact(
                CPF__c 		=  '44476157114',
                Email 		= 'manager1@org1.com',
                FirstName 	= 'User',
                LastName 	= 'Manager1',
                MobilePhone = '11223344556',
                AccountId 	= accDealer.Id
            );

            INSERT ctt;

            user = new User(
                ContactId 			= ctt.Id,
                ProfileId 			= Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName 			= 'User',
                LastName 			= 'Manager1',
                Username 			= 'manager1@org1.com.teste',
                Email 				= 'manager1@org1.com',
                Alias 				= 'ma1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c 				= '123456',
                EmailEncodingKey	='UTF-8',
                LanguageLocaleKey	='en_US',
                LocaleSidKey		='en_US',
                TimeZoneSidKey		='America/Los_Angeles'
            );

            INSERT user;

            hol = new Holiday();
            hol.Name = 'Independencia';
            hol.IsRecurrence = true;
            hol.RecurrenceStartDate =  Date.newInstance(2017, 9, 7);
            hol.RecurrenceType = 'RecursYearly';
            hol.RecurrenceDayOfMonth = 7;
            hol.RecurrenceMonthOfYear = 'September';
            hol.activitydate = Date.newInstance(2017, 9, 7);
            INSERT hol;

            //Test.stopTest();
        }
    }

    static testMethod void testDataIniMes() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 1, 16);
            Datetime data =
                DataUtils.dataToDatetimeIniMes(d);
            System.assertEquals(1, data.day());
            System.assertEquals(0, data.hour());
            System.assertEquals(0, data.minute());
        }
    }

    static testMethod void testDataFimMes() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 1, 1);
            Datetime data =
                DataUtils.dataToDatetimeFimMes(d);
            System.assertEquals(31, data.day());
            System.assertEquals(23, data.hour());
            System.assertEquals(59, data.minute());
        }
    }

    static testMethod void testDataFim() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 1, 15);
            Datetime data =
                DataUtils.dataToDatetimeFim(d);
            System.assertEquals(15, data.day());
            System.assertEquals(23, data.hour());
            System.assertEquals(59, data.minute());
        }
    }

    static testMethod void testDataIni() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 1, 15);
            Datetime data =
                DataUtils.dataToDatetimeIni(d);
            System.assertEquals(15, data.day());
            System.assertEquals(0, data.hour());
            System.assertEquals(0, data.minute());
        }
    }

    static testMethod void testParseHHmm() {
        System.runAs(user) {
            Time time2 = DataUtils.parseHHmm('10:00');
            System.assertEquals(10, time2.hour());
            System.assertEquals(0, time2.minute());
        }
    }
    static testMethod void testParseDDmmYYYY() {
        System.runAs(user) {
            Date data = DataUtils.parseDDmmYYYY('01/01/2017');
            System.assertEquals(1, data.day());
            System.assertEquals(1, data.month());
            System.assertEquals(2017, data.year());
        }
    }
    static testMethod void testDiffData() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 1, 15);
            Datetime i = DataUtils.dataToDatetimeIniMes(d);
            Datetime f = DataUtils.dataToDatetimeFimMes(d);

            DataUtils.DiffData diff = new DataUtils.DiffData(i, f);
            System.debug('Diff format: ' + diff.format());

            System.assertEquals(30, diff.diffDays);
        }
    }

    static testMethod void testIsWeekendDayTrue() {
      System.runAs(user) {
          Date d = Date.newInstance(2017, 1, 15);
          System.assertEquals(true, DataUtils.isWeekendDay(d));
      }
    }

    static testMethod void testIsWeekendDayFalse() {
      System.runAs(user) {
          Date d = Date.newInstance(2017, 1, 16);
          System.assertEquals(false, DataUtils.isWeekendDay(d));
      }
    }

    static testMethod void testHolidayEqualsDate() {
        System.runAs(user) {
            Date d = Date.newInstance(2017, 9, 7);
            System.assertEquals(true, DataUtils.isHolidayEqualsDate(hol, d));
        }
    }

    static testMethod void testIsHolidayListContains() {
        System.runAs(user) {
            final List<Holiday> holidayList =
                DataUtils.holidayRecurrenceList();

            final Date data = Date.newInstance(2017, 9, 7);

            System.assertEquals(true,
                DataUtils.IsHolidayListContains(holidayList, data));
        }
    }

    static testMethod void testIsWorkingDayTrue() {
        final List<Holiday> holidayList =
            DataUtils.holidayRecurrenceList();
        final Date currentDate = Date.newInstance(2017, 9, 8);

        System.assertEquals(true,
            DataUtils.isWorkingDay(currentDate, holidayList));
    }

    static testMethod void testIsWorkingDayFalse() {
        final List<Holiday> holidayList =
            DataUtils.holidayRecurrenceList();
        final Date currentDate = Date.newInstance(2017, 9, 7);

        System.assertEquals(false,
            DataUtils.isWorkingDay(currentDate, holidayList));
    }

    static testMethod void testNextWorkingDay() {
        final List<Holiday> holidayList =
            DataUtils.holidayRecurrenceList();

        final Date currentDate = Date.newInstance(2017, 9, 1);

        final Date next = DataUtils.nextWorkingDay(currentDate, 5, holidayList);

        System.assertEquals(11, next.day());
        System.assertEquals(9, next.month());
        System.assertEquals(2017, next.year());
    }

    static testMethod void testCalcWorkingDaysBetweenTwoDates() {
        final List<Holiday> holidayList =
            DataUtils.holidayRecurrenceList();

        final Date data = Date.newInstance(2017, 9, 1);
        final Date data2 = Date.newInstance(2017, 9, 11);
        final Integer days =
            DataUtils.calcWorkingDaysBetweenTwoDates(data, data2, holidayList);
        System.assertEquals(5, days);
    }

    static testMethod void testToDate() {
        final Date data = DataUtils.toDate('2017/09/07');
        System.assertEquals(7, data.day());
        System.assertEquals(9, data.month());
        System.assertEquals(2017, data.year());
    }
}