@isTest
private class Myr_VehRelEndPreviousRelation_Cls_Test {

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Germany', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Germany2', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
	}
  
	//BASIC CASE: the second confirmed relation on the same vehicle set the first relation to taken
	static testMethod void test_Basic_Case() { 
		//1--- Prepare the data
		Account a = Myr_Datasets_Test.insertPersonalAccountAndUser( 'toto', 'toto', '', 'toto@toto.com', '', '', 'NO_USER', '', 'France', '', '', '', '' );
		a.MYR_Status__c='Activated';
		update a;
		Account b = Myr_Datasets_Test.insertPersonalAccountAndUser( 'titi3', 'titi3', '', 'titi@titi.com', '', '', 'NO_USER', '', 'France', '', '', '', '' );
		b.MYR_Status__c='Activated';
		update b;
		VEH_Veh__c v = new VEH_Veh__c();
		v.Name='VF1CR1J0H40046301';
		v.VehicleBrand__c='RENAULT';
		insert v;
		VRE_VehRel__c vr1 = new VRE_VehRel__c();
		vr1.Account__c=a.Id;
		vr1.VIN__c=v.Id;
		vr1.Status__c='Active';
		vr1.TypeRelation__c = 'Owner';
		vr1.My_Garage_Status__c = 'confirmed';
		insert vr1;
		vr1 = [SELECT Id, Status__c, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :vr1.Id];
		system.AssertEquals('confirmed', vr1.My_Garage_Status__c);
		//2--- Insert a relation on the same vin for another account
		VRE_VehRel__c vr2 = new VRE_VehRel__c();
		vr2.Account__c=b.Id;
		vr2.VIN__c=v.Id;
		vr2.TypeRelation__c = 'Owner';
		vr2.My_Garage_Status__c = 'confirmed';
		insert vr2;
		//3--- Check that the first relation is now to the status taken, whereas the second is set to confirmed
		vr1 = [SELECT Id, Status__c, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :vr1.Id];
		system.AssertEquals('taken', vr1.My_Garage_Status__c);
		vr2 = [SELECT Id, Status__c, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :vr2.Id];
		system.AssertEquals('confirmed', vr2.My_Garage_Status__c);
    }

	//GERMANY 2: test that the close previous is not performed on Germany
	static testMethod void test_Deactivation_Germany2_GermanyExcluded() { 
		//Get the technical users
		User techUserGermany2 = Myr_Datasets_Test.getTechnicalUser('Germany2');
		User techUserGermany = Myr_Datasets_Test.getTechnicalUser('Germany');
		//Prepare the custom setting
		CS04_MYR_Settings__c myrSetting = [SELECT ID, Myr_LimitClosePrevious_Country__c FROM CS04_MYR_Settings__c];
		myrSetting.Myr_LimitClosePrevious_Country__c = 'Germany';
		update myrSetting;
		//Prepare a "Germany" account with a linked vehicle
		Account germanyAcc = null;
		VEH_Veh__c germanyVin = null;
		VRE_VehRel__c germanyVRE = null;
		system.runAs( techUserGermany ) {
			germanyAcc = Myr_Datasets_Test.insertPersonalAccountAndUser( 'Helmut', 'Kohl', '', 'helmut.kohl@germany.com', '', '', 'NO_USER', '', 'Germany', '', '', '', '' );
			germanyAcc.MYR_Status__c='Activated';
			update germanyAcc;
			germanyVin = new VEH_Veh__c( Name = 'VF1CR1J0H40046301' );
			insert germanyVin;
			germanyVRE = new VRE_VehRel__c( Account__c=germanyAcc.Id, VIN__c=germanyVin.Id, Status__c='Active', My_Garage_Status__c='confirmed' );
			insert germanyVRE;
		}
		//Prepare the germany2 account and take the relation of the first account
		//The My_Garage_Status__c should stay in the status 'confirmed' as the trigger will ignore it
		system.runAs( techUserGermany2 ) {
			Account germanyAcc2 = Myr_Datasets_Test.insertPersonalAccountAndUser( 'Helmut', 'Kohl', '', 'helmut.kohl@germany.com', '', '', 'NO_USER', '', 'Germany2', '', '', '', '' );
			germanyAcc2.MYR_Status__c='Activated';
			update germanyAcc2;
			VRE_VehRel__c germanyVRE2 = new VRE_VehRel__c( Account__c=germanyAcc2.Id, VIN__c=germanyVin.Id, Status__c='Active', My_Garage_Status__c='confirmed' );
			insert germanyVRE2;
			//check that the relation has the status confirmed
			germanyVRE2 = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :germanyVRE2.Id];
			system.assertEquals('confirmed', germanyVRE2.My_Garage_Status__c );
		}
		//check that the relation on Germany is still in confirmed status
		germanyVRE = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :germanyVRE.Id];
		system.assertEquals('confirmed', germanyVRE.My_Garage_Status__c );
	}

	//GERMANY 2: test that the close previous is performed on Germany (exclusion not set)
	static testMethod void test_Deactivation_Germany2_GermanyNOTExcluded() { 
		//Get the technical users
		User techUserGermany2 = Myr_Datasets_Test.getTechnicalUser('Germany2');
		User techUserGermany = Myr_Datasets_Test.getTechnicalUser('Germany');
		//Prepare the custom setting
		system.assertEquals(null, CS04_MYR_Settings__c.getInstance().Myr_LimitClosePrevious_Country__c);
		//Prepare a "Germany" account with a linked vehicle
		Account germanyAcc = null;
		VEH_Veh__c germanyVin = null;
		VRE_VehRel__c germanyVRE = null;
		system.runAs( techUserGermany ) {
			germanyAcc = Myr_Datasets_Test.insertPersonalAccountAndUser( 'Helmut', 'Kohl', '', 'helmut.kohl@germany.com', '', '', 'NO_USER', '', 'Germany', '', '', '', '' );
			germanyAcc.MYR_Status__c='Activated';
			update germanyAcc;
			germanyVin = new VEH_Veh__c( Name = 'VF1CR1J0H40046301' );
			insert germanyVin;
			germanyVRE = new VRE_VehRel__c( Account__c=germanyAcc.Id, VIN__c=germanyVin.Id, Status__c='Active', My_Garage_Status__c='confirmed' );
			insert germanyVRE;
		}
		//Prepare the germany2 account and take the relation of the first account
		//The My_Garage_Status__c should stay in the status 'confirmed' as the trigger will ignore it
		system.runAs( techUserGermany2 ) {
			Account germanyAcc2 = Myr_Datasets_Test.insertPersonalAccountAndUser( 'Helmut', 'Kohl', '', 'helmut.kohl@germany.com', '', '', 'NO_USER', '', 'Germany2', '', '', '', '' );
			germanyAcc2.MYR_Status__c='Activated';
			update germanyAcc2;
			VRE_VehRel__c germanyVRE2 = new VRE_VehRel__c( Account__c=germanyAcc2.Id, VIN__c=germanyVin.Id, Status__c='Active', My_Garage_Status__c='confirmed' );
			insert germanyVRE2;
			//check that the relation has the status confirmed
			germanyVRE2 = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :germanyVRE2.Id];
			system.assertEquals('confirmed', germanyVRE2.My_Garage_Status__c );
		}
		//check that the relation on Germany is still in confirmed status
		germanyVRE = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id = :germanyVRE.Id];
		system.assertEquals('taken', germanyVRE.My_Garage_Status__c );
	}
}