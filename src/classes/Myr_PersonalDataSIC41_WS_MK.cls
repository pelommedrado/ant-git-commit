/**
 * Mock Test Class to test the RBX/SIC WebService 
 */
@isTest
global class Myr_PersonalDataSIC41_WS_MK implements WebServiceMock {
	
	public Myr_PersonalDataSIC41_WS.getCustDataResponse_element sicResponse;

    /** @constructor **/
    global Myr_PersonalDataSIC41_WS_MK() {
        sicResponse = buildSICResponse();
    }
    
    /** Simulates invokation of the webservice **/
    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        system.debug('#### Myr_PersonalDataSIC41_WS_MK - doInvoke - BEGIN request=' + request);
        response.put('response_x', sicResponse);
        system.debug('#### Myr_PersonalDataSIC41_WS_MK - doInvoke - END');                                  
    }
    
    
    private Myr_PersonalDataSIC41_WS.getCustDataResponse_element buildSICResponse() {
    	
    	Myr_PersonalDataSIC41_WS.wsInfos wsInfos = new Myr_PersonalDataSIC41_WS.wsInfos();
        wsInfos.wsVersion='Test';
        wsInfos.wsEnv='Test';
        wsInfos.wsDb='Test';
    	
    	Myr_PersonalDataSIC41_WS.Survey pSurveyTest = new Myr_PersonalDataSIC41_WS.Survey();
        pSurveyTest.surveyOK='Test';
        pSurveyTest.value='Test';
        pSurveyTest.loyMessage='Test';
    	
        Myr_PersonalDataSIC41_WS.GetCustDataResponse pTestData = new Myr_PersonalDataSIC41_WS.GetCustDataResponse();
        pTestData.clientList = new List<Myr_PersonalDataSIC41_WS.PersonnalInformation>();
        
        Myr_PersonalDataSIC41_WS.LogsCustData logsCust =  new Myr_PersonalDataSIC41_WS.LogsCustData();
        logsCust.brand='Renault';
        logsCust.mode=1;
        logsCust.country='test';
        logsCust.brand='test';
        logsCust.demander='test';
        logsCust.vin='test';
        logsCust.lastName='test';
        logsCust.firstName='test';
        logsCust.city='test';
        logsCust.zip='test';
        logsCust.ident1='test';
        logsCust.idClient='test';
        logsCust.idMyr='test';
        logsCust.firstRegistrationDate='test';
        logsCust.registration='test';
        logsCust.email='test';
        logsCust.logDate='test';
        logsCust.ownedVehicles='test';
        logsCust.nbReplies='test';
        logsCust.wsVersion='test';
        logsCust.ipAdress='test';
        logsCust.codErreur='test';
        logsCust.dbCible='test';
        logsCust.wsHostname='test';
        logsCust.wsTexec=200001;
        
        Myr_PersonalDataSIC41_WS.RepDynamicComponent repDynamicComp = new Myr_PersonalDataSIC41_WS.RepDynamicComponent();
        repDynamicComp.repCode='test';
        repDynamicComp.typeCalc='test';
        repDynamicComp.ddo='test';
        repDynamicComp.kdo='test';
        repDynamicComp.dp3_0='test';
        repDynamicComp.dp3_1='test';
        repDynamicComp.dp3_2='test';
        repDynamicComp.dp3_3='test';
        repDynamicComp.dp3_4='test';
        repDynamicComp.dp3_5='test';
        repDynamicComp.dp3_6='test';
        repDynamicComp.dp3_7='test';
        repDynamicComp.dp3_8='test';
        repDynamicComp.dp3_9='test';
        Myr_PersonalDataSIC41_WS.VehicleRepDynamic repDynamic = new Myr_PersonalDataSIC41_WS.VehicleRepDynamic();
        repDynamic.repdynList = new List<Myr_PersonalDataSIC41_WS.RepDynamicComponent>();
        repDynamic.repdynList.add(repDynamicComp);
        
        Myr_PersonalDataSIC41_WS.Dealer dealer = new Myr_PersonalDataSIC41_WS.Dealer();
        String birId1 = 'test1';
        String birId2 = 'test2';
        dealer.birId = new List<String>();
        dealer.birId.add(birId1);
        dealer.birId.add(birId2);
        
        Myr_PersonalDataSIC41_WS.CustDataRequest custDataRequest = new Myr_PersonalDataSIC41_WS.CustDataRequest();
        custDataRequest.mode='test';
        custDataRequest.country='test';
        custDataRequest.brand='test';
        custDataRequest.demander='test';
        custDataRequest.vin='test';
        custDataRequest.lastName='test';
        custDataRequest.firstName='test';
        custDataRequest.city='test';
        custDataRequest.zip='test';
        custDataRequest.ident1='test';
        custDataRequest.idClient='test';
        custDataRequest.typeperson='test';
        custDataRequest.idMyr='test';
        custDataRequest.firstRegistrationDate='test';
        custDataRequest.registration='test';
        custDataRequest.email='test';
        custDataRequest.sinceDate='test';
        custDataRequest.ownedVehicles='test';
        custDataRequest.nbReplies='test';
        
        Myr_PersonalDataSIC41_WS.ServicePreferences servPref = new Myr_PersonalDataSIC41_WS.ServicePreferences();
        servPref.irn='test';
        servPref.sia='test';
        servPref.reqid='test';
        servPref.userid='test';
        servPref.language='test';
        servPref.country='test';
        
        Myr_PersonalDataSIC41_WS.GetCustDataRequest getCustData = new Myr_PersonalDataSIC41_WS.GetCustDataRequest();
        getCustData.custDataRequest = custDataRequest;
        getCustData.servicePrefs = servPref;
        
        Myr_PersonalDataSIC41_WS.getCustData_element custDataElement = new Myr_PersonalDataSIC41_WS.getCustData_element();
        custDataElement.request = getCustData;        
        
        Myr_PersonalDataSIC41_WS.GetCustDataError custDataError = new Myr_PersonalDataSIC41_WS.GetCustDataError();
        custDataError.fault='test';
        
        Myr_PersonalDataSIC41_WS.Desc_x descrip = new Myr_PersonalDataSIC41_WS.Desc_x();
        descrip.type_x='test';
        descrip.typeCode='test';
        descrip.subType='test';
        descrip.subTypeCode='test';
        
        Myr_PersonalDataSIC41_WS.WorkShop workshop = new Myr_PersonalDataSIC41_WS.WorkShop();
        workshop.date_x='test';
        workshop.km='test';
        workshop.birId='test';
        workshop.origin='test';
        workshop.description=descrip;
        
        Myr_PersonalDataSIC41_WS.contract contract = new Myr_PersonalDataSIC41_WS.contract();
        contract.idContrat='test';
        contract.type_x='test';
        contract.techLevel='test';
        contract.serviceLevel='test';
        contract.productLabel='test';
        contract.initKm='test';
        contract.maxSubsKm='test';
        contract.subsDate='test';
        contract.initContractDate='test';
        contract.endContractDate='test';
        contract.status='test';
        contract.updDate='test';
        contract.idMandator='test';
        contract.idCard='test';
        
        Myr_PersonalDataSIC41_WS.PersonnalInformation client = new Myr_PersonalDataSIC41_WS.PersonnalInformation();
        client.IdClient = '7026526';
        client.LastName = 'FERRONI';
        client.FirstName = 'ANTONIO';
        client.Title = '3';
        client.MiddleName = 'MiddleName';
        client.Lang = 'ITA';
        client.sex = '2';
        client.birthDay = '1948-12-17';
        client.typeperson = 'P';
        client.typeIdent1 = '1';
        client.ident1 = 'FRRNTN48T17F156W';
        client.ident2 = 'SICIDENT2';
        client.contact = new Myr_PersonalDataSIC41_WS.Contact();
        client.contact.phoneCode1 = '1';
        client.contact.phoneNum1 = '8136853';
        client.contact.phoneCode2 = '2';
        client.contact.phoneNum2 = '8136853';
        client.contact.phoneCode3 = '3';
        client.contact.phoneNum3 = '4858210';
        client.contact.email = 'ROSY.PARLAPARLA@GMAIL.COM';
        client.contact.optin = '0';
        client.address = new Myr_PersonalDataSIC41_WS.Address();
        client.address.strName = 'FAMAGOSTA';
        client.address.strType = 'VLE';
        client.address.strTypeLabel = 'VIALE';
        client.address.strNum = '34';
        client.address.compl1 = 'compl1';
        client.address.compl2 = 'compl2';
        client.address.compl3 = 'compl3';
        client.address.countryCode = 'ITA';
        client.address.zip = '20142';
        client.address.city = 'MILANO';
        client.address.qtrCode = '015';
        client.address.dptCode = '146';
        client.address.sortCode = '0003332';
        client.address.areaCode = 'MI';
        client.address.areaLabel = 'Milano District';
        client.vehicleList = new List<Myr_PersonalDataSIC41_WS.Vehicle>();
        //Dealer List
        client.dealerList = new List<Myr_PersonalDataSIC41_WS.Dealer>();
        Myr_PersonalDataSIC41_WS.Dealer dealer1 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer1.birId = new List<String>{'12345678'};
        Myr_PersonalDataSIC41_WS.Dealer dealer2 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer2.birId = new List<String>{'23456789'};
        Myr_PersonalDataSIC41_WS.Dealer dealer3 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer3.birId = new List<String>{'34567890','45678901'};
        Myr_PersonalDataSIC41_WS.Dealer dealer4 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer4.birId = new List<String>{'56789012'};
        Myr_PersonalDataSIC41_WS.Dealer dealer5 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer5.birId = new List<String>{'67890123'};
        Myr_PersonalDataSIC41_WS.Dealer dealer6 = new Myr_PersonalDataSIC41_WS.Dealer();
        dealer6.birId = new List<String>{'78901234'};
        client.dealerList.add(dealer1);
        client.dealerList.add(dealer2);
        client.dealerList.add(dealer3);
        client.dealerList.add(dealer4);
        client.dealerList.add(dealer5);
        client.dealerList.add(dealer6);
        //Vehicle List
        Myr_PersonalDataSIC41_WS.Vehicle vehicle = new Myr_PersonalDataSIC41_WS.Vehicle();
        vehicle.vin = 'VF17R5A0H48447489';
        vehicle.brandCode = 'RENAULT';
        vehicle.modelCode = 'CK4';
        vehicle.modelLabel = 'CLIO SPORTER';
        vehicle.firstRegistrationDate = '2013-09-26';
        vehicle.registrationDate = '2013-09-27';
        vehicle.registration = 'ES847JH';
        vehicle.possessionBegin = '2013-09-30';
        vehicle.paymentMethod = 'F';
        vehicle.purchaseNature = 'POS';
        vehicle.capacity = '898';
        vehicle.doorNum = '4';
        vehicle.versionLabel = 'DYR 09S E5';
        vehicle.new_x = 'N';
        vehicle.purchaseNature ='POS';
        vehicle.technicalInspectionDate = '2010-06-01';
        
        client.vehicleList.add(vehicle);
        pTestData.clientList.add(client);
        Myr_PersonalDataSIC41_WS.getCustDataResponse_element sicResponse = new Myr_PersonalDataSIC41_WS.getCustDataResponse_element();
		 
		Myr_PersonalDataSIC41_WS.CrmGetCustData crm = new Myr_PersonalDataSIC41_WS.CrmGetCustData();
		Myr_PersonalDataSIC41_WS.GetCustDataResponse res = new Myr_PersonalDataSIC41_WS.GetCustDataResponse();
		//sicResponse = crm.getCustData(getCustData);
		sicResponse.response = pTestData;

        return sicResponse; 
    }
}