@isTest
private class AgendamentoEditarServControlTest {
    
    public static Account acc { get;set; }
    public static Contact cont { get;set; }
    public static VEH_Veh__c vehicle1 { get;set; }
    
    static testMethod void unitTest01() {
        User user = criarUser();
        
        System.runAs(user) {
            
            Opportunity opp1 = new Opportunity();       
            opp1.AccountId = acc.Id;
            opp1.Name = 'OppNameTest_1';
            opp1.StageName = 'Identified';
            opp1.CloseDate = Date.today();
            opp1.CurrencyIsoCode = 'BRL';
            opp1.VehicleRegistrNbr__c = '1000ZB';
            opp1.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'Service');
            opp1.Dealer__c = acc.Id;
            
            insert opp1;
            
            Date hoje = System.today();
            Time horaFixa = Time.newInstance(15, 30, 0, 0);
            DateTime dt = DateTime.newInstance(hoje, horaFixa); //Datetime.now();
            
            Event evento = new Event();
            evento.Subject = 'opp.ServiceType__c';
            evento.WhatId = opp1.Id;
            evento.WhoId = cont.Id;
            evento.StartDateTime = dt;
            evento.EndDateTime = dt.addMinutes(15);
            
            insert evento;
            
            ApexPages.currentPage().getParameters().put('Id', opp1.Id);            
            AgendamentoEditarServicoController ct = new AgendamentoEditarServicoController();
            
            ct.salvar();
            ct.nome = 'Test';
            ct.sobrenome = ' Test';
            ct.salvar();
            ct.acc.CustomerIdentificationNbr__c = '13551258716';
            ct.salvar();
            ct.acc.PersonEmail = 'test@gmail.com';
            ct.salvar();
            ct.opp.ResponsibleTechnical__c = cont.Id;
            ct.salvar();
            ct.opp.ScheduledDate__c = Date.today();
            ct.salvar();
            ct.opp.ScheduledHour__c = '12:00';
            ct.salvar();
            ct.tipoRelText = 'User';
            ct.salvar();
            
            ct.textSearch = '123';
            ct.buscarVeiculo();
            
            ApexPages.currentPage().getParameters().put('hora', '12:00');
			ct.selectHora();
            
            ct.salvarCancelar();
            
            ct.cancelar();
            ct.isCancelar();
            ct.opp.StageName = 'Unperformed';
            ct.isCancelar();
            
            ct.salvarNovo();
        }
    }
    
    static User criarUser() {
        acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '79856523',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_Service__c = 'Available'
        );
        
        Database.insert( cont );
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='79856523',
            isCac__c = true
            
        );
        
        vehicle1 = new VEH_Veh__c(Name = '12111111111111119',
                                  VehicleRegistrNbr__c = '1000ZB',
                                  KmCheckDate__c = Date.today(),
                                  DeliveryDate__c = Date.today() + 30,
                                  VehicleBrand__c= 'Active',
                                  KmCheck__c = 100,
                                  Tech_VINExternalID__c = '9000000009');
        
        Database.insert( vehicle1 );
        
        return manager;
    }
}