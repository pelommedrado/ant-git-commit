/**
*   Class QueryBuilder
*   Classe responsável por gerar as funcionalidades basicas nos geradores de queries dinamicas
*   # 01 - Elvia Serpa - 27/03/2013 - Criação da classe
*/ 
public abstract class VFC99_QueryBuilder 
{
    public Map<String, String> parameters {get; set;}
    public VFC97_Criteria criteria {get; set;}

    public VFC99_QueryBuilder() {
        this.parameters = new Map<String, String>();
        this.criteria = new VFC97_Criteria();
    }
        
    public abstract void buildQuery();
}