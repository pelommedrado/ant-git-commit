public with sharing class VFC97_Criteria {
    
    public String query {get;set;}  
    public String soql  {get;set;} 
    
    public List<SObject> queryResult;
        
    public VFC97_Criteria(){
        soql = '';
    }
    
    public VFC97_Criteria(String query){
        this();
        this.query = query;
        queryResult = new List<SObject>();
    }


    public String toSoqlString () {
        System.debug(soql);
        return ( soql != null && soql != '' ) ? query + ' where ' + soql : query;
    }
    
    public VFC97_Criteria addResultsInto(List<SObject> queryResult) {
        this.queryResult = queryResult  ;   
        return this;
    } 
    
    public VFC97_Criteria add ( VFC97_Criteria criteria , String expression ) { 
        
        if (expression == null || expression == '') return this;
                
        if (criteria.soql != null && soql != '') { 
            criteria.soql += VFC98_Restriction.andd( expression );
        } else {    
            criteria.soql +=  expression;           
        }   
        return this; 
    } 

    public VFC97_Criteria add ( String expression ) {
        return add(this,expression );
    }
    
    public VFC97_Criteria limitedBy ( Integer expression ) {
        this.soql += VFC98_Restriction.addLimit(expression);
        return this;
    }
    
    public VFC97_Criteria orderingBy ( String expression ) {
        this.soql += VFC98_Restriction.orderBy(  expression);
        return this;
    }   
    
    public List<SObject> search(){      
        try {
            System.debug(this.toSoqlString());                          
            queryResult = Database.query(this.toSoqlString());
            
        } catch (System.QueryException e) {
            // ignore Exception
        }
        return queryResult;
    }
    
    public SObject searchFirstResult(){
        
        List<SObject> sObjects = search();
        
        if ( sObjects.isEmpty())
        {
            //throw new NoDataFoundException( soql + query )    ;
        }       
            
                
        return sObjects.get(0);
        
    }
}