/** TestClass for sheduling of purge of customer message
  The goal is not to test the result of the batch called it self
  as it's done into Myr_CustMessagePurge_BAT_TEST
  @author S.Ducamp
  @date 27.07.2015
  @version 1.0 
 */

@isTest
private class Myr_Batch_CustMessage_Purge_SCH_TEST { 
  
  @testSetup static void setCustomSettings() { 
    Myr_Datasets_Test.prepareRequiredCustomSettings();
    Myr_Datasets_Test.prepareCustomerMessageSettings();
    //prepare basic data sets
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts( 1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
  }

    static testMethod void test_schedulable_batch() {
        //Prepare a list of accounts
        Account acc = [SELECT Id FROM Account LIMIT 1];
        //take a template with no merge option
        Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
        system.AssertNotEquals(null, template);
        Customer_Message__c msgToDel = new Customer_Message__c(
          Account__c = acc.Id, 
      Template__c = template.Id, 
      Status__c = system.Label.Customer_Message_UnRead,
      ExpirationDate__c = system.today().addDays(-1),
      Channel__c = 'message', 
      Title__c = 'fake title', 
      Summary__c = 'fake summary', 
      Body__c = 'fake body'
        );
        insert msgToDel;
        
        List<Customer_Message__c> listToDel = [SELECT Id FROM Customer_Message__c WHERE (Status__c='deleted' AND StatusUpdateDate__c < LAST_N_DAYS:30) OR ( ExpirationDate__c < TODAY)];
        system.assertEquals(1, listToDel.size());
        
        Myr_Batch_CustMessage_Purge_SCH schedulableBat = new Myr_Batch_CustMessage_Purge_SCH();
        String schedule  = '0 0 4 * * ?'; //not a real importance: as it is a test, this is executed immediately at Test.soptTest  
        Test.startTest();
        String jobId = system.schedule('MYR Customer Messages Purge', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
        Test.stopTest();
        
        //Check the job
        ApexClass batchClass = [ SELECT Id, Name FROM ApexClass WHERE Name =:Myr_Batch_CustMessage_Purge_BAT.class.getName() ];
        List<AsyncApexJob> jobList = [ SELECT Id, Status FROM AsyncApexJob WHERE ApexClassId = :batchClass.Id AND JobType = 'BatchApex' ];     
        system.AssertEquals(1, jobList.size());
        //Check the result of the job
        //List<Customer_Message__c> listMsg = [SELECT Id FROM Customer_Message__c WHERE Id = :msgToDel.Id];
        //system.AssertEquals(0, listMsg.size()); =====> COULD NOT WORK AS THE SCHEDULABLE CLASS JUST PUTS IN QUEUE THE BATCH WIHOUT EXECUTING IT. 
    }
    
}