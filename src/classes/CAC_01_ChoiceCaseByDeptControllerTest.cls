@isTest
public class CAC_01_ChoiceCaseByDeptControllerTest {

    static testMethod void typerecordtype(){
        CAC_01_ChoiceCaseByDeptControllerTest.createCustonSet();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        ApexPages.StandardController sc = new ApexPages.standardController(lsCase.get(0));
        CAC_01_ChoiceCaseByDeptController sic = new CAC_01_ChoiceCaseByDeptController(sc);
        System.assert(sic.typerecordtype() == null);
    }
    
    
	static testMethod void selectRec(){
        CAC_01_ChoiceCaseByDeptControllerTest.createCustonSet();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        ApexPages.StandardController sc = new ApexPages.standardController(lsCase.get(0));
        
        CAC_01_ChoiceCaseByDeptController sic = new CAC_01_ChoiceCaseByDeptController(sc);
        sic.selectRec();
        sic.typeCaseSelect = 'Parts'; 
        sic.populateDepartment();
        List<SelectOption> populateDirection = sic.populateDirection;
        sic.getAvailableRecordType();
    }
    
    
    private static List<Case> returnNewCase(Id recordType){
        List<Case>lsCase = new List<Case>();
        for(integer i=0; i<30; i++){
            Case cas = new Case(
                Status = 'new',
                Subject__c = 'Diferido (Previsão de Peças)',
                Description = 'teste',
                Dealer_Contact_Name__c = 'teste',
                Contact_Phone__c  = '1122334455',
                Reference__c = 'teste',
                Order_Number__c = '1234567',
                RecordTypeId = recordType
            );
            lsCase.add(cas);
        }
        Database.insert(lsCase,true);
        return lsCase;
        
    }
    
    
    
    private static User createUser(String userName,String typeUserCac){
        Id profileId = Utils.getSystemAdminProfileId();
        String aliasName =  userName;
        if(aliasName.length()>=8)
            aliasName = aliasName.substring(0,7);
        User manager = new User(
            FirstName = aliasName,
            LastName = 'User',
            BypassVR__c = false,
            Email = 'test@org.com',
            Username = userName,
            Alias = userName.substring(0,7),
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = aliasName,
            ProfileId = profileId,
            BIR__c ='123ABC123',
            IsActive = true,
            isCac__c = true,
            roleInCac__c = typeUserCac
        );
        Database.insert( manager,true );
        return manager;
    }
    
    
    
    
    private static void createCustonSet(){
        cacweb_Mapping__c custonSet = new cacweb_Mapping__c(
            name = 'Cacweb_PartsWarehouse',
            Department__c = 'Peças',
            Departamento__c = 'Peças',
            DirectionInEnglish__c = 'Parts',
            Name_Queue__c= 'Cacweb_PartsWarehouse',
            Record_Type_Name__c = 'Cacweb_PartsWarehouse'
        );
        Database.insert(custonSet);
        
        cacweb_settingTime__c custon2 = new cacweb_settingTime__c(
            name = 'Diferido',
            alertExpire__c = 20,
            subject__c = 'Diferido (Previsão de Peças)',
            FinishHourAttendance__c = 17,
            StartHourAttendance__c = 08,
            hoursExpire__c = 500
        );
        Database.insert(custon2);
    }
    
}