/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto Task.
* @author Felipe Jesus Silva.
*/
public class VFC40_TaskBO 
{
    private static final VFC40_TaskBO instance = new VFC40_TaskBO();
    
    /**
    * Construtor privado para impedir a criação de instancias dessa classe.
    */
    private VFC40_TaskBO()
    {
        
    }
    
    /**
    * Método responsável por prover a instância dessa classe.
    */  
    public static VFC40_TaskBO getInstance()
    {
        return instance;
    }
    
    /**
    * Cria tarefas a partir de uma lista de oportunidades. Para cada oportunidade da lista, será criada uma tarefa correspondente.
    * @param lstSObjOpportunity Uma lista de oportunidades.
    */
    public void createTasksFromOpportunities(List<Opportunity> lstSObjOpportunity)
    {
        Task sObjTask = null;
        Map<String, Contact> mapSObjContact = null;
        Contact sObjContact = null;
        Set<String> setAccountId = new Set<String>();
        List<Task> lstSObjTask = new List<Task>();
        
        for(Opportunity sObjOpportunity : lstSObjOpportunity)
        {
            setAccountId.add(sObjOpportunity.AccountId);
        }
        
        /*obtém o mapeamento dos contatos pelo id das contas*/
        mapSObjContact = VFC42_ContactBO.getInstance().getMappingByAccount(setAccountId);
        
        for(Opportunity sObjOpportunity : lstSObjOpportunity)
        {       
            
            String sourceName = '';
            
            if(sObjOpportunity.OpportunitySource__c == 'PHONE'){
                sourceName = 'SAC';
            }else if(sObjOpportunity.OpportunitySource__c == 'TEST DRIVE'){
                sourceName = 'TEST DRIVE AGENDADO';
            }else if(sObjOpportunity.OpportunitySource__c == 'NETWORK'){
                sourceName = 'CONCESSIONÁRIA';
            }else{
                sourceName = sObjOpportunity.OpportunitySource__c;
            }
            
            sObjTask = new Task();          
            sObjTask.WhatId = sObjOpportunity.Id;
            sObjTask.OwnerId = sObjOpportunity.OwnerId;
            sObjTask.Subject = 'Oportunidade ' + sourceName;
            sObjTask.Priority = 'High';
            sObjTask.IsReminderSet = true;
            sObjTask.ActivityDate = Date.today() + 1;
            sObjTask.ReminderDateTime = DateTime.now().addMinutes(-1);
    
            /*obtém o contato relacionado a conta da oportunidade*/
            sObjContact = mapSObjContact.get(sObjOpportunity.AccountId);
            
            if(sObjContact != null)
            {
                sObjTask.WhoId = sObjContact.Id;
            }
            
            /*abaixo são feitas verificações em cima do campo OpportunityTransition__c para determinar a descrição e data de expiração da tarefa*/
            if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER)
            {
                sObjTask.Description = Label.TaskHotLeadToManager;
                sObjTask.ActivityDate = Date.today();
                
            }
            else if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER)
            {
                sObjTask.Description = Label.TaskTestDriveToManager;
                sObjTask.ActivityDate = Date.today();
            }
            else if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER)
            {
                sObjTask.Description = Label.TaskHotLeadManagerToSeller;
                sObjTask.ActivityDate = Date.today();
                
            }
            else if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.TEST_DRIVE_MANAGER_TO_SELLER)
            {
                sObjTask.Description = Label.TaskTestDriveManagerToSeller;
                sObjTask.ActivityDate = Date.today();
            }
            else if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.HOT_LEAD_RECEPTIONIST_TO_SELLER)
            {
                sObjTask.Description = Label.TaskHotLeadReceptionistToSeller;
                sObjTask.ActivityDate = Date.today();
            }
            else if(sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.TEST_DRIVE_RECEPTIONIST_TO_SELLER)
            {
                sObjTask.Description = Label.TaskTestDriveReceptionistToSeller;
                sObjTask.ActivityDate = Date.today();
            }
            else
            {
                sObjTask.Description = Label.TaskPassingReceptionistToSeller;               
                sObjTask.ActivityDate = Date.today();
            }
            
            
            /* Incluido devido a bug com tarefas */
            if(sObjOpportunity.OpportunitySource__c != 'TEST DRIVE'){
                lstSObjTask.add(sObjTask);    
            }else if(sObjTask.WhatId != null){
                lstSObjTask.add(sObjTask);
            }
            
        }
        
        /*insere a lista de tarefas*/
        VFC33_TaskDAO.getInstance().insertData(lstSObjTask);
    }
    
    public void closeTasksFromOpportunities(List<Opportunity> lstSObjOpportunity)
    {
        List<Task> lstSObjTask = null;
        Set<String> setOpportunityId = new Map<String, Opportunity>(lstSObjOpportunity).keySet();
        
        /*localiza todas tarefas desse conjunto de oportunidades que NÃO estejam encerradas*/
        lstSObjTask = VFC33_TaskDAO.getInstance().findByOpportunityId(setOpportunityId, 'Completed');
        
        for(Task sObjTask : lstSObjTask)
        {
            sObjTask.Status = 'Completed';
        }
        
        VFC33_TaskDAO.getInstance().updateData(lstSObjTask);
    }
}