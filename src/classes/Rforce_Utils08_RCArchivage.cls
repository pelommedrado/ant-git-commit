public class Rforce_Utils08_RCArchivage{
    
    private static String endpoint = System.label.VFP08_RCArchivesURL2;
    private static String certificate = System.label.clientCertNamex; 
  //  private static Integer timeout = 80000;
    private static Integer timeout = 120000;
  

      /** @return the list of the cases linked to the given vehicle **/
    public Rforce_RcArchivage.caseId[] getCaseIdList(String VehiculeVin,String countryCodeValue){
        String vin = VehiculeVin; 
        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;      
        RCARCH_WS.timeout_x = timeout;
        Rforce_RcArchivage.caseId[] RCARCH_RWS = RCARCH_WS.getCaseIdList(countryCodeValue, vin); 
        System.debug('Case Id List for'+VehiculeVin+'contains'+ RCARCH_RWS ); 
        return RCARCH_RWS;
    } 
    
      /** @return the list of the cases linked to the given Account **/
     public Rforce_RcArchivage.caseId[] getCaseIdListAccount(String LastName,String FirstName,String ZipCode,String City,String PhoneNumber,String BusinessName,String BusinessCustomId,String PersonCustomId){

        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;      
        RCARCH_WS.timeout_x = timeout;
        System.debug('Sur Rforce_Utils08_RCArchivage L:'+LastName+'-F:'+FirstName+'-Z:'+ZipCode+'-C:'+City+'-P:'+PhoneNumber+'-B:'+BusinessName+'-BCID:'+BusinessCustomId+'-PCID:'+PersonCustomId);
           
        Rforce_RcArchivage.caseId[] RCARCH_RWS = RCARCH_WS.getCaseIdListAccount(LastName, FirstName, ZipCode, City, PhoneNumber, BusinessName,BusinessCustomId,PersonCustomId); 
        
        System.debug('Sur Rforce_Utils08_RCArchivage Resultat:'+RCARCH_RWS);
         
        return RCARCH_RWS;
    } 
     /** @return the list of the cases linked to the given Account **/
     public Rforce_RcArchivageEnhanced.caseDetailsList[] getCaseIdListAccountConcat(String LastName,String FirstName,String ZipCode,String City,String PhoneNumber,String BusinessName,String BusinessCustomId,String PersonCustomId,String countryCode,String LocalId,String Email){

        Rforce_RcArchivageEnhanced.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivageEnhanced.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;      
        RCARCH_WS.timeout_x = timeout;
        System.debug('Sur Rforce_Utils08_RCArchivage L:'+LastName+'-F:'+FirstName+'-Z:'+ZipCode+'-C:'+City+'-P:'+PhoneNumber+'-B:'+BusinessName+'-BCID:'+BusinessCustomId+'-PCID:'+PersonCustomId);
           
        Rforce_RcArchivageEnhanced.caseDetailsList[] RCARCH_RWS = RCARCH_WS.getCaseIdListAccountConcat(LastName, FirstName, ZipCode, City, PhoneNumber, BusinessName,BusinessCustomId,PersonCustomId,countryCode,LocalId,Email); 
        
        System.debug('Sur Rforce_Utils08_RCArchivage Resultat:'+RCARCH_RWS);
         
        return RCARCH_RWS;
    } 
     /** @return the details for a given case id **/
    public Rforce_RcArchivage.caseDetails getCaseDetails(String caseId,String countryCodeValue) {    
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        Rforce_RcArchivage.caseDetails RCARCH_RWS = RCARCH_WS.getCaseDetails('RUS', caseId);
        
        return RCARCH_RWS; 
    }
    
     /** @return the details for a given case id **/
    public String getXmlDetails(String countryvalue,String caseId) {
        // ---- WEB SERVICE CALLOUT -----    
        
        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        String ConvertedText='';
        String RCARCH_RWS='';
        try
        {
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        RCARCH_RWS = RCARCH_WS.getXmlDetails(countryvalue,caseId);
        
        System.debug('RCARCH_RWS>>>>>>>>>>>>>>'+RCARCH_RWS);
            
         if(RCARCH_RWS!=null)
         {
         Blob b=EncodingUtil.base64Decode(RCARCH_RWS);
         ConvertedText=b.toString();
         }  
        }
         catch(CalloutException e) {
         system.debug ('RCARCH-GETATTCHAMENTCONTENT, CALLOUT EXCEPTION'+e.getMessage());
           
       } catch (Exception e) {
            system.debug ('RCARCH-GETATTCHAMENTCONTENT, UNKNOWN EXCEPTION'+e.getMessage());
       }
         System.debug('ConvertedText>>>>>>>>>>>'+ConvertedText);
         return ConvertedText;
    }
    
    
    
    /** @return the list of attachements for a given case **/
    public Rforce_RcArchivage.attachmentDetailsList[] getAttachmentDetailsList(String countryCode, String caseId) {
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        Rforce_RcArchivage.attachmentDetailsList[] RCARCH_RWS;
        
        try{
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        RCARCH_RWS = RCARCH_WS.getAttachmentDetailsList(countryCode, caseId); 
        System.debug('RCARCH_RWS===>'+RCARCH_RWS);
        }  
        
        catch(CalloutException e) {
         system.debug ('Rforce_RcArchivage.attachmentDetailsList, CALLOUT EXCEPTION'+e.getMessage());
           
       } 
       catch(NoDataFoundException e){
       system.debug ('Rforce_RcArchivage.attachmentDetailsList, NoDataFoundException EXCEPTION'+e.getMessage());
       }
       catch(Exception e) {
            system.debug ('Rforce_RcArchivage.attachmentDetailsList, UNKNOWN EXCEPTION'+e.getMessage());
       }
        return RCARCH_RWS;  
    }
    
    /** @return the content of the given attachment **/
   /* public String getAttachmentContent(String ParentId,String caseId, String attachmentName) {*/
     /*   // ---- WEB SERVICE CALLOUT -----    
        WS_RCArch_Case_REST WSARCH = new WS_RCArch_Case_REST();
        String doc = WSARCH.getAttachmentContent(ParentId, caseId, attachmentName);
        return doc; 
        
        */

      /*  Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        String Response = RCARCH_WS.getAttachmentContent(ParentId, caseId, attachmentName);
        
        System.debug('Response for the getAttachmentContent in WSRCArch Utils 2 is >>>>>>>>'+Response);
        
        return Response;  
       
    }*/

/** @return the content of the given attachment **/
    public String getAttachmentContent(String ParentId,String caseId, String attachmentName) {
     /*   // ---- WEB SERVICE CALLOUT -----    
        WS_RCArch_Case_REST WSARCH = new WS_RCArch_Case_REST();
        String doc = WSARCH.getAttachmentContent(ParentId, caseId, attachmentName);
        return doc; 
        
        */
        system.debug('######## Inside getAttachmentContent NEWWWWWWW ########');
        String Response='';
        Rforce_RcArchivageAttachment.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivageAttachment.ServiceImplementationPort();
       // try{
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        System.debug('Maximum Timeout in Webservice'+RCARCH_WS.timeout_x);
        Rforce_RcArchivageAttachment.attachmentContentDetails contentDetails = new Rforce_RcArchivageAttachment.attachmentContentDetails();
        /*
        Rforce_RcArchivage.ServiceImplementationPort RCARCH_WS = new Rforce_RcArchivage.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        */
        system.debug('## Before invoking RCARCH_WS.getAttachmentContent ##');
       
        contentDetails = RCARCH_WS.getAttachmentContent(ParentId, caseId, attachmentName);
        
        //String Response = RCARCH_WS.getAttachmentContent(ParentId, caseId, attachmentName);
        Response = contentDetails.attachmentContent;
        
        System.debug('Response for the getAttachmentContent in WSRCArch Utils 2 is >>>>>>>>'+Response);
      //  }
       /* catch(CalloutException e) {
         system.debug ('RCARCH-GETATTCHAMENTCONTENT, CALLOUT EXCEPTION'+e.getMessage());
           
       } 
       catch(NoDataFoundException e){
       system.debug ('RCARCH-GETATTCHAMENTCONTENT, NoDataFoundException EXCEPTION'+e.getMessage());
       }
       catch(Exception e) {
            system.debug ('RCARCH-GETATTCHAMENTCONTENT, UNKNOWN EXCEPTION'+e.getMessage());
       }
       */ 
        return Response;  
       
    }

}