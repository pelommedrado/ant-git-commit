/*****************************************************************************************
    Name    : BatchToUpdatevehicle2
    Desc    : This is to make Webservice callout to Update vehicle Details
    Approach: Batchapex Interface
                

******************************************************************************************/
global class BatchToUpdatevehicle2 implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

List<VEH_Veh__c> list_vehicleType=new List<VEH_Veh__c>();
VEH_Veh__c vehicleobject;

    global Database.Querylocator start (Database.Batchablecontext BC){
    

   
          return Database.getQueryLocator('Select Id,Name from VEH_Veh__c where BVM_data_Upto_date__c!=true and Id IN (Select VIN__c From VRE_VehRel__c where Account__r.MyRenaultID__c!=null)');
       
                     
    }
    global void execute (Database.Batchablecontext BC, list<VEH_Veh__c> scope){
     
         for(VEH_Veh__c s : scope){
           if (s.name != null){
           vehicleobject=  VFC13_UpdateVehicleCallout.getVehicleDetails(s.name,s.id);
           list_vehicleType.add(vehicleobject);
           }               
         }
         
         Database.update(list_vehicleType,false);
     
   }  
   global void finish(Database.Batchablecontext BC){

       

    }    
          
}