@isTest
private class OpportunityTransactionBoTest {

	private static User communityUser;
	private static Account dealerAcct, clientAcct;
	private static TDV_TestDriveVehicle__c testDriveVehicle;
	private static VEH_Veh__c vehicle;

	static {
			communityUser = MyOwnCreation.getInstance().CriaUsuarioComunidade();
			dealerAcct = [SELECT Name, RecordTypeId, IDBIR__c FROM Account WHERE IDBIR__c = '123456'];

			clientAcct = MyOwnCreation.getInstance().criaPersAccount();
			clientAcct.VehicleInterest_BR__c = 'FLUENCE';
			Database.insert(clientAcct);

			vehicle = new VEH_Veh__c(
					Name = '12111111111111119',
					VehicleRegistrNbr__c = '1000ZB',
					KmCheckDate__c = Date.today(),
					DeliveryDate__c = Date.today() + 30,
					VehicleBrand__c= 'Active',
					KmCheck__c = 100,
					Tech_VINExternalID__c = '9000000009');
			Database.Insert(vehicle);

			testDriveVehicle = new TDV_TestDriveVehicle__c(
					Account__c = dealerAcct.Id,
					AgendaOpeningDate__c = system.today(),
					AgendaClosingDate__c = system.today() + 30,
					Available__c = true,
					Vehicle__c = vehicle.Id);
			Database.Insert(testDriveVehicle);
	}

	@isTest static void deveGetTransactionStageName() {
		final Opportunity opp = createOppQuote();
		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		TST_Transaction__c transDb =
				opportunityTransactionBo.getTransactionStageName(opp);
		//System.assert(transDb.Opportunity_Status__c == 'Identified');
	}

	@isTest static void deveCriarMapComUltimaTransaction() {
		final Opportunity opp = createOppQuote();

		TST_Transaction__c trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;
		wait(1000);
		trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;
		wait(1000);
		trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);
		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());
		final Map<Id, List<TST_Transaction__c>> mapTrans =
			opportunityTransactionBo.createMapTransactionLast(oppList);

		final List<TST_Transaction__c> transLastList =	mapTrans.get(opp.Id);

		final List<TST_Transaction__c> transList =
			opportunityTransactionBo.obterTransactions(oppList);

		System.assertEquals(transLastList.get(0).Id, trans.Id);
	}

	@isTest static void deveObterTransactionListOpportunity() {
		final Opportunity opp = createOppQuote();

		TST_Transaction__c trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;
		trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;
		trans = TST_TransactionBuild.createTransaction(opp);
		INSERT trans;

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		final List<TST_Transaction__c> transList =
			opportunityTransactionBo.obterTransactions(oppList);
		System.assertEquals(3, transList.size());
	}

	@isTest static void deveObterTestDriveListOpportunity() {
		final Opportunity opp = createOppQuote();
		
		Test.startTest();

		TDV_TestDrive__c testDrive = createTestDrive(opp, createDatetime(1));
		INSERT testDrive;
		testDrive = createTestDrive(opp, createDatetime(2));
		INSERT testDrive;
		testDrive = createTestDrive(opp, createDatetime(3));
		INSERT testDrive;

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());
			
		Test.stopTest();

		final List<TDV_TestDrive__c> driveList = opportunityTransactionBo.obterTestDrives(oppList);
		System.assert(driveList.size() == 3);
	}

	@isTest static void deveDefinirVehicleInterest() {
		
		Test.startTest();
		
		final Opportunity opp = createOppQuote();
		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
		new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());
			
		Test.stopTest();

		final Opportunity oppDb = [SELECT Id, StageName, VehicleInterest__c FROM Opportunity WHERE Id=:opp.Id];
		final String result = opportunityTransactionBo.definirVehicleInterest(oppDb);
		System.assert(result.equals('FLUENCE'));
	}

	@isTest static void deveDefinirVehicleInterestTestDrive() {
		final Opportunity opp = createOppQuote();
		TDV_TestDrive__c testDrive = createTestDrive(opp, createDatetime(1));
		INSERT testDrive;
		opp.StageName = 'Test Drive';
		UPDATE opp;
		
		Test.startTest();

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		final Opportunity oppDb = [
			SELECT Id, StageName, VehicleInterest__c
			FROM Opportunity WHERE Id=:opp.Id
		];
		
		Test.stopTest();
		
		final String result = opportunityTransactionBo.definirVehicleInterest(oppDb);
		System.assert(result.equals('FLUENCE'));
	}

	@isTest static void deveDefinirVehicleInterestQuote() {
		Test.startTest();
		final Opportunity opp = createOppQuote();
		opp.StageName = 'Quote';
		UPDATE opp;

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		final Opportunity oppDb = [
			SELECT Id, StageName, VehicleInterest__c
			FROM Opportunity WHERE Id=:opp.Id
		];
		final String result = opportunityTransactionBo.definirVehicleInterest(oppDb);
		System.assert(result.equals('FLUENCE'));
		Test.stopTest();
	}

	@isTest static void deveCriarTransactions() {
		Test.startTest();
		final Opportunity opp = createOppQuote();
		UPDATE opp;
		TDV_TestDrive__c testDrive = createTestDrive(opp, createDatetime(1));
		INSERT testDrive;
		opp.StageName = 'Test Drive';
		UPDATE opp;

		opp.StageName = 'Quote';
		UPDATE opp;

		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		final List<TST_Transaction__c> transList = opportunityTransactionBo.obterTransactions(oppList);
		System.assertEquals(3, transList.size());
		Test.stopTest();
	}

	/*@isTest static void deveRetornarExceptionDefinirVehicleInterestOrder() {
		final Opportunity opp = createOppQuote();
		opp.StageName = 'Order';
		try {
			UPDATE opp;
			System.Assert(false);
		} catch (Exception ex) {
			System.Assert(ex.getMessage().contains('Oportunidades na fase de Orçamento'));
		}
	}*/

	@isTest static void deveCalcularStageDuration() {
		final Opportunity opp = createOppQuote();
		final List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);

		final OpportunityTransactionBo opportunityTransactionBo =
			new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());

		final Opportunity oppDb = [SELECT Id, StageName, CreatedDate FROM Opportunity WHERE Id=:opp.Id];
		final Decimal result = opportunityTransactionBo.calcularStageDuration(oppDb);
	}

	public static void wait(Integer millisec) {

	 if(millisec == null || millisec < 0) {
		 millisec = 0;
	 }

	 Long startTime = DateTime.now().getTime();
	 Long finishTime = DateTime.now().getTime();
	 while ((finishTime - startTime) < millisec) {
		 //sleep for parameter x millisecs
		 finishTime = DateTime.now().getTime();
	 }
  }

	private static DateTime createDatetime(Integer hora) {
		return Datetime.newInstanceGmt(
			System.now().yearGmt(), System.now().monthGmt(), System.now().dayGmt(),
			system.now().hourGmt() + hora, 0, 0);
	}
	private static TDV_TestDrive__c createTestDrive(Opportunity oppt, DateTime dateOfBooking) {
		TDV_TestDrive__c testDrive1 = new TDV_TestDrive__c(
				DateBooking__c = dateOfBooking,
				Dealer__c = dealerAcct.Id,
				TestDriveVehicle__c = testDriveVehicle.Id,
				Opportunity__c = oppt.Id,
				Status__c = 'Scheduled');
		return testDrive1;
	}

	private static Opportunity createOppQuote() {
		Opportunity oppt = new Opportunity(
				Name = 'Opportunity Test',
				StageName = 'Identified',
				CloseDate = System.today().addDays(5),
				AccountId = clientAcct.Id
		);
		Database.Insert(oppt);

		Quote quote = MyOwnCreation.getInstance().criaQuote();
		quote.OpportunityId = oppt.Id;
		Database.Insert(quote);
		return oppt;
	}
}