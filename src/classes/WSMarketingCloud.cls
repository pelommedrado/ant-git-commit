public class WSMarketingCloud {
	
	private static String client_secret 	{get; set;}
    private static String client_id 		{get; set;}
	private static String accessToken       {get; set;}
	 
    private static Map<String, marketing_cloud_configuration__c> mc {get;set;}
    
    public WSMarketingCloud(){
        getConfiguration();
    }
    
    //Get a Custom Config. Salesforce Marketing Cloud
    public static void getConfiguration(){
        mc = new Map<String, marketing_cloud_configuration__c>();
        
        mc = marketing_cloud_configuration__c.getAll();
        
        //Auth Marketing Cloud
        if(!Test.isRunningTest()){
            client_secret       = mc.get('default').clientSecret__c;
            client_id           = mc.get('default').clientId__c;
        }
    }
    
    
     public static void login(){
        try{
         	accessToken = '';
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            
            req.setEndpoint('https://auth.exacttargetapis.com/v1/requestToken');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
           
            req.setBody('{"clientId": "' + client_id + '","clientSecret": "' + client_secret + '"}');
            
            res = http.send(req);
            
            JSONParser parser = JSON.createParser(res.getBody());
            
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    
                    String text = parser.getText();
                    
                    parser.nextToken();
                    if (text == 'accessToken') {
                        accessToken = parser.getText();
                        System.debug('accessToken&&' + accessToken);
                        break;
                    } else {
                        System.debug(LoggingLevel.WARN, 'JsonClassifier consuming unrecognized property: '+text);
                       
                    }
                }
            }
            
        }catch(Exception e){
           
        }   
       
    }
    
    public static void sendSMS(Account acc, Integer numOppExpired){
        if(numOppExpired > 0){
            if(acc.DealerDirectorPhone__c != null && acc.DealerDirectorPhone__c != '' ){
                if(acc.DealerDirectorFstName__c != null && acc.DealerDirectorFstName__c != ''){
                    send(acc.DealerDirectorFstName__c, acc.DealerDirectorPhone__c, acc.IDBIR__c, numOppExpired);
                }
            }
            
            if(acc.DealerManagerPhone__c != null && acc.DealerManagerPhone__c != '' ){
                if(acc.DealerManagerFstName__c != null && acc.DealerManagerFstName__c != ''){
                    send(acc.DealerManagerFstName__c, acc.DealerManagerPhone__c, acc.IDBIR__c, numOppExpired);
                }
             }
             
             if(acc.OwnerPhone__c != null && acc.OwnerPhone__c != '' ){
                if(acc.DealerOwner__c != null && acc.DealerOwner__c != ''){
                    send(acc.DealerOwner__c, acc.OwnerPhone__c, acc.IDBIR__c, numOppExpired);
                }
              }
        } 
        
    }
   
  
    private static void send(String name, String phone, String idBir, Integer numOppExpired ){
        String urlRequest = 'https://www.exacttargetapis.com/sms/v1/messageContact/Nzo3ODow/send';
        String body = '';
          
        String mensagem = '';
   
        body = '{ '; 
    	body += ' "mobileNumbers": [';
    	body += ' "'+phone +'" ';
    	body += '],';
    	body += '"Subscribe": true,';
    	body += '"Resubscribe": true,';
    	body += '"keyword": "RENAULT",'; 
    	body += '"Override": true,';
   		body += ' "messageText": " Ola '+ name +'! Alertamos que a BIR '+ idBir + ' possui '+ numOppExpired +' leads nao atendidos no prazo e pendentes com vendedores. Acesse o R+C para mais detalhes." ';
        body += ' }';
       
       	HttpRequest req 	= new HttpRequest();
        HTTPResponse res 	= new HTTPResponse();
        req.setEndPoint(urlRequest);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        
        req.setMethod('POST');
        req.setCompressed(true);
        req.setBody(body);
        
      
        
        if(!Test.isRunningTest()){
            Http http = new Http();
            res = http.send(req);
        }
        
        System.debug('body&&' + body);
        System.debug('accessToken&&' + accessToken);
        System.debug('req&&' + req);
        System.debug('res&&' + res); 
    }
      
        
}