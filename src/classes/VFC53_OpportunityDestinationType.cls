/**
* Enum que define os tipos de destino da oportunidade.
* @author Felipe Jesus Silva.
*/
public enum VFC53_OpportunityDestinationType 
{
	QUALIFYING_ACCOUNT,
	TEST_DRIVE,
	QUOTE
}