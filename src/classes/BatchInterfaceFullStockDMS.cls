global class BatchInterfaceFullStockDMS implements Database.Batchable<sObject> {
    
    String query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        query = 'SELECT Status__c, Account__r.RecordTypeId, VIN__r.Status__c, VIN__r.Received_By_Stock_Full__c ' +
                'FROM VRE_VehRel__c WHERE Status__c = \'Active\'' +
                'AND Account__r.RecordTypeId = \'' + Utils.getRecordTypeId('Account', 'Network_Site_Acc') + '\'' +
                'AND (VIN__r.Status__c = \'Available\'' +
                'OR VIN__r.Status__c = \'Booking\'' +
            	'OR VIN__r.Status__c = \'Quality Blockade\'' +
            	'OR VIN__r.Status__c = \'Comercial Blockade\')' +
            	'AND VIN__r.Received_By_Stock_Full__c = false';
        
        System.debug('** query: ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<VRE_VehRel__c> vehrel){
        
        System.debug('***** VEHICLE RELATION LIST: ' + vehrel);
        // Lista para receber veículos a serem atualizados
        List<VEH_Veh__c> vehToUpdate = new List<VEH_Veh__c>();
        
        //Preenche lista de veículos
        for(Integer i = 0; i<vehrel.size();i++){
            VEH_Veh__c v = new VEH_Veh__c();
            v.Id = vehrel[i].VIN__r.Id;
            v.Status__c = 'Inconsistent';
            vehToUpdate.add(v);
        }
        
        System.debug('**** VEHICLES TO UPDATE: ' + vehToUpdate);
        //Atualiza veículos
        if(vehToUpdate != null || !vehToUpdate.isEmpty()){
            try{
                update vehToUpdate;
            } catch (Exception e){
                System.debug('** Error updating vehicles: ' + e.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){}
}