/* Apex class used to proxify the custom settings to avoid the application
	of the global salesforce CRUD within VF page
**/
public class Myr_AdministrationGUI_Proxify_CLS {

	/** Inner class to track modification for logging purposes **/
	public class Modify {
		public String fieldName;
		public String newValue;
		public String oldValue;
		//@inner constructor
		public Modify(String name, String n, String o) {
			this.fieldName = name;
			this.newValue = n;
			this.oldValue = o;
		}
	}

	/* Proxify the fields to avoid application of global SalesForce CRUD on the VF page **/
	public class ProxyField implements Comparable {
		public String name {get; set;}
		public String type {get; set;}
		public Object value {get; set;}
		public ProxyField( String n, String t, Object v) {
			system.debug('### Myr_AdministrationGUI_Proxify_CLS name=' + n + ', type=' + t + ', value=' + v);
			name = n;
			type = t;
			value = v;
		}
		//the field could be edited using a inputtext
		public Boolean getInText() {
			return ('STRING' == type || 'DOUBLE' == type);
		}
		//the field could be dited using a checkbox
		public Boolean getInBoolean() {
			return ('BOOLEAN' == type);
		}
		//the field is not editable
		public Boolean getInputeable() {
			return (getInText() || getInBoolean());
		}
		//compute the right value given the original one
		public Object computeValue(Object inValue) {
			String strValue = String.valueOf(inValue);
			if( String.isBlank(strValue) && 'STRING' == type ) {
				return inValue;
			} else if( String.isBlank(strValue) && 'STRING' != type) {
				return null;
			}
			return inValue;
		}
		//return the new value
		public Object getValue() {
			return computeValue( this.value );
		}
		//compare the original field with the possibly modified one
		public Integer compareTo(Object origValue) {
			String current = String.ValueOf(getValue());
			String orig = String.valueOf(computeValue( origValue ));
			if( ('null'.equalsIgnoreCase(current) || current == null || (current != null && current.length() == 0) )
				&& ('null'.equalsIgnoreCase(orig) || orig == null || (orig != null && orig.length() == 0) ) ) {
				return 0;
			} else if (current == orig) {
				return 0;
			} else {
				return 1;
			}	
		}
	}

	/* Create a proxy setting of any sobject to avoid the application of the CRUD on a VF Page **/
	public class ProxySetting {
		public SObject original {get; private set;}
		public Map<String, ProxyField> Fields {get; set;}
		private List<Modify> Modifications;
		//@inner constructor
		public ProxySetting(SObject original) {
			Fields = new Map<String, ProxyField>();
			this.original = original;
		}
		//add a field for the proxy
		public void addField( ProxyField p ) {
			Fields.put( p.name, p );
		}
		//copy the proxy values within the SObject
		public SObject copySettings() {
			Modifications = new List<Modify>();
			for( String f : Fields.keySet() ) {
				system.debug( '### - Myr_AdministrationGUI_Proxify_CLS - <copySettings> - EXISTING: ' + original.get(f) );
				system.debug( '### - Myr_AdministrationGUI_Proxify_CLS - <copySettings> - PROXY   : ' + Fields.get(f).value );
				if( Fields.get(f).compareTo(original.get(f)) != 0 ) {
					Modifications.add( new Modify( f, String.valueOf(Fields.get(f).getValue()), String.valueOf(original.get(f)) ) );
					if( 'DOUBLE' ==  Fields.get(f).type ) {
						system.debug( '### - Myr_AdministrationGUI_Proxify_CLS - <copySettings> - DOUBLE: ' + Fields.get(f).value );
						original.put( f, Double.valueOf(Fields.get(f).getValue()) );
					} else {
						original.put( f, Fields.get(f).getValue() );
					}
				}
			}
			return original;
		}

		public Boolean hasBeenModified() {
			return Modifications.size() > 0;
		}

		/* @return the list of modifications observed **/
		public List<Modify> getModifications() {
			return Modifications;
		}
	}

	/**
	* @description Create a proxy of the given sObject
	* @param setting the sobject to proxify
	* @param fields the field list to use (which possibly a subset of the field of the given object
	* @param sobjectname the name of the sobject to proxify 
	* @return ProxySetting 
	*/
	public static ProxySetting proxify(sObject setting, List<String> fields,  String sobjectname ) {
		ProxySetting proxy = new ProxySetting(setting);
		Map<String, Schema.SObjectField> mapFields = Schema.getGlobalDescribe().get(sobjectname).getDescribe().fields.getMap();
		for( String f : fields ) {
			ProxyField p = new ProxyField( f, mapFields.get(f).getDescribe().getType().name(), setting.get(f) );
			proxy.addField( p );
		}
		return proxy;
	} 

	/**
	* @description Create a proxy of the given sObject
	* @param setting the sobject to proxify
	* @param fields the field list to use (which possibly a subset of the field of the given object
	* @param sobjectname the name of the sobject to proxify 
	* @return ProxySetting 
	*/
	public static ProxySetting proxify(sObject setting, Set<String> fields,  String sobjectname ) {
		List<String> listFields = new List<String>();
		listFields.addAll( fields );
		return proxify(setting, listFields, sobjectname );
	}

 }