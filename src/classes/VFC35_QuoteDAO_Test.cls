/**
	Class   -   VFC35_QuoteDAO_Test
    Author  -   Suresh Babu
    Date    -   22/01/2013
    
    #01 <Suresh Babu> <22/01/2013>
        Created this class using test for VFC35_QuoteDAO.
**/
@isTest
private class VFC35_QuoteDAO_Test {

    @testSetup
    static void setup(){

        Account dealer = MyOwnCreation.getInstance().criaAccountDealer();
        Insert dealer;

        Opportunity opp = MyOwnCreation.getInstance().criaOpportunity();
        opp.Dealer__c = dealer.Id;
        Insert opp;

        Quote q = MyOwnCreation.getInstance().criaQuote();
        q.OpportunityId = opp.Id;
        Insert q;

    }

    static testMethod void test1(){

        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Set<Id> oppSet = new Set<Id>();
        oppSet.add(oppId);

        Test.startTest();

        VFC35_QuoteDAO.getInstance().fetchQuoteRecords_UsingOpportunityIDs(oppSet);

        Test.stopTest();
    }

    static testMethod void test2(){

        String quoteNumber = [SELECT Id, QuoteNumber FROM Quote LIMIT 1].QuoteNumber;
        Id dealerId = [SELECT Id FROM Account LIMIT 1].Id;

        Set<String> quoteNumberSet = new Set<String>();
        quoteNumberSet.add(quoteNumber);

        Test.startTest();

        VFC35_QuoteDAO.getInstance().fetchQuoteRecords_UsingQuoteNumber_DealerId(quoteNumberSet,dealerId);

        Test.stopTest();
    }

    static testMethod void test3(){

        Id dealerId = [SELECT Id FROM Account LIMIT 1].Id;

        Test.startTest();

        VFC35_QuoteDAO.getInstance().fetchQuoteRecords_UsingDealerId(dealerId);

        Test.stopTest();
    }

    static testMethod void test4(){

        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Test.startTest();

        VFC35_QuoteDAO.getInstance().findByOpportunityId(oppId);

        Test.stopTest();
    }

    static testMethod void test5(){

        Id qId = [SELECT Id FROM Quote LIMIT 1].Id;

        Test.startTest();

        VFC35_QuoteDAO.getInstance().findById(qId);

        Test.stopTest();
    }

    static testMethod void test6(){

        String qId = [SELECT Id FROM Quote LIMIT 1].Id;

        Test.startTest();

        VFC35_QuoteDAO.getInstance().fetchById(qId);

        Test.stopTest();
    }

    static testMethod void test7(){

        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Quote q = MyOwnCreation.getInstance().criaQuote();
        q.OpportunityId = oppId;

        Test.startTest();

        VFC35_QuoteDAO.getInstance().insertData(q);

        Test.stopTest();
    }

}