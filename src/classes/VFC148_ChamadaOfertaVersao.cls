public class VFC148_ChamadaOfertaVersao{


List<String> versao = new List<String>();
List<String> versao_removida = new List<String>();
//todasVersoes  recebe todas as vers?es
List<SelectOption> todasVersoes = new List<SelectOption>();
//versao2 recebe as vers?es selecionadas
List<SelectOption> versao2 = new List<SelectOption>();
List<PVVersion__c> vss = new List<PVVersion__c>();
Set<String> versoesSelecionadas = new Set<String>();

public Map<String,String> mapaVersaoId = new Map<String, String>();
private ApexPages.StandardSetController controller;

private final PVCall_Offer__c callOffer;

public VFC148_ChamadaOfertaVersao(){}

public VFC148_ChamadaOfertaVersao(ApexPages.StandardController controller) {
	      String idChamada = ApexPages.currentPage().getParameters().get('id');
        callOffer = [SELECT Id, Name, Commercial_Action__c, PV_CH_checkStatus__c FROM PVCall_Offer__c 
            WHERE Id = :idChamada];
        init(callOffer.Id);
        
}

public void deletarVersoes(String chamadaOferta, Set<String> unselectedIds){
	
	  List<VersionCallOffer__c> versoesDeletar = [Select Id From VersionCallOffer__c where Call_Offer__c=:chamadaOferta and Version__c in:unselectedIds];
    
    if(versoesDeletar != null && versoesDeletar.size() > 0)
      delete versoesDeletar;
      
    
    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Versoes da chamada atualizadas com sucesso!');
    ApexPages.addMessage(msg);
    
}

public void init(String chamadaOferta){
	  List<VersionCallOffer__c> selecionadas = [Select Id, Version__c From VersionCallOffer__c where Call_Offer__c =:chamadaOferta];
	  List<Id> versionsSelec = new List<Id>();
	  for(VersionCallOffer__c vco : selecionadas)
	     versionsSelec.add(vco.Version__c);
	     
    vss = [Select Id, Name  From PVVersion__c where Id in (select Version__c from VersionCallOffer__c where Call_Offer__c =:chamadaOferta)];
    System.debug('################ vss = '+vss);
    Set<SelectOption> ve = new Set<SelectOption>();
   // List<String> teste = new List<String>();
    for (PVVersion__c a : vss)
       if (!ve.contains(new SelectOption(a.Id,a.Name))){
          ve.add(new SelectOption(a.Id,a.Name));
          //versao_removida.add(a.Name);
       }
    //setVersao(teste);
    //versao_removida =teste;
    
    todasVersoes.clear();
      
      for (PVVersion__c x:todasVersoesQuery(versionsSelec)){
          if ((x.Name != null)){
           todasVersoes.add(new SelectOption(x.Id,x.Name));
           mapaVersaoId.put(x.Id,x.Name);
        }
      }
    versao2.addAll(ve);
    System.debug('################ vss = '+vss);
    System.debug('###################### teste: '+versao2);
    
   
}


public PVCall_Offer__c getCallOffer() {
        return callOffer;
}

public VFC148_ChamadaOfertaVersao(PVCall_Offer__c co){
	this.callOffer = co;
}

public list <PVVersion__c>todasVersoesQuery(List<Id> selecionadas){
  List<PVVersion__c> todasVersao = [SELECT ID, NAME FROM PVVersion__c WHERE Model__c IN (SELECT Model__c from PVCall_Offer__c WHERE ID = : calloffer.id) and Id not in :selecionadas order by name asc];
  return todasversao;
}



public List<String> getVersao(){
    return versao;
}

public List<String> getVersaoRemovida(){
    return versao_removida;
}

public List<SelectOption> gettodasVersoes(){
 
    return todasVersoes;
}


public List<SelectOption> getVersao2(){
    return versao2;
}

public String selectedVersions{get;set;}





public void  setVersao(List<String> novo){
  versao.clear();
  this.versao = novo;
}
public void setVersaoRemovida (List<String> novo){
  this.versao_removida = novo;
}
public void setTodasVersoes (List<SelectOption> novo){
  todasVersoes.clear();
  this.todasVersoes = novo;
}
public void setVersao2(List<SelectOption> novo){
  versao2.clear();
  this.versao2 = novo;
}


public PageReference salvar(){
    
    if(!callOffer.PV_CH_checkStatus__c){
    	
      String idOferta = calloffer.id;
      Set<String> ids = new Set<String>();
      Set<String> selectedIds = new Set<String>();
      Set<String> unselectedIds = new Set<String>();
      List<String> selecionada = new List<String>();
       
      // preenche o set de versoes selecionadas
      for(SelectOption so : versao2)
        selectedIds.add(so.getValue());
      
      // preenche o set de versoes nao selecionadas
      for(SelectOption so2 : todasVersoes)
        unselectedIds.add(so2.getValue());
      
      deletarVersoes(callOffer.Id, unselectedIds);
      
      
      for(String a : selectedIds)
        selecionada.add(a);
      
      list<PVVersion__c>  versa = todasVersoesQuery(selecionada);
      for(SelectOption conce : versao2)
          for (PVVersion__c c : versa)
             if((c.Name != null) && (c.Id.equals(conce.getValue())) )
                  if (!ids.contains(c.id))
                      ids.add(c.id);
      for(String idVer : selecionada)
          try{
         	    RelacionarOferta(idVer,idOferta);
         	    
          }catch(Exception e){
          	//ApexPages.addMessages(e);
          }
      ids.clear();
      
      ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Versoes da chamada atualizadas com sucesso!');
      ApexPages.addMessage(msg);
      //cancelar();
    }else{
      ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'As versões não podem ser atualizadas');
      ApexPages.addMessage(msg);
    }
    return null; 
}

public void RelacionarOferta(String versao,String oferta){
        VersionCallOffer__c relacao = new VersionCallOffer__c();
        relacao.Call_Offer__c = oferta;
        relacao.Version__c = versao;
        relacao.Status__c = 'Active';
        insert relacao;
}

public void RelacionarOfertaSemFor(List<PVCall_Offer__c> listaOferta){
    List<PVVersion__c> listaVersao;
    List<VersionCallOffer__c>  addVersao = new List<VersionCallOffer__c>();   
    
    for(PVCall_Offer__c oferta: listaOferta){
        listaVersao = [select Id, Name from PVVersion__c where Model__c =: oferta.Model__c];
        for(PVVersion__c versao: listaVersao){
            VersionCallOffer__c relacao = new VersionCallOffer__c();
            relacao.Call_Offer__c = oferta.id;
            relacao.Version__c = versao.id;
            relacao.Status__c = 'Active';
            addVersao.add(relacao);
        }
    }
    if(addVersao!=null)
    	insert addVersao;
}
public PageReference cancelar(){
    versao_removida.clear();
    versao2.clear();
    todasVersoes.clear();
    versao.clear();
    return null;
}

public PageReference remover() {
	    
        for (String a : versao_removida){
        	for (integer i = 0; i < versao.size();i++){
                if (versao.get(i).equals(a)){
                    versao.remove(i);
                   // todas_versoes = versao2.get(i);
                    versao2.remove(i);
                }
           }
           
        }
        return null;
}


public List<SelectOption> gettodasVersoes2(){
    versao2.clear();
    for (String a:versao){    
       versao2.add(new SelectOption(a,mapaVersaoId.get(a)));
    }
    return versao2;
}
public PageReference test(){
	versao2.clear();
    for (String a:versao){    
       versao2.add(new SelectOption(a,mapaVersaoId.get(a)));
    }
    return null;

}


}