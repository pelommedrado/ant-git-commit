@isTest(seeAllData=TRUE)
public class sfa2UtilsTest {
    
    public static testMethod void test(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Test.startTest();
        
        User communityUser = moc.CriaUsuarioComunidade();

        System.assert(Sfa2Utils.isCpfValido('45650316201'));
        
        System.assertEquals('45650316201', Sfa2Utils.limparFormatacaoCpfCnpj('456.503.162-01'));
        
        System.runAs(communityUser){
            Account accountUser = Sfa2Utils.obterContaUserComunidade();
            system.assert(!accountUser.Sell_from_stock__c);
            
            User u = Sfa2Utils.obterUsuarioLogado();
            system.assertEquals(communityUser.BIR__c, u.BIR__c);
            
            Account pAccount = Sfa2Utils.obterContaParente(u);
            system.assertEquals(communityUser.AccountId, pAccount.Id);
        }
        
        Test.stopTest();
    }
    
    public static testMethod void test2(){
        
        MyOwnCreation moc = new MyOwnCreation();        
        User communityUser = moc.CriaUsuarioComunidade();
        
        Account dealerGroup = moc.criaAccountDealer();
        dealerGroup.IDBIR__c = '34557780';
        Insert dealerGroup;
        
        Account dealer = moc.criaAccountDealer();
        dealer.ParentId = dealerGroup.Id;
        dealer.IDBIR__c = '1234589';
        Insert dealer;
        
        Account acc = moc.criaPersAccount();
        acc.VehicleInterest_BR__c = 'CLIO';
        Insert acc;
        
        VEH_Veh__c v = moc.criaVeiculo();
        v.Model__c = 'CLIO';
        v.Price__c = 1000;
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        Insert vr;
        
        // seeAllData TRUE
        Pricebook2 priceBook = [SELECT Id FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1];
        
        Test.startTest();
        
        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = acc.Id;
        Insert opp;
        
        Quote q = moc.criaQuote();
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = priceBook.Id;
        Insert q;
        
        String BIR = Sfa2Utils.obterBirConcessionaria(communityUser.Id);
        System.assertEquals('123456', BIR);
        
        List<User> lsUsers = Sfa2Utils.obterVendedoresConcessionaria(BIR);
        //System.assertEquals(communityUser.FirstName + ' ' + communityUser.LastName, lsUsers.get(0).Name);
        
        VRE_VehRel__c vrel = Sfa2Utils.obterVeiculo(vr.Id);
        System.assertEquals(v.Id, vrel.VIN__c);
        
        System.assertEquals('Open', Sfa2Utils.obterCotacao(opp.Id).get(0).Status);
        
        VFC61_QuoteDetailsVO quoteDetail = Sfa2Utils.criarOrcamento(opp.Id, vrel);
        System.assertEquals(opp.Id, quoteDetail.opportunityId);
        System.assertEquals('Open', quoteDetail.Status);
        
        Sfa2Utils.criarTestDrive(opp.Id, 'motivo');
        
        List<Account> lsAccount = Sfa2Utils.obterListaConta(dealerGroup);
        System.assertEquals(dealer.IDBIR__c, lsAccount.get(0).IDBIR__c);
        
        System.assert(sfa2Utils.isEmailValido('edvaldo@kolekto.com.br'));
        
        Test.stopTest();
        
    }
    
    public static testMethod void test3(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Test.startTest();
        
        Account acc = moc.criaPersAccount();
        acc.VehicleInterest_BR__c = 'CLIO';
        Insert acc;
        
        VEH_Veh__c v = moc.criaVeiculo();
        v.Model__c = 'CLIO';
        v.Price__c = 1000;
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        Insert vr;
        
        VehicleBooking__c vbk = new VehicleBooking__c();
        vbk.Vehicle__c = v.Id;
        vbk.Status__c = 'Active';
        Insert vbk;
        
        System.assert(sfa2Utils.derrubarReserva(vr));
        
        Test.stopTest();
        
    }

}