public class InterfaceSAP{

	public static void execute(Map<Id,Quote> oldQuotesMap, List<Quote> newQuotes){

        System.debug('***oldQuotesMap: '+oldQuotesMap);
        System.debug('***newQuotes: '+newQuotes);

		List<Quote> ccoList = new List<Quote>();
		List<Quote> afcList = new List<Quote>();
		List<Quote> dsfList = new List<Quote>();
		List<Quote> acoList = new List<Quote>();
		List<Quote> mcoList = new List<Quote>();
		List<Quote> cl1List = new List<Quote>();
        
        //CCO, MCO, AFC, DSF, ACO, CL1

		for(Quote q: newQuotes){

            if(q.Status != 'Canceled'){

    			if(oldQuotesMap.get(q.Id).PC_LogicCode__c != 'CCO' && q.PC_LogicCode__c == 'CCO')
    				ccoList.add(q);
                if(q.PC_LogicCode__c == 'MCO')
                    mcoList.add(q);
    			if(oldQuotesMap.get(q.Id).PC_LogicCode__c != 'AFC' && q.PC_LogicCode__c == 'AFC')
    				afcList.add(q);
    			if(oldQuotesMap.get(q.Id).PC_LogicCode__c != 'DSF' && q.PC_LogicCode__c == 'DSF')
    				dsfList.add(q);
    			if(oldQuotesMap.get(q.Id).PC_LogicCode__c != 'ACO' && q.PC_LogicCode__c == 'ACO')
    				acoList.add(q);
    			if(q.PC_LogicCode__c == 'CL1')
    				cl1List.add(q);

            }
		}

        if(!ccoList.isEmpty())
            processCCOs(ccoList);
        if(!mcoList.isEmpty())
            processMCOs(mcoList);
        if(!afcList.isEmpty())
            processAFCs(afcList);
        if(!dsfList.isEmpty())
            processDSFs(dsfList);
        if(!acoList.isEmpty())
            processACOs(acoList);
        if(!cl1List.isEmpty())
            processCL1s(cl1List);
    }

    // Alteração do cliente - OK
	public static void processCL1s(List<Quote> newQuotes){

        Set<String> cpfClientes = new Set<String>();

        for(Quote q: newQuotes){
            if(q.PC_CustomerNumberInternalSedre__c != null){
                cpfClientes.add(q.PC_CustomerNumberInternalSedre__c);
            }
        }

        if(!cpfClientes.isEmpty()){
            List<Account> accList = recuperarContasClientes(newQuotes,cpfClientes);
            criarAtualizarCliente(newQuotes, accList);
        }
	}

    // Alteração do veículo - OK
	public static void processMCOs(List<Quote> newQuotes){

		Map<String,Id> veiculoMap = recuperarVeiculos(newQuotes);

        criarAtualizarVeiculos(newQuotes,veiculoMap);
        
        List<VEH_Veh__c> veiculoList = [SELECT Id, Name FROM VEH_Veh__c WHERE Name =: newQuotes[0].PC_ChassisNumberCurrent__c ];
        
        List<QuoteLineItem> qliLst = [SELECT Id, Vehicle__c FROM QuoteLineItem WHERE QuoteId =: newQuotes[0].Id ORDER BY LastModifiedDate DESC LIMIT 1];
        
        if(!qliLst.isEmpty() && !veiculoList.isEmpty()){
            
            QuoteLineItem qli = qliLst[0];
            qli.Vehicle__c = veiculoList.get(0).Id;
            Database.update(qli);
            
        }
        
        //updateQuoteLineItem(newQuotes);
	}

    // Cancelamento de pedido Sedre - OK
	public static void processACOs(List<Quote> newQuotes){

		atualizarStageOportunidade(newQuotes, 'In Attendance');

		Map<String,Id> veiculoMap = recuperarVeiculos(newQuotes);

		Map<String,VehicleBooking__c> vbkMap = recuperarVBKmap(newQuotes, veiculoMap);

        atualizarStatusVBK(newQuotes, vbkMap, 'Canceled');

        atualizarStatusVeiculo(newQuotes, veiculoMap, 'Stock');

		atualizarStatusCotacao(newQuotes, 'Canceled');
	}
    
    // Desalocação de chassi no Sedre - OK
    public static void processDSFs(List<Quote> newQuotes){

        Map<String,Id> veiculoMap = recuperarVeiculos(newQuotes);

        Map<String,VehicleBooking__c> vbkMap = recuperarVBKmap(newQuotes, veiculoMap);

        atualizarStatusVeiculo(newQuotes,veiculoMap,'Available');

        atualizarStatusVBK(newQuotes,vbkMap,'Canceled');

        atualizarStatusCotacao(newQuotes, 'Waiting for allocation');
    }
    
    // Alocação de Chassi no Sedre - FALTA TESTAR.
    public static void processAFCs(List<Quote> newQuotes){
        
        System.debug('@@ processAFCs');
        Quote q = newQuotes.get(0);
        
        /*******PROVISORIO***************/
        List<VEH_Veh__c> veiculoList = [SELECT Id, Name FROM VEH_Veh__c WHERE Name =: q.PC_ChassisNumberCurrent__c ];
        
        System.debug('@@ q.PC_ChassisNumberCurrent__c: ' + q.PC_ChassisNumberCurrent__c);
        System.debug('@@ veiculoList: ' + veiculoList);
        
        VEH_Veh__c veiculo = new VEH_Veh__c();
        if(veiculoList.isEmpty()){            
            veiculo = new VEH_Veh__c(
                Name = q.PC_ChassisNumberCurrent__c,
                ModelSCR__c = q.PC_SemiclairModel__c,
                VersionSCR__c = q.PC_SemiclairVersion__c,
                OpcSCR__c = q.PC_SemiclairOptional__c,
                ColorSCR__c = q.PC_ColorBoxCriterion__c, // substituido -> PC_SemiclairColor__c
                Harmony__c = q.PC_SemiclairUpholstered__c,
                Upholstery__c = q.PC_SemiclairHarmony__c
            );
            
            System.debug('@@ veiculo: ' + veiculo);
            
            Insert veiculo;
        }
        
        List<VehicleBooking__c> vbkList = [
            SELECT Id, Vehicle__r.Name, Quote__c, Status__c, Quote__r.Opportunity.Dealer__r.ParentId
            FROM VehicleBooking__c 
            WHERE Vehicle__c =: !veiculoList.isEmpty() ? veiculoList.get(0).Id : veiculo.Id
            AND Quote__c =: q.Id
            AND Status__c = 'Active'
        ];
        
        System.debug('@@ vbkList: ' + vbkList);
        
        //if(!vbkList.isEmpty()){
            
            List<VehicleBooking__c> updateVBKList = new List<VehicleBooking__c>();
            
            // esta validação foi necessaria pois a trigger de cotação é disparada 2 vezes e refaz este processo.
            if(q.Status != 'Vehicle allocated'){

                /*if(!vbkList.isEmpty()){

                    for(VehicleBooking__c vbk: vbkList){
                        if(vbk.Status__c.equalsIgnoreCase('Active')){
                            VehicleBooking__c v = new VehicleBooking__c(
                                Id = vbk.Id,
                                Status__c = 'Canceled'
                            ); 
                            updateVBKList.add(v);
                        }
                    }
                
                }*/

                // cria o vbk para o veiculo vindo do SAP
                VehicleBooking__c vbk = new VehicleBooking__c(
                    Id = !vbkList.isEmpty() ? vbkList.get(0).Id : null,
                    Vehicle__c = !veiculoList.isEmpty() ? veiculoList.get(0).Id : veiculo.Id,
                    Quote__c = q.Id,
                    //BookingOwner__c = '',
                    //SellerName__c = '',
                    //SellerCPF__c = '',
                    Dealer__c = q.Opportunity.Dealer__c,
                    ClientName__c = q.Account.FirstName != null ? q.Account.FirstName : q.Account.Name,
                    ClientSurname__c = q.Account.LastName != null ? q.Account.LastName : q.Account.Name,
                    CustomerIdentificationNbr__c = q.Account.CustomerIdentificationNbr__c,
                    ClientEmail__c = q.Account.Email__c,
                    HomePhone__c = q.Account.HomePhone__c,
                    //MobilePhone__c = q.Account.MobilePhone__c,
                    Status__c = 'Active'
                ); 
                
                updateVBKList.add(vbk);
                
                List<Database.UpsertResult> result = Database.Upsert(updateVBKList, false);
                for(Database.UpsertResult sr : result){
                    if(!sr.isSuccess()){
                        salvarErros(newQuotes, result);
                        //return;
                    }
                }
                //Upsert updateVBKList;
            }
        //}
        
        List<QuoteLineItem> qliLst = [SELECT Id, Vehicle__c FROM QuoteLineItem WHERE QuoteId =: q.Id ORDER BY LastModifiedDate DESC LIMIT 1];
        
        if(!qliLst.isEmpty()){
            
            QuoteLineItem qli = qliLst[0];
            qli.Vehicle__c = !veiculoList.isEmpty() ? veiculoList.get(0).Id : veiculo.Id;
            Database.update(qli);
            
        }
        
        Quote quoteToCompare = VFC35_QuoteDAO.getInstance().fetchById(q.Id);
        
        // obter todas as concessionarias do mesmo grupo
        List<Account> dealersGroup =  [
            SELECT Id, IDBIR__c, Name 
            FROM Account 
            WHERE ParentId =: quoteToCompare.Opportunity.Dealer__r.ParentId
            AND RecordTypeId = :Utils.getRecordTypeId('Account', 'Network_Site_Acc')
        ];
        

        List<VRE_VehRel__c> vehicleRelationList = [
            SELECT Id, VIN__c, Account__r.ParentId
            FROM VRE_VehRel__c
            WHERE VIN__c =: !veiculoList.isEmpty() ? veiculoList.get(0).Id : veiculo.Id
            AND Account__c IN: dealersGroup // Pegar os ids das concessionarias do mesmo grupo
            AND Status__c = 'Active'
        ];
        
        if(vehicleRelationList.isEmpty()){
            q.Status = 'Vehicle allocated';
        }else{
            q.Status = 'Pre Order Requested';
        }
        
    	/*
        
        Map<String,Id> veiculoMap = recuperarVeiculos(newQuotes);
        
        // cria os veiculos que não existem
        criarVeiculos(newQuotes,veiculoMap);

        //recupera os veiculos recem inseridos para passar no recuperaVBK
        veiculoMap = recuperarVeiculos(newQuotes);

        // retirado o metodo de buscaVeiculoNaBVM pelo fato de não poder fazer chamada dentro de trigger
        //buscaVeiculoBVMECriaVeiculos(newQuotes, veiculoMap);

        //recupera os vbks
        Map<String,List<VehicleBooking__c>> mapVbk = recuperarVBK(newQuotes, veiculoMap);

        List<VehicleBooking__c> vbkList = criarVbk(newQuotes, veiculoMap, mapVbk);

        Map<String,VehicleBooking__c> vbkMap = recuperarVBKmap(newQuotes, veiculoMap);

        // recupera as relações de posse da concessionaria para ver se ja existe o veiculo para a alocação
        obterVeicRelEAtualizarCotacoes(vbkMap, newQuotes); */
    }
    
    // Criação de pedido no Sedre - OK
    public static void processCCOs(List<Quote> newQuotes){
        atualizarStatusCotacao(newQuotes, 'Order Sedre Created');
        updateQuoteLineItem(newQuotes);
    }

    public static void updateQuoteLineItem(List<Quote> newQuotes){

        List<QuoteLineItem> qliList = [
            SELECT Id, QuoteId, Model_AOC__c, Version_AOC__c, Color_AOC__c, Upholstery_AOC__c, Harmony_AOC__c, Optionals_AOC__c
            FROM QuoteLineItem 
            WHERE QuoteId 
            IN: newQuotes
        ];

        System.debug('***qliList: '+qliList);

        Set<String> modelSemiclair = new Set<String>();
        Set<String> versionSemiClair = new Set<String>();
        Set<String> optionalSemiClair = new Set<String>();

        // Add semiclairs that SAP sent in Set to Query IN
        for(Quote q: newQuotes){

            if( String.isNotEmpty(q.PC_SemiclairModel__c) )
                modelSemiclair.add(q.PC_SemiclairModel__c);
            
            if( String.isNotEmpty(q.PC_SemiclairVersion__c) )
                versionSemiClair.add(q.PC_SemiclairVersion__c);
            
            if( String.isNotEmpty(q.PC_SemiclairColor__c) )
                optionalSemiClair.add(q.PC_SemiclairColor__c);
            
            if( String.isNotEmpty(q.PC_SemiclairHarmony__c) )
                optionalSemiClair.add(q.PC_SemiclairHarmony__c);
            
            if( String.isNotEmpty(q.PC_SemiclairUpholstered__c) )
                optionalSemiClair.add(q.PC_SemiclairUpholstered__c);

            List<String> optionalSemiClairs = new List<String>();
            if(String.isNotEmpty(q.PC_SemiclairOptional__c)){
                optionalSemiClairs = q.PC_SemiclairOptional__c.split(' ');
                for(String opt: optionalSemiClairs){
                    optionalSemiClair.add(opt);
                }
            } 

        }

        System.debug('***optionalSemiClair: '+optionalSemiClair);

        // Get models by modelSemiclairs
        List<Model__c> modelos = [
            SELECT Id, Model_Spec_Code__c, Name 
            FROM Model__c 
            WHERE Model_Spec_Code__c IN: modelSemiclair
        ];

        System.debug('***modelos: '+modelos);

        // Get versions by versionSemiClair
        List<PVVersion__c> versoes = [
            SELECT Id, Semiclair__c, Name 
            FROM PVVersion__c 
            WHERE Semiclair__c IN: versionSemiClair 
            AND Model__c IN: modelos
        ];

        System.debug('***versoes: '+versoes);
        
        // Get optionals by optionalSemiClair
        List<Optional__c> optionals = [
            SELECT Id, Name, Optional_Code__c 
            FROM Optional__c 
            WHERE Optional_Code__c IN: optionalSemiClair 
            AND Version__c IN: versoes
        ];

        // Se não tiver versoes, não vai existir cores, estofados, harmonias e opcionais
        System.debug('***optionals: '+optionals);

        List<QuoteLineItem> updateQliList = new List<QuoteLineItem>();
        Map<String,Model__c> modelMap = new Map<String,Model__c>();
        Map<String,PVVersion__c> versionMap = new Map<String,PVVersion__c>();
        Map<String,Optional__c> optionalMap = new Map<String,Optional__c>();
        Map<Id,QuoteLineItem> mapQuoteItem = new Map<Id,QuoteLineItem>();

        // add in maps to return value according with semiclair
        for(Model__c m: modelos){
            if(String.isNotEmpty(m.Model_Spec_Code__c))
                modelMap.put(m.Model_Spec_Code__c, m);
        }

        for(PVVersion__c v: versoes){
            if(String.isNotEmpty(v.Semiclair__c))
                versionMap.put(v.Semiclair__c, v);
        }

        for(Optional__c o: optionals){
            if(String.isNotEmpty(o.Optional_Code__c))
                optionalMap.put(o.Optional_Code__c, o);
        }

        for(QuoteLineItem q: qliList){
            mapQuoteItem.put(q.QuoteId,q);
        }

        System.debug('***modelMap: '+modelMap);
        System.debug('***versionMap: '+versionMap);
        System.debug('***optionalMap: '+optionalMap);
        System.debug('***mapQuoteItem: '+mapQuoteItem);

        // for each quote create QuoteLineItem with values semiclairs and names
        for(Quote q: newQuotes){

            System.debug('***q: '+q);

            QuoteLineItem qli = new QuoteLineItem();

            qli.Id = mapQuoteItem.get(q.Id).Id;

            qli.Model_AOC__c = !modelMap.isEmpty() && modelMap.get( q.PC_SemiclairModel__c ).Id != null ?
                modelMap.get( q.PC_SemiclairModel__c ).Id : null;

            qli.Version_AOC__c = !versionMap.isEmpty() && versionMap.get( q.PC_SemiclairVersion__c ).Id != null ? 
                versionMap.get( q.PC_SemiclairVersion__c ).Id : null;

            // to get optionals in field with multiples optionals separated by spaces
            List<String> optionalSemiClairs = new List<String>();
            if(String.isNotEmpty(q.PC_SemiclairOptional__c)){
                optionalSemiClairs = q.PC_SemiclairOptional__c.split(' ');
            }

            System.debug('***optionalSemiClairs: '+optionalSemiClairs);

            // mapping semiclairs
            qli.Color_AOC__c        = q.PC_SemiclairColor__c;
            qli.Upholstery_AOC__c   = q.PC_SemiclairHarmony__c;             // Mapped with error from Power Center to field upholstery 
            qli.Harmony_AOC__c      = q.PC_SemiclairUpholstered__c;         // Mapped with error from Power Center to field harmony
            qli.Optionals_AOC__c    = String.join(optionalSemiClairs, '; ');

            if(!optionalMap.isEmpty()){

                // mapping names semiclairs
                qli.ColorName__c = optionalMap.get(q.PC_SemiclairColor__c) != null ? 
                    optionalMap.get(q.PC_SemiclairColor__c).Name : null;

                qli.UpholsteryName__c = optionalMap.get(q.PC_SemiclairHarmony__c) != null ? 
                    optionalMap.get(q.PC_SemiclairHarmony__c).Name : null;

                qli.HarmonyName__c = optionalMap.get(q.PC_SemiclairUpholstered__c) != null ? 
                    optionalMap.get(q.PC_SemiclairUpholstered__c).Name : null;

                // to get optionals names in field with multiples optionals separated by spaces
                qli.OptionalsNames__c = '';
                for(String opt: optionalSemiClairs){
                    qli.OptionalsNames__c += optionalMap.get(opt) != null ? 
                        optionalMap.get(opt).Name + '; ' : '';
                }

            }

            System.debug('***qli: '+qli);
            updateQliList.add(qli);

        }

        List<Database.SaveResult> results = Database.Update(updateQliList,false);
        salvarErros(newQuotes, results);
    }

    public static void obterVeicRelEAtualizarCotacoes(Map<String,VehicleBooking__c> vbkMap, List<Quote> newQuotes){

        System.debug('***vbkMap: '+vbkMap);

        Set<Id> vehicleIds = new Set<Id>();
        Set<Id> dealerIds = new Set<Id>();
        Map<String,Id> vbkQuoteIdMap = new Map<String,Id>();

        for(VehicleBooking__c vbk: vbkMap.values()){
            vehicleIds.add(vbk.Vehicle__c);
            dealerIds.add(vbk.Quote__r.Opportunity.Dealer__r.ParentId);
            vbkQuoteIdMap.put(vbk.Vehicle__c + String.valueOf(vbk.Quote__r.Opportunity.Dealer__r.ParentId), vbk.Quote__c);
        }

        // obter todas as concessionarias do mesmo grupo
        List<Account> dealersGroup =  [
            SELECT Id, IDBIR__c, Name 
            FROM Account 
            WHERE ParentId IN: dealerIds
        ];

        List<VRE_VehRel__c> vehicleRelationList = [
            SELECT Id, VIN__c, Account__r.ParentId
            FROM VRE_VehRel__c
            WHERE VIN__c IN: vehicleIds 
            AND Account__c IN: dealersGroup // Pegar os ids das concessionarias do mesmo grupo
            AND Status__c = 'Active'
        ];

        System.debug('****vehicleRelationList: ' + vehicleRelationList);

        Set<Id> quotesQueTemRelacaoPosse = new Set<Id>();

        for(VRE_VehRel__c vr: vehicleRelationList){

            quotesQueTemRelacaoPosse.add(vbkQuoteIdMap.get(vr.VIN__c + String.valueOf(vr.Account__r.ParentId)));
        }
        
        List<Quote> comRelacaoPosseList = new List<Quote>();
        List<Quote> semRelacaoPosseList = new List<Quote>();
        
        for(Quote q : newQuotes){
            
            if(quotesQueTemRelacaoPosse.contains(q.Id)) comRelacaoPosseList.add(q);
            
            if(!quotesQueTemRelacaoPosse.contains(q.Id)) semRelacaoPosseList.add(q);
        }

        atualizarStatusCotacao(comRelacaoPosseList,'Pre Order Requested');
        atualizarStatusCotacao(semRelacaoPosseList,'Vehicle allocated');
    }

    public static void atualizarStatusCotacao(List<Quote> newQuotes, String status){
        for(Quote q: newQuotes){
            q.Status = status;
        }
    }

    public static void criarVeiculos(List<Quote> newQuotes, Map<String,Id> veiculoMap){

        System.debug('***veiculoMap: '+veiculoMap);

        List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();

        for(Quote q: newQuotes){

            System.debug('***q.PC_ChassisNumberCurrent__c: '+q.PC_ChassisNumberCurrent__c);

            if(veiculoMap.get(q.PC_ChassisNumberCurrent__c ) == null){

                VEH_Veh__c v = new VEH_Veh__c(
                    Name = q.PC_ChassisNumberCurrent__c,
                    ModelSCR__c = q.PC_SemiclairModel__c,
                    VersionSCR__c = q.PC_SemiclairVersion__c,
                    OpcSCR__c = q.PC_SemiclairOptional__c,
                    ColorSCR__c = q.PC_ColorBoxCriterion__c, // substituido -> PC_SemiclairColor__c
                    Harmony__c = q.PC_SemiclairUpholstered__c,
                    Upholstery__c = q.PC_SemiclairHarmony__c
                );
                veiculoList.add(v);

            }
        }

        List<Database.SaveResult> results = Database.Insert(veiculoList,false);
        salvarErros(newQuotes, results);
    }

    public static void criarAtualizarVeiculos(List<Quote> newQuotes, Map<String,Id> veiculoMap){

        System.debug('***veiculoMap: '+veiculoMap);

        List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();

        for(Quote q: newQuotes){

            VEH_Veh__c v = new VEH_Veh__c(
                Id = veiculoMap.get(q.PC_ChassisNumberCurrent__c ),
                Name = q.PC_ChassisNumberCurrent__c,
                ModelSCR__c = q.PC_SemiclairModel__c,
                VersionSCR__c = q.PC_SemiclairVersion__c,
                OpcSCR__c = q.PC_SemiclairOptional__c,
                ColorSCR__c = q.PC_ColorBoxCriterion__c, // substituido -> PC_SemiclairColor__c
                Harmony__c = q.PC_SemiclairUpholstered__c,
                Upholstery__c = q.PC_SemiclairHarmony__c
            );
            veiculoList.add(v);

        }

        List<Database.UpsertResult> results = Database.Upsert(veiculoList,false);
        salvarErros(newQuotes, results);
    }

    public static void criarAtualizarCliente(List<Quote> newQuotes, List<Account> accountList){

        System.debug('***accountList: '+accountList);

        List<Account> upsertAccountCustomer = new List<Account>();
        Map<String,Account> mapCpfCliente = new Map<String,Account>();

        for(Account acc: accountList)
            mapCpfCliente.put(acc.CustomerIdentificationNbr__c,acc);

        for(Quote q: newQuotes){

            // cliente possui conta?
            if( mapCpfCliente.get(q.PC_CustomerNumberInternalSedre__c) != null ){
                upsertAccountCustomer.add( createOrUpdateAccountCustomer( q, mapCpfCliente.get(q.PC_CustomerNumberInternalSedre__c).Id) );

            // cliente não tem conta, cria a conta
            }else{
                upsertAccountCustomer.add(createOrUpdateAccountCustomer(q, null));
            }
        }

        List<Database.UpsertResult> results = Database.Upsert(upsertAccountCustomer,false);

        System.debug('***upsertAccountCustomer: '+upsertAccountCustomer);

        for(Quote q: newQuotes){
            for(Integer i=0; i < results.size(); i++){
                if( results.get(i).isSuccess())
                    q.BillingAccount__c = results.get(i).getId();
            }
        }

        salvarErros(newQuotes, results);
    }

    public static Account createOrUpdateAccountCustomer(Quote q, Id accountId){

        Account a = new Account();

        if(accountId != null)
            a.Id = accountId;

        if(q.PC_TypeOfCustomerSedre__c == '1'){

            a.FirstName = q.PC_FirstName__c;
            a.LastName = q.PC_PersonalOrCompanyClientName__c;
            a.RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc');

        }else if(q.PC_TypeOfCustomerSedre__c == '2'){

            a.Name = String.isNotEmpty(q.PC_CustomerCompany__c) ? q.PC_CustomerCompany__c: '';
            a.RecordTypeId = Utils.getRecordTypeId('Account','Company_Acc');

        }

        // dados da conta - verificar os valores recebidos, no teste recebi todos os valores nulos
        //a.PersEmailAddress__c = q.PC_ComplementaryEmailIndividualCo__c;
        //a.ProfEmailAddress__c = q.PC_ComplementaryEmailIndividualCo__c;
        a.Shipping_Street__c = q.PC_WayNome__c;
        a.Shipping_City__c = q.PC_City__c;
        a.Shipping_State__c = q.PC_State__c;
        a.PersLandline__c = q.PC_PersonalPhone__c;
        a.ShippingCity = q.PC_City__c;  
        a.ShippingState = q.PC_State__c; 
        a.ShippingStreet = q.PC_WayNome__c;
        a.Phone = q.PC_PersonalPhone__c;
        a.PersMobPhone__c = q.PC_MobilePhone__c;
        a.CustomerIdentificationNbr__c = q.PC_CustomerNumberInternalSedre__c;

        return a;
    }
    
    public static List<Account> recuperarContasClientes(List<Quote> newQuotes, Set<String> cpfClientes){

        if(!cpfClientes.isEmpty()){

            List<Account> accList = [
                SELECT Id, CustomerIdentificationNbr__c, FirstName, LastName, RecordTypeId, Name
                FROM Account
                WHERE CustomerIdentificationNbr__c 
                IN: cpfClientes
            ];

            return accList;
        }
        return new List<Account>();
    }
    
    /*public static void buscarVeiculoBVMECriaVeiculos(List<Quote> newQuotes, Map<String,Id> veiculoMap){
        List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();
        
        for(Quote q: newQuotes){
            if(veiculoMap.get(q.PC_ChassisNumberCurrent__c) == null){
                
                WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();               
                WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
                WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
                WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
                
                servicePref.bvmsi25SocEmettrice  = System.Label.SocieteEmettrice;
                servicePref.bvmsi25Vin = q.PC_ChassisNumberCurrent__c;
                //servicePref.bvmsi25CodePaysLang = CountryCode; //FRA 
                servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
                request.ServicePreferences = servicePref;
                
                VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
                VehWS.clientCertName_x = System.label.RenaultCertificate;           
                VehWS.timeout_x=40000;
                
                WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse vehBVM = VehWS.getApvGetDetVehXml(request);
                
                VEH_Veh__c veh = new VEH_Veh__c();

                veh.Name = q.PC_ChassisNumberCurrent__c;
                
                //AfterSalesType__c p460:tvv
                veh.AfterSalesType__c = vehBVM.detVeh.tvv != null ? vehBVM.detVeh.tvv : null; 
                
                //BodyType__c p460:libCarrosserie
                veh.BodyType__c = vehBVM.detVeh.libCarrosserie != null ? vehBVM.detVeh.libCarrosserie : null; 
                
                //DateofManu__c p460:dateTcmFab 
                if (vehBVM.detVeh.dateTcmFab != null && vehBVM.detVeh.dateTcmFab.length() > 7)
                    veh.DateofManu__c = date.newinstance(
                        Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(6, 10)), 
                        Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(3, 5)), 
                        Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(0, 2))
                    );
                
                //DeliveryDate__c p460:dateLiv
                if (vehBVM.detVeh.dateLiv != null && vehBVM.detVeh.dateLiv.length() > 7)
                    veh.DeliveryDate__c = date.newinstance(
                        Integer.valueOf(vehBVM.detVeh.dateLiv.substring(6, 10)), 
                        Integer.valueOf(vehBVM.detVeh.dateLiv.substring(3, 5)), 
                        Integer.valueOf(vehBVM.detVeh.dateLiv.substring(0, 2))
                    );
                
                //Description__c p460:libModel + p460:ligne2P12 + p460:ligne3P12 + p460:ligne4P12
                veh.Description__c = vehBVM.detVeh.libModel + ' ' + vehBVM.detVeh.ligne2P12 + ' ' + vehBVM.detVeh.ligne3P12 + ' ' + vehBVM.detVeh.ligne4P12;
                
                //EngineIndex__c p460:indMot
                veh.EngineIndex__c = vehBVM.detVeh.indMot != null ? vehBVM.detVeh.indMot : null;
                
                //EngineManuNbr__c p460:NMot
                veh.EngineManuNbr__c = vehBVM.detVeh.NMot != null ? vehBVM.detVeh.NMot : null;
                
                //EngineType__c p460:typeMot 
                veh.EngineType__c = vehBVM.detVeh.typeMot != null ? vehBVM.detVeh.typeMot : null;
                
                //GearBoxIndex__c p460:indBoi 
                veh.GearBoxIndex__c = vehBVM.detVeh.indBoi != null ? vehBVM.detVeh.indBoi : null;
                
                //GearBoxManuNbr__c p460:NBoi
                veh.GearBoxManuNbr__c = vehBVM.detVeh.NBoi != null ? vehBVM.detVeh.NBoi : null;
                
                //GearBoxType__c p460:typeBoi
                veh.GearBoxType__c = vehBVM.detVeh.typeBoi != null ? vehBVM.detVeh.typeBoi : null;
                
                //Model__c p460:libModel 
                veh.Model__c = vehBVM.detVeh.libModel != null ? vehBVM.detVeh.libModel : null;
                
                //VehicleBrand__c p460:marqCom 
                veh.VehicleBrand__c = vehBVM.detVeh.marqCom != null ? vehBVM.detVeh.marqCom : null;
                
                //VehicleManuNbr__c p460:NFab
                veh.VehicleManuNbr__c = vehBVM.detVeh.NFab != null ? vehBVM.detVeh.NFab : null;
                
                //VersionCode__c p460:version
                veh.VersionCode__c = vehBVM.detVeh.version != null ? vehBVM.detVeh.version : null;
                
                if(vehBVM.detVeh.criteres != null){
                    for (WS01_ApvGetDetVehXml.DetVehCritere crit : vehBVM.detVeh.criteres){
                        //EnergyType__c qet 019
                        veh.EnergyType__c = crit.cdObjOf == '019' ? crit.critereOf : null;  
                        //ModelCode__c qet 008
                        veh.ModelCode__c = crit.cdObjOf == '008' ? crit.critereOf : null;  
                    }
                }
                veiculoList.add(veh);
            }
        }
        List<Database.SaveResult> results = DataBase.Insert(veiculoList, false);
        salvarErros(newQuotes, results);
    }*/
    
    public static void atualizarStageOportunidade(List<Quote> newQuotes, String stage){

        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Quote q: newQuotes){
            Opportunity opp = new Opportunity(
                StageName = stage,
                Id = q.OpportunityId
            );
            oppList.add(opp);
        }

        List<Database.SaveResult> results = Database.Update(oppList,false);
        salvarErros(newQuotes, results);
    }
    
    public static void atualizarStatusVeiculo(List<Quote> newQuotes, Map<String,Id> veiculoMap, String status){

        System.debug('***veiculoMap: '+veiculoMap);

        List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();
        
        for(Quote q: newQuotes){

            if(status == 'Available' && q.Opportunity.Dealer__r.new_stock_version__c)
                status = 'Stock';

            VEH_Veh__c v = new VEH_Veh__c(
                Status__c = status,
                Id = veiculoMap.get(q.PC_ChassisNumberCurrent__c)
            );
            veiculoList.add(v);
        }

        List<Database.SaveResult> results = Database.Update(veiculoList,false);
        salvarErros(newQuotes, results);       
    }
    
    public static void atualizarStatusVBK(List<Quote> newQuotes, Map<String,VehicleBooking__c> vbkMap, String status){

        System.debug('***vbkMap: ' + vbkMap);

        List<VehicleBooking__c> vbkListUpdate = new List<VehicleBooking__c>();
        
        for(Quote q: newQuotes){

            if(!vbkMap.isEmpty() && vbkMap.get(q.Id + q.PC_ChassisNumberCurrent__c).Id != null){

                VehicleBooking__c v = new VehicleBooking__c(
                    Status__c = status,
                    Id = vbkMap.get(q.Id + q.PC_ChassisNumberCurrent__c).Id
                );
                vbkListUpdate.add(v);   
            }
        }

        List<Database.SaveResult> results = Database.Update(vbkListUpdate,false);
        salvarErros(newQuotes, results);      
    }
    
    public static Map<String,List<VehicleBooking__c>> recuperarVBK(List<Quote> newQuotes, Map<String,Id> veiculoMap){

        System.debug('***veiculoMap: ' + veiculoMap);

        List<VehicleBooking__c> vbkList = [
            SELECT Id, Vehicle__r.Name, Quote__c, Status__c, Quote__r.Opportunity.Dealer__r.ParentId
            FROM VehicleBooking__c 
            WHERE Vehicle__c IN: veiculoMap.values() 
            AND Quote__c IN: newQuotes 
            AND Status__c = 'Active'
        ];

        System.debug('***vbkList: '+vbkList);
        
        Map<String,List<VehicleBooking__c>> vbkMap = new Map<String,List<VehicleBooking__c>>();
        for(VehicleBooking__c vbk: vbkList){
            vbkMap.put(vbk.Quote__c,vbkList);
        }
        return vbkMap;
    }

    public static Map<String,VehicleBooking__c> recuperarVBKmap(List<Quote> newQuotes, Map<String,Id> veiculoMap){

        System.debug('***veiculoMap: ' + veiculoMap);

        List<VehicleBooking__c> vbkList = [
            SELECT Id, Vehicle__r.Name, Quote__c, Status__c, Quote__r.Opportunity.Dealer__r.ParentId
            FROM VehicleBooking__c 
            WHERE Vehicle__c IN: veiculoMap.values() 
            AND Quote__c IN: newQuotes 
            AND Status__c = 'Active'
        ];

        System.debug('***vbkList: '+vbkList);
        
        Map<String,VehicleBooking__c> vbkMap = new Map<String,VehicleBooking__c>();
        for(VehicleBooking__c vbk: vbkList){
            vbkMap.put(vbk.Quote__c + vbk.Vehicle__r.Name ,vbk);
        }
        return vbkMap;
    }

	public static List<VehicleBooking__c> criarVbk(List<Quote> newQuotes, Map<String,Id> veiculoMap, Map<String,List<VehicleBooking__c>> vbkMap){

        // TO DO: cancelar o vbk ativo pertencente a cotação

        System.debug('***veiculoMap: '+veiculoMap);
        System.debug('***vbkMap: '+vbkMap);

		List<VehicleBooking__c> vbkList = new List<VehicleBooking__c>();
        
        for(Quote q: newQuotes){

            // esta validação foi necessaria pois a trigger de cotação é disparada 2 vezes e refaz este processo.
            if(q.Status != 'Vehicle allocated'){

                for(VehicleBooking__c vbk: vbkMap.get(q.Id)){
                    if(vbk.Status__c.equalsIgnoreCase('Active')){
                        VehicleBooking__c v = new VehicleBooking__c(
                            Id = vbk.Id,
                            Status__c = 'Canceled'
                        ); 
                        vbkList.add(v);
                    }
                }

                /*

                // verifica se tem vbk ativo para esta cotação e cancela o vbk
                if( vbkMap.get( q.Id ) != null && vbkMap.get( q.Id ).Status__c == 'Active' ){

                    VehicleBooking__c vbk = new VehicleBooking__c(
                        Id = vbkMap.get(q.Id).Id,
                        Status__c = 'Canceled'
                    ); 
                    vbkList.add(vbk);
                }

                */

                // cria o vbk para o veiculo vindo do SAP
                VehicleBooking__c vbk = new VehicleBooking__c(
                    Vehicle__c = veiculoMap.get(q.PC_ChassisNumberCurrent__c),
                    Quote__c = q.Id,

                    // ver se recebemos estes dados
                    //BookingOwner__c = '',
                    //SellerName__c = '',
                    //SellerCPF__c = '',
                    //Dealer__c = '',
                    //ClientName__c = '',
                    //ClientSurname__c = '',
                    //CustomerIdentificationNbr__c = '',
                    //ClientEmail__c = '',
                    //HomePhone__c = '',
                    //MobilePhone__c = '',

                    Status__c = 'Active'
                ); 
                vbkList.add(vbk);

            }
        }

        System.debug('***vbkList: ' + vbkList);

        List<Database.UpsertResult> result = Database.Upsert(vbkList,false);
        salvarErros(newQuotes, result);

        return vbkList;
	}

	public static Map<String,Id> recuperarVeiculos(List<Quote> newQuotes){

		Set<String> setChassis = new Set<String>();
		Map<String,Id> veiculoMap = new Map<String,Id>();

		// Para recuperar o veiculo
		for(Quote q: newQuotes){
            if( String.isNotEmpty(q.PC_ChassisNumberCurrent__c) )
                setChassis.add( q.PC_ChassisNumberCurrent__c );
		}

		List<VEH_Veh__c> veiculoList = [SELECT Id, Name FROM VEH_Veh__c WHERE Name IN: setChassis];

        System.debug('***veiculoList: '+veiculoList);

		for(VEH_Veh__c v: veiculoList){
			veiculoMap.put(v.Name,v.Id);
		}
		return veiculoMap;
	}

	public static void salvarErros(List<Quote> newQuotes, List<Database.SaveResult> results){

        System.debug('***results: '+results);

        // Se apresentar erros salva no qeto de interface
		for(Quote q: newQuotes){
            q.PC_ErrorMessage__c = String.isEmpty(q.PC_ErrorMessage__c) ? '' : q.PC_ErrorMessage__c;
	        for(Integer i=0; i < results.size(); i++){
	            if(!results.get(i).isSuccess()){
            		q.PC_ErrorMessage__c += 'Erro: ' + String.valueOf(results.get(i).getErrors()) + '\n';
	            }
	        }
	    }
	}

    public static void salvarErros(List<Quote> newQuotes, List<Database.UpsertResult> results){

        System.debug('***results: '+results);

        // Se apresentar erros salva no qeto de interface
        for(Quote q: newQuotes){
            q.PC_ErrorMessage__c = String.isEmpty(q.PC_ErrorMessage__c) ? '' : q.PC_ErrorMessage__c;
            for(Integer i=0; i < results.size(); i++){
                if(!results.get(i).isSuccess()){
                    q.PC_ErrorMessage__c += 'Erro: ' + String.valueOf(results.get(i).getErrors()) + '\n';
                }
            }
        }
    }
    
}