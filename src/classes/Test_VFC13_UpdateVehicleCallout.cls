/*****************************************************************************************
    Name    : Test_VFC13_UpdateVehicleCallout
    Desc    : This is to test VFC13_UpdateVehicleCallout class
    Approach: WebServiceMock  interface for callout testing
                
                                                
 Modification Log : 
---------------------------------------------------------------------------------------
Developer                       Date                Description
---------------------------------------------------------------------------------------
Praveen Ravula                  6/5/2013           Created 

******************************************************************************************/




@istest
public class Test_VFC13_UpdateVehicleCallout{

    public static testMethod void testUpdate(){

      // Prepare test data
        List<veh_veh__c> lstvehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
        system.assert(lstvehicle .size() > 0);
       
     // Start test
         Test.startTest();

     // setting mockresponse for callout
         Test.setMock(WebServiceMock.class, new Test_WS01_ApvGetDetVehXml_WebServiceMock());
         For(veh_veh__c vcle:lstvehicle ){
         
            VFC13_UpdateVehicleCallout.getVehicleDetails(vcle.Name,vcle.Id);
         
         }
      //Stop test
         Test.stopTest();
         System.debug('TESTS ENDED');
    }
}