public with sharing class VFC132_TaskBeforeInsertExecution {

	private static final VFC132_TaskBeforeInsertExecution instance = new VFC132_TaskBeforeInsertExecution();
	
	private VFC132_TaskBeforeInsertExecution(){}

    public static VFC132_TaskBeforeInsertExecution getInstance(){
        return instance;
    }
    
   	public void validateAssingUser(List<Task> lstNewTask){
   		
   		User created = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() );
   		
   		List<String> idsOwener = new List<String>();
   		
   		for(Task task : lstNewTask){
   			idsOwener.add(task.OwnerId);  		
   		}
   		
   		Map<ID, Object> mapOwnerUsers = new Map<ID,sObject>([Select BIR__c from User where id in :idsOwener ]); 
   		
   		for(Task task : lstNewTask){
			
			User owner = (User) mapOwnerUsers.get(task.OwnerId);

			System.debug('>>> owner:' + owner);
			System.debug('>>> created:' + created);

   			if(owner.BIR__c != null && created.BIR__c != null){
	   			if(owner.BIR__c != created.BIR__c){
	   				//task.addError(Label.TaskAssignError);
	   			}
   			}
   		}
   	}


}