//Generated by wsdl2apex

public class WS01_ApvGetDetVehXml {
    public class DetVehCritere {
        public String cdCritOf;
        public String cdObjOf;
        public String critereOf;
        public String libCritOf;
        private String[] cdCritOf_type_info = new String[]{'cdCritOf','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] cdObjOf_type_info = new String[]{'cdObjOf','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] critereOf_type_info = new String[]{'critereOf','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libCritOf_type_info = new String[]{'libCritOf','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'cdCritOf','cdObjOf','critereOf','libCritOf'};
    }
    public class SiUnavailableException extends Exception {
        public WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg;
        private String[] detVehInfoMsg_type_info = new String[]{'detVehInfoMsg','http://xml.bvm.icm.apv.bservice.renault','DetVehInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'detVehInfoMsg'};
    }
    public class ApvGetDetVehXml {
        public String endpoint_x = 'http://icm.intra.renault.fr:80/icm/services/ApvGetDetVehXml';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault', 'WS01_ApvGetDetVehXml'};
        public WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse getApvGetDetVehXml(WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request) {
            WS01_ApvGetDetVehXml.getApvGetDetVehXml_element request_x = new WS01_ApvGetDetVehXml.getApvGetDetVehXml_element();
            WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element response_x;
            request_x.request = request;
            Map<String, WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element> response_map_x = new Map<String, WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element>();
            response_map_x.put('response_x', response_x);
            System.debug('endpoint_x >>>>>>>>>'+endpoint_x);
            System.debug('request_x>>>>>>>>'+request_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://xml.bvm.icm.apv.bservice.renault',
              'getApvGetDetVehXml',
              'http://xml.bvm.icm.apv.bservice.renault',
              'getApvGetDetVehXmlResponse',
              'WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            System.debug('response_x >>>>>>>>>'+response_x);

            return response_x.getApvGetDetVehXmlReturn;
        }
    }
    public class DetVeh {
        public String dateLiv;
        public String dateTcmFab;
        public String indBoi;
        public String indMot;
        public String libCarrosserie;
        public String libConst;
        public String libModel;
        public String ligne2P12;
        public String ligne3P12;
        public String ligne4P12;
        public String marqCom;
        public String marqCon;
        public String marqDis;
        public String marqFab;
        public String mil;
        public Integer nbCrt;
        public String NBoi;
        public String NFab;
        public String NMot;
        public String placSpec;
        public String ptma;
        public String ptmaar;
        public String ptmaav;
        public String ptr;
        public String refBoi;
        public String refMot;
        public String tapv;
        public String tvv;
        public String typeBoi;
        public String typeMot;
        public String version;
        public String vin;
        public WS01_ApvGetDetVehXml.DetVehCritere[] criteres;
        public String niveau;
        private String[] dateLiv_type_info = new String[]{'dateLiv','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] dateTcmFab_type_info = new String[]{'dateTcmFab','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] indBoi_type_info = new String[]{'indBoi','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] indMot_type_info = new String[]{'indMot','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libCarrosserie_type_info = new String[]{'libCarrosserie','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libConst_type_info = new String[]{'libConst','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libModel_type_info = new String[]{'libModel','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ligne2P12_type_info = new String[]{'ligne2P12','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ligne3P12_type_info = new String[]{'ligne3P12','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ligne4P12_type_info = new String[]{'ligne4P12','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] marqCom_type_info = new String[]{'marqCom','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] marqCon_type_info = new String[]{'marqCon','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] marqDis_type_info = new String[]{'marqDis','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] marqFab_type_info = new String[]{'marqFab','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] mil_type_info = new String[]{'mil','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] nbCrt_type_info = new String[]{'nbCrt','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] NBoi_type_info = new String[]{'NBoi','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] NFab_type_info = new String[]{'NFab','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] NMot_type_info = new String[]{'NMot','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] placSpec_type_info = new String[]{'placSpec','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ptma_type_info = new String[]{'ptma','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ptmaar_type_info = new String[]{'ptmaar','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ptmaav_type_info = new String[]{'ptmaav','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] ptr_type_info = new String[]{'ptr','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] refBoi_type_info = new String[]{'refBoi','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] refMot_type_info = new String[]{'refMot','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] tapv_type_info = new String[]{'tapv','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] tvv_type_info = new String[]{'tvv','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] typeBoi_type_info = new String[]{'typeBoi','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] typeMot_type_info = new String[]{'typeMot','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] version_type_info = new String[]{'version','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] vin_type_info = new String[]{'vin','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] criteres_type_info = new String[]{'criteres','http://xml.bvm.icm.apv.bservice.renault','DetVehCritere','1','-1','true'};
        private String[] niveau_type_info = new String[]{'niveau','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'dateLiv','dateTcmFab','indBoi','indMot','libCarrosserie','libConst','libModel','ligne2P12','ligne3P12','ligne4P12','marqCom','marqCon','marqDis','marqFab','mil','nbCrt','NBoi','NFab','NMot','placSpec','ptma','ptmaar','ptmaav','ptr','refBoi','refMot','tapv','tvv','typeBoi','typeMot','version','vin','criteres','niveau'};
    }
    public class ApvGetDetVehXmlResponse {
        public WS01_ApvGetDetVehXml.DetVeh detVeh;
        public WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg;
        private String[] detVeh_type_info = new String[]{'detVeh','http://xml.bvm.icm.apv.bservice.renault','DetVeh','1','1','true'};
        private String[] detVehInfoMsg_type_info = new String[]{'detVehInfoMsg','http://xml.bvm.icm.apv.bservice.renault','DetVehInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'detVeh','detVehInfoMsg'};
    }
    public class DetVehInfoMsg {
        public String codeRetour;
        public String msgRetour;
        private String[] codeRetour_type_info = new String[]{'codeRetour','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] msgRetour_type_info = new String[]{'msgRetour','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'codeRetour','msgRetour'};
    }
    public class NoDataFoundException extends Exception {
        public WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg;
        private String[] detVehInfoMsg_type_info = new String[]{'detVehInfoMsg','http://xml.bvm.icm.apv.bservice.renault','DetVehInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'detVehInfoMsg'};
    }
    public class getApvGetDetVehXmlResponse_element {
        public WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse getApvGetDetVehXmlReturn;
        private String[] getApvGetDetVehXmlReturn_type_info = new String[]{'getApvGetDetVehXmlReturn','http://xml.bvm.icm.apv.bservice.renault','ApvGetDetVehXmlResponse','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'getApvGetDetVehXmlReturn'};
    }
    public class ApvGetDetVehXmlRequest {
        public WS01_ApvGetDetVehXml.ServicePreferences servicePreferences;
        private String[] servicePreferences_type_info = new String[]{'servicePreferences','http://xml.bvm.icm.apv.bservice.renault','ServicePreferences','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'servicePreferences'};
    }
    public class BadRequestException extends Exception {
        public WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg;
        private String[] detVehInfoMsg_type_info = new String[]{'detVehInfoMsg','http://xml.bvm.icm.apv.bservice.renault','DetVehInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'detVehInfoMsg'};
    }
    public class SiErrorException extends Exception {
        public WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg;
        private String[] detVehInfoMsg_type_info = new String[]{'detVehInfoMsg','http://xml.bvm.icm.apv.bservice.renault','DetVehInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'detVehInfoMsg'};
    }
    public class getApvGetDetVehXml_element {
        public WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request;
        private String[] request_type_info = new String[]{'request','http://xml.bvm.icm.apv.bservice.renault','ApvGetDetVehXmlRequest','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'request'};
    }
    public class ServicePreferences {
        public String bvmsi25SocEmettrice;
        public String bvmsi25Vin;
        public String bvmsi25CodeLang;
        public String bvmsi25CodePaysLang;
        private String[] bvmsi25SocEmettrice_type_info = new String[]{'bvmsi25SocEmettrice','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] bvmsi25Vin_type_info = new String[]{'bvmsi25Vin','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] bvmsi25CodeLang_type_info = new String[]{'bvmsi25CodeLang','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] bvmsi25CodePaysLang_type_info = new String[]{'bvmsi25CodePaysLang','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://xml.bvm.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'bvmsi25SocEmettrice','bvmsi25Vin','bvmsi25CodeLang','bvmsi25CodePaysLang'};
    }
}