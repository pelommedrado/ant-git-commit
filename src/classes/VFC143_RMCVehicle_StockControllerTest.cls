@isTest
private class VFC143_RMCVehicle_StockControllerTest {

    static testMethod void myUnitTest() {
        Profile SellerProfile = [select id from Profile where Name= 'SFA - Seller' limit 1];
        
        Account parentAcc = new Account (Name ='ParentAcc');
        parentAcc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        parentAcc.ShippingCity = 'Barueri';
        parentAcc.ShippingState = 'SP';
        parentAcc.ShippingCountry = 'Brasil';
        parentAcc.ShippingPostalCode = '';
        parentAcc.Phone = '11999999999';
		parentAcc.ProfEmailAddress__c ='teste@teste.com'; 
		parentAcc.RecordTypeId = [select id from RecordType where developerName='Network_Acc' limit 1 ].id;
        database.insert(parentAcc);
        
        Account acc = new Account(Name = 'AR Motors Alphaville', parentID = parentAcc.id);
        acc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        acc.ShippingCity = 'Barueri';
        acc.ShippingState = 'SP';
        acc.ShippingCountry = 'Brasil';
        acc.ShippingPostalCode = '';
        acc.RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id;
        acc.IDBIR__c ='123ABC123';
        database.insert(acc);
        
        Contact contato = new Contact (FirstName = 'Test', LastName='Teste', AccountID=acc.id);
        database.insert(contato);
        
        User u = new User();
        u.Email = 'test@org.com';
        u.LastName = 'TestingUSer';
        u.Username = 'test@org1.com';
        u.Alias = 'tes';
        u.ProfileId = SellerProfile.id;
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.CommunityNickname = 'testing';
        u.ProfileId = SellerProfile.id;
        u.BIR__c ='123ABC123';
        u.contactID=contato.id;
        u.isCac__c=true;
        database.insert(u);
        
        Database.insert( new AccountShare(
          AccountId = acc.Id,
          UserOrGroupId = u.Id,
          AccountAccessLevel = 'Read' ,
          OpportunityAccessLevel = 'Read',
          CaseAccessLevel = 'Read'
        ) );
        
        Database.insert( new AccountShare(
          AccountId = parentAcc.Id,
          UserOrGroupId = u.Id,
          AccountAccessLevel = 'Read' ,
          OpportunityAccessLevel = 'Read',
          CaseAccessLevel = 'Read'
        ) );
        
        VEH_Veh__c veh = new VEH_Veh__c(
                    Name = '45444444444444444',
                    VehicleRegistrNbr__c = 'TH1003',
                    KmCheckDate__c = Date.today(),
                    DeliveryDate__c = Date.today() + 30,
                    VehicleBrand__c= 'Nissans',
                    KmCheck__c = 102,
                    Model__c = 'Fluence',
                    VersionCode__c = '1000',
                    DateofManu__c = Date.today() - 30,
                    ModelYear__c = 2012,
                    Color__c = 'Yellow',
                    Optional__c= 'Yes',
                    Price__c = 200000 ,
                    Status__c = 'Available',
                    Version__c = '1000',
                    Tech_VINExternalID__c = '9000000004');
        database.insert(veh);
        VRE_VehRel__c vehRel = new VRE_VehRel__c(
                    VIN__c = veh.Id,
                    StartDateRelation__c = Date.today() - 30,
                    EndDateRelation__c = Date.today() - 1,
                    Status__c= 'Active',
                    Account__c = parentAcc.id);
        database.insert(vehrel);
    
    	system.runAs(u){
	        PageReference pageRef = Page.VFP23_RMCVehicle_Stock;
	        Test.setCurrentPage(pageRef);
	        VFC143_RMCVehicle_StockController controller = new VFC143_RMCVehicle_StockController();
	        controller.modelName = 'Fluence';
	        controller.selectVehicle();
    	}
        
        
    }
}