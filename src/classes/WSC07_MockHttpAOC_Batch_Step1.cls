@isTest
global class WSC07_MockHttpAOC_Batch_Step1 implements HttpCalloutMock {

    /* This webservice method  called */
	global HTTPResponse respond(HTTPRequest req) {
		/* System assert check get method */
        System.assertEquals('GET', req.getMethod());
        /* initialize http response object */
        HttpResponse res = new HttpResponse();
        /* set header in http response */
        res.setHeader('Content-Type', 'application/json');
        /* set body in http response */
        res.setBody('{"marketingModelPresentation":{"asMap":{"mapRepresentation":{"map":{"KEM":{"presentationItem":{"key":"KEM","order":"000000","label":{"pt":"KANGOO EXPRESS"}}}},"metadata":{"key":"key","value":"presentationItem"}}},"colors":[{"presentationItem":{"key":"TEKNS","order":"00","label":{"pt":"Cinza Quartz"}}}],"key":"KEM","order":"000000","label":{"pt":"KANGOO EXPRESS"},"versionsPresentation":[{"presentationGroup":{"items":[{"presentationGroup":{"items":[{"versionItem":{"versionIdSpecCode":"VEC002_BRES","key":"VEC002_BRES","order":"0","label":{"pt":"Kangoo Express 1.6 16v"},"versionId":{"countrySpecCode":"BRES","versionSeq":"002"},"showableSpecCodes":["PROSCS"]}}],"key":"ENS_205","order":"0_0","label":{"pt":"Kangoo Express 2012/2013"}}}],"key":"ENS_203","order":"0","label":{"pt":"Kangoo Express"}}}]}}');
        /* set statuscode in http response */
        res.setStatusCode(200);
        return res;
	}
}