/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description Default mapping for Rforce required fields to create Attachment.
*/
global class Rforce_CreateAttachMapping_CLS {
/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description It accepts the Casedetails from the external service.
* @param case Details
* @param case instance
* @return List value according to request
*/
    public static List<Attachment> addAttachment(Rforce_W2CSchema_CLS.Casedetails caseDetails,Case cas)
    {
    
        List<Attachment> lstAttachment=new List<Attachment>();                                   
        for(Rforce_W2CSchema_CLS.CaseAttachments attch :caseDetails.attachmentDetails){   
            if(String.IsNotBlank(attch.attachmentName)  && String.IsNotBlank(attch.attachmentContentType) && attch.attachmentBody.size()>0){                     
            Attachment att = new Attachment();                                  
            att.Body=attch.attachmentBody;
            att.Name=attch.attachmentName+'.'+attch.attachmentContentType;
            att.parentId=cas.Id; 
            lstAttachment.add(att);
             }
        }                       
        try{
        // Perform some database operations that  might cause an exception.
            if(lstAttachment.size()>0){
            insert lstAttachment;
            }                      
        }catch(DmlException e) {
         // DmlException handling code here.
         System.debug('An exception occurred:' + e.getMessage());
         }catch(Exception e) {
         // Generic exception handling code here.
         System.debug('An Generic exception occurred:' + e.getMessage());
         } finally {
         // Perform some clean up.                     
         }
      return lstAttachment; 
    }
}