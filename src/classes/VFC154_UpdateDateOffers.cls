/**
*11/09/2014
* Felipe Livino {Kolekto}
*Projeto: Plataforma Varejo
*Objetivo: Atualizar a data da ultima modifica??o da oferta caso cumpra os requesitos de cada m�todo.
*/
public class VFC154_UpdateDateOffers {
  
  
  
  
  public VFC154_UpdateDateOffers(){}
  
  List<Group_Offer__c> listGroupOffer ;
  Map<Id,Group_Offer__c> mapGroupOffer = new Map<Id,Group_Offer__c>();
  
	public VFC154_UpdateDateOffers(List<Group_Offer__c> listGroupOfferTgr, Map<Id,Group_Offer__c> mapGroupOfferTgr){
		this.listGroupOffer = listGroupOfferTgr;
		mapGroupOffer =  mapGroupOfferTgr.clone();
	}
	
	public void NewMethodGroupOfferUpdateOffer(){
    	List<String> idsGroupOffers = new List<String>();
    	for(Group_Offer__c groupOffer : listGroupOffer){
    		//verifica se a data de fim é diferente
    		if(groupOffer.Date_End_Offer__c != mapGroupOffer.get(groupOffer.Id).Date_End_Offer__c || 
    			groupOffer.Date_Start_Offer__c != mapGroupOffer.get(groupOffer.Id).Date_Start_Offer__c){
    			idsGroupOffers.add(groupOffer.id);
    			System.debug('###### idsGroupOffers'+idsGroupOffers);
    		}
    	}
    	if(!idsGroupOffers.isEmpty() && idsGroupOffers.size()>0){
    		List<Offer__c> ofertas = [select id ,PV_OF_CheckUpdate__c from Offer__c 
	          where Group_Offer__c in: (idsGroupOffers) 
	                and Status__c = 'ACTIVE'
	                and Stage__c = 'Approved'];
	        System.debug('######## ofertas'+ofertas);        
	      	if(ofertas!=null){
		      	List<Offer__c> listaOfertas = new List<Offer__c>();
		     	for(Offer__C oferta : ofertas){
			      	if(oferta.PV_OF_CheckUpdate__c )
			         oferta.PV_OF_CheckUpdate__c = false;
			        else
			         oferta.PV_OF_CheckUpdate__c = true; 
			        listaOfertas.add(oferta);
		    	}
		    	System.debug('######## listaOfertas '+listaOfertas); 
		    	Database.update(listaOfertas);
      		}
    	}
    
         
    
  }
	
	public void NewMethodgroupOfferUpdateStatusOffer(){
     
      List<String> idGroupOffer = new List<String>();
     
      for(Group_Offer__c groupOffer : listGroupOffer){
      	 if(groupOffer.Status__c == 'Inactive')
      		idGroupOffer.add(groupOffer.id);
      }
	  if(!idGroupOffer.isEmpty() && idGroupOffer.size()>0){	
	      List<Offer__c> ofertas = [select id , LastModifiedDate from Offer__c 
	          where Group_Offer__c in:(idGroupOffer) and Status__c = 'ACTIVE'];
	     
	      if(ofertas!=null){
		  	List<Offer__c> listaOfertas = new List<Offer__c>();
		    for(Offer__C oferta : ofertas){
		    	oferta.Status__c = 'INACTIVE';
		        listaOfertas.add(oferta);
		    }
		    update(listaOfertas);
	        
	      }
	  }
      
      
      
  }
	
	
	
	public void groupOfferUpdateStatusOffer(Group_Offer__c grupoNovo){
      if(grupoNovo.Status__c == 'Inactive'){
        List<Offer__c> ofertas = [select id , LastModifiedDate from Offer__c 
          where Group_Offer__c =: grupoNovo.id 
                and Status__c = 'ACTIVE'];
        if(ofertas!=null){
	        List<Offer__c> listaOfertas = new List<Offer__c>();
	        for(Offer__C oferta : ofertas){
	          oferta.Status__c = 'INACTIVE';
	          listaOfertas.add(oferta);
	        }
	        update(listaOfertas);
        }
      }
      
      
      
  }
	
	
	
  
  //Atualiza??o caso o grupo de oferta de mude
  public void groupOfferUpdateOffer(Group_Offer__c grupoAntigo, Group_Offer__c grupoNovo){
    if(grupoAntigo.Date_End_Offer__c != grupoNovo.Date_End_Offer__c || 
        grupoAntigo.Date_Start_Offer__c != grupoNovo.Date_Start_Offer__c){
      List<Offer__c> ofertas = [select id ,PV_OF_CheckUpdate__c from Offer__c 
          where Group_Offer__c =: grupoNovo.id 
                and Status__c = 'ACTIVE'
                and Stage__c = 'Approved'];
      if(ofertas!=null){
	      List<Offer__c> listaOfertas = new List<Offer__c>();
	      for(Offer__C oferta : ofertas){
	      	if(oferta.PV_OF_CheckUpdate__c )
	         oferta.PV_OF_CheckUpdate__c = false;
	        else
	         oferta.PV_OF_CheckUpdate__c = true; 
	        listaOfertas.add(oferta);
	      }
	      Database.update(listaOfertas);
      }
    }
  }
  
  
    
    /*
    
    List<Offer__c> ofertas = [select id , LastModifiedDate from Offer__c 
          where Group_Offer__c =: grupo.id 
                and Status__c = 'ACTIVE'
                and Stage__c = 'Approved'];
    if(ofertas!=null){
      List<Offer__c> listaOfertas = new List<Offer__c>();
      for(Offer__C oferta : ofertas){
        oferta.LastModifiedDate = Date.Today();
        listaOfertas.add(oferta);
      }
      update(listaOfertas);
    }
  }*/
  //Atualiza a oferta quando os campos de vantagens da oferta ou foto destaque forem alterados
  public void modelUpdateToOffer(PVVersion__c versaoAntiga, PVVersion__c versaoNova){
    //if(versaoAntiga!=null )
    if(versaoAntiga.Comfort__c != versaoNova.Comfort__c || versaoAntiga.Power__c != versaoNova.Power__c ||
         versaoAntiga.Power__c != versaoNova.Power__c ||
          versaoAntiga.Security__c != versaoNova.Security__c || versaoAntiga.Technology__c != versaoNova.Technology__c || 
          versaoAntiga.Volume__c != versaoNova.Volume__c || versaoAntiga.FeaturedPicture__c != versaoNova.FeaturedPicture__c){
          	
             
             List<Offer__c> ListaOferta = [select id , LastModifiedDate,PV_Of_LastUpdate__c from Offer__c where Version__c =: versaoNova.id and Status__c = 'ACTIVE' and Stage__c = 'Approved'];
             List<Offer__c> ofertasAtualizadas = new List<Offer__c>();
             
             if(ListaOferta!=null)  {  
                for(Offer__c oferta : ListaOferta){
                    if(oferta.PV_Of_LastUpdate__c)
		                  	oferta.PV_Of_LastUpdate__c = false;
		                else
                        oferta.PV_Of_LastUpdate__c = true; 
                    ofertasAtualizadas.add(oferta);
                }
               if(ofertasAtualizadas!=null && ofertasAtualizadas.size()>0)    
               update  ofertasAtualizadas;
             }
    }
    
  }
  //Atualiza a versao com os valores das vantagens ou da foto
 
  
    
  
  public static void DealerGroupUpdateOffer(Dealer_Group_Offer__c dealer){
  	 //atualiza o grupo de oferta
  	 Group_Offer__c groupD = new Group_Offer__c();
  	 groupD.id = dealer.GroupOffer__c;
  	 groupD.PV_GO_LastUpdate__c = true;
  	 update groupD;
  	 
  	 //abaixo atualiza a oferta
	    List<Offer__c> listaOferta = [select id, PV_Of_LastUpdate__c from Offer__c where Group_Offer__c =:dealer.GroupOffer__c];
	    List<Offer__c> ofertasAtualizadas = new List<Offer__c>();
	    if(ListaOferta!=null)  {  
	              for(Offer__c oferta : ListaOferta){
	                 if(oferta.PV_Of_LastUpdate__c)
	                   oferta.PV_Of_LastUpdate__c = false;
	                 else
	                   oferta.PV_Of_LastUpdate__c = true; 
	                 ofertasAtualizadas.add(oferta);
	              }
	              if(ofertasAtualizadas!=null && ofertasAtualizadas.size()>0)    
	              update  ofertasAtualizadas;
	    }
  }
  
  
  public  static void DealerUpdateOffer(Dealer_Offer__c dealer){
  	   List<Offer__c> listaOferta = [select id, PV_Of_LastUpdate__c from Offer__c where Id =:dealer.Offer__c];
       List<Offer__c> ofertasAtualizadas = new List<Offer__c>();
       if(ListaOferta!=null)  {  
                for(Offer__c oferta : ListaOferta){
                   if(oferta.PV_Of_LastUpdate__c)
                     oferta.PV_Of_LastUpdate__c = false;
                   else
                     oferta.PV_Of_LastUpdate__c = true; 
                   ofertasAtualizadas.add(oferta);
                }
                if(ofertasAtualizadas!=null && ofertasAtualizadas.size()>0)    
                  update  ofertasAtualizadas;
      }
  }
  
  
  
  
  
}