public class DashLembreteServico {

    public List<Lembrete> obterLembrete(Opportunity opp) {
        List<Lembrete> lembreteList = new List<Lembrete>();

        List<Task> taskList = [
            SELECT Id, ActivityDatetime__c, toLabel(Subject), toLabel(Status), WhoId
            FROM Task  WHERE whatId =: opp.Id AND ActivityDatetime__c != NULL
            ORDER BY ActivityDatetime__c ASC
        ];
        for(Task t : taskList) {
            Lembrete lem = createLembrete(t);
            lembreteList.add(lem);
        }
        List<Event> lemEventList = [
            SELECT Id, ActivityDatetime__c, toLabel(Subject), toLabel(Status__c) FROM Event
            WHERE whatId =: opp.Id AND ActivityDatetime__c != NULL
            ORDER BY ActivityDatetime__c ASC
        ];
        for(Event e : lemEventList) {
            Lembrete lem = createLembrete(e);
            lembreteList.add(lem);
        }
        return lembreteList;
    }

    public List<Lembrete> obterLembrete(Id userId) {
      System.debug('obterLembrete()');

        Datetime futuras = System.now().addMinutes(15);
        List<Lembrete> lembreteList = new List<Lembrete>();

        System.debug('userId: ' + userId);
        System.debug('Data futuras:' + futuras);
        List<Task> taskList = [
            SELECT Id, ActivityDatetime__c, toLabel(Subject), whoId
            FROM Task WHERE OwnerId =: userId AND ActivityDatetime__c <=: futuras
            AND Status NOT IN('Deferred', 'Completed') AND IsReminderSet = true
            ORDER BY ActivityDatetime__c ASC
        ];

        for(Task tak : taskList) {
          final Lembrete lem = createLembrete(tak);
          lembreteList.add(lem);
        }

        List<Event> lemEventList = [
            SELECT Id, ActivityDatetime__c, toLabel(Subject) FROM Event
            WHERE OwnerId =: userId AND ActivityDatetime__c <=: futuras
            AND Status__c NOT IN('Canceled', 'Completed')  AND IsReminderSet = true
            ORDER BY ActivityDatetime__c ASC
        ];

        for(Event e : lemEventList) {
          final Lembrete lem = createLembrete(e);
          lembreteList.add(lem);
        }

        return sortLembrete(lembreteList, 1);
    }

    public Lembrete createLembrete(Task task) {
        Lembrete le = new Lembrete();
        le.Id 		= task.Id;
        le.task 	= task;
        le.subject = task.Subject;
        le.assunto 	= DashboardUtil.traduzirSubject(task.Subject);
        le.data 	= DataUtils.getUserDatetime(task.ActivityDatetime__c);

        if(task.WhoId != null && task.WhoId.getSObjectType() == Lead.sObjectType){
            le.lead = true;
        }

        calcFormatarTempoAtraso(le);

        return le;
    }

    public Lembrete createLembrete(Event event) {
        Lembrete le = new Lembrete();
        le.Id = event.Id;
        le.event = event;
        le.subject = event.Subject;
        le.assunto = DashboardUtil.traduzirSubject(event.Subject);

        le.data = DataUtils.getUserDatetime(event.ActivityDatetime__c);

        calcFormatarTempoAtraso(le);

        return le;
    }

    public Lembrete createLembrete(Opportunity opp) {
      final String subjectActivity = decideBDCsubject(opp);
      if(String.isEmpty(subjectActivity)) {
        System.debug('Nao foi possivel encontrar uma valor selecionado');
        return null;
      }
      final Lembrete lem = new Lembrete();
      lem.subject = subjectActivity;
      lem.assunto = DashboardUtil.traduzirSubject(subjectActivity);
      return lem;
    }

    /** Decidir assuntos */
    private String decideBDCsubject(Opportunity opp){
      System.debug('decideBDCsubject()');

      if(opp.ChaseOpportunity__c) {
          return 'Persecution';
      } else if(opp.RescueOpportunity__c) {
          return 'Rescue';
      } else if(opp.ConfirmDeliveryOpportunity__c) {
          return 'Delivery Confirmation';
      }
      return null;
    }

    public Lembrete createLembrete(Task task, Opportunity opp) {
      final Lembrete lem = createLembrete(task);
      lem.opp = opp;
      lem.descricao = task.Description;
      lem.ownerId 	= task.OwnerId;
      lem.ownerName = task.Owner.Name;
      if(!String.isEmpty(task.AccountId)) {
        lem.NomeCliente = task.Account.Name;
      }
      if(lem.opp != null) {
        lem.veiculo = lem.opp.VehicleInterest__c;
      }
      return lem;
    }

    public Lembrete createLembrete(Event event, Opportunity opp) {
      final Lembrete lem = createLembrete(event);
      lem.opp = opp;
      lem.descricao = event.Description;
      lem.ownerId 	= event.OwnerId;
      lem.ownerName = event.Owner.Name;

      if(!String.isEmpty(event.AccountId)) {
        lem.NomeCliente = event.Account.Name;
      }
      if(lem.opp != null) {
        lem.veiculo = lem.opp.VehicleInterest__c;
      }
      return lem;
    }

    private void calcFormatarTempoAtraso(Lembrete lem) {
        final DataUtils.DiffData diffD = new  DataUtils.DiffData(
            lem.data,
            DataUtils.getUserDatetime(Datetime.now())
        );
        final Integer segundo = 1000;
        final Integer minuto = 60;
        final Double diffMilesegundo = (diffD.tFim - diffD.tIni);
        final Integer atrasoMin = (Integer) (diffMilesegundo / segundo) / minuto;
        lem.atrasado = atrasoMin > 0;
        lem.atraso = diffD.format();
    }

    public List<Lembrete> sortLembrete(List<Lembrete> lembretes, Integer order) {
        lembretes.sort();
        if(order == 0) { //ASC
            return lembretes;
        } else {
            List<Lembrete> lemListTemp = new List<Lembrete>();
            for(Integer i = lembretes.size()-1; i >=0; i-- ) {
                lemListTemp.add(lembretes.get(i));
            }
            return lemListTemp;
        }
    }

    public class Lembrete implements Comparable {
        public Id id 			{ get;set; }
        public Task task 		{ get;set; }
        public Event event 		{ get;set; }
        public Datetime data 	{ get;set; }
        public String atraso 	{ get;set; }
        public String assunto 	{ get;set; }
        public String subject 	{ get;set; }
        public Boolean atrasado { get;set; }
        public Opportunity opp 	{ get;set; }
        public String nomeCliente { get;set; }
        public String descricao { get;set; }
        public String veiculo	{ get;set; }
        public Id ownerId       { get;set; }
        public String ownerName { get;set; }
        public boolean lead 	{ get;set; }

        public Lembrete() {
            this.lead = false;
        }
        public Integer compareTo(Object compareTo) {
            Lembrete compareToLem = (Lembrete) compareTo;

            if(this.data > compareToLem.data) {
                return 1;
            } else if(this.data < compareToLem.data) {
                return -1;
            }
            return 0;
        }
    }
}