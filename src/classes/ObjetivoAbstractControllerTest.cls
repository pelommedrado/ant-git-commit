@isTest
private class ObjetivoAbstractControllerTest {
	
	static Monthly_Goal_Group__c goalGroup;
	static Monthly_Goal_Dealer__c goalDealer;
	static User userSeller;
	static Account dealer;

	static{

		User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];

		System.runAs(localUser) {

			dealer = AccountBuild.getInstance().createAccountDealer();
			dealer.Dealer_Matrix__c = '1234';
			insert dealer;

			Contact userCnt = ContactBuild.getInstance().createContact(dealer.Id);
			insert userCnt;

			userSeller = UserBuild.getInstance().createUser(userCnt.Id, [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id);
			insert userSeller;

			goalGroup = MonthlyGoalBuild.getInstance().createGoalGroup('June', 2017, '1234');
			insert goalGroup;

			goalDealer = MonthlyGoalBuild.getInstance().createGoalDealer(goalGroup.Id, dealer.Id);
			insert goalDealer;

		}	

	}
	
	@isTest static void shouldInitManager() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoGeral')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoGeralController objCtrl = new ObjetivoGeralController();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldInitExecutive() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoGeral')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Executive'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoGeralController objCtrl = new ObjetivoGeralController();
		}
		

		Test.stopTest();

	}
	
}