public with sharing class Myr_PersonalDataMDM_Cls {

	/**
	* @description  Call the MDM PErsonal Data webservice through Transverse team implementation
	*               and map the results wuithin the MyRenault representation (Myr_PersonalDataResponse_Cls)
	* @params Myr_PersonalData_WS.RequestParameters the parameters to use
	* @return Myr_PersonalDataResponse_Cls, the response 
	* TODO : once all the webservices call will be made by transverse team, we will destroy the MyRenault
	*        common format (Myr_PersonalDataResponse_Cls) and use the Transverse team common format instead
	*        ( TRVResponseWebservices)
	*/
	public static Myr_PersonalDataResponse_Cls callMDM(Myr_PersonalData_WS.RequestParameters iParams) { 
		system.debug('### Myr_PersonalDataMDM_Cls - <callMDM> - params=' + iParams);
		Myr_PersonalDataResponse_Cls mdmResponse = new Myr_PersonalDataResponse_Cls();
		TRVRequestParametersWebServices.RequestParameters mdmParams = prepareTRVMDMParameters(iParams);
		TRVResponseWebservices trvResponse = TRVWebservicesManager.requestSearchParty(mdmParams);
		system.debug('### Myr_PersonalDataMDM_Cls - <callMDM> - SearchPArty, number of clients found: ' 
			+ ((trvResponse != null && trvResponse.response != null && trvResponse.response.infoClients != null)?String.valueOf(trvResponse.response.infoClients.size()):'0'));
		if( trvResponse != null && trvResponse.response != null && trvResponse.response.infoClients != null && trvResponse.response.infoClients.size() == 1) {
			system.debug('### Myr_PersonalDataMDM_Cls - <callMDM> - only 1 response found, call GetParty');
			//ok we found only one reponse :-D, we can continue by getting the vehicles
			mdmParams.strPartyID = trvResponse.response.infoClients[0].strPartyID;
			TRVResponseWebservices trvDetailsResponse = TRVWebservicesManager.requestGetParty(mdmParams);
			mdmResponse = mapTRVMDMResponse(trvDetailsResponse);
		}
		return mdmResponse;
	}

	/**
	* @description Map the original MyRenault Personal Data parameters within the format of call of the transverse team
	* @param iParams, the original RequestParameters
	* @return  
	*/
	@TestVisible private static TRVRequestParametersWebServices.RequestParameters prepareTRVMDMParameters(Myr_PersonalData_WS.RequestParameters iParams) {
		TRVRequestParametersWebServices.RequestParameters trvParams = new TRVRequestParametersWebServices.RequestParameters();
		trvParams.user = iParams.user;
		trvParams.demander = iParams.demander;
		trvParams.user_x = iParams.user_x;
		trvParams.vin = iParams.vin;
		trvParams.registration = iParams.registration;
		trvParams.country = iParams.country; 
		trvParams.technicalCountry = iParams.country; //added for multi countries problems ....
		trvParams.lastname = iParams.lastname;
		trvParams.firstname = iParams.firstname;
		trvParams.brand = iParams.brand;
		trvParams.email = iParams.email;
		trvParams.city = iParams.city;
		trvParams.zip = iParams.zip;
		trvParams.idClient = iParams.idClient;
		trvParams.ident1 = iParams.ident1;
		trvParams.strLocalID = iParams.strLocalID;
		trvParams.strFixeLandLine = iParams.strFixeLandLine;
		trvParams.strMobile = iParams.strMobile;
		trvParams.strPartyID = iParams.strPartyID;
		trvParams.nbReplies = String.valueOf(iParams.nbReplies);
		trvParams.logger = false; //no logs for MyRenault !&
		return trvParams;
	}
		
	/**
	* @description Map the response found by the transverse team within the MyRenault format
	* @param trvResponse, the response sent by Call made by Transversal team
	* @return Myr_PersonalDataResponse_Cls the MyRenault response
	*/
	private static Myr_PersonalDataResponse_Cls mapTRVMDMResponse(TRVResponseWebservices trvResponse) {
		system.debug('### Myr_PersonalDataMDM_Cls - <mapTRVMDMResponse> - trvResponse = ' + trvResponse);
		Myr_PersonalDataResponse_Cls mdmResponse = new Myr_PersonalDataResponse_Cls();
		mdmResponse.response.dataSource = Myr_PersonalData_WS.DataSourceType.MDM;
		for( TRVResponseWebservices.InfoClient trvClient : trvResponse.response.infoClients ) {
			Myr_PersonalDataResponse_Cls.InfoClient myrClient = new Myr_PersonalDataResponse_Cls.InfoClient();
			//---IDENTITY
			myrClient.LastName = trvClient.LastName1;
			myrClient.MiddleName = trvClient.LastName2;
			myrClient.FirstName = trvClient.FirstName1;
			myrClient.FirstName2 = trvClient.FirstName2; 
			myrClient.Title = trvClient.Title;
			myrClient.Lang = trvClient.Lang;
			myrClient.email = trvClient.email; 
			myrClient.emailPro = trvClient.emailPro;
			myrClient.sex = trvClient.sex;
			//---RENAULT LOCAL IDS
			myrClient.strPartyID = 'MDM-' + trvClient.strPartyID;
			myrClient.strPartySub = trvClient.partySub;
			myrClient.strDOB = trvClient.strDOB;
			myrClient.DateOfBirth = parseMDMDates(trvClient.strDOB, 'Date of Birth');
			myrClient.strAccoutType = trvClient.strAccountType;
			//myrClient.typeperson = trvClient.typeperson;
			myrClient.CustIdentNumber = trvClient.localCustomerID1;
			myrClient.CustIdentNumber2 = trvClient.localCustomerID2;
			//myrClient.MyrId = trvClient.myRenaultID;
			myrClient.MaritalStatus = trvClient.marital; 
			myrClient.RenaultGroupStaff = false;
			if( !String.isBlank(trvClient.renaultEmployee) ) {
				if( trvClient.renaultEmployee.equalsIgnoreCase('Y') ) {
					myrClient.RenaultGroupStaff = true;
				} else if( trvClient.renaultEmployee.equalsIgnoreCase('N') ){
					myrClient.RenaultGroupStaff = false;
				}
			}
			myrClient.NumberOfChildren = trvClient.nbrChildrenHome; 
			if( !String.isBlank( trvClient.deceased ) ) {
				if( trvClient.deceased.equalsIgnoreCase('Y') ) {
					myrClient.Deceased = 'Yes';
				} else if( trvClient.deceased.equalsIgnoreCase('N') ) {
					myrClient.Deceased = 'No';
				}
			}
			myrClient.Industry = trvClient.OccupationalCategoryCode;
			myrClient.JobClass = trvClient.OccupationalCategoryCodeP;
			myrClient.CommercialName = trvClient.commercialName;
			myrClient.FinancialStatus = trvClient.financialStatus;
			myrClient.PartySegment = trvClient.partySegment;

			//---COMMUNICATION AGRREEMENT
			myrClient.GlobalCommAgreement = trvClient.GlobalCommAgreement;
			myrClient.PostCommAgreement = trvClient.PostCommAgreement;
			myrClient.TelCommAgreemen = trvClient.TelCommAgreement;
			myrClient.SMSCommAgreement = trvClient.SMSCommAgreement;
			myrClient.EmailCommAgreement = trvClient.EmailCommAgreement;
			myrClient.PreferedMedia = trvClient.preferredMedia; //Not mapped in MyRenault model
			myrClient.stopComFlag = trvClient.stopComFlag;
			myrClient.stopComFlagDate = trvClient.stopComFlagDate;
			myrClient.stopComSMDFlag = trvClient.stopComSMDFlag;
			myrClient.stopComSMDFlagDate = trvClient.stopComSMDFlagDate;

			//---PHONES
			if( !String.isBlank(trvClient.strFixeLandLine) ) {
				myrClient.LandLine1 = trvClient.strFixeLandLine.replaceAll('[\\s]*','');
			}
			if( !String.isBlank(trvClient.strFixeLandLinePro) ) {
				myrClient.LandLine2 = trvClient.strFixeLandLinePro.replaceAll('[\\s]*','');
			}
			if( !String.isBlank(trvClient.strMobile) ) {
				myrClient.MobilePhone1 = trvClient.strMobile.replaceAll('[\\s]*','');
			}
			//---ADDRESS
			myrClient.AreaCode = trvClient.region;
			//myrClient.streetType = trvClient.streetType; //In dup with StrType ... which field has to be used for MDM ?
			//myrClient.strNumber = trvClient.strNumber;   //In dup with StrNum ... which field has to be used for MDM ?
			myrClient.StrNum = trvClient.StrNum;
			myrClient.StrType = trvClient.StrType;
			myrClient.StrName = trvClient.AddressLine1;
			myrClient.StrCompl1 = trvClient.AddressLine2;
			myrClient.StrCompl2 = trvClient.Compl1;
			myrClient.StrCompl3 = trvClient.Compl3;
			myrClient.CountryCode = trvClient.CountryCode;
			myrClient.Zip = trvClient.Zip;
			myrClient.City = trvClient.City;
			myrClient.PersonOtherStreet = trvClient.PersonOtherStreet;
			myrClient.PersonOtherCity = trvClient.PersonOtherCity;
			myrClient.PersonOtherState = trvClient.PersonOtherState;
			myrClient.PersonOtherPostalCode = trvClient.PersonOtherPostalCode;
			myrClient.PersonOtherCountry = trvClient.PersonOtherCountry;

			//--- VEHICLES
			for( TRVResponseWebservices.Vehicle trvVeh : trvClient.vcle ) {
				Myr_PersonalDataResponse_Cls.Vehicle myrVeh = new Myr_PersonalDataResponse_Cls.Vehicle();
				myrVeh.vin = trvVeh.vin;
				myrVeh.IdClient = trvVeh.IdClient;
				myrVeh.brandCode = trvVeh.brandCode;
				myrVeh.model = trvVeh.model;
				myrVeh.modelCode = trvVeh.modelCode;
				myrVeh.modelLabel = trvVeh.modelLabel;
				myrVeh.VNVO_NewVehicle = null;
				if( !String.isBlank(trvVeh.vnvo) ) {
					if( trvVeh.vnvo.equals('N') || trvVeh.vnvo.equals('VN') ) {
						myrVeh.VNVO_NewVehicle = true; 
					} else if (trvVeh.vnvo.equals('O') || trvVeh.vnvo.equals('VO')) {
						myrVeh.VNVO_NewVehicle = false; 
					}
				}

				//myrVeh.versionLabel = trvVeh.versionLabel;
				myrVeh.firstRegistrationDate = parseMDMDates(trvVeh.firstRegistrationDate, 'First REgistration Date');
				myrVeh.lastRegistrationDate = parseMDMDates(trvVeh.lastRegistrationDate, 'First REgistration Date');
				myrVeh.registration = trvVeh.registrationNumber;
				myrVeh.possessionBegin = parseMDMDates(trvVeh.possessionBegin, 'Possession Begin');
				myrVeh.possessionEnd = parseMDMDates(trvVeh.possessionEnd, 'Possession End');
				myrVeh.DeliveryDate = parseMDMDates(trvVeh.deliveryDate, 'Devivery Date');
				myrVeh.TypeRelationship = trvVeh.vehicleType;
				//myrVeh.startDate = trvVeh.startDate;
				//myrVeh.endDate = trvVeh.endDate;
				myrClient.vcle.add(myrVeh);
			}
			mdmResponse.response.infoClients.add( myrClient ) ;

			//--- DEALERS: no dealers currently in MDM
		}
		system.debug('### Myr_PersonalDataMDM_Cls - <mapTRVMDMResponse> - mdmResponse = ' + mdmResponse);
		return mdmResponse;
	}

	/** Format of the date expected for BCS is YYYY-MM-DD
  		@return Date parsed from the entry
  	**/
  	private static Date parseMDMDates(String iDate, String iInfoDate) {
  		try {
			Date newDate = null;
			if( iDate.contains('-') ) {
				String[] myDate = iDate.split('-');
				newDate = Date.newinstance(Integer.valueOf(myDate[0]), Integer.valueOf(myDate[1]), Integer.valueOf(myDate[2]));
			} else {
				newDate = Date.parse(iDate);
			}
			return newDate;	
		} catch (Exception e) {
			//continue without interrupting the treatment
			system.debug('### Myr_PersonalData_WS - <mapTRVMDMResponse> - '+iInfoDate+' conversion problem: ' + e.getMessage());
		}
		return null;
  	}

}