@RestResource(urlMapping='/facebookId/*')
global class FacebookIdToAccountWS {

	public static String facebookUserId {get;set;}
	public static String facebookPageId {get;set;}
	public static String clientUserId 	{get;set;}
	public static String calloutType	{get;set;}
	public static String customP 		{get;set;}

	global class WSResponse{
        public String message;
        public String error;
        public Id accountId;
    }

    @HttpPost
    global static WSResponse assignFacebookId(String facebook_user_id, String facebook_page_id,
                                     String api_key, String client_user_id, String category) {
    	facebookUserId = facebook_user_id;
    	facebookPageId = facebook_page_id;
    	clientUserId   = client_user_id;
    	calloutType    = category == null ? '' : category;

    	List<Account> accToAssign = [SELECT Id FROM Account WHERE Id =: clientUserId];


    	System.debug('Account For Assignment: '+accToAssign);

		WSResponse wsReturn = new WSResponse();    	

    	if(!accToAssign.isEmpty()){

    		customP = '';

    		if(category.equalsIgnoreCase('PV')){

	    		List<SurveySent__c> pesquisa = [SELECT Id, BIR__c FROM SurveySent__c WHERE Account__c =: accToAssign[0].Id ORDER BY SendDate__c DESC LIMIT 1];

	    		if(!pesquisa.isEmpty()){
	    			Account concessionaria = [SELECT Id, Name FROM Account WHERE IDBIR__c =: pesquisa[0].BIR__c];
	    		
	    			customP = concessionaria.Name;
    			}
    			
    		}

    		accToAssign[0].FacebookPageId__c = facebookPageId;
    		accToAssign[0].FacebookUserId__c = facebookUserId;
			try{
    			Database.update(accToAssign[0]);
    		}
    		catch(Exception ex){
    			System.debug('Account Excetpion: '+ex.getMessage());
				wsReturn.message = 'Error';
				wsReturn.error = ex.getMessage();

				sendEmailError(wsReturn);
				return wsReturn;
    		}
			try{
				Database.executeBatch(new BotConnectionBatch(facebookUserId, facebookPageId, clientUserId, calloutType, customP));
			}
			catch(Exception ex){
				System.debug('Batch/Callout Exception: '+ex.getMessage());
				wsReturn.message = 'Error';
				wsReturn.error = ex.getMessage();

				sendEmailError(wsReturn);
				return wsReturn;
			}
			wsReturn.message = 'Sucess';
			wsReturn.error = '';
			wsReturn.accountId = accToAssign[0].Id;
			return wsReturn;
    	}
    	wsReturn.message = 'Error';
		wsReturn.error = 'Account not Found';
		sendEmailError(wsReturn);
		return wsReturn;
    }

   	public static void facebookConnection(String facebookId, String facebookPageId, String clientUserId, 
   											String category, String customProperty){
   		String endpoint = 'https://app.nama.ai/api/v1/bots/58/send_message';
   		if(category.equalsIgnoreCase('VN')){
	   		HTTPRequest req = new HTTPRequest();
			req.setEndpoint(endpoint);
			req.setMethod('POST');

			req.setHeader('Content-Type', 'application/json');
			req.setHeader('Authorization', 'Token token=FeF9uMK63SN8wRpawePKmoJ4');

			String body = '{"user_identifier": "'+facebookId+'",'
						+ '"channel": "FacebookPage",'
						+ '"channel_identifier": "'+facebookPageId+'",'
						+ '"function": "GREETING_RENATA",'
						+ '"custom_properties":'
						+ '{'
						+ '"car": "'+customProperty+'",'
						+ '"client_user_id": "'+clientUserId+'"'
						+ '}'
						+ '}';

			req.setBody(body);

			Http h = new Http();
			HttpResponse res = h.send(req);

			System.debug('GREETING_RENATA: '+res.getStatus());
		}
		else if(category.equalsIgnoreCase('PV')){
			HTTPRequest req = new HTTPRequest();
			req.setEndpoint(endpoint);
			req.setMethod('POST');

			req.setHeader('Content-Type', 'application/json');
			req.setHeader('Authorization', 'Token token=FeF9uMK63SN8wRpawePKmoJ4');

			String body = '{"user_identifier": "'+facebookId+'",'
						+ '"channel": "FacebookPage",'
						+ '"channel_identifier": "'+facebookPageId+'",'
						+ '"function": "GREETING_RENAM",'
						+ '"custom_properties":'
						+ '{'
						+ '"dealership": "'+customProperty+'",'
						+ '"client_user_id": "'+clientUserId+'"'
						+ '}'
						+ '}';

			req.setBody(body);

			Http h = new Http();
			HttpResponse res = h.send(req);

			System.debug('GREETING_RENAM: '+res.getStatus());
		}
		else{
			HTTPRequest req = new HTTPRequest();
			req.setEndpoint(endpoint);
			req.setMethod('POST');

			req.setHeader('Content-Type', 'application/json');
			req.setHeader('Authorization', 'Token token=FeF9uMK63SN8wRpawePKmoJ4');

			String body = '{"user_identifier": "'+facebookId+'",'
						+ '"channel": "FacebookPage",'
						+ '"channel_identifier": "'+facebookPageId+'",'
						+ '"function": "GREETING_RENATA",'
						+ '"custom_properties":'
						+ '{'
						+ '"car": "'+customProperty+'",'
						+ '"client_user_id": "'+clientUserId+'"'
						+ '}'
						+ '}';

			req.setBody(body);

			Http h = new Http();
			HttpResponse res = h.send(req);

			System.debug('GREETING_RENATA: '+res.getStatus());
		}
   	}

   	public static void sendEmailError(WSResponse fail){
   		String treatedMessage 	= fail.message == null ? '' : fail.message; 
   		String treatedError 	= fail.error == null ? '' : fail.error; 
   		String treatedId	 	= fail.accountId == null ? '' : fail.accountId; 

   		String today = String.valueOf(System.today());
   		List<String> dateAux = new List<String>();
   		dateAux = today.split('-');
   		if(dateAux.size() == 3){
   			today = dateAux[2]+'/'+dateAux[1]+'/'+dateAux[0];
   		}

   		List<Messaging.SingleEmailMessage> lstEmailSend = new List<Messaging.SingleEmailMessage>();

   		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.subject  = 'Facebook Connection Fail - '+today;
        message.htmlBody 	= '<html><body><div>Erros: <br/>Message: '+treatedMessage+'<br/>Error: '
        					+ treatedError+'<br/>AccountId: '+treatedId+'<br/></div></body></html>';
        message.toAddresses = new List<String>{'marques@kolekto.com.br', 'prandini@kolekto.com.br', 
        'lucas@kolekto.com.br ', 'marcelino@kolekto.com.br '};
		lstEmailSend.add(message);

   		Messaging.SendEmailResult[] results;
		try{
			results = Messaging.sendEmail(lstEmailSend);
		}
        catch(Exception ex){
            System.debug('Email Exception: '+ex);
        }
   	}

}