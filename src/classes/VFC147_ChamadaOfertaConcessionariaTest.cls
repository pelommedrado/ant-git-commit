@isTest
public with sharing class VFC147_ChamadaOfertaConcessionariaTest {
    
    static testMethod void myUnitTest(){
        Model__c model = new Model__c(
            ENS__c = 'abc',
            Market__c = 'abc',
            Status__c = 'Active',
            Model_Spec_Code__c = 'ABC',
            Phase__c = '1',
            Model_PK__c = 'ABC-1'
        );
        Database.insert( model );
        
        PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
            Model__c = model.Id,
            Start_Date__c = System.today().addMonths( -1 ),
            End_Date__c = System.today().addMonths( 1 ),
            Type_of_Action__c = 'abc',
            Status__c = 'Active'
        );
        Database.insert( commercialAction );
        
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
            Commercial_Action__c = commercialAction.Id,
            Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
            Call_Offer_End_Date__c = System.today().addMonths( 1 ),
            Minimum_Entry__c = 100.0,
            Period_in_Months__c = 5,
            Month_Rate__c = 0.99
        );
        Database.insert( callOffer );
        
        User manager = new User(
            
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
            //RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
            Name = 'Concession�ria teste',
            IDBIR__c = '123ABC123',
            RecordTypeId = '012D0000000KAoH',
            Country__c = 'Brazil',
            NameZone__c = 'R2',
            ShippingState = 'SP',
            ShippingCity = 'S�o Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true
            
        );
        Database.insert( dealerAcc );
        
        PVCall_Offer__c cal = new PVCall_Offer__c(
            Commercial_Action__c = commercialAction.Id,
            Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
            Call_Offer_End_Date__c = System.today().addMonths( 1 ),
            Minimum_Entry__c = 100.0,
            Period_in_Months__c = 5,
            Month_Rate__c = 0.99
        );
        Database.insert( cal );
        
        List<Account> cssInit = new List<Account>(); 
        VFC147_ChamadaOfertaConcessionaria vfdw2 = new VFC147_ChamadaOfertaConcessionaria();
        //vfdw2.init(cal.Id);
        
        List<Account> lta = new List<Account>();
        lta.add(dealeracc);
        VFC147_ChamadaOfertaConcessionaria vfc = new VFC147_ChamadaOfertaConcessionaria(callOffer);
        
        vfc.todas_Css();
        vfc.adicionar_css();
        
        
        vfc.getAllCidade();
        vfc.getAllest();
        vfc.cidade_adicionada();
        vfc.salvar();
        vfc.getCallOffer();
        vfc.getCidades10();
        vfc.getCidades_removidas();
        vfc.getCss10();
        vfc.getEstado_adicionado();
        vfc.getEstadual();
        vfc.getReg();
        vfc.getRegiao_removida();
        vfc.getTodas_Cidades();
        vfc.getTodosEstados();
        
        vfc.getCid_removidas();
        vfc.cancelar();
        
        vfc.relacionarTodasCss(callOffer.Id);
        vfc.getCidades_adicionadas();
        vfc.removeEstados();
        vfc.removeEstados2();
        vfc.remover_css();
        List<String> lista2 = new List<String>();
        lista2.add('teste');
        lista2.add('teste2');
        vfc.setCidades_removidas(lista2);
        vfc.setEstadual(lista2);
        vfc.setCidades10(lista2);
        vfc.setCss10(lista2);
        vfc.getTodas2_Css();
        vfc.setCidades_adicionadas(lista2);
        vfc.setEstado_adicionado(lista2);
        vfc.setReg(lista2);
        List<SelectOption> lista3 = new List<SelectOption>();
        lista3.add(New SelectOption('teste','teste'));
        vfc.setTodosEstados(lista3);
        vfc.setRegiao_removida(lista2);
        vfc.init(callOffer.Id);
        
        List<SelectOption> selcList = vfc.getRegiao2();
        vfc.setRegiao2(new SelectOption('a', 'a'));
        
        List<SelectOption> selcList2 = vfc.getTodas_reg();
        vfc.setTodas_reg(new SelectOption('b', 'b'));
        String reg2 = vfc.reg2;
        vfc.addRegional();
        vfc.remRegional();
        vfc.remover_cidades();
        
        ApexPages.CurrentPage().getparameters().put('id', callOffer.id);
        ApexPages.StandardController sc = new ApexPages.standardController(callOffer);
        VFC147_ChamadaOfertaConcessionaria sic = new VFC147_ChamadaOfertaConcessionaria(sc);
        
    }
}