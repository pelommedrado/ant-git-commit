@isTest
public with sharing class TestVFC07_GoodwillCalculator {
    
    static testMethod void testgetSRCParticipationRate() {
        
        User usr = new User (LastName = 'Rotondo', 
                             alias = 'lro',
                             Email = 'lrotondo@rotondo.com',
                             BypassVR__c = true,
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_US',
                             ProfileId = Label.PROFILE_SYSTEM_ADMIN,
                             TimeZoneSidKey = 'America/Los_Angeles', 
                             UserName='lrotondo@lrotondo.com');
        
        System.runAs(usr) {
            
            Case c = new Case();
            c.Description='This is the description';
            insert c;
            
            Goodwill__c g = new Goodwill__c(Case__c = c.Id);
            
            Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
            
            System.debug('RecordId>>>>>>>>'+RecordId);
            
            g.RecordTypeId=RecordId;
            
            insert g;
            
            Integer i = 0;
            
            VFC07_GoodwillCalculator controller = new VFC07_GoodwillCalculator(new ApexPages.StandardController(g));
            //controller.Organ__c = 'AIR CONDITIONING COMPRESSOR';
            //controller.Country__c = 'Algeria';
            
            Test.StartTest();
            // Visit the page so we also run the constructor code
            PageReference pageRef = Page.VFP07_GoodwillCalculator;
            Test.setCurrentPage(pageRef);
            
            Integer src = controller.getSRCParticipationRate();
            controller.Save();
            controller.getselectListCountries();
            controller.getselectListOrgan();
            
            Test.StopTest();
        }
    }
}