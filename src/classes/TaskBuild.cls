@IsTest
public class TaskBuild {

    public static TaskBuild instance = null;

    public static TaskBuild getInstance() {
        if(instance == null) {
            instance = new TaskBuild();
        }
        return instance;
    }

    private TaskBuild() {
    }

    public Task createTask(Id userId, Id oppId) {
        final Task task = createTask(userId);
        task.WhatId	= oppid;
        return task;
    }

    public Task createTask(Id userId) {
        final Task task = new Task();
        task.OwnerId 			= userId;
        task.Subject 			= 'Subject';
        task.ActivityDate 		= Date.today();
        task.ReminderDateTime 	= Datetime.now();
        task.ActivityDatetime__c 	= Datetime.now();
        task.IsReminderSet 		= true;
        return task;
    }

    public List<Task> createListTask(Id userId, Id oppId, Integer size) {
        List<Task> taskList = new List<Task>();
        for(Integer i = 0; i < size; i++) {
            Task task = createTask(userId, oppId);
            task.Subject 			= 'Task' + i;
            task.ActivityDate 		= System.now().addMinutes(i).Date();
            task.ActivityDatetime__c 	= System.now().addMinutes(-10-i);
            task.ReminderDateTime 	= System.now().addMinutes(-10-i);
            task.IsReminderSet 		= true;
            task.Status 			= 'Not Started';
            taskList.add(task);
        }
        return taskList;
    }
}