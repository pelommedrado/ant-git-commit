@isTest
public with sharing class VehicleInvoiceCheckerTest {
	
	@isTest static void shouldRunBatch() {
		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.DDD1__c = '11';
		fatura.Phone_1__c = '27381122';
		fatura.DDD2__c = '12';
		fatura.Phone_2__c = '99098876';
		fatura.DDD3__c = '13';
		fatura.Phone_3__c = '20987761';
		fatura.External_Key__c = '123';
		Database.insert(fatura);

		VEH_Veh__c vehicle = new VEH_Veh__c();
		vehicle.ValidInvoice__c = 'NI';
		vehicle.DeliveryDate__c = System.today();
		vehicle.Name = '3425GSVFAGS234637';
		Database.insert(vehicle);

		test.startTest();

		ID batchprocessid = Database.executeBatch(new VehicleInvoiceBatch(), 10);

		test.stopTest();

		List<faturaDealer2__c> checkFaturas 				= [SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,
																		Delivery_Date__c,
																		isReturned__c,
																		isCancelled__c,
																		Status__c 
																FROM faturaDealer2__c];

		System.debug('!!!Batch Result - Fatura:'   			+	checkFaturas);

		List<VEH_Veh__c> checkVehicles						=	[SELECT Id, 
																		Name, 
																		DateofManu__c, 
																		ModelYear__c, 
																		DeliveryDate__c,
																		ValidInvoice__c
																FROM VEH_Veh__c];

		System.debug('!!!Batch Result - Veículos:' 			+	checkVehicles);

		String hoje = String.valueOf(System.today());
		List<String> lista = hoje.split('-');
		hoje = lista[2]+'/'+lista[1]+'/'+lista[0];

		System.assertEquals(hoje ,checkFaturas[0].Delivery_Date__c);

		System.assertEquals('S', checkVehicles[0].ValidInvoice__c);

	}

	@isTest static void shouldRunBatchAgain() {

		VEH_Veh__c vehicle = new VEH_Veh__c();
		vehicle.ValidInvoice__c = 'NI';
		vehicle.DeliveryDate__c = System.today();
		vehicle.Name = '3425GSVFAGS234637';
		Database.insert(vehicle);

		test.startTest();

		ID batchprocessid = Database.executeBatch(new VehicleInvoiceBatch(), 10);

		test.stopTest();

		List<faturaDealer2__c> checkFaturas 				= [SELECT 	Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,
																		Delivery_Date__c,
																		isReturned__c,
																		isCancelled__c,
																		Status__c 
																FROM faturaDealer2__c];

		System.debug('!!!Batch Result - Fatura:'   			+	checkFaturas);

		List<VEH_Veh__c> checkVehicles						=	[SELECT Id, 
																		Name, 
																		DateofManu__c, 
																		ModelYear__c, 
																		DeliveryDate__c,
																		ValidInvoice__c
																FROM VEH_Veh__c];

		System.debug('!!!Batch Result - Veículos:' 			+	checkVehicles);


		System.assertEquals('N', checkVehicles[0].ValidInvoice__c);

	}
    
    @isTest
    public static void itShouldSchedule() {
    	Test.StartTest();
        VehicleInvoiceBatch sh1 = new VehicleInvoiceBatch();      
        String sch = '0 0 23 * * ?';
        System.schedule('Test check', sch, sh1);
    	Test.stopTest();
    }
}