/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class ScheduleMessageAction {
    global ScheduleMessageAction() {

    }
    @InvocableMethod(label='Schedule Messages' description='Schedules the given messages for delivery and returns the status of each item.')
    global static List<LiveText.ActionResult> scheduleMessages(List<LiveText.ScheduleItem> items) {
        return null;
    }
}
