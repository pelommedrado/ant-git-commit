public class VFC156_ALRFlowNewActivateInsurance implements FileUploaderCaller {
    
    public String varUserId {get; set;}
    public String varTrackingId {get; set;}
    public String varActivateIsuranceID {get; set;}
    public String varDocumentID {get; set;}
    public String varFaseUpload {get; set;}
  	public String idAct {get; set;}
    
    public Flow.Interview.ALR_AllFlow_Damage flowDamage {get;set;}
    public String varResourceImagem {get; set;}
    
    public FileUploaderCaller self{
        get{
            return this;
        }
    }

    public Id oldParentId;
    public FileUploaderController componentController{get;set{this.componentController = value; system.debug( '@@@ COMPONENT CONTROLLER SETTER @@@' );}}
    public String varExtensionFile {
        get{
            System.debug( '@@@ EXTENSIONS GETTER @@@ ' + this.varExtensionFile );
            return this.varExtensionFile;
        } 
        set{
            System.debug( '@@@ EXTENSIONS SETTER @@@ ' + value );
            this.varExtensionFile = value;
        }
    }
    public Map< String, Id> phaseAttachmentMap;
    
    public VFC156_ALRFlowNewActivateInsurance(){
        
    }
    
    public VFC156_ALRFlowNewActivateInsurance(ApexPages.StandardController controller) {
        
        /*System.debug('**** PageCurrent: '+system.currentPageReference().getParameters().get('Id'));
        System.debug('**** PageCurrentID - retURL: '+ApexPages.currentPage().getParameters().get('retURL'));
        system.debug('**** controller: '+controller);*/
        
        varUserId = System.UserInfo.getUserId();
      
        String tracking = ApexPages.currentPage().getParameters().get('retURL');
        string idTracking;
        //system.debug('**** tracking: '+tracking);
        
        if(!String.isBlank(tracking)){
            if(tracking.length() > 18){
               idTracking = tracking.substringAfterLast('/');
                if(idTracking.length() == 18){
                    varTrackingId = idTracking;
                }else{
                    varTrackingId = null;
                } 
            }
            System.debug('*** varTrackingId '+ varTrackingId);

        }
        this.phaseAttachmentMap = new Map< String, Id >();
         
    }
    
    public String getUrlImg(){
        
        String urlImg;
        if(flowDamage != null){
            urlImg = flowDamage.ALR_varResourceImagem;
            varResourceImagem = urlImg;
        }
        return urlImg;
    }
    
    public Id parentId{
    get{
      return ApexPages.currentPage().getParameters().get( 'pid' );
    }   
  }
    
    public Boolean getAuxPanel(){
    
        Boolean loadUp = false;
        if(flowDamage != null){
            /*system.debug('**** flowDamage.ALR_varFaseUpload '+flowDamage.ALR_varFaseUpload);
            system.debug('**** flowDamage.ALR_varDocumentID '+flowDamage.ALR_varDocumentID);*/
            if(flowDamage.ALR_varDocumentID != null && !String.isBlank(flowDamage.ALR_varDocumentID )){
                if(flowDamage.ALR_varFaseUpload != null && !String.isBlank(flowDamage.ALR_varFaseUpload )){
                    varDocumentID = flowDamage.ALR_varDocumentID;
                    varFaseUpload =  flowDamage.ALR_varFaseUpload;
                    //System.debug('**** flowDamage.ALR_varExtensionFile: '+flowDamage.ALR_varExtensionFile);
                    if(!String.isEmpty(flowDamage.ALR_varExtensionFile)){
                        varExtensionFile = flowDamage.ALR_varExtensionFile;
                        if(!varExtensionFile.equalsIgnoreCase('pdf')){
                           varExtensionFile = 'jpg';
                        }
                    }
                  
                    idAct = flowDamage.ALR_varActivateIsuranceID;
                    loadUp = true;
                }
            }
        System.debug( '@@@ GETAUXPANEL @@@\nFlow Data: ' + flowDamage.ALR_varExtensionFile + '\nController Data: ' + this.varExtensionFile );
        }


        if( this.componentController != null ){
            if( this.phaseAttachmentMap.containsKey( varDocumentID + varFaseUpload ) ){
                Id attId = this.phaseAttachmentMap.get( varDocumentID + varFaseUpload );
                if( this.componentController.attId != attId ){
                    this.componentController.errorMsg = null;
                    this.componentController.attId = attId;
                }
            } else {
                String errorMsg = this.componentController.errorMsg;
                this.componentController.reset();
                this.componentController.errorMsg = errorMsg;
            } 
            //else this.componentController.reset();

            //this.componentController.type = 'pdf';//varExtensionFile;
        }

        return loadUp;
    }
    
    public void fileUploaderCallback(FileUploaderController target){

        this.phaseAttachmentMap.put( this.varDocumentID + this.varFaseUpload, target.attId );
    }
    
    public void registerController (FileUploaderController target){
        this.componentController = target;
    }

    public String getMsgErro(){
        String msgErro;
        if(flowDamage != null){
            msgErro = flowDamage.ALR_varMsgErro;
        }
        return msgErro;
    }
    
    public PageReference getFinish(){
        PageReference pageRef;
        
        if(flowDamage != null){
            if(!String.isEmpty(flowDamage.ALR_varTrackingID)){
                varTrackingId = flowDamage.ALR_varTrackingID;
            }
        }
        
        if(!String.isEmpty(idAct)){
            pageRef = new Pagereference('/'+idAct);   
        }else{
            if(!String.isEmpty(varTrackingId)){
                 pageRef = new Pagereference('/'+varTrackingId);
            }else{
                pageRef = new Pagereference('/home/home.jsp');
            }
        }
                 
        return pageRef;
    }

}