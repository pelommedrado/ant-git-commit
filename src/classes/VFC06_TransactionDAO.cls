/**
*	Class   -   VFC06_TransactionDAO.cls
*   Author  -   Suresh Babu
*   Date    -   04/10/2012
*    
*   #01 <Suresh Babu> <04/10/2012>
*        Created this Class to handle records from Transaction related Queries.
*/
public with sharing class VFC06_TransactionDAO extends VFC01_SObjectDAO {
	private static final VFC06_TransactionDAO instance = new VFC06_TransactionDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC06_TransactionDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC06_TransactionDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get Transaction Record based on Lead Id
    * @param leadId - use LeadId to fetch records from Transaction object.
    * @return lstTransactions - fetch and return the result in lstTransactions list.
    */
    public List<TST_Transaction__c> findTransactionRecords( Id leadId ){
        List<TST_Transaction__c> lstTransactions = null;
        
        lstTransactions = [SELECT 
        						Account__c, BrandAndModelCompetitive__c, ContactStatus__c,
        						CreatedById, CurrentMilage__c, CurrentVehicle__c, LastModifiedDate, CreatedDate,
        						IsDeleted, OwnerId, Id, IntentToPurchaseNewVehicle__c, IsRenaultVehicleOwner__c,
        						IsVehicleOwner__c, Lead__c, SystemModstamp, Name, OtherInformations__c, Recommendation1__c,
        						Recommendation2__c, Recommendation3__c, TransactionStatus__c, TypeOfContact__c, TypeOfService__c,
        						WhyNotContact__c, WhyNotOpportunity__c, LastModifiedById 
    						FROM
        						TST_Transaction__c
        					WHERE
        						Lead__c =: leadId        						
        					];
        
        return lstTransactions;
    }
    
     /**
    * This Method was used to get Transaction Record based on Account Id
    * @param tansactionId - use tansactionId Id to fetch records from Transaction object.
    * @return lstTransactions - fetch and return the result in lstTransactions list.
    */
    public TST_Transaction__c findTransactionById( Id tansactionId ){
        TST_Transaction__c Transactions = null;
    	 Transactions = [SELECT 
    						Account__c, BrandAndModelCompetitive__c, ContactStatus__c,
    						CreatedById, CurrentMilage__c, CurrentVehicle__c, LastModifiedDate, CreatedDate,
    						IsDeleted, OwnerId, Id, IntentToPurchaseNewVehicle__c, IsRenaultVehicleOwner__c,
    						IsVehicleOwner__c, Lead__c, SystemModstamp, Name, OtherInformations__c, Recommendation1__c,
    						Recommendation2__c, Recommendation3__c, TransactionStatus__c, TypeOfContact__c, TypeOfService__c,
    						WhyNotContact__c, WhyNotOpportunity__c, LastModifiedById 
						FROM
    						TST_Transaction__c
    					WHERE
    						Id =: tansactionId 
    						limit 1       						
    					];
    
    	return Transactions;
    }
    
      /**
    * This Method was used to get Transaction Record based on Account Id
    * @param accountId - use Account Id to fetch records from Transaction object.
    * @return lstTransactions - fetch and return the result in lstTransactions list.
    */
    public Id findMaxTransactionByAccountId( Id accountId ){
        AggregateResult Transactions = null;
        try{
        	 Transactions = [SELECT 
        						max(LastModifiedDate), Id
    						FROM
        						TST_Transaction__c
        					WHERE
        						Account__c =: accountId
        					group by 
        						Id 
        					limit 1       						
        					];
        
        	return String.valueOf(Transactions.get('Id'));
        }catch(QueryException ex){
        	return null;
        }
    }
}