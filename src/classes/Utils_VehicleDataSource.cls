Public class Utils_VehicleDataSource {
    
    // Boolean variable used to specify if the dummy implementation must be used or the real one
    public Boolean Test;
    
    
    public WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse getVehicleData(VFC04_VehicleData VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
        // --- PRE TREATMENT ----
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();       
        WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
        servicePref.bvmsi25SocEmettrice  = System.label.SocieteEmettrice;      
        //servicePref.bvmsi25Vin = 'VF1BGRG0633285766';
        servicePref.bvmsi25Vin = VehController.getVin();        
        servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase();  
        servicePref.bvmsi25CodePaysLang = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
        VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL;  
        VehWS.clientCertName_x = System.label.RenaultCertificate;       
        VehWS.timeout_x=40000;
        
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();    
    
        if (Test==true) {
            VEH_WS = Utils_Stubs.VehicleStub();    
        } else {
            VEH_WS = VehWS.getApvGetDetVehXml(request);
        }
        return VEH_WS;  
    }    
}