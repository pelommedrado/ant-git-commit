public class Sfa2Utils {
    
    private static final List<Integer> pesoCPF = new List<Integer>{11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final List<Integer> pesoCNPJ = new List<Integer>{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
    
    public static Boolean isEmailValido(String email) {
        String patternForEmail = '[a-z0-9!#$%&'+'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'+
            '*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?';
        Pattern isEmailFormat = Pattern.compile(patternForEmail);
        Matcher emailMatcher = isEmailFormat.matcher(email);
        return emailMatcher.Matches();
    }
    
    /**
	 * Verificar se o CPF e valido
	 */
    public static Boolean isCpfValido(String CPF) {      
        if ((cpf==null) || (cpf.length()!=11)) return false;
        
        if( isSequencia(CPF) ){
            return false;
        }
        
        Integer digito1 = calcularDigito(cpf.substring(0,9), pesoCPF);
        Integer digito2 = calcularDigito(cpf.substring(0,9) + digito1, pesoCPF);
        return cpf.equals(cpf.substring(0,9) + String.valueOf(digito1) + String.valueOf(digito2));
    }

    /**
    * Verificar se o CNPJ é valido
    */
    public static boolean isValidCNPJ(String cnpj) {
        if ((cnpj==null)||(cnpj.length()!=14)) return false;
        
        Integer digito1 = calcularDigito(cnpj.substring(0,12), pesoCNPJ);
        Integer digito2 = calcularDigito(cnpj.substring(0,12) + digito1, pesoCNPJ);
        return cnpj.equals(cnpj.substring(0,12) + String.valueOf(digito1) + String.valueOf(digito2));
    }
    
    private static Integer calcularDigito(String str, Integer[] peso) {
        Integer soma = 0;
        for (Integer indice=str.length()-1, digito; indice >= 0; indice-- ) {
            digito = Integer.valueOf(str.substring(indice,indice+1));
            soma += digito * peso[peso.size() - str.length() + indice];
        }
        soma = 11 - math.mod(soma, 11);
        return soma > 9 ? 0 : soma;
    }
    
    private static Boolean isSequencia(String CPF){     
        Pattern isSequencia = Pattern.compile('(?!(\\d)\\1{10})\\d{11}');
		Matcher cpfMatcher = isSequencia.matcher( CPF );
        
        return !cpfMatcher.Matches();
    } 
    
    /**
     * Remover a formatacao do CPF ou CNPJ
     */
    public static String limparFormatacaoCpfCnpj(String param) {
        if(param == null) {
            return param;
        }
        return param.replaceAll('[/\\.-]', '');
    }
    
    /**
	 * Obter conta do usuario da comunidade
	 */    
    public static Account obterContaUserComunidade() {
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        Id AccID  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        return [
            SELECT Id, Sell_from_stock__c, ReturnPeriod__c, ReturnActiveToSFA__c, new_stock_version__c
            FROM Account
            WHERE Id =: AccID
        ];
    }
    
    /**
	 * Obter codigo BIR da concessionarioa
	 */
    public static String obterBirConcessionaria(String userId) {
        if(userId == null) {
            return null;
        }
        
        return [
            SELECT BIR__c 
            FROM User 
            WHERE Id =: userId
        ].BIR__c;
    }
    
    /**
     * Obter os usuarios dos vendedores da concessionaria
     */
    public static List<User> obterVendedoresConcessionaria(String bir) {
        System.debug('### obtendo vendedores da concessionaria ' + bir);
        return [
            SELECT Id, Name  
            FROM User
            WHERE BIR__c =: bir AND DVESeller_BR__c = false AND isActive = TRUE
        ];
    }
    
    /**
     * Obter veiculo selecionado
  	 */
    public static VRE_VehRel__c obterVeiculo(String veiculoId) {
        if(veiculoId == null) {
            return null; 
        }
        
        List<VRE_VehRel__c> vrList = [
            SELECT Id,
            Account__r.IDBIR__c, 
            Days_in_Stock__c, 
            VIN__r.Color__c, 
            VIN__r.Name,
            VIN__r.Booking_User__r.Name, 
            VIN__r.Model__c,
            VIN__r.Optional__c, 
            VIN__r.Price__c, 
            VIN__r.Status__c, 
            VIN__r.Version__c, 
            VIN__r.Year_Model_Version__c
            FROM VRE_VehRel__c
            WHERE Id =: veiculoId
        ];
        
        if(vrList.isEmpty()) {
            return null;
        }
        
        return vrList.get(0);
    }
    
    public static List<Quote> obterCotacao(String oppId) {
        return [
            SELECT Id, Status
            FROM Quote
            WHERE OpportunityId =: oppId
        ];
    }
    
    public static VFC61_QuoteDetailsVO criarOrcamento(String oportunidadeId, VRE_VehRel__c veiculoRel) {
        System.debug('### CriarOrcamento');
        System.debug('### OportunidadeId:' + oportunidadeId);
        
        //novo orcamento        
        VFC61_QuoteDetailsVO quoteDetailsVO = VFC65_QuoteDetailsBusinessDelegate.getInstance().
            getQuoteDetails(null, //Id orcamento 
                            oportunidadeId);
        
        return criarOrcamento(quoteDetailsVO, veiculoRel);
    }
    
    /**
     * Criar orcamento
     */
    public static VFC61_QuoteDetailsVO criarOrcamento(VFC61_QuoteDetailsVO quoteDetailsVO, VRE_VehRel__c veiculoRel) {
        
        if(veiculoRel != null) {
            //adicionar o veiculo no orcamento
            addVeiculo(quoteDetailsVO, veiculoRel);
        }
        
        quoteDetailsVO = VFC65_QuoteDetailsBusinessDelegate.getInstance().
            getQuoteDetails(quoteDetailsVO.sObjQuote.Id, 
                            quoteDetailsVO.opportunityId);
        
        //corrigi bug do valor do orcamento
        for(VFC83_QuoteLineItemVO vo : quoteDetailsVO.lstQuoteLineItemVO) {
            System.debug('### VFC83_QuoteLineItemVO' + vo);
            vo.unitPriceMask = vo.unitPriceMask.replace('.', ',');
            System.debug('### VFC83_QuoteLineItemVO' + vo);
        }
        
        //data de validacao
        quoteDetailsVO.sObjQuote.ExpirationDate = DateTime.now().date();
        quoteDetailsVO.entryMask 				= '0';
        quoteDetailsVO.amountFinancedMask 		= '0';
        quoteDetailsVO.priceUsedVehicleMask 	= '0';
        quoteDetailsVO.valueOfParcelMask 		= '0';
        
        VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(quoteDetailsVO);
        
        //sincronizar orcamento com a oportunidade
        VFC63_QuoteBusinessDelegate.getInstance().synchronizationOpportunity(
            quoteDetailsVO.opportunityId, 
            quoteDetailsVO.sObjQuote.Id);
        
        return quoteDetailsVO;
    }
    
    /**
     * Adicionar veiculo no orcamento
     */
    public static void addVeiculo(VFC61_QuoteDetailsVO quoteDetailsVO, VRE_VehRel__c veiculoRel) {
        String[] modelos = veiculoRel.VIN__r.Model__c.split(' ');
        
        System.debug('### Nome modelos: ' + modelos); 
        
        Product2 produto = [
            SELECT id, Name, Version__c, Model__r.Name, ModelSpecCode__c
            FROM Product2
            WHERE Name IN: modelos AND RecordType.Name = 'Model'
        ];
        
        System.debug('### produtoId:' + produto.Id);
        System.debug('### VeiculoId' + veiculoRel.VIN__r.Id);
        System.debug('### QuoteId:' + quoteDetailsVO.sObjQuote.Id);
        System.debug('### preco:' + veiculoRel.VIN__r.Price__c);
        
        
        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(
            produto.Id, 
            veiculoRel.VIN__r.Id, 
            quoteDetailsVO.sObjQuote.Id, 
            veiculoRel.VIN__r.Price__c);
        
        VehicleBooking__c vbk = obterReserva(veiculoRel);
        if(vbk != null && String.isEmpty(vbk.Quote__c)) {
            VehicleBooking__c vbkNew = new VehicleBooking__c(Id=vbk.Id);
            vbkNew.Quote__c = quoteDetailsVO.sObjQuote.Id;
            UPDATE vbkNew;
        }
    }
    
    public static VehicleBooking__c obterReserva(VRE_VehRel__c veiculoRel) {
        List<VehicleBooking__c> vbList = [
            SELECT Id, ClientName__c, ClientSurname__c, 
            CustomerIdentificationNbr__c, HomePhone__c, ClientEmail__c,
            MobilePhone__c, Vehicle__c, Vehicle__r.Name,
            Quote__c
            FROM VehicleBooking__c
            WHERE Vehicle__c =: veiculoRel.VIN__c 
            AND Status__c = 'Active'  
        ];
        
        if(vbList.isEmpty()) {
            return null;
        }
        
        return vbList.get(0);
    }
    
    public static Boolean derrubarReserva(VRE_VehRel__c veiculoRel) {
        VehicleBooking__c vbk = obterReserva(veiculoRel);
        
        if(vbk == null) {
            return false;
        }
        
        VehicleBooking__c vbkNew = new VehicleBooking__c(Id= vbk.Id);
        vbkNew.Status__c = 'Canceled';
        vbkNew.IntegrationStatus__c = 'VEHICLE BOOKING SENT';
        
        UPDATE vbkNew;
        
        VEH_Veh__c vei = new VEH_Veh__c(Id=veiculoRel.VIN__c);
        vei.Status__c = 'Available';
        
        UPDATE vei;
        
        if(!String.isEmpty(vbk.Quote__c)) {
            List<QuoteLineItem> veiItemList = [
                SELECT Id, QuoteId, UnitPrice, Vehicle__r.Id
                FROM QuoteLineItem
                WHERE QuoteId =: vbk.Quote__c AND Vehicle__r.Id =: vei.Id
            ];
            
            if(!veiItemList.isEmpty()) {
                DELETE veiItemList;
            }
        }
        return true;
    }
    
    /**
     * Criar o test drive
     */
    public static void criarTestDrive(String oportunidadeId, String motivo) {
        System.debug('### CriarTestDrive');
        System.debug('### OportunidadeId:' + oportunidadeId);
        
        VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().
            getTestDriveByOpportunity(oportunidadeId);
        
        //selecionar o primeiro motivo
        testDriveVO.reasonCancellation = motivo;
        
        VFC56_TestDriveBusinessDelegate.getInstance().
            cancelTestDriveAndUpdateOpportunityForQuoteStage(testDriveVO);
    }
    
    /**
* Obter o usuario a parti do login
*/
    public static User obterUsuarioLogado() {
        return [
            SELECT BIR__c, ContactId, Contact.Account.ParentId
            FROM User 
            WHERE Id =: Userinfo.getUserId()
        ];
    }
    
    /**
* Obter a conta pai (grupo) da concessionaria do usuario
*/
    public static Account obterContaParente(User user) {
        Account acc = [
            SELECT Id, ParentId
            FROM Account 
            WHERE Id =: user.Contact.AccountId /*AND parentId != null*/
            LIMIT 1
        ];
        System.debug('Acc: ' + acc);
        
        return new Account(Id=acc.ParentId);
        /*return [
            SELECT Id
            FROM Account 
            WHERE Id =: acc.ParentId
        ];*/
    }
    
    /**
* Obter lista de concessionarias do grupo a partir da concessionaria pai (grupo).
*/
    public static List<Account> obterListaConta(Account parentAcc) {
        List<Account> acc =  [
            SELECT Id, IDBIR__c 
            FROM Account 
            WHERE ParentId =: parentAcc.Id
        ];
        system.debug('*******acc: '+acc);
        return acc;
    }

    // Obter lista de concessionarias do grupo a partir do usuario.
    public static List<Account> obterListaConcessionariasDoGrupo(User user){
        List<Account> acc =  [
            SELECT Id, IDBIR__c, Name 
            FROM Account 
            WHERE ParentId =: user.Contact.Account.ParentId
        ];
        system.debug('*******acc: '+acc);
        return acc;
    }

}