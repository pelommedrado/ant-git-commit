/**
	Class   -   VFC29_ContactDAO_Test
    Author  -   RameshPrabu
    Date    -   31/10/2012
    
    #01 <RameshPrabu> <31/10/2012>
        Created this class using test for VFC29_ContactDAO.
**/
@isTest
private class VFC29_ContactDAO_Test {

    static testMethod void VFC29_ContactDAO_Test_1() {
    	List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        List<Contact> lstConatct = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToContact(lstDealerRecords);
        //List <Account> lstAccount =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        Test.startTest();
        List<Contact> lstConatcts = VFC29_ContactDAO.getInstance().fetchContactRecordsUsingCriteria();
        Test.stopTest();
        System.assert(lstConatct.size() > 0);
    }
    
    static testmethod void VFC29_ContactDAO_Test_2(){
    	List<Account> lstAccountRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
    	
    	Test.startTest();
    	Contact contactRecord = VFC29_ContactDAO.getInstance().fetchContactUsingAccountId( lstAccountRecords[0].Id );
    	
    	List<Id> lstAccountIDs = new List<Id>();
    	for( Account acc : lstAccountRecords ){
    		lstAccountIDs.add( acc.Id );
    	}
    	
    	List<Contact> lstContacts = VFC29_ContactDAO.getInstance().fetchContactRecords_UsingDealers(lstAccountIDs);
    	Test.stopTest();
    }
    
}