@isTest(SeeAllData=true)
public with sharing class TestVFC03_AdminGoodwillGrids {
 
static User getEnvSetup(){
        // Get the context of a user
          Profile p = [SELECT Id FROM Profile WHERE Name='CCBIC - Agent Front Office']; 
          User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
          LocaleSidKey='fr', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com',
          Country = 'France');

/*       VFC05_LanguageMapping__c mycs = VFC05_LanguageMapping__c.getValues('Short_Code__c');
          
          if(mycs == null) {
            mycs = new VFC05_LanguageMapping__c(Name= 'France');
            mycs.Short_Code__c = 'FR';
            insert mycs;
          }
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            system.debug('Userlanguage: ' + u.LanguageLocaleKey);*/
          return u;
    }
    
    static void fillParameters(VFC03_AdminGoodwillGrids controller){
        controller.gwParams.Organ__c = 'AIR CONDITIONING COMPRESSOR;BATTERY';
        controller.gwParams.VehicleBrand__c = 'Renault';
        controller.gwParams.Country__c = 'Algeria';
        
        controller.gwParams.Max_Age__c = 100;
        controller.gwParams.Max_Mileage__c = 100;
        controller.gwParams.Age_interval__c = 10;
        controller.gwParams.Mileage_Interval__c = 10;
    
    }
    static testMethod void testSave(){
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
        
        User u = getEnvSetup();
    
        Test.startTest();
        SelectOption[] countries = new SelectOption[]{}; 
        System.runAs(u){
            
            countries = controller.getselectListCountries();
            system.debug('Countries' + countries.size());
            //System.assert(countries.size() == 1);
        }       
        u.Country = 'GRIDADMINISTRATOR';
        update u;
        System.runAs(u){

            countries = new SelectOption[]{}; 
            countries = controller.getselectListCountries();
            //System.assert(countries.size() > 2);
            
        }       
        Boolean p = controller.parametersAreFilledIn(); // They aren't at this stage
        //System.assertEquals(p, false);
        controller.paramSelected();
        
        fillParameters(controller);
    
        controller.paramSelected();
        p = controller.parametersIntLessMax();
        //System.assertEquals(p, true);
        fillParameters(controller);
        
        controller.createGrid();
        controller.Save();
        
        // Test straightforward load recreate and save
        fillParameters(controller);
        controller.paramSelected();
        controller.recreate();
        controller.Save();
        
        controller.createGrid();
        Goodwill_Grid_Line__c emptyobject = new Goodwill_Grid_Line__c();
        controller.fillEmptyFields(emptyobject);
        
        controller.deleteGridAndClearParam();
        Test.stopTest();
    }
    
    static testMethod void testparametersIntLessMax(){
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
        Boolean p;
        
        Test.startTest();
        
        controller.parametersIntLessMax();
        
        // Populate some parameters
        fillParameters(controller);
        
        controller.gwParams.Max_Age__c = 10;
        controller.gwParams.Max_Mileage__c = 10;
        controller.gwParams.Age_interval__c = 100;
        controller.gwParams.Mileage_Interval__c = 100;
        p = controller.parametersIntLessMax();
        //System.assertEquals(p, false);
        Test.stopTest();
    }
    static testMethod void testcreateGrid(){
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
            // Test without any parameters
            Test.startTest();
            controller.createGrid();
        
            controller.gwParams.Max_Age__c = 1000;
            controller.gwParams.Max_Mileage__c = 1000;
            controller.gwParams.Age_interval__c = 10;
            controller.gwParams.Mileage_Interval__c = 10;
            // Test with some parameters
            controller.createGrid();
            Test.stopTest();
    }
    
    static testMethod void testDelete(){
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
        
        Test.startTest();
        controller.deleteGrid(); // So we can see what happens when there is no data
        
        // Now test deleting when there is something to delete
        fillParameters(controller);
        controller.gwParams.Organ__c = 'BATTERY';
        controller.createGrid();
        controller.save();
        controller.deleteGrid();
        
        // Now test deleting on multiple organs (the stub uses more than one)
        fillParameters(controller);
        controller.createGrid();
        controller.save();
        controller.deleteGrid();
        
        // Now test deleting after selecting something but nothing else
        fillParameters(controller);
        controller.paramSelected();
        controller.deleteGrid();
        Test.stopTest();
    }
    static testMethod void testparametersAreFilledIn(){
        // also tests clearParameters
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
        Boolean p;
        Test.startTest();
        
        fillParameters(controller);
        controller.paramSelected();
        p = controller.parametersAreFilledIn(); // They ARE at this stage because we just filled them.
       //System.assertEquals(p, true);
        
        controller.clearParameters();
        p = controller.parametersAreFilledIn(); // They aren't at this stage because we just cleared them.
        //System.assertEquals(p, false);
        Test.stopTest();

    }
}