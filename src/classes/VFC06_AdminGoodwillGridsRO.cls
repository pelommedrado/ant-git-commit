public class VFC06_AdminGoodwillGridsRO {
    public String selectCountry {get;set;}
    public String selectBrand {get;set;}
    public String selectOrgan {get;set;}
    public Goodwill_Grid_Line__c lineOne {get;set;}
    
    public gwParams gwParams {get;set;}
    public list<GoodwillGridLineWrapper> gwGridLinesWrapper {get;set;}
    private list<Goodwill_Grid_Line__c> gwGridLines;
    private static final Integer maxColumns = 61;
    private static final Integer maxLines = 27;

    private class gwParams {
        public Decimal Age {get;set;}
        public Decimal Mileage {get;set;}
        public Decimal Max_Age {get;set;}
        public Decimal Max_Mileage {get;set;}
    }

    public VFC06_AdminGoodwillGridsRO()
    {
        gwParams = new gwParams();
        gwGridLinesWrapper = new list<GoodwillGridLineWrapper>();
        gwGridLines = new list<Goodwill_Grid_Line__c>();
		lineOne = new Goodwill_Grid_Line__c();
		
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillParamSelectionRO);
        ApexPages.addMessage(myMsg);
    }
    
    public void paramSelected()
    {
        
        if (selectCountry == '--None--'
            || selectBrand == '--None--'
            || selectOrgan == '--None--')
        {
            gwGridLinesWrapper.clear();
            gwGridLines.clear();
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillParamSelectionRO);
            ApexPages.addMessage(myMsg);
        }
        else
        {
            gwGridLinesWrapper.clear();
            gwGridLines.clear();
            
            gwGridLines = [Select Valid_from__c, Valid_to__c, Additional_Information__c, Age_interval__c, Max_Age__c, Mileage_Interval__c, Max_Mileage__c, Row_Number__c, f1__c, f2__c, f3__c, f4__c, f5__c, f6__c, f7__c, f8__c, f9__c, f10__c, f11__c, f12__c, f13__c, f14__c, f15__c, f16__c, f17__c, f18__c, f19__c, f20__c, f21__c, f22__c, f23__c, f24__c, f25__c, f26__c, f27__c, f28__c, f29__c, f30__c, f31__c, f32__c, f33__c, f34__c, f35__c, f36__c, f37__c, f38__c, f39__c, f40__c, f41__c, f42__c, f43__c, f44__c, f45__c, f46__c, f47__c, f48__c, f49__c, f50__c, f51__c, f52__c, f53__c, f54__c, f55__c, f56__c, f57__c, f58__c, f59__c, f60__c, f61__c from Goodwill_Grid_Line__c where Country__c = :selectCountry AND VehicleBrand__c = :selectBrand AND Organ__c = :selectOrgan order by Row_Number__c];
            
            if (gwGridLines.size() > 0)
            {
                gwParams.Age = gwGridLines.get(0).Age_interval__c;
                gwParams.Max_Age = gwGridLines.get(0).Max_Age__c;
                gwParams.Mileage = gwGridLines.get(0).Mileage_Interval__c;
                gwParams.Max_Mileage = gwGridLines.get(0).Max_Mileage__c;
                currentNotesID = gwGridLines.get(0).ID;
                lineOne = gwGridLines.get(0);         
            }
            else
            {
                gwParams.Age = null;
                gwParams.Max_Age = null;
                gwParams.Mileage = null;
                gwParams.Max_Mileage = null;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.GoodwillNoGrid);
                ApexPages.addMessage(myMsg);
            }
            
            Integer rowNum = 1;
            for (Goodwill_Grid_Line__c g : gwGridLines)
            {
                gwGridLinesWrapper.add(new GoodwillGridLineWrapper(rowNum, g));
                rowNum++;
            }
        }
    }
    
   
     public class GoodwillGridLineWrapper
    {
        public Goodwill_Grid_Line__c gwGrid {get;set;}
        public Integer rowNumber {get; set;}

        public GoodwillGridLineWrapper(Integer rn, Goodwill_Grid_Line__c gw)
        {
            rowNumber = rn;
            gwGrid = gw;
        }      
    }
    
    public ID currentNotesID {get;set;}
   
    public SelectOption[] getselectListCountries(){
        SelectOption[] s = new SelectOption[]{};
            s.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
            s.add(new SelectOption(p.getValue(), p.getValue()));
        }
        
        return s;
    }
    
    public SelectOption[] getselectListBrand(){
        SelectOption[] s = new SelectOption[]{};
        s.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.VehicleBrand__c.getDescribe().getPicklistValues()){
            s.add(new SelectOption(p.getValue(), p.getValue()));
        }
        
        return s;
    }
    
    public SelectOption[] getselectListOrgan(){
        SelectOption[] s = new SelectOption[]{};
        s.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Organ__c.getDescribe().getPicklistValues()){
            s.add(new SelectOption(p.getValue(), p.getValue()));
        }
        
        return s;
    }
    
}