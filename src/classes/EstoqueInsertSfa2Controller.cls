public class EstoqueInsertSfa2Controller extends EstoqueAbstractController {
    private String oppId;
    private String quoteId;
    
    public List<SelectOption> listModelo { get;set; }
    
    /**
     * Contrutor da classe.
     */
    public EstoqueInsertSfa2Controller () {
        this.vehicleList = new List<EstoqueAbstractController.ObjVeiculo>();
        
        this.oppId = Apexpages.currentPage().getParameters().get('oppId');
        this.quoteId = Apexpages.currentPage().getParameters().get('Id');
        
        VFC46_QualifyingAccountVO qualifyingAccountVO =
            VFC133_QualifyingAccountBOWOS.getInstance().getAccountByOpportunityId(oppId);
        
        String ref = qualifyingAccountVO.vehicleInterestBR.split(' ')[0];
        for(SelectOption sl : modelos) {
            if(sl.getValue().equalsIgnoreCase(ref)) {
                this.modelo = sl.getValue();
                break;
            }
        }
        
        listModelo = new List<SelectOption>();
        List<Model__c> listModel = [
            SELECT Id, Name, Model_PK__c
            FROM Model__c
            WHERE Status_SFA__c = 'Active'
            ORDER BY Name
        ];

        Datetime dateWithKwid = Datetime.newInstance(2017, 08, 03);
        
        for(Model__c m : listModel) {

            if(!m.Name.equalsIgnoreCase(Label.VehicleToDistrinet)){
                listModelo.add(new SelectOption(m.Id, m.Name + ' (' + m.Model_PK__c + ')'));

            } else if(oppInterest != null && oppInterest.TransactionCode__c != null){
                listModelo.add(new SelectOption(m.Id, m.Name + ' (' + m.Model_PK__c + ')'));

            } else if(oppInterest != null && oppInterest.CreatedDate <= dateWithKwid){
                listModelo.add(new SelectOption(m.Id, m.Name + ' (' + m.Model_PK__c + ')'));

            }
        }
    }
      
    public Pagereference includeItemVehicle() {
        String veiculoId = 
            Apexpages.currentPage().getParameters().get('veh');
        
        if(String.isEmpty(veiculoId)) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 'É necessário selecionar um veículo'));  
            return null;        
        }
        
        VRE_VehRel__c veiculoRel = Sfa2Utils.obterVeiculo(veiculoId);
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            VehicleBooking__c vbk = Sfa2Utils.obterReserva(veiculoRel);
            if(vbk != null && !String.isEmpty(vbk.Quote__c)) {
                if(!Sfa2Utils.derrubarReserva(veiculoRel)) {
                    ApexPages.addMessage(new ApexPages.Message(
                        ApexPages.Severity.ERROR, 'Nao foi possivel derrubar a reserva')); 
                    return null;
                }
            }
            
            VFC61_QuoteDetailsVO quoteVo = VFC65_QuoteDetailsBusinessDelegate.getInstance().
                getQuoteDetails(this.quoteId, this.oppId);
            
            Sfa2Utils.criarOrcamento(quoteVo, veiculoRel);
            
            PageReference pageRef = new PageReference(
                '/apex/ClientePreOrdemSfa2?Id=' + quoteVo.sObjQuote.Id + '&oppId=' + oppId);
            pageRef.setRedirect(true);
            return pageRef;      
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            Database.rollback( sp );
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
        }            
    }
    
    public Pagereference  includeItemGeneric() {
        String strMessage = null;
        ApexPages.Message message = null;       
        
        String modelSelect = 
            Apexpages.currentPage().getParameters().get('modelo');
        String milesimeSelect = 
            Apexpages.currentPage().getParameters().get('milesime');
        String versionSelect = 
            Apexpages.currentPage().getParameters().get('versao');
        String corSelect = 
            Apexpages.currentPage().getParameters().get('cor');
        String estofadoSelect = 
            Apexpages.currentPage().getParameters().get('estofado');
        String harmoniaSelect = 
            Apexpages.currentPage().getParameters().get('harmonia');
        String opcional = 
            Apexpages.currentPage().getParameters().get('opcional');
        
        System.debug('### modelSelect: ' + modelSelect);
        System.debug('### milesimeSelect: ' + milesimeSelect);
        System.debug('### versionSelect: ' + versionSelect);
        System.debug('### corSelect: ' + corSelect);
        System.debug('### estofadoSelect: ' + estofadoSelect);
        System.debug('### harmoniaSelect: ' + harmoniaSelect);
        System.debug('### opcional: ' + opcional);
        
        if(String.isEmpty(modelSelect) || String.isEmpty(milesimeSelect) || String.isEmpty(versionSelect) 
           || String.isEmpty(corSelect) || String.isEmpty(estofadoSelect) || String.isEmpty(harmoniaSelect)) {
               strMessage = 'Selecione as opções.';    
               message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
               ApexPages.addMessage(message);  
               return null;        
           }
        
        List<String> opcionalList = opcional.split(',');
        
        try {
            
            VFC61_QuoteDetailsVO quoteDetailsVO =
                VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, this.oppId);
            
            Sfa2Utils.criarOrcamento(quoteDetailsVO, null);
            
            VFC87_NewVehicleBusinessDelegate.getInstance().includeVersionTable(
                quoteDetailsVO.Id, modelSelect, versionSelect, corSelect, estofadoSelect, harmoniaSelect, opcionalList);
            
            PageReference pageRef = new PageReference('/apex/ClientePreOrdemSfa2?Id=' + this.quoteId + '&oppId=' + oppId);
            pageRef.setRedirect(true);
            
            return pageRef;
            
        } catch(Exception ex) {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(message);
            
            return null;
        }
    }
    
    public Pagereference cancelVehico() {
        PageReference pageRef = new PageReference('/apex/ClientePreOrdemSfa2?Id=' + this.quoteId + '&oppId=' + oppId);
        pageRef.setRedirect(true);
        return pageRef;      
    }
    
    @RemoteAction
    public static Boolean isCotacao(String vehickeId) {
        System.debug('vehickeId:' + vehickeId);
        VRE_VehRel__c veiculoRel = Sfa2Utils.obterVeiculo(vehickeId);
        System.debug('***veiculoRel: '+veiculoRel);
        VehicleBooking__c vbk = Sfa2Utils.obterReserva(veiculoRel);
        if(vbk == null) {
            return false;
        } else if(vbk.Quote__c != null) {
            return true;
        }
        return false;
    }
    
    @RemoteAction
    public static Map< String, List< Optional__c > > loadOptionals (Id versionId){
        Map<String, List<Optional__c>> optionalsMap = new Map<String, List<Optional__c>>();
        
        optionalsMap.put( 'optionals', [
            SELECT Id, Name, Amount__c, Type__c
            FROM Optional__c 
            WHERE Version__c = :versionId 
            order by Name
        ] );
        return optionalsMap;
    }
    
    @RemoteAction
    public static List<PVVersion__c> loadVersions(Id modelId, String milesime) {
        return [
            SELECT Id, Name,  PVC_Maximo__c, PVR_Minimo__c, Price__c
            FROM PVVersion__c 
            WHERE Model__c = :modelId
            AND Milesime__r.Milesime__c = :milesime
            ORDER BY Name
        ];
    }
    
    @RemoteAction
    public static List< String > loadMilesimes(Id modelId){
        List<String> milesimeList = new List<String>();
        
        for( AggregateResult aggRes : [
            SELECT Milesime__c 
            FROM PV_ModelMilesime__c 
            WHERE Model__c = :modelId 
            GROUP BY Milesime__c 
            ORDER BY Milesime__c ] ) { 
                milesimeList.add( (String) aggRes.get( 'Milesime__c' ) );
            }
        return milesimeList;
    }
}