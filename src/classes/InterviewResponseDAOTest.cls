@isTest
private class InterviewResponseDAOTest {
	@TestSetup static void setup(){
		Account newAcc = new Account();
		newAcc.Name = 'accTeste';
		newAcc.Phone = '12345678';
		Database.insert(newAcc);

		Contact newContact = new Contact();
		newContact.LastName = 'testeContact';
		newContact.AccountId = newAcc.Id;
		Database.insert(newContact);

		Interview__c newInterview = new Interview__c();
		newInterview.Name = 'Pesquisa PV';
		Database.insert(newInterview);

		InterviewResponseBuild instance = InterviewResponseBuild.getInstance();
		instance.answerLink = 'www.teste.com.br';
		instance.returnedToRepair = 'Repair shop';

		InterviewResponse__c newInterviewRes = instance.newInterviewResp(newAcc.Id, newContact.Id, newInterview.Id);
		Database.insert(newInterviewRes);

		instance = InterviewResponseBuild.getInstance();
		instance.answerLink = 'www.teste2.com.br';
		instance.returnedToRepair = 'Repair shop2';

		InterviewResponse__c newInterviewRes2 = instance.newInterviewResp(newAcc.Id, newContact.Id, newInterview.Id);
		Database.insert(newInterviewRes2);
	}

	@isTest static void shouldGetInterviewResById() {
		Id interviewRes = [SELECT Id FROM InterviewResponse__c LIMIT 1].Id;
		InterviewResponse__c check = InterviewResponseDAO.getInterviewResById(interviewRes);

		System.assertEquals(false, check == null);
		System.assertEquals('www.teste.com.br', check.AnswerLink__c);
		System.assertEquals('Facebook', check.ContactChannel__c);
		System.assertEquals('Repair shop', check.CustomerReturnedRenaultRepairShop__c);
	}

	@isTest static void shouldGetInterviewResByString() {
		String interviewRes = [SELECT Id FROM InterviewResponse__c LIMIT 1].Id;
		InterviewResponse__c check = InterviewResponseDAO.getInterviewResById(interviewRes);

		System.assertEquals(false, check == null);
		System.assertEquals('www.teste.com.br', check.AnswerLink__c);
		System.assertEquals('Facebook', check.ContactChannel__c);
		System.assertEquals('Repair shop', check.CustomerReturnedRenaultRepairShop__c);
	}

	@isTest static void shouldGetInterviewResByIdSet() {
		Set<Id> interviewResIdSet = new Set<Id>();
		List<InterviewResponse__c> lstInterviewRes = [SELECT Id FROM InterviewResponse__c LIMIT 2];
		for(InterviewResponse__c interviewRes : lstInterviewRes){
			interviewResIdSet.add(interviewRes.Id);
		}

		List<InterviewResponse__c> lstCheck = InterviewResponseDAO.getInterviewResById(interviewResIdSet);

		System.assertEquals(false, lstCheck.isEmpty());

		System.assertEquals('www.teste.com.br', lstCheck[0].AnswerLink__c);
		System.assertEquals('Facebook', lstCheck[0].ContactChannel__c);
		System.assertEquals('Repair shop', lstCheck[0].CustomerReturnedRenaultRepairShop__c);

		System.assertEquals('www.teste2.com.br', lstCheck[1].AnswerLink__c);
		System.assertEquals('Facebook', lstCheck[1].ContactChannel__c);
		System.assertEquals('Repair shop2', lstCheck[1].CustomerReturnedRenaultRepairShop__c);
	}

}