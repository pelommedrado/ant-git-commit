/**
* Classe controladora da página de test drive do SAC.
* @author Felipe Jesus Silva.
*/
public class VFC109_TestDriveSACController 
{
	private String accountId;
	public VFC110_TestDriveSACVO testDriveVO {get;set;}
	
	public VFC109_TestDriveSACController(ApexPages.StandardController controller)
	{
		this.accountId = controller.getId();
	}
	
	public PageReference initialize()
	{
		this.testDriveVO = VFC111_TestDriveSACBusinessDelegate.getInstance().getTestDriveByAccount(this.accountId);
		this.initializeTestDrive();
		return null; 
	}
	
	/**
	* Inicializa a tela.
	*
	*/
	private void initializeTestDrive()
	{	
		this.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
		
		/*constroi o picklist de veículos de interesse*/
		this.buildPicklistVehiclesInterest();	
		
		/*constroi o picklist de veículos disponíveis*/
		this.buildPicklistVehiclesAvailable();
			
		/*constroi a agenda do primeiro veículo disponível*/
		this.buildAgendaVehicle();
	}
	
	private void buildPicklistVehiclesInterest()
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		
		for(Product2 sObjModel : this.testDriveVO.lstSObjModel)
		{
			lstSelOption.add(new SelectOption(sObjModel.Name, sObjModel.Name));
		}
		
		this.testDriveVO.lstSelOptionVehicleInterest = lstSelOption;
		
		/*essa lista não é mais necessária*/
		this.testDriveVO.lstSObjModel = null;		
	}
	
	private void buildPicklistVehiclesAvailable()
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		
		if(this.testDriveVO.lstSObjTestDriveVehicleAvailable != null)
		{
			for(TDV_TestDriveVehicle__c sObjTestDriveVehicle : this.testDriveVO.lstSObjTestDriveVehicleAvailable)
			{
				if(String.isNotEmpty(sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c))
				{
					lstSelOption.add(new SelectOption(sObjTestDriveVehicle.Id, sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c));
				}			
			}
			
			/*essa lista não é mais necessária*/
			this.testDriveVO.lstSObjTestDriveVehicleAvailable = null;
		}
		
		this.testDriveVO.lstSelOptionVehiclesAvailable = lstSelOption;					
	}
	
	private void buildAgendaVehicle()
	{
		VFC114_ScheduleTestDriveSACVO scheduleVO = null; 
		String hourAux = null;
		String minuteAux = null;
		TDV_TestDrive__c sObjTestDriveAux = null;
		Map<String, TDV_TestDrive__c> mapTestDrive = null;
		Date currentDate = null;
		DateTime startDate = null;
		DateTime endDate = null;
		List<VFC114_ScheduleTestDriveSACVO> lstAux = null;
		
		this.testDriveVO.lstAgendaVehicle.clear();
		
		if(this.testDriveVO.lstSObjTestDriveAgenda != null)
		{
			mapTestDrive = new Map<String, TDV_TestDrive__c>();
			currentDate = Date.today();
			startDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 8, 0, 0);		
			endDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 20, 0, 0);
			lstAux = new List<VFC114_ScheduleTestDriveSACVO>();
			
			for(TDV_TestDrive__c sObjTestDrive : this.testDriveVO.lstSObjTestDriveAgenda)
			{
				hourAux = sObjTestDrive.DateBooking__c.hour().format();
				minuteAux = + sObjTestDrive.DateBooking__c.minute().format();
			
				mapTestDrive.put(hourAux + minuteAux, sObjTestDrive);
			}
			
			while(startDate <= endDate)
			{
				scheduleVO = new VFC114_ScheduleTestDriveSACVO();
						
				hourAux = startDate.hour() < 10 ? ('0' + startDate.hour()) : (startDate.hour().format());
			
				minuteAux = startDate.minute() < 10 ? ('0' + startDate.minute()) : (startDate.minute().format());
			
				scheduleVO.hour = hourAux + ':' + minuteAux;
					
				hourAux = startDate.hour().format();
				minuteAux = startDate.minute().format();
			
				/*obtém do map um test drive com esse horário*/
				sObjTestDriveAux = mapTestDrive.get(hourAux + minuteAux);
			
				/*verifica se foi retornado null, o que indica que esse horário está disponível*/
				if(sObjTestDriveAux == null)
				{
					scheduleVO.schedulingStatus = 'available';
				}
				else
				{
					scheduleVO.schedulingStatus = 'scheduled';
				}
					
				lstAux.add(scheduleVO);
			
				if(lstAux.size() == 5)
				{		
					this.testDriveVO.lstAgendaVehicle.add(lstAux);
				
					lstAux = new List<VFC114_ScheduleTestDriveSACVO>();
				}
							
				startDate = startDate.addMinutes(30);	
			}
		
			this.testDriveVO.dateBookingNavigationFmt = this.testDriveVO.dateBookingNavigation.format();
			
			/*essa lista não é mais necessária*/
			this.testDriveVO.lstSObjTestDriveAgenda = null;
		}				
	}
	
	/**
	* Trata o evento onchange do campo modelo desejado (veículo de interesse).
	*/
	public void processDealerInterestChange()
	{
		this.getVehiclesAvailable();
	}
	
	/**
	* Trata o evento onchange do campo modelo desejado (veículo de interesse).
	*/
	public void processVehicleInterestChange()
	{
		this.getVehiclesAvailable();
	}
	
	private void getVehiclesAvailable()
	{
		VFC110_TestDriveSACVO testDriveVOResponse = VFC111_TestDriveSACBusinessDelegate.getInstance().getVehiclesAvailable(this.testDriveVO);
		
		/*armazena a lista de veículos disponíveis no VO principal*/
		this.testDriveVO.lstSObjTestDriveVehicleAvailable = testDriveVOResponse.lstSObjTestDriveVehicleAvailable;
			
		/*constroi o picklist de veículos disponíveis*/
		this.buildPicklistVehiclesAvailable();
		
		/*seta os detalhes do primeiro veículo disponível*/
		this.testDriveVO.model = testDriveVOResponse.model;
		this.testDriveVO.version = testDriveVOResponse.version;
		this.testDriveVO.color = testDriveVOResponse.color;
		this.testDriveVO.plaque = testDriveVOResponse.plaque;
		this.testDriveVO.dateBookingNavigation = testDriveVOResponse.dateBookingNavigation;
		this.testDriveVO.dealerOwnerId = testDriveVOResponse.dealerOwnerId;
		this.testDriveVO.selectedTime = '';
		
		/*armazena a agenda do primeiro veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda do primeiro veículo disponível*/
		this.buildAgendaVehicle();				
	}
	
	/**
	* Trata o evento onchange do campo veículos disponíveis.
	*/
	public void processVehicleAvailableChange()
	{
		VFC110_TestDriveSACVO testDriveVOResponse = null;
		
		/*seta a data atual no atributo dateBooking para obter a agenda do dia inicialmente*/
		this.testDriveVO.dateBookingNavigation = Date.today();
			
		/*obtém os detalhes do veículo*/
		testDriveVOResponse = VFC111_TestDriveSACBusinessDelegate.getInstance().getVehicleDetails(this.testDriveVO);
		
		/*seta os detalhes do veículo selecionado*/	
		this.testDriveVO.model = testDriveVOResponse.model;
		this.testDriveVO.version = testDriveVOResponse.version;
		this.testDriveVO.color = testDriveVOResponse.color;
		this.testDriveVO.plaque = testDriveVOResponse.plaque;
		this.testDriveVO.selectedTime = '';
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda do veículo selecionado*/	
		this.buildAgendaVehicle();		
	}
	
	public void getAgendaNextDay()
	{
		VFC110_TestDriveSACVO testDriveVOResponse = null;
		
		/*incrementa um dia no datebooking para obter a agenda do dia seguinte*/
		this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation + 1;
		
		/*obtém a agenda do dia seguinte*/
		testDriveVOResponse = VFC111_TestDriveSACBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda*/
		this.buildAgendaVehicle();
	}
	
	public void getAgendaPreviousDay()
	{
		VFC110_TestDriveSACVO testDriveVOResponse = null;
		
		/*decrementa um dia no datebooking para obter a agenda de um dia atrás*/
		this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation - 1;
		
		/*obtém a agenda de um dia atrás*/
		testDriveVOResponse = VFC111_TestDriveSACBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda*/
		this.buildAgendaVehicle();		
	}


	/**
	* Trata a ação do botão de confirmação de test drive. (Botão Sim do pop-up de confirmação do agendamento).
	*/
	public PageReference confirmSchedulingTestDrive()
	{
		PageReference pageRef = null;
		String[] arrSelectedTime = null;
		Integer hour = null;
		Integer minutes = null;
		String strMessage = null;
		ApexPages.Message message = null;
		
		/*faz o split do horário no formato HH:mm para a hora e os minutos separados*/			
		arrSelectedTime = this.testDriveVO.selectedTime.split(':');
			
		/*converte a hora e os minutos para inteiro*/
		hour = Integer.valueOf(arrSelectedTime[0]);
		minutes = Integer.valueOf(arrSelectedTime[1]);
			
		/*cria o datebooking completo*/
		this.testDriveVO.dateBooking = DateTime.newInstance(this.testDriveVO.dateBookingNavigation.year(), 
														    this.testDriveVO.dateBookingNavigation.month(), 
		                                                    this.testDriveVO.dateBookingNavigation.day(), 
		                                                    hour,
	                                                        minutes, 
	                                                        0); 
		                                                    		                        	                                               	                                                   		                                                 	
		try
		{
			Account dealerAccount = VFC113_AccountBO.getInstance().getById(testDriveVO.sObjAccount.DealerInterest_BR__c);

			if (dealerAccount.isDealerActive__c)
				VFC111_TestDriveSACBusinessDelegate.getInstance().scheduleTestDrive(this.testDriveVO);

			// Send email to the customer and dealer owner
			sendTestDriveEmail(dealerAccount);

			pageRef = new PageReference('/' + this.testDriveVO.accountId); 
		}
		catch (Exception e)
		{		
			strMessage = 'Não foi possível agendar o Test Drive. Motivo: \n' + e.getMessage();
			message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
			ApexPages.addMessage(message);
		}
		return pageRef;			
	}


	/**
	* Trata a ação do botão Cancelar Agendamento (retorna para a conta de origem).
	*/
	public PageReference cancelScheduling()
	{
		PageReference pageRef = new PageReference('/' + this.testDriveVO.accountId);
		return pageRef;
	}


	/**
	 * Send email to the customer and dealer owner
	 */
	private void sendTestDriveEmail(Account dealerAccount)
	{
		// Build data to send an email confirmation
		String name = this.testDriveVO.customerName;
		String email = this.testDriveVO.persEmail;
		String phone = this.testDriveVO.persMobPhone;
		if (phone == null || phone == '') {
			phone = (this.testDriveVO.persLandline == null || this.testDriveVO.persLandline == '') ? '' : this.testDriveVO.persLandline;
		}
		String postalCode = '';
		String vehicleModel = this.testDriveVO.vehicleInterest;
		Datetime dateTimeForBooking = this.testDriveVO.dateBooking;
		
		if (dealerAccount.isDealerActive__c)
		{
			// Send email to customer
            String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template04', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, testDriveVO.accountId);
            system.debug('*** isDealerActive__c errorMessageCustomer:'+errorMessageCustomer);

            // Send email to dealer
            String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template06', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, testDriveVO.accountId);
            system.debug('*** isDealerActive__c errorMessage:'+errorMessage);
		}
		else
		{
			// Send email to customer
            String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template05', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, testDriveVO.accountId);
            system.debug('*** Not isDealerActive__c errorMessageCustomer:'+errorMessageCustomer);

            // Send email to dealer
            String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template07', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, testDriveVO.accountId);
            system.debug('*** Not isDealerActive__c errorMessage:'+errorMessage);
		}
	}
	
}