/**
* @author sebastien.ducamp@atos
* @description This class is used to manage the custom setting CS04_MYR_Settings__c without asking repeatedly for ARPE
* This class automatically display all the fields available for this custom field and the associated description.
* So, the description shoul be clear and comprehensible for everybody
* version 1.0: 16.07 / Sébastien DUCAMP / initial revision
*/
global class Myr_AdministrationGUI_Global_CTR { 

	public CS04_MYR_Settings__c InnerSettings {get; set;}
	public Map<String, List<String>> GlobalFields {get; set;}
	public Boolean EditMode {get; private set;}
	public Myr_AdministrationGUI_Proxify_CLS.ProxySetting MyrSettings {get; set;}
	private Myr_AdministrationGUI_Tools GUITools;

	/* @constructor **/
	public Myr_AdministrationGUI_Global_CTR() {
		getSettings();
	}

	/* Initial query to retrieve all the fields **/
	private void getSettings() {
		system.debug('### Myr_AdministrationGUI_Global_CTR - <getSettings> - BEGIN');
		GUITools = new Myr_AdministrationGUI_Tools(); 
		EditMode = false;
		Myr_MyRenaultTools.SelectAll queryAll = Myr_MyRenaultTools.buildSelectAllQuery( CS04_MYR_Settings__c.class.getName(), null );
		InnerSettings = Database.query( queryAll.Query );
		List<String> Fields = queryAll.Fields;
		GlobalFields = GUITools.buildGroups( Fields );
		MyrSettings = Myr_AdministrationGUI_Proxify_CLS.proxify(InnerSettings, Fields, CS04_MYR_Settings__c.class.getName());
		system.debug('### Myr_AdministrationGUI_Global_CTR - <getSettings> - END');
	}

	/* Save the current configuration **/
	public void save() {
		//Save the data
		SObject OldSetting = MyrSettings.original;
		try {
			InnerSettings = (CS04_MYR_Settings__c) MyrSettings.copySettings( );
			update InnerSettings;
		} catch (Exception e) {
			String message = String.format( system.Label.Myr_GUI_Global_SaveFailed, new List<String>{e.getMessage()});
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, message ) );
			return;
		}
		//Log the information
		try {
			INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
									, Myr_AdministrationGUI_Global_CTR.class.getName()
									, 'Save Global'
									, InnerSettings.Id
									, 'OK'
									, 'List of Modifications: ' + MyrSettings.getModifications()
									, INDUS_Logger_CLS.ErrorLevel.Info );
		} catch (Exception e) {
			//Do nothing: do not interrompt the treatment because of a logging problem
		}
		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, system.Label.Myr_GUI_Global_SaveSuccess) );
		EditMode = false;
	}

	/* Switch in edit mode **/
	public void edit() {
		EditMode = true;
	}

	/* Cancel the current configuration **/
	public void cancel() {
		EditMode = false;
		getSettings();
	}


}