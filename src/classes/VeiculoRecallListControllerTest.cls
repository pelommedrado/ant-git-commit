@isTest
private class VeiculoRecallListControllerTest {
    
    static VEH_Veh__c v;
    static Recall_Relation__c recall;
    static Case caso;
    
    static Id getRecordTypeId(String developerName) {
        RecordType sObjRecordType = [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName =: developerName];
        
        return sObjRecordType.Id;
    }
    
    static {
        Account account1 = new Account(
            Name = 'Account Test 01',
            RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id,
            IDBIR__c = '1001');
        insert account1;
        
        Id recordTypeId = getRecordTypeId('Personal_Acc');
        
        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.RgStateTexto__c = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone = '1133334444';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.Sex__c = 'Man';
        sObjAccPersonal.MaritalStatus__c = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate = System.today();
        insert sObjAccPersonal;
        
        MyOwnCreation moc = new MyOwnCreation();
        v = moc.criaVeiculo();
        v.Name = '12345678901234567';
        v.Model__c = 'CLIO';
        v.Price__c = 1;
        v.DateofManu__c = system.today();
        Insert v;
        
        Recall__c rec = new Recall__c();
        rec.Recall_Name__c = 'Recall_Name__c';
        
        INSERT rec;
        
        recall = new Recall_Relation__c();
        recall.Vehicle__c = v.Id;
        recall.Recall__c = rec.Id;
        
        INSERT recall;
        
        caso = new Case();
        caso.VIN__c = v.Id;
        INSERT caso;
    }
    
    static testMethod void unitTest01() {
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(v);
        
        VeiculoRecallListController ct = new VeiculoRecallListController(stdCont);
        
        Apexpages.currentPage().getParameters().put('recallId', recall.Id);
        ct.clienteInformado();
    }
    
    static testMethod void unitTest02() {
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(caso);
        
        VeiculoRecallListController ct = new VeiculoRecallListController(stdCont);
        
        Apexpages.currentPage().getParameters().put('recallId', recall.Id);
        ct.clienteInformado();
    }
}