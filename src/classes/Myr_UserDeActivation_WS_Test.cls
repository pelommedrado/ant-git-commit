/*  Test class for desactivation of user WS. Test cases are :
    The deactivation of the user performed OK
    The user was already deactivated.
    Value missing : Parameter userId
    userId not found
    An issue occurred during the deactivation of the user: <technical error message>
    The user was existing, but the profile of the user was not the one attempted. Impossible to deactivate the user: <profile name>    
*************************************************************************************************************
2014 Oct : Creation
2016 Sep : R16.10 11.1 BlackEagle, D. Veron (AtoS) US-3655 : If an account is desactivated, status of associated customer messages are set to deleted
************************************************************************************************************/
@isTest
private class Myr_UserDeActivation_WS_Test {
	
	//check the logs contained in the logger__c
	private static void checkLogs(String inputs, String output) {
  		//Exception_Type__c & Message__c & Record__c are not filterable as they are long text area
	  	List<Logger__c> logs = [SELECT Id, RunId__c, Error_Level__c, Exception_Type__c, Function__c, Message__c, Project_Name__c, Record__c FROM Logger__c ];
	  	Boolean entry = false;
	  	Boolean code = false;
	  	system.debug('### checkLogs - inputs=' + inputs);
		system.debug('### checkLogs - output=' + output);
   		for( Logger__c log : logs ) {
			system.debug('### checkLogs - log=' + log);
			if( log.Record__c.equalsIgnoreCase(inputs)) {
				entry = true;
			} 
			if( log.Exception_Type__c.equalsIgnoreCase(output) ) {
				code = true;
			}
   		}
		system.debug('### checkLogs - entry=' + entry);
		system.debug('### checkLogs - code=' + code);
   		system.assertEquals(true, entry && code);
  	}
    
    @testSetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    } 
    
    //Deactivation of Renault only: user deactivated
    static testMethod void test_010_DeactivationRenaultOnly() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Test.startTest(); //potential future methods launched
            Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
            acc.MYR_Status__c = system.Label.Myr_Status_Activated;
            acc.MYD_Status__c = '';
            update acc;
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, null);
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( false, usr.isActive );
        String usr_regex = 
            (CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c).toUpperCase()
            + ('.' + acc.Id + '@').toUpperCase()
            + '(\\d*)'
            + ('\\.com').toUpperCase();
        system.debug('>>>> Compare regex='+usr_regex+' with '+usr.Username);
        system.assertEquals( true, Pattern.matches(usr_regex, usr.Username.toUpperCase()) );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myr_Status_Deleted, acc.MYR_Status__c);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=null>';
        checkLogs(inputs, response.info.code);
    }
    
    //Deactivation of Dacia only: user deactivated
    static testMethod void test_020_DeactivationDaciaOnly() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Test.startTest(); //potential future methods launched
            Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
            acc.MYR_Status__c = '';
            acc.MYD_Status__c = system.Label.Myd_Status_Activated;
            update acc;
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, 'Dacia');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( false, usr.isActive );
        String usr_regex = 
            (CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c).toUpperCase()
            + ('.' + acc.Id + '@').toUpperCase()
            + '(\\d*)'
            + ('\\.com').toUpperCase();
        system.debug('>>>> Compare regex='+usr_regex+' with '+usr.Username);
        system.assertEquals( true, Pattern.matches(usr_regex, usr.Username.toUpperCase()) );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myd_Status_Deleted, acc.MYD_Status__c);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=Dacia>';
        checkLogs(inputs, response.info.code);
    }
    
    //Deactivation of Dacia, Renault remains active: user remains active
    static testMethod void test_030_DeactivationDaciaRenaultActive() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        acc.MYR_Status__c = system.Label.Myr_Status_activated;
        acc.MYD_Status__c = system.Label.Myd_Status_activated;
        update acc;
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        String usernameBefore = usr.Username;
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, 'Dacia');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( true, usr.isActive );
        system.assertEquals( usernameBefore, usr.Username );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myd_Status_Deleted, acc.MYD_Status__c);
        system.assertEquals(system.Label.Myr_Status_Activated, acc.MYR_Status__c);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=Dacia>';
        checkLogs(inputs, response.info.code);
    }
    
    //Deactivation of Renault, Dacia remains active: user remains active
    static testMethod void test_040_DeactivationRenaultDaciaActive() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        acc.MYR_Status__c = system.Label.Myr_Status_Activated;
        acc.MYD_Status__c = system.Label.Myd_Status_Activated;
        update acc;
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        String usernameBefore = usr.Username;
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, 'ReNaUlT');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( true, usr.isActive );
        system.assertEquals( usernameBefore, usr.Username );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myd_Status_Activated, acc.MYD_Status__c);
        system.assertEquals(system.Label.Myr_Status_Deleted, acc.MYR_Status__c);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=ReNaUlT>';
        checkLogs(inputs, response.info.code);
    }
    
    //User is deactivated even if MYDStatus and MYR_Status are already set to deleted
    static testMethod void test_050_UserDeactivationRenaultDaciaAlreadyDeleted() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        acc.MYR_Status__c = system.Label.Myr_Status_Deleted;
        acc.MYD_Status__c = system.Label.Myd_Status_Deleted;
        update acc;
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, 'Renault');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( false, usr.isActive );
        String usr_regex = 
            (CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c).toUpperCase()
            + ('.' + acc.Id + '@').toUpperCase()
            + '(\\d*)'
            + ('\\.com').toUpperCase();
        system.assertEquals( true, Pattern.matches(usr_regex, usr.Username.toUpperCase()) );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myd_Status_Deleted, acc.MYD_Status__c);
        system.assertEquals(system.Label.Myr_Status_Deleted, acc.MYR_Status__c);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=Renault>';
        checkLogs(inputs, response.info.code);
    }
	
     
    //test bad brand parameter
    static testMethod void test_060_BadBrandParameter () { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, 'Tricot');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS505', response.info.code);
        system.assertEquals('Invalid parameter accountBrand :Tricot', response.info.message);
        
        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=Tricot>';
        checkLogs(inputs, response.info.code);
    }
         
    //The user was already deactivated.
    static testMethod void test_070_Desactivated() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Unactivated customer community user
        List<Account> myListAcc=Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.INACTIVE_USERS);
        String myStr=myListAcc[0].Id;
        system.runAs(techUser) {
        	Test.startTest(); //potential future methods launched
            response = Myr_UserDeActivation_WS.deActivateUser(myStr, null);
            Test.stopTest();
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS001', response.info.code);
        system.assertEquals('The user was already deactivated', response.info.message);
        
        String inputs='Inputs: <accountId='+myStr+', brand=null>';
        checkLogs(inputs, response.info.code);
    }
    
    //Value missing : Parameter userId
    static testMethod void test_080_UserId_Missing() { 
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;  
        system.runAs(techUser) {
        	Test.startTest(); //potential future methods launched
            response = Myr_UserDeActivation_WS.deActivateUser('', null);
            Test.StopTest();
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS504', response.info.code);
        system.assertEquals('Value missing : Parameter accountId', response.info.message);
        
        String inputs='Inputs: <accountId=, brand=null>';
        checkLogs(inputs, response.info.code); 
    }
    
    //userId not found
    static testMethod void test_090_UserId_Not_Found() { 
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null; 
        system.runAs(techUser) {
        	Test.startTest(); //potential future methods launched
            response = Myr_UserDeActivation_WS.deActivateUser('1234', null);
            Test.stopTest();
        }
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS501', response.info.code);
        system.assertEquals(true, response.info.message.contains('accountId not found'));
        
        String inputs='Inputs: <accountId=1234, brand=null>';
        checkLogs(inputs, response.info.code);
    }
    
    //The user was existing, but the profile of the user was not the one attempted. Impossible to deactivate the user. Profile name :<profile name>
    static testMethod void test_100_User_Existing() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //New unactivated user (customer community)
        List<Account> myListAcc=Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS_BADPROFILE);
        String myStr=myListAcc[0].Id;
        Test.startTest();
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myStr, null);
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS503', response.info.code);
        system.assertEquals(true, response.info.message.contains('The user was existing, but the profile of the user was not the one attempted. Impossible to deactivate the user. Profile name :'));
        
        String inputs='Inputs: <accountId='+myStr+', brand=null>';
        checkLogs(inputs, response.info.code);
    }

	//Deactivation with no user: OK WS03MS001 and account status = deleted whatever the inital status
	static testMethod void test_110_HQREQ03957_REN_DeactivationWOUser() { 
		 Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.NO_USERS);
        Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        acc.MYR_Status__c = system.Label.Myr_Status_Created;
        update acc;
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(acc.Id, 'Renault');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals(true, response.info.message.contains('The deactivation of the user performed OK'));
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(null, acc.MYD_Status__c);
        system.assertEquals(system.Label.Myr_Status_Deleted, acc.MYR_Status__c);
	}
	

	//Deactivation with no user: OK WS03MS001 and account status = deleted whatever the inital status
	static testMethod void test_120_HQREQ03957_DAC_DeactivationWOUser() { 
		Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        //Custom settings & technical user
        User techUser=Myr_Datasets_Test.getTechnicalUser('France');
        //Activated customer community user
        List<Account> myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.NO_USERS);
        Account acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        acc.MYD_Status__c = system.Label.Myr_Status_Created;
        update acc;
        Test.startTest(); //potential future methods launched
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(acc.Id, 'Dacia');
            }
        Test.stopTest();
        system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals(true, response.info.message.contains('The deactivation of the user performed OK'));
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myd_Status_Deleted, acc.MYD_Status__c);
        system.assertEquals(null, acc.MYR_Status__c);
	}
	

	//Status of customer messages are set to deleted
    static testMethod void test_130_Customer_Messages_Set_Deleted() { 
        Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response = null;
        User techUser;
		List<Account> myListAcc;
		Customer_Message__c C_M;
		List<Customer_Message__c> L_M;
		Account acc;
		List<Customer_Message_Templates__c> CMT;

		Customer_Message_Settings__c sett = new Customer_Message_Settings__c(Name='CUS_CASE', Type__c='Case online', NbInputs__c=3);
		insert sett;

		Customer_Message_Templates__c Temp = new Customer_Message_Templates__c( 
                MessageType__c=sett.Id
              , Country__c='France'
              , Brand__c='Renault'
              , Language__c='fr'
              , isActive__c=true
              , Title__c='Case numero ' + Label.Customer_Message_Input2
              , Body__c='Votre '+Label.Customer_Message_Input1+' num�ro '+Label.Customer_Message_Input2+' est maintenant @ l\'�tat '+Label.Customer_Message_Input3
              , Summary__c=Label.Customer_Message_Input1+' num�ro '+Label.Customer_Message_Input2+', ' +Label.Customer_Message_Input3
              , Channel__c='message, popin');
		insert Temp;

		techUser = Myr_Datasets_Test.getTechnicalUser('France');
		CMT = [select Id from Customer_Message_Templates__c];
        myListAcc = Myr_Datasets_Test.insertPersonalAccounts(1,Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		
		acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
		
		C_M = new Customer_Message__c();
		C_M.Account__c=myListAcc[0].Id;
		C_M.Body__c='body';
		C_M.Title__c='title';
		C_M.Template__c=CMT[0].Id;
		C_M.Channel__c='email';
		C_M.Status__c='read';
		insert C_M;
		
        Test.startTest();
            system.runAs(techUser) {
                response = Myr_UserDeActivation_WS.deActivateUser(myListAcc[0].Id, null);
            }
        Test.stopTest();
        
		system.assertNotEquals(null, response);
        system.assertEquals('WS03MS000', response.info.code);
        system.assertEquals('The deactivation of the user performed OK', response.info.message);
        User usr = [SELECT Id, isActive, Username FROM User WHERE AccountId = :myListAcc[0].Id];
        system.assertEquals( false, usr.isActive );
        String usr_regex = 
            (CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c).toUpperCase()
            + ('.' + acc.Id + '@').toUpperCase()
            + '(\\d*)'
            + ('\\.com').toUpperCase();
        system.debug('>>>> Compare regex='+usr_regex+' with '+usr.Username);
        system.assertEquals( true, Pattern.matches(usr_regex, usr.Username.toUpperCase()) );
        acc = [SELECT Id, MYD_Status__c, MYR_Status__c FROM Account WHERE Id = :myListAcc[0].Id];
        system.assertEquals(system.Label.Myr_Status_Deleted, acc.MYR_Status__c);

		L_M = [select Id, Body__c, Status__c from Customer_Message__c];
		system.assertEquals(1, L_M.size());
		system.assertEquals('body', L_M[0].Body__c);
		system.assertEquals('deleted', L_M[0].Status__c);

        String inputs='Inputs: <accountId='+myListAcc[0].Id+', brand=null>';
        checkLogs(inputs, response.info.code);   

    }

}