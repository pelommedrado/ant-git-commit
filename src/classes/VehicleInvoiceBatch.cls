global class VehicleInvoiceBatch implements Schedulable, Database.Batchable<sObject> {

	String query;

	global VehicleInvoiceBatch() {
		this.query = 'SELECT Id, Name, DeliveryDate__c'
     	+ ' FROM Veh_VEH__c WHERE ValidInvoice__c = \'NI\'';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('execute: ' + scope.size());
	  VehicleInvoiceChecker servico = new VehicleInvoiceChecker();
	  servico.startSetting(scope);
	}

	global void finish(Database.BatchableContext BC) {

	}
    
	global void execute(SchedulableContext sc) {
  	System.debug('Iniciando a execucao do batch');
    VehicleInvoiceBatch fatBatch = new VehicleInvoiceBatch();
    Database.executeBatch(fatBatch);
  }
}