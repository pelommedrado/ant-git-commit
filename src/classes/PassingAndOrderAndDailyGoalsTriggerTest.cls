@isTest
public class PassingAndOrderAndDailyGoalsTriggerTest {
    
    static testMethod void myTest(){
        
        Account acc = new Account(
            Name = 'Dealer',
            IDBIR__c = '123456',
            Dealer_Matrix__c = '123456',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc')
        );
        Insert acc;
        
        Account acc2 = new Account(
            Name = 'Dealer2',
            IDBIR__c = '1234567',
            Dealer_Matrix__c = '1234567',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc')
        );
        Insert acc2;
        
        DailyGoals__c dg = new DailyGoals__c(
            BIR_Number__c = '1234567',
            Dealer_Matrix__c = acc2.Id,
            Goal_Date__c = system.today(),
            Goals_Number__c = 1,
            Vehicle_Model__c = 'B4S'
        );
        Insert dg;

        DailyGoals__c dg2 = new DailyGoals__c(
            BIR_Number__c = '1234567',
            Dealer_Matrix__c = acc.Id,
            Goal_Date__c = system.today(),
            Goals_Number__c = 1,
            Vehicle_Model__c = 'B9S'
        );
        Insert dg2;
        
        PassingAndPurchaseOrder__c pp = new PassingAndPurchaseOrder__c(
            Account__c = acc2.Id,
            Date__c = system.today(),
            Status__c = 'Sent'
        );
        Insert pp;
        
        PassingAndPurchaseOrder__c pp2 = new PassingAndPurchaseOrder__c(
            Account__c = acc.Id,
            Date__c = system.today(),
            Status__c = 'Draft',
            Feirao__c = true
        );
        Insert pp2;
        
    }
    
    static testMethod void myTest2(){
        
        Account acc2 = new Account(
            Name = 'Dealer2',
            IDBIR__c = '1234567',
            Dealer_Matrix__c = '1234567',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc')
        );
        Insert acc2;
        
        PassingAndPurchaseOrder__c pp = new PassingAndPurchaseOrder__c(
            Account__c = acc2.Id,
            Date__c = system.today(),
            Status__c = 'Sent'
        );
        Insert pp;
        
    }
    
}