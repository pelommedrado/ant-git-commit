@isTest
private class VFC144_ManagerLeadBoxControllerTest {

    static testMethod void myUnitTest() {
        User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concessionária teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
        Database.insert( dealerAcc );
        Opportunity opp = new Opportunity(
          Name = 'Test opp',
          Dealer__c = dealerAcc.Id,
          OwnerId = manager.Id,
          OpportunitySource__c = 'ABC',
          StageName = 'Identified',
          CloseDate = System.today().addDays( 10 )
        );
        Database.insert( opp );
        
        System.runAs( manager ){
          VFC144_ManagerLeadBoxController controller = new VFC144_ManagerLeadBoxController();
          String welcomeMessage = controller.welcomeMessage;
          List< SelectOption > campaignSelectList = controller.campaignSelectList;
          List< SelectOption > sellerSelectList = controller.sellerSelectList;
          List< Opportunity > oppList = controller.oppList;
          //controller.oppIdListAsString = JSON.Serialize( new List< Id >{opp.Id} );
          controller.forward();
        }
    }
}