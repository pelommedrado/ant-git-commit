/**
* Classe que representa exceções geradas na atualização de Test Drive.
* @author Felipe Jesus Silva.
*/
public class VFC59_UpdateTestDriveException extends Exception
{
	public VFC59_UpdateTestDriveException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}