@isTest

class AttachmentTrigger_Test {
    
    static TestMethod void TestInsertWithoutValue() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français'  ,Case_RecordType__c='FR_Case_RecType');
        insert ctr;        
        User usr = new User (LastName = 'Rotondo', alias = 'lro',RecordDefaultCountry__c='France', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {           
            Test.startTest();  
            String OwnerUser = usr.Id;
            Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert Acc;
            Case cs = new case (Type='Complaint', Origin='RENAULT SITE', AccountId=Acc.Id, ContactId=Acc.PersoncontactId, Status='New', Description='Trigger test clas', CountryCase__c='Argentina');
            insert cs;
            String idCase = cs.Id;        
            Attachment obj = new Attachment();        
            obj.Name = 'TestValue';
            obj.IsPrivate = False;
            obj.OwnerId = OwnerUser;
            obj.ParentId = idCase ;
            obj.Body = Blob.valueOf('Test Body');
                    
            insert obj;   
        }
        Id caid;
        Id RectypeId=[SELECT Id FROM RecordType WHERE DeveloperName = 'Network_Site_Acc'].Id;
        Account ac=new Account(Name='Test Clas Account',IDBIR__c='1233g',RecordTypeId=RectypeId,Country__c='India',DealershipStatus__c='Active' );
        insert ac;
        system.debug('ac-->'+ac.Country__c);
        Contact cn=new Contact(LastName='NwtCon',AccountId=ac.Id,Email = 'testContact@test.com');
        insert cn;
        system.debug('cn-->'+cn);        
        Id profid=[SELECT Id FROM Profile WHERE Name = 'CORE - Service Profile'].Id;

        Id crurid=UserInfo.getUserId();
        System.runAs(usr) {
              /*  Id Rectype=[SELECT Id FROM RecordType WHERE DeveloperName = 'CORE_ACC_Personal_Account_RecType'].Id;
                Account ac1=new Account(LastName='TestAccount',RecordTypeId=Rectype,Country__c='India',PersEmailAddress__c='test@test.com',ComAgreemt__c='Yes' );
                insert ac1;
                system.debug('ac1-->'+ac1);*/
                Case Case1 = new Case  (Origin = 'Dealer', Type = 'Information Request', SubType__c = 'Booking', Status = 'Open', Priority = 'Urgent',
                                        Description = 'Description', From__c = 'Customer', Kilometer__c = 1000000, AccountId = ac.Id,ContactId=cn.Id,Dealer__c=ac.id,DealerContactLN__c=cn.Id);
                insert Case1;                
                caid=Case1.Id;
                Case1.OwnerId=crurid;
                update Case1;
                system.debug('Case1-->'+Case1);                            
        } 
        Attachment att = new Attachment();        
        att.Name = 'TestComValue';
        att.IsPrivate = False;
        att.ParentId = caid ;
        att.Body = Blob.valueOf('Test Body');       
        att.OwnerId=crurid;
        insert att;

    } 
    
    static testMethod void afterdeleteAttach(){
    
           Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Fran?ais', Case_RecordType__c = 'FR_Case_RecType');
        insert ctr;


        User usr = new User (LastName = 'Mehdi', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'mehdi@mehdi.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'mehdi@mehdi.testren.com');
        System.runAs(usr) {
            Account acc = new Account(Name = 'test');
            List<RecordType> listRT = [SELECT Id From RecordType WHERE sobjecttype = 'Task'];
            //just to have a minimal code coverage for task_afterdelete_trigger
            Task tsk1 = new Task(
                Subject = 'Test Trigger',
                Type = '',
                Priority = 'Normal',
                Status = 'To Do',
                OwnerId = UserInfo.getUserId(),
                WhoId = acc.Id,
                recordtypeid = listRT[0].Id
            );
            insert tsk1;
        List<Attachment> attLst=new List<Attachment>();
        Attachment att= new Attachment();
        att.Name = 'test.gif';
        att.Body = Blob.valueOf('attachment  test');
        att.parentId=tsk1.Id;
        attLst.add(att); 
        insert att;      
        Rforce_AttachmentUtils_CLS.activitymanagementonattachment(attLst,usr);
    
    }
    }
        static testMethod void afterdeleteAttachlink(){
    
           Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Fran?ais', Case_RecordType__c = 'FR_Case_RecType');
        insert ctr;


        User usr = new User (LastName = 'Mehdi', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'mehdi@mehdi.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'mehdi@mehdi.testren.com');
        System.runAs(usr) {
            Account acc = new Account(Name = 'test');
            List<RecordType> listRT = [SELECT Id From RecordType WHERE sobjecttype = 'Task'];
            //just to have a minimal code coverage for task_afterdelete_trigger
            Task tsk1 = new Task(
                Subject = 'Test Trigger',
                Type = '',
                Priority = 'Normal',
                Status = 'To Do',
                OwnerId = UserInfo.getUserId(),
                WhoId = acc.Id,
                recordtypeid = listRT[0].Id
            );
            insert tsk1;
        List<Attachment> attLst=new List<Attachment>();
        Attachment att= new Attachment();
        att.Name = 'test.gif';
        att.Body = Blob.valueOf('attachment  test');
        att.parentId=tsk1.Id;
        attLst.add(att);  
    }
    }
     
    static testMethod void afterdeleteAttachemail(){
    
           Profile profile=[SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = new User (LastName='Rotondo',BypassVR__c=TRUE,alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=profile.Id,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
        insert usr;
        System.runAs(usr){
           Test.startTest();
           Group grp = new Group(Name='Rforce_Sprint_Dev_Team');   
           insert grp;                 
           GroupMember grpMember = new GroupMember();
           grpMember.GroupId = grp.id;
           grpMember.UserOrGroupId=usr.id;     
           insert grpMember;
           AssignmentRule assignmentRule = new AssignmentRule();
           assignmentRule=[select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
           Database.DMLOptions dmlOpts = new Database.DMLOptions();
           dmlOpts.assignmentRuleHeader.assignmentRuleId= assignmentRule.id;  
           Id recid=[Select id from recordtype where DeveloperName='CORE_ACC_Company_Account_RecType'].Id;   
           Account Acc = new Account(Name='Test1',Phone='0000',ProfEmailAddress__c = 'addr1@mail.com',recordtypeid=recid);
           insert Acc;          
           Contact Con = new Contact(LastName='Test Contact',Salutation='Mr.',Email='lro@lro.com',AccountId=Acc.Id);
           insert Con;           
           VEH_Veh__c Vehicule = new VEH_Veh__c (Name = '21342312323123456',  VehicleBrand__c = 'Renault', KmCheck__c = 100 , KmCheckDate__c = date.today());
           insert Vehicule;            
           Case cs = new Case(Origin='Phone',Type='Complaint',Status='New',Priority='Normal',Description='Description',CountryCase__c='CS',Subject='Test',From__c='Customer',VIN__c = Vehicule.Id,AccountId=Acc.Id,ContactId=Con.Id,OwnerId=usr.Id);  
           cs.setOptions(dmlOpts);
           insert cs;           
           cs.Tech_ExternalID__c='500m000000204Uh';
           cs.Status = 'Closed';         
           update cs;
           Case cs1 = new Case(Origin='Phone',Type='Complaint',Status='New',Priority='Normal',Description='Description',CountryCase__c='CS',Subject='Test',From__c='Customer',VIN__c = Vehicule.Id,Tech_ExternalID__c='500m0000001vFNV',AccountId=Acc.Id,ContactId=Con.Id,OwnerId=usr.Id);
           insert cs1;           
           EmailMessage msg= new EmailMessage(ParentId=cs.Id,FromAddress='test@rntbci.com',Incoming=true,ToAddress='testmail@sample.com',Subject=cs.Id,TextBody='TextBody',HtmlBody='HtmlBody');         
           insert msg;  
       
          
            List<Attachment> attLst=new List<Attachment>();
            Attachment att= new Attachment();
            att.Name = 'test.gif';
            att.Body = Blob.valueOf('attachment  test');
            att.parentId=msg.Id;
            attLst.add(att);
            //AttachmentTriggerHandler attach=new AttachmentTriggerHandler();
            AttachmentTriggerHandler.onBeforeInsert(attLst);    
              
                 
        }
    }
     static testMethod void activity(){
    
            Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français'  ,Case_RecordType__c='FR_Case_RecType');
        insert ctr;        
        User usr = new User (LastName = 'Rotondo', alias = 'lro',RecordDefaultCountry__c='France', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {           
            Test.startTest();  
            String OwnerUser = usr.Id;
            Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert Acc;
            Case cs = new case (Type='Complaint',EmailContactDealer__c='test@gmail.com', Origin='RENAULT SITE', AccountId=Acc.Id, ContactId=Acc.PersoncontactId, Status='New', Description='Trigger test clas', CountryCase__c='Argentina');
            insert cs;
        List<Attachment> attLst=new List<Attachment>();
        Attachment att= new Attachment();
        att.Name = 'test.gif';
        att.Body = Blob.valueOf('attachment  test');
        att.parentId=cs.Id;
        attLst.add(att);       
        //AttachmentTriggerHandler.onAfterDelete(attLst);
        Rforce_AttachmentUtils_CLS.activitymanagementonattachment(attLst,usr);
    
    }
    }
       
}