public with sharing class VFC12_CreateVehicleRelationForCase 
{
	public String accountId {get;set;}
	public String vehicleId {get;set;}
	public String vehicleName {get;set;}
	public VRE_VehRel__c vehRel {get;set;}
	
	public VFC12_CreateVehicleRelationForCase()
    {
    	accountId = Apexpages.currentPage().getParameters().get('accountId');
    	vehicleId = Apexpages.currentPage().getParameters().get('vehicleId');
    	vehicleName = '';
    	vehRel = new VRE_VehRel__c();
    	
		if (accountId != null && accountId != '' && vehicleId != null && vehicleId != '')
		{
			vehRel.Account__c = accountId;
			vehRel.VIN__c = vehicleId;
			vehicleName = [Select Name from VEH_Veh__c where Id = :vehicleId Limit 1].Name;
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.AccountIdMandatory + '<br/>' + System.label.VehicleIdMandatory);
        	ApexPages.addMessage(myMsg);
		}
    }
    
    public PageReference save()
    {
    	try
    	{
    		insert vehRel;
    	}
    	catch(DmlException dmle)
    	{
    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSave);
        	ApexPages.addMessage(myMsg);
    	}
    	
    	return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/500/e?retURL='+accountId+'&def_account_id='+accountId+'&CF'+System.label.VinFieldIdCase+'_lkid='+vehicleId+'&CF'+System.label.VinFieldIdCase+'='+vehicleName);
    }
    
	public PageReference skip()
    {
    	return new PageReference('/500/e?retURL=' + accountId + '&def_account_id=' + accountId + '&CF'+System.label.VinFieldIdCase+'_lkid='+vehicleId+'&CF'+System.label.VinFieldIdCase+'='+vehicleName);
    }
    
    public PageReference back()
    {
    	return new PageReference('/apex/VFP11_SearchVehicleFromAccountToCase?Id='+accountId);
    }
    
    public PageReference cancel()
    {
    	return new PageReference('/' + accountId);
    }
}