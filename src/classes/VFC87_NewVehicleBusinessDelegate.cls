/**
* Classe responsável por fazer o papel de business delegate da página de detalhe de Inserção de Novo Veiculos.
* @author Elvia Serpa.
*/ 
public class VFC87_NewVehicleBusinessDelegate 
{
    private VFC80_VehicleVO VehicleVO = new VFC80_VehicleVO();
            
    private static final VFC87_NewVehicleBusinessDelegate instance = new VFC87_NewVehicleBusinessDelegate();
        
    /**
    * Construtor privado para impedir a criação de instancias dessa classe.
    */
    private VFC87_NewVehicleBusinessDelegate()
    {
        
    }
    
    /**
    * Método responsável por prover a instância dessa classe.
    */  
    public static VFC87_NewVehicleBusinessDelegate getInstance()
    {
        return instance;
    }

    /**
    * 
    */  
    public VFC80_VehicleVO getAccountModel(String QuoteId)
    {
        VehicleVO.objsQuote = VFC35_QuoteDAO.getInstance().fetchById(QuoteId); 
        
        return VehicleVO; 
    }
    
    /**
    * 
    */  
    public List<Product2> selectModel()
    {
        List<Product2> lstProduct = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');

        return  lstProduct; 
    }

    /**
    * 
    */  
    public List<Product2> selectVersion(string model)
    {
    	
    	Product2 sObjModel = VFC22_ProductDAO.getInstance().findById(model);
    	
        List<Product2> lstProductVersion = VFC22_ProductDAO.getInstance().fetchProductsByModel(sObjModel.Name);   
        
        return  lstProductVersion;          
    }
   
    

    /**
    * 
    */  
    public List<Vehicle_Item__c> selectColor(String model)
    {
    	Product2 sObjModel = VFC22_ProductDAO.getInstance().findById(model);
    	
        List<Vehicle_Item__c> lstColor = VFC64_VehicleItem_DAO.getInstance().fetchVehicleItemByModel(sObjModel.Name);

        return  lstColor;   
    }

    /**
    * 
    */      
    public void  includeItemVehicle(string idProduto, String idVehico, string quoteId, Decimal unitPrice)
    {
        PricebookEntry sObjPricebookEntry = getPricebookEntry(idProduto);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.quoteId = QuoteId;
        quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = unitPrice;
        quoteLineItem.Vehicle__c = idVehico;
        
        VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem);     
    }   

    /**
    * 
    */      
    public void  includeItemGeneric(String idProduto,  string quoteId)
    {
        PricebookEntry sObjPricebookEntry = getPricebookEntry(idProduto);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quoteId;
        quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = sObjPricebookEntry.UnitPrice;
        
        VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem);     
    }
    
    public void  includeVersionTable(String quoteId, String modelId, String versionId, String corId, 
            String estofadoId, String harmoniaId, List<String> opcionalList) {
        
        Model__c model = [
            SELECT Id, Name, Model_PK__c
            FROM Model__c
            WHERE Status_SFA__c = 'Active' AND Id =: modelId
        ];
        String[] modelos = model.Name.split(' ');
        
        Product2 produto = [
            SELECT id, Name, Version__c, Model__r.Name, ModelSpecCode__c
            FROM Product2
            WHERE Name IN: modelos /*AND RecordType.Name = 'Model'*/ LIMIT 1
        ];
        
        PVVersion__c version = [
            SELECT Id, Price__c
            FROM PVVersion__c
            WHERE Id =: versionId
        ];

        Set<Id> optionalIds = new Set<Id>();
        optionalIds.add(corId);
        optionalIds.add(estofadoId);
        optionalIds.add(harmoniaId);

        System.debug('***opcionalList: '+opcionalList);

        for(String opt: opcionalList){
            optionalIds.add(opt);
        }
        
        List<Optional__c> opList = [
            SELECT Id, Name, Optional_Code__c, Amount__c, Type__c
            FROM Optional__c
            WHERE Id IN: optionalIds
        ];

        System.debug('***opList: '+opList);
        
        List<String> optionalCodes = new List<String>();
        List<String> optionalNames = new List<String>();
        String colorName, upholsteryName, harmonyName, colorCode, upholsteryCode, harmonyCode;
        
        for(Optional__c o : opList) {
            if(o.Type__c != 'Cor' && o.Type__c != 'Trim' && o.Type__c != 'Harmonia'){
                optionalNames.add(o.Name);
                optionalCodes.add(o.Optional_Code__c);
            }
            if(o.Type__c == 'Cor'){
                colorName = o.Name;
                colorCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Trim'){
                upholsteryName = o.Name;
                upholsteryCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Harmonia'){
                harmonyName = o.Name;
                harmonyCode = o.Optional_Code__c;
            }
        }
        
        PricebookEntry sObjPricebookEntry = getPricebookEntry(produto.Id);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quoteId;
        quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 0;
        
        quoteLineItem.Model_AOC__c 		= modelId;
        quoteLineItem.Version_AOC__c 	= versionId;
        quoteLineItem.Color_AOC__c 		= colorCode;
        quoteLineItem.ColorName__c      = colorName;
        quoteLineItem.Upholstery_AOC__c = upholsteryCode;
        quoteLineItem.UpholsteryName__c = upholsteryName;
        quoteLineItem.Harmony_AOC__c 	= harmonyCode;
        quoteLineItem.HarmonyName__c    = harmonyName;
        quoteLineItem.Optionals_AOC__c	= String.join(optionalCodes, ' ');
        quoteLineItem.OptionalsNames__c = String.join(optionalNames, ' ');

        System.debug('***quoteLineItem: '+quoteLineItem);
        
        VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem);

    }
    
    /**
    * 
    */      
    private Pricebook2 getStandardPriceBook()
    {
        Pricebook2 sObjPricebook2 = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();
        return sObjPricebook2;
    } 
    
    /**
    * 
    */      
    private PricebookEntry getPricebookEntry(String idProduto)
    {
    	System.debug('###################################################### idProduto' + idProduto);
    	
        PricebookEntry sObjPriceBookEntry = null;
        String priceBookId =  getStandardPriceBook().Id;
        
        System.debug('###################################################### priceBookId' + priceBookId);
        
        sObjPriceBookEntry = VFC75_AccessoryDAO.getInstance().getPricebookEntry(idProduto, priceBookId); 
    
    	System.debug('###################################################### sObjPriceBookEntry' + sObjPriceBookEntry);
    	
        return sObjPriceBookEntry;
    }   
    
    /**
    * 
    */  
    public List<VFC94_VehicleItemVO> vehiclesAvailableGrid(String quoteId, VFC80_VehicleVO objsVeihico)
    {
        VehicleVO.lstVehico = new List<VFC94_VehicleItemVO>();
        String strMessage = null;
        ApexPages.Message message = null;   
        
        System.debug('>>> objsVeihico'+objsVeihico);
        
        Quote objsQuote = VFC35_QuoteDAO.getInstance().fetchById(quoteId);
        
        Product2 modelo = VFC22_ProductDAO.getInstance().findById(objsVeihico.itemModel);
        
        Account parentAccount = VFC113_AccountBO.getInstance().getById(objsQuote.Opportunity.Dealer__c);
        
        if(parentAccount.ParentId == null){
            strMessage = 'Concessionária não esta relacionada a nenhum grupo.'; 
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);
                        
            return null;
        }
    
        //Chama a class responsáveil pela geração da query 
        VFC100_VehicleQueryBuilder vehicleQueryBuilder = new VFC100_VehicleQueryBuilder();
        //List<VRE_VehRel__c> listVehicle =  vehicleQueryBuilder.search(objsQuote.Opportunity.Dealer__c, modelo.Name, objsVeihico.itemVersion, objsVeihico.itemYear, objsVeihico.itemColor);
        List<VRE_VehRel__c> listVehicle =  vehicleQueryBuilder.search(parentAccount.ParentId, modelo.Name, objsVeihico.itemVersion, objsVeihico.itemYear, objsVeihico.itemColor);
        System.debug('>>> listVehicle'+listVehicle);        
        
        if(listVehicle.Size() == 0)
        {
            strMessage = 'Não existe veículo para este filtro.'; 
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);
                        
            return null;
        }
        
        Map <String, VFC94_VehicleItemVO> mapVehicleItemVO = new Map <String, VFC94_VehicleItemVO>();
        
        //Depois um query para trazer os produtos
        for(VRE_VehRel__c auxVehicle : listVehicle)
        {
            VFC94_VehicleItemVO VehicleVOItem = new VFC94_VehicleItemVO();
            VehicleVOItem.Id = auxVehicle.Id;   
            VehicleVOItem.idVehico = auxVehicle.VIN__c;
            VehicleVOItem.priceVehicle = auxVehicle.VIN__r.Price__c;
            VehicleVOItem.formattedPriceVehicle = VFC69_Utility.priceFormats(VehicleVOItem.priceVehicle);
            //vehicleItemVO.isCheckVehico = false;
            VehicleVOItem.status = convertStatus(auxVehicle.VIN__r.Status__c);

            VehicleVOItem.chassi = auxVehicle.VIN__r.Name;
            VehicleVOItem.modelo = auxVehicle.VIN__r.Model__c;
            VehicleVOItem.versao = auxVehicle.VIN__r.Version__c;
            VehicleVOItem.anoModelo = auxVehicle.VIN__r.ModelYear__c;
            VehicleVOItem.cor = auxVehicle.VIN__r.Color__c;
            VehicleVOItem.opcionais = auxVehicle.VIN__r.Optional__c;
            VehicleVOItem.anoFabricacao = auxVehicle.VIN__r.DateofManu__c.year();
            VehicleVOItem.CheckVehicodisabled = false;
            
            //Account vehicleAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(auxVehicle.Account__r.ParentId);
            
            VehicleVOItem.bir  = auxVehicle.Account__r.IDBIR__c;
            
            if(auxVehicle.DateOfEntryInStock__c != null ){
                //VehicleVOItem.tempoEstoque = auxVehicle.DateOfEntryInStock__c.format();
                VehicleVOItem.tempoEstoque = String.valueOf(auxVehicle.DateOfEntryInStock__c.daysBetween(Date.today())); 
            }
                
            mapVehicleItemVO.put(VehicleVOItem.idVehico, VehicleVOItem);    
        }
        
        Set<String> setVehicleId = new Set<String>();
        
        for(VFC94_VehicleItemVO vehicleItem: mapVehicleItemVO.values()){
        		setVehicleId.add(vehicleItem.idVehico);
        }
        
       	List<VehicleBooking__c> listVehicleBooking = VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(setVehicleId, 'Active');
       	
       	for(VehicleBooking__c vbk: listVehicleBooking ){
       		VFC94_VehicleItemVO vehicleItemVO = mapVehicleItemVO.get(vbk.Vehicle__c);
       		vehicleItemVO.CheckVehicodisabled = true;
       	}
        
        	
      	VehicleVO.lstVehico.addAll(mapVehicleItemVO.values());
        
        
        
        System.debug('>>> VehicleVO.lstVehico'+VehicleVO.lstVehico);
                
        return VehicleVO.lstVehico;
    }     
    
    public String convertStatus(String status){
    	
    	if(status == 'Stock'){
    		return 'Estoque';
    	}else if(status == 'In Transit'){
    		return 'Em trânsito';    		
    	}else if(status == 'Quality Blockade'){
    		return 'Bloqueio de Qualidade';    		
    	}else if(status == 'Comercial Blockade'){
    		return 'Bloqueio Comercial';    		
    	}else if(status =='Billed'){
    		return 'Faturado';    		
    	}
    	
    	return status;    	
    	
    }          
}