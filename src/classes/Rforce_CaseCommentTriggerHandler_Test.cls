@isTest
private class Rforce_CaseCommentTriggerHandler_Test {
    @isTest
    static void itShouldpass() {

        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', CurrencyCode__c = 'EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id, developername from RecordType where sObjectType = 'Account' and developername = 'Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', IDBIR__c = '00001314');
            insert Acc;
            System.debug('###Acc Id =' + Acc.Id);

        Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Status='New', Description='Trigger test clas', CountryCase__c='Colombia', dealer__c=acc.id);
            insert cs;

            System.debug('###itShouldpass method - Case Id =' + cs.Id);
            CaseComment caseCom = new CaseComment();
            caseCom.ParentId = cs.Id;


            caseCom.CommentBody = 'commentB';
            List<Casecomment> listcomment = new List<Casecomment>();
            try {
                insert caseCom;
                System.debug('###itShouldpass method - Insert Success' + caseCom.Id);
                listcomment.add(caseCom);
            } catch (Exception e) {

            }

            if (listcomment.size() > 0) {
                System.debug('###cod - listsize ###'+listcomment.size());
                Rforce_CaseCommentTriggerHandler_CLS.onBeforeInsert(listcomment);
                Rforce_CaseCommentTriggerHandler_CLS.sendEmail(listcomment);
            }

            Test.stopTest();
        }
    }

    @isTest
    static void itShouldFail() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', CurrencyCode__c = 'EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id, developername from RecordType where sObjectType = 'Account' and developername = 'Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, IDBIR__c = '00001314');
            insert Acc;
        Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Status='New', Description='Trigger test clas', CountryCase__c='Colombia', dealer__c=acc.id);
            insert cs;
            CaseComment caseCom = new CaseComment();
            caseCom.ParentId = cs.Id;
            System.debug('###Case Id =' + cs.Id);
            caseCom.CommentBody = 'commentB';
            List<Casecomment> listcomment = new List<Casecomment>();
            try {
                insert caseCom;
                System.debug('###Insert Success' + caseCom.Id);
                listcomment.add(caseCom);
            } catch (Exception e) {

            }
            Rforce_CaseCommentTriggerHandler_CLS.onBeforeInsert(listcomment);
            Rforce_CaseCommentTriggerHandler_CLS.sendEmail(listcomment);

            Test.stopTest();
        }
    }
}