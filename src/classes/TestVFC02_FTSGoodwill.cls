@isTest
public with sharing class TestVFC02_FTSGoodwill  
{
static testMethod void testFTSGoodwill()
{
   
    Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
    
    Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
    insert Acc;
    
    Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
    insert Con;
    VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766',DeliveryDate__c=date.parse('02/02/2007'));
    insert veh;     
    Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Approved',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id,VIN__c=veh.Id,CaseBrand__c='Renault',Kilometer__c=3333);
    insert c;
   // Case c1 = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id,CaseBrand__c='Renault');
    //insert c1;
    Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
     Id RecordId1= [select Id from RecordType where sObjectType='Goodwill__c' and Name='TECHNICAL' limit 1].Id;
    Goodwill__c g = new Goodwill__c (case__c = c.Id,BudgetCode__c='SRC',ExpenseCode__c='VN Warranty',ResolutionCode__c='Technical Goodwill',RecordTypeId=RecordId1,ORDate__c= date.parse('02/02/2012'),GoodwillStatus__c='Open', Organ__c= 'BATTERY', QuotWarrantyRate__c=1000,Country__c= 'Brazil');
    Goodwill__c g1 = new Goodwill__c (case__c = c.Id,BudgetCode__c='SRC',ExpenseCode__c='VN Warranty',ResolutionCode__c='CAR RENTAL GOODWILL',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2012'),Organ__c='BATTERY',GoodwillStatus__c='Approved', QuotWarrantyRate__c=1000,Country__c= 'Brazil');
   
    //insert g1;
    //insert g;
    Goodwill_Grid_Line__c grid= new Goodwill_Grid_Line__c (CurrencyIsoCode='BRL',Country__c ='Brazil', VehicleBrand__c = 'Renault',Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c= 800000);
    insert grid;
    Goodwill_Grid_Line__c grid1= new Goodwill_Grid_Line__c (CurrencyIsoCode='BRL',Country__c = 'Brazil', VehicleBrand__c = 'Renault',Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c= 800000);
    insert grid1;
    System.debug('Grid******'+grid1);
    FTS__c ft=new  FTS__c();
    ft.Case__c=c.Id;
    ft.ORDate__c= date.parse('02/02/2012');
    ft.Total_Amount_Labour__c=67;
    ft.Dealer_Labour_Rate__c=69;
    ft.Total_Amount_Of_Parts__c=354;
    insert ft;
    
  
    Test.StartTest();
    
    
    List<Goodwill__c> addgodwil=new  List<Goodwill__c>();
    try{
    addgodwil.add(g);
    addgodwil.add(g1);
    upsert addgodwil;
    System.debug('GW******'+addgodwil);
    
    }
    catch(DmlException d){
    system.debug('here we get dml err message'+d.getMessage());
    }
    catch(Exception e){
    system.debug('here we get generic err message'+e.getMessage());
    }
        //
   // ApexPages.currentPage().getParameters().put('CF00ND0000004qc7b',c.id);
  
    ApexPages.currentPage().getParameters().put('CF00ND0000004qc7b_lkid',c.id);
    //ApexPages.currentPage().getParameters().put('Id',c.Id);
   // ApexPages.currentPage().getParameters().put('ftsIds',ft.id);
    VFC02_FTSGoodwill ctrl =new VFC02_FTSGoodwill(new ApexPages.StandardSetController(addgodwil));
    
    ctrl.initialize();
    ctrl.CalculateSRC();
    ctrl.getEduGoCount();
    ctrl.getDisplay();
    ctrl.getShow();
    ctrl.recalculate();
    ctrl.saveGoodwill();
    ctrl.getGoodwillDetials();
    ctrl.getCaseDetials();
    
   // Test.stopTest();
   // Test.startTest();
    ApexPages.currentPage().getParameters().put('Id',c.Id);
    ApexPages.currentPage().getParameters().put('ftsIds',ft.id);
    VFC02_FTSGoodwill ctrl1 =new VFC02_FTSGoodwill(new ApexPages.StandardSetController(addgodwil));
    
    ctrl1.initialize();
    ctrl1.CalculateSRC();
    ctrl1.getEduGoCount();
    ctrl1.getDisplay();
    ctrl1.getShow();
    ctrl1.recalculate();
    ctrl1.saveGoodwill();
    Test.stopTest();

}
}