/**
* Classe que representa exceções geradas na criação de Test Drive.
* @author Felipe Jesus Silva.
*/
public class VFC58_CreateTestDriveException extends Exception
{
	public VFC58_CreateTestDriveException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}