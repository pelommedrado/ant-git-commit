/**
* @author Sumanth
* @date 16/07/2014
* @description All events on attachments are called in this class.
*/
public with sharing class AttachmentTriggerHandler {       
    public static void onAfterInsert(List<Attachment> attachmentlist){
        User u=[Select Id,Profile.Name,Email from User where ID= :userinfo.getuserid()];
        //Create a task to SRC user when a attachment is inserted by dealer or IKR User
        //Send an email to Dealer when a attachment is inserted by SRC agent
        Rforce_AttachmentUtils_CLS.activitymanagementonattachment(attachmentlist,u);
    } 
    public static void onBeforeInsert(List<Attachment> attachmentlist){
        //When email with one or multiple attachments is sent by customer on a closed case a new related case is created and these attachment are attached to the related case.
        Rforce_AttachmentUtils_CLS.attachmenton_email_for_a_closedcase(attachmentlist);
    }       
}