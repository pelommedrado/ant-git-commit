public with sharing class VFC159_CaseDAO {
    
    private static final VFC159_CaseDAO instance = new VFC159_CaseDAO();
    
    private VFC159_CaseDAO(){}

    public static VFC159_CaseDAO getInstance(){
        return instance;
    }
    
    public Case fetchCaseUsingCaseId(Id CaseId)
    {
        try
        {
            Case c = [SELECT Origin, CaseSubSource__c, Type, SubType__c from Case where Id = :CaseId limit 1];              
            return c;
        }
        catch(QueryException ex)
        {
            system.debug('Error in fetchCaseUsingCaseId: '+ex);
            return null;
        }
    }
}