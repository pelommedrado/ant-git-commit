public class veh_criticClientController {
    
    public String vehId {get;set;}
    
    public veh_criticClientController(ApexPages.StandardController sc) {
        vehId = sc.getId();
    }
    
    public Boolean isCritic {
        get{
            Boolean isCritic = [SELECT CriticalCustomer__c
                               FROM VEH_Veh__c
                               WHERE Id =: vehId].CriticalCustomer__c;
            
            return isCritic;
        }
    }
}