@isTest
public with sharing class Rforce_MyRMyD_VFCx_Test {

//Added for 8.0_SP2:To test Company account Redirection
static testMethod void CheckRenaultNull()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Germany', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();

Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000',AccountBrand__c = 'Renault' , RecordTypeId = RTID, PersEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = null,MYD_Status__c = null,AccountSource = 'Renault' , MyDaciaID__c =null , MyRenaultID__c = null);
insert Acc;  
  
PageReference pageRef = page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('sBrandName','Renault'); 

ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller); 
testextn.sBrandName='Renault';
testextn.sMyRStatus = 'Preregistered';
testextn.sMyDStatus = 'Preregistered';
testextn.sMyRUserName = 'renault@test.com';
testextn.sMyDUserName = 'dacia@test.com';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
testextn.sEmail = 'test@email.com';
testextn.bIsRefreshPage = true;
testextn.sMatchingEmail = Acc.PersEmailAddress__c;
testextn.sMatchingBrandName = Acc.AccountBrand__c;
testextn.sErrorCode = 'WS01MS520';
Test.stopTest();    
}
}
 //static testMethod void CheckDacia()
 static testMethod void CheckRenaultNotNull()
 {
 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyD',MYR_Status__c = 'Activated',MYD_Status__c = 'Activated',AccountSource = 'Dacia',MyDaciaID__c ='test@daciamail.com', MyRenaultID__c = 'test@renaultmail.com');
insert Acc;
  
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault');
Test.setCurrentPage(pageRef);

ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);
testextn.sBrandName='Dacia'; 
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
testextn.sMatchingEmail = Acc.PersEmailAddress__c;
testextn.sMatchingBrandName = Acc.AccountBrand__c;
Myr_ManageAccount_MatchingKeys_Cls myrManageAccount = new Myr_ManageAccount_MatchingKeys_Cls('Renault','addr1@mail.com',Acc.Id);
Myr_ManageAccount_MatchingKeys_Cls.Response myrManageAccountResponse = myrManageAccount.checkEmailAvailability();
myrManageAccountResponse.ErrorCode = 'test';
Test.stopTest();    
}
}

static testMethod void Nullemail()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = null , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = 'Created',MYD_Status__c = 'Created',AccountSource = 'Renault');
insert Acc;
  
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenarioRenaultIDNull()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = 'Preregistered',MYD_Status__c='Preregistered',AccountSource = 'Renault',MyDaciaID__c=null,MyRenaultID__c=null);
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenarioRenaultIDNotNull()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = 'Preregistered',MYD_Status__c='Preregistered',AccountSource = 'Renault',MyDaciaID__c='dacia@test.com',MyRenaultID__c='renault@test.com');
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenarioAllFilled()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = 'Preregistered',AccountSource = 'Renault',MyRenaultID__c='renault@test.com',MyDaciaID__c='dacia@test.com',MYD_Status__c='Preregistered');
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenarioDaciaIDNull()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYR_Status__c = 'Deleted',AccountSource = 'Renault',MyDaciaID__c=null);
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Renault'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenarioDaciaIDNotNull()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYD_Status__c = 'Deleted',AccountSource = 'Renault',MyRenaultID__c=null);
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Dacia'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
static testMethod void checkScenario()
 { 
Country_Info__c ctr = new Country_Info__c (Name = 'UK-IE', Country_Code_2L__c = 'UK', Language__c = 'English',Case_RecordType__c = 'UK_Case_RecType');
insert ctr;   
User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'UK-IE', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');        
System.runAs(usr) {
Test.startTest();       
list<Account> accLst= new list<Account>();
Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, PersEmailAddress__c = 'test@testing.com' , ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Account_Sub_Sub_Source__c = 'MyR',MYD_Status__c = 'Deleted',AccountSource = 'Renault',MyRenaultID__c='renault@test.com');
insert Acc;
PageReference pageRef = Page.Rforce_MyRMyD_Gigya_Page;
pageRef.getParameters().put('sBrandName','Dacia'); 
Test.setCurrentPage(pageRef);

system.currentPageReference().getParameters().put('sBrandName','Renault'); 
 
//pageRef.getParameters().put('Id',Acc.Id);


ApexPages.StandardController controller=new ApexPages.StandardController(Acc);
Rforce_MyRMyD_VFCx  testextn = new Rforce_MyRMyD_VFCx (controller);

testextn.sBrandName='Renault';
testextn.createAccount();
testextn.resetPassword();
testextn.resendActivationEmail();
Test.stopTest();      
}
}
}