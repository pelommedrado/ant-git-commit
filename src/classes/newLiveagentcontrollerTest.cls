@isTest
public class newLiveagentcontrollerTest {
    
    public static testMethod void myTest(){
        
        MyOwnCreation moc = new MyOwnCreation();

        Account conta = moc.criaPersAccount();
        Insert conta;

        Case caso = moc.criaCaso();
        caso.Vehicle_Plate__c = 'DCD8889';
        caso.AccountId = conta.Id;
        Insert caso;

        VEH_Veh__c veiculo = moc.criaVeiculo();
        veiculo.VehicleRegistrNbr__c = 'DCD8889';
        Insert veiculo;
        
        newLiveagentcontroller controller = new newLiveagentcontroller();

        controller.Casesubject = 'assunto';
        controller.cpf = 'cpf';
        controller.email = 'email';
        controller.firstName = 'firstName';
        controller.lastName = 'lastName';
        controller.phone = 'phone';
        controller.placa = 'placa';

        controller.redirect();

        controller.getItems();
        List<SelectOption> selOpts = controller.getItems();
        system.assert(!selOpts.isEmpty());
        
        controller.createOwnerShipRelation(caso);
        
    }

}