/**
*	Class	-	WSC07_MockHttpResponseGenerator
*	Author	-	Subbarao
*	Date	-	13/03/2013
*
*	#01 <Subbarao> <13/03/2013>
*		This class was used to test Http callout.
**/
@isTest
global class WSC07_MockHttpResponseGenerator implements HttpCalloutMock{
	/* This webservice method  called */
	global HTTPResponse respond(HTTPRequest req) {
		
		/* System assert check get method */
        System.assertEquals('GET', req.getMethod());
        
        /* initialize http response object */
        HttpResponse res = new HttpResponse();
       
        /* set body in http response */
        res.setBody('{"requestedURI":"http://br.co.rplug.renault.com/docs","docs":[{"docId":{"program":"X76","phase":"3","modelSpecCode":"KEM","modelKind":"UM","market":"BRESIL","tariffNumber":"9364","tariffAdditiveNumber":"00","commercialKind":"VU","creationTimeStamp":"1356576421925","brand":"APL03","clientId":"TARIF","clientType":"GP","localSemiclair":"KEM"},"doc":"http://br.co.rplug.renault.com/doc/BABg"}],"docgroup":"http://br.co.rplug.renault.com/doc/BBCAAAAq9ihinhlhhh"}');
       
        return res;
	}
}