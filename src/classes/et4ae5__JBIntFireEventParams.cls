/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class JBIntFireEventParams {
    @InvocableVariable( required=true)
    global String ContactKey;
    @InvocableVariable( required=true)
    global String ContactPersonType;
    @InvocableVariable( required=true)
    global String EventDataConfig;
    @InvocableVariable( required=true)
    global String EventDefinitionKey;
    @InvocableVariable( required=true)
    global String OwnerMID;
    @InvocableVariable( required=true)
    global String SalesforceObjectId;
    @InvocableVariable( required=true)
    global String SalesforceObjectName;
    @InvocableVariable( required=true)
    global String VersionNumber;
    global JBIntFireEventParams() {

    }
}
