/**
* Classe que representa os dados de Iten de veículo.
* @author Elvia Serpa.
*/
public with sharing class VFC94_VehicleItemVO 
{
    /* atributos referentes a Grid Vehico */
    public Boolean isCheckVehico {get;set;}
    public String status {get;set;}
    public String idVehico {get;set;}   
    public String id {get;set;}     
    public String chassi {get;set;} 
    public String modelo {get;set;}
    public String versao {get;set;} 
    public Decimal anoFabricacao {get;set;}
    public Decimal anoModelo {get;set;}
    public String cor {get;set;}
    public String tempoEstoque {get;set;}
    public Decimal priceVehicle {get;set;}
    public String formattedPriceVehicle {get;set;}  
    public Boolean CheckVehicodisabled  { get; set; }   
    public String idProduto;
    public String opcionais{ get; set; }
    public String bir{ get; set; }
    
    public VFC94_VehicleItemVO() 
    {
        
    }       
}