public with sharing class ConexaoRenaultBo {

	private static ConexaoRenaultBo classInstance;

  static {
    classInstance = new ConexaoRenaultBo();
  }

  private ConexaoRenaultBo() {}

  public static ConexaoRenaultBo getInstance() {
    return classInstance;
  }

  public void executeFatura2(List<FaturaDealer2__c> faturas) {
    System.debug('executeFatura2()');

		final Integer days = Integer.valueOf(Label.ConexaoRenault_Fatura2_DiasUteis);
		final List<Holiday> holidayList = DataUtils.holidayRecurrenceList();

		for(FaturaDealer2__c fat : faturas) {
      try {
				Date dataRef = fat.Invoice_Date_Mirror__c.date();

				final Date newDate =
					//DataUtils.nextWorkingDay(fat.Invoice_Date_Mirror__c.date(), days, holidayList);
					dataRef.addDays(days);
				fat.VN_Survey_Date__c = newDate;
			} catch(Exception ex) {
				System.debug('Ex:' + ex);
			}
    }

		try {
			Database.update(faturas);

		} catch(Exception ex) {
			System.debug('Ex:' + ex);
		}
  }

  public void executeFatura(List<FaturaDealer__c> faturas) {
    System.debug('executeFatura()');

		final Integer days = Integer.valueOf(Label.ConexaoRenault_Fatura_DiasUteis);
		final List<Holiday> holidayList = DataUtils.holidayRecurrenceList();

		for(FaturaDealer__c fat : faturas) {
      try {
				Date dataRef = fat.NFDTANFI_Mirror__c;
				final Date newDate =
					//DataUtils.nextWorkingDay(fat.NFDTANFI_Mirror__c, days, holidayList);
					dataRef.addDays(days);
				fat.PV_Survey_Date__c = newDate;
			} catch(Exception ex) {
				System.debug('Ex:' + ex);
			}
    }

		try {
			Database.update(faturas);

		} catch(Exception ex) {
			System.debug('Ex:' + ex);
		}
  }

  /*private List<Account> retrieveAccount(Set<String> cpfSet) {
    System.debug('retrieveAccount()');
    List<Account> accList = new List<Account>();

    if(cpfSet.isEmpty()) {
      return accList;
    }

    return [
      SELECT Id, CustomerIdentificationNbr__c FROM Account
      WHERE CustomerIdentificationNbr__c =: cpfSet
    ];
  }

	private Map<String, Account> createAccountMap(List<Account> accList) {
		final Map<String, Account> mapCpfAcc = new Map<String, Account>();
		for(Account acc: accList) {
			mapCpfAcc.put(acc.CustomerIdentificationNbr__c, acc);
		}
		return mapCpfAcc;
	}*/
}