public with sharing class LMT_CampaignsMembersLoadController {
    
    public Id Campanha {get;set;} 
    
    // Seta uma campanha para inicio da página
    public void setCampaign(){
        List<Campaign> campanhaList = [select Id from Campaign where WebToOpportunity__c = false
                                       AND PAActiveShareAllowed__c = true AND Status = 'Planned' limit 1];
        campanha = (!campanhaList.isEmpty()) ? campanhaList.get(0).Id : null ;
    }
    
    //reprocessar todos os de registros
    public void reprocess(){

        Id invalid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Invalid');
        
        String query = 'SELECT Id, AddressOK__c, AfterSales__c, AfterSalesOffer__c, BehaviorCustomer__c, Birthdate__c, BusinessPhone__c, ' +
            'CadastralCustomer__c, CampaignCode__c, City__c, ContemplationLetter__c, Country__c, CPF_CNPJ__c, CurrentMilage__c, ' +
            'CustomerProfile__c, DateOfContemplation__c, Detail__c, Email__c, EmailOK__c, EndDateOfFinanciating__c, ErrorMessage__c, ' +
            'FirstName__c, Home_Phone_Web__c, HomePhone__c, InformationUpdatedInTheLast5Years__c, IsRenaultVehicleOwner__c, LastName__c, ' +
            'LeadSource__c, MarketdatasCluster__c, MobilePhone__c, NUMDBM__c, ParticipatedInCampaigns__c, ParticipateOnOPA__c, ' + 
            'PhoneNumberOK__c, PostalCode__c, PreApprovedCredit__c, PropensityModel__c, RecentBirthdate__c, Related_campaign__c, ' + 
            'RFV__c, State__c, Street__c, Sub_Detail__c, SubSource__c, ValueOfCredit__c, VehicleOfCampaign__c, VehicleOfInterest__c, ' +
            'CPF_and_CodCampaing__c FROM InterfaceCampaignMembers__c WHERE RecordTypeId = \'' + invalid + '\'';        
        
        Database.executeBatch( new CampaignMemberBatch(query), 1000);
        ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.INFO,
              'Processo agendado com sucesso! Aguarde alguns minutos e atualize a página para acompanhar o processamento.');
        ApexPages.addMessage(message);

    }
    
    //deleta todos registros invalidos
    public void limpaInvalidos(){
        Id invalid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Invalid');
        List<InterfaceCampaignMembers__c> imc = [select Id from InterfaceCampaignMembers__c where RecordTypeId =: invalid limit 49900];
        if(!imc.isEmpty() && imc.size() < 9900){
            Database.delete(imc,false);
        }else{
            Database.executeBatch(new deleteInterfaceCampaignMembersBatch('SELECT Id FROM InterfaceCampaignMembers__c WHERE RecordTypeId = \'' + invalid + '\''), 1000);
            String msg = 'Exclusão iniciada com sucesso! Aguarde alguns minutos e atualize a página para acompanhar a solicitação.';          
            sendMessage(ApexPages.severity.INFO, msg);           
        }
    }
    
    //deleta todos registros validos    
    public void limpaValidos(){
        Id valid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Valid');
        List<InterfaceCampaignMembers__c> imc = [select Id from InterfaceCampaignMembers__c where RecordTypeId =: valid limit 49900];
        if(!imc.isEmpty() && imc.size() < 9900){
            Database.delete(imc,false);
        }else{
            Database.executeBatch(new deleteInterfaceCampaignMembersBatch('SELECT Id FROM InterfaceCampaignMembers__c WHERE RecordTypeId = \'' + valid + '\''), 1000);         
            String msg = 'Exclusão iniciada com sucesso! Aguarde alguns minutos e atualize a página para acompanhar a solicitação.';            
            sendMessage(ApexPages.severity.INFO, msg);
        }
    }
    
    // deleta todos Leads e Membros de uma campanha
    public PageReference deleteMembers(){
        String Error;
        List<Campaign> campaignList = [select Id,WebToOpportunity__c,PAActiveShareAllowed__c,Status from Campaign where Id =: campanha];
        
        if(!campaignList.isEmpty() && campaignList.get(0).WebToOpportunity__c == true){
            error = 'Não é possível excluir campanha com: WebToOpportunity ativo';
            sendMessage(ApexPages.Severity.Error,error);
            return null;
        }
        if(!campaignList.isEmpty() && campaignList.get(0).PAActiveShareAllowed__c == false){
            error = 'Não é possível excluir campanha com: "Permite compartilhar com PA" desativado';
            sendMessage(ApexPages.Severity.Error,error);
            return null;
        }
        if(!campaignList.isEmpty() && !campaignList.get(0).Status.Equals('Planned')){
            error = 'Não é possível excluir campanha com: Status diferente "Planejado"';
            sendMessage(ApexPages.Severity.Error,error);
            return null;
        }

        if(!campaignList.isEmpty()){
            
            List<Lead> leadList = [Select Id from Lead where Id in (SELECT LeadId FROM CampaignMember WHERE CampaignId =: Campanha) limit 49900];
            
            if(!leadList.isEmpty() && leadList.size() <= 9900)
                Database.Delete(leadList);
            else{
                Database.executeBatch(new deleteLeadsBatch('SELECT LeadId FROM CampaignMember WHERE CampaignId = \'' + Campanha + '\''), 1000);                
                String msg = 'Exclusão iniciada com sucesso! Aguarde alguns minutos e atualize a página para acompanhar a solicitação.';               
                sendMessage(ApexPages.severity.INFO, msg);
            }
        }
        return null;
    }   
    
    public void sendMessage(ApexPages.severity severity, String value){
        ApexPages.Message message = new Apexpages.Message(severity, value );
        ApexPages.addMessage(message);
    }
    
    //deleta até 9999 leads e Membros de Campanha selecionados
    public void deleteFew(){
        List<Campaign> campaignId = [select Id from Campaign where WebToOpportunity__c = false
                                       AND PAActiveShareAllowed__c = true AND Status = 'Planned'];
        if(!campaignId.isEmpty()){
            String passedParams = ApexPages.currentPage().getParameters().get('param');
            List<Id> CMIds = passedParams.split(',');
            List<Lead> LeadIds = [select Id from Lead where Id in (Select LeadId from CampaignMember where Id in: CMIds) limit 9999];
            Database.delete(LeadIds,false);
        }else{
            ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.INFO,
                'Esta Campanha está com o campo: "Permite Compartilhar PA" desativado ou "Web to Opportunity" ativado, não é permitida a exclusão.');
            ApexPages.addMessage(message);
        }
    }
    
    //Retorna a quantidade de registros não processados
    public String getUnproccess(){
        Id unprocessedId = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Unprocessed');
        Integer qtde = [Select count() from InterfaceCampaignMembers__c where RecordTypeId =: unprocessedId limit 15000];
        String msg = String.valueOf(qtde);
        if(qtde == 15000)
            msg = '15 mil ou mais registros'; 
        return msg;
    }
    
    //Retorna a quantidade de registros validos
    public String getValids(){
        Id validId = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Valid');
        Integer qtde = [Select count() from InterfaceCampaignMembers__c where RecordTypeId =: validId limit 15000];
        String msg = String.valueOf(qtde);
        if(qtde == 15000)
            msg = '15 mil ou mais registros'; 
        return msg;
    }
    
    //Retorna a quantidade de registros invalidos
    public String getInvalids(){
        Id invalidId = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Invalid');
        Integer qtde = [Select count() from InterfaceCampaignMembers__c where RecordTypeId =: invalidId limit 15000];
        String msg = String.valueOf(qtde);
        if(qtde == 15000)
            msg = '15 mil ou mais registros'; 
        return msg;
    }
    
    // Executa o Batch para processar os Membros de Campanha
    public void executeBatch(){
        
        Id unprocessedRecordType = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Unprocessed');
        
        String query = 'SELECT Id, Voucher_Protocol__c, LeadRecordType__c, AddressOK__c, AfterSales__c, AfterSalesOffer__c, BehaviorCustomer__c, Birthdate__c, BusinessPhone__c, ' +
            'CadastralCustomer__c, CampaignCode__c, City__c, ContemplationLetter__c, Country__c, CPF_CNPJ__c, CurrentMilage__c, ' +
            'CustomerProfile__c, DateOfContemplation__c, Detail__c, Email__c, EmailOK__c, EndDateOfFinanciating__c, ErrorMessage__c, ' +
            'FirstName__c, Home_Phone_Web__c, HomePhone__c, InformationUpdatedInTheLast5Years__c, IsRenaultVehicleOwner__c, LastName__c, ' +
            'LeadSource__c, MarketdatasCluster__c, MobilePhone__c, NUMDBM__c, ParticipatedInCampaigns__c, ParticipateOnOPA__c, ' + 
            'PhoneNumberOK__c, PostalCode__c, PreApprovedCredit__c, PropensityModel__c, RecentBirthdate__c, Related_campaign__c, ' + 
            'RFV__c, State__c, Street__c, Sub_Detail__c, SubSource__c, ValueOfCredit__c, VehicleOfCampaign__c, VehicleOfInterest__c, ' +
            'CPF_and_CodCampaing__c FROM InterfaceCampaignMembers__c WHERE RecordTypeId = \'' + unprocessedRecordType + '\'';        
        
        Database.executeBatch( new CampaignMemberBatch(query), 1000);
        ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.INFO,
              'Processo agendado com sucesso! Aguarde alguns minutos e atualize a página para acompanhar o processamento.');
        ApexPages.addMessage(message);
    }
    
    // recupera Id do documento padrão do CSV
    public Id getCsvPadraoId(){
        List<Document> document = [select Id from Document where Name =: Label.rotuloCSV];
        if(!document.isEmpty()){
            Id docId = document.get(0).Id;
            return docId;
        }else{
            ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.ERROR,
                'Não existe documento com o nome: ' + Label.rotuloCSV);
            ApexPages.addMessage(message);
            return null;
        }
    }
    
    // recupera Id do relatório de Não Processados
    public Id getRelatorioUnproccessedId(){
        List<Report> report = [select Id from Report where Name =: Label.relatorio_unproccessed_icm];
        if(!report.isEmpty()){
            Id repId = report.get(0).Id;
            return repId;
        }else{
            ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.ERROR,
                'Não existe relatório com o nome: ' + Label.relatorio_unproccessed_icm);
            ApexPages.addMessage(message);
            return null;
        }
    }
    
    // recupera Id do relatório de Validos
    public Id getRelatorioValidId(){
        List<Report> report = [select Id from Report where Name =: Label.relatorio_valid_icm];
        if(!report.isEmpty()){
            Id repId = report.get(0).Id;
            return repId;
        }else{
            ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.ERROR,
                'Não existe relatório com o nome: ' + Label.relatorio_valid_icm);
            ApexPages.addMessage(message);
            return null;
        }
    }
    
    // recupera Id do relatório de Invalidos
    public Id getRelatorioInvalidId(){
        List<Report> report = [select Id from Report where Name =: Label.relatorio_invalid_icm];
        if(!report.isEmpty()){
            Id repId = report.get(0).Id;
            return repId;
        }else{
            ApexPages.Message message = new Apexpages.Message(ApexPages.Severity.ERROR,
                'Não existe relatório com o nome: ' + Label.relatorio_invalid_icm);
            ApexPages.addMessage(message);
            return null;
        }
    }
    
    // recupera as opções de Campanhas
    public List<SelectOption> getCampaignsName(){
        List<Campaign> lsCampaign = [select Id, Name from Campaign where WebToOpportunity__c = false
                                       AND PAActiveShareAllowed__c = true AND Status = 'Planned'];
        List<SelectOption> options = new List<SelectOption>();
        for(Campaign c: lsCampaign){
            options.add(new SelectOption(c.Id,c.Name));
        }
        return options;
    }
    
    // carrega a tabela de Membros de Campanha
    public List<CampaignMember> getListCampaignMembers(){
        List<CampaignMember> lsCM = [Select Id, Name, CPF_CNPJ__c, DBMCampaignCode__c 
                                     from CampaignMember where CampaignId =: Campanha limit 999];
        return lsCM;
    }
    
    //metodo para usar o CSV passado pelo Batch
    public List<InterfaceCampaignMembers__c> importCSVFile(List<InterfaceCampaignMembers__c> startList){
        
        // Nova Lista para receber imc sem duplicados
        List<InterfaceCampaignMembers__c> finalList = new List<InterfaceCampaignMembers__c>();
        
        //Lista de leads para inserir
        List<Lead> lsLead = new List<Lead>();
        
        //Set de códigos de campanhas
        Set<String> setCodCampanhas = new Set<String>();
        
        //Set de cpfs dos Leads
        Set<String> CpfsDosLeadsCSV = new Set<String>(); 
        
        //Set para não repetir membros de campanha da lista
        Set<String> CpfCodCampanhaDaLista = new Set<String>();
        
        //Set para não repetir membros de campanha do BD
        Set<String> setCpfsCodCampanhasDoBD = new Set<String>();
        
        //Mapa para pegar os ids das campanhas com base no código
        Map<String,Campaign> mapCampaigns = new Map<String,Campaign>();

        //lista de Membros de campanhas para inserir
        List<CampaignMember> lsMembrosCampanhas = new List<CampaignMember>();
        
        Id invalid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Invalid');
        Id valid = Utils.getRecordTypeId('InterfaceCampaignMembers__c', 'Valid');
              
        //Para cada linha recupera as campanhas para inserir no mapa
        for(InterfaceCampaignMembers__c icm: startList){
            
            //trata campo obrigatorio de Campanha
            if(String.isBlank(icm.CampaignCode__c))
                msgErroCampoObrigatorio(icm, 
                                        Schema.InterfaceCampaignMembers__c.fields.CampaignCode__c.getDescribe().getLabel());
            
            //trata campo obrigatorio de CPF
            if(String.isBlank(icm.CPF_CNPJ__c))
                msgErroCampoObrigatorio(icm, Schema.InterfaceCampaignMembers__c.fields.CPF_CNPJ__c.getDescribe().getLabel());
            
            //pega os codigos de campanhas do CSV;
            if(String.isNotBlank(icm.CampaignCode__c))
                setCodCampanhas.add(icm.CampaignCode__c);

            //pega os cpfs dos leads do CSV;
            if(String.isNotBlank(icm.CPF_CNPJ__c))
                CpfsDosLeadsCSV.add(icm.CPF_CNPJ__c);
            
            //Para evitar duplicidade de leads
            if( !CpfCodCampanhaDaLista.contains(icm.CPF_and_CodCampaing__c) ){
                CpfCodCampanhaDaLista.add(icm.CPF_and_CodCampaing__c);
                finalList.add(icm);
            }else{
                duplicatedMemberInList(icm);
            }
            
        }      
        
        //busca as campanhas do set
        List<Campaign> lsCampaign = [select Id, DBMCampaignCode__c, Set_values_for_Origins__c, Source__c, 
                                     Sub_Source__c, Detail__c,Sub_Detail__c
                                     from Campaign where DBMCampaignCode__c in: setCodCampanhas];
        
        system.debug('lsCampaign = '+lsCampaign);
        
        //mapa para recuperar os Ids das campanhas
        if(!lsCampaign.isEmpty()){
            for(Campaign c : lsCampaign){
                mapCampaigns.put(c.DBMCampaignCode__c, c);
            }
        }
        
        /****************************** Cod original *****************************************
                
        //busca os Membros de Campanha com CPF existentes no BD  ******* cod original **********
        List<CampaignMember> MembrosCampanhaDoBD = [select Id, CPF_CNPJ__c, CampaignId, DBMCampaignCode__c
                                                    from CampaignMember 
                                                    where CPF_CNPJ__c in: CpfsDosLeadsCSV
                                                    and CampaignId in: lsCampaign];

		if(!MembrosCampanhaDoBD.isEmpty()){
            for(CampaignMember cm: MembrosCampanhaDoBD)
                setCpfsCodCampanhasDoBD.add(cm.CPF_CNPJ__c + cm.DBMCampaignCode__c);
        }
		******************************************************************************************/

        
        /************************* alternativa de ajuste *************************************/

        //lista para retornar todos os cpfs de membros de campanha do BD que estão nas campanhas do CSV
        List<CampaignMember> listCamMember = [Select CPF_CNPJ__c,DBMCampaignCode__c from CampaignMember where CampaignId in: lsCampaign];
        
        //verifica se os cpfs da lista estão no BD, só insere se conter no BD(Para msg de erro de Membro de Campanha duplicado p/ campanha)
        for(CampaignMember cm: listCamMember){
            if(CpfCodCampanhaDaLista.contains(cm.CPF_CNPJ__c + cm.DBMCampaignCode__c)){
                setCpfsCodCampanhasDoBD.add(cm.CPF_CNPJ__c + cm.DBMCampaignCode__c);
            }
        }
        
        /*****************************************************************************************/

        //cria os leads e add na lista para inserir    
        lsLead.addAll(createdLead(finalList,mapCampaigns,setCpfsCodCampanhasDoBD));  
        
        List<Database.SaveResult> results = Database.Insert(lsLead,false);
        List<InterfaceCampaignMembers__c> lsICM = new List<InterfaceCampaignMembers__c>();
        for(Integer i=0; i < results.size(); i++){
            Database.SaveResult result = results[i];
            if(!result.isSuccess()){
                InterfaceCampaignMembers__c icm = new InterfaceCampaignMembers__c(
                    Id = lsLead[i].InterfaceCampaignMembersId__c,
                    RecordTypeId = invalid,
                    ErrorMessage__c = String.valueOf(result.getErrors())
                );
                lsICM.add(icm);
            }
        }
        
        //atualiza registros de Interface Campaign Members
        //Database.update(lsICM,false);

        //add Membros de Campanha
        lsMembrosCampanhas.addAll( addCampaignMember(lsLead) );
        try{
            Database.Insert(lsMembrosCampanhas,false);
        }catch(Exception e){
            system.debug('Erro ao inserir Membro de Campanha: '+e);
        }
        
        return lsICM;
        
        
    }
    
    
    /*
    *
    *
    * ********* Métodos encapsulados ******
    * 
    * 
    */
        
    private List<Lead> createdLead(List<InterfaceCampaignMembers__c> lsIcm, Map<String,Campaign> mapCampaigns, Set<String> setCpfsCodCampanhasDoBD){
        List<Lead> lsLead = new List<Lead>();
        
        //Para cada item da lista cria um lead
        for(InterfaceCampaignMembers__c icm: lsIcm){
            
            if(icm.CampaignCode__c != null && icm.CPF_CNPJ__c != null){
                
                boolean validCurrentMilage,validNUMDBM,validValueOfCredit,validBirthdate,validCampanha;
                boolean validDateOfContemplation,memberExist,validEndDateOfFinanciating,validLastName;
                
                // faz as validações antes de criar o Lead
                validCampanha = isvalidCampanha(mapCampaigns,icm);
                memberExist = existMemberCampaing(setCpfsCodCampanhasDoBD,icm);
                validCurrentMilage = 
                    isValidoDecimal(Schema.InterfaceCampaignMembers__c.fields.CurrentMilage__c.getDescribe().getLabel(),
                                    icm.CurrentMilage__c,icm);
                validNUMDBM = 
                    isValidoDecimal(Schema.InterfaceCampaignMembers__c.fields.NUMDBM__c.getDescribe().getLabel(),
                                    icm.NUMDBM__c,icm);
                validValueOfCredit = 
                    isValidoDecimal(Schema.InterfaceCampaignMembers__c.fields.ValueOfCredit__c.getDescribe().getLabel(),
                                    icm.ValueOfCredit__c,icm);
                validBirthdate = 
                    isValidaDate(Schema.InterfaceCampaignMembers__c.fields.Birthdate__c.getDescribe().getLabel(),
                                 icm.Birthdate__c,icm);
                validDateOfContemplation = 
                    isValidaDate(Schema.InterfaceCampaignMembers__c.fields.DateOfContemplation__c.getDescribe().getLabel(),
                                 icm.DateOfContemplation__c,icm);
                validEndDateOfFinanciating = 
                    isValidaDateTime(Schema.InterfaceCampaignMembers__c.fields.EndDateOfFinanciating__c.getDescribe().getLabel(),
                                     icm.EndDateOfFinanciating__c,icm);
                validLastName = 
                    isValidLastName(Schema.InterfaceCampaignMembers__c.fields.LastName__c.getDescribe().getLabel(), icm);
                
                // Se todas as validações passaram, logo cria o Lead
                if(!memberExist && validCurrentMilage && validNUMDBM && validValueOfCredit && validBirthdate && 
                   validDateOfContemplation && validEndDateOfFinanciating && validLastName && validCampanha){
                       
                       // Se as validações estão ok, marca o registro como valido
                       icm.RecordTypeId = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Valid');
                       
                       //retorna a Campanha relacionada para origens e campo related Campaign
                       Campaign c = mapCampaigns.get(icm.CampaignCode__c);
                       
                       Lead newLead = new Lead(
                           AddressOK__c = icm.AddressOK__c,
                           AfterSales__c = icm.AfterSales__c,
                           AfterSalesOffer__c = icm.AfterSalesOffer__c,
                           BehaviorCustomer__c = icm.BehaviorCustomer__c,
                           BusinessPhone__c = icm.BusinessPhone__c,
                           CadastralCustomer__c = icm.CadastralCustomer__c,
                           City = icm.City__c,
                           Related_campaign__c = c.Id,
                           ContemplationLetter__c = icm.ContemplationLetter__c,
                           Country__c = icm.Country__c,
                           CPF_CNPJ__c = icm.CPF_CNPJ__c,
                           CurrentMilage__c = String.isNotBlank(icm.CurrentMilage__c) ? Decimal.valueOf(icm.CurrentMilage__c) : null,
                           CustomerProfile__c = icm.CustomerProfile__c,
                           Email = icm.Email__c,
                           EmailOK__c = icm.EmailOK__c,
                           FirstName = icm.FirstName__c,
                           Home_Phone_Web__c = icm.Home_Phone_Web__c,
                           HomePhone__c = icm.HomePhone__c,
                           InformationUpdatedInTheLast5Years__c = icm.InformationUpdatedInTheLast5Years__c,
                           IsRenaultVehicleOwner__c = icm.IsRenaultVehicleOwner__c,
                           LastName = icm.LastName__c,
                           MarketdatasCluster__c = icm.MarketdatasCluster__c,
                           MobilePhone = icm.MobilePhone__c,
                           Mobile_Phone__c = icm.MobilePhone__c,
                           NUMDBM__c = String.isNotBlank(icm.NUMDBM__c) ? Decimal.valueOf(icm.NUMDBM__c) : null,
                           ParticipatedInCampaigns__c = icm.ParticipatedInCampaigns__c,
                           ParticipateOnOPA__c = icm.ParticipateOnOPA__c,
                           PhoneNumberOK__c = icm.PhoneNumberOK__c,
                           PostalCode = icm.PostalCode__c,
                           PreApprovedCredit__c = icm.PreApprovedCredit__c,
                           PropensityModel__c = icm.PropensityModel__c,
                           RecentBirthdate__c = icm.RecentBirthdate__c,
                           RFV__c = icm.RFV__c,
                           State = icm.State__c,
                           Street = icm.Street__c,
                           ValueOfCredit__c = String.isNotBlank(icm.ValueOfCredit__c) ? Decimal.valueOf(icm.ValueOfCredit__c) : null,
                           VehicleOfCampaign__c = icm.VehicleOfCampaign__c,
                           VehicleOfInterest__c = icm.VehicleOfInterest__c,
                           Voucher_Protocol__c = icm.Voucher_Protocol__c,
                           RecordTypeId = icm.LeadRecordType__c
                       );
                       
                       /*trata os campo de data de aniversario
                       String[] Birthdate = String.isNotBlank(icm.Birthdate__c) ? icm.Birthdate__c.split('-') : null;
                       newLead.Birthdate__c = Birthdate != null ? Date.valueOf(Birthdate[0]+'-'+Birthdate[1]+'-'+Birthdate[2]): null;*/
                       newLead.Birthdate__c = (icm.Birthdate__c != null) ? Date.valueOf(icm.Birthdate__c) : null;
                       
                       /*trata o campo data de contemplação
                       String[] DateOfContemplation = String.isNotBlank(icm.DateOfContemplation__c) ? icm.DateOfContemplation__c.split('-') : null;
                       newLead.DateOfContemplation__c = DateOfContemplation != null ? 
                           Date.valueOf(DateOfContemplation[0]+'-'+DateOfContemplation[1]+'-'+DateOfContemplation[2]) : null;*/
                       newLead.DateOfContemplation__c = (icm.DateOfContemplation__c != null) ? Date.valueOf(icm.DateOfContemplation__c) : null;
                       
                       //trata campo de datetime
                       String subStringEndDateOfFinanciating = String.isNotBlank(icm.EndDateOfFinanciating__c) ?
                           icm.EndDateOfFinanciating__c.substring(0,10) : null;
                       String[] EndDateOfFinanciating = subStringEndDateOfFinanciating != null ? subStringEndDateOfFinanciating.split('-') : null;
                       newLead.EndDateOfFinanciating__c = EndDateOfFinanciating != null ? 
                           Date.valueOf(EndDateOfFinanciating[0]+'-'+EndDateOfFinanciating[1]+'-'+EndDateOfFinanciating[2]) : null;
                       
                       //trata as origens dos Leads
                       if(c.Source__c != null && String.isNotBlank(c.Source__c)){
                           newLead.LeadSource = c.Source__c;
                           newLead.SubSource__c = c.Sub_Source__c;
                           newLead.Detail__c = c.Detail__c;
                           newLead.Sub_Detail__c = c.Sub_Detail__c;
                       }else{
                           newLead.LeadSource = icm.LeadSource__c;
                           newLead.SubSource__c = icm.SubSource__c;
                           newLead.Detail__c = icm.Detail__c;
                           newLead.Sub_Detail__c = icm.Sub_Detail__c;
                       }
                       
                       //salva o Id do registro de Interface Campaigns Members no Lead
                       newLead.InterfaceCampaignMembersId__c = icm.Id;
                       
                       //add na lista
                       lsLead.add(newLead);
                   }
            }
        }
        return lsLead;
    }
    
    //Add Membros de Campanha
    public List<CampaignMember> addCampaignMember(List<Lead> lsLead){      
        List<CampaignMember> lsMembrosCampanhas = new List<CampaignMember>();
        
        //adiciona cada lead as campanhas do CSV
        for(Lead lead: lsLead){
            CampaignMember cm = new CampaignMember(
                CampaignId = lead.Related_campaign__c,
                LeadId = lead.Id,
                Currencyisocode = 'BRL',
                Status = 'Sent',
                Source__c = lead.LeadSource,
                SubSource__c = lead.SubSource__c,
                Detail__c = lead.Detail__c,
                Sub_Detail__c = lead.Sub_Detail__c
            );
            lsMembrosCampanhas.add(cm);
        }
        return lsMembrosCampanhas;
    }
    
    // Verifica se existe campanha referente ao codigo 
    public boolean isValidCampanha(map<String,Campaign> mapCampaigns, InterfaceCampaignMembers__c icm ){
        if( mapCampaigns.isEmpty() || mapCampaigns.get(icm.CampaignCode__c) == null){
            String temp  = 'Não existe campanha para este código: ' + icm.CampaignCode__c + ';';
            addMsg(icm, temp);
            return false;
        }else{
            return true;
        }
    }
    
    // verifica se Sobrenome não é nulo
    public boolean isValidLastName(String Campo, InterfaceCampaignMembers__c icm ){
        if(icm.LastName__c == null){
            msgErroCampoObrigatorio( icm, Campo);
            return false;
        }else
            return true;  
    }
    
    //verifica se já existe cpf e codigo de campanha na lista
    public void duplicatedMemberInList(InterfaceCampaignMembers__c icm){
        String temp  = 'Dados duplicados no Batch: Este CPF/CNPJ: ' + icm.CPF_CNPJ__c + ' está duplicado para a campanha: ' + icm.CampaignCode__c + ' ;';
        addMsg(icm, temp);  
    }
    
    //verifica se existe membro de campanha no BD
    public boolean existMemberCampaing(Set<String> setCpfsCodCampanhasDoBD, InterfaceCampaignMembers__c icm){
        if(setCpfsCodCampanhasDoBD.contains(icm.CPF_CNPJ__c + icm.CampaignCode__c)){
            erroMembroCampanhaExistente(icm);
            return true;
        }else
            return false;
    }
    
    //verifica se o campo é numerico
    public boolean isValidoDecimal(String campo, String valor, InterfaceCampaignMembers__c icm){
        
        if( valor == null)
            return true;
        
        Pattern myPattern = Pattern.Compile('\\d+');
        Matcher myMatcher = myPattern.matcher(valor);
        
        if(myMatcher.Matches())
            return true;
        else{
            String temp  = 'O conteúdo do campo ' + campo + ' recebe apenas números, dado recebido: ' + valor + ';';
            addMsg(icm, temp);
            return false;
        } 
    }
    
    //verifica se o campo de data esta no formato valido
    public boolean isValidaDate(String campo, String valor, InterfaceCampaignMembers__c icm){
        
        if(valor == null)
            return true;
        
        Pattern myPattern = Pattern.compile('(19|20)\\d\\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])');
        Matcher myMatcher = myPattern.matcher(valor);
        
        if(myMatcher.matches())
            return true;
        else{
            String temp  = 'O conteúdo do campo ' + campo + ' não está no formato de data válido, dado recebido: ' + valor + ';';
            addMsg(icm, temp);
            return false;
        }
    }
    
    //verifica se o campo de datatime esta no formato correto
    public boolean isValidaDateTime(String campo, String valor, InterfaceCampaignMembers__c icm){
        if(valor == null)
            return true;
        
        Pattern myPattern = Pattern.compile('(19|20)\\d\\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])\\s\\d\\d:\\d\\d:\\d\\d');
        Matcher myMatcher = myPattern.matcher(valor);
        
        if(myMatcher.matches())
            return true;
        else{
            String temp  = 'O conteúdo do campo ' + campo + ' não está no formato de data válido, dado recebido: ' + valor + ';';
            addMsg(icm, temp);
            return false;
        }
    }
    
    //salva mensagem de erro para membro existente no BD
    public void erroMembroCampanhaExistente(InterfaceCampaignMembers__c icm){
        String temp  = 'Este CPF/CNPJ de Membro de Campanha: ' + icm.CPF_CNPJ__c + ' já existe para a Campanha: ' + icm.CampaignCode__c + ';';
        addMsg(icm, temp);
    }
    
    //salva mensagem de erro para campo obrigatorio
    public void msgErroCampoObrigatorio(InterfaceCampaignMembers__c icm, String campo) {
        String temp = 'Conteúdo não informado para campo obrigatório: ' + campo + ';';
        addMsg(icm, temp);
    }
    
    // cria a mensagem de erro
    public void addMsg(InterfaceCampaignMembers__c icm, String msg) {
        if(icm.ErrorMessage__c == null) {
            icm.ErrorMessage__c = '';
        }
        icm.RecordTypeId = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Invalid');
        icm.ErrorMessage__c = icm.ErrorMessage__c + msg;
    }
    
}