/**
* Classe que representa exceções geradas na atualização de orçamento.
* @author Christian Ranha.
*/
public with sharing class VFC92_UpdateQuoteException extends Exception {

	public VFC92_UpdateQuoteException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}