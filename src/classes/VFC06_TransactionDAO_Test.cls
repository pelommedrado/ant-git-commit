/**
	Class   -   VFC06_TransactionDAO_Test
    Author  -   RameshPrabu
    Date    -   06/10/2012
    
    #01 <RameshPrabu> <06/10/2012>
        Created this class using test for VFC06_TransactionDAO.
**/
@isTest
private class VFC06_TransactionDAO_Test {

    static testMethod void VFC06_TransactionDAO_Test() {
        // TO DO: implement unit test
        
        //RCM_Recommendation__c[] lstrecommendation = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRecommendationObject();
        List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        List<TST_Transaction__c> lstTransactionsInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToTransactionObject();
		//system.debug('lstTransactionsInsert...' +lstTransactionsInsert);
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
		Test.startTest();
        List<TST_Transaction__c> lstTransactions = VFC06_TransactionDAO.getInstance().findTransactionRecords(lstTransactionsInsert[0].Lead__c);
        Test.stopTest();
        system.debug('lead Details...' +lstTransactions);
        System.assert(lstTransactions.size() > 0);
        TST_Transaction__c Transactions = VFC06_TransactionDAO.getInstance().findTransactionById(lstTransactionsInsert[0].Id);
        Id TransactionId = VFC06_TransactionDAO.getInstance().findMaxTransactionByAccountId(lstAccounts[0].Id);
    }
}