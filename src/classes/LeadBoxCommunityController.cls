global with sharing class LeadBoxCommunityController {
    
    //private Id accDealerId 				{ get;set; }
    public List< ItemOpp > oppList 		{ get;set; }
    
    public List<SelectOption> campanhaSelectOption 			{ get;set; }
    public List<SelectOption> concessionariaSelectOption 	{ get;set; }
    
    public String campanhaFilter 		{ get;set; }
    public String concessionariaFilter 	{ get;set; }
    public String oppFilter {get;set;}
    
    public LeadBoxCommunityController() {
        init();        
        loadOppMap();
    }

    private void init() {
        this.oppFilter = '0124E0000004ZhL';
        /*String userBIR = [
            SELECT BIR__c 
            FROM User 
            WHERE Id =: Userinfo.getUserId()
        ].BIR__c;
        
        accDealerId = [
            SELECT Id 
            FROM Account
            WHERE RecordType.DeveloperName = 'Network_Site_Acc'
            and IDBIR__c =: userBIR
        ].Id;*/
        
        List<Opportunity> oList = [
            SELECT Campaign_Name__c, Dealer__c
            FROM Opportunity 
            //WHERE Dealer__c =: accDealerId  
            LIMIT 999
        ];
        
        loadCampanha(oList);
        loadConcessionaria(oList);
    }   
    
    private void loadConcessionaria(List<Opportunity> oList) {
        List<String> accIdList = new List<String>();
        for(Opportunity op: oList) {
            accIdList.add(op.Dealer__c);
        }
        
        List<Account> accList = [
            SELECT Id, Name
            FROM Account
            WHERE id=: accIdList
            ORDER BY Name LIMIT 999
        ];
        
        concessionariaSelectOption = new List<SelectOption>();
        for(Account acc : accList) {
            concessionariaSelectOption.add( new SelectOption( acc.Id, acc.Name ) );
        }
    }
    
    private void loadCampanha(List<Opportunity> oList) {
        Set<String> campaignNameSet = new Set<String>();
        for(Opportunity op: oList) {
            if(!String.isEmpty(op.Campaign_Name__c)) campaignNameSet.add( op.Campaign_Name__c );
        }
        List< String > campaignNameList = new List< String >( campaignNameSet );
        campaignNameList.sort();
        
        campanhaSelectOption = new List<SelectOption>();
        for(String campaignName : campaignNameList ) campanhaSelectOption.add( new SelectOption( campaignName, campaignName ) );
    }
    
    public void loadOppMap() {
        List<Opportunity> oList = Database.query(
            'SELECT Id, Name, CreatedDate, ReasonLossCancellation__c,' + 
            'StageName, Vehicle_of_Interest__c, VehicleInterest__c, Dealer__c,' + 
            'OpportunitySource__c, Campaign_Name__c, Seller__c, OwnerId ' +
            
            'FROM Opportunity ' +
            'WHERE StageName IN (\'Identified\',\'In Attendance\') ' +
            (!String.isBlank(campanhaFilter) ? 'AND Campaign_Name__c = \'' + campanhaFilter + '\' ' : '') +
            (!String.isBlank(oppFilter) ? 'AND RecordTypeId = \'' + oppFilter + '\' ' : '') +
            (!String.isBlank(concessionariaFilter) ? 'AND Dealer__c = \'' + concessionariaFilter + '\' ' : '') +
            //'AND Dealer__c = \'' + accDealerId + '\' ' +
            'ORDER BY CreatedDate ASC LIMIT 999'
        );
        
        oppList = new List<ItemOpp>();
        
        Map<Id, List<Contact>> mapCon = new Map<Id, List<Contact>>();
        for(Opportunity op : oList) {
            ItemOpp iOpp = new ItemOpp();
            iOpp.opp = op;
            
            oppList.add(iOpp);
            
            if(!String.isEmpty(op.Dealer__c)) {
                mapCon.put(op.Dealer__c, new List<Contact>());    
            }
        }
        
        System.debug('mapCon:' + mapCon);
        
        List<Contact> contactList = [
            SELECT Id, Name, AccountId 
            FROM Contact
            WHERE AccountId=: mapCon.keySet() AND Available_to_SFA__c = 'Available'
        ];
        
        for(Contact con : contactList) mapCon.get(con.AccountId).add(con);
        
        for(ItemOpp item : oppList) {
            List<Contact> conList = mapCon.get(item.opp.Dealer__c);
            
            if(conList != null) {
                for(Contact c : conList) item.vendedor.add(new SelectOption(c.Id, c.Name));
            }
        }
    }
    
    @RemoteAction
    global static String atualizar(List<String> valueList) {
        System.debug('Iniciando a atualizacao dos valores');
        
        try {
            
            List<Opportunity> oppList = new List<Opportunity>();
            
            Map<Id, List<Opportunity>> mapOpp = new Map<Id, List<Opportunity>>();
            
            for(String value: valueList) {
                System.debug('Value:' + value);
                
                List<String> line = value.split(';');
                
                System.debug('Line: ' + line);
                
                String id 		= line.get(0);
                String vendedor = line.get(1);
                String motivo 	= line.get(2);
               	String status 	= line.get(3);
                
                Opportunity op = new Opportunity();
                op.Id 			= id;
                op.StageName 	= status;
                
                // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
                op.DateTimeSellerUpdatedStageOpp__c = System.now();
                
                if(status == 'Lost' && !String.isEmpty(motivo)) op.ReasonLossCancellation__c = motivo;
                
                if(!String.isEmpty(vendedor)) {
                    op.Seller__c = vendedor;
                    
                    List<Opportunity> ls = mapOpp.get(vendedor);
                    if(ls == null) mapOpp.put(vendedor, ls = new List<Opportunity>());
                    
                    ls.add(op);
                }
                
                oppList.add(op);
            }
            
            List<Contact> listContact = [
                SELECT Id, Email
                FROM Contact
                WHERE Id =: mapOpp.keySet()
            ];
            
            for(Contact ct : listContact) {
                List<Opportunity> ls = mapOpp.get(ct.Id);
                for(Opportunity o : ls) o.EmailSellerNotSFA__c = ct.Email;
            }
            
            UPDATE oppList;
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex.getMessage());
            
            return 'false';
        }
        
        return 'true';
    }
        
    public class ItemOpp {
        public Opportunity opp { get;set; }
        public List<SelectOption> vendedor { get;set; }
        public ItemOpp() {
            vendedor = new List<SelectOption>();
        }
    }
}