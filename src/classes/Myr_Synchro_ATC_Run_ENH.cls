public class Myr_Synchro_ATC_Run_ENH { 
  /*****************************************************************************************
  Name    : Myr_Synchro_ATC_Run_ENH
  Desc    : This class is created for supporting changes in the class "Myr_Synchro_ATC_Run"
  Author  : Satyajit Singh (AtoS)
  Project : MyRenault
  
  ******************************************************************************************/
  // This function is implemented for retrieving customer and VIN response status message
    public Static String responseStatusMessage(String msgValInXML) {
        String msgToDisplay = '';
        try {           
            Dom.Document doc = new Dom.Document();
            doc.load(msgValInXML);
            Dom.XMLNode root = doc.getRootElement();
            msgToDisplay = root.getChildElement(System.label.Myr_Synchro_ATC_Root_Elt, null).getText();         
        }
        catch (Exception e) {
            msgToDisplay = '';
        }
        return msgToDisplay; 
    }

    // This function will be used for making Synchro_ATC btn invisible under Vehicle section if Account is not activated
    public Static void Myr_Call_Visibilty_Fn01(Myr_Synchro_ATC_Run.Page_Handler P_H,Myr_Synchro_ATC_Run.Line_Handler myH){
        if(P_H.MYR_Status != 'Activated' && P_H.MYR_Status != ''){        
            myH.Veh_Bttn_Synchro_Rnd = false;   
            myH.Origin = P_H.img_Transparent;           
        }   
    }

    // This function will be used for making all thumbs invisible under Account section if Account is not activated
    public Static void Myr_Call_Visibilty_Fn02(Myr_Synchro_ATC_Run.Page_Handler P_H){
        if(P_H.MYR_Status != 'Activated' && P_H.MYR_Status != ''){        
            P_H.Account_Name_Img = P_H.img_Transparent;
            P_H.Salutation_Digital_Img = P_H.img_Transparent;
            P_H.Mobile_Phone_Img = P_H.img_Transparent;
            P_H.Street_Img = P_H.img_Transparent;
            P_H.Zip_Code_Img = P_H.img_Transparent;
            P_H.City_Img = P_H.img_Transparent;
        }   
    }

    // This function will be used for making Synchro_ATC btn invisible under Account section if Account is not activated
    public Static void Myr_Call_Visibilty_Fn03(Myr_Synchro_ATC_Run.Page_Handler P_H){
        if(P_H.MYR_Status != 'Activated' && P_H.MYR_Status != ''){        
            P_H.Buttn_Acc_Rnd = false;          
        }   
    }

    // This function will be used for making thumbs invisible under vehicle section if Account is not activated
    public Static void Myr_Call_Visibilty_Fn04(Myr_Synchro_ATC_Run.Page_Handler P_H,Myr_Synchro_ATC_Run.Line_Handler MyVeh){
        if(P_H.MYR_Status != 'Activated' && P_H.MYR_Status != ''){        
            MyVeh.Origin = P_H.img_Transparent;
        }   
    }

    // This function will be used for making synchro ATC update btn invisible under Account section if CSDB Activated is checked
    public Static void Myr_Call_Visibilty_Fn05(Myr_Synchro_ATC_Run.Page_Handler P_H){
    if(P_H.CSDB_Account_Activated && P_H.Buttn_Acc_Lbl == System.Label.Myr_Synchro_ATC_Acc_Button + ' - Update'){        
      P_H.Buttn_Acc_Rnd = false;
    }  
  }

  /* This function will be used for making the visibility for syncro ATC btn 
     in Vehicle section if Syncro ATC-update btn on Account is clicked  */
  public Static void Myr_Call_Visibilty_Fn06(Myr_Synchro_ATC_Run.Page_Handler P_H){
    try{          
      if(!P_H.Buttn_Acc_Rnd && !P_H.Buttn_Veh_Rnd && P_H.Buttn_Acc_Lbl == System.Label.Myr_Synchro_ATC_Acc_Button + ' - Update'){ 
        P_H.Buttn_Veh_Rnd = true;
      }  
    }
    catch (exception e){

    }    
  }

}