@isTest
public class LMT_02_InterestInformationControllerTest {
    
    static Account css;
    static Account account;
    static Lead lead;
    static Lead leadOld;
    static Campaign cp;
    static Product2 p;
    static VEH_Veh__c v;
    static TDV_TestDriveVehicle__c testDriveVehicle;
    static TST_Transaction__c leadTransaction;
    
    static{
        
        css = new Account(
            Name='Teste',
            RecordTypeId=Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
            IDBIR__c='524125'
        );
        Insert(css);
        
        account = new Account(
            FirstName = 'Teste',
            LastName = 'SobreNome',
            PersEmailAddress__c = 'teste@test.com'
        );
        Insert(account);
        
        lead = new Lead(
            FirstName='Teste',
            LastName='Test',
            CPF_CNPJ__c = '47850135164',
            Email='teste@test.com',
            City = 'teste',
            RecordTypeId = Utils.getRecordTypeId('Lead', 'DVR'),
            DealerOfInterest__c = css.Id,
            VehicleOfInterest__c = 'Clio',
            AddressDateUpdated__c = system.today(),
            TypeOfInterest__c = 'Commercial Information - Personal Customer'
        );
        Insert(lead);
        
        leadOld = new Lead(
            FirstName='Teste',
            LastName='Test',
            CPF_CNPJ__c = '47850135164',
            Email='teste@test.com',
            RecordTypeId = Utils.getRecordTypeId('Lead', 'DVR'),
            DealerOfInterest__c = css.Id,
            VehicleOfInterest__c = 'Clio',
            AddressDateUpdated__c = system.today(),
            TypeOfInterest__c = 'Commercial Information - Personal Customer'
        );
        Insert(leadOld);
        
        cp = new Campaign(
            IsActive=true,
            RecordTypeId = Utils.getRecordTypeId('Campaign', 'SingleCampaign'),
            Dealer__c = css.Id,
            PAActiveShareAllowed__c = true,
            ManualInclusionAllowed__c = true,
            DBMCampaignCode__c = '123456',
            Name = 'Campanha Teste'
        );
        Insert cp;
        
        p = new Product2(
            Name = 'Clio',
            RecordTypeId = Utils.getRecordTypeId('Product2', 'PDT_Model'),
            ActiveToPlatform__c = true
        );
        Insert p;
        
        v = new VEH_Veh__c(
            Name = 'jjjjjjjjjjjjjjjjj',
            Color__c = 'vermelho',
            Model__c = 'Clio',
            VehicleRegistrNbr__c = 'DHG-4512',
            VersionCode__c = '1.8',
            Version__c = 'Hat'
        );
        Insert v;
        
        testDriveVehicle = new TDV_TestDriveVehicle__c(
            Available__c = true,
            Account__c = css.Id,
            Vehicle__c = v.Id
        );
        Insert testDriveVehicle;
        
        leadTransaction  = new TST_Transaction__c();
        leadTransaction.Contact_date__c = system.today();
        Insert(leadTransaction);
        
    }
    
    static testMethod void myTest1(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;

        controller.getLeadState();
        system.assert( controller.getLeadState() != null );
        
        controller.getOptHoras();
        system.assert( controller.getOptHoras() != null );
        
        controller.buttonCancelar();
        system.assert( controller.buttonCancelar() != null );
        
        controller.getMotivos();
        system.assert( controller.getMotivos() != null );       
        
        controller.buttonEfetivo();
        system.assertEquals( 'Effective', controller.statusContato);
        
        controller.buttonNaoEfetivo();
        system.assertEquals( 'Not Effective', controller.statusContato);
        
        Test.stopTest();
    }
    
    static testMethod void myTest2(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;
        
        controller.motivoNaoContato = 'teste';
        controller.leadTransaction = leadTransaction;
        controller.SalvaNaoOportunidade();
        system.assert( controller.SalvaNaoOportunidade() != null ); 
        
        controller.InsertOpportunity();
        controller.notContactHour = '08:00';
        controller.saveNotEffective();
        system.assert( controller.saveNotEffective() != null ); 
        
        controller.qualifyLead();
        system.assertEquals( controller.leadInfo.FirstName, controller.lead.FirstName );
        system.assertEquals( controller.leadInfo.Street, controller.lead.Street );
        
        controller.getCampaign();
        system.assert( controller.getCampaign() != null ); 
        
        
        Test.stopTest();
    }
    
    static testMethod void myTest5(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;
        
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.sObjAccount.Dealer_of_Interest_Show_Room__c = lead.DealerOfInterest__c;
        controller.testDriveVO.vehicleInterest = lead.VehicleOfInterest__c;
        system.assert( controller.saveAndConvertLead() != null );
        
        
        Test.stopTest();
    }
    
    static testMethod void myTest3(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;
        
        controller.DVE = Utils.getRecordTypeId('Lead', 'Vendas_Empresa');
        controller.convertLeadNotCreateOpportunity(account.Id, leadOld);
        
        controller.objCustomerActivityEntityForm = new  VFC17_CustomerActivityEntity();
        controller.fetchListCampaigns();
        system.assert( !controller.objCustomerActivityEntityForm.lstSelectOptionCampaigns.isEmpty() );
        
        
        Test.stopTest();
    }
    
    static testMethod void myTest4(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;
        
        controller.initialize();
        system.assert( controller.testDriveVO != null );
        
        controller.processVehicleInterestChange();
        system.assert( controller.testDriveVO.model != null );
        
        controller.processDealerInterestChange();
        system.assert( controller.testDriveVO.color != null );
        
        controller.testDriveVO.testDriveVehicleId = testDriveVehicle.Id;
        controller.processVehicleAvailableChange();
        system.assert( controller.testDriveVO.color != null );
        
        List<Product2> lsPrd = new List<Product2>();
        lsPrd.add(p);
        lead.CRV_CurrentVehicle__c = v.Id;
        controller.mapToValueObject(lead,lsPrd);
        system.assert( controller.mapToValueObject(lead,lsPrd) != null );
        
        system.debug(controller.getOwnerTaskUsers() != null);
        controller.getSchedulingDate();
        
        controller.lead2 = lead;
        controller.campaignId = cp.Id;
        controller.addIndicatedLead(true);
        
        Test.stopTest();
    }
    
    static testMethod void myTest6(){
        
        PageReference pg = Page.LMT_02_InterestInformation;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.StandardController(lead);
        ApexPages.currentPage().getParameters().put('Id',lead.Id);
        
        Test.startTest();
        
        LMT_02_InterestInformationController controller = new LMT_02_InterestInformationController(sc);
        controller.leadId = lead.Id;
        
        controller.initialize();
        system.assert( controller.testDriveVO != null );
        
        controller.processVehicleInterestChange();
        system.assert( controller.testDriveVO.model != null );
        
        controller.processDealerInterestChange();
        system.assert( controller.testDriveVO.color != null );
        
        controller.testDriveVO.testDriveVehicleId = testDriveVehicle.Id;
        controller.processVehicleAvailableChange();
        system.assert( controller.testDriveVO.color != null );
        
        List<Product2> lsPrd = new List<Product2>();
        lsPrd.add(p);
        lead.CRV_CurrentVehicle__c = v.Id;
        controller.mapToValueObject(lead,lsPrd);
        system.assert( controller.mapToValueObject(lead,lsPrd) != null );
        
        system.debug(controller.getOwnerTaskUsers() != null);
        controller.getSchedulingDate();
        
        controller.lead2 = lead;
        controller.campaignId = cp.Id;
        controller.addIndicatedLead(true);
        
        Test.stopTest();
    }

}