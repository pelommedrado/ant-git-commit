/** Just to be able to schedule the purge of the logger ... **/
global class INDUS_Batch_Purge_Logger_Schedulable_CLS implements Schedulable {

    global void execute(SchedulableContext ctx)  {
        INDUS_Batch_Purge_Logger_CLS purge = new INDUS_Batch_Purge_Logger_CLS();
        Database.executeBatch(purge, 50000);
    }
}