public abstract class ObjetivoAbstractController {
    
    //TEST CLASS: ObjetivoGeralControllerTest

    public Monthly_Goal_Group__c mgg { get;set; }
    
    public List<Monthly_Goal_Dealer__c> mgd { get;set; }
    
    public String titulo { get;set; }
    public Decimal total					{ get;set; }
    
    public List<SelectOption> mesOptionList { get;set; }
    public List<SelectOption> anoOptionList { get;set; }
    
    public List<String> monthList			{ get;set; }
    
    public User user	   { get;set; }
    public Account accUser { get;set; }
    
    public Boolean isManager {
        get {
            //get users from permission set BR_SFA_SalesWay_Manager
            Set<Id> salesManager = new Set<Id>();
            List<PermissionSetAssignment> psets = [SELECT Id, AssigneeId
                                                   FROM PermissionSetAssignment
                                                   WHERE PermissionSetId IN
                                                   (SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager')];
            for(PermissionSetAssignment ass: psets){
                salesManager.add(ass.AssigneeId);
            }
            return salesManager.contains(UserInfo.getUserId());
            //return user.UserRole.PortalRole == 'Manager';
        }
    }
    
    public Boolean isExecutive {
        get {
            //get users from permission set BR_SFA_SalesWay_Executive
            Set<Id> salesExecutive = new Set<Id>();
            List<PermissionSetAssignment> psets = [SELECT Id, AssigneeId
                                                   FROM PermissionSetAssignment
                                                   WHERE PermissionSetId IN
                                                   (SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Executive')];
            for(PermissionSetAssignment ass: psets){
                salesExecutive.add(ass.AssigneeId);
            }
            return salesExecutive.contains(UserInfo.getUserId());
            //return user.UserRole.PortalRole == 'Executive';
        }
    }
    
    public void init() {
        this.monthList  = new List<String>();
        this.mesOptionList = new List<SelectOption>();
        this.mesOptionList.add(new SelectOption('', '--Nenhum--'));
        this.anoOptionList = new List<SelectOption>();
        this.anoOptionList.add(new SelectOption('', '--Nenhum--'));
        
        this.mgd = new List<Monthly_Goal_Dealer__c>();
        this.total = 0;
        
        this.user = [
            SELECT Contact.AccountId, UserRole.PortalRole 
            FROM User 
            WHERE IsActive = TRUE AND Id =: UserInfo.getUserId() 
        ];
        
        system.debug('***User: '+user);
        
        this.accUser = [
            SELECT Name, Dealer_Matrix__c 
            FROM Account WHERE Id =: user.Contact.AccountId
        ];
        
        system.debug('***accUser: '+accUser);
        
        if(isExecutive) {
            Account accGroup = [
                SELECT Id, Name
                FROM Account
                WHERE IDBIR__c =: accUser.Dealer_Matrix__c Limit 1
            ];
            
            this.titulo = 'Matriz: '+accGroup.Name;     
            
        } else if(isManager) {
            this.titulo = this.accUser.Name;
            
        }
       
        Schema.DescribeFieldResult fieldResult =
            Monthly_Goal_Group__c.Month__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple) {
            this.monthList.add(f.getValue());
            mesOptionList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        
        Date data = Date.today();
        this.mgg = new Monthly_Goal_Group__c();
        this.mgg.Month__c = this.monthList.get(data.month()-1);
        this.mgg.Year__c = data.year();
        
        String param = Apexpages.currentPage().getParameters().get('mes');
        if(!String.isEmpty(param)) {
             this.mgg.Month__c = param;
        }
        param = Apexpages.currentPage().getParameters().get('ano');
        if(!String.isEmpty(param)) {
             this.mgg.Year__c = Decimal.valueOf(param);
        }
        
        List<Monthly_Goal_Group__c> mList = [
            SELECT ID, Month__c, Year__c
            FROM Monthly_Goal_Group__c
        ];
        
        Set<String> mes = new Set<String>();
        Set<Decimal> ano = new Set<Decimal>();
        for(Monthly_Goal_Group__c mg: mList) {
            if(mg.Month__c != null) {
                mes.add(mg.Month__c);
            }
            if(mg.Year__c != null) {
                ano.add(mg.Year__c);
            }
        }
        
        for(Decimal d : ano) {
            anoOptionList.add(new SelectOption(String.valueOf(d), String.valueOf(d)));
        }
    }
    
    public void updateData() {
        if(String.isEmpty(mgg.Month__c) || mgg.Year__c == null) {
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.WARNING, 
                'Não foi possivel realizar a busca, verifique o mês e o ano selecionado') );
            
            return;
        }
        
        List<Monthly_Goal_Group__c> newList = [
            SELECT ID, Month__c, Year__c, Matrix_Bir__c,
            General_Objetive__c,
            
            Clio_Proposal_percent__c,
            Duster_Oroch_Proposal_percent__c,
            Duster_Proposal_percent__c,
            Fluence_Proposal_percent__c,
            Kangoo_Proposal_percent__c,
            Logan_Proposal_percent__c,
            Master_Proposal_percent__c,
            Sandero_Proposal_percent__c,
            Sandero_RS_Proposal_percent__c,
            Sandero_Stepway_Proposal_percent__c,
            
            Clio_Proposal__c,
            Duster_Oroch_Proposal__c,
            Duster_Proposal__c,
            Fluence_Proposal__c,
            Kangoo_Proposal__c,
            Logan_Proposal__c,
            Master_Proposal__c,
            Sandero_Proposal__c,
            Sandero_RS_Proposal__c,
            Sandero_Stepway_Proposal__c
            
            FROM Monthly_Goal_Group__c
            WHERE Month__c =: mgg.Month__c AND Year__c =: mgg.Year__c AND Matrix_Bir__c =: accUser.Dealer_Matrix__c LIMIT 1 
        ];
        
        if(!newList.isEmpty()) {
            this.mgg = newList.get(0);
            
            System.debug('Mgg: ' + mgg);
            
            if(isManager) {
                
                this.mgd  = [
                    SELECT Id, Dealer__r.Name, General_Objetive__c,
                    Clio_Proposal__c, Duster_Proposal__c, Duster_Oroch_Proposal__c,
                    Fluence_Proposal__c, Kangoo_Proposal__c, Logan_Proposal__c,
                    Master_Proposal__c, Sandero_Proposal__c, Sandero_RS_Proposal__c, 
                    Sandero_Stepway_Proposal__c
                    FROM Monthly_Goal_Dealer__c
                    WHERE Monthly_Goal_Group__c =: this.mgg.Id AND Dealer__c =: accUser.Id
                ]; 
                
            } else if(isExecutive) {
                
                this.mgd  = [
                    SELECT Id, Dealer__r.Name, General_Objetive__c,
                    Clio_Proposal__c, Duster_Proposal__c, Duster_Oroch_Proposal__c,
                    Fluence_Proposal__c, Kangoo_Proposal__c, Logan_Proposal__c,
                    Master_Proposal__c, Sandero_Proposal__c, Sandero_RS_Proposal__c, 
                    Sandero_Stepway_Proposal__c
                    FROM Monthly_Goal_Dealer__c
                    WHERE Monthly_Goal_Group__c =: this.mgg.Id
                ];     
            }
           
            System.debug('Mgd: ' +  this.mgd );
            
        } else {
            mgg.General_Objetive__c = 0;
            this.mgd = new List<Monthly_Goal_Dealer__c>();
            
        }
        
        updateMgd();
    }
    
    public Decimal calc(Decimal obj, Decimal por) {
        Decimal value = obj * (por/100);
        //return value;  
        return Math.round(value);  
    }
    
    public abstract void updateMgd();
}