/*****************************************************************************************
    Name            : Rforce_AccountCascadingSearch_CLS_Test
    Description     : This class is used to test Rforce_AccountCascadingSearch_CLS class
    Approach        : According to the Data source and Record default country testing the Rforce_AccountSearch_Controller class
    Project Name    : Rforce
    Release Number  : 9.0_SP2
    Implemented Date:24/09/2015
    Implemented By  :Venkatesh Kumar Sakthivel
 
******************************************************************************************/
@isTest
public class Rforce_AccountCascadingSearch_CLS_Test{
  
// Code added by Venkatesh on 25/Oct/2015 for cascading search on Account for case insertion.
// Code added by Venkatesh on 25/Oct/2015 for cascading search on Account for case insertion.
    
       public static testMethod void testCascadingsearchconditionNotmatching()

 {
   Country_Info__c ctr = new Country_Info__c (Name = 'Brazil', Country_Code_2L__c = 'BR', Language__c = 'PTB', Case_RecordType__c='BR_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Brazil', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
   System.runAs(usr) 
        
   {
    Test.startTest();
    
    Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
    Account Accnt = new Account(FirstName = 'FirstName',Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Brazil',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
    insert Accnt;
    Case cas = new case (CountryCase__c='Brazil',LastName_Web__c='LastNamesample',Address_Web__c='Newaddress for Francee',SuppliedEmail='casecadetestsample@salesforce.com',SuppliedPhone='9444342371',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>(); 
    cascadecasevalue.add(cas);
    Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;
            
    Test.stopTest();
        }
    }
    
public static testMethod void testCascadingsearchconditionMatching()

 {
   Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
   
   System.runAs(usr) 
        
   {
    Test.startTest();
    
    Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
    Account Accnt1 = new Account(FirstName = 'FirstName',Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY,BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
    insert Accnt1;
    Case cas1 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest@salesforce.com',SuppliedPhone='9840512016',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Lastname,SuppliedEmail,CountryCase__c,Address,SuppliedPhone
    List<Case> cascadecasevalue = new List<Case>(); 
        
    cascadecasevalue.add(cas1);
    
    Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;
            
    Test.stopTest();
        }
    }    
   
    public static testMethod void testCascadingsearchconditionmultiple1() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt2 = new Account(FirstName = 'FirstName', Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt2;
     Account Accnt3 = new Account(FirstName = 'FirstName', Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt3;
     Account Accnt4 = new Account(FirstName = 'FirstName', Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt4;
    //Matching Lastname,address,phone,country, mail different
     Account Accnt5 = new Account(FirstName = 'FirstName',Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='samemailid@salesforce.com');
     insert Accnt5;                               
     Case cas2 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='samemailid@salesforce.com',SuppliedPhone='9840512016',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Case has a same mail id as one account           
     List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas2);           
         
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;         
     Test.stopTest();
        }
    }
    
    
     public static testMethod void testCascadingsearchconditionmultiple2() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt6 = new Account(FirstName = 'FirstName',Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt6;
     Account Accnt7 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt7;
     Account Accnt8 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt8;
    //Matching Lastname,address,phone,country, mail different
     Account Accnt9 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '9999999999', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013',HomePhone__c ='4561234567', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9999999999',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt9;                               
     Case cas3 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest@salesforce.com',SuppliedPhone='4561234567',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Case has a same phone number as one account             
     List<Case> cascadecasevalue = new List<Case>();            
                            
     cascadecasevalue.add(cas3);           
            
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;           
     Test.stopTest();
        }
    }
    
    public static testMethod void testCascadingsearchconditionmultiple3() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt10 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt10;
     Account Accnt11 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt11;
     Account Accnt12 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt12;
    //Matching Lastname,address,phone,country, mail different
     Account Accnt13 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'Sameaddress', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt13;                               
     Case cas4 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='Sameaddress',SuppliedEmail='casecadetest@salesforce.com',SuppliedPhone='9840512016',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Case has a same Address as one account             
     List<Case> cascadecasevalue = new List<Case>();            
                          
     cascadecasevalue.add(cas4);           
            
    Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;            
     Test.stopTest();
        }
    }
    public static testMethod void testCascadingsearchconditionmultiple4() {
  Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt14 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt14;
     Account Accnt15 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt15;
     Account Accnt16 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt16;
    Account Accnt17 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512016',HomePhone__c = '9840512016',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt17;                               
     Case cas5 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest@salesforce.com',SuppliedPhone='9840512016',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //More than one value for Lastname address and  country      
     List<Case> cascadecasevalue = new List<Case>();            
                         
     cascadecasevalue.add(cas5);           
            
    Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;          
     Test.stopTest();
        }
    }
    public static testMethod void testCascadingsearchconditionmultiple5() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt18 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512011',HomePhone__c = '9840512011',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt18;
     Account Accnt19 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street45', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512022',HomePhone__c = '9840512022',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt19;
     Account Accnt20 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street67', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512033',HomePhone__c = '9840512033',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt20;
    Account Accnt21 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street89', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '98405120444',HomePhone__c = '98405120444',PersEmailAddress__c='casecadetest@salesforce.com');
     insert Accnt21;                               
     Case cas6 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest@salesforce.com',SuppliedPhone='9840512016',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Lastname,SuppliedEmail,CountryCase__c           
     List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas6);           
           
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;           
     Test.stopTest();
        }
    }
    
  public static testMethod void testCascadingsearchconditionmultiple6() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt25 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c = '9840512022',PersEmailAddress__c='casecadetest1@salesforce.com');
     insert Accnt25;
     Account Accnt26 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c = '9840512022',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt26;
     Account Accnt27 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c = '9840512022',PersEmailAddress__c='casecadetest3@salesforce.com');
     insert Accnt27;
     Account Accnt28 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c = '9444216321',PersEmailAddress__c='casecadetest4@salesforce.com');
     insert Accnt28;
     // Lastname phone country match                       
     Case cas8 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest11@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    //Lastname,SuppliedEmail,CountryCase__c           
     List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas8);           
          
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;            
     Test.stopTest();
        }
    }
    public static testMethod void testCascadingsearchconditionmultiple8() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt29 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512023',HomePhone__c='9840512023',PersEmailAddress__c='casecadetest1@salesforce.com');
     insert Accnt29;
     Account Accnt30 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512024',HomePhone__c='9840512024',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt30;
     Account Accnt31 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512026',HomePhone__c='9840512026',PersEmailAddress__c='casecadetest3@salesforce.com');
     insert Accnt31;                        
     Case cas9 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street',SuppliedEmail='casecadetest11@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                          
     cascadecasevalue.add(cas9);           
           
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;          
     Test.stopTest();
        }
    }
    
public static testMethod void testCascadingsearchconditionmultiple9() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt32 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512023',HomePhone__c='9840512023',PersEmailAddress__c='casecadetest1@salesforce.com');
     insert Accnt32;
     Account Accnt33 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street45', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840512024',HomePhone__c='9840512024',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt33;
                           
     Case cas10 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street90',SuppliedEmail='casecadetest2@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas10);           
        
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;           
     Test.stopTest();
        }
    }

public static testMethod void testCascadingsearchconditionmultiple10() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt34 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840112023',HomePhone__c = '9840112023',PersEmailAddress__c='casecadetest1@salesforce.com');
     insert Accnt34;
     Account Accnt35 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street45', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840332024',HomePhone__c = '9840332024',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt35;
     Account Accnt36 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street412', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9899912024',HomePhone__c = '9899912024',PersEmailAddress__c='casecadetest25@salesforce.com');
     insert Accnt36;
                           
     Case cas11 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street903',SuppliedEmail='casecadetest432@salesforce.com',SuppliedPhone='9899912024',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                          
     cascadecasevalue.add(cas11);           
          
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;         
     Test.stopTest();
        }
    }
    
    
public static testMethod void testCascadingsearchconditionmultiple11() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt37 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840112023',HomePhone__c='9840112023',PersEmailAddress__c='casecadetest1@salesforce.com');
     insert Accnt37;
     Account Accnt38 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street45', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9844444023',HomePhone__c='9844444023',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt38;
     Account Accnt39 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street412', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9899912024',HomePhone__c='9899912024',PersEmailAddress__c='casecadetest2@salesforce.com');
     insert Accnt39;
                           
     Case cas12 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street903',SuppliedEmail='casecadetest2@salesforce.com',SuppliedPhone='9899912024',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas12);           
            
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;         
     Test.stopTest();
        }
    }
    
public static testMethod void testCascadingsearchconditionmultiple12() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt40 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0001', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9840112023',HomePhone__c='9840112023',PersEmailAddress__c='casecadetest21@salesforce.com');
     insert Accnt40;
     Account Accnt42 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street412', ComAgreemt__c = 'Yes',country__c = 'Colombia',PersonMobilePhone = '9444216321',HomePhone__c='9444216321',PersEmailAddress__c='casecadetest223@salesforce.com');
     insert Accnt42;
                           
     Case cas13 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street903',SuppliedEmail='casecadetest552@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas13);           
            
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;   
     Rforce_AccountCascadingSearch_CLS.WebtoCase_UpdateAccount(cascadecasevalue);
           
     Test.stopTest();
        }
    }  

public static testMethod void Lastnamephonecountry() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt43 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0001', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c='9840112023',PersEmailAddress__c='casecadetest21@salesforce.com');
     insert Accnt43;
     Account Accnt44 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street412', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c='9444216321',PersEmailAddress__c='casecadetest223@salesforce.com');
     insert Accnt44;
                           
     Case cas14 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street903',SuppliedEmail='casecadetest552@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas14);           
            
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;   
     Rforce_AccountCascadingSearch_CLS.WebtoCase_UpdateAccount(cascadecasevalue);
           
     Test.stopTest();
        }
    }
public static testMethod void lstLastnameAddressPhoneCountry() {
        Country_Info__c ctr = new Country_Info__c (Name = 'Colombia', Country_Code_2L__c = 'CO', Language__c = 'Spanish', Case_RecordType__c='CO_Case_RecType');
   insert ctr;
   User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) 
        
    {
     Test.startTest();
     
     Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
     Account Accnt43 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0001', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c='9840112023',PersEmailAddress__c='casecadetest21@salesforce.com');
     insert Accnt43;
     Account Accnt44 = new Account(FirstName = 'FirstName',  Language__c = 'My Profile Language', LastName = 'LastName', Phone = '0000', RecordTypeId = RTID_COMPANY, BillingPostalCode = '75013', BillingStreet = 'my street23', ComAgreemt__c = 'Yes',country__c = 'Colombia',HomePhone__c='9444216321',PersEmailAddress__c='casecadetest223@salesforce.com');
     insert Accnt44;
                           
     Case cas14 = new case (CountryCase__c='Colombia',LastName_Web__c='LastName',Address_Web__c='my street23',SuppliedEmail='casecadetest552@salesforce.com',SuppliedPhone='9444216321',Type='Complaint', Origin='RENAULT SITE',Status='New', Description='Trigger test clas');
    List<Case> cascadecasevalue = new List<Case>();            
                           
     cascadecasevalue.add(cas14);           
            
     Rforce_AccountCascadingSearch_CLS.searchAccount(cascadecasevalue) ;   
     Rforce_AccountCascadingSearch_CLS.WebtoCase_UpdateAccount(cascadecasevalue);
           
     Test.stopTest();
        }
    } 
    
}