global class OpportunitySharingBatch implements Database.Batchable<sObject> {
    private List<Opportunity> oppList;
    
    public OpportunitySharingBatch(final List<Opportunity> oppList) {
        this.oppList = oppList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        final String query = 'SELECT Id, Dealer__r.Name, Dealer__c,' + 
            ' Dealer__r.ParentId, RecordTypeId ' + 
            'FROM Opportunity WHERE Id IN: oppList';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        System.debug('execute ' + scope);
        //OpportunitySharing.sharing(scope);
    }
    
    global void finish(Database.BatchableContext bc) {
        System.debug('finish');
    }
}