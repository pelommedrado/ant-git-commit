public class VFC151_OfferUpgrade {

  public VFC151_OfferUpgrade(){}

    public static void mudarId(List<Offer__c> lsOffer){
    	List<Id> lsIdOfferUpgrade = new List<Id>(); //armazena os ids da oferta que é o upgrade
        Map<Id,Offer__c>mapOfferta = new Map<Id,Offer__c>();//armazena a nova oferta porem a chave é o id da oferta upgrade
        for(Offer__c offer : lsOffer){
        	if(offer.Offer_Upgrade__c!=null){
          		lsIdOfferUpgrade.add(offer.Offer_Upgrade__c);
          		mapOfferta.put(offer.Offer_Upgrade__c,offer);         
        	}
      	} 
        if(!lsIdOfferUpgrade.isEmpty()){
            List<Offer__c> lsOfferUpdate = new List<Offer__c>();
            for(Offer__c offerBase: [select Id,Offer_Upgrade__c FROM Offer__c where Id in:(lsIdOfferUpgrade)]){
                //primeiro adiciona tudo oq precisa na oferta base
                if(mapOfferta.containsKey(offerBase.Id)){
                    lsOfferUpdate.add(new Offer__c(
                            id = offerBase.Id,
                            Offer_Upgrade__c = mapOfferta.get(offerBase.Id).id
                        )
                    );
                    //agora apaga o valor da oferta q é  upgrade
                    lsOfferUpdate.add(new Offer__c(
                            id = mapOfferta.get(offerBase.Id).id,
                            Offer_Upgrade__c = null,
                            isUpgrade__c = offerBase.id
                        )
                    );
                }
            }
            if(!lsOfferUpdate.isEmpty()){
                Database.update(lsOfferUpdate);
            }
        }
        /*	String upgrade; 
     // Map<Id,Offer__c>mapOfferta = new Map<Id,Offer__c>();
      List<Id> lsIdOfferUpgrade = new List<Id>();
      for(Offer__c offer : lsOffer){
        if(offer.Offer_Upgrade__c!=null){
          lsIdOfferUpgrade.add(offer.Offer_Upgrade__c);
          //mapOfferta.put(offer.id,offer);         
        }
      } 
      System.debug('### lsIdOfferUpgrade '+lsIdOfferUpgrade);      
      if(!lsIdOfferUpgrade.isEmpty()){
       List<Offer__c>lsOfferUpdate = new List<Offer__c>();
       System.debug('### [select Id,Name,Offer_Upgrade__c FROM Offer__c where Id in:(lsIdOfferUpgrade)] '+[select Id,Name,Offer_Upgrade__c FROM Offer__c where Id in:(lsIdOfferUpgrade)]); 
       for(Offer__c ofertaInicial: [select Id,Name,Offer_Upgrade__c FROM Offer__c where Id in:(lsIdOfferUpgrade)]){
         System.debug('### mapOfferta '+mapOfferta);
         if(mapOfferta.containsKey(ofertaInicial.id)){
          ofertaInicial.Offer_Upgrade__c = mapOfferta.get(ofertaInicial.id).Id;
          lsOfferUpdate.add(ofertaInicial);
          lsOfferUpdate.add(new Offer__c(
           id = ofertaInicial.Offer_Upgrade__c,
                      isUpgrade__c = 'TESTES',//b.Id,
                      Offer_Upgrade__c = null
                      ));
        }
      } 
      if(!lsOfferUpdate.isEmpty()){
        System.debug('### lsOfferUpdate '+lsOfferUpdate);
        Database.upsert(lsOfferUpdate);
      } 
    }*/

  }


}