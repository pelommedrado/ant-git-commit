/**
* Classe que será disparada pela trigger do objeto VehicleBooking__c após a inserção dos registros.
* @author Felipe Jesus Silva.
*/
public class VFC96_VehicleBookAfterInsertExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{	
		List<VehicleBooking__c> lstSObjVehicleBooking = (List<VehicleBooking__c>) lstNewtData;
		
		this.checkStatus(lstSObjVehicleBooking);
	}
	
	private void checkStatus(List<VehicleBooking__c> lstSObjVehicleBooking)
	{		
		Account acc;
        User u = Sfa2Utils.obterUsuarioLogado();
        if(u.ContactId != null){
            acc = Sfa2Utils.obterContaUserComunidade();
            System.debug('******---- CONTA: ' + acc);
        }
        
        VEH_Veh__c sObjVehicle = null;
		List<VEH_Veh__c> lstSObjVehicle = new List<VEH_Veh__c>();
		
		for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBooking)
		{		
			if(sObjVehicleBooking.Status__c == 'Active')
			{
				/*instancia o veículo que será atualizado como reservado*/
				sObjVehicle = new VEH_Veh__c(Id = sObjVehicleBooking.Vehicle__c);
				//Ao reservar um veículo o seu status não é mais alterado para reservado
				//sObjVehicle.Status__c = 'Reserved';
				//Ajuste inserido no dia 09/08/2013 - Solicitado por Hugo Medrado
				sObjVehicle.Booking_User__c = sObjVehicleBooking.CreatedById;
                
                //Ao reservar um veíuclo seu status volta à ser alterado para Reservado - 18/07/2016
                if(u.ContactId != null){
                    System.debug('*******Novo estoque: ' + acc.new_stock_version__c);
                    if(acc.new_stock_version__c == true){
                        sObjVehicle.Status__c = 'Booking';
                    }
                }
                
				
				/*adiciona o veículo na lista*/
				lstSObjVehicle.add(sObjVehicle);
			}
		}
		
		if(!lstSObjVehicle.isEmpty())
		{
			VFC65_VehicleBO.getInstance().updateVehicles(lstSObjVehicle);
		}		
	}

}