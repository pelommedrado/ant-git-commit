@isTest
private class SurveyAnswerTest {
	
	@TestSetup static void setup(){
		DateTime daysBefore = System.today().addDays(-2);

		List<Account> accToInsert = new List<Account>();

		Account acc1 = new Account();
		acc1.Name = 'accTeste';
		acc1.Phone = '12345678';
		accToInsert.add(acc1);

		Account acc2 = new Account();
		acc2.Name = 'accTeste2';
		acc2.Phone = '12345678';
		accToInsert.add(acc2);

		Account acc3 = new Account();
		acc3.Name = 'accTeste3';
		acc3.Phone = '12345678';
		accToInsert.add(acc3);

		Account acc4 = new Account();
		acc4.Name = 'accTeste4';
		acc4.Phone = '12345678';
		accToInsert.add(acc4);

		Database.insert(accToInsert);

		List<Interview__c> interviewToInsert = new List<Interview__c>();

		Interview__c interviewPV = new Interview__c();
		interviewPV.Name = 'Pesquisa PV';
		interviewToInsert.add(interviewPV);

		Interview__c interviewVN = new Interview__c();
		interviewVN.Name = 'Pesquisa VN';
		interviewToInsert.add(interviewVN);

		Database.insert(interviewToInsert);

		List<InterviewResponse__c> interviewResToInsert = new List<InterviewResponse__c>(); 

		InterviewResponseBuild instance = InterviewResponseBuild.getInstance();

		InterviewResponse__c interviewRes = instance.newInterviewResp(acc1.Id, null, interviewPV.Id);
		interviewResToInsert.add(interviewRes);

		interviewRes = instance.newInterviewResp(acc2.Id, null, interviewVN.Id);
		interviewResToInsert.add(interviewRes);

		interviewRes = instance.newInterviewResp(acc3.Id, null, interviewPV.Id);
		interviewResToInsert.add(interviewRes);

		Database.insert(interviewResToInsert);

		List<SurveySent__c> SSToInsert = new List<SurveySent__c>();

		SurveySent__c ss1 = new SurveySent__c();
		ss1.SendDate__c = daysBefore;
		ss1.Status__c = 'Delivered';
		ss1.SendSurvey__c = 'Pesquisa PV';
		ss1.Account__c = acc1.Id;
		SSToInsert.add(ss1);

		SurveySent__c ss2 = new SurveySent__c();
		ss2.Status__c = 'Delivered';
		ss2.SendDate__c = daysBefore;
		ss2.SendSurvey__c = 'Pesquisa VN';
		ss2.Account__c = acc2.Id;
		SSToInsert.add(ss2);

		SurveySent__c ss3 = new SurveySent__c();
		ss3.Status__c = 'Delivered';
		ss3.SendDate__c = daysBefore;
		ss3.SendSurvey__c = 'Pesquisa PV';
		ss3.Account__c = acc3.Id;
		SSToInsert.add(ss3);

		SurveySent__c ss4 = new SurveySent__c();
		ss4.Status__c = 'Delivered';
		ss4.SendDate__c = daysBefore;
		ss4.SendSurvey__c = 'Pesquisa VN';
		ss4.Account__c = acc4.Id;
		SSToInsert.add(ss4);

		Database.insert(SSToInsert);
	}

	@isTest static void shouldRunBatch() {
		Test.startTest();
		ID batchprocessid = Database.executeBatch(new SurveyAnswerBatch(), 50);
		Test.stopTest();

		List<SurveySent__c> allSS = [SELECT Id, Name, Account__c, 
											SurveyAnswered__c, Status__c, SendDate__c 
									FROM SurveySent__c];

		Integer surveyChecked 	= 0;
		Integer surveyUnChecked = 0;

		for(SurveySent__c item : allSS){
			if(item.SurveyAnswered__c){
				surveyChecked++;
			}
			else{
				surveyUnChecked++;
			}
		}

		System.assertEquals(3, surveyChecked);
		System.assertEquals(1, surveyUnChecked);
	}	

	@isTest static void shouldScheduleBatch() {
		SurveyAnswerScheduled instance = new SurveyAnswerScheduled();

		Test.startTest();
		String sch='0 5 2 * * ?';
		System.schedule('Batch Schedule', sch , instance);
		Test.stopTest();
	}	
}