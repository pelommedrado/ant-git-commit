/* Purge of accounts
  *************************************************************************************************************
  18 Mar 2016 : Creation - purge of german accounts
  *************************************************************************************************************/
global class Myr_Batch_Germany2_SCH implements Schedulable {
    global void execute(SchedulableContext sc) {

        //Get accounts to delete
        List<Myr_Batch_Germany2_ToolBox.TB_Answer> Acc_Ids = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('');

        //Delete child records 
        //Database.executebatch(new Myr_Batch_Germany2_CustomerMessage_BAT(Acc_Ids));
        //ID jobID = System.enqueueJob(new Myr_Batch_Germany2_CustomerMessage_BAT(Acc_Ids));
        /*Database.executebatch(new Myr_Batch_Germany2_SynchroAtc_BAT(Acc_Ids));
        
        Database.executebatch(new Myr_Batch_Germany2_Logger_BAT(Acc_Ids));
        Database.executebatch(new Myr_Batch_Germany2_Relation_BAT(Acc_Ids));
        
        //Release and anonymize community users
        Database.executebatch(new Myr_Batch_Germany2_User_BAT(Acc_Ids));
        
        //Delete or anonymize accounts
        List<Myr_Batch_Germany2_ToolBox.TB_Answer> Acc_To_Del = new List<Myr_Batch_Germany2_ToolBox.TB_Answer>();
        List<Myr_Batch_Germany2_ToolBox.TB_Answer> Acc_To_Anonymize = new List<Myr_Batch_Germany2_ToolBox.TB_Answer>();
        for (Myr_Batch_Germany2_ToolBox.TB_Answer TB : Acc_Ids){  
            if (!TB.User_Associated){
                Acc_To_Del.add(TB);
            }else{
                Acc_To_Anonymize.add(TB);
            }
        }
        Database.executebatch(new Myr_Batch_Germany2_Account_BAT(Acc_To_Del, 'DELETE'));
        Database.executebatch(new Myr_Batch_Germany2_Account_BAT(Acc_To_Anonymize, 'RESET'));
        //Reset only one brand
        //Renault
        Database.executebatch(new Myr_Batch_Germany2_Account_BAT(Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('RENAULT'), 'RENAULT'));
        //Dacia
        Database.executebatch(new Myr_Batch_Germany2_Account_BAT(Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('DACIA'), 'DACIA'));
        */
    }
}