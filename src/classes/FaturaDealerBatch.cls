global class FaturaDealerBatch implements Schedulable, Database.Batchable<sObject> {
    
    public String query { get; set; }

    public FaturaDealerBatch() {
        this(null);
    }
    
    public FaturaDealerBatch(String selectQuery) {
        this.query = selectQuery;
        
        if(this.query == null) {
            this.query =
                'SELECT Id, Error_Messages__c, RecordTypeId, ' 		+ 
                ' NFBANDEI__c, NFCODOPR__c, NFTIPONF__c, NFTIPREG__c, NFBIRENV__c, NFBIREMI__c,' 	+ 
                ' NFDTANFI__c, NFDTANAS__c, NFTIPCLI__c, NFCPFCGC__c, NFNRONFI__c, '				+
                ' NFEMAILS__c, NFDDDRES__c, NFTELRES__c, NFDDDCEL__c, NFTELCEL__c, NFDDDCOM__c, NFTELCOM__c, NFDDDFAX__c, NFNROFAX__c,' +
                ' NFCHASSI__c, NFANOFAB__c, NFANOMOD__c, NFDTASIS__c, Count__c,'					+
                ' NFBIRENV_Mirror__c, NFBIREMI_Mirror__c, NFCODOPR_Mirror__c, NFTIPONF_Mirror__c,'  +
                ' NFCPFCGC_Mirror__c, NFEMAILS_Mirror__c, NFDDDRES_Mirror__c, NFDDDCEL_Mirror__c,'	+
                ' NFTELCEL_Mirror__c, NFTELRES_Mirror__c, NFANOFAB_Mirror__c, NFANOMOD_Mirror__c,'  +
                ' NFCHASSI_Mirror__c, NFDTANFI_Mirror__c, NFDTASIS_Mirror__c, NFNRONOS__c'						+
                ' FROM FaturaDealer__c'																+
                ' WHERE RecordTypeId = \'' + FaturaDealerServico.UNPROCESSED + '\' ';
        }
    }
    
    global void execute(SchedulableContext sc) {
        System.debug('Iniciando a execucao do batch');
        FaturaDealerBatch fatBatch = new FaturaDealerBatch(null);
        Database.executeBatch(fatBatch);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<FaturaDealer__c> scope) {
        FaturaDealerServico servico = new FaturaDealerServico();
        servico.iniciarValidacao(scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[] {'pelommedrado@gmail.com'});
        mail.setReplyTo('batch@acme.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
*/
    }
}