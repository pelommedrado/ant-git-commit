global class VFC124_CampaignBatch implements Database.Batchable<sObject>, Database.Stateful
{
	/**
	 * Start method: fetch records to be processed
	 */
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		system.debug(LoggingLevel.INFO, '*** VFC124_CampaignBatch.start()');
		String query = VFC121_CampaignDAO.getInstance().buildCampaignQueryForBatchProcess();
		return Database.getQueryLocator(query);
	}
	
	/**
	 * Execute method: do the work
	 */
	global void execute(Database.BatchableContext BC, List<sObject> lstObjects)
	{
		system.debug(LoggingLevel.INFO, '*** VFC124_CampaignBatch.execute()');
		
		Date dtToday = Date.today();
		List<Campaign> lstCampaignToUpdate = new List<Campaign>();

		for (sObject campaignRecord : lstObjects)
		{
            Campaign campaign = (Campaign)campaignRecord;
		
			// Check if campaign with:
			// - Status equals to Planned 
			// - and today date between StartDate and EndDate
			// should be activated and have their Status changed to 'In Progress'
			if (campaign.Status == 'Planned' 
			 && campaign.StartDate <= dtToday && campaign.EndDate >= dtToday)
			{
				campaign.Status = 'In Progress';
				campaign.IsActive = true;
				lstCampaignToUpdate.add(campaign);
				system.debug(LoggingLevel.INFO, '*** 1st: campaign.Id='+campaign.Id);
			}

			// Check if campaign with:
			// - Status equals to In Progress
			// - and today is greater than EndDate
			// should be deactivated and Status changed to Finalized
			else if (campaign.Status == 'In Progress' 
			 && campaign.EndDate < dtToday)
			{
				campaign.Status = 'Finalized';
				campaign.IsActive = false;
				lstCampaignToUpdate.add(campaign);
				system.debug(LoggingLevel.INFO, '*** 2nd: campaign.Id='+campaign.Id);
			}
		}
		
		if (lstCampaignToUpdate.size() > 0)
		{
			system.debug(LoggingLevel.INFO, '*** lstCampaignToUpdate='+lstCampaignToUpdate);
			update lstCampaignToUpdate;
		}
	}
	
	/**
	 * Finish method: execute post processing
	 */
	global void finish(Database.BatchableContext BC)
	{
		system.debug(LoggingLevel.INFO, '*** VFC124_CampaignBatch.finish()');
        system.debug(LoggingLevel.INFO, '*** Processo concluído');
	}
}