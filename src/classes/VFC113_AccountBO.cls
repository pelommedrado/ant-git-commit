/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto Account.
* @author Felipe Jesus Silva.
*/
public class VFC113_AccountBO 
{
	private static final VFC113_AccountBO instance = new VFC113_AccountBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC113_AccountBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC113_AccountBO getInstance()
	{
		return instance;
	}
	
	public Account getById(String id)
	{
		Account sObjAccount = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(id);
		
		if(sObjAccount == null)
		{
			throw new VFC89_NoDataFoundException('A conta não foi encontrada.');
		}
		
		return sObjAccount;
	}

}