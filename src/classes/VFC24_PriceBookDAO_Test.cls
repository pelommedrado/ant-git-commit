/**
	Class   -   VFC24_PriceBookDAO_Test
    Author  -   Suresh Babu
    Date    -   29/10/2012
    
    #01 <Suresh Babu> <29/10/2012>
        Created this class using test for VFC24_PriceBookDAO.
    #02 <Subbarao> <13/03/2013>
        Modified the class to get code Coverage for added methods of VFC24_PriceBookDAO.
**/
@isTest 
public class VFC24_PriceBookDAO_Test {
	
	static testMethod void VFC24_PriceBookDAO_Test() {			
        
        Test.startTest();
        List<Product2> lstProducts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects(); 
        system.assert(lstProducts.size()!=0);
    	PricebookEntry PriceBook = VFC24_PriceBookDAO.getInstance().findPricebyProductId(lstProducts[0].Id);
    	List<PricebookEntry> PriceBookLst = VFC24_PriceBookDAO.getInstance().fetch_All_PricebookEntry();    	
    	VFC24_PriceBookDAO.getInstance().return_Map_ProductCode_PriceBookEntry();
    	VFC24_PriceBookDAO.getInstance().return_Map_ProductID_PriceBookEntry();
     	Test.stopTest();
        
		
	}
}