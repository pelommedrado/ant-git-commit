@isTest
public class CAC_SupportCaseControllerTest {

    static testMethod void teste(){
        
        W2CParams__c w2c = new W2CParams__c(
        	Org_Id__c = '005c0000001YdcR',
            RecordTypeId__c = '012Q0000000Gj9cAAC',
            Name = 'PRD',
            URLW2C__c = 'www.teste.teste.com'
        );
        Database.insert(w2c);
        
        
        CAC_SupportCaseController support = new CAC_SupportCaseController();
       
        
        System.assert(support.orgId == '005c0000001YdcR');
        System.assert(support.recordTypeId == '012Q0000000Gj9cAAC');
        System.assert(support.emailUser.equals(UserInfo.getUserEmail()));
       	//support.submitForm();
        System.assert(support.userId.equals(UserInfo.getUserId()));
        System.assert(support.userName.equals(UserInfo.getFirstName()+' '+UserInfo.getLastName()));
        System.assert(support.idUserToURL.equals(UserInfo.getUserId()));
      // System.debug('#### support.userAccount '+support.userAccount);
        //System.assert(support.userAccount == '');
        
        
        CAC_SupportCaseControllerTest.updateUserBir();
        CAC_SupportCaseController ca = new CAC_SupportCaseController();
        ca.serializedForm = 'abcd';
        //ca.submitForm();
        CAC_SupportCaseController support2 = new CAC_SupportCaseController();
        System.assert(support2.userAccount == 'Concessionaria teste');
        Boolean birAcc = CAC_SupportCaseController.supportUser;
        
    }
    
    private static void updateUserBir(){
        Account dealer = conta();
        User usuario = new User(
        	id = UserInfo.getUserId(),
            BIR__c = dealer.IDBIR__c
        );
        Database.update(usuario);
    }
    
    private static Account conta (){
        
        User manager = new User(
            
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        String idTipoRegistro  = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id;
        Account dealerAcc = new Account(
            
            Name = 'Concessionaria teste',
            IDBIR__c = '123ABC123',
            RecordTypeId = idTipoRegistro,
            Country__c = 'Brazil',
            NameZone__c = 'R2',
            ShippingState = 'SP',
            ShippingCity = 'Sao Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true
            
        );
        Database.insert( dealerAcc );
        
        MyOwnCreation userMoc = new MyOwnCreation();
        User user = userMoc.CriaUsuarioComunidade();
        System.runAs(user){
            
            CAC_SupportCaseController support = new CAC_SupportCaseController();
            String testeAcc = support.userAccount;
            Boolean teste = CAC_SupportCaseController.supportUser;
            
        }
        
        
        return dealerAcc;
        
    }
    
    
    
    
    
    
    
}