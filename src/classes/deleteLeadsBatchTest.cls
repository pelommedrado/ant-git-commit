@isTest
private class deleteLeadsBatchTest {
    
    static testmethod void test() {
        
        // The query used by the batch job.
        String query = 'SELECT LeadId FROM CampaignMember';
        
        // Create some test CampaignMember and lead items to be delete by the batch job.
        List<CampaignMember> cmList = new List<CampaignMember>();
        List<Lead> leadList = new List<Lead>();
        
        Campaign c = new Campaign(Name='Teste',WebToOpportunity__c = false);
        Insert c;
        
        for (Integer i=0;i<10;i++) {
            Lead lead = new Lead(
                CPF_CNPJ__c = '72413718362' + i,
                LastName = 'Sobrenome' + i
            );
            Insert lead;
            
            CampaignMember cm = new CampaignMember(
                LeadId = lead.Id,
                CampaignId = c.Id
            );
            cmList.add(cm);
        }
        insert cmList;
        
        Test.startTest();
        deleteLeadsBatch batch = new deleteLeadsBatch(query);
        Database.executeBatch(batch);
        Test.stopTest();
        
        // Verifica se os Leads foram excluidos
        Integer i = [SELECT COUNT() FROM Lead];
        System.assertEquals(0, i);
    }
}