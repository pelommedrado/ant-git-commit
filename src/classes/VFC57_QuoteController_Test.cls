/** 
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC57_QuoteController_Test {

	static testMethod void unitTest01()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                      IDBIR__c = '10570');
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id,
							                       Pricebook2Id = pb2.Id);
		insert opportunity1;

        Quote q = new Quote (Name = 'QuoteNameTest_1',
        					 OpportunityId = opportunity1.Id,
        					 Pricebook2Id = pb2.Id);
        insert q;
		
    	Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);

    	// Start Test
    	Test.startTest();

        VFC57_QuoteController controller = new VFC57_QuoteController(stdCont);
         
        controller.initialize();
        
        VFC60_QuoteVO quoteItemVO = controller.quoteItemVO;
        for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes) {
            auxQuote.isCheckOpp = true;
        }
        VFC61_QuoteDetailsVO qD = controller.quoteDetailsVO;
        
        controller.cancelOpportunity();
        controller.getIsReadOnly();
        controller.hasSyncedQuote();
        controller.newQuote();
        controller.openPopUpCancellation();
        controller.processReasonCancellationChange();
        
        controller.sendToRequest();
        
        for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes) {
            auxQuote.status = 'Canceled';
        }
        
        controller.sendToRequest();
        
        controller.sync();
        controller.synchronizeQuote(false);
 		

		// Stop Test
		Test.stopTest();
    }
}