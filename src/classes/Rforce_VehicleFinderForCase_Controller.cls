/**
    *    @Author Bid Admin
    *    @Modifiedby RNTBCI(Ramamoorthy Dakshinamoorthy)
    *    @Date 22-07-2015
    *    @Description This class is used to search the details of the vehicle
    *    Project Rforce
    */
     /*  Modification History  
              Modified By  :Monisha(RNTBCI)
              Modified Date:12/08/2015
              Modificaition Desc: Added the constructor for pagination.
     */
    public with sharing class Rforce_VehicleFinderForCase_Controller 
    {
        public String VIN {get;set;}
        public String VRN {get;set;}
        public String LanguageCode {get;set;}
        public String CountryCode {get;set;}
        public String accountId {get;set;}
        public String accountName {get;set;}
        public String countryFromAcc {get;set;}
        public String urlForLink {get;set;}
        public String vehiclePrefix {get;set;}
        public String ChampIn {get;set;}
        public String ChampOut {get;set;}
        public String ValeurChamp {get;set;}
        public List<VEH_Veh__c> VehicleModList {get; set;}
        public String businessHrs {get;set;}
        public String nameFromBhrs=null; 
        public String vehId{get;set;}
        public String vehicleRelationExists{get;set;}
        public String vehName{get;set;}  
        public Boolean bIsVehilceFound{get;set;}
        
        public Rforce_VehicleFinderForCase_Controller(){
            VIN = '';
            VRN = '';
            LanguageCode = '';
            CountryCode = '';
            accountName = '';
            urlForLink = URL.getSalesforceBaseUrl().toExternalForm();       
            Schema.DescribeSObjectResult descrVehicle = Schema.SObjectType.VEH_Veh__c;
            vehiclePrefix = descrVehicle.getKeyPrefix();
            ChampIn='';
            ValeurChamp='';
            ChampOut='OF';
            accountId = Apexpages.currentPage().getParameters().get('Id');
            if (accountId != null && accountId != ''){
                list<Account> accs = [Select Name,country__c from Account where Id = :accountId limit 1];
                VehicleModList=new List<VEH_Veh__c>();
                VehicleModList=[Select v.Id, Name, VehicleRegistrNbr__c, VehicleBrand__c, Model__c, DateofManu__c, DeliveryDate__c, KmCheck__c, KmCheckDate__c, EnergyType__c From VEH_Veh__c v where Id IN (Select VIN__c from VRE_VehRel__c where Account__r.Id = :accountId and Status__c ='Active')];
                if (accs.size() > 0)
                    accountName = accs.get(0).Name;
                    countryFromAcc = accs.get(0).country__c;               
                try{
                  nameFromBhrs = [SELECT Name FROM BusinessHours where Name = :countryFromAcc].Name;          
                 }catch(Exception nameFromBhrs ){}
                if(nameFromBhrs == null){      
                  businessHrs ='Default';
                }else{
                  businessHrs =nameFromBhrs;
                 }
            }    
        }   
        public List<VEH_Veh__c> getvehiclePageList(){
          List<VEH_Veh__c> vehforPagination = new List<VEH_Veh__c>();
          System.debug('**VehicleModList**'+VehicleModList);
        for(VEH_Veh__c va : VehicleModList){
        vehforPagination.add(va);
        }
        System.debug('**vehforPagination**'+vehforPagination);
        return vehforPagination;
        
        }
        public PageReference selectVehicle(){    
            return new PageReference('/' + accountId);            
        } 
    /**
    *    @Author Bic Admin
    *    @Modifiedby RNTBCI(Ramamoorthy Dakshinamoorthy)
    *    @Date 22-07-2015
    *    @Description This method is used search the vehicle details
    *    Project Rforce
    */    
        public void searchVehicle()
        {
            String query = 'Select Id, Name from VEH_Veh__c ';
            String whereClause = '';
            VEH_Veh__c vehicleFound;
            PageReference pageRef;
            Rforce_BVMComplet_WS.ApvGetDetVehResponse rep=null;       
            VIN = VIN.trim();
            VRN = VRN.trim();
            LanguageCode = LanguageCode.trim();
            CountryCode = CountryCode.trim();        
            if (VIN != null && VIN != '')
                whereClause = ' where Name = \'' + VIN + '\'';        
            if (VRN != null && VRN != '')
            {
            if (whereClause != '')
               whereClause = whereClause + ' AND VehicleRegistrNbr__c = \'' + VRN + '\'';
            else
                    whereClause = whereClause + ' WHERE VehicleRegistrNbr__c = \'' + VRN + '\'';
            }
            if (whereClause == ''){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.searchVehicleFilterMandatory);
                ApexPages.addMessage(myMsg);
            }
            else{
                query = query + whereClause + ' LIMIT 1';            
            try {
                 vehicleFound = Database.query(query);
                 vehId=vehicleFound.Id;
                 vehName=vehicleFound.Name;               
                 vehicleRelationExists = '0';               
                for (VEH_Veh__c v : VehicleModList){
                 if (v.Id == vehicleFound.Id){
                    vehicleRelationExists= '1';
                    break;
                 }   
                }               
            }catch (QueryException qe){
            if (VIN != null && VIN != ''){ 
               ChampIn='AA';
               ValeurChamp=VIN;                   
               rep = searchBVMComplet();                    
            }else if (VRN != null && VRN != ''){
               ChampIn='HB';
               ValeurChamp=VRN.trim();                                  
               rep = searchBVMComplet();                    
             }
             else{
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, System.label.noVehicleFound + ' ' + System.label.CreateVehicleManually + ' :<b><a href=\'' + urlForLink + '/' + vehiclePrefix + '/e?retURL=' + accountId + '&Name=' + VIN+ '\'>' + System.label.CreateVehicle + '</a></b>');
                  ApexPages.addMessage(myMsg);
             }
             if (rep != null && rep.detVeh != null){
                 pageRef = createVehicleBVM(rep);
             }
              else{
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, System.label.noVehicleFound + ' ' + System.label.CreateVehicleManually + ' :<b><a href=\'' + urlForLink + '/' + vehiclePrefix + '/e?retURL=' + accountId + '&Name=' + VIN+ '\'>' + System.label.CreateVehicle + '</a></b>');
                  ApexPages.addMessage(myMsg);
              }
             }         
                //return pageRef;
            }
        }   
    public Rforce_BVM_WS.ApvGetDetVehXmlResponse searchBVM()
        {
            Boolean Success ;
            string strDebut = 'Début Trace Réponse WS';
            string strFin = 'Fin Trace Réponse WS';
            Rforce_BVM_WS.ApvGetDetVehXmlRequest request = new Rforce_BVM_WS.ApvGetDetVehXmlRequest();               
            Rforce_BVM_WS.ServicePreferences servicePref = new Rforce_BVM_WS.ServicePreferences();
            Rforce_BVM_WS.ApvGetDetVehXml VehWS = new Rforce_BVM_WS.ApvGetDetVehXml();
            Rforce_BVM_WS.ApvGetDetVehXmlResponse VEH_WS = new Rforce_BVM_WS.ApvGetDetVehXmlResponse();      
            try{  
                servicePref.bvmsi25SocEmettrice  = System.Label.SocieteEmettrice;
                servicePref.bvmsi25Vin = VIN;
                servicePref.bvmsi25CodePaysLang = CountryCode; //FRA 
                servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
                request.ServicePreferences = servicePref;             
                VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
                VehWS.clientCertName_x = System.label.RenaultCertificate;           
                VehWS.timeout_x=40000;           
                return (Rforce_BVM_WS.ApvGetDetVehXmlResponse)VehWS.getApvGetDetVehXml(request);
            }            
            catch (Exception e){                    
                system.debug(VEH_WS);
                system.debug('erreur : ' + String.valueOf(e));
                return null;
            }
        }    
        public Rforce_BVMComplet_WS.ApvGetDetVehResponse searchBVMComplet(){
            Boolean Success ;
            Rforce_BVMComplet_WS.ApvGetDetVehRequest request = new Rforce_BVMComplet_WS.ApvGetDetVehRequest();               
            Rforce_BVMComplet_WS.ServicePreferences servicePref = new Rforce_BVMComplet_WS.ServicePreferences();
            Rforce_BVMComplet_WS.ApvGetDetVeh VehWS = new Rforce_BVMComplet_WS.ApvGetDetVeh();
            Rforce_BVMComplet_WS.ApvGetDetVehResponse VEH_WS = new Rforce_BVMComplet_WS.ApvGetDetVehResponse();
            try{  
                servicePref.bvmsi24CodeChampIn  = ChampIn;
                servicePref.bvmsi24CodeChampOut  = ChampOut;
                servicePref.bvmsi24ValeurChamp = ValeurChamp;
                servicePref.bvmsi24CodePaysLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FRA 
                servicePref.bvmsi24CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
                request.ServicePreferences = servicePref;              
                VehWS.endpoint_x = System.label.Url_BVM_Complet; 
                VehWS.clientCertName_x = System.label.RenaultCertificate;           
                VehWS.timeout_x=40000;       
                return (Rforce_BVMComplet_WS.ApvGetDetVehResponse)VehWS.getApvGetDetVeh(request);
            }catch (Exception e){                    
                system.debug(VEH_WS);
                system.debug('erreur : ' + String.valueOf(e));
                return null;
            }
        }   
        public PageReference createVehicleBVM(Rforce_BVMComplet_WS.ApvGetDetVehResponse vehBVM)
        {
             VEH_Veh__c veh = new VEH_Veh__c();
             if (vehBVM.detVeh.bvmso24Vin != null)
                veh.Name = vehBVM.detVeh.bvmso24Vin;            
             if (vehBVM.detVeh.bvmso24NImmat != null)
                veh.VehicleRegistrNbr__c = vehBVM.detVeh.bvmso24NImmat;           
             if (vehBVM.detVeh.bvmso24DateImmat1 != null && vehBVM.detVeh.bvmso24DateImmat1.length() > 7)
                veh.FirstRegistrDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(0, 2)));                   
              if (vehBVM.detVeh.bvmso24DateImmat != null && vehBVM.detVeh.bvmso24DateImmat.length() > 7)
                veh.LastRegistrDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(0, 2)));          
              if (vehBVM.detVeh.bvmso24NRecCom != null)
                veh.CommunityReceptionCode__c = vehBVM.detVeh.bvmso24NRecCom;         
              if(vehBVM.detVeh.bvmso24PaysImmat !=null)
                veh.RegistrationCountrycode__c = vehBVM.detVeh.bvmso24PaysImmat;        
              if(vehBVM.detVeh.bvmso24RefBoi != null)
                veh.GearboxPN__c = vehBVM.detVeh.bvmso24RefBoi;       
             if(vehBVM.detVeh.bvmso24RefMot !=null)
                veh.EnginePN__c = vehBVM.detVeh.bvmso24RefMot;       
             if(vehBVM.detVeh.bvmso24LibUsimot !=null)
                veh.EngineAssemblyFactoryName__c = vehBVM.detVeh.bvmso24LibUsimot;       
             if(vehBVM.detVeh.bvmso24LibUsiboi != null)
                veh.GearboxAssemblyFactoryName__c =  vehBVM.detVeh.bvmso24LibUsiboi;              
             if(vehBVM.detVeh.bvmso24LibUsiveh != null)
               veh.AssemblyFactoryName__c = vehBVM.detVeh.bvmso24LibUsiveh;         
             if(vehBVM.detVeh.bvmso24Ligne2P12 != null)
                veh.Line2OvalPlate__c = vehBVM.detVeh.bvmso24Ligne2P12;       
             if(vehBVM.detVeh.bvmso24PlacSpec != null)
                veh.SpecificPlateLine__c = vehBVM.detVeh.bvmso24PlacSpec;       
             if(vehBVM.detVeh.bvmso24Ligne3P12 != null)
                veh.Line3OvalPlate__c = vehBVM.detVeh.bvmso24Ligne3P12;       
            if(vehBVM.detVeh.bvmso24Ligne4P12 != null)
                veh.Line4OvalPlate__c = vehBVM.detVeh.bvmso24Ligne4P12;      
             if(vehBVM.detVeh.bvmso24Ptmaav != null)
               veh.MaxGrossWeightofFrontAxle__c = vehBVM.detVeh.bvmso24Ptmaav;         
             if(vehBVM.detVeh.bvmso24Ptr != null)
                veh.GrossTrainWeight__c = vehBVM.detVeh.bvmso24Ptr;       
              if(vehBVM.detVeh.bvmso24Ptma !=null)
                veh.MaxGrossVehicleWeight__c = vehBVM.detVeh.bvmso24Ptma;           
             if(vehBVM.detVeh.bvmso24Ptmaar != null)
                veh.MaxWeightRearAxle__c = vehBVM.detVeh.bvmso24Ptmaar;                  
             //VersionCode__c p460:version
              if (vehBVM.detVeh.bvmso24Version != null)
                veh.VersionCode__c = vehBVM.detVeh.bvmso24Version;          
             //DeliveryDate__c p460:dateLiv
            if (vehBVM.detVeh.bvmso24DateLiv != null && vehBVM.detVeh.bvmso24DateLiv.length() > 7)
                veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(0, 2)));        
            if (vehBVM.detVeh.bvmso24PaysLiv != null)
                veh.CountryOfDelivery__c = vehBVM.detVeh.bvmso24PaysLiv;                      
            //AfterSalesType__c p460:tvv
            if (vehBVM.detVeh.bvmso24Tvv != null)
                veh.AfterSalesType__c = vehBVM.detVeh.bvmso24Tvv;      
            //BodyType__c p460:libCarrosserie
            if (vehBVM.detVeh.bvmso24LibCarrosserie != null)
                veh.BodyType__c = vehBVM.detVeh.bvmso24LibCarrosserie;      
            //DateofManu__c p460:dateTcmFab 
            if (vehBVM.detVeh.bvmso24DateTcmFab != null && vehBVM.detVeh.bvmso24DateTcmFab.length() > 7)
                veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(0, 2)));       
            //Description__c p460:libModel + p460:ligne2P12 + p460:ligne3P12 + p460:ligne4P12
                veh.Description__c = vehBVM.detVeh.bvmso24LibModel + ' ' + vehBVM.detVeh.bvmso24Ligne2P12 + ' ' + vehBVM.detVeh.bvmso24Ligne3P12 + ' ' + vehBVM.detVeh.bvmso24Ligne4P12;        
            //EngineIndex__c p460:indMot
            if (vehBVM.detVeh.bvmso24IndMot != null)
                veh.EngineIndex__c = vehBVM.detVeh.bvmso24IndMot;           
            //EngineManuNbr__c p460:NMot
            if (vehBVM.detVeh.bvmso24NMot != null)
                veh.EngineManuNbr__c = vehBVM.detVeh.bvmso24NMot;               
            //EngineType__c p460:typeMot 
            if (vehBVM.detVeh.bvmso24TypeMot != null)
                veh.EngineType__c = vehBVM.detVeh.bvmso24TypeMot;           
            //GearBoxIndex__c p460:indBoi 
            if (vehBVM.detVeh.bvmso24IndBoi != null)
                veh.GearBoxIndex__c = vehBVM.detVeh.bvmso24IndBoi;           
            //GearBoxManuNbr__c p460:NBoi
            if (vehBVM.detVeh.bvmso24NBoi != null)
                veh.GearBoxManuNbr__c = vehBVM.detVeh.bvmso24NBoi;          
            //GearBoxType__c p460:typeBoi
            if (vehBVM.detVeh.bvmso24TypeBoi != null)
                veh.GearBoxType__c = vehBVM.detVeh.bvmso24TypeBoi;       
            //Model__c p460:libModel 
            if (vehBVM.detVeh.bvmso24LibModel != null)
                veh.Model__c = vehBVM.detVeh.bvmso24LibModel;           
            //VehicleBrand__c p460:marqCom 
            if (vehBVM.detVeh.bvmso24MarqCom != null)
                veh.VehicleBrand__c = vehBVM.detVeh.bvmso24MarqCom;           
            //VehicleManuNbr__c p460:NFab
            if (vehBVM.detVeh.bvmso24NFab != null)
                veh.VehicleManuNbr__c = vehBVM.detVeh.bvmso24NFab;                  
            if (vehBVM.detVeh.bvmso24CodeModel2 != null)
                veh.ModelCode__c = vehBVM.detVeh.bvmso24CodeModel2; 
            if (vehBVM.detVeh.bvmso24DataCrit != null){
                 String nrj;
                 String cdnrj;             
                    for (integer i = 0; i < vehBVM.detVeh.bvmso24DataCrit.length(); i++){       
                     if (vehBVM.detVeh.bvmso24DataCrit.substring(i, i + 3) == '019'){
                        nrj = vehBVM.detVeh.bvmso24DataCrit.substring(i, i + 6);                             
                     }
                    i = i + 97;
                    }            
                 cdnrj=mapNrj(nrj);           
                 veh.EnergyType__c =cdnrj;            
            }      
            try{
                insert veh;
                vehId=veh.Id;
                vehicleRelationExists='0';
            }catch(DMLException dmle){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSaveVehicle);
                ApexPages.addMessage(myMsg);
                return null;
            }      
            return new PageReference('/apex/Rforce_CreateVehicleRelation_Page?accountId=' + accountId + '&vehicleId=' + veh.Id);
        }    
        public string mapNrj(String cd) {   
        String EssenceTest;   
        if (cd == System.Label.VEH_BVM_Data019001)
                EssenceTest = System.Label.VEH_Essence_ELEC;
            else if (cd == System.Label.VEH_BVM_Data019002)
                EssenceTest = System.Label.VEH_Essence_FLEXFL;
            else if (cd == System.Label.VEH_BVM_Data019003)
                EssenceTest = System.Label.VEH_Essence_CNGS;
            else if (cd == System.Label.VEH_BVM_Data019004)
                EssenceTest = System.Label.VEH_Essence_BIODIE;
            else if (cd == System.Label.VEH_BVM_Data019006)
                EssenceTest = System.Label.VEH_Essence_BIOESS;
            else if (cd == System.Label.VEH_BVM_Data019008)
                EssenceTest = System.Label.VEH_Essence_GNV;
            else if (cd == System.Label.VEH_BVM_Data019010)
                EssenceTest = System.Label.VEH_Essence_ESS;
            else if (cd == System.Label.VEH_BVM_Data019011)
                EssenceTest = System.Label.VEH_Essence_ESSSPB;
            else if (cd == System.Label.VEH_BVM_Data019020)
                EssenceTest = System.Label.VEH_Essence_DIESEL;
            else if (cd == System.Label.VEH_BVM_Data019030)
                EssenceTest = System.Label.VEH_Essence_GPL;
            else if (cd == System.Label.VEH_BVM_Data019035)
                EssenceTest = System.Label.VEH_Essence_BICARB;
            else if (cd == System.Label.VEH_BVM_Data019999)
                EssenceTest = System.Label.VEH_Essence_SAN019;    
        return EssenceTest;    
    }    
        public PageReference skip(){        
            if (Test.isRunningTest()){        
                Account acc = new Account(Name='Test1',Phone = '+1234',ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingPostalCode = '75013', ShippingStreet = 'my street',Country__c='France');    
                insert acc;
                accountId=acc.Id;            
                try{     
                 nameFromBhrs = [SELECT Name FROM BusinessHours where Name =:acc.Country__c].Name;          
                }catch(Exception e){
                        system.debug(e);
                 }                              
              if(nameFromBhrs==null){
                    businessHrs='Default'; 
              }
              if(nameFromBhrs != null){
                    businessHrs='nameFromBhrs'; 
              }  
              Account acc1 = new Account(Name='Test1',Phone = '+1234' , ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Romania', ShippingCountry = 'Romania', ShippingPostalCode = '75013', ShippingStreet = 'my street',Country__c='Romania');   
              insert acc1;
              accountId=acc1.Id;                
               try{     
               nameFromBhrs = [SELECT Name FROM BusinessHours where Name =:acc1.Country__c].Name;          
               }catch(Exception e1){
                        system.debug(e1);
                }
                if(nameFromBhrs==null){
                    businessHrs='Default'; 
                }
                if(nameFromBhrs != null){
                    businessHrs='nameFromBhrs'; 
                }            
            }
             if(isRforceCommunityUser())
                return new PageReference('/500/e?retURL=' + accountId + '&def_account_id=' + accountId+'&'+System.Label.Rforce_Country_Case+'='+countryFromAcc+'&BusinessHours='+businessHrs);       
             else
                return null;
        }   
        public PageReference cancel(){
          if(isRforceCommunityUser())
            return new PageReference('/' + accountId);
           else 
            return null; 
        }
        public void srveh(){
            vehId='a0126000000qaL7';
        }
        public boolean isRforceCommunityUser(){
            User u=[Select Id,Profile.Name, Profile.UserLicense.Name from User where Id=: UserInfo.getUserId()];
              if(u.Profile.UserLicense.Name=='Gold Partner')
                  return true;
              else 
                  return false;    
        }
    }