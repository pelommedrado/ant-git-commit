@isTest
public class Rforce_CreateCaseAttach_VOCC_TST{
    private static testMethod void webServiceVOCC(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
                          LocaleSidKey='fr', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com');
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR',Country_Code_3L__c = 'FRA', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr; 
        System.runAs(u){
        Test.startTest();                         
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        Account Acc = new Account(Name='Acc1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
        Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRGGGG3285766',DeliveryDate__c=date.parse('02/02/2007'));
        insert veh;    
        Case c1 = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id,VIN__c=veh.Id);
        insert c1;   
         
        String strparentNumber=[Select casenumber from Case where Id=:c1.Id].caseNumber;
         
        Rforce_W2CSchema_CLS.Casedetails casedetail=new Rforce_W2CSchema_CLS.Casedetails();
        casedetail.caseNumber=strparentNumber;
        casedetail.Priority='High';
        casedetail.emailOptIn='True';
        casedetail.description='test web service';
        casedetail.licenseNumber='asdfasf';
        casedetail.suppliedPhone='455656';
        casedetail.City='France';
        casedetail.postalCode='605004';
        casedetail.address='test';
        casedetail.lastName='lname';
        casedetail.firstName='fname';
        casedetail.comAgreement='true';
        casedetail.state='Paris';
        casedetail.vin='12345678912345';
        casedetail.dealer='test';
        casedetail.suppliedEmail='test123@gmail.com';
        casedetail.country='France';
        casedetail.language='French';
        casedetail.caseFrom='Customer';
        casedetail.detail='';
        casedetail.subType='VOC Call Center';
        casedetail.type='Complaint';
        casedetail.subSource= 'VOC Call Center';
        casedetail.caseBrand='Renault';
        casedetail.origin='VOC';
        casedetail.subject='test';
        casedetail.cellPhone='3454';
        casedetail.company='dfgdf';
        casedetail.title='Mr.';
        casedetail.customerExperience='test-test-test';
        Case c= new Case();
        Rforce_CreateCaseAttach_VOCC_CLS.createCase(casedetail,c);
        Test.stopTest();
       } 
    }
}