/* 
* Classe envia mensagem de aquecimento para o Lead no aquecimento DVR - Por Edvaldo Fernandes @kolekto
* 
*/
global class LMT_02_warmLeadController{
    
    WebService static String buttonWarmLead(Id leadId){
        
        String message;
        Lead lead = [
            SELECT  Date_Last_Not_Interest__c,
                    LastHeating__c,
                    LastHeatingUser__c
            FROM    Lead
            WHERE   Id =: leadId
            LIMIT 1
        ];
        
        //Não envia mensagem se campo estiver nulo
        if( lead.Date_Last_Not_Interest__c == null ){
            message = null;
        }
        
        //envia a mensagem se diferente de nulo e com mais de 3 meses de "Não interesse"
        if( lead.Date_Last_Not_Interest__c != null && system.today() < lead.Date_Last_Not_Interest__c.addDays(90) ){
            
            //Converte a data para padrão Brasil
            String dataForm = formataData(lead.Date_Last_Not_Interest__c);
            //Soma a data com 3 meses
            Date sumdate = lead.Date_Last_Not_Interest__c + 90;
            String dataMoreThreeMonths = formataData(sumdate);
            
            message = 'Este lead foi marcado como Sem Interesse em ' + dataForm +
                ' e só poderá ser trabalhado novamente após '+ dataMoreThreeMonths +'.';

            return message;
        }


        if(lead.LastHeating__c != null && System.now() < lead.LastHeating__c.addMinutes(15) && lead.LastHeatingUser__c != UserInfo.getUserId()){
            
            message = 'Este lead está sendo atendido por outro analista!';
            
            return message;
            
        }

        lead.LastHeating__c = System.now();
        lead.LastHeatingUser__c = UserInfo.getUserId();
        update lead;

        return message;      
    }
    
    //converte a data para padrão Brasil
    public static String formataData(Date data){
        
        String dataFormatada;       
        if(data != null){
            String dia = String.valueOf(data.day());
            if(dia.length() == 1)
                dia = '0'+dia;
            String mes = String.valueOf(data.month());
            if(mes.length() == 1)
                mes = '0'+mes;
            String ano = String.valueOf(data.year());
            dataFormatada = dia+'/'+mes+'/'+ano;
        }        
        return dataFormatada;
    }
    
}