@isTest
private class ConexaoRenaultBatchTest {

    static {
      FaturaDealer__c fat = new FaturaDealer__c();
      fat.NFTIPREG__c = '10';
      fat.NFBIRENV__c = '07600999';
      fat.NFBIREMI__c = '07600997';
      fat.NFSEQUEN__c = '2780';
      fat.NFNROREG__c = '00029';
      fat.NFBANDEI__c = 'REN';
      fat.NFCPFCGC__c = '37970';
      insert fat;

      fat = new FaturaDealer__c();
      fat.NFTIPREG__c = '10';
      fat.NFBIRENV__c = '07600999';
      fat.NFBIREMI__c = '07600999';
      fat.NFSEQUEN__c = '2780';
      fat.NFNROREG__c = '00029';

      fat.NFBANDEI__c = 'REN';
      fat.NFCODOPR__c = '25';
      fat.NFCODFIS__c = '5403  VN';
      fat.NFTIPONF__c = 'VN';
      fat.NFNRONFI__c = '00073295';
      fat.NFCHASSI__c = '11111111111111911';

      fat.NFSERNFI__c = 'U';
      fat.NFDTANFI__c = '20150827';
      fat.NFNOMCLI__c = 'ITAMAR JOSE VIEIRA FERNANDES';
      fat.NFTIPVIA__c = 'RUA';
      fat.NFNOMVIA__c = 'Manoel Mancellos Moura';

      fat.NFNROVIA__c = '681';
      fat.NFCPLEND__c = 'AP 103';
      fat.NFBAIRRO__c = 'Canasvieiras';
      fat.NFCIDADE__c = 'FLORIANÓPOLIS';
      fat.NFNROCEP__c = '88054030';

      fat.NFESTADO__c = 'SC';
      fat.NFPAISRE__c = '00';
      fat.NFESTCIV__c = '';
      fat.NFSEXOMF__c = 'M';
      fat.NFDTANAS__c = '195908';
      fat.NFTIPCLI__c = 'F';
      fat.NFCPFCGC__c = '37970380972';

      fat.NFEMAILS__c = 'pelommedrado@gmail.com';
      fat.NFDDDRES__c = '11';
      fat.NFTELRES__c = '77778888';
      fat.NFDDDCEL__c = '11';
      fat.NFTELCEL__c = '99996666';
      fat.NFANOFAB__c = '2000';
      fat.NFANOMOD__c = '2000';
      fat.NFCODOPR_Mirror__c = '19';
      fat.NFDTANFI_Mirror__c = System.today();
      fat.RecordTypeId = FaturaDealerServico.VALID;
      INSERT fat;
    }

    @isTest
    static void itShouldConexaoRenaultBatch() {
        Database.executeBatch(new ConexaoRenaultBatch());
        List<FaturaDealer__c> fatList = [
            SELECT Id, Error_Messages__c, RecordType.Name
            FROM FaturaDealer__c
        ];

        System.assertEquals(false, fatList.isEmpty());
	}

  static testMethod void itShouldConexaoRenaultSchedulable() {
    String chron = '0 0 23 * * ?';
    System.schedule('Test Sched', chron, new ConexaoRenaultBatch());
  }
  
}