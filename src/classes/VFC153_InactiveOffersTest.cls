@isTest
private class VFC153_InactiveOffersTest {

    static testMethod void myUnitTest() {
    
    
    User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
     Database.insert( manager );
        
    Account acc = new Account(
      Name = 'DealerAcc', 
        ProfEmailAddress__c='email@email.com',
      ShippingCity = 'Cidade', 
      ShippingState = 'Estado', 
      NameZone__c = 'R2', 
      TV_Signal__c = 'Prata',
      Active_PV__c = true 
      //RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
    );
    Database.insert( acc );

    Model__c model = new Model__c(
      Status__c = 'Active',
            Model_Spec_Code__c = 'ABC',
            Phase__c = '1',
            Model_PK__c = 'ABC-1'
    );
    Database.insert( model );

    PVVersion__c version = new PVVersion__c( 
      Model__c = model.Id, 
      Version_Id_Spec_Code__c = 'ABC', 
      Price__c = 100000, 
      PVC_Maximo__c = 100, 
      PVR_Minimo__c = 100
    );
    Database.insert( version );

   

    PVCommercial_Action__c commAction = new PVCommercial_Action__c(
      Model__c = model.Id,
      Start_Date__c = System.today().addDays( -5 ),
      End_Date__c = System.today().addDays( 5 ),
      Name = 'Commercial Action',
      Status__c = 'Active'
    );
    Database.insert( commAction );

    PVCall_Offer__c callOffer = new PVCall_Offer__c(
      Commercial_Action__c = commAction.Id,
      Call_Offer_Start_Date__c = system.today().addDays( -3 ),
      Call_Offer_End_Date__c = System.today().addDays( 3 )
    );
    Database.insert( callOffer );

    Offer_Item__c pricingTemplate = new Offer_Item__c(
        Name = 'Pricing Template', 
        RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Pricing_Template'),
        Code__c = 'PT'
    );
    Database.insert(pricingTemplate);

    Group_Offer__c groupOffer = new Group_Offer__c(
      Type_of_Offer__c = 'Cooperada',
      Number_Offer__c = '1',
      Type_of_Action__c = 'Internet',
      Date_Start_Offer__c = System.today().addDays( -1 ),
      Date_End_Offer__c = System.today().addDays( 1 )
    );

    Database.insert( groupOffer );

    Offer__c offer = new Offer__c( 
      Group_Offer__c = groupOffer.Id,
      From_To_Offer__c = false,
      hasUpgrade__c = false,
      Offer_to_Internet__c = true,
      Total_Inventory_Vehicle__c = 3,
      Model__c = model.Id,
      Version__c = version.Id,
      //Optional__c = optionalList[0].Id,
      //Painting__c = optionalList[1].Id,
      Featured_Product_Text__c = 'air conditioning;steering;electric Lock',
      
      Condition__c = 'Financiado',
      Commercial_Action__c = commAction.Id,
      ValueTo__c = 33020,
      Minimum_Input__c = 20,
      Number_of_Installments__c = 60,
      Monthly_Tax__c = 0.19,
      Coefficient__c = 0.0336,
      Entry_Value__c = 6604,
      Installment_Value__c = 968.27,
      Stamp__c = 'Marketing of factory;Big marketing in factory',
      Offer_Start_Date_Website__c = System.today().addDays( -1 ),
      Offer_End_Date_Website__c = System.today().addDays( 1 ),
      Pricing_Template__c = 'Sight',
      Featured_In_Offer__c = 'Complete',
      Stage__c = 'Approved',
      Pricing_Template_Lookup__c = pricingTemplate.Id
    );

    Database.insert( offer );
   
     callOffer.Status__c = 'Inactive';
    Database.update(callOffer);
    callOffer.Status__c ='Active';
	Database.update(callOffer);
    offer.Status__c = 'ACTIVE';
    Database.update(offer);
        

    commAction.Status__c = 'Inactive';
    Database.update(commAction);
    
    }
    
}