@isTest	
public with sharing class SCC_TD_Scan_WS_TEST {
	
	// -------------------------------------
    // trackTriggerInsertTest() method
    // -------------------------------------      
    static testMethod void scanPagesTest() {
        test.startTest();
        
        JSONParser oParser = JSON.createParser(SCC_TD_Scan_WS.scanPages());
        map<String, String> mapInfos = (map<String, String>)oParser.readValueAs(map<String, String>.class);
        
        system.debug('### SCC_TD_Scan_WS_TEST - scanPagesTest() - mapInfos: ' + mapInfos);

        system.assertEquals(Decimal.valueOf(mapInfos.get('STORAGE_USAGE')), 0.048500000);
        system.assertEquals(Decimal.valueOf(mapInfos.get('API_USAGE')), 345497);
        system.assertEquals(Decimal.valueOf(mapInfos.get('CODE_USED')), 20.5);
        system.assertEquals(Decimal.valueOf(mapInfos.get('DATA_STORAGE_USED')), 41);
        system.assertEquals(Decimal.valueOf(mapInfos.get('FILE_STORAGE_USED')), 7);
        system.assertEquals(Decimal.valueOf(mapInfos.get('NB_ACCOUNTS')), 2357789);
        
        test.stopTest();
    }
}