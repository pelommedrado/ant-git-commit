/** Apex class to crypt and uncrypt informations used by LMT **/	
public without sharing class LMT_Crypto_CLS {
	
	/** @return a valid key for this class to be used to encrypt **/
	public static String getValidKey() {
		Blob cryptoKey = Crypto.generateAesKey(128);
		return EncodingUtil.base64Encode(cryptoKey);
	}
	
	/** @return an encoded string with the key stored in the custom label **/
	public static String encode(String word, String key) {
		try {
	        system.debug('##### LMT_SBOLCreateAppointment_EXT - <encode> - BEGIN: ' + word);
	        
	        Blob cryptoKey = EncodingUtil.base64Decode( key );
	        Blob blobWord = Blob.valueOf(word);
	        Blob encryptWord = Crypto.encryptWithManagedIV('AES128', cryptoKey, blobWord);
	        String encryptWordStr = Encodingutil.base64Encode(encryptWord);
	        
	        system.debug('##### LMT_SBOLCreateAppointment_EXT - <encode> - END: ' + encryptWordStr); 
	        
	        return encryptWordStr; 
    	} catch (Exception e) {
    		system.debug('##### LMT_SBOLCreateAppointment_EXT - <encode> - exception: ' + e.getMessage()); 
    		return null;
    	}
	}
	
    /** Keep the LMT functionnality
    	@return an encoded string with the key stored in the custom label
    **/
    public static String encode(String word) {
    	return encode(word, Label.LMT_Key);
    }
    
    /** @return an decoded string with the key stored in the custom label **/
    public static String decode(String value, String key) {
    	try {
	        system.debug('##### LMT_SBOLCreateAppointment_EXT - <decode> - BEGIN: ' + value);
	        
	        Blob cryptoKey = EncodingUtil.base64Decode( key ); 
	        Blob valueBlob = EncodingUtil.base64Decode(value);
	        Blob decryptValue = Crypto.decryptWithManagedIV('AES128', cryptokey, valueBlob);
	        String decryptValueStr = decryptValue.toString();
	        
	        system.debug('##### LMT_SBOLCreateAppointment_EXT - <decode> - END: ' + decryptValueStr);
	        
	        return decryptValueStr; 
    	} catch (Exception e) {
    		system.debug('##### LMT_SBOLCreateAppointment_EXT - <decode> - exception: ' + e.getMessage());
    		return null;
    	}
    }
    
    /** Keep the LMT functionnality
    	 @return an decoded string with the key stored in the custom label
   	**/
    public static String decode(String value) {
    	return decode(value, Label.LMT_Key);
    }
}