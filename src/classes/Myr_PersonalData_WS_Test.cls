/**
 * This class is used to Test the Personal Data general WS 
 
 */
@isTest
private class Myr_PersonalData_WS_Test { 
     
     
     
     
     
     
    @testsetup 
    static void prepareCustomSettings() {
        Myr_Datasets_Test.prepareMyrCustomSettings();
		list<WebServicesInfo__c> listws= new list<WebServicesInfo__c>();
		WebServicesInfo__c wsinfo = new WebServicesInfo__c();  
		wsinfo.name = 'MDM'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CMDM_Renault_uat_1_6';  
		listws.add(wsinfo); 
		wsinfo = new WebServicesInfo__c(); 
		wsinfo.name = 'BCS'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/BCS_CrmGetCustDataService';
		listws.add(wsinfo);
		wsinfo = new WebServicesInfo__c(); 
		wsinfo.name = 'RBX'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CrmGetCustData_4_1';
		listws.add(wsinfo);
		insert listws; 
		MDMCivillity__c civility = new MDMCivillity__c();
		civility.Name = 'MDM-M';
		civility.value__c = 'M';
		civility.Civility__c = 'Mr.';
		civility.dataSource__c='MDM';
		insert civility;
		Gender__c genderM = new Gender__c();
		genderM.Name = 'MDM-M';
		genderM.Country__c = '';
		genderM.value__c = 'M';
		genderM.Gender__c = 'Man';
		genderM.datasource__c='MDM';
		insert genderM;
		MDMPartySubType__c subType = new MDMPartySubType__c();
		subType.Name = 'MDM-AC';
		subType.dataSource__c = 'MDM';
		subType.sub__c = 'Craftsman/Retailer';
		subType.value__c = 'AC';
		insert subType;
		MDMFinancialStatus__c finance = new MDMFinancialStatus__c();
		finance.Name = 'MDM-ACT';
		finance.datasource__c = 'MDM';
		finance.value__c = 'ACT';
		finance.FinancialStatus__c = 'ACT';
		insert finance;
		MaritalStatus__c marital = new MaritalStatus__c();
		marital.Name = 'MDM-Single';
		marital.value__c = 'Single';
		marital.datasource__c = 'MDM';
		marital.MaritalStatus__c = 'Single';
		insert marital;
		MDMCategory__c category = new MDMCategory__c();
		category.Name = 'MDM-B1';
		category.value__c = 'B1';
		category.datasource__c = 'MDM';
		category.JobClass__c = 'Employee';
		insert category;
		MDMPartySegment__c partySeg = new MDMPartySegment__c();
		partySeg.Name = 'MDM-Others';
		partySeg.value__c = 'Others';
		partySeg.datasource__c = 'MDM';
		partySeg.PartySegment__c = 'Others';
		insert partySeg;
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Turkey', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('MidCE', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Nordic', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Poland', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    } 
  
  
  
	  static testMethod void requestparam() 
	  {
	  	 //Myr_PersonalData_WS myr = new Myr_PersonalData_WS(); 
	  	 Myr_PersonalData_WS.RequestParameters param = new Myr_PersonalData_WS.RequestParameters();
	  	 
	  	 param.user = new user(); 
	  	 param.vin = '4556'; 
	  	 param.registration = '54'; 
	  	 param.country ='fra';
	  	 param.lastname ='test'; 
	  	 param.firstname ='test'; 
	  	 param.brand = 'test'; 
	  	 param.email = 'test@test.col';
	  	 param.city ='test'; 
		 param.zip = 'test'; 
		 param.idClient = 'test'; 
		 param.ident1 = 'test';
		 param.strLocalID = 'test'; 
		 param.strFixeLandLine = '02154054'; 
		 param.strmobile = '052154054';
		 param.strPartyID = '25454';
		 param.nbReplies = 1; 
		 param.demander = 're7_SRC'; 
		 param.user_x = 'test'; 
		 	  
	  }
  
  
  
  
  /** Test with no country **/
  static testMethod void testUnitNoCountry() {
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'France';
        c2.Country_Code_2L__c = '';
        c2.Country_Code_3L__c = '';
        c2.DataSource__c = '';
        c2.Language__c = 'FR';
        insert c2;
    //Mock: 
        Test.setMock(WebServiceMock.class, new AsyncTRVwsdlmdm());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.country='France'; 
         request.brand='RENAULT';
//         request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
		 system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			 Test.startTest();  
			   TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			 Test.stopTest();
			 system.assertEquals(null, response);
		 }
  }

  /** Test MDM **/
    static testMethod void testUnitMDM() {
        //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataMDM_MK(Myr_PersonalDataMDM_MK.MODE.MYRENAULT));
    
        Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'France';
        c2.Country_Code_2L__c = 'FR';
        c2.Country_Code_3L__c = 'FRA';
        c2.DataSource__c = 'MDM';
        c2.Language__c = 'FR';
        insert c2;
        
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.country='France'; 
         request.brand='RENAULT';
//         request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
		 system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			Test.startTest();     
			 TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request); 
			 Test.stopTest();
			 system.assertNotEquals(null, response);
	//		 system.assertEquals( Myr_PersonalData_WS.DataSourceType.MDM, response.response.dataSource);
		 }
    }
    
    /** Test SIC **/
     static testMethod void testUnitSIC() {
        Country_Info__c c3 = new Country_Info__c();
        c3.Name = 'Italy';
        c3.Country_Code_2L__c = 'IT';
        c3.Country_Code_3L__c = 'ITA';
        c3.DataSource__c = 'SIC';
        c3.Language__c = 'IT';
        insert c3;
        //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataSIC41_WS_MK());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.country='Italy'; 
         request.brand='RENAULT';
         //request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
		 system.runAs( Myr_Datasets_Test.getTechnicalUser('Italy') ) {
			 Test.startTest();    
			   TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			 Test.stopTest();
			 system.assertNotEquals(null, response);
			//system.assertEquals( Myr_PersonalData_WS.DataSourceType.SIC, response.response.dataSource);
		 }
     }
     
     /** Test BCS **/
     static testMethod void testUnitBCS() {
       Country_Info__c c1 = new Country_Info__c();
        c1.Name = 'Turkey';
        c1.Country_Code_2L__c = 'TR';
        c1.Country_Code_3L__c = 'TRY';
        c1.DataSource__c = 'BCS';
        c1.Language__c = 'TR';
        insert c1;
      //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataBCS10_WS_MK());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.country='Turkey'; 
         request.brand='RENAULT';
		 system.runAs( Myr_Datasets_Test.getTechnicalUser('Turkey') ) {
			 Test.startTest();    
			   TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			 Test.stopTest();
			 system.assertNotEquals(null, response);
//			 system.assertEquals( Myr_PersonalData_WS.DataSourceType.BCS, response.response.dataSource);
		 }
     }
     
     /** Test with DefaultCountry = MidCe **/
   /*  static testMethod void testUnitMidCE() {
       Country_Info__c c1 = new Country_Info__c();
        c1.Name = 'Slovakia';
        c1.Country_Code_2L__c = 'SK';
        c1.Country_Code_3L__c = 'SVK';
        c1.DataSource__c = 'BCS';
        c1.Language__c = 'Slovak';
        c1.TypeOfCountry__c = 'MidCE';
        insert c1;
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'MidCE';
        c2.Country_Code_2L__c = '';
        c2.Country_Code_3L__c = '';
        c2.DataSource__c = '';
        c2.Language__c = 'English';
        c2.TypeOfCountry__c = 'MidCE';
        insert c2;
        User usr = Myr_Datasets_Test.getTechnicalUser('MidCE');
        //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataBCS10_WS_MK());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com'; 
         request.brand='RENAULT';
        // request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
         request.user = usr;
         request.country = 'Slovakia';
		 system.runAs( usr ) {
			 Test.startTest();
			   TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			 Test.stopTest();
			 system.assertNotEquals(null, response);
			// system.assertEquals( Myr_PersonalData_WS.DataSourceType.BCS, response.response.dataSource);
		 }
     }
     */
     /** Test with DefaultCountry = Nordic ===> Call BCS **/
   /*  static testMethod void testUnitNordic() {
		Country_Info__c c1 = new Country_Info__c();
        c1.Name = 'Sweden';
        c1.Country_Code_2L__c = 'SE';
        c1.Country_Code_3L__c = 'SWE';
        c1.DataSource__c = 'BCS';
        c1.Language__c = 'Swedish';
        c1.TypeOfCountry__c = 'Nordic';
        insert c1;
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'Nordic';
        c2.Country_Code_2L__c = '';
        c2.Country_Code_3L__c = '';
        c2.DataSource__c = '';
        c2.Language__c = 'English';
        c2.TypeOfCountry__c = 'Nordic';
        insert c2;

        User usr = Myr_Datasets_Test.getTechnicalUser('Nordic');
        //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataBCS10_WS_MK());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.brand='RENAULT';
         request.country = 'Sweden';
       //  request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
         request.user = usr;
		 system.runAs(usr) {
         Test.startTest();
			TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			system.assertNotEquals(null, response);
	//		system.assertEquals( Myr_PersonalData_WS.DataSourceType.BCS, response.response.dataSource);
			Test.stopTest();
		 }
     }*/
     
     /** Test with DefaultCountry = Poland **/
    /* static testMethod void testUnitPoland() {
       Country_Info__c c1 = new Country_Info__c();
        c1.Name = 'Lithuania';
        c1.Country_Code_2L__c = 'LT';
        c1.Country_Code_3L__c = 'LTU';
        c1.DataSource__c = 'SIC';
        c1.Language__c = 'Polish';
        c1.TypeOfCountry__c = 'Poland';
        insert c1;
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'Poland';
        c2.Country_Code_2L__c = '';
        c2.Country_Code_3L__c = '';
        c2.DataSource__c = '';
        c2.Language__c = 'English';
        c2.TypeOfCountry__c = 'Poland';
        insert c2;
        User usr = Myr_Datasets_Test.getTechnicalUser('Poland');

        //Mock: 
        Test.setMock(WebServiceMock.class, new Myr_PersonalDataSIC41_WS_MK());
        //Request: 
        TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
         request.lastname = 'TEST';
         request.firstname = 'TEST';
         request.email='test@test.com';
         request.brand='RENAULT';
         request.country = 'Lithuania';
        // request.callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
         request.user = usr;
		 system.runAs( Myr_Datasets_Test.getTechnicalUser('Poland') ) {
			 Test.startTest();
			   TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request);
			 Test.stopTest();
			 system.assertNotEquals(null, response);
			// system.assertEquals( Myr_PersonalData_WS.DataSourceType.SIC, response.response.dataSource);
		 }
     }*/
}