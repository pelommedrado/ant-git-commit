/**
* @author Vinoth Baskaran(RNTBCI)
* @date 28/07/2015
* @description This Class is an Custom Soap service and it will create case and attachement accordding to the user input.
*/
global class Rforce_CreateCaseAttach_VOCD_CLS {

/**
* @author Vinoth Baskaran
* @date 28/07/2015
* @description It accepts the Casedetails from the external service.
* @param case Details
* @return String value according to request
*/
    public static boolean createCase(Rforce_W2CSchema_CLS.Casedetails vocdCaseDetails,Case c){
    boolean bCasInsert;
        if(vocdCaseDetails.origin ==System.label.Rforce_CAS_VOCD_Origin && vocdCaseDetails.type ==System.label.Rforce_CAS_VOCD_Type && vocdCaseDetails.subType==System.label.Rforce_CAS_VOC_Dealer && vocdCaseDetails.subSource==System.label.Rforce_CAS_VOC_Dealer &&
             (vocdCaseDetails.detail == system.label.Rforce_CAS_VOC_Dealer_Sales || vocdCaseDetails.detail==system.label.Rforce_CAS_VOC_Dealer_Service) && vocdCaseDetails.suppliedEmail !='' &&
           vocdCaseDetails.country!=''  && vocdCaseDetails.customerExperience != '' && vocdCaseDetails.reasonOfReturn !='' && vocdCaseDetails.problemSolved !='' && vocdCaseDetails.brandRecommendation != ''){
           
                  
            c.Subject=system.label.Rforce_CAS_VOCD_Subject;           
            c.Priority=system.label.Rforce_CAS_Priority_Urgent;                           
            c.Guaranty_Code__c=vocdCaseDetails.guarantyCode;            
            c.IDBIR_Web__c=vocdCaseDetails.caseIDBIR;                   
            c.From__c=System.label.Rforce_CAS_From_Customer;                  
           // c.Description=System.label.Rforce_VOC_Customer_Experience+'\n' +vocdCaseDetails.customerExperience + '\n' +System.label.Rforce_VOC_Reason_Of_return+'\n' + vocdCaseDetails.reasonOfReturn+'\n'+System.label.Rforce_VOC_Problem_Solved+'\n' + vocdCaseDetails.problemSolved +'\n'+System.label.Rforce_VOC_Brand_Recommendation+'\n' + vocdCaseDetails.brandRecommendation;  
            c.Description=System.label.Rforce_VOC_Customer_Experience+'\n' +vocdCaseDetails.customerExperience +'\n'+ '\n' +System.label.Rforce_VOC_Reason_Of_return+'\n' + vocdCaseDetails.reasonOfReturn+'\n'+'\n'+System.label.Rforce_VOC_Problem_Solved+'\n' + vocdCaseDetails.problemSolved +'\n'+'\n'+System.label.Rforce_VOC_Brand_Recommendation+'\n' + vocdCaseDetails.brandRecommendation;
            c.First_registration_Web__c=vocdCaseDetails.firstRegistration;            
            bCasInsert=Rforce_CreateCaseMapping_CLS.insertCase(c);                                                                                                       
        }        
    return bCasInsert;
    }  
/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description It accepts the attachment list and case instance.
* @param case Details and case instance
* @return String value according to request
*/   
  public static String getResponse(List<Attachment> attachLst,Case c,Integer attachCount){ 
          Case cs=[Select CaseNumber,Description,(Select Id,Name from attachments) from Case where Id=:c.Id];                                                                        
          return System.label.Rforce_CAS_Case+' '+cs.CaseNumber+' '+System.label.Rforce_CAS_WithAttach+' '+ '' +attachCount +' ' +System.label.Rforce_VOCC_Out_of+ ' '+'' +cs.attachments.size()+' '+System.label.Rforce_CAS_Attachments;                                                                      
   }
   public static String getResponseWithoutAttach(Case c){   
         Case cs1=[Select CaseNumber,Description from Case where Id=:c.Id];                                                        
         return System.label.Rforce_CAS_Case+' '+cs1.CaseNumber+' '+System.label.Rforce_CAS_WithoutAttach; 
   } 
}