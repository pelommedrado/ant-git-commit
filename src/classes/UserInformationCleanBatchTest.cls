@isTest
private class UserInformationCleanBatchTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test() {
        
        String jobId = System.schedule('UserInformationCleanBatchTest',
                                       CRON_EXP,
                                       new UserInformationCleanBatch());
        
        UserInformation__c userInfoApp 	= new UserInformation__c();
        userInfoApp.CreatedDate = Datetime.now().addDays(-100);
        userInfoApp.User__c 			= UserInfo.getUserId();
        userInfoApp.Application__c 		= 'CAC';
        insert userInfoApp;
        
        UserInformationCleanBatch batch = new UserInformationCleanBatch();
        Database.executeBatch(batch);
    }
}