public with sharing class VFC22_ProductDAO extends VFC01_SObjectDAO {
    
    private static final VFC22_ProductDAO instance = new VFC22_ProductDAO();

    private VFC22_ProductDAO(){}

    public static VFC22_ProductDAO getInstance() {
        return instance;
    }
    
    /**
     * This Method was used to get Product Record 
     * @param productName - use productName to fetch records from Product object.
     * @return Product - fetch and return the result in Product.
     */
    public List<Product2> findProductRecordsbyName(String productName) {
        List<Product2> Product = 
        	[SELECT Id, Name, IsActive, Model__c, ModelYear__c, Optionals__c, ProductCode, 
        			Description, Family, RecordTypeId, Version__c  
               FROM Product2
              WHERE Name =: productName 
           ORDER BY Name, Version__c ASC];
        return Product;
    }
    
    /**
     * This Method was used to fetch all Product Records
     */
    public Map<String, Product2> fetchAllProducts() {
        Map<String, Product2> mapModelSpecCodeProduct = new Map<String , Product2>();
        
        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family 
               FROM Product2
			  WHERE Name != null     
           ORDER BY Name, Version__c ASC];
        
        
        for (Product2 prod : lstProducts) {
            mapModelSpecCodeProduct.put(prod.ModelSpecCode__c, prod);
        }
        return mapModelSpecCodeProduct;
    }
    
    /**
     * This Method was used to fetch all Model Product Records
     */
    public Map<String , Product2> fetchAll_Model_Products() {
        Map<String, Product2> mapModelSpecCodeProduct = new Map<String , Product2>();
        
        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
			   FROM Product2
			  WHERE RecordType.DeveloperName = 'PDT_Model'
                AND ModelSpecCode__c != null
           ORDER BY Name, Version__c ASC];

        for (Product2 prod : lstProducts) {
            mapModelSpecCodeProduct.put(prod.ModelSpecCode__c, prod);
        }
        
        return mapModelSpecCodeProduct;
    }
    
    /**
     * This Method was used to fetch all Model Product with Model Id in Map Records
     */
    public Map<Id , Product2> fetchAll_Model_Id_In_Map() {
        Map<Id , Product2> mapModelSpecCodeProduct = new Map<Id , Product2>();

        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
               FROM Product2
              WHERE RecordType.DeveloperName = 'PDT_Model'
                AND ModelSpecCode__c != null
           ORDER BY Name, Version__c ASC];
        
		for (Product2 prod : lstProducts) {
            mapModelSpecCodeProduct.put(prod.Id, prod);
        }

        return mapModelSpecCodeProduct;
    }
    
    /**
     *
     */
    public List<Product2> fetch_All_Version_Product(String model_Code) {
        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
                FROM Product2
               WHERE RecordType.DeveloperName = 'PDT_ModelVersion'
                 AND ProductCode LIKE : model_Code
            ORDER BY Name, Version__c ASC];
        return lstProducts;
    }
    
    /**
    * This Method was used to fetch all Product Version Records
    */
    public Map<Id , List<Product2>> fetchAll_Version_Products() {
        Map<Id , List<Product2>> mapModelSpecCodeProduct = new Map<Id , List<Product2>>();
        
        List<Product2> lstProducts = [SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                               Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                               ProductCode, Description, Family
                          FROM Product2
                         WHERE RecordType.DeveloperName = 'PDT_ModelVersion'
                      ORDER BY Name, Version__c ASC];
        
        for (Product2 prod : lstProducts) {
        	
            List<Product2> temp = new List<Product2>();
            if (mapModelSpecCodeProduct.containsKey(prod.Model__c)) {
                temp = mapModelSpecCodeProduct.get(prod.Model__c);
                temp.add(prod);
                mapModelSpecCodeProduct.put(prod.Model__c, temp);
            }
            else {
                temp.add(prod);
                mapModelSpecCodeProduct.put(prod.Model__c, temp);
            }
        }
        return mapModelSpecCodeProduct;
    }
    
    /**
     * Method was used to fetch all Product version Records.
     **/
    public Map<Id, Product2> fetch_All_Versions_Map() {
        Map<Id, Product2> map_Versions = new Map<Id, Product2>();
        
        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
                FROM Product2
                WHERE RecordType.DeveloperName = 'PDT_ModelVersion'
                ORDER BY Name, Version__c ASC];
        for (Product2 prod : lstProducts) {
            map_Versions.put(prod.Id, prod);
        }
        return map_Versions;
    }
    
    /**
     * Method was used to fetch all Product version with Product Code Records.
    **/
    public Map<String, Product2> fetch_All_Versions_Map_with_ProductCode() {
        Map<String, Product2> map_Versions = new Map<String, Product2>();
        
        List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, IsActive, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
               FROM Product2
              WHERE RecordType.DeveloperName = 'PDT_ModelVersion'
                AND ProductCode != null
           ORDER BY Name, Version__c ASC];
        
        for (Product2 prod : lstProducts) {
            map_Versions.put(prod.ProductCode, prod);
        }
        return map_Versions;
    }
    
     /**
	   * This Method was used to fetch a Accessory Product Record
	   */
    public Product2 get_Accessory_Products() {
        Product2 accessory = [SELECT Id, Name, RecordTypeId, IsActive, ProductCode, Description  
	                           FROM Product2
	                          WHERE RecordType.DeveloperName = 'PDT_Accessory'
	                          limit 1];
        return accessory;
    }
    
    /**
    * Este método foi usado para buscar um registro de Product2
    */
    public List<Product2> fetchProductsByRecordType() {
        return  [SELECT Id, Name, RecordType.DeveloperName, Model__c, Model__r.Name, ModelSpecCode__c
                   FROM Product2 
                  WHERE RecordType.DeveloperName = 'PDT_Model'];
    }
    
    /**
     * Este método foi usado para buscar um registro de Product2
     * @param: Model e RecordType
     */
    public List<Product2> fetchProductsByModel(String model) {
        return [SELECT Id, RecordType.DeveloperName, Model__c, Model__r.Name, Model__r.Version__c, Version__c  
                  FROM Product2 
                 WHERE Model__r.Name = :model
                   AND RecordType.DeveloperName = 'PDT_ModelVersion'];
    }
    
    /**
     *
     */
    public List<Product2> fetchProductsByVersion(String version) {
        return [SELECT Id, Name, RecordType.DeveloperName, Model__c, Model__r.Version__c, Version__c  
                  FROM Product2 
                 WHERE Version__c = :version
                   AND RecordType.DeveloperName = 'PDT_ModelVersion'];
    }

	/**
	 *
	 */    
    public List<Product2> findByRecordType(String developerName)
    {

        System.debug('****VFC22_ProductDAO.findByRecordType');
        
        List<Product2> lstSObjProduct = [SELECT Id, Name, Model__c, Model__r.Name, ModelSpecCode__c,   
                                                RecordType.DeveloperName                                             
                                           FROM Product2 
                                          WHERE RecordType.DeveloperName =: developerName
                                       ORDER BY Name];
        return lstSObjProduct;              
    }   
    
    /**
     *
     */
    public Product2 findById(String id)
    {
        return [SELECT Id, Name, Model__c, Model__r.Name,  ModelSpecCode__c                                                 
                  FROM Product2
				 WHERE Id =: id];   
    }
    
    /**
     * Return a list of active Models
     */
    public List<Product2> fetchActiveModelsByRecordTypeDevName() {
    	List<Product2> lstProducts = 
        	[SELECT Id, Name, RecordTypeId, ModelYear__c, Optionals__c, ProductModelCode__c,
                    Model__c, ModelSpecCode__c, ModelUrlCode__c, Version__c, VersionIdSpecCode__c,
                    ProductCode, Description, Family
			   FROM Product2
			  WHERE RecordType.DeveloperName = 'PDT_Model'
                AND IsActive = True
           ORDER BY Name, Version__c ASC];
		return lstProducts;
    }
}