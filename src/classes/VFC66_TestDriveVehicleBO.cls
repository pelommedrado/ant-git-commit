/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto TDV_TestDriveVehicle__c.
* @author Felipe Jesus Silva.
*/
public class VFC66_TestDriveVehicleBO
{
	private static final VFC66_TestDriveVehicleBO instance = new VFC66_TestDriveVehicleBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC66_TestDriveVehicleBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC66_TestDriveVehicleBO getInstance()
	{
		return instance;
	}
	
	public TDV_TestDriveVehicle__c getById(String id)
	{
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		
		sObjTestDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().findById(id);
		
		return sObjTestDriveVehicle;
	}
	
	public List<TDV_TestDriveVehicle__c> getVehiclesAvailable(String dealerId, String vehicleModel)
	{

		System.debug('****VFC66_TestDriveVehicleBO.getVehiclesAvailable');
		
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().findByCriteria(dealerId, true, vehicleModel);
		
		return lstSObjTestDriveVehicle;
	}
	
}