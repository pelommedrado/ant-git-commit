/** Mock WebService for MDM
  * Used for test purposes 
  **/
@isTest
global class Myr_PersonalDataMDM_MK implements WebServiceMock {
	
	global enum MODE {
		NORMAL, 
		MYRENAULT
	}

	global MODE MdmMode;

	global TRVwsdlMdm.GetPartyResponse_element detailedResp;
	global TRVwsdlMdm.SearchPartyResponse_element searchPartyResp;

	/** @constructor **/	
	global Myr_PersonalDataMDM_MK(MODE mdmMode) {
		this.MdmMode = (mdmMode == null) ? MODE.NORMAL : mdmMode;
		detailedResp = buildDetailedResponse();
		searchPartyResp = buildSearchPartyResponse();
	}

	/** Simulates inovkation **/
	global void doInvoke( Object stub, Object request, Map<String, Object> response,
           					String endpoint, String soapAction, String requestName,
           					String responseNS, String responseName, String responseType) {
    
    	system.debug('#### Myr_PersonalDataMDM_MK - doInvoke - BEGIN request=' + request);
    	if( requestName == 'SearchPartyRequest' ) 
    	{
    		response.put( 'response_x', searchPartyResp );
    	} 
    	else if ( requestName == 'GetPartyRequest' ) 
    	{
    		response.put( 'response_x', detailedResp );
    	}
    	else if ( requestName == 'GetCustomerOrderRequest') 
    	{
    		response.put('response_x', buildCustomerOrderResponse()); 
    	}
    	
    	system.debug('#### AsyncTRVwsdlmdm - doInvoke - END');       		           	
    }
    
    /** Build a response with several persons **/
    public TRVwsdlMdm.SearchPartyResponse_element buildSearchPartyResponse() {
    	TRVwsdlMdm.SearchPartyResponse_element resp = new TRVwsdlMdm.SearchPartyResponse_element();
    	resp.ResponseMessage = 'Search Party OK';
    	List<TRVwsdlMdm.PartyOverview> listParty = new List<TRVwsdlMdm.PartyOverview>();
    	//In MyRenault mode, returns only one response
    	//response 1
    	TRVwsdlMdm.PartyOverview party1 = new TRVwsdlMdm.PartyOverview();
    	party1.PartyId = '345678';
        party1.PartyType = 'O';
        party1.MatchScore = 99;
        party1.FirstName = 'JONATHAN';
        party1.LastName = 'LAMBERT';
        party1.DateOfBirth = Date.newInstance(1980,1,1);
        party1.Sex = '1';
        party1.OccupationalCategoryCode = 'EMPLOYE';
        party1.Language = 'FR';
        party1.AddressLine1 = '71 RUE DES AMANDIERS';
        party1.AddressLine2 = '';
        party1.City = 'PARIS';
        party1.Region = '';
        party1.Postcode = '75000';
        party1.Country = 'FRANCE';
        party1.FixedPhoneNumber = new List<String>();
        party1.FixedPhoneNumber.add('+33 3 29 66 53 42');
        party1.MobilePhoneNumber = new List<String>();
        party1.MobilePhoneNumber.add('+33 6 85 29 09 96');
        party1.EmailAddress = new List<String>();
        party1.EmailAddress.add('mag.perrin21@free.fr');
        listParty.add(party1);
        if( MODE.MYRENAULT != MdmMode ) {
	    	//response 2
	    	TRVwsdlMdm.PartyOverview party2 = new TRVwsdlMdm.PartyOverview();
	    	party2.PartyId = '1082019';
	        party2.PartyType = '';
	        party2.MatchScore = 90;
	        party2.FirstName = 'JONATHAN';
	        party2.LastName = 'LAMBERT';
	        party2.DateOfBirth = Date.newInstance(1980,1,1);
	        party2.Sex = '1';
	        party2.Language = 'FR';
	        party2.AddressLine1 = '4 IMPASSE SAMUEL DE CHAMPLAIN';
	        party2.AddressLine2 = '';
	        party2.City = 'PARIS';
	        party2.Region = '';
	        party2.Postcode = '75000';
	        party2.Country = 'FRANCE';
	        party2.FixedPhoneNumber = new List<String>();
	        party2.FixedPhoneNumber.add('+33 3 29 66 53 42');
	        party2.MobilePhoneNumber = new List<String>();
	        party2.MobilePhoneNumber.add('+33 6 85 29 09 96');
	        party2.EmailAddress = new List<String>();
	        party2.EmailAddress.add('mag.perrin21@free.fr');
	        listParty.add(party2);
        }
        resp.PartyOverview = listParty;
        return resp;
    }
    
    /** Build the detailed response for one person **/
    public  TRVwsdlMdm.GetPartyResponse_element buildDetailedResponse() {

	TRVwsdlMdm.Party party = new TRVwsdlMdm.Party();
		party.PartyType = 'I';
		party.PartySubType = 'AC';
		party.PartyStatus = 'A';
		party.FirstName1 = 'JONATHAN';
		party.FirstName2 = 'JONATHAN2';
		party.LastName1 = 'LAMBERT';
		party.LastName2 = 'LAMBERT2';
		party.FullName = 'JONATHAN LAMBERT';
		party.DateOfBirth = Date.newInstance(1979, 1, 1);
		party.Civility = 'M';
		party.Sex = 'M';
		party.Title = 'M';
		party.FinancialStatus = 'ACT';
		party.MaritalStatus ='Single';
		party.Language = 'FRA';
		party.OccupationalCategoryCode = 'B1';
		party.OccupationalCategoryDescription = 'EMPLOYE';
		party.PartySegment = 'Others';
		party.PartySubType = 'AC'; 
		party.OrganisationName = '?'; 
		party.RenaultGroupStaff ='Y';
		party.NumberOfChildrenAtHome = 10; 
		party.deceased = '?';
		party.PreferredMedia ='SMS';
		party.commercialName = '';
		party.NumberOfEmployees = 10;
		party.CompanyActivityDescription = '?'; 
		party.legalNature = '?';
		
		party.PartyIdentification = new List<TRVwsdlMdm.PartyIdentification>();
		TRVwsdlMdm.PartyIdentification pi = new TRVwsdlMdm.PartyIdentification();
		pi.IdentificationType = 'NATID';
		pi.IdentificationValue = '21534';
		party.PartyIdentification.add(pi);
		
		party.CommunicationAgreement = new List<TRVwsdlMdm.CommunicationAgreement>();
		TRVwsdlmdm.CommunicationAgreement comm  = new TRVwsdlMdm.CommunicationAgreement();
		comm.CommunicationAgreementType = '?';
		comm.Brand = '?';
		comm.CommunicationAgreement= '?';
		comm.CommunicationAgreementOrigin= '?';
		comm.UpdatedDate= system.today();
		party.CommunicationAgreement.add(comm);
		
		party.StopCommunication = new List<TRVwsdlMdm.StopCommunication>();
		TRVwsdlmdm.StopCommunication stop1  = new TRVwsdlMdm.StopCommunication();
		stop1.StopCommunicationType = 'ROB';
		stop1.StopCommunication = 'Y';
		stop1.UpdatedDate= system.today();
		TRVwsdlmdm.StopCommunication stop2  = new TRVwsdlMdm.StopCommunication();
		stop2.StopCommunicationType = 'SMD';
		stop2.StopCommunication = 'N';
		stop2.UpdatedDate= system.today();
		party.StopCommunication.add(stop1);
		party.StopCommunication.add(stop2);
		
		party.PreferredDealer = new List<TRVwsdlMdm.PreferredDealer>();
		TRVwsdlmdm.PreferredDealer pdealer  = new TRVwsdlMdm.PreferredDealer();
		pdealer.DealerCode = '?';
		party.PreferredDealer.add(pdealer);
		
		party.address = new List<TRVwsdlMdm.Address>();
		TRVwsdlMdm.Address pa = new TRVwsdlMdm.Address();
		pa.city = 'ST FLORENT SUR CHER';
		pa.postcode = '18400';
		pa.AddressLine1 = 'MASSOEUVRE';
		pa.AddressLine2 = '';
		pa.AddressLine3 = ''; 
		pa.AddressLine4 = '';
		pa.region = 'CVL';
		pa.AddressType = 'MAIN';
		party.address.add(pa);
		TRVwsdlMdm.Address pa2 = new TRVwsdlMdm.Address();
		pa2.city = 'ST FLORENT SUR CHER';
		pa2.postcode = '18400';
		pa2.AddressLine1 = 'MASSOEUVRE';
		pa2.AddressLine2 = '';
		pa2.AddressLine3 = ''; 
		pa2.AddressLine4 = '';
		pa2.region = 'CVL';
		pa2.AddressType = 'PRO';
		party.address.add(pa2);
		
		party.ElectronicAddress = new List<TRVwsdlMdm.ElectronicAddress>();
		TRVwsdlMdm.ElectronicAddress email = new TRVwsdlMdm.ElectronicAddress();
		email.ElectronicAddressValue = 'mag.perrin21@free.fr';
		email.ElectronicAddressType = 'EMAIL';
		party.ElectronicAddress.add(email);
		
		party.PhoneNumber = new List<TRVwsdlMdm.PhoneNumber>();
		TRVwsdlMdm.PhoneNumber land = new TRVwsdlMdm.PhoneNumber();
		land.PhoneNumberType = 'LAND';
		land.PhoneNumberValue = '+33 3 29 66 53 42';
		land.Valid = 'Y';
		//land.LastUpdateDate = Date.newInstance(2014, 7, 29);
		party.PhoneNumber.add(land);
		TRVwsdlMdm.PhoneNumber mobile = new TRVwsdlMdm.PhoneNumber();
		mobile.PhoneNumberType = 'MOBILE';
		mobile.PhoneNumberValue = '+33 6 85 29 09 96';
		mobile.Valid = 'Y';
//		mobile.LastUpdateDate = Date.newInstance(2014, 7, 29);		
		party.PhoneNumber.add(mobile);
		TRVwsdlMdm.Vehicle veh1 = new TRVwsdlMdm.Vehicle();		
		veh1.VehicleType = 'VP';
		veh1.VehicleIdentificationNumber = 'VF1BMSE0637005985';
		veh1.Brand = 'DACIA';
		veh1.BrandLabel = 'DACIA';
		veh1.Model = 'TW2';
		veh1.ModelLabel = 'TWINGO II';
		veh1.CartecId = '63468';
		veh1.RegistrationDate = Date.newInstance(2006, 12, 12);
		veh1.firstRegistrationDate = Date.newInstance(2006, 12, 01);
		veh1.CurrentMileage = 0;
		veh1.CurrentMileageDate = Date.newInstance(1900, 1, 1);
		veh1.PreviousRegistrationNumber = '7938VH88';
		veh1.PartyVehicleType = 'OWN';
		veh1.DeliveryDate = Date.newInstance(2006, 12, 12);
		veh1.OrderDate = Date.newInstance(2007, 1, 2);
		veh1.PurchaseDate = Date.newInstance(2007, 1, 2);
		veh1.RegistrationNumber = '7938VH88';
		veh1.PurchaseMileage = 0;
		veh1.NewVehicle = 'VN';
		veh1.StartDate = Date.newInstance(2014, 7, 29);
		TRVwsdlMdm.Vehicle veh2 = new TRVwsdlMdm.Vehicle();		
		veh2.VehicleType = 'VP';
		veh2.VehicleIdentificationNumber = 'NIFTGYHUYTGRF456T';
		veh2.Brand = 'NISSAN';
		veh2.BrandLabel = 'NISSAN';
		veh2.Model = '';
		veh2.ModelLabel = 'QASHQAI';
		veh2.CartecId = '70000';
		veh2.RegistrationDate = Date.newInstance(2006, 12, 12);
		veh2.CurrentMileage = 0;
		veh2.CurrentMileageDate = Date.newInstance(1900, 1, 1);
		veh2.PreviousRegistrationNumber = '7654TG67';
		veh2.PartyVehicleType = 'OWN';
		veh2.DeliveryDate = Date.newInstance(2006, 12, 12);
		veh2.OrderDate = Date.newInstance(2007, 1, 2);
		veh2.PurchaseDate = Date.newInstance(2007, 1, 2);
		veh2.RegistrationNumber = '7654TG67';
		veh2.PurchaseMileage = 0;
		veh2.NewVehicle = 'VN';
		veh2.StartDate = Date.newInstance(2014, 7, 29);
		TRVwsdlMdm.Vehicle veh3 = new TRVwsdlMdm.Vehicle();		
		veh3.VehicleType = 'VP';
		veh3.VehicleIdentificationNumber = 'VF1BMSE0637005985';
		veh3.Brand = '';
		veh3.BrandLabel = '';
		veh3.Model = 'TW2';
		veh3.ModelLabel = 'TWINGO II';
		veh3.CartecId = '88888';
		veh3.RegistrationDate = Date.newInstance(2006, 12, 12);
		veh3.CurrentMileage = 0;
		veh3.CurrentMileageDate = Date.newInstance(1900, 1, 1);
		veh3.PreviousRegistrationNumber = '7890IK76';
		veh3.PartyVehicleType = 'OWN';
		veh3.DeliveryDate = Date.newInstance(2006, 12, 12);
		veh3.OrderDate = Date.newInstance(2007, 1, 2);
		veh3.PurchaseDate = Date.newInstance(2007, 1, 2);
		veh3.RegistrationNumber = '7890IK76';
		veh3.PurchaseMileage = 0;
		veh3.NewVehicle = 'VN';
		veh3.StartDate = Date.newInstance(2014, 7, 29);
		party.Vehicle = new List<TRVwsdlMdm.Vehicle>();
		party.Vehicle.add(veh1);
		party.Vehicle.add(veh2);
		party.Vehicle.add(veh3);
		
		TRVwsdlMdm.GetPartyResponse_element mdmResponse = new TRVwsdlMdm.GetPartyResponse_element();
		mdmResponse.ResponseMessage = ':: Get Party OK ::';
		mdmResponse.Party = party;
		return mdmResponse;
    }
    public TRVwsdlMdm.GetCustomerOrderResponse_element buildCustomerOrderResponse()
    {
    	
    	TRVwsdlMdm.CustomerOrderOverview party = new TRVwsdlMdm.CustomerOrderOverview();
    	list <TRVwsdlMdm.CustomerOrderOverview> partylst = new list <TRVwsdlMdm.CustomerOrderOverview>();
    	
    	party.PartyType = '?';
        party.PartySubType = '?';
        party.PartyStatus= '?';
        party.FirstName1= 'Francois';
        party.FirstName2= '?';
        party.LastName1= 'Toto';
        party.LastName2= '?';
        party.FullName= '?';
        party.DateOfBirth = system.today(); 
        party.YearOfBirth= '?';
        party.Civility= '?';
        party.Title= '?';
        party.Sex= '?';
        party.MaritalStatus= '?';
        party.Language= '?';
        party.RenaultGroupStaff= '?';
        party.NumberOfChildrenAtHome= 10;
        party.Deceased= '?';
        party.EmailRental= '?';
        party.PreferredMedia= '?';
        party.OccupationalCategoryCode= '?';
        party.PartySegment= '?';
        party.OrganisationName= '?';
        party.CommercialName= '?';
        party.ProtocolName= '?';
        party.NumberOfVehicles= 10;
        party.NumberOfEmployees= 10;
        party.FinancialRisk= '?';
        party.FinancialStatus= '?';
        party.CompanyActivityCode= '?';
    
    	party.Order = new List<TRVwsdlMdm.Order>();
		TRVwsdlMdm.Order po = new TRVwsdlMdm.Order();
		po.OrderID = '?';
		po.OrderNumber = '?';
		po.OrderType = '?';
		po.OrderStatus = '?';
		po.SellingDealer = '?'; 
		po.DeliveryDealer = '?';
		po.VIN = '?';
		po.Brand = '?';
		po.Model = '?';
		po.VersionCode = '?';
		po.OrderDate = system.today();
		po.OrderCancellationDate = system.today();
		po.DesiredDeliveryDate = system.today();
		po.ManufacturingDate = system.today();
		po.ShippingDate = system.today();
		po.DealerDeliveryDate = system.today();
		po.DeliveryDate = system.today();
		po.DeliveryCancellationDate = system.today();
		po.CreatedDate = system.today();
		po.UpdatedDate = system.today();
		po.TechnicalUpdatedDate = system.today();
		party.Order.add(po); 
		
		partylst.add(party); 
		TRVwsdlMdm.GetCustomerOrderResponse_element mdmResponse = new TRVwsdlMdm.GetCustomerOrderResponse_element();
		mdmResponse.ResponseMessage = ':: Get Customer Order OK ::';
		mdmResponse.CustomerOrderOverview = partylst;

		return mdmResponse;
    }
}