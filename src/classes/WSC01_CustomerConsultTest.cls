@isTest
global class WSC01_CustomerConsultTest{
	
    //Scenario 1: Phone Positive
    static testMethod void customerRequestPhonePositive(){

        String phone = '1155550000';
        
        test.startTest();        
            loadData();
            
            Id [] fixedSearchResults = new Id[1];
            fixedSearchResults[0] = conta.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            List<List<SObject>> searchList = [FIND '1155550000' IN PHONE FIELDS RETURNING Account];
            
            WSC01_CustomerConsult.customerRequestPhone(phone);           
        test.stopTest();
    }
    
    //Scenario 2: Phone Negative
    static testMethod void customerRequestPhoneNegative(){

        String phone = '11999990001';
        
        test.startTest();        
            loadData();
                    
            Id [] fixedSearchResults = new Id[1];
            fixedSearchResults[0] = contato.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            List<List<SObject>> searchList = [FIND '11999990001' IN PHONE FIELDS RETURNING Contact];
            
        	//Without custom configuration default
        	premiumCars.Name = 'test';
        	update premiumCars;
            WSC01_CustomerConsult.customerRequestPhone(phone); 
        
        	//With custom configuration default and without id premium vehicles
        	premiumCars.Name = 'default';
        	premiumCars.id_premium_vehicles__c = null;        
        	update premiumCars;
            WSC01_CustomerConsult.customerRequestPhone(phone);
        
        	//With custom configuration default and with id premium vehicles
        	premiumCars.Name = 'default';
        	premiumCars.id_premium_vehicles__c = 'modelOne';
        	update premiumCars;
            WSC01_CustomerConsult.customerRequestPhone(phone);
                  
        test.stopTest();
    }    
    
    //Scenario 3: CPF Positive
    static testMethod void customerRequestCPFPositive(){

        String cpf = '73354802000173';
        
        test.startTest();        
            loadData();
            
            Id [] fixedSearchResults = new Id[1];
            fixedSearchResults[0] = conta.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            List<List<SObject>> searchList = [FIND '73354802000173' IN ALL FIELDS RETURNING Account];
            
            WSC01_CustomerConsult.customerRequestCPF(cpf);           
        test.stopTest();
    }

    global static void loadData(){
        
        //Create account
        contaPC = new Account();
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Personal_Acc' LIMIT 1];
        contaPC.RecordTypeId = rt.Id;
        contaPC.FirstName = 'Ricardo';
        contaPC.LastName = 'Montes';
        contaPC.PersLandline__c = '1155550000';
        contaPC.PersonMobilePhone = '11999990000';
        contaPC.PersonEmail = 'ricardo@email.com';
        contaPC.CustomerIdentificationNbr__c = '70376361425';
        contaPC.ComAgreemt__c = 'Yes';
		insert contaPC;
        
        //Create account
        conta = new Account();
        RecordType rt1 = [SELECT Id FROM RecordType WHERE DeveloperName = 'Company_Acc' LIMIT 1];
        conta.RecordTypeId = rt1.Id;
        conta.Name = 'Montes';
        conta.Phone = '1155550000';
        conta.AccountSource = 'TELEFONE';
        conta.CustomerIdentificationNbr__c = '73354802000173';
        insert conta;
        
        //Create Contact
        contato = new Contact();
        contato.FirstName = 'Marcio';
        contato.LastName = 'Silva';
        contato.AccountId = conta.Id;
        contato.MobilePhone = '11999990001';
		contato.Phone = '1155550001';
		contato.Email = 'marcio@email.com.br';
        insert contato;
        
        //Create Case Account
        caso = new Case();
        caso.AccountId = conta.Id;
        caso.ContactId = contato.Id;
        caso.Status = 'Novo';
		caso.Origin = 'Telefone';
		caso.Subject = 'Teste';
        caso.Description = 'Teste';
		insert caso;
        
        //Create Case Contact
        caso1 = new Case();
        caso1.AccountId = contaPC.Id;
        caso1.ContactId = contato.Id;
        caso1.Status = 'Novo';
		caso1.Origin = 'Telefone';
		caso1.Subject = 'Teste';
        caso1.Description = 'Teste';
		insert caso1;
            
        //Create Vehicle Account
    	veiculo = new VEH_Veh__c();
        veiculo.Name = '3N1AB6AE8DL601129';
        veiculo.Model__c = 'modelOne';
		insert veiculo;
    	
        //Create Onwership Relation
		relacaoPosse = new VRE_VehRel__c();
        relacaoPosse.Status__c = 'Ativo';
        relacaoPosse.Account__c = conta.Id;
		relacaoPosse.VIN__c = veiculo.Id;
		relacaoPosse.TypeRelation__c = 'Proprietário';
        insert relacaoPosse;
	
        //Create a custom configuration
        premiumCars = new cti_premium_cars__c();
        premiumCars.Name = 'default';
        premiumCars.id_premium_vehicles__c = 'modelOne;modelTwo';
        insert premiumCars;        
    }
    
    global static Account conta {get;set;}
    global static Account contaPC {get;set;}
    global static Contact contato {get;set;}
    global static Case caso {get;set;}
    global static Case caso1 {get;set;}
    global static VEH_Veh__c veiculo {get;set;}
    global static VRE_VehRel__c relacaoPosse {get;set;}
    global static cti_premium_cars__c premiumCars {get;set;}
}