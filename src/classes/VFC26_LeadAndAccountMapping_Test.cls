/**
    Class   -   VFC26_LeadAndAccountMapping_Test
    Author  -   Suresh Babu
    Date    -   29/10/2012
    
    #01 <Suresh Babu> <29/10/2012>
        Created this class using test for VFC26_LeadAndAccountMapping_Test.
**/
@isTest
private class VFC26_LeadAndAccountMapping_Test {
     static testMethod void VFC26_LeadAndAccountMapping_Test() {
        
        Account accountRecord = new Account();
        accountRecord.ShippingStreet = 'Av república argentina,1014';
        accountRecord.ShippingCity = 'Curitiba';
        accountRecord.ShippingState = 'PR';
        accountRecord.ShippingPostalCode = '80620010';
        accountRecord.ShippingCountry = 'Brasil';
        accountRecord.FirstName = 'Test';
        accountRecord.LastName = 'Account';
        accountRecord.PersMobPhone__c = '9876543210';
        accountRecord.AddressDate__pc = Date.today();
        accountRecord.AddressOK_BR__c = 'Y';
        accountRecord.Address__pc = 'Yes';
        /*accountRecord.AfterSales_BR__c;
        accountRecord.AfterSalesOffer_BR__c;
        accountRecord.BehaviorCustmr_BR__c;
        accountRecord.Birthdate__c;
        accountRecord.ProfMobPhone__c;
        accountRecord.ProfMobPhoneDate__c;
        accountRecord.ProfPhone__c;
        accountRecord.ProfLandline__c;
        accountRecord.ProfPhoneDate__c;
        accountRecord.ProfPhone__c;
        accountRecord.CadastralCustmer_BR__c;
        accountRecord.ContemplationLetter_BR__c;
        accountRecord.CustomerIdentificationNbr__c;
        accountRecord.CurrentVehicle_BR__c;
        accountRecord.DateContemplation_BR__c;
        accountRecord.IDBIRPrefdDealer__c;
        accountRecord.EmailOK_BR__c;
        accountRecord.PersEmailAddress__c;
        accountRecord.EndDateOfFinanciating_BR__c;
        accountRecord.PersLandline__c;
        accountRecord.PersPhoneDate__c;
        accountRecord.PersPhone__c;
        accountRecord.InfoUpdatedLast5Yrs_BR__c;
        accountRecord.MktdatasCluster_BR__c;
        accountRecord.NUMDBM_BR__c;
        accountRecord.ParticipateOnOPA_BR__c;
        accountRecord.PersEmailDate__c;
        accountRecord.PersEmail__c;
        accountRecord.PersMobilePhoneDate__c;
        accountRecord.PersMobilePhone__c;
        accountRecord.PhoneNumberOK_BR__c;
        accountRecord.PreApprovedCredit_BR__c;
        accountRecord.PropensityModel_BR__c; */
        accountRecord.RecentBirthdate_BR__c = 'N';
        accountRecord.SMSDate__pc = Date.today();
        accountRecord.SMS__pc = 'Yes';
        accountRecord.ValueOfCredit__c = 50000;
        accountRecord.VehicleInterest_BR__c = 'Fluence Sedan';
        accountRecord.LeadSource__c = 'INTERNET';
        accountRecord.LeadSubsource__c = 'HOT SITES';
        
        Test.startTest();
        VFC12_AccountDAO.getInstance().insertData( accountRecord );
        List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        List<TST_Transaction__c> lstTransactionsInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToTransactionObject();
        Id TransactionId = VFC06_TransactionDAO.getInstance().findMaxTransactionByAccountId(lstAccounts[0].Id);
        Lead leadRecord1 = new Lead();
        Id leadRecordId = null;
        leadRecord1 = VFC26_LeadAndAccountMapping.getInstance().accountToLeadMapping( leadRecordId, accountRecord, TransactionId );
        Test.stopTest();
        system.assertEquals( 'CSC', leadRecord1.LeadSource );
    }
    
    static testMethod void VFC26_LeadAndAccountMapping_Test1() {
        
        String devName = 'Personal_Acc';
        Id accountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( devName );
        
        Lead leadRecord = new Lead();
        
        leadRecord.Street = 'Av república argentina,1014';
        leadRecord.City = 'Curitiba';
        leadRecord.State = 'PR';
        leadRecord.PostalCode = '80620010';
        leadRecord.Country = 'Brasil';
        leadRecord.FirstName = 'Test';
        leadRecord.LastName = 'Lead';
        leadRecord.MobilePhone = '9876543210';
        leadRecord.AddressDateUpdated__c = Date.today(); 
        leadRecord.AddressOK__c  = 'Y';
        leadRecord.AddressUpdated__c = 'Yes';
        leadRecord.RecentBirthdate__c = 'N';
        leadRecord.SMSDateUpdated__c = Date.today();
        leadRecord.SMSUpdated__c = 'Yes';
        leadRecord.SubSource__c = 'HOT SITES';
        leadRecord.ValueOfCredit__c = 50000;
        leadRecord.VehicleOfInterest__c = 'Fluence Sedan';
        leadRecord.LeadSource = 'INTERNET';
        leadRecord.SubSource__c = 'HOT SITES';
        leadRecord.RecordTypeId = Utils.getRecordTypeId('Lead', 'Vendas_Empresa_PF');
        
        
        
        Test.startTest();
        VFC09_LeadDAO.getInstance().insertData( leadRecord );
        Test.stopTest();
        Account accountRecord1 = new Account();
        Id accountId = null; 
        accountRecord1 = VFC26_LeadAndAccountMapping.getInstance().leadToAccountMapping( leadRecord, leadRecord, accountId );
        system.Debug(' Acccount ----->'+accountRecord1);
        system.assertEquals( accountRecord1.LeadSource__c, leadRecord.LeadSource );
    }
    
    static testMethod void VFC26_LeadAndAccountMapping_Test2() {
        
        String devName = 'Personal_Acc';
        Id accountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( devName );
        
        Lead leadRecord = new Lead();
        
        leadRecord.Street = 'Av república argentina,1014';
        leadRecord.City = 'Curitiba';
        leadRecord.State = 'PR';
        leadRecord.PostalCode = '80620010';
        leadRecord.Country = 'Brasil';
        leadRecord.FirstName = 'Test';
        leadRecord.LastName = 'Lead';
        leadRecord.MobilePhone = '9876543210';
        leadRecord.AddressDateUpdated__c = Date.today(); 
        leadRecord.AddressOK__c  = 'Y';
        leadRecord.AddressUpdated__c = 'Yes';
        leadRecord.RecentBirthdate__c = 'N';
        leadRecord.SMSDateUpdated__c = Date.today();
        leadRecord.SMSUpdated__c = 'Yes';
        leadRecord.SubSource__c = 'HOT SITES';
        leadRecord.ValueOfCredit__c = 50000;
        leadRecord.VehicleOfInterest__c = 'Fluence Sedan';
        leadRecord.LeadSource = 'INTERNET';
        leadRecord.SubSource__c = 'HOT SITES';
        leadRecord.RecordTypeId = Utils.getRecordTypeId('Lead', 'Vendas_Empresa_PF');
        
        
        //Comentado por Marcelino
        /*Test.startTest();
        VFC09_LeadDAO.getInstance().insertData( leadRecord );
        Test.stopTest();
        Account accountRecord1 = new Account();
        Id accountId = null; 
        accountRecord1 = VFC26_LeadAndAccountMapping.getInstance().leadToAccountMapping( leadRecord, accountRecordTypeId, accountId );
        system.Debug(' Acccount ----->'+accountRecord1);
        system.assertEquals( accountRecord1.LeadSource__c, leadRecord.LeadSource );*/
    }
}