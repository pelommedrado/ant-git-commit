/**
Last Modified : 20/04/2016 by Ludovic Marboutie 
Tested by : TRVWebservicesManager_Test
**/

public with sharing class TRVWebservicesRBX 
{
    
    public static TRVwsdlRbx.CrmGetCustData sicDataSource;
    
    //public TRVwsdlRbx.GetCustDataRequest request;
        
    
    public static TRVResponseWebservices getAccountSearch(TRVRequestParametersWebServices.RequestParameters eParams)
    {
        //construction request mdm
        system.debug('#### TRVWebservicesRBX - getAccountSearch - TRV - BEGIN'); 
        
        TRVwsdlRbx.GetCustDataResponse sicResponse = getSICData(eParams);
        TRVResponseWebservices response = mapPersonalDataFields(sicResponse, eParams);
        
        system.debug('#### TRVWebservicesRBX - getAccountSearch - TRV - END'); 
 
        return response;
        
    }
    
    
    
    private static TRVwsdlRbx.GetCustDataResponse getSICData(TRVRequestParametersWebServices.RequestParameters eParams) 
    {
        //check values
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - BEGIN'); 
        if( eParams.vin == null && eParams.Registration == null && eParams.ident1 == null & eParams.idClient == null && eParams.email == null && ( (eParams.lastname == null && eParams.zip == null) || (eParams.lastname == null && eParams.city == null) ) ) 
        {
                //throw new Myr_PersonalDataSIC_Cls_Exception('The minimum parameters for RBX should one of the following combinations: vin or registration or ident1 or idClient or email or lastname + city or lastname + zip');
                system.debug('#### TRVWebservicesRBX - getSICData - TRV - Argument or combinations is null [Warning]'); 
                system.debug('##Ludo erreur : The minimum parameters for RBX should one of the following combinations: vin or registration or ident1 or idClient or email or lastname + city or lastname + zip'); 
        }  
        //Prepare the header7
        sicDataSource = new TRVwsdlRbx.CrmGetCustData(); 
        //TODO voir label
        //sicDataSource.endpoint_x = System.label.RbxWsUrl;
         
        WebServicesInfo__c ws = [select name, Login__c, Password__c, EndPoint__c from WebServicesInfo__c where name = 'RBX' LIMIT 1];
        sicDataSource.endpoint_x = ws.EndPoint__c;
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - Endpoint RBX : ' + sicDataSource.endpoint_x);
        sicDataSource.clientCertName_x = System.label.clientcertnamex;
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - ClientCertName RBX : ' + sicDataSource.clientCertName_x);
        sicDataSource.timeout_x = 120000;
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - TimeOut RBX : ' + sicDataSource.timeout_x);
        sicDataSource.inputHttpHeaders_x = new Map<String, String>();
        sicDataSource.inputHttpHeaders_x.put('Content-Type', 'application/soap+xml; charset=utf-8');
       	
        sicDataSource.inputHttpHeaders_x.put('Keep-Alive', '1115');
        
        //Prepare the request parameters: 
        TRVwsdlRbx.CustDataRequest custDataRequest = new TRVwsdlRbx.CustDataRequest();
        
        
        //custDataRequest.mode = '7'; //by default: specific mode for Myr //TODO should be set in settings
        custDataRequest.mode = String.valueOf(eParams.callmode);         
        custDataRequest.country = eParams.country;
        custDataRequest.brand = eParams.brand;
        
        if (eParams.demander == 'MOEWS')
        	custDataRequest.demander = eParams.demander;
        else
        	custDataRequest.demander = 'PRODCCB'; //Default for Salesforce
        
        custDataRequest.vin = eParams.vin;
        custDataRequest.lastName = eParams.LastName;
        custDataRequest.firstName = eParams.firstName;
        custDataRequest.city = eParams.City;
        custDataRequest.zip = eParams.Zip;
        custDataRequest.ident1 = eParams.ident1;
        custDataRequest.idClient = eParams.idClient;
        custDataRequest.typeperson = '';
        custDataRequest.idMyr = '';
        custDataRequest.firstRegistrationDate = '';
        custDataRequest.registration = eParams.Registration;
        custDataRequest.email = eParams.email;
        custDataRequest.sinceDate = '';
        custDataRequest.ownedVehicles = '';
        custDataRequest.nbReplies = '10';
        
        TRVwsdlRbx.ServicePreferences servicesPrefs = new TRVwsdlRbx.ServicePreferences();
        servicesPrefs.irn = '?';
        servicesPrefs.sia = '?';
        servicesPrefs.reqid = '?';
        servicesPrefs.userid = '?';
        servicesPrefs.language = '?';
        servicesPrefs.country = '?';
        
        TRVwsdlRbx.GetCustDataRequest request = new TRVwsdlRbx.GetCustDataRequest();
        request.servicePrefs =  servicesPrefs;
        request.custDataRequest = custDataRequest;
        
        sicDataSource.inputHttpHeaders_x.put('Content-Length', String.ValueOf(request.toString().length()));
        
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - InputHttpHeader RBX : ' + sicDataSource.inputHttpHeaders_x); 
        system.debug('#### TRVWebservicesRBX - getSICData - TRV -  : services Prefs RBX : ' + servicesPrefs);
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - custDataRequest RBX : ' + custDataRequest);
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - request RBX BEGIN');

        TRVwsdlRbx.GetCustDataResponse responseSIC = sicDataSource.getCustData(request);
		
		system.debug('##Ludo Response RBX : ' + responseSIC);
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - request RBX END');
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - response RBX : ' + responseSIC);
        system.debug('#### TRVWebservicesRBX - getSICData - TRV - END'); 
        return responseSIC; 
    }
    
    /** Map the SIC response into a PersonalData response **/
    public static TRVResponseWebservices mapPersonalDataFields(TRVwsdlRbx.GetCustDataResponse iSICResponse, TRVRequestParametersWebServices.RequestParameters eParams) {
        
        TRVResponseWebservices pDataResponse = new TRVResponseWebservices();
        system.debug('#### TRVWebservicesRBX - mapPersonalDataFields - TRV - BEGIN');
        //TODO a voir
        //pDataResponse.response.dataSource = Myr_PersonalData_WS.DataSourceType.SISMSC;
        
        if( iSICResponse == null ) 
        {
            system.debug('#### TRVWebservicesRBX - mapPersonalDataFields - TRV - Argument is null[Warning]');
            return null;
        }
        if( iSICResponse.clientList != null ) {
            system.debug('#### TRVWebservicesRBX - mapPersonalDataFields - TRV - Parsing BEGIN');
            
            //table for transco 
            //get custom setting transco
            map<String, MDMCivillity__c> mdmCivility = MDMCivillity__c.getAll(); //Civility
            List<String> civilityNames = new List<String>();
            civilityNames.addAll(mdmCivility.keySet());
            civilityNames.sort();

            map<String, Gender__c> mdmGender= Gender__c.getAll(); //Gender/Sex
            List<String> genderNames = new List<String>();
            genderNames.addAll(mdmGender.keySet());
            genderNames.sort();

            Map<String, Language__c> mdmLanguage= Language__c.getAll(); //language
            List<String> languageNames = new List<String>();
            languageNames.addAll(mdmLanguage.keySet());
            languageNames.sort();       
                
            Map<String, MDMCountry__c> mdmCountry= MDMCountry__c.getAll();  //country
            List<String> countryNames = new List<String>();
            countryNames.addAll(mdmCountry.keySet());
            countryNames.sort();
            
            Map<String, MDMVehicleRelationType__c> mdmVehicleRelationType= MDMVehicleRelationType__c.getAll(); //vehicle relation type
            List<String> vehicleRelationsNames = new List<String>();
            vehicleRelationsNames.addAll(mdmVehicleRelationType.keySet());
            vehicleRelationsNames.sort();
            
            
            
            for( TRVwsdlRbx.PersonnalInformation sicClient : iSICResponse.clientList ) {
                //Personal Information
                TRVResponseWebservices.InfoClient infoClient = new TRVResponseWebservices.InfoClient();
                //infoClient.IdClient = sicClient.idClient;
                infoClient.datasource = 'RBX'; 
                infoClient.DataSourceOrigin = eParams.DataSourceOrigin;     //DataSource origin
                
                infoClient.accountBrand = eparams.brand; 
                infoClient.countryCode2 = eparams.country2L; 
                
                
                if (String.isEmpty(sicClient.idClient) == false)
                    infoClient.strPartyID = sicClient.idClient.trim();      //MDM like IdClient for BCS or RBX
                if (String.isEmpty(sicClient.birthDay) == false)
                    infoClient.strDOB = sicClient.birthDay.trim();                      //MDM
                if (String.isEmpty(sicClient.lastName) == false)
                {
                    infoClient.LastName1 = sicClient.lastName.trim();
                    infoClient.name = sicClient.lastName.trim();
                }
                if (String.isEmpty(sicClient.middleName) == false)
                    infoClient.lastname2 = sicClient.middleName.trim();
                
                //Country SF
                if (String.isEmpty(eparams.countrySF) == false) 
                    infoClient.countrySF = eparams.countrySF;
                if (String.isEmpty(eparams.AdriaticCountrySF) == false) 
                    infoClient.AdriaticCountrySF = eparams.AdriaticCountrySF;
                if (String.isEmpty(eparams.MiDCECountrySF) == false) 
                    infoClient.MiDCECountrySF = eparams.MiDCECountrySF;
                if (String.isEmpty(eparams.NRCountrySF) == false) 
                    infoClient.NRCountrySF = eparams.NRCountrySF;
                if (String.isEmpty(eparams.PlCountrySF) == false) 
                    infoClient.PlCountrySF = eparams.PlCountrySF;
                if (String.isEmpty(eparams.UkIeCountrySF) == false) 
                    infoClient.UkIeCountrySF = eparams.UkIeCountrySF;
                
                
                
                if (String.isEmpty(sicClient.title) == false)   
                {
                    for (String civilityName : civilityNames) {
                        MDMCivillity__c civility = mdmCivility.get(civilityName);
                        if (String.isEmpty(civility.value__c) == false)
                            if (civility.Value__c.equalsIgnoreCase(sicClient.title) && civility.dataSource__c == 'RBX') 
                                infoClient.civility = civility.Civility__c;
                    }
                    if(String.isEmpty(infoClient.civility) == true)
                        infoClient.civility = sicClient.title; //value by default
                }
                
                system.debug('##Ludo salutation : ' + sicClient.title);
                system.debug('##Ludo salutation 1 : ' + infoClient.civility);
                
                if (String.isEmpty(sicClient.lang) == false)
                {
                    for (String languageName: languageNames) 
                    {
                        Language__c language = mdmLanguage.get(languageName);
                        if (String.isEmpty(language.value__c) == false)
                            if (language.Value__c.equalsIgnoreCase(sicClient.lang) && language.dataSource__c == 'RBX') 
                                infoClient.Lang = language.MDMLanguage__c;
                    }
                    if(String.isEmpty(infoClient.Lang) == true)
                        infoClient.Lang = sicClient.lang;
                }
                if (String.isEmpty(sicClient.sex) == false)
                {
                    for (String genderName : genderNames) 
                    {
                        Gender__c gender = mdmGender.get(genderName);
                        if (String.isEmpty(gender.value__c) == false)
                            if (gender.Value__c.equalsIgnoreCase(sicClient.sex) && gender.dataSource__c == 'RBX') 
                                infoClient.sex  = gender.Gender__c;
                    }
                    if(String.isEmpty(infoClient.sex) == true)
                        infoClient.sex  = sicClient.sex;
                }
                
                if (String.isEmpty(sicClient.firstName) == false)
                    infoClient.FirstName1 = sicClient.firstName.trim();
                
                if (String.isEmpty(sicClient.idMyr) == false)
                    infoClient.idMyr = sicClient.idMyr.trim();
                
                if (String.isEmpty(sicClient.typeperson) == false)
                    infoClient.strAccountType = sicClient.typeperson.trim();
                
                if (infoClient.strAccountType != '' && infoClient.strAccountType == 'P')
                {
                    if(String.isEmpty(sicClient.ident1) == false)
                        infoClient.localCustomerID1 = sicClient.ident1.trim();
                    if(String.isEmpty(sicClient.ident2) == false)
                        infoClient.localCustomerID2 = sicClient.ident2.trim();
                }
                else
                {
                    if(String.isEmpty(sicClient.ident1) == false)
                        infoClient.companyID = sicClient.ident1.trim();
                    if(String.isEmpty(sicClient.ident2) == false)
                        infoClient.secondCompanyID = sicClient.ident2.trim();
                }
                
                if(String.isEmpty(sicClient.ident1) == false)
                    infoClient.siret = sicClient.ident1;
                if(String.isEmpty(sicClient.ident2) == false)
                    infoClient.siren = sicClient.ident2;    
                    

                if( sicClient.contact != null ) 
                {
                    if (String.isEmpty(sicClient.contact.email) == false)
                        infoClient.email = sicClient.contact.email.trim();
                    
                    if (String.isEmpty(sicClient.contact.phoneNum1) == false)
                        if (String.isEmpty(sicClient.contact.phonecode1 ) == false)
                            infoClient.strFixeLandLine = sicClient.contact.phonecode1 + sicClient.contact.phoneNum1.trim();
                        else 
                            infoClient.strFixeLandLine = sicClient.contact.phoneNum1.trim();
                    if (String.isEmpty(sicClient.contact.phoneNum2) == false)
                        if (String.isEmpty(sicClient.contact.phonecode2) == false)
                            infoClient.strFixeLandLinePro = sicClient.contact.phonecode2+sicClient.contact.phoneNum2.trim();
                        else 
                            infoClient.strFixeLandLinePro = sicClient.contact.phoneNum2.trim();
                    if (String.isEmpty(sicClient.contact.phoneNum3) == false)
                        if (String.isEmpty(sicClient.contact.phonecode3) == false)
                            infoClient.strMobile = sicClient.contact.phonecode3 + sicClient.contact.phoneNum3.trim();
                        else 
                            infoClient.strMobile = sicClient.contact.phoneNum3.trim();
                }
                if( sicClient.address != null ) 
                {
                    if (String.isEmpty(sicClient.address.strName) == false)         
                        infoClient.Addressline1 = sicClient.address.strName.trim();                 //MDM: strAddressLine
                    if (String.isEmpty(sicClient.address.compl2) == false)
                        infoClient.Addressline2 = sicClient.address.compl2.trim();
                    if (String.isEmpty(sicClient.address.compl1) == false) 
                        infoClient.compl1 = sicClient.address.compl1.trim();
                    if (String.isEmpty(sicClient.address.compl3) == false) 
                        infoClient.compl3 = sicClient.address.compl3.trim();
                    if (String.isEmpty(sicClient.address.strNum) == false) 
                        infoClient.StrNum = sicClient.address.strNum.trim();
                        
                    if (String.isEmpty(sicClient.address.strTypeLabel) == false)
                        infoClient.StrType = sicClient.address.strTypeLabel.trim();
                    else if(String.isEmpty(sicClient.address.strType) == false)
                        infoClient.StrType = sicClient.address.strType.trim();
                    
                    if (String.isEmpty(sicClient.address.countryCode) == false)
                    {
                        for (String countryName : countryNames) 
                        {
                            MDMCountry__c country = mdmCountry.get(countryName);
                            if (String.isEmpty(country.value__c) == false)              
                                if (country.Value__c.equalsIgnoreCase(sicClient.address.countryCode) && country.dataSource__c == 'RBX') 
                                    infoClient.CountryCode = country.Country__c;
                        }
                        if(String.isEmpty(infoClient.CountryCode ) == true)
                            infoClient.CountryCode = sicClient.address.countryCode; //value by default
                    }   
                    
                    if (String.isEmpty(sicClient.address.zip) == false)
                        infoClient.Zip = sicClient.address.zip.trim();
                    if (String.isEmpty(sicClient.address.city) == false)
                        infoClient.City = sicClient.address.city.trim();
                    if (String.isEmpty(sicClient.address.areaCode) == false)
                        infoClient.region = sicClient.address.areaCode.trim();  //todo check is good
                }
                
                //Communication Agreement 
                if( sicClient.CommunicationAgreement != null ) 
		        {
		            for( TRVwsdlrbx.CommunicationAgreement com : sicClient.CommunicationAgreement ) 
		            {
		                if (com.CommunicationAgreementType == 'EML')
		                {
		                	if (String.isEmpty(com.CommunicationAgreement) == false)
		                	{
			                    if (com.CommunicationAgreement.equalsIgnoreCase('Y') || com.CommunicationAgreement.equalsIgnoreCase('O'))
			                        infoClient.EmailCommAgreement = 'Yes'; 
			                    else 
			                        infoClient.EmailCommAgreement = 'No';
			                    infoClient.EmailCommAgreementDate = Date.valueOf(com.UpdatedDate); 
		                	}
		                }       
		                if (com.CommunicationAgreementType == 'TEL')
		                {
		                	if (String.isEmpty(com.CommunicationAgreement) == false)
		                	{
			                    if (com.CommunicationAgreement.equalsIgnoreCase('Y') || com.CommunicationAgreement.equalsIgnoreCase('O'))
			                        infoClient.TelCommAgreement = 'Yes'; 
			                    else 
			                        infoClient.TelCommAgreement = 'No';
			                    infoClient.TelCommAgreementDate = Date.valueOf(com.UpdatedDate);
		                	}
		                }
		                if (com.CommunicationAgreementType == 'SMS')
		                {
							if (String.isEmpty(com.CommunicationAgreement) == false)
		                	{
		                    	if (com.CommunicationAgreement.equalsIgnoreCase('Y') || com.CommunicationAgreement.equalsIgnoreCase('O'))
		                        	infoClient.SMSCommAgreement = 'Yes';
		                    	else
		 	                       infoClient.SMSCommAgreement = 'No';
		    	                infoClient.SMSCommAgreementDate = Date.valueOf(com.UpdatedDate);
		    	     
		                	}
		                }       
		                if (com.CommunicationAgreementType == 'COU')
		                {
							if (String.isEmpty(com.CommunicationAgreement) == false)
		                	{
		                    	if (com.CommunicationAgreement.equalsIgnoreCase('Y') || com.CommunicationAgreement.equalsIgnoreCase('O'))
		                        	infoClient.PostCommAgreement = 'Yes'; 
		                    	else 
		                        	infoClient.PostCommAgreement = 'No';
		                    	infoClient.PostCommAgreementDate = Date.valueOf(com.UpdatedDate);
		                	}
		                }
		           
		            }
		        }
		        
		        if( sicClient.StopCommunication != null ) 
		        {
		            for( TRVwsdlRbx.StopCommunication stop : sicClient.StopCommunication ) 
		            {
						if(String.isEmpty(stop.StopCommunicationType) == false)
		            	{
		            		if (stop.StopCommunicationType == 'U' || stop.StopCommunicationType == 'ROB')
		            		{
		            			system.debug('##Ludo passage stop comm à U ');
			                	if (stop.StopCommunication.equalsIgnoreCase('Y') || stop.StopCommunication.equalsIgnoreCase('O'))
			                	{
			                		system.debug('##Ludo passage stop comm à Yes ');
			                    	infoClient.stopComFlag = 'Yes';
			                	} 
			                	else
			                	{ 
			                		system.debug('##Ludo passage stop comm à No ' + stop.StopCommunicationType);
			                    	infoClient.stopComFlag = 'No';
			                	}
			                	infoClient.stopComFlagDate = Date.valueOf(stop.UpdatedDate);
			                	system.debug('##Ludo date stop comm : ' + Date.valueOf(stop.UpdatedDate));
		            		}
		            	 
							/*if (stop.StopCommunicationType == 'SMD')
							{
								if (stop.StopCommunicationType.equalsIgnoreCase('Y'))
									infoClient.stopComSMDFlag = 'Yes'; 
								else 
									infoClient.stopComSMDFlag = 'No';
								infoClient.stopComSMDFlagDate = Date.valueOf(stop.UpdatedDate); 
							}*/
						}
		            }
		        }
                
                //Vehicle informations
                infoClient.vcle = new List<TRVResponseWebservices.Vehicle>();
                for( TRVwsdlRbx.Vehicle sicVehicle : sicClient.vehicleList ) 
                {
                    TRVResponseWebservices.Vehicle vehicle = new TRVResponseWebservices.Vehicle();
                    
                    if (String.isEmpty(sicVehicle.vin) == false)
                        vehicle.vin = sicVehicle.vin.trim();
                    if (String.isEmpty(sicClient.idClient) == false)
                        vehicle.IdClient = sicClient.idClient.trim();
                    
                    if (String.isEmpty(sicVehicle.brandLabel) == false)
                        vehicle.brandCode = sicVehicle.brandLabel.trim();
                    else if (String.isEmpty(sicVehicle.brandCode) == false)
                    	vehicle.brandCode = sicVehicle.brandCode.trim();
                    
                    if (String.isEmpty(sicVehicle.modelCode) == false)
                    
                        vehicle.modelCode = sicVehicle.modelCode.trim();
                    if (String.isEmpty(sicVehicle.modelLabel) == false)
                        vehicle.modelLabel = sicVehicle.modelLabel.trim();
                    if (String.isEmpty(sicVehicle.versionLabel) == false)
                        vehicle.versionLabel = sicVehicle.versionLabel.trim();
                        
                        
                    if(String.isEmpty(sicVehicle.firstRegistrationDate) == false)
                        vehicle.firstRegistrationDate = Date.valueof(sicVehicle.firstRegistrationDate.trim()).format();
                    if(String.isEmpty(sicVehicle.possessionBegin) == false)
                        vehicle.possessionBegin = Date.valueof(sicVehicle.possessionBegin.trim()).format();
                    if(String.isEmpty(sicVehicle.possessionEnd) == false)
                        vehicle.possessionEnd = Date.valueof(sicVehicle.possessionEnd.trim()).format();
                    if(String.isEmpty(sicVehicle.registrationDate) == false)
                        vehicle.lastRegistrationDate = Date.valueof(sicVehicle.registrationDate.trim()).format();   
                   
                    if (String.isEmpty(sicVehicle.registration) == false)
                        vehicle.registrationNumber = sicVehicle.registration.trim();
                   
                   	if (String.isEmpty(sicVehicle.new_x) == false)
                        vehicle.vnvo = sicVehicle.new_x.trim();
                    
                   
                    if (String.isEmpty(sicVehicle.purchaseNature) == false)
                    {
                        for (String vehicleName: vehicleRelationsNames) {
                            MDMVehicleRelationType__c vehicleRelationValue = mdmVehicleRelationType.get(vehicleName);
                            if (String.isEmpty(vehicleRelationValue.value__c) == false) 
                                if (vehicleRelationValue.Value__c.equalsIgnoreCase(sicVehicle.purchaseNature) && vehicleRelationValue.dataSource__c == 'RBX') 
                                    vehicle.vehicleType = vehicleRelationValue.VehicleRelationType__c;
                        }
                        if(String.isEmpty(vehicle.vehicleType ) == true)
                            vehicle.vehicleType = sicVehicle.purchaseNature; //value by default
                    }
                        
                    // Contracts  
                    
                    if (sicVehicle.contractList != null)
                    	if (sicVehicle.contractList.size() > 0)
                    	{
		                    vehicle.cont = new List<TRVResponseWebservices.Contract>();   
		                    for(TRVwsdlRbx.Contract contr : sicVehicle.contractList)
		                    {
		                    	TRVResponseWebservices.Contract contract = new TRVResponseWebservices.Contract();
		                    	
		                    	//mapping fields of contracts 
		                    	if (String.isEmpty(contr.idContrat) == false)
		                    		contract.idContrat = contr.idContrat.trim();
		                    	if (String.isEmpty(contr.type_x) == false)
		                    		contract.type_x = contr.type_x.trim();
		                    	if (String.isEmpty(contr.techLevel) == false)
		                    		contract.techLevel = contr.techLevel.trim();
		                    	if (String.isEmpty(contr.serviceLevel) == false)
		                    		contract.serviceLevel = contr.serviceLevel.trim();
		                    	if (String.isEmpty(contr.productLabel) == false)
		                    		contract.productLabel = contr.productLabel.trim();
		                    	if (String.isEmpty(contr.initKm) == false)
		                    		contract.initKm = contr.initKm.trim();
		                    	if (String.isEmpty(contr.maxSubsKm) == false)
		                    		contract.maxSubsKm = contr.maxSubsKm.trim();
		                    	if (String.isEmpty(contr.subsDate) == false)
		                    		contract.subsDate = contr.subsDate.trim();
		                    	if (String.isEmpty(contr.initContractDate) == false)
		                    		contract.initContractDate = contr.initContractDate.trim();
		                    	if (String.isEmpty(contr.endContractDate) == false)
		                    		contract.endContractDate = contr.endContractDate.trim();
		                    	if (String.isEmpty(contr.status) == false)
		                    		contract.status = contr.status.trim();
		                    	if (String.isEmpty(contr.updDate) == false)
		                    		contract.updDate = contr.updDate.trim();
		                    	if (String.isEmpty(contr.idMandator) == false)
		                    		contract.idMandator = contr.idMandator.trim();
		                    	if (String.isEmpty(contr.idCard) == false)
		                    		contract.idCard = contr.idCard.trim();
		                    	
		                    	vehicle.cont.add(contract);  	    
		                    }
	                    }
	                    infoClient.vcle.add(vehicle);
	                }
                
                //add info into InfoClient 
                pDataResponse.response.infoClients.add(infoClient);
                pDataResponse.response.rawResponse = iSICResponse.toString();
                
            }
            system.debug('#### TRVWebservicesRBX - mapPersonalDataFields - TRV - Parsing END');
        }
        system.debug('#### TRVWebservicesRBX - mapPersonalDataFields - TRV - END');
        return pDataResponse;
    }
    
    
}