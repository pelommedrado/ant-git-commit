public with sharing class WSC06_DMSStockBO
{
	private static final WSC06_DMSStockBO instance = new WSC06_DMSStockBO();
	
    /*private constructor to prevent the creation of instances of this class*/
    private WSC06_DMSStockBO(){}
    
    /**
     * Method responsible for providing the instance of this class..
     */  
    public static WSC06_DMSStockBO getInstance(){
        return instance;
    }

    public List<VRE_VehRel__c> fetchVehicleRelationRecordsUsingAccountIDBIR( String numberBIR ){
		List<VRE_VehRel__c> lstVehicleRelationRecords = VFC18_VehicleRelationDAO.getInstance().fetchVREUsingAccountIDBIR( numberBIR );
		return lstVehicleRelationRecords;
    }

    /*
    public List<VEH_Veh__c> fetchVehicleRecordsUsingRelationIDs( List<Id> lstVehiclesIDs ){
    	List<VEH_Veh__c> lstVehicleRecords = new List<VEH_Veh__c>();
    	lstVehicleRecords = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingVehicleRelationIDs( lstVehiclesIDs );
    	return lstVehicleRecords;
    }
    
    public List<VEH_Veh__c> fetchAllVehicleRecords(){
    	List<VEH_Veh__c> lstVehicleRecords = new List<VEH_Veh__c>();
    	lstVehicleRecords = VFC21_VehicleDAO.getInstance().fetchVehicleRecords();
    	return lstVehicleRecords;
    }
    */
    
    public List<Account> fetchAccountRecordsUsingIDBIR( String numberBIR ){
    	List<Account> lstAccountRecords = new List<Account>();
    	lstAccountRecords = VFC12_AccountDAO.getInstance().checkAccountsWithIDBIR( numberBIR );
    	return lstAccountRecords;
    }
    
    public List<WSC04_VehicleDMSVO> returnVehicleDMS( String numberBIR ){
    	
    	Map<String,WSC04_VehicleDMSVO> lstVehicleVO = new Map <String,WSC04_VehicleDMSVO>();
    	List<Account> lstAccountRecords = WSC06_DMSStockBO.getInstance().fetchAccountRecordsUsingIDBIR(numberBIR);
    	Account dealer = VFC12_AccountDAO.getInstance().fetchDealersUsing_IDBIR(numberBIR).values().get(0);
		
		if( lstAccountRecords.size() > 0 ){
			
			List<VRE_VehRel__c> lstVehicleRelationRecords = WSC06_DMSStockBO.getInstance().fetchVehicleRelationRecordsUsingAccountIDBIR(numberBIR);
			
			if( lstVehicleRelationRecords.size() > 0 ){
				for( Account accounts : lstAccountRecords ){
					for( VRE_VehRel__c relations : lstVehicleRelationRecords ){
						if( accounts.Id == relations.Account__c ){
							WSC04_VehicleDMSVO vehicleDMS = new WSC04_VehicleDMSVO();
							if( relations.VIN__r.Name != null )
								vehicleDMS.VIN = relations.VIN__r.Name;
							if( relations.VIN__r.Model__c != null )
								vehicleDMS.model = relations.VIN__r.Model__c;
							if( relations.VIN__r.Version__c != null )
								vehicleDMS.version = relations.VIN__r.Version__c;
							if( relations.VIN__r.DateofManu__c != null )
								vehicleDMS.manufacturingYear = relations.VIN__r.DateofManu__c.year();
							if( relations.VIN__r.ModelYear__c != null )
								vehicleDMS.modelYear  = Integer.valueOf( relations.VIN__r.ModelYear__c );
							if( relations.VIN__r.Color__c != null )
								vehicleDMS.color  = relations.VIN__r.Color__c;
							if( relations.VIN__r.Optional__c != null )
								vehicleDMS.optionals = relations.VIN__r.Optional__c;
							if( relations.VIN__r.Price__c != null )
								vehicleDMS.price = relations.VIN__r.Price__c;
							if( relations.VIN__r.LastModifiedDate != null )
								vehicleDMS.lastUpdateDate = relations.VIN__r.LastModifiedDate;
							if( relations.VIN__r.Status__c != null )
								vehicleDMS.status = getVehicleStatus(relations,dealer);
							if( relations.DateOfEntryInStock__c != null )
								vehicleDMS.entryInventoryDate = relations.DateOfEntryInStock__c;
							
							//vehicleDMS.error = false;
							lstVehicleVO.put(relations.VIN__r.Id, vehicleDMS );
						}
					}
				}
			}
		}
		
		if(dealer.WebServiceVersion__c == '1.0'){
			
			checkReservedVehicles(lstVehicleVO);
			
    	}
		
		
		return lstVehicleVO.values();
    }
    
    public String getVehicleStatus(VRE_VehRel__c relations, Account dealer){
    	
    	if(dealer.WebServiceVersion__c == '1.0'){
    		if( relations.VIN__r.Status__c == 'Quality Blockade'|| relations.VIN__r.Status__c == 'Comercial Blockade' || relations.VIN__r.Status__c == 'Stock'){
    			return 'Available';
    		}
    	}else if(dealer.WebServiceVersion__c == '2.0'){
    		// nada a fazer
    	}
    	
    	return relations.VIN__r.Status__c;
    	
    }
    
    public void checkReservedVehicles(Map<String,WSC04_VehicleDMSVO> lstVehicleVO){
    	
    	List<VehicleBooking__c> listBooking = VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(lstVehicleVO.keyset(), 'Active');
    	
    	for(VehicleBooking__c booking:listBooking){
			lstVehicleVO.get(booking.Vehicle__c).Status = 'Reserved' ;    		
    	}
    	
    }
    
    
    //
	// update vehicle objects in SFDC
	//
	public List<WSC04_VehicleDMSVO> updateLstVehicleDMSVO(String stringBIR, List<WSC04_VehicleDMSVO> lstVehicleDMSVO, 
	   List<WSC04_VehicleDMSVO> lstErrorVehicleDMSVO)
	{
		
		Id personalAccountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
		
		system.debug('*** updateLstVehicleDMSVO(): begin');

		// Returned list
		List<WSC04_VehicleDMSVO> lstVehicleDMSVOWithStatus = new List<WSC04_VehicleDMSVO>();

		// List of Vehicles and VehiclesRelations to update or insert
		List<VEH_Veh__c> lstVehiclesToUpsert = new List<VEH_Veh__c>();
		Map<String,VEH_Veh__c> mapVehiclesBookingToUpsert = new Map<String,VEH_Veh__c>();
		Map<String,VEH_Veh__c> mapVehiclesBookingToCancel = new Map<String,VEH_Veh__c>();
		Map<String,VRE_VehRel__c> mapVehiclesRelationsToUpsert = new Map<String,VRE_VehRel__c>();
		List<Id> lstVehiclesToInativeRelations = new List<Id>();

		// Map for Vehicle input list
		Map<String, WSC04_VehicleDMSVO> mapVehicleDMSVO = new Map<String, WSC04_VehicleDMSVO>();
		for (WSC04_VehicleDMSVO vo : lstVehicleDMSVO) {
			mapVehicleDMSVO.put(vo.VIN, vo);
		}

		system.debug('*** VIN.size() --> '+mapVehicleDMSVO.size());
		system.debug('*** VIN records --> '+mapVehicleDMSVO);
		
		// We should have only one Dealer Account:
		Account dealer = WSC06_DMSStockBO.getInstance().fetchAccountRecordsUsingIDBIR(stringBIR)[0];
		system.debug('*** dealer.Id:'+dealer.Id);

		// Map<VIN, VEH_Veh__c>
		Map<String, VEH_Veh__c> mapVehicle = VFC21_VehicleDAO.getInstance().fetchVehicleUsingVIN(mapVehicleDMSVO.KeySet());
		system.debug('*** mapVehicle.size():'+mapVehicle.size());

		// Map of Vehicles to update
		Map<String, VEH_Veh__c> mapVehiclesToUpdate = new Map<String, VEH_Veh__c>();

		try
		{
			for (WSC04_VehicleDMSVO vehicleVO : lstVehicleDMSVO)
			{
				VEH_Veh__c vehicleDB = mapVehicle.get(vehicleVO.VIN);
				system.Debug('*** vehicleDB --> '+vehicleDB);
				
				if (vehicleDB != NULL)
				{
					// AN UPDATE
					system.debug('*** UPDATE VEHICLE');

					try
					{
						// Convert the date from DB to user GMT
						Datetime dtDB = Datetime.newInstanceGMT(vehicleDB.LastModifiedDate.year(), vehicleDB.LastModifiedDate.month(),
							vehicleDB.LastModifiedDate.day(), vehicleDB.LastModifiedDate.hour(), vehicleDB.LastModifiedDate.minute(),
							vehicleDB.LastModifiedDate.second());

						if (vehicleVO.lastUpdateDate <= dtDB) {
							//vehicleVO.error = true;
							vehicleVO.errorMessage = 'B203 - Vehicle not updated: lastModifiedDate is before than the record at Salesforce.';
							lstErrorVehicleDMSVO.add(vehicleVO);
						}
						else if (vehicleDB.Status__c != vehicleVO.Status 
						      && vehicleDB.Status__c == 'In Transit' && vehicleVO.Status == 'To be billed') {
							vehicleVO.errorMessage = 'B204 - status could not be changed from [In Transit] to [To be billed].';
							lstErrorVehicleDMSVO.add(vehicleVO);
						}
						//else if (vehicleDB.Status__c != vehicleVO.Status 
						//      && vehicleDB.Status__c == 'Available' && (vehicleVO.Status == 'To be billed' 
						//                                             || vehicleVO.Status == 'In Transit')) {
						//	vehicleVO.errorMessage = 'B206 - status could not be changed from [Available] to [To be billed, In Transit].';
						//	lstErrorVehicleDMSVO.add(vehicleVO);
						//}
						//else if (vehicleDB.Status__c != vehicleVO.Status 
						//      && vehicleDB.Status__c == 'Billed') {
						//	vehicleVO.errorMessage = 'B208 - status could not be changed from [Billed] to any other status.';
						//	lstErrorVehicleDMSVO.add(vehicleVO);
						//}
						//else if (vehicleDB.Status__c != vehicleVO.Status 
						  //    && vehicleDB.Status__c == 'Reserved' && vehicleVO.Status != 'Billed') {
						      	// Retirada a validação a pedido da Renault - 22/05/2013 - Thiago Alves dos Santos
							//vehicleVO.errorMessage = 'B209 - status could not be changed from [Reserved] to [Available, To be billed, In Transit].';
							//lstErrorVehicleDMSVO.add(vehicleVO);
						//}
						else {
							// Vehicle data
							vehicleDB.Model__c = vehicleVO.model;
							vehicleDB.Version__c = vehicleVO.version;
							vehicleDB.DateofManu__c = Date.newinstance(vehicleVO.manufacturingYear, 1, 1);
							vehicleDB.ModelYear__c = vehicleVO.modelYear;
							vehicleDB.Color__c = vehicleVO.color;
							vehicleDB.Price__c = vehicleVO.price;

							//Se veículo In Transit converte para Available - Solicitado pela Renault
							//if(vehicleVO.status == 'In Transit'){
							//	vehicleVO.status = 'Available';
							//	vehicleVO.optionals =  vehicleVO.optionals + ' [EM TRANSITO] ';
							//}
							
							if(vehicleVO.status == 'Reserved'){
								vehicleVO.status = 'Stock';
								mapVehiclesBookingToUpsert.put(vehicleDB.Name,vehicleDB);
							}else{
								mapVehiclesBookingToCancel.put(vehicleDB.Name,vehicleDB);
							}
							
							if(vehicleVO.status == 'Available'){
								vehicleVO.status = 'Stock';
							}
							
							vehicleDB.Status__c = vehicleVO.status;
							vehicleDB.Optional__c = vehicleVO.optionals;

							lstVehiclesToUpsert.add(vehicleDB);
							System.debug('######## vehicleDB '+vehicleDB);
						}
					}
					catch (Exception e) {
						//vehicleVO.error = true;
						vehicleVO.errorMessage = 'B301 - Unhandled error: ' + e.getMessage();
						lstErrorVehicleDMSVO.add(vehicleVO);
					}
				}
				else
				{
					// AN INSERT
					system.debug('*** INSERT VEHICLE');
					
					// Incluido a possibilidade de incluir veículos com o status Reservado 24/05/2013
					if((dealer.WebServiceVersion__c == '1.0') && ( vehicleVO.status != 'Available' && vehicleVO.status != 'Reserved' && 
	    		          vehicleVO.status != 'Billed' && vehicleVO.status != 'In Transit')) {
	    				
	    				vehicleVO.errorMessage = 'B205 - To include a vehicle, the status must belong to one of this list [Available / Billed / In Transit / Reserved].';
						lstErrorVehicleDMSVO.add(vehicleVO);
    				
    				}else if((dealer.WebServiceVersion__c == '2.0')	&& ( vehicleVO.status != 'Stock' && vehicleVO.status != 'Quality Blockade' && 
	    		          vehicleVO.status != 'Comercial Blockade' && vehicleVO.status != 'Billed' && vehicleVO.status != 'In Transit')) {
	    				
	    				vehicleVO.errorMessage = 'B205 - To include a vehicle, the status must belong to one of this list [Available / Billed / In Transit / Reserved].';
						lstErrorVehicleDMSVO.add(vehicleVO);
    				
    				}else { 
						VEH_Veh__c newVehicleDB = new VEH_Veh__c();
						newVehicleDB.Name = vehicleVO.VIN;
						newVehicleDB.Model__c = vehicleVO.model;
						newVehicleDB.Version__c = vehicleVO.version;
						newVehicleDB.DateofManu__c = Date.newinstance(vehicleVO.manufacturingYear, 1, 1); // use 1st of January
						newVehicleDB.ModelYear__c = vehicleVO.modelYear;
						newVehicleDB.Color__c = vehicleVO.color;
						newVehicleDB.Price__c = vehicleVO.price;
						
						if(vehicleVO.status == 'Reserved'){
							vehicleVO.status = 'Stock';
							mapVehiclesBookingToUpsert.put(newVehicleDB.Name,newVehicleDB);
						}else{
							mapVehiclesBookingToCancel.put(newVehicleDB.Name,newVehicleDB);
						}
						
						if(vehicleVO.status == 'Available'){
							vehicleVO.status = 'Stock';
						}
						
	
						newVehicleDB.Optional__c = vehicleVO.optionals;
						newVehicleDB.Status__c = vehicleVO.status;
						
						lstVehiclesToUpsert.add(newVehicleDB);
					}
				}
			} // end for


			system.debug('*** lstVehiclesToUpsert.size(): '+lstVehiclesToUpsert.size());
			system.debug('*** lstVehiclesToUpsert: '+lstVehiclesToUpsert);
			system.debug('*** lstVehiclesBookingToUpsert.size(): '+mapVehiclesBookingToUpsert.size());
			system.debug('*** lstVehiclesBookingToUpsert: '+ mapVehiclesBookingToUpsert);
			


			if (lstVehiclesToUpsert.size() > 0)
			{

				// Begin transaction
				Savepoint sp = Database.setSavepoint();
				
				try
				{
					//
					// 1st: Upsert vehicles
					//
					List<Id> lstVehIds = new List<Id>();
					List<Database.Upsertresult> lstVehicleResults = Database.upsert( lstVehiclesToUpsert, false );
					for( Database.Upsertresult vehicleUpsert : lstVehicleResults )
					{
						if( !vehicleUpsert.isSuccess() ){
							WSC04_VehicleDMSVO errorVeh = new WSC04_VehicleDMSVO();
							//errorVeh.error = true;
							errorVeh.errorMessage = 'B301 - Unhandled error: ' + vehicleUpsert.getErrors();
							lstErrorVehicleDMSVO.add( errorVeh );
						}
						else {
							lstVehIds.add(vehicleUpsert.getId());
						}
					}
		
		
					system.debug('*** lstVehIds.size(): '+lstVehIds.size());
					system.debug('*** lstVehIds: '+lstVehIds);
					
					//
					// 2nd: Handle Vehicle Relation
					//
		
					// Map<VIN__c, VRE_VehRel__c>
					Map<String, VRE_VehRel__c> mapVehicleRelation = VFC18_VehicleRelationDAO.getInstance().
						fetchVehicleRelationUsingVehicleIds(dealer.Id, lstVehIds);
					
					system.debug('*** mapVehicleRelation.size():'+mapVehicleRelation.size());
					system.debug('*** mapVehicleRelation :'+mapVehicleRelation);
		
					// Query Vehicles for the VIN  to upsert the relation
					Map<String, VEH_Veh__c> mapUpdatedVehicle = VFC21_VehicleDAO.getInstance().
						fetchVehicleUsingIds(lstVehIds);
					system.debug('*** mapUpdatedVehicle.size():'+mapUpdatedVehicle.size());
					system.debug('*** mapUpdatedVehicle :'+mapUpdatedVehicle);
					
					// For each vehicle previously inserted
					for (VEH_Veh__c veh : mapUpdatedVehicle.values()) {
						
						WSC04_VehicleDMSVO vehFromDMS = mapVehicleDMSVO.get(veh.Name);
		
						// Verify if the vehicle has a Relation
						VRE_VehRel__c vehicleRelationDB = mapVehicleRelation.get(veh.Name);
		
						if (vehicleRelationDB != NULL) {
							// UPDATE Relation
							system.debug('*** Vehicle Relation UPDATE');
							vehicleRelationDB.DateOfEntryInStock__c = vehFromDMS.entryInventoryDate;
							vehicleRelationDB.Status__c = 'Active';		
							mapVehiclesRelationsToUpsert.put(vehicleRelationDB.Name,vehicleRelationDB);							
						}
						else {
							// INSERT Relation
							system.debug('*** Vehicle Relation INSERT');
		
							VRE_VehRel__c newVehicleRelationDB = new VRE_VehRel__c();
							newVehicleRelationDB.VIN__c = veh.Id;
							newVehicleRelationDB.Account__c = dealer.Id;
							newVehicleRelationDB.DateOfEntryInStock__c = vehFromDMS.entryInventoryDate;
							
							System.debug('Ativando: ' + newVehicleRelationDB.Account__c);
							mapVehiclesRelationsToUpsert.put(newVehicleRelationDB.VIN__c,newVehicleRelationDB);
							
							// Adicionando veículo para inativar relações
							lstVehiclesToInativeRelations.add(veh.Id);
							
							// REMOVER
							//001D000000yoQyo - Taguatinga
							//001D0000010zDL8 - Colorado
							
							// Se o veículo for de Taguatinga cria uma cópia para Colorado
							//if(newVehicleRelationDB.Account__c == '001D000000yoQyo'){
							//	VRE_VehRel__c newVehicleRelationDB_2 = new VRE_VehRel__c();
							//	newVehicleRelationDB_2.VIN__c = veh.Id;
							//	newVehicleRelationDB_2.Account__c = '001D0000010zDL8';
							//	newVehicleRelationDB_2.DateOfEntryInStock__c = vehFromDMS.entryInventoryDate;
							//	lstVehiclesRelationsToUpsert.add(newVehicleRelationDB_2);
							//}
							
							// Se o veículo for de Colorado cria uma cópia para Taguatinga
							//if(newVehicleRelationDB.Account__c == '001D0000010zDL8'){
							//	VRE_VehRel__c newVehicleRelationDB_3 = new VRE_VehRel__c();
							//	newVehicleRelationDB_3.VIN__c = veh.Id;
							//	newVehicleRelationDB_3.Account__c = '001D000000yoQyo';
							//	newVehicleRelationDB_3.DateOfEntryInStock__c = vehFromDMS.entryInventoryDate;
							//	lstVehiclesRelationsToUpsert.add(newVehicleRelationDB_3);
							//}
                            
                            // REMOVER
							//001D000000yoQwH - Space Estoque
							//001D000000yoQwN - Space Recreio
							
							// Se o veículo for do estoque central da Space é replicado para Space recreio.
							//if(newVehicleRelationDB.Account__c == '001D000000yoQwH'){
							//	VRE_VehRel__c newVehicleRelationDB_2 = new VRE_VehRel__c();
							//	newVehicleRelationDB_2.VIN__c = veh.Id;
							//	newVehicleRelationDB_2.Account__c = '001D000000yoQwN';
							//	newVehicleRelationDB_2.DateOfEntryInStock__c = vehFromDMS.entryInventoryDate;
							//	mapVehiclesRelationsToUpsert.put(newVehicleRelationDB_2.name,newVehicleRelationDB_2);
							//}						
							
						}
					}
					
					system.Debug(' mapVehiclesRelationsToUpsert ---->'+mapVehiclesRelationsToUpsert);
		
		
					//
					// UPSERT For Vehicle Relations
					//
					
					System.debug('VEICULOS PARA INATIVAR RELACIONAMENTOS: ' + lstVehIds);
					 
					// Inativando outras relations
					//List<VRE_VehRel__c> mapVehiclesRelationsToInative = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationActiveUsingVehicleIds(lstVehiclesToInativeRelations);
					List<VRE_VehRel__c> mapVehiclesRelationsToInative = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationActiveUsingVehicleIds(lstVehIds);
					
						
					System.debug('RELACIONAMENTOS PARA INATIVAR: ' + mapVehiclesRelationsToInative);	
						
					for(VRE_VehRel__c vehRelActive : mapVehiclesRelationsToInative){
						if(vehRelActive.Account__r.RecordTypeId != personalAccountRecordTypeId && !mapVehiclesRelationsToUpsert.containsKey(vehRelActive.Name)){
							System.debug('Inativando:' + vehRelActive.Name + ' - ' + vehRelActive.Account__c);
							vehRelActive.Status__c = 'Inactive';
							mapVehiclesRelationsToUpsert.put(vehRelActive.name, vehRelActive);
						}
					}
					
					List<Database.Upsertresult> vehicleRelation_Results = Database.upsert(mapVehiclesRelationsToUpsert.values());
					
					for( Database.Upsertresult vehicelRelationUpsert : vehicleRelation_Results ){
						if( !vehicelRelationUpsert.isSuccess() ){
							WSC04_VehicleDMSVO errorDMS = new WSC04_VehicleDMSVO();
							//errorDMS.error = true;
							errorDMS.errorMessage = 'B301 - Unhandled error: ' + vehicelRelationUpsert.getErrors();
							lstErrorVehicleDMSVO.add( errorDMS );
						}
					}
					
					if(mapVehiclesBookingToUpsert.size() != 0){
											
						// Booking Vehicles
						List<VehicleBooking__c> lstReserves = new List<VehicleBooking__c>();
						Map<String,VEH_Veh__c> vehiclesToBooking	= VFC21_VehicleDAO.getInstance().fetchVehicleUsingVIN(mapVehiclesBookingToUpsert.keyset());
						System.debug('vehiclesToBooking...>> ' + vehiclesToBooking); 
											
						for(VEH_Veh__c vehicle : vehiclesToBooking.values()){
							VehicleBooking__c reserve = new VehicleBooking__c();
							reserve.Vehicle__c = vehicle.Id;
							reserve.Status__c = 'Active';
							lstReserves.add(reserve);
						}
						
						List<Database.Upsertresult> vehicleBookings_Results = Database.upsert(lstReserves);
						
						for( Database.Upsertresult vehicleBookings : vehicleBookings_Results ){
							if( !vehicleBookings.isSuccess() ){
								WSC04_VehicleDMSVO errorDMS = new WSC04_VehicleDMSVO();
								//errorDMS.error = true;
								errorDMS.errorMessage = 'B301 - Unhandled error: ' + vehicleBookings.getErrors();
								lstErrorVehicleDMSVO.add( errorDMS );
							}
						}
					}
					
					if(mapVehiclesBookingToCancel.size() != 0){
						
						Set<String> setVehicleId = new Set<String>();
						for(VEH_Veh__c v:mapVehiclesBookingToCancel.values()){
							setVehicleId.add(v.Id);
						}
						
						// Booking Vehicles
						List<VehicleBooking__c> lstReserves = new List<VehicleBooking__c>();
						lstReserves = VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(setVehicleId, 'Active');
						//Map<String,VEH_Veh__c> vehiclesToBooking	= VFC21_VehicleDAO.getInstance().fetchVehicleUsingVIN(mapVehiclesBookingToCancel.keyset());
						//System.debug('vehiclesToCancel...>> ' + vehiclesToBooking); 
											
						for(VehicleBooking__c reserve :lstReserves){
							if(reserve.Quote__c == null){
								reserve.Status__c = 'Canceled';
							}
						}
						
						List<Database.Upsertresult> vehicleBookings_Results = Database.upsert(lstReserves);
						
						for( Database.Upsertresult vehicleBookings : vehicleBookings_Results ){
							if( !vehicleBookings.isSuccess() ){
								WSC04_VehicleDMSVO errorDMS = new WSC04_VehicleDMSVO();
								//errorDMS.error = true;
								errorDMS.errorMessage = 'B301 - Unhandled error: ' + vehicleBookings.getErrors();
								lstErrorVehicleDMSVO.add( errorDMS );
							}
						}
					}
				}
				catch( Exception e ){
					system.debug('*** rollback');
					system.debug('*** Error e.message:'+e.getMessage());
					Database.rollback(sp);
					
					WSC04_VehicleDMSVO vehicleErrorVO = new WSC04_VehicleDMSVO();
					vehicleErrorVO.errorMessage = 'B301 - Unhandled error: ' + e;
					lstErrorVehicleDMSVO.add(vehicleErrorVO);
				}
			}
		}
		catch( Exception e ){
			system.debug('*** Error e.message:'+e.getMessage());
			system.debug('*** Error e.stack:'+e.getStackTraceString());
			WSC04_VehicleDMSVO vehicleErrorVO = new WSC04_VehicleDMSVO();
			//vehicleErrorVO.error = true;
			vehicleErrorVO.errorMessage = 'B301 - Unhandled error: ' + e;
			lstErrorVehicleDMSVO.add(vehicleErrorVO);
			return lstErrorVehicleDMSVO;
		}
		
		return lstErrorVehicleDMSVO;
	}
	
	
}