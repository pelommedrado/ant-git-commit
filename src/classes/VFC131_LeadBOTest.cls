@isTest(SeeAllData=true)
public with sharing class VFC131_LeadBOTest {

    static User u;
    static Lead lead;
    static Lead leadOld;
    static Account dealer;
    static Campaign camp;
    static Account persAcc;
    static Opportunity opp;
    static Model__c model;
    static Quote quote;

    static void setup(){
        
        u = [SELECT Id FROM User WHERE Name = 'Renault do Brasil'];

        dealer = new Account(
            Name='teste',
            RecordTypeId = Utils.getRecordTypeId('Account','Network_Site_Acc'),
            Manager_Type__c = 'Community Cloud',
            IDBIR__c = '4512'
        );

        persAcc = new Account(
            FirstName='teste',
            PersEmailAddress__c = 'teste@test.com',
            Lastname='teste2'
        );

        List<Account> accList = new List<Account>();
        accList.add(dealer);
        accList.add(persAcc);
        Insert accList;

        lead = new Lead(
            FirstName = 'teste',
            LastName = 'teste',
            CRV_CurrentVehicle_WebToLead__c = 'teste',
            CPF_CNPJ__c = '59512193299',
            PromoCode__c = 'test',
            Email = 'testeamil@email.com.br',
            Phone = '12452365',
            DealerOfInterest__c = dealer.Id,
            IdDealer__c = dealer.Id,
            RecordTypeId = Utils.getRecordTypeId('Lead','DVR'),
            LeadSource = 'DEALER',
            SubSource__c = 'PROSPECT'
        );

        leadOld = new Lead(
            FirstName = 'teste2',
            LastName = 'teste2',
            CRV_CurrentVehicle_WebToLead__c = 'teste',
            CPF_CNPJ__c = '14105936140',
            PromoCode__c = 'test',
            Email = 'testeamil@email.com.br',
            Phone = '12452365',
            DealerOfInterest__c = dealer.Id,
            IdDealer__c = dealer.Id,
            RecordTypeId = Utils.getRecordTypeId('Lead','DVR'),
            LeadSource = 'DEALER',
            SubSource__c = 'PROSPECT'
        );

        List<Lead> leadList = new List<Lead>();
        leadList.add(lead);
        leadList.add(leadOld);
        Insert leadList;

        opp = MyOwnCreation.getInstance().criaOpportunity();
        opp.AccountId = persAcc.Id;
        Insert opp;

        quote = MyOwnCreation.getInstance().criaQuote();
        quote.OpportunityId = opp.Id;
        Insert quote;

        VEH_Veh__c v = new VEH_Veh__c(Name='12452365985412547');
        Insert v;
        
        camp = new Campaign(Name = 'teste');
        Insert camp;

        model = new Model__c(
            Name = 'CLIO',
            Status_SFA__c = 'Active',
            Model_PK__c = 'CLIO'
        );
        Insert model;
        
        PVVersion__c version = new PVVersion__c(
            Model__c = model.Id,
            Name = 'Version',
            Semiclair__c = 'LI H Test',
            PVC_Maximo__c = 1,
            PVR_Minimo__c = 1,
            Price__c = 34000,
            Version_Id_Spec_Code__c = 'LI H Test'
        );
        Insert version;
        
        Optional__c opt1 = new Optional__c(
            Name = 'Cor',
            Optional_Code__c = 'Cor',
            Version__c = version.Id,
            Type__c = 'Cor'
        );
        
        Optional__c opt2 = new Optional__c(
            Name = 'Trim',
            Optional_Code__c = 'Trim',
            Version__c = version.Id,
            Type__c = 'Trim'
        );
        
        Optional__c opt3 = new Optional__c(
            Name = 'Harmonia',
            Optional_Code__c = 'Harm',
            Version__c = version.Id,
            Type__c = 'Harmonia'
        );
        
        Optional__c opt4 = new Optional__c(
            Name = 'Pack',
            Optional_Code__c = 'Pack',
            Version__c = version.Id,
            Type__c = 'Pack'
        );
        
        List<Optional__c> optList = new List<Optional__c>();
        optList.add(opt1);
        optList.add(opt2);
        optList.add(opt3);
        optList.add(opt4);
        Insert optList;

        Product2 prod = new Product2(
            Name = 'CLIO'
        );
        Insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
            UnitPrice = 10000, 
            IsActive = true
        );
        Insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IsActive = true
        );
        Insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = prod.Id,
            UnitPrice = 12000, 
            IsActive = true
        );
        Insert customPrice;

    }
    
    static testMethod void testDVR(){

        setup();

        Test.startTest();
        
        system.runAs(u){
            
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead,camp);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(persAcc , lead, lead);
            VFC131_LeadBO.getInstance().convertLeadNotCreateOpportunity(persAcc.Id, leadOld);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(null , lead, lead);
            
            Id accountId = [SELECT ConvertedAccountId FROM Lead WHERE Id =: lead.Id].ConvertedAccountId;
            
            Opportunity opp1 = [
                SELECT Id, OpportunitySource__c, OpportunitySubSource__c, Detail__c, Sub_Detail__c 
                FROM Opportunity 
                WHERE AccountId =: accountId
            ];
            
            system.assertEquals(lead.IsConverted == true, false, 'Lead Convertido!!!');
            system.assertEquals(opp1.OpportunitySource__c, lead.LeadSource);
            system.assertEquals(opp1.OpportunitySubSource__c, lead.SubSource__c);
            system.assertEquals(opp1.Detail__c, lead.Detail__c);
            system.assertEquals(opp1.Sub_Detail__c, lead.Sub_Detail__c); 
            
        }

        Test.stopTest();
    }
    
    static testMethod void testVendasPF(){
        
        setup();

        Test.startTest();

        system.runAs(u){
            
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead,camp);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(persAcc , lead, lead);
            VFC131_LeadBO.getInstance().convertLeadNotCreateOpportunity(persAcc.Id, leadOld);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(null , lead, lead);
            
            Id accountId = [SELECT ConvertedAccountId FROM Lead WHERE Id =: lead.Id].ConvertedAccountId;
            
            Opportunity opp1 = [
                SELECT Id, OpportunitySource__c, OpportunitySubSource__c, Detail__c, Sub_Detail__c 
                FROM Opportunity 
                WHERE AccountId =: accountId
            ];
            
            system.assertEquals(lead.IsConverted == true, false, 'Lead Convertido!!!');
            system.assertEquals(opp1.OpportunitySource__c, lead.LeadSource);
            system.assertEquals(opp1.OpportunitySubSource__c, lead.SubSource__c);
            system.assertEquals(opp1.Detail__c, lead.Detail__c);
            system.assertEquals(opp1.Sub_Detail__c, lead.Sub_Detail__c); 
            
        }

        Test.stopTest();
        
    }
    
    static testMethod void testService(){
        
        setup();
        
        Test.startTest();
        
        system.runAs(u){
            
            lead.RecordTypeId = Utils.getRecordTypeId('Lead', 'Service');
            lead.VehicleOfInterest__c = 'CLIO';
            lead.SecondVehicleOfInterest__c = 'CLIO';
            lead.VehicleRegistrNbr__c = 'DXC1234';
            lead.BIR_Dealer_of_Interest__c = '4512';
            Update lead;
            
            List<Lead> lsLead = new List<Lead>();
            lsLead.add(lead);
            
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead, camp);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(persAcc , lead, lead);
            VFC131_LeadBO.getInstance().convertLeadNotCreateOpportunity(persAcc.Id, leadOld);
            VFC131_LeadBO.getInstance().mappingLeadToAccount(null , lead, lead);
            VFC131_LeadBO.getInstance().updateLookupVehicleOfInterest(lsLead);
            VFC131_LeadBO.getInstance().updateLookupSecondVehicleOfInterest(lsLead);
            
            Id accountId = [SELECT ConvertedAccountId FROM Lead WHERE Id =: lead.Id].ConvertedAccountId;
            
            Opportunity opp1 = [
                SELECT Id, OpportunitySource__c, OpportunitySubSource__c, Detail__c, Sub_Detail__c 
                FROM Opportunity 
                WHERE AccountId =: accountId
            ];
            
            system.assertEquals(lead.IsConverted == true, false, 'Lead Convertido!!!');
            system.assertEquals(opp1.OpportunitySource__c, lead.LeadSource);
            system.assertEquals(opp1.OpportunitySubSource__c, lead.SubSource__c);
            system.assertEquals(opp1.Detail__c, lead.Detail__c);
            system.assertEquals(opp1.Sub_Detail__c, lead.Sub_Detail__c); 
            
        }
        
        Test.stopTest();
        
    }

    static testMethod void testCreateQuote(){
        
        setup();

        Test.startTest();
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().createQuote(opp); 
        }

        Test.stopTest();
        
    }

    /*static testMethod void testCreateQuoteLineItem(){
        
        setup();

        Test.startTest();
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().createQuoteLineItem(quote.Id, lead, model);
        }

        Test.stopTest();
    
    }*/

    static testMethod void testCreateTestDrive(){
        
        setup();

        Test.startTest();
        
        system.runAs(u){
            VFC131_LeadBO.createTestDrive(opp.Id, opp.AccountId, 'CLIO');
        }

        Test.stopTest();
        
    }

    static testMethod void testInsertTask(){
        
        setup();

        Test.startTest();
        
        system.runAs(u){

            opp = MyOwnCreation.getInstance().criaOpportunity();
            opp.AccountId = persAcc.Id;
            opp.OwnerId = UserInfo.getUserId();
            Insert opp;

            VFC131_LeadBO.getInstance().insertTask(opp);
        }

        Test.stopTest();
        
    }

    static testMethod void testGeraLeadvoucher(){
        
        setup();

        Test.startTest();
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().geraLeadvoucher(lead);
        }

        Test.stopTest();
        
    }

    static testMethod void testAtribuiProprietario(){
        
        setup();

        Test.startTest();

        dealer.Manager_Type__c = 'Community Cloud';
        Update dealer;
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().atribuiProprietario(opp, dealer);
        }

        Test.stopTest();
        
    }
    
    static testMethod void testSuperLead() {
        
        setup();

        Test.startTest();
        
        lead.TransactionCode__c  = '#@$36623DFShE';
        lead.VehicleOfInterest__c = 'CLIO';
        Update lead;
        
        model.IsNewRelease__c = true;
        Update model;
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead,camp);
        }

        Test.stopTest();
        
    }
    
    static testMethod void testSuperLeadWithSemiclairs() {
        
        setup();

        Test.startTest();
        
        lead.TransactionCode__c  = '#@$36623DFShE';
        lead.VehicleOfInterest__c = 'CLIO';
        lead.SemiclairVersion__c = 'LI H Test';
        lead.SemiclairColor__c = 'Cor';
        lead.SemiclairHarmony__c = 'Harm';
        lead.SemiclairUpholstered__c = 'Trim';
        lead.SemiclairOptional__c = 'Pack';
        Update lead;
        
        model.IsNewRelease__c = true;
        Update model;
        
        system.runAs(u){
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead,camp);
        }

        Test.stopTest();
        
    }
    
}