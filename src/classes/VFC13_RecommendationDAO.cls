/**
*	Class   -   VFC13_RecommendationDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*    
*   #01 <RameshPrabu> <31/08/2012>
*        Created this Class to handle record from Recommendation related Queries.
*/
public with sharing class VFC13_RecommendationDAO {
	
	private static final VFC13_RecommendationDAO instance = new VFC13_RecommendationDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC13_RecommendationDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC13_RecommendationDAO getInstance(){
        return instance;
    }
    
    /** 
    * This Method was used to get Recommendation Records 
    * @return lstRecommendation - fetch and return the result in lstRecommendation
    */
    public List<RCM_Recommendation__c> findRecommendationsWithName(){
        List<RCM_Recommendation__c> lstRecommendation = null;
        lstRecommendation = [select 
        						Id, 
        						Name, RecordTypeId,
        						Description__c
							from
								RCM_Recommendation__c
							where 
								Name != null];
		
        return lstRecommendation;
    }
}