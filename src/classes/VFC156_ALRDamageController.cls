public with sharing class VFC156_ALRDamageController implements FileUploaderCaller {
    
    public Flow.Interview.ALR_FlowDamage flowDamage {get;set;}
    public String varResourceImagem {get; set;}
    public String idDamage {get; set;}
    public String varActivateIsuranceID {get; set;}
    public String varDocumentID {get; set;}
    public String varFaseUpload {get; set;}
    public Id actInsID {get; set;}
    public String idAct {get; set;}
    
    public FileUploaderCaller self{
        get{
            return this;
        }
    }
    
    public Id oldParentId;
    public FileUploaderController componentController{get;set{this.componentController = value; system.debug( '@@@ COMPONENT CONTROLLER SETTER @@@' );}}
   
     public String varExtensionFile {
        get{
            System.debug( '@@@ EXTENSIONS GETTER @@@ ' + this.varExtensionFile );
            return this.varExtensionFile;
        } 
        set{
            System.debug( '@@@ EXTENSIONS SETTER @@@ ' + value );
            this.varExtensionFile = value;
        }
    }
    
    public Map< String, Id> phaseAttachmentMap;
    
    public VFC156_ALRDamageController(ApexPages.StandardController controller) {
        
        system.debug('### actInsID VFC156_ALRDamageController'+(Id)controller.getRecord().get( 'Id' ));
        system.debug('##### retURL '+ApexPages.currentPage().getParameters().get('retURL'));
        system.debug('##### getParameters '+ApexPages.currentPage().getParameters());
        if(String.isNotBlank((Id)controller.getRecord().get( 'Id' )))
        	actInsID = (Id)controller.getRecord().get( 'Id' );
        
     //   actInsID = ApexPages.currentPage().getParameters().get( 'actInsID' );
        varActivateIsuranceID = actInsID;
        system.debug('### actInsID VFC156_ALRDamageController '+actInsID+' varActivateIsuranceID '+varActivateIsuranceID);
        
        if(String.isEmpty(actInsID)){
            system.debug('##### VFC156_ALRDamageController '+ApexPages.currentPage().getParameters().get('retURL'));
            String acionamento = ApexPages.currentPage().getParameters().get('retURL');
            String idAcionamento;
            if(!String.isBlank(acionamento)){
                if(acionamento.length() > 8)
                   idAcionamento = acionamento.substringAfterLast('/');
                
                system.debug('**** idAcionamento: '+idAcionamento);
                varActivateIsuranceID = idAcionamento;
            }
        }
        
        this.phaseAttachmentMap = new Map< String, Id >();

    }
    
    public String getUrlImg(){
        
        String urlImg;
        if(flowDamage != null){
            urlImg = flowDamage.ALR_varResourceImagem;
            varResourceImagem = urlImg;
        }
        return urlImg;
    }
    
    public String getMsgErro(){
        String msgErro;
        if(flowDamage != null){
            msgErro = flowDamage.ALR_varMsgErro;
        }
        return msgErro;
    }
    
    public Id parentId{
    get{
      return ApexPages.currentPage().getParameters().get( 'pid' );
    }   
  }
    
    public Boolean getAuxPanel(){
    
        Boolean loadUp = false;
        if(flowDamage != null){
            /*system.debug('**** flowDamage.ALR_varFaseUpload '+flowDamage.ALR_varFaseUpload);
            system.debug('**** flowDamage.ALR_varDocumentID '+flowDamage.ALR_varDocumentID);*/
            if(flowDamage.ALR_varDocumentID != null && !String.isBlank(flowDamage.ALR_varDocumentID )){
                if(flowDamage.ALR_varFaseUpload != null && !String.isBlank(flowDamage.ALR_varFaseUpload )){
                    varDocumentID = flowDamage.ALR_varDocumentID;
                    varFaseUpload =  flowDamage.ALR_varFaseUpload;
                    //System.debug('**** flowDamage.ALR_varExtensionFile: '+flowDamage.ALR_varExtensionFile);
                    if(!String.isEmpty(flowDamage.ALR_varExtensionFile)){
                        varExtensionFile = flowDamage.ALR_varExtensionFile;
                        if(!varExtensionFile.equalsIgnoreCase('pdf')){
                           varExtensionFile = 'jpg';
                        }
                    }
                  
                    idAct = flowDamage.ALR_varActivateIsuranceID;
                    loadUp = true;
                }
            }
        System.debug( '@@@ GETAUXPANEL @@@\nFlow Data: ' + flowDamage.ALR_varExtensionFile + '\nController Data: ' + this.varExtensionFile );
        }

		system.debug('**** this.componentController '+this.componentController);
        if( this.componentController != null ){
            if( this.phaseAttachmentMap.containsKey( varDocumentID + varFaseUpload ) ){
                Id attId = this.phaseAttachmentMap.get( varDocumentID + varFaseUpload );
                if( this.componentController.attId != attId ){
                    this.componentController.errorMsg = null;
                    this.componentController.attId = attId;
                }
            } else {
                String errorMsg = this.componentController.errorMsg;
                this.componentController.reset();
                this.componentController.errorMsg = errorMsg;
            } 
            //else this.componentController.reset();

            //this.componentController.type = 'pdf';//varExtensionFile;
        }

        return loadUp;
    }
    
    public void fileUploaderCallback(FileUploaderController target){
		
        System.debug('**** fileUploaderCallback '+this.varDocumentID +' '+this.varFaseUpload +' '+ target.attId);
        this.phaseAttachmentMap.put( this.varDocumentID + this.varFaseUpload, target.attId );
    }
    
    public void registerController (FileUploaderController target){
        this.componentController = target;
    }
    
        
    public PageReference getFinish(){
        system.debug('*** varActivateIsuranceID '+varActivateIsuranceID);
        PageReference pageRef = new Pagereference('/'+varActivateIsuranceID);
        return pageRef;
    }
    
}