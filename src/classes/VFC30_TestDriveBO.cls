/**
*   Class   -   VFC30_TestDriveBO
*   Author  -   RameshPrabu
*   Date    -   15/11/2012
*
*   #01 <RameshPrabu> <15/11/2012>
*       BO class for Test Drive object to logical function.
**/
public with sharing class VFC30_TestDriveBO 
{
    private static final VFC30_TestDriveBO instance = new VFC30_TestDriveBO();
        
    /*private constructor to prevent the creation of instances of this class*/
    private VFC30_TestDriveBO()
    {
    }

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC30_TestDriveBO getInstance()
    {
        return instance;
    }
    
    /**
    *   This Method was used to some validate the  Test Drive Object fields before insertion
    *   @param  testDriveTrigger    -   New Test Drive record passed  and validate Test Drive Object fields.
    */
    public void validateNewTestDrive( List<TDV_TestDrive__c> testDriveTrigger ){
        
        for( TDV_TestDrive__c testDriveRecord : testDriveTrigger ){
            if( testDriveRecord.Dealer__c != null && testDriveRecord.TestDriveVehicle__c != null ){
                /* TestDrive Object TestDriveVehicle look up field id pass and return TestDriveVehicle record id from TestDriveVehicle Object */
                Id TestDriveAccountId = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicleAccount_UsingId(testDriveRecord.TestDriveVehicle__c);
                DateTime StartDateTime;
                DateTime EndDateTime;
                /* if testDrive DateofBooking field is not equal to null */
                if(testDriveRecord.DateBooking__c != null){
                    StartDateTime = testDriveRecord.DateBooking__c;
                    EndDateTime = StartDateTime.addMinutes(30);
                }
                /* TestDrive Object and DateofBooking parameter pass and return TestDrive record records from TestDrive Object */
                List<TDV_TestDrive__c> lstTestDriveRecords = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsingTestDriveVehId(testDriveRecord.TestDriveVehicle__c, StartDateTime);
                system.debug('lstTestDriveRecords....' +lstTestDriveRecords);
                /* If check TestDriveVehicle Account Id Not equal to TestDrive Dealer Id */
                
                system.debug('*** testDriveRecord.Dealer__c:'+testDriveRecord.Dealer__c);
                system.debug('*** TestDriveAccountId:'+TestDriveAccountId);

                if(testDriveRecord.Dealer__c != TestDriveAccountId){
                    /* add Error message in New entry page */
                    String errorMessage = Label.TDVErrorMsg;
                    testDriveRecord.Dealer__c.addError( errorMessage );
                    /* check TestDrive list of Records Not equal to empty */
                }else if(!lstTestDriveRecords.isEmpty()){
                    /* add Error message in New entry page */
                    String errorMessage = Label.ERRTDVMessage2;
                    testDriveRecord.DateBooking__c.addError( errorMessage );
                }
            }
        }
    }
    
    /**
    *   This Method was used to some validate the  Test Drive Object fields before updation
    *   @param  newTestDriveTrigger -   New Test Drive record passed  and validate Test Drive Object fields.
    *   @param  oldTestDriveTrigger -   old Test Drive record passed  and validate Test Drive Object fields.
    */
    public void validateEditTestDrive( List<TDV_TestDrive__c> newTestDriveTrigger, List<TDV_TestDrive__c> oldTestDriveTrigger  ){
        
        for( TDV_TestDrive__c newTestDrives : newTestDriveTrigger ){
            for( TDV_TestDrive__c oldTestDrives : oldTestDriveTrigger ){
                
                /* if check test drive Dealer field and Test Drive Vehicle field is not equal to null */
                if(newTestDrives.Dealer__c != null && newTestDrives.TestDriveVehicle__c != null){
                    /* TestDrive Object TestDriveVehicle look up field id pass and return TestDriveVehicle record id from TestDriveVehicle Object */
                    Id TestDriveAccountId = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicleAccount_UsingId(newTestDrives.TestDriveVehicle__c);
                    DateTime StartDateTime;
                    DateTime EndDateTime;
                    /* if testDrive DateofBooking field is not equal to null */
                    if(newTestDrives.DateBooking__c != null){
                        StartDateTime = newTestDrives.DateBooking__c;
                        EndDateTime = StartDateTime.addMinutes(30);
                    }
                    /* TestDrive Object and DateofBooking parameter pass and return TestDrive record records from TestDrive Object */
                    List<TDV_TestDrive__c> lstTestDriveRecords = VFC28_TestDriveDAO.getInstance().fetchTestDriveRecordsUsingTestDriveVehId(newTestDrives.TestDriveVehicle__c, StartDateTime);
                    system.debug('lstTestDriveRecords....' +lstTestDriveRecords);
                    
                    if(newTestDrives.DateBooking__c != oldTestDrives.DateBooking__c){
                        /* check TestDrive list of Records Not equal to empty */
                        if(!lstTestDriveRecords.isEmpty()){
                            /* add Error message in New entry page */
                            String errorMessage = Label.ERRTDVMessage2;
                            newTestDrives.DateBooking__c.addError( errorMessage );
                        }
                    }
                    /* If check TestDriveVehicle Account Id Not equal to TestDrive Dealer Id */
                    if(newTestDrives.Dealer__c != TestDriveAccountId){
                        /* add Error message in New entry page */
                        String errorMessage = Label.TDVErrorMsg;
                        newTestDrives.Dealer__c.addError( errorMessage );
                    }
                }
            }
        }
    }
    
    public TDV_TestDrive__c getTestDriveConfirmedOrOpenFromOpportunity(String opportunityId)
    {

        System.debug('*****VFC30_TestDriveBO.getTestDriveConfirmedOrOpenFromOpportunity');

        TDV_TestDrive__c sObjTestDrive = this.createOrRetrieveTestDriveFromOpportunity(opportunityId);
        
        if(sObjTestDrive.Status__c.equals('Scheduled'))
        {
            sObjTestDrive.Status__c = 'Confirmed';
            
            this.updateTestDrive(sObjTestDrive);
        }
        
        return sObjTestDrive;
    }
 
    
    /**
    * Cria ou obtém um test drive a partir do id de uma oportunidade.
    * @param opportunityId O id da oportunidade.
    * @return Um objeto TDV_TestDrive__c.
    * @author Felipe Jesus Silva.
    */
    public TDV_TestDrive__c createOrRetrieveTestDriveFromOpportunity(String opportunityId)
    {

        System.debug('*****VFC30_TestDriveBO.createOrRetrieveTestDriveFromOpportunity');

        List<TDV_TestDrive__c> lstSObjTestDrive = null;
        TDV_TestDrive__c sObjTestDrive = null;
        Set<String> setStatus = new Set<String>{'Confirmed', 'Open', 'Scheduled'};
        
        /*faz a busca para verificar se já existe algum test drive confirmado, aberto ou agendado (nessa busca no máximo UM registro será retornado)*/
        lstSObjTestDrive = VFC28_TestDriveDAO.getInstance().findByOpportunityId(opportunityId, setStatus);
        
        /*verifica se a busca não retornou nenhum registro*/
        if(lstSObjTestDrive.isEmpty())
        {   
            /*insere o objeto e pega o objeto de retorno que já vem com o campo name preenchido*/
            sObjTestDrive = this.createNewTestDrive(opportunityId);    
        }
        else
        {
            sObjTestDrive = lstSObjTestDrive.get(0);
        }
        
        return sObjTestDrive;
    }
    
    public TDV_TestDrive__c createNewTestDrive(String opportunityId)
    {
        TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
        sObjTestDrive.Opportunity__c = opportunityId;
        sObjTestDrive.Status__c = 'Open';
            
        /*insere o objeto e pega o objeto de retorno que já vem com o campo Name preenchido*/
        sObjTestDrive = this.createNewTestDrive(sObjTestDrive); 
                
        return sObjTestDrive;
    }
    
    public TDV_TestDrive__c createNewTestDrive(TDV_TestDrive__c sObjTestDrive)
    {
        TDV_TestDrive__c sObjTestDriveCreated = null;
        
        try
        {
            /*insere o objeto e pega o objeto de retorno que já vem com o campo name preenchido*/
            sObjTestDriveCreated = VFC28_TestDriveDAO.getInstance().insertData(sObjTestDrive); 
        }
        catch(DMLException ex)
        {
            throw new VFC58_CreateTestDriveException(ex, ex.getDMLMessage(0));
        }
        
        return sObjTestDriveCreated;
    }
    
    public TDV_TestDrive__c getTestDriveSchedule(String opportunityId)
    {
        List<TDV_TestDrive__c> lstSObjTestDrive = null;
        TDV_TestDrive__c sObjTestDrive = null;
        
        /*faz a busca para verificar se já existe algum test drive agendado (nessa busca no máximo UM registro será retornado)*/
        lstSObjTestDrive = VFC28_TestDriveDAO.getInstance().findByOpportunityId(opportunityId, new Set<String>{'Scheduled'});
        
        if(!lstSObjTestDrive.isEmpty())
        {
            sObjTestDrive = lstSObjTestDrive.get(0);
        }
        
        return sObjTestDrive;
    }
    
    public List<TDV_TestDrive__c> getTestsDriveScheduledOrConfirmed(String testDriveVehicleId, Date dateBooking)
    {

        System.debug('*****VFC30_TestDriveBO.getTestsDriveScheduledOrConfirmed');

        List<TDV_TestDrive__c> lstSObjTestDrive = null;
        Set<String> setStatus = new Set<String>{'Scheduled', 'Confirmed'};
        
        lstSObjTestDrive = VFC28_TestDriveDAO.getInstance().findByStatusVehicleDateBooking(setStatus, testDriveVehicleId, dateBooking);
        
        return lstSObjTestDrive;
    }
    
    
    public void updateTestDrive(TDV_TestDrive__c sObjTestDrive)
    {
        try
        {
            VFC28_TestDriveDAO.getInstance().updateData(sObjTestDrive);
        }
        catch(DMLException ex)
        {
            throw new VFC59_UpdateTestDriveException(ex, ex.getDMLMessage(0));
        }
    }
    
}