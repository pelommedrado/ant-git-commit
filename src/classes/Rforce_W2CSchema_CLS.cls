/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description This class is used in Custom Soap service as Schema.So this Class variables will not be in coding standards.
*/
global class Rforce_W2CSchema_CLS{

    global class Casedetails{
        webservice String priority,emailOptIn,description,licenseNumber,suppliedPhone,city,postalCode,address,lastName,firstName,comAgreement,
                   state,vin,dealer,language,caseFrom,detail,subject,cellPhone,company,title,caseIDBIR;           
        webservice List<CaseAttachments> attachmentDetails;
        webservice String country,suppliedEmail,type,origin,subType,subSource,caseBrand,dealerAfterSalesPostalCode;
        webservice Date firstRegistration,dateOfPurchase;
        webservice String customerExperience,reasonOfReturn,problemSolved,brandRecommendation,caseNumber,buyAgainIntention,renaultContactTitle,renaultContactFirstName,renaultContactLastName,renaultContactEmail;  
        webservice integer guarantyCode,kilometer;  
        
           
    }
    global class CaseAttachments{
        webservice Blob attachmentBody;
        webservice String attachmentName;
        webservice String attachmentContentType;
    } 
   
}