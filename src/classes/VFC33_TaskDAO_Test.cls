/**
	Class   -   VFC33_TaskDAO_Test
    Author  -   Suresh Babu
    Date    -   07/12/2012
    
    #01 <Suresh Babu> <07/12/2012>
        Created this class using test for VFC33_TaskDAO.
**/
@isTest
private class VFC33_TaskDAO_Test {

    static testMethod void myUnitTest()
    {
    	// Prepare test data
    	DateTime currentDateTime = Datetime.newInstance( system.now().year(), system.now().month(), system.now().day(), system.now().hour(), system.now().minute() - 1, 0);
    	
    	List<Account> lstNWSiteAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
		List<Opportunity> lstOpportunity = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToOpportunity(lstNWSiteAccounts);
		
    	Task newTask = new Task();
		newTask.WhatId = lstOpportunity[0].Id;
		newTask.Subject = Label.OPPCustomerService;
		newTask.OwnerId = Userinfo.getUserId();
		newTask.ActivityDate = system.today();
		newTask.Description = Label.OPPCustomerWaiting;
		newTask.Priority = 'High';
		newTask.ReminderDateTime = currentDateTime;
		
		Task newTask2 = new Task();
		
		// Start test
		Test.startTest();
		
		// Test01
		VFC33_TaskDAO.getInstance().insertData( newTask );
		
		// Test02
		Task test02a = VFC33_TaskDAO.getInstance().findById(newTask.Id);
		Task test02b = VFC33_TaskDAO.getInstance().findById(newTask2.Id);
		
		// Test03
		String status = 'In Progress';
		Set<String> setOpportunityId = new Set<String>();
		List<Task> test03 = VFC33_TaskDAO.getInstance().findByOpportunityId(setOpportunityId, status);

		// Stop test
		Test.stopTest();
    }
}