/******************************************************************
  Test class for web service "Myr_CleanAccount_WS"
 *******************************************************************
  2016 Dec : R17.01 v13.0 AmberFoxBaby, D. Veron (AtoS) : Creation
 *******************************************************************/
@isTest
private class Myr_CleanAccount_WS_Test {
	private static Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response res;
	private static Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question req;
	private static User techUser;

	@testsetup static void setCustomSettings() {
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser;
		Account a;

		Myr_Datasets_Test.prepareRequiredCustomSettings();

		listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser> ();
		listReqTechUser.add(new Myr_Datasets_Test.RequestedTechUser(Myr_Datasets_Test.getRoleName(), '', false));
		Myr_Datasets_Test.insertTechnicalUsers(listReqTechUser);
	}
	
	static void Common_Actions() {
		techUser = Myr_Datasets_Test.getTechnicalUser(Myr_Datasets_Test.getRoleName());
		req = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
	}
	
	//Check logs
	static void Check_Log(String ExceptionType, String Msg, String AccountId) {
		List<Logger__c> L_L;
		Logger__c Log;

		L_L = [select
		         Account__c
		       , Apex_Class__c
		       , Duration__c
		       , Function__c
		       , Message__c
		       , Project_Name__c
		       , Exception_Type__c
		       from Logger__c];

		System.assertEquals(1, L_L.size());
		Log = L_L[0];

		System.assertEquals('Myr_CleanAccount_WS', Log.Apex_Class__c);
		System.assertEquals(true, Log.Function__c.contains('CleanAccount'));
		System.assertEquals('MYR', Log.Project_Name__c);

		if (ExceptionType != null) {
			System.assertEquals(ExceptionType, Log.Exception_Type__c);
		}
		if (Msg != null) {
			System.assertEquals(true, Log.Message__c.contains(Msg));
		}
		if (AccountId != null) {
			System.assertEquals(AccountId, Log.Account__c);
		}
	}
	
	//Mandatory parameters missing
	Static testMethod void test010() {
		Common_Actions();

		req.accountId = '';
		req.accountBrand = '';
		
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS501', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(true, res.info.label.contains('accountId'));
		System.assertEquals(true, res.info.label.contains('accountBrand'));
		Check_Log('WS10MS501', 'An issue occurred: the following parameters are missing:', null);
	}
	
	//accountBrand missing 
	Static testMethod void test020() {
		Common_Actions();

		req.accountId = '123';
		req.accountBrand = '';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS501', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(false, res.info.label.contains('accountId'));
		System.assertEquals(true, res.info.label.contains('accountBrand'));
		Check_Log('WS10MS501', 'An issue occurred: the following parameters are missing:', null);
	}

	//accountSource missing 
	Static testMethod void test030() {
		Common_Actions();

		req.accountId = '';
		req.accountBrand = 'Renault';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS501', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(true, res.info.label.contains('accountId'));
		System.assertEquals(false, res.info.label.contains('accountBrand'));
		Check_Log('WS10MS501', 'An issue occurred: the following parameters are missing:', null);
	}

	//Too long value : accountBrand
	static testMethod void test040() {
		Common_Actions();

		req.accountId = '123456789012345';
		req.accountBrand = 'sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss';
		
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS502', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are too long:'));
		System.assertEquals(true, res.info.label.contains('accountBrand'));
		System.assertEquals(false, res.info.label.contains('accountId'));
		Check_Log('WS10MS502', 'An issue occurred: the following parameters are too long:', null);
	}

	//Too long value : accountId 
	static testMethod void test050() {
		Common_Actions();

		req.accountId = '12345678901234511111111111111';
		req.accountBrand = 'Renault';
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();
		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS502', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are too long:'));
		System.assertEquals(true, res.info.label.contains('accountId'));
		System.assertEquals(false, res.info.label.contains('accountBrand'));
		Check_Log('WS10MS502', 'An issue occurred: the following parameters are too long', null);
	}

	//Too long value : accountId 
	static testMethod void test060() {
		Common_Actions();

		req.accountId = '12345678901234511111111111111';
		req.accountBrand = 'sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss';
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();
		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS502', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: the following parameters are too long:'));
		System.assertEquals(true, res.info.label.contains('accountId'));
		System.assertEquals(true, res.info.label.contains('accountBrand'));
		Check_Log('WS10MS502', 'An issue occurred: the following parameters are too long', null);
	}

	//Wrong brand value
	static testMethod void test070() {
		Common_Actions();

		req.accountId = '123456789012345';
		req.accountBrand = 'TDET';
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();
		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS503', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: wrong accountBrand :'));
		System.assertEquals(true, res.info.label.contains('TDET'));
		Check_Log('WS10MS503', 'An issue occurred: wrong accountBrand :', null);
	}

	//accountId not found
	static testMethod void test080() {
		Common_Actions();

		req.accountId = '123456789012345';
		req.accountBrand = 'Renault';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS504', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: accountId does not exist in the sfdc database'));
		System.assertEquals(true, res.info.label.contains(req.accountId));
		Check_Log('WS10MS504', 'An issue occurred: accountId does not exist in the sfdc database', null);
	}

	//Account not existing
	static testMethod void test090() {
		Common_Actions();

		req.accountId = '123456789012345';
		req.accountBrand = 'Renault';
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();
		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS504', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: accountId does not exist in the sfdc database'));
		Check_Log('WS10MS504', 'An issue occurred: accountId does not exist in the sfdc database', null);
	}
	
	//Exception thrown during update, delete ok
	Static testMethod void test100() {
		Account a;
		List<Account> listAcc;
		Common_Actions();

		a = new Account();
		a.Firstname = 'Test_Exception';
		a.Lastname = 'Lastname';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;

		req.accountId = a.Id;
		req.accountBrand = 'Renault';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS000', res.info.code);
		System.assertEquals(true, res.info.label.contains('Cleaning ok'));
		Check_Log('WS10MS000', 'Cleaning ok', null);
	}
	
	//Exceptions thrown during update and delete
	Static testMethod void test110() {
		Account a;
		List<Account> listAcc;
		Common_Actions();

		a = new Account();
		a.Firstname = 'Test_Exception';
		a.Lastname = 'Test_Exception';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;

		req.accountId = a.Id;
		req.accountBrand = 'Renault';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS999', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: unexpected exception :'));

		listAcc = [select MYR_Status__c, MYD_Status__c, MyRenaultID__c, MyDaciaID__c from Account];
		System.assertEquals(1, listAcc.size());
		System.assertEquals(System.Label.Myr_Status_Created, listAcc[0].MYR_Status__c);
		System.assertEquals('renault@test.com', listAcc[0].MyRenaultID__c);
		System.assertEquals(System.Label.Myr_Status_Created, listAcc[0].MYD_Status__c);
		System.assertEquals('dacia@test.com', listAcc[0].MyDaciaID__c);

		Check_Log('WS10MS999', 'An issue occurred: unexpected exception :', null);
	}

	//Update ok - Renault
	Static testMethod void test120() {
		Account a;

		Common_Actions();

		a = new Account();
		a.FirstName = 'Firstname';
		a.LastName = 'MYR_HELIOS_';
		a.PersEmailAddress__c = 'renaultd@recordtype.com';
		a.MyRenaultID__c = 'renaultd@recordtype.com';
		a.MyDaciaID__c = 'renaultd@recordtype.com';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType();
		a.HomePhone__c = '3456543456';
		insert a;

		req.accountId = a.Id;
		req.accountBrand = 'Renault';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS000', res.info.code);
		System.assertEquals(true, res.info.label.contains('Cleaning ok'));

		a = [select MYR_Status__c, MyRenaultID__c, MYD_Status__c, MyDaciaID__c from Account];
		System.assertEquals(a.MYR_Status__c, null);
		System.assertEquals(a.MyRenaultID__c, null);
		System.assertEquals(a.MYD_Status__c, System.Label.Myr_Status_Created);
		System.assertEquals(a.MyDaciaID__c, 'renaultd@recordtype.com');
		Check_Log('WS10MS000', 'Cleaning ok', a.Id);
	}

	//Update ok - Dacia
	Static testMethod void test130() {
		Account a;
		Common_Actions();

		a = new Account();
		a.FirstName = 'Firstname';
		a.LastName = 'MYR_HELIOS_';
		a.PersEmailAddress__c = 'renaultd@recordtype.com';
		a.MyRenaultID__c = 'renaultd@recordtype.com';
		a.MyDaciaID__c = 'renaultd@recordtype.com';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType();
		a.HomePhone__c = '3456543456';

		insert a;

		req.accountId = a.Id;
		req.accountBrand = 'Dacia';

		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS000', res.info.code);
		System.assertEquals(true, res.info.label.contains('Cleaning ok'));

		a = [select MYR_Status__c, MyRenaultID__c, MYD_Status__c, MyDaciaID__c from Account];
		System.assertEquals(a.MYR_Status__c, system.Label.Myr_Status_Created);
		System.assertEquals(a.MyRenaultID__c, 'renaultd@recordtype.com');
		System.assertEquals(a.MYD_Status__c, null);
		System.assertEquals(a.MyDaciaID__c, null);
		Check_Log('WS10MS000', 'Cleaning ok', a.Id);
	}
	
	//Update ko
	Static testMethod void test140() {
		Account a;
		List<Account> L_A;
		Common_Actions();

		a = new Account();
		a.FirstName = 'Firstname';
		a.LastName = 'Test_Exception';
		a.MyRenaultID__c = 'renaultd@recordtype.com';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType();
		insert a;

		req.accountId = a.Id;
		req.accountBrand = 'Renault';
		
		Test.startTest();
		System.runAs(techUser) {
			res = Myr_CleanAccount_WS.CleanAccount(req);
		}
		Test.stopTest();

		System.assertNotEquals(null, res);
		System.assertEquals('WS10MS999', res.info.code);
		System.assertEquals(true, res.info.label.contains('An issue occurred: unexpected exception :'));
		System.assertEquals(true, res.info.label.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
		
		L_A = [select MYR_Status__c, MyRenaultID__c, MYD_Status__c, MyDaciaID__c from Account];
		System.assertEquals(1, L_A.size());
		
		a = L_A[0];
		
		System.assertEquals(a.MyRenaultID__c, 'renaultd@recordtype.com');
		System.assertEquals(a.MYR_Status__c, system.Label.Myr_Status_Created);
		Check_Log('WS10MS999', 'An issue occurred: unexpected exception :', null);
	}
	
	//Check_Inputs
	Static testMethod void test150_Check_Inputs(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=null; 
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Check_Inputs();
		}
		
		System.assertEquals('WS10MS501', R_T.info.code);
		System.assertEquals(true, R_T.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(true, R_T.info.label.contains('accountId'));

		q.accountBrand='';
		q.accountId='123456789012345678';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);

		System.runAs(techUser) {
			R_T=H_C.Check_Inputs();
		}
		System.assertEquals('WS10MS501', R_T.info.code);
		System.assertEquals(true, R_T.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(true, R_T.info.label.contains('accountBrand'));

		q.accountId='';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		System.runAs(techUser) { 
			R_T=H_C.Check_Inputs();
		}
		Test.stopTest();
		System.assertEquals('WS10MS501', R_T.info.code);
		System.assertEquals(true, R_T.info.label.contains('An issue occurred: the following parameters are missing:'));
		System.assertEquals(true, R_T.info.label.contains('accountId'));
		System.assertEquals(true, R_T.info.label.contains('accountBrand'));
	}
	
	//Missing_Mandatory_Parameter
	Static testMethod void test160_Missing_Mandatory_Parameter(){
		Common_Actions();
		String R_T;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=null;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Missing_Mandatory_Parameter();
		}
		System.assertEquals(' accountId', R_T);

		q.accountBrand=null;
		q.accountId='123456789012345678';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		System.runAs(techUser) {
			R_T=H_C.Missing_Mandatory_Parameter();
		}
		System.assertEquals(' accountBrand', R_T);

		q.accountBrand=null;
		q.accountId=null;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		System.runAs(techUser) {
			R_T=H_C.Missing_Mandatory_Parameter();
		}
		Test.stopTest();
		System.assertEquals(true, R_T.contains('accountId'));
		System.assertEquals(true, R_T.contains('accountBrand'));
	}
	
	//Too_Long_Param_Value
	Static testMethod void test170_Too_Long_Param_Value(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		String R_T;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renaultsssssssssssssssssssszzzzzzzzzzzzzzzzzzzzzzzzzzzzttttttttttttttttt';
		q.accountId='123456789012345678';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Too_Long_Param_Value();
		}
		System.assertEquals(' accountBrand', R_T);

		q.accountBrand='Renault';
		q.accountId='123456789012345678123456123456789012345678123456';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		System.runAs(techUser) {
			R_T=H_C.Too_Long_Param_Value();
		}
		System.assertEquals(' accountId', R_T);

		q.accountBrand='Renaultsssssssssssssssssssszzzzzzzzzzzzzzzzzzzzzzzzzzzzttttttttttttttttt';
		q.accountId='123456789012345678123456';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		System.runAs(techUser) {
			R_T=H_C.Too_Long_Param_Value();
		}
		Test.stopTest();
		System.assertEquals(true, R_T.contains('accountId'));
		System.assertEquals(true, R_T.contains('accountBrand'));
	}
	

	//Database_Query : account found
	Static testMethod void test180_Database_Query(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		List<Account> listAcc;
		Account a;

		a = new Account();
		a.Firstname = 'Firstname';
		a.Lastname = 'Lastname';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=a.Id;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Database_Query();
		}
		Test.stopTest();
		System.assertEquals(null, R_T);
		System.assertEquals(a, H_C.Acc);
	}

	//Database_Query : account not found
	Static testMethod void test190_Database_Query(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId='123456789012345678';
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Database_Query();
		}
		Test.stopTest();
		System.assertEquals('WS10MS504', R_T.info.code);
		System.assertEquals(true, R_T.info.label.contains('An issue occurred: accountId does not exist in the sfdc database'));
		System.assertEquals(true, R_T.info.label.contains(q.accountId));
		System.assertEquals(null, H_C.Acc);
	}
	
	//Clean_Account
	Static testMethod void test200_Clean_Account(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		List<Account> listAcc;
		Account a;

		a = new Account();
		a.Firstname = 'Firstname';
		a.Lastname = 'Lastname';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=a.Id;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Database_Query();
			R_T=H_C.Clean_Account();
		}
		Test.stopTest();
		a=[select Firstname, Lastname, MYR_Status__c, MyRenaultID__c, MYD_Status__c, MyDaciaID__c from Account];
		System.assertEquals(a.Firstname, 'Firstname');
		System.assertEquals(a.Lastname, 'Lastname');
		System.assertEquals(a.MYR_Status__c, null);
		System.assertEquals(a.MyRenaultID__c, null);
		System.assertEquals(a.MYD_Status__c, System.Label.Myr_Status_Created);
		System.assertEquals(a.MyDaciaID__c, 'dacia@test.com');
	}

	//Delete the account
	Static testMethod void test210_Delete_Account(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		List<Account> listAcc;
		Account a;

		a = new Account();
		a.Firstname = 'Test_Exception';
		a.Lastname = 'Lastname';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=a.Id;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			R_T=H_C.Database_Query();
			try {
				throw new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Exception('Exception during delete');
			} catch (Exception e) {
				H_C.Except=e;
			}
			R_T=H_C.Delete_Account();
		}
		Test.stopTest();
		listAcc=[select Id from Account];
		System.assertEquals(listAcc.size(),0);
	}
	

	//Build a message
	Static testMethod void test220_Build_Message(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		List<Logger__c> listLog;
		Account a;

		a = new Account();
		a.Firstname = 'Test_Exception';
		a.Lastname = 'Test_Exception';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=a.Id;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			try {
				throw new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Exception('Exception during delete');
			} catch (Exception e) {
				H_C.Except=e;
			}
			R_T=H_C.Build_Resp('WS10MS000','Reste du message');
		}
		Test.stopTest();
		listLog=[select Id from Logger__c];
		System.assertEquals(listLog.size(), 1);
		System.assertEquals(true, R_T.info.label.contains('Reste du message'));
		System.assertEquals(true, R_T.info.label.contains(Label.Myr_CleanAccount_WS10MS000_Msg));
	}

	//Send a log
	Static testMethod void test230_Send_Log(){
		Common_Actions();
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question q;
		Myr_CleanAccount_WS.Handler_CleanAccount H_C;
		Myr_CleanAccount_WS.Myr_CleanAccount_WS_Response R_T;
		List<Logger__c> listLog;
		Account a;
		Long BeginTime=DateTime.now().getTime();

		a = new Account();
		a.Firstname = 'Test_Exception';
		a.Lastname = 'Test_Exception';
		a.MYR_Status__c = System.Label.Myr_Status_Created;
		a.MyRenaultID__c = 'renault@test.com';
		a.MYD_Status__c = System.Label.Myr_Status_Created;
		a.MyDaciaID__c = 'dacia@test.com';
		insert a;
		
		q = new Myr_CleanAccount_WS.Myr_CleanAccount_WS_Question();
		q.accountBrand='Renault';
		q.accountId=a.Id;
		H_C = new Myr_CleanAccount_WS.Handler_CleanAccount(q);
		
		Test.startTest();
		System.runAs(techUser) {
			Myr_CleanAccount_WS.Send_Log('Log_Run_Id', 'Code_Err', 'Msg_Err', 'Lvl_Err', a.Id, BeginTime, BeginTime+10);
		}
		Test.stopTest();
		listLog=[select Account__c, Apex_Class__c , Duration__c , Function__c , Message__c , Project_Name__c , Exception_Type__c from Logger__c];
		System.assertEquals(1, listLog.size());
		System.assertEquals(a.Id, listLog[0].Account__c);
		System.assertEquals('Myr_CleanAccount_WS', listLog[0].Apex_Class__c);
		System.assertEquals(10, listLog[0].Duration__c);
		System.assertEquals('CleanAccount', listLog[0].Function__c);
		System.assertEquals('Msg_Err', listLog[0].Message__c);
		System.assertEquals('MYR', listLog[0].Project_Name__c);
		System.assertEquals('Code_Err', listLog[0].Exception_Type__c);
	}
}