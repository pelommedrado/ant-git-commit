/**
* Classe controladora da página de confirmação de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC78_TestDriveConfirmationController 
{
	private String opportunityId;
	public VFC77_TestDriveConfirmationVO testDriveVO {get;set;}
	
	public VFC78_TestDriveConfirmationController(ApexPages.StandardController controller) 
	{
		this.opportunityId = controller.getId();	
	}
	
	public PageReference initialize()
	{
		this.testDriveVO = VFC79_TestDriveConfirmBusinessDelegate.getInstance().getTestDriveByOpportunity(this.opportunityId);
		
		this.initializeTestDrive();
					
		return null;
	}
	
	private void initializeTestDrive()
	{
		this.testDriveVO.lstAgendaVehicle = new List<List<VFC64_ScheduleTestDriveVO>>();
		
		/*seta as informações da confirmação*/
		this.testDriveVO.dateBookingFmt = this.testDriveVO.dateBooking.format() + 'hs';		
		this.testDriveVO.vehicleScheduled = this.testDriveVO.model + ' ' + this.testDriveVO.plaque;
		
		this.testDriveVO.lstSelOptionInterestVehicle = VFC49_PickListUtil.buildPickList(Account.VehicleInterest_Br__c);
		
		/*constroi o picklist de veículos disponíveis*/
		this.buildPicklistVehiclesAvailable();
		
		/*constroi a agenda do veículo agendado*/
		this.buildAgendaVehicle();
	}
	
	private void buildPicklistVehiclesAvailable()
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		
		if(this.testDriveVO.lstSObjTestDriveVehicleAvailable != null)
		{
			for(TDV_TestDriveVehicle__c sObjTestDriveVehicle : this.testDriveVO.lstSObjTestDriveVehicleAvailable)
			{
				if(String.isNotEmpty(sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c))
				{
					lstSelOption.add(new SelectOption(sObjTestDriveVehicle.Id, sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c));
				}			
			}
			
			/*essa lista não é mais necessária*/
			this.testDriveVO.lstSObjTestDriveVehicleAvailable = null;
		}
		
		this.testDriveVO.lstSelOptionVehiclesAvailable = lstSelOption;					
	}
	
	private void buildAgendaVehicle()
	{
		VFC64_ScheduleTestDriveVO scheduleVO = null; 
		String hourAux = null;
		String minuteAux = null;
		TDV_TestDrive__c sObjTestDriveAux = null;
		Map<String, TDV_TestDrive__c> mapTestDrive = null;
		Date currentDate = null;
		DateTime startDate = null;
		DateTime endDate = null;
		List<VFC64_ScheduleTestDriveVO> lstAux = null;
		
		this.testDriveVO.lstAgendaVehicle.clear();
		
		if(this.testDriveVO.lstSObjTestDriveAgenda != null)
		{
			mapTestDrive = new Map<String, TDV_TestDrive__c>();
			currentDate = Date.today();
			startDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 8, 0, 0);		
			endDate = Datetime.newInstance(currentDate.year(), currentDate.month(), currentDate.day(), 20, 0, 0);
			lstAux = new List<VFC64_ScheduleTestDriveVO>();
			
			for(TDV_TestDrive__c sObjTestDrive : this.testDriveVO.lstSObjTestDriveAgenda)
			{
				hourAux = sObjTestDrive.DateBooking__c.hour().format();
				minuteAux = + sObjTestDrive.DateBooking__c.minute().format();
			
				mapTestDrive.put(hourAux + minuteAux, sObjTestDrive);
			}
			
			while(startDate <= endDate)
			{
				scheduleVO = new VFC64_ScheduleTestDriveVO();
						
				hourAux = startDate.hour() < 10 ? ('0' + startDate.hour()) : (startDate.hour().format());
			
				minuteAux = startDate.minute() < 10 ? ('0' + startDate.minute()) : (startDate.minute().format());
			
				scheduleVO.hour = hourAux + ':' + minuteAux;
					
				hourAux = startDate.hour().format();
				minuteAux = startDate.minute().format();
			
				/*obtém do map um test drive com esse horário*/
				sObjTestDriveAux = mapTestDrive.get(hourAux + minuteAux);
			
				/*verifica se foi retornado null, o que indica que esse horário está disponível*/
				if(sObjTestDriveAux == null)
				{
					scheduleVO.schedulingStatus = 'available';
				}
				else
				{
					if(sObjTestDriveAux.Id == this.testDriveVO.id)
					{					
						scheduleVO.schedulingStatus = 'confirmed';
					}
					else
					{
						scheduleVO.schedulingStatus = 'scheduled';
					}
				}
					
				lstAux.add(scheduleVO);
			
				if(lstAux.size() == 5)
				{		
					this.testDriveVO.lstAgendaVehicle.add(lstAux);
				
					lstAux = new List<VFC64_ScheduleTestDriveVO>();
				}
							
				startDate = startDate.addMinutes(30);	
			}
		
			this.testDriveVO.dateBookingNavigationFmt = this.testDriveVO.dateBookingNavigation.format();
			
			/*essa lista não é mais necessária*/
			this.testDriveVO.lstSObjTestDriveAgenda = null;
		}				
	}
	
	/**
	* Trata o evento onchange do campo modelo desejado (veículo de interesse).
	*/
	public void processVehicleInterestChange()
	{
		VFC77_TestDriveConfirmationVO testDriveVOResponse = VFC79_TestDriveConfirmBusinessDelegate.getInstance().getVehiclesAvailable(this.testDriveVO);
		
		/*armazena a lista de veículos disponíveis no VO principal*/
		this.testDriveVO.lstSObjTestDriveVehicleAvailable = testDriveVOResponse.lstSObjTestDriveVehicleAvailable;
			
		/*constroi o picklist de veículos disponíveis*/
		this.buildPicklistVehiclesAvailable();
		
		/*seta os detalhes do primeiro veículo disponível*/
		this.testDriveVO.model = testDriveVOResponse.model;
		this.testDriveVO.version = testDriveVOResponse.version;
		this.testDriveVO.color = testDriveVOResponse.color;
		this.testDriveVO.plaque = testDriveVOResponse.plaque;
		this.testDriveVO.dateBookingNavigation = testDriveVOResponse.dateBookingNavigation;
		this.testDriveVO.selectedTime = '';
		
		/*armazena a agenda do primeiro veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda do primeiro veículo disponível*/
		this.buildAgendaVehicle();					
	}
	
	/**
	* Trata o evento onchange do campo veículos disponíveis.
	*/
	public void processVehicleAvailableChange()
	{
		VFC77_TestDriveConfirmationVO testDriveVOResponse = null;
		
		/*seta a data atual no atributo dateBooking para obter a agenda do dia inicialmente*/
		//this.testDriveVO.dateBookingNavigation = Date.today();
			
		/*obtém os detalhes do veículo*/
		testDriveVOResponse = VFC79_TestDriveConfirmBusinessDelegate.getInstance().getVehicleDetails(this.testDriveVO);
		
		/*seta os detalhes do veículo selecionado*/	
		this.testDriveVO.model = testDriveVOResponse.model;
		this.testDriveVO.version = testDriveVOResponse.version;
		this.testDriveVO.color = testDriveVOResponse.color;
		this.testDriveVO.plaque = testDriveVOResponse.plaque;
		this.testDriveVO.selectedTime = '';
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda do veículo selecionado*/	
		this.buildAgendaVehicle();	
	}
	
	public void getAgendaNextDay()
	{
		VFC77_TestDriveConfirmationVO testDriveVOResponse = null;
		
		/*incrementa um dia no datebooking para obter a agenda do dia seguinte*/
		this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation + 1;
		
		/*obtém a agenda do dia seguinte*/
		testDriveVOResponse = VFC79_TestDriveConfirmBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda*/
		this.buildAgendaVehicle();
	}
	
	public void getAgendaPreviousDay()
	{
		VFC77_TestDriveConfirmationVO testDriveVOResponse = null;
		
		/*decrementa um dia no datebooking para obter a agenda de um dia atrás*/
		this.testDriveVO.dateBookingNavigation = this.testDriveVO.dateBookingNavigation - 1;
		
		/*obtém a agenda de um dia atrás*/
		testDriveVOResponse = VFC79_TestDriveConfirmBusinessDelegate.getInstance().getAgendaVehicle(this.testDriveVO);
		
		/*seta a agenda do veículo no VO principal*/
		this.testDriveVO.lstSObjTestDriveAgenda = testDriveVOResponse.lstSObjTestDriveAgenda;
		
		/*constroi a agenda*/
		this.buildAgendaVehicle();		
	}
	
	public void confirmSchedulingTestDrive()
	{
		String currentStatus = null;	
		DateTime currentDateBooking = null;	
		String[] arrSelectedTime = null;
		Integer hour = null;
		Integer minutes = null;
		String strMessage = null;
		ApexPages.Message message = null;
		
		/*armazena o status atual do test drive*/
		currentStatus = this.testDriveVO.status;
		
		/*armazena a data de agendamento atual*/
		currentDateBooking = this.testDriveVO.dateBooking;
		
		/*altera o status do test drive para confirmado*/
		this.testDriveVO.status = 'Confirmed';
		
		/*faz o split do horário no formato HH:mm para a hora e os minutos separados*/			
		arrSelectedTime = this.testDriveVO.selectedTime.split(':');
			
		/*converte a hora e os minutos para inteiro*/
		hour = Integer.valueOf(arrSelectedTime[0]);
		minutes = Integer.valueOf(arrSelectedTime[1]);
			
		/*cria o datebooking completo*/
		this.testDriveVO.dateBooking = DateTime.newInstance(this.testDriveVO.dateBookingNavigation.year(), this.testDriveVO.dateBookingNavigation.month(), 
		                                                   this.testDriveVO.dateBookingNavigation.day(), hour,
	                                                       minutes, 0); 
		                                                    		                        	                                               	                                                   		                                                 	
		try
		{
			VFC79_TestDriveConfirmBusinessDelegate.getInstance().updateTestDrive(this.testDriveVO);
			
			this.testDriveVO.dateBookingFmt = this.testDriveVO.dateBooking.format() + 'hs';
			
			this.testDriveVO.vehicleScheduled = this.testDriveVO.model + ' ' + this.testDriveVO.plaque;
			
			/*marca o horário na lista como confirmado*/
			this.markAsConfirmed();
		}
		catch(VFC59_UpdateTestDriveException ex)
		{		
			/*volta para o status que estava*/
			this.testDriveVO.status = currentStatus;
			
			/*volta para a data de agendamento que estava*/
			this.testDriveVO.dateBooking = currentDateBooking;
			
			strMessage = 'Não foi possível confirmar o agendamento do Test Drive. Motivo: \n' + ex.getMessage();
					
			message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
			
			ApexPages.addMessage(message);
		}			
	}
	
	private void markAsConfirmed()
	{
		for(List<VFC64_ScheduleTestDriveVO> lstAux : this.testDriveVO.lstAgendaVehicle)
		{
			for(VFC64_ScheduleTestDriveVO scheduleVO : lstAux)
			{				
				if(scheduleVO.hour.equals(testDriveVO.selectedTime))
				{
					scheduleVO.schedulingStatus = 'confirmed';
				}		
				
				else if(scheduleVO.schedulingStatus.equals('confirmed'))
				{
					scheduleVO.schedulingStatus = 'available';
				}					
			}
		}
	}
	
	/**
	* Trata a ação do botão confirmar agendamento.
	*/
	public PageReference confirmScheduling()
	{
		PageReference pageRef = null;
		
		if(this.testDriveVO.status.equals('Confirmed'))
		{
			pageRef = new PageReference('/' + this.opportunityId);
		}
		
		return pageRef;
	}
	
	public PageReference updateTestDriveForConfirmed()
	{
		PageReference pageRef = null;
		String strMessage = null;
		ApexPages.Message message = null;
		String currentStatus = this.testDriveVO.status;
		
		/*seta o status para não realizado*/
		this.testDriveVO.status = 'Confirmed';
		
		try
		{
			VFC79_TestDriveConfirmBusinessDelegate.getInstance().updateTestDrive(this.testDriveVO);
			
			pageRef = new PageReference('/' + this.opportunityId);
		}
		catch(VFC59_UpdateTestDriveException ex)
		{
			/*volta para o status que estava*/
			this.testDriveVO.status = currentStatus;
			
			strMessage = 'Não foi possível confirmar o agendamento. Motivo: \n' + ex.getMessage();
						
			message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);
			
			ApexPages.addMessage(message);
		}
	
		return pageRef;
	}	
	
	/**
	* Trata a ação do botão cancelar agendamento (botão principal).
	*/
	public void openPopUpCancelScheduling()
	{
		this.testDriveVO.reasonCancellation = '';
		
		/*verifica se o picklist ainda não foi construído*/
		if(this.testDriveVO.lstSelOptionReasonCancellation == null)
		{
			/*cria o picklist que exibe as opções de cancelamento do test drive*/
			this.testDriveVO.lstSelOptionReasonCancellation = VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.ReasonCancellation__c, '');
		}
	}	
	
	/**
	* Trata a ação do botão cancelar agendamento (pop-up).
	*/
	public PageReference cancelScheduling()
	{
		PageReference pageRef = null;
		String strMessage = null;
		ApexPages.Message message = null;
		String currentStatus = this.testDriveVO.status;
		
		/*seta o status para não realizado*/
		this.testDriveVO.status = 'Not Performed';
		
		try
		{
			VFC79_TestDriveConfirmBusinessDelegate.getInstance().updateTestDrive(this.testDriveVO);
			
			pageRef = new PageReference('/' + this.opportunityId);
		}
		catch(VFC59_UpdateTestDriveException ex)
		{
			/*volta para o status que estava*/
			this.testDriveVO.status = currentStatus;
			
			strMessage = 'Não foi possível cancelar o agendamento. Motivo: \n' + ex.getMessage();
						
			message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);
			
			ApexPages.addMessage(message);
		}
	
		return pageRef;
	}
		
	
}