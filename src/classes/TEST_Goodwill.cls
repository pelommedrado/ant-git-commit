@isTest
private class TEST_Goodwill {

   static testMethod void testAmount()
   {
   
       User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',BypassVR__c=true,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
        System.runAs(usr)
        {
            Test.startTest();
             
       //prepare custom settings
       Car_Mapping__c carmap = new Car_Mapping__c();
       carmap.Name = 'High Priced';
       carmap.Amount__c = 500;    
       insert carmap;
   
   
      Case testCase = new Case(
           Origin='INTERNAL - URGENT CLIENT', Status='New', Priority='Urgent', 
           Type='Other', From__c='Customer', Description='one case');
       insert testCase ;
       system.debug('TEST 1');
       
       Goodwill__c goodwill = new Goodwill__c();
       goodwill.Case__c = testCase.Id;
       Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
       
       System.debug('RecordId>>>>>>>>'+RecordId);
       
       goodwill.RecordTypeId=RecordId;
       insert goodwill;
       System.assertEquals(goodwill.Car_Rental_Amount__c, null);
       
       system.debug('TEST 2');
       goodwill.Amount__c = 1000;
       update goodwill;
       System.assertEquals(goodwill.Car_Rental_Amount__c, null);
       
       system.debug('TEST 3');
       goodwill.Number_of_Days__c = 5;
       goodwill.BackUp_Car__c = 'High Priced';
       goodwill.GoodwillStatus__c = 'Refused';
       update goodwill;
       System.assertEquals(goodwill.Car_Rental_Amount__c, null);
       
       system.debug('TEST 4');
       goodwill.GoodwillStatus__c = 'Approved';
       update goodwill;
       Map<String, Car_Mapping__c> prices = Car_Mapping__c.getAll();
       if (prices.containsKey('High Priced')) {
            Double calcul = [SELECT Car_Rental_Amount__c FROM Goodwill__c WHERE ID = :goodwill.Id][0].Car_Rental_Amount__c;
            Double price = prices.get('High Priced').Amount__c;
            system.debug('Price' + price);
            Double value = 5 * price;
            system.debug('COMPARE ' + calcul + ' AND ' + value);
            //System.assertEquals(calcul, value);
       }
       
       system.debug('TEST 5');
       goodwill.Amount__c = 1000;
       update goodwill;
       system.debug('COMPARE ' + goodwill.Car_Rental_Amount__c);
       if (prices.containsKey('High Priced')) {
            Double price = prices.get('High Priced').Amount__c;
            Double calcul = [SELECT GoodwillAmount__c FROM Goodwill__c WHERE ID = :goodwill.Id][0].GoodwillAmount__c;
            Double value = 1000 + 5 * price;
            system.debug('COMPARE ' + goodwill.Car_Rental_Amount__c+ ' AND ' + value);
            //System.assertEquals(calcul, value);
       }
       
       system.debug('TEST 6 - VOLUME - DML TESTS');
       List<Goodwill__c> listGoodwills = new List<Goodwill__c>();
       for (Integer i = 0; i < 200; ++i) {
           Goodwill__c item = new Goodwill__c ();
           item.Amount__c = 1000;
          
           item.RecordTypeId=RecordId;
           item.Number_of_Days__c = 5;
           item.BackUp_Car__c = 'High Priced';
           item.GoodwillStatus__c = 'Approved';
           item.Case__c = testCase.Id;
           listGoodwills.add(item);
       }
       insert listGoodwills;
       
       Test.stopTest();
       }
   }
}