@isTest
private class DashboardLeadControllerTest {

	private static Account accDealer, person1, person2, person3, person4;
    private static Contact manager1Contact, manager2Contact, seller1Contact, seller2Contact;
    private static User manager1, manager2, seller1, seller2;
    private static Lead lead1;
    private static Opportunity oppt1, oppt2, opp3, oppt4;
    private static Quote qt1;

    static {
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        system.runAs( localUser ){
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;

            // CRIAÇÃO DE CONTA DE CONCESSIONÁRIA
            accDealer = MyOwnCreation.getInstance().criaAccountDealer();
            accDealer.ChaseTaskTime__c = 16;

            // CRIAÇÃO DE CONTAS DE CLIENTES
            person1 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste1',
                PersEmailAddress__c = 'test1@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );

            person2 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste2',
                PersEmailAddress__c = 'test2@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );

            person3 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste3',
                PersEmailAddress__c = 'test3@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );

            person4 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste4',
                PersEmailAddress__c = 'test4@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );

            Database.insert(new List<Account>{accDealer, person1, person2, person3, person4});



            // CRIAÇÃO DOS CONTATOS DOS USUÁRIOS MANAGER
            manager1Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'manager1@org1.com',
                FirstName = 'User',
                LastName = 'Manager1',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );

            manager2Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'manager2@org1.com',
                FirstName = 'User',
                LastName = 'Manager2',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );

            // CRIAÇÃO DOS CONTATOS DOS USUÁRIOS SELLER
            seller1Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'seller1@org1.com',
                FirstName = 'User',
                LastName = 'Seller1',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );

            seller2Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'seller2@org1.com',
                FirstName = 'User',
                LastName = 'Seller2',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );

            Database.insert(new List<Contact>{manager1Contact, manager2Contact, seller1Contact, seller2Contact});


            // CRIAÇÃO DOS USUÁRIOS MANAGER
            manager1 = new User(
                ContactId = manager1Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName = 'User',
                LastName = 'Manager1',
                Username = 'manager1@org1.com.teste',
                Email = 'manager1@org1.com',
                Alias = 'ma1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );

            manager2 = new User(
                ContactId = manager2Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName = 'User',
                LastName = 'Manager2',
                Username = 'manager2@org1.com.teste',
                Email = 'manager2@org1.com',
                Alias = 'ma2',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );

            // CRIAÇÃO DOS USUÁRIOS SELLER
            seller1 = new User(
                ContactId = seller1Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente'),
                FirstName = 'User',
                LastName = 'Seller1',
                Username = 'seller1@org1.com.teste',
                Email = 'seller1@org1.com',
                Alias = 'se1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );

            seller2 = new User(
                ContactId = seller2Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente'),
                FirstName = 'User',
                LastName = 'Seller2',
                Username = 'seller2@org1.com.teste',
                Email = 'seller2@org1.com',
                Alias = 'se2',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );

            Database.insert(new List<User>{manager1, manager2, seller1, seller2});


            MLC_Molicar__c mol = new MLC_Molicar__c(
                Brand__c = 'RENAULT',
                Model__c = 'DUSTER',
                Configuration__c = 'SUV'
            );

            Database.insert(mol);


            lead1 = new Lead(
                OwnerId = manager1.Id,
                FirstName = 'Test',
                LastName = 'Lead',
                CPF_CNPJ__c = Utils.generateCpf(),
                Email = 'testlead@email.com',
                HomePhone__c = '11'+Utils.randomNumberAsString(8),
                MobilePhone = '119'+Utils.randomNumberAsString(8),
                LeadSource = 'Internet',
                SubSource__c = 'Marketing',
                VehicleOfInterest__c = 'Sandero',
                IsVehicleOwner__c = 'Y',
                CRV_CurrentVehicle__c = mol.Id,
                Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found'
            );

            Database.insert(lead1);


            oppt1 = new Opportunity(
                OwnerId = manager1.Id,
                CloseDate = date.today().addDays(5),
                StageName = 'Identified',
                Amount = 10000,
                Expiration_Time__c = datetime.newInstance(date.today().addDays(5), time.newInstance(17, 0, 0, 0)),
                Name = 'Oportunidade (Sir Teste1)',
                AccountId = person1.Id,
                Dealer__c = accDealer.Id,
                OpportunitySource__c = 'TEST DRIVE',
                OpportunitySubSource__c = 'INTERNET',
                SourceMedia__c = 'Site Concessionária'
            );

            oppt2 = new Opportunity(
                OwnerId = manager2.Id,
                CloseDate = date.today().addDays(7),
                StageName = 'Identified',
                Amount = 10000,
                Expiration_Time__c = datetime.newInstance(date.today().addDays(5), time.newInstance(17, 0, 0, 0)),
                Name = 'Oportunidade (Sir Teste2)',
                AccountId = person2.Id,
                Dealer__c = accDealer.Id,
                OpportunitySource__c = 'TEST DRIVE',
                OpportunitySubSource__c = 'INTERNET',
                SourceMedia__c = 'Site Concessionária'
            );

            Database.insert(new List<Opportunity>{oppt1, oppt2});


            qt1 = new Quote(
                Name = 'teste',
                Status = 'Open',
                OpportunityId = oppt1.Id
            );
            Database.insert(qt1);
        }
    }


	@isTest static void testStatusLeadList() {
		DashboardLeadController ctrl = new DashboardLeadController();

		Test.startTest();
		List<SelectOption> statusLeadList = ctrl.statusLeadList;
		Test.stopTest();
	}

	@isTest static void testChangeStatus_nqualificado() {
		DashboardLeadController ctrl = new DashboardLeadController();
		ctrl.statusLead = 'nqualificado';

		Test.startTest();
		ctrl.changeStatus();
		Test.stopTest();
	}

	@isTest static void testChangeStatus_ncontato() {
		DashboardLeadController ctrl = new DashboardLeadController();
		ctrl.statusLead = 'ncontato';

		Test.startTest();
		ctrl.changeStatus();
		Test.stopTest();
	}

	@isTest static void testChangeStatus_qualificado() {
		DashboardLeadController ctrl = new DashboardLeadController();
		ctrl.statusLead = 'qualificado';

		Test.startTest();
		ctrl.changeStatus();
		Test.stopTest();
	}

	@isTest static void testGetLead() {
		DashboardLeadController ctrl = new DashboardLeadController();

		Test.startTest();
		ctrl.getLead(lead1.Id);
		Test.stopTest();
	}

	@isTest static void testCancelar(){
		DashboardLeadController ctrl = new DashboardLeadController();

		Test.startTest();
		ctrl.cancelar();
		Test.stopTest();
	}

	@isTest static void testSave(){
        Task tsk = new Task(
            OwnerId = manager1.Id,
            WhoId = lead1.Id,
            Subject = 'Prospection Attendance',
            Status = 'In Progress',
            ActivityDatetime__c = date.today(), 
            IsVisibleInSelfService = true
        );
        Database.insert(tsk);

		Test.setCurrentPageReference(new PageReference('/apex/DashboardLead'));
        System.currentPageReference().getParameters().put('actId', tsk.Id);

		DashboardLeadController ctrl = new DashboardLeadController();
        //ctrl.selectedBrand = 'RENAULT';
        //ctrl.selectedModel = 'Sandero';
        ctrl.statusLead = 'nqualificado';
        ctrl.motivo = 'Porque sim';
        ctrl.previsao = 'Amanhã';

        Test.startTest();
		ctrl.save();
		Test.stopTest();
	}

}