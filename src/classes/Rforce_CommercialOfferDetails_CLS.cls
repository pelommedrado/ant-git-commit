/*****************************************************************************************
    Name            : Rforce_CommercialOfferDetails_CLS
    Description     : This apex class is a POJO class which will contains the values declaration Adobe Commercial Offer
    Project Name    : Rforce
    Release Number  : 9.1
    Implemented Date: 08/12/2015
    Imnplemented By : Venkatesh Kumar Sakthivel
    
******************************************************************************************/

global class Rforce_CommercialOfferDetails_CLS{  
        
    public String strAccFirstName{ get; set; }
    public String strAccLastName{ get; set; }
    public String strAccOfferTitle{ get; set; }    
    public String strAccOfferSalutation{ get; set; }   
    public String strAcctCompanyID{ get; set; }
    public String strAccountID{ get; set; }
    public String strAcctCustomerIdentificationNumber{ get; set; }   
    public String strAcctPhonenumber{ get; set; }    
    public String strVenhVIN{ get; set; }    
    public String strVenhVehicleModel{ get; set; }
    public String strVenhYearModel{ get; set; }
    public String strComofferMarketingDesc{ get; set; }
    public String strCommofferName{ get; set; }
    public String strLegalConditions{ get; set; }
    public String strEligibility{ get; set; }
    public String strOffercode{ get; set; }
    public String strOfferLabel{ get; set; }
    public Date dteCommofferValidityDate{ get; set; }

    global Rforce_CommercialOfferDetails_CLS(){}
    
    global Rforce_CommercialOfferDetails_CLS(String strAccFirstName,String strAccLastName,String strAccOfferTitle,String strAccOfferSalutation,String strAcctCompanyID,String strAccountID,String strAcctCustomerIdentificationNumber,String strAcctPhonenumber,String strVenhVIN,String strVenhVehicleModel,String strVenhYearModel,String strComofferMarketingDesc,String strCommofferName,String strLegalConditions,String strEligibility,String strOffercode,Date dteCommofferValidityDate, String strOfferLabel){
        
        this.strAccFirstName= strAccFirstName;
        this.strAccLastName= strAccLastName;
        this.strAccOfferTitle= strAccOfferTitle;
        this.strAccOfferSalutation= strAccOfferSalutation;
        this.strAcctCompanyID= strAcctCompanyID;
        this.strAccountID= strAccountID;
        this.strAcctCustomerIdentificationNumber= strAcctCustomerIdentificationNumber;
        this.strAcctPhonenumber= strAcctPhonenumber;
        this.strVenhVIN= strVenhVIN;
        this.strVenhVehicleModel= strVenhVehicleModel;
        this.strVenhYearModel= strVenhYearModel;
        this.strComofferMarketingDesc= strComofferMarketingDesc;
        this.strCommofferName= strCommofferName;
        this.strLegalConditions=strLegalConditions;
        this.strEligibility=strEligibility;
        this.strOffercode=strOffercode;
        this.strOfferLabel=strOfferLabel;           
        this.dteCommofferValidityDate= dteCommofferValidityDate;
    }  

}