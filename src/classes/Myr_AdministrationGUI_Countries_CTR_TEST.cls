/**
* @author sebastien.ducamp@atos
* @description Test class for Myr_AdministrationGUI_Countries_CTR_TEST
* @version 1.0 / 16.07 / Sébastien Ducamp / Initial revision
*/
@isTest
private class Myr_AdministrationGUI_Countries_CTR_TEST { 

	//Settings are created through this setup method to keep the control on the assertions
	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareMyrCustomSettings();
		List<Country_Info__c> listCountries = new List<Country_Info__c>();
		listCountries.add(new Country_Info__c(
            	Name='France', Case_RecordType__c = 'FR_Case_RecType', CompanyAccount_RecordType__c ='CORE_ACC_Company_Account_RecType',
                CompanyContact_RecordType__c='CORE_CON_Contact_RecType', Country_Code_2L__c='FR', Country_Code_3L__c = 'FRA', CurrencyCode__c = 'EUR',
                DataSource__c = 'MDM', DealerAccount_RecordType__c = 'Network_Site_Acc', DealerEmployee_RecordType__c = 'CORE_CON_Network_Contact_RecType', 
                GoodWill_RecordType__c='Null 2', Language__c = 'French', LanguageCode__c = 'FRA', PersonalAccount_RecordType__c = 'CORE_ACC_Personal_Account_RecType', 
                TypeOfCountry__c = '', Myr_MatchingKey__c = 'FLE', EmailEncodingCode__c='ISO-8859-1'
				, LanguageLocaleCode__c = 'fr', Myr_MatchingKeysPersData__c='FLE'
				, Myr_ATC_Synchronisable__c=true , Country_Code_2L_ISO__c='FR'
                , Myr_alerting__c='Maintenance & car notifications;Offers and special events;Newsletter & brand communications'));
		 listCountries.add(new Country_Info__c(
            	Name='Italy', Case_RecordType__c = 'IT_Case_RecType', CompanyAccount_RecordType__c ='CORE_ACC_Company_Account_RecType',
                CompanyContact_RecordType__c='CORE_CON_Contact_RecType', Country_Code_2L__c='IT', Country_Code_3L__c = 'ITA', CurrencyCode__c = 'EUR',
                DataSource__c = 'SIC', DealerAccount_RecordType__c = 'Network_Site_Acc', DealerEmployee_RecordType__c = 'CORE_CON_Network_Contact_RecType', 
                GoodWill_RecordType__c='IT_GOO_Commercial_RecType', Language__c = 'Italian', LanguageCode__c = 'ITA', PersonalAccount_RecordType__c = 'CORE_ACC_Personal_Account_RecType', 
                TypeOfCountry__c = '', Myr_MatchingKey__c = 'CFL;FLE', EmailEncodingCode__c='ISO-8859-1',
                Myr_CustomerIdentificationNbr_Pattern__c='[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[A-Z0-9]{3}[A-Z]{1}',
                Myr_EnablePersDataCall__c=true, Myr_MatchingKeysPersData__c='CcFL;FLcE',Myr_Enable_Login_GetVehicle__c=true
				,Myr_ATC_Synchronisable__c=true, Country_Code_2L_ISO__c='IT',
                Gigya_REN_K__c='gig_ren_key', Gigya_DAC_K__c='gig_dac_key', Gigya_Datacenter__c='gig_datacenter', Myr_TC_Acceptance__c=false
				, Myr_alerting__c='Maintenance & car notifications;Offers and special events;Newsletter & brand communications'));
		insert listCountries;
	}

	//Test initialization of the controller
	static testmethod void testInit () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( 2, controller.CountriesSettings.keySet().size() );
			system.assertEquals( 2, controller.CountryFields.keySet().size() );
			for( String key : controller.EditableFields.keySet() ) {
				if( key.containsIgnoreCase( 'Myr' ) ) {
					system.assertEquals(true, controller.EditableFields.get(key) );
				} else {
					system.assertEquals(false, controller.EditableFields.get(key) );
				}
			}
		}
	}

	//Test initialization of the controller
	static testmethod void testInitWithReadModeOnly () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( false, controller.CountryFields.containsKey('Datasource__c') );
			//trigger the test by setting a new field to display in read-only mode
			controller.MyrSettings.Myr_GUI_CountryInfoFixedFields__c = 'Datasource__c'; //set one existing field
			controller.getSettings();
			//check the result
			system.assertEquals( true, controller.EditableFields.containsKey('Datasource__c') );
			system.assertEquals( false, controller.EditableFields.get('Datasource__c') );
		}
	}

	//Test initialization of the controller
	static testmethod void testInitWithReadModeOnly_Failed () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( false, controller.EditableFields.containsKey('Datasource__c') );
			controller.MyrSettings.Myr_GUI_CountryInfoFixedFields__c = 'Datasource__c'; //set one existing field and another fake one
			controller.MyrSettings.Myr_GUI_CountryInfoFixedFields__c = 'Datasource__c,FakeField__c';
			controller.getSettings();
			system.assertEquals( true, controller.EditableFields.containsKey('Datasource__c') );
			system.assertEquals( false, controller.EditableFields.containsKey('FakeField__c') );
		}
	}

	//Test initialization of the controller
	static testmethod void testInitWithReadModeOnly_GlobalFailed () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			controller.ExceptionReadField = true;
			system.assertEquals( false, controller.EditableFields.containsKey('Datasource__c') );
			controller.MyrSettings.Myr_GUI_CountryInfoFixedFields__c = 'Datasource__c'; //set one existing field and another fake one
			controller.MyrSettings.Myr_GUI_CountryInfoFixedFields__c = 'Datasource__c,FakeField__c';
			controller.getSettings();
			system.assertEquals( false, controller.EditableFields.containsKey('Datasource__c') );
			system.assertEquals( false, controller.EditableFields.containsKey('FakeField__c') );
		}
	}

	//Test the list of countries available
	static testMethod void testListCountries() {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( 2, controller.CountriesSettings.keySet().size() );
			system.assertEquals( 2 + 1, controller.getCountries().size() );
		}
	}

	//Test save OK
	static testMethod void testSave_OK() {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			controller.SelectedCountry='France';
			system.assertEquals( 'FLE', (String) controller.CountriesSettings.get(controller.SelectedCountry).Fields.get('myr_matchingkeyspersdata__c').value );
			controller.CountriesSettings.get(controller.SelectedCountry).Fields.get('myr_matchingkeyspersdata__c').value = 'CFL';
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			system.assertEquals( 'CFL', [SELECT Id, Myr_MatchingKeysPersData__c FROM Country_Info__c WHERE Name=:controller.SelectedCountry][0].Myr_MatchingKeysPersData__c );
		}
	}

	//Test Save Failed
	static testMethod void testSave_Failed() {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			controller.SelectedCountry='France';
			system.assertEquals( 'FLE', controller.CountriesSettings.get(controller.SelectedCountry).Fields.get('myr_matchingkeyspersdata__c').value );
			controller.CountriesSettings.get(controller.SelectedCountry).Fields.get('myr_matchingkeyspersdata__c').value = 'longstringggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg';
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
			system.assertEquals( 'FLE', [SELECT Id, Myr_MatchingKeysPersData__c FROM Country_Info__c WHERE Name=:controller.SelectedCountry][0].Myr_MatchingKeysPersData__c );
		}
	}

	//Test Edit button behavior
	static testMethod void testEdit() {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
		}
	}

	//Test Cancel button behavior
	static testMethod void testCancel() {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Countries_CTR controller = new Myr_AdministrationGUI_Countries_CTR();
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			controller.cancel();
			system.assertEquals( false, controller.EditMode );
		}
	}

}