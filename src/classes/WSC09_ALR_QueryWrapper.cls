public class WSC09_ALR_QueryWrapper {
    
    private final static String STATUS_PENDING_PROCESSING = 'Pending processing';
   
    public static Attachment getAttachment(String idAttachment){
        return [SELECT Id, Name, Body, Description FROM Attachment where Id =: idAttachment];
    } 
    
    //getAvaria for lsatUpdateTime 
    public static List<ALR_Activate_Insurance__c> getActivate_Insurance_lastUpdate(Datetime lastUpdateTime){
        System.debug('##### passou nesse');
        return [select ALR_Amount_Damage__c,ALR_Approved_Value_Parts__c,ALR_Approved_Value_Salvado__c,ALR_Approved_Value_Service__c,
			ALR_Arrival_Dealership__c,ALR_BIR_Dealer__c,ALR_Carrier__c,ALR_Chassi__c,ALR_Check_Caveat__c,ALR_Check_CTe__c,
			ALR_Check_Invoice_Carrier__c,ALR_Check_Invoice_Combined__c,ALR_Check_Invoice_Parts__c,ALR_Check_Invoice_Salvado__c,
			ALR_Check_Invoice_Service__c,ALR_Check_Photo_Chassi__c,ALR_City_Occurrence__c,ALR_Claim_Number__c,
			ALR_CNPJ_Carrier__c,ALR_Color__c,ALR_Comercial_Phone__c,ALR_Contact_Email__c,ALR_Contact_Name__c,ALR_CTe__c,
			ALR_Date_Docs_Received__c,ALR_Date_Docs_Refused__c,ALR_Date_Error_Insurance__c,ALR_Date_Estimate_Received__c,
			ALR_Date_Estimate_Requested__c,ALR_Date_Finalized_Process__c,ALR_Date_Inspection_Received__c,
			ALR_Date_Inspection_Requested__c,ALR_Date_Insurance_Ended__c,ALR_Date_No_Send_Insurer__c,ALR_Date_Occurrence__c,
			ALR_Date_Open_Insurance__c,ALR_Date_Pending_Processing__c,ALR_Date_Process__c,ALR_Date_Received_Insurer__c,
			ALR_Date_Repair_Released__c,ALR_Date_Reparation_Requested__c,ALR_Date_Total_Loss_Defined__c,
			ALR_DDD_Comercial_Phone__c,ALR_DDD_Mobile__c,ALR_DDD__c,ALR_Dealer__c,ALR_Description_Pending__c,
			ALR_Driver_Name__c,ALR_Error_Description__c,ALR_Estimated_Loss_Value__c,ALR_Estimated_Value__c,
			ALR_Flag_Caveat_CTe__c,ALR_Flag_Caveat__c,ALR_Flag_CTe__c,ALR_Flag_Invoice_Carrier__c,ALR_Flag_Invoice_Combined__c,
			ALR_Flag_Invoice_Parts_Refused__c,ALR_Flag_Invoice_Parts__c,ALR_Flag_Invoice_Salvado__c,
			ALR_Flag_Invoice_Service_Refused__c,ALR_Flag_Invoice_Service__c,ALR_indemnification_of_parts__c,
			ALR_Indemnification_of_Service__c,ALR_Indemnification_total_loss__c,ALR_Inspection_Type__c,ALR_Inspector_Email__c,
			ALR_Inspector_Name__c,ALR_Invoice_Combined_Refused__c,ALR_Invoice_Number_Serie__c,ALR_Invoice_Parts_Refused__c,
			ALR_Invoice_Salavado_Refused__c,ALR_Invoice_Service_Refused__c,ALR_Invoice_Value__c,ALR_Km__c,
			ALR_Link_Estimate_Orion__c,ALR_Sinister_Nature__c,ALR_Local_Ocorrence__c,ALR_Mobile__c,ALR_Model__c,ALR_Myself__c,
			ALR_Necessary_Repair_TL_Docs__c,ALR_Neighborhood_Occurrence__c,ALR_Observation__c,ALR_Pending_Docs_Received__c,
			ALR_Pending_Docs_Refused__c,ALR_Pending_Estimate__c,ALR_Pending_Inspection__c,ALR_Pending_Insurance_Ended__c,
			ALR_Pending_Insurance_Open__c,ALR_Pending_Released_Repair__c,ALR_Pending_Reparation__c,ALR_Phase_Process__c,
			ALR_Phone__c,ALR_Plate__c,ALR_Reason_Refusal__c,ALR_Send_Email__c,ALR_Send_SMS__c,ALR_Status_System__c,ALR_Status__c,
			ALR_Trunck_Position_Number__c,ALR_UF_Occurrence__c,ALR_Version__c,ALR_Year_Model_Version__c,
			ALR_Zip_Code_Occurrence__c,CreatedById,CreatedDate,CurrencyIsoCode,LastModifiedDate,Id,Name,The_Damaged_Vehicle_Region__c,
            	ALR_Chassi__r.ALR_Brand_Vehicle__c,ALR_Chassi__r.ALR_BIR_Dealer__c,ALR_Chassi__r.ALR_Model__c,
                ALR_Chassi__r.ALR_Chassi__r.Manufacturing_Year__c,ALR_Chassi__r.ALR_Chassi__r.Name, ALR_Chassi__r.ALR_Chassi__r.ALR_InvoiceNumber__c, ALR_Chassi__r.ALR_Chassi__r.ALR_SerialNumberOnInvoice__c
			FROM ALR_Activate_Insurance__c where ALR_lastUpdateTime__c > :lastUpdateTime and ALR_Status_System__c =: WSC09_ALR_QueryWrapper.STATUS_PENDING_PROCESSING
            ];
    }
    
    //retorna avarias sem nenhum parametro
    public static List<ALR_Activate_Insurance__c> getActivate_Insurance_zeroParamethers(){
        return [select ALR_Amount_Damage__c,ALR_Approved_Value_Parts__c,ALR_Approved_Value_Salvado__c,ALR_Approved_Value_Service__c,
			ALR_Arrival_Dealership__c,ALR_BIR_Dealer__c,ALR_Carrier__c,ALR_Chassi__c,ALR_Check_Caveat__c,ALR_Check_CTe__c,
			ALR_Check_Invoice_Carrier__c,ALR_Check_Invoice_Combined__c,ALR_Check_Invoice_Parts__c,ALR_Check_Invoice_Salvado__c,
			ALR_Check_Invoice_Service__c,ALR_Check_Photo_Chassi__c,ALR_City_Occurrence__c,ALR_Claim_Number__c,
			ALR_CNPJ_Carrier__c,ALR_Color__c,ALR_Comercial_Phone__c,ALR_Contact_Email__c,ALR_Contact_Name__c,ALR_CTe__c,
			ALR_Date_Docs_Received__c,ALR_Date_Docs_Refused__c,ALR_Date_Error_Insurance__c,ALR_Date_Estimate_Received__c,
			ALR_Date_Estimate_Requested__c,ALR_Date_Finalized_Process__c,ALR_Date_Inspection_Received__c,
			ALR_Date_Inspection_Requested__c,ALR_Date_Insurance_Ended__c,ALR_Date_No_Send_Insurer__c,ALR_Date_Occurrence__c,
			ALR_Date_Open_Insurance__c,ALR_Date_Pending_Processing__c,ALR_Date_Process__c,ALR_Date_Received_Insurer__c,
			ALR_Date_Repair_Released__c,ALR_Date_Reparation_Requested__c,ALR_Date_Total_Loss_Defined__c,
			ALR_DDD_Comercial_Phone__c,ALR_DDD_Mobile__c,ALR_DDD__c,ALR_Dealer__c,ALR_Description_Pending__c,
			ALR_Driver_Name__c,ALR_Error_Description__c,ALR_Estimated_Loss_Value__c,ALR_Estimated_Value__c,
			ALR_Flag_Caveat_CTe__c,ALR_Flag_Caveat__c,ALR_Flag_CTe__c,ALR_Flag_Invoice_Carrier__c,ALR_Flag_Invoice_Combined__c,
			ALR_Flag_Invoice_Parts_Refused__c,ALR_Flag_Invoice_Parts__c,ALR_Flag_Invoice_Salvado__c,
			ALR_Flag_Invoice_Service_Refused__c,ALR_Flag_Invoice_Service__c,ALR_indemnification_of_parts__c,
			ALR_Indemnification_of_Service__c,ALR_Indemnification_total_loss__c,ALR_Inspection_Type__c,ALR_Inspector_Email__c,
			ALR_Inspector_Name__c,ALR_Invoice_Combined_Refused__c,ALR_Invoice_Number_Serie__c,ALR_Invoice_Parts_Refused__c,
			ALR_Invoice_Salavado_Refused__c,ALR_Invoice_Service_Refused__c,ALR_Invoice_Value__c,ALR_Km__c,
			ALR_Link_Estimate_Orion__c,ALR_Sinister_Nature__c,ALR_Local_Ocorrence__c,ALR_Mobile__c,ALR_Model__c,ALR_Myself__c,
			ALR_Necessary_Repair_TL_Docs__c,ALR_Neighborhood_Occurrence__c,ALR_Observation__c,ALR_Pending_Docs_Received__c,
			ALR_Pending_Docs_Refused__c,ALR_Pending_Estimate__c,ALR_Pending_Inspection__c,ALR_Pending_Insurance_Ended__c,
			ALR_Pending_Insurance_Open__c,ALR_Pending_Released_Repair__c,ALR_Pending_Reparation__c,ALR_Phase_Process__c,
			ALR_Phone__c,ALR_Plate__c,ALR_Reason_Refusal__c,ALR_Send_Email__c,ALR_Send_SMS__c,ALR_Status_System__c,ALR_Status__c,
			ALR_Trunck_Position_Number__c,ALR_UF_Occurrence__c,ALR_Version__c,ALR_Year_Model_Version__c,
			ALR_Zip_Code_Occurrence__c,CreatedById,CreatedDate,CurrencyIsoCode,LastModifiedDate,Id,Name,The_Damaged_Vehicle_Region__c,
            	ALR_Chassi__r.ALR_Brand_Vehicle__c,ALR_Chassi__r.ALR_BIR_Dealer__c,ALR_Chassi__r.ALR_Model__c,
                ALR_Chassi__r.ALR_Chassi__r.Manufacturing_Year__c,ALR_Chassi__r.ALR_Chassi__r.Name, ALR_Chassi__r.ALR_Chassi__r.ALR_InvoiceNumber__c, ALR_Chassi__r.ALR_Chassi__r.ALR_SerialNumberOnInvoice__c
			FROM ALR_Activate_Insurance__c where ALR_Status_System__c =: WSC09_ALR_QueryWrapper.STATUS_PENDING_PROCESSING
            ];
    }
    
    //getAvarias for Id
	public static List<ALR_Activate_Insurance__c> getActivate_Insurance_Id(String idActiveInsurance){
        return [select ALR_Amount_Damage__c,ALR_Approved_Value_Parts__c,ALR_Approved_Value_Salvado__c,ALR_Approved_Value_Service__c,
			ALR_Arrival_Dealership__c,ALR_BIR_Dealer__c,ALR_Carrier__c,ALR_Chassi__c,ALR_Check_Caveat__c,ALR_Check_CTe__c,
			ALR_Check_Invoice_Carrier__c,ALR_Check_Invoice_Combined__c,ALR_Check_Invoice_Parts__c,ALR_Check_Invoice_Salvado__c,
			ALR_Check_Invoice_Service__c,ALR_Check_Photo_Chassi__c,ALR_City_Occurrence__c,ALR_Claim_Number__c,
			ALR_CNPJ_Carrier__c,ALR_Color__c,ALR_Comercial_Phone__c,ALR_Contact_Email__c,ALR_Contact_Name__c,ALR_CTe__c,
			ALR_Date_Docs_Received__c,ALR_Date_Docs_Refused__c,ALR_Date_Error_Insurance__c,ALR_Date_Estimate_Received__c,
			ALR_Date_Estimate_Requested__c,ALR_Date_Finalized_Process__c,ALR_Date_Inspection_Received__c,
			ALR_Date_Inspection_Requested__c,ALR_Date_Insurance_Ended__c,ALR_Date_No_Send_Insurer__c,ALR_Date_Occurrence__c,
			ALR_Date_Open_Insurance__c,ALR_Date_Pending_Processing__c,ALR_Date_Process__c,ALR_Date_Received_Insurer__c,
			ALR_Date_Repair_Released__c,ALR_Date_Reparation_Requested__c,ALR_Date_Total_Loss_Defined__c,
			ALR_DDD_Comercial_Phone__c,ALR_DDD_Mobile__c,ALR_DDD__c,ALR_Dealer__c,ALR_Description_Pending__c,
			ALR_Driver_Name__c,ALR_Error_Description__c,ALR_Estimated_Loss_Value__c,ALR_Estimated_Value__c,
			ALR_Flag_Caveat_CTe__c,ALR_Flag_Caveat__c,ALR_Flag_CTe__c,ALR_Flag_Invoice_Carrier__c,ALR_Flag_Invoice_Combined__c,
			ALR_Flag_Invoice_Parts_Refused__c,ALR_Flag_Invoice_Parts__c,ALR_Flag_Invoice_Salvado__c,
			ALR_Flag_Invoice_Service_Refused__c,ALR_Flag_Invoice_Service__c,ALR_indemnification_of_parts__c,
			ALR_Indemnification_of_Service__c,ALR_Indemnification_total_loss__c,ALR_Inspection_Type__c,ALR_Inspector_Email__c,
			ALR_Inspector_Name__c,ALR_Invoice_Combined_Refused__c,ALR_Invoice_Number_Serie__c,ALR_Invoice_Parts_Refused__c,
			ALR_Invoice_Salavado_Refused__c,ALR_Invoice_Service_Refused__c,ALR_Invoice_Value__c,ALR_Km__c,
			ALR_Link_Estimate_Orion__c,ALR_Sinister_Nature__c,ALR_Local_Ocorrence__c,ALR_Mobile__c,ALR_Model__c,ALR_Myself__c,
			ALR_Necessary_Repair_TL_Docs__c,ALR_Neighborhood_Occurrence__c,ALR_Observation__c,ALR_Pending_Docs_Received__c,
			ALR_Pending_Docs_Refused__c,ALR_Pending_Estimate__c,ALR_Pending_Inspection__c,ALR_Pending_Insurance_Ended__c,
			ALR_Pending_Insurance_Open__c,ALR_Pending_Released_Repair__c,ALR_Pending_Reparation__c,ALR_Phase_Process__c,
			ALR_Phone__c,ALR_Plate__c,ALR_Reason_Refusal__c,ALR_Send_Email__c,ALR_Send_SMS__c,ALR_Status_System__c,ALR_Status__c,
			ALR_Trunck_Position_Number__c,ALR_UF_Occurrence__c,ALR_Version__c,ALR_Year_Model_Version__c,
			ALR_Zip_Code_Occurrence__c,CreatedById,CreatedDate,CurrencyIsoCode,LastModifiedDate,Id,Name,The_Damaged_Vehicle_Region__c,
                ALR_Chassi__r.ALR_Brand_Vehicle__c,ALR_Chassi__r.ALR_BIR_Dealer__c,ALR_Chassi__r.ALR_Model__c,
                ALR_Chassi__r.ALR_Chassi__r.Manufacturing_Year__c,ALR_Chassi__r.ALR_Chassi__r.Name, ALR_Chassi__r.ALR_Chassi__r.ALR_InvoiceNumber__c, ALR_Chassi__r.ALR_Chassi__r.ALR_SerialNumberOnInvoice__c
			FROM ALR_Activate_Insurance__c where id =: idActiveInsurance 
               ];
    }
    
    public static Map<String,Account> getCss(){
        Map<String, Account> mapAccount= new Map<String, Account>(); 
        for(Account conta : [SELECT IDBIR__c,Name,CompanyID__c, ShippingPostalCode FROM Account where Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH']){
            mapAccount.put(conta.IDBIR__c, conta);
        }

        return mapAccount;
    } 
    
    public static Map<Id, String> getSinisterNature (List<ALR_Activate_Insurance__c> lsIdActivateInsurance){
        
        Map<Id, String> mapSinisterNature = new Map<Id, String>();
        List<ALR_Activate_Insurance__c> lsActIns = [SELECT toLabel(ALR_Sinister_Nature__c) FROM ALR_Activate_Insurance__c WHERE Id in: lsIdActivateInsurance];
        for(ALR_Activate_Insurance__c act : lsActIns){
            mapSinisterNature.put(act.Id, act.ALR_Sinister_Nature__c);
        }
        
        return mapSinisterNature;
    }
    
    
    
    
    public static List<ALR_Damage__c> getDamage(String idActiveInsurance){
        return [
            SELECT ALR_Activate_Insurance__c,ALR_Cod_Quadrant__c,ALR_Cod_Size_Of_Damage__c,ALR_Damage_Area__c,
            ALR_Damage_Type__c,ALR_Flash_Aves__c,ALR_Photo1__c,ALR_Photo2__c,ALR_Photo3__c,ALR_Position__c,
            ALR_Referencial_Part__c,ALR_Size_Damage__c,ALR_Status__c,ALR_URL_Photo1__c,
            CreatedById,CreatedDate,CurrencyIsoCode,LastModifiedDate,Id,Name
            FROM ALR_Damage__c where ALR_Activate_Insurance__c =:idActiveInsurance 
        ];
    }
     
    
    public static Map<ID,ALR_Damage__c> getMapDamage(List<ALR_Activate_Insurance__c> lsActiveInsurance){
        return new Map<Id,ALR_Damage__c>(
        								[select Id, toLabel(ALR_Damage_Area__c),toLabel(ALR_Referencial_Part__c),
                                      toLabel(ALR_Position__c), toLabel(ALR_Cod_Quadrant__c), toLabel(ALR_Damage_Type__c),
                                      toLabel(ALR_Flash_Aves__c),toLabel(ALR_Size_Damage__c) ,CreatedById
                                      from ALR_Damage__c where ALR_Activate_Insurance__c in:(lsActiveInsurance) ]
        );
    }
    
    public static Map<ID,ALR_Damage__c> getMapDamage(String idActiveInsurance){
        Map<Id,ALR_Damage__c> mapaReturn = new Map<Id,ALR_Damage__c>();
        List<ALR_Damage__c> listdamage = [select Id, toLabel(ALR_Damage_Area__c),toLabel(ALR_Referencial_Part__c),
                                      toLabel(ALR_Position__c), toLabel(ALR_Cod_Quadrant__c), toLabel(ALR_Damage_Type__c),
                                      toLabel(ALR_Flash_Aves__c),toLabel(ALR_Size_Damage__c) ,CreatedById
                                      from ALR_Damage__c where ALR_Activate_Insurance__c =:idActiveInsurance ];
        for(ALR_Damage__c damage: listdamage)
            mapaReturn.put(damage.id,damage);
        
        return mapaReturn;
    }
    public static Map<ID,ALR_Damage__c> getMapDamageNoLabel(String idActiveInsurance){
        Map<Id,ALR_Damage__c> mapaReturn = new Map<Id,ALR_Damage__c>();
        List<ALR_Damage__c> listdamage = [select Id, ALR_Damage_Area__c,ALR_Referencial_Part__c,
                                      ALR_Position__c, ALR_Cod_Quadrant__c, ALR_Damage_Type__c,
                                     ALR_Flash_Aves__c,ALR_Size_Damage__c ,CreatedById
                                      from ALR_Damage__c where ALR_Activate_Insurance__c =:idActiveInsurance ];
        for(ALR_Damage__c damage: listdamage)
            mapaReturn.put(damage.id,damage);
        
        return mapaReturn;
    }
}