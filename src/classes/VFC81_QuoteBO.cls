/**
*   Class   -   VFC80_QuoteBO
*   Author  -   Christian Ranha
*   Date    -   24/03/2013
*   #01 <ChristianRanha> <15/11/2012>
*   BO class for Quote object to logical function.
**/
public with sharing class VFC81_QuoteBO {

    private static final VFC81_QuoteBO instance = new VFC81_QuoteBO();
        
    /*private constructor to prevent the creation of instances of this class*/
    private VFC81_QuoteBO()
    {
    }

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC81_QuoteBO getInstance()
    {
        return instance;
    }

    /**
    * Create externalId to quote
    * @param Quote List
    * @author Edvaldo Fernandes [ kolekto ].
    */
    public void setExternalId(Map<Id,Quote> oldQuotes, List<Quote> quoteList){

        for(Quote q: quoteList){
            if( oldQuotes != null && oldQuotes.get(q.Id).External_Id__c == null ){
                q.External_Id__c = q.QuoteNumber;
            }
        }

    }

    public void setDateTimePreOrderReceived(List<Quote> quotes){
        for(Quote q: quotes){
            if(q.Status == 'Pre Order Sent'){
                q.Pre_Order_Received_Date_Time__c = System.now();
            }
            if(q.Status == 'Pre Order Requested'){
                q.Pre_Order_Sent_Date_Time__c = System.now();
            }
        }
    }

    /**
    * Cria ou obtém um orçamento.
    * @param quoteId O id do orçamento.
    * @param opportunityId O id da oportunidade.
    * @return Um objeto Quote.
    * @author Christian Ranha.
    */
    public Quote getQuoteCreatedOrOpenFromOpportunity(String quoteId, String opportunityId)
    {
        Quote sObjQuote = null;
        
        /*obtêm objeto quote para edição*/
        if (quoteId != null)
        {
            sObjQuote = VFC35_QuoteDAO.getInstance().findById(quoteId);     
        }
        /*cria um novo objeto quote*/
        else
        {
            sObjQuote = this.createNewQuote(opportunityId);
        }
        
        return sObjQuote;
    }
    
     /**
    * Cria um orçamento.
    * @param opportunityId O id da oportunidade.
    * @return Um objeto Quote.
    * @author Christian Ranha.
    */
    public Quote createNewQuote(String opportunityId)
    {
    	String vehicleName = '';
        Opportunity sObjOpportunity = null;
        Quote sObjQuote = new Quote();
		Pricebook2 sObjPriceBook2 = null;
		
        /*obtêm objeto oportunidade pelo id*/
        sObjOpportunity = VFC47_OpportunityBO.getInstance().findById(opportunityId);
        
        /*Obtêm o catalogo de preço padrão*/
        sObjPriceBook2 = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();
        
        /*carrega informações iniciais*/
        if(sObjOpportunity.Account.VehicleInterest_BR__c != null )
        	vehicleName = sObjOpportunity.Account.VehicleInterest_BR__c;
        	
        sObjQuote.Name =  'ORÇAMENTO ' + ' (' + sObjOpportunity.Account.Name + ')';
        sObjQuote.Status = 'Open';
        sObjQuote.Pricebook2Id = sObjPriceBook2.Id;
        sObjQuote.OpportunityId = opportunityId;        

        try
        {
            /*insere o objeto e pega o objeto de retorno que já vem com o campo name preenchido*/
            sObjQuote = VFC35_QuoteDAO.getInstance().insertData(sObjQuote);  
        }
        catch(DMLException ex)
        {
            throw new VFC82_CreateQuoteException(ex, ex.getDMLMessage(0));
        }
            
        return sObjQuote;
    }
    
    /**
    * Atualiza um orçamento.
    * @param sObjQuote Um objeto quote.
    * @author Christian Ranha.
    */
    public void updateQuote(Quote sObjQuote)
	{
		try
		{
			VFC35_QuoteDAO.getInstance().updateData(sObjQuote);
		}
		catch(DMLException ex)
		{
			throw new VFC92_UpdateQuoteException(ex, ex.getDMLMessage(0));		
		}		
	}

    /**
    * Método que substitui o workflow "Update LastStatusValue Field" para resolver erro de 101 querys
    * @param Quote List
    * @author Edvaldo - [ kolekto ]
    */
    public void updateLastStatusField(Map<Id,Quote> quoteNews, Map<Id,quote> quoteOlds){
        System.debug('**updateLastStatusField()');

        for(Quote q: quoteNews.values()){
            q.Last_Status_Value__c = quoteOlds.get(q.Id).Status != null ? quoteOlds.get(q.Id).Status : null;
        }

    } 
}