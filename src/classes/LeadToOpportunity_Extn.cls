public with sharing class LeadToOpportunity_Extn {

    Id leadId=ApexPages.currentPage().getParameters().get('Id');
    String strCriteria = ApexPages.currentPage().getParameters().get('Criteria');
    List<Lead> leadRec=new List<Lead>();
     Opportunity oppRec;
    public LeadToOpportunity_Extn(ApexPages.StandardController controller) {
  
   
    }
   public PageReference opportunityRedirectPage(){
    
    leadRec=[Select Id,Account__c,Name,Current_Vehicle__c,Account__r.ShippingStreet,DealerOfInterest__r.IDBIR__c,Account__r.ShippingPostalCode,Account__r.ShippingState,Account__r.ShippingCity,Account__r.ShippingCountry,TypeOfInterest__c,SubType_Of_Interest__c,Street,City,State,Country,PostalCode,Broucher__c,CurrentMilage__c,CRV_CurrentVehicle__c,Email,HomePhone__c,BusinessMobilePhone__c,MobilePhone,OptinPhone__c,BusinessPhone__c,OptinSMS__c,OptinEmail__c,Account__r.IDBIRPrefdDealer__r.IDBIR__c,IntentToPurchaseNewVehicle__c,DealerOfInterest__c,Brand_Of_Interest__c,VehicleOfInterest__c,Second_Brand_Of_Interest__c,SecondVehicleOfInterest__c,IsVehicleOwner__c,IsRenaultVehicleOwner__c,OtherInformation__c from Lead where Id=:leadId];
    oppRec=new Opportunity();
    Id oppRecordId= [select Id from RecordType where sObjectType='opportunity' and Name='CCBIC Core'].Id;
    
 System.debug('type********'+leadRec[0].TypeOfInterest__c+'sub'+leadRec[0].SubType_Of_Interest__c+'intend*******'+leadRec[0].IntentToPurchaseNewVehicle__c+'lmt*******'+strCriteria);
    if(strCriteria.equalsignorecase('LMT') && leadRec[0].TypeOfInterest__c!=null && leadRec[0].SubType_Of_Interest__c!=null && leadRec[0].DealerOfInterest__r.IDBIR__c!=null && (leadRec[0].IntentToPurchaseNewVehicle__c=='<1 Month' || leadRec[0].IntentToPurchaseNewVehicle__c=='2 Months' || leadRec[0].IntentToPurchaseNewVehicle__c=='3 Months'|| leadRec[0].IntentToPurchaseNewVehicle__c=='4 Months'|| leadRec[0].IntentToPurchaseNewVehicle__c=='5 Months') ){
 
    oppRec.RecordTypeId=oppRecordId;
    oppRec.StageName='Identified';
    oppRec.CloseDate=date.today()+15; 
    oppRec.Name=leadRec[0].Name;
    oppRec.AccountId=leadRec[0].Account__c;
    oppRec.Status_Sent__c=true;
    oppRec.Brand_Of_Interest__c=leadRec[0].Brand_Of_Interest__c;
    oppRec.Current_Vehicle__c=leadRec[0].Current_Vehicle__c;
    if(leadRec[0].VehicleOfInterest__c!=null){
    oppRec.Vehicle_Of_Interest__c=leadRec[0].VehicleOfInterest__c;
    }
    oppRec.Type_Of_Interest__c=leadRec[0].TypeOfInterest__c;
    oppRec.SubType_Of_Interest__c=leadRec[0].SubType_Of_Interest__c;
    oppRec.Second_Brand_Of_Interest__c=leadRec[0].Second_Brand_Of_Interest__c;
    oppRec.Second_Vehicle_of_Interest__c=leadRec[0].SecondVehicleOfInterest__c;
    oppRec.IsRenaultVehicleOwner__c=leadRec[0].IsRenaultVehicleOwner__c;
    oppRec.IsVehicleOwner__c=leadRec[0].IsVehicleOwner__c;
    oppRec.CurrentMilage__c=leadRec[0].CurrentMilage__c;
    oppRec.CurrentVehicle__c=leadRec[0].CRV_CurrentVehicle__c;
    oppRec.IntenttoPurchaseNewVehicle__c=leadRec[0].IntentToPurchaseNewVehicle__c;
    oppRec.LeadEmail__c=leadRec[0].Email;
    oppRec.HomePhone__c=leadRec[0].HomePhone__c;
    oppRec.ReceiveNewsbyPhone__c=leadRec[0].OptinPhone__c;
    oppRec.ReceiveNewsbyEmail__c=leadRec[0].OptinEmail__c;
    oppRec.ReceiveNewsbySMS__c=leadRec[0].OptinSMS__c;
    oppRec.BusinessPhone__c=leadRec[0].BusinessPhone__c;
    oppRec.Mobile__c=leadRec[0].MobilePhone;
    oppRec.Business_Mobile_Phone__c=leadRec[0].BusinessMobilePhone__c;
    oppRec.Broucher__c=leadRec[0].Broucher__c;
    oppRec.DealerofInterest__c=leadRec[0].DealerOfInterest__c;
    oppRec.OtherInformation__c=leadRec[0].OtherInformation__c;
    oppRec.Street__c=leadRec[0].Account__r.ShippingStreet;
    oppRec.City__c=leadRec[0].Account__r.ShippingCity;
    oppRec.State__c=leadRec[0].Account__r.ShippingState;
    oppRec.Country__c=leadRec[0].Account__r.ShippingCountry;
    oppRec.PostalCode__c=leadRec[0].Account__r.ShippingPostalCode;
  /*
    if(leadRec[0].IntentToPurchaseNewVehicle__c=='<1 Month'){
    oppRec.DateOfPurchaseIntent__c=date.today()+30;
    }
    else if(leadRec[0].IntentToPurchaseNewVehicle__c=='2 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*2);
    }
     else if(leadRec[0].IntentToPurchaseNewVehicle__c=='3 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*3);
    }
     else if(leadRec[0].IntentToPurchaseNewVehicle__c=='4 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*4);
    }
     else if(leadRec[0].IntentToPurchaseNewVehicle__c=='5 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*5);
    }
     else if(leadRec[0].IntentToPurchaseNewVehicle__c=='6-12 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*6);
    }
     else if(leadRec[0].IntentToPurchaseNewVehicle__c=='>12 Months'){
    oppRec.DateOfPurchaseIntent__c=date.today()+(30*12);
    }
    */
    
   // oppRec.PostalCode__c=Integer.valueOf(leadRec[0].PostalCode);  
    try{
    
    insert oppRec;
    leadRec[0].Converted__c=true;
    leadRec[0].Status='Hot Lead';
    update leadRec;
    }
    catch(Exception e){
    System.debug('e*****'+e);
    }
    return new pageReference('/'+oppRec.Id);
}
    
    else{
    try{
    leadRec[0].Converted__c=false;
    leadRec[0].Status='Portfolio';
    leadRec[0].Sent_To_BCS__c=true;
    update leadRec;
    }catch(Exception e){
    System.debug('e*****'+e);
    }
      return new pageReference('/'+leadId);
    }
   
   }


}