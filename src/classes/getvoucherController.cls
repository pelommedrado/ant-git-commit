public class getvoucherController {
    
    public List<Opportunity> oppList{get;set;}
    public String CPF{get;set;}
    
    public getvoucherController(){
        oppLoad();
        getCPF();
    }
    
    public List<Opportunity> lsOpp{
        get{
            return oppList;
        }
    }
    
    
    public void getCPF(){
        CPF = ApexPages.currentPage().getParameters().get('CPF');
    }
    
    public void oppLoad(){
        
        // recupera as oportunidades
        List<Opportunity> lsOpp = Database.query( 
            'SELECT Id, Name, Account_CPF__c, LastModifiedDate, StageName, Dealer__c, Seller__c ' +
            'FROM Opportunity WHERE ' +
            '(StageName = \'In Attendance\' OR StageName = \'Billed\' OR StageName = \'Test Drive\' OR StageName = \'Order\' OR StageName = \'Identified\' ) ' +
            'AND CreatedDate = LAST_N_DAYS:30 ORDER BY CreatedDate ASC limit 999'
        );
        
        oppList = new List<Opportunity>();
        oppList.addAll(lsOpp);
    }
    
    public PageReference consultarVouchers(){
        String oppId = ApexPages.currentPage().getParameters().get('oppId');
        System.debug('oppId:' + oppId);
        
        Opportunity opp = [SELECT Account_CPF__c, StageName FROM Opportunity WHERE Id =: oppId limit 1];
        
        PageReference pg = new PageReference('/apex/getvoucherconsult?Id=' + opp.Id + 
                                             '&CPF='+ opp.Account_CPF__c + '&stage=' + opp.StageName);
        return pg;
    }
   

}