@isTest
public class LMT_02_warmLeadControllerTest {

    static Lead lead;

    static{

        lead = new Lead();
        lead.FirstName='Teste';
        lead.LastName='Test';
        lead.CPF_CNPJ__c = '47850135164';
        lead.Email='teste@test.com';
        lead.RecordTypeId = Utils.getRecordTypeId('Lead', 'DVR');
        lead.VehicleOfInterest__c = 'Clio';
        insert lead;

    }
    
     static testMethod void shouldWork(){

        Test.startTest();
         
        LMT_02_warmLeadController.buttonWarmLead(lead.Id);
        System.assert(LMT_02_warmLeadController.buttonWarmLead(lead.Id) == null);

        Test.stopTest();
         
     }

     static testMethod void shouldBeNotInterested(){

        Test.startTest();
         
        lead.Date_Last_Not_Interest__c = System.today();
        update lead;
         
        LMT_02_warmLeadController.buttonWarmLead(lead.Id);
        System.assert(LMT_02_warmLeadController.buttonWarmLead(lead.Id)!= null);

        Test.stopTest();
         
     }

     static testMethod void shouldBeHeating(){

        Test.startTest();
         
        lead.LastHeating__c = System.now();
        update lead;
         
        LMT_02_warmLeadController.buttonWarmLead(lead.Id);
        System.assert(LMT_02_warmLeadController.buttonWarmLead(lead.Id)!= null);

        Test.stopTest();
         
     }

}