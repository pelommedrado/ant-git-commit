@isTest
public class LMT_OrigensAccountTest {
    
    public static testMethod void testOrigens(){
        
        Test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        User user = moc.criaUser();
        user.ProfileId = [Select Id from Profile WHERE UserType = 'Standard' AND Id !=: Utils.getSystemAdminProfileId() limit 1].Id;
        Insert user;
        
        system.runAs(user){
            
            Lead lead = moc.CriaLead();
            lead.CPF_CNPJ__c = '38137428496';
            lead.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            Insert lead;
            
            Account persAcc = moc.criaPersAccount();
            persAcc.CustomerIdentificationNbr__c = '38137428496';
            Insert persAcc;
            
        }
        
        Test.stopTest();
        
    }

}