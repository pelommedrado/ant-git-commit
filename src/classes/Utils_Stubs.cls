Public class Utils_Stubs {

    // Order Stub
    public static WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VehicleStub() {
     
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse DummyResponse = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
        WS01_ApvGetDetVehXml.DetVehInfoMsg DummyVehInfoMsg = new WS01_ApvGetDetVehXml.DetVehInfoMsg();
        DummyVehInfoMsg.codeRetour = '0000';
        DummyVehInfoMsg.msgRetour = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        
        WS01_ApvGetDetVehXml.DetVeh DummyDetVeh = new WS01_ApvGetDetVehXml.DetVeh();
        DummyDetVeh.dateLiv = '22.07.2005';
        DummyDetVeh.dateTcmFab = '25.01.2005';
        DummyDetVeh.libConst='RENAULT';
        DummyDetVeh.libModel= 'LAGUNA II';
        DummyDetVeh.version='IP2 19DCI+';
        DummyDetVeh.libCarrosserie='BERLINE 5PRTES';
        DummyDetVeh.typeMot='F9Q';
        DummyDetVeh.NMot='C058210';
        DummyDetVeh.typeBoi='PK6';
        DummyDetVeh.NBoi='C012125';
        DummyDetVeh.indBoi='068';
        DummyDetVeh.NFab='S714313';
        DummyDetVeh.tvv='BGRG06';
        
                      
        DummyResponse.detVehInfoMsg = DummyVehInfoMsg;
        DummyResponse.detVeh = DummyDetVeh;         
        return DummyResponse;
    }    
    
    // Warranty Stub
    public static WS06_servicesBcsDfr.ListDetWarrantyCheck WarrantyStub() {
    
        WS06_servicesBcsDfr.ListDetWarrantyCheck DummyResponse = new WS06_servicesBcsDfr.ListDetWarrantyCheck();
        WS06_servicesBcsDfr.ArrayOfErrMsg DummyInfoMsg = new WS06_servicesBcsDfr.ArrayOfErrMsg();
        WS06_servicesBcsDfr.ErrMsg DummyMsg = new WS06_servicesBcsDfr.ErrMsg();
        
   //     DummyMsg.typeMsg = '0000';
   //     DummyMsg.zoneMsg = 'RECHERCHE VALIDE, VEHICULE TROUVE';
   //     DummyInfoMsg.ErrMsg.add(DummyMsg); 
        
        WS06_servicesBcsDfr.ListDetWarrantyCheck DummyDet = new WS06_servicesBcsDfr.ListDetWarrantyCheck();
        
        // Assistance warranty;
        WS06_servicesBcsDfr.ArrayOfAssistanceWarranty assArray =  new WS06_servicesBcsDfr.ArrayOfAssistanceWarranty();
        WS06_servicesBcsDfr.AssistanceWarranty[] ass =  new WS06_servicesBcsDfr.AssistanceWarranty[]{};
        WS06_servicesBcsDfr.AssistanceWarranty oneass =  new WS06_servicesBcsDfr.AssistanceWarranty();
        
        oneass.datFinGarASS = '12.01.6001';
        oneass.kmMaxGarASS = '98749';
        ass.add(oneass);
        assArray.AssistanceWarranty = ass;
        DummyDet.listAssWarranty = assArray;
        
        // BatteryWarranty ;
         WS06_servicesBcsDfr.ArrayOfBatteryWarranty bttArray =  new WS06_servicesBcsDfr.ArrayOfBatteryWarranty();
        WS06_servicesBcsDfr.BatteryWarranty[] btt =  new WS06_servicesBcsDfr.BatteryWarranty[]{};
        WS06_servicesBcsDfr.BatteryWarranty onebtt =  new WS06_servicesBcsDfr.BatteryWarranty();
        
        onebtt.datFinGarBTT = '12.01.3501';
        onebtt.kmMaxGarBTT = '97659';
        btt.add(onebtt);
        bttArray.BatteryWarranty = btt;
        DummyDet.listBttWarranty = bttArray;
        
        // CorrosionWarranty ;
        WS06_servicesBcsDfr.ArrayOfCorrosionWarranty corArray =  new WS06_servicesBcsDfr.ArrayOfCorrosionWarranty();
        WS06_servicesBcsDfr.CorrosionWarranty[] cor =  new WS06_servicesBcsDfr.CorrosionWarranty[]{};
        WS06_servicesBcsDfr.CorrosionWarranty onecor =  new WS06_servicesBcsDfr.CorrosionWarranty();
        
        onecor.datFinGarCOR = '12.01.3441';
        onecor.kmMaxGarCOR = '23999';
        cor.add(onecor);
        corArray.CorrosionWarranty = cor;
        DummyDet.listCorWarranty = corArray;
        
// ManufacturerWarranty ;
        WS06_servicesBcsDfr.ArrayOfManufacturerWarranty manArray =  new WS06_servicesBcsDfr.ArrayOfManufacturerWarranty();
        WS06_servicesBcsDfr.ManufacturerWarranty[] man =  new WS06_servicesBcsDfr.ManufacturerWarranty[]{};
        WS06_servicesBcsDfr.ManufacturerWarranty oneman =  new WS06_servicesBcsDfr.ManufacturerWarranty();
        
        oneman.datFinGarGAR = '12.01.3301';
        oneman.kmMaxGarGAR = '65999';
        man.add(oneman);
        manArray.ManufacturerWarranty = man;
        DummyDet.listGarWarranty = manArray;
                
// GME ;
        WS06_servicesBcsDfr.ArrayOfGMPeWarranty gmeArray =  new WS06_servicesBcsDfr.ArrayOfGMPeWarranty();
        WS06_servicesBcsDfr.GMPeWarranty[] gme =  new WS06_servicesBcsDfr.GMPeWarranty[]{};
        WS06_servicesBcsDfr.GMPeWarranty onegme =  new WS06_servicesBcsDfr.GMPeWarranty();
        
        onegme.datFinGarGME = '12.01.3201';
        onegme.kmMaxGarGME = '45999';
        gme.add(onegme);
        gmeArray.GMPeWarranty = gme;
        DummyDet.listGmeWarranty = gmeArray;
        
// PaintingWarranty ;
        WS06_servicesBcsDfr.ArrayOfPaintingWarranty turArray =  new WS06_servicesBcsDfr.ArrayOfPaintingWarranty();
        WS06_servicesBcsDfr.PaintingWarranty[] tur =  new WS06_servicesBcsDfr.PaintingWarranty[]{};
        WS06_servicesBcsDfr.PaintingWarranty onetur =  new WS06_servicesBcsDfr.PaintingWarranty();
        
        onetur.datFinGarTUR = '12.01.2101';
        onetur.kmMaxGarTUR = '99999';
        tur.add(onetur);
        turArray.PaintingWarranty = tur;
        DummyDet.listTurWarranty = turArray;

// CorrosionWarranty ;
        onecor =  new WS06_servicesBcsDfr.CorrosionWarranty();
        
        onecor.datFinGarCOR = '12.01.3441';
        onecor.kmMaxGarCOR = '23999';
        cor.add(onecor);
        corArray.CorrosionWarranty = cor;
        DummyDet.listCorWarranty = corArray;
                              
//        DummyResponse.GetInfoWarrantyCheckReturn.listErrMsg = DummyInfoMsg;
        DummyResponse  = DummyDet;    
        return DummyResponse;
    }    
    
    // OTS Stub
    public static WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse OTSStub() {
    
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse DummyResponse = new WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse();
        WS02_otsIcmApvBserviceRenault.OtsInfoMsg[] DummyArrayOtsInfoMsg = new WS02_otsIcmApvBserviceRenault.OtsInfoMsg[]{};
        WS02_otsIcmApvBserviceRenault.OtsInfoMsg DummyOtsInfoMsg = new WS02_otsIcmApvBserviceRenault.OtsInfoMsg();
        DummyOtsInfoMsg.code = '0000';
        DummyOtsInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummyArrayOtsInfoMsg.Add(DummyOtsInfoMsg);

        WS02_otsIcmApvBserviceRenault.Ots[] DummyArrayOtsDet = new WS02_otsIcmApvBserviceRenault.Ots[]{};
        WS02_otsIcmApvBserviceRenault.Ots DummyOtsDet = new WS02_otsIcmApvBserviceRenault.Ots();
        DummyOtsDet.libOts = 'Pose d\'une attache sur le dispositif de verrouillage du siege conducteur.';
        DummyOtsDet.numOts = '0B5X';
        DummyOtsDet.descOts='RENAULT a identifie un risque de non-verrouillage permanent ou par intermittence du siege conducteur, ce qui est susceptible de mettre en cause votre securite. L\'intervention consiste a poser une attache sur le dispositif de verrouillage du siege concerne.';
        DummyOtsDet.numNoteTech = '4818D';
        DummyArrayOtsDet.Add(DummyOtsDet);
        
        DummyOtsDet = new WS02_otsIcmApvBserviceRenault.Ots();
        DummyOtsDet.libOts = 'A second OTS because this can be a list';
        DummyOtsDet.numOts = '0B5X1';
        DummyOtsDet.descOts='RENAULT a identifie un risque de non-verrouillage permanent ou par intermittence du siege conducteur, ce qui est susceptible de mettre en cause votre securite. L\'intervention consiste a poser une attache sur le dispositif de verrouillage du siege concerne.';
        DummyOtsDet.numNoteTech = '1234D';
        DummyArrayOtsDet.Add(DummyOtsDet);
                      
        DummyResponse.apvGetListOtsMsg = DummyArrayOtsInfoMsg;
        DummyResponse.apvGetListOts = DummyArrayOtsDet;         
        return DummyResponse;
    }    
    
     // Maintenance Stub
    public static WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse MaintStub() {
    
        WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse DummyResponse = new WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse();
        WS03_fullRepApvBserviceRenault.InfoMsg DummyInfoMsg = new WS03_fullRepApvBserviceRenault.InfoMsg();
        DummyInfoMsg.codeMsg = '0000';
        DummyInfoMsg.libelleMsg = 'RECHERCHE VALIDE, VEHICULE TROUVE';
         
        WS03_fullRepApvBserviceRenault.PgmEnt DummyDet = new WS03_fullRepApvBserviceRenault.PgmEnt();
        DummyDet.libFix = new WS03_fullRepApvBserviceRenault.PgmEntLibFix[]{};
        
        WS03_fullRepApvBserviceRenault.PgmEntLibFix lbl = new WS03_fullRepApvBserviceRenault.PgmEntLibFix();
        WS03_fullRepApvBserviceRenault.PgmEntLibFix lbl2 = new WS03_fullRepApvBserviceRenault.PgmEntLibFix();
        WS03_fullRepApvBserviceRenault.PgmEntLibFix lbl3 = new WS03_fullRepApvBserviceRenault.PgmEntLibFix();
        WS03_fullRepApvBserviceRenault.PgmEntLibFix lbl4 = new WS03_fullRepApvBserviceRenault.PgmEntLibFix();
        
        lbl.numLib = '26';
        lbl.libelleFix = 'Le respect du programme d\'entretien, des périodicités et normes des produits et ingrédients est obligatoire pendant la période de garantie et s\'applique au pays de vente de votre véhicule. En dehors du pays d\'origine, veuillez consultez un représentant de la marque.';
        DummyDet.libFix.add(lbl);
        lbl2.numLib = '27';
        lbl2.libelleFix = 'OPERATIONS A REALISER AU PREMIER DES 2 TERMES ATTEINTS :';
        DummyDet.libFix.add(lbl2);
        lbl3.numLib = '28';
        lbl3.libelleFix = 'Les opérations suivantes doivent être effectuées pendant le service:';
        DummyDet.libFix.add(lbl3);
        lbl4.numLib = '43';
        lbl4.libelleFix = 'Important: Le fabricant recommande d\'adapter la fréquence de remplacement de certaines parties ou de liquides qui sont touchés par les conditions particulières d\'utilisation. Veuillez vous référer à la feuille jointe pour plus de détails';
        DummyDet.libFix.add(lbl4);
        
      
        WS03_fullRepApvBserviceRenault.PgmEntSecondary[] seclabels = new WS03_fullRepApvBserviceRenault.PgmEntSecondary[]{};
        
        WS03_fullRepApvBserviceRenault.PgmEntSecondary se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Anti-corrosion check';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the signalling and the exterior/interior lighting';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check and lubricate the bonnet lock';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition of the windscreen and door mirrors';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the exhaust pipe';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the wipers blades and screen washer fluid levels';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the levels, condition and sealing of the cooling circuit';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the presence of the airbag and engine compartment labels';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the levels, condition and sealing of the brake/clutch circuit';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the 12 V battery with the test tool';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition and sealing of the gaiters/rubber mountings/ball joints/shock absorbers';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the computers with the diagnostic tool';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition and pressure of the tyres and road wheel security';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the operation of the instrument panel warning lights';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the presence of the wheel valve caps';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'reinitialise the maintenance/oil service display';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the wear of the brake discs and paps';
        seclabels.add(se);
        
        se = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'documentation and positioning of the maintenance label';
        seclabels.add(se);     
        
    
        DummyDet.secondary = seclabels;
            
        //for (Integer s=0;s<10;s++){
        //    WS03_fullRepApvBserviceRenault.PgmEntSecondary seclabel = new WS03_fullRepApvBserviceRenault.PgmEntSecondary();
        //    seclabel.labelSecondary = 'Operation to be performed ' + s;
        //    seclabels.add(seclabel);
        //}
             
    
        WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary[] primaryOperations = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary[]{};
        
        WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Renault Service';        
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Drain the engine oil';
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the cabin filter';
        op.period_Distan_Toutes = '1';
        op.period_Km_Toutes = '20000';
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the oil filter';
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the brake fluid';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '4';
        
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the coolant';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '4';
        
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the accessories belt and rollers';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '6';
        
        primaryOperations.add(op);
        
        op = new WS03_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the timing belt and rollers';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '6';
        
        primaryOperations.add(op);
        
        
        
        DummyDet.merePrimary = primaryOperations;
        
        DummyResponse.InfoMsg = DummyInfoMsg;
        DummyResponse.PgmEnt = DummyDet;         
        return DummyResponse;
        
        
    }    
    
    // WarrantyHistory Stub
    public static WS05_ApvGetDonIran1.ApvGetDonIran1Response WarrantyHistoryStub() {
    
        WS05_ApvGetDonIran1.ApvGetDonIran1Response DummyResponse = new WS05_ApvGetDonIran1.ApvGetDonIran1Response();
        WS05_ApvGetDonIran1.DonIran1 DummydonIran1 = new WS05_ApvGetDonIran1.DonIran1();
        
        WS05_ApvGetDonIran1.DonIran1InfoMsg[] DummylistDonIran1InfoMsg = new WS05_ApvGetDonIran1.DonIran1InfoMsg[]{};
        WS05_ApvGetDonIran1.DonIran1InfoMsg DummyInfoMsg = new WS05_ApvGetDonIran1.DonIran1InfoMsg ();
        
        DummyInfoMsg.code = '0000';
        DummyInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummylistDonIran1InfoMsg.Add(DummyInfoMsg);

        WS05_ApvGetDonIran1.DonIran1Sortie[] DummyArraySortie = new WS05_ApvGetDonIran1.DonIran1Sortie[]{};
        WS05_ApvGetDonIran1.DonIran1Sortie DummyDet = new WS05_ApvGetDonIran1.DonIran1Sortie();
        
        
        DummyDet.catInt= 'R';
        DummyDet.datOuvOr= '07.09.2010';
        DummyDet.km= '55839';
        DummyDet.libInt= 'CLIMATISATION,YC. TABLEAU DE COMMANDE';
        DummyDet.libRc= 'BLOQUE,COMM.SANS EFFET,N\'ENCLENCHE,NE CHARGE PAS';
        DummyDet.numInt= '008131727301';
        DummyDet.numOts= 'H';
        DummyDet.rc= '7P';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '0';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '100';
        DummyDet.outModulo= '201005';
          
        DummyArraySortie.Add(DummyDet);
        DummyDet = new WS05_ApvGetDonIran1.DonIran1Sortie();
        
        DummyDet.catInt= 'A';
        DummyDet.datOuvOr= '06.09.2010';
        DummyDet.km= '56502';
        DummyDet.libInt= 'BOITE DE VITESSE TOUT TYPE';
        DummyDet.libRc= 'BLOQUE,COMM.SANS EFFET,N\'ENCLENCHE,NE CHARGE PAS';
        DummyDet.numInt= '008169731901';
        DummyDet.numOts= 'Y';
        DummyDet.rc= '7P';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '50';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '50';
        DummyDet.outModulo= '201006';
        DummyArraySortie.Add(DummyDet);
        
        DummyArraySortie.Add(DummyDet);
        DummyDet = new WS05_ApvGetDonIran1.DonIran1Sortie();
        
        DummyDet.catInt= 'A';
        DummyDet.datOuvOr= '13.11.2009';
        DummyDet.km= '39968';
        DummyDet.libInt= 'DIRECTION, COLONNE, BOITIER, ASSISTANCE';
        DummyDet.libRc= '';
        DummyDet.numInt= '007721893901';
        DummyDet.numOts= '0B1E';
        DummyDet.rc= '';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '100';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '0';
        DummyDet.outModulo= '201007';
        DummyArraySortie.Add(DummyDet);
        
        
        DummydonIran1.sortie = DummyArraySortie;
                      
        DummyResponse.listDonIran1InfoMsg= DummylistDonIran1InfoMsg;
        DummyResponse.donIran1 = DummydonIran1;         
        return DummyResponse;
    }    
    
      // Contract Stub
    public static WS04_CustdataCrmBserviceRenault.GetCustDataResponse ContractStub() {
    
        WS04_CustdataCrmBserviceRenault.GetCustDataResponse DummyResponse = new WS04_CustdataCrmBserviceRenault.GetCustDataResponse();
        // WS04_CustdataCrmBserviceRenault.OtsInfoMsg[] DummyArrayInfoMsg = new WS04_CustdataCrmBserviceRenault.InfoMsg[]{};
        // WS04_CustdataCrmBserviceRenault.OtsInfoMsg DummyInfoMsg = new WS04_CustdataCrmBserviceRenault.InfoMsg();
        
        DummyResponse.responseCode = '0000';
        
        
      //  Contract[]->Vehincle[]->PersonalInformation[]->Response
        WS04_CustdataCrmBserviceRenault.Contract DummyDet = new WS04_CustdataCrmBserviceRenault.Contract();
        
        DummyDet.idContrat = '09911249308';
//        DummyDet.type_x = '2321321321';
        DummyDet.productLabel = '43/12 - GARANTIE OR 12 MOIS';
//        DummyDet.initKm = '24,060';
        DummyDet.maxSubsKm = '524,000';
    //    DummyDet.subsDate = '2321321321';
 //       DummyDet.initContractDate = '06.03.2012';
  //      DummyDet.endContractDate = '05.03.2013';
        DummyDet.status = 'ACTIF';
        //DummyDet.updDate = '2321321321';
    //    DummyDet.idMandator = '2321321321';
        DummyDet.idCard = '8250992901649641';
        WS04_CustdataCrmBserviceRenault.Contract[] contractList = new WS04_CustdataCrmBserviceRenault.Contract[]{};
        contractList.Add(DummyDet);
        
        // Simulate 2 contracts being returned
        DummyDet = new WS04_CustdataCrmBserviceRenault.Contract();
        DummyDet.idContrat = '065656565';
//        DummyDet.type_x = '2321321321';
        DummyDet.productLabel = 'Some product ';
//        DummyDet.initKm = '24,060';
        DummyDet.maxSubsKm = '10,524,000';
    //    DummyDet.subsDate = '2321321321';
 //       DummyDet.initContractDate = '06.03.2012';
  //      DummyDet.endContractDate = '05.03.2013';
        DummyDet.status = 'PAS ACTIF';
        //DummyDet.updDate = '2321321321';
    //    DummyDet.idMandator = '2321321321';
        DummyDet.idCard = '54654654654';
        
        contractList.Add(DummyDet);
        
        WS04_CustdataCrmBserviceRenault.Vehicle vehicle = new WS04_CustdataCrmBserviceRenault.Vehicle();
        vehicle.contractList = contractList;
        WS04_CustdataCrmBserviceRenault.Vehicle[] vehicleList = new WS04_CustdataCrmBserviceRenault.Vehicle[]{};
        vehicleList.add(vehicle);
        WS04_CustdataCrmBserviceRenault.PersonnalInformation pi = new WS04_CustdataCrmBserviceRenault.PersonnalInformation();
        pi.firstName = 'LUC';
        pi.lastName = 'BUCHETON';
        WS04_CustdataCrmBserviceRenault.Address address = new WS04_CustdataCrmBserviceRenault.Address();
        address.strNum = '58';
        address.strName = 'Avenue GAILARDIN';
        address.city = 'MONTARGIS';
        address.zip = '45200';
        address.countryCode = 'FRA';
        pi.address = address;
        pi.vehicleList = vehicleList;
        WS04_CustdataCrmBserviceRenault.PersonnalInformation[] clientList = new WS04_CustdataCrmBserviceRenault.PersonnalInformation[]{};
        clientList.add(pi);
        DummyResponse.clientList = clientList;
        
                      
//        DummyResponse.apvGetListOtsMsg = DummyArrayOtsInfoMsg;
   //     DummyResponse.contractList = DummyContractList;         
        return DummyResponse;
    }    
    
    // WarrantyHistoryDetail Stub
    public static WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response WarrantyHistoryDetailStub() {
    
        WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response DummyResponse = new WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response();
        WS07_iran2BimIcmApvBserviceRenault.DonIran2 DummydonIran2 = new WS07_iran2BimIcmApvBserviceRenault.DonIran2();
        
        WS07_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg[] DummylistDonIran2InfoMsg = new WS07_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg[]{};
        WS07_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg DummyInfoMsg = new WS07_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg ();
        
        DummyInfoMsg.code = '0000';
        DummyInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummylistDonIran2InfoMsg.Add(DummyInfoMsg);

        //WS05_ApvGetDonIran1.DonIran2[] DummyArraySortie = new WS05_ApvGetDonIran1.DonIran1Sortie[]{};
        WS07_iran2BimIcmApvBserviceRenault.DonIran2 DummyDet = new WS07_iran2BimIcmApvBserviceRenault.DonIran2();
        
        
        DummyDet.constClient = 'R';
        DummyDet.diag = '07.09.2010';
        WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces[] pieces = new WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces[]{};
        WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces piece = new WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces();
        piece.qtePi = '1';
        piece.libePi = 'CONTROLE TECHNI';
        piece.refePidms = 'CT';
        pieces.add(piece);
        
        piece = new WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces();
        piece.qtePi = '3';
        piece.libePi = 'CONTROLE TECHNI STUFF';
        piece.refePidms = 'CT43234';
        pieces.add(piece);
        
        WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre[] mainOeuvres = new WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre[]{};
        WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre mainOeuvre = new WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre();
        mainOeuvre.libOpecode = 'CLIMATISATION,YC. TABLEAU DE COMMANDE';
        mainOeuvre.opecode = '5242';
        mainOeuvre.tpsMo = '0.3';
          
        mainOeuvres.add(mainOeuvre);
        mainOeuvre = new WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre();
        mainOeuvre.libOpecode = 'CLIMATISATION,YC. blah TABLEAU DE blah COMMANDE ';
        mainOeuvre.opecode = '9877';
        mainOeuvre.tpsMo = '0.9';  
        mainOeuvres.add(mainOeuvre);
        
        DummyDet.pieces = pieces;
        DummyDet.mainOeuvre = mainOeuvres;
        
        DummyResponse.listDonIran2InfoMsg= DummylistDonIran2InfoMsg;
        DummyResponse.donIran2 = DummyDet;         
        return DummyResponse;
    }    
    
}