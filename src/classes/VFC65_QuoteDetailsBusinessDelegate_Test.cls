/** 
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC65_QuoteDetailsBusinessDelegate_Test
{
	static testMethod void unitTest01()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                      IDBIR__c = '1065');
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id,
							                       Pricebook2Id = pb2.Id);
		insert opportunity1;

        Quote aQuote = new Quote (Name = 'QuoteNameTest_1',
        					 OpportunityId = opportunity1.Id,
        					 Pricebook2Id = pb2.Id);
        insert aQuote;
		
		 // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;

		//VehicleBooking__c vBooking = new VehicleBooking__c();
        //vBooking.Quote__c = aQuote.Id;
        //vBooking.Vehicle__c = v.Id;
        //vBooking.Status__c = 'Active';
        //insert vBooking;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
				                     FROM PricebookEntry 
				                     WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
				                     AND Pricebook2Id =: pb2.Id
				                     AND IsActive = true
				                     AND CurrencyIsoCode = 'BRL'
				                     limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = aQuote.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;

		VFC83_QuoteLineItemVO quoteLineItemVO = new VFC83_QuoteLineItemVO();
		quoteLineItemVO.Id = qLineItem.Id;
		quoteLineItemVO.unitPrice = 100;
		quoteLineItemVO.totalPrice = 1000;
		quoteLineItemVO.vehicleId = v.Id;
		quoteLineItemVO.vehicleName = 'Duster';
		quoteLineItemVO.isReserved = false;
		quoteLineItemVO.item = '';		
		quoteLineItemVO.description = 'Desc';
		quoteLineItemVO.unitPriceMask = '1000';

		VFC61_QuoteDetailsVO quoteDetailsVO = new VFC61_QuoteDetailsVO();
		quoteDetailsVO.Id = aQuote.Id;
		quoteDetailsVO.opportunityId = opportunity1.Id;
		quoteDetailsVO.sObjQuote = aQuote;
		quoteDetailsVO.entryMask = '1000';
		quoteDetailsVO.amountFinancedMask = '1000';
		quoteDetailsVO.priceUsedVehicleMask = '100';
		quoteDetailsVO.valueOfParcelMask = '100';
		quoteDetailsVO.expirationDate = Date.today() + 30;
		quoteDetailsVO.vehicleBookingId = v.Id;
		quoteDetailsVO.lstQuoteLineItemVO = new List<VFC83_QuoteLineItemVO>{quoteLineItemVO};
		//quoteDetailsVO.vehicleBookingId = vBooking.Id;
		quoteDetailsVO.idReservedVehicle = v.Id;

    	// Start Test
    	Test.startTest();

		VFC65_QuoteDetailsBusinessDelegate instance = VFC65_QuoteDetailsBusinessDelegate.getInstance();
		instance.getQuoteDetails(aQuote.Id, opportunity1.Id);
		instance.getQuoteLineItems(aQuote.Id);
		//instance.updateQuoteDetails(quoteDetailsVO);
		instance.reserveVehicle(quoteDetailsVO);
		instance.updateQuoteItemsDetails(new List<VFC83_QuoteLineItemVO>{quoteLineItemVO});
		instance.deleteQuoteLineItem(quoteDetailsVO, qLineItem.Id);

		// Stop Test
		Test.stopTest();
    }
	
	static testMethod void unitTest02()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                      IDBIR__c = '2065');
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id,
							                       Pricebook2Id = pb2.Id);
		insert opportunity1;

        Quote aQuote = new Quote (Name = 'QuoteNameTest_1',
        					 OpportunityId = opportunity1.Id,
        					 Pricebook2Id = pb2.Id,
        					 BalanceLastTwoYears__c = false);
        insert aQuote;
		
		 // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;

		VehicleBooking__c vBooking = new VehicleBooking__c();
        vBooking.Quote__c = aQuote.Id;
        vBooking.Vehicle__c = v.Id;
        vBooking.Status__c = 'Active';
        insert vBooking;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
				                     FROM PricebookEntry 
				                     WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
				                     AND Pricebook2Id =: pb2.Id
				                     AND IsActive = true
				                     AND CurrencyIsoCode = 'BRL'
				                     limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = aQuote.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;

		VFC83_QuoteLineItemVO quoteLineItemVO = new VFC83_QuoteLineItemVO();
		quoteLineItemVO.Id = qLineItem.Id;
		quoteLineItemVO.unitPrice = 100;
		quoteLineItemVO.totalPrice = 1000;
		quoteLineItemVO.vehicleId = v.Id;
		quoteLineItemVO.vehicleName = 'Duster';
		quoteLineItemVO.isReserved = false;
		quoteLineItemVO.item = '';		
		quoteLineItemVO.description = 'Desc';
		quoteLineItemVO.unitPriceMask = '1000';

		VFC61_QuoteDetailsVO quoteDetailsVO = new VFC61_QuoteDetailsVO();
		quoteDetailsVO.Id = aQuote.Id;
		quoteDetailsVO.opportunityId = opportunity1.Id;
		quoteDetailsVO.sObjQuote = aQuote;
		quoteDetailsVO.entryMask = '1000';
		quoteDetailsVO.amountFinancedMask = '1000';
		quoteDetailsVO.priceUsedVehicleMask = '100';
		quoteDetailsVO.valueOfParcelMask = '100';
		quoteDetailsVO.expirationDate = Date.today() + 30;
		quoteDetailsVO.vehicleBookingId = v.Id;
		quoteDetailsVO.lstQuoteLineItemVO = new List<VFC83_QuoteLineItemVO>{quoteLineItemVO};
		quoteDetailsVO.vehicleBookingId = vBooking.Id;
		quoteDetailsVO.idReservedVehicle = v.Id;

    	// Start Test
    	Test.startTest();

		VFC65_QuoteDetailsBusinessDelegate instance = VFC65_QuoteDetailsBusinessDelegate.getInstance();
		//instance.updateQuoteDetails(quoteDetailsVO);

		// Stop Test
		Test.stopTest();
    }

	static testMethod void unitTest03(){

    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [
        	select Id, SobjectType,DeveloperName, Name 
        	from RecordType 
        	where (
        		SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc')
    		)
		]){
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}

        Account account1 = new Account(
			Name = 'Account Test 01',
			RecordTypeId = recTypeIds.get('Network_Site_Acc'),
			IDBIR__c = '2065'
		);
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(
			Name = 'Opportunity Test 01',
			StageName = 'Identified',
			CloseDate = Date.today() + 30,
			OpportunitySource__c = 'NETWORK',
			OpportunitySubSource__c = 'THROUGH',
			AccountId = account1.Id,
			Pricebook2Id = pb2.Id,
			OwnerId = UserInfo.getUserId()
		);
		insert opportunity1;

        Quote aQuote = new Quote (
			Name = 'QuoteNameTest_1',
			OpportunityId = opportunity1.Id,
			Pricebook2Id = pb2.Id,
			Status = 'Billed',
			BalanceLastTwoYears__c = false
		);
        insert aQuote;
		
		// criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;

		VehicleBooking__c vBooking = new VehicleBooking__c();
        vBooking.Quote__c = aQuote.Id;
        vBooking.Vehicle__c = v.Id;
        vBooking.Status__c = 'Available';
        insert vBooking;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
				                     FROM PricebookEntry 
				                     WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
				                     AND Pricebook2Id =: pb2.Id
				                     AND IsActive = true
				                     AND CurrencyIsoCode = 'BRL'
				                     limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = aQuote.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;

        Test.startTest();

		VFC83_QuoteLineItemVO quoteLineItemVO = new VFC83_QuoteLineItemVO();
		quoteLineItemVO.Id = qLineItem.Id;
		quoteLineItemVO.unitPrice = 100;
		quoteLineItemVO.totalPrice = 1000;
		quoteLineItemVO.vehicleId = v.Id;
		quoteLineItemVO.vehicleName = 'Duster';
		quoteLineItemVO.isReserved = false;
		quoteLineItemVO.item = '';		
		quoteLineItemVO.description = 'Desc';
		quoteLineItemVO.unitPriceMask = '1000';

		VFC61_QuoteDetailsVO quoteDetailsVO = new VFC61_QuoteDetailsVO();
		quoteDetailsVO.Id = aQuote.Id;
		quoteDetailsVO.opportunityId = opportunity1.Id;
		quoteDetailsVO.sObjQuote = aQuote;
		quoteDetailsVO.entryMask = '1000';
		quoteDetailsVO.amountFinancedMask = '1000';
		quoteDetailsVO.priceUsedVehicleMask = '100';
		quoteDetailsVO.valueOfParcelMask = '100';
		quoteDetailsVO.expirationDate = Date.today() + 30;
		quoteDetailsVO.vehicleBookingId = v.Id;
		quoteDetailsVO.lstQuoteLineItemVO = new List<VFC83_QuoteLineItemVO>{quoteLineItemVO};
		quoteDetailsVO.vehicleBookingId = vBooking.Id;
		quoteDetailsVO.idReservedVehicle = v.Id;
		quoteDetailsVO.signalValueMask = 'R$10.00';
		quoteDetailsVO.accePrice1 = 'R$10.00';
		quoteDetailsVO.accePrice2 = 'R$10.00';
		quoteDetailsVO.accePrice3 = 'R$10.00';
		quoteDetailsVO.balanceLastTwoYears = true;
		quoteDetailsVO.cdcCFinancing = true;
		quoteDetailsVO.cpf = true;
		quoteDetailsVO.cpfAllMembers = true;
		quoteDetailsVO.driversLicense = true;
		quoteDetailsVO.identityCard = true;
		quoteDetailsVO.cpf = true;
		quoteDetailsVO.proofIncome = true;
		quoteDetailsVO.proofResidence = true;
		quoteDetailsVO.lastStatementIncomeTax = true;
		quoteDetailsVO.lastPaycheckStatement = true;
		quoteDetailsVO.lastStatementBank = true;
		quoteDetailsVO.driversLicense = true;
		quoteDetailsVO.socialContract = true;
		quoteDetailsVO.lastStatementIncomeTaxEnterprise = true;
		quoteDetailsVO.identityCardAllMembers = true;
		quoteDetailsVO.cpfAllMembers = true;
		quoteDetailsVO.registrationFormSignedPartners = true;
		quoteDetailsVO.lastBalanceSheet = true;
		quoteDetailsVO.balanceLastTwoYears = true;
		quoteDetailsVO.relationshipFleetDebt = true;
		quoteDetailsVO.dut = true;
		quoteDetailsVO.procuration = true;
		quoteDetailsVO.inspection = true;
		quoteDetailsVO.finesPaid = true;
		quoteDetailsVO.publicProcuration = true;
		quoteDetailsVO.licensing = true;
		quoteDetailsVO.leaseTermination = true;
		quoteDetailsVO.manualSpareKey = true;
		quoteDetailsVO.lsgFinancing = true;
		quoteDetailsVO.status = 'Billed';

		VFC65_QuoteDetailsBusinessDelegate instance = VFC65_QuoteDetailsBusinessDelegate.getInstance();
		instance.updateQuoteDetails(quoteDetailsVO);

		// Stop Test
		Test.stopTest();
    }
}