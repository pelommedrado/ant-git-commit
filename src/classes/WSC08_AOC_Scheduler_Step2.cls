/**
*   WebService Class    -   WSC08_AOC_Scheduler_Step2
*   Author              -   Suresh Babu
*   Date                -   20/03/2013
*   
*   #01 <Suresh Babu> <20/03/2013>
*       Scheduler class to initialize the Batch2 process for AOC.
**/

global class WSC08_AOC_Scheduler_Step2 implements Schedulable{
	global void execute(SchedulableContext ctx) {
		WSC07_AOC_Batch_Step2 b2 = new WSC07_AOC_Batch_Step2();
        Database.executeBatch(b2, 1);
	}
}