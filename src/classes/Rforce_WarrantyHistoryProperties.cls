public with sharing class Rforce_WarrantyHistoryProperties {
    
    public class WarrantyHistoryBasics {
        public String catInt {get;set;}
        public String datOuvOr {get;set;}
        public String km {get;set;}
        public String libInt {get;set;}
        public String libRc {get;set;}
        public String numInt {get;set;}
        public String numOts {get;set;}
        public String rc {get;set;}
        public String txPec1Entretien {get;set;}
        public String txPec1Incident {get;set;}
        public String txPec2 {get;set;}
        public String txPec3 {get;set;}
        public String txPec4 {get;set;}
        public String modulo {get;set;}
        public String histoBim {get;set;}
    
        // Part of detail but there is only one of them
        public String constClient {get;set;}
        public String diag {get;set;}

        public Rforce_WarrantyHistoryProperties.PiecesandOeuvres[] PandO {get;set;} 
    }
    
    public class PiecesandOeuvres {
        public String Code {get;set;}
        public String Temps {get;set;}
        public String Quantite {get;set;}
        public String Libelles {get;set;}
    }
}