@isTest
private class CAC_CaseUtilsNormalyTest {
  
  public static testMethod void WaitingReply(){
        CAC_CaseUtilsNormalyTest.createCustonSet();
        CAC_CaseUtilsNormaly caseUtil = new CAC_CaseUtilsNormaly();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste1@teste2.com',
                'Analyst'
            )
        ){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,lsOldCase);
            caseUtils.statusAnswersWaiting();
        }
        Database.insert(returnComentarioCaso(lsCaseId));
        
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Analyst'
            )
        ){
            CAC_CaseUtilsNormaly caseUtils3 = new CAC_CaseUtilsNormaly(lsCase,lsOldCase);
            caseUtils3.statusAnswersWaiting();
        }
        Test.stopTest();
    }

    public static testMethod void statusAnswers(){
        CAC_CaseUtilsNormalyTest.createCustonSet();
        CAC_CaseUtilsNormaly caseUtil = new CAC_CaseUtilsNormaly();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Analyst'
            )
        ){
            for(Case casea :lsCase){
                casea.Description = 'abc';
                casea.Status = 'Answered';
                casea.Answer__c ='slajlksjlkaj';
                lsCaseId.add(casea.ID);
            }
            CAC_CaseUtilsNormaly caseUtils3 = new CAC_CaseUtilsNormaly(lsCase,lsOldCase);
            caseUtils3.statusAnswers();
            
        }
        
        
    }
    
    public static testMethod void corretDayReturn(){
         List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        
        Long hours = 200;
        dateTime data;
         CAC_CaseUtilsNormaly caseUtils = new  CAC_CaseUtilsNormaly(lsCase,new Map<id,Case>(lsCase));
        data = caseUtils.corretDayReturn(hours,08,17);
    } 
    
    public static testMethod void AvailableRecordType(){
        CAC_CaseUtilsNormalyTest.createCustonSet();
        CAC_CaseUtilsNormaly caseUtil = new CAC_CaseUtilsNormaly();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste3@teste1.com',
                'Analyst'
            )
        ){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,lsOldCase);
            caseUtils.getAvailableRecordType();
        }
        
    }
     
    public static testMethod void updateField(){
        
        User usr = new User(
            Id = UserInfo.getUserId(),
            roleInCac__c = 'Dealer',
            isCac__c = true
        );
        update usr;
        
        CAC_CaseUtilsNormalyTest.createCustonSet();
        User user1 = CAC_CaseUtilsNormalyTest.createUser('a1dcqadasdsas@test.com','Dealer');
        
        //Testa Status New
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        for(Case caso: lsCase)
            caso.OwnerID = user1.Id;
        Update lsCase;

        Map<Id,Case> lsOldCase  = new Map<Id,Case>(lsCase);  
        CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,lsOldCase);
        caseUtils.updateField();
        
        //Testa Status Answered
        List<Case> lsCase2 = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> newListCase = new List<Case>();
        for(Case caso2: lsCase2){
            caso2.OwnerID = user1.Id;
            caso2.Status = 'Answered';
            newListCase.add(caso2);
        }

        Map<Id,Case> lsOldCase2  = new Map<Id,Case>(newListCase);  
        CAC_CaseUtilsNormaly caseUtils2 = new CAC_CaseUtilsNormaly(newListCase,lsOldCase2);
        caseUtils2.updateField();
        
        //Testa Status Under Analysis
        List<Case> lsCase3 = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> newListCase3 = new List<Case>();
        for(Case caso3: lsCase3){
            caso3.OwnerID = user1.Id;
            caso3.Status = 'Under Analysis';
            newListCase.add(caso3);
        }

        Map<Id,Case> lsOldCase3  = new Map<Id,Case>(newListCase);  
        CAC_CaseUtilsNormaly caseUtils3 = new CAC_CaseUtilsNormaly(newListCase,lsOldCase3);
        caseUtils3.updateField();
    } 
    
    public static testMethod void alterOwnerAnalyst(){
        
        User user = CAC_CaseUtilsNormalyTest.createUser('a1asdapiopias@test.com','Analyst');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        for(Case caase : lsCase){
            caase.Status = 'Under Analysis';
            lsNewCase.add(caase);
        }
        
        System.runAs(user){
            CAC_CaseUtilsNormaly.alterOwner(lsNewCase, oldCaseMap);
        }
    }
    
    public static testMethod void alterOwnerDealer(){
        
        User user = new User(
            Id = UserInfo.getUserId(),
            roleInCac__c = 'Dealer',
            isCac__c = true
        );
        update user;
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        for(Case caase : lsCase){
            lsNewCase.add(caase);
        }
        
        CAC_CaseUtilsNormaly.alterOwner(lsNewCase, oldCaseMap);

    }
    
    public static testMethod void blockStatusError1(){
        User user = CAC_CaseUtilsNormalyTest.createUser('a1asdapiopias@test.com',Utils.getProfileId('CAC - Full'));
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        System.runAs(user){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }   
    }
    
    public static testMethod void blockStatusError2(){
        User user = CAC_CaseUtilsNormalyTest.createUser('a1asdapiopias@test.com','Analyst');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'New';
        
        System.runAs(user){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }    
    }
    
    public static testMethod void blockStatusError3(){
        User user = CAC_CaseUtilsNormalyTest.createUser('a1asdapiopias@test.com','Analyst');
        List<Case> lsCase;
        
        
        lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        System.runAs(user){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
    }  
    
    public static testMethod void blockStatusError4(){
        User user = CAC_CaseUtilsNormalyTest.createUser('a1asdapiopias@test.com','Dealer');
        List<Case> lsCase;
        
        
        lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Waiting Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);

        System.runAs(user){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
    }
    
    public static List<CaseComment> returnComentarioCaso(List<Id> lsCaseId){
        List<CaseComment>lsCaseComment = new List<CaseComment>();
        for(Id caseId : lsCaseId){
            CaseComment comenta = new CaseComment(
                CommentBody = 'asdasdadadsadadsadasdasdasd',
                IsPublished = true,
                ParentId = caseId
            );
            lsCaseComment.add(comenta);
        }
        
        return lsCaseComment;
    }
    
    public static List<Case> returnNewCase(Id recordType){
        List<Case>lsCase = new List<Case>();
        //for(integer i=0; i<30; i++){
            Case cas = new Case(
                Status = 'New',
                Subject__c = 'Diferido (Previsão de Peças)',
                Description = 'teste',
                Dealer_Contact_Name__c = 'teste',
                Contact_Phone__c  = '1122334455',
                Reference__c = 'teste',
                Order_Number__c = '1234567',
                RecordTypeId = recordType
            );
            lsCase.add(cas);
        //}
        Database.insert(lsCase,true);
        return lsCase;   
    }

    public static User createUser(String userName,String typeUserCac){
        Id profileId = Utils.getSystemAdminProfileId();
        String aliasName =  userName;
        if(aliasName.length()>=8)
            aliasName = aliasName.substring(0,7);
        User manager = new User(
            FirstName = aliasName,
            LastName = 'User',
            BypassVR__c = false,
            Email = 'test@org.com',
            Username = userName,
            Alias = userName.substring(0,7),
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = aliasName,
            ProfileId = profileId,
            BIR__c ='123ABC123',
            IsActive = true,
            isCac__c = true,
            roleInCac__c = typeUserCac
        );
        Database.insert( manager,true );
        return manager;
    }
    
    public static void createCustonSet(){
        cacweb_Mapping__c custonSet = new cacweb_Mapping__c(
            name = 'Cacweb_PartsWarehouse',
            Department__c = 'Peças',
            Departamento__c = 'Peças',
            DirectionInEnglish__c = 'Parts',
            Name_Queue__c= 'Cacweb_PartsWarehouse',
            Record_Type_Name__c = 'Cacweb_PartsWarehouse'
        );
        Database.insert(custonSet);
        
        cacweb_settingTime__c custon2 = new cacweb_settingTime__c(
            name = 'Diferido',
            alertExpire__c = 20,
            subject__c = 'Diferido (Previsão de Peças)',
            FinishHourAttendance__c = 17,
            StartHourAttendance__c = 08,
            hoursExpire__c = 500
        );
        Database.insert(custon2);
    }
}