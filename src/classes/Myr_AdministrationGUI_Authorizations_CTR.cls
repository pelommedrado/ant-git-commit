/**
* @author sebastien.ducamp@atos
* @description This class is used to manage the custom setting Country_Info__c without asking repeatedly for ARPE.
* This class automatically display all the MYRrenault fields available for this custom field and the associated description.
* So, the description shoul be clear and comprehensible for everybody
* version 1.0: 16.07 / Sébastien DUCAMP / initial revision
*/
public class Myr_AdministrationGUI_Authorizations_CTR { 

    /* Member variables **/
    public List<AuthWrapper> AuthorizationsSettings {get; set;}
    public List<String> GlobalFields {get; set;}
    public String SelectedAuthorization {get; set;}
    //command the panel display
    public Boolean EditMode {get; set;}
    public Boolean AddMode {get; set;}
    //new authorization
    public String SelectedLocation {get; set;}
    public Myr_AdministrationGUI_Proxify_CLS.ProxySetting NewAuth {get; private set;}
	
	public List<SelectOption> SelectProfiles {get; private set;}
	public String SelectedProfile {get; set;}
	public VEH_Veh__c tekObject {get; set;} //technical wrapper, just used to get a user


    public class AuthWrapper {
        public Myr_AdministrationGUI_Proxify_CLS.ProxySetting proxy {get; set;}
        public CS04_MYRAuthorization_Settings__c auth {get; set;}
    }

    /* @contructor **/
    public Myr_AdministrationGUI_Authorizations_CTR() {
        getAuthData();
    }

    /* Query the data from Authorization settings **/
    private void getAuthData() {
        EditMode = false;
        AddMode = false;
        Myr_MyRenaultTools.SelectAll queryAll = Myr_MyRenaultTools.buildSelectAllQuery( CS04_MYRAuthorization_Settings__c.class.getName(), '__c' );
        String query = queryAll.Query.removeEndIgnoreCase( 'FROM ' + CS04_MYRAuthorization_Settings__c.class.getName() );
        query += ', Id, Name, SetupOwnerId, SetupOwner.Name, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate ';
        query += 'FROM ' + CS04_MYRAuthorization_Settings__c.class.getName();
		query += ' ORDER BY SetupOwner.Name';
        List<CS04_MYRAuthorization_Settings__c> InnerSettings = Database.query( query );
        GlobalFields = queryAll.Fields;
        AuthorizationsSettings = new List<AuthWrapper>();
        for( CS04_MYRAuthorization_Settings__c auth : InnerSettings ) {
            AuthWrapper wrapper = new AuthWrapper();
            wrapper.auth = auth;
            wrapper.proxy = Myr_AdministrationGUI_Proxify_CLS.proxify( auth, GlobalFields, CS04_MYRAuthorization_Settings__c.class.getName() );
            AuthorizationsSettings.add( wrapper );
        }
    }

    /* Switch into Add Mode **/
    public void add() {
		List<Profile> listProfiles = [SELECT Id, Name FROM Profile ORDER BY Name];
		SelectProfiles = new List<SelectOption>();
		for( Profile prof : listProfiles ) {
			SelectProfiles.add( new SelectOption(prof.Id, prof.Name ) );
		}
		tekObject = new VEH_Veh__c();

        CS04_MYRAuthorization_Settings__c auth = new CS04_MYRAuthorization_Settings__c();
        NewAuth = Myr_AdministrationGUI_Proxify_CLS.proxify( auth, GlobalFields, CS04_MYRAuthorization_Settings__c.class.getName() );
        AddMode = true;
    }

    /* Insert the new authorization within SalesForce **/
    public void addAuthorization() {
        //Add the new authorization
        CS04_MYRAuthorization_Settings__c auth = null;
        try {
            system.debug('### Myr_AdministrationGUI_Authorizations_CTR - <addAuthorization> - NewAuth=' + NewAuth );
            auth = (CS04_MYRAuthorization_Settings__c) NewAuth.copySettings();
            if( 'Profile'.equalsIgnoreCase( SelectedLocation ) ) {
				auth.SetupOwnerId = SelectedProfile;
            } else if( 'User'.equalsIgnoreCase( SelectedLocation ) ) {
				auth.SetupOwnerId = tekObject.OwnerId;
            }
            system.debug('### Myr_AdministrationGUI_Authorizations_CTR - <addAuthorization> - auth=' + auth );
            insert auth;
            AddMode = false;
            getAuthData();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, system.Label.Myr_GUI_Auth_AddSucess ) );
        } catch (Exception e) {
			String message = String.format(system.Label.Myr_GUI_Auth_AddFailed, new List<String>{e.getMessage()});
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, message ) );
            system.debug('### Myr_AdministrationGUI_Authorizations_CTR - <addAuthorization> - exception stack=' + e.getStackTraceString() );
        }
        //Log the information
        try {
            INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
                                    , Myr_AdministrationGUI_Authorizations_CTR.class.getName()
                                    , 'Add Authorization'
                                    , auth.Id
                                    , 'OK'
                                    , 'New Authorization=' + auth
                                    , INDUS_Logger_CLS.ErrorLevel.Info );
        } catch (Exception e) {
            //Do nothing: do not interrompt the treatment because of a logging problem
        }
    }

    /* Clarify the logs for authorization **/
    public class LogModification {
        public String name;
        public List<Myr_AdministrationGUI_Proxify_CLS.Modify> Modifications;
        public LogModification( String n, List<Myr_AdministrationGUI_Proxify_CLS.Modify> l ) {
            name = n;
            Modifications = l;
        }
    }

    /* Save the current modifications **/
    public void save() {
        //Information modified
        system.debug('### Myr_AdministrationGUI_Authorizations_CTR - <save> - BEGIN AuthorizationsSettings='+AuthorizationsSettings);
        List<CS04_MYRAuthorization_Settings__c> listAuth = new List<CS04_MYRAuthorization_Settings__c>();
        List<LogModification> listModif = new List<LogModification>();
        try {
            for( AuthWrapper wrapper: AuthorizationsSettings ) {
                CS04_MYRAuthorization_Settings__c OldObject = wrapper.auth;
                CS04_MYRAuthorization_Settings__c auth = (CS04_MYRAuthorization_Settings__c) wrapper.proxy.copySettings();
                if( wrapper.proxy.hasBeenModified() ) {
                    listAuth.add( auth );
                    listModif.add( new LogModification(OldObject.SetupOwner.Name, wrapper.proxy.getModifications() ) );
                }
            }
            update listAuth;
            getAuthData();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, system.Label.Myr_GUI_Auth_SaveSuccess ) );
        } catch (Exception e) {
			String message = String.format(system.Label.Myr_GUI_Auth_SaveFailed, new List<String>{e.getMessage()});
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, message + e.getMessage() ) );
        }
        //Log the information
        try {
            INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
                                    , Myr_AdministrationGUI_Authorizations_CTR.class.getName()
                                    , 'Save Authorizations'
                                    , ''
                                    , 'OK'
                                    , 'List of Modifications: ' + listModif
                                    , INDUS_Logger_CLS.ErrorLevel.Info );
        } catch (Exception e) {
            //Do nothing: do not interrompt the treatment because of a logging problem
        }
    }

    /* Cancel the current modifications **/
    public void cancel() {
        getAuthData();
        EditMode = false;
        AddMode = false;
    }

    /* Switch in Edit mode **/
    public void edit() {
        EditMode = true;
        AddMode = false;
    }

    /* Delete the selected authorization **/
    public void deleteAuthorization() {
        CS04_MYRAuthorization_Settings__c setting = null;
        //Delete
        try {
            for( AuthWrapper wrapper : AuthorizationsSettings ) {
                if( SelectedAuthorization == wrapper.auth.Id ) {
                    setting = wrapper.auth;
                }
            }
            delete setting;
            getAuthData();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, system.Label.Myr_GUI_Auth_DeleteSuccess ) );
        } catch (Exception e) {
            String message = String.format(system.Label.Myr_GUI_Auth_DeleteFailed, new List<String>{e.getMessage()});
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, message ) );
        }
        //Log the information
        try {
            INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
                                    , Myr_AdministrationGUI_Authorizations_CTR.class.getName()
                                    , 'Delete Authorization'
                                    , ''
                                    , 'OK'
                                    , 'Deletion = Name:' + setting.SetupOwner.Name + '\r\ndetails:' + setting
                                    , INDUS_Logger_CLS.ErrorLevel.Info );
        } catch (Exception e) {
            //Do nothing: do not interrompt the treatment because of a logging problem
        }
    }

    /* @return the list of possible locations **/
    public List<SelectOption> getLocations() {
        List<SelectOption> locations = new List<SelectOption>();
        locations.add( new SelectOption('', '') );
        locations.add( new SelectOption('Profile', 'Profile') );
        locations.add( new SelectOption('User', 'User') );
        return locations;
    }

}