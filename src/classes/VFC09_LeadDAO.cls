public with sharing class VFC09_LeadDAO extends VFC01_SObjectDAO {
    
    private static final VFC09_LeadDAO instance = new VFC09_LeadDAO();
    
    private VFC09_LeadDAO(){}
    
    public static VFC09_LeadDAO getInstance(){
        return instance;
    }
    
    /** 
* This Method was used to get Lead Record based on leadId 
* @param leadId - This field using get lead details by lead Id
* @return leadRecordById - fetch and return the result in leadRecordById list
*/
    public Lead findLeadByLeadId(Id leadId) {
        
        Lead leadRecordById = 
            [SELECT l.Website,l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c, l.Detail__c, l.PromoCode__c,
             l.VehicleOfInterest__c, l.Vehicle_of_Interest_Formula__c, l.VehicleOfCampaign__c, l.ValueOfCredit__c,  l.Company, 
             l.VIN__c, l.TypeOfInterest__c, l.Title, l.TestDriveModel__c, l.TestDriveDone__c, l.Sub_Detail__c,l.City, 
             l.TestDriveDate__c, l.SystemModstamp, l.SubSource__c, l.Street, l.Status, l.State, l.RefAddressCountry__c, 
             l.StartDateOfOwnership__c, l.SellerDealer__c, l.SecondVehicleOfInterest__c, l.Salutation, l.RefAddressState__c, 
             l.SMSUpdated__c, l.SMSDateUpdated__c, l.RecordTypeId, l.RecentBirthdate__c, l.Rating, l.IdOffer__c,l.CPF_CNPJ__c,
             l.RFV__c, l.PropensityModel__c, l.PreApprovedCredit__c, l.PostalCode, l.PhoneNumberOK__c, l.RefAddressCity__c, 
             l.Phone, l.PersonalMobilePhoneUpdated__c, l.PersonalMobilePhoneDateUpdated__c, VehicleOfInterestLookup__c,
             l.PersonalEmailUpdated__c, l.PersonalEmailDateUpdated__c, l.ParticipatedInCampaigns__c, l.RefAddressPostalCode__c, 
             l.ParticipateOnOPA__c, l.OwnerId, l.OtherInformation__c, l.OptinSMS__c, l.OptinPhone__c,  l.RefAddressStreet__c, 
             l.OptinEmail__c, l.OptInDealer__c, l.NumberOfVehiclesInTheFleet__c, l.Owner.type, l.Account__c,l.isResAddressRef__c,
             l.NumberOfRenaultVehiclesInTheFleet__c, l.NumberOfEmployees, l.Name, l.NUMDBM__c, l.AddressDateUpdated__c,
             l.NOfVehiclesOfTheIntendsPurchase__c, l.MobilePhone, l.MasterRecordId, l.MarketdatasCluster__c, l.AddressOK__c, 
             l.LeadSource, l.LastTransferDate, l.LastName, l.LastModifiedDate, l.LastModifiedById, l.AddressUpdated__c, 
             l.LastActivityDate, l.JigsawContactId, l.Jigsaw, l.IsVehicleOwner__c, l.IsUnreadByOwner, l.AfterSalesOffer__c,
             l.IsRenaultVehicleOwner__c, l.IsDeleted, l.IsConverted, l.IntentToPurchaseNewVehicle__c, l.AfterSales__c, 
             l.InformationUpdatedInTheLast5Years__c, l.Industry, l.Id, l.HomePhone__c, l.HomePhoneUpdated__c, l.AnnualRevenue, 
             l.HomePhoneDateUpdated__c, l.HaveOwnFleet__c, l.HasOptedOutOfFax, l.HasOptedOutOfEmail, l.AverageRenewalPeriod__c, 
             l.FleetProvider__c, l.FirstName, l.Fax, l.Event__c, l.EndDateOfOwnership__c, l.EndDateOfFinanciating__c, 
             l.EmailOK__c, l.EmailBouncedReason, l.EmailBouncedDate, l.Email, l.DoNotCall, l.Description, l.BehaviorCustomer__c, 
             l.DealerOfInterest__c, l.DateOfContemplation__c, l.CustomerProfile__c, NonEffectiveContact__c, Type_DVE__c,
             l.CurrentMilage__c, l.CreatedDate, l.CreatedById, l.Country, l.ConvertedOpportunityId, l.ConvertedDate,l.IdDealer__c, 
             l.ConvertedContactId, l.ConvertedAccountId, l.ContemplationLetter__c, SecondVehicleOfInterestLookup__c, l.CNPJ__c,
             l.CadastralCustomer__c, l.CRV_CurrentVehicle__c, l.BusinessPhone__c, l.Birthdate__c,l.Offer__c, l.SFAConverted__c,
             l.BusinessPhoneUpdated__c,l.BusinessPhoneDateUpdated__c,l.BusinessMobilePhone__c,l.BusinessMobilePhoneDateUpdated__c, 
             l.BusinessMobilePhoneUpdated__c, l.DealerOfInterest__r.OwnerId, l.Last_date_to_be_confirmed__c, 
             l.Transactions_to_be_confirmed__c, l.Indicated_by__c, l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, 
             l.City__c, l.State__c, l.Country__c,l.PostalCode__c, l.RefAddressNumber__c, l.RefAddressComplement__c, 
             l.RefAddressNeighborhood__c, l.SemiclairModel__c, l.SemiclairVersion__c, l.SemiclairHarmony__c, l.SemiclairOptional__c, 
             l.SemiclairUpholstered__c, l.SemiclairColor__c, l.TransactionCode__c, l.RgStateTexto__c, l.Sex__c, l.MaritalStatus__c
             FROM Lead l
             WHERE l.Id =: leadId];
        
        return leadRecordById;
    }
    
    /**
*
*/    
    public List<Lead> fetchLeadsByQueryString(String leadQuery) {
        List<Lead> lstLeadRecordsUsingCondition = Database.query(leadQuery);
        return lstLeadRecordsUsingCondition;
    }
    
    /**
*
*/
    public List<Lead> fetchLeadUsingNameOrCPF(String firstName, String lastName, String cpf) {
        
        List<Lead> lstLeadRecordsUsingCondition = 
            [SELECT l.Id, l.Name, l.Account__c, l.CustomerProfile__c, l.Customer_External_Key__c, l.Form_ID__c, 
             l.Internet_Support__c, l.Street, l.RecentBirthdate__c, l.TestDriveDate__c, l.TestDriveDone__c, l.TestDriveModel__c, 
             l.City, l.State, l.Vehicle_of_Interest_Formula__c, l.PostalCode, l.Country, l.FirstName, l.LastName, l.MobilePhone,
             l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c,
             l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
             l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
             l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
             l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.HomePhoneDateUpdated__c,
             l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,
             l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c, l.PersonalMobilePhoneDateUpdated__c,
             l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c,
             l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource, l.SubSource__c,
             l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c,l.isResAddressRef__c, l.RefAddressStreet__c, l.RefAddressCity__c, 
             l.RefAddressPostalCode__c, l.RefAddressState__c, l.RefAddressCountry__c, VehicleOfInterestLookup__c, 
             l.SecondVehicleOfInterestLookup__c, l.IdOffer__c, l.PromoCode__c, l.IdDealer__c, l.Offer__c, NonEffectiveContact__c,
             l.OptInDealer__c, l.Event__c, l.OtherInformation__c, l.ParticipatedInCampaigns__c, l.OptinEmail__c, l.OptinPhone__c,
             l.OptinSMS__c, l.TypeOfInterest__c, l.VehicleOfCampaign__c,  l.Detail__c, l.Sub_Detail__c, l.OwnerId, l.Owner.type,
             l.RFV__c, l.SecondVehicleOfInterest__c, l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, l.City__c, l.State__c, l.Country__c, l.PostalCode__c,
             l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c
             FROM Lead l
             WHERE l.IsConverted = false
             AND (l.FirstName =: firstName OR l.LastName =: lastName OR l.CPF_CNPJ__c =: cpf)];
        return lstLeadRecordsUsingCondition;
    }
    
    /**
*
*/
    public Id fetchLeadUsingByAccoundRecord(String firstName, String lastName) {
        try {
            Lead lstLeadRecordsUsingCondition = 
                [SELECT l.Id, l.Name, l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c, l.Street, l.City, l.State,
                 l.Vehicle_of_Interest_Formula__c, l.PostalCode, l.Country, l.FirstName, l.LastName, l.MobilePhone,
                 l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c,
                 l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
                 l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
                 l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
                 l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.IdOffer__c,
                 l.HomePhoneDateUpdated__c,l.Offer__c, NonEffectiveContact__c, l.PromoCode__c, l.IdDealer__c, 
                 l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,
                 l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c,VehicleOfInterestLookup__c, 
                 l.PersonalMobilePhoneDateUpdated__c,SecondVehicleOfInterestLookup__c, l.RefAddressCity__c, 
                 l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c,
                 l.RecentBirthdate__c,l.RefAddressCountry__c, l.RefAddressPostalCode__c, l.RefAddressState__c, 
                 l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource, l.SubSource__c,
                 l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c,l.isResAddressRef__c, l.RefAddressStreet__c, l.Street__c, l.Number__c, l.Complement__c, 
                 l.Neighborhood__C, l.City__c, l.State__c, l.Country__c, l.PostalCode__c, l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c
                 FROM Lead l
                 WHERE l.IsConverted = false
                 AND (l.FirstName =: firstName AND l.LastName =: lastName) 
                 limit 1];
            
            return lstLeadRecordsUsingCondition.Id;
        }
        catch(QueryException ex) {
            return null;
        }
    }
    
    /**
*
*/
    public Id fetchLeadUsingByAccoundId(Id AccountId) {
        try {
            Lead lstLeadRecordsUsingCondition = 
                [SELECT l.Id, l.Name, l.Account__c
                 FROM Lead l
                 WHERE l.IsConverted = false
                 AND l.Account__c =: AccountId 
                 limit 1];
            
            return lstLeadRecordsUsingCondition.Id;
        }
        catch(QueryException ex) {
            return null;
        }
    }
    
    /**
*
*/
    public Map<Id, Lead> returnMapWithIdAndLeadRecord(Set<String> set_ImportedIDs) {
        Map<Id, Lead> mapLeadsWithID = new Map<Id, Lead>(
            [SELECT l.Id, l.Description, l.Type_DVE__c, l.recordTypeId, l.Name, l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c,
             l.Detail__c, l.Sub_Detail__c, l.Street, l.City, l.State, l.PostalCode, l.Vehicle_of_Interest_Formula__c, l.Country,
             l.FirstName, l.LastName, l.MobilePhone, l.PromoCode__c, l.IdDealer__c, l.Offer__c, l.Owner.type, l.OwnerId,  
             l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c, l.IdOffer__c,
             l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
             l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
             l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
             l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.HomePhoneDateUpdated__c,
             l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,l.RFV__c,
             l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c, l.PersonalMobilePhoneDateUpdated__c,
             l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c, l.Event__c, 
             l.RecentBirthdate__c,l.VehicleOfCampaign__c, l.TestDriveModel__c, l.TypeOfInterest__c, l.OptinSMS__c, 
             l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource, l.SubSource__c,
             l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c, l.Account__c, l.isResAddressRef__c, l.RefAddressStreet__c,
             l.RefAddressCity__c, l.TestDriveDate__c, l.TestDriveDone__c,l.SecondVehicleOfInterest__c, l.OptinPhone__c, 
             l.RefAddressPostalCode__c, l.RefAddressState__c, l.RefAddressCountry__c, l.VehicleOfInterestLookup__c, 
             l.SecondVehicleOfInterestLookup__c, l.NonEffectiveContact__c, l.CustomerProfile__c, l.OptInDealer__c,
             l.OtherInformation__c, l.ParticipatedInCampaigns__c, l.OptinEmail__c, l.Home_Phone_Web__c,l.Mobile_Phone__c, l.VehicleRegistrNbr__c,
             l.ProposalScheduleDate__c, l.ProposalScheduleHour__c, l.ServiceType__c, l.ScheduledPeriod__c, l.BIR_Dealer_of_Interest__c, 
             l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, l.City__c, l.State__c, l.Country__c, l.PostalCode__c,
             l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c,l.SemiclairModel__c, l.SemiclairVersion__c,
             l.SemiclairHarmony__c, l.SemiclairOptional__c, l.SemiclairUpholstered__c, l.SemiclairColor__c, l.TransactionCode__c, 
             l.RgStateTexto__c, l.Sex__c, l.MaritalStatus__c, l.OptinWhatsApp__c
             FROM Lead l
             WHERE l.IsConverted = False
             AND l.Id IN : set_ImportedIDs]);
        return mapLeadsWithID;
    }
    
    /**
*
*/
    public Map<Decimal, Lead> returnLeadRecordsUsingNUMDBMField(Set<Decimal> set_ImportedNUMDBM) {
        Map<Decimal, Lead> map_LeadWithNUMDBM = new Map<Decimal, Lead>();
        
        List<Lead> lsLeadRecords = 
            [SELECT l.Id, l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c, l.Name, l.Street, l.City, l.State, 
             l.PostalCode, l.Vehicle_of_Interest_Formula__c, l.Country, l.FirstName, l.LastName, l.MobilePhone,
             l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c,
             l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
             l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
             l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
             l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.HomePhoneDateUpdated__c,
             l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,
             l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c, l.PersonalMobilePhoneDateUpdated__c,
             l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c, 
             l.RecentBirthdate__c, l.SecondVehicleOfInterestLookup__c,l.RefAddressCountry__c, l.VehicleOfInterestLookup__c, 
             l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource, l.SubSource__c,
             l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c, l.Account__c, l.isResAddressRef__c, l.RefAddressStreet__c, 
             l.RefAddressCity__c, l.PromoCode__c, l.IdDealer__c, l.Offer__c, NonEffectiveContact__c, l.IdOffer__c, 
             l.RefAddressPostalCode__c, l.RefAddressState__c, l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, l.City__c, l.State__c, l.Country__c,
             l.PostalCode__c, l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c
             FROM Lead l
             WHERE l.IsConverted = false
             AND l.NUMDBM__c IN : set_ImportedNUMDBM];
        if (lsLeadRecords.size() > 0) {
            for (Lead leadRecord : lsLeadRecords) {
                map_LeadWithNUMDBM.put(leadRecord.NUMDBM__c , leadRecord);
            }
        }
        return map_LeadWithNUMDBM;
    }
    
    /**
*
*/
    public Map<String, List<Lead>> returnLeadsUsingCPF_CNPJ(Set<String> set_ImportedCPFCNPJ) {
        Map<String, List<Lead>> mapLeadWithCPFCNPJ = new Map<String, List<Lead>>();
        
        List<Lead> lstLeadRecords = 
            [SELECT l.Id, l.Name, l.Street, l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c, l.City, l.State, 
             l.PostalCode, l.Vehicle_of_Interest_Formula__c,  l.Country, l.FirstName, l.LastName, l.MobilePhone,
             l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c,
             l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
             l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
             l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
             l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.HomePhoneDateUpdated__c,
             l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,
             l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c, l.PersonalMobilePhoneDateUpdated__c,
             l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c, 
             l.RecentBirthdate__c,SecondVehicleOfInterestLookup__c,l.RefAddressCountry__c, VehicleOfInterestLookup__c,  
             l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource, l.SubSource__c,
             l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c, l.Account__c, l.isResAddressRef__c, l.RefAddressStreet__c, 
             l.RefAddressCity__c, l.IdOffer__c, l.PromoCode__c, l.IdDealer__c, l.Offer__c, NonEffectiveContact__c,
             l.RefAddressPostalCode__c, l.RefAddressState__c, l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, l.City__c, l.State__c, l.Country__c,
             l.PostalCode__c, l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c
             FROM Lead l
             WHERE l.IsConverted = False
             AND l.CPF_CNPJ__c IN : set_ImportedCPFCNPJ];
        
        if (lstLeadRecords.size() > 0) {
            
            for (Lead leadRecord : lstLeadRecords) {
                List<Lead> lstTempLeads = new List<Lead>();
                if (mapLeadWithCPFCNPJ.containsKey(leadRecord.CPF_CNPJ__c)) {
                    lstTempLeads = mapLeadWithCPFCNPJ.get(leadRecord.CPF_CNPJ__c);
                    lstTempLeads.add(leadRecord);
                    mapLeadWithCPFCNPJ.put(leadRecord.CPF_CNPJ__c, lstTempLeads);
                }
                else {
                    lstTempLeads.add(leadRecord);
                    mapLeadWithCPFCNPJ.put(leadRecord.CPF_CNPJ__c, lstTempLeads);
                }
            }
        }
        return mapLeadWithCPFCNPJ;
    }
    
    /**
* Added in 13/08/2013: AND l.IsConverted = false
*/
    public List<Lead> returnLeadsWithAccountId(Id anAccountID) {
        List<Lead> lstLeadRecords = 
            [SELECT l.Id, l.Name, l.Customer_External_Key__c, l.Form_ID__c, Internet_Support__c, l.Street, l.City, l.State, 
             l.PostalCode, l.Country, l.FirstName, l.LastName, l.MobilePhone,
             l.AddressDateUpdated__c, l.AddressOK__c, l.AddressUpdated__c, l.AfterSales__c, l.AfterSalesOffer__c,
             l.BehaviorCustomer__c, l.Birthdate__c, l.BusinessMobilePhone__c, l.BusinessMobilePhoneDateUpdated__c,
             l.BusinessMobilePhoneUpdated__c, l.BusinessPhone__c, l.BusinessPhoneDateUpdated__c, l.BusinessPhoneUpdated__c,
             l.CadastralCustomer__c, l.ContemplationLetter__c, l.CPF_CNPJ__c, l.CRV_CurrentVehicle__c, l.DateOfContemplation__c,
             l.DealerOfInterest__c, l.EmailOK__c, l.Email, l.EndDateOfFinanciating__c, l.HomePhone__c, l.HomePhoneDateUpdated__c,
             l.HomePhoneUpdated__c, l.InformationUpdatedInTheLast5Years__c, l.MarketdatasCluster__c, l.NUMDBM__c,
             l.ParticipateOnOPA__c, l.PersonalEmailDateUpdated__c, l.PersonalEmailUpdated__c, l.PersonalMobilePhoneDateUpdated__c,
             l.PersonalMobilePhoneUpdated__c, l.PhoneNumberOK__c, l.PreApprovedCredit__c, l.PropensityModel__c,
             l.RecentBirthdate__c, l.SMSDateUpdated__c, l.SMSUpdated__c, l.ValueOfCredit__c, l.VehicleOfInterest__c, l.LeadSource,
             l.SubSource__c,l.PromoCode__c, l.IdOffer__c, l.IdDealer__c, l.Offer__c,NonEffectiveContact__c,
             SecondVehicleOfInterestLookup__c, VehicleOfInterestLookup__c, l.RefAddressCountry__c, l.RefAddressState__c, 
             l.IsConverted, l.Phone, l.IsRenaultVehicleOwner__c, l.Account__c, l.isResAddressRef__c, l.RefAddressStreet__c, 
             l.RefAddressCity__c, l.RefAddressPostalCode__c, l.Street__c, l.Number__c, l.Complement__c, l.Neighborhood__C, l.City__c, l.State__c, l.Country__c,
             l.PostalCode__c, l.RefAddressNumber__c, l.RefAddressComplement__c, l.RefAddressNeighborhood__c
             FROM Lead l
             WHERE l.Account__c = :anAccountID
             AND l.IsConverted = false];
        
        if (lstLeadRecords.size() > 0)
            return lstLeadRecords;
        else 
            return null;
    }
}