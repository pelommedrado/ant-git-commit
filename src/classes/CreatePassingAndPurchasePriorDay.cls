public class CreatePassingAndPurchasePriorDay{

	public static final String PERMISSAO_GRUPO  = 'BR_Passing_and_Purchase_Order_Group';
    public static final String PERMISSAO_DEALER = 'BR_Passing_and_Purchase_Order_Dealer';

	public void main(){
        
        Id recordTypeDealer = Utils.getRecordTypeId('Account', 'Network_Site_Acc');

		// recupera todos os pedidos e passagens com a data anterior
		Set<Id> purchases = getPurchases();

		// recupera todas as contas de concessionarias ativas e que não tem registro de pedidos e passagens com data anterior
		List<Account> lscontasToInsert = getContasToInsert(purchases);

		Set<Id> setContas = new Set<Id>();
		for(Account a: lscontasToInsert){
			setContas.add(a.Id);
		}

		// recupera todas as Bir(s) das Matrizes
		Set<String> setMatrizBir = new Set<String>();
		for(Account a: lsContasToInsert){
			setMatrizBir.add(a.Dealer_Matrix__c);
		}

		system.debug('setMatrizBir: ' + setMatrizBir);

		// recupera todas concessionarias com o mesmo Dealer_Matrix
		List<Account> lsMatriz = [Select Id from Account where Active_to_PPO__c = true
			AND RecordTypeID =: recordTypeDealer
			AND Dealer_Matrix__c IN: (setMatrizBir)];

		// recupera os conjuntos de permissões de usuarios de concessionarias e grupo
		List<PermissionSet> permissionDealerUsers = [ SELECT Id, Name FROM PermissionSet 
													WHERE Name = 'BR_Passing_and_Purchase_Order_Dealer'];
		List<PermissionSet> permissionGroupUsers = [ SELECT Id, Name FROM PermissionSet 
													WHERE Name = 'BR_Passing_and_Purchase_Order_Group'];
		Set<Id> dealerUsers = new Set<Id>();
		Set<Id> groupUsers = new Set<Id>();

		// recupera os usuarios do conjunto de permissão das concessionarias
		if(!permissionDealerUsers.isEmpty()){
			List<PermissionSetAssignment> assigneeDealers = [SELECT AssigneeId FROM PermissionSetAssignment 
				WHERE PermissionSetId =: permissionDealerUsers];
			for(PermissionSetAssignment psa: assigneeDealers){
				dealerUsers.add(psa.AssigneeId);
			}
		}
        
        system.debug('Usuarios com conj. permissão Dealer: ' + dealerUsers);
        
		// recupera os usuarios do conjunto de permissão dos grupos de concessionarias
		if(!permissionGroupUsers.isEmpty()){
			List<PermissionSetAssignment> assigneeGroups = [SELECT AssigneeId FROM PermissionSetAssignment 
				WHERE PermissionSetId =: permissionGroupUsers];
			for(PermissionSetAssignment psa: assigneeGroups){
				groupUsers.add(psa.AssigneeId);
			}
		}
        
        system.debug('Usuarios com conj. permissão Grupo: ' + groupUsers);

		// recupera os usuarios e suas respectivas contas de concessionaria
		List<User> lsUserDealer = [Select Id, AccountId, Contact.Account.Dealer_Matrix__c from User where Id in: (dealerUsers)];
		List<User> lsUserGroups = [Select Id, AccountId, Contact.Account.Dealer_Matrix__c from User where Id in: (groupUsers)];
		Map<Id,User> mapUserDealer = new Map<Id,User>();
		Map<Id,User> mapUserGroup = new Map<Id,User>();

		// coloca os usuarios e concessionarias no mapa
		for(Account a: lsContasToInsert){
			for(User ud: lsUserDealer){
				if(a.Id.equals(ud.AccountId)){
					mapUserDealer.put(a.Id,ud);
				}
            }
            system.debug('Mapa de Usuario com conj. permissao Dealer e Conta: ' + mapUserDealer);
            
			for(User ug: lsUserGroups){
				if(a.Id.equals(ug.AccountId)){
					mapUserGroup.put(a.Id,ug);
				}
			}
            system.debug('Mapa de Usuario com conj. permissao Grupo e Conta: ' + mapUserGroup);
		}

		// cria uma lista de pedidos e passagens para inserir
		List<PassingAndPurchaseOrder__c> lsPurchases = createPurchase(getAllDealers(), lsContasToInsert, mapUserDealer, mapUserGroup);
        
        system.debug('Lista de Pedidos para inserir: ' + lsPurchases);

		// insere a lista de imprime os erros se houver
		List<Database.SaveResult> results = Database.Insert(lsPurchases,false);
		for(Database.SaveResult sr: results){
			if(!sr.isSuccess())
				system.debug('Error: ' + sr.getErrors());
		}
	}

	public List<PassingAndPurchaseOrder__c> createPurchase(List<Account> allDealers, List<Account> lsAccount, 
                                                           Map<Id,User> mapUserDealer, Map<Id,User> mapUserGroup){
        system.debug('### lsAccount: ' + lsAccount);
        system.debug('### mapUserDealer: ' + mapUserDealer);
        system.debug('### mapUserGroup: ' + mapUserGroup);

        Id userDataLoader = [Select Id from User where Name = 'Data Loader'].Id;

		List<PassingAndPurchaseOrder__c> lsPurchases = new List<PassingAndPurchaseOrder__c>();
        
        for(Account a: lsAccount){
            PassingAndPurchaseOrder__c pp = new PassingAndPurchaseOrder__c();
            pp.Account__c = a.Id;
            pp.Date__c = system.today().addDays(-1);
            pp.Status__c = 'Pending';
            if(mapUserDealer.get(a.Id) != null){
                pp.OwnerId = mapUserDealer.get(a.Id).Id;
            }
            else if(mapUserGroup.get(a.Id) != null){
                pp.OwnerId = mapUserGroup.get(a.Id).Id;
            }
            else if(mapUserGroup.get(a.Id) == null){
                for(Account acc: allDealers){
                    if(mapUserGroup.get(acc.Id) != null && mapUserGroup.get(acc.Id).Contact.Account.Dealer_Matrix__c.equals(a.Dealer_Matrix__c))
                		pp.OwnerId = mapUserGroup.get(acc.Id).Id;
                }
            }
            else{
                pp.OwnerId = userDataLoader;
            }
            lsPurchases.add(pp);
        }
        return lsPurchases;
    }

	public Set<Id> getPurchases(){
		List<PassingAndPurchaseOrder__c> lsPurchase = [Select Account__c from PassingAndPurchaseOrder__c where Date__c = YESTERDAY];

		Set<Id> setPurchase = new Set<Id>();
		for(PassingAndPurchaseOrder__c p: lsPurchase){
			setPurchase.add(p.Account__c);
		}
		system.debug('setPurchase: ' + setPurchase);

		return setPurchase;
	}
    
    public List<Account> getAllDealers(){
        Id recordTypeDealer = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
        
        List<Account> lsContasToInsert = [Select Id, Dealer_Matrix__c from Account where Active_to_PPO__c = true
			AND RecordTypeID =: recordTypeDealer ];

		system.debug('lsContasToInsert: ' + lsContasToInsert);

		return lsContasToInsert;
    }

	public List<Account> getContasToInsert(Set<Id> setPurchase){
        
        Id recordTypeDealer = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
		
		List<Account> lsContasToInsert = [Select Id, Dealer_Matrix__c from Account where Active_to_PPO__c = true
			AND RecordTypeID =: recordTypeDealer
			AND Id NOT IN: (setPurchase)];

		system.debug('lsContasToInsert: ' + lsContasToInsert);

		return lsContasToInsert;
    }

}