@isTest(SeeAllData=true)
public class TestVFC06_AdminGoodwillGridsRO {

    static void fillParameters(VFC06_AdminGoodwillGridsRO controller){
        controller.selectOrgan = 'AIR CONDITIONING COMPRESSOR';
        controller.selectBrand = 'Renault';
        controller.selectCountry = 'Brazil';
    }

    static testMethod void testparamSelected(){
        
        VFC06_AdminGoodwillGridsRO controller = new VFC06_AdminGoodwillGridsRO();
        controller.paramSelected();
        
        PageReference pageRef = Page.VFP06_AdminGoodwillGridsRO;
        Test.setCurrentPage(pageRef);
        controller.selectBrand = '--None--';
        controller.selectOrgan = '--None--';
        controller.selectCountry = '--None--';
        controller.paramSelected();
                
        fillParameters(controller);
        system.debug('organ' + controller.selectOrgan);
        controller.paramSelected();
    }
    
    static testMethod void testSelectLists(){
        VFC06_AdminGoodwillGridsRO controller = new VFC06_AdminGoodwillGridsRO();
        
        SelectOption[] c = controller.getselectListCountries();
        SelectOption[] b = controller.getselectListBrand();
        SelectOption[] o = controller.getselectListOrgan();
        
        System.assert(c.size() > 2);
        System.assert(b.size() > 2);
        System.assert(o.size() > 2);
        
    }

/*
static User getEnvSetup(){
        // Get the context of a user
          Profile p = [SELECT Id FROM Profile WHERE Name='CCBIC-Agent']; 
          User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
          LocaleSidKey='fr', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com',
          Country = 'France');


          return u;
    }
    */
    /*
    static testMethod void testSave(){
        VFC03_AdminGoodwillGrids controller = new VFC03_AdminGoodwillGrids();
        
        User u = getEnvSetup();
    
        //Test.startTest();
        SelectOption[] countries = new SelectOption[]{}; 
        System.runAs(u){
            
            countries = controller.getselectListCountries();
            system.debug('Countries' + countries.size());
            System.assert(countries.size() == 1);
        }       
        u.Country = 'GRIDADMINISTRATOR';
        update u;
        System.runAs(u){

            countries = new SelectOption[]{}; 
            countries = controller.getselectListCountries();
            System.assert(countries.size() > 2);
            
        }       
        Boolean p = controller.parametersAreFilledIn(); // They aren't at this stage
        System.assertEquals(p, false);
        controller.paramSelected();
        
        controller.parametersIntLessMax();
        
        // Populate some parameters
        fillParameters(controller);
        
        controller.gwParams.Max_Age__c = 10;
        controller.gwParams.Max_Mileage__c = 10;
        controller.gwParams.Age_interval__c = 100;
        controller.gwParams.Mileage_Interval__c = 100;
        p = controller.parametersIntLessMax();
        System.assertEquals(p, false);
        controller.createGrid();
        
        controller.gwParams.Max_Age__c = 1000;
        controller.gwParams.Max_Mileage__c = 1000;
        controller.gwParams.Age_interval__c = 10;
        controller.gwParams.Mileage_Interval__c = 10;
        controller.createGrid();
        
        controller.deleteGrid(); // So we can see what happens when there is no data
        
        // try deleting single organs
        fillParameters(controller);
        controller.gwParams.Organ__c = 'BATTERY';
        controller.createGrid();
        controller.save();
        controller.deleteGrid();
        
        fillParameters(controller);
    
        controller.paramSelected();
        p = controller.parametersIntLessMax();
        System.assertEquals(p, true);
        fillParameters(controller);
        
        controller.createGrid();
        controller.Save();
        
        fillParameters(controller);
        controller.paramSelected();
        controller.recreate();
        controller.Save();
        
        fillParameters(controller);
        controller.paramSelected();
        controller.deleteGrid();
        controller.clearParameters();
        p = controller.parametersAreFilledIn(); // They aren't at this stage because we just cleared them.
        System.assertEquals(p, false);
        controller.createGrid();
        Goodwill_Grid_Line__c emptyobject = new Goodwill_Grid_Line__c();
        controller.fillEmptyFields(emptyobject);
        
        controller.deleteGridAndClearParam();
    //  Test.stopTest();
    }
*/
}