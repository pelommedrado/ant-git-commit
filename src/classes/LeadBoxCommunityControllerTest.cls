@isTest
private class LeadBoxCommunityControllerTest {
    
    static testMethod void test() {
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
            RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
            Name = 'Concessionária teste',
            IDBIR__c = '123ABC123',
            NameZone__c = 'R2',
            OwnerId = manager.Id
        );
        Database.insert( dealerAcc );
        
        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            Dealer__c = dealerAcc.Id,
            OwnerId = manager.Id,
            OpportunitySource__c = 'ABC',
            StageName = 'Identified',
            CloseDate = System.today().addDays( 10 )
        );
        Database.insert( opp );
        
        System.runAs( manager ) {
            LeadBoxCommunityController ctl = new LeadBoxCommunityController();
            
            LeadBoxCommunityController.ItemOpp itOp = new LeadBoxCommunityController.ItemOpp();
            itOp.opp = new Opportunity();
            
            List<SelectOption> campaignSelectList = ctl.campanhaSelectOption;
            List<SelectOption> conceSelectList = ctl.concessionariaSelectOption;
            
            List<String> valueList = new List<String>();
            valueList.add(opp.Id + ';;;' + 'Identified;');
            
            LeadBoxCommunityController.atualizar(valueList);
        }
    }
}