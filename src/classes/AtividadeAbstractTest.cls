@isTest
public class AtividadeAbstractTest {

    static testMethod void test1(){

        Test.startTest();

        Task t = new Task();
        t.ActivityDatetime__c = System.today();
        t.Subject = 'Assunto';
        Insert t;

        DashboardAtividadeController ctrl = new DashboardAtividadeController();

        AtividadeAbstract.ActivityMap actMap = new AtividadeAbstract.ActivityMap();
        actMap.subject = 'Assunto';
        actMap.activityDate = '12/10/1994';
        actMap.activityHour = '13:00';
        
        ctrl.getAct(t.Id);
        ctrl.getBrands();
        ctrl.getBrandDetails();
        ctrl.getModels();
        ctrl.getModelDetails();
        ctrl.getHourList();
        ctrl.getStatusList();
        ctrl.getSubjectList();
        ctrl.getReasonList();
        ctrl.saveRecord(actMap);
        ctrl.mapActivityToTask(actMap);
        ctrl.mapActivityToEvent(actMap);
        ctrl.setRedirectPage(actMap);
        ctrl.getUserDatetime(System.now());

        Test.stopTest();

    }

    static testMethod void test2(){

        Test.startTest();

        Event t = new Event();
        t.ActivityDate = System.today();
        t.Subject = 'Assunto';
        t.DurationInMinutes = 15;
        t.ActivityDateTime = System.now();
        t.ReminderDateTime = System.now();
        Insert t;

        DashboardAtividadeController ctrl = new DashboardAtividadeController();

        AtividadeAbstract.ActivityMap actMap = new AtividadeAbstract.ActivityMap();
        actMap.subject = 'Assunto';
        actMap.activityDate = '12/10/1994';
        actMap.activityHour = '13:00';

        ctrl.getAct(t.Id);
        ctrl.getBrands();
        ctrl.getBrandDetails();
        ctrl.getModels();
        ctrl.getModelDetails();
        ctrl.getHourList();
        ctrl.getStatusList();
        ctrl.getSubjectList();
        ctrl.getReasonList();
        ctrl.saveRecord(actMap);
        ctrl.mapActivityToTask(actMap);
        ctrl.mapActivityToEvent(actMap);
        ctrl.setRedirectPage(actMap);
        ctrl.getUserDatetime(System.now());

        Test.stopTest();

    }

}