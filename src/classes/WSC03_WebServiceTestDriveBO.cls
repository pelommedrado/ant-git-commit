public with sharing class WSC03_WebServiceTestDriveBO
{

	private static final WSC03_WebServiceTestDriveBO instance = new WSC03_WebServiceTestDriveBO();
	
	private static final Integer TEST_DRIVE_INTERVAL_IN_MINUTES = 30; // 30 minutes

    private WSC03_WebServiceTestDriveBO(){}

    public static WSC03_WebServiceTestDriveBO getInstance(){
        return instance;
    }
    
    public class DateInterval {
    	Date startDate {get;set;}
    	Date endDate   {get;set;}
    	
    	public DateInterval(Date startDate, Date endDate)
    	{
    		this.startDate = startDate;
    		this.endDate = endDate;
    	}
    }


    public List<String> fetchTestDriveVehicleAvailability(List<String> rangeOfWorkingHours, Id AccountId, 
    	String model, Date startBookingDate, Date endBookingDate, Boolean addDate)
    {

		// Buil a set with datatime requested
		Set<Datetime> setRequestedSpots = this.buildWorkingDaysAndHoursList(rangeOfWorkingHours, startBookingDate, endBookingDate);
        system.debug('*** setRequestedSpots='+setRequestedSpots);

		// Build a set with datatime available according to Test drive vehicle availability
    	List<TDV_TestDriveVehicle__c> lstTestDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().
    		fetchTestDriveVehicle_UsingDataRange(AccountId, model, startBookingDate, endBookingDate);
    	system.debug('*** lstTestDriveVehicle='+lstTestDriveVehicle);
    	
    	//
    	List<DateInterval> lstDateInterval = new List<DateInterval>();
    	for (TDV_TestDriveVehicle__c tdv : lstTestDriveVehicle)
    	{
    		Date useStartDate = tdv.AgendaOpeningDate__c < startBookingDate ? startBookingDate : tdv.AgendaOpeningDate__c;
    		Date useEndDate = tdv.AgendaClosingDate__c > endBookingDate ? endBookingDate : tdv.AgendaClosingDate__c;

    		lstDateInterval.add(new DateInterval(useStartDate, useEndDate));
    	}

		//
		Set<Date> setDates = buildUnionListOfDays(lstDateInterval);

		//
    	Set<Datetime> setAvailableTDSpots = buildWorkingDaysAndHoursListBySet(rangeOfWorkingHours, setDates);
		system.debug('*** setAvailableTDSpots='+setAvailableTDSpots);
    	
    	// Build a set with the instersection of the two sets
    	setRequestedSpots.retainAll(setAvailableTDSpots);
    	system.debug('*** setRequestedSpots intersection='+setAvailableTDSpots);

    	// Fetch a list of test drive already scheduled in the date interval requested 
    	List<TDV_TestDrive__c> lstTestDrive = VFC28_TestDriveDAO.getInstance().
    		fetchTestDriveRecordsUsingCriteriaByRange(AccountId, model, startBookingDate, endBookingDate);
		
		system.debug('*** used spots lstTestDrive='+lstTestDrive);
    	for (TDV_TestDrive__c td : lstTestDrive)
    	{
    		setRequestedSpots.remove(td.DateBooking__c);
    	}
    	
		// Convert a Set<Datetime> to List<String> in format DD/MM/YYYY HH:MI
		List<String> lstHoursAvailable = convertDatetimeToString(setRequestedSpots, addDate);
		system.debug('*** ordered lstHoursAvailable='+lstHoursAvailable);

    	return lstHoursAvailable;
    }
    
    private Set<Date> buildUnionListOfDays(List<DateInterval> lstDateInterval)
    {
    	Set<Date> setDays = new Set<Date>();
    	
    	for (DateInterval DI : lstDateInterval)
    	{
    		while (DI.startDate <= DI.endDate)
    		{
    			setDays.add(DI.startDate);
    			DI.startDate = DI.startDate.addDays(1);
    		}
    	}
    	return setDays;
    }

    private Set<Datetime> buildWorkingDaysAndHoursListBySet(List<String> rangeOfWorkingHours, Set<Date> setDates)
    {
    	Set<Datetime> setDaysAndHours = new Set<Datetime>();

    	List<String> startHour = rangeOfWorkingHours[0].split(':');
    	List<String> endHour = rangeOfWorkingHours[1].split(':');

		Time aTimeBegin = Time.newInstance(Integer.valueOf(startHour[0]), Integer.valueOf(startHour[1]), 0, 0);
    	Time aTimeEnd = Time.newInstance(Integer.valueOf(endHour[0]), Integer.valueOf(endHour[1]), 0, 0);
    	
    	for (Date aDate : setDates)
    	{
    		Datetime beginDateTime = Datetime.newInstance(aDate.year(), aDate.month(), aDate.day(), aTimeBegin.hour(), aTimeBegin.minute(), 0);
    		Datetime endDateTime = Datetime.newInstance(aDate.year(), aDate.month(), aDate.day(), aTimeEnd.hour(), aTimeEnd.minute(), 0);
    		
    		while (beginDateTime <= endDateTime)
    		{
    			setDaysAndHours.add(beginDateTime);
    			beginDateTime = beginDateTime.addMinutes(TEST_DRIVE_INTERVAL_IN_MINUTES);
    		}
    	}
    	return setDaysAndHours;
    }    
    
    private Set<Datetime> buildWorkingDaysAndHoursList(List<String> rangeOfWorkingHours, Date startBookingDate, Date endBookingDate)
    {
    	Set<Datetime> setDaysAndHours = new Set<Datetime>();

    	List<String> startHour = rangeOfWorkingHours[0].split(':');
    	List<String> endHour = rangeOfWorkingHours[1].split(':');

		Time aTimeBegin = Time.newInstance(Integer.valueOf(startHour[0]), Integer.valueOf(startHour[1]), 0, 0);
    	Datetime beginDate = Datetime.newInstance(startBookingDate.year(), startBookingDate.month(), startBookingDate.day(), aTimeBegin.hour(), aTimeBegin.minute(), 0);
    	
    	Time aTimeEnd = Time.newInstance(Integer.valueOf(endHour[0]), Integer.valueOf(endHour[1]), 0, 0);
    	Datetime endDate = Datetime.newInstance(endBookingDate.year(), endBookingDate.month(), endBookingDate.day(), aTimeEnd.hour(), aTimeEnd.minute(), 0);
    	
    	Datetime aDate = beginDate;
    	while (aDate <= endDate)
    	{
    		setDaysAndHours.add(aDate);
    		aDate = aDate.addMinutes(TEST_DRIVE_INTERVAL_IN_MINUTES);
    		
    		// If the time is at the end of the day, go to next day, at the beginning of the hour
    		if (aDate.time() > aTimeEnd) {
    			aDate = aDate.addDays(1);
    			aDate = Datetime.newInstance(aDate.date(), aTimeBegin);
    		}
    	}
    	return setDaysAndHours;
    }
    
    
    private List<String> convertDatetimeToString(Set<Datetime> setDatetime, Boolean addDate)
    {
    	List<Datetime> lstDatetime = new List<Datetime>(setDatetime);
    	lstDatetime.sort();
    	List<String> lstString = new List<String>();
    	for (Datetime aDatetime : lstDatetime)
    	{
    		if (addDate)
    			lstString.add(VFC69_Utility.formatDateYYYYMMDDHHMM(aDatetime));
    		else
    			lstString.add(VFC69_Utility.formatDateHHMM(aDatetime));
    	}
    	return lstString;
    }
}