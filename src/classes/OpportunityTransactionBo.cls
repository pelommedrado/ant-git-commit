public class OpportunityTransactionBo {

  public static final String identified   = 'Identified';
  public static final String inAttendance = 'In Attendance';
  public static final String testDrive    = 'Test Drive';
  public static final String quote        = 'Quote';
  public static final String order      = 'Order';
  public static final String billed       = 'Billed';

  private List<Opportunity> oppList { get;set; }
  private Map<Id, Opportunity> oppOldMap { get;set; }
  private Map<Id, List<TST_Transaction__c>> transLastMap { get;set; }
  private Map<Id, TDV_TestDrive__c> oppIdTestDriveLastMap { get;set; }
  private Map<Id, VehicleBooking__c> oppIdVehicleLastMap { get;set; }

  public OpportunityTransactionBo(List<Opportunity> oppList, Map<Id, Opportunity> oppOldMap) {
    this.oppList = oppList;
    this.oppOldMap = oppOldMap;
    this.transLastMap = createMapTransactionLast(oppList);
    this.oppIdTestDriveLastMap = createMapTestDriveLast(oppList);
    this.oppIdVehicleLastMap = createVehicleBookingLast(oppList);
  }

  
  public Map<Id, List<TST_Transaction__c>> createMapTransactionLast(List<Opportunity> oppList) {
    System.debug('createMapTransactionLast()');

    final Map<Id, List<TST_Transaction__c>> oppIdTransLastMap = new Map<Id, List<TST_Transaction__c>>();

    final List<TST_Transaction__c> transList = obterTransactions(oppList);
    for(TST_Transaction__c trans : transList) {
      final List<TST_Transaction__c> temp = oppIdTransLastMap.get(trans.Opportunity__c);
      if(temp == null) {
        List<TST_Transaction__c> tList = new List<TST_Transaction__c>();
        tList.add(trans);
        oppIdTransLastMap.put(trans.Opportunity__c, tList);
      } else /*if(trans.CreatedDate > temp.CreatedDate)*/ {
        temp.add(trans);
        //oppIdTransLastMap.put(trans.Opportunity__c, trans);
      }
    }
    return oppIdTransLastMap;
  }

  public List<TST_Transaction__c> obterTransactions(List<Opportunity> oppList) {
    if(oppList.isEmpty()) {
      return new List<TST_Transaction__c>();
    }
    return [
      SELECT Id, Opportunity__c, Opportunity_Status__c,
      Opportunity_Last_Status__c, CreatedDate
      FROM TST_Transaction__c
      WHERE Opportunity__c IN :oppList
      AND Opportunity_Status__c != NULL AND Type__c = NULL
      ORDER BY CreatedDate DESC
    ];
  }

  public Map<Id, TDV_TestDrive__c> createMapTestDriveLast(List<Opportunity> oppList) {
    System.debug('createMapTestDriveLast()');

    final Map<Id, TDV_TestDrive__c> oppIdTestDriveLastMap = new Map<Id, TDV_TestDrive__c>();

    final List<TDV_TestDrive__c> testDriveList = obterTestDrives(oppList);
    for(TDV_TestDrive__c test : testDriveList) {
      final TDV_TestDrive__c temp = oppIdTestDriveLastMap.get(test.Opportunity__c);
      if(temp == null) {
        oppIdTestDriveLastMap.put(test.Opportunity__c, test);
      } else if(test.CreatedDate > temp.CreatedDate) {
        oppIdTestDriveLastMap.put(test.Opportunity__c, test);
      }
    }
    System.debug(oppIdTestDriveLastMap.values());
    return oppIdTestDriveLastMap;
  }

  public List<TDV_TestDrive__c> obterTestDrives(List<Opportunity> oppList) {
    if(oppList.isEmpty()) {
      return new List<TDV_TestDrive__c>();
    }
    return [
        SELECT Id, Opportunity__c, Status__c, Model__c, CreatedDate
        FROM TDV_TestDrive__c
        WHERE Opportunity__c IN :oppList
        ORDER BY CreatedDate DESC
    ];
  }

  public Map<Id, VehicleBooking__c> createVehicleBookingLast(List<Opportunity> oppList) {
    System.debug('createVehicleBookingLast()');
    final List<Quote> quoteList = [
      SELECT Id, CreatedDate, OpportunityId FROM Quote
      WHERE OpportunityId IN :oppList ORDER BY CreatedDate DESC
    ];
    //final Map<Id, Quote> oppIdQuoteLastMap = createOpportunityQuoteLastMap(quoteList);
    //System.debug('Map Quote: ' + oppIdQuoteLastMap);
    final Map<Id, VehicleBooking__c> quoteIdVehicleBookingLastMap = createQuoteVehicleBookingMap(quoteList);
    final Map<Id, VehicleBooking__c> oppIdVehicleBookingLastMap = new Map<Id, VehicleBooking__c>();
    for(Quote quo: quoteList) {
      final VehicleBooking__c vb =  quoteIdVehicleBookingLastMap.get(quo.Id);

      if(vb != null) {
        final VehicleBooking__c temp = oppIdVehicleBookingLastMap.get(quo.OpportunityId);
        if(temp == null) {
          oppIdVehicleBookingLastMap.put(quo.OpportunityId, vb);
        } else if(vb.CreatedDate > temp.CreatedDate) {
          oppIdVehicleBookingLastMap.put(quo.OpportunityId, vb);
        }
      }
    }
    return oppIdVehicleBookingLastMap;
  }

  /*private Map<Id, Quote> createOpportunityQuoteLastMap(List<Quote> quoteList) {
    final Map<Id, Quote> oppIdQuoteLastMap = new Map<Id, Quote>();
    for(Quote quo : quoteList) {
      final Quote temp = oppIdQuoteLastMap.get(quo.OpportunityId);
      if(temp == null) {
        oppIdQuoteLastMap.put(quo.OpportunityId, quo);
      } else if(quo.CreatedDate > temp.CreatedDate) {
        oppIdQuoteLastMap.put(quo.OpportunityId, quo);
      }
    }
    return oppIdQuoteLastMap;
  }*/

  public Map<Id, VehicleBooking__c> createQuoteVehicleBookingMap(List<Quote> quoteList) {
    final List<VehicleBooking__c> vehicleList = [
      SELECT Modelo__c, Status__c, Quote__c, CreatedDate FROM VehicleBooking__c
      WHERE Quote__c IN:(quoteList) AND Status__c IN ('Active', 'Closed')
      ORDER BY CreatedDate DESC
    ];
    final Map<Id, VehicleBooking__c> quoteIdVehicleBookingLastMap = new Map<Id, VehicleBooking__c>();
    for(VehicleBooking__c vehicleBoo : vehicleList) {
      quoteIdVehicleBookingLastMap.put(vehicleBoo.Quote__c, vehicleBoo);
    }
    return quoteIdVehicleBookingLastMap;
  }

  public void execCreate() {
      Set<Id> oppsIds = new Set<Id>();
      System.debug('OpportunityTransactionBo.execCreate()');
      //final List<TST_Transaction__c> transactionList = new List<TST_Transaction__c>();
      for(Opportunity opp: oppList) {
          oppsIds.add(opp.Id);
          //transactionList.add(createTransaction(opp, identified, ''));
      }
      
      execCreate(oppsIds);
      
      /*try {
          Database.insert(transactionList);

      } catch(Exception ex) {
          System.debug('Ex: ' + ex);
      }*/
  }
    
    @future
    public static void execCreate(Set<Id> oppIds){
        
        final List<TST_Transaction__c> transactionList = new List<TST_Transaction__c>();
        final List<Opportunity> oppList = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityIds(oppIds);
        
        OpportunityTransactionBo oppTransBo = new OpportunityTransactionBo(oppList, new Map<Id, Opportunity>());
        
        for(Opportunity opp: oppList) {
          //oppsIds.add(opp.Id);
          transactionList.add(oppTransBo.createTransaction(opp, 'Identified', ''));
          }
        
          try {
              Database.insert(transactionList);
    
          } catch(Exception ex) {
              System.debug('Ex: ' + ex);
          }
        
    }
    
  public void execUpdate() {
    System.debug('OpportunityTransactionBo.execUpdate()');

    final List<TST_Transaction__c> transactionList = new List<TST_Transaction__c>();

    for(Opportunity opp: oppList) {
      final TST_Transaction__c transOppStage = getTransactionStageName(opp);

      if(transOppStage == null) {
        final List<TST_Transaction__c> newTransList = createNewTransactionList(opp);
        transactionList.addAll(newTransList);
      } else {
        transOppStage.VehicleOfInterest__c = definirVehicleInterest(opp);
        transactionList.add(transOppStage);
      }
    }

    try {
      Database.upsert(transactionList);

    } catch(Exception ex) {
        System.debug('Ex: ' + ex);
    }
  }

  public TST_Transaction__c getTransactionStageName(Opportunity opp) {
    System.debug('getTransactionStageName()');
    final List<TST_Transaction__c> transLastList = transLastMap.get(opp.Id);
    if(transLastList == null || transLastList.isEmpty()) {
      return null;
    }
    TST_Transaction__c temp = null;
    for(TST_Transaction__c trans : transLastList) {
      if(trans.Opportunity_Status__c.equalsIgnoreCase(opp.StageName)) {
        if(temp == null) {
          temp = trans;
        } else if(trans.CreatedDate > temp.CreatedDate) {
          temp = trans;
        }
      }
    }

    return temp;
  }

  private List<TST_Transaction__c> createNewTransactionList(Opportunity opp) {
    System.debug('createNewTransactionList()');

    final Opportunity oppOld = oppOldMap.get(opp.Id);
    final List<TST_Transaction__c> newTransList = new List<TST_Transaction__c>();

    if(!isFaseAlterada(opp, oppOld)) {
      return newTransList;
    }

    if(isFase(oppOld, identified) && isFase(opp, testDrive)) {
      newTransList.add(createTransaction(opp, inAttendance, identified));
      newTransList.add(createTransaction(opp, testDrive, inAttendance));

    } else if(isFase(oppOld, identified) && isFase(opp, quote)) {
      newTransList.add(createTransaction(opp, inAttendance, identified));
      newTransList.add(createTransaction(opp, quote, inAttendance));

    } else {
      newTransList.add(createTransaction(opp, oppOld));
    }

    return newTransList;
  }

  private Boolean isFaseAlterada(Opportunity opp, Opportunity oppOld) {
    if(String.isEmpty(opp.StageName) || String.isEmpty(oppOld.StageName)) {
      return false;
    }
    return opp.StageName != oppOld.StageName;
  }

  private Boolean isFase(Opportunity opp, String fase) {
    return opp.StageName.equalsIgnoreCase(fase);
  }

  private TST_Transaction__c createTransaction(Opportunity opp, Opportunity oppOld) {
    return createTransaction(opp, opp.StageName, oppOld.StageName);
  }

  public TST_Transaction__c createTransaction(Opportunity opp, String fase, String faseLast) {
    TST_Transaction__c tst = new TST_Transaction__c();
    tst.Opportunity_Last_Status__c = faseLast;
    tst.Opportunity__c = opp.Id;
    tst.Opportunity_Status__c = fase;
    tst.Stage_Duration__c = calcularStageDuration(opp);
    tst.VehicleOfInterest__c = definirVehicleInterest(opp);
    return tst;
  }

  public String definirVehicleInterest(Opportunity opp) {
    System.debug('definirVehicleInterest()');
    if(isFase(opp, testDrive)) {
      return definirVehicleInterestTestDriveLast(opp);

    }
    return definirVehicleInterestVehicleBookingLast(opp);
  }

  public String definirVehicleInterestTestDriveLast(Opportunity opp) {
    System.debug('definirVehicleInterestTestDriveLast()');
    final TDV_TestDrive__c testDriveLast = oppIdTestDriveLastMap.get(opp.Id);
    System.debug('TDV_TestDrive__c: ' + testDriveLast);

    if(testDriveLast == null || String.isEmpty(testDriveLast.Status__c)) {
      return opp.VehicleInterest__c;
    } else if(testDriveLast.Status__c.equalsIgnoreCase('Performed')) {
      return testDriveLast.Model__c;
    }
    return opp.VehicleInterest__c;
  }

  public String definirVehicleInterestVehicleBookingLast(Opportunity opp) {
    System.debug('definirVehicleInterestVehicleBookingLast()');
    final VehicleBooking__c vehicleBooking = oppIdVehicleLastMap.get(opp.Id);
    if(vehicleBooking != null) {
      return vehicleBooking.Modelo__c;
    }
    //if(isFase(opp, order) || isFase(opp, billed)) {
    //  throw new Utils.GenericException(
    //    'Oportunidades na fase de Orçamento devem possui um Vehicle Booking');
    //}
    return opp.VehicleInterest__c;
  }

  public Decimal calcularStageDuration(Opportunity opp) {
    System.debug('calcularStageDuration()');

    Decimal timeCreatedMille = 0;
    final Opportunity oppOld = oppOldMap.get(opp.Id);
    if(oppOld == null) {
      return timeCreatedMille;
    }

    final TST_Transaction__c transactionLast = getTransactionStageName(oppOld);
    if(transactionLast != null) {
      timeCreatedMille = transactionLast.CreatedDate.getTime();
    } else {
      timeCreatedMille = opp.CreatedDate.getTime();
    }

    final Decimal timeMille = System.now().getTime();
    final Decimal diffMille = timeMille - timeCreatedMille;
    final Decimal hourMille = 3600000; //1000*60*60 ou 1h;
    //return Math.floor(diffMille / hourMille);
    return (Decimal)(diffMille / hourMille);
  }
}