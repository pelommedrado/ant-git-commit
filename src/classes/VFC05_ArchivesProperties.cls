public class VFC05_ArchivesProperties {
  
  public class ArchivesCases {
    public String IdCase {get;set;}
      public DateTime activityCloseDate {get;set;}
        public String accountId {get;set;}
        public String assetId {get;set;}
        public String brandInfoExchangeFlag {get;set;}
        public String buId {get;set;}
        public String build {get;set;}
        public String city {get;set;}
        public String clientAgreement {get;set;}
        public String conflictId {get;set;}
        public String contactCellPhNum {get;set;}
        public String contactHomePhNum {get;set;}
        public String contactId {get;set;}
        public String contactWorkPhNum {get;set;}
        public String country {get;set;}
        public DateTime created {get;set;}
        public String createdBy {get;set;}
        public DateTime createdDate {get;set;}
        public String descText {get;set;}
        public String detail {get;set;}
        public String firstName {get;set;}
        public String from_x {get;set;}
        public String lastName {get;set;}
        public DateTime lastUpdated {get;set;}
        public String lastUpdatedBy {get;set;}
        public String login {get;set;}
        public String modificationNumber {get;set;}
        public String name {get;set;}
        public String negoResult {get;set;}
        public String orgId {get;set;}
        public DateTime purchaseDate {get;set;}
        public String purchaseIntention {get;set;}
        public String quoteType {get;set;}
        public String rcName {get;set;}
        public String rcRowId {get;set;}
        public String resAuthor {get;set;}
        public DateTime resDate {get;set;}
        public String rowId {get;set;}
        public String serialNum {get;set;}
        public String srArea {get;set;}
        public String srAreaCode {get;set;}
        public String srBrand {get;set;}
        public String srCustNum {get;set;}
        public String srNum {get;set;}
        public String srSubtypeCode {get;set;}
        public Integer state {get;set;}
        public String subArea {get;set;}
        public String traitment {get;set;}
        public String zipCode {get;set;}
        public VFC05_ArchivesProperties.ArchivesAttachments[] Attach {get;set;}
  } 
  
  public class ArchivesAttachments{
    public String attachmentName {get;set;}
        public DateTime createdDate {get;set;}
        public String extension {get;set;}
  }
}