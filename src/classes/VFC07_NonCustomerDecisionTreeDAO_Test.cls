/**
	Class   -   VFC07_NonCustomerDecisionTreeDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC07_NonCustomerDecisionTreeDAO.
**/
@isTest
public with sharing class VFC07_NonCustomerDecisionTreeDAO_Test {

	static testMethod void VFC07_NonCustomerDecisionTreeDAO_Test() {
        // TO DO: implement unit test
        List<NCT_NonCustomerDecisionTree__c> lstNonCustomerDecisionTreeInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToNonCustomerRelationshipObjects();
        String ClusterMD = 'Class 2';
		Boolean CurrentVehicle = false;
		String PreApprovedCredit = 'Y';
		Boolean VehicleOfCampaign =true;
		Boolean VehicleOfInterest = true;
		Test.startTest();
		List<NCT_NonCustomerDecisionTree__c> lstNonCustomerDecisionTree = VFC07_NonCustomerDecisionTreeDAO.getInstance().findNonCustomerDecisionTree(lstNonCustomerDecisionTreeInsert[0].ClusterMD__c, lstNonCustomerDecisionTreeInsert[0].CurrentVehicle__c, lstNonCustomerDecisionTreeInsert[0].PreApprovedCredit__c, lstNonCustomerDecisionTreeInsert[0].VehicleOfInterest__c, lstNonCustomerDecisionTreeInsert[0].VehicleOfCampaign__c);
		Test.stopTest();
		System.assert(lstNonCustomerDecisionTree.size() > 0);
    }
}