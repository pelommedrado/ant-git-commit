/*****************************************************************************************
    Name            : Rforce_StringBuffer_CLS
    Description     : Generic String Buffer class.
    Project Name    : Rforce
    Release Number  : 9.1
    Implemented Date: 10-12-2015
    Implemented By : Venkatesh Kumar Sakthivel
******************************************************************************************/


global class Rforce_StringBuffer_CLS {

private String theString;

global Rforce_StringBuffer_CLS(){
this('');
}

global Rforce_StringBuffer_CLS(String str){
theString = str;
}

global Rforce_StringBuffer_CLS(Decimal d){
theString = '' + d;
}

global Rforce_StringBuffer_CLS(Long l){
theString = '' + l;
}

global Rforce_StringBuffer_CLS(Integer i){
theString = '' + i;
}

global Rforce_StringBuffer_CLS(Boolean b){
theString = '' + b;
}

global Rforce_StringBuffer_CLS(Date d){
theString = '' + d;
}

global Rforce_StringBuffer_CLS append(String str){
theString += str; return this; 
}

global Rforce_StringBuffer_CLS append(Decimal d){
theString += d; return this;
}

global Rforce_StringBuffer_CLS append(Long l){
theString += l; return this;
}

global Rforce_StringBuffer_CLS append(Integer i){
theString += i; return this;
}

global Rforce_StringBuffer_CLS append(Boolean b){
theString += b; return this;
}

global Rforce_StringBuffer_CLS append(Date d){
theString += d; return this;
}

global String toStr(){
return theString;
}
}