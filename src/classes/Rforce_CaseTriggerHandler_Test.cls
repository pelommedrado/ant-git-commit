/*****************************************************************************************
    Name    : Rforce_CaseTriggerHandler_Test
    Desc    : To Test the Case Trigger Handler
    Approach:
    Author  : Praneel PIDIKITI (Atos Integration)
    Project : Rforce
******************************************************************************************/
@istest
public with sharing class Rforce_CaseTriggerHandler_Test {

    private static testMethod void testCaseInsert() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;

        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id,developername from RecordType where sObjectType = 'Account' and developername='Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', IDBIR__c='00001314');
            insert Acc;
           Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Dealer__c=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Colombia');
            insert cs;
            List<Case> CasList = new List<Case>();
            CasList.add(cs);
            Rforce_CaseTriggerHandler_CLS.onAfterInsert(CasList);
            Rforce_CaseTriggerHandler_CLS.onAfterUpdate(CasList);
            Test.stopTest();
        }

    }
}