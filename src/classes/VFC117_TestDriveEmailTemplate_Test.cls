@isTest 
private class VFC117_TestDriveEmailTemplate_Test
{
    static testMethod void unitTest01()
    {
        // Prepare test data
        RecordType rec = [SELECT Id FROM RecordType WHERE DeveloperName = 'Network_Site_Acc'];

        Account dealerAccount = new Account();
        dealerAccount.RecordTypeId = rec.Id;
        dealerAccount.Name = 'Full Name';
        dealerAccount.ShortndName__c = 'Short Name';
        dealerAccount.Email__c = 'dealer@mail.com'; 
        dealerAccount.ShippingStreet = 'my street';
        dealerAccount.ShippingCity = 'Paris';
        dealerAccount.ShippingState = 'IDF';

        String name = 'John Doe';
        String emailCustomer = 'customer@email.com';
        String phone = '55551010';
        String cep = '22800100'; 
        String vehicle = 'DUSTER';
        Datetime DateHour = Datetime.newInstance(2013, 10, 11, 10, 15, 0);

        Test.startTest();

        String test01 = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate04(dealerAccount, name, phone, cep, vehicle, DateHour);
        String test02 = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate05(dealerAccount, name, phone, cep, vehicle, DateHour);
        String test03 = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate06(dealerAccount, name, phone, cep, vehicle, DateHour);
        String test04 = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate07(dealerAccount, name, emailCustomer, phone, cep, vehicle, DateHour);

        Test.stopTest();
    }
}