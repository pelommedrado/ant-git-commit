public with sharing class AccountCaseCorrection {

	public AccountCaseCorrection(List<Case> caseList) {
		Map<Id, List<Case>> caseByAccountMap = new Map<Id, List<Case>>();

		for(Case iCase : caseList){
			if(caseByAccountMap.get(iCase.AccountId) == null){
				caseByAccountMap.put(iCase.AccountId, new List<Case>());
				caseByAccountMap.get(iCase.AccountId).add(iCase);
			}
			else{
				caseByAccountMap.get(iCase.AccountId).add(iCase);
			}
		}

		System.debug('!!!caseByAccountMap:'+caseByAccountMap);

		List<Account> accToUpdate = new List<Account>();

		for(Id acc : caseByAccountMap.keySet()){
			Double countCaso1 = 0;
			Double countCaso2 = 0;
			DateTime creationDate;
			DateTime closingDate;
			for(Case iCase : caseByAccountMap.get(acc)){
                System.debug(iCase.Id);
				if((iCase.Type != null && iCase.Type.equalsIgnoreCase('Complaint')) && 
				  (iCase.Status != null && (iCase.Status.equalsIgnoreCase('Open') || iCase.Status.equalsIgnoreCase('Waiting for reply') 
				|| iCase.Status.equalsIgnoreCase('Waiting for documents') || iCase.Status.equalsIgnoreCase('Escalated')
				|| iCase.Status.equalsIgnoreCase('Answered') || iCase.Status.equalsIgnoreCase('Resolved')))){
				  	System.debug('!!!CASO1');
					countCaso1++;
				}
				if((iCase.Type != null && iCase.Type == 'Complaint') && (iCase.Status != null && iCase.Status == 'Closed')){
				  	System.debug('!!!CASO2');
					countCaso2++;
					if(creationDate == null){
						creationDate = iCase.CreatedDate;
					}
					else if(iCase.CreatedDate < creationDate){
						creationDate = iCase.CreatedDate;
					}
					if(closingDate == null){
						closingDate = iCase.ClosedDate;
					}
					else if(iCase.ClosedDate > closingDate){
						closingDate = iCase.CreatedDate;
					}
				}
			}
			System.debug('!!!countCaso1:'+countCaso1 + ', countCaso2:'+ countCaso2);
			System.debug('!!!creationDate:'+creationDate + ', closingDate:'+ closingDate);
			accToUpdate.add(updateSetting(acc, countCaso1, countCaso2, creationDate, closingDate));
		}
		Database.update(accToUpdate);
	}

	public Account updateSetting(Id accId, Double caso1, Double caso2, DateTime creationDate, DateTime closingDate){
		Account acc = new Account();
		acc.Id = accId;
		if(caso1 == 0){
			if(caso2 == 0){
				acc.StopComRCFrom__c = null;
				acc.StopComRCTo__c = null;
			}
			else{
				acc.StopComRCFrom__c = creationDate;
				acc.StopComRCTo__c = closingDate;
			}
		}
		return acc;
	}

}