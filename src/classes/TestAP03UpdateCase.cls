@isTest
private class TestAP03UpdateCase
{
    static testMethod void AP03UpdateCase_TEST()
    {
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
        
         User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com',BypassVR__c=true,BypassWF__c=true, RecordDefaultCountry__c='United States');
      
         System.runAs(usr)
        {
        Account Acc = new Account(Name='Test1',Phone='1000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;    
        Contact Con = new Contact(FirstName = 'Mandatory', LastName='Test Contact',Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        
        Case c = new Case (Origin='Mobile',Type='Information Request',SubType__c='Booking',Status='Open',Priority='Urgent',Description='Description',From__c='Customer', AccountId=Acc.Id, ContactId=Con.Id);
        insert c;
         
        Goodwill__c g = new Goodwill__c (case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId,ExpenseCode__c='OTS Issue',BudgetCode__c='SRC', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000);
        
       
        
        Test.startTest();
    
        insert g;
        g.ResolutionCode__c = 'EXTENDED WARRANTY';
        update g;
    
        Test.stopTest();
        }
    }
}