/** Apex class used within Myr_Customer_Message objects triggers
	This class is basically used to check the data entered in templates
	
	@author S. Ducamp / Atos
	@date 10.07.2015
	@revision 1.0
	
	V1.0: S.Ducamp, check html tags and matching keys. D.Veron, Template.Name = Setting.Name + Template.Country + Template.Brand + Template.Language
	@version 16.10: SDP / F1415-US3696 / Clarify merge functionnality
**/
public with sharing class Myr_CustMessageTriggerHandler_Cls {
	
	/** Inner exception class **/
	public class Myr_CustMessageTriggerHandler_Cls_Exception extends Exception {}
	
	/** check if the template is valid, 
		@throws exception otherwise
	**/
	public static void checkTemplateValidity(List<Customer_Message_Templates__c> listNewTempl, Map <Id, Customer_Message_Templates__c> mapOldTempl) {
		String tags = CS04_MYR_Settings__c.getInstance().Myr_Customer_Message_HtmlTags__c;
		for( Customer_Message_Templates__c templ : listNewTempl ) {
			system.debug('>>>>>> trigger:templ.Body__c='+templ.Body__c);
			if( !String.isBlank(templ.Body__c) ) {
				if( !Myr_CreateCustMessage_Cls.checkHtmlTags(templ.Body__c) ) {
					throw new Myr_CustMessageTriggerHandler_Cls_Exception(system.Label.Customer_Message_Tpl_ErrHtmlTags + tags.unescapeHtml4());
				}
			} 
			system.debug('>>>>>> trigger:templ.Body__c='+templ.Body__c);
		}
	}
	
	/** Check if the settings are valid, meaning that the matching keys specified are correct **/
	public static void checkSettingsValidity(List<Customer_Message_Settings__c> listNewSets, Map <Id, Customer_Message_Settings__c> mapOldSets) {	
		//If a MergeAction has been selected, check that account__c and type__c are selected in the MergeKeys__c field otherwise add them.
		for( Customer_Message_Settings__c setting : listNewSets ) { 
			system.debug( '### Myr_CustMessageTriggerHandler_Cls - <checkSettingsValidity> - MergeAction__c='+setting.MergeAction__c+', MergeKeys__c='+setting.MergeKeys__c);
			if( !String.isBlank(setting.MergeAction__c) ) {
				if( String.isBlank( setting.MergeKeys__c ) ) {
					setting.MergeKeys__c = 'account__c;template__c';
				} else {
					if( !setting.MergeKeys__c.containsIgnoreCase( 'account__c' ) ) {
						setting.MergeKeys__c += ';account__c';
					}
					if( !setting.MergeKeys__c.containsIgnoreCase( 'template__c' ) ) {
						setting.MergeKeys__c += ';template__c';
					}
				}
			} else {
				setting.MergeKeys__c = '';
			}
			system.debug( '### Myr_CustMessageTriggerHandler_Cls - <checkSettingsValidity> - MergeAction__c='+setting.MergeAction__c+', MergeKeys__c='+setting.MergeKeys__c);
		}
	}

	/**
		Template name = Setting.Name + Template.Country + Template.Brand + Template.Language
		Called each time a template is inserted or updated
	**/
	public static void setNameOfCustomerMessageTemplate(List<Customer_Message_Templates__c> lt) {
		List<Id> settingIds = new List<Id>();
		for (Customer_Message_Templates__c t : lt){
			settingIds.add(t.MessageType__c);
		}
		List<Customer_Message_Settings__c> ls = [select Name from Customer_Message_Settings__c where Id in :settingIds];
		for( Customer_Message_Templates__c t : lt ){
			for( Customer_Message_Settings__c s : ls ){			
				if(t.MessageType__c==s.Id){
					t.Name= s.Name + '_' + t.country__c + '_' + t.Language__c + '_' + t.Brand__c;
					break;
				}
			}
		}
	}
}