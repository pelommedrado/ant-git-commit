global class WSC09_ALRVehicleTracking {
    
    private static Datetime now(){
        Datetime now = System.now();
        return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
    }
    
    global class VehicleTracking{
        
        //Valores fixos
        webservice String idInterface;      //F01
        webservice String versionInterface; //001
        webservice String sendingSystem;    //ALR
        webservice String receivingSystem;  //SLF
        webservice String dealerArrivalDate;
        
        webservice String vin;      
        webservice String statusVehicle; //PGEO
        webservice String pilotDate;
        webservice String forecastDate;
        webservice String invoiceNumber;
        webservice String invoiceSerie;
        webservice String invoiceDate;
        webservice String CTENumber;
        webservice String isBlock;
        webservice String typeBlock;
        webservice String BIRDealer;
    }
    
    global class ReturnServiceTracking{
        
        webservice String message;
        webservice Datetime lastUpdateTime;
    }
    
    global class ReturnServiceMADP{
        
        //Valores fixos
        webservice String idInterface;      //F02
        webservice String versionInterface; //001
        webservice String sendingSystem;    //SLF
        webservice String receivingSystem;  //ALR
        
        webservice String message;
        webservice Datetime lastUpdateTime;
        webservice ListVinMADP lsReturnMADP;
    }
    
    global class ReturnMADP{ 
        
        webservice String vin;
        webservice String BIRDealer;
        webservice String dealerArrivalDate;
    }
    
    global class ListVinMADP{
        webservice List<ReturnMADP> returnMADP = new List<ReturnMADP>();
    }
    
    
    /* Serviço para retornar ao MADP a chegada do Veículo */
    
    webservice static ReturnServiceMADP getMADP (Datetime lastUpdateTime){
        
        ReturnServiceMADP returnServiceMADP = new ReturnServiceMADP();
        //valores fixos
        returnServiceMADP.idInterface  = 'F02';
        returnServiceMADP.versionInterface = '001';
        returnServiceMADP.sendingSystem = 'SLF';
        returnServiceMADP.receivingSystem = 'ALR';
        returnServiceMADP.message = 'Success';
        returnServiceMADP.lastUpdateTime = now();
        
        
        List<ReturnMADP> lsVechicles = new List<ReturnMADP>();
        if(lastUpdateTime!=null){
            List<ALR_Tracking__c> listTracking = [select id,name,ALR_BIR_Dealer__c,ALR_Date_MADP__c,ALR_BuyDealer__r.IDBIR__c from ALR_Tracking__c where LastModifiedDate >:  lastUpdateTime
                                                  and ALR_Date_MADP__c <> null limit 49999];
            if(listTracking !=null && !listTracking.isEmpty()){
                for(ALR_Tracking__c lstTracking : listTracking){
                    ReturnMADP returnMADP = new ReturnMADP();
                    returnMADP.vin = lstTracking.name;
                    returnMADP.BIRDealer =  lstTracking.ALR_BuyDealer__r.IDBIR__c;
                    returnMADP.dealerArrivalDate = string.valueofGmt(lstTracking.ALR_Date_MADP__c);
                    lsVechicles.add(returnMADP);
                }
            }else{
                returnServiceMADP.message = 'Date Invalid';
            }
        }    
        returnServiceMADP.lsReturnMADP = new ListVinMADP();
        
        if(lsVechicles==null || lsVechicles.isEmpty())      
            returnServiceMADP.message = 'The search returns no results'; 
        returnServiceMADP.lsReturnMADP.returnMADP = lsVechicles;
        
        
        
        
        System.debug('**** List retornoMADP: '+returnServiceMADP.lsReturnMADP.returnMADP.size());
        
        return returnServiceMADP;
    }
    
    
    
    /* Serviço para atualizar o rastreamento dos veículos em trânsito 
* Parametros: Datetime lastUpdateTime
*             List<VehicleTracking>
* Retorno:    Retorno(data último update, msg)
*/
    webservice static ReturnServiceTracking updateTracking (Datetime lastUpdateTime, List<VehicleTracking> lsVehicleTracking){
        
        //cria o retorno para cada registro conforme sucesso ou erro gerado
        ReturnServiceTracking returnService = new ReturnServiceTracking();
        returnService.message = atualizaTracking(lsVehicleTracking); //em caso de erro populado no catch (try/catch)
        returnService.lastUpdateTime = now();
        
        
        
        return returnService;   
    }
    
    public static String atualizaTracking(List<VehicleTracking> lsVehicleTracking){
        String message = 'Atenção: O Tracking não foi atualizado';
        System.debug('####### lsVehicleTracking'+lsVehicleTracking);
        //pega os ids do veiculo para fazer uma busca no Tracking assim conseguimos trazer os ids de Tracking
        Map<String,String> mapIdTracking = getIdTrackingWithChassi(lsVehicleTracking);
        
        //Parte da atualização
        if(lsVehicleTracking != null && !lsVehicleTracking.isEmpty() && lsVehicleTracking.size()>0){
            
            //Lista para armazenar os trackings e fazer o update
            List<ALR_Tracking__c> listUpdateTracking = new List<ALR_Tracking__c>();
            ALR_Tracking__c tracking;
            
            
            
            List<String> listChassi = new List<String> ();
            //for para pegar os Ids do chassi
            for(VehicleTracking veiculoTracking :lsVehicleTracking){
                if(veiculoTracking.vin!=null && !veiculoTracking.vin.equals(''))
                    listChassi.add(veiculoTracking.vin.trim());
            }
            
            System.debug('Lista de Chassi ALR -> '+listChassi);
            Map<String,String> mapChassiAndId = new Map<String,String>();
           // System.debug('SELECT Id,name FROM ALR_Tracking__c WHERE Name in:(listChassi)'+ [SELECT Id,name FROM ALR_Tracking__c WHERE Name in:(listChassi)]);
            for(ALR_Tracking__c aux : [SELECT Id,name FROM ALR_Tracking__c WHERE Name in:(listChassi)])
                mapChassiAndId.put(aux.name,aux.Id);
            
            //Lista de veiculos
            List<String> listChassiVehicle = new List<String>();
            for(VehicleTracking veiculoTracking : lsVehicleTracking)
                listChassiVehicle.add(veiculoTracking.vin.trim());                
            List<VEH_Veh__c> lsVehicle = getVehicle(listChassiVehicle);
            
            List<VEH_Veh__c> updateVehicle = new List<VEH_Veh__c>();
            
            for(VehicleTracking veiculoTracking : lsVehicleTracking)  {
                
                
                tracking = new ALR_Tracking__c();
                //verificação de erros/campos Inválidos
                if(veiculoTracking.vin == null || veiculoTracking.vin.equals('') )
                    return 'ERRO: number vin invalid';
                
                //-------------------------------------------------------------- 
                //conversão
                
                
                if(mapChassiAndId.containsKey(veiculoTracking.vin)){
                    try{            
                        //atualiza veiculo - dados nota fiscal
                        for(VEH_Veh__c vehicle : lsVehicle){
                            if(vehicle.name.equals(veiculoTracking.vin)){
                                VEH_Veh__c veic = new VEH_Veh__c();
                                veic.Id = vehicle.Id;
                                veic.ALR_InvoiceNumber__c = veiculoTracking.invoiceNumber;
                                veic.ALR_SerialNumberOnInvoice__c = veiculoTracking.invoiceSerie;
                                if(!String.isBlank(veiculoTracking.invoiceDate))
                                    veic.Billing_Date_SAP__c = setStringToDateFormat(veiculoTracking.invoiceDate);
                                updateVehicle.add(veic);
                            }
                            
                        }
                        
                        
                        tracking.Id = mapChassiAndId.get(veiculoTracking.vin);
                        if(String.isNotBlank(veiculoTracking.statusVehicle))
                            tracking.ALR_Vehicle_Position__c = veiculoTracking.statusVehicle;
                        
                        
                        if(String.isNotBlank(veiculoTracking.pilotDate)){
                            tracking.ALR_Pilot_Delivery__c = setStringToDateFormat(veiculoTracking.pilotDate);
                        }else{
                            tracking.ALR_Pilot_Delivery__c = null;
                        }
                        if(String.isNotBlank(veiculoTracking.forecastDate)){
                            tracking.ALR_Forecast_Delivery__c =  setStringToDateFormat(veiculoTracking.forecastDate);
                        }else{
                            tracking.ALR_Forecast_Delivery__c = null;
                            // Se recebeu o Forecast Delivery em branco, a data de chegada deve ser limpa
                            tracking.ALR_Date_MADP__c = null;
                        }
                        if(String.isNotBlank(veiculoTracking.dealerArrivalDate)){
                            System.debug('########veiculoTracking.dealerArrivalDate'+veiculoTracking.dealerArrivalDate);
                            tracking.ALR_Date_MADP__c = setStringToDateFormat(veiculoTracking.dealerArrivalDate);
                            
                        }
                        
                        
                        
                        System.debug('#### veiculoTracking.dealerArrivalDate'+veiculoTracking.dealerArrivalDate);
                        //System.debug('####setStringToDateFormat(veiculoTracking.dealerArrivalDate)'+setStringToDateFormat(veiculoTracking.dealerArrivalDate));
                        System.debug('#### ALR_Date_MADP__c'+tracking.ALR_Date_MADP__c );
                        
                        tracking.ALR_CTe__c =  veiculoTracking.CTENumber;
                        if(!String.isBlank(veiculoTracking.typeBlock)){ //&&  veiculoTracking.isBlock!=null){
                            
                            if(veiculoTracking.isBlock.equalsIgnoreCase('y')){// || veiculoTracking.isBlock.equalsIgnoreCase('S')){//) && 
                                //(veiculoTracking.typeBlock != null && !veiculoTracking.typeBlock.equalsIgnoreCase(''))){
                                tracking.ALR_Status_System_Lock__c ='Blocked';
                                tracking.ALR_Lock_Type__c =  veiculoTracking.typeBlock;
                            }
                        }else{
                            tracking.ALR_Status_System_Lock__c =	'Unbloked';
                            tracking.ALR_Lock_Type__c =null;
                        }
                        if(String.isNotBlank(veiculoTracking.statusVehicle)){
                            if(veiculoTracking.statusVehicle.equals('On compound')){
                                tracking.ALR_in_Compound__c = setStringToDateFormat(String.valueOf(now()));
                            }
                            if(veiculoTracking.statusVehicle.equals('on TCC compound')){
                                tracking.ALR_On_TCC_compound__c = setStringToDateFormat(String.valueOf(now()));
                                tracking.ALR_Vehicle_Position__c = 'On the way to the next compound';
                            }
                            if(veiculoTracking.statusVehicle.equals('On the way to the next compound')){
                                tracking.ALR_Way_Next_Compound__c = setStringToDateFormat(String.valueOf(now()));
                            }
                            if(veiculoTracking.statusVehicle.equals('On the way to a dealer')){
                                tracking.ALR_Way_Dealer__c = setStringToDateFormat(String.valueOf(now()));
                            }
                            if(veiculoTracking.statusVehicle.equals('On dealer stock')){
                                tracking.ALR_Dealer_Stock__c = setStringToDateFormat(String.valueOf(now()));
                            }
                            if(veiculoTracking.statusVehicle.equals('Destroyed')){
                                tracking.ALR_Destroyed__c = setStringToDateFormat(String.valueOf(now()));
                            }
                        } 	
                    }catch(Exception e){return 'ERRO: invalid data '+e;}
                    listUpdateTracking.add(tracking);
                }
                else{
                    return 'Number of vin invalid';
                }
            }
            try{
                System.debug('##### listUpdateTracking '+listUpdateTracking);
                update updateVehicle;
                update listUpdateTracking;
                message = 'SUCCESSFUL';
            }catch(Exception e){return 'Erro: Type of data invalid: '+e;}
            
        }
        
        return message;
        
    }
    
    public static Map<String,String> getIdTrackingWithChassi(List<VehicleTracking> lsVehicleTracking){
        //pega os ids do veiculo para fazer uma busca no Tracking assim conseguimos trazer os ids de Tracking
        Map<String,String> mapIdTracking = new Map<String,String>();
        List<String> listIdVehicle = new List<String>();
        
        for(VehicleTracking veihicleTra : lsVehicleTracking)
            if(veihicleTra.vin!=null && !veihicleTra.vin.equals(''))
            listIdVehicle.add(veihicleTra.vin);
        
        for(ALR_Tracking__c tracking : [select id,name from ALR_Tracking__c where ALR_Chassi__c in:(listIdVehicle)]){
            mapIdTracking.put(tracking.Name, tracking.Id);
        }
        return mapIdTracking;
    }
    
    private static List<VEH_Veh__c> getVehicle(List<String> listChassi){
        return [select id,name,ALR_InvoiceNumber__c,ALR_SerialNumberOnInvoice__c, Billing_Date_SAP__c from VEH_Veh__c where name in:(listChassi) ];
    } 
    
    private static Date setStringToDateFormat(String myDate) {
        if(String.isNotBlank(myDate)){
            String[] myDateOnly = myDate.split(' ');
            String[] strDate = myDateOnly[0].split('-');
            Integer myIntDate = integer.valueOf(strDate[2]);
            Integer myIntMonth = integer.valueOf(strDate[1]);
            Integer myIntYear = integer.valueOf(strDate[0]);
            Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
            return d;
        }
        return null;
    }
    
    private static String verificaDatas(String datas){
        Pattern nonWordChar = Pattern.compile('[a-zA-Z]');
        if(nonWordChar.matcher(datas).replaceAll('1223123123123123112312313').length()>10) return nonWordChar.matcher(datas).replaceAll('1223123123123123112312313');
        return datas;
        
    }
    
}