public with sharing class ProspectionManagerController {

	public String statusSelected {get;set;}
	public Id campanhaSelected {get;set;}
	public Id dealerSelected {get;set;}
	public Id sellerSelected {get;set;}
	public Integer qtde {get;set;}
	public Datetime creationDate {get;set;}
    
    public User usuario = Sfa2Utils.obterUsuarioLogado();
	public List<Account> dealersDoGrupo = Sfa2Utils.obterListaConcessionariasDoGrupo(usuario);

    public List<CampaignMember> leadList {get;set;}

    public Lead l {get;set;}

	public ProspectionManagerController() {

        l = new Lead();
        this.leadList = getLeads();

    }

	public List<SelectOption> getStatus(){
		
		List<SelectOption> option = new List<SelectOption>();
        option.add( new SelectOption('', 'Selecionar'));
		option.add(	new SelectOption('attr','Atribuidos'));
		option.add(	new SelectOption('notAttr','Não atribuidos'));

		return option;
	}

	public List<SelectOption> getCampanhas(){

		List<SelectOption> option = new List<SelectOption>();
        option.add( new SelectOption('', 'Selecionar'));
        
		List<Campaign> campList = [SELECT Id, Name 
                                    FROM Campaign 
                                    WHERE IsActive = true
                                    AND Status = 'In Progress' LIMIT 900];

		for(Campaign c: campList){
			option.add(new SelectOption(c.Id, c.Name));
		}

		return option;
	}

	public List<SelectOption> getDealers(){

		List<SelectOption> option = new List<SelectOption>();
        option.add( new SelectOption('', 'Selecionar'));
        
		for(Account c: dealersDoGrupo){
			option.add(new SelectOption(c.Id, c.Name));
		}

		return option;
	}

	public List<SelectOption> getSellers(){
        
        Set<Id> sellers = new Set<Id>();

		List<SelectOption> option = new List<SelectOption>();
        option.add( new SelectOption('', 'Selecionar'));
        
        if(String.isNotEmpty(dealerSelected)){
            List<User> usersId = [SELECT Id, 
                                  Name 
                                  FROM User
                                  WHERE Contact.AccountId =: dealerSelected LIMIT 900];
            
            for(User c: usersId) sellers.add(c.Id);
            
            Map<Id, List<PermissionSet>> psets = Utils.getUserPermissionSetsMap( sellers );
            
            for(User c: usersId){

                Set<String> ownerPermissions = new Set<String>();
    
                for(PermissionSet ps : psets.get(c.Id)) ownerPermissions.add(ps.Name);
    
                if(ownerPermissions.contains('BR_SFA_Seller')) option.add(new SelectOption(c.Id, c.Name)); 

        	}
        }

		return option;
	}

	public List<CampaignMember> getLeads(){

        List<CampaignMember> leads = new List<CampaignMember>();

        System.debug('##### creationDate: ' + l.Birthdate__c);
        System.debug('##### campanhaSelected: ' + campanhaSelected);

        Time t = Time.newInstance(0, 0, 0, 0);

        creationDate = l.Birthdate__c != null ? Datetime.newInstance(l.Birthdate__c, t) : null;

        String query;

        query = 'SELECT Id, ' +
                + 'LeadId, ' +
                + 'Campaign.Name, ' +
                + 'CampaignId, ' +
                + 'Lead.Owner.Name, ' +
                + 'Lead.OwnerId, ' +
                + 'Lead.Name, ' +
                + 'Lead.CreatedDate, ' +
                + 'Lead.VehicleOfInterest__c, ' +
                + 'Lead.Related_campaign__c, ' +
                + 'Lead.DealerOfInterest__c ' +
                + 'FROM CampaignMember ' +
                + 'WHERE Campaign.IsActive = true ' +
                + 'AND Campaign.Status = \'In Progress\' ' +
                + (String.isNotEmpty(campanhaSelected) ? 'AND CampaignId = \'' + campanhaSelected + '\' ' : '') +
                + (String.isNotEmpty(dealerSelected) && statusSelected == 'attr' ? 'AND Lead.DealerOfInterest__c = \'' + dealerSelected + '\' ' : '') +
                + (String.isNotEmpty(sellerSelected) && statusSelected == 'attr' ? 'AND Lead.OwnerId = \'' + sellerSelected + '\' ' : '') +
                + (creationDate != null ? 'AND Lead.CreatedDate >= :creationDate ' : '') +
                + 'LIMIT 999 ';

        leads = Database.query(query);        

		return leads;
	}

    public void filterLeads(){

        this.leadList = getLeads();

        if(String.isNotEmpty(statusSelected)){

            Map<Id, List<PermissionSet>> psets = Utils.getUserPermissionSetsMap( getLeadOwners() );

            if(statusSelected == 'attr'){

                this.leadList = getAttrLeads(psets, this.leadList);

            }

            if(statusSelected == 'notAttr'){

                this.leadList = getNotAttrLeads(psets, this.leadList);

            }

        }

    }

    public List<CampaignMember> getAttrLeads(Map<Id, List<PermissionSet>> psets, List<CampaignMember> leads){

        List<CampaignMember> leadsFiltered = new List<CampaignMember>();

        for(CampaignMember c : leads){

            Set<String> ownerPermissions = new Set<String>();

            for(PermissionSet ps : psets.get(c.Lead.OwnerId)) ownerPermissions.add(ps.Name);

            if(ownerPermissions.contains('BR_SFA_Seller')) leadsFiltered.add(c); 

        }

        return leadsFiltered;

    }

    public List<CampaignMember> getNotAttrLeads(Map<Id, List<PermissionSet>> psets, List<CampaignMember> leads){

        List<CampaignMember> leadsFiltered = new List<CampaignMember>();

        for(CampaignMember c : leads){

            Set<String> ownerPermissions = new Set<String>();

            for(PermissionSet ps : psets.get(c.Lead.OwnerId)) ownerPermissions.add(ps.Name);

            if(!ownerPermissions.contains('BR_SFA_Seller')) leadsFiltered.add(c); 

        }

        return leadsFiltered;

    }
    
    public Set<Id> getLeadOwners(){
            
        Set < Id > owners = new Set < Id >();
        for(CampaignMember p : getLeads()) owners.add(p.Lead.OwnerId);
        
        return owners;
        
    }

    public PageReference decideAttrAction(){


        if(String.isEmpty(statusSelected)){
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR,
                    'Preencha o status dos leads a serem atribuídos ou removidos.'));

            return null;
        }

        if(String.isEmpty(dealerSelected) ||
           String.isEmpty(sellerSelected) ||
           qtde == null){
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR,
                    'Preencha todos os campos da seção "Atribuição".'));

            return null;
        }

        if(qtde == 0){
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR,
                    'A quantidade deve ser maior que zero.'));

            return null;
        }

        if(qtde > leadList.size()){
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR,
                    'A quantidade atribuída/removida deve ser menor ou igual à quantidade de leads.'));

            return null;
        }

        if(statusSelected == 'notAttr'){
            return attrLeads();
        }

        if(statusSelected == 'attr'){
            return removeAttrLeads();
        }

        return null;

    }

    public PageReference attrLeads(){

        List<Lead> toUpdate = new List<Lead>();

        for(Integer i = 0; i < qtde; i++){

            Lead l = new Lead();
            l.Id = leadList[i].LeadId;
            l.DealerOfInterest__c = dealerSelected;
            l.OwnerId = sellerSelected;
            toUpdate.add(l);
        }

        return updateLeads(toUpdate);
    }

     public PageReference removeAttrLeads(){

        List<Lead> toUpdate = new List<Lead>();

        for(Integer i = 0; i < qtde; i++){

            Lead l = new Lead();
            l.Id = leadList[i].LeadId;
            l.DealerOfInterest__c = null;
            l.OwnerId = UserInfo.getUserId();
            toUpdate.add(l);
        }

        return updateLeads(toUpdate);

    }

    public PageReference updateLeads(List<Lead> toUpdate){

        String errors = '';

        try{

            Database.SaveResult[] sr = Database.update(toUpdate, false);
            List<Database.Error> errorsList = new List<Database.Error>(); 

            for(Database.SaveResult s : sr){
                if(!s.isSuccess()) errorsList.addAll(s.getErrors());
            }

            for(Database.Error e : errorsList){
                errors += e.getMessage() + '\n';
            }

            if(errors != ''){

                 ApexPages.addMessage( new ApexPages.Message(
                 ApexPages.Severity.ERROR,
                     errors));

                 return null;

            }

            filterLeads();

            ApexPages.addMessage( new ApexPages.Message(
                 ApexPages.Severity.CONFIRM,
                     'Sucesso!.'));


        } catch(Exception e){

            ApexPages.addMessage( new ApexPages.Message(
             ApexPages.Severity.ERROR,
                 e.getMessage()));
        }

        return null;

    }

    public PageReference deleteList(){

        if(String.isEmpty(statusSelected) ||
            statusSelected == 'attr'){
            ApexPages.addMessage( new ApexPages.Message(
                ApexPages.Severity.ERROR,
                    'Apenas listas de leads não atribuídos podem ser apagadas.'));

            return null;
        }

        try{

            filterLeads();

            List<Lead> toDelete = new List<Lead>();

            for(CampaignMember c : this.leadList){

                Lead l = new Lead();
                l.Id = c.LeadId;
                toDelete.add(l);

            }

            delete toDelete;

            filterLeads();

            ApexPages.addMessage( new ApexPages.Message(
                 ApexPages.Severity.CONFIRM,
                     'Sucesso!.'));


        } catch(Exception e){

            ApexPages.addMessage( new ApexPages.Message(
             ApexPages.Severity.ERROR,
                 e.getMessage()));
        }

        return null;

    }
    
    public PageReference newLead(){
        
        return new PageReference( '/00Q/e?retURL=%2F00Q%2Fo&RecordType=' + Utils.getRecordTypeId('Lead', 'DVR') + '&ent=Lead' );
        
    }
}