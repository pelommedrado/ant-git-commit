public with sharing class VFC17_CustomerActivityEntity
{
	/* get the lead record and store in leadRecord */
	public Lead leadRecord {get;set;}
	/* get the lead Id and store in leadId	*/
	public Id leadId {get;set;}
	/* get the lead record and store in leadInfo */
	public Lead leadInfo {get;set;}
	/* Boolean value to show the field testDriveHeld from Lead. */
	public Boolean testDriveHeld {get; set;} 
	/* Boolean value to show the field optInDealership from Lead. */
	public Boolean optInDealership {get; set;}
	/* Boolean value to show the field receiveNewsByPhone from Lead. */
	public Boolean receiveNewsbyPhone {get; set;} 
	/* Boolean value to show the field receiveNewsviaEmail from Lead. */
	public Boolean receiveNewsviaEmail {get; set;}
	/* Boolean value to show the field receiveUpdateviaSMS from Lead. */
	public Boolean receiveUpdateviaSMS {get; set;}
	/* get the recommendation value store in recommendationValue1 */
	public String recommendationValue1 {get; set;}
	/* get the recommendation id store in recommendationValueId1 */
	public String recommendationValueId1 {get; set;}
	/* get the recommendation value store in recommendationValue2 */
	public String recommendationValue2 {get; set;}
	/* get the recommendation id value store in recommendationValueId2 */
	public String recommendationValueId2 {get; set;}
	/* get the recommendation value store in recommendationValue3 */
	public String recommendationValue3 {get; set;}
	/* get the recommendation id store in recommendationValueId3 */
	public String recommendationValueId3 {get; set;}
	/* Boolean value to show the field recommendation1 */
	public Boolean recommendation1 {get; set;}
	/* Boolean value to show the field recommendation2 */
	public Boolean recommendation2 {get; set;}
	/* Boolean value to show the field recommendation3 */
	public Boolean recommendation3 {get; set;}
	/* Boolean value to show the field recommendation1 */
	public Boolean chkRecommendation1 {get; set;}
	/* Boolean value to show the field recommendation2 */
	public Boolean chkRecommendation2 {get; set;}
	/* Boolean value to show the field recommendation3 */
	public Boolean chkRecommendation3 {get; set;}
	/* disable or enable the value enableWhyNotContact */
	public Boolean enableWhyNotContact {get; set;}
	/* disable or enable the value enableWhatKM */
	public Boolean enableWhatKM {get; set;}
	/* disable or enable the value enableHasVehicle */
	public Boolean enableHasVehicle {get; set;}
	/* disable or enable the value enableBrand */
	public Boolean enableBrand {get; set;}
	/* disable or enable the value enableModel */
	public Boolean enableModel {get; set;}
	/* disable or enable the value enableBrandCompetitive */
	public Boolean enableBrandCompetitive {get; set;}
	/* disable or enable the value enableWhynotOpp */
	public Boolean enableWhynotOpp {get; set;}
	/* disable or enable the value enableLeadStatus */
	public Boolean enableLeadStatus {get; set;}
	/* disable or enable the value enableEvent */
	public Boolean enableEvent {get; set;}
	/* disable or enable the value enableTestDriveModel */
	public Boolean enableTestDriveModel {get; set;}
	/* disable or enable the value enableTestDriveHeld */
	public Boolean enableTestDriveHeld {get; set;}
	/* disable or enable the value enableTestDriveDate */
	public Boolean enableTestDriveDate {get; set;}
	/* disable or enable the value enableOptInDealer */
	public Boolean enableOptInDealer {get; set;}
	/* get recommendation id and description store to mapRecommendationInfo */
	public map<Id,String> mapRecommendationInfo {get; set;}
	/* get recommendation id and name store to mapRecommendationInfoName */
	public map<Id,String> mapRecommendationInfoName {get; set;}
	/* get recordType id and Id store to mapRecommendationInfoRecordType */
	public map<Id,String> mapRecommendationRecordType {get; set;}
	/* Non Recommendation display info message */ 
	public String infoMessageforNonRecommendations {get; set;}
	public Boolean noRecommendationFoundCondition {get; set;}
	/* get molicar brands store to setBrandDetails */
	public List<String> setBrandDetails {get; set;} 
	/* get molicar models store to lstModelDetails */
	public List<String> lstModelDetails {get; set;}
	/* get non customer decision tree data store to lstNonCustomerTreeDetails */
	public List<NCT_NonCustomerDecisionTree__c> lstNonCustomerTreeDetails {get; set;}
	/* get customer decision tree data store to lstCustomerTreeDetails */
	public List<CDT_CustomerDecisionTree__c> lstCustomerTreeDetails {get; set;}
	/* get relationship decision tree data store to lstRelationshipTreeDetails */
	public List<RDT_RelationshipDecisionTree__c> lstRelationshipTreeDetails {get; set;}
	/* get After sales decision tree data store to lstAfterSalesTreeDetails */
	public List<AST_AfterSalesDecisionTree__c> lstAfterSalesTreeDetails {get; set;}
	/* get Transaction data store to lstTransactionRecords */
	public List<TST_Transaction__c> lstTransactionRecords {get; set;}
	/* get value from Transaction object */
	public TST_Transaction__c transactionRecord {get;set;}
	/* get the brand value to store thisBrand */ 
	public String thisBrand {get;set;}
	/* get the model value to store thisModel */
	public String thisModel {get;set;}
	/* get the status value to store leadStatus */
	public String leadStatus {get;set;}
	/* Store Transaction record ids.*/
	public Set<Id> transactionRecommendationIds {get; set;}
	/* Store Quarentine Period value.*/
	public Integer quarentinePeriod {get; set;}
	
	public Boolean enabledReferenceAddress {get; set;}
	
	// Campaign
	public String campaignId {get; set;}
	public List<SelectOption> lstSelectOptionCampaigns {get;set;}
	
	// Vehicle list from object
	public List<SelectOption> lstSelectOptionVehicles {get;set;}
	
	/* Constructor */
	public VFC17_CustomerActivityEntity()
	{
		this.leadRecord = new Lead();
		this.transactionRecommendationIds = new Set<Id>();
		this.quarentinePeriod = 0;
		this.leadInfo = new Lead();
		this.testDriveHeld = false;
		this.optInDealership = false;
		this.receiveNewsbyPhone = false; /* Boolean value to show the field receiveNewsByPhone from Lead. */ 
		this.receiveNewsviaEmail = false;
		this.receiveUpdateviaSMS = false;
		this.recommendationValue1 = '';
		this.recommendationValueId1 = '';
		this.recommendationValue2 = '';
		this.recommendationValueId2 = '';
		this.recommendationValue3 = '';
		this.recommendationValueId3 = '';
		this.noRecommendationFoundCondition = true;
		this.infoMessageforNonRecommendations = '';
		this.recommendation1 = false;
		this.recommendation2 = false;
		this.recommendation3 = false;
		this.chkRecommendation1 = false;
		this.chkRecommendation2 = false;
		this.chkRecommendation3 = false;
		this.enableWhyNotContact = false;
		this.enableWhatKM = false;
		this.enableHasVehicle = false;
		this.enableBrand = false;
		this.enableModel = false;
		this.enableBrandCompetitive = false;
		this.enableWhynotOpp = false;
		this.enableLeadStatus = false;
		this.enableEvent = false;
		this.enableTestDriveModel = false;
		this.enableTestDriveHeld = false;
		this.enableTestDriveDate = false;
		this.enableOptInDealer = false;
		this.mapRecommendationInfo = new map<Id,String>();
		this.mapRecommendationInfoName = new map<Id,String>();
		this.mapRecommendationRecordType = new map<Id,String>();
		this.setBrandDetails = new List<String>();
		this.lstModelDetails = new List<String>();
		this.lstNonCustomerTreeDetails = new List<NCT_NonCustomerDecisionTree__c>();
		this.lstCustomerTreeDetails = new List<CDT_CustomerDecisionTree__c>();
		this.lstRelationshipTreeDetails = new List<RDT_RelationshipDecisionTree__c>();
		this.lstAfterSalesTreeDetails = new List<AST_AfterSalesDecisionTree__c>();
		this.lstTransactionRecords = new List<TST_Transaction__c>();
		this.transactionRecord = new TST_Transaction__c();
		this.thisBrand = '';
		this.thisModel = '';
		this.leadStatus = '';
		this.enabledReferenceAddress = false;
		
		// Campaign
		this.campaignId = '';
		this.lstSelectOptionCampaigns = new List<SelectOption>();
		
		this.lstSelectOptionVehicles = new List<SelectOption>(); 
	}
}