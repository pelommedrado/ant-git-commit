public class VFC121_CampaignDAO  // With or without <=================================================================
{
	private static final VFC121_CampaignDAO instance = new VFC121_CampaignDAO();
    
	private VFC121_CampaignDAO(){}

    public static VFC121_CampaignDAO getInstance(){
        return instance;
    }


	/**
     *  Fetch a map of Campaign Parent records, where the key is the DBMCampaignCode__c, 
     *  given a set of DBMCampaignCode__c
     *  @param setDBMCampaignCode: a set of DBMCampaignCode__c
     *  @return mapCampaign
    **/
    public Map<String, Campaign> fetchCampaignParentByDBMCampaignCodeSet(Set<String> setDBMCampaignCode)
    {
        Map<String, Campaign> mapCampaign = new Map<String, Campaign>();       

        List<Campaign> lstCampaign = [SELECT Id, Name, DBMCampaignCode__c, RecordTypeId
                                        FROM Campaign 
									   WHERE RecordType.DeveloperName = 'ParentCampaign'
									     AND DBMCampaignCode__c IN :setDBMCampaignCode];
        for (Campaign campaign : lstCampaign)
        {
            mapCampaign.put(campaign.DBMCampaignCode__c, campaign);
        }
        return mapCampaign;
    }

	/**
     *  Fetch a map of Campaign Single records, where the key is the DBMCampaignCode__c, 
     *  given a set of DBMCampaignCode__c
     *  @param setDBMCampaignCode: a set of DBMCampaignCode__c
     *  @return mapCampaign
    **/
    public Map<String, Campaign> fetchCampaignSingleByDBMCampaignCodeSet(Set<String> setDBMCampaignCode)
    {
        Map<String, Campaign> mapCampaign = new Map<String, Campaign>();       

        List<Campaign> lstCampaign = [SELECT Id, Name, DBMCampaignCode__c, RecordTypeId
                                        FROM Campaign 
									   WHERE RecordType.DeveloperName = 'SingleCampaign'
									     AND DBMCampaignCode__c IN :setDBMCampaignCode];
        for (Campaign campaign : lstCampaign)
        {
            mapCampaign.put(campaign.DBMCampaignCode__c, campaign);
        }
        return mapCampaign;
    }

	/**
     *  Fetch a map of Campaigns records, where the key is the Id, given a set of Ids
     *  @param setIds: a set of Ids
     *  @return mapCampaign
    **/
    public Map<Id, Campaign> fetchCampaignByIdSet(Set<Id> setIds)
    {
        Map<Id, Campaign> mapCampaign = new Map<Id, Campaign>();       

        List<Campaign> lstCampaign = [SELECT Id, Name, DBMCampaignCode__c, RecordTypeId, type,
        									 isActive, Status, StartDate, EndDate, DealerInclusionAllowed__c,
        									 DealerShareAllowed__c, ManualInclusionAllowed__c, 
        									 PAActiveShareAllowed__c, ParentId, WebToOpportunity__c
                                        FROM Campaign 
									   WHERE Id IN :setIds];
        for (Campaign campaign : lstCampaign)
        {
            mapCampaign.put(campaign.Id, campaign);
        }
        return mapCampaign;
    }
    
    /**
     *  Fetch a Campaigns records given his Ids
     *  @param Id: campaign's Id
     *  @return Campaign object
    **/
    public Campaign fetchCampaignById(Id campaignId)
    {
		try
		{
        	Campaign campaign = [SELECT Id, Name, DBMCampaignCode__c, RecordTypeId, Description, Type,
    									 isActive, Status, StartDate, EndDate, DealerInclusionAllowed__c,
    									 DealerShareAllowed__c, ManualInclusionAllowed__c, 
    									 PAActiveShareAllowed__c, ParentId
                                    FROM Campaign 
								   WHERE Id = :campaignId];
 	       return campaign;
		}
		catch (Exception e)
		{
			system.debug(LoggingLevel.ERROR, '*** campaign not found ['+campaignId+']');
			return null;
		}
    }
    
    /**
     *  Fetch a map of children campaigns records list, where the key is the parent campaignId, given a set of parent Ids
     *  @param setIds: a set of Ids
     *  @return mapCampaign of List<Campaign>
    **/
    public Map<Id, List<Campaign>> fetchChildrenCampaignListByIdSet(Set<Id> setIds)
    {
        Map<Id, List<Campaign>> mapCampaign = new Map<Id, List<Campaign>>();       

        List<Campaign> lstAllCampaign = [SELECT Id, Name, ParentId, RecordTypeId
                                           FROM Campaign 
								    	  WHERE ParentId IN :setIds];
        for (Campaign campaign : lstAllCampaign)
        {
            if (!mapCampaign.containsKey(campaign.ParentId))
            	mapCampaign.put(campaign.ParentId, new List<Campaign>{campaign});
            else
            {
            	List<Campaign> lstCampaign = mapCampaign.get(campaign.ParentId);
            	lstCampaign.add(campaign);
            	mapCampaign.put(campaign.ParentId, lstCampaign);
            }
        }
        return mapCampaign;
    }
    
    /**
     * Build a query for Campaign batch process
     */
    public String buildCampaignQueryForBatchProcess()
    {
    	String query = 'SELECT Id, Name, ParentId, RecordTypeId, Status, StartDate, EndDate, IsActive'
                     + ' FROM Campaign'
				     + ' WHERE Status IN (\'Planned\', \'In Progress\')'
				     + '   AND ParentId = null';
	    return query;
    }
    
    /**
     * Fetch all active campaigns that belongs to a Dealer or to everyone (dealer == null)
     * 3th rule for Dealer
     */
    public List<Campaign> fetchCampaignForDealer(Id accountDealerId)
    {
    	List<Campaign> lstCampaign = 
    		[SELECT Id, Name, ParentId, Type, RecordTypeId, Status, StartDate, EndDate, IsActive, 
    				Description, CampaignMemberRecordTypeId, Dealer__c, Vehicle__c, DBMCampaignCode__c, 
    				ID_BIR__c, DealerShareAllowed__c, PAActiveShareAllowed__c, DealerInclusionAllowed__c, 
    				ManualInclusionAllowed__c
			   FROM Campaign
		      WHERE IsActive = true
				AND RecordType.DeveloperName = 'SingleCampaign'
				AND (Dealer__c = :accountDealerId OR Dealer__c = null)
				AND DealerShareAllowed__c = true];
    	return lstCampaign;
    }
    
	/**
     * Fetch all active campaigns that belongs to a Dealer or to everyone (dealer == null)
     * 3th rule for Heating page (PA)
     */
    public List<Campaign> fetchCampaignForPA(Id accountDealerId)
    {
    	List<Campaign> lstCampaign = 
    		[SELECT Id, Name, ParentId, Type, RecordTypeId, Status, StartDate, EndDate, IsActive, 
    				Description, CampaignMemberRecordTypeId, Dealer__c, Vehicle__c, DBMCampaignCode__c, 
    				ID_BIR__c, DealerShareAllowed__c, PAActiveShareAllowed__c, DealerInclusionAllowed__c, 
    				ManualInclusionAllowed__c
			   FROM Campaign
		      WHERE IsActive = true
				AND RecordType.DeveloperName = 'SingleCampaign'
				AND (Dealer__c = :accountDealerId OR Dealer__c = null)
				AND PAActiveShareAllowed__c = true];
    	return lstCampaign;
    }
}