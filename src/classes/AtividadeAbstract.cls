public abstract class AtividadeAbstract {

	public String selectedBrand   		{ get;set; }
    public String selectedModel   		{ get;set; }

    public List<String> modelDetailList { get;set; }

    public Boolean isTask		  		{ get;set; }
	public Boolean isEvent		  		{ get;set; }
	public Boolean createNext 			{ get;set; }
	public Boolean isVehicleOwner 		{ get;set; }

    public User u {
    	get {
    		return [select Id, Name from User where id =: UserInfo.getUserId()];
    	}
    }

    public String previsao { get;set; }
    public List<SelectOption> previsaoList {
        get {
            List<SelectOption> selectList = new List<SelectOption>();
            selectList.add(new SelectOption('Within next 1 year', 'Dentro de 1 ano'));
            selectList.add(new SelectOption('Within next 2 years', 'Dentro de 2 ano'));
            selectList.add(new SelectOption('Within next 3 years', 'Dentro de 3 ano'));
            selectList.add(new SelectOption('Within next 4 years', 'Dentro de 4 ano'));
            selectList.add(new SelectOption('Within next 5 years', 'Dentro de 5 ano'));
            selectList.add(new SelectOption('Within next 6 months', 'Dentro de 6 meses'));
            selectList.add(new SelectOption('3 - 6 Months', 'Entre 3 e 6 meses'));
            selectList.add(new SelectOption('No Information', 'Sem Informação'));
            selectList.add(new SelectOption('No Intention', 'Sem Intenção'));
            return selectList;
        }
    }

	//MÉTODO PARA BUSCAR TAREFA/EVENTO
	public ActivityMap getAct(Id actId) {

		this.isTask  = false;
		this.isEvent = false;

		List<Task> t 	= new List<Task>();
        List<Event> e 	= new List<Event>();
        ActivityMap actMap 	= new ActivityMap();

        t = [
            SELECT Id, Owner.Name, Subject, Reason__c, Status, Number_of_attempts__c,
            Description, ActivityDatetime__c, ReminderDateTime,
            IsReminderSet, WhatId, WhoId
            FROM Task where id =: actId
        ];

		if(!t.isEmpty()){

			this.isTask = true;

			actMap.t 				= t[0];
			actMap.owner 			= t[0].Owner.Name;
			actMap.subject 			= t[0].Subject;
			actMap.reason 			= t[0].Reason__c;
    	    actMap.status 			= t[0].Status;
            actMap.description 		= t[0].Description;
            actMap.activityDate 	= t[0].ActivityDatetime__c.format('dd/MM/yyyy');
			actMap.activityHour 	= ( t[0].ReminderDateTime != null ? t[0].ReminderDateTime.format('HH:mm') : '00:00' );
			actMap.reminderDatetime = ( t[0].ReminderDateTime != null ? getUserDatetime(t[0].ReminderDateTime) : null );
			actMap.reminder 		= t[0].IsReminderSet;
			actMap.whoId			= t[0].WhoId;
			actMap.whatId			= t[0].WhatId;
			actMap.numTentativa		= t[0].Number_of_attempts__c;
        }

        e = [
            SELECT Id, Owner.Name, Subject, Status__c, Reason__c, Number_of_attempts__c,
            Description, StartDateTime, ReminderDateTime,
            IsReminderSet, WhatId, WhoId
            FROM Event WHERE id =: actId
        ];

		if(!e.isEmpty()){
			System.debug('HHH:' + e[0].StartDateTime.format('HH:mm'));
			this.isEvent = true;

			actMap.e 					= e[0];
			actMap.owner 				= e[0].Owner.Name;
			actMap.subject 				= e[0].Subject;
			actMap.reason 				= e[0].Reason__c;
			actMap.status 				= e[0].Status__c;
			actMap.description 			= e[0].Description;
			actMap.activityDate			= getUserDatetime(e[0].StartDateTime).format('dd/MM/yyyy');
			actMap.activityHour 		= e[0].StartDateTime.format('HH:mm');
			actMap.reminderDatetime 	= getUserDatetime(e[0].ReminderDateTime);
			actMap.reminder 			= e[0].IsReminderSet;
			actMap.whoId 				= e[0].WhoId;
			actMap.whatId				= e[0].WhatId;
            actMap.numTentativa			= e[0].Number_of_attempts__c;
		}
		return actMap;
	}

	//CARREGA MARCAS
	public List<Selectoption> getBrands() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', '--Nenhum--'));

        List<String> brandsDetailList = getBrandDetails();
            for (String molicar : brandsDetailList) {
                option.add(new SelectOption(molicar, molicar));
        }

        return option;
    }

    //BUSCA MARCAS
    public List<String> getBrandDetails() {
        List<String> brandsDetailList = new List<String>();
        AggregateResult[] agrMolicarBrandDetails =
            VFC11_MolicarDAO.getInstance().findMolicarBrand();

        if(!agrMolicarBrandDetails.isEmpty()) {
            for(AggregateResult Brand : agrMolicarBrandDetails) {
                // To avoid duplication add Brand values to set list of Brands.
                brandsDetailList.add(String.valueOf(Brand.get('Brand__c')));
            }
        }

        return brandsDetailList;
    }

    //CARREGA MODELOS
    public List<Selectoption> getModels() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', '--Nenhum--'));

        //check Brand name must not be null and the model names list also not equalto null
        if( (selectedBrand != null || selectedBrand != '') && modelDetailList != null) {
            if(!modelDetailList.isEmpty()) {
                for (String model : this.modelDetailList) {
                    option.add(new SelectOption(model, model));
                }
            }
        }

        return option;
    }

    //BUSCA MODELOS
    public void getModelDetails() {
        this.modelDetailList = new List<String>();
        List<MLC_Molicar__c> lstMolicarModels = new List<MLC_Molicar__c>();

        //get Brand values from Molicar object using selected Brand Id
        lstMolicarModels =
            VFC11_MolicarDAO.getInstance().findMolicarByBrand(this.selectedBrand);

        if(!lstMolicarModels.isEmpty()) {
            for (MLC_Molicar__c molicar : lstMolicarModels) {
                this.modelDetailList.add(molicar.Model__c);
            }
        }
    }

    //MONTA SELEÇÃO DE HORAS
    public List<SelectOption> getHourList(){

    	List<SelectOption> option = new List<SelectOption>();
    	option.add(new SelectOption('', '00:00'));

    	Datetime initHour = Datetime.newInstance(System.today(), Time.newInstance(00, 15, 0, 0));
    	Datetime limitHour = Datetime.newInstance(System.today(), Time.newInstance(23, 45, 0, 0));

    	while(initHour < limitHour){
    		option.add(new SelectOption(initHour.format('HH:mm'), initHour.format('HH:mm')));

    		initHour = initHour.addMinutes(15);
    	}

    	return option;

    }

    //MONTA SELEÇÃO DE STATUS
    public List<SelectOption> getStatusList(){

  		List<SelectOption> options = new List<SelectOption>();
  		options.add( new SelectOption('', '--Nenhum--'));
   		Schema.DescribeFieldResult fieldResult = Event.Status__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

	   for( Schema.PicklistEntry f : ple){
	      options.add(new SelectOption(f.getValue(), f.getLabel()));
	   }

	   return options;
	}

	//MONTA SELEÇÃO DE ASSUNTO
	public List<SelectOption> getSubjectList(){

  		List<SelectOption> options = new List<SelectOption>();
  		options.add( new SelectOption('', '--Nenhum--'));

   		Schema.DescribeFieldResult fieldResult = Event.Subject.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

	   for( Schema.PicklistEntry f : ple){
	      options.add(new SelectOption(f.getValue(), f.getLabel()));
	   }

	   return options;
	}

	public List<SelectOption> getReasonList(){

  		List<SelectOption> options = new List<SelectOption>();
  		options.add( new SelectOption('', '--Nenhum--'));

   		Schema.DescribeFieldResult fieldResult = Event.Reason__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

	   for( Schema.PicklistEntry f : ple) {
	      options.add(new SelectOption(f.getValue(), f.getLabel()));
	   }

	   return options;
	}

    public String saveRecord(ActivityMap actToSave) {
        System.debug('saveRecord()');

        String toRedirect = null;

        //BUSCA CUSTOM LABEL COM OS ASSUNTOS PARA DECIDIR SE CRIA EVENTO OU TAREDA
        String taskSubjects = Label.agendaTarefas;
        String eventSubjects = Label.agendaEventos;

        //CUSTOM LABEL COM ASSUNTOS QUE NÃO DEVEM SALVAR COMO REALIZADOS NESTE PASSO, MAS PELO FLUXO
        String doNotSave = Label.agendaDoNotComplete;

        this.isTask  = ( taskSubjects.contains(actToSave.subject)  ? true : false );
        this.isEvent = ( eventSubjects.contains(actToSave.subject) ? true : false );

        if(isTask) {
            final Task taskToSave = mapActivityToTask(actToSave);

            if(doNotSave.containsIgnoreCase(actToSave.subject) &&
               actToSave.status == 'Completed' && actToSave.t != null) {
                   toRedirect = setRedirectPage(actToSave);

               } else /*if(!doNotSave.containsIgnoreCase(actToSave.subject)) */{
                   UPSERT taskToSave;
                   toRedirect = setRedirectPage(actToSave);
               }
        }

        if(isEvent) {
            final Event eventToSave = mapActivityToEvent(actToSave);

            if(doNotSave.containsIgnoreCase(actToSave.subject) &&
               actToSave.status == 'Completed' && actToSave.e != null) {
                   toRedirect = setRedirectPage(actToSave);

               } else /*if(!doNotSave.containsIgnoreCase(actToSave.subject))*/ {

                   UPSERT eventToSave;
                   toRedirect = setRedirectPage(actToSave);
               }
        }
        return toRedirect;
    }

    public Task mapActivityToTask(ActivityMap actToSave) {
        System.debug('mapActivityToTask() ' + actToSave);
        Task tas 		 = ( actToSave.t != null ? actToSave.t : new Task() );
        tas.OwnerId 	 = this.u.Id;
        tas.Subject 	 = actToSave.subject;
        tas.Reason__c	 = actToSave.reason;
        tas.Status	 	 = actToSave.status;
        tas.Description  = actToSave.description;
        tas.WhatId		 = actToSave.whatId;
        tas.WhoId		 = actToSave.whoId;
        tas.ActivityDatetime__c = Date.newInstance(
            Integer.valueOf(actToSave.activityDate.split('/')[2]),
            Integer.valueOf(actToSave.activityDate.split('/')[1]),
            Integer.valueOf(actToSave.activityDate.split('/')[0]));

        Integer h = Integer.valueOf(actToSave.activityHour.split(':')[0]);
        Integer m = Integer.valueOf(actToSave.activityHour.split(':')[1]);
        Time ti = Time.newInstance(h, m, 0, 0).addMinutes(-15);
        tas.ReminderDateTime = Datetime.newInstance(tas.ActivityDatetime__c.date(), ti);
        tas.IsReminderSet 		= actToSave.reminder;

        tas.isTestDrivePerformed__c = false;
        if(actToSave.isTestDrivePerformed != null) {
            tas.isTestDrivePerformed__c = actToSave.isTestDrivePerformed;
        }

        tas.WhyTestDriveNotPerformed__c 	= actToSave.whyTestDriveNotPerformed;
        tas.AttendanceSatisfactionLevel__c 	= actToSave.attendanceSatisfactionLevel;
        tas.OppLossReason__c 				= actToSave.oppLossReason;
        tas.Comment__c						= actToSave.comment;
        tas.Number_of_attempts__c			= actToSave.numTentativa;
        tas.vehicleDeliveredCombinedDate__c = actToSave.vehicleDeliveredCombinedDate;
        return tas;
    }

    public Event mapActivityToEvent(ActivityMap actToSave) {
        System.debug('mapActivityToEvent()' + actToSave);
        Event even 			= ( actToSave.e != null ? actToSave.e : new Event() );
        even.OwnerId 		= this.u.Id;
        even.Subject 		= actToSave.subject;
        even.Reason__c		= actToSave.reason;
        even.Status__c 		= actToSave.status;
        even.Description 	= actToSave.description;
        even.WhatId			= actToSave.whatId;
        even.WhoId			= actToSave.whoId;
        even.StartDateTime 	= Datetime.newInstance(
            Integer.valueOf(actToSave.activityDate.split('/')[2]),
            Integer.valueOf(actToSave.activityDate.split('/')[1]),
            Integer.valueOf(actToSave.activityDate.split('/')[0]),
            Integer.valueOf(actToSave.activityHour.split(':')[0]),
            Integer.valueOf(actToSave.activityHour.split(':')[1]),
            0);
				even.ActivityDatetime__c = even.StartDateTime;
        even.ReminderDateTime 	= even.StartDateTime.addMinutes(-15);
        even.IsReminderSet 		= actToSave.reminder;
        even.DurationInMinutes 	= 0;
        even.Number_of_attempts__c			= actToSave.numTentativa;
        return even;
    }

    public String setRedirectPage(ActivityMap actToSave) {
        System.debug('actToSave: ' + actToSave);

        final String assunto = actToSave.subject;
        final Boolean compl = actToSave.status == 'Completed';

        final Set<String> valueSet = new Set<String>();
        valueSet.add('Rescue');
        valueSet.add('Persecution');
        valueSet.add('Delivery Confirmation');

        if(valueSet.contains(assunto)) {
            return 'DashboardTask?profile=BDC';
        }

        if(assunto.equalsIgnoreCase('Perform Test Drive') && compl) {
            final List<Quote> quoteList = [
                SELECT Id FROM Quote WHERE OpportunityId =: actToSave.whatId AND Status != 'Canceled'
                ORDER BY LastModifiedDate DESC
            ];

            if(!quoteList.isEmpty()) {
                final Quote qu = quoteList.get(0);
                return 'ClienteTestDriveSfa2?Id=' + actToSave.whatId + '&quoteId=' + qu.Id;

            } else {

                //Opportunity oppToQuote = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(actToSave.whatId);
                VFC61_QuoteDetailsVO orc = Sfa2Utils.criarOrcamento(actToSave.whatId, null);
                return 'ClienteTestDriveSfa2?Id=' + actToSave.whatId + '&quoteId=' + orc.Id;
                /*ApexPages.addMessage( new ApexPages.Message(
                    ApexPages.Severity.ERROR, 'É necessária uma cotação para realizar' +
                    ' esta atividade. Por favor, siga com a negociação para dar continuidade.'));

                return null;*/
            }

               /*if([select id from Quote  where OpportunityId =: actToSave.whatId order by LastModifiedDate desc].size() > 0) {

                   return 'ClienteTestDriveSfa2?Id=' + actToSave.whatId + '&quoteId=' + [select id
                                                                                         from Quote
                                                                                         where OpportunityId =: actToSave.whatId
                                                                                         order by LastModifiedDate desc].Id;

               } else {

                   ApexPages.addMessage( new ApexPages.Message(
                       ApexPages.Severity.ERROR,
                       'É necessária uma cotação para realizar esta atividade. Por favor, siga com a negociação para dar continuidade.'));

                   return null;
               }*/
        }

        if(assunto.equalsIgnoreCase('Perform Visit') && compl) {
            return 'ClienteOportunidadeSfa2?Id=' + actToSave.whatId;

        } else if(assunto.equalsIgnoreCase('Customer Service') && compl) {
            return 'ClienteOportunidadeSfa2?Id=' + actToSave.whatId;

        } else if(assunto.equalsIgnoreCase('Prospection Attendance') && compl) {
            return 'ClienteOportunidadeSfa2?Id=' + actToSave.whatId;

        } else if(assunto.equalsIgnoreCase('Negotiation') && compl) {
            return 'ClienteOportunidadeDetalheSfa2?Id=' + actToSave.whatId;
        }

        return 'DashboardTask';
    }

	public class ActivityMap {
		public String owner 			 { get;set; }
		public String subject			 { get;set; }
		public String status			 { get;set; }
		public String reason			 { get;set; }
		public String description 		 { get;set; }
		public String activityDate 		 { get;set; }
		public String activityHour 		 { get;set; }
		public Datetime reminderDateTime { get;set; }
		public Boolean reminder 		 { get;set; }
		public Id whatId 				 { get;set; }
		public Id whoId 				 { get;set; }
        public Decimal numTentativa		 { get;set; }

		public Double attendanceSatisfactionLevel { get;set; }
		public Boolean isTestDrivePerformed		  { get;set; }
		public String whyTestDriveNotPerformed	  { get;set; }
		public String oppLossReason				  { get;set; }
        public String comment					  { get;set; }
        public String vehicleDeliveredCombinedDate { get;set; }
		public Opportunity opp	{ get;set; }
		public Task t 			{ get;set; }
		public Event e 			{ get;set; }
	}

    public Datetime getUserDatetime(Datetime dt) {
        return dt.addSeconds(UserInfo.getTimeZone().getOffset(dt) / 1000);
    }
}