public class MonthlyGoalDealer_Controller {
    
    public static void createFromAcc(List<Account> dealers){
        
        System.debug('*******createFromAcc()');
        System.debug('dealers: ' + dealers);
        
        Set<String> setMatrix = new Set<String>();
        Set<String> setExistingMatrix = new Set<String>();
        
        List<Monthly_Goal_Group__c> groups = new List<Monthly_Goal_Group__c>();
        List<Monthly_Goal_Dealer__c> dealersGoal = new List<Monthly_Goal_Dealer__c>();
        List<Monthly_Goal_Group__c> groupsToInsert = new List<Monthly_Goal_Group__c>();
        
        //get distinct matrix from accounts
        for(Account acc : dealers){
            if(acc.RecordTypeId == Utils.getRecordTypeId('Account', 'Network_Site_Acc') && acc.Dealer_Matrix__c != null){
                setMatrix.add(acc.Dealer_Matrix__c);
            }
        }
        
        System.debug('setMatrix: ' + setMatrix);
        
        groups = [SELECT Id, Matrix_Bir__c
                  FROM Monthly_Goal_Group__c
                  WHERE Matrix_Bir__c IN : setMatrix
                  AND Month__c =: System.now().format('MMMMM')
                  AND Year__c =: System.today().year()];
        
        System.debug('groups: ' + groups);
        
        for(Monthly_Goal_Group__c mgg : groups){
            setExistingMatrix.add(mgg.Matrix_Bir__c);
        }
        
        System.debug('setExistingMatrix: ' + setExistingMatrix);
        
        for(String sets : setMatrix){
            if(!setExistingMatrix.contains(sets)){
                Monthly_Goal_Group__c g = new Monthly_Goal_Group__c();
                g.Matrix_Bir__c = sets;
                g.Year__c = System.today().year();
                g.Month__c = System.now().format('MMMMM');
                groupsToInsert.add(g);
                
            }
        }
        
        System.debug('groupsToInsert: ' + groupsToInsert);
        
        /*
		 *  INSERE OS GRUPOS QUE NÃO EXISTEM AINDA ANTES DE INSERIR AS CONCESSIONÁRIAS
		 * 
		 * */
        
        Savepoint sp1 = Database.setSavepoint();
        
        try{
            Database.insert(groupsToInsert);
        } catch(Exception e){
            System.debug('############ erro ao inserir grupo: ' + e);
            Database.rollback(sp1);
        }
        
        /*
		 * APÓS INSERIR GRUPOS QUE AINDA NÃO EXISTIAM, INSERE AS CONCESSIONÁRIAS
		 * 
		 * */
        
        for(Account acc : dealers){
            
            if(acc.RecordTypeId == Utils.getRecordTypeId('Account', 'Network_Site_Acc') && acc.Dealer_Matrix__c != null){
                
                Monthly_Goal_Dealer__c d = new Monthly_Goal_Dealer__c();
                
                for(Monthly_Goal_Group__c g : groups){
                    
                    if(acc.Dealer_Matrix__c == g.Matrix_Bir__c){
                        
                        d.Dealer__c = acc.Id;
                        d.Monthly_Goal_Group__c = g.Id;
                        dealersGoal.add(d);
                    }
                    
                }
            }
            
        }
        
        System.debug('dealersGoal: ' + dealersGoal);
        
        /*
         *  INSERE NOVOS OBJETIVOS DE CONCESSIONÁRIAS
         * */
        
        Savepoint sp2 = Database.setSavepoint();
        
        try{
            Database.insert(dealersGoal);
        } catch(Exception e){
            System.debug('############ erro no dealer: ' + e);
            Database.rollback(sp2);
        }

			
        
    } 
    
}