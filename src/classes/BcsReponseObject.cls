public class BcsReponseObject{

        public String IdClient{get;set;}
        public String LastName{get;set;}
        public String Title{get;set;}
        public String Lang{get;set;}
        public String FirstName{get;set;}
        public String email{get;set;}
        public String typeperson{get;set;}
        public String GlobalCommAgreement{get;set;}
        public String PostCommAgreement{get;set;}
        public String TelCommAgreemen{get;set;}
        public String SMSCommAgreement{get;set;}
        public String FaxCommAgreement{get;set;}
        public String EmailCommAgreement{get;set;}
        public String phoneNum1{get;set;}
        public String phoneNum2{get;set;}
        public String phoneNum3{get;set;}
        public String StrName{get;set;}
        public String Compl2{get;set;}
        public String CountryCode{get;set;}
        public String Zip{get;set;}
        public String City{get;set;}
        public String vin{get;set;}
        public String brandCode{get;set;}
        public String modelCode{get;set;}
        public String modelLabel{get;set;}
        public String versionLabel{get;set;}
        public String firstRegistrationDate{get;set;}
        public String registration{get;set;}
        public String possessionBegin{get;set;}
      

}