@isTest
global class ChamadaServicoFakeAOCTest implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        
        system.debug('$$$req: '+req);
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/docs'){
            
            res.setBody(
                '{'+
                '"requestedURI": "http://br.co.rplug.renault.com/docs",'+
                '"docs": ['+
                '{'+
                '"docId": {'+
                '"program": "X70",'+
                '"phase": "3",'+
                '"modelSpecCode": "MTM",'+
                '"modelKind": "UM",'+
                '"market": "BRESIL",'+
                '"tariffNumber": "9363",'+
                '"tariffAdditiveNumber": "00",'+
                '"commercialKind": "VU",'+
                '"creationTimeStamp": "1356576361939",'+
                '"brand": "APL03",'+
                '"clientId": "TARIF",'+
                '"clientType": "GP",'+
                '"localSemiclair": "MTM"'+
                '},'+
                '"doc": "http://br.co.rplug.renault.com/doc/BA_"'+
                '}'+
                '],'+
                '"docgroup": "http://br.co.rplug.renault.com/doc/BBCAAAAw_jBjw3B3BiEshjjhhhhh",'+
                '"terms_of_use": "http://doc.rplug.renault.com/terms_of_use.html"'+
                '}'
            );     
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/doc/BA_'){
            res.setBody(
                '{'+
                '"requestedURI": "http://br.co.rplug.renault.com/doc/BA_",'+
                '"URI": "http://br.co.rplug.renault.com/doc/BA_",'+
                '"canonicalURI": "http://br.co.rplug.renault.com/doc/BA_#this",'+
                '"configuration": "http://br.co.rplug.renault.com/c/BA_/A",'+
                '"docId": {'+
                '"program": "X70",'+
                '"phase": "3",'+
                '"modelSpecCode": "MTM",'+
                '"modelKind": "UM",'+
                '"market": "BRESIL",'+
                '"tariffNumber": "9363",'+
                '"tariffAdditiveNumber": "00",'+
                '"commercialKind": "VU",'+
                '"creationTimeStamp": "1356576361939",'+
                '"brand": "APL03",'+
                '"clientId": "TARIF",'+
                '"clientType": "GP",'+
                '"localSemiclair": "MTM"'+
                '},'+
                '"validity": {'+
                '"expirationDate": null,'+
                '"applicationDate": "2012-12-28"'+
                '},'+
                '"diversityData": {'+
                '"localSemiClair": "http://br.co.rplug.renault.com/localsemiclair/BA_",'+
                '"specificationCategories": "http://br.co.rplug.renault.com/speccats/BA_",'+
                '"versionsCategories": "http://br.co.rplug.renault.com/category/BA_"'+
                '},'+
                '"presentationData": {'+
                '"marketingModelPresentation": "http://br.co.rplug.renault.com/pres/BA_"'+
                '},'+
                '"pricesData": {'+
                '"priceType": ['+
                '{'+
                '"reference": "PVCTTC",'+
                '"currency": "BRL"'+
                '}'+
                '],'+
                '"defaultPriceType": {'+
                '"reference": "PVCTTC",'+
                '"currency": "BRL"'+
                '},'+
                '"defaultPriceTypeVersion": {'+
                '"reference": "PVCTTC",'+
                '"currency": "BRL"'+
                '},'+
                '"defaultPriceTypeOption": {'+
                '"reference": "PVCTTC",'+
                '"currency": "BRL"'+
                '},'+
                '"pricesList": "http://br.co.rplug.renault.com/pricelist/BA_"'+
                '}'+
                '}'
            );
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/localsemiclair/BA_'){
            
            res.setBody(
                '{'+
                '"requestedURI": "http://br.co.rplug.renault.com/localsemiclair/BA_",'+
                '"localSemiClair": {'+
                '"mapRepresentation": {'+
                '"map": {'+
                '"FGL2H2 EU4": "FGL2H2 EU4",'+
                '"CCL2H1 EU4": "CCL2H1 EU4",'+
                '"FGL1H1 EU4": "FGL1H1 EU4",'+
                '"FU32 VIT EU4": "FU32 VIT EU4",'+
                '"FGL3H2 EU4": "FGL3H2 EU4"'+
                '},'+
                '"metadata": {'+
                '"key": "specCode",'+
                '"value": "localSemiClair"'+
                '}'+
                '}'+
                '}'+
                '}'
            );   
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/speccats/BA_'){
            res.setBody(
                '{'+
                '"specCat": ['+
                '{'+
                '"origin": "FGP",'+
                '"specCatCode": "123",'+
                '"diversified": true,'+
                '"@type": "colorThemeSpecCat",'+
                '"spec": ['+
                '{'+
                '"specCode": "HARM01",'+
                '"@type": "spec"'+
                '}'+
                ']'+
                '},'+
                '{'+
                '"origin": "FGP",'+
                '"specCatCode": "141",'+
                '"diversified": false,'+
                '"@type": "colorSpecCat",'+
                '"spec": ['+
                '{'+
                '"specCode": "NV676",'+
                '"@type": "spec"'+
                '},'+
                '{'+
                '"specCode": "O389",'+
                '"@type": "spec"'+
                '},'+
                '{'+
                '"specCode": "OV727",'+
                '"@type": "spec"'+
                '},'+
                '{'+
                '"specCode": "TE266",'+
                '"@type": "spec"'+
                '},'+
                '{'+
                '"specCode": "TEKNH",'+
                '"@type": "spec"'+
                '}'+
                ']'+
                '},'+
                '{'+
                '"origin": "AOC",'+
                '"specCatCode": "Qualite Peinture",'+
                '"diversified": true,'+
                '"@type": "colorQualitySpecCat",'+
                '"spec": ['+
                '{'+
                '"specCode": "QPA$MV",'+
                '"@type": "spec"'+
                '},'+
                '{'+
                '"specCode": "QPA$O",'+
                '"@type": "spec"'+
                '}'+
                ']'+
                '},'+
                '{'+
                '"origin": "FGP",'+
                '"specCatCode": "122",'+
                '"diversified": true,'+
                '"@type": "trimSpecCat",'+
                '"spec": ['+
                '{'+
                '"specCode": "DRAP11",'+
                '"@type": "spec"'+
                '}'+
                ']'+
                '}'+
                ']'+
                '}'
            );
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/category/BA_'){
            res.setBody(
                '{'+
                '"requestedURI": "http://br.co.rplug.renault.com/category/BA_",'+
                '"versionCategories": ['+
                '{'+
                '"versionIdSpecCode": "VEC016_BRES",'+
                '"categories": {'+
                '"mapRepresentation": {'+
                '"map": {'+
                '"650": "SYSTEMATIC",'+
                '"MTM": "SYSTEMATIC",'+
                '"FGL1H1 EU4": "SYSTEMATIC",'+
                '"RETROE": "SYSTEMATIC",'+
                '"DRAP11": "SYSTEMATIC",'+
                '"HARM01": "SYSTEMATIC",'+
                '"NV676": "OPTION",'+
                '"O389": "OPTION",'+
                '"OV727": "OPTION",'+
                '"TE266": "OPTION",'+
                '"TEKNH": "OPTION",'+
                '"BANAL": "SYSTEMATIC",'+
                '"PKCL0A": "OPTION",'+
                '"PKSG00": "OPTION",'+
                '"QPA$MV": "OPTION",'+
                '"QPA$O": "DEFAULT",'+
                '"NFG$2016/2017": "SYSTEMATIC",'+
                '"SCL_FGL1H1 EU4": "SYSTEMATIC",'+
                '"VEC016_BRES": "SYSTEMATIC"'+
                '},'+
                '"metadata": {'+
                '"key": "specCode",'+
                '"value": "category"'+
                '}'+
                '}'+
                '}'+
                '}'+
                ']'+
                '}'
            );
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/pres/BA_'){
            res.setBody(
                '{'+
                '"marketingModelPresentation": {'+
                '"asMap": {'+
                '"mapRepresentation": {'+
                '"map": {'+
                '"DEC41": {'+
                '"presentationItem": {'+
                '"key": "DEC41",'+
                '"label": {'+
                '"pt": "Motor"'+
                '},'+
                '"order": "1"'+
                '}'+
                '}'+
                '}'+
                '}'+
                '},'+
                '"label": {'+
                '"pt": "MASTER"'+
                '},'+
                '"versionsPresentation": ['+
                '{'+
                '"presentationGroup": {'+
                '"items": ['+
                '{'+
                '"presentationGroup": {'+
                '"items": ['+
                '{'+
                '"versionItem": {'+
                '"versionIdSpecCode": "VEC016_BRES",'+
                '"key": "VEC016_BRES",'+
                '"label": {'+
                '"pt": "Master Furgão L1H1 2.5 dCi"'+
                '},'+
                '"order": "0",'+
                '"versionId": {'+
                '"countrySpecCode": "BRES",'+
                '"versionSeq": "016"'+
                '},'+
                '"showableSpecCodes": ['+
                '"QPA$MV",'+
                '"QPA$O",'+
                '"ABS",'+
                '"CHAUFO",'+
                '"CA",'+
                '"CPE",'+
                '"RETROE",'+
                '"CLCVIT",'+
                '"CLCTU1",'+
                '"DRAP11",'+
                '"HARM01",'+
                '"BANAL",'+
                '"PLDCT",'+
                '"PKCL0A",'+
                '"PKSG00",'+
                '"LVAVEL",'+
                '"ABCO01",'+
                '"PARP18",'+
                '"SPFMOT",'+
                '"PFMOT",'+
                '"NFG$POIEXTNOIR",'+
                '"NFG$SKICHR",'+
                '"NFG$CONSOLE",'+
                '"NFG$TELMOBILE",'+
                '"NFG$LAVAI",'+
                '"NFG$A++BIS",'+
                '"NFG$SURTAP",'+
                '"NFG$2016/2017"'+
                ']'+
                '}'+
                '}'+
                '],'+
                '"key": "ENS_198",'+
                '"label": {'+
                '"pt": "Furgão L1H1 8m3 2012/2013"'+
                '},'+
                '"order": "1_0"'+
                '}'+
                '}'+
                '],'+
                '"key": "ENS_197",'+
                '"label": {'+
                '"pt": "Master Furgão"'+
                '},'+
                '"order": "1"'+
                '}'+
                '}'+
                ']'+
                '}'+
                '}'
            );
        }
        
        if(req.getEndpoint() == 'http://br.co.rplug.renault.com/pricelist/BA_'){
            res.setBody(
                '{'+
                '"requestedURI": "http://br.co.rplug.renault.com/pricelist/BA_",'+
                '"priceList": ['+
                '{'+
                '"priceType": {},'+
                '"versionPriceList": ['+
                '{},'+
                '{},'+
                '{},'+
                '{},'+
                '{'+
                '"versionIdSpecCode": "VEC016_BRES",'+
                '"price": 86270,'+
                '"optionsPrices": {'+
                '"mapRepresentation": {'+
                '"map": {'+
                '"PKSG00": 8630,'+
                '"PKCL0A": 5760,'+
                '"QPA$MV": 1275'+
                '},'+
                '"metadata": {'+
                '"key": "specCode",'+
                '"value": "price"'+
                '}'+
                '}'+
                '}'+
                '}'+
                ']'+
                '}'+
                ']'+
                '}'
            );
        }
        
        return res;
    }
}