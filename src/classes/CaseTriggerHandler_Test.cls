/*****************************************************************************************
    Name    : CaseTriggerHandler_Test
    Desc    : To Test the Case Trigger Handler
    Approach:
    Author  : Praneel PIDIKITI (Atos Integration)
    Project : Rforce
******************************************************************************************/
@istest
public with sharing class CaseTriggerHandler_Test {

    private static testMethod void testCaseInsert() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français',CurrencyCode__c='EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id,developername from RecordType where sObjectType = 'Account' and developername='Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', IDBIR__c='00001314');
            insert Acc;
           Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Dealer__c=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Colombia');
            insert cs;
            cs.Status='Resolved';
            cs.ProductFault__c='test';
            update cs;
            List<Case> CasList = new List<Case>();
            CasList.add(cs);
            CaseTriggerHandler.onAfterInsert(CasList);
            CaseTriggerHandler.onAfterUpdate(CasList);
            Test.stopTest();
        }

    }
    private static testMethod void testCaseInsert1() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français',CurrencyCode__c='EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id,developername from RecordType where sObjectType = 'Account' and developername='Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', IDBIR__c='00001314');
            insert Acc;
            Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Dealer__c=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Colombia',AccountId=Acc.id);
            insert cs;
            Test.stopTest();
        }

    }  
    private static testMethod void testCaseInsert2() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français',CurrencyCode__c='EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id,developername from RecordType where sObjectType = 'Account' and developername='CORE_ACC_Personal_Account_RecType'].Id;
            Account Acc = new Account(FirstName = 'Test1',LastName='test2', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', IDBIR__c='00001314',CustomerIdentificationNbr__c='CPF123');
            insert Acc;
            Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Dealer__c=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Colombia',CPF_Web__c ='CPF123');
            insert cs;
            Test.stopTest();
        }

    } 
    private static testMethod void testCaseInsert3()
    {
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com',BypassVR__c=true,BypassWF__c=true);

        Account Acc = new Account(Name='Test1',Phone='1000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;    
        Contact Con = 
        new Contact(FirstName = 'Mandatory', LastName='Test Contact',Salutation='Mr.',ProEmail__c ='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        
        VEH_Veh__c v = new VEH_Veh__c (Name = '45678934567123456', VehicleBrand__c = 'Renault', DeliveryDate__c= date.parse('04/06/2010'));
        insert v;
                 
        Case c = new Case (Origin='Mobile',Type='Information Request',SubType__c='Booking',VIN__c= v.Id, Status='Open',CaseBrand__c= 'RNO', Priority='Urgent',Kilometer__c= 100000, Description='Description ',From__c='Customer', AccountId=Acc.Id, ContactId=Con.Id);
        insert c;
        Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
       
        Goodwill__c g = new Goodwill__c(GoodwillStatus__c='Inprogres',case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        Goodwill_Grid_Line__c ggl = new Goodwill_Grid_Line__c (Country__c = 'Slovakia', VehicleBrand__c = 'Renault',Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c= 800000);
               
         System.runAs(usr)
        {                 
        Test.startTest();
    
        insert ggl;
        insert g;
        c.GWStatus__c='Approved';
        update g;
    
        Test.stopTest();       
    }
    }  
    private static testMethod void testCaseInsert4()
    {
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com',BypassVR__c=true,BypassWF__c=true);

        Account Acc = new Account(Name='Test1',Phone='1000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;    
        Contact Con = 
        new Contact(FirstName = 'Mandatory', LastName='Test Contact',Salutation='Mr.',ProEmail__c ='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        
        VEH_Veh__c v = new VEH_Veh__c (Name = '45678934567123456', VehicleBrand__c = 'Renault', DeliveryDate__c= date.parse('04/06/2010'));
        insert v;
                 
        Case c = new Case (Origin='Mobile',Type='Information Request',SubType__c='Booking',VIN_Web__c= v.Name, Status='Open',CaseBrand__c= 'RNO', Priority='Urgent',Kilometer__c= 100000, Description='Description ',From__c='Customer', AccountId=Acc.Id, ContactId=Con.Id);
        insert c;
        Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
       
        Goodwill__c g = new Goodwill__c(GoodwillStatus__c='Inprogres',case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        Goodwill_Grid_Line__c ggl = new Goodwill_Grid_Line__c (Country__c = 'Slovakia', VehicleBrand__c = 'Renault',Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c= 800000);
               
         System.runAs(usr)
        {                 
        Test.startTest();
    
        insert ggl;
        insert g;
        c.GWStatus__c='Refused';
        update g;
    
        Test.stopTest();       
    }
    }              
}