public class VFC05_ArchivesXMLData{

    public String FirstName {get;set;}
    public String LastName {get;set;}
    public String Status {get;set;}
    public String Comment {get;set;}
    public String IdCase {get;set;}
    public String Description {get;set;}
    public String NegociationResult {get;set;}
    public String Area{get;set;}
    public String Created {get;set;}
    public String QuoteType {get;set;}
    public String FromValue{get;set;}
    public String RCSRBrand {get;set;}
    public String CreatedByName {get;set;}
    public String Owner {get;set;}
    public String Source {get;set;}
    public String ClosedDate {get;set;}
    public String ClosedBy {get;set;}
    
    
    
    public String ResolutionDate{get;set;}
    public String ResolutionAuthor {get;set;}
    public String Mileage {get;set;}
    public String PreferedLanguage2 {get;set;}
    public String InvoiceAmount {get;set;}
    public String ClaimAmount {get;set;}
    public String ClientAmount {get;set;}
    public String AccountName {get;set;}
    public String TitleSIC {get;set;}
    public String AccessPoint {get;set;}
    
    public String SubArea2 {get;set;}
    public String CustomerRefNumber {get;set;}
    public String RCDetail {get;set;}  
    public String Severity {get;set;}
    public String Decision {get;set;}
    
    public String TestData {get;set;}
    public List<VFC05_ArchivesXMLData.Attachment> AttachmentsList{get;set;}
    //public VFC05_ArchivesXMLData.ActivityList[] ActivitiesList{get;set;}
    public List<VFC05_ArchivesXMLData.ActivityList> ActivitiesLists{get;set;}
    public List<VFC05_ArchivesXMLData.ContactList> ContactList{get;set;}
    public VFC05_ArchivesXMLData.Vehicle Vehicle{get;set;}
    public VFC05_ArchivesXMLData.Concerned Concerned{get;set;}
    public VFC05_ArchivesXMLData.EffectPerception EffectPerception{get;set;}
    public VFC05_ArchivesXMLData.CustomerCompensation CustomerCompensation{get;set;}
    public List<VFC05_ArchivesXMLData.Employee> EmployeeList{get;set;}
    public VFC05_ArchivesXMLData.Paying Paying{get;set;}
    public VFC05_ArchivesXMLData.OwnerOrganization OwnerOrganization{get;set;}
    public VFC05_ArchivesXMLData.FleetOrganization FleetOrganization{get;set;}
    
    public class Attachment{
    	public String ActivityFileName {get;set;}
        public String ActivityFileExt {get;set;}
        public String CreatedByName2 {get;set;}
        public String ActivityFileDate {get;set;}
        public String Comment3 {get;set;}
        public String ActivityFileSAFName {get;set;}
    }
    
    public class ActivityList{
        public String Type2 {get;set;}
        public String OriginDestination {get;set;}
        public String Status2 {get;set;}
        public String Created {get;set;}
        public String Done {get;set;}
        public String Description2 {get;set;}
        public String Comment2 {get;set;}
        public String SerialNumber2 {get;set;}
        public String OwnedBy {get;set;}
        public String ContactLastName {get;set;}
        public String ContactFirstName {get;set;}
        public String AccountName {get;set;}
        public String RCFleetName {get;set;}
        public String AccountLocation {get;set;}
        public String SRNumber2 {get;set;}   
        public List<VFC05_ArchivesXMLData.Attachment> AttachmentsList {get;set;}
  }

    public class ContactList{
        public String TitleSIC {get;set;}
        public String Title {get;set;}
        public String LastName {get;set;}
        public String FirstName {get;set;}
        public String NoClient {get;set;}
        public String CustomerIdentificationType {get;set;}
        public String CustomerIdentificationNr2 {get;set;}
        public String SICId {get;set;}
        public String PreferedLanguage2 {get;set;}
        public String Account {get;set;}
        public String PreferedMethodContact {get;set;}
        public String EmailAddress {get;set;}
        public List<VFC05_ArchivesXMLData.Contact_PersonalAddress> PersonalAddressList {get;set;}
        //Need to add list of ListOfContact_PersonalAddress
    }
    
    public class Contact_PersonalAddress{
    	public String PersonalCompleteStreetAddress {get;set;}
        public String PersonalAdditionalStreetAddress {get;set;}
        public String PersonalStreetNumber {get;set;}
        public String PersonalStreetType {get;set;}
        public String PersonalStreetName {get;set;}
        public String PersonalCity {get;set;}
        public String PersonalPostalCode2 {get;set;}
        public String PersonalMailBox {get;set;}
        public String PersonalCountry2 {get;set;}
        public String PersonalCellularPhone {get;set;}
        public String PersonalWorkPhone {get;set;}
        public String PersonalHomePhone {get;set;}
    }
    
	//Vehicle Class
     public class Vehicle{
         public String Build {get;set;}
         public String SerialNumber2 {get;set;}
         public String ProductModel {get;set;}
         public String Origin {get;set;}
         public String FactoryNumber2 {get;set;}
         public String DealerName2 {get;set;}
         public String ProductVersionSIC {get;set;}
         public String UsedNew {get;set;}
         public String AssetSICId {get;set;}
         public String DealerNumber2 {get;set;}
         public String InstallDate {get;set;}
         public String UsedCarPurchaseDate {get;set;}
         public String RegisteredDate {get;set;}
         
         //Need to add AccountOwnerNameSIC from list
     }
    
     //Concerned
     public class Concerned{
         public String DealerNumber {get;set;}
         public String Name22 {get;set;}
         public String CorporateShortName {get;set;}
         public String TradeRegistration {get;set;}
         public String ParentDivisionName {get;set;}
         public String RCDealerBrand {get;set;}
         public String CorporateStatus {get;set;}
         public String Type {get;set;}
         public String Zone {get;set;}
         public String MainPhoneNumber {get;set;}
         public String MainFaxNumber {get;set;}
         //Need to add Bussiness Address list
         public List<VFC05_ArchivesXMLData.Concerned_BusinessAddress> ConcernedBusinessAddressList {get;set;}
     }
    
     //Concerned BusinessAddress
     public class Concerned_BusinessAddress
     {
         public String CompleteAddress {get;set;}
         public String AdditionalAddress {get;set;}
         public String StreetNumber {get;set;}
         public String StreetType {get;set;}
         public String StreetName {get;set;}
         public String City {get;set;}
         public String PostalCode {get;set;}
         public String MailBox {get;set;}
     }   
    
     //Effect Perception Class
     public class EffectPerception{
         public String ClientView {get;set;}
         public String ClientDemand {get;set;}
         public String ServiceCause {get;set;}
         public String Risk {get;set;}
         public String CommercialCause {get;set;}
         public String ProductCauseFunction {get;set;}
         public String NITGCode {get;set;}
         public String ProductCause {get;set;}
         
     }
     //Customer Compensation
     public class CustomerCompensation{
         public String Mileage {get;set;}
         public String InvoiceAmount {get;set;}
         public String ClaimAmount {get;set;}
         public String ClientAmount {get;set;}         
     }
    
    //Concerned Dealer Employee
    public class Employee{
        public String FirstName {get;set;}
        public String JobTitle {get;set;}
        public String LastName {get;set;}
        public String ActivePhone {get;set;}
        public String Description3 {get;set;}
        public String Position {get;set;}
        
    }
    
    //Paying
    public class Paying{
        public String DealerNumber {get;set;}
        public String Name22 {get;set;}
        public String CorporateShortName {get;set;}
        public String TradeRegistration {get;set;}
        public String ParentDivisionName {get;set;}
        public String RCDealerBrand {get;set;}
        public String CorporateStatus {get;set;}
        public String Type {get;set;}
        public String Zone {get;set;}
        public String MainPhoneNumber {get;set;}
        public String MainFaxNumber {get;set;}
        //To add list
        public List<VFC05_ArchivesXMLData.Paying_BusinessAddress> PayingBusinessAddressList {get;set;}
    }
    public class Paying_BusinessAddress{
         public String CompleteAddress {get;set;}
         public String AdditionalAddress {get;set;}
         public String StreetNumber {get;set;}
         public String StreetType {get;set;}
         public String StreetName {get;set;}
         public String City {get;set;}
         public String PostalCode {get;set;}
         public String MailBox {get;set;}
    }
    
    //Owner Organization
    public class OwnerOrganization{
        public String Name2 {get;set;}
        public String OrgIdentificationNr2 {get;set;}
        public String Type {get;set;}
        public String ParentAccountName {get;set;}
        public String Preferedlanguage {get;set;}
        public String OrgIdentificationType {get;set;}
        public String SICId {get;set;}
        public String OrgIdentificationNr1 {get;set;}
        public String Siret {get;set;}
        public String Location2 {get;set;}
        public String MainPhoneNumber {get;set;}
        public String MainFaxNumber {get;set;}
        //To add list
        public List<VFC05_ArchivesXMLData.BusinessAddress> BusinessAddressList {get;set;}
    }
    public class BusinessAddress{
         public String CompleteAddress {get;set;}
         public String AdditionalAddress {get;set;}
         public String StreetNumber {get;set;}
         public String StreetType {get;set;}
         public String StreetName {get;set;}
         public String City {get;set;}
         public String PostalCode {get;set;}
         public String MailBox {get;set;}
         public String ProvinceSIC {get;set;}
         public String Country {get;set;}
    }
    
    //Fleet Organization
    public class FleetOrganization{
        public String Name2 {get;set;}
        public String OrgIdentificationNr2 {get;set;}
        public String Type {get;set;}
        public String ParentAccountName {get;set;}
        public String Preferedlanguage {get;set;}
        public String OrgIdentificationType {get;set;}
        public String SICId {get;set;}
        public String OrgIdentificationNr1 {get;set;}
        public String Siret {get;set;}
        public String Location2 {get;set;}
        public String MainPhoneNumber {get;set;}
        public String MainFaxNumber {get;set;}
        //To add list
        public List<VFC05_ArchivesXMLData.BusinessAddress> BusinessAddressList {get;set;}
        
    }
}