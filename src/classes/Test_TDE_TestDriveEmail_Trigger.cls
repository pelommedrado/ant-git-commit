@isTest
private class Test_TDE_TestDriveEmail_Trigger{
    
    public void test01(){
        TDE_TestDriveEmail__c tde = new TDE_TestDriveEmail__c();
        tde.Account__c = '001D00000104lje';
        tde.DateBooking__c = System.Today();
        tde.Dealer__c = '001D000000yoQtb';
        tde.Test_Drive_Vehicle__c = 'a03D000000Rh4gE';
        insert tde;
    }
}