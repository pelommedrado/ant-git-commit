@isTest
public class LeadToolsTest {
    
    @isTest static void deveGerDealerByBIR(){
        
        User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concessionária teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
        
        Database.insert( dealerAcc );
        
        Account ret = LeadTools.gerDealerByBIR(dealerAcc.IDBIR__c);
        system.assertEquals(dealerAcc.IDBIR__c, ret.IDBIR__c);
    }

}