@isTest(seealldata=true)
public with sharing class TestVFC03_FTSGoodwill_Edit {
    
    private static void criarCenario() { 
        Id RTID_COMPANY = [
            SELECT Id 
            FROM RecordType 
            WHERE sObjectType = 'Account' AND DeveloperName = 'Company_Acc' 
            LIMIT 1
        ].Id;
        
        Account Acc = new Account(Name='Test1', Phone='0000', RecordTypeId=RTID_COMPANY, 
                                  ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', 
                                  ShippingCountry = 'France', ShippingState = 'IDF', 
                                  ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
        
        Contact Con = new Contact(LastName = 'Test Contact', FirstName = 'Test', Salutation = 'Mr.',
                                  ProEmail__c = 'lro@lro.com', AccountId = Acc.Id);
        insert Con;
        
        VEH_Veh__c veh = new VEH_Veh__c(name = 'VF1BGRG0633285766', 
                                        DeliveryDate__c = date.parse('02/02/2007'));
        insert veh;
        
        Case c = new Case(Origin='Email', Type='Information Request', 
                          SubType__c = 'Booking', Status='Open', Priority='Medium', 
                          Description='Description', From__c='Customer', 
                          AccountId = Acc.Id, ContactId = Con.Id, VIN__c = veh.Id);
        insert c;
        
        Case c1 = new Case(Origin='Email', Type='Information Request', 
                           SubType__c = 'Booking', Status = 'Open', Priority = 'Medium', 
                           Description = 'Description', From__c = 'Customer', AccountId = Acc.Id, 
                           ContactId=Con.Id);
        insert c1;
        
        Id RecordId = [
            SELECT Id 
            FROM RecordType 
            WHERE sObjectType = 'Goodwill__c' AND Name = 'CAR RENTAL' LIMIT 1
        ].Id;
        
        Goodwill__c g = new Goodwill__c(case__c = c.Id, ResolutionCode__c = 'Voucher', 
                                        RecordTypeId = RecordId, ORDate__c = date.parse('02/02/2010'),
                                        ExpenseCode__c = 'OTS Issue', BudgetCode__c = 'SRC',
                                        Organ__c = 'BATTERY', SRCPartRate__c = 0.5, 
                                        QuotWarrantyRate__c = 1000, Country__c = 'Brazil');
        insert g;
        
        Goodwill_Grid_Line__c grid = new Goodwill_Grid_Line__c(Country__c = 'Brazil', 
                                                               VehicleBrand__c = 'Renault', 
                                                               Organ__c = 'BATTERY', 
                                                               Max_Age__c = 100, Max_Mileage__c= 800000);
        insert grid;
        
        ApexPages.currentPage().getParameters().put('Id',g.Id);
        ApexPages.currentPage().getParameters().put('retURL',c.id);
        //ApexPages.currentPage().getParameters().put('CF00ND0000004qc7b',c.id);
        //ApexPages.StandardController controller1=new ApexPages.StandardController(g);
    }
    
    static testMethod void testVFC03_FTSGoodwill_Edit3() {
        Test.StartTest();
        
        criarCenario();
        
        VFC03_FTSGoodwill_Edit tst1 = new VFC03_FTSGoodwill_Edit();
        
        tst1.getgudwill().GoodwillStatus__c = 'Approved';
        tst1.getgudwill().DeviationReason__c = null;
        tst1.saveGoodwill();
        
        Test.stopTest();
    }
    
    static testMethod void testVFC03_FTSGoodwill_Edit2() {
        Test.StartTest();
        
        criarCenario();
        
        VFC03_FTSGoodwill_Edit tst1 = new VFC03_FTSGoodwill_Edit();
        
        tst1.getgudwill().BudgetCode__c = null;
        tst1.saveGoodwill();
        
        Test.stopTest();
    }
    
    static testMethod void testVFC03_FTSGoodwill_Edit1() {
        Test.StartTest();
        
        criarCenario();
        
        VFC03_FTSGoodwill_Edit tst1 = new VFC03_FTSGoodwill_Edit();
        
        tst1.getgudwill().ExpenseCode__c = null;
        tst1.saveGoodwill();
        
        Test.stopTest();
    }
    
    static testMethod void testVFC03_FTSGoodwill_Edit() {
        Test.StartTest();
        
        criarCenario();
        
        VFC03_FTSGoodwill_Edit tst1=new VFC03_FTSGoodwill_Edit();
        String st = tst1.getTotalPercentage;
        tst1.getgudwill();
        tst1.cancel();
        tst1.saveGoodwill();
        tst1.CalculateSRC();
        
        Test.stopTest();
    }
}