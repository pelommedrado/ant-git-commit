@isTest
private class FileUploaderControllerTest {
    
    public class TestCaller implements FileUploaderCaller{
        
        public void fileUploaderCallback (FileUploaderController target){
            target.reset( true );
        }
        
        public void registerController (FileUploaderController target){
            target.reset( true );
        }
        
    }
    
    @isTest static void test_method_one() {
        Task t = new Task();
        Database.insert( t );
        
        FileUploaderController controller = new FileUploaderController();
        TestCaller caller = new TestCaller();
        controller.step = 'step';
        controller.contentType = 'contentType';
        controller.communityPath = 'communityPath';
        String baseURL = controller.baseURL;
        String debug = controller.debug;
        controller.validate();
        
        controller.parentId = UserInfo.getUserId();
        controller.validate();
        
        controller.parentId = t.Id;
        controller.externalController = caller;
        controller.validate();
        
        controller.parentId = t.Id;
        controller.externalController = caller;
        controller.encodedImage = EncodingUtil.base64Encode( Blob.valueOf( 'ABC' ) );
        controller.name = '/path/to/file/file.png';
        controller.type = 'png;jpg';
        controller.validate();
        
        controller.parentId = t.Id;
        controller.externalController = caller;
        controller.encodedImage = EncodingUtil.base64Encode( Blob.valueOf( 'ABC' ) );
        controller.name = '/path/to/file/file.pdf';
        controller.type = 'png;jpg';
        controller.strict = false;
        controller.validate();
        
        controller.parentId = t.Id;
        controller.externalController = caller;
        controller.encodedImage = EncodingUtil.base64Encode( Blob.valueOf( 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ' ) );
        controller.megabyteLimit = 1/1024/1024;
        controller.validate();
        
        controller.parentId = t.Id;
        controller.externalController = caller;
        controller.encodedImage = EncodingUtil.base64Encode( Blob.valueOf( 'ABC' ) );
        controller.name = 'C:\\path\\to\\file\\file';
        controller.strict = true;
        controller.type = 'png;jpg';
        controller.validate();
        
        Id taskId = t.Id;
        Database.delete( t );
        controller.parentId = taskId;
        controller.validate();
        
        String dummyStr = controller.encodedImage + controller.attURL;
        Boolean dummyBool = controller.isImg || controller.isPDF;
        
    }
}