@isTest
public class VEH_Vehicle_TriggerTest {
	
	@isTest static void test_method_one() {
        VEH_Veh__c vehicle = new VEH_Veh__c(
            ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            //IDBIRSellingDealer__c = css1.IDBIR__c,
            //Id_Bir_BuyDealer__c = css2.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'fffffffffffffffff',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'fffffffffffffffff',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        
        insert vehicle;
        
        update vehicle;
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}