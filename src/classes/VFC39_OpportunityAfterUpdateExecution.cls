/**
 * Classe que será disparada pela trigger do objeto Opportunity após a atualização dos registros.
 * @author Felipe Jesus Silva.
 */
public class VFC39_OpportunityAfterUpdateExecution implements VFC37_TriggerExecution {
    
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{
		Opportunity sObjOldOpportunity = null;
		List<Opportunity> lstSObjOpportunity = (List<Opportunity>) lstNewtData;
		Map<Id, Opportunity> mapSObjOldOpportunity = (Map<Id, Opportunity>) mapOldData;
		List<Opportunity> lstSObjOppForCreationTasks = new List<Opportunity>();
		List<Opportunity> lstSObjOppForCloseTasksAndTestDrives = new List<Opportunity>();
    	List<TDV_TestDrive__c> lstObjTestDrive = null;
		
		for(Opportunity sObjOpportunity : lstSObjOpportunity)
		{
			sObjOldOpportunity = mapSObjOldOpportunity.get(sObjOpportunity.Id);
			
			/*verifica se o campo OpportunityTransition foi modificado e não está com o status In Attendance Seller*/
			if((sObjOpportunity.OpportunityTransition__c != sObjOldOpportunity.OpportunityTransition__c) && 
			   (sObjOpportunity.OpportunityTransition__c != VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER))
			{
				/*adiciona a oportunidade na lista das que vão gerar novas tarefas*/
				lstSObjOppForCreationTasks.add(sObjOpportunity);
			}
			
			/*verifica se a oportunidade foi cancelada*/
			else if(sObjOpportunity.StageName == 'Lost')
			{
				/*adiciona a oportunidade na lista das que vão gerar encerramento de tarefas*/
				lstSObjOppForCloseTasksAndTestDrives.add(sObjOpportunity);
			}
			
			lstObjTestDrive = VFC28_TestDriveDAO.getInstance().findByOpportunityId(sObjOpportunity.Id, new Set<String>{'Scheduled'});
		
			if(!lstObjTestDrive.isEmpty())
    		{
    			for(TDV_TestDrive__c testDrive :lstObjTestDrive){
    				testDrive.OwnerId = sObjOpportunity.OwnerId;
    				VFC28_TestDriveDAO.getInstance().updateData(testDrive);
    			}
    		}
			
			
		}
		
		if(!lstSObjOppForCreationTasks.isEmpty())
		{
			//this.createTasks(lstSObjOppForCreationTasks);
		}
		
		if(!lstSObjOppForCloseTasksAndTestDrives.isEmpty())
		{
			this.closeTasks(lstSObjOppForCloseTasksAndTestDrives);
		}			
	}
	
	private void createTasks(List<Opportunity> lstSObjOpportunity)
	{
		VFC40_TaskBO.getInstance().createTasksFromOpportunities(lstSObjOpportunity);		
	}
	
	private void closeTasks(List<Opportunity> lstSObjOpportunity)
	{
		VFC40_TaskBO.getInstance().closeTasksFromOpportunities(lstSObjOpportunity);
	}
}