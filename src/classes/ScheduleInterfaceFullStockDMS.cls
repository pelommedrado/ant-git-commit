global class ScheduleInterfaceFullStockDMS implements Schedulable {
    
    global void execute(SchedulableContext sc){
        BatchInterfaceFullStockDMS b = new BatchInterfaceFullStockDMS();
        Database.executeBatch(b);
        
        /*
         * Script para agendar batch *
         * 
        ScheduleInterfaceFullStockDMS S = new ScheduleInterfaceFullStockDMS();
    	String sch = '0 0 1 ? * *';
    	String JobId = system.schedule('Interface Full Stock DMS', sch, s);*/
    }

}