@isTest
private class WSC04_VehicleDMSVO_Test {

    static testMethod void myUnitTest() {
        WSC04_VehicleDMSVO vehicleDMSVO = new WSC04_VehicleDMSVO();

        vehicleDMSVO.VIN = 'Test';
	    vehicleDMSVO.model = 'Fluence';
	    vehicleDMSVO.version = '1000';
		vehicleDMSVO.manufacturingYear = 2010;
	   	vehicleDMSVO.modelYear = 2012;
		vehicleDMSVO.color = 'Yellow';
		vehicleDMSVO.optionals = 'yes';
		vehicleDMSVO.entryInventoryDate = system.today() - 20;
		vehicleDMSVO.price = 20000;
		vehicleDMSVO.lastUpdateDate = system.now() ;
		vehicleDMSVO.status = 'Available';

		//vehicleDMSVO.error = true;
		vehicleDMSVO.errorMessage = 'Not Available';
    }
}