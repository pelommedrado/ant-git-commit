/*
* Classe genérica criada inicialmente para execução de métodos relacionados à atividades
* por Daniel Soares - Dezembro de 2016
*/

public class ActivityUtils {
    public static void setChaseTaskTime(List<Opportunity> triggerNew){
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'Sales'];

        Set<Id> opptDealerIdSet = new Set<Id>();
        for(Opportunity oppt : triggerNew){
            if(String.isNotEmpty(oppt.Dealer__c))
                opptDealerIdSet.add(oppt.Dealer__c);
        }

        System.debug('@@@ opptDealerIdSet: ' + opptDealerIdSet);

        Map<Id, Account> mapOpptDealer = new Map<Id, Account>([
            SELECT ChaseTaskTime__c
            FROM Account
            WHERE Id in :opptDealerIdSet AND ChaseTaskTime__c != null
        ]);

        for(Opportunity oppt : triggerNew) {
            if(!mapOpptDealer.containsKey(oppt.Dealer__c)) continue;

            Account opptAccount = mapOpptDealer.get(oppt.Dealer__c);

            Long milisecondsForChase = (Long) opptAccount.ChaseTaskTime__c * 60 * 60 * 1000;

            oppt.ScheduleChase__c = BusinessHours.add(bh.Id, datetime.now(), milisecondsForChase);
        }
    }

    // Criação de tarefa de prospecção no update do Lead
    public static void createProspectionTaskForLead(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap) {

        System.debug('*********createProspectionTaskForLead:');
        List<Task> lstTaskUpsert = new List<Task>();

        Set<Id> ownerIdSet = new Set<Id>();

        for(Lead l : triggerNew)
            ownerIdSet.add(l.OwnerId);

        for(Lead l : triggerOldMap.values())
            ownerIdSet.add(l.OwnerId);

        Map<Id, List<PermissionSet>> mapUserPermissionSets = Utils.getUserPermissionSetsMap(ownerIdSet);

        System.debug('***mapUserPermissionSets: '+mapUserPermissionSets);

        List<Task> lstProspectionTasks = [
            SELECT Id, Subject, Status, OwnerId, WhoId
            FROM Task
            WHERE Subject = 'Prospection Attendance'
            AND Status not in ('Not Started', 'Canceled', 'Deferred')
            AND WhoId in :triggerNew
        ];

        for(Lead newLead : triggerNew) {
            Id leadId = newLead.Id;

            Lead oldLead = triggerOldMap.get(leadId);

            System.debug('***newLead: '+newLead);
            System.debug('***oldLead: '+oldLead);

            if(oldLead.OwnerId != newLead.OwnerId &&
               mapUserPermissionSets.containsKey(oldLead.OwnerId) &&
               mapUserPermissionSets.containsKey(newLead.OwnerId)) {
                Set<String> newOwnerPermissions = new Set<String>();

                for(PermissionSet ps : mapUserPermissionSets.get(newLead.OwnerId)) newOwnerPermissions.add(ps.Name);

                List<Task> oldOwnerProspectionTasks = new List<Task>();
                for(Task tk : lstProspectionTasks){
                    if(tk.WhoId == leadId && tk.OwnerId == oldLead.OwnerId) oldOwnerProspectionTasks.add(tk);
                }

                if(newOwnerPermissions.contains('BR_SFA_Seller_Agenda') && !oldOwnerProspectionTasks.isEmpty()) {
                System.debug('*********if(newOwnerPermissions.contains("BR_SFA_Seller") && !oldOwnerProspectionTasks.isEmpty()) ');
                    for(Task tk : oldOwnerProspectionTasks){
                        tk.OwnerId = newLead.OwnerId;
                        //tk.IsReminderSet = true;
                        //tk.ReminderDateTime = Utils.now();

                        lstTaskUpsert.add(tk);
                    }

                } else if(newOwnerPermissions.contains('BR_SFA_Seller_Agenda')) {
                    System.debug('*********f(newOwnerPermissions.contains("BR_SFA_Seller")) ');
                    lstTaskUpsert.add(new Task(
                        OwnerId = newLead.OwnerId, WhoId = newLead.Id,
                        Subject = 'Prospection Attendance', Status = 'In Progress',
                        ActivityDatetime__c = date.today(), IsVisibleInSelfService = true
                        //IsReminderSet = true,
                        //ReminderDateTime = Utils.now()
                    ));
                }
            }
        }

        if(!lstTaskUpsert.isEmpty()) upsert lstTaskUpsert;
    }

}