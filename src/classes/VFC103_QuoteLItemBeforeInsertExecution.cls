/**
* Classe que será disparada pela trigger do objeto QuoteLineItem antes de inserir um registro.
* @author Elvia Serpa.
* Obs: Nome da class foi abreviada de VFC103_QuoteLineItemBeforeInsertExecution para VFC103_QuoteLItemBeforeInsertExecution
* devido o numero de caracter
*/
public class  VFC103_QuoteLItemBeforeInsertExecution  implements VFC37_TriggerExecution
{   
    public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
    {   
        List<QuoteLineItem> lstSObjQuoteLineItem = (List<QuoteLineItem>) lstNewtData;
        
        //this.checkQuoteLineItem(lstSObjQuoteLineItem);
        this.checkVehicle(lstSObjQuoteLineItem);
    }

	private void checkVehicle(List<QuoteLineItem> lstSObjQuoteLineItem){
		
		Quote sObjQuote = null;
		List<QuoteLineItem> lstSObjQuoteLineItemQuote = null;
		
		 for(QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem){
		 	
		 	System.debug('-------------- sObjQuoteLineItem.Id' +sObjQuoteLineItem);
		 	
		 	
		 	PricebookEntry priceBookEntry = [SELECT 
    								Id, 
	                                Pricebook2Id, 
	                                Product2Id, 
	                                UnitPrice, 
	                                Product2.Description,
	                                Product2.RecordType.DeveloperName
								FROM 
									PricebookEntry
								WHERE
									Id =: sObjQuoteLineItem.PricebookEntryId];
									
		 	System.debug('-------------- priceBookEntry'+ priceBookEntry);
		 	System.debug('-------------- RT'+ priceBookEntry.Product2.RecordType.DeveloperName);
		 	if(priceBookEntry.Product2.RecordType.DeveloperName != 'PDT_Accessory'){
		 	
			 	sObjQuote =  VFC35_QuoteDAO.getInstance().findById(sObjQuoteLineItem.QuoteId);
			 	
			 	if(sObjQuote != null){
			 		
			 		lstSObjQuoteLineItemQuote = VFC36_QuoteLineItemDAO.getInstance().getQuoteLineItemByQuoteId(sObjQuote.Id, new set<String> { 'PDT_ModelVersion', 'PDT_Model' }); 
			 		
			 		if (!lstSObjQuoteLineItemQuote.isEmpty()){
			 			sObjQuoteLineItem.addError('Já existe um veículo cadastrado para este orçamento.');		 			
			 		}
			 		
			 	}
		 	}
		 	
		 } 
	}

    /* Comentado por @Edvaldo {kolekto} - porque a chamada deste metodo está comentado acima, logo não está em uso.

    private void checkQuoteLineItem(List<QuoteLineItem> lstSObjQuoteLineItem)
    {
        PricebookEntry sObjPbEntry = null;
        List<PricebookEntry> lstSObjPbEntry = null;
        Map<String, PricebookEntry> mapSObjPbEntry = null;
        
        //Set contendo os ids dos QuoteLineItem
        Set<String> setQuoteLineItem = new Set<String>();
        
        //Map contendo os Itens de Cotação
        Map<String, QuoteLineItem> mapQuoteLineItem = new Map<String, QuoteLineItem>();
        
        Set<String> setPricebookEntry = new Set<String>();
            
        system.debug('lstSObjQuoteLineItem'+lstSObjQuoteLineItem);
        
        //Caso esteje trabalhando com massa de dados garda os ids para ser usado nas buscas.
        for(QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem) 
        {   
            setPricebookEntry.add(sObjQuoteLineItem.PricebookEntryId);
        }
        
        //Busca todas as tableas de preço pelo Id
        lstSObjPbEntry = VFC24_PriceBookDAO.getInstance().findById(setPricebookEntry);
        
        mapSObjPbEntry = new Map<String, PricebookEntry>(lstSObjPbEntry); 
        
        for(QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem) 
        {   
            sObjPbEntry = mapSObjPbEntry.get(sObjQuoteLineItem.PricebookEntryId);
            
            if(sObjPbEntry.Product2.RecordType.DeveloperName == 'PDT_ModelVersion' || sObjPbEntry.Product2.RecordType.DeveloperName == 'PDT_Model')
            {
                setQuoteLineItem.add(sObjQuoteLineItem.QuoteId);
            }
        }
        
        if(!setQuoteLineItem.isEmpty())
        {
            mapQuoteLineItem = VFC84_QuoteLineItemBO.getInstance().mapByQuoteId('PDT_ModelVersion' , setQuoteLineItem);
            
            //Faz a validação se existe mais de um intem inserido
            for(QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem) 
            {   
                if(mapQuoteLineItem.containsKey(sObjQuoteLineItem.QuoteId))
                {
                    sObjQuoteLineItem.addError('Já existe um veículo cadastrado para este orçamento.');
                }
            }
        }       
    }*/
}