@isTest
private class VFC76_AccessoryBO_Test {

	static Id getRecordTypeId(String developerName)
	{
		RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
		
		return sObjRecordType.Id;
	}
	
	static Account createDealerAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Network_Site_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccDealer = new Account();
		sObjAccDealer.Name = 'Dealer Test';
		sObjAccDealer.IDBIR__c='1000';
        sObjAccDealer.Phone='1000';
		sObjAccDealer.RecordTypeId = recordTypeId; 
		sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com'; 
		sObjAccDealer.ShippingCity = 'Paris';
		sObjAccDealer.ShippingCountry = 'France';
		sObjAccDealer.ShippingState = 'IDF';
		sObjAccDealer.ShippingPostalCode = '75013';
		sObjAccDealer.ShippingStreet = 'my street';
		
		return sObjAccDealer;
	}
	
	static Account createPersonalAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Personal_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccPersonal = new Account();
		sObjAccPersonal.FirstName = 'Personal';
		sObjAccPersonal.LastName = 'Account';
		sObjAccPersonal.Phone='1000';
		sObjAccPersonal.RecordTypeId = recordTypeId; 
		sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
		sObjAccPersonal.ShippingCity = 'Paris';
		sObjAccPersonal.ShippingCountry = 'France';
		sObjAccPersonal.ShippingState = 'IDF';
		sObjAccPersonal.ShippingPostalCode = '75013';
		sObjAccPersonal.ShippingStreet = 'my street';
		sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
		sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
		//sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
		//sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
		sObjAccPersonal.PersLandline__c = '1133334444';
		sObjAccPersonal.PersMobPhone__c = '1155556666';
		
		return sObjAccPersonal;
	}
	
	static VEH_Veh__c createVehicle()
	{
		VEH_Veh__c sObjVehicle = new VEH_Veh__c();
		sObjVehicle.Name = 'TESTCHASSISANDERO';
		sObjVehicle.Model__c = 'Sandero';
		sObjVehicle.Version__c = 'Stepway 1.6 8V';
		sObjVehicle.VersionCode__c = 'S1';
		sObjVehicle.Color__c = 'Black';
		sObjVehicle.VehicleRegistrNbr__c = 'ABC-1234';
		
		return sObjVehicle;
	}
	
	static TDV_TestDrive__c createTestDrive()
	{
		TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
		sObjTestDrive.Status__c = 'Scheduled';
		sObjTestDrive.Opportunity__c = null;
		
		return sObjTestDrive;
	}

    static testMethod void testVFC76_AccessoryBO() {
    	Account sObjAccDealer = null;
		Account sObjAccPersonal = null;
		VEH_Veh__c sObjVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		
		/*cria e insere a conta que representa a concessionária*/
		sObjAccDealer = createDealerAccount();		
		insert sObjAccDealer;
		
		/*cria a conta pessoal e vincula a concessionária*/
		sObjAccPersonal = createPersonalAccount();
		sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
		insert sObjAccPersonal;
		
		/*cria e insere o veículo*/
		sObjVehicle = createVehicle();
		insert sObjVehicle;
		
		/*cria a insere o objeto TestVehicle*/
		sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
		insert sObjTestDriveVehicle;
    	
        VFC76_AccessoryBO.getInstance().getById(sObjTestDriveVehicle.id);
    }
}