/**
 * Mock Test Class to test the BCS WebService 
 */
@isTest
global class AsyncTRVwsdlBcs implements WebServiceMock {
	
	public static TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element bcsResponse;

	/** @constructor **/
    global AsyncTRVwsdlBcs() {
    	bcsResponse = buildBCSResponse();
    }
    
    /** Simulates invokation of the webservice **/
    global void doInvoke( Object stub, Object request, Map<String, Object> response,
           					String endpoint, String soapAction, String requestName,
           					String responseNS, String responseName, String responseType) {
        system.debug('#### AsyncTRVwsdlBcs - doInvoke - BEGIN request=' + request);
        response.put('response_x', bcsResponse);
    	system.debug('#### AsyncTRVwsdlBcs - doInvoke - END');         						
    }
    
    /** Build the BCS response
     * @return TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element
    **/
    private TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element buildBCSResponse() {
    	
    	TRVwsdlBcs.description descrip = new TRVwsdlBcs.description();
        descrip.type_x='test';
        descrip.typeCode='test';
        descrip.subType='test';
        descrip.subTypeCode='test';
        
        TRVwsdlBcs.wsInfos wsInfos = new TRVwsdlBcs.wsInfos();
        wsInfos.wsVersion='test';
        wsInfos.wsEnv='test';
        
        TRVwsdlBcs.survey survey = new TRVwsdlBcs.survey();
        survey.surveyOK='test';
        survey.value='test';
        survey.loyMessage='test';
        
        TRVwsdlBcs.dealerList dealerList = new TRVwsdlBcs.dealerList();
        TRVwsdlBcs.birId birId1 = new TRVwsdlBcs.birId();
       	list<TRVwsdlBcs.birId> lstBir = new list<TRVwsdlBcs.birId>(); 
       	dealerList.birId = lstBir;
    	
    	TRVwsdlBcs.servicePrefs servPref = new TRVwsdlBcs.servicePrefs();
        servPref.irn='test';
        servPref.sia='test';
        servPref.reqid='test';
        servPref.userid='test';
        servPref.language='test';
        servPref.country='test';
        
        TRVwsdlBcs.custDataRequest custDataRequest = new TRVwsdlBcs.custDataRequest();
        custDataRequest.mode='test';
        custDataRequest.country='test';
        custDataRequest.brand='test';
        custDataRequest.demander='test';
        custDataRequest.vin='test';
        custDataRequest.lastName='test';
        custDataRequest.firstName='test';
        custDataRequest.city='test';
        custDataRequest.zip='test';
        custDataRequest.ident1='test';
        custDataRequest.idClient='test';
        custDataRequest.idMyr='test';
        custDataRequest.firstRegistrationDate='test';
        custDataRequest.registration='test';
        custDataRequest.email='test';
        custDataRequest.ownedVehicles='test';
        custDataRequest.nbReplies='test';
        
        TRVwsdlBcs.request request = new TRVwsdlBcs.request();
        request.servicePrefs = servPref;
        request.custDataRequest = custDataRequest;
        
        TRVwsdlBcs.getCustData getCustData = new TRVwsdlBcs.getCustData();
        getCustData.request = request;
    	
    	TRVwsdlBcs.getCustDataTopElmt getCustDataTopElmt = new TRVwsdlBcs.getCustDataTopElmt();
        getCustDataTopElmt.getCustData = getCustData;
        
        TRVwsdlBcs.BCScommAgreement BCScommAgreement = new TRVwsdlBcs.BCScommAgreement();
        BCScommAgreement.Global_Comm_Agreement='test';
        BCScommAgreement.Preferred_Communication_Method='test';
        BCScommAgreement.Post_Comm_Agreement='test';
        BCScommAgreement.Tel_Comm_Agreement='test';
        BCScommAgreement.SMS_Comm_Agreement='test';
        BCScommAgreement.Fax_Comm_Agreement='test';
        BCScommAgreement.Email_Comm_Agreement='test';
    	
    	TRVwsdlBcs.contact contact = new TRVwsdlBcs.contact();
        contact.phoneCode1='00';
        contact.phoneNum1='+33101010101';
        contact.phoneCode2='00';
        contact.phoneNum2='+33101010101';
        contact.phoneCode3='00';
        contact.phoneNum3='+33101010101';
        contact.email='test@test.com';
        contact.preferredCom='test';
        contact.optin='test';
        
        TRVwsdlBcs.workshopList workshop = new TRVwsdlBcs.workshopList();
        workshop.date_x='test';
        workshop.km='test';
        workshop.birId='test';
        workshop.description = descrip;
        
        TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Input_element getCustData_Input_element = new TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Input_element();
        getCustData_Input_element.getCustData = getCustData;
            	
    	TRVwsdlBcs.response pDataResponse = new TRVwsdlBcs.response();
		
		TRVwsdlBcs.clientList client = new TRVwsdlBcs.clientList();
		client.IdClient = '13553167';
		client.typeperson = 'P';
		client.ident1 = '1X-6789-YH';
		client.ident2 = '1X-2ND-IDENTIF';
		client.idMyr = 'identifMYR';
		client.LastName = 'DURAND';	
		client.FirstName = 'RAYNALD';
		client.middleName = 'JEAN-JEAN';
		client.birthDay = '1950-01-23';
		Client.sex = 'man';
		//client.MyRenaultID__c = 'mail110@atos.net'; 
		client.title = 'Mr.';
		client.Lang = 'FRA';
		client.address = new TRVwsdlBcs.address();
		client.address.strName = '13 RUE FELIX TERRIER';
		/*client.address.compl1 = 'compl1';
		client.address.compl2 = 'compl2';
		client.address.compl3 = 'compl3';*/
		client.address.countryCode = 'FR';
		client.address.zip = '75020';
		client.address.city = 'PARIS';
		client.address.areaCode = 'IDF';
		client.contact = contact;
		client.BCScommAgreement = new List<TRVwsdlBcs.BCScommAgreement>{BCScommAgreement};
		TRVwsdlBcs.vehicleList vehicle = new TRVwsdlBcs.vehicleList();
		vehicle.vin = 'VF1C4050500764359';
		vehicle.brandCode = 'RENAULT';
		vehicle.modelCode = 'XX';
		vehicle.modelLabel = 'OTHER';
		vehicle.versionLabel = 'VersionLabel';
		vehicle.registrationDate = '1991-09-10';
		vehicle.new_x = 'VN';
		vehicle.firstRegistrationDate = '1987-05-01';
		vehicle.registration = '381JNK75';
		vehicle.possessionBegin = '1991-09-10';
		vehicle.technicalInspectionDate = '2010-06-01';
		
		pDataResponse.clientList = new List<TRVwsdlBcs.clientList>();
		client.vehicleList = new List<TRVwsdlBcs.vehicleList>();
		client.vehicleList.add(vehicle);
		pDataResponse.clientList.add(client);
		TRVwsdlBcs.getCustDataResponse getCustDataResponse = new TRVwsdlBcs.getCustDataResponse();
		getCustDataResponse.response = pDataResponse;
		TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element response = new TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element();
		response.getCustDataResponse = getCustDataResponse;
		
		TRVwsdlBcs.BCS_spcCrmGetCustDataService BCS_spcCrmGetCustDataService = new TRVwsdlBcs.BCS_spcCrmGetCustDataService();
		TRVwsdlBcs.getCustDataResponseTopElmt getCustDataResponseTopElmt = new TRVwsdlBcs.getCustDataResponseTopElmt();
        getCustDataResponseTopElmt.getCustDataResponse = getCustDataResponse;

    	return response;
    }
}