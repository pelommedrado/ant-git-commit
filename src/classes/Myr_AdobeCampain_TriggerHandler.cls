/*****************************************************************************************
 Name    : Myr_AdobeCampain_TriggerHandler                     Creation date : 29 Apr 2016
 Desc    : Adobe Campain synchronizations 
 Author  : Donatien Veron (AtoS)
 Project : myRenault
******************************************************************************************/
public class Myr_AdobeCampain_TriggerHandler{  
	private static Set<String> hasRun = new Set<String>();

	//Called by the Account After Update Trigger
	public static void onAccountAfterUpdate( List<Account> listNewAccount, Map<Id, Account> mapOldAccount ) {
		system.debug('### Myr_AdobeCampain_TriggerHandler - <onAccountAfterUpdate> - BEGIN');
		String preRegistered = system.Label.Myr_Status_Preregistrered;
		List<Account> L_A= new List<Account>();
		Map<Id,Account> M_A = new Map<Id,Account>();
		for( Account a : listNewAccount ) {
			String oldMyRStatus = (mapOldAccount!=null && mapOldAccount.get(a.Id)!=null) ? mapOldAccount.get(a.Id).Myr_Status__c : '';
			String oldMyDStatus = (mapOldAccount!=null && mapOldAccount.get(a.Id)!=null) ? mapOldAccount.get(a.Id).Myd_Status__c : '';
			if( preRegistered.equalsIgnoreCase( a.Myr_Status__c ) && !preRegistered.equalsIgnoreCase( oldMyRStatus )
				|| preRegistered.equalsIgnoreCase( a.Myd_Status__c ) && !preRegistered.equalsIgnoreCase( oldMyDStatus ) ) 
			{
				L_A.add( a );
				M_A.put( a.Id,mapOldAccount.get(a.Id) );	
			} 
		}
		//preregistered case
		if( L_A.size()>0 ) {
			system.Debug('### Myr_AdobeCampain_TriggerHandler - <onAccountAfterUpdate> - AC_Preregistration('+L_A+', '+M_A+')');
			AC_Preregistration(L_A, M_A);
		}
		system.debug('### Myr_AdobeCampain_TriggerHandler - <onAccountAfterUpdate> - END');
	}

	//Evaluation of conditions  
	public static List<Account> AC_Preregistration_Selecting(list<Account> listAcc, Map<Id,Account> oldMap) {
		String MyBrand;
		List<Account> L_A = new List<Account>();
		for (Account myA: listAcc){
			MyBrand=get_Adobe_Brand(new Brand_Input(    system.Label.Myr_Status_Preregistrered
								, myA.MYR_Status__c
								, myA.MYD_Status__c
								, myA.MyR_Last_Connection_Date__c
								, myA.MyD_Last_Connection_Date__c
								));
		  System.debug('AC_Preregistration_Selecting MyBrand=<' + MyBrand + '>');
		  System.debug('AC_Preregistration_Selecting.myA.MYR_Status__c=' + myA.MYR_Status__c);
		  System.debug('AC_Preregistration_Selecting.myA.Country__c=' + myA.Country__c);
		  System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable=' + Myr_AdobeCampain.Is_AC_Synchronizable(myA.Country__c, MyBrand, system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration));
		  System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable.oldMap=<' + oldMap + '>');
		  if (oldMap!=null){
			System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable.oldMap.get(myA.Id)=<' + oldMap.get(myA.Id) + '>');
			System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable.oldMap.get(myA.Id).MYR_Status__c=<' + oldMap.get(myA.Id).MYR_Status__c + '>');
			System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable.oldMap.get(myA.Id).MYD_Status__c=<' + oldMap.get(myA.Id).MYD_Status__c + '>');
		  }
      
		  if(
			  !String.isEmpty(myA.Country__c)
			&& Myr_AdobeCampain.Is_AC_Synchronizable(myA.Country__c, MyBrand, system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration)
			  &&
			  (
				(
				  oldMap==null
				  &&
				  (
				  (String.isNotEmpty(myA.MYR_Status__c) && myA.MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Preregistrered))
				  || 
					(String.isNotEmpty(myA.MYD_Status__c) && myA.MYD_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Preregistrered))
				)          
			  )
			  ||
			  (
				oldMap!=null
				&& 
				(
				  (
					String.isNotEmpty(myA.MYR_Status__c) 
					&&
					myA.MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Preregistrered) 
					&&
					(oldMap.get(myA.Id)==null || String.isEmpty(oldMap.get(myA.Id).MYR_Status__c) || !oldMap.get(myA.Id).MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Preregistrered))
				  )
				  ||
				  (
					String.isNotEmpty(myA.MYD_Status__c) 
					&&
					  myA.MYD_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Preregistrered)
					&&
					(oldMap.get(myA.Id)==null || String.isEmpty(oldMap.get(myA.Id).MYD_Status__c) || !oldMap.get(myA.Id).MYD_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Preregistrered))
				  )
				)
			  )
			)
		  ){
			System.debug('AC_Preregistration_Selecting.Is_AC_Synchronizable.myA=<' + myA + '>');
			L_A.add(myA);
		  }
		}
		return L_A;
	} 
	
	//Called from Account After Update (only one possibility to make an activated account
	private static void AC_Synchronization(Id Account_Id, String Country, String My_Brand) { 
		system.debug('### Myr_AdobeCampain_TriggerHandler.AC_Synchronization : Account_Id=<' + Account_Id + '>, Country=<'+ Country + '>, My_Brand=<' + My_Brand + '>');
		try{	        
			if( Myr_AdobeCampain.Is_Org_AC_Synchronizable() && Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand, system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation)){
				if( system.isFuture() ) {
					Myr_AdobeCampain.sync(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation, My_Brand, Account_Id);
				} else {
					Myr_AdobeCampain.synchronize(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation, My_Brand, Account_Id);
				}
			}else{
				system.debug('### Myr_AdobeCampain_TriggerHandler.AC_Synchronization : Myr_AdobeCampain.Is_Org_AC_Synchronizable()=<' + Myr_AdobeCampain.Is_Org_AC_Synchronizable() + '>, Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand,\'Activation\')=<'+ Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand,'Activation') + '>');
			}
		}catch (Exception e){     
			//
		}
    }
  
  //Called from Account_AfterInsert_Trigger & Account_AfterUpdate_Trigger
	public static void AC_Preregistration(list<Account> listAcc, Map<Id,Account> oldMap) {
    String MyBrand;
    System.debug('Myr_AdobeCampain_TriggerHandler.AC_Preregistration : You can follow execution of Preregistration =<'+!hasRun.contains('AdobeCampain_Synchro_For_Account')+'>');
    if (!hasRun.contains('AdobeCampain_Synchro_For_Account')){
      hasRun.add('AdobeCampain_Synchro_For_Account');
      List<Account> L_A = AC_Preregistration_Selecting(listAcc, oldMap);
      System.debug('Myr_AdobeCampain.AC_Preregistration : L_A.size=<' + L_A.size() + '>');
      
      for(Account MyA : L_A){
        MyBrand=get_Adobe_Brand(new Brand_Input(  system.Label.Myr_Status_Preregistrered
                            , myA.MYR_Status__c
                            , myA.MYD_Status__c
                            , myA.MyR_Last_Connection_Date__c
                            , myA.MyD_Last_Connection_Date__c
                            ));
        if (System.isFuture()){
          Myr_AdobeCampain.sync(system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration, MyBrand, MyA.Id);
        }else{
          Myr_AdobeCampain.synchronize(system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration, MyBrand, MyA.Id);
        }
      }
    }
  }
  
  public static List<Id> AC_NewCar_Selecting(list<VRE_VehRel__c> listVR, Map<Id,VRE_VehRel__c> oldMap) {        
    list<Id> L_I = new list<Id>();
    System.debug('AC_NewCar_Selecting:listVR=<' + listVR + '>');
    System.debug('AC_NewCar_Selecting:oldMap=<' + oldMap + '>');

    for(VRE_VehRel__c myVr : listVR){
    System.debug('myVr.account__c=<' + myVr.account__c + '>');
      System.debug('myVr.vin__c=<' + myVr.vin__c + '>');
      System.debug('oldMap=<' + oldMap + '>');
      if (oldMap!=null){
        System.debug('oldMap.get(myVr.Id).account__c=<' + oldMap.get(myVr.Id).account__c + '>');
        System.debug('oldMap.get(myVr.Id).vin__c=<' + oldMap.get(myVr.Id).vin__c + '>');
      }
      
      if(
           String.isNotEmpty(myVr.account__c) 
        && String.isNotEmpty(myVr.vin__c)
        &&
        (  oldMap==null
          ||
          (     oldMap!=null 
              && 
              (
                 !myVr.account__c.equals(oldMap.get(myVr.Id).account__c)
              || !myVr.vin__c.equals(oldMap.get(myVr.Id).vin__c)
              )
          ) 
        ) 
      ){
        
        L_I.add(myVr.Id);
      }
    }
    System.debug('AC_NewCar_Selecting:L_I=<' + L_I + '>');
        return L_I; 
    }

  public static String Get_Adobe_Brand(Brand_Input B_I){ 
    String MyBrand;
    System.debug('Get_Adobe_Brand:Status=<'+ B_I.Status+'>');
    System.debug('Get_Adobe_Brand:Myr_Status=<'+ B_I.Myr_Status+'>');
    System.debug('Get_Adobe_Brand:Myd_Status=<'+ B_I.Myd_Status+'>');
    System.debug('Get_Adobe_Brand:MyR_Last_Connection_Date=<'+ B_I.MyR_Last_Connection_Date+'>');
    System.debug('Get_Adobe_Brand:MyD_Last_Connection_Date=<'+ B_I.MyD_Last_Connection_Date+'>');

    If(    B_I.Myr_Status!=null
       &&  B_I.Myr_Status.equalsIgnoreCase(B_I.Status)
       && (B_I.Myd_Status==null || (B_I.Myd_Status!=null && !B_I.Myd_Status.equalsIgnoreCase(B_I.Status)))
    ){
      System.debug('Get_Adobe_Brand:1');
      MyBrand=Myr_MyRenaultTools.Brand.RENAULT.name();
    }
    
    If(    B_I.Myd_Status!=null 
       &&  B_I.Myd_Status.equalsIgnoreCase(B_I.Status)
       && (B_I.Myr_Status==null || (B_I.Myr_Status!=null && !B_I.Myr_Status.equalsIgnoreCase(B_I.Status)))
    ){
      System.debug('Get_Adobe_Brand:2');
      MyBrand=Myr_MyRenaultTools.Brand.DACIA.name();
    }

    If(    B_I.Myr_Status!=null
      && B_I.Myr_Status.equalsIgnoreCase(B_I.Status)
      && B_I.Myd_Status!=null
      && B_I.Myd_Status.equalsIgnoreCase(B_I.Status)
    ){
      If(B_I.MyR_Last_Connection_Date!=null && B_I.MyD_Last_Connection_Date==null){
        System.debug('Get_Adobe_Brand:3');
        MyBrand=Myr_MyRenaultTools.Brand.RENAULT.name();
      }else{
        if (B_I.MyR_Last_Connection_Date==null && B_I.MyD_Last_Connection_Date!=null){
          System.debug('Get_Adobe_Brand:4');
          MyBrand=Myr_MyRenaultTools.Brand.DACIA.name();
        }else{
          If(B_I.MyD_Last_Connection_Date > B_I.MyR_Last_Connection_Date){
            System.debug('Get_Adobe_Brand:5');
            MyBrand=Myr_MyRenaultTools.Brand.DACIA.name();
          }else{
            System.debug('Get_Adobe_Brand:6');
            MyBrand=Myr_MyRenaultTools.Brand.RENAULT.name();
          }
        }
      }
    }
    System.debug('Myr_AdobeCampain_TriggerHandler.Get_Adobe_Brand=<' + MyBrand + '>');
    return MyBrand;
  }

  public class Brand_Input{
    public String Status { get; set; }
    public String Myr_Status { get; set; }
    public String Myd_Status { get; set; }
    public Date MyR_Last_Connection_Date { get; set; }
    public Date MyD_Last_Connection_Date { get; set; }
    
    public Brand_Input(String M_Status, String Myr_S, String Myd_S, Date MyR_L, Date MyD_L) { 
            this.Status = M_Status;
      this.Myr_Status = Myr_S;
      this.Myd_Status = Myd_S;
      this.MyR_Last_Connection_Date = MyR_L;
      this.MyD_Last_Connection_Date = MyD_L;
        }
  }

  //Called by VehicleRelation_AfterInsert_Trigger & VehicleRelation_AfterUpdate_Trigger
    public static void AC_NewCar(list<VRE_VehRel__c> listVehRel, Map<Id,VRE_VehRel__c> oldMap) {
    System.debug('Myr_AdobeCampain.AC_NewCar : !hasRun.contains(\'AdobeCampain_Synchro_For_Vehicle\')=<'+!hasRun.contains('AdobeCampain_Synchro_For_Vehicle')+'>');
    if (!hasRun.contains('AdobeCampain_Synchro_For_Vehicle')){
      hasRun.add('AdobeCampain_Synchro_For_Vehicle');
      
      List<Id> L_I = AC_NewCar_Selecting(listVehRel, oldMap);
      System.debug('Myr_AdobeCampain.AC_NewCar:L_I.size=<' + L_I.size() + '>');
      if (L_I.size()>0){
        if (System.isFuture()){
          NewCar_Sy(L_I, UserInfo.getUserName());
        }else{
          NewCar_Sy_Async(L_I, UserInfo.getUserName());
        }
      } 
    } 
  }

  @Future(callout=true)
  public static void NewCar_Sy_Async(list<Id> L_I, String username) {
    NewCar_Sy(L_I, username);
  }

  public static void NewCar_Sy(list<Id> L_I, String username) {
        Account M_A;
    List<Account> L_A;
        List<Id> myList;
        String Prefix;
    List<VRE_VehRel__c> lVr;
    String MyBrand;
    String Restriction='';

    System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:L_I=<' + L_I + '>');
    if (L_I.size()>0){
      //Initialization & preparation
      L_A = new List<Account>(); 
      Prefix = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;

      myList = new List<Id>();
      for (Id myId: L_I){
        If(!Restriction.equals('')){
          Restriction+=', ';
        }
        Restriction+= '\'' + myId + '\'';
      }
      //Query the database
      String query=''; 
      query +=' select '; 
      query +='  Id ';
      query +='  , vin__c ';
      query +='  , vin__r.Name ';
      query +='  , account__c ';
      query +='  , account__r.Firstname ';
      query +='  , account__r.Lastname ';
      query +='  , account__r.country__c ';
            query +='  , account__r.MyRenaultID__c ';
      query +='  , account__r.MyDaciaID__c ';
      query +='  , account__r.MyR_Last_Connection_Date__c ';
      query +='  , account__r.MyD_Last_Connection_Date__c ';
      query +='  , account__r.Myr_Status__c ';
      query +='  , account__r.Myd_Status__c ';
      query +='  , account__r.RecordTypeId '; 
            query +='  from '; 
      query +='  VRE_VehRel__c ';
            query +='  where ';
      query +='  Id in (' + Restriction +')';
      query +='  and (account__r.Myr_Status__c = \'' + system.Label.Myr_Status_Activated + '\'';
      query +='  or account__r.Myd_Status__c = \'' + system.Label.Myr_Status_Activated +'\')';
      try {
        System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:query=<' + query + '>'); 
        lVr = Database.query(query);
      } catch(Exception e) {  
        System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:Exception=<' + e.getMessage() + '>'); 
        return;
      }
            
      //Filtering
      for (VRE_VehRel__c myVr: lVr){
        System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:myVr.vin__r.Name=<' + myVr.vin__r.Name + '>'); 
        System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:myVr.account__r.country__c=<' + myVr.account__r.country__c + '>'); 

        if (   String.isNotEmpty(myVr.vin__r.Name)
          && String.isNotEmpty(myVr.account__r.country__c)
        ){

          MyBrand=get_Adobe_Brand(
            new Brand_Input(
                system.Label.Myr_Status_Activated
              , myVr.account__r.MYR_Status__c
              , myVr.account__r.MYD_Status__c
              , myVr.account__r.MyR_Last_Connection_Date__c
              , myVr.account__r.MyD_Last_Connection_Date__c
            )
          );
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:MyBrand=<' + MyBrand + '>');
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:myVr.account__r.country__c=<' + myVr.account__r.country__c + '>');
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:username=<' + username + '>');
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:Prefix + myVr.account__r.MyRenaultID__c=<' + Prefix + myVr.account__r.MyRenaultID__c + '>');
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:Prefix + myVr.account__r.MyDaciaID__c=<' + Prefix + myVr.account__r.MyDaciaID__c + '>');
          System.debug('Myr_AdobeCampain_TriggerHandler.NewCar_Sy:Synchronisable=' + Myr_AdobeCampain.Is_AC_Synchronizable(myVr.account__r.country__c, Myr_MyRenaultTools.Brand.RENAULT.name(), system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar));
          if(
            (
              MyBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name())
              && Myr_AdobeCampain.Is_AC_Synchronizable(myVr.account__r.country__c, Myr_MyRenaultTools.Brand.RENAULT.name(), system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar)
              && (username.equalsIgnoreCase(Prefix + myVr.account__r.MyRenaultID__c) || Test.isRunningTest())
            )
            ||
            (
              MyBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name()) 
              && Myr_AdobeCampain.Is_AC_Synchronizable(myVr.account__r.country__c, Myr_MyRenaultTools.Brand.DACIA.name(), system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar)
              && (username.equalsIgnoreCase(Prefix + myVr.account__r.MyDaciaID__c) || Test.isRunningTest())
            )
          ){
            System.debug('Myr_AdobeCampain_TriggerHandler.synchronize.MyBrand=<' + MyBrand + '>, myVr.account__c=<' + myVr.account__c + '>');
            Myr_AdobeCampain.sync(system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar, MyBrand, myVr.account__c);
          } 
        }
      }
    }
  }

  //Dedicated to testClass
  public class Manage_Log{
    private String Log;
    public Manage_Log(String Log_To_Search_Into) { 
            this.Log = Log_To_Search_Into;
        }
    public String GetMyLog(String Field_Name){ 
      String Log_Value;
      
      Log_Value = this.Log.substring(
                this.Log.indexOf(Field_Name)+Field_Name.length()+3
              , this.Log.indexOf(
                '"'
                , this.Log.indexOf(Field_Name)+Field_Name.length()+3
                )
            ); 
      return Log_Value;
    }
  }
}