@isTest 
private class WSC04_WebServiceDMS_Test {

	static testMethod void consultStockTesting() {
		
		WSC04_VehicleDMSVO vehicleDMSVO = new WSC04_VehicleDMSVO();
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO1 = new List<WSC04_VehicleDMSVO>();

        vehicleDMSVO.VIN = 'Test';
	    vehicleDMSVO.model = 'Fluence';
	    vehicleDMSVO.version = '1000';
		vehicleDMSVO.manufacturingYear = 2010;
	   	vehicleDMSVO.modelYear = 2012;
		vehicleDMSVO.color = 'Yellow';
		vehicleDMSVO.optionals = 'yes';
		vehicleDMSVO.entryInventoryDate  = system.today() - 20;
		vehicleDMSVO.price = 20000;
		vehicleDMSVO.lastUpdateDate = system.now();
		vehicleDMSVO.status = 'Available';
	
		//vehicleDMSVO.error = true;
		//vehicleDMSVO.errorMessage = 'Not Available';

		lstVehicleDMSVO1.add(vehicleDMSVO);
        
		
		List<VRE_VehRel__c>  lstVehicleRelation =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
		
		Integer numberBIRempty;
		List<WSC04_VehicleDMSVO> lstVehicleDMSVOempty = WSC04_WebServiceDMS.consultStock(numberBIRempty);
		
		Integer numberBIR1 = 3000;
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO2 = WSC04_WebServiceDMS.consultStock(numberBIR1);
		
		Integer numberBIR = 1000;
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO = WSC04_WebServiceDMS.consultStock(numberBIR);
		system.debug('lstVehicleDMSVO...' +lstVehicleDMSVO);
		
		List<WSC04_VehicleDMSVO> updateLstVehicleDMSVOError = WSC04_WebServiceDMS.updateStock(numberBIR1, lstVehicleDMSVO2);
		List<WSC04_VehicleDMSVO> updateLstVehicleDMSVO = WSC04_WebServiceDMS.updateStock(numberBIR, lstVehicleDMSVO);
		
		Test.startTest();
		Integer numberBIR2 = 2000;
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO3 = WSC04_WebServiceDMS.consultStock(numberBIR2);
		system.debug('lstVehicleDMSVO...' +lstVehicleDMSVO);
		Test.stopTest();
		System.assert(lstVehicleDMSVO.size() > 0);
		List<WSC04_VehicleDMSVO> updateLstVehicleDMSVO1 = WSC04_WebServiceDMS.updateStock(numberBIR2, lstVehicleDMSVO3);
	}
    
    static testMethod void consultStockTesting2() {
		
		WSC04_VehicleDMSVO vehicleDMSVO = new WSC04_VehicleDMSVO();
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO1 = new List<WSC04_VehicleDMSVO>();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        insert dealer;
        
        Opportunity opp = moc.criaOpportunity();
        insert opp;
        
        Quote quote = moc.criaQuote();
        quote.OpportunityId = opp.id;
        quote.Status = 'Active';
        insert quote;
        
        VEH_Veh__c veh = moc.criaVeiculo();
        insert veh;
        
        VEH_Veh__c veh2 = moc.criaVeiculo();
        veh2.Name = '12345678912458966';
        insert veh2;
        
        VehicleBooking__c vbk = new VehicleBooking__c();
        vbk.Vehicle__c = veh.Id;
        vbk.Status__c = 'Active';
        vbk.Quote__c = quote.id;
        insert vbk;

        vehicleDMSVO.VIN = '12345678912458967';
	    vehicleDMSVO.model = 'Fluence';
	    vehicleDMSVO.version = '1000';
		vehicleDMSVO.manufacturingYear = 2010;
	   	vehicleDMSVO.modelYear = 2012;
		vehicleDMSVO.color = 'Yellow';
		vehicleDMSVO.optionals = 'yes';
		vehicleDMSVO.entryInventoryDate  = system.today() - 20;
		vehicleDMSVO.price = 20000;
		vehicleDMSVO.lastUpdateDate = system.now();
		vehicleDMSVO.status = 'Available';
	
		//vehicleDMSVO.error = true;
		//vehicleDMSVO.errorMessage = 'Not Available';

		lstVehicleDMSVO1.add(vehicleDMSVO);
		
        
        test.startTest();
		
		Integer numberBIR2 = 123456;
        List<WSC04_VehicleDMSVO> updateLstVehicleDMSVO2 = WSC04_WebServiceDMS.updateStock(numberBIR2, lstVehicleDMSVO1);
        
        test.stopTest();
	}
    
    static testMethod void consultStockTesting3() {
		
		WSC04_VehicleDMSVO vehicleDMSVO = new WSC04_VehicleDMSVO();
		List<WSC04_VehicleDMSVO> lstVehicleDMSVO1 = new List<WSC04_VehicleDMSVO>();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        dealer.WebServiceVersion__c = '1.0';
        insert dealer;
        
        Opportunity opp = moc.criaOpportunity();
        insert opp;
        
        Quote quote = moc.criaQuote();
        quote.OpportunityId = opp.id;
        quote.Status = 'Active';
        insert quote;
        
        VEH_Veh__c veh = moc.criaVeiculo();
        insert veh;
        
        VEH_Veh__c veh2 = moc.criaVeiculo();
        veh2.Name = '12345678912458966';
        insert veh2;
        
        VRE_VehRel__c vrl = new VRE_VehRel__c();
        vrl.Account__c = dealer.Id;
        vrl.VIN__c = veh.Id;
        insert vrl;
        
        VehicleBooking__c vbk = new VehicleBooking__c();
        vbk.Vehicle__c = veh.Id;
        vbk.Status__c = 'Active';
        vbk.Quote__c = quote.id;
        insert vbk;

        vehicleDMSVO.VIN = '12345678912458967';
	    vehicleDMSVO.model = 'Fluence';
	    vehicleDMSVO.version = '1000';
		vehicleDMSVO.manufacturingYear = 2010;
	   	vehicleDMSVO.modelYear = 2012;
		vehicleDMSVO.color = 'Yellow';
		vehicleDMSVO.optionals = 'yes';
		vehicleDMSVO.entryInventoryDate  = system.today() - 20;
		vehicleDMSVO.price = 20000;
		vehicleDMSVO.lastUpdateDate = system.now();
		vehicleDMSVO.status = 'Reserved';
	
		//vehicleDMSVO.error = true;
		//vehicleDMSVO.errorMessage = 'Not Available';

		lstVehicleDMSVO1.add(vehicleDMSVO);
		
        
        test.startTest();
		
		Integer numberBIR2 = 123456;
        List<WSC04_VehicleDMSVO> updateLstVehicleDMSVO2 = WSC04_WebServiceDMS.updateStock(numberBIR2, lstVehicleDMSVO1);
        
        test.stopTest();
	}

	/*
	static testMethod void consultPreOrdersTesting() {
		//List<Account> lstNWSiteAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
		//List<VRE_VehRel__c>  lstVehicleRelation =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
		List<Quote> lstQuotes =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToQuote();
		
        Test.startTest();
        
		Integer numberBIRempty;
		List<WSC04_PreOrderDMSVO> lstPreOrderDMSVO2empty = WSC04_WebServiceDMS.consultPreOrders(numberBIRempty);
		
		Integer numberBIR3 = 3000;
		List<WSC04_PreOrderDMSVO> lstPreOrderDMSVO2 = WSC04_WebServiceDMS.consultPreOrders(numberBIR3);
		
		Integer numberBIR4 = 1000;
		List<WSC04_PreOrderDMSVO> lstPreOrderDMSVO = WSC04_WebServiceDMS.consultPreOrders(numberBIR4);
		system.debug('lstPreOrderDMSVO...' +lstPreOrderDMSVO);
        
		Test.stopTest();
		//System.assert(lstPreOrderDMSVO.size() > 0);
		
		List<WSC04_PreOrderDMSVO> updateLstPreOrderDMSVOError = WSC04_WebServiceDMS.updatePreOrders(numberBIR3, lstPreOrderDMSVO2);
		List<WSC04_PreOrderDMSVO> updateLstPreOrderDMSVO1 = WSC04_WebServiceDMS.updatePreOrders(numberBIR4, lstPreOrderDMSVO);
	}*/
}