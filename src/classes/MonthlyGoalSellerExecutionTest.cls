@isTest
private class MonthlyGoalSellerExecutionTest {

    static testMethod void test1() {
        new MonthlyGoalSellerExecution().obterSeller();
    }
    
    static testMethod void test2() {
        new MonthlyGoalSellerExecution().create(null, null);
    }
    
    static testMethod void test3() {
        List<Monthly_Goal_Dealer__c> mgdList = new List<Monthly_Goal_Dealer__c>();
        new MonthlyGoalSellerExecution().createFromGoalDealer(mgdList);
    }
}