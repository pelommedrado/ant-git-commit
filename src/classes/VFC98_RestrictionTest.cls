@isTest
private class VFC98_RestrictionTest {
 
    static testMethod void deveLogicalOperator() {
        String operator = 'AND';
        String constrant = 'constrant';
        String result = VFC98_Restriction.logicalOperator(operator, constrant);
        System.assertEquals(operator + constrant, result);
    }
    
    static testMethod void deveConstrantWithOutSingleQuotes() {
        String fieldName = 'fieldName';
        String operator = 'AND';
        String value = 'value';
        String result = VFC98_Restriction.constrantWithOutSingleQuotes(fieldName, value, operator);
        System.assertEquals(fieldName + operator + value, result);
    }
    
    static testMethod void deveConstrantWithParentheses() {
        String fieldName = 'fieldName';
        String operator = 'AND';
        String value = 'value';
        String result = VFC98_Restriction.constrantWithParentheses(fieldName, value, operator);
        System.assertEquals('( ' + fieldName + operator + value + ' )', result.trim());
    }
    
     static testMethod void deveConstrant() {
        String fieldName = 'fieldName';
        String operator = '=';
        String result = VFC98_Restriction.constrant(fieldName, operator);
        System.assertEquals(fieldName + operator, result);
    }
    
     static testMethod void deveConstrant1() {
        String fieldName = 'fieldName';
        String operator = '=';
        String value = 'value'; 
        String result = VFC98_Restriction.constrant(fieldName, value, operator);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveConstrant2() {
        String fieldName = 'fieldName';
        String operator = '=';
        Boolean value = true; 
        String result = VFC98_Restriction.constrant(fieldName, value, operator);
        System.assertEquals(fieldName + operator + value, result);
    }
    
    static testMethod void deveEq() {
        String fieldName = 'fieldName';
        String operator = ' = ';
        String value = 'value';
        String result = VFC98_Restriction.eq(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveEq1() {
        String fieldName = 'fieldName';
        String operator = ' = ';
        Boolean value = false;
        
        String result = VFC98_Restriction.eq(fieldName, value);
        System.assertEquals(fieldName + operator + value, result);
    }
    
    static testMethod void deveEq2() {
        String fieldName = 'fieldName';
        String operator = ' = ';
        String value = 'value';
        Boolean nNull = true;
        
        String result = VFC98_Restriction.eq(fieldName, value, nNull);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveEq3() {
        String fieldName = 'fieldName';
        String operator = ' = ';
        String value = 'value';
        Boolean nNull = false;
        
        String result = VFC98_Restriction.eq(fieldName, value, nNull);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveNotEq() {
        String fieldName = 'fieldName';
        String operator = ' != ';
        String value = 'value';
        
        String result = VFC98_Restriction.notEq(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveLt() {
        String fieldName = 'fieldName';
        String operator = ' <  ';
        String value = 'value'; 
        String result = VFC98_Restriction.lt(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveLe() {
        String fieldName = 'fieldName';
        String operator = ' <= ';
        String value = 'value'; 
        String result = VFC98_Restriction.le(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveGt() {
        String fieldName = 'fieldName';
        String operator = ' > ';
        String value = 'value'; 
        String result = VFC98_Restriction.gt(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveGe() {
        String fieldName = 'fieldName';
        String operator = ' >= ';
        String value = 'value'; 
        String result = VFC98_Restriction.ge(fieldName, value);
        System.assertEquals(fieldName + operator + '\'' + value + '\'', result);
    }
    
    static testMethod void deveBetweenDates() {
        String fieldName = 'fieldName';
        String lo = 'lo';
        String hi = 'hi';
        String result = VFC98_Restriction.betweenDates(fieldName, lo, hi);
        System.assertEquals('( fieldName >= lo and fieldName <= hi )', result.trim());
    }
    
    static testMethod void deveBetween() {
        String fieldName = 'fieldName';
        Date lo = Date.parse('05/09/2015');
        Date hi = Date.parse('06/09/2015');
        String result = VFC98_Restriction.between(fieldName, lo, hi);
        String exp = '( fieldName >= 2015-09-04T00:00:00Z and fieldName <= 2015-09-05T00:00:00Z )';
		//System.assertEquals(exp, result.trim());
    }
    
    static testMethod void deveBetweenNotEqual() {
        String fieldName = 'fieldName';
        Date lo = Date.parse('05/09/2015');
        Date hi = Date.parse('06/09/2015');
        String result = VFC98_Restriction.betweenNotEqual(fieldName, lo, hi);
        String exp = '( fieldName > 2015-09-05 or fieldName <  2015-09-06 )';
		//System.assertEquals(exp, result.trim());
    }
    
    static testMethod void deveBetweenDatesNotEqual() {
        String fieldName = 'fieldName';
        String lo = 'lo';
        String hi = 'hi';
        String result = VFC98_Restriction.betweenDatesNotEqual(fieldName, lo, hi);
        System.assertEquals('( fieldName > lo or fieldName <  hi )', result.trim());
    }
    
    static testMethod void deveBetween1() {
        String fieldName = 'fieldName';
        String lo = '05/09/2015';
        String hi = '06/09/2015';
        String result = VFC98_Restriction.between(fieldName, lo, hi);
        String exp = 'fieldName >= \'05/09/2015\' and fieldName <= \'06/09/2015\'';
		System.assertEquals(exp, result.trim());
    }
    
    static testMethod void deveIsLike() {
        String fieldName = 'fieldName';
        String value = 'value'; 
        String result = VFC98_Restriction.isLike(fieldName, value);
        System.assertEquals('fieldName like \'value\'', result.trim());
    }
    
    static testMethod void deveEqOrLike() {
        String fieldName = 'fieldName';
        String value = 'value'; 
        String result = VFC98_Restriction.eqOrLike(fieldName, value);
        System.assertEquals('fieldName = \'value\'', result.trim());
    }
    
    static testMethod void deveIsNotNull() {
        String fieldName = 'fieldName';
        String value = 'value'; 
        String result = VFC98_Restriction.isNotNull(fieldName, value);
        System.assertEquals('fieldName != null', result.trim());
    }
    
    static testMethod void deveIsNotNull1() {
        String fieldName = 'fieldName';
         
        String result = VFC98_Restriction.isNotNull(fieldName);
        System.assertEquals('fieldName != null', result.trim());
    }
    
    static testMethod void deveIsNull() {
        String fieldName = 'fieldName';
        String value = 'value'; 
        String result = VFC98_Restriction.isNull(fieldName, value);
        System.assertEquals('fieldName = null', result.trim());
    }
    
    static testMethod void deveNotExpression() {
        String expression = 'expression';
        String result = VFC98_Restriction.notExpression(expression);
        System.assertEquals('NOT  ( expression )', result.trim());
    }
    
    static testMethod void deveIsIn() {
        String fieldName = 'fieldName';
        String[] values = new String[]{ '1', '2', '3' };
            
        String result = VFC98_Restriction.isIn(fieldName, values);
        System.assertEquals('fieldName in ( \'1\',\'2\',\'3\' )', result.trim());
    }
    
     static testMethod void deveIsIn1() {
        String fieldName = 'fieldName';
        String value = '1';
            
        String result = VFC98_Restriction.isIn(fieldName, value);
        System.assertEquals('fieldName in 1', result.trim());
    }
    
    static testMethod void deveIsNotIn() {
        String fieldName = 'fieldName';
        String[] values = new String[]{ '1', '2', '3' };
            
        String result = VFC98_Restriction.isNotIn(fieldName, values);
        System.assertEquals('fieldName NOT  in ( \'1\',\'2\',\'3\' )', result.trim());
    }
    
    static testMethod void deveConvertArrayStringIntoInClause() {
        String[] values = new String[]{ '1', '2', '3' };
        String result = VFC98_Restriction.convertArrayStringIntoInClause(values);
        System.assertEquals('( \'1\',\'2\',\'3\' )', result.trim());
    }
    
    static testMethod void deveAndd() {
        String constrant = 'constrant'; 
        String result = VFC98_Restriction.andd(constrant);
        System.assertEquals(' and constrant', result);
    }
    
    static testMethod void deveOr() {
        String constrant = 'constrant'; 
        String result = VFC98_Restriction.orr(constrant);
        System.assertEquals(' or constrant', result);
    }
    
    static testMethod void deveAddLimit() {
        String result = VFC98_Restriction.addLimit(10);
        System.assertEquals(' limit 10', result);
    }
    
    static testMethod void deveOrderBy() {
        String expression = 'expression';
        String result = VFC98_Restriction.orderBy(expression);
        System.assertEquals(' order by expression', result);
    }
}