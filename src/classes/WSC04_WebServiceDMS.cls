global class WSC04_WebServiceDMS
{

    //**************************************************************************
    // 1. Implements "consultaEstoque"
    //**************************************************************************
    webservice static List<WSC04_VehicleDMSVO> consultStock(Integer numberBIR)
    {
        String strNumberBIR = String.valueOf(numberBIR);

        List<WSC04_VehicleDMSVO> lstVehicleVO = new List<WSC04_VehicleDMSVO>();

        // 1st: Verify if numberBIR not null and cannot be zero.
        List<WSC04_VehicleDMSVO> lstVehicleErrorVO = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForStock(strNumberBIR, 'A101');
        if (lstVehicleErrorVO[0].errorMessage != '') {
            return lstVehicleErrorVO;
        }
 
        // 2nd: Verify if numberBIR exists in Salesforce
        List<WSC04_VehicleDMSVO> lstVehicleErrorVO1 = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRExistsInSalesforceForStock(strNumberBIR, 'A201');
        if (lstVehicleErrorVO1[0].errorMessage != '') {
            return lstVehicleErrorVO1;
        }

        // 3th: Fetch DB for the information
        lstVehicleVO = WSC05_DMS_ValidationBO.getInstance().consultLstVehicleDMSVO(strNumberBIR);

        return lstVehicleVO;
    }


    //**************************************************************************
    // 2. Implements "consultaPreOrdens"
    //**************************************************************************
    webservice static List<WSC04_PreOrderDMSVO> consultPreOrders(Integer numberBIR)
    {
        String strNumberBIR = String.valueOf(numberBIR);

        // 1st: Verify if numberBIR not null and cannot be zero.
        List<WSC04_PreOrderDMSVO> lstPreOrderErrorVO = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForPreOrder(strNumberBIR, 'C101');
        if (lstPreOrderErrorVO[0].errorMessage != '') {
            return lstPreOrderErrorVO;
        }
        
        // 2nd: Verify if numberBIR exists in Salesforce
        List<WSC04_PreOrderDMSVO> lstPreOrderErrorVO1 = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRExistsInSalesforceForPreOrder(strNumberBIR, 'C201');
        if (lstPreOrderErrorVO1[0].errorMessage != '') {
            return lstPreOrderErrorVO1;
        }

        // 3th: Fetch DB for the information
        List<WSC04_PreOrderDMSVO> lstPreOrderVO = WSC05_DMS_ValidationBO.getInstance().consultLstPreOrder(strNumberBIR);

        return lstPreOrderVO;
    }


    //**************************************************************************
    // 3. Implements "atualizaEstoque"
    //**************************************************************************
    webservice static List<WSC04_VehicleDMSVO> updateStock(Integer numberBIR, List<WSC04_VehicleDMSVO> lstVehicleDMSVO)
    {
        String strNumberBIR = String.valueOf(numberBIR);

        // Create a list for vehicles that passed the check
        List<WSC04_VehicleDMSVO> lstOKVehicleDMSVO = new List<WSC04_VehicleDMSVO>();

        // Create a list for vehicles that failed the check
        List<WSC04_VehicleDMSVO> lstErrorVehicleDMSVO = new List<WSC04_VehicleDMSVO>();

        // 1st: Verify if numberBIR not null and cannot be zero.
        List<WSC04_VehicleDMSVO> lstVehicleErrorVO = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForStock(strNumberBIR, 'B101');
        if (lstVehicleErrorVO[0].errorMessage != '') {
            return lstVehicleErrorVO;
        }

        // 2nd: Verify if numberBIR exists in Salesforce
        List<WSC04_VehicleDMSVO> lstVehicleErrorVO1 = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRExistsInSalesforceForStock(strNumberBIR, 'B201');
        if (lstVehicleErrorVO1[0].errorMessage != '') {
            return lstVehicleErrorVO1;
        }

        system.debug('*** lstVehicleDMSVO.size():'+lstVehicleDMSVO.size());
        system.debug('*** lstVehicleDMSVO:'+lstVehicleDMSVO);

        // 3th: Validate vehicle list
        WSC05_DMS_ValidationBO.getInstance().checkLstVehicleDMSVO(lstVehicleDMSVO, lstOKVehicleDMSVO, lstErrorVehicleDMSVO, strNumberBIR);
        
        system.debug('*** lstOKVehicleDMSVO.size():'+lstOKVehicleDMSVO.size());
        system.debug('*** lstOKVehicleDMSVO:'+lstOKVehicleDMSVO);
        system.debug('*** lstErrorVehicleDMSVO.size():'+lstErrorVehicleDMSVO.size());
        system.debug('*** lstErrorVehicleDMSVO:'+lstErrorVehicleDMSVO);

        // 4th: Upsert vehicle list
        if (lstOKVehicleDMSVO.size() > 0)
            WSC06_DMSStockBO.getInstance().updateLstVehicleDMSVO(strNumberBIR, lstOKVehicleDMSVO, lstErrorVehicleDMSVO);

        system.debug('**lstErrorVehicleDMSVO -->'+lstErrorVehicleDMSVO);
        return lstErrorVehicleDMSVO;
    }
    

    //**************************************************************************
    // 4. Implements "atualizaPreOrdens"
    //**************************************************************************
    webservice static List<WSC04_PreOrderDMSVO> updatePreOrders(Integer numberBIR, List<WSC04_PreOrderDMSVO> lstPreOrderDMSVO)
    {
        String strNumberBIR = String.valueOf(numberBIR);

        List<WSC04_PreOrderDMSVO> lstPreOrderDMSVOResult = new List<WSC04_PreOrderDMSVO>();

        // 1st: Verify if numberBIR not null and cannot be zero.
        List<WSC04_PreOrderDMSVO> lstPreOrderErrorVO = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForPreOrder(strNumberBIR, 'D101');
        if (lstPreOrderErrorVO[0].errorMessage != '') {
            return lstPreOrderErrorVO;
        }

        // 2nd: Verify if numberBIR exists in Salesforce
        List<WSC04_PreOrderDMSVO> lstPreOrderErrorVO1 = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRExistsInSalesforceForPreOrder(strNumberBIR, 'D201');
        if (lstPreOrderErrorVO1[0].errorMessage != '') {
            return lstPreOrderErrorVO1;
        }

        // 3th: Verify and update the preOrder
        lstPreOrderDMSVOResult = WSC05_DMS_ValidationBO.getInstance().updateLstPreOrderDMSVO(strNumberBIR, lstPreOrderDMSVO);

        return lstPreOrderDMSVOResult;
    }
    
    //**************************************************************************
    // 5. Implements "enviaLead"
    //**************************************************************************
    /*webservice static String enviaLeads(Integer numberBIR, List<WSC04_LeadSVO> listaLead)
    {
        

        return 'Registros inseridos com sucesso';
    }*/

}