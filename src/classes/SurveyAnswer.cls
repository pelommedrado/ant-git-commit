public with sharing class SurveyAnswer {
	
	public SurveyAnswer(List<SurveySent__c> surveyList) {
		Date hoje = System.today();
		DateTime daysBeforeIni = System.today().addDays(-2);

		SET<Id> surveyAccounts = new SET<Id>();

		for(SurveySent__c survey : surveyList){
			surveyAccounts.add(survey.Account__c);
		}

		Id PVInterviewId;
		Id VNInterviewId;
		List<Interview__c> lstInterview = [SELECT Id, Name FROM Interview__c];

		for(Interview__c interview : lstInterview){
			if(interview.Name.contains('PV')){
				PVInterviewId = interview.Id;
			}
			else if(interview.Name.contains('VN')){
				VNInterviewId = interview.Id;
			}
		}

		List<InterviewResponse__c> interviewResponses = [SELECT Id,AccountId__c, InterviewId__c
														FROM InterviewResponse__c 
														WHERE AnswerDate__c >=: daysBeforeIni 
														AND AccountId__c IN: surveyAccounts]; 

		Map<String,Map<Id, List<InterviewResponse__c>>> interviewByTypeMap = new Map<String,Map<Id, List<InterviewResponse__c>>>();
		interviewByTypeMap.put('PV', new Map<Id, List<InterviewResponse__c>>());
		interviewByTypeMap.put('VN', new Map<Id, List<InterviewResponse__c>>());

		for(InterviewResponse__c interviewRes : interviewResponses){
			if(interviewRes.InterviewId__c == PVInterviewId){
				if(interviewByTypeMap.get('PV').get(interviewRes.AccountId__c) == null){
					interviewByTypeMap.get('PV').put(interviewRes.AccountId__c, new List<InterviewResponse__c>());
				}
				interviewByTypeMap.get('PV').get(interviewRes.AccountId__c).add(interviewRes);
			}
			else if(interviewRes.InterviewId__c == VNInterviewId){
				if(interviewByTypeMap.get('VN').get(interviewRes.AccountId__c) == null){
					interviewByTypeMap.get('VN').put(interviewRes.AccountId__c, new List<InterviewResponse__c>());
				}
				interviewByTypeMap.get('VN').get(interviewRes.AccountId__c).add(interviewRes);
			}
		}

		List<SurveySent__c> surveyToUpdate = new List<SurveySent__c>();

		for(SurveySent__c survey : surveyList){
			if(survey.SendSurvey__c == 'Pesquisa PV'){
				if(interviewByTypeMap.get('PV').get(survey.Account__c) != null &&
					interviewByTypeMap.get('PV').get(survey.Account__c).size() > 0){
					survey.SurveyAnswered__c = true;
					surveyToUpdate.add(survey);
				}
			}
			else if(survey.SendSurvey__c == 'Pesquisa VN'){
				if(interviewByTypeMap.get('VN').get(survey.Account__c) != null &&
					interviewByTypeMap.get('VN').get(survey.Account__c).size() > 0){
					survey.SurveyAnswered__c = true;
					surveyToUpdate.add(survey);
				}
			}
		}

		Database.update(surveyToUpdate);
	}
}