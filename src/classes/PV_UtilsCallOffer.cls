public class PV_UtilsCallOffer {
    
    private List<PVCall_Offer__c> callOfferList;
    private String userId;
    private List<PV_FinancingConditions__c> lsFinancingConditions;
    Map<Id,PVCall_Offer__c>callOfferMap;
    public PV_UtilsCallOffer(){
        userId = UserInfo.getUserId();
        callOfferList = new List<PVCall_Offer__c>();
        lsFinancingConditions = new List<PV_FinancingConditions__c>();
    }
    
    public PV_UtilsCallOffer(List<PVCall_Offer__c> lsCallOffer,Map<Id,PVCall_Offer__c>mapCallOffer){
        this.userId = UserInfo.getUserId();
        this.callOfferList = new List<PVCall_Offer__c>();
        this.lsFinancingConditions = new List<PV_FinancingConditions__c>();
        if(!lsCallOffer.isEmpty())
            this.callOfferList.addAll(lsCallOffer);
        callOfferMap = mapCallOffer;
        lsFinancingConditions.addAll([select id,Coefficient__c,Month__c,Tax__c from PV_FinancingConditions__c where Status__c = 'ACTIVE' and TypePerson__c = 'PF' limit 300]);
    }
    
    //method used in Trigger to beforeInsert 
    public void searchFinancingConditions(){

            
            if(!callOfferList.isEmpty())        
                for(PVCall_Offer__c callOffer : callOfferList){
                    if(Trigger.isInsert){
                        if(String.isNotBlank(callOffer.Status__c) && !callOffer.Status__c.equals('Inactive')){
                            if(returnValueCaseValid(callOffer.Month_Rate__c) && returnValueCaseValid(callOffer.Period_in_Months__c) && !lsFinancingConditions.isEmpty()){
                                
                                for(PV_FinancingConditions__c financingConditions : lsFinancingConditions){
                                    
                                    if(financingConditions.Month__c == callOffer.Period_in_Months__c && financingConditions.Tax__c == callOffer.Month_Rate__c){
                                        callOffer.Coefficient__c = financingConditions.Coefficient__c;
                                        callOffer.Status__c = 'Pending Analyst';
                                        break;
                                    }else{
                                        callOffer.Status__c = 'Pending Analyst';
                                    }
                                }
                                
                                
                            }
                        }
                    }
                    if(Trigger.isUpdate){
                        
                        if(String.isNotBlank(callOffer.Status__c) && !callOffer.Status__c.equals('Inactive') && 
                           !callOffer.Status__c.equals('Active') &&
                           callOfferMap.containsKey(callOffer.Id) && 
                           !callOffer.Status__c.equals('Pending') &&
                           
                           String.isNotBlank(callOfferMap.get(callOffer.Id).Status__c)  && 
                           (!callOfferMap.get(callOffer.Id).Status__c.equals('Pending') || !callOfferMap.get(callOffer.Id).Status__c.equals('Active'))){
                               if(returnValueCaseValid(callOffer.Month_Rate__c) && returnValueCaseValid(callOffer.Period_in_Months__c) && !lsFinancingConditions.isEmpty()
                                 ){
                                     System.debug('#### problema encontrado');	       
                                     for(PV_FinancingConditions__c financingConditions : lsFinancingConditions){
                                         
                                         if(financingConditions.Month__c == callOffer.Period_in_Months__c && financingConditions.Tax__c == callOffer.Month_Rate__c){
                                             callOffer.Coefficient__c = financingConditions.Coefficient__c;
                                             callOffer.Status__c = 'Pending Analyst';
                                             break;
                                         }
                                     }
                                     
                                     
                                 }
                           }
                    }
                    
                }
     
    }
    
    
    
    
    
    //metodo usado para verificar se o valor não se encontra vazio
    public boolean returnValueCaseValid(String value){
        if(String.isNotBlank(value))
            return true;
        return false;
    }
    
    public boolean returnValueCaseValid(Decimal value){
        if(value!=null){
            return true;
        }
        return false;
    }
}