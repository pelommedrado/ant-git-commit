//Use to simulate a response from the WebService
@isTest
global class Test_WS01_ApvGetDetVehXml_WebServiceMock implements WebServiceMock  {

    
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           	
           	
           	
         WS01_ApvGetDetVehXml.DetVehInfoMsg detVehInfoMsg1=new  WS01_ApvGetDetVehXml.DetVehInfoMsg();
    	 detVehInfoMsg1.codeRetour='AA';
    	 detVehInfoMsg1.msgRetour='success';
    	 
    	 WS01_ApvGetDetVehXml.DetVeh detvehmock=new WS01_ApvGetDetVehXml.DetVeh();
    	 
         detvehmock.version='Version';
		 detvehmock.tvv='tvv';
		 detvehmock.typeMot='typeMot';
	     detvehmock.indMot='indMot';
   		 detvehmock.NMot='NMot';
  	     detvehmock.typeBoi='typeBoi';
    	 detvehmock.indBoi='indBoi';
   	     detvehmock.NBoi='NBoi';
         detvehmock.dateTcmFab='11.02.2008';
         detvehmock.dateLiv='11.02.2008';
         detvehmock.libModel='ligne2P12';
         detvehmock.libCarrosserie='BERLINE 4PRTES';
    	 LIST<WS01_ApvGetDetVehXml.DetVehCritere> criteriaList=new LIST<WS01_ApvGetDetVehXml.DetVehCritere>();
    	 WS01_ApvGetDetVehXml.DetVehCritere detVehCriteriaMock=new WS01_ApvGetDetVehXml.DetVehCritere();
    	 detVehCriteriaMock.cdCritOf='criteria';
    	 detVehCriteriaMock.cdObjOf='019';
    	 criteriaList.add(detVehCriteriaMock);
         detvehmock.criteres=criteriaList;
         WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse responseMock=new  WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
         responseMock.detVehInfoMsg=detVehInfoMsg1;
         responseMock.detVeh=detvehmock;
        
        WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element element_mock= new WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element();
        
        
        element_mock.getApvGetDetVehXmlReturn=responseMock;
        

         WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element response_x=new WS01_ApvGetDetVehXml.getApvGetDetVehXmlResponse_element();

        response.put('response_x', element_mock); 

   }
}