public abstract with sharing class FaturaService {

	private static final String BR = 'BR';
	public static Id ACC_PERSONAL = Utils.getRecordTypeId('Account', 'Personal_Acc');
	public static Id ACC_COMPANY 	= Utils.getRecordTypeId('Account', 'Company_Acc');
	public static Id ACC_DEALER		= Utils.getRecordTypeId('Account', 'Network_Site_Acc');

	protected List<FaturaDealer2__c> faturaValidList = new List<FaturaDealer2__c>();
	protected Set<String> customerIdSet = new Set<String>();
    protected Set<String> customerBrIdSet = new Set<String>();

	protected Set<String> validCnpjSet = new Set<String>();
	protected List<FaturaDealer2__c> faturaList;

	public FaturaService(List<FaturaDealer2__c> faturaList) {
		this.faturaList = faturaList;
	}

	public void startValidation() {
		System.debug('startValidation()');

		for(FaturaDealer2__c fatura : faturaList) {
	      	if(isValidation(fatura)) {
				fatura.Error_Messages__c = null;

				validation(fatura);
				if(isFaturaValid(fatura)) {
					faturaValid(fatura);
				}
				else {
					faturaInvalid(fatura);
				}
			}
    	}

		if(!faturaValidList.isEmpty()) {
			processValid();
		}

        Database.upsert(faturaList);
		processExit();
	}

	protected abstract Boolean isValidation(FaturaDealer2__c fatura);
	protected abstract void validation(FaturaDealer2__c fatura);
	protected abstract void processValid();

	protected virtual Boolean isFaturaValid(FaturaDealer2__c fatura) {
		return fatura.Error_Messages__c == null;
	}

	protected virtual void faturaValid(FaturaDealer2__c fatura) {
		fatura.Status__c = 'Valid';
		faturaValidList.add(fatura);
		customerIdSet.add(fatura.Customer_Identification_Number__c);
        customerBrIdSet.add(BR + fatura.Customer_Identification_Number__c);

        if(!String.isEmpty(fatura.CNPJ_Issuer_Register__c)) {
        	validCnpjSet.add(fatura.CNPJ_Issuer_Register__c);
        }
        if(!String.isEmpty(fatura.CNPJ_Matrix_Register__c)) {
        	validCnpjSet.add(fatura.CNPJ_Matrix_Register__c);
        }
	}

	protected virtual void faturaInvalid(FaturaDealer2__c fatura) {
		fatura.Status__c = 'Invalid';
	}

	protected virtual void processExit() {
		System.debug('processExit()');
	}

	protected Map<String, Account> processAccount() {
		System.debug('processAccount()');

		final List<Account> accountAllList = 	[
				SELECT Id, FirstName, LastName, Email__c, Shipping_Street__c,
					Shipping_Complement__c, Shipping_Neighborhood__c, Shipping_City__c,
					Shipping_PostalCode__c, Shipping_State__c, Phone, PersLandline__c,
					PersMobPhone__c, CustomerIdentificationNbr__c, RecordTypeId,
					Tech_ACCExternalID__c, Fax FROM Account
				WHERE CustomerIdentificationNbr__c IN: customerIdSet
        	OR Tech_ACCExternalID__c IN: customerBrIdSet
		];

		final Map<String, Account> customerIdAccountMap = new Map<String, Account>();
		for(Account acc : accountAllList) {
            final String key = getAccountKey(acc);
            if(!String.isEmpty(key)) {
                customerIdAccountMap.put(key, acc);
            }
		}

		for(FaturaDealer2__c fatura : faturaValidList) {
			if(isUpdateAccount(fatura)) {
				final String cpfCnpj = fatura.Customer_Identification_Number__c;

				Account account = customerIdAccountMap.get(cpfCnpj);
				account = updateAccount(account, fatura);
				if(account.Id == null) {
					customerIdAccountMap.put(fatura.Customer_Identification_Number__c, account);
				}
				System.debug('Account: ' + account);
			}
		}

		return customerIdAccountMap;
	}

	protected virtual Boolean isUpdateAccount(FaturaDealer2__c fatura) {
		return true;
	}

	private Account updateAccount(Account account, FaturaDealer2__c fatura) {
		if(account == null) {
			account = createAccount(fatura);
		}
		account.Email__c 	= fatura.Customer_Email__c;
		account.Shipping_Street__c = fatura.Address__c;
		account.Shipping_Complement__c = fatura.Address_Complement__c;
		account.Shipping_Neighborhood__c = fatura.Neighborhood__c;
		account.Shipping_City__c = fatura.City__c;
		account.Shipping_PostalCode__c = fatura.Postal_Code__c;
		account.Shipping_State__c = fatura.State__c;

        if(String.isEmpty(account.CustomerIdentificationNbr__c)) {
            account.CustomerIdentificationNbr__c = fatura.Customer_Identification_Number__c;
        }

		if(account.RecordTypeId == ACC_PERSONAL) {
			account.FirstName 	= formatCustomerName(fatura).substringBefore(' ');
			account.LastName 	= formatCustomerName(fatura).substringAfter(' ');
			//account.PersonBirthdate = fatura.Birth_Date__c == null ? null : Date.valueOf(fatura.Birth_Date__c);
			if(!String.isEmpty(fatura.Phone_1__c)) {
				account.PersLandline__c = fatura.Phone_1__c;
			}
			if(!String.isEmpty(fatura.Phone_2__c)) {
				account.PersMobPhone__c = fatura.Phone_2__c;
			}
			if(!String.isEmpty(fatura.Phone_3__c)) {
				account.Fax	= fatura.Phone_3__c;
			}
		}
		else {
			account.Name = formatCustomerName(fatura);
			account.Phone = fatura.Phone_1__c;
		}
		return account;
	}

	private Account createAccount(FaturaDealer2__c fatura) {
		final Account newAccount  = new Account();
		if(FaturaDealer2Utils.isCustomerTypeEstrangeiro(fatura)) {
			newAccount.CustomerIdentificationNbr__c = fatura.External_Key__c;
		}
		else {
			newAccount.CustomerIdentificationNbr__c = fatura.Customer_Identification_Number__c;
		}

		newAccount.Tech_ACCExternalID__c = generateExternal(newAccount);

		if(FaturaDealer2Utils.isCustomerTypeFisico(fatura)) {
			newAccount.RecordTypeId = ACC_PERSONAL;
		}
		else if(FaturaDealer2Utils.isCustomerTypeJuridico(fatura)) {
			newAccount.RecordTypeId = ACC_COMPANY;
		}
		return newAccount;
	}

	private String generateExternal(Account acc) {
		return BR + acc.CustomerIdentificationNbr__c;
	}

    private String getAccountKey(Account acc) {
        if(!String.isEmpty(acc.CustomerIdentificationNbr__c)) {
            return acc.CustomerIdentificationNbr__c;
        } else if(!String.isEmpty(acc.Tech_ACCExternalID__c)) {
            return acc.Tech_ACCExternalID__c.replace(BR, '');
        }
        return null;
    }

	//Melhorias Fatura Dealer 2
	//3. Criar integração entre os objetos Fatura Dealer 2 e Account por CPF/CNPJ
	//4. Assimilar as contas de Concessionária e Matriz às faturas por meio de lookups
	protected void assignAccount(Map<String, Account> accountByCustomerIdMap){
        System.debug('assignAccount()');
        System.debug('validCnpjSet:' + validCnpjSet.size());

        List<Account> dealersAndGroupsByBir = new List<Account>();
        if(!validCnpjSet.isEmpty()) {
            dealersAndGroupsByBir = [
                SELECT Id, IDBIR__c, Dealer_Matrix_ICB__c, CompanyID__c
                FROM Account
                WHERE CompanyID__c IN: validCnpjSet
                AND RecordTypeId =: ACC_DEALER
            ];
        }

		Map<String, Account> accountByCnpjMap = new Map<String, Account>();

		for(Account iAcc : 	dealersAndGroupsByBir){
			accountByCnpjMap.put(iAcc.CompanyID__c, iAcc);
		}

		for(FaturaDealer2__c fatura : faturaValidList) {
			if(isUpdateAccount(fatura)) {
				final String cpfCnpj = fatura.Customer_Identification_Number__c;
				final Account accountToAssign 	= accountByCustomerIdMap.get(cpfCnpj);
				final Account dealerToAssign 	= accountByCnpjMap.get(fatura.CNPJ_Issuer_Register__c);
				final Account matrixToAssign 	= accountByCnpjMap.get(fatura.CNPJ_Matrix_Register__c);
				if(accountToAssign != null){
					fatura.CustomerAccount__c = accountToAssign.Id;
				}
				if(dealerToAssign != null){
					fatura.DealerAccount__c = dealerToAssign.Id;
				}
				if(matrixToAssign != null){
					fatura.MatrixAccount__c = matrixToAssign.Id;
				}
			}
		}
	}

	private String formatCustomerName(FaturaDealer2__c fatura) {
		if(String.isEmpty(fatura.Customer_Name__c)) {
			return 'Nao identificado';
		}
		return fatura.Customer_Name__c;
	}
}