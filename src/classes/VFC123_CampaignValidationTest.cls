@isTest
public class VFC123_CampaignValidationTest {
    
    public static testMethod void myTest(){
        
        Account a = new Account(Name='test',IDBIR__c='123456',RecordTypeId=Utils.getRecordTypeId('Account', 'Network_Site_Acc'));
        Insert a;
        
        Campaign campPai = new Campaign(Name='Test2',ID_BIR__c='123457',DBMCampaignCode__c='Code',Dealer__c=a.Id);
        Insert campPai;
        
        Campaign c = new Campaign(Name='Test',ID_BIR__c='123456',ParentId=campPai.Id,DBMCampaignCode__c='Code2');
        Insert c;
        
        List<Campaign> lsCampaign = new List<Campaign>();
        lsCampaign.add(c);
        
        VFC123_CampaignValidation obj = new VFC123_CampaignValidation();
        
        obj.hasError = true;
        obj.validadeCampaignForDataLoader(lsCampaign);
        
        obj.hasError = false;
        c.ID_BIR__c = null;
        a.IDBIR__c = null;
        obj.validadeCampaignForDataLoader(lsCampaign);
        
    }
    
}