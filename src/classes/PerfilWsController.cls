global class PerfilWsController {

    webservice static Boolean perfilWs(Id oppId) {
        System.debug('perfilWs:');
        
        String profileName = [
            SELECT Name 
            FROM Profile 
            WHERE Id =: Userinfo.getProfileId() 
            LIMIT 1
        ].Name;
        
        Set<String> permissionByUserList = 
            new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());
        
        if(profileName == 'SFA - Receptionist'  || 
           (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess'))) {
               return true;
               
           }
        return false;
    }
}