/**
*   WebService Class    -   WSC07_AOC_Batch_Step2
*   Author              -   Suresh Babu
*   Date                -   12/03/2013
*   
*   #01 <Suresh Babu> <12/03/2013>
*       This AOC Batch class to fetch every values from Webservice and do the callouts and save the values in SFDC.
**/

global class WSC07_AOC_Batch_Step2 implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
	
	public List<wrapper_priceList> lst_PriceList = new List<wrapper_priceList>();
	public List<wrapper_presentationGroup> lst_PresentationGroup = new List<wrapper_presentationGroup>();
	public Map<String, wrapper_presentationItem> map_Key_with_PresItem = new Map<String, wrapper_presentationItem>();
	
	
	public Map<String, Product2> map_All_Models = new Map<String, Product2>();
	public Map<String, Product2> map_All_VersionsWith_ProductCode = new Map<String, Product2>();
	
	public Map<String, Item__c> map_Items_with_Key = new Map<String, Item__c>();
	public Map<String, Map<String, Vehicle_Item__c>> map_VehicleItems_with_ModelAndItem = new Map<String, Map<String, Vehicle_Item__c>>();
	
	public Map<String, List<wrapper_vehicleItems>> map_ModelIDwith_VehicleItems = new Map<String, List<wrapper_vehicleItems>>();
	
	public Id RecordTypeID_Option;
	
	public Set<Item__c> set_Items = new Set<Item__c>();
	public Set<Vehicle_Item__c> set_Vehicle_Items = new Set<Vehicle_Item__c>();
	
	public List<Item__c> lst_Item_Insert = new List<Item__c>();
	public List<Vehicle_Item__c> lst_Vehicle_Item_Upsert = new List<Vehicle_Item__c>();
	public Map<String, List<Vehicle_Item__c>> map_ItemNameWith_VehicleITem = new Map<String, List<Vehicle_Item__c>>();
	
	public Set<wrapper_allInformationfrom_JSON> set_Allvalues_from_JSON = new Set<wrapper_allInformationfrom_JSON>();
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		system.Debug('*** AOC_Batch_Step2.start()');
		
		map_All_Models = VFC22_ProductDAO.getInstance().fetchAll_Model_Products();
		map_All_VersionsWith_ProductCode = VFC22_ProductDAO.getInstance().fetch_All_Versions_Map_with_ProductCode();
		
		RecordTypeID_Option = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Option' );
		
		String query = 'SELECT Name, Id, Document_URL__c FROM CS_Models__c WHERE Name != null';
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		system.Debug('*** AOC_Batch_Step2.execute()');

		for( sObject modelRecord : scope ){
            CS_Models__c model = (CS_Models__c)modelRecord;
            //system.Debug( ' model -->'+model);
            
            String docURL = model.Document_URL__c;
            String persistURL;
            String categoryURL;
            String priseListURL;
            String urlCode;
            
            if( docURL.contains( 'http://br.co.rplug.renault.com/doc/' )){
            	urlCode = docURL.replace('http://br.co.rplug.renault.com/doc/', '');
            	
            	if( docURL.contains('/doc/') ){
	            	persistURL = docURL.replace( '/doc/', '/pres/');
	            	categoryURL = docURL.replace( '/doc/', '/category/');
	            	priseListURL = docURL.replace( '/doc/', '/pricelist/');
					
		            connectToWS( persistURL, 'pres', model.Name, urlCode );
		            connectToWS( categoryURL, 'category', model.Name, urlCode );
		            connectToWS( priseListURL, 'pricelist', model.Name, urlCode );
	            }
            }
		}
	}
	
	global void connectToWS(String uri, String AOCClient, String key, String urlCode ){
		
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(uri);
		
		Http http = new Http();
		HttpResponse res = http.send(req);
		String response = res.getBody();
		
		if( AOCClient == 'pres' ){
			parsePres( response, key, urlCode );
		}
		else if( AOCClient == 'category' ){
			parseCategory( response, key  );
		}
		else if( AOCClient == 'pricelist' ){
			parsePriceList( response, key );
		}
	}
	
	public void parsePres( String presJSON, String keyValue, String URLCode ){
		
		System.JSONParser parser = JSON.createParser(presJSON);
	    while (parser.nextToken() != null) {
	    	/*
			// Get Equpement values from JSON...
			if( parser.getCurrentName() == 'equipementsPresentation' ){
				while ( parser.getCurrentName() != 'technicalSpecificationsPresentation' && parser.nextToken() != null ){
					
					system.Debug(' current name --->'+ parser.getCurrentName());
					if( parser.getCurrentName() == 'presentationGroup' ){
						parser.nextToken();
						if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
							wrapper_presentationGroup presentation_Group = (wrapper_presentationGroup)parser.readValueAs(wrapper_presentationGroup.class);
							presentation_Group.model_Key = keyValue;
							
							system.Debug(' presentation_Group -->'+presentation_Group);
							
							
							List<wrapper_PresentationItem_Array> item_Array = presentation_Group.items;
							system.Debug( ' item_Array -->'+item_Array);
							
							for( wrapper_PresentationItem_Array item : item_Array ){
								if( item.presentationItem != null ){
									wrapper_presentationItem pres_Item = item.presentationItem;
									map_Key_with_PresItem.put( pres_Item.key, pres_Item );
								}
							}
							
							lst_PresentationGroup.add( presentation_Group );
						}//if
					}//if
				}//while
			}//if
			*/
			
			//
			// Fetch presentation_Item
			//
			if( parser.getCurrentName() == 'map' ){
				while( parser.nextToken() != null && parser.getCurrentName() != 'metadata' ){
					String accessory = '';
					if( parser.getCurrentToken() == JSONToken.FIELD_NAME ){
						accessory = parser.getText();
						
						parser.nextToken();
						if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
							wrapper_presentation_Values presentation_Item = (wrapper_presentation_Values)parser.readValueAs(wrapper_presentation_Values.class);
							presentation_Item.accessory_key = accessory;
							
							wrapper_presentationItem pres_Item = presentation_Item.presentationItem;
							map_Key_with_PresItem.put( pres_Item.key, pres_Item );
						}//if
					}//if
					
				}// while
			}//if
			
	    }//while
	}
	
	public void parseCategory( String categoryJSON, String keyValue ){
		System.JSONParser parser = JSON.createParser(categoryJSON);
		
		// Query Versions and Items and Vehicle Item records in MAP.
		String modelKey_Like = keyValue + '%';
		map_VehicleItems_with_ModelAndItem = VFC64_VehicleItem_DAO.getInstance().return_Map_ModelCode_ItemCode_VehicleItems( modelKey_Like );
		
		//system.Debug(' map_Key_with_PresItem -->'+map_Key_with_PresItem);
		String versionSpecCode;
		
		while( parser.nextToken() != null ){
			if( parser.getCurrentName() == 'versionIdSpecCode' ){
				parser.nextToken();
				
				system.debug('### map_All_Models.get( keyValue )='+map_All_Models.get( keyValue ));
				if (map_All_Models.get(keyValue) != null)
					versionSpecCode = map_All_Models.get( keyValue ).ModelSpecCode__c + '-' + parser.getText();
			}
			
			if( parser.getCurrentName() == 'map' ){
				while( parser.getCurrentToken() != JSONToken.END_OBJECT && parser.nextToken() != null ){
					
					if( parser.getCurrentToken() == JSONToken.FIELD_NAME ){
						if( map_Key_with_PresItem.containsKey( parser.getText())){
							system.Debug('versionSpecCode ---->'+versionSpecCode);
							
							
							Product2 version_Product = map_All_VersionsWith_ProductCode.get( versionSpecCode );
							system.Debug('version_Product ---->'+version_Product);
							wrapper_presentationItem pres_Item = map_Key_with_PresItem.get( parser.getText() );
							
							wrapper_allInformationfrom_JSON wrap_JSON = new wrapper_allInformationfrom_JSON();
							wrap_JSON.vehicleCode = versionSpecCode;
							if(version_Product != null){
								system.Debug('version_Product Id---->'+map_All_VersionsWith_ProductCode.get( versionSpecCode ).Id);
								wrap_JSON.model_version_ID = map_All_VersionsWith_ProductCode.get( versionSpecCode ).Id;
							}
							
							wrap_JSON.item_Key = pres_Item.key;
							wrap_JSON.item_Value = pres_Item.label.pt;
							
							parser.nextToken();
							
							wrap_JSON.category_value = parser.getText();
							
							set_Allvalues_from_JSON.add( wrap_JSON );
						}//if
					}// if
				}// while
			}// if
		}//while
		
		category_Parsing();
		
		set_Allvalues_from_JSON.clear();
		map_Key_with_PresItem.clear();
		map_VehicleItems_with_ModelAndItem.clear();
	}
	
	// List<Id> lst_VersionIDs, Set<Item__c> set_ItemRecords
	public void category_Parsing(){
		// fetch Items with its key value..
		map_Items_with_Key = VFC63_Item_DAO.getInstance().fetch_Option_Items_Map_Name();
		
		system.Debug(' set_Allvalues_from_JSON --->'+set_Allvalues_from_JSON);
		
		for( wrapper_allInformationfrom_JSON wrap_JSON : set_Allvalues_from_JSON ){
			system.Debug(' wrap_JSON.vehicleCode --->'+wrap_JSON.vehicleCode);
			
			if( map_VehicleItems_with_ModelAndItem.containsKey( wrap_JSON.vehicleCode )){
				Map<String, Vehicle_Item__c> inner_Map = map_VehicleItems_with_ModelAndItem.get( wrap_JSON.vehicleCode );
				
				//system.Debug(' wrap_JSON.item_Key --->'+wrap_JSON.item_Key);
				//system.Debug(' inner_Map --->'+inner_Map);
				if( !inner_Map.containsKey( wrap_JSON.item_Key )){
					//	Insert New Item which came from JSON...
					Item__c new_Item = new Item__c();
					Item__c Old_Item = new Item__c();
					Vehicle_Item__c new_Vehicle_Item = new Vehicle_Item__c();
					
					if( !map_Items_with_Key.containsKey( wrap_JSON.item_Key )){
						
						new_Item.Name = wrap_JSON.item_Key;
						new_Item.Key__c = wrap_JSON.item_Key;
						new_Item.Label__c = wrap_JSON.item_Value;
						new_Item.RecordTypeId = RecordTypeID_Option;
						
						set_Items.add( new_Item );
					}
					else{
						
						Old_Item = map_Items_with_Key.get( wrap_JSON.item_Key );
						new_Vehicle_Item.Item__c = Old_Item.Id;
					}
					
					new_Vehicle_Item.ModelVersion__c = wrap_JSON.model_version_ID;
					new_Vehicle_Item.Category__c = wrap_JSON.category_value;
					new_Vehicle_Item.isActive__c = true;
					
					if( new_Vehicle_Item.Item__c == null ){
						List<Vehicle_Item__c> let_Temp_VEH = new List<Vehicle_Item__c>();
						if( map_ItemNameWith_VehicleITem.containsKey( new_Item.Key__c )){
							let_Temp_VEH = map_ItemNameWith_VehicleITem.get( new_Item.Key__c );
							let_Temp_VEH.add( new_Vehicle_Item );
							map_ItemNameWith_VehicleITem.put( new_Item.Key__c, let_Temp_VEH );
						}
						else{
							let_Temp_VEH.add( new_Vehicle_Item );
							map_ItemNameWith_VehicleITem.put( new_Item.Key__c, let_Temp_VEH );
						}
					}
					else{
						lst_Vehicle_Item_Upsert.add( new_Vehicle_Item );
					}
				}
				else{
					Vehicle_Item__c OLD_Vehicle_Item = new Vehicle_Item__c();
					OLD_Vehicle_Item = inner_Map.get( wrap_JSON.item_Key );
					
					OLD_Vehicle_Item.isActive__c = true;
					// For UPDATE vehicle Item records..
					lst_Vehicle_Item_Upsert.add( OLD_Vehicle_Item );
					
					inner_Map.remove( wrap_JSON.item_Key );
				}
			}
			else{
				// Insert New Item record ans Vehicle Item records..
				Item__c new_Item = new Item__c();
				Item__c Old_Item = new Item__c();
				Vehicle_Item__c new_Vehicle_Item = new Vehicle_Item__c();
				
				if( !map_Items_with_Key.containsKey( wrap_JSON.item_Key )){
					
					new_Item.Name = wrap_JSON.item_Key;
					new_Item.Key__c = wrap_JSON.item_Key;
					new_Item.Label__c = wrap_JSON.item_Value;
					new_Item.RecordTypeId = RecordTypeID_Option;
					
					set_Items.add( new_Item );
				}
				else{
					
					Old_Item = map_Items_with_Key.get( wrap_JSON.item_Key );
					new_Vehicle_Item.Item__c = Old_Item.Id;
				}
				
				new_Vehicle_Item.ModelVersion__c = wrap_JSON.model_version_ID;
				new_Vehicle_Item.Category__c = wrap_JSON.category_value;
				new_Vehicle_Item.isActive__c = true;
				
				if( new_Vehicle_Item.Item__c == null ){
					List<Vehicle_Item__c> let_Temp_VEH = new List<Vehicle_Item__c>();
					if( map_ItemNameWith_VehicleITem.containsKey( new_Item.Key__c )){
						let_Temp_VEH = map_ItemNameWith_VehicleITem.get( new_Item.Key__c );
						let_Temp_VEH.add( new_Vehicle_Item );
						map_ItemNameWith_VehicleITem.put( new_Item.Key__c, let_Temp_VEH );
					}
					else{
						let_Temp_VEH.add( new_Vehicle_Item );
						map_ItemNameWith_VehicleITem.put( new_Item.Key__c, let_Temp_VEH );
					}
				}
				else{
					lst_Vehicle_Item_Upsert.add( new_Vehicle_Item );
				}
			}
		}
		
		/**
		// Suresh - Commented as mentioned by Renault
		// IF No Key came from JSON we should disable by here..!
		for( Map<String, Vehicle_Item__c> map_UPDATE : map_VehicleItems_with_ModelAndItem.values()){
			for( Vehicle_Item__c vehicle_Update : map_UPDATE.values() ){
				vehicle_Update.isActive__c = false;
				
				lst_Vehicle_Item_Upsert.add( vehicle_Update );
			}
		}
		*/
		
		for( Vehicle_Item__c veh : lst_Vehicle_Item_Upsert ){
			system.Debug(' vehicle ------>'+veh);
		}
	}
	
	
	public void parsePriceList(String priceListJSON, String keyValue){
		System.JSONParser parser = JSON.createParser(priceListJSON);
		
		while( parser.nextToken() != null ){
			if( parser.getCurrentName() == 'priceList' ){
				parser.nextToken();
				parser.nextToken();
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					wrapper_priceList priceList = (wrapper_priceList)parser.readValueAs(wrapper_priceList.class);
					priceList.model_Key = keyValue;
					lst_PriceList.add( priceList );
					system.Debug(' priceList ------>'+priceList);
				}
			}
		}
    }
    
    // Wrapper class to store All information from JSON for Category
    
    public class wrapper_allInformationfrom_JSON{
    	public String vehicleCode {get;set;}
    	public String item_Key {get;set;}
    	public String category_value {get;set;}
    	public String model_version_ID {get;set;}
    	public String item_Value {get;set;}
    	public wrapper_allInformationfrom_JSON(){
    		
    	}
    }
    
    // Wrapper class to store the Vehicle Item informations..
    public class wrapper_vehicleItems{
    	public Id modelVersion {get;set;}
    	public String item_Key {get;set;}
    	public String category_val {get;set;}
    	public wrapper_vehicleItems( Id wrap_modelVersion, String wrap_item_Key, String wrap_category_val ){
    		modelVersion = wrap_modelVersion;
    		item_Key = wrap_item_Key;
    		category_val = wrap_category_val;
    	}
    }
    
    // wrapper classes to store the map values...
    
    public class wrapper_presentation_Values{
    	public wrapper_presentationItem presentationItem;
    	public String accessory_key {get;set;}
    	
    	public wrapper_presentation_Values( wrapper_presentationItem wrap_presentationItem ){
    		presentationItem = wrap_presentationItem;
    	}
    }
    
    
    // Wrapper class to store the Equipements.....
    public class wrapper_presentationGroup{
    	public String model_Key {get;set;}
    	public List<wrapper_PresentationItem_Array> items;
    	public String key {get;set;}
    	public wrapper_presentationGroup_Label label;
    	public wrapper_presentationGroup( String wrap_model_Key, List<wrapper_PresentationItem_Array> wrap_items, String wrap_Key, wrapper_presentationGroup_Label wrap_label ){
    		model_Key = wrap_model_Key;
    		if (wrap_items != null) items = wrap_items.clone();
    		key = wrap_Key;
    		label = wrap_label;
    	}
    }
    
    public class wrapper_presentationGroup_Label{
    	public String pt {get;set;}
    }
    
    public class wrapper_PresentationItem_Array{
    	public wrapper_presentationItem presentationItem;
    	public wrapper_PresentationItem_Array( wrapper_presentationItem wrap_presentationItem ){
    		presentationItem = wrap_presentationItem;
    	}
    }
    
    public class wrapper_presentationItem{
    	public String key {get;set;}
    	public wrapper_presentationItem_Label label;
    	
    	public wrapper_presentationItem( String wrap_Key, wrapper_presentationItem_Label wrap_label ){
    		key = wrap_Key;
    		label = wrap_label;
    	}
    }
    
    public class wrapper_presentationItem_Label{
    	public String pt {get;set;}
    }
    
    // Wrapper class to store the Price informations..
    public class wrapper_priceList{
    	public String model_Key {get;set;}
    	List<wrapper_VersionPrice> versionPriceList;
    	public wrapper_priceList( List<wrapper_VersionPrice> wrap_versionPriceList, String wrap_model_Key ){
    		versionPriceList = wrap_versionPriceList;
    		model_Key = wrap_model_Key;
    	}
    }
    
    public class wrapper_VersionPrice{
    	public String versionIdSpecCode {get;set;}
    	public Decimal price {get;set;}
    }
    
    public void Test_Wrapper(){
    	wrapper_VersionPrice vP1 = new wrapper_VersionPrice();
    	vP1.price = 27400;
    	vP1.versionIdSpecCode = 'VEC039_BRES';
    	
    	wrapper_VersionPrice vP2 = new wrapper_VersionPrice();
    	vP2.price = 36370;
    	vP2.versionIdSpecCode = 'VEC041_BRES';
    	
    	List<wrapper_VersionPrice> lstPR = new List<wrapper_VersionPrice>();
    	lstPR.add( vP1 );
    	lstPR.add( vP2 );
    	
    	wrapper_priceList price = new wrapper_priceList( lstPR, 'LCM' );
    	system.Debug(' price -->'+price);
    	
    }
	
	global void finish(Database.BatchableContext BC)
	{
		system.Debug('*** AOC_Batch_Step2.finish()');

		system.Debug(' lst_PriceList -->'+lst_PriceList);
		
		// Fetch standard Pricebook2
		Pricebook2 standardPriceBook = new Pricebook2();
		standardPriceBook = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();
		
		// Fetch a map of Models where key is ModelSpecCode__c
		Map<String , Product2> map_ProductModel_with_ModelSpec = new Map<String, Product2>();
		map_ProductModel_with_ModelSpec = VFC22_ProductDAO.getInstance().fetchAll_Model_Products();
		system.Debug(' map_ProductModel_with_ModelSpec -->'+map_ProductModel_with_ModelSpec);
		
		// Fetch a map with a list of versions where key is ModelId
		Map<Id, List<Product2>> map_Versions_with_ModelId = new Map<Id, List<Product2>>();
		map_Versions_with_ModelId = VFC22_ProductDAO.getInstance().fetchAll_Version_Products();
		
		system.Debug(' map_Versions_with_ModelId -->'+map_Versions_with_ModelId);
		
		// Fetch a map of PriceBookEntry where key is ProductId
		Map<Id, PricebookEntry> map_PriceBookEntry_With_ProductId = new Map<Id, PricebookEntry>();
		map_PriceBookEntry_With_ProductId = VFC24_PriceBookDAO.getInstance().return_Map_ProductID_PriceBookEntry();
		system.Debug(' map_PriceBookEntry_With_ProductId -->'+map_PriceBookEntry_With_ProductId);
		
		List<PricebookEntry> lst_PriceBookEntry_Upsert = new List<PricebookEntry>();
		
		//
		// Process Prices
		//
		for (wrapper_priceList wrap_PriceList : lst_PriceList)
		{
			List<wrapper_VersionPrice> lst_VersionPrice = new List<wrapper_VersionPrice>();
			lst_VersionPrice = wrap_PriceList.versionPriceList;
			
			for( wrapper_VersionPrice versionPrice : lst_VersionPrice )
			{
				String versionCode = wrap_PriceList.model_Key + '-' + versionPrice.versionIdSpecCode;
				
				system.Debug(' versionPrice -->'+versionPrice);
				if (map_All_VersionsWith_ProductCode.containsKey( versionCode ) )
				{
					Id product_version_ID =  map_All_VersionsWith_ProductCode.get( versionCode ).Id;
					if( map_PriceBookEntry_With_ProductId.containsKey( product_version_ID ) )
					{
						PricebookEntry prod_priceBookEntry = map_PriceBookEntry_With_ProductId.get( product_version_ID );
						
						prod_priceBookEntry.UnitPrice = versionPrice.price;
						system.Debug(' prod_priceBookEntry ---->'+prod_priceBookEntry);
						lst_PriceBookEntry_Upsert.add( prod_priceBookEntry );
					}
					else
					{
						PricebookEntry new_priceBookEntry = new PricebookEntry();
						new_priceBookEntry.IsActive = true;
						new_priceBookEntry.UnitPrice = versionPrice.price;
						new_priceBookEntry.Product2Id = product_version_ID;
						new_priceBookEntry.Pricebook2Id = standardPriceBook.Id;
						new_priceBookEntry.UseStandardPrice = false;
						
						system.Debug(' new_priceBookEntry ---->'+new_priceBookEntry);
						lst_PriceBookEntry_Upsert.add( new_priceBookEntry );
					}
				}
			}
		}
		
		// Remove possible duplicates?
		Set<PricebookEntry> set_version_pricebook = new Set<PricebookEntry>();
		set_version_pricebook.addAll( lst_PriceBookEntry_Upsert );
		lst_PriceBookEntry_Upsert.clear();
		lst_PriceBookEntry_Upsert.addAll( set_version_pricebook );
		
		system.Debug(' lst_PriceBookEntry_Upsert -->'+lst_PriceBookEntry_Upsert);
		
		/**
		// Do the upsert
		**/
		VFC24_PriceBookDAO.getInstance().upsertData( lst_PriceBookEntry_Upsert );
		
		
		/**
		//	Category Processing..
		**/
		
		// WE can dirctly UPDATE "lst_Vehicle_Item_Upsert" set for Vehicle Item..
		
		system.Debug(' map_ItemNameWith_VehicleITem -->'+map_ItemNameWith_VehicleITem.keySet());
		system.Debug(' set_Items ---->'+set_Items);
		for( Item__c itm : set_Items ){
			system.Debug( ' set Items ---->'+itm);
		}
		
		lst_Item_Insert.addAll( set_Items );
		
		/****************************************************************
		// For Tesing ONLY...
		for( Integer i = 0; i < lst_Item_Insert.size(); i++ ){
			Item__c Inserted_Item = lst_Item_Insert[i];
			if( map_ItemNameWith_VehicleITem.containsKey( Inserted_Item.Key__c )){
				List<Vehicle_Item__c> lst_Vehicle_Items = map_ItemNameWith_VehicleITem.get( Inserted_Item.Key__c );
				
				for( Vehicle_Item__c vehicle : lst_Vehicle_Items ){
					lst_Vehicle_Item_Upsert.add( vehicle );
				}
			}
		}
		
		for( Vehicle_Item__c veh : lst_Vehicle_Item_Upsert ){
			system.Debug(' vehicle UPsert -->'+veh);
		}
		
		set_Vehicle_Items.addAll( lst_Vehicle_Item_Upsert );
		lst_Vehicle_Item_Upsert.clear();
		lst_Vehicle_Item_Upsert.addAll( set_Vehicle_Items );
		
		for( Vehicle_Item__c veeh : lst_Vehicle_Item_Upsert ){
			system.Debug( 'after validating -->'+veeh);
		}
		
		////////////////
		***************************************************************/
		
		
		// Insert Items
		List<Database.Saveresult> lst_Item_SaveResult = VFC63_Item_DAO.getInstance().insertData( lst_Item_Insert );
		
		for (Integer i = 0; i < lst_Item_SaveResult.size(); i++)
		{
	    	Database.Saveresult insert_Result = lst_Item_SaveResult[i];
	    	
			if( insert_Result.isSuccess() ){
				Item__c Inserted_Item = lst_Item_Insert[i];
				
				if( map_ItemNameWith_VehicleITem.containsKey( Inserted_Item.Key__c )){
					List<Vehicle_Item__c> lst_Vehicle_Items = map_ItemNameWith_VehicleITem.get( Inserted_Item.Key__c );
					
					for( Vehicle_Item__c vehicle : lst_Vehicle_Items ){
						vehicle.Item__c = insert_Result.getId();
						lst_Vehicle_Item_Upsert.add( vehicle );
					}
				}
			}
	    }
	    
	    set_Vehicle_Items.addAll( lst_Vehicle_Item_Upsert );
		lst_Vehicle_Item_Upsert.clear();
		lst_Vehicle_Item_Upsert.addAll( set_Vehicle_Items );
		
		// Upsert Vehicle Item...
		List<Database.Upsertresult> lst_Vehicle_Item_Upsertresult = VFC64_VehicleItem_DAO.getInstance().upsertData( lst_Vehicle_Item_Upsert );


		if (!Test.isRunningTest())
		{
			// Create / update a PricebookEntry record that will determine the model's
			// price. The price will be the smallest version price available.
			WSC07_AOC_BO.getInstance().updateModePricesWithTheSmallestVersionPrice();
		}
	}
}