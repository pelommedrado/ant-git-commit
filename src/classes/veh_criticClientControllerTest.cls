@isTest
public class veh_criticClientControllerTest {
    
    static testMethod void test1(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        VEH_Veh__c veh = moc.criaVeiculo();
        insert veh;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(veh);
        
        veh_criticClientController c = new veh_criticClientController(sc);
        System.assertEquals(veh.CriticalCustomer__c, c.isCritic);
        
        test.stopTest();
        
    }

}