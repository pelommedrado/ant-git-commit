@isTest
public with sharing class Utils_WarrantyDataSourceTest {

  static testMethod void MyTest(){
     
    Utils_WarrantyDataSource uwd = new Utils_WarrantyDataSource();
    VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
    Insert veh;
    
    // Visit the page so we also run the constructor code
    PageReference pageRef = Page.VFP05_VehicleAttributes;
    Test.setCurrentPage(pageRef);

    // Get ready to run the methods
    VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
    uwd.getWarrantyData(vehController);

    /*  Código comentado por @Edvaldo - {kolekto} por achar que se trata de uma classe de teste. É classe de teste?

  		public with sharing class Utils_WarrantyDataSourceTest {
     
        public WS06_servicesBcsDfr.ListDetWarrantyCheck getWarrantyData() {     

          String vin = 'VF1JZ030642742321';        
          String countryCode = 'FR';  
          String date_x = '10-10-99';

        
          WS06_servicesBcsDfr.ApvGetInfoWarrantyCheck WarWS = new WS06_servicesBcsDfr.ApvGetInfoWarrantyCheck();
          WarWS.endpoint_x = 'https://webservices2.renault.fr/ccb/0/ApvGetInfoWarrantyCheck'; 
          WarWS.clientCertName_x = 'CCB_dev_002';      
          WarWS.timeout_x=40000;
          WS06_servicesBcsDfr.ListDetWarrantyCheck WAR_WS = WarWS.GetInfoWarrantyCheck(vin, countryCode, date_x);

          return WAR_WS;

        }

      }

    */

  }
       
}