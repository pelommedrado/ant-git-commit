/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class RetrievePrefferedPhoneNumber {
    global RetrievePrefferedPhoneNumber() {

    }
    @TestVisible
    webService static Boolean agentHasShortcodeNumberAssigned() {
        return null;
    }
    @TestVisible
    webService static String getToPhoneNumberFromObject(Id selectedObjId) {
        return null;
    }
    @TestVisible
    webService static String getToPhoneNumberFromObjects(List<Id> selectedObjIds) {
        return null;
    }
}
