global class GroupGoalsBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        String query = 'SELECT Id, Dealer_Matrix__c ' + 
            + 'FROM Account ' +
            + 'WHERE RecordTypeId = \'' + Utils.getRecordTypeId('Account', 'Network_Site_Acc') + '\' ' +
            + 'AND IDBIR__c != null ' +
            + 'AND Dealer_Matrix__c != null ' + 
            + 'AND Active_to_PPO__c = true';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<Account> groups){
        
        List<Monthly_Goal_Group__c> mggList = new List<Monthly_Goal_Group__c>();
        List<Monthly_Goal_Group__c> mggListExisting = new List<Monthly_Goal_Group__c>();
        
        Set<String> setDistinctMatrix = new Set<String>();
        Set<String> setMatrixExisting = new Set<String>();
        
        System.debug('Groups: ' + groups);
        
        //get distinct matrix values
        for(Account acc : groups){
            setDistinctMatrix.add(acc.Dealer_Matrix__c);
        }
        
        mggListExisting = [SELECT Id, Matrix_Bir__c, Month__c, Year__c
                           FROM Monthly_Goal_Group__c
                           WHERE Month__c =: System.now().format('MMMMM')
                           AND Year__c =: System.today().year()
                           AND Matrix_Bir__c IN : setDistinctMatrix];
        
        for(Monthly_Goal_Group__c mgg : mggListExisting){
            setMatrixExisting.add(mgg.Matrix_Bir__c);
        }
        
        //create group goals
        for(String sets : setDistinctMatrix){
            
            if(!setMatrixExisting.contains(sets)){
                
                Monthly_Goal_Group__c mgg = new Monthly_Goal_Group__c();
                mgg.Matrix_Bir__c = sets;
                mgg.Month__c = System.now().format('MMMMM');
                mgg.Year__c = System.today().year();
                
                mggList.add(mgg);
            }
        }
        
        //insert goals
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            Database.insert(mggList);
            
        } catch(Exception e){
            
            System.debug('#################3 erro: ' + e);
            Database.rollback(sp);
        }
        
        
    }
    
    global void finish(Database.BatchableContext BC){}
    
}