@isTest
public class PromotionalActionsControllerTest {
    
    public static testMethod void test1(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Opportunity opp = moc.criaOpportunity();
        Insert opp;
       
        PageReference pageRef = new PageReference('/apex/PromotionalActions');
        Test.setCurrentPage(pageRef);
        
        PromotionalActionsController controller = new PromotionalActionsController();
        pageRef.getParameters().put('oppId', opp.Id);
        
        List<Opportunity> oppList = controller.lsOpp;
        
        // testa retorno da pagina
        String page = controller.consultarVouchers().getUrl();
        system.assertEquals('/apex/InCourseLeads?CPF='+opp.Account_CPF__c+'&Id='+opp.Id+'&stage='+opp.StageName, page);

        // testa retorno da pagina
        String page2 = controller.loadPageInCourseLeads().getUrl();
        String tipoVoucher = ApexPages.currentPage().getParameters().put('tpVoucher','passante');
        system.assertEquals('/apex/InCourseLeads?tipoVoucher='+tipoVoucher, page2);
    }

}