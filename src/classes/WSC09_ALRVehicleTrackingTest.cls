@isTest
private class WSC09_ALRVehicleTrackingTest {

    static testMethod void myUnitTest() {
          
          
          
          Account dealerAcc = new Account(
              Name = 'Concessionaria teste',
              IDBIR__c = '123ABC123',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc );
        System.debug('######## conta inserido '+dealerAcc);
        
        Account dealerAcc2 = new Account(
              Name = 'Concessionaria teste2',
              IDBIR__c = '123ABC124',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc2 );
        System.debug('######## conta inserido '+dealerAcc2);
        
        Account dealerAcc3 = new Account(
              Name = 'Concessionaria teste3',
              IDBIR__c = '123ABC125',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc3 );
        System.debug('######## conta inserido '+dealerAcc3);
        
        
        
        Account css1 = dealerAcc;
        Account css2 = dealerAcc2;
        Account css3 = dealerAcc3;
          
        
        
              
          VEH_Veh__c vehicle = new VEH_Veh__c(
            ALR_InvoiceNumber__c = '6775434',
              ALR_SerialNumberOnInvoice__c = '543',
              BVM_data_Upto_date__c =false,
              Color__c = 'PRATA KNH',
              CountryOfDelivery__c ='Brasil',
              Billing_Date_SAP__c = date.today(),
              IDBIRSellingDealer__c = css1.IDBIR__c,
              Id_Bir_BuyDealer__c = css2.IDBIR__c,
              Is_Available__c = true,
              ModelCode__c = 'B4M',
              ModelYear__c = 2015,
              Model__c = 'Novo Sandero',
              Name = '93Y5SRD6GAB290228',
              Optional__c = 'ALAR01',
              Price__c =36080.0 ,
              Status__c = 'In Transit',
              Tech_VINExternalID__c = '93Y5SRD6GAB290228',
              VehicleBrand__c = 'Renault',
              Version__c = 'AUTP10D L2',
              VehicleSource__c = 'SAP'
          );
          
          
          VEH_Veh__c vehicle2 = new VEH_Veh__c(
            ALR_InvoiceNumber__c = '6775434',
              ALR_SerialNumberOnInvoice__c = '543',
              BVM_data_Upto_date__c =false,
              Color__c = 'PRATA KNH',
              CountryOfDelivery__c ='Brasil',
              Billing_Date_SAP__c = date.today(),
              IDBIRSellingDealer__c = css1.IDBIR__c,
              Id_Bir_BuyDealer__c = css1.IDBIR__c,
              Is_Available__c = true,
              ModelCode__c = 'B4M',
              ModelYear__c = 2015,
              Model__c = 'Novo Sandero',
              Name = '93Y5SRD6FEL290288',
              Optional__c = 'ALAR01',
              Price__c =36080.0 ,
              Status__c = 'In Transit',
              Tech_VINExternalID__c = '93Y5SRD6FEL290288',
              VehicleBrand__c = 'Renault',
              Version__c = 'AUTP10D L2',
              VehicleSource__c = 'SAP'
          );
          
         List<VEH_Veh__c>  lstVeicOld = new List<VEH_Veh__c>();
         lstVeicOld.add(vehicle);
         lstVeicOld.add(vehicle2);
         Database.insert(lstVeicOld);
          
         
         List<ALR_Tracking__c> listTracking = [select ALR_BIR_Dealer__c,ALR_Brand_Vehicle__c,ALR_BuyDealer__c,ALR_ChassiNotDuplicate__c,ALR_Chassi__c,ALR_Color__c,
             ALR_CTe__c,ALR_Date_Billing__c,ALR_Date_MADP__c,ALR_Dealer_Stock__c,ALR_Dealer__c,ALR_Destroyed__c,ALR_Forecast_Delivery__c,
             ALR_Invoice_Number_Serie__c,ALR_in_Compound__c,ALR_Lock_Type__c,ALR_Model__c,ALR_On_TCC_compound__c,ALR_Pilot_Delivery__c,ALR_Status_Delivery__c,
             ALR_Status_Lock__c,ALR_Status_System_Delivery__c,ALR_Status_System_Lock__c,ALR_Status_Tracking__c,ALR_Vehicle_Position__c,ALR_Version__c,
             ALR_Way_Dealer__c,ALR_Way_Next_Compound__c,ALR_Year_Version__c,CreatedById,CreatedDate,CurrencyIsoCode,Id,IsDeleted,LastActivityDate,
             LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,OwnerId,SystemModstamp  
              from ALR_Tracking__c where ALR_Chassi__c in:(lstVeicOld)] ;
    
    
    WSC09_ALRVehicleTracking veicleTracking = new WSC09_ALRVehicleTracking();
    WSC09_ALRVehicleTracking.ReturnServiceMADP objeto = new WSC09_ALRVehicleTracking.ReturnServiceMADP();
    
    objeto = WSC09_ALRVehicleTracking.getMADP(System.now().addDays(-1));
    
    
    //preenchimento do vehicleTracking
    WSC09_ALRVehicleTracking.VehicleTracking veicTracking= new   WSC09_ALRVehicleTracking.VehicleTracking();
    
    veicTracking.vin = lstVeicOld.get(0).name;
    veicTracking.CTENumber = '123456789123456789';
    veicTracking.forecastDate = String.valueOf(date.today().addMonths(-1));
    veicTracking.isBlock = 'y';
    veicTracking.pilotDate = String.valueOf(date.today().addDays(-1));
    veicTracking.statusVehicle = 'on TCC compound';
    veicTracking.typeBlock = 'Quality';
    veicTracking.dealerArrivalDate = String.valueOf(date.today().addMonths(-2));
    
    
    WSC09_ALRVehicleTracking.VehicleTracking veicTracking2= new   WSC09_ALRVehicleTracking.VehicleTracking();
    veicTracking2.vin = lstVeicOld.get(1).name;
    veicTracking2.CTENumber = '12345';
    veicTracking2.forecastDate = String.valueOf(date.today().addMonths(-1));
    veicTracking2.isBlock = 'y';
    veicTracking2.pilotDate = String.valueOf(date.today().addDays(-1));
    veicTracking2.statusVehicle = 'on compound';
    veicTracking2.typeBlock = 'Quality';
    veicTracking2.dealerArrivalDate = String.valueOf(date.today().addMonths(-2));
    
    
    List<WSC09_ALRVehicleTracking.VehicleTracking> lsVehicleTracking = new List<WSC09_ALRVehicleTracking.VehicleTracking>();
    lsVehicleTracking.add(veicTracking);
    lsVehicleTracking.add(veicTracking2);
    
    WSC09_ALRVehicleTracking.ReturnServiceTracking returnService= new WSC09_ALRVehicleTracking.ReturnServiceTracking();
    returnService = WSC09_ALRVehicleTracking.updateTracking(System.now(),lsVehicleTracking);
    
    
            
    }
 }