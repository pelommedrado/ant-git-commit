public with sharing class AP01Case 
{
    public static void updateVehicle(list<Case> listeCases)
    {
        list<VEH_Veh__c> vehiculesToUpdate = new list<VEH_Veh__c>();
        map<Id, Decimal> mapVehiculeKM = new map<Id, Decimal>();
        map<Id, Case> mapCase = new map<Id, Case>();
        
        for (Case c : listeCases)
        {
            mapCase.put(c.Id, c);
            
            // On récupère le plus gros kilométrage pour chaque véhicule
            if (mapVehiculeKM.get(c.VIN__c) != null)
            {
                if (c.Kilometer__c > mapVehiculeKM.get(c.VIN__c))
                    mapVehiculeKM.put(c.VIN__c, c.Kilometer__c);
            }
            else mapVehiculeKM.put(c.VIN__c, c.Kilometer__c);
        }
    
        for (VEH_Veh__c v : [Select Id, KmCheck__c, KmCheckDate__c from VEH_Veh__c where Id IN :mapVehiculeKM.keySet()])
        {
            if (mapVehiculeKM.get(v.Id) > v.KmCheck__c)
            {
                v.KmCheck__c = mapVehiculeKM.get(v.Id);
                v.KmCheckDate__c = Date.today();
                vehiculesToUpdate.add(v);
            }
        }
        
       
        try
        {
            if(vehiculesToUpdate.size()>0)
            {
            Database.update(vehiculesToUpdate);
            }
        }
        
         catch(NullPointerException nle)
        {
            for (Integer i = 0; i < nle.getNumDml(); i++)
                mapCase.get(nle.getDmlId(i)).addError(System.Label.UnableToSave);
        }
        catch(DmlException dmle)
        {
            for (Integer i = 0; i < dmle.getNumDml(); i++)
                mapCase.get(dmle.getDmlId(i)).addError(System.Label.UnableToSave);
        }
    }

}