/*****************************************************************************************
 Name    : Myr_Synchro_ATC_TriggerHandler_Test                 Creation date : 07 Aug 2015
 Desc    : Handler dedicated to CSDB synchronizations
 Author  : Donatien Veron (AtoS) 
 Project : myRenault
******************************************************************************************/
@isTest
public class Myr_Synchro_ATC_TriggerHandler_Test {
    @testsetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
        //Insert the required technical users for this test class
        List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
        listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('UK', null, false) );
        listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, false) );
        listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', 'Account.SynchroATC, VehicleRelation.SynchroATC, Vehicle.SynchroATC', false) );
        Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    }
  
    //No synchronization, because at least one of the criteria : {Myr_Status__c='Activated', rlinkEligibility__c='Y', My_Garage_Status__c = 'confirmed'} is false
    static testMethod void test010()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a; 
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Created', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, Status__c='Inactive', My_Garage_Status__c = 'confirmed');
            insert vr;
            
            v.rlinkEligibility__c='N';
            update v;
            a.Myr_Status__c='Activated';
            update a;
            vr.My_Garage_Status__c = 'unconfirmed';
            update vr;
            v.rlinkEligibility__c='Y';
            update v;
    
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
  
  
    //No synchronization, because the country of the account is not set as "ATC synchronisable"
    static testMethod void test020()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
        Country_Info__c myC = [SELECT Id FROM Country_Info__c where Name='Italy'];
        myC.Myr_ATC_Synchronisable__c=false;
        update myC;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Activated', Salutation_Digital__c='Mrs', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris', Country__c='Italy');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            
            a.Salutation_Digital__c='Mrs';
            update a;
            vr.My_Garage_Status__c = 'unconfirmed';
            update vr;
            vr.My_Garage_Status__c = 'confirmed';
            update vr;
            v.rlinkEligibility__c='N';
            update v;
            v.rlinkEligibility__c='Y';
            update v;
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
  
  
    //No synchronization, because the ATC synchronization is disabled on the whole org
    static testMethod void test030()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        CS04_MYR_Settings__c setting = [select Myr_ATC_Activated__c from CS04_MYR_Settings__c];
        setting.Myr_ATC_Activated__c=false;
        update setting;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Activated', Salutation_Digital__c='Mrs', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            
            a.Salutation_Digital__c='Mrs';
            update a;
            a.Myr_Status__c='Created';
            update a;
            a.Myr_Status__c='Activated';
            update a;
            vr.My_Garage_Status__c = 'unconfirmed';
            update vr;
            vr.My_Garage_Status__c = 'confirmed';
            update vr;
            v.rlinkEligibility__c='N';
            update v;
            v.rlinkEligibility__c='Y';
            update v;
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
  
  
    //No synchronization, because the org is set to avoid synchronization
    static testMethod void test040()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        CS04_MYR_Settings__c setting = [select Myr_ATC_Activated__c from CS04_MYR_Settings__c];
        setting.Myr_ATC_Activated__c=false;
        update setting;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Activated', Salutation_Digital__c='Mrs', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            
            a.Salutation_Digital__c='Mrs';
            update a;
            a.Myr_Status__c='Created';
            update a;
            a.Myr_Status__c='Activated';
            update a;
            vr.My_Garage_Status__c = 'unconfirmed';
            update vr;
            vr.My_Garage_Status__c = 'confirmed';
            update vr;
            v.rlinkEligibility__c='N';
            update v;
            v.rlinkEligibility__c='Y';
            update v;
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
  
    //No synchronization, because the user is set to avoid synchronization
    static testMethod void test050()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('France');
        
        CS04_MYR_Settings__c setting = [select Myr_ATC_Activated__c from CS04_MYR_Settings__c];
        setting.Myr_ATC_Activated__c=false;
        update setting;
        
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Activated', Salutation_Digital__c='Mrs', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            
            a.Salutation_Digital__c='Mrs';
            update a;
            a.Myr_Status__c='Created';
            update a;
            a.Myr_Status__c='Activated';
            update a;
            vr.My_Garage_Status__c = 'unconfirmed';
            update vr;
            vr.My_Garage_Status__c = 'confirmed';
            update vr;
            v.rlinkEligibility__c='N';
            update v;
            v.rlinkEligibility__c='Y';
            update v;
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
  
  
    //OK
    static testMethod void test060()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=false, Myr_Status__c='Activated', HomePhone__c='0629526341', PersEmailAddress__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            List<Account> la = [select Id from Account];
            system.assertEquals(1, la.size());
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            List<VEH_Veh__c> lv = [select Id from VEH_Veh__c];
            system.assertEquals(1, lv.size());
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            Test.startTest();
            insert vr;
            List<VRE_VehRel__c> lr = [select Id from VRE_VehRel__c];
            system.assertEquals(1, lr.size());
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        Synchro_ATC__c s = ls[0];
        system.assertEquals(1, ls.size());
        system.assertEquals(s.Account__c, a.Id);
        system.assertEquals(s.Account__c, vr.Account__c);
        system.assertEquals(s.VIN__c, v.Id);
        system.assertEquals(s.VIN__c, vr.vin__c);
        system.assertEquals(s.Relation_Vehicle__c, vr.Id);
        system.assertEquals('POST', s.Customer_Http_Method__c);
    }
  

    //Account activation OK
    static testMethod void test070()  
    {  
        User usr = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( usr ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),  Firstname='Donatien', LastName='Veron', CSDB_Account_Activated__c=false, Myr_Status__c='Created', Salutation_Digital__c='Mrs', PersMobPhone__c='0629753081', BillingStreet='my street', BillingPostalCode='35832', BillingCity='Rennes', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            List<Synchro_ATC__c> hs = [select Id from Synchro_ATC__c];
            system.assertEquals(0, hs.size());
            
    
            Test.startTest();
            a.Myr_Status__c='Activated';
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        Synchro_ATC__c s0 = ls[0];
        system.assertEquals('Activation_On_Account', ls[0].Action_On_Object__c);   
    }
 
    //Account activation, VIN KO
    static testMethod void test080()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_KO));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),  Firstname='Donatien', LastName='Veron', CSDB_Account_Activated__c=false, Myr_Status__c='Created', Salutation_Digital__c='Mrs', PersMobPhone__c='0629753081', BillingStreet='my street', BillingPostalCode='35832', BillingCity='Rennes', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            List<Synchro_ATC__c> hs = [select Id from Synchro_ATC__c];
            system.assertEquals(0, hs.size());
    
            Test.startTest();
            a.Myr_Status__c='Activated';
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        Synchro_ATC__c s0 = ls[0];
        system.assertEquals('Activation_On_Account', s0.Action_On_Object__c);
    }
   
    //Account update & activate
    static testMethod void test090()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),  Firstname='Donatien', LastName='Veron', CSDB_Account_Activated__c=false, Myr_Status__c='Created', Salutation_Digital__c='Mrs', PersMobPhone__c='0629753081', BillingStreet='my street', BillingPostalCode='35832', BillingCity='Rennes', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            List<Synchro_ATC__c> hs = [select Id from Synchro_ATC__c];
            system.assertEquals(0, hs.size());
    
            Test.startTest();
            a.Firstname='Don';
            a.Myr_Status__c='Activated';
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        Synchro_ATC__c s0 = ls[0];
        system.assertEquals('Activation_and_Update_On_Account', s0.Action_On_Object__c);
    }
  
 
    //Activation of an account
    static testMethod void test100()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        Account b;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),Firstname='Donatien', Lastname='Atos', CSDB_Account_Activated__c=true, Myr_Status__c='Created', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Test.startTest();
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            
            a.Myr_Status__c='Activated';
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c where Account__c=:myA and vin__c=:myV];
        Synchro_ATC__c s0 = ls[0];
        system.assertEquals('Activation_On_Account', s0.Action_On_Object__c);
    }
  
    //Account update
    static testMethod void test110()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),  Firstname='Donatien', LastName='Veron', CSDB_Account_Activated__c=false, Myr_Status__c='Created', Salutation_Digital__c='Mrs', PersMobPhone__c='0629753081', BillingStreet='my street', BillingPostalCode='35832', BillingCity='Rennes', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            List<Synchro_ATC__c> hs = [select Id from Synchro_ATC__c];
            system.assertEquals(0, hs.size());
    
            Test.startTest();
            a.Firstname='Don2'; 
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id;
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        system.debug(' ### test110 - ls=' + ls);
        //system.assertEquals(2, ls.size()); 
        //system.assertEquals('Update_On_Account', ls[0].Action_On_Object__c);
    }
  
    //Account delete (CSDB = true)
    static testMethod void test120()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        Account a;
        VEH_Veh__c v;
        VRE_VehRel__c vr;
        system.runAs( techUser ) {
            a  = new Account(RecordTypeId=Myr_Users_Utilities_Class.getPersonalAccountRecordType(),  Firstname='Donatien', LastName='Veron', CSDB_Account_Activated__c=true, Myr_Status__c='Created', Salutation_Digital__c='Mrs', PersMobPhone__c='0629753081', BillingStreet='my street', BillingPostalCode='35832', BillingCity='Rennes', HomePhone__c='0629526341', MyRenaultID__c='dd@ff.com', ShippingCity='Paris', country__c='UK');
            insert a;
            v  = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            vr = new VRE_VehRel__c(account__c=a.Id, vin__c=v.Id, My_Garage_Status__c = 'confirmed');
            insert vr;
            List<Synchro_ATC__c> hs = [select Id from Synchro_ATC__c];
            system.assertEquals(0, hs.size());
    
            Test.startTest();
            a.Myr_Status__c='Deleted';
            update a;
            Test.stopTest();
        }
        Id myA = a.Id;
        Id myV = v.Id; 
        Id myVR = vr.Id;
        List<Synchro_ATC__c> ls = [select Account__c, Customer_Http_Method__c, Customer_Question__c, Customer_Response__c, Customer_Response_Http_Status_Code__c, Logger__c, Action_On_Object__c, Relation_Vehicle__c, Status__c, VIN__c, VIN_Question__c, VIN_Response__c, VIN_Http_Method__c, VIN_Response_Http_Status_Code__c from Synchro_ATC__c];
        Synchro_ATC__c s0 = ls[0];
        system.assertEquals('Delete_On_Account', s0.Action_On_Object__c);
        Account mA = [select CSDB_Account_Activated__c from Account];
        //system.assertEquals(false, mA.CSDB_Account_Activated__c);
    }
 
  
    //Exception on insertSynchro
    static testMethod void test130()  
    {  
        User techUser = Myr_Datasets_Test.getTechnicalUser('UK');
        Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.MODE_CUST_OK_VIN_OK));
        system.runAs( techUser ) {
            Test.startTest(); 
            VEH_Veh__c v = new VEH_Veh__c(Name='VF1ACVYB047512414', rlinkEligibility__c='Y');
            insert v;
            Id myIdV=v.Id;  
            try{
                Id myId = Myr_Synchro_ATC.insertSynchro
                (myIdV, null, null, null, null, null, null, null, null, null, null, null, null, null,null);
            }catch(Exception e){
                system.assertEquals(true,e.getMessage().contains('System.DmlException'));               
            }
            Test.stopTest();
        }
        List<Synchro_ATC__c> ls = [select Id from Synchro_ATC__c];
        system.assertEquals(0, ls.size());
    }
}