public class VFC116_Util_Instance
{

	public static String getSalesforcePrefix()
	{
		String prefix = '';
		
		//
        // Possible Scenarios:
        //
        // (1) ion--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
        // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
        // (3) ion.my.salesforce.com    --- 4 parts, Instance is not determinable
        // Split up the hostname using the period as a delimiter
        List<String> parts = system.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
        if (parts.size() == 3)
        	prefix = parts[0];
        else if (parts.size() == 5)
        	prefix = parts[1];
        else
        	prefix = '';

		return prefix;
	}
	
	
}