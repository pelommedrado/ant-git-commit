@isTest
public class PV_AcaoComercialTest {

    public static testMethod void myTest(){
        
        Model__c modelo = new Model__c(Model_PK__c = 'test1');
        Insert modelo;
        
        Model__c modelo2 = new Model__c(Model_PK__c = 'test2');
        Insert modelo2;
        
        PVCommercial_Action__c pv = new PVCommercial_Action__c(
            Status__c = 'Active',
            End_Date__c = system.today() + 1,
            Start_Date__c = system.today(),
            Model__c = modelo.Id
        );
        Insert pv;
        
        PVCall_Offer__c ca = new PVCall_Offer__c(
            Commercial_Action__c = pv.Id
        );
        Insert ca;
        
        pv.Model__c = modelo2.Id;
        Update pv;

    }
    
    public static testMethod void myTest2(){
        
        PVCommercial_Action__c pv = new PVCommercial_Action__c(
            Status__c = 'Inactive',
            End_Date__c = system.today() + 1,
            Start_Date__c = system.today()
        );
        Insert pv;
        Update pv;
        
    }

}