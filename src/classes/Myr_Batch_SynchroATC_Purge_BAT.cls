/*  Purge of Synchro_ATC
*************************************************************************************************************
25 August 2014  : Creation
*************************************************************************************************************/
global class Myr_Batch_SynchroATC_Purge_BAT extends INDUS_Batch_AbstractBatch_CLS { 
    global Database.QueryLocator start(Database.BatchableContext info){ 
        String query = 'Select Id from Synchro_ATC__c where (status__c=\'OK\' and CreatedDate <= LAST_N_DAYS:'+ CS04_MYR_Settings__c.getInstance().Myr_ATC_Batch_OK__c +') or (status__c=\'KO\' and CreatedDate <= LAST_N_DAYS:'+ CS04_MYR_Settings__c.getInstance().Myr_ATC_Batch_KO__c +')';
        System.debug('#####' + query);
        return Database.getQueryLocator(query); 
    }
   
    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> scope){
        return new DatabaseAction(scope, Action.ACTION_DELETE, false);
    }   
      
    global override void doFinish(Database.BatchableContext info){ 
        System.debug( '#### Myr_Batch_SynchroATC_Purge_BAT - Finish '); 
    } 
    
    global override String getClassName(){
        return Myr_Batch_SynchroATC_Purge_BAT.class.GetName();
    }

}