public with sharing class VFC73_NewAccessoryController {
        
        private String quoteId;
        private String quoteLineItemId;
        public String itemDescription {get; set;}
        

        public VFC73_NewAccessoryController(ApexPages.StandardController controller) 
        {
                this.quoteId = controller.getId();      
        }
        
        public Pagereference initialize(){
                this.quoteLineItemId = ApexPages.currentPage().getParameters().get('quoteLineItemId');
                
                if(this.quoteLineItemId != null &&  this.quoteLineItemId != ''){
                        this.loadAccessory(quoteLineItemId);
                }
                
                return null;
        }

        /**
        * Trata a ação do botão salvar acessório.
        */
        public PageReference saveAccessory()
        {
                PageReference pageRef = null; 
                String strMessage = null;
                ApexPages.Message message = null;
                
                if(itemDescription == '' || itemDescription == null){
                	strMessage = 'O campo itens deve ser preenchido.';
                	message = new ApexPages.Message(ApexPages.Severity.WARNING, strMessage);
                    ApexPages.addMessage(message);
                    return pageRef; 
                }
                
                try 
                {
                        
                        if(this.quoteLineItemId != null &&  this.quoteLineItemId != ''){
                                VFC74_AccessoryBusinessDelegate.getInstance().updateAccessory(quoteLineItemId, itemDescription);
                        }else{
                                VFC74_AccessoryBusinessDelegate.getInstance().createNewAccessory(quoteId, itemDescription);
                        }
                        pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+quoteId);
                        
                pageRef.setRedirect(true);
                
                return pageRef;
                         
                }
                catch(Exception ex)
                {
                        
                        strMessage = 'Não foi possível salvar o Acessório. Motivo: \n' + ex.getMessage();
                        
                        message = new ApexPages.Message(ApexPages.Severity.WARNING, strMessage);
                        
                        ApexPages.addMessage(message);
                }
                
        return pageRef; 
        }
        
        /**
        * Trata a ação do botão cancelar inclusão.
        */
        public PageReference cancelRequest()
        {
       PageReference pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+quoteId);
       pageRef.setRedirect(true);
       return pageRef;          
        }
        
        /**
        * Trata a ação de carregar um registro de acessório para edição.
        */
        private void loadAccessory(String quoteLineItemId){
        
                String strMessage = null;
                ApexPages.Message message = null;
                
                QuoteLineItem quoteLineItem = VFC74_AccessoryBusinessDelegate.getInstance().getQuoteLineItemById(quoteLineItemId);
                
                if(quoteLineItem != null){                      
                        this.itemDescription = quoteLineItem.Description;
                }else{
                        strMessage = 'Aceesório não encontrado';
                        message = new ApexPages.Message(ApexPages.Severity.WARNING, strMessage);
                        ApexPages.addMessage(message);
                }
                
                
        }

}