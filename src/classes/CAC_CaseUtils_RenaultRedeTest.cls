@isTest
public class CAC_CaseUtils_RenaultRedeTest {
	
    public static testMethod void WaitingReply(){
        CAC_CaseUtils_RenaultRedeTest.createCustonSet();
        CAC_CaseUtils_RecordTypeIsRenaultRede caseUtil = new CAC_CaseUtils_RecordTypeIsRenaultRede();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste1@teste2.com',
                'Dealer'
            )
        ){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
            caseUtils.statusAnswersWaiting();
        }
        Database.insert(returnComentarioCaso(lsCaseId));
        
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Dealer'
            )
        ){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils3 = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
            caseUtils3.statusAnswersWaiting();
        }
        Test.stopTest();
    }
    
    
    
    
    public static testMethod void statusAnswers(){
        CAC_CaseUtils_RenaultRedeTest.createCustonSet();
        CAC_CaseUtils_RecordTypeIsRenaultRede caseUtil = new CAC_CaseUtils_RecordTypeIsRenaultRede();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Dealer'
            )
        ){
            for(Case casea :lsCase){
                casea.Description = 'abc';
                casea.Status = 'Answered';
                casea.Answer__c ='slajlksjlkaj';
                lsCaseId.add(casea.ID);
            }
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils3 = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
            caseUtils3.statusAnswers();
            
        }
        
        
    }
    
    
    public static testMethod void corretDayReturn(){
         List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        
        Long hours = 200;
        dateTime data;
         CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new  CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,new Map<id,Case>(lsCase));
        data = caseUtils.corretDayReturn(hours,08,17);
    }
    
    
    public static testMethod void AvailableRecordType(){
        CAC_CaseUtils_RenaultRedeTest.createCustonSet();
        CAC_CaseUtils_RecordTypeIsRenaultRede caseUtil = new CAC_CaseUtils_RecordTypeIsRenaultRede();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste3@teste1.com',
                'Dealer'
            )
        ){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
            caseUtils.getAvailableRecordType();
        }
        
    }
    
    public static testMethod void updateField(){
        
        User usr = new User(
            Id = UserInfo.getUserId(),
            roleInCac__c = 'Analyst',
            isCac__c = true
        );
        update usr;
        
        CAC_CaseUtils_RenaultRedeTest.createCustonSet();
        User user1 = CAC_CaseUtils_RenaultRedeTest.createUser('a1dcqadasdsas@test.com','Dealer');
        
        //Testa Status New
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_RenaultNetworking_Logistic'));
        for(Case caso: lsCase)
            caso.OwnerID = usr.Id;
        Update lsCase;
        
        Map<Id,Case> lsOldCase  = new Map<Id,Case>(lsCase);  
        CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
        caseUtils.updateField();
    }
    
    /*public static testMethod void updateField(){
        
        User usr = new User(
            Id = UserInfo.getUserId(),
        	roleInCac__c = 'Analyst',
            isCac__c = true
        );
        update usr;
        
        CAC_CaseUtils_RenaultRedeTest.createCustonSet();
        
        User user1 = CAC_CaseUtils_RenaultRedeTest.createUser('a1dcqadasdsas@test.com','Analyst');
        List<Case> lsCase;
  
            lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_RenaultNetworking_Trainning'));
  		
        for(Case caso : lsCase){
            caso.OwnerID = UserInfo.getUserId();
            caso.Status = 'New';
        }
          

        Map<Id,Case> lsOldCase  = new Map<Id,Case>(lsCase);
    
        CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,lsOldCase);
        caseUtils.updateField();

    }   */
     
    
    public static testMethod void alterOwnerFull(){
        
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapiopias@test.com','Dealer');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        Account conta = createAccount();
        for(Case caase : lsCase){
            caase.OwnerId = conta.QueueId__c;
            caase.Status = 'Under Analysis';
            lsNewCase.add(caase);
        }
        
        
        
        System.runAs(user){
            CAC_CaseUtils_RecordTypeIsRenaultRede.alterOwner(lsNewCase);
        }
    }
    
    
    
    public static testMethod void blockStatusError1(){
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapiopias@test.com',Utils.getProfileId('CAC - Full'));
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        System.runAs(user){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
        
        
    }
    
    public static testMethod void blockStatusError2(){
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapiopias@test.com','Dealer');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'New';
        
        System.runAs(user){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
        
        
    }
    
    public static testMethod void blockStatusError3(){
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapiopias@test.com','Dealer');
        List<Case> lsCase;
        
        
        lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        System.runAs(user){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
    }
    
    
    
    public static testMethod void blockStatusError4(){
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapioaasas@test.com','Analyst');
        List<Case> lsCase;
        
        
        lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Waiting for reply';
        
        System.runAs(user){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
    }
    
    
    
    
    
    public static testMethod void alterOwnerDealer(){
        List<Case> lsCase;
        User user = CAC_CaseUtils_RenaultRedeTest.createUser('a1asdapiopias@test.com','Analyst');
        System.runAs(user){
            lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_RenaultNetworking_Trainning'));
        }
        
    }
    
    
    public static List<CaseComment> returnComentarioCaso(List<Id> lsCaseId){
        List<CaseComment>lsCaseComment = new List<CaseComment>();
        for(Id caseId : lsCaseId){
            CaseComment comenta = new CaseComment(
                CommentBody = 'asdasdadadsadadsadasdasdasd',
                IsPublished = true,
                ParentId = caseId
            );
            lsCaseComment.add(comenta);
        }
        
        return lsCaseComment;
    }
    
    
    
    
    public static List<Case> returnNewCase(Id recordType){
        List<Case> lsCase = new List<Case>();
        Case cas = new Case(
            Status = 'New',
            Subject__c = 'Diferido (Previsão de Peças)',
            Description = 'teste',
            Dealer_Contact_Name__c = 'teste',
            Contact_Phone__c  = '1122334455',
            Reference__c = 'teste',
            Order_Number__c = '1234567',
            RecordTypeId = recordType
        );
        lsCase.add(cas);
        Insert lsCase; 
        return lsCase;
        
    }
    
    
    
    public static User createUser(String userName,String typeUserCac){
        Id profileId = Utils.getSystemAdminProfileId();
        String aliasName =  userName;
        if(aliasName.length()>=8)
            aliasName = aliasName.substring(0,7);
        User manager = new User(
            FirstName = aliasName,
            LastName = 'User',
            BypassVR__c = false,
            Email = 'test@org.com',
            Username = userName,
            Alias = userName.substring(0,7),
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = aliasName,
            ProfileId = profileId,
            BIR__c ='123ABC123',
            IsActive = true,
            isCac__c = true,
            roleInCac__c = typeUserCac
        );
        Database.insert( manager,true );
        return manager;
    }
    
    
    
    public static Account createAccount(){
        User manager = new User(
            
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        
        Group grupo = new Group(
        	DeveloperName = 'br_teste_dealer',
            Name = 'br_teste_dealer',
            Type = 'Queue'
        );
        Database.insert(grupo);
        
        
        Id dealerRecordType = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id;
        Account dealerAcc = new Account(
            RecordTypeId = dealerRecordType,
            Name = 'Concessionaria teste',
            IDBIR__c = '123ABC123',
            Country__c = 'Brazil',
            NameZone__c = 'R2',
            ShippingState = 'SP',
            ShippingCity = 'São Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true,
            QueueId__c = grupo.Id
        );
        Database.insert( dealerAcc );
        
        return dealerAcc;
    }
    
    
    
    public static void createCustonSet(){
        cacweb_Mapping__c custonSet = new cacweb_Mapping__c(
            name = 'Cacweb_PartsWarehouse',
            Department__c = 'Peças',
            Departamento__c = 'Peças',
            DirectionInEnglish__c = 'Parts',
            Name_Queue__c= 'Cacweb_PartsWarehouse',
            Record_Type_Name__c = 'Cacweb_PartsWarehouse'
        );
        Database.insert(custonSet);
        
        cacweb_settingTime__c custon2 = new cacweb_settingTime__c(
            name = 'Diferido',
            alertExpire__c = 20,
            subject__c = 'Diferido (Previsão de Peças)',
            FinishHourAttendance__c = 17,
            StartHourAttendance__c = 08,
            hoursExpire__c = 500
        );
        Database.insert(custon2);
    }
    
}