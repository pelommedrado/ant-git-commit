global class CAC_SendCaseButton {

    
    //atribui usuário á fila
    WebService static Id updateUser(String caseID) {
        if(String.isNotBlank(caseID) && String.isNotEmpty(caseID)){
            
            Case cases = [select id, RecordTypeId from Case where id =: caseID];
            
            List<RecordType> lsRecordType = Utils.getRecordType(cases.RecordTypeId);
            String nameQueue = '';  
            boolean encontradoValor = false;
            Id ownerIdCase;
            
            for(cacweb_Mapping__c cacMap : getCustonSet()){
                if(encontradoValor == false){
                    for(RecordType recordType : lsRecordType){
                        if(String.isNotEmpty(cacMap.Record_Type_Name__c) && 
                           String.isNotEmpty(recordType.DeveloperName) && 
                           cacMap.Record_Type_Name__c.equals(recordType.DeveloperName)){
                            nameQueue = cacMap.Name_Queue__c;
                            encontradoValor = true;
                            break;
                        }
                    }
                }else
                    break;
            }
            for( Group groupQueue : [select Id, DeveloperName from Group where Type = 'Queue']){
                if(String.isNotEmpty(nameQueue) && groupQueue.DeveloperName.equals(nameQueue)){
                    ownerIdCase = groupQueue.id;
                    break;
                }
            }
            return ownerIdCase;
        }
        return caseID;
    }
    webService static  String verifyComment(String caseID){
        
        Case caso  = [select OwnerId,Status, LastModifiedDate from Case where id =:caseID];
        
               
        if(UserInfo.getUserId() == caso.OwnerId){
	        List<CaseComment> lscasecom = [select id,CreatedById,CreatedDate from CaseComment where ParentId =: caseID order by CreatedDate asc];
            if(!lscasecom.isEmpty() && caso.LastModifiedDate < lscasecom.get(lscasecom.size()-1).CreatedDate && lscasecom.get(lscasecom.size()-1).CreatedById == UserInfo.getUserId()){
                return 'true';
            }
            return 'false';
        }
        return 'false2';
        
    } 
    
    webService static  String verifyCommentAnalyst(String caseID){
        
        Case caso  = [select OwnerId,Status, LastModifiedDate from Case where id =:caseID];
        if(UserInfo.getUserId() != caso.OwnerId){
	        List<CaseComment> lscasecom = [select id,CreatedById,CreatedDate,IsPublished from CaseComment where ParentId =: caseID order by CreatedDate asc];
            if(!lscasecom.isEmpty() && caso.LastModifiedDate < lscasecom.get(lscasecom.size()-1).CreatedDate && lscasecom.get(lscasecom.size()-1).IsPublished && lscasecom.get(lscasecom.size()-1).CreatedById == UserInfo.getUserId()){
                return 'true';
            }
            return 'false';
        }
        return 'false2';
        
    } 
    
    
    private static List<cacweb_Mapping__c> getCustonSet(){
       return  cacweb_Mapping__c.getAll().values();
    }
}