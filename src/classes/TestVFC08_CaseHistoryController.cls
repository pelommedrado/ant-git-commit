@isTest(SeeAllData=true)
public with sharing class TestVFC08_CaseHistoryController {

static testMethod void testCaseHistoryController() 
    {     
    
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',BypassVR__c=true,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
        System.runAs(usr)
        {
    
        Integer i;
        String s1;
        Integer i2,i3;
        Test.startTest();
        Case MyCase = new Case();
        insert MyCase;
        MyCase.status = 'status';
        update MyCase;
        ApexPages.currentPage().getParameters().put('id', MyCase.Id );
        VFC08_CaseHistoryController CHC = new VFC08_CaseHistoryController();
        CHC.sortByCreateBy();
        CHC.sortByDueDate();
        CHC.sortBySubject();
        CHC.sortByType();
        CHC.sortByOwner();
  

        String Idtest = '00000';
        Boolean Bool ;
        Bool = CHC.isId(Idtest);
        String EmailTest = 'test@email.com';
        //Bool = CHC.notEmail(EmailTest );
        //CHC.quicksort(EmailTest,0,0);
                
        VFC08_CaseHistoryController.CaseHistoryItem CHItem1 = new VFC08_CaseHistoryController.CaseHistoryItem(system.now(), 'toto', 'toto', 'toto', 'toto', 'toto', system.today(), 'toto', 'toto', 'toto');
        VFC08_CaseHistoryController.CaseHistoryItem CHItem2 = new VFC08_CaseHistoryController.CaseHistoryItem(system.now(), 'toto', 'toto', 'toto', 'toto', 'toto', system.today(), 'toto', 'toto', 'toto');
        Bool = CHC.compare('CREATEDATE', CHItem1, CHItem2);
        Bool = CHC.compare('DUEDATE', CHItem1, CHItem2);
        Bool = CHC.compare('OWNER', CHItem1, CHItem2);
        Bool = CHC.compare('CREATEBY', CHItem1, CHItem2);
        Bool = CHC.compare('TYPE', CHItem1, CHItem2);
        Bool = CHC.compare('SUBJECT', CHItem1, CHItem2);
       
        //VFC08_CaseHistoryController.CaseHistoryItem CHI = new VFC08_CaseHistoryController.CaseHistoryItem(date.parse('01/01/2011'), 'a','a','a','a','a', date.parse('02/02/2012'),'b','b','b');
       
       
        //i=CHC.partition(s1,i2,i3);
        Test.stopTest();
        System.debug('TESTS ENDED');
    }
    }
    
}