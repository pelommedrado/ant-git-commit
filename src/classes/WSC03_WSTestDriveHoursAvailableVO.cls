/**
*   WebService Class    -   WSC03_WSTestDriveHoursAvailableVO
*   Author              -   Suresh Babu
*   Date                -   28/11/2012
*   
*   #01 <Suresh Babu> <28/11/2012>
*       Hours Available Value Object class to return Available Hours to return  
**/
global class WSC03_WSTestDriveHoursAvailableVO {
	webservice String HourAvailable {get;set;}
	webservice String errorMessage {get;set;}
	
	global WSC03_WSTestDriveHoursAvailableVO(){}
}