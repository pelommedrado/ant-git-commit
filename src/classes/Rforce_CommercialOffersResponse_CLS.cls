/*****************************************************************************************
    Name            : Rforce_CommercialOffersResponse_CLS
    Description     : This apex class is a POJO class which will contains the reponse value for EMail and VIN.
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Imnplemented By : Ashok Muntha
 
******************************************************************************************/
global class Rforce_CommercialOffersResponse_CLS implements Comparable {

 public String strDesMkt{get;set;}
 public String strDeskEli{get;set;}
 public String strDeskLeg{get;set;}
 public String strPictGen{get;set;}
 public String strPictDetail{get;set;}
 public String strNewOffer{get;set;}
 
 public String strPropID{get;set;}
 public String strPropOfferID{get;set;}
 public String strPropOfferSpaceID{get;set;}
 public String strPropWeight{get;set;}
 public String strPropRank{get;set;}
 public String strPropCode{get;set;}
 public String strPropLabel{get;set;}
 public String strPropStartDate{get;set;}
 public String strPropEndDate{get;set;}
 public String strValidityDate{get;set;}
 public String strUsedOffer{get;set;}
 public String strBrandListID{get;set;}
 public Integer intPropWeight{get;set;}
 
 //public String strCategoryType{get;set;}
 //public String strContextWeight{get;set;} 
 
 public enum SORT_BY {ByOfferID,ByID }
 public static SORT_BY sortBy = SORT_BY.ByOfferID; 

 public Rforce_CommercialOffersResponse_CLS(String strDesMkt, String strDeskEli, String strDeskLeg, String strPictGen, String strPictDetail,
                                            String strNewOffer,String strPropID, String strPropOfferID, String strPropOfferSpaceID, String strPropWeight, 
                                            String strPropRank, String strPropCode, String strPropLabel, String strPropStartDate, String strPropEndDate, 
                                            String strValidityDate, String strUsedOffer,String strBrandListID,Integer intPropWeight){
 
     this.strDesMkt=strDesMkt;
     this.strDeskEli=strDeskEli;
     this.strDeskLeg=strDeskLeg;
     this.strPictGen=strPictGen;
     this.strPictDetail=strPictDetail;
     this.strNewOffer=strNewOffer;
     this.strPropID=strPropID;
     this.strPropOfferID=strPropOfferID;
     this.strPropOfferSpaceID=strPropOfferSpaceID;
     this.strPropWeight=strPropWeight;
     this.strPropRank=strPropRank;
     this.strPropCode=strPropCode;
     this.strPropLabel=strPropLabel;
     this.strPropStartDate=strPropStartDate;
     this.strPropEndDate=strPropEndDate;
     this.strValidityDate=strValidityDate;
     this.strUsedOffer=strUsedOffer;
     this.strBrandListID=strBrandListID;
     this.intPropWeight=intPropWeight;
 }
 
 global Integer compareTo(Object objToCompare) { 
     //return strPropWeight.compareTo(((Rforce_CommercialOffersResponse_CLS)objToCompare).strPropWeight); 
     return Integer.valueOf(((Rforce_CommercialOffersResponse_CLS)objToCompare).intPropWeight - intPropWeight);
 }
}