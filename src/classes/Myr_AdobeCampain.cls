/*****************************************************************************************
  Name    : Myr_AdobeCampain                                  Creation date : 10 June 2016
  Desc    : Project myRenault - Adobe Campain synchronizations
  Author  : Donatien Veron (AtoS)
 ****************************************************************************************/
public class Myr_AdobeCampain {
  private static CS04_MYR_Settings__c setting = CS04_MYR_Settings__c.getInstance();
  
  //Synchronize (asynchronous) 
  @future(callout = true)
  public static void synchronize(String My_Operation_Type, String My_Brand, Id My_AccountId) {
    sync(My_Operation_Type, My_Brand, My_AccountId);
  }

  //Synchronize (synchronous)
  public static void sync(String My_Operation_Type, String My_Brand, Id My_AccountId) {
    System.debug('Myr_AdobeCampain:sync:BEGIN.My_Id=<' + My_AccountId + '>, My_Operation_Type : <' + My_Operation_Type + '>, My_Brand=<' + My_Brand + '>');

    //Init
    Request_And_Response R_R = new Request_And_Response(My_Operation_Type, My_Brand, My_AccountId);
    
    //Grab the record's informations to My_Brand
    Boolean retBoolean=R_R.Grab_Informations();
    System.debug('Myr_AdobeCampain:sync.retBoolean=<' + retBoolean + '>');
    if (retBoolean){
      //Cast
      R_R.Cast();

      //Send synchronizations
      R_R.Send_Synchronization();

      //Logs
      R_R.Manage_Logs();
    }
  }

  //CMDM id
  public static String CMDM_Id_Without_Prefix(String CMDM_Id){    
    if (String.isEmpty(CMDM_Id)){
      return ''; 
    }
    return CMDM_Id.substring(4); 
  }

  //Get if the country is synchronizable
  public static Boolean Is_Org_AC_Synchronizable() {
    return setting.Myr_AC_Activated__c;
  }
    
  //Is synchronisable ?
  public static Boolean Is_AC_Synchronizable(String My_Country, String My_Brand, String My_Flow) { 
    Boolean My_ReturnCode;
    try{
      System.debug('Is_AC_Synchronizable.String.isEmpty(My_Country)=<' + String.isEmpty(My_Country) + '>');
      System.debug('Is_AC_Synchronizable.String.isEmpty(My_Brand)=<' + String.isEmpty(My_Brand) + '>');
      System.debug('Is_AC_Synchronizable.String.isEmpty(My_Flow)=<' + String.isEmpty(My_Flow) + '>');
      System.debug('Is_AC_Synchronizable.My_Country=<' + My_Country + '>');
      System.debug('Is_AC_Synchronizable.My_Brand=<' + My_Brand + '>');
      System.debug('Is_AC_Synchronizable.My_Flow=<' + My_Flow + '>');

      System.debug('Is_AC_Synchronizable.Country_Info__c.getInstance(My_Country)=<' + Country_Info__c.getInstance(My_Country) + '>');
      
      if(String.isEmpty(My_Country) 
        || String.isEmpty(My_Brand)
        || String.isEmpty(My_Flow)
        || My_Country==null 
        || My_Brand==null 
        || My_Flow==null
        || Country_Info__c.getInstance(My_Country)==null
        || (!My_Brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name()) && !My_Brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name()))
        || (!My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation) && !My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration) && !My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar))
      ){
        System.debug('Is_AC_Synchronizable.My_ReturnCode=<1>');
        My_ReturnCode=false;
      }else{
        System.debug('Is_AC_Synchronizable.My_ReturnCode=<2>');
        if(My_Brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name())){
          System.debug('Is_AC_Synchronizable.My_ReturnCode=<DACIA>');
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation)){
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Dacia_Activation__c;
          }
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration)){
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Dacia_Preregistration__c;
          }
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar)){
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Dacia_NewCar__c;
          }
        }
        
        if(My_Brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name())){
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation)){
            System.debug('Is_AC_Synchronizable.My_ReturnCode=<RENAULT>');
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Renault_Activation__c;
          }
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Preregistration)){
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Renault_Preregistration__c;
          }
          if(My_Flow.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar)){
            My_ReturnCode=Country_Info__c.getInstance(My_Country).Myr_AC_Renault_NewCar__c;
          }
        }
      }
    } catch(Exception e) {
      System.debug('Is_AC_Synchronizable.My_ReturnCode=<3>');
      My_ReturnCode=false;
      System.debug('Is_AC_Synchronizable.e.getMessage()=<' + e.getMessage() + '>');
    }
    System.debug('Is_AC_Synchronizable.My_ReturnCode=<' + My_ReturnCode + '>');
    return My_ReturnCode;
  }

  public class Vehicle {
    public String Id { get; set; }
    public String SFID { get; set; }
    public String VIN { get; set; }
    public String Registration_Number { get; set; }
    public Date   First_Registration_Date { get; set; }
    public String Brand { get; set; }
    public String Delivery_Date { get; set; }
    public String Model_Code_DB { get; set; }
    public String Model_Label_DB { get; set; }
    public String Model_Code { get; set; }
    public String Version_Code { get; set; }
    public String Version_Label { get; set; }
    public String Energy_Type { get; set; }
    public String Initiale_Paris_Flag { get; set; }
    public String Rlink_Eligibility { get; set; }
    public String Current_Mileage { get; set; }
    public String Annual_Mileage { get; set; }
  }

  public class Request_And_Response {
    public String user { get; set; }
    public String password { get; set; } 
    public String charset { get; set; }
    public String Account_SFDCID { get; set; }
    public String AC_Request_String { get; set; }
    public String AC_Question_EndPoint { get; set; }
    public String AC_Response_String { get; set; }
    public Integer AC_Response_Http_Status_Code { get; set; }
    public String AC_Response_Status_Code_Applicative { get; set; }
    public Account M_A { get; set; }
    public List<Vehicle> L_V { get; set; }
    public String Operation_Type { get; set; }
    public String My_Brand { get; set; }
    public Id Account_Id { get; set; }
    public Id RecordTypeId_PersonalAccount { get; set; }
	public Id RecordTypeId_LoggerApplication { get; set; }
    public Long BeginTime { get; set; }

    public Request_And_Response(String My_Operation_Type, String My_Brand, Id My_AccountId){
      this.Operation_Type = My_Operation_Type;
      this.My_Brand = My_Brand; 
      this.Account_Id = My_AccountId;
      this.RecordTypeId_PersonalAccount = Myr_Users_Utilities_Class.getPersonalAccountRecordType();
      this.BeginTime=Datetime.now().getTime();
      this.Connectivity();
	  this.RecordTypeId_LoggerApplication = [SELECT Id FROM RecordType WHERE SObjectType = 'Logger__c' AND developerName = 'Application'].Id;
    }

    public void Connectivity(){
      String End_Point = '';
      String My_LoginPassword='';

      this.charset = 'UTF-8';
      
      if ([select IsSandbox from Organization].IsSandbox){
        My_LoginPassword=setting.Myr_AC_Authorization_NoProd__c;
        End_Point+= setting.Myr_AC_EndPoint_NoProd__c;
      }else{
        My_LoginPassword=setting.Myr_AC_Authorization__c;
        End_Point+= setting.Myr_AC_EndPoint__c;
      }
      this.user=EncodingUtil.base64Encode(Blob.valueOf(My_LoginPassword.substring(0,My_LoginPassword.indexOf(':'))));
      this.password=EncodingUtil.base64Encode(Blob.valueOf(My_LoginPassword.substring(My_LoginPassword.indexOf(':')+1)));
      this.AC_Question_EndPoint=End_Point;
    }

    //Grab informations
    public Boolean Grab_Informations() {
      List<VRE_VehRel__c> listRel;
      String query='';
      Vehicle Veh;
      Id aId=this.Account_Id;
      Id RecordTypeId_PersonalAccount=this.RecordTypeId_PersonalAccount;

      System.debug('Myr_AdobeCampain.Grab_Informations.Operation_Type=<' + this.Operation_Type + '>, this.My_Brand=<' + this.My_Brand + '>, this.My_AccountId=<' + aId + '>');

      query  ='SELECT '; 
      query +='  Country__c, Id, Tech_ACCExternalID__c, AccountSource, AccSubSource__c ';
      query +=', Account_Sub_Sub_Source__c, MYR_Status__c, MYD_Status__c, MyRenaultID__c, MyDaciaID__c, Salutation_Digital__c  ';
      query +=', FirstName, SecondFirstName__c, LastName, BillingState, alerting__c, SecondSurname__c, CustomerIdentificationNbr__c ';
      query +=', PersMobPhone__c, BillingStreet, BillingPostalCode, BillingCity, BillingCountry, MYR_Dealer_1_BIR__c ';
      query +=', MYD_Dealer_1_BIR__c, ComAgreemt__c, PersEmail__pc, StopComRCFrom__c, StopComRCTo__c, StopComFlag__c ';
      query +=', StopComSMDFlag__c ';
      query +='FROM account ';
      query +='WHERE Id=\'' + aId + '\' ';   
      query +='AND RecordTypeId=\'' + RecordTypeId_PersonalAccount + '\' ';
      try {
        this.M_A = Database.query(query);
        System.debug('Myr_AdobeCampain.Grab_Informations(Account):Account initialized:'+this.M_A);
      } catch(Exception e) { 
        System.debug('Myr_AdobeCampain.Grab_Informations(Account):Exception=<' + e.getMessage() + '>'); 
        return false;
      }
      System.debug('Myr_AdobeCampain.Grab_Informations(Account):' + query); 

      if (this.Operation_Type.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation) || this.Operation_Type.equalsIgnoreCase(system.Label.Myr_Synchro_Adobe_Campain_Flow_NewCar)) {
        query  ='SELECT '; 
        query +='  Id, vin__r.Id, vin__r.Name, vin__r.VehicleRegistrNbr__c, vin__r.FirstRegistrDate__c ';
        query +=', vin__r.VehicleBrand__c, vin__r.DeliveryDate__c, vin__r.Model_Code2__c  ';
        query +=', vin__r.Commercial_Model__c, vin__r.ModelCode__c, vin__r.VersionCode__c, vin__r.Version__c ';
        query +=', vin__r.EnergyType__c, vin__r.Initiale_Paris__c ';
        query +=', vin__r.rlinkEligibility__c, vin__r.KmCheck__c, vin__r.KmPerYear__c, account__c ';
        query +='FROM VRE_VehRel__c ';
        query +='WHERE account__c=\'' + aId + '\' '; 
        query +='AND account__r.RecordTypeId=\'' + RecordTypeId_PersonalAccount + '\' ';
      
        System.debug('Myr_AdobeCampain.Grab_Informations:(Vehicle) ' + query); 
        try {
          listRel = Database.query(query);
          this.L_V =new List<Vehicle>();
          for (VRE_VehRel__c Rel : listRel){
            Veh = new Vehicle();
            Veh.Id=Rel.vin__r.Id;
            Veh.VIN=Rel.vin__r.Name;
            Veh.Registration_Number=Rel.vin__r.VehicleRegistrNbr__c;
            Veh.First_Registration_Date=Rel.vin__r.FirstRegistrDate__c;
            Veh.Brand=String.valueOf(Rel.vin__r.VehicleBrand__c);
            Veh.Delivery_Date=String.valueOf(Rel.vin__r.DeliveryDate__c);
            Veh.Model_Code_DB=Rel.vin__r.Model_Code2__c; 
            Veh.Model_Label_DB=Rel.vin__r.Commercial_Model__c;
            Veh.Model_Code=Rel.vin__r.ModelCode__c;
            Veh.Version_Code=Rel.vin__r.VersionCode__c;
            Veh.Version_Label=Rel.vin__r.Version__c;
            Veh.Energy_Type=Rel.vin__r.EnergyType__c;
            Veh.Initiale_Paris_Flag=String.valueOf(Rel.vin__r.Initiale_Paris__c);
            Veh.Rlink_Eligibility=String.valueOf(Rel.vin__r.rlinkEligibility__c);
            Veh.Current_Mileage=String.valueOf(Rel.vin__r.KmCheck__c);
            Veh.Annual_Mileage=String.valueOf(Rel.vin__r.KmPerYear__c);
            Veh.SFID=String.valueOf(Rel.Id);
            this.L_V.add(Veh);
          }
        } catch(Exception e) { 
          System.debug('Myr_AdobeCampain.Grab_Informations (Account) :  Exception=<' + e.getMessage() + '>'); 
          return false;
        }    
      }
      return true;
    }

    public void Cast() {
      String Str = '';
      String Email_Communication_Agreement;
      String Stop_Com_SMD_Flag;
      String Stop_Com_Global_Flag;
      String Initiale_Paris_Flag;
      String Rlink_Eligibility;

      System.debug('Myr_AdobeCampain.Cast:=this.M_A.PersEmail__pc<' + this.M_A.PersEmail__pc + '>, this.M_A.StopComSMDFlag__c=' + this.M_A.StopComSMDFlag__c + ', this.M_A.StopComFlag__c=<' + this.M_A.StopComFlag__c + '>'); 
      System.debug('Myr_AdobeCampain.Cast:=this.M_A.MyRenaultID__c<' + this.M_A.MyRenaultID__c + '>');
      
      if (this.M_A.PersEmail__pc!=null && this.M_A.PersEmail__pc.equalsIgnoreCase('Yes'))  {
        Email_Communication_Agreement='1';
      }else{
        Email_Communication_Agreement='0';
      }

      if (this.M_A.StopComSMDFlag__c!=null && this.M_A.StopComSMDFlag__c.equalsIgnoreCase('Yes'))  {
        Stop_Com_SMD_Flag='1';
      }else{
        Stop_Com_SMD_Flag='0'; 
      }

      if (this.M_A.StopComFlag__c!=null && this.M_A.StopComFlag__c.equalsIgnoreCase('Yes'))  {
        Stop_Com_Global_Flag='1';
      }else{
        Stop_Com_Global_Flag='0';
      }
            
      Str += '{';
      Str += '"request":{';
      Str += '"Connexion":{';
      Str += ' "user":"' + this.user + '",';
      Str += ' "password":"' + this.password + '",';
      Str += ' "charset":"' + this.charset + '"'; 
      Str += '},';
      Str += ' "Synchro_Flow":{'; 
      Str += '  "customers":{';
      Str += '    "Brand":"' + Translate_Brand(this.My_Brand) + '",';
      Str += '    "Country":"' + Nvl(this.M_A.Country__c) + '",';
      Str += '    "Account_SFDCID":"' + this.Account_Id + '",';
      Str += '    "Account_CMDMID":"' + Nvl(CMDM_Id_Without_Prefix(this.M_A.Tech_ACCExternalID__c)) + '",';
      Str += '    "Account_Source":"' + Nvl(this.M_A.AccountSource) + '",'; 
      Str += '    "Account_Sub_Source":"' + Nvl(this.M_A.AccSubSource__c) + '",';
      Str += '    "Account_Sub_Sub_Source":"' + Nvl(this.M_A.Account_Sub_Sub_Source__c) + '",';
	  //SDP: patch in the case of Activation flow: send Activated as is because the field Myr_Status and Myd_Status
	  //won't have the proper value at this time ...
      //Str += '    "MyR_Account_Status":"' + Nvl(this.M_A.MYR_Status__c) + '",';
      //Str += '    "MyD_Account_Status":"' + Nvl(this.M_A.MYD_Status__c) + '",';
	  if( system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation.equalsIgnoreCase( Operation_Type ) ) {
		if( Myr_MyRenaultTools.checkRenaultBrand( My_Brand ) ) {
			Str += '    "MyR_Account_Status":"' + Nvl(system.Label.Myr_Status_Activated) + '",';
			Str += '    "MyD_Account_Status":"' + Nvl(this.M_A.MYD_Status__c) + '",';
		} else { //Dacia brand
			Str += '    "MyR_Account_Status":"' + Nvl(this.M_A.MYR_Status__c) + '",';
			Str += '    "MyD_Account_Status":"' + Nvl(system.Label.Myr_Status_Activated) + '",';
		}
	  } else {
		Str += '    "MyR_Account_Status":"' + Nvl(this.M_A.MYR_Status__c) + '",';
		Str += '    "MyD_Account_Status":"' + Nvl(this.M_A.MYD_Status__c) + '",';
	  }
	  //SDP / END OF PATCH
      Str += '    "My_Renault_Username":"' + Nvl(this.M_A.MyRenaultID__c) + '",';
      Str += '    "My_Dacia_Username":"' + Nvl(this.M_A.MyDaciaID__c) + '",';
      Str += '    "Title":"' + Nvl(this.M_A.Salutation_Digital__c) + '",';
      Str += '    "First_Name":"' + Nvl(this.M_A.FirstName) + '",';
      Str += '    "Second_First_Name":"' + Nvl(this.M_A.SecondFirstName__c) + '",';
      Str += '    "Last_Name":"' + Nvl(this.M_A.LastName) + '",';
      Str += '    "Second_Last_Name":"' + Nvl(this.M_A.SecondSurname__c) + '",';
      Str += '    "Customer_Identification_Number":"' + Nvl(this.M_A.CustomerIdentificationNbr__c) + '",';
      Str += '    "Mobile_Phone":"' + Nvl(this.M_A.PersMobPhone__c) + '",';
      Str += '    "Address_Street":"' + Nvl(this.M_A.BillingStreet) + '",';
      Str += '    "Address_Zip_Code":"' + Nvl(this.M_A.BillingPostalCode) + '",';
      Str += '    "Address_City":"' + Nvl(this.M_A.BillingCity) + '",';
      Str += '    "Address_Country":"' + Nvl(this.M_A.BillingCountry) + '",';
      Str += '    "Prefered_Dealer_Code_R":"' + Nvl(this.M_A.MYR_Dealer_1_BIR__c) + '",';
      Str += '    "Prefered_Dealer_Code_D":"' + Nvl(this.M_A.MYD_Dealer_1_BIR__c) + '",';
      Str += '    "Global_Communication_Agreement":"' + Nvl(this.M_A.ComAgreemt__c) + '",';
      Str += '    "Email_Communication_Agreement":"' + Email_Communication_Agreement + '",';
      Str += '    "Stop_Com_RC_From":"' + Nvl(String.valueOf(this.M_A.StopComRCFrom__c)) + '",';
      Str += '    "Stop_Com_RC_To":"' + Nvl(String.valueOf(this.M_A.StopComRCTo__c)) + '",';
      Str += '    "Stop_Com_Global_Flag":"' + Stop_Com_Global_Flag + '",';
      Str += '    "Stop_Com_SMD_Flag":"' + Stop_Com_SMD_Flag + '",';
	  Str += '    "Relation":[{';

		System.debug('this.L_V=<' + this.L_V+'>');
		if (this.L_V!=null && this.L_V.size()>0) {
			for (Vehicle v : this.L_V) {
				if(v.Initiale_Paris_Flag!=null && v.Initiale_Paris_Flag.equalsIgnoreCase('true')){
					Initiale_Paris_Flag='1';
				} else {
					Initiale_Paris_Flag='0';
				}

				if (v.Rlink_Eligibility!=null && v.Rlink_Eligibility.equalsIgnoreCase('Y'))  {
					Rlink_Eligibility='1';
				} else {
					Rlink_Eligibility='0';
				}
            
				Str += '       "SFID":"' + Nvl(v.SFID)+ '",';
				Str += '       "Vehicle":{';
				Str += '         "SFDCID":"' + Nvl(v.Id) + '",';
				Str += '         "VIN":"' + Nvl(v.VIN) + '",';
				Str += '         "Registration_Number":"' + Nvl(v.Registration_Number)  + '",';
				Str += '         "First_Registration_Date":"' + Nvl(String.valueOf(v.First_Registration_Date)) + '",';
				Str += '         "Brand":"' + Nvl(v.Brand) + '",';
				Str += '         "Delivery_Date":"' + Nvl(v.Delivery_Date) + '",';
				Str += '         "Model_Code_DB":"' + Nvl(v.Model_Code_DB) + '",';
				Str += '         "Model_Label_DB":"' + Nvl(v.Model_Label_DB) + '",';
				Str += '         "Model_Code":"' + Nvl(v.Model_Code) + '",';
				Str += '         "Version_Code":"' + Nvl(v.Version_Code) + '",';
				Str += '         "Version_Label":"' + Nvl(v.Version_Label) + '",';
				Str += '         "Energy_Type":"' + Nvl(v.Energy_Type) + '",';
				Str += '         "Initiale_Paris_Flag":"' + Initiale_Paris_Flag + '",';
				Str += '         "Rlink_Eligibility":"' + Rlink_Eligibility  + '",';
				Str += '         "Current_Mileage":"' + Nvl(String.valueOf(v.Current_Mileage))  + '",';
				Str += '         "Annual_Mileage":"' + Nvl(String.valueOf(v.Annual_Mileage))  + '"';
				Str += '       }';
			}
		} else {
          Str += '       "SFID":"",';
          Str += '       "Vehicle":{';
          Str += '         "SFDCID":" ",';
          Str += '         "VIN":" ",';
          Str += '         "Registration_Number":"",';
          Str += '         "First_Registration_Date":"",';
          Str += '         "Brand":"",';
          Str += '         "Delivery_Date":"",';
          Str += '         "Model_Code_DB":"",';
          Str += '         "Model_Label_DB":"",';
          Str += '         "Model_Code":"",';
          Str += '         "Version_Code":"",';
          Str += '         "Version_Label":"",';
          Str += '         "Energy_Type":"",';
          Str += '         "Initiale_Paris_Flag":"",';
          Str += '         "Rlink_Eligibility":"",';
          Str += '         "Current_Mileage":"",';
          Str += '         "Annual_Mileage":""';
          Str += '       }';
      }
       
      Str += '   }]';
      Str += ' },';
      Str += ' "operationType":"' + this.Operation_Type + '"';
      Str += ' }';
      Str += '},';
      Str += '"response":{';
      Str += '  "Description":"",';
      Str += '  "Code":""';
      Str += '}';
      Str += '}';
      this.AC_Request_String=Str;
}

    public String Nvl(String value) {
      String str;
      if (value!=null && !value.equals('null')){
        return value;
      }else{
        return '';
      }
    }

    public String Translate_Brand(String The_Brand) {
      if (The_Brand!=null && The_Brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name())){
        return 'DAC';
      }else{
        return 'REN';
      }
    }

    public String NvlBool(String value) {
      String str;
      if (value!=null && !value.equalsIgnoreCase('null')){
        if (value.equals('true')){
          str='1';
        }else{
          str='0';
        }
      }else{
        str='0';
      }
      return str;
    }

    //Send notifications
    public void Send_Synchronization() {
      Http http;
      HttpRequest req;
      HttpResponse res;
      Dom.Document doc;
      String Response_Body_Truncated;
      String str;

      System.debug('### Send_Synchronization : this.AC_Request_String=<' + this.AC_Request_String + '>');

      req = new HttpRequest();
      req.setMethod('POST'); 
      req.setEndPoint(this.AC_Question_EndPoint);
      req.setHeader('Content-Type', 'application/xml; charset=utf-8');
      req.setBody(this.AC_Request_String);
      req.setTimeout(Integer.valueOf(setting.Myr_AC_TimeOut__c));

      try {
        http = new Http();
        if (!Test.isRunningTest()){ 
          res = http.send(req);
        }else{
          str  = '{ "request":{ "Connexion":{ "user":"", "password":"", "charset":"UTF-8" }, "Synchro_Flow":{ "customers":{';
          str += '"Brand":"", "Country":"", "Account_SFDCID":"", "Account_CMDMID":"", "Account_Source":"", "Account_Sub_Source":"",';
          str += '"Account_Sub_Sub_Source":"",';
          str += '"MyR_Account_Status":"",';
          str += '"MyD_Account_Status":"",';
          str += '"My_Renault_Username":"",'; 
          str += '"My_Dacia_Username":"",';
          str += '"Title":"",';
          str += '"First_Name":"",';
          str += '"Second_First_Name":"",';
          str += '"Last_Name":"",';
          str += '"Second_Last_Name":"",';
          str += '"Customer_Identification_Number":"",';
          str += '"Mobile_Phone":"",';
          str += '"Address_Street":"",';
          str += '"Address_Zip_Code":"",';
          str += '"Address_City":"",';
          str += '"Address_Country":"",';
          str += '"Prefered_Dealer_Code_R":"",';
          str += '"Prefered_Dealer_Code_D":"",';
          str += '"Global_Communication_Agreement":"",';
          str += '"Email_Communication_Agreement":"",';
          str += '"Stop_Com_RC_From":"",';
          str += '"Stop_Com_RC_To":"",';
          str += '"Stop_Com_Global_Flag":"",';
          str += '"Stop_Com_SMD_Flag":"",';
          str += '"Relation":[{';
          str += '"SFID":"",';
          str += '"Vehicle":{';
          str += '"SFDCID":"",';
          str += '"VIN":"",';
          str += '"Registration_Number":"",';
          str += '"First_Registration_Date":"",';
          str += '"Brand":"",';
          str += '"Delivery_Date":"",';
          str += '"Model_Code_DB":"",';
          str += '"Model_Label_DB":"",';
          str += '"Model_Code":"",';
          str += '"Version_Code":"",';
          str += '"Version_Label":"",';
          str += '"Energy_Type":"",';
          str += '"Initiale_Paris_Flag":"",';
          str += '"Rlink_Eligibility":"",';
          str += '"Current_Mileage":"",';
          str += '"Annual_Mileage":""';
          str += '}';
          str += '}]';
          str += '},';
          str += '"operationType":""';
          str += '}';
          str += '},';
          str += '"response":{';
          str += '"Description":"Success",';
          str += '"Code":"' + this.M_A.BillingCity + '"';
          str += '}';
          str += '}';
          str += '}';
        
          res = new HttpResponse();
          res.setBody(str);
        }
        this.AC_Response_Http_Status_Code = res.getStatusCode();
        Response_Body_Truncated=res.getBody();
        System.debug('Myr_AdobeCampain.Send_Synchronization:Response_Body_Truncated:' + Response_Body_Truncated);
        this.AC_Response_String = res.getBody();
        this.AC_Response_Http_Status_Code = res.getStatusCode();
      } catch(Exception e) {
        this.AC_Response_String = e.getMessage();
        this.AC_Response_Http_Status_Code = - 1;
        System.debug('Myr_AdobeCampain.Send_Synchronization:ExceptionLabel=<' + this.AC_Response_String + '>');
      }
    }

    //Logs
    public void Manage_Logs() {
      Logger__c M_L;
      System.debug('Myr_AdobeCampain.Manage_Logs:this.Account_Id=<' + this.Account_Id + '>');
      
      if (this.Account_Id!=null){
		System.debug('Myr_AdobeCampain.Manage_Logs:this=<' + this + '>');
		M_L = new Logger__c();
		M_L.RecordTypeId=this.RecordTypeId_LoggerApplication;
        M_L.Message__c=
            'EndPoint=<' + this.AC_Question_EndPoint + '>'
          + ', Question=<' + this.AC_Request_String + '>'
          + ', Response=<' + this.AC_Response_String + '>'
          + ', Response Http code=<' + this.AC_Response_Http_Status_Code + '>';
        M_L.Apex_Class__c='Myr_AdobeCampain';
        M_L.Function__c='Synchro_AdobeCampain:' + this.Operation_Type;
        M_L.Project_Name__c=String.valueOf(INDUS_Logger_CLS.ProjectName.MYR);
        M_L.Error_Level__c=String.valueOf(INDUS_Logger_CLS.ErrorLevel.Info.name());
        M_L.account__c=this.Account_Id;
        M_L.Completed_Date__c = System.today();
        M_L.Duration__c=Datetime.now().getTime()-this.BeginTime; 
        insert M_L;
      }else{ 
        System.debug('Myr_AdobeCampain.Manage_Logs:this.Account_Id is empty');
      }
    }
  }
}