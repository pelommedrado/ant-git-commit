@isTest
public with sharing class VFC152_PV_LEADTest {
  
    static testMethod void deveGeraQuarentena() {
        Lead lead = criarLead();
        
        VFC152_PV_LEAD vfc = new VFC152_PV_LEAD();
        vfc.geraQuarentena(lead);
        
        Quarantine__c quar = [SELECT Id FROM Quarantine__c LIMIT 1];
        
        System.assert(quar != null);
    }
    
    private static Lead criarLead() {
        Lead lea = new Lead(
            RecordTypeId = '012D0000000KCzVIAW',
            CurrencyIsoCode = 'BRL',
            Status = 'Identified',
            CPF_CNPJ__c = '11111111111',
            State = 'Sao Paulo',
            City = 'São Paulo',
            Email = 'test@test.com',
            FirstName = 'test',
            LastName = 'test',
            Phone = '123456789',
            LeadSource ='INTERNET',
            SubSource__c = 'RCI',
            VehicleOfInterest__c = 'Clio',
            SecondVehicleOfInterest__c = 'Sandero',
            PV_OfferNotFound__c = true
        );
        
        Database.insert(lea);
        return lea;
    }
    
  /**static testMethod void myUnitTest(){
      Lead lea = new Lead(
        RecordTypeId = '012D0000000KCzVIAW',
        CurrencyIsoCode = 'BRL',
        Status = 'Identified',
        CPF_CNPJ__c = '11111111111',
        State = 'Sao Paulo',
        City = 'São Paulo',
        Email = 'test@test.com',
        FirstName = 'test',
        LastName = 'test',
        Phone = '123456789',
        LeadSource ='INTERNET',
        SubSource__c = 'RCI',
        VehicleOfInterest__c = 'Clio',
        SecondVehicleOfInterest__c = 'Sandero',
        PV_OfferNotFound__c = true
      );
      
      Database.insert(lea); 
  }
**/
}