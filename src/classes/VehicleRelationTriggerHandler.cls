// ---------------------------------------------------------------------------
// Project: RForce
// Name: RForce_CasUpdateModifiedDate
// Desc: Vehicle Relation Trigger Handler
// CreatedBy: Sumanth(RNTBCI)
// CreatedDate: 05/12/2014
// ---------------------------------------------------------------------------

public without sharing class VehicleRelationTriggerHandler {
 
    public static void onAfterInsert(list <VRE_VehRel__c> listVRE, boolean isInsert, Map <Id, VRE_VehRel__c> oldMap){
        system.debug('### afterInsert in VRE handler'+listVRE);
        Rforce_UpdateCustStatus_VehicleRelation.updatecustomerstatus(listVRE);       
    }
    
    //public static void onBeforeInsert(list <VRE_VehRel__c> listVRE, boolean isInsert, Map <Id, VRE_VehRel__c> oldMap){

    
    //}
    public static void onAfterUpdate(list <VRE_VehRel__c> listVRE, boolean isUpdate, Map <Id, VRE_VehRel__c> oldMap){
        system.debug('### afterUpdate in VRE handler'+listVRE);
        List<VRE_VehRel__c> vrelist=new List<VRE_VehRel__c>();
        for(VRE_VehRel__c vr:listVRE){
            VRE_VehRel__c oldvr= oldMap.get(vr.Id); 
            if(oldvr.Status__c!=vr.Status__c){
                vrelist.add(vr);
            }    
        }
        if(vrelist.size()>0){
            Rforce_UpdateCustStatus_VehicleRelation.updatecustomerstatus(vrelist);
        }    
    }    
    
    //public static void onBeforeUpdate(list <VRE_VehRel__c> listVRE, boolean isUpdate, Map <Id, VRE_VehRel__c> oldMap){  
   
    //}
    public static void onBeforeDelete(Boolean isdelete,list <VRE_VehRel__c> listVRE){
        Rforce_UpdateCustStatus_VehicleRelation.updatecustomerstatus_ondelete(listVRE);       
    }
}