@isTest
private class Utils_ALR_ActivateInsuranceTest {

    static testMethod void myUnitTest() {
       
       Utils_ALR_ActivateInsurance activateInsurance = Utils_ALR_ActivateInsurance.getInstance(); 
       Account dealerAcc = new Account(
              Name = 'Concessionaria teste',
              IDBIR__c = '123ABC123',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc );
        System.debug('######## conta inserido '+dealerAcc);
        
        Contact contato = new Contact(
            AccountId = dealerAcc.Id,
            FirstName = 'Teste',
            LastName = 'Teste22',
            Email= 'teste@teste.com.br',
            Phone='1135587458',
            PersMobPhone__c='1199587458',
            ProPhone__c='1136547859'
            );
        insert contato;
        Utils_ALR_ActivateInsurance.getContact(contato.Id);
        
        Account dealerAcc2 = new Account(
              Name = 'Concessionaria teste2',
              IDBIR__c = '123ABC124',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc2 );
        System.debug('######## conta inserido '+dealerAcc2);
        
        Account dealerAcc3 = new Account(
              Name = 'Concessionaria teste3',
              IDBIR__c = '123ABC125',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc3 );
        System.debug('######## conta inserido '+dealerAcc3);
        
        Account css1 = dealerAcc;
        Account css2 = dealerAcc2;
        Account css3 = dealerAcc3;
      
        VEH_Veh__c vehicle = new VEH_Veh__c(
          ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            IDBIRSellingDealer__c = css1.IDBIR__c,
            Id_Bir_BuyDealer__c = css2.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'fffffffffffffffff',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'fffffffffffffffff',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        
        
        VEH_Veh__c vehicle2 = new VEH_Veh__c(
          ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            IDBIRSellingDealer__c = css1.IDBIR__c,
            Id_Bir_BuyDealer__c = css1.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'ffffffffffffffffa',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'ffffffffffffffffa',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        
        List<VEH_Veh__c>  lstVeicOld = new List<VEH_Veh__c>();
       lstVeicOld.add(vehicle);
       lstVeicOld.add(vehicle2);
       Database.insert(lstVeicOld);
       
       List<ALR_Tracking__c> listTracking = [select id,name from ALR_Tracking__c where ALR_Chassi__c in:(lstVeicOld)] ;
       
       List<ALR_Activate_Insurance__c> listActivateInsurance = new List<ALR_Activate_Insurance__c>();//lista para armazenar o acionamento do seguro
       
       for(ALR_Tracking__c track : listTracking){
           if(track.Name.equalsIgnoreCase(vehicle2.Name)){
               
               ALR_Activate_Insurance__c at = new ALR_Activate_Insurance__c(
                 ALR_Amount_Damage__c = 2,
                 ALR_Approved_Value_Parts__c = 790.00,
                 ALR_Approved_Value_Salvado__c = 300.00,
                 ALR_Approved_Value_Service__c = 200.00,
                 ALR_Arrival_Dealership__c = date.today(),
                 ALR_Carrier__c = 'BRAZUL TRANSPORTE DE VEICULOS',
                 ALR_Chassi__c = track.id,
                 ALR_Check_Caveat__c = true,
                 ALR_Check_CTe__c = true,
                 ALR_Check_Invoice_Carrier__c = true,
                 ALR_Check_Invoice_Combined__c = true,
                 ALR_Check_Invoice_Parts__c = true,
                 ALR_Check_Invoice_Salvado__c = true,
                 ALR_Check_Invoice_Service__c = true,
                 ALR_Check_Photo_Chassi__c = true,
                 ALR_City_Occurrence__c = 'São Paulo',
                 ALR_Claim_Number__c = '789109',
                 ALR_Comercial_Phone__c = '22223333',
                 ALR_Contact_Email__c = 'teste@test.com',
                 ALR_Contact_Name__c = 'test',
                 ALR_CTe__c = '123123',
                 ALR_Date_Docs_Received__c = date.today(),
                 ALR_Date_Docs_Refused__c = date.today().addDays(-3),
                 ALR_Date_Error_Insurance__c = date.today().addDays(-3),
                 ALR_Date_Estimate_Received__c = date.today().addDays(-1),
                 ALR_Date_Estimate_Requested__c = date.today(),
                 ALR_Date_Finalized_Process__c = date.today(),
                 ALR_Date_Inspection_Received__c = date.today(),
                 ALR_Date_Inspection_Requested__c = date.today().addDays(-1),
                 ALR_Date_Insurance_Ended__c = date.today().addDays(-1), 
                 ALR_Date_No_Send_Insurer__c = date.today(),
                 ALR_Date_Occurrence__c = date.today().addDays(-15),
                 ALR_Date_Open_Insurance__c = date.today().addDays(-2),
                 ALR_Date_Pending_Processing__c  = date.today().addDays(-2),
                 ALR_Date_Process__c = date.today().AddMonths(-1),
                 ALR_Date_Received_Insurer__c = date.today(),
                 ALR_Date_Repair_Released__c = date.today().addDays(-3),
                 ALR_Date_Reparation_Requested__c = date.today().addDays(-2),
                 ALR_Date_Total_Loss_Defined__c = date.today().addDays(-2),
                 ALR_DDD_Comercial_Phone__c = '11',
                 ALR_DDD_Mobile__c = '11',
                 ALR_DDD__c = '11',
                 ALR_Description_Pending__c = 'teste teste teste teste',
                 ALR_Driver_Name__c = 'teste teste',
                 ALR_Error_Description__c = 'testando os testes com a classe de teste',
                 ALR_Estimated_Loss_Value__c = 12.4,
                 ALR_Event_Phase_Process__c = '108',
                 ALR_Flag_Caveat_CTe__c = true,
                 ALR_Flag_Caveat__c = true,
                 ALR_Flag_CTe__c = true,
                 ALR_Flag_Invoice_Carrier__c = true,
                 ALR_Flag_Invoice_Combined__c = true,
                 ALR_Flag_Invoice_Parts_Refused__c = true,
                 ALR_Flag_Invoice_Parts__c = true,
                 ALR_Flag_Invoice_Salvado__c = true,
                 ALR_Flag_Invoice_Service_Refused__c = true,
                 ALR_Flag_Invoice_Service__c = true,
                 ALR_indemnification_of_parts__c = true,
                 ALR_Indemnification_of_Service__c = true,
                 ALR_Indemnification_total_loss__c = true,
                 ALR_Inspection_Type__c = 'Imagem',
                 ALR_Inspector_Email__c = 'teste@teste.com',
                 ALR_Inspector_Name__c = 'Teste',
                 ALR_Invoice_Combined_Refused__c =true,
                 ALR_Invoice_Parts_Refused__c =true,
                 ALR_Invoice_Salavado_Refused__c =false,
                 ALR_Invoice_Service_Refused__c =true,
                 ALR_lastUpdateTime__c = System.now(),
                 ALR_Local_Ocorrence__c = 'Pais Leme',
                 ALR_Mobile__c = '111199992',
                 ALR_Myself__c = false,
                 ALR_Necessary_Repair_TL_Docs__c = 'Nota Fiscal de Serviços',
                 ALR_Neighborhood_Occurrence__c = 'Pinheiros',
                 ALR_Observation__c = 'teste',
                 ALR_Pending_Docs_Received__c =   'teste teste',
                 ALR_Pending_Docs_Refused__c = 'teste teste',
                 ALR_Pending_Estimate__c = 'teste',
                 ALR_Pending_Inspection__c = 'teste teste',
                 ALR_Pending_Insurance_Ended__c = 'testando com a classe de testes',
                 ALR_Pending_Insurance_Open__c = 'testando com a classe de testes',
                 ALR_Pending_Released_Repair__c = 'testando com a classe de testes',
                 ALR_Pending_Reparation__c = 'teste testando',
                 ALR_Phase_Process__c = '100',
                 ALR_Phone__c = '11112222',
                 ALR_Reason_Refusal__c = 'testando com a classe de testes',
                 ALR_Send_Email__c = true,
                 ALR_Send_SMS__c = true,
                 ALR_Sinister_Nature__c = '187',
                 ALR_Status_System__c = 'Pending processing',
                 ALR_Status__c = 'Pending dealership',
                 ALR_Trunck_Position_Number__c = '5',
                 ALR_UF_Occurrence__c = 'SP',
                 ALR_Zip_Code_Occurrence__c = '07910090',
                 The_Damaged_Vehicle_Region__c = 'lateral direita'
             );
             listActivateInsurance.add(at);
               List<ALR_Document_Insurance__c> lsDocs = new List<ALR_Document_Insurance__c>();
              ALR_Document_Insurance__c dI1 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = at.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Service',
               ALR_Invoice_Value__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '365',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='22'
           );  
               
               ALR_Document_Insurance__c dI2 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = at.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Part',
               ALR_Invoice_ValueOfParts__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '145',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='25'
               
           );  
               
               ALR_Document_Insurance__c dI3 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = at.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c =date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Salvado',
               ALR_InvoiceValueSalvado__c = 36000.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '123',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='123'
               
           );
               
              lsDocs.add(dI1);
               lsDocs.add(dI2);
               lsDocs.add(dI3);
               insert lsDocs;
               
             upsert at;
             at.ALR_Status__c = 'Sent';
             at.ALR_Event_Phase_Process__c = '108';
             at.ALR_Caveat_CTe_refused__c = true;
             at.ALR_Check_Caveat__c = false;
             
             at.ALR_CTe_refused__c = true;
             at.ALR_Check_CTe__c = false;
             at.ALR_Photo_Chassi_Marking_glass_refused__c = true;
             at.ALR_Check_Photo_Chassi__c = false;
             at.ALR_Invoice_Carrier_refused__c = true;
             at.ALR_Invoice_Service_Refused__c = true;
             at.ALR_Check_Invoice_Service__c = false;
             at.ALR_Check_Invoice_Carrier__c = false;
             at.ALR_Check_Invoice_Combined__c = false;  
             at.ALR_Invoice_Parts_Refused__c = true;
             at.ALR_Check_Invoice_Parts__c = false;
             at.ALR_Phase_Process__c = '104';
             at.ALR_InvoiceServiceRequested__c = true;
             at.ALR_InvoicePartRequested__c = true;
               
               try{
                   update at;
               }catch(Exception e){
                   system.debug('TESTE'+e);
               }
             at.ALR_Phase_Process__c = '105';  
             try{
                   update at;
               }catch(Exception e){
                   system.debug('TESTE'+e);
               }
              
               
             List<ALR_Activate_Insurance__c>lsActInsurance = new List<ALR_Activate_Insurance__c>();
             Map<Id, ALR_Activate_Insurance__c> mapOldActInsurance = new Map<Id, ALR_Activate_Insurance__c>();
             mapOldActInsurance.put(at.Id, at);
             lsActInsurance.add(at);
             activateInsurance.validaEnvioSeguradora(lsActInsurance, mapOldActInsurance);
               
             at.ALR_Status__c = 'Pending dealership';
             at.ALR_Caveat_CTe_refused__c = true;
             at.ALR_Check_Caveat__c = false;
             update at;
             mapOldActInsurance.put(at.Id, at);
             lsActInsurance.add(at);
             activateInsurance.validaEnvioSeguradora(lsActInsurance, mapOldActInsurance);  
               
               
           }else{
               ALR_Activate_Insurance__c activateInsurance2 = new ALR_Activate_Insurance__c(
                 ALR_Amount_Damage__c = 2,
                 ALR_Approved_Value_Salvado__c = 1300.00,
                 ALR_Arrival_Dealership__c = date.today(),
                 ALR_Carrier__c = 'BRAZUL TRANSPORTE DE VEICULOS',
                 ALR_Chassi__c = track.id,
                 ALR_Check_Caveat__c = true,
                 ALR_Check_CTe__c = true,
                 ALR_Check_Invoice_Carrier__c = true,
                 ALR_Check_Invoice_Combined__c = true,
                 ALR_Check_Invoice_Parts__c = true,
                 ALR_Check_Invoice_Salvado__c = true,
                 ALR_Check_Invoice_Service__c = true,
                 ALR_Check_Photo_Chassi__c = true,
                 ALR_City_Occurrence__c = 'São Paulo',
                 ALR_Claim_Number__c = '789109',
                 ALR_Comercial_Phone__c = '22223333',
                 ALR_Contact_Email__c = 'teste@test.com',
                 ALR_Contact_Name__c = 'test',
                 ALR_CTe__c = '123123',
                 ALR_Date_Docs_Received__c = date.today(),
                 ALR_Date_Docs_Refused__c = date.today().addDays(-3),
                 ALR_Date_Error_Insurance__c = date.today().addDays(-3),
                 ALR_Date_Estimate_Received__c = date.today().addDays(-1),
                 ALR_Date_Estimate_Requested__c = date.today(),
                 ALR_Date_Finalized_Process__c = date.today(),
                 ALR_Date_Inspection_Received__c = date.today(),
                 ALR_Date_Inspection_Requested__c = date.today().addDays(-1),
                 ALR_Date_Insurance_Ended__c = date.today().addDays(-1), 
                 ALR_Date_No_Send_Insurer__c = date.today(),
                 ALR_Date_Occurrence__c = date.today().addDays(-15),
                 ALR_Date_Open_Insurance__c = date.today().addDays(-2),
                 ALR_Date_Pending_Processing__c  = date.today().addDays(-2),
                 ALR_Date_Process__c = date.today().AddMonths(-1),
                 ALR_Date_Received_Insurer__c = date.today(),
                 ALR_Date_Repair_Released__c = date.today().addDays(-3),
                 ALR_Date_Reparation_Requested__c = date.today().addDays(-2),
                 ALR_Date_Total_Loss_Defined__c = date.today().addDays(-2),
                 ALR_DDD_Comercial_Phone__c = '11',
                 ALR_DDD_Mobile__c = '11',
                 ALR_DDD__c = '11',
                 ALR_Description_Pending__c = 'teste teste teste teste',
                 ALR_Driver_Name__c = 'teste teste',
                 ALR_Error_Description__c = 'testando os testes com a classe de teste',
                 ALR_Estimated_Loss_Value__c = 12.4,
                 ALR_Event_Phase_Process__c = '108',
                 ALR_Flag_Caveat_CTe__c = true,
                 ALR_Flag_Caveat__c = true,
                 ALR_Flag_CTe__c = true,
                 ALR_Flag_Invoice_Carrier__c = true,
                 ALR_Flag_Invoice_Combined__c = true,
                 ALR_Flag_Invoice_Parts_Refused__c = true,
                 ALR_Flag_Invoice_Parts__c = true,
                 ALR_Flag_Invoice_Salvado__c = true,
                 ALR_Flag_Invoice_Service_Refused__c = true,
                 ALR_Flag_Invoice_Service__c = true,
                 ALR_indemnification_of_parts__c = true,
                 ALR_Indemnification_of_Service__c = true,
                 ALR_Indemnification_total_loss__c = true,
                 ALR_Inspection_Type__c = 'Imagem',
                 ALR_Inspector_Email__c = 'teste@teste.com',
                 ALR_Inspector_Name__c = 'Teste',
                 ALR_Invoice_Combined_Refused__c =true,
                 ALR_Invoice_Parts_Refused__c =true,
                 ALR_Invoice_Salavado_Refused__c =false,
                 ALR_Invoice_Service_Refused__c =true,
                 ALR_lastUpdateTime__c = System.now(),
                 ALR_Local_Ocorrence__c = 'Pais Leme',
                 ALR_Mobile__c = '111199992',
                 ALR_Myself__c = false,
                 ALR_Necessary_Repair_TL_Docs__c = 'Nota Fiscal de Serviços',
                 ALR_Neighborhood_Occurrence__c = 'Pinheiros',
                 ALR_Observation__c = 'teste',
                 ALR_Pending_Docs_Received__c =   'teste teste',
                 ALR_Pending_Docs_Refused__c = 'teste teste',
                 ALR_Pending_Estimate__c = 'teste',
                 ALR_Pending_Inspection__c = 'teste teste',
                 ALR_Pending_Insurance_Ended__c = 'testando com a classe de testes',
                 ALR_Pending_Insurance_Open__c = 'testando com a classe de testes',
                 ALR_Pending_Released_Repair__c = 'testando com a classe de testes',
                 ALR_Pending_Reparation__c = 'teste testando',
                 ALR_Phase_Process__c = '100',
                 ALR_Phone__c = '11112222',
                 ALR_Reason_Refusal__c = 'testando com a classe de testes',
                 ALR_Send_Email__c = true,
                 ALR_Send_SMS__c = true,
                 ALR_Sinister_Nature__c = '187',
                 ALR_Status_System__c = 'Pending processing',
                 ALR_Status__c = 'Not sent',
                 ALR_Trunck_Position_Number__c = '5',
                 ALR_UF_Occurrence__c = 'SP',
                 ALR_Zip_Code_Occurrence__c = '07910090',
                 The_Damaged_Vehicle_Region__c = 'lateral direita'
             );
               
             upsert activateInsurance2;
             activateInsurance2.ALR_Status__c = 'Sent';
             activateInsurance2.ALR_Amount_Damage__c = 0;
             activateInsurance2.ALR_Check_CTe__c = false;
             activateInsurance2.ALR_Check_Caveat__c = false;
             activateInsurance2.ALR_Check_Photo_Chassi__c = false;
             activateInsurance2.ALR_Check_Invoice_Carrier__c = false;
               try{
                   update activateInsurance2;
               }catch(Exception e){
                   system.debug('TESTE'+e);
               }
             List<ALR_Activate_Insurance__c>lsActInsurance = new List<ALR_Activate_Insurance__c>();
             Map<Id, ALR_Activate_Insurance__c> mapOldActInsurance = new Map<Id, ALR_Activate_Insurance__c>();
             mapOldActInsurance.put(activateInsurance2.Id, activateInsurance2);
             lsActInsurance.add(activateInsurance2);
             activateInsurance.validaEnvioSeguradora(lsActInsurance, mapOldActInsurance);
               
             activateInsurance2.ALR_Status__c = 'Pending dealership';
             activateInsurance2.ALR_Caveat_CTe_refused__c = true;
             activateInsurance2.ALR_Check_Caveat__c = false;
             update activateInsurance2;
             mapOldActInsurance.put(activateInsurance2.Id, activateInsurance2);
             lsActInsurance.add(activateInsurance2);
             activateInsurance.validaEnvioSeguradora(lsActInsurance, mapOldActInsurance);  
               
           }
       }
       
       Database.upsert(listActivateInsurance);
       Map<Id, ALR_Activate_Insurance__c> mapOldActInsurance = new Map<Id, ALR_Activate_Insurance__c>();
       integer i=0;
       List<ALR_Document_Insurance__c> listDocs = new List<ALR_Document_Insurance__c>();
       for(ALR_Activate_Insurance__c actI : listActivateInsurance){
           actI.ALR_Status__c = 'Pending dealership';
           mapOldActInsurance.put(actI.ID, actI); ///gabi
         if(i==0){
           ALR_Document_Insurance__c documentInsurance1 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'CT-e',
                ALR_Invoice_Number__c = '1234567890',
               ALR_Serie__c = '789',
               ALR_Sub_Serie__c ='25',
               ALR_Status__c = 'Active'
               
           );
           ALR_Document_Insurance__c documentInsurance2 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Caveat',
                ALR_Invoice_Number__c = '1234567890',
               ALR_Serie__c = '256',
               ALR_Sub_Serie__c ='22',
               ALR_Status__c = 'Active'
           );
           ALR_Document_Insurance__c documentInsurance3 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Shipping Invoice',
                ALR_Invoice_Number__c = '1234567890',
               ALR_Serie__c = '745',
               ALR_Sub_Serie__c ='55',
               ALR_Status__c = 'Active'
               
           );
             listDocs.add(documentInsurance1);
             listDocs.add(documentInsurance2);
             listDocs.add(documentInsurance3);
             i++;
         }
         
         if(i==1){
           ALR_Document_Insurance__c documentInsurance4 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Photo Chassi/Marking on glass',
               ALR_Status__c = 'Active'
               
           );
           ALR_Document_Insurance__c documentInsurance5 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Service',
               ALR_Invoice_Value__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '124',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='22'
               
           );
           ALR_Document_Insurance__c documentInsurance6 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Part',
               ALR_Invoice_ValueOfParts__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '12344',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='12345'
           );
             //listDocs.add(documentInsurance4);
             //listDocs.add(documentInsurance5);
            //listDocs.add(documentInsurance6);
            i++;
         }
         
         if(i==2){
           ALR_Document_Insurance__c documentInsurance7 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Conjugated',
               ALR_Invoice_Value__c = 3600.00,
               ALR_Invoice_ValueOfParts__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '478',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='88'
               
           );
           ALR_Document_Insurance__c documentInsurance8 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c =date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Salvado',
               ALR_InvoiceValueSalvado__c = 36000.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '785',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='55'
               
           );
           ALR_Document_Insurance__c documentInsurance9 = new ALR_Document_Insurance__c(
               ALR_Activate_Insurance__c = actI.id,
               ALR_Doc_Name__c = 'teste',
               ALR_Doc_Type__c = 'Invoice',
               ALR_Issuance_Invoice__c = date.today(),
               ALR_Invoice_Model__c = 'Talon',
               ALR_Invoice_Number__c = '1234567890',
               ALR_Invoice_Type__c = 'Parts',
               ALR_Invoice_ValueOfParts__c = 3600.00,
               ALR_Observacao__c = 'teste',
               ALR_Serie__c = '123',
               ALR_Status__c = 'Active',
               ALR_Sub_Serie__c ='22'
               
               
           );
             //listDocs.add(documentInsurance7);
             //listDocs.add(documentInsurance8);
             //listDocs.add(documentInsurance9);
             i++;
             break;
         }
         
         }
         Database.insert(listDocs);
         
         List<ALR_Activate_Insurance__c> listActiveInsuranceOldd   = new   List<ALR_Activate_Insurance__c>();                                                                                                   
         for(ALR_Activate_Insurance__c actI :listActivateInsurance){
             actI.ALR_Zip_Code_Occurrence__c = '07910001';
             listActiveInsuranceOldd.add(actI);
         }
         Database.update(listActiveInsuranceOldd);
         
        
         
         Blob bodyBlob=Blob.valueOf('IA==');
         List<Attachment> lsAttachment = new List<Attachment> ();
         Integer d = 0;
         for(ALR_Document_Insurance__c actInsu : listDocs){
           Attachment novoAttachment = new Attachment (
               body = bodyBlob,
               ParentId = actInsu.id,
               IsPrivate = false
           );
           if(actInsu.ALR_Doc_Type__c.equalsIgnoreCase('Photo distance')
             || actInsu.ALR_Doc_Type__c.equalsIgnoreCase('Photo Chassi/Marking on glass'))
               novoAttachment.Name = 'Fotos.png';
             else
               novoAttachment.Name = 'Documents.pdf';  
             
           lsAttachment.add(novoAttachment); 
             d++;
         }
      Database.insert(lsAttachment);
         
         
      
         
      List<ALR_Damage__c> lsDamage = new List<ALR_Damage__c>();
       for(ALR_Activate_Insurance__c actInsu : listActivateInsurance){
            ALR_Damage__c damage = new ALR_Damage__c(
            ALR_Activate_Insurance__c = actInsu.id,
            ALR_Cod_Quadrant__c  = 'superior central' ,
            //ALR_Cod_Size_Of_Damage__c = '1',
            ALR_Damage_Area__c = '2',
            ALR_Damage_Type__c = '34',
            ALR_Flash_Aves__c = '4',
            ALR_Position__c = '1',
            ALR_Referencial_Part__c = '172',
            ALR_Size_Damage__c = '1',
            ALR_Status__c = 'Active'
          );
            lsDamage.add(damage);
        }
      Database.insert(lsDamage);
        
      
      
      //   List<ALR_Activate_Insurance__c> listActivateInsurance = [select id from ALR_Activate_Insurance__c where ALR_Chassi__c in:(listTracking)]; 
        // System.debug('########### listActivateInsurance testClass'+listActivateInsurance);
      //   Utils_ALR_ActivateInsurance activateInsurance = Utils_ALR_ActivateInsurance.getInstance();
         Utils_ALR_ActivateInsurance.validateUser(listActivateInsurance,listActiveInsuranceOldd);
         activateInsurance.duplicidadeActivanteInsurance(listActivateInsurance);
         activateInsurance.newActivateIncurance(listActivateInsurance);
         activateInsurance.validaEnvioSeguradora(listActivateInsurance, mapOldActInsurance);
         activateInsurance.newDamage(lsDamage);
         
        List<ALR_Activate_Insurance__c> listAuxActInsu = new List<ALR_Activate_Insurance__c>();
        for(ALR_Activate_Insurance__c actInsu : listActivateInsurance){
          actInsu.ALR_Arrival_Dealership__c = null;
          listAuxActInsu.add(actInsu);
          
        }
        activateInsurance.newActivateIncurance(listAuxActInsu);
       
       List<ALR_Tracking__c> lsTracking = new List<ALR_Tracking__c>();
       for(ALR_Tracking__c track : listTracking){
         track.ALR_Date_MADP__c = date.today();
         lsTracking.add(track);
       }
       Database.update(lsTracking);
       
       
       
       
      
       //  activateInsurance.checkDocuments(listDocs);
         //Utils_ALR_ActivateInsurance activateInsurance = Utils_ALR_ActivateInsurance.getInstance();  
      List<ALR_Document_Insurance__c>   lsDocx = new   List<ALR_Document_Insurance__c>();      
      for(ALR_Damage__c damag : lsDamage){
          for(integer f =0;i<3;i++){
          ALR_Document_Insurance__c docInsu = new  ALR_Document_Insurance__c();
          docInsu.ALR_Activate_Insurance__c = null;
          docInsu.ALR_Damage__c = damag.Id;
          System.debug('############ damag.Id: '+damag.Id);  
          docInsu.ALR_Doc_Type__c = 'Photo distance';   
          docInsu.ALR_Issuance_Invoice__c =date.today();
          lsDocx.add(docInsu);
          }   
        
      }
      Database.insert(lsDocx);  
        //Blob bodyBlob=Blob.valueOf('IA==');
         List<Attachment> lsAttachment2 = new List<Attachment> ();
         Integer e = 0;
         for(ALR_Document_Insurance__c actInsu : lsDocx){
           Attachment novoAttachment = new Attachment (
               body = bodyBlob,
               ParentId = actInsu.id,
               
               IsPrivate = false,
               Name = 'teste'+e+'.txt'
           );
           System.debug('########## novoAttachment '+novoAttachment);
           lsAttachment2.add(novoAttachment); 
             e++;
         }
          Database.insert(lsAttachment2);
        
      //activateInsurance.setIdAttToDamage(lsAttachment2);
      
    }
  
}