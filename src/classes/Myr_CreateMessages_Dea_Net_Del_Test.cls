@isTest 
private class Myr_CreateMessages_Dea_Net_Del_Test {
	private static String Country;

	@testSetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
        //initialize a list of customer message settings and customer message templates for tests
        Myr_Datasets_Test.prepareCustomerMessageSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, true) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Germany', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Germany2', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
		Country=Myr_Datasets_Test.getRoleName();
    }  
    
	static Account Dea_Net_Del_Common(String My_Country, String Brand) {
		List<Customer_Message_Templates__c> List_Templ;
		List<Customer_Message__c> listMsg;
		Customer_Message_Templates__c Templ_D_Ne_D;
		Customer_Message_Templates__c Templ_D_No_D;
		Customer_Message__c My_Message;
		Customer_Message__c Mess_D_Ne_D;
		Customer_Message__c Mess_D_No_D;
		Account Account_Resultat;
		Account My_Dealer;
		Account acc;
		String Favourite_Dealer_BIR_Id;
		String Dealer_Name;
		
		System.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')){
			acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Paul','Durandet','','paul.durandet@atos.net','paul.durandet@atos.net', '', 'USER_ACTIF', '', 'France', '', '', '', '');
			if (Brand.equalsIgnoreCase('Renault')){
				acc.MyR_Dealer_1_BIR__c=Favourite_Dealer_BIR_Id;
				acc.Myr_Status__c='Activated';
			}else{
				acc.MyD_Dealer_1_BIR__c=Favourite_Dealer_BIR_Id;
				acc.Myd_Status__c='Activated';
			}
			update acc;
		}
		return acc;
	}
	
    static testMethod void test_Dealer_Network_Deleted(){
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Logger__c> L_L;
		Country_Info__c C_I = [SELECT Myr_Customer_Message_DND_Asynchronous__c FROM Country_Info__c WHERE Name = 'France'];
		C_I.Myr_Customer_Message_DND_Asynchronous__c=false;
		update C_I;
		Test.startTest();
	   		Myr_CreateMessages_Dea_Net_Del.Dealer_Network_Deleted(a.id, 'Renault', 'France');
    	Test.stopTest();
		L_L = [select Message__c from Logger__c];
		
		System.AssertEquals('Launch_Mode=SYNC',L_L[0].Message__c);
	}

	static testMethod void test_Dealer_Network_Deleted_Async(){
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Logger__c> L_L;
		Country_Info__c C_I = [SELECT Myr_Customer_Message_DND_Asynchronous__c FROM Country_Info__c WHERE Name = 'France'];
		C_I.Myr_Customer_Message_DND_Asynchronous__c=true;
		update C_I;
		Test.startTest();
	   		Myr_CreateMessages_Dea_Net_Del.Dealer_Network_Deleted(a.id, 'Renault', 'France');
    	Test.stopTest();
		L_L = [select Message__c from Logger__c];
		
		System.AssertEquals('Launch_Mode=ASYNC',L_L[0].Message__c);
	}

	static testMethod void test_Manage_Asynchronously(){
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Logger__c> L_L;
		Test.startTest();
	   		Myr_CreateMessages_Dea_Net_Del.Manage_Asynchronously(a.id, 'Renault');
    	Test.stopTest();
		L_L = [select Message__c from Logger__c];
		
		System.AssertEquals('Launch_Mode=ASYNC',L_L[0].Message__c);
	}
	
	static testMethod void test_Manage(){
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Customer_Message__c> listMsg;
		String SOQL_QUERY='SELECT Title__c, Template__r.Name FROM Customer_Message__c WHERE Account__c =\'' + a.Id + '\'';
		
		a.MyR_Dealer_1_BIR__c='DEL_123402';
		update a;
		 
		User communityUser = [SELECT Id FROM User WHERE AccountId = :a.Id];
		Test.startTest();
		System.runAs(communityUser) {
	   		Myr_CreateMessages_Dea_Net_Del.Manage(a.id, 'Renault', Myr_CreateMessages_Dea_Net_Del.Mode_Launch.SYNC);
		}
    	Test.stopTest();
		
		listMsg=Database.query(SOQL_QUERY);
		
		System.assertEquals(2,listMsg.size());

		for(Customer_Message__c c : listMsg){
			if(c.Template__r.Name.startsWith('DEALER_NETWORK_DELETED')){
				System.AssertEquals('[Renault] Oups ! Votre concessionnaire Renault a ete supprime de votre compte', listMsg[0].Title__c);
			}
		}
	} 
	
	static testMethod void test_Prepare(){ 
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Customer_Message__c> listMsg;
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		String SOQL_QUERY='SELECT Title__c, Template__r.Name FROM Customer_Message__c WHERE Account__c =\'' + a.Id + '\'';
		
		a.MyR_Dealer_1_BIR__c='DEL_123402'; 
		update a;
		 
		User communityUser = [SELECT Id FROM User WHERE AccountId = :a.Id];
		Test.startTest();
		System.runAs(communityUser) {
	   		M_H = Myr_CreateMessages_Dea_Net_Del.Prepare(a.id, 'Renault');
		}
    	Test.stopTest();
		System.AssertEquals(a.id, M_H.My_Account.Id);

		listMsg=Database.query(SOQL_QUERY);
		
		System.assertEquals(1,listMsg.size());
	}
	
	
	static testMethod void test_Launch_Actions(){
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Customer_Message__c> listMsg;
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		String SOQL_QUERY='SELECT Title__c, Template__r.Name FROM Customer_Message__c WHERE Account__c =\'' + a.Id + '\'';
		
		a.MyR_Dealer_1_BIR__c='DEL_123402';
		update a;
		 
		User communityUser = [SELECT Id FROM User WHERE AccountId = :a.Id];
		Test.startTest();
		System.runAs(communityUser) {
	   		M_H = Myr_CreateMessages_Dea_Net_Del.Prepare(a.id, 'Renault');
			Myr_CreateMessages_Dea_Net_Del.Launch_Actions(M_H);
		}
    	Test.stopTest();
		System.AssertEquals(a.id, M_H.My_Account.Id);

		listMsg=Database.query(SOQL_QUERY);
		
		System.assertEquals(2,listMsg.size());

		for(Customer_Message__c c : listMsg){
			if(c.Template__r.Name.startsWith('DEALER_NETWORK_DELETED')){
				System.AssertEquals('[Renault] Oups ! Votre concessionnaire Renault a ete supprime de votre compte', listMsg[0].Title__c);
			}
		}
	}
	
	static testMethod void test_Create_A_Message() { 
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Customer_Message__c> listMsg;
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		String SOQL_QUERY='SELECT Title__c, Template__r.Name, Body__c FROM Customer_Message__c WHERE Account__c =\'' + a.Id + '\'';
		Account My_Dealer;
		
		System.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')){
			My_Dealer = Myr_Datasets_Test.insertDealer('DealerTheBestAroundTheWord', '123123', 'Dealer_Network_Deleted@test.com', Country, '35000', 'Rennes', 'rue de paris');
		}
		
		M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler(a, 'Renault');
		M_H.Brand='Renault';
		M_H.My_Account=a;
		M_H.My_Dealer=My_Dealer;
		 
		User communityUser = [SELECT Id FROM User WHERE AccountId = :a.Id];
		Test.startTest();
		System.runAs(communityUser) {
			Myr_CreateMessages_Dea_Net_Del.Create_A_Message(M_H);
		}
    	Test.stopTest();

		listMsg=Database.query(SOQL_QUERY);
		
		System.assertEquals(1,listMsg.size());

		for(Customer_Message__c c : listMsg){
			if(c.Template__r.Name.startsWith('DEALER_NETWORK_DELETED')){
				System.AssertEquals('[Renault] Oups ! Votre concessionnaire '+M_H.Brand+' a ete supprime de votre compte', listMsg[0].Title__c);
				System.AssertEquals('[Renault] Nous vous informons que votre concessionnaire '+M_H.My_Dealer.Name+' ne fait plus partie du reseau '+M_H.Brand+'. Choisissez vite un autre concessionnaire, vous pourrez alors le contacter facilement ou prendre directement rendez-vous avec lui. En quelques clics, c est fait ! Merci de votre confiance. L equipe MY Renault', listMsg[0].Body__c);
			}
		}
	}
	
	static testMethod void test_Set_Dealer_To_Empty() {
		Account a = Dea_Net_Del_Common('France','Renault');
		List<Customer_Message__c> listMsg;
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		String SOQL_QUERY='SELECT Title__c, Template__r.Name, Body__c FROM Customer_Message__c WHERE Account__c =\'' + a.Id + '\'';
		Account My_Dealer;
		
		System.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')){
			My_Dealer = Myr_Datasets_Test.insertDealer('DealerTheBestAroundTheWord', '123123', 'Dealer_Network_Deleted@test.com', Country, '35000', 'Rennes', 'rue de paris');
		}
		
		M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler(a, 'Renault');
		M_H.Brand='Renault';
		M_H.My_Account=a;
		M_H.My_Dealer=My_Dealer;
		 
		User communityUser = [SELECT Id FROM User WHERE AccountId = :a.Id];
		Test.startTest();
		System.runAs(communityUser) {
			Myr_CreateMessages_Dea_Net_Del.Create_A_Message(M_H);
		}
    	Test.stopTest();

		listMsg=Database.query(SOQL_QUERY);
		
		System.assertEquals(1,listMsg.size());

		for(Customer_Message__c c : listMsg){
			if(c.Template__r.Name.startsWith('DEALER_NETWORK_DELETED')){
				System.AssertEquals('[Renault] Oups ! Votre concessionnaire '+M_H.Brand+' a ete supprime de votre compte', listMsg[0].Title__c);
				System.AssertEquals('[Renault] Nous vous informons que votre concessionnaire '+M_H.My_Dealer.Name+' ne fait plus partie du reseau '+M_H.Brand+'. Choisissez vite un autre concessionnaire, vous pourrez alors le contacter facilement ou prendre directement rendez-vous avec lui. En quelques clics, c est fait ! Merci de votre confiance. L equipe MY Renault', listMsg[0].Body__c);
			}
		}	
	}
	/*
	static testMethod void test_Set_Status_To_Read() {
   		Account a = Dea_Net_Del_Common(Country,'Renault');
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler(a,'Renault');
		
		Test.startTest();
	   		Myr_CreateMessages_Dea_Net_Del.Set_Status_To_Read(M_H);
    	Test.stopTest();
	}
	
	static testMethod void test_Search_Dealer() {
		Account a = Dea_Net_Del_Common(Country,'Renault');
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler(a,'Renault');
		
		Test.startTest();
	   		Myr_CreateMessages_Dea_Net_Del.Search_Dealer(M_H);
    	Test.stopTest();
	}
	*/
}