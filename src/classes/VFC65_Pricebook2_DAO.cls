/**
*	Class   	-   VFC65_Pricebook2_DAO
*   Author  	-   Suresh Babu 
*   Date    	-   12/03/2013
*	DEscription	-	DAO class for Pricebook Object..
*/

public class VFC65_Pricebook2_DAO {
	private static final VFC65_Pricebook2_DAO instance = new VFC65_Pricebook2_DAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC65_Pricebook2_DAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC65_Pricebook2_DAO getInstance(){
        return instance;
    }
    
    /**
    	Method to fetch the Standard Price Book record..
    **/
    public Pricebook2 fetch_StandardPriceBook(){
    	Pricebook2 priceBook = new Pricebook2();
    	
    	try{
    		priceBook = [SELECT 
								IsActive, IsStandard, Id, Name 
							FROM 
								Pricebook2
							WHERE
								IsStandard = true
								limit 1
							];
    		return priceBook;
    	}
    	catch(QueryException ex){
        	return null;
        }	
    }
}