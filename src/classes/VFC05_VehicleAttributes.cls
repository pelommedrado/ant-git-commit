public with sharing class VFC05_VehicleAttributes 
{
    public Utils_WarrantyDataSource WarrantyDataSource; // Data source to access the Web Service
    public Utils_OTSDataSource OTSDataSource;
    public Utils_MaintDataSource MaintDataSource;
    public Utils_WarrantyHistoryDataSource WarrantyHistoryDataSource;
    public Utils_ContractDataSource ContractDataSource;
    public Utils_WarrantyHistoryDetailDataSource WarrantyHistoryDetailDataSource;
    public WS_RCArch_UtilsV2 CasesDataSource;
    public String modulo {get;set;}
    public String numInt {get;set;}
    public String histoBim {get;set;}
    public String rc {get;set;}
    public String toParse{get;set;}
    public VFC05_ArchivesXMLData xmlData{get;set;}
    
    //XML Data
    public VFC05_ArchivesProperties.ArchivesCases[] ArchCasesList {get;set;}
    public VFC05_ArchivesProperties.ArchivesCases caseDetails {get;set;}
    public VFC05_ArchivesProperties.ArchivesAttachments[] ArchAttachmentsList {get;set;}  
    private VEH_Veh__c vehicle;
    public String IdCase {get;set;}
    public String attachmentURL{get;set;}
    public String attachmentXMLURL{get;set;}
    public String countryInfo{get;set;} 
    
    //
    
    public String accountId{get;set;}
        public String assetId{get;set;}
        public String brandInfoExchangeFlag{get;set;}
        public String buId{get;set;}
        public String build{get;set;}
        public String city{get;set;}
        public String clientAgreement{get;set;}
        public String conflictId{get;set;}
        public String contactCellPhNum{get;set;}
        public String contactHomePhNum{get;set;}
        public String contactId{get;set;}
        public String contactWorkPhNum{get;set;}
        public String country{get;set;}
        public DateTime created{get;set;}
        public String createdBy{get;set;}
        public DateTime createdDate{get;set;}
        public String descText{get;set;}
        public String detail{get;set;}
        public String firstName{get;set;}
        public String from_x{get;set;}
        public String lastName{get;set;}
        public DateTime lastUpdated{get;set;}
        public String lastUpdatedBy{get;set;}
        public String login{get;set;}
        public String modificationNumber{get;set;}
        public String name{get;set;}
        public String negoResult{get;set;}
        public String orgId{get;set;}
        public DateTime purchaseDate{get;set;}
        public String purchaseIntention{get;set;}
        public String quoteType{get;set;}
        public String rcName{get;set;}
        public String rcRowId{get;set;}
        public String resAuthor{get;set;}
        public DateTime resDate{get;set;}
        public String rowId{get;set;}
        public String serialNum{get;set;}
        public String srArea{get;set;}
        public String srAreaCode{get;set;}
        public String srBrand{get;set;}
        public String srCustNum{get;set;}
        public String srNum{get;set;}
        public String srSubtypeCode{get;set;}
        public Integer state{get;set;}
        public String subArea{get;set;}
        public String traitment{get;set;}
        public String zipCode{get;set;}

    public Integer showDetail 
    {
        get { if(showDetail == null) {showDetail = 0; return showDetail;} else return showDetail; }
        set { showDetail = value; }
    }
    
           public Integer showValue
        {
            get { if(showValue == null) {showValue = 0; return showValue;} else return showValue; }
            set { showValue = value; }
        }
    
      public Integer showError 
        {
            get { if(showError == null) {showError = 0; return showError;} else return showError; }
            set { showError = value; }
        }
    // Show detail key
    // 0 Choose page
    // 1 Vehicle
    // 2 Warranty
    // 4 Contract
    // 5 OTR
    // 6 Maintenance
    // 7 Warranty History
  
  
    /*=======================================
      CONSTRUCTOR
    =======================================*/     
    public VFC05_VehicleAttributes(ApexPages.StandardController stdController) 
    { 
        WarrantyDataSource = new Utils_WarrantyDataSource();
        OTSDataSource = new Utils_OTSDataSource();
        MaintDataSource = new Utils_MaintDataSource();
        ContractDataSource = new Utils_ContractDataSource();
        WarrantyHistoryDataSource = new Utils_WarrantyHistoryDataSource();
        WarrantyHistoryDetailDataSource = new Utils_WarrantyHistoryDetailDataSource();
        CasesDataSource = new WS_RCArch_UtilsV2();



        
       VEH_Veh__c tmp= (VEH_Veh__c)stdController.getRecord();
        //System.debug('### => vehicle.id : ' + vehicle.id);        
        this.vehicle = [select id, name,CountryOfDelivery__c from VEH_Veh__c where id = :tmp.id];

        if(vehicle.CountryOfDelivery__c=='')
        {

           System.debug('Country Of Delivery is null>>>>>>>>>>>>>>');
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'Please Make sure the Record Default Country is not empty'));


        }
      
    
       
            
            
 
        // WARNING: If you comment out this code, the webservices will fail with an error, there are no endpoints yet.
        if (!Test.isRunningTest())
        {
            WarrantyDataSource.test = false; //dummy implementation used       
            OTSDataSource.test = false;
            MaintDataSource.test = false;
            ContractDataSource.test = false;
            WarrantyHistoryDataSource.test = false;
            WarrantyHistoryDetailDataSource.test = false;
        }
    }

    public VFC05_WarrantyProperties[] wpList {get;set;}   
    public VFC05_OTSProperties[] otsPropList {get;set;} 
    public VFC05_MaintProperties maint {get;set;} 
    public VFC05_ContractProperties[] contracts {get;set;}
    public VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics[] whList {get;set;}
    public VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics whd {get;set;}
     

    public String getCountryInfo()
    {
        
        if (Test.isRunningTest())
        {
            return 'PTB';
        }
        
    User u = [Select Id, RecordDefaultCountry__c from user where Id =:UserInfo.getUserId()];    
    Country_Info__c c =[Select Id, Country_Code_3L__c from Country_Info__c where Name=:u.RecordDefaultCountry__c ];
    System.debug('Country Value is >>>>>'+c.Country_Code_3L__c);
    return c.Country_Code_3L__c;
    } 
     
    public void getWarrantyData() 
    {
        try 
        { 
            WS06_servicesBcsDfr.ListDetWarrantyCheck  results = WarrantyDataSource.getWarrantyData(this);
            
                
            if (results == null)
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            else
            {
                showDetail = 2;
             
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results not null'));
                WS06_servicesBcsDfr.ArrayOfErrMsg detInfoMsg = results.listErrMsg;
                WS06_servicesBcsDfr.ListDetWarrantyCheck detWar = results;
                     
                if (detInfoMsg != null)
                {
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + detInfoMsg.ErrMsg.zoneMsg));
                }
                    
                if (detWar != null) 
                {
                    wpList = new VFC05_WarrantyProperties[]{};
                    VFC05_WarrantyProperties wp = new VFC05_WarrantyProperties();
                    
                    if (detWar.listGarWarranty != null && detWar.listGarWarranty.ManufacturerWarranty != null)
                    {
                        for (WS06_servicesBcsDfr.ManufacturerWarranty w : detWar.listGarWarranty.ManufacturerWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'Manufacturer';
                            wp.datFinGar = w.datFinGarGAR;
                            wp.kmMaxGar = w.kmMaxGarGAR;
                            wpList.Add(wp);
                        }
                    }
                    
                    if (detWar.listAssWarranty != null && detWar.listAssWarranty.AssistanceWarranty != null)
                    {   
                        for (WS06_servicesBcsDfr.AssistanceWarranty w : detWar.listAssWarranty.AssistanceWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'Assistance';
                            wp.datFinGar = w.datFinGarASS;
                            wp.kmMaxGar = w.kmMaxGarASS;
                            wpList.Add(wp);
                        }
                    }
                    
                    if (detWar.listTurWarranty != null && detWar.listTurWarranty.PaintingWarranty != null)
                    {   
                        for (WS06_servicesBcsDfr.PaintingWarranty w : detWar.listTurWarranty.PaintingWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'Painting';
                            wp.datFinGar = w.datFinGarTUR;
                            wp.kmMaxGar = w.kmMaxGarTUR;
                            wpList.Add(wp);
                        } 
                    }
                    
                    if (detWar.listCorWarranty != null && detWar.listCorWarranty.CorrosionWarranty != null)
                    {
                        for (WS06_servicesBcsDfr.CorrosionWarranty w : detWar.listCorWarranty.CorrosionWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'Corrosion';
                            wp.datFinGar = w.datFinGarCOR;
                            wp.kmMaxGar = w.kmMaxGarCOR;
                            wpList.Add(wp);
                        }
                    }
                    
                    if (detWar.listBttWarranty != null && detWar.listBttWarranty.BatteryWarranty != null)     
                    {
                        for (WS06_servicesBcsDfr.BatteryWarranty w : detWar.listBttWarranty.BatteryWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'Battery';
                            wp.datFinGar = w.datFinGarBTT;
                            wp.kmMaxGar = w.kmMaxGarBTT;
                            wpList.Add(wp);
                        }
                    }
                     
                    if (detWar.listGmeWarranty != null &&  detWar.listGmeWarranty.GMPeWarranty != null)
                    {   
                        for (WS06_servicesBcsDfr.GMPeWarranty w : detWar.listGmeWarranty.GMPeWarranty)
                        {
                            wp = new VFC05_WarrantyProperties();
                            wp.Type = 'GMPe'; 
                            wp.datFinGar = w.datFinGarGME;
                            wp.kmMaxGar = w.kmMaxGarGME;
                            wpList.Add(wp);
                        }
                    }
                }
            }
        }
        catch (Exception e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'error'));
        }
    }    

    public void getOTSData() 
    {
        try 
        {   
            WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse  results = OTSDataSource.getOTSData(this);
         
            if (results == null) 
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            else 
            {
                showDetail = 5;
            
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results not null'));
                WS02_otsIcmApvBserviceRenault.otsInfoMsg otsInfoMsg;
                if (results.apvGetListOtsMsg != null)
                    otsInfoMsg = results.apvGetListOtsMsg[0];
                
                WS02_otsIcmApvBserviceRenault.Ots[] otsRes = results.apvGetListOts;
                    
                if (otsInfoMsg != null) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + otsInfoMsg.code));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + otsInfoMsg.text));
                } 
                
                if (otsRes != null) 
                {
                    otsPropList = new VFC05_OTSProperties[]{};
                    VFC05_OTSProperties otsProp;
                    
                    for (WS02_otsIcmApvBserviceRenault.Ots WSots : otsRes)
                    {
                        otsProp = new VFC05_OTSProperties();
                        otsProp.OTSType = WSots.libCouleur;
                        otsProp.OTSNo = WSots.numOts;
                        otsProp.OTSDescription = WSots.descOts;
                        otsProp.OTSTechnicalNoteNo = WSots.numNoteTech;
                        otsPropList.Add(otsProp);
                    }
                }           
            }
        }
        catch (Exception CalloutException) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'General error (try failed)'));
        } 
    }  
    
    public void getMaintenanceData() 
    {
        try 
        {    
            WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse  results = MaintDataSource.getMaintenanceData(this);
           
            if (results == null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            }
                                                             
            else 
            {
                showDetail = 6;
            
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results not null'));
                WS03_fullRepApvBserviceRenault.infoMsg infoMsg = results.infoMsg;
                WS03_fullRepApvBserviceRenault.PgmEnt maintRes = results.PgmEnt;
                    
                if (infoMsg != null) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + infoMsg.codeMsg));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'MessageRetour : ' + infoMsg.libelleMsg));
                }
                 
                if (maintRes != null) 
                {
                    maint = new VFC05_MaintProperties();   
                                         
                    maint.MaintTitle1 = findMaintLabel(maintRes.libFix, '26'); 
                    maint.MaintTitle2 = findMaintLabel(maintRes.libFix, '27');
                    maint.MaintTitle3 = findMaintLabel(maintRes.libFix, '28');
                    maint.MaintTitle4 = findMaintLabel(maintRes.libFix, '43');
                        
                    // build the list of secondary operations
                    String secondaryText = '';
                    String Style = ' style="background-color:#E2E2E2;"';                    
                    if (maintRes.secondary != null)
                    {
                        for (Integer s=0; s<maintRes.secondary.size();s=s+2 )
                        {
                            if ((maintRes.secondary[s].labelSecondary!=null ) && (maintRes.secondary[s].labelSecondary!=''))
                                secondarytext += '<tr><td>' + maintRes.secondary[s].labelSecondary + '</td>';
                            else secondarytext += '<tr><td>Vide</td>';
                                
                            if( (maintRes.secondary[s+1].labelSecondary!=null ) && (maintRes.secondary[s+1].labelSecondary!='') )
                                secondarytext += '<td>' + maintRes.secondary[s+1].labelSecondary + '</td></tr>';
                            else secondarytext += '<td>Vide</td></tr>';
                        }
                    } 
                                        
                    maint.MaintSecondaryOperations = secondarytext;
                        
                    //Build the list of primary operations, km and miles
                    String primaryText;
                    String sTableLine = '';
                    String sStyle = ' style="background-color:#E2E2E2;"';
                    
                    if (maintRes.merePrimary != null)
                    {
                        for (Integer s=0; s<maintRes.merePrimary.size(); s++)
                        {
                            primaryText += maintRes.merePrimary[s].libelle + ' - every ';
                            primaryText += maintRes.merePrimary[s].period_Distan_Toutes + ' year(s) - or every ';
                            primaryText += maintRes.merePrimary[s].period_Km_Toutes + '<br />';
                            
                            // Nouveau traitement
                            if (maintRes.merePrimary[s].libelle != null && maintRes.merePrimary[s].libelle!='')
                            {
                                Integer iLine = Math.mod(s,2);
                                
                                if (iLine!=null && iLine == 0)
                                    sTableLine += '<tr'+sStyle+'>';
                                else
                                    sTableLine += '<tr>';
                                    
                                sTableLine += '<td>' + maintRes.merePrimary[s].libelle + '</td>';
                                
                                if (maintRes.merePrimary[s].period_Km_Toutes != null)
                                    sTableLine += '<td>' + maintRes.merePrimary[s].period_Km_Toutes + '</td>';
                                else
                                    if (maintRes.merePrimary[s].period_Distan_Toutes != null)
                                        sTableLine += '<td>' + maintRes.merePrimary[s].period_Distan_Toutes + '</td>';
                                    else
                                        sTableLine += '<td>&nbsp;</td>';
                                    
                                sTableLine += '<td>&nbsp;</td>';
                                // Mettre les autres Valeurs pour chaque année
                                sTableLine += '';
                                
                                if (maintRes.merePrimary[s].period_Distan_Toutes != null && Integer.valueOf(maintRes.merePrimary[s].period_Distan_Toutes.trim()) != 0)
                                {
                                    for (Integer i = 1; i < 13; i++)
                                    {
                                        if (Math.mod(i, Integer.valueOf(maintRes.merePrimary[s].period_Distan_Toutes.trim())) == 0)
                                            sTableLine += '<td width="10">X</td>';
                                        else
                                            sTableLine += '<td width="10">&nbsp;</td>';
                                    }
                                }
                                /*
                                    sTableLine += '<td width="10"></td>'+
                                                  '<td width="10"></td>'+
                                                  
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>'+
                                                  '<td width="10">&nbsp;</td>';
                                */
                                sTableLine += '</tr>';
                            }
                        }
                    }
                    
                    maint.MaintPrimaryOperations = sTableLine;
                    //maint.MaintPrimaryOperations = primaryText;
                }       
            }          
        }
        catch (Exception CalloutException) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'General error (try failed)'));
        }  
    }  

    public void getWarrantyHistoryData() 
    {
        try 
        {   
            WS05_ApvGetDonIran1.ApvGetDonIran1Response  results = WarrantyHistoryDataSource.getWarrantyHistoryData(this);
          
            if (results == null)
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            else 
            {
                showDetail = 7;
            
                // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results not null'));
                WS05_ApvGetDonIran1.DonIran1InfoMsg InfoMsg;
                if (results.listDonIran1InfoMsg != null)
                    InfoMsg = results.listDonIran1InfoMsg[0];
                
                WS05_ApvGetDonIran1.DonIran1 donIran1Res = results.donIran1;
                
                if (InfoMsg != null) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + InfoMsg.code));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + InfoMsg.text));
                } 
                
                if (donIran1Res != null) 
                {
                    whList = new VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics[]{};
                    VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics wh;

                    
                    if (donIran1Res.sortie != null)
                    {

                        for (WS05_ApvGetDonIran1.DonIran1Sortie WSsortie : donIran1Res.sortie)
                        {
                            wh = new VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics();
                            wh.datOuvOr = WSsortie.datOuvOr;
                            wh.km = WSsortie.km;
                            wh.libRc = WSsortie.libRc;
                            wh.rc = WSsortie.rc;
                            wh.libInt = WSsortie.libInt;
                            wh.numOts = WSsortie.numOts;
                            wh.numInt = WSsortie.numInt;
                            wh.modulo = WSsortie.outmodulo;
                            wh.histoBim = WSsortie.BHistoBim;
                            wh.rc = WSsortie.rc;
                            whList.add(wh);
                        }
                    }
                }    
            }
        }
        catch (Exception CalloutException) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'General error (try failed)'));
        } 
    }
      
    
    WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response  results {get;set;}
    
    public void getWarrantyHistoryDetailData() 
    {
        try 
        { 
            system.debug('valeur numInt : ' + numInt);
            system.debug('valeur modulo : ' + modulo);
            system.debug('valeur histoBim : ' + histoBim);
            system.debug('valeur rc : ' + rc);
            
            WarrantyHistoryDetailDataSource.numInt = numInt;
            WarrantyHistoryDetailDataSource.modulo = modulo;
            WarrantyHistoryDetailDataSource.histoBim = histoBim;
            WarrantyHistoryDetailDataSource.rc = rc;
            
            results = WarrantyHistoryDetailDataSource.getWarrantyHistoryDetailData(this);
           
            if (results==null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            }                  
            else 
            {
                showDetail = 7;

                WS07_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg InfoMsg;
                
                if (results.listDonIran2InfoMsg != null)
                    InfoMsg = results.listDonIran2InfoMsg.get(0);
                    
                WS07_iran2BimIcmApvBserviceRenault.DonIran2 donIran2Res = results.donIran2;
                
                if (InfoMsg != null) 
                {
                    System.debug('###### InfoMsg.code '+InfoMsg.code+' InfoMsg.text '+InfoMsg.text);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + InfoMsg.code));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + InfoMsg.text));
                } 
                
                if (donIran2Res != null) 
                {
                    whd = new VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics();
                    whd.PandO = new VFC05_WarrantyHistoryProperties.PiecesandOeuvres[]{};
                         
                    whd.constClient = results.donIran2.constClient;
                    whd.diag = results.donIran2.diag;
                        
                    VFC05_WarrantyHistoryProperties.PiecesandOeuvres whdProp;
                    
                    if (results.donIran2 != null && results.donIran2.pieces != null)
                    {
                        for (WS07_iran2BimIcmApvBserviceRenault.DonIran2Pieces p : results.donIran2.pieces)
                        {
                            whdProp = new VFC05_WarrantyHistoryProperties.PiecesandOeuvres();
                            whdProp.Code = p.refePidms;
                            whdProp.Quantite = p.qtePi;
                            whdProp.Libelles = p.libePi;
                            whd.PandO.add(whdProp);
                        }
                    }
                       
                    if(results.donIran2 != null && results.donIran2.mainOeuvre != null)
                    {
                        for (WS07_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre o : results.donIran2.mainOeuvre)
                        {
                            whdProp = new VFC05_WarrantyHistoryProperties.PiecesandOeuvres();
                            whdProp.Code = o.opecode;
                            whdProp.Temps = o.tpsMo;
                            whdProp.Libelles = o.libOpecode;
                            whd.PandO.add(whdProp);
                        }
                    }
                }
            }
        } 
        catch (Exception CalloutException) 
        {
            System.debug('###### General error (try failed)');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'General error (try failed)'));
        } 
    }  
    

    public void getContractData() {
        try {     
            WS04_CustdataCrmBserviceRenault.GetCustDataResponse  results = ContractDataSource.getContractData(this);
         
            if (results==null) {
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'results null'));
    
            } else {
                showDetail = 4;
            
                 WS04_CustdataCrmBserviceRenault.Contract[] conRes = results.clientList[0].vehicleList[0].contractList;
                 WS04_CustdataCrmBserviceRenault.PersonnalInformation pi = results.clientList[0];
                 
                if (results.responseCode != null) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + results.responseCode));
              //      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'codeRetour : ' + otsInfoMsg.text));

                } 
                if (conRes != null) {
                        contracts = new VFC05_ContractProperties[]{};
                        VFC05_ContractProperties con;
                        for(WS04_CustdataCrmBserviceRenault.Contract WScon : conRes){
                            con = new VFC05_ContractProperties();
                            con.idContract = WScon.idContrat;
                            con.lastName = pi.lastName;
                            con.firstName = pi.Firstname;
                            con.address = pi.address.strNum + ' ' + pi.address.strName + ' ' + pi.address.zip + ' ' + pi.address.city;
                            con.idContractCard = WScon.idCard;
                            con.productLabel = WScon.productLabel;
                            con.country = pi.address.countryCode;
                            con.status = WScon.status;
                            con.kmMaxSubscription = Wscon.maxSubsKm;
                            contracts.add(con);
                        }
                }                
             
            }
    
        } 
        catch (Exception CalloutException) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'General error (try failed)'));
        } 
    }  
    
    //////////////////// Helper functions ////////////////////////
    private String findMaintLabel(WS03_fullRepApvBserviceRenault.PgmEntLibFix[] labelArray, String requestedNumber)
    {
        Integer i = 0;
        while (requestedNumber != labelArray.get(i).numLib) 
        {
            i++;
            if (i == labelArray.size()) return 'numLib entry not found.';
        }   
        return labelArray.get(i).libelleFix;
    }


        public void getArchives() { 

System.debug('getCountryInfo()>>>>>>>>>>>>>>>'+getCountryInfo());

           if(getCountryInfo()=='')
           {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'Please make sure CountryOfDelivery field is filled '));
           }

             
        try{
            
            WebserviceRcArchivageV2.caseId[] resultCaseList = CasesDataSource.getCaseIdList(vehicle.Name,getCountryInfo());
           
            
            if (resultCaseList == null){
                showDetail = 99;  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            }
            else {
                showDetail = 11;   
                
                System.debug('resultCaseList  retrieved for'+vehicle.Id+'is >>>>>>>>>>>>>'+resultCaseList);  
                ArchCasesList = new VFC05_ArchivesProperties.ArchivesCases[]{};
                VFC05_ArchivesProperties.ArchivesCases Arch;
                
                for(WebserviceRcArchivageV2.caseId myCaseId : resultCaseList)
                {
                    Arch = new VFC05_ArchivesProperties.ArchivesCases();
                    Arch.IdCase = myCaseId.caseId;
                    Arch.country= myCaseId.countryCode;
                    System.debug('CaseIdretrieved for'+vehicle.Id+'is >>>>>>>>>>>>>'+Arch.IdCase);  
                    ArchCasesList.add(Arch);
                }
            }
        }
        catch (System.CalloutException e){
            showDetail = 99;  
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'No cases found'));
            System.debug('Error message in the caselist method>>>>>>>>>>>'+e);
        }
    }
    
 public void getCaseDetails (){

        try{
            String IdCase = ApexPages.currentPage().getParameters().get('IdCase');
            String country = ApexPages.currentPage().getParameters().get('country');
           // String Output=CasesDataSource.getXmlDetails(getCountryInfo(),IdCase);  
            String Output=CasesDataSource.getXmlDetails(country,IdCase);  
            if (Output== null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            }
            else  {
                showDetail = 12;
                 VFC05ArchivageXMLParseEngine parsingEngine =new VFC05ArchivageXMLParseEngine ();
                    xmlData=parsingEngine.getXMLData(Output);

            }
  
        }
        catch (CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_CASEDETAILS));
        }
      

    }


    public void getAttachements() {
        try {
        
            IdCase = ApexPages.currentPage().getParameters().get('IdCase');
            String country = ApexPages.currentPage().getParameters().get('country');
            WebserviceRcArchivageV2.attachmentDetailsList[] resultAttachment = CasesDataSource.getAttachmentDetailsList(country, IdCase);
            if (resultAttachment == null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'results null'));
            }
            else {
                showDetail = 13;             
                ArchAttachmentsList = new VFC05_ArchivesProperties.ArchivesAttachments[]{};
                VFC05_ArchivesProperties.ArchivesAttachments Arch;
                              
                for(WebserviceRcArchivageV2.attachmentDetailsList myAttList : resultAttachment)
                {
                    Arch = new VFC05_ArchivesProperties.ArchivesAttachments();
                    Arch.attachmentName = myAttList.attachmentName;
                    Arch.extension = myAttList.extension;
                    Arch.createdDate = myAttList.createdDate;
                    ArchAttachmentsList.add(Arch);
                }
            }
        }
        catch (CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'No Attachments found'));
             }  
        
     
    }

    
   public void getAttachementData(){
     
        String vehId = System.currentPagereference().getParameters().get('id');
        String attachmentName = System.currentPagereference().getParameters().get('attachmentName');
        String resultURL = CasesDataSource.getAttachmentContent(vehId,IdCase,attachmentName);
        
         if (resultURL == null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'Results null'));
            }
            else {
                 showDetail = 14;     
                  attachmentURL=resultURL;
                }
    } 
   public Void getCaseDetailXML(){


    

    } 
    
    
    public PageReference OpenXMLPage()
     {
         return Page.OpenXMLAttachment;
     }


    public String getVin() 
    {
        if (Test.isRunningTest())
        {
            return 'VF1BGRG0633285766';
        }
        return vehicle.name;
    }
}