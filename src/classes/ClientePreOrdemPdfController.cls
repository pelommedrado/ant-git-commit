public class ClientePreOrdemPdfController {
	private String quoteId;
	private Opportunity opportunity;
    public VFC60_QuoteVO quoteVO { get;set; }
	public VFC61_QuoteDetailsVO quoteDetailsVO {get;set;}
	public VEH_Veh__c vehicle;
	private List<VFC83_QuoteLineItemVO> accessories;
	private String totalAccessories;
	public List<String> optional{get;set;}
	public Account dealer;
	public String veiculoId{get;set;}
	public String maskPriceVehicle{get;set;}
	public List<String> maskPriceAccessories{get;set;}
	public List<String> customerAdress;

    public String accePrice1Mask {get;set;}
    public String accePrice2Mask {get;set;}
    public String accePrice3Mask {get;set;}
    public String totalAccesPriceMask {get;set;}

    public ClientePreOrdemPdfController(ApexPages.StandardController controller) {
        this.quoteId = controller.getId();
    }

	public PageReference initialize() {
    	Double entry 			= 0.00;
    	Double priceUsed 		= 0.00;
    	Double amountFinanced 	= 0.00;
    	Double valueOfParcel 	= 0.00;
        Double accePrice1       = 0.00;
        Double accePrice2       = 0.00;
        Double accePrice3       = 0.00;
        Double totalAccesPrice  = 0.00;

        this.quoteVO = VFC63_QuoteBusinessDelegate.getInstance().getQuoteById(this.quoteId);

    	this.quoteDetailsVO = VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, '');

        this.opportunity = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(quoteDetailsVO.opportunityId);

        if(quoteDetailsVO == null || opportunity == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Orçamento ou Oportunidade não localizada(o)'));
            return null;
        }

        if(String.isNotEmpty(quoteDetailsVO.entryMask)) entry = Double.valueOf(quoteDetailsVO.entryMask);

        if(String.isNotEmpty(quoteDetailsVO.priceUsedVehicleMask)) priceUsed = Double.valueOf(quoteDetailsVO.priceUsedVehicleMask);

        if(String.isNotEmpty(quoteDetailsVO.amountFinancedMask)) amountFinanced = Double.valueOf(quoteDetailsVO.amountFinancedMask);

        if(String.isNotEmpty(quoteDetailsVO.valueOfParcelMask)) valueOfParcel = Double.valueOf(quoteDetailsVO.valueOfParcelMask);

        if(String.isNotEmpty(quoteDetailsVO.accePrice1)) accePrice1 = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice1);

        if(String.isNotEmpty(quoteDetailsVO.accePrice2)) accePrice2 = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice2);

        if(String.isNotEmpty(quoteDetailsVO.accePrice3)) accePrice3 = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice3);

        totalAccesPrice = accePrice1 + accePrice2 + accePrice3;
        totalAccesPriceMask = VFC69_Utility.priceFormats(totalAccesPrice);

        quoteDetailsVO.entryMask = VFC69_Utility.priceFormats(entry);
       	quoteDetailsVO.priceUsedVehicleMask = VFC69_Utility.priceFormats(priceUsed);
       	quoteDetailsVO.amountFinancedMask = VFC69_Utility.priceFormats(amountFinanced);
       	quoteDetailsVO.valueOfParcelMask = VFC69_Utility.priceFormats(valueOfParcel);
        accePrice1Mask = VFC69_Utility.priceFormats(accePrice1);
        accePrice2Mask = VFC69_Utility.priceFormats(accePrice2);
        accePrice3Mask = VFC69_Utility.priceFormats(accePrice3);

				if(String.isNotEmpty(quoteDetailsVO.totalPriceMask)) {
					Double totalPrice =
						VFC69_Utility.priceFormatToDouble(quoteDetailsVO.totalPriceMask);

					quoteDetailsVO.totalPriceMask = VFC69_Utility.priceFormats(totalPrice + totalAccesPrice);
				}

     	return null;
    }

    public Account getDealer() {
        Account dealer = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(opportunity.Dealer__c);

        if(dealer != null){
            return dealer;

        } else {
            return new Account();
        }
    }

    public List<String> getCustomerAdress() {
        Account auxCustomerAdress = VFC12_AccountDAO.getInstance().
            fetchAccountUsingAccountId(opportunity.AccountId);

        List<String> adress = new List<String>();

        if(String.isNotEmpty(auxCustomerAdress.ShippingStreet)){
            adress.add(auxCustomerAdress.ShippingStreet);
            adress.add(auxCustomerAdress.ShippingCity + ' ' + auxCustomerAdress.ShippingState+ ' ' + auxCustomerAdress.ShippingPostalCode + ' ' + auxCustomerAdress.ShippingCountry);

        } else {
            adress.add('');
            adress.add('');
        }

        return adress;
    }

    public List<VFC83_QuoteLineItemVO> getAccessories() {
        List<VFC83_QuoteLineItemVO> lstQuoteLineItem = new List<VFC83_QuoteLineItemVO>();
        maskPriceAccessories = new List<String>();
        integer i = 0;

        List<VFC83_QuoteLineItemVO> lis = quoteDetailsVO.lstQuoteLineItemVO;
        for(VFC83_QuoteLineItemVO quoteLineItem : lis) {

            if(quoteLineItem.item == 'Acessório' && i < 5) {
                lstQuoteLineItem.add(quoteLineItem);

                if(quoteLineItem.unitPrice != null){
                    maskPriceAccessories.add(VFC69_Utility.priceFormats(quoteLineItem.unitPrice));

                } else {
                    maskPriceAccessories.add(VFC69_Utility.priceFormats(0));
                }
            }
        }

        //garante que haverá 6 linhas em acessórios
        if(lstQuoteLineItem.size() < 6) {
            for(integer j=(lstQuoteLineItem.size()-1); j < 6; j++){
                lstQuoteLineItem.add(new VFC83_QuoteLineItemVO());
                maskPriceAccessories.add(VFC69_Utility.priceFormats(0));
            }
        }

     	return lstQuoteLineItem;
    }

    public String getTotalAccessories() {
        Double Totalprice = 0;
        List<VFC83_QuoteLineItemVO> lis = quoteDetailsVO.lstQuoteLineItemVO;

        for(VFC83_QuoteLineItemVO quoteLineItem : lis) {
            if(quoteLineItem.item == 'Acessório') {
                Totalprice += quoteLineItem.unitPrice;
            }
        }
        return VFC69_Utility.priceFormats(Totalprice);
    }

    public VEH_Veh__c getVehicle() {
        List<VEH_Veh__c> Vehicles = new List<VEH_Veh__c>();
        VEH_Veh__c Vehicle = new VEH_Veh__c();

        List<VFC83_QuoteLineItemVO> lis = quoteDetailsVO.lstQuoteLineItemVO;
        for(VFC83_QuoteLineItemVO quoteLineItem : lis) {
            if(String.isNotEmpty(quoteLineItem.vehicleId)) {
                Vehicles = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingVehicleRelationIDs(new List<Id> { quoteLineItem.vehicleId } );

                if(Vehicles.size() > 0) {
                    Vehicle = Vehicles.get(0);

                    maskPriceVehicle = VFC69_Utility.priceFormats(Vehicle.Price__c);

                    if(String.isNotEmpty(Vehicles.get(0).Optional__c) && Vehicles.get(0).Optional__c.length() > 98) {
                        Vehicles.get(0).Optional__c = Vehicles.get(0).Optional__c.substring(0, 98);
                    }

                    return Vehicles.get(0);
                }
            }
        }
        return Vehicle;
    }

}