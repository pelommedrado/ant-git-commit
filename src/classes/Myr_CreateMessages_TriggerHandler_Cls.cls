/****************************************************************************************************************************************
 Name    : Myr_CreateMessages_TriggerHandler_Cls
 Desc    : Notifications
 Approach: The methods filter the records, and if all the conditions are ok, send notifications synchronously or asynchronously
 Project : MyRenault
*****************************************************************************************************************************************
2015 Oct, Author=S. Ducamp       : Customer message "DEALER_ACCOUNT_CREATOR"
2015 Dec, Author=D. Veron (AtoS) : Patch because of issue : "Non-selective query against large object type (more than 100000 rows)..."
2015 Dec, Author=D. Veron (AtoS) : Patch on notification GAR_TRANSFERRED (status taken)
2015 Dec, Author=D. Veron (AtoS) : GAR_NO_CAR, ACCOUNT_MISSING_INFO, DEALER_NO_DEALER on login handling
2016 Feb, Author=D. Veron (AtoS) : Input_2 : Change on vehicle designation; Model__c replace ModelCode__c, Version__c replace VersionCode__c
2016 May, Author=C. Gueltas (Atos) : Patch on notification GAR_NO_CAR
2016 May, Author=C. Gueltas (Atos) : Change a condition on persemailaddress__c in the function manage_Dealer()
2016 Aug, Author=S. Ducamp(Atos) : F1415-US3571 Correction of the notifications to use the refresh after x days functionnality
2016 Aug, Author=D. Veron (AtoS) : R16.10 F1854-US3654 - DEALER_NETWORK_DELETED
2016 Nov, Author=D. Veron (AtoS) : R16.11 : Patch on channels
*****************************************************************************************************************************************/
 public without sharing class Myr_CreateMessages_TriggerHandler_Cls {
    private List<Account> m_Accounts; 
    private List<User> m_Users;
    private List<Myr_CustMessage_Cls> mMsgsToInsert;
    private CS04_MYR_Settings__c m_MyrSettings;
    @TestVisible private static Set<String> hasRun = new Set<String>();

	public static final String hasRun_DAA_Renault = 'Account.Notif_Dealer.Renault';
	public static final String hasRun_DAA_Dacia = 'Account.Notif_Dealer.Dacia';
    
    // @default constructor
    public Myr_CreateMessages_TriggerHandler_Cls() {
        mMsgsToInsert = new List<Myr_CustMessage_Cls>(); 
        m_MyrSettings = CS04_MYR_Settings__c.getInstance();
    }
    
	@future
    public static void Msg_Handler_Async(Id User_Id, String MyBrand) {
    	Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		Myr_CreateCustMessage_Cls.CreateMsgResponse response;
		
		M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler();
     	M_H.logRunId = Myr_ManageAccount_WS.class.getName() + DateTime.now().getTime();
      	M_H.idParentLog = INDUS_Logger_CLS.addGroupedLog (M_H.logRunId, null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateMessages_TriggerHandler_Cls', 'Msg_Handler_Async', '', 'Begin', 'Begin', INDUS_Logger_CLS.ErrorLevel.Info.name());
      	M_H.User_Id=User_Id;
      	M_H.Brand=MyBrand;
      	
      	if(LH_Check_Conditions(M_H)){
	        LH_Add_And_Insert(M_H);
	        response = Myr_CreateCustMessage_Cls.createMessages(M_H.listMessages);
      	}
    }
    
    //Check conditions
    public static boolean LH_Check_Conditions(Msg_Handler M_H) {
		list<User> List_User;
      	list<Contact> List_Contact;
      	list<Account> List_Account;
      	String SOQL_QUERY='';
		String Prefx='###Myr_CreateMessages_TriggerHandler_Cls.LH_Check_Conditions:';

      	System.Debug(Prefx+'BEGIN Connected user :Id=<' + M_H.User_Id + '>, ProfileName=<' + Myr_Users_Utilities_Class.getHeliosCommunityProfile().Name + '>');
      	try{
      		List_User = [select contactId, LanguageLocaleKey from user where id=:M_H.User_Id and Profile.Name=:Myr_Users_Utilities_Class.getHeliosCommunityProfile().Name];
        	if(list_User==null || list_User.size()!=1){
          		doLog(M_H, 'LH_Check_Conditions',null ,'Select_User','Connected user :<Id=' + Userinfo.getUserId() + ',ProfileName=' + Myr_Users_Utilities_Class.getHeliosCommunityProfile().Name + '> is not a community one.',INDUS_Logger_CLS.ErrorLevel.Info.name());
        	}else{
          		M_H.My_User=list_User[0];
          		System.debug(Prefx+'DEBUG 000 . 2' + M_H.My_User);
          		SOQL_QUERY='select Id, accountId from Contact where Id=\'' + M_H.My_User.contactId + '\'';
		        System.Debug(Prefx+'SOQL_QUERY_Contact = <' + SOQL_QUERY + '>');
		        List_Contact = Database.query(SOQL_QUERY);
		        if(List_Contact==null || List_Contact.size()!=1){
		            System.Debug(Prefx+'No contact associated. SOQL_QUERY=<'+ SOQL_QUERY + '>');
		        }else{
		            M_H.My_Contact=List_Contact[0];
		            SOQL_QUERY='select Id, Myr_Status__c, Myd_Status__c, FirstName, SecondSurname__c, LastName, BillingStreet, BillingCity, BillingPostalCode, PersMobPhone__c , HomePhone__c, MyR_Dealer_1_BIR__c, MyD_Dealer_1_BIR__c, MyR_Status_UpdateDate__c, MyD_Status_UpdateDate__c, country__c ';
		            if(Schema.SObjectType.Account.fields.getMap().keySet().contains('second_last_name__pc')) {
		              SOQL_QUERY+=' ,Second_Last_Name__pc ';
			        }
			        SOQL_QUERY += ' from Account where Id=\'' + M_H.My_Contact.accountId + '\'';
			        System.Debug(Prefx+'SOQL_QUERY_Account = <' + SOQL_QUERY + '>');
			        List_Account = Database.query(SOQL_QUERY);
			        if(List_Account==null || List_Account.size()!=1){
			        	System.Debug(Prefx+'No contact associated. SOQL_QUERY=<'+ SOQL_QUERY + '>');
			        }else{
			        	M_H.My_Account=List_Account[0];
			            System.debug(Prefx+'DEBUG 000 . 3');
			            return true;
			        }
				}
			}
		}catch (Exception e) {
	      System.debug(Prefx+' : Error=<' + e.getStackTraceString()+'>');
	    }
	    return false;
    }

	//Add and insert messages (call LH_Add_GAR_NO_CAR, LH_Add_ACCOUNT_MISSING_INFO)
    public static void LH_Add_And_Insert(Msg_Handler M_H) {
		LH_Add_And_Insert(M_H, system.today());
	}
   
	//Add and insert messages (call LH_Add_GAR_NO_CAR, LH_Add_ACCOUNT_MISSING_INFO)
    public static void LH_Add_And_Insert(Msg_Handler M_H, Date dateReference) {
		String accountId;

		accountId = M_H.My_Account.id;
		if (dateReference == null) { 
			dateReference = system.today();
		}

		GAR_NO_CAR(M_H, accountId, dateReference);

		ACCOUNT_MISSING_INFO(M_H, accountId, dateReference);
    }

	//GAR_NO_CAR
	public static void GAR_NO_CAR(Msg_Handler M_H, String accountId, Date dateReference) {
		String Channels;
		String Prefx;
		List<VRE_VehRel__c> List_Act_Relation;
		List<Customer_Message__c> List_Notif_Garage;
		
		Prefx='###Myr_CreateMessages_TriggerHandler_Cls.GAR_NO_CAR:';
		try{
			List_Act_Relation = [Select Id, account__r.country__c from VRE_VehRel__c where account__c = :accountId AND Status__c='Active' and My_Garage_Status__c='confirmed'];
			List_Notif_Garage = [SELECT 
									Id
									, Status__c
									, LastModifiedDate
								 from Customer_Message__c 
								 where 
									Template__r.Name like 'GAR_NO_CAR%'
									and Brand__c = :M_H.Brand 
									and account__c = :accountId
								];

			if(List_Act_Relation!=null && List_Act_Relation.size()>0) {	
				if(List_Notif_Garage!=null && List_Notif_Garage.size()>0){
					List_Notif_Garage[0].Status__c = 'read';
					update List_Notif_Garage[0];
				}
			} else {
    			if (   M_H.Brand.equalsIgnoreCase('Renault') 
					&& !String.isBlank(M_H.My_Account.Myr_Status__c) 
					&& M_H.My_Account.Myr_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Activated)
				){	
        			LH_Add_GAR_NO_CAR(M_H, Myr_CustMessage_Cls.Brand.Renault);
				}
				if (   M_H.Brand.equalsIgnoreCase('Dacia') 
					&& !String.isBlank(M_H.My_Account.Myd_Status__c) 
					&& M_H.My_Account.Myd_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Activated)
				){	
        			LH_Add_GAR_NO_CAR(M_H, Myr_CustMessage_Cls.Brand.Dacia); 
				}
			}
		}catch (Exception e) {
			System.debug(Prefx+'Error Stack=<'+e.getStackTraceString()+'>, Error Label=<'+e+'>');
		}	
	}	

	// ACCOUNT_MISSING_INFO
	public static void ACCOUNT_MISSING_INFO(Msg_Handler M_H, String accountId, Date dateReference) {
		Account acc;
		String Prefx;
		Boolean isMissingInfo;
		Integer nbUserLogin;
		Myr_CustMessage_Cls New_ACCOUNT_MISSING_INFO_Msg;
		List<Customer_Message__c> List_Notif_AccMissingInfo;

		Prefx='### Myr_CreateMessages_TriggerHandler_Cls.ACCOUNT_MISSING_INFO';

		try{	        
			//Do not send this message at the first connection
			nbUserLogin=0;
			
			if (Test.isRunningTest()) {
				acc = [Select Lastname FROM account Where Id = :accountId];
				if (acc.Lastname.equalsIgnoreCase('TESTACCMISSINGINFO')) {
					nbUserLogin = 2;
				}
			} else {
				nbUserLogin = database.countQuery('SELECT count() from LoginHistory where UserId = \'' + M_H.User_Id + '\'' );	
			}

			if (nbUserLogin>1) {

				List_Notif_AccMissingInfo = [SELECT Id
												  , Status__c
												  , LastModifiedDate
												  , Body__c
												  , Template__r.Channel__c
											 FROM Customer_Message__c 
											 WHERE 
												Template__r.Name like 'ACCOUNT_MISSING_INFO%' 
												and Brand__c = :M_H.Brand 
												and account__c = :accountId
											];
			
				New_ACCOUNT_MISSING_INFO_Msg = new Myr_CustMessage_Cls();
    			LH_Add_ACCOUNT_MISSING_INFO_Input2(M_H, New_ACCOUNT_MISSING_INFO_Msg);
				isMissingInfo = !String.isBlank(New_ACCOUNT_MISSING_INFO_Msg.input2);
				System.debug(Prefx+'New_ACCOUNT_MISSING_INFO_Msg.input2 =<' + New_ACCOUNT_MISSING_INFO_Msg.input2 + '>'); 
				System.debug(Prefx+'isAccMissingInfo =<' + isMissingInfo + '>');
			
				if(!isMissingInfo){
					if(List_Notif_AccMissingInfo!=null && List_Notif_AccMissingInfo.size()>0){	
						System.debug(Prefx+'CAS1 There is no missing info and existing notification');			
						List_Notif_AccMissingInfo[0].Status__c = 'read';
						update List_Notif_AccMissingInfo[0];
					}
				}else{
					System.debug(Prefx+'CAS2 there is missing info > the notification is replaced');			
    				if(    M_H.Brand.equalsIgnoreCase('Renault') 
						&& !String.isBlank(M_H.My_Account.Myr_Status__c) 
						&& M_H.My_Account.Myr_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Activated)
					){
						LH_Add_ACCOUNT_MISSING_INFO(M_H, Myr_CustMessage_Cls.Brand.Renault, New_ACCOUNT_MISSING_INFO_Msg);
					}
					if(    M_H.Brand.equalsIgnoreCase('Dacia') 
						&& !String.isBlank(M_H.My_Account.Myd_Status__c) 
						&& M_H.My_Account.Myd_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Activated)
					){
						LH_Add_ACCOUNT_MISSING_INFO(M_H, Myr_CustMessage_Cls.Brand.Dacia, New_ACCOUNT_MISSING_INFO_Msg);
					}
				}
			}
		}catch (Exception e) {
			System.debug(Prefx+'Error = ' + e.getStackTraceString() + ' # ' + e);	
		}
	}
	
	//Add GAR_NO_CAR notification (call LH_Messages)
    public static void LH_Add_GAR_NO_CAR(Msg_Handler M_H, Myr_CustMessage_Cls.Brand MyBrand) {
    	Myr_CustMessage_Cls New_Msg;

		New_Msg = new Myr_CustMessage_Cls();
		LH_Messages(M_H, 'GAR_NO_CAR', MyBrand, New_Msg);
		M_H.listMessages.add(New_Msg);
    }
    
    //Add ACCOUNT_MISSING_INFO notification (call LH_Messages)
    public static void LH_Add_ACCOUNT_MISSING_INFO(Msg_Handler M_H, Myr_CustMessage_Cls.Brand MyBrand, Myr_CustMessage_Cls New_MissingInfoMsg) {
      Myr_CustMessage_Cls New_Msg;
	 
	  New_Msg = new Myr_CustMessage_Cls();
      LH_Messages(M_H, system.Label.Myr_Notif_Account_Missing_Info, MyBrand, New_Msg);
	  New_Msg.input2 = New_MissingInfoMsg.input2;
      M_H.listMessages.add(New_Msg);
    }
  
	//Initialize input2 of notification "ACCOUNT_MISSING_INFO"
	public static void LH_Add_ACCOUNT_MISSING_INFO_Input2(Msg_Handler M_H, Myr_CustMessage_Cls New_Msg) {
    	List<Customer_Message_Templates__c> lcmt;
		List<Account> lac;
		Account The_Account;
		String My_Brand=M_H.Brand;
		String Versatile_Setting;
		String My_Name;
		String My_Label;
		String[] Field_Array = null;
		String[] Field_Array2 = null;
		String SOQL_QUERY;
		Boolean Is_Empty=true;
		String Prefx='####Myr_CreateMessages_TriggerHandler_Cls.LH_Add_ACCOUNT_MISSING_INFO_Input2:';
		
		System.debug(Prefx + 'BillingStreet=<' + M_H.My_Account.BillingStreet +'>, ' +  'BillingCity=<' + M_H.My_Account.BillingCity +'>, ' + 'BillingPostalCode=<' + M_H.My_Account.BillingPostalCode +'>, ' + 'PersMobPhone__c=<' + M_H.My_Account.PersMobPhone__c +'>, ' + 'HomePhone__c=<' + M_H.My_Account.HomePhone__c +'>');
		System.debug(Prefx + 'Get Versatile_setting__c with brand__c=<' +My_Brand +'>, ' +  'Language__c=<' + M_H.My_User.LanguageLocaleKey +'>, ' + 'country__c=<' + M_H.My_Account.country__c +'>');
		
		//Get the setting
		try{
			lcmt = [select Versatile_Setting__c from Customer_Message_Templates__c where Name like 'ACCOUNT_MISSING_INFO%' and brand__c=:My_Brand  and Language__c=:M_H.My_User.LanguageLocaleKey  and country__c=:M_H.My_Account.country__c];
		}catch (Exception e) {
			System.debug(Prefx + 'Error = ' + e.getStackTraceString());
			return;
		}
		if(lcmt.size()==1 && !String.isBlank(lcmt[0].Versatile_Setting__c)){
			Versatile_Setting=lcmt[0].Versatile_Setting__c.tolowercase();
		}else{
			Versatile_Setting ='BillingStreet|Billing Street;BillingCity|Billing City;BillingPostalCode|Billing Postal Code;PersMobPhone__c|Personal Mobile;HomePhone__c|Home Phone';
		}
		System.debug(Prefx + 'Versatile_Setting=<' + Versatile_Setting + '>'); 
		
		//Prepare soql query
		Field_Array= Versatile_Setting.split(';');
		SOQL_QUERY = 'select ';
		for(String Field_Couple: Field_Array){
			System.debug(Prefx + 'Field_Couple : ' + Field_Couple);
			Field_Array2 = Field_Couple.split('\\|');
			if(Schema.SObjectType.Account.fields.getMap().keySet().contains(Field_Array2[0].tolowercase())){
				System.debug(Prefx + 'Field_Array2[0] : <' + Field_Array2[0] + '>, Field_Array2[1] : <' + Field_Array2[1] + '>');
				if(!Is_Empty){
					SOQL_QUERY += ',';
				}
				SOQL_QUERY += Field_Array2[0] + ' ';
				Is_Empty=false;
			}
		}
		if(Is_Empty){
			System.debug(Prefx + 'No correct field. Issue on setting on Customer Message Template.Versatile Setting');
			return;
		}
		SOQL_QUERY += ' from Account where Id=\'' + M_H.My_Account.Id + '\'';
		System.debug(Prefx + 'SOQL_QUERY=<' + SOQL_QUERY + '>');
		try{
			lac  = Database.query(SOQL_QUERY);
		}catch (Exception e) {
			System.debug(Prefx + 'Error = ' + e.getStackTraceString());
			return;
		}

		//
		if(lac.size()!=1){
			System.debug(Prefx + 'lac not good : ');
		}else{
			New_Msg.input2='';
			The_Account=lac[0];
			Is_Empty=true;
			for(String Field_Couple: Field_Array){
				Field_Array2 = Field_Couple.split('\\|');
				System.debug(Prefx + 'Field_Array2[0] = ' + Field_Array2[0]);
				System.debug(Prefx + 'Field_Array2[1] = ' + Field_Array2[1]);
				if(String.isBlank(String.valueOf(M_H.My_Account.get(Field_Array2[0].tolowercase())))){
					if(!Is_Empty){
						New_Msg.input2 += ', ';
					}
					New_Msg.input2+=Field_Array2[1];
					Is_Empty=false;
				}
			}
		}
	}
    
    //Purge previous message and prepare a message (call LH_Add_Common_Actions)
    public static void LH_Messages(Msg_Handler M_H, String Type_Template, Myr_CustMessage_Cls.Brand MyBrand, Myr_CustMessage_Cls New_Msg) {
		System.debug('###Myr_CreateMessages_TriggerHandler_Cls.LH_Messages: M_H=<' + M_H + '>, Type_Template=<' + Type_Template + '>, MyBrand=<'+MyBrand+'>, New_Msg=<' +New_Msg+'>');
		//Set a messages
		LH_Add_Common_Actions(M_H, New_Msg);
		New_Msg.typeid = Type_Template;
		New_Msg.Brand=MyBrand;
	}
    
    //Common actions need for any message action
    public static void LH_Add_Common_Actions(Msg_Handler M_H, Myr_CustMessage_Cls New_Msg) {
		System.debug('###Myr_CreateMessages_TriggerHandler_Cls.LH_Add_Common_Actions: M_H=<' + M_H + '>, New_Msg=<' + New_Msg + '>');
		
		New_Msg.accountSfdcId = M_H.My_Account.Id;
		New_Msg.brand=Myr_CustMessage_Cls.BrandValueOf(M_H.Brand);
		New_Msg.input1 = M_H.My_Account.FirstName;
		if(!String.isBlank(M_H.My_Account.SecondSurname__c)){
        	New_Msg.input1 += ' ' + M_H.My_Account.SecondSurname__c;
      	}
      	New_Msg.input1 += ' ' + M_H.My_Account.LastName;
      	if(M_H.Second_Last_Name!=null & !String.isBlank(M_H.Second_Last_Name)){
       		New_Msg.input1 += ' ' + M_H.Second_Last_Name;
      	}
      	if(!String.isBlank(String.valueOf(M_H.My_Account.get('second_last_name__pc')))){
        	M_H.Second_Last_Name = String.valueOf(M_H.My_Account.get('second_last_name__pc'));
      	}
    }
    
    //Garage notifications (GAR_TRANSFERRED, GAR_CAR_AUTO_ADDED)
    public void manage_Garage_Notifications(list <VRE_VehRel__c> listRelations, Map <Id, VRE_VehRel__c> oldMap) {             
    	list<Relation_Extended> lst_GAR_TRANSFERRED;
		list<Relation_Extended> lst_GAR_CAR_AUTO_ADDED;
        list<Id> lst_Gar_Id;
        Myr_CustMessage_Cls msg;
		list<User> list_User;
        String Connected_User_UserName;
        String Connected_User_Profile;
        User Connected_User;
		Id persAccRTId;
		list<VRE_VehRel__c> lvre;
		List<Id> list_Acc_Id;

		lst_GAR_TRANSFERRED = new list<Relation_Extended>();
		lst_GAR_CAR_AUTO_ADDED = new list<Relation_Extended>();
        lst_Gar_Id = new list<Id>();

        for(VRE_VehRel__c myR : listRelations) {
            if (oldMap!=null){//After an Update
                if (!String.isBlank(myR.My_Garage_Status__c) && myR.My_Garage_Status__c.equalsIgnoreCase('taken') && !String.isBlank(oldMap.get(myR.Id).My_Garage_Status__c) && oldMap.get(myR.Id).My_Garage_Status__c.equalsIgnoreCase('confirmed')){
                    lst_GAR_TRANSFERRED.add(new Relation_Extended(myR, myR.Id, System.Label.Myr_Notif_Gar_Transferred)); 
                    System.Debug('### MyRenault - manage_Garage_Notifications : UPDATE - GAR_TRANSFERRED for relation : <' + myR.Id + '>');
                }
             }
              
	      	//After update or insert
	        if (!String.isBlank(myR.My_Garage_Status__c) && myR.My_Garage_Status__c.equalsIgnoreCase('confirmed')){
	            System.Debug('### MyRenault - manage_Garage_Notifications : INSERT. myR.My_Garage_Status__c : <' + myR.My_Garage_Status__c + '>');
	            lst_GAR_CAR_AUTO_ADDED.add(new Relation_Extended(myR, myR.Id, System.Label.Myr_Notif_Gar_Car_Auto_Added));
	        }
        }
        
        if (lst_GAR_TRANSFERRED.size()>0 ||  lst_GAR_CAR_AUTO_ADDED.size()>0){
            for(Relation_Extended r : lst_GAR_TRANSFERRED){
                lst_Gar_Id.add(r.sfdcId);   
            }
            for(Relation_Extended r : lst_GAR_CAR_AUTO_ADDED){
                lst_Gar_Id.add(r.sfdcId);   
            }
            //SDP: 18.01.2016 / HotFix Brazil. Do not insert notifications for Dealer, i.e. insert notifications only for Personal Account
			persAccRTId = Myr_Users_Utilities_Class.getPersAccRecordTypeId(); 
            lvre  = [select Id, account__r.Id, account__r.Firstname, account__r.Lastname, vin__r.Name, vin__r.VehicleBrand__c, vin__r.VehicleRegistrNbr__c, vin__r.Model__c, vin__r.Version__c from VRE_VehRel__c where Id in :lst_Gar_Id AND Account__r.RecordTypeId=:persAccRTId];
			//SDP: 18.01.2016 / HotFix Brazil.
            if(lvre.size()>0){
              	if( lst_GAR_CAR_AUTO_ADDED.size()>0){
                	list_Acc_Id = new List<Id>();  
                	for( VRE_VehRel__c r : lvre) {
                  		list_Acc_Id.add(r.account__r.Id);
                	} 
                	list_User  = [select username, accountId , Profile.Name from User where accountId in :list_Acc_Id or id = :Userinfo.getUserId()];
                	for( User uu : list_User) {
                  		if(uu.Id==Userinfo.getUserId()){
                    		Connected_User_UserName = uu.username;
                    		Connected_User_Profile = uu.Profile.Name;
                  		}
                	}
              	}
              	for( VRE_VehRel__c r : lvre) { 
                	for( Relation_Extended ri : lst_GAR_TRANSFERRED) {
                    	if(ri.sfdcId==r.Id){
                        	msg = new Myr_CustMessage_Cls();
                          	msg.input3 = String.valueOf(date.today());
                          	Set_Message(msg, r, ri);
                          	mMsgsToInsert.add(msg);
                      	}
                  	}
                  	for( Relation_Extended ri : lst_GAR_CAR_AUTO_ADDED) {
                      	if(ri.sfdcId==r.Id){
                        	for(User u :list_User){
                          		if(r.account__r.Id==u.accountId){
                            		if(u.username!=Connected_User_UserName && !Connected_User_Profile.equalsIgnoreCase('heliosDealer') && !Connected_User_Profile.equalsIgnoreCase('heliosTechnique')){
                                		msg = new Myr_CustMessage_Cls();
                                		Set_Message(msg, r, ri);
                                		mMsgsToInsert.add(msg);
                            		}
                          		}
                        	}
                      	}
                  	}
                }
              	insertMessages(mMsgsToInsert);
            }
        }    
    }
    
    public void Set_Message(Myr_CustMessage_Cls msg, VRE_VehRel__c r, Relation_Extended ri) {
		String Prfx;

		Prfx='###Myr_CreateMessages_TriggerHandler_Cls.Set_Message:';

		msg.typeid = ri.selecType;
		msg.vin = r.vin__r.Name;
		msg.accountSfdcId = r.Account__c;
		msg.brand=Myr_CustMessage_Cls.BrandValueOf(r.vin__r.VehicleBrand__c);
		msg.input1 = r.Account__r.Firstname + ' ' + r.Account__r.Lastname; 
		System.Debug(Prfx+'r.vin__r.Model__c :' + r.vin__r.Model__c + ', r.vin__r.Version__c=' + r.vin__r.Version__c + ', r.vin__r.VehicleRegistrNbr__c=<' + r.vin__r.VehicleRegistrNbr__c + '>');
		if(!String.isBlank(r.vin__r.Model__c)){
    		msg.input2 = r.vin__r.Model__c;
    		if(!String.isBlank(r.vin__r.Version__c)){
    			msg.input2 += ' ' + r.vin__r.Version__c;
    		}
    	}
    	if(String.isBlank(msg.input2)){
			msg.input2 = r.vin__r.VehicleRegistrNbr__c;
		}
		if(String.isBlank(msg.input2)){
			msg.input2 = r.vin__r.Name;
		}
		System.Debug(Prfx+'msg.accountSfdcId=<'+msg.accountSfdcId + '>, msg.input1 : <' + msg.input1 + '>, msg.input2 : <' + msg.input2 + '>, msg.input3 : <' + msg.input3 + '>');
    }

	//DEALER_AUTO_ADDED: forbid the generation of DEALER_AUTO_ADDED notification by filling the hasRun set
	public static void forbid_DEALER_AUTO_ADDED_Generation() {
		hasRun.add( hasRun_DAA_Renault );
		hasRun.add( hasRun_DAA_Dacia );
	}
       
    //"DEALER_AUTO_ADDED" : Called from Account_AfterUpdate_Trigger
    public void manage_Dealer(list <Account> listAccounts, Map <Id, Account> oldMap) {
        Boolean isUserHeliosCommunity;
		List<String> lstMsgs;

		isUserHeliosCommunity = true;

		try { isUserHeliosCommunity = UserInfo.getProfileId().equalsIgnoreCase(Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id);
		} catch (Exception e) {
        		//
      	}
	   
	    if (!isUserHeliosCommunity) {
			lstMsgs = new List<String>();  
	   
			for(Account myA : listAccounts) {
	    		if (!String.isBlank(myA.MyR_Dealer_1_BIR__c) && !myA.MyR_Dealer_1_BIR__c.equalsIgnoreCase(oldMap.get(myA.Id).MyR_Dealer_1_BIR__c) && ! hasRun.contains('Account.Notif_Dealer.Renault')){
	          		hasRun.add('Account.Notif_Dealer.Renault');
	          		lstMsgs.add(myA.Id + ';' + System.Label.Myr_Notif_Dealer_Auto_Added + ';' + myA.Firstname + ' ' + myA.Lastname + ';Renault;' + String.valueOf(myA.MyR_Dealer_1_BIR__c) + ';' + myA.country__c);
				}
				if (!String.isBlank(myA.MyD_Dealer_1_BIR__c) && !myA.MyD_Dealer_1_BIR__c.equalsIgnoreCase(oldMap.get(myA.Id).MyD_Dealer_1_BIR__c) && ! hasRun.contains('Account.Notif_Dealer.Dacia')){
					hasRun.add('Account.Notif_Dealer.Dacia');
					lstMsgs.add(myA.Id + ';' + System.Label.Myr_Notif_Dealer_Auto_Added + ';' + myA.Firstname + ' ' + myA.Lastname + ';Dacia;'   + String.valueOf(myA.MyD_Dealer_1_BIR__c) + ';' + myA.country__c);
				}
			}
			if(lstMsgs.size()>0){
				//SDP: (23.03.2016) adpatation due to asynchronous personal data callout
				if ( System.isFuture() ) {
					//Already in future method, so notification is generated synchronously
					Notification_Sending( lstMsgs );
				} else {
					Async_Notification_Sending(lstMsgs);
				}
			}
		}
    }

	@future
    public static void Async_Notification_Sending(List<String> lstMsgs) {
		Notification_Sending( lstMsgs );
	}
    
    //"DEALER_AUTO_ADDED" : Asynchronous sending
    private static void Notification_Sending(List<String> lstMsgs) {
		List<Myr_CustMessage_Cls> lstMsgsToInsert = new List<Myr_CustMessage_Cls>();
		Myr_CustMessage_Cls m;
		List<String> Field_Values;
		String str;
		String str2;
		String SOQL_QUERY;
		List<String> lstBirID;
		String Prefx;
		List<Account> lstAccount;
		Country_Info__c countryInf;
		Myr_CreateCustMessage_Cls.CreateMsgResponse Resp;

		Prefx='###Myr_CreateMessages_TriggerHandler_Cls.Notification_Sending:';
      
		//Compile all the BIR IDs on a single list
		lstBirID = new List<String>();
		for(String msg : lstMsgs){
        	Field_Values = msg.split(';', 6);
        	if(!String.isBlank(Field_Values[4])){
          		lstBirID.add(Field_Values[4]);
        	}
      	}

      	if (lstBirID.size()>0){
        	//Query to get the favourite dealer's names
      		try{
        		lstAccount= [select 
									IDBIR__c
									, Name 
							from Account 
							where 
									IDBIR__c in :lstBirID 
									and recordtype.DeveloperName=:Myr_Users_Utilities_Class.getDealerRecordType()
							];
      		}catch (Exception e) {
        		//
      		}
      		
          	for(String msg : lstMsgs){
	            m = new Myr_CustMessage_Cls();
	            Field_Values = msg.split(';', 6);
	            str='-';
	            
	            if (lstAccount.size()>0){
	            	for(Account a : lstAccount){
	                	if(a.IDBIR__c.equalsIgnoreCase(Field_Values[4])){
	                  		str=a.Name;
	                	}
	              	}
	            }
	            System.debug(Prefx+'Field_Values[0]=<' +Field_Values[0]+ '>, Field_Values[1]=<' + Field_Values[1] + '>, Field_Values[2]=<' + Field_Values[2] + ', Field_Values[3]=<' + Field_Values[3] + '>, Field_Values[4]=<'+ Field_Values[4]+'>, Field_Values[5]=<'+ Field_Values[5]);
				m.accountSfdcId = Field_Values[0]; 
				m.typeid = Field_Values[1]; 
				m.input1 = Field_Values[2];
				m.input2 = str;
				countryInf = Country_Info__c.getInstance(Field_Values[5]);
				if( null != countryInf ) {
					m.language=countryInf.LanguageLocaleCode__c;
					System.debug(Prefx+'DEALER_AUTO_ADDED language=' + m.language);
				}
	        	m.country=Field_Values[5];
				m.brand = Myr_CustMessage_Cls.BrandValueOf(Field_Values[3]);
				
        		//Delete of equivalent notifications
		        try{
		        	SOQL_QUERY='select Id from Customer_Message__c  where Account__c=\'' + m.accountSfdcId + '\' and Template__r.Name like \'' + m.typeid + '%\' and Brand__c=\'' + Field_Values[3] + '\'';
		          	System.debug(SOQL_QUERY);
		           	Database.Delete(SOQL_QUERY);
		        }catch (Exception e) {
					//
		        }
		        lstMsgsToInsert.add(m);
			}
      	}
		//Send messages
		System.debug(Prefx+'m.accountSfdcId=<' +m.accountSfdcId+ '>, m.typeid=<' + m.typeid + '>, m.input1=<' + m.input1 + ', m.brand=<' + m.brand + '>, m.input2=<'+m.input2 +'>');
		Resp = Myr_CreateCustMessage_Cls.createMessages(lstMsgsToInsert);
	}
     
	//"DEALER_ACCOUNT_CREATOR" (called from Myr_ManageAccount_WS or Myr_CreateAccount_WS)
	@future(callout=true)
	public static void manage_DEALER_ACCOUNT_CREATOR(Id accountId, String AccountBrand, String Country, String subscriberDealerID, String Account_Names) { 
		Country_Info__c myC;
		List<Account> listDeale;
	    String dealerAddress;
	    Account myD;
		List<Myr_CustMessage_Cls> lstMsgsToInsert;
		Myr_CustMessage_Cls m;
		String Prefx;
		Myr_CreateCustMessage_Cls.CreateMsgResponse Resp;
		
		Prefx='###Myr_CreateMessages_TriggerHandler_Cls.manage_DEALER_ACCOUNT_CREATOR:';

		System.debug(Prefx+'BEGIN : accountId=<'+accountId + '>, AccountBrand = <'+AccountBrand +'>, Country=<' + Country + '>, subscriberDealerID=<' + subscriberDealerID+'>');
		
       	try{
         	myC = [select LanguageLocaleCode__c from Country_Info__c where Name=:country];
       	}catch (Exception e) {
       		System.debug(Prefx+'ERROR : country : <'+ country + '> not set into Country_Info__c'); 
         	return;
       	}
    	if(myC==null || String.isBlank(myC.LanguageLocaleCode__c)){
      		System.debug(Prefx+'ERROR : LanguageLocaleCode of "Country Info" for country=<' + Country +  '> is empty');
    	}else{
	        if(!String.isBlank(subscriberDealerID)){
	        	listDeale = [select name, billingstreet, BillingPostalCode, billingcity, BillingCountry from Account where IDBIR__c=:subscriberDealerID];
	          	if(listDeale!=null && listDeale.size()==1){
	            	myD=listDeale[0];
	          		if(!String.isBlank(myD.billingstreet)){
	            		dealerAddress=' ' + myD.billingstreet;
	            		if(!String.isBlank(myD.BillingPostalCode)){
		            		dealerAddress+=' ' + myD.BillingPostalCode;
						}
						if(!String.isBlank(myD.billingcity)){
		            		dealerAddress+=' ' + myD.billingcity;
						}
						if(!String.isBlank(myD.BillingCountry)){
		            		dealerAddress+=' ' + myD.BillingCountry;
						}
	          		}
				}
	        }
	
	        lstMsgsToInsert = new List<Myr_CustMessage_Cls>();
	        m = new Myr_CustMessage_Cls();
	        m.accountSfdcId = accountId;
	        m.typeid = 'DEALER_ACCOUNT_CREATOR';
	        m.input1 = (myD == null) ? '' : myD.Name; 
	        if(!String.isBlank(dealerAddress)){
	          	m.input2 = dealerAddress;
	        }else{
	        	m.input2 = '<dealer not found>';
	        }
	        m.brand = Myr_CustMessage_Cls.BrandValueOf(AccountBrand);
	        m.language=myC.LanguageLocaleCode__c;
	        m.country=Country; 
	      	System.debug('m.accountSfdcId=<' +m.accountSfdcId+ '>, m.typeid=<' + m.typeid + '>, m.input1=<' + m.input1 + '>, m.brand=<' + m.brand + '>, m.input2=<'+m.input2 +'>');
	      	lstMsgsToInsert.add(m);
	        Resp = Myr_CreateCustMessage_Cls.createMessages(lstMsgsToInsert);
	        if(myD!=null){
	        	System.debug(Prefx+'END : input1=<'+myD.Name + ' ' + myD.billingstreet + ' ' + myD.billingcity + '>, language=<' + myC.LanguageLocaleCode__c +  '>');
	        }else{
	        	System.debug(Prefx+'ERROR : input1=<<dealer not found>>, language=<' + myC.LanguageLocaleCode__c +  '>');
	        }
    	}
    }
	
	//insert the messages (Potential error messages are pushed into "Logger")
    public void insertMessages(List<Myr_CustMessage_Cls> listMessages) {
        Myr_CreateCustMessage_Cls.CreateMsgResponse Resp;
		
		Resp = Myr_CreateCustMessage_Cls.createMessages(listMessages);
    }
    
    //Inner class
    public class Relation_Extended {
        private VRE_VehRel__c relation;
        private Id associatedId;
        private String selectiontype;
        private String Type_Id;
        private User The_User;
        
        //Constructor
        public Relation_Extended (VRE_VehRel__c myRelation, Id associatedId, String selectiontype){
            this.relation=myRelation;
            this.associatedId=associatedId;
            this.selectiontype=selectiontype;
        }
        
        public VRE_VehRel__c rv {
            get { return this.relation;}
        }
        public User Rel_User {
            get { return this.The_User;}
        }
        public Id sfdcId {
            get { return this.associatedId;}
        }
        public String selecType {
            get { return this.selectiontype;}
        }
    }

    //Inner class
	public class Msg_Handler {
		public User My_User;
		public VRE_VehRel__c My_Relation;
		public Contact My_Contact;
		public Account My_Account;
		public Account My_Dealer;
		public String Brand;
		public String Second_Last_Name;
		public Myr_CustMessage_Cls msg;
		public List<Myr_CustMessage_Cls> listMessages = new List<Myr_CustMessage_Cls>();
		public Myr_CustMessage_Cls New_Msg;
		public Id idParentLog;
		public String logRunId;
		public Id User_Id;
		public String DND_Type;
		public String Dealer_Bir_Id;
		public Boolean Dnd_Generated;

		public Msg_Handler(Account a, String My_Brand){
			this.My_Account=a;
			this.Brand=My_Brand;
			this.Dnd_Generated=false;
		}

		public Msg_Handler(){
			//
		}
    }

    private static void doLog(Msg_Handler M_H, String Function, String Record, String ExceptType, String Message, String error) {
		Id id;
		
		id=INDUS_Logger_CLS.addGroupedLog (M_H.logRunId, M_H.idParentLog, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateMessages_TriggerHandler', Function, Record, ExceptType, Message, error);
	}
}