public class SelectOfferDealerController {
    
    //public List<String> regiao { get;set; }
    
    public List<SelectOption> regiaoList { get;set; }
    public List<SelectOption> estadoList { get;set; }
    public List<SelectOption> cidadeList { get;set; }
    public List<SelectOption> dealerList { get;set; }
    
    public List<SelectOption> regiaoSelectList 	{ get;set; }
    public List<SelectOption> estadoSelectList 	{ get;set; }
    public List<SelectOption> cidadeSelectList 	{ get;set; }
    public List<SelectOption> dealerSelectList 	{ get;set; }
    
    private Set<String> regSelect;
    private Set<String> estSelect;
    private Set<String> cidSelect;
    
    private Map<String, Id> dealerMap;
    private Map<Id, String> accMapAll;
    
    private Set<String> regAllList;
    private Set<String> estAllList;
    private Set<String> cidAllList;
    
    private Map<String, Id> accAllMap;
    
    private Offer__c callOffer; 
    
    public SelectOfferDealerController(ApexPages.StandardController controller) { 
        init();
    }
    
    public void init() {
        
        this.regSelect = new Set<String>();
        this.estSelect = new Set<String>();
        this.cidSelect = new Set<String>();
        this.dealerMap = new Map<String, Id>();
        Map<String, Id> dealerSelectMap = new Map<String, Id>();
        
        this.regAllList = new Set<String>();
        this.estAllList = new Set<String>();
        this.cidAllList = new Set<String>();
        this.accAllMap = new Map<String, Id>();
        
        this.regAllList.add('R1');
        this.regAllList.add('R2');
        this.regAllList.add('R3');
        this.regAllList.add('R4');
        this.regAllList.add('R5');
        this.regAllList.add('R6');
        
        String idChamada = ApexPages.currentPage().getParameters().get('id');
        List<Account> dealerAll = /*new List<Account>();*/dealerAllOffer(idChamada);
        
        if(!dealerAll.isEmpty()) {
            
            for(Account acc : dealerAll) {
                if(acc.NameZone__c != NULL)
                	regSelect.add(acc.NameZone__c);
                estSelect.add(acc.ShippingState);
                cidSelect.add(acc.ShippingCity);
                dealerSelectMap.put(acc.Name, acc.Id);
            }
            
            List<Account> accAll = [
                SELECT Id, Name, NameZone__c, ShippingState, ShippingCity 
                FROM Account 
                WHERE Active_PV__c = true AND NameZone__c IN: regSelect
            ];
            
            for(Account ac : accAll) {
                estAllList.add(ac.ShippingState);
                cidAllList.add(ac.ShippingCity);
                
                accAllMap.put(ac.Name, ac.Id);
            }
        }
        
        loadRegiao(regSelect, regAllList);
        loadEstado(estSelect, estAllList);
        loadCidade(cidSelect, cidAllList);
        loadDealer(dealerSelectMap, accAllMap);
        
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.INFO, 'Selecione as concessionárias referentes a chamada de oferta'));
    }
    
    private List<Account> dealerAllOffer(String idChamada) {
        this.callOffer = [
            SELECT Id, Name FROM Offer__c 
            WHERE Id = :idChamada
        ];
        
        List<Dealer_Offer__c> dealerOffList = [
            SELECT Dealer__c FROM Dealer_Offer__c 
            WHERE Offer__c =: callOffer.Id     
        ];
        
        List<Id> dealerIdList = new List<Id>(); 
        for(Dealer_Offer__c dOff: dealerOffList) {
            dealerIdList.add(dOff.Dealer__c);
        }
        
        return [
            SELECT Id, Name, NameZone__c, ShippingState, ShippingCity   
            FROM Account 
            WHERE Active_PV__c = true AND  Id IN:dealerIdList
            ORDER BY Name, NameZone__c, ShippingState, ShippingCity
        ];
    }
    
    private void loadRegiao(Set<String> regSelect, Set<String> regAllList) {
        List<String> valuesList = convertSet(regSelect);
        this.regiaoSelectList = new List<SelectOption>();
        for(String stg : valuesList) {
            this.regiaoSelectList.add(new SelectOption(stg, stg));
            regAllList.remove(stg);
        }
        
        valuesList = convertSet(regAllList);
        this.regiaoList = new List<SelectOption>();
        for(String stg : valuesList) {
            regiaoList.add(new SelectOption(stg, stg));
        }
    }
    
    private void loadEstado(Set<String> estSelect, Set<String> estAllList) {
        List<String> valuesList = convertSet(estSelect);
        this.estadoSelectList = new List<SelectOption>();
        for(String stg : valuesList) {
            this.estadoSelectList.add(new SelectOption(stg, stg));
            estAllList.remove(stg);
        }
        
        valuesList = convertSet(estAllList);
        this.estadoList = new List<SelectOption>();
        for(String stg : valuesList) {
            estadoList.add(new SelectOption(stg, stg));
        }
    }
    
    private List<String> convertSet(Set<String> valuesSet) {
        List<String> valuesList = new List<String>();
        for(String v : valuesSet) {
            valuesList.add(v);
        }
        valuesList.sort();
        return valuesList;
    }
    
    private void loadCidade(Set<String> cidSelect, Set<String> cidAllList) {
        List<String> valuesList = convertSet(cidSelect);
        this.cidadeSelectList = new List<SelectOption>();
        for(String stg : valuesList) {
            this.cidadeSelectList.add(new SelectOption(stg, stg));
            cidAllList.remove(stg);
        }
        
        valuesList = convertSet(cidAllList);
        this.cidadeList = new List<SelectOption>();
        for(String stg : valuesList) {
            cidadeList.add(new SelectOption(stg, stg));
        }
    }
    
    private void loadDealer(Map<String, Id> dealerMap, Map<String, Id> deaAllList) {
        List<String> deal = convertSet(dealerMap.keySet());
        
        this.accMapAll = new Map<Id, String>();
        this.dealerSelectList = new List<SelectOption>();
        
        for(String key : deal) {
            Id accID = dealerMap.get(key);
            
            this.dealerSelectList.add(new SelectOption(accID, key));
            deaAllList.remove(key);
            
            this.accMapAll.put(accID, key);
        }
        
        List<String> dealAll = convertSet(deaAllList.keySet());
        
        this.dealerList = new List<SelectOption>(); 
        for(String key : dealAll) {
            Id acci = deaAllList.get(key);
            dealerList.add(new SelectOption(acci, key));
            
            this.accMapAll.put(acci, key);
        }
    }
    
    public PageReference atualizarRegiao() {
        
        String param = ApexPages.currentPage().getParameters().get('param');
        String action = ApexPages.currentPage().getParameters().get('action');
        
        List<String> regL = param.split(',');
        
        System.debug('Param: ' + param);
        if(!regL.isEmpty()) {
            
            for(String st : regL) {
                
                if(action.equals('+')) {
                    regSelect.add(st);     
                    regAllList.remove(st);
                    
                } else if(action.equals('-')) {
                    regSelect.remove(st);
                    regAllList.add(st);
                    
                    estAllList.clear();
                    cidAllList.clear();
                }
            }
            
            List<Account> accList = [
                SELECT Id, Name, NameZone__c, ShippingState, ShippingCity   
                FROM Account 
                WHERE NameZone__c IN: regSelect AND Active_PV__c = true 
                AND ShippingState NOT IN:(estAllList) AND ShippingCity NOT IN:(cidAllList)
            ];
            
            estSelect.clear();
            cidSelect.clear();
            this.dealerMap = new Map<String, Id>();
            this.accAllMap = new Map<String, Id>();  
            
            for(Account ac : accList) {
                estSelect.add(ac.ShippingState);
                cidSelect.add(ac.ShippingCity);
                
                dealerMap.put(ac.Name, ac.Id);
                accAllMap.put(ac.Name, ac.Id);
            }   
            
            loadRegiao(regSelect, regAllList);
            loadEstado(estSelect, estAllList);
            loadCidade(cidSelect, cidAllList);
            loadDealer(dealerMap, accAllMap);
        }
        
        return null;
    }
    
    public PageReference atualizarEstado() {
        
        String param = ApexPages.currentPage().getParameters().get('param');
        String action = ApexPages.currentPage().getParameters().get('action');
        
        List<String> regL = param.split(',');
        
        System.debug('Param: ' + param);
        
        if(!regL.isEmpty()) {
            
            for(String st : regL) {
                
                if(action.equals('+')) {
                    estSelect.add(st);     
                    estAllList.remove(st);
                    
                } else if(action.equals('-')) {
                    estSelect.remove(st);
                    estAllList.add(st);
                    
                    cidAllList.clear();
                }
            }
            
            List<Account> accList = [
                SELECT Id, Name, NameZone__c, ShippingState, ShippingCity   
                FROM Account 
                WHERE NameZone__c IN: regSelect AND 
                ShippingState IN:estSelect AND Active_PV__c = true 
                AND ShippingState NOT IN:(estAllList) AND ShippingCity NOT IN:(cidAllList)
            ];
            
            cidSelect.clear();
            this.dealerMap = new Map<String, Id>();
            this.accAllMap = new Map<String, Id>(); 
            
            for(Account ac : accList) {
                cidSelect.add(ac.ShippingCity);
                dealerMap.put(ac.Name, ac.Id);
                accAllMap.put(ac.Name, ac.Id);
            }
            
            loadEstado(estSelect, estAllList);
            loadCidade(cidSelect, cidAllList);
            loadDealer(dealerMap, accAllMap);
        }
        
        return null;
    }
    
    public PageReference atualizarCidade() {
        
        String param = ApexPages.currentPage().getParameters().get('param');
        String action = ApexPages.currentPage().getParameters().get('action');
        
        List<String> regL = param.split(',');
        
        System.debug('Param: ' + param);
        
        if(!regL.isEmpty()) {
            
            for(String st : regL) {
                
                if(action.equals('+')) {
                    cidSelect.add(st);     
                    
                } else if(action.equals('-')) {
                    cidSelect.remove(st);
                    cidAllList.add(st);
                }
            }
            
            List<Account> accList = [
                SELECT Id, Name, NameZone__c, ShippingState, ShippingCity   
                FROM Account 
                WHERE NameZone__c IN: regSelect AND 
                ShippingState IN:estSelect AND ShippingCity IN:cidSelect AND Active_PV__c = true 
            ];
            
            this.dealerMap = new Map<String, Id>();
            this.accAllMap = new Map<String, Id>();
            
            if(accList.isEmpty()) {
                cidSelect.clear();
                
            } else {
                for(Account ac : accList) {
                    dealerMap.put(ac.Name, ac.Id);
                    accAllMap.put(ac.Name, ac.Id);
                }     
            }
            
            loadCidade(cidSelect, cidAllList);
            loadDealer(dealerMap, accAllMap);
        }
        
        return null;
    }
    
    public PageReference atualizarDealer() {
        String param = ApexPages.currentPage().getParameters().get('param');
        String action = ApexPages.currentPage().getParameters().get('action');
        List<String> regL = param.split(',');
        
        if(regL.isEmpty()) {
            return null;
        }
        
        for(String st : regL) {
            
            if(action.equals('+')) {
                String name = accMapAll.get(st);
                
                dealerMap.put(name, st);
                accAllMap.remove(name);
                
            } else if(action.equals('-')) {
                String name = accMapAll.get(st);
                
                dealerMap.remove(name);
                accAllMap.put(name, st);
            }
        }
        
        loadDealer(dealerMap, accAllMap);
        
        return null;
    }
    
    public PageReference salvar() {
        
        List<Id> accIdSet = dealerMap.values();
        
        /*if(accIdSet.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.WARNING, 'Selecione uma concessionária'));
            return null;
        }*/
        
        Savepoint sp = Database.setSavepoint();
        try {
            
            Offer__c oferta = new Offer__c();
            oferta.Id = ApexPages.currentPage().getParameters().get('id');
            oferta.PV_RegionaisSelected__c = '{';
            for(SelectOption option: regiaoSelectList)
                oferta.PV_RegionaisSelected__c += option.getValue() + ',';
            oferta.PV_RegionaisSelected__c = oferta.PV_RegionaisSelected__c.substring( 0,oferta.PV_RegionaisSelected__c.length() - 1 );
            oferta.PV_RegionaisSelected__c += '}';
            
            Update oferta;
            
            List<Dealer_Offer__c> offDelList = [
                SELECT Offer__c, Dealer__c FROM Dealer_Offer__c 
                WHERE Offer__c =:callOffer.Id
            ];
            
            DELETE offDelList;
            
            List<Dealer_Offer__c> offInsList = new List<Dealer_Offer__c>();
            
            for(Id ac : accIdSet) {
                Dealer_Offer__c newDealer = new Dealer_Offer__c();
                newDealer.Dealer__c = ac;
                newDealer.Offer__c = callOffer.Id;

                offInsList.add(newDealer);
            }

            INSERT offInsList;
            
            //PARA DEPURAÇÃO DE ERROS
            /*Database.SaveResult [] sr = Database.insert(offInsList, false); //INSERT offInsList;
            System.debug('****** INSERT SAVERESULT: ' + sr);

            for(Integer i=0 ; i < sr.size(); i++){
            if(!sr[i].isSuccess()){
                System.debug('***ERROS SAVING: ' + sr[i].getErrors());
                System.debug('***RECORD WITH ERROR: ' +  offInsList[i]);
            }
            }*/
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.CONFIRM, 'Alterações salvas com sucesso'));
            
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(ex)));
            Database.rollback( sp );
        }
        
        return null;
    }
}