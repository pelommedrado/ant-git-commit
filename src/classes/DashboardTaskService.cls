public class DashboardTaskService {

    public Date data = Datetime.now().date();
    public String paramSubject { get;set; }
    public String paramStatus { get;set; }
    public String dataIniString { get;set; }
    public String dataFimString { get;set; }

    public List<String> statusNotInList = new List<String> { 'Completed', 'Canceled'};
    public List<Id> allSellers { get;set; }
    public List<Opportunity> oppList { get;set; }

    public DashboardTaskService() {
    }

    public List<Task> buscarTaskList() {
      System.debug('buscarTaskList()');

      final Datetime dataIniDate = createDatetimeStart();
      final Datetime dataFimDate = createDatetimeEnd();
      final Datetime dataAgo = Datetime.now();

      final List<String> paramSubjectList = DashboardUtil.paramFilter(paramSubject);
      final String query = queryBuildTask(paramSubjectList);
      System.debug('paramSubject: ' + paramSubject);
      System.debug('paramStatus: ' + paramStatus);
      System.debug('Query Task: ' + query);

      final List<Task> taskList = Database.query(query);
      return taskList;
    }

    public List<Event> buscarEventList() {
      System.debug('buscarEventList()');

      final Datetime dataIniDate = createDatetimeStart();
      final Datetime dataFimDate = createDatetimeEnd();
      final Datetime dataAgo = Datetime.now();

      final List<String> paramSubjectList = DashboardUtil.paramFilter(paramSubject);
      final String query = queryBuildEvent(paramSubjectList);
      System.debug('paramSubject: ' + paramSubject);
      System.debug('paramStatus: ' + paramStatus);
      System.debug('Query Event: ' + query);

      final List<Event> eventList = Database.query(query);
      return eventList;
    }

    private String queryBuildTask(List<String> paramSubjectList) {
        String queryWhereStatus = queryBuildTaskStatus();
        if(String.isEmpty(queryWhereStatus)) {
          if(isFilterRage()) {
            queryWhereStatus = ' AND ActivityDatetime__c >=: dataIniDate'
              + ' AND ActivityDatetime__c <=: dataFimDate ';
          }
        }
        return 'SELECT Id, Status, OwnerId, Owner.Name, Account.Name, '
              + 'toLabel(Subject), Description, ActivityDatetime__c, WhatId, WhoId '
              + 'FROM Task WHERE WhatId IN: oppList AND OwnerId IN:allSellers AND ActivityDatetime__c != null'
              + ' AND Status NOT IN:statusNotInList '
              + (paramSubjectList.isEmpty() ? '' : 'AND Subject IN: paramSubjectList ')
              + queryWhereStatus;
    }

    private String queryBuildTaskStatus() {
        if(String.isEmpty(paramStatus)) {
          return '';
        }

        if(paramStatus == '5') {
          return ' AND ActivityDatetime__c <=: dataAgo';
        } else if(paramStatus == '6') {
          return ' AND ActivityDatetime__c >=: dataIniDate'
               + ' AND ActivityDatetime__c <=: dataFimDate';
        }
        return '';
    }

    private String queryBuildEvent(List<String> paramSubjectList) {
        String queryWhereStatus = queryBuildEventStatus();
        if(String.isEmpty(queryWhereStatus)) {
          if(isFilterRage()) {
            queryWhereStatus = ' AND ActivityDatetime__c >=: dataIniDate'
              + ' AND ActivityDatetime__c <=: dataFimDate ';
          }
        }
        return 'SELECT Id, Account.Name, OwnerId, Owner.Name, toLabel(Subject),'
            + ' Description, Status__c, ActivityDatetime__c, WhatId, WhoId'
            + ' FROM Event WHERE WhatId IN: oppList AND OwnerId IN: allSellers AND ActivityDatetime__c != null'
            + ' AND Status__c NOT IN:statusNotInList '
            + (paramSubjectList.isEmpty() ? '' : 'AND Subject IN: paramSubjectList ')
            + queryWhereStatus;
    }

    private String queryBuildEventStatus() {
      if(String.isEmpty(paramStatus)) {
        return '';
      }
      if(paramStatus == '5') {
        return ' AND ActivityDatetime__c >=: dataIniDate AND ActivityDatetime__c <=: dataAgo';
      } else if(paramStatus == '6') {
        return ' AND ActivityDatetime__c >=: dataIniDate'
             + ' AND ActivityDatetime__c <=: dataFimDate';
      }
      return '';
    }

    private Datetime createDatetimeStart() {
      if(isFilterRage()) {
          return DataUtils.dataToDatetimeIni(DataUtils.parseDDmmYYYY(dataIniString));
      } else if(!String.isEmpty(paramStatus)) {
        return DataUtils.dataToDatetimeIni(data);
      }
      return DataUtils.getUserDatetime(DataUtils.dataToDatetimeIniMes(data));
    }

    private Datetime createDatetimeEnd() {
      if(isFilterRage()) {
          return DataUtils.dataToDatetimeFim(DataUtils.parseDDmmYYYY(dataFimString));
      } else if(!String.isEmpty(paramStatus)) {
        return DataUtils.dataToDatetimeFim(data);
      }
      return DataUtils.getUserDatetime(DataUtils.dataToDatetimeFimMes(data));
    }

    private Boolean isFilterRage() {
      return String.isNotEmpty(dataIniString) && String.isNotEmpty(dataFimString);
    }
}