public with sharing class VFC112_TestDriveVehicle_Import_BO {
	
	private static final VFC112_TestDriveVehicle_Import_BO instance = new VFC112_TestDriveVehicle_Import_BO();
	
	public static VFC112_TestDriveVehicle_Import_BO getInstance(){
		return instance ;
	}
	  
	private VFC112_TestDriveVehicle_Import_BO(){}
	  
	public void AfterInsertOn_TestDriveVehicle_Import(List<VEH_Veh__c> newImports)
	{
		for(VEH_Veh__c sObjVehicle : newImports){
	  		if(sObjVehicle.Status__c == 'TEST DRIVE'){
	  			this.createTestDriveVehicleRelation(sObjVehicle);
	  		}
	  	}	  	
	}
	  	  
	private void createTestDriveVehicleRelation(VEH_Veh__c sObjVehicle){
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = new TDV_TestDriveVehicle__c();
		sObjTestDriveVehicle.Available__c = true;
		sObjTestDriveVehicle.Vehicle__c = sObjVehicle.Id;
		sObjTestDriveVehicle.Account__c = getDealer(sObjVehicle.IDBIRSellingDealer__c).Id;
		sObjTestDriveVehicle.AgendaOpeningDate__c =  date.newinstance(2013, 1, 1);
		sObjTestDriveVehicle.AgendaClosingDate__c =  date.newinstance(2050, 1, 1); 
		Database.upsert(sObjTestDriveVehicle);		 
	}
	
	private Account getDealer(String BIR){
		Map<Id,Account> dealers = null; 
		dealers = VFC12_AccountDAO.getInstance().fetchDealersUsing_IDBIR(BIR);
		return dealers.values();
	}
}