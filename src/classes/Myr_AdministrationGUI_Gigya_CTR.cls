/**
* @author sebastien.ducamp@atos
* @description This class is used to manage the custom setting CS_Gigya_Settings__c and country Gigya settings
* without asking repeatedly for ARPE.
* This class automatically display all the fields available for this custom field and the associated description.
* So, the description shoul be clear and comprehensible for everybody
* This allows the user to set the country gigya settings and test them directly by checking the connectivity
* version 1.0: 16.07 / Sébastien DUCAMP / initial revision
*/
public class Myr_AdministrationGUI_Gigya_CTR { 

    /* Inner representation for the Gigya settings **/
    public class GigyaSetting {

        public String ApiKey {get; set;}
        public String SecretKey {get; set;}
        public String EncodedValue {get; set;}
        public String DataCenter {get; set;}
		public String ApiFieldName;
		public String DataCenterFieldName;

        //inner @constructor with encoded value
        public GigyaSetting(Country_Info__c c, String apiField, String datacenterField) {
			ApiFieldName = apiField;
			DataCenterFieldName = datacenterField;
            this.DataCenter = (String) c.get(datacenterField);
            this.EncodedValue = (String) c.get(apiField);
            decode();
        }
        //encode the api key and secret key into encodedvalue
        public String encode() {
			EncodedValue='';
			if( !String.isBlank(ApiKey) && !String.isBlank(SecretKey)) {
				EncodedValue = LMT_Crypto_CLS.encode(ApiKey+':'+SecretKey,system.Label.Myr_Gigya_Key);
			}
            return EncodedValue;
        }
        //decode tne encoded value into apikey and secretKey
        public void decode() {
            String DecodedValue = LMT_Crypto_CLS.decode( EncodedValue, system.Label.Myr_Gigya_Key );
            if( !String.isBlank(DecodedValue) ) {
                List<String> values = DecodedValue.split(':');
                if( null != values && values.size() == 2 ) {
                    ApiKey = values[0];
                    SecretKey = values[1];
                }
            } else {
                ApiKey = '';
                SecretKey = '';
                EncodedValue = '';
            }
        }
    }

    /* Membeer variables **/
    public Map<String, Country_Info__c> CountrySettings {get; set;}
    public Myr_AdministrationGUI_Proxify_CLS.ProxySetting GigyaSettings {get; set;}
    public CS_Gigya_Settings__c InnerSettings {get; set;}
    public Map<String, List<String>> GlobalFields {get; set;}
	private Myr_AdministrationGUI_Tools GUITools;
    //The current country selected through a picklist
    public String SelectedCountry {get; set;}
    //The current brand selected through a picklist
    public String SelectedBrand {get; set;}
    //The map to retrieve the gigya values
    public Map<String, Map<String, GigyaSetting>> CountryBrandGigyaSettings {get; set;}
    //Boolean to indicate if we are in Edit Mode
    public Boolean EditMode {get; private set;}
    //Boolean to indicate if the popup should be displayed or not
    public Boolean Popup {get; set;}
	//List of possible options for Gigya settings
	public List<SelectOption> Brands {get; private set;}

    /* @constructor **/
    public Myr_AdministrationGUI_Gigya_CTR() {
        getSettings();
        EditMode = false;
        Popup = false;
    }
    
    /* @return the list<SelectOption> of all the available countries **/
    public List<SelectOption> getCountries() {
        List<SelectOption> options = new List<SelectOption>();
        if( CountrySettings == null ) return options;
        options.add( new SelectOption( '', '' ) );
        for( String country : CountrySettings.keySet() ) {
            options.add( new SelectOption( country, country ) );
        }
        return options;
    }

    /* Prepare all the settins used on this page **/
    public void getSettings() {
        getCountriesSettings();
        getGigyaSettings();
    }

    /* Set the list of Gigya settings for the countries **/
    private void getCountriesSettings() {
        CountrySettings = new Map<String, Country_Info__c>();
        CountryBrandGigyaSettings = new Map< String, Map<String, GigyaSetting>>();
		List<Country_Info__c> listCountries = [SELECT Id, Name, 
			Gigya_Datacenter_NoProd__c, Gigya_DAC_K_NoProd__c, Gigya_REN_K_NoProd__c, 
			Gigya_Datacenter__c, Gigya_DAC_K__c, Gigya_REN_K__c,
			Gigya_DAC_NoEmailVerif_K__c, Gigya_DAC_NoEmailVerif_K_NoProd__c,
			Gigya_REN_NoEmailVerif_K__c, Gigya_REN_NoEmailVerif_K_NoProd__c
			FROM Country_Info__c ORDER BY Name LIMIT 1000];
		Map<String, Myr_GigyaCaller_Cls.GigyaIdsCouple> mapCoupleGigya = new Map<String, Myr_GigyaCaller_Cls.GigyaIdsCouple>();
		//Prepare the Gigya otpions
		Brands = new List<SelectOption>();
		Brands.add( new SelectOption('','') );
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			for( Myr_GigyaCaller_Cls.ENVIRONMENT env : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get( brand ).keySet() ) {
				String key = brand + '_' + env.name();
				Myr_GigyaCaller_Cls.GigyaIdsCouple gigyaSetting = Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).get(env);
				mapCoupleGigya.put( key, gigyaSetting );
				Brands.add( new SelectOption(key, key) );
			}
		}
		//Prepage the countries settings
		for( Country_Info__c cInf : listCountries ) {
			CountrySettings.put( cInf.Name, cInf );
			Map<String, GigyaSetting> gigSetting = new Map<String, GigyaSetting>();
			for( String key : mapCoupleGigya.keySet() ) {
				gigSetting.put( key, new GigyaSetting(cInf, mapCoupleGigya.get(key).field, mapCoupleGigya.get(key).datacenter) );
			}
			CountryBrandGigyaSettings.put( cInf.Name, gigSetting );
		}
		System.debug('CountryBrandGigyaSettings::'+CountryBrandGigyaSettings);
    }

    /* Set the list of global Gigya settings **/
    private void getGigyaSettings() {
		GUITools = new Myr_AdministrationGUI_Tools();
        Myr_MyRenaultTools.SelectAll queryAll = Myr_MyRenaultTools.buildSelectAllQuery( CS_Gigya_Settings__c.class.getName(), null );
        InnerSettings = Database.query( queryAll.Query );
        GlobalFields = GUITools.buildGroups(queryAll.Fields);
        GigyaSettings = Myr_AdministrationGUI_Proxify_CLS.proxify( InnerSettings, queryAll.Fields, CS_Gigya_Settings__c.class.getName() );
    }

    /* Save the global gigya settings **/
    public void save() {
        //Save Gigya
        SObject OldSetting = GigyaSettings.original;
        try {
            InnerSettings = (CS_Gigya_Settings__c) GigyaSettings.copySettings();
            update InnerSettings;
        } catch (Exception e) {
			String message = String.format( system.Label.Myr_GUI_Gigya_SaveFailed, new List<String>{e.getMessage()});
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, message ) );
            return;
        }
        //Log the information
        try {
            INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
                                    , Myr_AdministrationGUI_Gigya_CTR.class.getName()
                                    , 'Save Gigya Global'
                                    , InnerSettings.Id
                                    , 'OK'
                                    , 'List of Modifications: ' + GigyaSettings.getModifications()
                                    , INDUS_Logger_CLS.ErrorLevel.Info );
        } catch (Exception e) {
            //Do nothing: do not interrompt the treatment because of a logging problem
        }
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, system.Label.Myr_GUI_Gigya_SaveSuccess) );
        EditMode = false;
    }

    /* Save the current country gigya settings **/
    public void saveCountry() {
		system.debug('### Myr_AdministrationGUI_Gigya_CTR - <saveCountry> - BEGIN');
        Country_Info__c cInf = null;
        Country_Info__c OldcInf = null;
        //Modify the country
        try {
            GigyaSetting setting = CountryBrandGigyaSettings.get(SelectedCountry).get(SelectedBrand);
            cInf = CountrySettings.get(SelectedCountry);
			OldcInf = cInf.clone();
			cInf.put( setting.DataCenterFieldName, setting.DataCenter );
			cInf.put( setting.ApiFieldName, setting.encode() );
			system.debug('### Myr_AdministrationGUI_Gigya_CTR - <saveCountry> - cInf.Name='+cInf.Name+', datacenter='+setting.DataCenterFieldName+', field='+setting.ApiFieldName);
			/*
			if( 'Renault'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter__c = setting.DataCenter;
				cInf.Gigya_REN_K__c = setting.encode();
			} else if ( 'Dacia'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter__c = setting.DataCenter;
				cInf.Gigya_DAC_K__c = setting.encode();
			} else if( 'Renault_NoProd'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter_NoProd__c = setting.DataCenter;
				cInf.Gigya_REN_K_NoProd__c = setting.encode();
			} else if ( 'Dacia_NoProd'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter_NoProd__c = setting.DataCenter;
				cInf.Gigya_DAC_K_NoProd__c = setting.encode();				
			} else if ( 'Renault_NoEmailVerif_NoProd'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter_NoProd__c = setting.DataCenter;
				cInf.Gigya_REN_NoEmailVerif_K_NoProd__c = setting.encode();				
			} else if ( 'Dacia_NoEmailVerif_NoProd'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter_NoProd__c = setting.DataCenter;
				cInf.Gigya_DAC_NoEmailVerif_K_NoProd__c = setting.encode();				
			} else if ( 'Renault_NoEmailVerif'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter__c = setting.DataCenter;
				cInf.Gigya_REN_NoEmailVerif_K__c = setting.encode();				
			} else if ( 'Dacia_NoEmailVerif'.equalsIgnoreCase( SelectedBrand ) ) {
				cInf.Gigya_Datacenter__c = setting.DataCenter;
				cInf.Gigya_DAC_NoEmailVerif_K__c = setting.encode();				
			}
			*/
            update cInf;
        } catch (Exception e) {
			String message = String.format( system.Label.Myr_GUI_Gigya_SaveCountryFailed, new List<String>{cInf.Name, e.getMessage()});
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, message ) );
            return;
        }

        //Log the information
        try {
            INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
                                    , Myr_AdministrationGUI_Gigya_CTR.class.getName()
                                    , 'Save Gigya Country'
                                    , cInf.Id
                                    , 'OK'
                                    , 'Old Record=' + String.valueOf(OldcInf) + '\r\nNew Record=' + String.valueOf(cInf)
                                    , INDUS_Logger_CLS.ErrorLevel.Info );
        } catch (Exception e) {
            //Do nothing: do not interrompt the treatment because of a logging problem
        }
		String message = String.format( system.Label.Myr_GUI_Gigya_SaveCountrySuccess, new List<String>{cInf.Name});
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, message) );
        Popup = false;
    }

    /* Cancel the current modifications **/
    public void cancel() {
        EditMode = false;
    }

    /* Cancel the current country modifications **/
    public void cancelCountry() {
        Popup = false;
    }

    /* Switch to Edit Mode **/
    public void edit() {
        EditMode = true;
    }

    /* Display the popup to edit gigya settings **/
    public void displayPopup() {
        system.debug('#### Myr_AdministrationGUI_Gigya_CTR - <displayPopup> - Country='+SelectedCountry);
        system.debug('#### Myr_AdministrationGUI_Gigya_CTR - <displayPopup> - Brand='+SelectedBrand);
		SelectedBrand = null;
		getSettings();
        Popup = true;
    }

    /* check the connectivity with Gigya **/
    public void checkConnectivity() {
		/*
        checkConnectivity( 'Renault',Myr_GigyaCaller_Cls.ENVIRONMENT.PROD);		// check Connectivity for Renault
        checkConnectivity( 'Renault',Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);	// check Connectivity for Renault_NoProd
        checkConnectivity( 'Dacia',Myr_GigyaCaller_Cls.ENVIRONMENT.PROD);		// check Connectivity for Dacia
		checkConnectivity( 'Dacia',Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD);		// check Connectivity for Dacia_NoProd
		*/
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			for( Myr_GigyaCaller_Cls.ENVIRONMENT env : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).keySet() ) {
				checkConnectivity( brand, env );
			}
		}
    }

    private void checkConnectivity(String brand, Myr_GigyaCaller_Cls.ENVIRONMENT env) {
		try  {
			Myr_GigyaCaller_Cls caller = new Myr_GigyaCaller_Cls( SelectedCountry, brand, env );
			String message = String.format( system.Label.Myr_GUI_Gigya_ConnectionSuccess, new List<String>{brand + '_' + env.name()});
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, message ) );
		} catch( Exception e ) {
			String message = String.format( system.Label.Myr_GUI_Gigya_ConnectionFailed, new List<String>{brand + '_' + env.name(), e.getMessage()});
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, message ) );
		}
    }
}