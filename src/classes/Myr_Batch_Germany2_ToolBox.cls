/* Purge of accounts
  *************************************************************************************************************
  18 Mar 2016 : PurpleDragonBaby 10.1 : Creation - purge of german accounts
  *************************************************************************************************************/
global class Myr_Batch_Germany2_ToolBox {

	//Get german accounts
	public static List<TB_Answer> Get_German_Account_Ids(String brand) {
		return Get_Account_Ids(brand, 'Germany2');
	}

	//Get accounts, depending on a brand
	public static List<TB_Answer> Get_Account_Ids(String brand, String country) {
		System.debug('Myr_Batch_Germany2_ToolBox.Get_Account_Ids : Brand=' + brand + ', country=' + country);
		List<TB_Answer> T_B;
		TB_Answer My_TB;
		List<Id> L_I = new List<Id> ();
		List<Account> L_A;
		List<User> L_U;
		Boolean my_Boolean;
		String RetentionDay = Country_Info__c.getInstance(country).Myr_Deleting_Retention__c;

		//Request accounts into the database
		String query = 'Select Id from Account where country__c=\'' + country + '\' and ((';
		if (brand.equalsIgnoreCase('DACIA')) { 
			query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myr_Status__c!=\'' + system.Label.Myr_Status_Deleted + '\' and Myr_Status__c!=null';
		} else if (brand.equalsIgnoreCase('RENAULT')) {
			query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myd_Status__c!=\'' + system.Label.Myd_Status_Deleted + '\' and Myd_Status__c!=null';
		} else {
			query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MyD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay;
			query += ') OR (';
			query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myd_Status__c=null';
			query += ') OR (';
			query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myr_Status__c=null';
		}
		query += '))';
		query += Add_Governor_Limit();
		System.debug('Myr_Batch_Germany2_ToolBox.Get_Account_Ids : query=' + query);
		L_A = Database.query(query);

		//Get corresponding community users
		for (Account a : L_A) {
			L_I.add(a.Id);
		}
		L_U = [select accountid from User where accountid in :L_I];
		System.debug('L_U'+L_U.size());
		//Response 
		T_B = new List<TB_Answer> ();
		for (Account a : L_A) {
			My_TB = new TB_Answer();
			my_Boolean = false;
			for (User u : L_U) {
				if (u.accountid == a.Id) {
					my_Boolean = true;
				}
			}
			My_TB.a = a;
			My_TB.User_Associated = my_Boolean;
			T_B.add(My_TB); 
		}
		System.debug('Size of the response list T_B.size ='+T_B.size());
		return T_B;
	}

	//Generate an unique value - dedicated to anonymize username
	public static String generateCancelName() {
		System.debug('Myr_Batch_Germany2_ToolBox.generateCancelName');
		String timestamp = String.valueOf(datetime.now().getTime());
		String prefix = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c;
		return prefix + '@' + timestamp + '.com';
	}

	//Add a generic governor limit, suitable for all query
	public static String Add_Governor_Limit() {
		return ' limit 50000 ';
	}

	//Set empty the account
	public static Account Set_Empty(SObject record, String Mode) {
		System.debug('Myr_Batch_Germany2_ToolBox.Set_Empty : record=' + record + 'Mode=' + Mode);
		Account a = (Account) record;
		if (Mode.equals('ALL') || Mode.equals('RENAULT')) {
			a.MYR_Status__c = null;
			a.MYR_Status_UpdateDate__c = null;
			a.MyRenaultID__c = null;
			a.MyR_Account_Delete_Reason__c = null;
			a.MYR_CreationDate__c = null;
			a.MYR_Dealer_2_BIR__c = null;
			a.MYR_Dealer_3_BIR__c = null;
			a.MYR_Dealer_4_BIR__c = null;
			a.MYR_Dealer_5_BIR__c = null;
			a.MyR_Last_Connection_Date__c = null;
			a.MyR_Dealer_1_BIR__c = null;
			a.MyR_Prefered_Dealer_Update_Date__c = null;
			a.MyR_Subscriber_Dealer_BIR__c = null;
			a.MyR_Subscriber_Dealer_IPN__c = null;
		}
		if (Mode.equals('ALL') || Mode.equals('DACIA')) {
			a.MyD_Status__c = null;
			a.MyD_Status_UpdateDate__c = null;
			a.MyDaciaID__c = null;
			a.MyD_Account_Delete_Reason__c = null;
			a.MyD_CreationDate__c = null;
			a.MyD_Dealer_2_BIR__c = null;
			a.MyD_Dealer_3_BIR__c = null;
			a.MyD_Dealer_4_BIR__c = null;
			a.MyD_Dealer_5_BIR__c = null;
			a.MyD_Last_Connection_Date__c = null;
			a.MyD_Dealer_1_BIR__c = null;
			a.MyD_Prefered_Dealer_Update_Date__c = null;
			a.MyD_Subscriber_Dealer_BIR__c = null;
			a.MyD_Subscriber_Dealer_IPN__c = null;
		}
		if (Mode.equals('ALL')) {
			a.country__c = null;
			a.Firstname = null;
			a.Lastname = null;
			a.PersEmailAddress__c = null;
			a.BillingStreet = null;
			a.BillingCity = null;
			a.Language__c = null;
			a.Salutation_Digital__c = null;
			a.SecondaryEmail__c = null;
			a.SecondSurname__c = null;
		} 
		return a;
	}

	//Inner class
	global class TB_Answer {
		public Account a;
		public Boolean User_Associated;

		public TB_Answer(){
			User_Associated=false;
		}
	}
}