public with sharing class AccountSearchPageController {


 

  public Integer selectedId{get;set;}
  public WS09_BCS_GetCustData bcsDatasource;
  public BcsReponseObject xmlData{get;set;}
  public Account acc{get;set;}
  public BcsReponseObject selectedAccount{get;set;}
  public boolean shwSection{get;set;}
  public Id AccountId{get;set;}
  boolean display;
  String show;
  public List<BcsReponseObject> xmlDataList{get;set;}
  public List<BcsReponseObjectWrapper> xmlDataWrapper{get;set;}
  public Account updateAcc{get;set;}


    public PageReference TestButton() {
        return null;
    }
    public Pagereference getSelected()
    {
      System.debug('======Inside the Get Function======');
      System.debug('======Inside the Get Function======' +selectedId);
    return null;
    }
    
    
      public AccountSearchPageController (ApexPages.StandardController controller) {

    }
    
     public String getCountryInfo()
    {
      System.debug('In GetCountryInfo Function>>>>>'); 
      User u = [Select Id, RecordDefaultCountry__c from user where Id =:UserInfo.getUserId()];    
      Country_Info__c c =[Select Id, Country_Code_2L__c from Country_Info__c where Name=:u.RecordDefaultCountry__c ];
      System.debug('Country Value is >>>>>'+c.Country_Code_2L__c);
      return c.Country_Code_2L__c;
    }
     

    public String getfName{
    get;
    set {getfName=value;
    }
    }
    public String getAcName{
    get;
    set {getAcName=value;
    }
    }
    public String getAccEmail{
    get;
    set {getAccEmail=value;
   
    }
    }
    public String getPhNo{
    get;
    set {getPhNo=value;
    }
    }
     public String getStreet{
    get;
    set {getStreet=value;
    }
    }
    public String getCity{
    get;
    set {getCity=value;
    }
    }
     public String getZip{
    get;
    set {getZip=value;
    }
    }
    public AccountSearchPageController() {
    bcsDatasource=new WS09_BCS_GetCustData();

  selectedId=null;
    }
    
    
    public PageReference initialize() {
    
        AccountId= ApexPages.currentPage().getParameters().get('id');
        show= ApexPages.currentPage().getParameters().get('show');
        try{
        
        acc =[Select Id,Name,LastName,FirstName,Bcs_Id__c,PersEmailAddress__c,PersMobPhone__c,BillingStreet,BillingCity,BillingPostalCode  from Account where Id = :AccountId LIMIT 1];
        
        }
        catch(Exception e)
        {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getmessage()));
        }
        if(show=='true'){
        shwSection=true;
        }
        else{
        shwSection=false;
        }
        return null;
    }
    
    
     public String getAccount() {
     if (AccountId!=null)
     {
     acc =[Select Id,Name,Bcs_Id__c,PersEmailAddress__c from Account where Id = :AccountId LIMIT 1];
     
     }

        return null;
    }

    public PageReference processSelectedcmd() {
        return null;
    }
     
     
     
    public PageReference MergeAccount() {
    
   
    updateAcc=new Account();
    if(getfName!=null){
    updateAcc.firstName=getfName;
    }
    if(getAcName!=null){
    updateAcc.lastName=getAcName;
 
    }
    if(getAccEmail!=null){
    updateAcc.PersEmailAddress__c=getAccEmail;
  
    }
    if(getPhNo!=null){
    updateAcc.PersMobPhone__c=getPhNo;
    }
    if(getStreet!=null){
    updateAcc.BillingStreet=getStreet;
    }
    if(getCity!=null){
    updateAcc.BillingCity=getCity;
    }
    if(getZip!=null){
    updateAcc.BillingPostalCode=getZip;
    }
    updateAcc.Id=AccountId;
    try{
    update updateAcc;
    }
    catch(Exception e){}
     
    display=true;
    Pagereference Mergepag=new Pagereference('/apex/AccountMergePage?Id='+AccountId+'&show='+display);
    Mergepag.setRedirect(true);    
    return Mergepag;
    }


    
     public Integer showDetail 
    {
        get { if(showDetail == null) {showDetail = 0; return showDetail;} else return showDetail; }
        set { showDetail = value; }
    }
    
     public String VIN{get;set;}
     public String country{get;set;}
     public String brand{get;set;}
     public String demander{get;set;}
     public String lastName{get;set;}
     public String firstName{get;set;}
     public String city{get;set;}
     public String zip{get;set;}
     public String ident1{get;set;}
     public String idClient{get;set;}
     public String idMyr{get;set;}
     public String firstRegistrationDate{get;set;}
     public String registration{get;set;}
     public String email{get;set;}
     public String ownedVehicles{get;set;}
     public String nbReplies{get;set;}
     public String bcsId{get;set;}
    
    public Account b;

    
    public Pagereference createNewAccount() {
    
    String urlExtension='/001/e?RecordType=01290000000T0iS&ent=Account';
    if(firstName!=null)
    {
        urlExtension=urlExtension+'&name_firstacc2='+firstName;
    }
    if(lastName!=null)
    {
        urlExtension=urlExtension+'&name_lastacc2='+lastName;
    }
    if(email!=null)
    {
        urlExtension=urlExtension+'&00ND0000004qc9K='+email;
    }
    if(city!=null)
    {
        urlExtension=urlExtension+'&acc17city='+city;
    }
    
    System.debug('url Extension'+urlExtension);
    
    Pagereference pag=new Pagereference(urlExtension);
    
    return pag;
    }

    
    public PageReference createAccount() {
    
    if(selectedId==null)
    {    
    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,'You have to Select One Value'));
    
    return null;
    }

  else{
    try{
    b= [Select Id from Account where Bcs_Id__c =:xmldataList[selectedId].IdClient];
    }
    catch(Exception e)
    {
   
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getmessage()));
    }
    if(b == null)
    {
   
    try
    {
    Account a= new Account();
    
    
    if(xmlDataList[selectedId].FirstName!=null)
    {
    
    a.FirstName=xmlDataList[selectedId].FirstName;
    }
    a.LastName=xmlDataList[selectedId].LastName;  
    
    
    
    if(xmlDataList[selectedId].StrName!=null && xmlDataList[selectedId].Compl2!=null )
    
    {
    
    a.ShippingStreet=xmlDataList[selectedId].StrName+' ' +xmlDataList[selectedId].Compl2;     
    
    }
    
    
    if(xmlDataList[selectedId].email!=null )
    { 
    a.PersEmailAddress__c=xmlDataList[selectedId].email;     
    
    }
    
    
    if(xmlDataList[selectedId].CountryCode!=null)
    {
    a.ShippingCountry=xmlDataList[selectedId].CountryCode;
    }
    
    
    if(xmlDataList[selectedId].Zip!=null)
    {
    a.ShippingPostalCode=xmlDataList[selectedId].Zip;
    
    }
    
    if(xmlDataList[selectedId].City !=null)
    {  
    a.ShippingCity=xmlDataList[selectedId].City;
    
    }
    if(xmlDataList[selectedId].IdClient!=null  )
    {  
    
    a.Bcs_Id__c=xmlDataList[selectedId].IdClient;
    
    }
    
    a.ComAgreemt__c='Partial'; 
    
    
    if(xmlDataList[selectedId].vin!=null)
    {
    }
    
    insert a;
    return new PageReference('/' + a.id );   
    
    }
    
    catch(NullPointerException ex)
    {
    
    
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,ex.getmessage()));
    return null;
  
  }
   
      
   }
    else 
    {
    PageReference reference=new PageReference('/apex/AccountMergePage?id=' + b.id);
    reference.setRedirect(false);        
    return reference;
    }
    }

}
  
  public class BcsReponseObjectWrapper
  {
  public BcsReponseObject bcsObject{get;set;}
  public Integer rowNumber{get;set;}
  
  }

   
 
   public Pagereference getAccountList(){
   
    xmlDataWrapper=New List<BcsReponseObjectWrapper>();
    xmlDataList=New List<BcsReponseObject>();

    if(Test.isRunningTest())
    {String Output = 'Output';}
    else{
    String Output=bcsDatasource.getAccountInfo(vin,registration,getCountryInfo(),lastName,brand,email,city,zip);
    BcsParsingEngine parsingEngine =new BcsParsingEngine();
    try
    {
    xmlDataList=parsingEngine.parseXML(Output);
    System.debug('Size of the List Retrieved'+xmlDataList.size());
    }
    catch(Exception e){}
    
    
    Integer row=0;
    for(BcsReponseObject b :xmlDataList)
    {
    BcsReponseObjectWrapper wrapper=new BcsReponseObjectWrapper();
    wrapper.bcsObject=b;
    wrapper.rowNumber=row;
    xmlDataWrapper.add(wrapper);
    row++;
    }

    if(String.isBlank(Output))
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Customers found'));
    }
    }
   return null;
  }


}