public class ClientePreOrdemSfa2Controller {
    public VFC60_QuoteVO quoteVO { get;set; }

    private String quoteId;
    private String oppId;
    public String mobilePhoneQuote { get;set; }
    public String customerEmailQuote { get;set; }
    public String homePhoneQuote { get;set; }
    public Boolean useExistBillingAccount { get;set; }
    public Boolean camposPreOrder { get;set;}

    public VEH_Veh__c veiculo { get; set; }
    public VFC61_QuoteDetailsVO quoteDetailsVO { get; set; }

    public String selectedBrand {get;set;}
    public String selectedModel {get;set;}

    public List<Selectoption> brands {
        get {
            List<SelectOption> option = new List<SelectOption>();
            option.add(new SelectOption('', ''));
            option.add(new SelectOption('RENAULT', 'RENAULT'));
            option.add(new SelectOption('CHEVROLET', 'CHEVROLET'));
            option.add(new SelectOption('CITROEN', 'CITROEN'));
            option.add(new SelectOption('FIAT', 'FIAT'));
            option.add(new SelectOption('FORD', 'FORD'));
            option.add(new SelectOption('GMC', 'GMC'));
            option.add(new SelectOption('HONDA', 'HONDA'));
            option.add(new SelectOption('HYUNDAI', 'HYUNDAI'));
            option.add(new SelectOption('PEUGEOT', 'PEUGEOT'));
            option.add(new SelectOption('TOYOTA', 'TOYOTA'));
            option.add(new SelectOption('VOLKSWAGEN', 'VOLKSWAGEN'));
            return option;
        }
    }

    public ClientePreOrdemSfa2Controller(ApexPages.StandardController controller) {
        this.quoteId = controller.getId();
        this.quoteVO = new VFC60_QuoteVO();
        this.useExistBillingAccount = false;
        this.oppId = Apexpages.currentPage().getParameters().get('oppId');
    }

    public PageReference initializeQuote() {
        this.quoteVO = VFC63_QuoteBusinessDelegate.getInstance().getQuoteById(this.quoteId);

        if(quoteVO.sObjQuote.BillingAccount__r == null){
            quoteVO.sObjQuote.BillingAccount__r = new Account();
        }

        this.camposPreOrder = false;
        this.veiculo = null;
        this.quoteDetailsVO =
            VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, oppId);

        if(this.quoteDetailsVO.sObjQuote.UsedVehicle__r != null) {
            selectedBrand = this.quoteDetailsVO.sObjQuote.UsedVehicle__r.Brand__c;
            selectedModel = this.quoteDetailsVO.sObjQuote.UsedVehicle__r.Model__c;
        }

        this.quoteDetailsVO.lstSelOptionPaymentMethods =
            VFC49_PickListUtil.buildPickList(Quote.PaymentMethods__c);
        if(!quoteDetailsVO.lstQuoteLineItemVO.isEmpty()) {
            String quoteItemId = quoteDetailsVO.lstQuoteLineItemVO.get(0).Id;
            String veiculoId = quoteDetailsVO.lstQuoteLineItemVO.get(0).vehicleId;

            if(veiculoId != null) {
                veiculo = [
                    SELECT Id, Name, Version__c, Color__c, Optional__c, Upholstery__c, Harmony__c
                    FROM VEH_Veh__c
                    WHERE Id =: veiculoId
                ];

                System.debug('Veiculo: ' + veiculo);

            } else {

                // Recupera detalhes do veiculo que estão na linha de cotação
                QuoteLineItem quoteItem = [
                    SELECT Id, Version_AOC__r.Name, ColorName__c, UpholsteryName__c, HarmonyName__c, OptionalsNames__c
                    FROM QuoteLineItem
                    WHERE Id =: quoteItemId
                ];

                system.Debug('***quoteItem: '+quoteItem);

                veiculo = new VEH_Veh__c();

                veiculo.Color__c = quoteItem.ColorName__c;
                veiculo.Upholstery__c = quoteItem.UpholsteryName__c;
                veiculo.Harmony__c = quoteItem.HarmonyName__c;
                veiculo.Version__c = quoteItem.Version_AOC__r.Name;
                veiculo.Optional__c = quoteItem.OptionalsNames__c;
                //veiculo.Optional__c = String.join(optionalNameList,', ');
            }
        }
        return null;
    }

    public PageReference addVeiculo() {
        if(salvar() != null) {
            PageReference pageRef = new PageReference(
                '/apex/EstoqueInsertSfa2?Id=' + this.quoteId + '&oppId=' + this.oppId);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

    public PageReference removerVeiculo() {
        try {
            for(VFC83_QuoteLineItemVO vo : quoteDetailsVO.lstQuoteLineItemVO) {
                VFC65_QuoteDetailsBusinessDelegate.getInstance().deleteQuoteLineItem(this.quoteDetailsVO, vo.Id);
            }
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.CONFIRM, 'Veículo excluído com sucesso'));

            initializeQuote();

        } catch(VFC90_DeleteQuoteLineItemException ex) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
        }

        return null;
    }

    private void salvarOrcamento() {
        if(!quoteDetailsVO.lstQuoteLineItemVO.isEmpty()) {
            String qItemId = quoteDetailsVO.lstQuoteLineItemVO.get(0).Id;
            Double vl = VFC69_Utility.priceFormatToDouble(quoteVO.valueTotal);
            QuoteLineItem item = new QuoteLineItem(Id=qItemId, unitPrice = vl);
            update item;
        }

        Quote quote = new Quote(Id= quoteDetailsVO.sObjQuote.Id, PaymentMethods__c = quoteDetailsVO.paymentMethods);
        Date data = quoteDetailsVO.sObjQuote.ExpirationDate;
        Date deliveryDate = quoteDetailsVO.sObjQuote.DeliveryDate__c;
        quote.DeliveryDate__c = deliveryDate != NULL ? deliveryDate : NULL;
        quote.ExpirationDate = data != NULL ? data : NULL;
        update quote;
    }

    public PageReference salvar() {
        ApexPages.Message message = null;

        saveAddress();

        if(String.isEmpty(quoteVO.accountFirstName) || String.isEmpty(quoteVO.accountLastName)) {
            message = new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Preencha os campos Nome e Sobrenome.');
            ApexPages.addMessage(message);
            return null;
        }

        if(String.isEmpty(quoteVO.accountResPhone) && String.isEmpty(quoteVO.accountCelPhone) &&
           String.isEmpty(quoteVO.accountComPhone) && String.isEmpty(quoteVO.accountEmail) ) {
               message = new ApexPages.Message(
                   ApexPages.Severity.ERROR,
                   'Você deve informar pelo menos uma forma de contato (Email, Telefone Residencial ou Celular)');
               ApexPages.addMessage(message);
               return null;
           }

        try {

            // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
            quoteVO.sObjQuote.Opportunity.DateTimeSellerUpdatedStageOpp__c = System.now();

            Database.update(quoteVO.sObjQuote.Opportunity);
            VFC63_QuoteBusinessDelegate.getInstance().salvarVFC60_QuoteVO(quoteVO, false);

            System.debug('quoteDetailsVO' + quoteDetailsVO);
            VFC65_QuoteDetailsBusinessDelegate.getInstance().updateQuoteDetails(this.quoteDetailsVO);

            salvarOrcamento();

            final PageReference pageRef = new PageReference(
                '/apex/ClienteOportunidadeDetalheSfa2?Id=' + quoteVO.opportunityId);
            //pageRef.setRedirect(true);
            return pageRef;

        } catch(DMLException ex) {
            System.debug('Ex: ' + ex);

            String temp = tratarMsgErro(ex.getDMLMessage(0));
            message = new ApexPages.Message(
                ApexPages.Severity.ERROR, temp == null ? ex.getDMLMessage(0) : temp);
            ApexPages.addMessage(message);
        }
        return null;
    }

    public PageReference saveRequest() {
        String strMessage = null;
        ApexPages.Message message = null;

        //validação de campo customizado devido ao request ajax
        System.debug('### QuoteVO: ' + quoteVO);
        System.debug('### BillingAccount__r.MaritalStatus__c: ' + quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c);
        System.debug('### BillingAccount__r.Sex__c:' + quoteVO.sObjQuote.BillingAccount__r.Sex__c);

        String mensagem = 'Para enviar a Pré Ordem outros campos são necessários. Verifique abaixo os campos com a marcação.';
        //valida dados obrigatórios da conta principal
        if(isFaltaDadosContaPrincipal()) {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, mensagem);
            ApexPages.addMessage(message);
            return null;

        }

        //caso a flag não for selecionada
        //if(!useExistBillingAccount){
        if(quoteVO.tipoCliente == 'cpf') {
            if(isFaltaDadosContaFaturaCpf()) {
                message = new ApexPages.Message(ApexPages.Severity.ERROR, mensagem);
                ApexPages.addMessage(message);
                return null;
            }

        } else if(quoteVO.tipoCliente == 'cnpj') {
            if(isFaltaDadosContaFaturaCnpj()){
                message = new ApexPages.Message(ApexPages.Severity.ERROR, mensagem);
                ApexPages.addMessage(message);
                return null;
            }
        }
        //}

        if(quoteDetailsVO.lstQuoteLineItemVO.isEmpty()) {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário a inclusão de um veículo.');
            ApexPages.addMessage(message);
            return null;
        }

        if( quoteDetailsVO.sObjQuote.DeliveryDate__c == null ){
            message = new ApexPages.Message(
                ApexPages.Severity.ERROR, 'É necessário informar a data de entrega.');
            ApexPages.addMessage(message);
            return null;
        }

        String bookingMessage = validateBooking();
        if(String.isNotEmpty(bookingMessage)) {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, bookingMessage);
            ApexPages.addMessage(message);
            return null;
        }

        try {

            // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
            quoteVO.sObjQuote.Opportunity.DateTimeSellerUpdatedStageOpp__c = System.now();

            System.debug('############# quoteVO.sObjQuote.Opportunity.StageName: ' + quoteVO.sObjQuote.Opportunity.StageName);

            Database.update(quoteVO.sObjQuote.Opportunity);
            salvarOrcamento();

            System.debug('############# quoteVO.sObjQuote.Opportunity.StageName: ' + quoteVO.sObjQuote.Opportunity.StageName);

            VFC63_QuoteBusinessDelegate.getInstance().saveQuote(quoteVo);

            //testDistrinet.call(quoteVo.sObjQuote.Id);
            if(quoteVO.vehicle.equals(Label.VehicleToDistrinet) && quoteVO.vehicleVin == null){
                DistrinetCallServico.call(quoteVo.sObjQuote.Id);
            }

            PageReference pageRef = new PageReference('/apex/ClienteOportunidadeDetalheSfa2?Id='+quoteVO.opportunityId);
            pageRef.setRedirect(true);
            return pageRef;

        } catch(DMLException ex) {
            strMessage = ex.getDMLMessage(0);
            String temp = tratarMsgErro(strMessage);
            message = new ApexPages.Message(
                ApexPages.Severity.ERROR, temp == null ? strMessage : temp);
            ApexPages.addMessage(message);
        }
        return null;
    }

    private void saveAddress() {
        if(!String.isEmpty(quoteVO.accountStreet)) {
            quoteVO.billStreet  = quoteVO.accountStreet;
        }
        if(!String.isEmpty(quoteVO.accountZipCode)) {
            quoteVO.billZipCode = quoteVO.accountZipCode;
        }
        if(!String.isEmpty(quoteVO.accountCity)) {
            quoteVO.billCity    = quoteVO.accountCity;
        }
        if(!String.isEmpty(quoteVO.accountState)) {
            quoteVO.billState   = quoteVO.accountState;
        }
    }

    private String tratarMsgErro(String error) {
        if(error.contains('duplicate') && error.contains('CustomerIdentificationNbr__c')) {
            return 'Este cliente já existe em nossa base. Por favor, localize a conta existente.';
        }
        return null;
    }

    private Boolean isFaltaDadosContaPrincipal() {
        if(String.isEmpty(quoteVO.accountFirstName) ||
           String.isEmpty(quoteVO.accountLastName) 	||
           String.isEmpty(quoteVO.accountCnpjCpf) 	||
           String.isEmpty(quoteVO.accountEmail) 	||
           String.isEmpty(quoteVO.accountResPhone) 	||
           String.isEmpty(quoteVO.accountStreet) 	||
           String.isEmpty(quoteVO.accountZipCode) 	||
           String.isEmpty(quoteVO.accountState) 	||
           String.isEmpty(quoteVO.accountCity) 		||
           String.isEmpty(quoteVO.accountRG) 		||
           String.isEmpty(quoteVO.sObjQuote.Opportunity.Account.Sex__c) ||
           String.isEmpty(quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c )	||
           (quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate == null)) {
               this.camposPreOrder = true;
               return true;
           }
        return false;
    }

    private Boolean isFaltaDadosContaFaturaCpf() {
        if(String.isEmpty(quoteVO.billFirstName) 	||
           String.isEmpty(quoteVO.billLastName) 	||
           String.isEmpty(quoteVO.billCpf) 			||
           String.isEmpty(quoteVO.billEmail) 		||
           String.isEmpty(quoteVO.billResPhone) 	||
           String.isEmpty(quoteVO.billStreet) 		||
           String.isEmpty(quoteVO.billZipCode) 		||
           String.isEmpty(quoteVO.billState) 		||
           String.isEmpty(quoteVO.billCity) 		||
           String.isEmpty(quoteVO.billRG) 			||
           String.isEmpty(quoteVO.sObjQuote.BillingAccount__r.Sex__c) 				||
           String.isEmpty(quoteVo.sObjQuote.BillingAccount__r.MaritalStatus__c )	||
           (quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate == null)) {
               this.camposPreOrder = true;
               return true;
           }
        return false;
    }

    private Boolean isFaltaDadosContaFaturaCnpj() {
        if(((quoteVO.billPJName == ''|| quoteVO.billPJName == null)  )
           || ((quoteVO.billPJCnpj == '' || quoteVO.billPJCnpj == null) )
           || ((quoteVO.billPJEmail == '' || quoteVO.billPJEmail == null) )
           || ((quoteVO.billPJStreet == '' || quoteVO.billPJStreet == null) )
           || ((quoteVO.billPJZipCode == '' || quoteVO.billPJZipCode == null) )
           || ((quoteVO.billPJState == '' || quoteVO.billPJState == null) )
           || ((quoteVO.billPJCity == '' || quoteVO.billPJCity == null) )){
               return true;
           }
        return false;
    }

    public String validateBooking(){

        List<VehicleBooking__c> vbkLst = VFC85_VehicleBookingDAO.getInstance().findByQuoteAndStatus(quoteVo.sObjQuote.Id, 'Active');

        if(vbkLst.size() > 0){
            return '';

        } else if(vbkLst.size() == 0){

            List<QuoteLineItem> qliLst = VFC36_QuoteLineItemDAO.getInstance().fetchQuoteLineItems_UsingQuoteIds( new Set<Id>{quoteVo.sObjQuote.Id});

            if(qliLst.size() == 0){
                return 'Não existe nenhum veículo relacionado à este orçamento. Por favor, inclua um veículo para dar continuidade ao processo.';

            } else if(qliLst.size() > 0 && String.isNotEmpty(qliLst[0].Vehicle__c)){
                return 'A reserva deste veículo foi cancelada ou expirou. Por favor, reserve um novo veículo para enviar o pedido.';

            } else if(qliLst.size() > 0 && String.isEmpty(qliLst[0].Vehicle__c)){
                return '';
            }
        }

        return '';

    }

    public void refresh() {
    }

    public void useAccountInfo() {
        quoteVO.tipoCliente = 'cpf';

        if(useExistBillingAccount){
            quoteVO.FKQuote.BillingAccount__c = this.quoteVO.sObjQuote.Opportunity.Account.Id;
            changeBillingAccount();
        }
    }

    public PageReference changeTipoCliente() {
        quoteVO.FKQuote.BillingAccount__c = null;

        return null;
    }

    public PageReference changeBillingAccount() {
        System.debug('*** changeBillingAccount ' + quoteVO);

        Account billingAccount = null;

        if(useExistBillingAccount) {
            quoteVO.billFirstName   = quoteVO.accountFirstName;
            quoteVo.billLastName    = quoteVO.accountLastName;
            quoteVo.billCpf         = quoteVO.accountCnpjCpf;
            quoteVo.billEmail       = quoteVO.accountEmail;
            quoteVo.billCelPhone    = quoteVo.accountCelPhone;
            quoteVo.billFax         = quoteVo.accountFax;
            quoteVO.billResPhone    = quoteVO.accountResPhone;
            quoteVo.billComPhone    = quoteVo.accountComPhone;
            quoteVO.billStreet      = quoteVO.accountStreet;
            quoteVO.billZipCode     = quoteVO.accountZipCode;
            quoteVO.billState       = quoteVO.accountState;
            quoteVO.billCity        = quoteVO.accountCity;
            quoteVO.billRG          = quoteVO.accountRG;

            if(quoteVO.sObjQuote.BillingAccount__r == null)
                quoteVO.sObjQuote.BillingAccount__r = new Account();

            quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate = quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate;
            quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c = quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c;
            quoteVO.sObjQuote.BillingAccount__r.Sex__c = quoteVO.sObjQuote.Opportunity.Account.Sex__c;
            quoteVO.tipoCliente = 'cpf';

        } else if(quoteVO.FKQuote.BillingAccount__c != null) {

            billingAccount = VFC63_QuoteBusinessDelegate.getInstance().getCustomerById(quoteVO.FKQuote.BillingAccount__c);

            Id personalAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
            Id companyAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Company_Acc');


            if(billingAccount != null) {
                System.debug('### billingAccount'  + billingAccount);

                if(billingAccount.RecordTypeId != null &&
                   billingAccount.RecordTypeId == companyAccount) {
                       quoteVO.tipoCliente = 'cnpj';

                   } else if(billingAccount.RecordTypeId != null &&
                             billingAccount.RecordTypeId == personalAccount) {
                                 quoteVO.tipoCliente = 'cpf';
                             }

                System.debug('### quoteVO.tipoCliente: '  + quoteVO.tipoCliente);

                VFC63_QuoteBusinessDelegate.getInstance().updateQuoteVOByAccount(billingAccount, quoteVO);
            }
        }
        return null;
    }

    /*
* Add values to Lead State field in visualforce page.
* @return option - return the State result and to store values form record
*/
    public List<SelectOption> getStates() {
        /* Initialize the selectOption to store options */
        List<SelectOption> state_Option = new List<SelectOption>();

        /* Add none value to the picklist option */
        state_Option.add(new SelectOption('', ''));

        // Hard coded state values.
        state_Option.add(new SelectOption('AC', 'AC'));
        state_Option.add(new SelectOption('AL', 'AL'));
        state_Option.add(new SelectOption('AP', 'AP'));
        state_Option.add(new SelectOption('AM', 'AM'));
        state_Option.add(new SelectOption('BA', 'BA'));
        state_Option.add(new SelectOption('CE', 'CE'));
        state_Option.add(new SelectOption('DF', 'DF'));
        state_Option.add(new SelectOption('ES', 'ES'));
        state_Option.add(new SelectOption('GO', 'GO'));
        state_Option.add(new SelectOption('MA', 'MA'));
        state_Option.add(new SelectOption('MT', 'MT'));
        state_Option.add(new SelectOption('MS', 'MS'));
        state_Option.add(new SelectOption('MG', 'MG'));
        state_Option.add(new SelectOption('PR', 'PR'));
        state_Option.add(new SelectOption('PB', 'PB'));
        state_Option.add(new SelectOption('PA', 'PA'));
        state_Option.add(new SelectOption('PE', 'PE'));
        state_Option.add(new SelectOption('PI', 'PI'));
        state_Option.add(new SelectOption('RJ', 'RJ'));
        state_Option.add(new SelectOption('RN', 'RN'));
        state_Option.add(new SelectOption('RS', 'RS'));
        state_Option.add(new SelectOption('RO', 'RO'));
        state_Option.add(new SelectOption('RR', 'RR'));
        state_Option.add(new SelectOption('SC', 'SC'));
        state_Option.add(new SelectOption('SE', 'SE'));
        state_Option.add(new SelectOption('SP', 'SP'));
        state_Option.add(new SelectOption('TO', 'TO'));
        /* Return Option */
        return state_Option;
    }

    /**
* Get the address information based on the zip code.
*/
    public PageReference getAddressGivenZipCode() {
        system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCode()');

        String zipCodeNumber = quoteVO.accountZipCode;

        if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
            ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
            system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

            if (zipCode != null) {
                quoteVO.accountStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
                quoteVO.accountCity = zipCode.City__c;
                quoteVO.accountState = zipCode.State__c;
            }
        }
        return null;
    }

    /**
* Get the address information based on the zip code for the CPF section.
*/
    public PageReference getAddressGivenZipCodeCPF() {
        system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCodeCPF()');

        String zipCodeNumber = quoteVO.billZipCode;

        if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
            ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
            system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

            if (zipCode != null) {
                quoteVO.billStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
                quoteVO.billCity = zipCode.City__c;
                quoteVO.billState = zipCode.State__c;
            }
        }
        return null;
    }

    /**
* Get the address information based on the zip code for the CNPJ section.
*/
    public PageReference getAddressGivenZipCodeCNPJ() {
        system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCodeCNPJ()');

        String zipCodeNumber = quoteVO.billPJZipCode;

        if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
            ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
            system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

            if (zipCode != null) {
                quoteVO.billPJStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
                quoteVO.billPJCity = zipCode.City__c;
                quoteVO.billPJState = zipCode.State__c;
            }
        }
        return null;
    }

    public PageReference getAccount() {
        System.debug(LoggingLevel.INFO, '*** getAccount()');

        String accountCnpjCpf = quoteVO.accountCnpjCpf;

        if(accountCnpjCpf != null &&
           (accountCnpjCpf.length() == 11 || accountCnpjCpf.length() == 14)) {

               Account mergeAccount = VFC12_AccountDAO.getInstance().fetchAccountToMerge(accountCnpjCpf);

               System.debug(LoggingLevel.INFO, '*** mergeAccount =' + mergeAccount);

               if(mergeAccount != null) {
                   quoteVO.sObjQuote.Opportunity.Account   = mergeAccount;
                   quoteVO.sObjQuote.Opportunity.AccountId = mergeAccount.Id;
                   quoteVo.accountFirstName                = mergeAccount.FirstName;
                   quoteVo.accountLastName                 = mergeAccount.LastName;
                   quoteVo.accountCnpjCpf                  = mergeAccount.CustomerIdentificationNbr__c;
                   quoteVo.accountEmail                    = mergeAccount.PersEmailAddress__c;
                   quoteVo.accountCelPhone                 = mergeAccount.PersMobPhone__c;
                   quoteVo.accountResPhone                 = mergeAccount.PersLandline__c;
                   quoteVo.accountComPhone                 = mergeAccount.ProfLandline__c;
                   quoteVo.accountFax                      = mergeAccount.Fax;
                   quoteVo.accountStreet                   = mergeAccount.ShippingStreet;
                   quoteVo.accountZipCode                  = mergeAccount.ShippingPostalCode;
                   quoteVo.accountState                    = mergeAccount.ShippingState;
                   quoteVo.accountCity                     = mergeAccount.ShippingCity;
                   quoteVO.sObjQuote.Opportunity.Account.MaritalStatus__c  = mergeAccount.MaritalStatus__c;
                   quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate   = mergeAccount.PersonBirthdate;
                   quoteVO.sObjQuote.Opportunity.Account.Sex__c            = mergeAccount.Sex__c;
                   quoteVo.accountRG                                       =
                       (mergeAccount.RGStateRegistration__c > 0) ? String.valueOF(mergeAccount.RGStateRegistration__c) : '';
               }
           }

        return null;
    }

    public Boolean mergeRecords(Account masterAccount , Account accountToMerge) {
        Boolean success = false;
        // Array to hold the results

        MERGE accountToMerge masterAccount;

        return success;
    }

    public void checkCDC() {
        quoteDetailsVO.lsgFinancing = false;
    }

    public void checkLSG() {
        quoteDetailsVO.cdcCFinancing = false;
    }

    public void save2() {
        salvar();
    }
}