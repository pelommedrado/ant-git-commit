/**
* @author sebastien.ducamp@atos
* @description Test class for Myr_AdministrationGUI_Authorizations_CTR
* @version 1.0 / 16.07 / Sébastien Ducamp / Initial revision
*/
@isTest
private class Myr_AdministrationGUI_Auth_CTR_TEST {

	private static final String HELIOSADMIN_LASTNAME = 'UnitTestClassAuth';

	//Prepare CS04_MYRAuthorization_Settings__c
	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareMyrCustomSettings();
		//Prepare user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		insert heliosAdminUser;
		//Prepare the authorization settings for Gigya (System Administrator profile)
		List<CS04_MYRAuthorization_Settings__c> listAuth = new List<CS04_MYRAuthorization_Settings__c>();
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = true;
		authGigya.Gigya_Level2__c = true;
		authGigya.SetupOwnerId = heliosAdminUser.Id;
		listAuth.add( authGigya ) ; 
		CS04_MYRAuthorization_Settings__c authGigya2 = new CS04_MYRAuthorization_Settings__c();
		authGigya2.Gigya_Level1__c = true;
		authGigya2.Gigya_Level2__c = true;
		authGigya2.SetupOwnerId = Myr_Users_Utilities_Class.getHeliosLocalAdministratorProfile().Id;
		listAuth.add( authGigya2 ) ;
		insert listAuth; 
	}

	//tool function used to return the value associated to a label in a SelectOption list
	private static String getValueFromSelectOption( List<SelectOption> iOptions, String iLabel ) {
		for( SelectOption so : iOptions ) {
			if( so.getLabel().equalsIgnoreCase( iLabel ) ) {
				return so.getValue();
			}
		}
		return null;
	}

	//Test initialization
	static testmethod void testInit() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		
		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//check the results
			system.assertNotEquals( null, controller.AuthorizationsSettings );
			system.assertEquals( 2, controller.AuthorizationsSettings.size() );
			system.assertEquals( true, controller.AuthorizationsSettings.size() > 0 );
			system.assertEquals( 3, controller.getLocations().size() );
		}
	}

	//Test Add Authorization
	static testmethod void testAddAuthorization_Profile_OK() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		//Page should be modified to allow this without editing a user (required a manage internal user authorization)
		//system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the add button
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
			controller.add();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( true, controller.AddMode );
			system.assertNotEquals( null, controller.NewAuth );
			system.assertNotEquals( null, controller.SelectProfiles );
			//check Profiles
			system.assertEquals( true, controller.SelectProfiles.size() > 0 );
			system.assertNotEquals( null, getValueFromSelectOption(controller.SelectProfiles, 'HeliosTechnique') );
			//behavior of the add authorization
			Id newProdifleId = Myr_Users_Utilities_Class.getHeliosTechniqueProfile().Id;
			controller.SelectedLocation = 'Profile';
			controller.SelectedProfile = getValueFromSelectOption(controller.SelectProfiles, 'HeliosTechnique');
			controller.NewAuth.Fields.get('gigya_level1__c').value = true;
			controller.NewAuth.Fields.get('gigya_level2__c').value = false;
			controller.NewAuth.Fields.get('atc_level1__c').value = true;
			controller.addAuthorization();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			CS04_MYRAuthorization_Settings__c newSetting = CS04_MYRAuthorization_Settings__c.getInstance( newProdifleId );
			system.assertEquals( true, newSetting.Gigya_Level1__c );
			system.assertEquals( false, newSetting.Gigya_Level2__c );
			system.assertEquals( true, newSetting.ATC_Level1__c );
		//}
	}
	
	//Test Add Authorization
	static testmethod void testAddAuthorization_User_OK() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		User otherUser = Myr_Datasets_Test.getDealerUser('testdealer','unitmyr', 'France');
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		//Page should be modified to allow this without editing a user (required a manage internal user authorization)
		//system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the add button
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
			controller.add();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( true, controller.AddMode );
			system.assertNotEquals( null, controller.NewAuth );
			system.assertNotEquals( null, controller.tekObject );
			//behavior of the add authorization
			controller.SelectedLocation = 'User';
			controller.tekObject.OwnerId = otherUser.Id;
			//check field on proxification
			system.assertEquals( false, controller.NewAuth.Fields.get('gigya_level1__c').getInText() );
			system.assertEquals( true, controller.NewAuth.Fields.get('gigya_level1__c').getInBoolean() );
			system.assertEquals( true, controller.NewAuth.Fields.get('gigya_level1__c').getInputeable() );
			controller.NewAuth.Fields.get('gigya_level1__c').value = true;
			controller.NewAuth.Fields.get('gigya_level2__c').value = false;
			controller.NewAuth.Fields.get('atc_level1__c').value = true;
			controller.addAuthorization();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			CS04_MYRAuthorization_Settings__c newSetting = CS04_MYRAuthorization_Settings__c.getInstance( otherUser.Id );
			system.assertEquals( true, newSetting.Gigya_Level1__c );
			system.assertEquals( false, newSetting.Gigya_Level2__c );
			system.assertEquals( true, newSetting.ATC_Level1__c );
		//}
	}

	//Test Add Authorization With Exception
	static testmethod void testAddAuthorization_Failed() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		//Page should be modified to allow this without editing a user (required a manage internal user authorization)
		//system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			controller.add();
			controller.SelectedLocation = 'Profile';
			//behavior of the add authorization
			Id newProfileId = '0017E000003uxly'; //fake id
			controller.SelectedProfile = newProfileId;
			controller.NewAuth.Fields.get('gigya_level1__c').value = true;
			controller.NewAuth.Fields.get('gigya_level2__c').value = false;
			controller.NewAuth.Fields.get('atc_level1__c').value = true;
			controller.addAuthorization();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
			system.assertEquals( 0, [SELECT Id FROM CS04_MYRAuthorization_Settings__c WHERE Id = :newProfileId].size() );
		//}
	}

	//Test Save Authorization OK
	static testmethod void testSaveAuthorization_OK() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the edit button
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			//behavior of the save authorization
			Id modifiedId = controller.AuthorizationsSettings[0].auth.SetupOwnerId;
			Boolean oldValue = (Boolean) controller.AuthorizationsSettings[0].proxy.Fields.get('gigya_level1__c').value;
			controller.AuthorizationsSettings[0].proxy.Fields.get('gigya_level1__c').value = !oldValue;
			controller.SelectedAuthorization = modifiedId;
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			CS04_MYRAuthorization_Settings__c modifiedSetting = CS04_MYRAuthorization_Settings__c.getInstance( modifiedId );
			system.assertEquals( !oldValue, modifiedSetting.Gigya_Level1__c );
		} 
	}

	//Test Save Authorization With Exception
	static testmethod void testSaveAuthorization_Failed() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the edit button
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			//behavior of the save authorization
			controller.AuthorizationsSettings[0].proxy.Fields.get('gigya_level1__c').value = 'truc'; //fake boolean
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
		}
	}

	//Test Delete Authorization OK
	static testmethod void testDeleteAuthorization_OK() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the edit button
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			//behavior of the save authorization
			Id deleteSetupId = controller.AuthorizationsSettings[0].auth.SetupOwnerId;
			system.assertEquals( 1, [SELECT Id FROM CS04_MYRAuthorization_Settings__c WHERE SetupOwnerId = :deleteSetupId].size() );
			controller.SelectedAuthorization = controller.AuthorizationsSettings[0].auth.Id;
			controller.deleteAuthorization(); 
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			system.assertEquals( 0, [SELECT Id FROM CS04_MYRAuthorization_Settings__c WHERE SetupOwnerId = :deleteSetupId].size() );
		}
	}

	//Test Delete Authorization With Exception
	static testmethod void testDeleteAuthorization_Failed() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the edit button
			system.assertEquals( false, controller.EditMode );
			controller.edit(); 
			system.assertEquals( true, controller.EditMode );
			//behavior of the save authorization
			controller.SelectedAuthorization = '0017E000003uxly';
			controller.deleteAuthorization();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
		}
	}

	//Test Cancel Behavior
	static testmethod void testCancelOption() {
		//Prepare a user
		User heliosAdminUser = [SELECT Id FROM User WHERE Lastname = :HELIOSADMIN_LASTNAME];
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );

		system.runAs( heliosAdminUser ) {
			//Trigger a test
			Myr_AdministrationGUI_Authorizations_CTR controller = new Myr_AdministrationGUI_Authorizations_CTR();
			//behavior of the edit button
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
			controller.cancel();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
			controller.add();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( true, controller.AddMode );
			controller.cancel();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.AddMode );
		}
	}

}