/****************************************************************************
  Apex REST WebService used to provide a login handler for Helios
  This class is called within Myr_LoginHandlerRest_WS class
*****************************************************************************
2015 Oct, Author=S. Ducamp (AtoS) : initial revision, just logs
2016 May, Author=D. Veron  (AtoS) : Customer Message DEALER_NETWORK_DELETED
2016 August, Author=S. Ducamp (AtoS) : R16.10 F1854-US3654 - Add the possibility 
										 to use get vehicle from rubics in synchronous mode
****************************************************************************/
public without sharing class Myr_LoginHandlerRest_Cls {
	//
	private static CS04_MYR_Settings__c setting = CS04_MYR_Settings__c.getInstance();

	//Enum to simulate exception and test them
	public enum TEST_FAILURE {ACC_UPDATE}
	public static final String UPDVEHREL_USREMAIL_FAILURE = 'failure.unit_test@atos.test';

	//Inner exception class
	public class LoginHandlerRestException extends Exception{}
	
	/** Login Handler main function
		@return void
	**/
	public static void loginHandler(Myr_MyRenaultTools.Brand brand) {
		MyRenault_Session A_I;

		system.debug('#### Myr_LoginHandlerRest_Cls - <loginHandler> - BEGIN');
		//Get the current user from the context
		String userId = UserInfo.getUserId();
		//If it's not a Helios Community User: reject the request and do nothing (TODO: to be check with Christophe)
		if( UserInfo.getProfileId() != Myr_Users_Utilities_Class.getHeliosCommunityProfile().Id ) {
			User user = [SELECT Id, AccountId FROM User WHERE Id=:userId];
			addLog( user.AccountId, 'loginHandler', 'UserId='+userId+', brand='+brand.name()
					, 'Invalid User', 'The user is not an Helios Community User: ' + UserInfo.getUserName()
					, system.now().getTime()
					, INDUS_Logger_CLS.ErrorLevel.Warn );
			return; //top here the execution 
		}

		//SDP --- F1854 / US3654 --- GetVehicle could be done in synchronous mode
		A_I = getVehiclesFromRubics( userId, brand.name() );
		system.debug('#### Myr_LoginHandlerRest_Cls - <loginHandler> - MyRenaultSession='+A_I);
		//SDP --- F1854 / US3654 --- Update of the Existing Vehicle Relations has to be done Synchronously !!!!!!!
		updateVehicleRelations( userId );
		
		//------ CAUTION ------------
		//SDP: All the methods below should be asynchronous to avoid delay in response !
		//We just take into account the login information to execute the treatment after that
		//------ CAUTION ------------
		updateAccount( userId, brand.name());
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler_Async(Userinfo.getUserId(), brand.name()); 
		
		//DEALER_NETWORK_DELETED & DEALER_NO_DEALER
		Myr_CreateMessages_Dea_Net_Del.Dealer_Network_Deleted(A_I.Account_Id, brand.name(), A_I.Country);
		
		system.debug('#### Myr_LoginHandlerRest_Cls - <loginHandler> - END');
	} 
	
	//-------------------------------------------------------------------------------------------------------------------------
	//------ ASYNCHRONOUS LOGIN FUNCTIONS	
	/** Update Account informations
		@future asynchronous method to avoid delay in loginhandler
	**/
	@future
	private static void updateAccount(String userId, String brand) {
		Account acc = new Account();
		Long beginTime = system.now().getTime();
		try {
			User usr = [SELECT Id, AccountId FROM User WHERE Id=:userId];
			acc = [SELECT Id, MyD_Last_Connection_Date__c, MyR_Last_Connection_Date__c, Lastname FROM Account WHERE Id=:usr.AccountId];
			if( Test.isRunningTest() && TEST_FAILURE.ACC_UPDATE.name().equalsIgnoreCase(acc.Lastname) ) {
				throw new LoginHandlerRestException('Test Exception');
			}
			if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(brand) ) {
				acc.MyR_Last_Connection_Date__c = system.today(); 
			} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(brand) ) {
				acc.MyD_Last_Connection_Date__c = system.today();
			}
			update acc;
			addLog( acc.Id, 'updateAccount', 'UserId='+userId+', brand='+brand
					, 'Info', 'update the last connection dates'
					, beginTime
					, INDUS_Logger_CLS.ErrorLevel.Info );
		} catch (Exception e) {
			addLog( acc.Id, 'updateAccount', 'UserId='+userId+', brand='+brand
					, e.getTypeName()
					, 'Problem when trying to update the last connection date for the user: ' +e.getMessage()
					, beginTime
					, INDUS_Logger_CLS.ErrorLevel.Warn );
		}
	}

	/** Update the vehicle relations
		F1854 - US3654 - EB_SFDC_MyGarageStatus_Improvement_EN-v1.2.docx
	**/
	private static void updateVehicleRelations(String userId) {
		Long beginTime = system.now().getTime();
		system.debug('#### Myr_LoginHandlerRest_Cls - <updateVehicleRelations> - BEGIN');
		User user = [SELECT Email, AccountId FROM User WHERE Id=:userId];
		try {
			system.debug('>>>>> user.Email='+user.Email);
			if( Test.isRunningTest() && user.Email.equalsIgnoreCase(UPDVEHREL_USREMAIL_FAILURE) ) {
				throw new LoginHandlerRestException('Test Exception');
			}
			Myr_ManageAccount_VehGarageStatus relationsUpdater = new Myr_ManageAccount_VehGarageStatus( user.AccountId );
			relationsUpdater.setGaragetStatusAndCommit();
			addLog( user.AccountId, 'updateVehicleRelations', 'UserId='+userId
					, 'Info', 'update vehicle garage status'
					, beginTime
					, INDUS_Logger_CLS.ErrorLevel.Info );
			system.debug('#### Myr_LoginHandlerRest_Cls - <updateVehicleRelations> - END');
		} catch( Exception e ) {
			addLog( user.AccountId, 'updateVehicleRelations', 'UserId='+userId
					, e.getTypeName()
					, 'Problem when trying to update vehicle relations: ' +e.getMessage()
					, beginTime
					, INDUS_Logger_CLS.ErrorLevel.Warn );
		}
	}

	// ----- GET VEHICLE FROM RUBICS ---------------

	/**
	* Call RBX/SIC to retrieve/update the vehicles of the current customer
	* @param parentLogId, the id of the parent log
	* @param userId, the id of the user
	* @param brand, the brand, either Renault, either Dacia
	*/
	private static MyRenault_Session getVehiclesFromRubics( String userId, String brand ) {
		Country_Info__c countryInfo;
		Account acc;
		User usr;
		system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics> - BEGIN - userId=' + userId);
		try {
			usr = [SELECT Id, AccountId FROM User WHERE Id=:userId];
			acc = [SELECT Id, Country__c, Bcs_Id__c, Bcs_Id_Dacia__c FROM Account WHERE Id = :usr.AccountId];
			//Verify that the webservice that will be called is RBX. In this case, we try to get the vehicles
			countryInfo = Country_Info__c.getInstance( acc.Country__c );
			if ( null != countryInfo ) {
				system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics> - datasource='+countryInfo.DataSource__c
							+', enablePersData='+countryInfo.Myr_EnablePersDataCall__c
							+', enableLoginVeh='+countryInfo.Myr_Enable_Login_GetVehicle__c
							+', synchronous='+countryInfo.Myr_Enable_Login_GetVehicle_Synchrone__c);
				String options = 'RBX|SIC';
				if( options.containsIgnoreCase(countryInfo.DataSource__c) && countryInfo.Myr_EnablePersDataCall__c && countryInfo.Myr_Enable_Login_GetVehicle__c) {
					system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics> - RBX/SIC detected');
					if( countryInfo.Myr_Enable_Login_GetVehicle_Synchrone__c == true ) {
						getVehiclesFromRubics_Sync( acc.Id, brand );
					} else {
						getVehiclesFromRubics_ASync( acc.Id, brand );
					}
				}
			}
		} catch (Exception e) {
			system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics> - BEGIN - exception=' + e.getMessage());
		}

		return (new MyRenault_Session(acc.Country__c,acc.Id));
	}

	/**
	* Call RBX/SIC to retrieve/update the vehicles of the current customer - SYNCHRONOUS mode
	* @param accId, the id of the account
	* @param brand, the brand, either Renault, either Dacia
	*/
	private static void getVehiclesFromRubics_Sync( String accId, String brand ) {
		system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics_Sync> - BEGIN');
		getVehiclesFromRubics_Inner( accId, brand );
	}

	/**
	* Call RBX/SIC to retrieve/update the vehicles of the current customer - ASYNCHRONOUS mode
	* @param userId, the id of the user
	* @param brand, the brand, either Renault, either Dacia
	*/
	@future(callout=true)
	private static void getVehiclesFromRubics_ASync( String accId, String brand ) {
		system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics_ASync> - BEGIN');
		getVehiclesFromRubics_Inner( accId, brand );
	}

	/**
	* Inner function called in async or sync mode that make the call to Personal Data to retrieve the vehicles
	* @param accId, the id of the account to update
	* @param brand, the brand, either Renault, either Dacia
	*/
	private static void getVehiclesFromRubics_Inner( String accId, String brand ) {
		system.debug('### Myr_LoginHandlerRest_Cls - <getVehiclesFromRubics_Inner> - BEGIN accId='+accId+', brand='+brand);
		//prepare the call and update the vehicle
		Myr_ManageAccount_PersDataUpdate_Cls persDataUpdate = new Myr_ManageAccount_PersDataUpdate_Cls( accId, brand, null, Myr_LoginHandlerRest_Cls.class.getName());
		//Call Personal Data only by using Personal Data Id
		persDataUpdate.callPersonalData( new List<String>{Myr_ManageAccount_PersDataCall_Cls.Keys.PID.name()} ); //response has no importance currently
		//update the informations
		persDataUpdate.updateVehicles();
	}

	
	//-------------------------------------------------------------------------------------------------------------------------
	//------ LOG FUNCTIONS
	/** Add the logs for this webservice in synchronous mode: no risk of a mixed_dml exception **/
    private static void addLog(String accId, String function, String record, String exceptType, String message, Long beginTime, INDUS_Logger_CLS.ErrorLevel errorLevel) {
		String runId = null;
		String parentLogId = null;
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String apexClass = Myr_LoginHandlerRest_Cls.class.getName();
		Long endTime = system.now().getTime();

		//insert the log
		INDUS_Logger_CLS.addGroupedLogFull( runId, parentLogId, project, apexClass, function, exceptType, message, String.valueOf(errorLevel), accId, null, null, beginTime, endTime, record );
    }
    
	//Used to keep in mind session informations of the user connected
	private class MyRenault_Session {
		String Country;
		Id Account_Id;
		public MyRenault_Session(String c, Id a){
			Country=c;
			Account_Id=a;
		}
	 }
}