@isTest
private class WSMarketingCloudTest {

	private static testMethod void test() {
        Id dealerRecordTypeId   = Utils.getRecordTypeId('Account','Network_Site_Acc');
        Id parentId             = SandboxBuild.criaConcessionariaGrupo(dealerRecordTypeId);
        Account dealer          = SandboxBuild.criaConcessionaria(parentId, dealerRecordTypeId);
        
        dealer.DealerDirectorPhone__c = '5511987623411';
        dealer.DealerDirectorFstName__c = 'Luiz Prandini';
        
        dealer.DealerManagerPhone__c = '5511987623411';
        dealer.DealerManagerFstName__c = 'Joaquim Gonzales';
        
        dealer.OwnerPhone__c = '5511987623411';
        dealer.DealerOwner__c =  'Maria Gonzales';
        
        update dealer;
        
        WSMarketingCloud cls = new WSMarketingCloud();
        WSMarketingCloud.login();
        WSMarketingCloud.sendSMS(dealer, 3);
        
	}

}