public class ReembolsoController {

    public Lead lead { get;set; }
    
    public List<CampaignMember> leadList { get;set; }
    private Datetime dataIni { get; set; }
    private Datetime dataFim { get; set; }
    
    public List<SelectOption> statusSelectOption {
        get {
            List<SelectOption> selectList = new List<SelectOption>();
            selectList.add(new SelectOption('Rescued', 	'Resgatado'));
            selectList.add(new SelectOption('Valid', 	'Validado'));
            
            return selectList;
        } 
    }
    
    public ReembolsoController() {
        lead = new Lead();
        pesquisar();
    }
    
    public PageReference pesquisar() {
        dataIni = lead.BusinessMobilePhoneDateUpdated__c;
        if(dataIni != null) {
            dataIni = Datetime.newInstanceGmt(
                dataIni.yearGmt(), dataIni.monthGmt(), dataIni.dayGmt(), 0, 0, 0);  
        }
        dataFim = lead.BusinessPhoneDateUpdated__c;
        if(dataFim != null) {
            dataFim = Datetime.newInstanceGmt(
                dataFim.yearGmt(), dataFim.monthGmt(), dataFim.dayGmt(), 23, 59, 59);  
        }
        
        System.debug('Data Ini:' + dataIni);
        System.debug('Data Fim:' + dataFim);
        
        leadList = Database.query(
            'SELECT Lead.Id, Campaign.Name,Campaign.Description, Lead.Name, Lead.CPF_CNPJ__c, Lead.Voucher_Protocol__c, Lead.Description, Campaign.EndDate, Lead.Status ' + 
            'FROM CampaignMember ' + 
            'WHERE Lead.RecordTypeId = \'' + Utils.getRecordTypeId('Lead', 'Voucher') + '\' ' +
            (!String.isEmpty(lead.Voucher_Protocol__c) ? 
             ' AND (Lead.Voucher_Protocol__c = \'' + lead.Voucher_Protocol__c + 
             '\' OR Lead.CPF_CNPJ__c = \'' + lead.Voucher_Protocol__c + '\') ' : '') +
            (!String.isEmpty(lead.Vehicle_Invoice__c) ? ' AND Lead.Vehicle_Invoice__c = \'' + lead.Vehicle_Invoice__c + '\'' : '') +
            (!String.isEmpty(lead.VIN__c) ? ' AND Lead.VIN__c = \'' + lead.VIN__c + '\'' : '') +
            (!String.isEmpty(lead.Status) ? ' AND Lead.Status = \'' + lead.Status + '\''  : '') + 
            
            (dataIni != null && dataFim != null     ? 
             'AND Lead.CreatedDate >=: dataIni AND Lead.CreatedDate <=: dataFim ' : '' )  +
            ' LIMIT 700'
        );
        
        return null;
    }
    
    public PageReference limpar() {
        lead = new Lead();
        pesquisar();
        return null;
    }
}