@isTest
private class DistrinetLogicCodeBatchTest {
	
	public static List<DistrinetLogicCode__c> dlcList;
	public static DistrinetLogicCode__c cco;
	
	static{
		
		Opportunity opp = new Opportunity();
		opp.Name = 'Oportunidade (Lucas Andrade)';
		opp.StageName = 'Identified';
        opp.CloseDate = system.today();
        insert opp;
        
        Quote quote = new Quote();
        quote.Name = 'ORÇAMENTO (Lucas Andrade)';
        quote.Status = 'Open';
        quote.OpportunityId = opp.Id;
        insert quote;
        
        Quote qt = [ SELECT Id, QuoteNumber FROM Quote WHERE Id =: quote.Id];

		dlcList = new List<DistrinetLogicCode__c>();

		cco = new DistrinetLogicCode__c(
				PC_LogicCode__c = 'CCO',
				PC_CustomerNumberInternalSedre__c = '1234678901234567890',
				PC_ChassisNumberCurrent__c = '12345678901234567',
				PC_SemiclairModel__c = '123', 
				PC_SemiclairVersion__c = '123456789012', 
				PC_SemiclairOptional__c = '1234567890', 
				PC_ColorBoxCriterion__c = '123456', 
				PC_SemiclairUpholstered__c = '123456',
				PC_SemiclairHarmony__c = '123456', 
				PC_SemiclairColor__c = '123456',
				PC_FirstName__c = 'Nome1',
				PC_PersonalOrCompanyClientName__c = 'Corp. Name',
				PC_CustomerCompany__c = 'Test Inc.',
				PC_WayNome__c = 'Some Way',
				PC_City__c = 'São Paulo',
				PC_State__c = 'SP',
				PC_PersonalPhone__c = '1140028922',
				PC_MobilePhone__c = '1140028922',
				PC_ErrorMessage__c = '',
				PC_OrderNumberSedre__c = '123456',
				PC_OwnerAccountControl__c = '123456',
				PC_ExternalCustomerCodeSedre__c = '123456', 
				PC_DmsOrderNumber__c = qt.QuoteNumber
			);
		dlcList.add(cco);
		DistrinetLogicCode__c afc = new DistrinetLogicCode__c(
			PC_LogicCode__c = 'AFC',
			PC_CustomerNumberInternalSedre__c = '1234678901234567890',
			PC_ChassisNumberCurrent__c = '12345678901234567',
			PC_SemiclairModel__c = '123', 
			PC_SemiclairVersion__c = '123456789012', 
			PC_SemiclairOptional__c = '1234567890', 
			PC_ColorBoxCriterion__c = '123456', 
			PC_SemiclairUpholstered__c = '123456',
			PC_SemiclairHarmony__c = '123456', 
			PC_SemiclairColor__c = '123456',
			PC_FirstName__c = 'Nome1',
			PC_PersonalOrCompanyClientName__c = 'Corp. Name',
			PC_CustomerCompany__c = 'Test Inc.',
			PC_WayNome__c = 'Some Way',
			PC_City__c = 'São Paulo',
			PC_State__c = 'SP',
			PC_PersonalPhone__c = '1140028922',
			PC_MobilePhone__c = '1140028922',
			PC_ErrorMessage__c = '',
			PC_OrderNumberSedre__c = '123456',
			PC_OwnerAccountControl__c = '123456',
			PC_ExternalCustomerCodeSedre__c = '123456', 
			PC_DmsOrderNumber__c = qt.QuoteNumber
		);
		dlcList.add(afc);
		DistrinetLogicCode__c dsf = new DistrinetLogicCode__c(
			PC_LogicCode__c = 'DSF',
			PC_CustomerNumberInternalSedre__c = '1234678901234567890',
			PC_ChassisNumberCurrent__c = '12345678901234567',
			PC_SemiclairModel__c = '123', 
			PC_SemiclairVersion__c = '123456789012', 
			PC_SemiclairOptional__c = '1234567890', 
			PC_ColorBoxCriterion__c = '123456', 
			PC_SemiclairUpholstered__c = '123456',
			PC_SemiclairHarmony__c = '123456', 
			PC_SemiclairColor__c = '123456',
			PC_FirstName__c = 'Nome1',
			PC_PersonalOrCompanyClientName__c = 'Corp. Name',
			PC_CustomerCompany__c = 'Test Inc.',
			PC_WayNome__c = 'Some Way',
			PC_City__c = 'São Paulo',
			PC_State__c = 'SP',
			PC_PersonalPhone__c = '1140028922',
			PC_MobilePhone__c = '1140028922',
			PC_ErrorMessage__c = '',
			PC_OrderNumberSedre__c = '123456',
			PC_OwnerAccountControl__c = '123456',
			PC_ExternalCustomerCodeSedre__c = '123456', 
			PC_DmsOrderNumber__c = qt.QuoteNumber
		);
		dlcList.add(dsf);
		DistrinetLogicCode__c aco = new DistrinetLogicCode__c(
			PC_LogicCode__c = 'ACO',
			PC_CustomerNumberInternalSedre__c = '1234678901234567890',
			PC_ChassisNumberCurrent__c = '12345678901234567',
			PC_SemiclairModel__c = '123', 
			PC_SemiclairVersion__c = '123456789012', 
			PC_SemiclairOptional__c = '1234567890', 
			PC_ColorBoxCriterion__c = '123456', 
			PC_SemiclairUpholstered__c = '123456',
			PC_SemiclairHarmony__c = '123456', 
			PC_SemiclairColor__c = '123456',
			PC_FirstName__c = 'Nome1',
			PC_PersonalOrCompanyClientName__c = 'Corp. Name',
			PC_CustomerCompany__c = 'Test Inc.',
			PC_WayNome__c = 'Some Way',
			PC_City__c = 'São Paulo',
			PC_State__c = 'SP',
			PC_PersonalPhone__c = '1140028922',
			PC_MobilePhone__c = '1140028922',
			PC_ErrorMessage__c = '',
			PC_OrderNumberSedre__c = '123456',
			PC_OwnerAccountControl__c = '123456',
			PC_ExternalCustomerCodeSedre__c = '123456', 
			PC_DmsOrderNumber__c = qt.QuoteNumber
		);
		dlcList.add(aco);
		DistrinetLogicCode__c mco = new DistrinetLogicCode__c(
			PC_LogicCode__c = 'MCO',
			PC_CustomerNumberInternalSedre__c = '1234678901234567890',
			PC_ChassisNumberCurrent__c = '12345678901234567',
			PC_SemiclairModel__c = '123', 
			PC_SemiclairVersion__c = '123456789012', 
			PC_SemiclairOptional__c = '1234567890', 
			PC_ColorBoxCriterion__c = '123456', 
			PC_SemiclairUpholstered__c = '123456',
			PC_SemiclairHarmony__c = '123456', 
			PC_SemiclairColor__c = '123456',
			PC_FirstName__c = 'Nome1',
			PC_PersonalOrCompanyClientName__c = 'Corp. Name',
			PC_CustomerCompany__c = 'Test Inc.',
			PC_WayNome__c = 'Some Way',
			PC_City__c = 'São Paulo',
			PC_State__c = 'SP',
			PC_PersonalPhone__c = '1140028922',
			PC_MobilePhone__c = '1140028922',
			PC_ErrorMessage__c = '',
			PC_OrderNumberSedre__c = '123456',
			PC_OwnerAccountControl__c = '123456',
			PC_ExternalCustomerCodeSedre__c = '123456', 
			PC_DmsOrderNumber__c = qt.QuoteNumber
		);
		dlcList.add(mco);
		DistrinetLogicCode__c cl1 = new DistrinetLogicCode__c(
			PC_LogicCode__c = 'CL1',
			PC_CustomerNumberInternalSedre__c = '1234678901234567890',
			PC_ChassisNumberCurrent__c = '12345678901234567',
			PC_SemiclairModel__c = '123', 
			PC_SemiclairVersion__c = '123456789012', 
			PC_SemiclairOptional__c = '1234567890', 
			PC_ColorBoxCriterion__c = '123456', 
			PC_SemiclairUpholstered__c = '123456',
			PC_SemiclairHarmony__c = '123456', 
			PC_SemiclairColor__c = '123456',
			PC_FirstName__c = 'Nome1',
			PC_PersonalOrCompanyClientName__c = 'Corp. Name',
			PC_CustomerCompany__c = 'Test Inc.',
			PC_WayNome__c = 'Some Way',
			PC_City__c = 'São Paulo',
			PC_State__c = 'SP',
			PC_PersonalPhone__c = '1140028922',
			PC_MobilePhone__c = '1140028922',
			PC_ErrorMessage__c = '',
			PC_OrderNumberSedre__c = '123456',
			PC_OwnerAccountControl__c = '123456',
			PC_ExternalCustomerCodeSedre__c = '123456', 
			PC_DmsOrderNumber__c = qt.QuoteNumber
		);
		dlcList.add(cl1);
	}

	@isTest static void shouldSuccess() {
		
		Database.insert(dlcList);
		
		Test.startTest();
		DistrinetLogicCodeBatch b = new DistrinetLogicCodeBatch();
		database.executebatch(b);
		Test.stopTest();
	}

	@isTest static void shouldTestScheduler() {
		
		Database.insert(cco);
		
		Test.startTest();
		System.schedule('Distrinet Job Teste', '0 0 * * * ?', new DistrinetLogicCodeScheduler());
		Test.stopTest();
	}
	
	@isTest static void shouldUpdateLogicCodeStatus() {
		
		dlcList[0].PC_DmsOrderNumber__c = null;
		Database.insert(dlcList);
		
		Test.startTest();
		
		DistrinetLogicCodeBatch b = new DistrinetLogicCodeBatch();
		database.executebatch(b);
		
		Test.stopTest();
	}
	
}