/*****************************************************************************************
    Name    : Test_Batchtoupdatevehicle
    Desc    : This class is uded to test  code coverage of Bacthtoupdatevehicle 
    Approach: Batchapex Interface
                
                                                
 Modification Log : 
---------------------------------------------------------------------------------------
Developer                       Date                Description
---------------------------------------------------------------------------------------
Praveen Ravula                  4/26/2013           Created 

******************************************************************************************/

@isTest
public class Test_Batchtoupdatevehicle{

  
  @isTest static void batchtest(){

      // Prepare test data
        List<veh_veh__c> lstvehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
        system.assert(lstvehicle .size() > 0);
        Batchtoupdatevehicle btuv = new Batchtoupdatevehicle();
        Database.BatchableContext BC;
        

    // Start test
        Test.startTest();

         Test.setMock(WebServiceMock.class, new Test_WS01_ApvGetDetVehXml_WebServiceMock());

         btuv.start(BC);
         btuv.execute(BC, lstvehicle);  
         btuv.finish(BC);
        
        
        // Stop test
        Test.stopTest();
    }

// second batch test
  @isTest static void batchtest2(){

     // Prepare test data
        List<veh_veh__c> lstvehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
        system.assert(lstvehicle .size() > 0);
        Batchtoupdatevehicle2 btuv = new Batchtoupdatevehicle2();
        Database.BatchableContext BC;
        

    // Start test
        Test.startTest();

         Test.setMock(WebServiceMock.class, new Test_WS01_ApvGetDetVehXml_WebServiceMock());

         btuv.start(BC);
         btuv.execute(BC, lstvehicle);  
         btuv.finish(BC);
         
        
        // Stop test
        Test.stopTest();
  } 
}