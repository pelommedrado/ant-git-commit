/**
 * @author Vinoth Baskaran
 * @date 17/07/2015
 * @description This Class is an Custom Soap service and it will create case and attachment according to the user input.
 */

global class Rforce_W2CwithAttachmentService{
/**
 * @author Vinoth Baskaran
 * @date 17/07/2015
 * @description It accepts the Casedetails from the external service.
 * @param case Details
 * @return String value according to request
 */
 
    webService static String createCaseAndAttachment(Rforce_W2CSchema_CLS.Casedetails caseDetails){       
        Case caseDetailMapping,cs; 
        boolean bCaseStatus;
        Integer iAttachCount=0;
        boolean bChkAttachment=false;
        String  sReturnValue;
        List<Attachment>attchmentList;
        /*Default Detail mapping of Rforce Required case Fields */
        if(caseDetails.description!='' && caseDetails.type!='' && caseDetails.caseBrand!='' && caseDetails.origin!='' && caseDetails.subSource!='' && caseDetails.suppliedEmail!='' && caseDetails.country!='' && caseDetails.subType != null){
        caseDetailMapping=Rforce_CreateCaseMapping_CLS.createCaseMapping(caseDetails);                         
        }
        /*End ofDefault Detail mapping of Rforce Required case Fields*/
        
        if(caseDetailMapping != null){        
        bCaseStatus = Rforce_W2CService_CLS.createCase(caseDetails,caseDetailMapping);            
        }
        else{
        return System.label.Rforce_CAS_Requiredfields;
        }
    
        /*Default Detail mapping to create Attachment  */
        if(bCaseStatus==true){                    
        cs=[Select CaseNumber from Case where Id=:caseDetailMapping.Id];                  
            if(caseDetails.attachmentDetails != null){                      
                for(Rforce_W2CSchema_CLS.CaseAttachments attch :caseDetails.attachmentDetails){                
                    if(String.IsNotBlank(attch.attachmentName)  || String.IsNotBlank(attch.attachmentContentType) || attch.attachmentBody.size()>0){   
                    bChkAttachment=true;                    
                    iAttachCount++;                                       
                    }                                                              
                }
            }
            if(bChkAttachment){
            attchmentList= Rforce_CreateAttachMapping_CLS.addAttachment(caseDetails,caseDetailMapping);          
            }
            else
            {
            return System.label.Rforce_CAS_Case+' '+cs.CaseNumber+' '+System.label.Rforce_CAS_Sucessfully_Inserted;
            }
                                    
        } 
        else
        {
        return system.label.Rforce_CAS_ErrorMsg;
        }                    
         
                
           if(bCaseStatus== true && attchmentList.size() >0){  
           sReturnValue=Rforce_W2CService_CLS.getResponse(attchmentList,caseDetailMapping,iAttachCount);                
          }    
              else if(bCaseStatus== true && attchmentList.size()==0 && iAttachCount > 0 && bChkAttachment==true){
               sReturnValue=Rforce_W2CService_CLS.getResponseWithoutAttach(caseDetailMapping,attchmentList); 
              }                      
    return sReturnValue;
    }
    
}