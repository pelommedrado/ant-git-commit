public with sharing class cloneOfferController {
    
    public String idRegistro {get;set;}
    
    public cloneOfferController(ApexPages.StandardController controller){
        
        idRegistro = apexpages.currentpage().getparameters().get('id');
        System.debug('OFFER ID: ' + idRegistro);
    }

}