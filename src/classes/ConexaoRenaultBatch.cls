global class ConexaoRenaultBatch implements Schedulable, Database.Batchable<sObject> {

	String query = null;

	global ConexaoRenaultBatch() {
		query = buildQueryFatura();
	}

	global void execute(SchedulableContext sc) {
  	System.debug('Iniciando a execucao do batch');
    ConexaoRenaultBatch fatBatch = new ConexaoRenaultBatch();
    Database.executeBatch(fatBatch);
  }

	private String buildQueryFatura() {
		return 'SELECT Id, NFDTANFI_Mirror__c, PV_Survey_Date__c'
			+ ' FROM FaturaDealer__c'
			+ ' WHERE NFTIPREG__c = \'10\' AND NFCODOPR_Mirror__c = \'19\''
			+ ' AND NFTIPCLI__c = \'F\''
			+ ' AND InvoiceDateFilled__c = TRUE AND PV_Survey_Date__c = NULL'
			+ ' AND RecordTypeId = \'' + FaturaDealerServico.VALID + '\'';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('execute()');
		ConexaoRenaultBo.getInstance().executeFatura(scope);
	}

	global void finish(Database.BatchableContext BC) {
	}
}