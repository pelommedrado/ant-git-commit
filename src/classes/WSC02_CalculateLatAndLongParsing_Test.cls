/**
 *	Class	-	WSC02_CalculateLatAndLongParsing_Test
 *	Author	-	Suresh Babu
 *	Date	-	01/11/2012
 *
 *	#01 <Suresh Babu> <01/11/2012>
 		Test class to test WSC02_CalculateLatAndLongParsing class.
 */
@isTest 
private class WSC02_CalculateLatAndLongParsing_Test {
	  static List<Account> getTestAccount(){
	    List<Account> lstAccountRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertDealerAccount_ForCalculateLatAndLong();
	    return lstAccountRecords;
	  }
    @isTest static void testCallout() {
		String accountName = 'AR Motors Alphaville';
		Id recordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Network_Site_Acc' );
		//List<Account> lstAccountRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertDealerAccount_ForCalculateLatAndLong();
		List<Account> lstAccountRecords = getTestAccount();
		Account accountForCallout = VFC12_AccountDAO.getInstance().fetchNetworkSiteAccountUsingName(accountName, recordTypeId);
		
		system.Debug(' Account -->'+accountForCallout);
		system.Debug(' postal code -->'+accountForCallout.ShippingPostalCode);
		
		String Street, City, State, PostalCode, Country;
					
		Street 		= 	( accountForCallout.ShippingStreet == null ? '' : accountForCallout.ShippingStreet );
		City		=	( accountForCallout.ShippingCity == null ? '' : accountForCallout.ShippingCity );
		State		=	( accountForCallout.ShippingState == null ? '' : accountForCallout.ShippingState );
		PostalCode	=	( accountForCallout.ShippingPostalCode == null ? '' : accountForCallout.ShippingPostalCode );
		Country		=	( accountForCallout.ShippingCountry == null ? '' : accountForCallout.ShippingCountry );
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new WSC02_MockHttpResponseGenerator_Test());
        WSC02_CalculateLatAndLongParsing.calloutToGoogleToGetLatAndLong(accountForCallout.Id, Street, City, State, PostalCode, Country);
        Test.stopTest();
    }
    
    @isTest static void testInsertion(){
    	WSC02_CalculateLatAndLongParsing webserviceController = new WSC02_CalculateLatAndLongParsing();
    	Account acc = VFC03_InsertSObjectsRecordsForTestClass.getInstance().singleAccountInsertion();
    }
    
    @isTest static void testUpdation(){
    	String accountName = 'AR Motors Alphaville';
		Id recordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Network_Site_Acc' );
    	//Account accountForCallout = VFC12_AccountDAO.getInstance().fetchNetworkSiteAccountUsingName(accountName, recordTypeId);
    	//List<Account> lstAccountRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertDealerAccount_ForCalculateLatAndLong();
		List<Account> lstAccountRecords = getTestAccount();
		Account accountForCallout = VFC12_AccountDAO.getInstance().fetchNetworkSiteAccountUsingName(accountName, recordTypeId);
    	String Street, City, State, PostalCode, Country;
					
		Street 		= 	( accountForCallout.ShippingStreet == null ? '' : accountForCallout.ShippingStreet );
		City		=	( accountForCallout.ShippingCity == null ? '' : accountForCallout.ShippingCity );
		State		=	( accountForCallout.ShippingState == null ? '' : accountForCallout.ShippingState );
		PostalCode	=	( accountForCallout.ShippingPostalCode == null ? '' : accountForCallout.ShippingPostalCode );
		Country		=	( accountForCallout.ShippingCountry == null ? '' : accountForCallout.ShippingCountry );
		
		
    	if( Street.contains(' ') ){
			Street = Street.replace(' ', '');
 		}
 		else{
 			Street = ' ' + Street;
 		}
 	   	if( City.contains(' ') ){
 	   		City = City.replace(' ', '');
 	   	}
 	   	else{
 	   		City = ' ' +City ;
 	   	}
 	   	if( State.contains(' ') ){
 	   		State = State.replace(' ', '');
 	   	}
 	   	else{
 	   		State = ' ' + State;
 	   	}
		if( PostalCode.contains(' ') ){
 	   		PostalCode = PostalCode.replace(' ', '');
 	   	}
 	   	else{
 	   		PostalCode = ' ' + PostalCode;
 	   	}
 	   	
 	   	if( Country.contains(' ') ){
 	   		Country = Country.replace(' ', '');
 	   	}
 	   	else{
 	   		Country = ' ' + Country;
 	   	}
 	   	
 	   	accountForCallout.ShippingStreet = Street;
 	   	accountForCallout.ShippingCity = City;
 	   	accountForCallout.ShippingState = State;
 	   	accountForCallout.ShippingPostalCode = PostalCode;
 	   	accountForCallout.ShippingCountry = Country; 
    	
    	VFC12_AccountDAO.getInstance().updateData( accountForCallout );
    }
}