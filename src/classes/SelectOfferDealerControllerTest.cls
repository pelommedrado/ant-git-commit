@isTest
private class SelectOfferDealerControllerTest {
    private static Model__c 	model 		= null;
    private static Optional__c 	optional1 	= null;
    private static Optional__c 	optional2 	= null;
    private static PVVersion__c version 	= null;
    private static Account	 	dealerAcc 	= null;
    private static Account 		dealerAcc2 	= null;
    
    static {
        model = new Model__c(
            Program__c = 'X92', Phase__c = '2',
            Model_Spec_Code__c = 'L9M', Id_Milesime__c = 'BACI', Market__c = 'abc',
            Status__c = 'Active', Model_PK__c = 'L9M-2'
        );
        INSERT model;
        
        version = new PVVersion__c(
            name = 'Expression 1.6 16v Automático',
            Model__c  = model.id,
            Version_Id_Spec_Code__c = 'VEC043_BRES',
            Price__c = 40000.00,
            PVC_Maximo__c = 100,
            PVR_Minimo__c = 100
        );
        INSERT version;
        
        optional1 = new Optional__c(
            Version__c = version.id,
            name = '-',
            Type__c = 'Item',
            Optional_Code__c = 'DPSEC',
            Amount__c = 0.00
        );
        INSERT optional1;
        
        optional2 = new Optional__c(
            Version__c = version.id,
            name = '-',
            Type__c = 'Paint',
            Optional_Code__c = 'DPEC',
            Amount__c = 0.00
        );
        INSERT optional2;
        
        /*PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
Model__c = model.Id,
Start_Date__c = System.today().addMonths( -1 ),
End_Date__c = System.today().addMonths( 1 ),
Type_of_Action__c = 'abc',
Status__c = 'Active'
);
INSERT commercialAction;

PVCall_Offer__c callOffer = new PVCall_Offer__c(
Commercial_Action__c = commercialAction.Id,
Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
Call_Offer_End_Date__c = System.today().addMonths( 1 ),
Minimum_Entry__c = 100.0,
Period_in_Months__c = 5,
Month_Rate__c = 0.99
);
INSERT callOffer;*/
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        INSERT manager;
        
        dealerAcc = new Account(
            Name = 'Concessionaria teste',
            IDBIR__c = '123ABC123',
            RecordTypeId = '012D0000000KAoH',
            Country__c = 'Brazil',
            NameZone__c = 'R2',
            ShippingState = 'SP',
            ShippingCity = 'Sao Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true    
        );
        INSERT dealerAcc;
        
        dealerAcc2 = new Account(
            Name = 'Concessionaria teste2',
            IDBIR__c = '123ABC124',
            RecordTypeId = '012D0000000KAoH',
            Country__c = 'Brazil',
            NameZone__c = 'R3',
            ShippingState = 'SP',
            ShippingCity = 'Sao Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true
            
        );
        INSERT dealerAcc2;
        
        Account dealerAcc3 = new Account(
            Name = 'Concessionaria teste3',
            IDBIR__c = '123ABC094',
            RecordTypeId = '012D0000000KAoH',
            Country__c = 'Brazil',
            NameZone__c = 'R3',
            ShippingState = 'SP',
            ShippingCity = 'Sao Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true
            
        );
        Database.insert( dealerAcc3 );
    }
    
    static testMethod void test1() {
        Offer_Item__c pricingTemplate = new Offer_Item__c(
            Name = 'Pricing Template', 
            RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Pricing_Template'),
            Code__c = 'PT'
        );
        Database.insert(pricingTemplate);
        
        Group_Offer__c groupOffer = new Group_Offer__c(
            Type_of_Offer__c = 'Cooperada',
            Type_of_Action__c = 'Internet',
            Date_Start_Offer__c =  (System.now()).date(),
            Date_End_Offer__c =  (System.now().addMonths(1)).date(),
            Status__c = 'Active',
            Number_Offer__c='1'
        );
        Database.insert( groupOffer );
        
        Offer__c oferta = new Offer__c(
            Status__c = 'ACTIVE',
            Group_Offer__c = groupOffer.Id,
            Stage__c = 'Approved',
            Model__c = model.Id,
            Version__c = version.Id,
            Optional__c = optional1.id,
            Painting__c = optional2.id,
            Minimum_Input__c = 10,
            Number_Of_Installments__c = 40,
            Monthly_Tax__c = 0,
            Coefficient__c = 0.2356,
            Pricing_Template__c = 'À vista',
            Value_From__c = 29900.00,
            ValueTo__c = 27900.00,
            Entry_Value__c = 25110.00,
            Installment_Value__c = 77.00,
            Total_Inventory_Vehicle__c = 3,
            Pricing_Template_Lookup__c = pricingTemplate.Id
        );
        Database.insert(oferta);
        
        Dealer_Offer__c dealerOffer = new Dealer_Offer__c(
            Offer__c = oferta.id,
            Dealer__c = dealerAcc.id,
            Status__c = 'Active'
        );
        Database.insert(dealerOffer);
        
        Dealer_Offer__c dealerOffer2 = new Dealer_Offer__c(
            Offer__c = oferta.id,
            Dealer__c = dealerAcc2.id,
            Status__c = 'Active'
        );
        Database.insert(dealerOffer2);
        
        Apexpages.currentPage().getParameters().put('id', oferta.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(oferta);
        
        SelectOfferDealerController ctl = new SelectOfferDealerController(stdCont);
        
        Apexpages.currentPage().getParameters().put('param', 'R3');
        Apexpages.currentPage().getParameters().put('action', '-');
        ctl.atualizarRegiao();
        Apexpages.currentPage().getParameters().put('param', 'R3');
        Apexpages.currentPage().getParameters().put('action', '+');
        ctl.atualizarRegiao();
        
        Apexpages.currentPage().getParameters().put('param', 'SP');
        Apexpages.currentPage().getParameters().put('action', '-');
        ctl.atualizarEstado();
        Apexpages.currentPage().getParameters().put('param', 'SP');
        Apexpages.currentPage().getParameters().put('action', '+');
        ctl.atualizarEstado();
        
        Apexpages.currentPage().getParameters().put('param', 'Sao Paulo');
        Apexpages.currentPage().getParameters().put('action', '-');
        ctl.atualizarCidade();
        Apexpages.currentPage().getParameters().put('param', 'Sao Paulo');
        Apexpages.currentPage().getParameters().put('action', '+');
        ctl.atualizarCidade();
        
        Apexpages.currentPage().getParameters().put('param', dealerAcc2.Id);
        Apexpages.currentPage().getParameters().put('action', '-');
        ctl.atualizarDealer();
        Apexpages.currentPage().getParameters().put('param', dealerAcc2.Id);
        Apexpages.currentPage().getParameters().put('action', '+');
        ctl.atualizarDealer();
        
        ctl.salvar();
    }
    
}