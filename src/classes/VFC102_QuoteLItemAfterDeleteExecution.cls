/**
* Classe que será disparada pela trigger do objeto QuoteLineItem após a deleção dos registros.
* @author Felipe Jesus Silva.
*/
public class VFC102_QuoteLItemAfterDeleteExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{	
		List<QuoteLineItem> lstSObjQuoteLineItem = (List<QuoteLineItem>) lstOldData;
		
		this.cancelVehicleBookings(lstSObjQuoteLineItem);
	}
	
	private void cancelVehicleBookings(List<QuoteLineItem> lstSObjQuoteLineItem)
	{
		Set<String> setQuoteId = new Set<String>();
		
		for(QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem)
		{
			if(String.isNotEmpty(sObjQuoteLineItem.Vehicle__c))
			{
				setQuoteId.add(sObjQuoteLineItem.QuoteId);
			}			
		}
		
		if(!setQuoteId.isEmpty())
		{
			try
			{
				VFC86_VehicleBookingBO.getInstance().updateVehicleBookingsFromQuotesForCanceledStatus(setQuoteId);
			}
			catch(VFC89_NoDataFoundException ex)
			{				
			}
		}
	}
}