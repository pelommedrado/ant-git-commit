/*****************************************************************************************
    Name    : Account_BeforeUpdate_Trigger
    Desc    : This is to update Account country and language fields based on user country details
    Approach: Used Custom settings to store country information
    Author  : Praneel PIDIKITI (Atos Integration)
    Project : Rforce
******************************************************************************************/

public with sharing class AccountTriggerHandler {
    public static void onAfterUpdate(list <Account> listAccount, boolean isInsert, Map <Id, Account> oldMap){
  
        String comagrmnt;
        set<id>  acclistyes = new set <id>();
        set<id>  acclistno = new set <id>();
        List<Account> accList = new List<Account>();
     //When all the global communication agreemente are No the communication agrement is made yes if all are no then it is made no
        for (Account acc : listAccount) {
            if ((acc.ComAgreemt__c == system.label.Acc_CommAgremt_Partial) && (acc.Address__pc == system.label.Acc_CommAgremt_Yes && acc.ProfPhone__pc == system.label.Acc_CommAgremt_Yes
                 && acc.PersPhone__pc == system.label.Acc_CommAgremt_Yes && acc.ProEmailAddress__pc == system.label.Acc_CommAgremt_Yes && acc.PersEmail__pc == system.label.Acc_CommAgremt_Yes
                 && acc.ProfMobiPhone__pc == system.label.Acc_CommAgremt_Yes && acc.PersMobiPhone__pc == system.label.Acc_CommAgremt_Yes && acc.SMS__pc == system.label.Acc_CommAgremt_Yes )) {
                acclistyes.add(acc.id);
            }else if ((acc.ComAgreemt__c == system.label.Acc_CommAgremt_Partial) && (acc.Address__pc == system.label.Acc_CommAgremt_No && acc.ProfPhone__pc == system.label.Acc_CommAgremt_No
                       && acc.PersPhone__pc == system.label.Acc_CommAgremt_No && acc.ProEmailAddress__pc == system.label.Acc_CommAgremt_No && acc.PersEmail__pc == system.label.Acc_CommAgremt_No
                       && acc.ProfMobiPhone__pc == system.label.Acc_CommAgremt_No && acc.PersMobiPhone__pc == system.label.Acc_CommAgremt_No && acc.SMS__pc == system.label.Acc_CommAgremt_No )) {
                acclistno.add(acc.id);
            }
        }
        for (Account a : [Select ComAgreemt__c from account where id IN : acclistyes]) {
            a.ComAgreemt__c = system.label.Acc_CommAgremt_Yes;
            accList.add(a);
        }
        for (Account a : [Select ComAgreemt__c from account where id IN : acclistno]) {
            a.ComAgreemt__c = system.label.Acc_CommAgremt_No;
            accList.add(a);
        }
        try{
            update accList;
        }catch(Exception e){
            system.debug(e);
        } 
           
        //Added by sumanth on 20/03/2015 for Intaile Paris
        Rforce_SpecialCustomerStatus.updatespecialcustomerstatus(listAccount);
    }
}