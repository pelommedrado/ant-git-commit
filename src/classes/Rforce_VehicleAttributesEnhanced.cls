/*****************************************************************************************
    Name            : Rforce_VehicleAttributesEnhanced
    Description     : This class is used to get the Vehicle Details from Archive webservices
    Approach        : WebService  interface for displaying the Vehicle Details and insering attachment in Notes&Attachment section
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 14-05-2015
    Imnplemented By : Rajavel Baskaran
 
******************************************************************************************/
public class Rforce_VehicleAttributesEnhanced {

     public String strCaseId {get; set;}
     public String strCountryCase {get; set;}
     public String strParentId {get; set;}
     public Rforce_ArchivesXMLData xmlData{get;set;}
     public Rforce_Utils08_RCArchivage CasesDataSource;
     public String attachmentURL{get;set;}
     public Rforce_ArchivesProperties.ArchivesAttachments[] ArchAttachmentsList {get;set;}  
     public Integer showDetail 
    {
        get { if(showDetail == null) {showDetail = 0; return showDetail;} else return showDetail; }
        set { showDetail = value; }
    }     
     List<Rforce_ArchivesXMLData.ActivityList> ActivitiesListsn = new List<Rforce_ArchivesXMLData.ActivityList>();
     public Rforce_VehicleAttributesEnhanced(ApexPages.StandardController controller) {   
     CasesDataSource = new Rforce_Utils08_RCArchivage();  
     strCaseId =    ApexPages.currentPage().getParameters().get('sCaseID');     
     strCountryCase = ApexPages.currentPage().getParameters().get('sCountryCase');
     this.strParentId = ApexPages.currentPage().getParameters().get('sParentId');     
     getCaseDetails(strCaseId ,strCountryCase);
     getAttachements(strCountryCase,strCaseId);
    }
    
    public void getCaseDetails(String strCaseId ,String strCountryCase){
     try{
          system.debug('## Rforce_VehicleAttributesEnhanced Inside getCaseDetails() ##');                 
          String Output=CasesDataSource.getXmlDetails(strCountryCase,strCaseId);              
          if (Output== null){
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Error_Rnull));
           }
          else  {                
                 Rforce_ArchivageXMLParseEngine parsingEngine =new Rforce_ArchivageXMLParseEngine ();
                 xmlData=parsingEngine.getXMLData(Output);                
            }  
        }
        catch (CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_CASEDETAILS));
        }
      
    }
    
    public void getAttachements(String strCountryCase,String strCaseId) {
        try {
            system.debug('## Rforce_VehicleAttributesEnhanced Inside getAttachements () ##');
            Rforce_RcArchivage.attachmentDetailsList[] resultAttachment = CasesDataSource.getAttachmentDetailsList(strCountryCase,strCaseId);            
            if (resultAttachment == null){
                ArchAttachmentsList = new Rforce_ArchivesProperties.ArchivesAttachments[]{};
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_NOATTACHMENT));
            }
            else {
                //showDetail = 13;             
                ArchAttachmentsList = new Rforce_ArchivesProperties.ArchivesAttachments[]{};
                Rforce_ArchivesProperties.ArchivesAttachments Arch;                              
                for(Rforce_RcArchivage.attachmentDetailsList myAttList : resultAttachment)
                {
                    Arch = new Rforce_ArchivesProperties.ArchivesAttachments();
                    Arch.attachmentName = myAttList.attachmentName;
                    Arch.extension = myAttList.extension;
                    Arch.createdDate = myAttList.createdDate;
                    Arch.Idcase=strCaseId;
                    Arch.country=strCountryCase;
                    ArchAttachmentsList.add(Arch);
                }
                system.debug('## Rforce_VehicleAttributesEnhanced ArchAttachmentsList size is..::'+ ArchAttachmentsList.size());
                system.debug('##Rforce_VehicleAttributesEnhanced ArchAttachmentsList Value is..::'+ ArchAttachmentsList);
            }
        }
        catch (CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_NOATTACHMENT));
         }  
        
     
    }
    
     /* REQ-00258 -- Converted the binary format from webservice into UTF-8 readable format */
    
   public void getAttachementData(){
    system.debug('## Inside getAttachementData ##');    
    try{
        
        String IdCase = System.currentPagereference().getParameters().get('sCaseID');
        String country =System.currentPagereference().getParameters().get('sCountryCase');
        String parentIdfromPage = ApexPages.currentPage().getParameters().get('parentId');
        System.debug('parentIdfromPage '+parentIdfromPage);
         
        String attachmentName = System.currentPagereference().getParameters().get('attachmentName');
        String strContentType=System.currentPagereference().getParameters().get('contentType');
        String strParentId =System.currentPagereference().getParameters().get('sParentId');
        String pId = ApexPages.currentPage().getParameters().get('sParentId');       
        
        Blob BlobDec;
        String resultURL = CasesDataSource.getAttachmentContent(country, IdCase, attachmentName);
        System.debug('resultURL'+resultURL);
        String stUrlUTF8 = EncodingUtil.urlEncode(resultURL, 'UTF-8');
        BlobDec = Blob.valueOf(stUrlUTF8);
        String BlobDec1=EncodingUtil.base64Encode(BlobDec);
        BlobDec = EncodingUtil.base64Decode(resultURL);
        if (resultURL == null || resultURL == ''){
               system.debug('## Inside if part ##');
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_No_Record));
         }else {
               system.debug('## Inside else part ##');
               showDetail = 14;     
               attachmentURL=resultURL;
               Attachment attachment = new Attachment();
               attachment.Body = BlobDec;
               attachment.Name= attachmentName+'.'+strContentType;
               attachment.ParentId = strParentId;
               insert attachment;
               attachmentURL=attachment.Id;                  
             }
          } 
          catch(CalloutException ce) {
          String strSplit = ce.getMessage();          
          if(strSplit.contains('No attachment/xml content')){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_NOATTACHMENT));
         }
         else if(strSplit.contains('Large File')){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_LargeFile));
         }
         else{
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_Technical_Issue));
         }          
       }           
    }         
  }