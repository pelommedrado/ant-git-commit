public with sharing class Utils_ContractDataSource {

    // Boolean variable used to specify if the dummy implementation must be used or the real one
    public Boolean Test;
      
    public WS04_CustdataCrmBserviceRenault.GetCustDataResponse getContractData(VFC05_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
        // --- PRE TREATMENT ---- 
 
        WS04_CustdataCrmBserviceRenault.GetCustDataRequest request = new WS04_CustdataCrmBserviceRenault.GetCustDataRequest();
        WS04_CustdataCrmBserviceRenault.ServicePreferences servicePref = new WS04_CustdataCrmBserviceRenault.ServicePreferences();
        WS04_CustdataCrmBserviceRenault.CustDataRequest custPref = new WS04_CustdataCrmBserviceRenault.CustDataRequest();
        
        custPref.vin = VehController.getVin(); 
        custPref.mode = '008';
        system.debug(UserInfo.getName());
        custPref.country = this.getUserLanguage();
        custPref.demander = 'MOEWS';
        servicePref.userid= '?';
        servicePref.country = 'FR'; 
        servicePref.language = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        request.servicePrefs = servicePref; 
        request.custDataRequest = custPref;
    
        // ---- WEB SERVICE CALLOUT -----    
        WS04_CustdataCrmBserviceRenault.CrmGetCustData ConWS = new WS04_CustdataCrmBserviceRenault.CrmGetCustData();
        ConWS.endpoint_x = System.label.VFP05_ContractURL;
        ConWS.clientCertName_x = System.label.RenaultCertificate;
        ConWS.timeout_x=40000;
        
        WS04_CustdataCrmBserviceRenault.GetCustDataResponse CON_WS = new WS04_CustdataCrmBserviceRenault.GetCustDataResponse();    
    
        if (Test==true) {
            CON_WS = Utils_Stubs.ContractStub();    
        } else { 
            CON_WS = ConWS.getCustData(request);
        }
         return CON_WS;  
    }
    
    private String getUserLanguage(){
        return UserInfo.getLanguage().substring(0, 2).toUpperCase();
    /*  system.debug(UserInfo.getName());
        Map<String, VFC05_LanguageMapping__c> cs = new Map<String, VFC05_LanguageMapping__c>{};
        cs = VFC05_LanguageMapping__c.getAll();
        system.debug('User language is:' + UserInfo.getLanguage() + UserInfo.getName());
        GetUserInfoResult userinforesult = new GetUserInfoResult();
        userinforesult = UserInfo.getUserInfo();
        return userinforesult.userLanguage;*/
        //return cs.get(UserInfo.getLanguage()).Short_Code__c;
    }
}