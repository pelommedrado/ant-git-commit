/**
*	Class   -   VFC24_PriceBookDAO
*   Author  -   RameshPrabu 
*   Date    -   22/10/2012
*    
*   #01 <RameshPrabu> <22/10/2012>
*        Created this Class to handle records from Price Book related Queries.
*/
public with sharing class VFC24_PriceBookDAO extends VFC01_SObjectDAO {
	private static final VFC24_PriceBookDAO instance = new VFC24_PriceBookDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC24_PriceBookDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC24_PriceBookDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get Product Record 
    * @param productId - use productId to fetch records from PricebookEntry object.
    * @return Price - fetch and return the result in PricebookEntry.
    */
    public PricebookEntry findPricebyProductId(Id productId ){
        PricebookEntry Price = null;
        try{
	        Price = [SELECT
	        			IsActive, UnitPrice, Id, Pricebook2Id, ProductCode, Product2Id, Name, UseStandardPrice    
	    			FROM
	        			PricebookEntry
	        		WHERE
	        			Product2Id =: productId  AND
        				IsActive = true
        			limit 1
	        		];
	        
	        return Price;
        }catch(QueryException ex){
        	return null;
        }
    }
    
    /**
    	Method fetch all PriceBookEntry records..
    **/
    public List<PricebookEntry> fetch_All_PricebookEntry(){
    	List<PricebookEntry> lst_All_PriceBookEntry = null;
    	lst_All_PriceBookEntry = [SELECT 
    									IsActive, UnitPrice, Id, Pricebook2Id, ProductCode,
    									Product2Id, Name, UseStandardPrice 
    								FROM 
    									PricebookEntry 
    								WHERE 
    									Name != null
    								];
    	return lst_All_PriceBookEntry;
    }
    
    /**
    	Method returns the PriceBookEntry record with ProductCode in Map.
    **/
    public Map<String, PricebookEntry> return_Map_ProductCode_PriceBookEntry(){
    	Map<String, PricebookEntry> map_PriceBookEntry = new Map<String, PricebookEntry>();
    	List<PricebookEntry> lst_All_PriceBookEntry = new List<PricebookEntry>();
    	lst_All_PriceBookEntry = [SELECT 
    									IsActive, UnitPrice, Id, Pricebook2Id, ProductCode,
    									Product2Id, Name, UseStandardPrice 
    								FROM 
    									PricebookEntry 
    								WHERE 
    									Name != null
    								];
    	for( PricebookEntry priceEntry : lst_All_PriceBookEntry ){
    		map_PriceBookEntry.put( priceEntry.ProductCode, priceEntry );
    	}
    	return map_PriceBookEntry;
    }
    
    /**
    	Method returns the PriceBookEntry record with ProductID in Map.
    **/
    public Map<Id, PricebookEntry> return_Map_ProductID_PriceBookEntry(){
    	Map<Id, PricebookEntry> map_PriceBookEntry = new Map<Id, PricebookEntry>();
    	List<PricebookEntry> lst_All_PriceBookEntry = new List<PricebookEntry>();
    	lst_All_PriceBookEntry = [SELECT 
    									IsActive, UnitPrice, Id, Pricebook2Id, ProductCode,
    									Product2Id, Name, UseStandardPrice 
    								FROM 
    									PricebookEntry 
    								WHERE 
    									Name != null
    								];
    	for( PricebookEntry priceEntry : lst_All_PriceBookEntry ){
    		map_PriceBookEntry.put( priceEntry.Product2Id, priceEntry );
    	}
    	return map_PriceBookEntry;
    }
    
    public List<PricebookEntry> findById(Set<String> setId)
    {
    	List<PricebookEntry> lstSObjPbEntry = [SELECT Id,
    	                                              Product2.RecordType.DeveloperName
    	                                       FROM PricebookEntry
    	                                       WHERE Id IN : setId];
    	
    	return lstSObjPbEntry;
    }   
}