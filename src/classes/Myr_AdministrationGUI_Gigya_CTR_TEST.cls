/**
* @author sebastien.ducamp@atos
* @description Test class for Myr_AdministrationGUI_Gigya_CTR
* @version 1.0 / 16.07 / Sébastien Ducamp / Initial revision
*/
@isTest
private class Myr_AdministrationGUI_Gigya_CTR_TEST { 

	private static final String KEY_RENAULT = 'gig_ren_key';
	private static final String KEY_DACIA = 'gig_dac_key';
	private static final Boolean isSandbox = [select IsSandbox from Organization].IsSandbox;

	/* Helper functions for Encode/Decode functions **/
	private static String getRenaultEncodedKey() {
		return LMT_Crypto_CLS.encode( KEY_RENAULT, system.Label.Myr_Gigya_Key);
	}

	private static String getDaciaEncodedKey() {
		return LMT_Crypto_CLS.encode( KEY_DACIA, system.Label.Myr_Gigya_Key);
	}

	private static String getDecodedValue(String value) {
		return LMT_Crypto_CLS.decode( value, system.Label.Myr_Gigya_Key);
	}
	
	//Return the Gigya_Datacenter_NoProd__c or Gigya_Datacenter__c
	private static String getGigyaDatacenter(Country_Info__c country) {
		if (isSandbox){
			return country.Gigya_Datacenter_NoProd__c;
		} else {
			return country.Gigya_Datacenter__c;
		}		
	}

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareMyrCustomSettings();
		List<Country_Info__c> listCountries = new List<Country_Info__c>();		
		listCountries.add(new Country_Info__c(
            	Name='France', Language__c = 'Italian'
				, Gigya_REN_K_NoProd__c=getRenaultEncodedKey(), Gigya_DAC_K_NoProd__c=getDaciaEncodedKey(), Gigya_Datacenter_NoProd__c='gig_datacenter' 
				, Gigya_REN_K__c=getRenaultEncodedKey(), Gigya_DAC_K__c=getDaciaEncodedKey(), Gigya_Datacenter__c='gig_datacenter' 				
				, Gigya_DAC_NoEmailVerif_K__c=getDaciaEncodedKey(), Gigya_DAC_NoEmailVerif_K_NoProd__c=getDaciaEncodedKey()
				, Gigya_REN_NoEmailVerif_K__c=getRenaultEncodedKey(), Gigya_REN_NoEmailVerif_K_NoProd__c=getRenaultEncodedKey()
				));
			listCountries.add(new Country_Info__c(
            	Name='Italy', Language__c = 'French'
				,Gigya_REN_K_NoProd__c=getRenaultEncodedKey(), Gigya_DAC_K_NoProd__c=getDaciaEncodedKey(), Gigya_Datacenter_NoProd__c='gig_datacenter'
				,Gigya_REN_K__c=getRenaultEncodedKey(), Gigya_DAC_K__c=getDaciaEncodedKey(), Gigya_Datacenter__c='gig_datacenter'
				,Gigya_DAC_NoEmailVerif_K__c=getDaciaEncodedKey(), Gigya_DAC_NoEmailVerif_K_NoProd__c=getDaciaEncodedKey()
				,Gigya_REN_NoEmailVerif_K__c=getRenaultEncodedKey(), Gigya_REN_NoEmailVerif_K_NoProd__c=getRenaultEncodedKey()
				));
		insert listCountries;
		Myr_Datasets_Test.prepareGigyaCustomSettings(isSandBox);
	}

	//Test the initialization of the controller
	static testmethod void test_Init () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		Integer nbOptions = 0;
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			nbOptions += Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).keySet().size();
		}
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			system.assertNotEquals( null, controller.InnerSettings );
			system.assertEquals( 'https://socialize.gigya.com/socialize.getToken?', controller.InnerSettings.Url_Token__c );
			system.assertEquals( 2, controller.CountrySettings.keySet().size() );
			system.assertEquals( true, controller.GlobalFields.size() > 0 );
			system.assertNotEquals( null, controller.CountryBrandGigyaSettings );
			system.assertEquals( 2, controller.CountryBrandGigyaSettings.keySet().size() );
			system.assertEquals( nbOptions, controller.CountryBrandGigyaSettings.get('France').keySet().size() );
		}		
	}

	//Test the SelectOption list for countries
	static testmethod void testGetCountries () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			system.assertEquals( 2 + 1 /* empty option */, controller.getCountries().size() );
		}
	}

	//Test the SelectOption for Brands
	static testmethod void testGetBrands () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		Integer nbOptions = 0;
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			nbOptions += Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).keySet().size();
		}
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			system.assertEquals( nbOptions + 1 /* empty option*/, controller.Brands.size() );
		}	
	}

	//Test Encode Decode function
	static testmethod void test_gigya_EncodeDecode () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			controller.SelectedCountry = 'France';
			controller.SelectedBrand = 'RENAULT_PROD';
			String api_key = 'api_key';
			String secret_key = 'secret_key';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = api_key;
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = secret_key;
			system.assertNotEquals( KEY_RENAULT, controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).encode() );
			system.assertNotEquals( KEY_RENAULT, controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).EncodedValue);
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).decode();
			system.assertEquals( api_key + ':' + secret_key, 
				controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey 
				+ ':'
				+ controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey 
				);
		}
	}

	//Test behaviour of the buttons
	static testmethod void test_Cancel_Edit_Popup_Behaviour () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.Popup );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			system.assertEquals( false, controller.Popup );
			controller.cancel();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.Popup );
			controller.displayPopup();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( true, controller.Popup );
			controller.cancelCountry();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( false, controller.Popup );
		}
	}

	//Test the behaviour of the check connectivity button
	static testmethod void test_checkConnectivity_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//prepare gigya connections
		Country_Info__c countrySet = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(countrySet));
		mock.setToken_OK();
		Test.setMock( HttpCalloutMock.class, mock );
		//Trigger the test and check the result
		Integer nbOptions = 0;
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			nbOptions += Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).keySet().size();
		}
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			controller.SelectedCountry = 'Italy';
			Test.startTest();
			controller.checkConnectivity();
			Test.stopTest();
			system.assertEquals(true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			system.assertEquals( nbOptions, ApexPages.getMessages().size() );
		}
	}

	//Test the behaviour of the checkConnectivity button when the connection fails
	static testmethod void test_checkConnectivity_Failed () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//prepare gigya connections
		Country_Info__c countrySet = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(countrySet));
		mock.setToken_EmptyResp();
		Test.setMock( HttpCalloutMock.class, mock );
		//Trigger the test and check the result
		Integer nbOptions = 0;
		for( String brand : Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.keySet() ) {
			nbOptions += Myr_GigyaCaller_Cls.MAP_BRAND_ENV_FIELDS.get(brand).keySet().size();
		}
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			controller.SelectedCountry = 'Italy';
			controller.checkConnectivity();
			system.assertEquals(true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
			system.assertEquals( nbOptions, ApexPages.getMessages().size() );
		}
	}

	//Test the Gigya configuration save
	static testmethod void test_SaveGigya_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			controller.edit();
			controller.GigyaSettings.Fields.get('max_account_deletion__c').value = 2;
			controller.save();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			CS_Gigya_Settings__c gigyaCheck = [SELECT Id, Max_Account_Deletion__c FROM CS_Gigya_Settings__c];
			system.assertEquals( 2, gigyaCheck.Max_Account_Deletion__c );
		}
	}

	//Test Failure when trying to save a new configuration for Gigya
	static testmethod void test_SaveGigya_Failed () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Integer oldValue = Integer.valueOf(controller.InnerSettings.Max_Account_Deletion__c);
			controller.edit();
			controller.GigyaSettings.Fields.get('max_account_deletion__c').value = 18; //>7, will vbe rejected
			controller.save();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
			system.assertEquals( oldValue, [SELECT Id, Max_Account_Deletion__c FROM CS_Gigya_Settings__c][0].Max_Account_Deletion__c );
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Renault_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDacia_NoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter_NoProd = checkCountry.Gigya_Datacenter_NoProd__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'RENAULT_' + Myr_GigyaCaller_Cls.ENVIRONMENT.PROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );			
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_REN_K__c));
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter__c );	
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDacia_NoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenter_NoProd, checkCountry.Gigya_Datacenter_NoProd__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Dacia_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldDacia_NoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter_NoProd = checkCountry.Gigya_Datacenter_NoProd__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'DACIA_' + Myr_GigyaCaller_Cls.ENVIRONMENT.PROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia_NoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenter_NoProd, checkCountry.Gigya_Datacenter_NoProd__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_DAC_K__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter__c );
		}
	}

	
	//Test save of a country configuration
	static testmethod void test_SaveCountry_Renault_NoProd_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDacia_NoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'RENAULT_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );			
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_REN_K_NoProd__c));
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c );
			system.assertEquals( oldDacia_NoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDatacenter, checkCountry.Gigya_Datacenter__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter_NoProd__c );	
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Dacia_NoProd_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'DACIA_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenter, checkCountry.Gigya_Datacenter__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_DAC_K_NoProd__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter_NoProd__c );
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Dacia_NoProd_NoVerifEmail_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDaciaNoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'DACIA_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_NOPROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDaciaNoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenter, checkCountry.Gigya_Datacenter__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter_NoProd__c );
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Renault_NoProd_NoVerifEmail_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDaciaNoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldDatacenternoProd = checkCountry.Gigya_Datacenter_NoProd__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'RENAULT_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_NOPROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDaciaNoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenter, checkCountry.Gigya_Datacenter__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter_NoProd__c );
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Renault_NoVerifEmail_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDaciaNoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldDatacenterNoProd = checkCountry.Gigya_Datacenter_NoProd__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'RENAULT_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_PROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDaciaNoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenterNoProd, checkCountry.Gigya_Datacenter_NoProd__c  );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif, checkCountry.Gigya_DAC_NoEmailVerif_K__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_REN_NoEmailVerif_K__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter__c);
		}
	}

	//Test save of a country configuration
	static testmethod void test_SaveCountry_Dacia_NoVerifEmail_OK () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDaciaNoProd = checkCountry.Gigya_DAC_K_NoProd__c;
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldRenault_NoProd = checkCountry.Gigya_REN_K_NoProd__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			String oldDatacenterNoProd = checkCountry.Gigya_Datacenter_NoProd__c;
			String oldRenault_NoEmailVerif = checkCountry.Gigya_REN_NoEmailVerif_K__c;
			String oldRenault_NoEmailVerif_NoProd = checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c;
			String oldDacia_NoEmailVerif = checkCountry.Gigya_DAC_NoEmailVerif_K__c;
			String oldDacia_NoEmailVerif_NoProd = checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c;
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'DACIA_' + Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_PROD.name();
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'NewApiKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c,Gigya_REN_K_NoProd__c, Gigya_DAC_K_NoProd__c,Gigya_Datacenter_NoProd__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c);
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldRenault_NoProd, checkCountry.Gigya_REN_K_NoProd__c );
			system.assertEquals( oldDatacenterNoProd, checkCountry.Gigya_Datacenter_NoProd__c  );
			system.assertEquals( oldRenault_NoEmailVerif_NoProd, checkCountry.Gigya_REN_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldDacia_NoEmailVerif_NoProd, checkCountry.Gigya_DAC_NoEmailVerif_K_NoProd__c );
			system.assertEquals( oldRenault_NoEmailVerif, checkCountry.Gigya_REN_NoEmailVerif_K__c );
			system.assertEquals( oldDaciaNoProd, checkCountry.Gigya_DAC_K_NoProd__c );
			system.assertEquals( 'NewApiKey:NewSecretKey', getDecodedValue(checkCountry.Gigya_DAC_NoEmailVerif_K__c) );
			system.assertEquals( 'jp2', checkCountry.Gigya_Datacenter__c);
		}
	}

	//Test Failure when trying to save the country configuration
	static testmethod void test_SaveCountry_Failed () {
		//Prepare a user
		User heliosAdminUser = Myr_Datasets_Test.getHeliosAdminUser( 'firstname', 'UnitTestClassAuth', 'France' );
		//Prepare a page
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		//trigger the test and check the result
		system.runAs( heliosAdminUser ) {
			Country_Info__c checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c 
				, gigya_dac_noemailverif_k__c, gigya_dac_noemailverif_k_noprod__c, gigya_ren_noemailverif_k__c, gigya_ren_noemailverif_k_noprod__c
				FROM Country_Info__c WHERE Name = 'Italy'];
			String oldRenault = checkCountry.Gigya_REN_K__c;
			String oldDacia = checkCountry.Gigya_DAC_K__c;
			String oldDatacenter = checkCountry.Gigya_Datacenter__c;
			Myr_AdministrationGUI_Gigya_CTR controller = new Myr_AdministrationGUI_Gigya_CTR();
			controller.SelectedCountry='Italy';
			controller.displayPopup();
			controller.SelectedBrand = 'RENAULT_PROD';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).ApiKey = 'longstringggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).SecretKey = 'NewSecretKey';
			controller.CountryBrandGigyaSettings.get(controller.SelectedCountry).get(controller.SelectedBrand).DataCenter = 'jp2';
			controller.saveCountry();
			system.assertEquals( true,  ApexPages.hasMessages( ApexPages.Severity.ERROR ) );	
			checkCountry = [SELECT Id, Gigya_REN_K__c, Gigya_DAC_K__c, Gigya_Datacenter__c FROM Country_Info__c WHERE Name = 'Italy'];
			system.assertEquals( oldRenault, checkCountry.Gigya_REN_K__c );
			system.assertEquals( oldDacia, checkCountry.Gigya_DAC_K__c );
			system.assertEquals( oldDatacenter, checkCountry.Gigya_Datacenter__c );
		}
	}




}