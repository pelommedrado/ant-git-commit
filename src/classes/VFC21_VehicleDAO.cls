/**
*	Class	-	VFC21_VehicleDAO
*	Author	-	Surseh Babu
*	Date	-	29/10/2012
*
*	#01 <Suresh Babu> <29/10/2012>
*		DAO class for Vehicle object to Query records.
**/
public with sharing class VFC21_VehicleDAO extends VFC01_SObjectDAO{
	private static final VFC21_VehicleDAO instance = new VFC21_VehicleDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC21_VehicleDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC21_VehicleDAO getInstance(){
        return instance;
    }
    
    /**
    * 	This Method was used to get Vehicle object Record 
    * 	@return	lstVehicleRecords - fetch and return the List Vehicle Records from Vehicle object.
    */
    public List<VEH_Veh__c> fetchVehicleRecords(){
    	List<VEH_Veh__c> lstVehicleRecords = null;
    	
    	lstVehicleRecords = [SELECT  
    							AfterSalesType__c, BodyType__c, /**CountRelation__c,**/ CountryOfDelivery__c, 
    							DateofManu__c, DeliveryDate__c, Description__c, EnergyType__c, 
    							EngineIndex__c, EngineManuNbr__c, EngineType__c, FirstRegistrDate__c, 
    							GearBoxIndex__c, GearBoxManuNbr__c, GearBoxType__c, IDBIRSellingDealer__c, 
    							KmCheck__c, KmCheckDate__c, LastModifiedById, LastModifiedDate, LastRegistrDate__c, 
    							MaintenanceDealer__c, Model__c, ModelCode__c, NumberofDoors__c, Id, SaleNature__c, 
								Name, VehicleBrand__c, VehicleManuNbr__c, VehicleRegistrNbr__c, VehicleType__c, 
								VersionCode__c, Version__c
							FROM 
								VEH_Veh__c
							WHERE
								Model__c != null
							];
		return lstVehicleRecords;
    }
    
    /**
    * 	This Method was used to check if the model exists
    *	@param	model			-	Car Model name, to find the records in this model. 
    * 	@return	lstVehicleRecords - fetch and return the List Vehicle Records from Vehicle object.
    */
    public List<VEH_Veh__c> fetchVehicleRecordsUsingModelName(String model)
    {
    	List<VEH_Veh__c> lstVehicleRecords = null;
    	
    	lstVehicleRecords = [SELECT Id, Name, Model__c, ModelCode__c 
						  	   FROM VEH_Veh__c
						  	  WHERE Model__c =: model
						  	  limit 1];
		return lstVehicleRecords;
    }
    
    /**
    *	This method was used to get the Vehicle records using its Child record Vehicle Relation.
    *	@param	
    *	@return	lstVehicleRecords	-	return the list on Vehicle records
    **/
    public List<VEH_Veh__c> fetchVehicleRecordsUsingVehicleRelationIDs( List<Id> vehicleRelationIds ){
    	List<VEH_Veh__c> lstVehicleRecords = null;
    	
    	lstVehicleRecords = [SELECT
    							Name, Status__c, Model__c, DateofManu__c, ModelYear__c, Manufacturing_Year__c, Color__c,
    							Optional__c, Price__c, LastModifiedDate, VersionCode__c, Version__c
    						FROM
    							VEH_Veh__c 
    						WHERE
    							Id=: vehicleRelationIds
							];
		return lstVehicleRecords;
    }
    
    /**
    *	This method was used to get the Vehicle records using its VIN number.
    *	@param	
    *	@return	mapVehicles	-	return the list on Vehicle records
    **/
    public Map<String, VEH_Veh__c> fetchVehicleUsingVIN( Set<String> setVIN ){
    	Map<String, VEH_Veh__c> mapVehicles = new Map<String, VEH_Veh__c>();
    	List<VEH_Veh__c> lstVehicles = null;

    	lstVehicles = [SELECT
							Name, Status__c, Model__c, DateofManu__c, ModelYear__c, Color__c,
							Optional__c, Price__c, LastModifiedDate, VersionCode__c, Version__c
						FROM
							VEH_Veh__c 
						WHERE
							Name IN : setVIN
						];
		for (VEH_Veh__c veh: lstVehicles) {
			mapVehicles.put(veh.Name, veh);
		}
		return mapVehicles;
    }
    
    
    /**
    *	This method was used to get the Vehicle records using its Ids.
    *	@param	
    *	@return	mapVehicles	-	return the list on Vehicle records
    **/
    public Map<String, VEH_Veh__c> fetchVehicleUsingIds( List<Id> lstIds ){
    	Map<String, VEH_Veh__c> mapVehicles = new Map<String, VEH_Veh__c>();
    	List<VEH_Veh__c> lstVehicles = null;

    	lstVehicles = [SELECT
							Name, Status__c, Model__c, DateofManu__c, ModelYear__c, Color__c,
							Optional__c, Price__c, LastModifiedDate, VersionCode__c, Version__c
						FROM
							VEH_Veh__c 
						WHERE
							Id IN : lstIds
						];
		for (VEH_Veh__c veh: lstVehicles) {
			mapVehicles.put(veh.Name, veh);
		}
		return mapVehicles;
    }
    
}