@isTest
private class WSC04_PreOrderDMSVO_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
   		WSC04_PreOrderDMSVO preOrderVO = new WSC04_PreOrderDMSVO();
		preOrderVO.CPF_CNPJ = '12345678910';
		preOrderVO.VIN = 'FY123XKAJFAI891P1';
		preOrderVO.status = 'Aberto';
		preOrderVO.creationDate = Date.newInstance(2012, 11, 30);	
	    preOrderVO.expirationDate = Datetime.newInstance(2012, 12, 30, 8, 0, 0);	
	    preOrderVO.customerName = 'João da Silva';
	    preOrderVO.streetAndComplement = 'Avenida Brasil, 1000, apto 201';
	    preOrderVO.city = 'Rio de Janeiro';
	    preOrderVO.zipCode = '22180200';
	    preOrderVO.state = 'Rio de Janeiro';
		preOrderVO.country = 'Brasil';
		preOrderVO.customerEmail = 'joao.silva@gmail.com.xpto';
		preOrderVO.maritalStatus = 'Casado';
		preOrderVO.sex = 'M';
		preOrderVO.birthDate = Date.newInstance(1980, 5, 15);	
		//preOrderVO.street= 'street';
		//preOrderVO.complement = 'complement';
		//preOrderVO.addressNumber = 'number';
		//preOrderVO.neighborhood = 'neighbor';
		
		preOrderVO.accessoriesList = 'Bancos de couro | Sensor de estacionamento';
		preOrderVO.entryValue = 40000;
		
		//preOrderVO.error = true;
		preOrderVO.errorMessage = 'Error';
    }
}