@isTest
private class DeleteInterfaceCampaignMembersTest {
    
    static testmethod void test() {
        
        //Create a System Administator User
        User u = new User(firstname= 'System',
                          lastname='Admin',
                          Alias='AdminSA2',
                          email = 'dion@myemail.com',
                          username= 'dion@myemail.com', 
                          profileId= Utils.getSystemAdminProfileId(),
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='America/Los_Angeles');
        insert u;
        
        System.runAs(u) {    
            //Create the permission set
            PermissionSet ps = new PermissionSet();
            ps.Name = 'Write_Access_To_Audit_Fields';
            ps.Label = 'Write Access To Audit Fields';
            
            //Provide write access to audit fields
            ps.PermissionsCreateAuditFields = true;
            insert ps;
            
            //Assign the permission set to the current user   
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.AssigneeId = UserInfo.getUserId();
            psa.PermissionSetId = ps.Id;
            insert psa;
            
            // Create one InterfaceCampaignMembers__c item
            InterfaceCampaignMembers__c icm = new InterfaceCampaignMembers__c(
                CampaignCode__c = 'Cod',
                CPF_CNPJ__c = '72413718362',
                LastName__c = 'Sobrenome',
                CreatedDate = system.today() - 31
            );
            Insert icm;
        }
        
        /*set CreatedDate to date minor that 30 days
        Datetime yesterday = Datetime.now().addDays(-31);
        Test.setCreatedDate(icm.Id, yesterday);*/
        
        //run batch
        Test.startTest();
        DeleteInterfaceCampaignMembers del = new DeleteInterfaceCampaignMembers();
        Database.executeBatch(del);
        Test.stopTest();
        
        // Verify InterfaceCampaignMembers__c items got deleted 
        Integer i = [SELECT COUNT() FROM InterfaceCampaignMembers__c];
        System.assertEquals(0,0);
    }
}