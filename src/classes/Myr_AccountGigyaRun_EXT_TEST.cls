/** Apex class used to test the Goigya extension for Account
 * The goal is to test the extension itself and not the Gigya behavior
 * as this is already done by Myr_GigyaFunctions_Test.
*/

@isTest
private class Myr_AccountGigyaRun_EXT_TEST { 

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		//prepare the Gigya settings
		CS_Gigya_Settings__c gigSettings = new CS_Gigya_Settings__c();
		gigSettings.Url_Token__c = 'https://socialize.gigya.com/socialize.getToken?';
		gigSettings.Url_Reset_Password__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resetPassword?';
		gigSettings.Url_Resend_Activation_Email__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.resendVerificationCode?';
		gigSettings.Url_Account_Search__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.search?';
		gigSettings.Url_Delete_Account__c = 'https://accounts.<Data_Center_ID>.gigya.com/accounts.deleteAccount?';
		gigSettings.Max_Account_Deletion__c = 2;
		insert gigSettings;
		//prepare the settings for the country to test
		Country_Info__c setItaly = [SELECT Id, Gigya_Datacenter__c, Gigya_DAC_K__c, Gigya_REN_K__c, Gigya_Datacenter_NoProd__c, Gigya_DAC_K_NoProd__c, Gigya_REN_K_NoProd__c FROM Country_Info__c WHERE Name='Italy'];
		if ([select IsSandbox from Organization].IsSandbox){
			setItaly.Gigya_Datacenter_NoProd__c = 'us1';
			setItaly.Gigya_DAC_K_NoProd__c = LMT_Crypto_CLS.encode('gigya:renaultkey', system.Label.Myr_Gigya_Key);
			setItaly.Gigya_REN_K_NoProd__c = LMT_Crypto_CLS.encode('gigya:daciakey', system.Label.Myr_Gigya_Key);
		} else {
			setItaly.Gigya_Datacenter__c = 'us1';
			setItaly.Gigya_DAC_K__c = LMT_Crypto_CLS.encode('gigya:renaultkey', system.Label.Myr_Gigya_Key);
			setItaly.Gigya_REN_K__c = LMT_Crypto_CLS.encode('gigya:daciakey', system.Label.Myr_Gigya_Key);		
		}
		update setItaly;
		//Prepare the authorization settings for Gigya (System Administrator profile)
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = true;
		authGigya.Gigya_Level2__c = true;
		authGigya.Gigya_Level3__c = true;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya; 
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, true) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
	}

	//impossible to do this in testsetup even if enclosed withing startTest / stopTest ....
	private static void insertPersonalAccount() {
		//Insert a user for the following tests
		User techUser = Myr_Datasets_Test.getTechnicalUserWithRole('Italy');
    	system.runAs(techUser) {
    		Account acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'USER_ACTIF', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c FROM Account WHERE Id = :acc.Id];
    		system.AssertEquals('Italy',acc.Country__c);
    	}
		
	}

	//Return the Gigya_Datacenter_NoProd__c or Gigya_Datacenter__c
	private static String getGigyaDatacenter(Country_Info__c country) {
		if ([select IsSandbox from Organization].IsSandbox){
			return country.Gigya_Datacenter_NoProd__c;
		} else {
			return country.Gigya_Datacenter__c;
		}		
	}

	//Test the email pattern
	static testmethod void checkEmailPattern() {
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test.test.test'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test@test@test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test@test.fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test@test.test.test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('32@test.fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('32@32.fr'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('32@32'));
		system.assertEquals(false ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test.Fr'));
		system.assertEquals(true ,Myr_MyRenaultTools.isEmailValid('test-32_yahoo@test.Fr.com'));
	}

	//Test loading of the Gigya RENAULT information 
	static testmethod void testLoadingGigyaRenault_OK() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Trigger the test for Renault and check the result
		ext.loadGigyaRenault();
		system.AssertEquals(true, ext.FoundGigyaRenault);
		system.assertEquals(null, ext.ErrorGigyaRenault);
		system.assertNotEquals(null, ext.GigyaResultRenault);
		Test.stopTest();
	}

	//Test loading of the Gigya DACIA information
	static testmethod void testLoadingGigyaDacia_OK() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Trigger the test for Dacia and check the result
		Test.startTest();
		ext.loadGigyaDacia();
		system.AssertEquals(true, ext.FoundGigyaDacia);
		system.assertEquals(null, ext.ErrorGigyaDacia);
		system.assertNotEquals(null, ext.GigyaResultDacia);
		Test.stopTest();
	}

	//Test loading of the Gigya RENAULT information
	static testmethod void testLoadingGigyaRenault_KO() {
		insertPersonalAccount();
		//Set the mock with a failed response
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_BadRequest();
		mock.setAccSch_BadRequest();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		Test.startTest();
		//Trigger the test for Renault and check the result
		ext.loadGigyaRenault();
		system.AssertEquals(false, ext.FoundGigyaRenault);
		system.assertNotEquals(null, ext.ErrorGigyaRenault);
		system.assertEquals(null, ext.GigyaResultRenault);
		Test.stopTest();
	}

	//Test loading of the Gigya RENAULT information
	static testmethod void testLoadingGigyaRDacia_KO() {
		insertPersonalAccount();
		//Set the mock with a failed response
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_BadRequest();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Trigger the test for Dacia and check the result
		ext.loadGigyaDacia();
		system.AssertEquals(false, ext.FoundGigyaDacia);
		system.assertNotEquals(null, ext.ErrorGigyaDacia);
		system.assertEquals(null, ext.GigyaResultDacia);
		Test.stopTest();
	}

	//RENAULT: test Reset PAssword functionnality
	static testmethod void testResetPassword_Renault() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.resetPassword();
		system.assertNotEquals(null, ext.LastResponseRenault);
		Test.stopTest();
	}

	//DACIA: test Reset PAssword functionnality
	static testmethod void testResetPassword_Dacia() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDacia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.resetPassword();
		system.assertNotEquals(null, ext.LastResponseDacia);
		Test.stopTest();
	}

	//RENAULT: test resend activation email functionnality
	static testmethod void testResendActivationEmail_Renault() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.resendActivationEmail();
		system.assertNotEquals(null, ext.LastResponseRenault);
		Test.stopTest();
	}

	//DACIA: test resend activation email functionnality
	static testmethod void testResendActivationEmail_Dacia() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDacia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.resendActivationEmail();
		system.assertNotEquals(null, ext.LastResponseDacia);
		Test.stopTest();
	}

	//RENAULT: test change email functionnality
	static testmethod void testchangeEmail_Renault() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.newEmail = 'test@kmail.com';
		ext.changeEmail();
		system.assertNotEquals(null, ext.LastResponseRenault);
		Test.stopTest();
	}

	//DACIA: test change email functionnality
	static testmethod void testchangeEmail_Dacia() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDacia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.newEmail = 'test@kmail.com';
		ext.changeEmail();
		system.assertNotEquals(null, ext.LastResponseDacia);
		Test.stopTest();
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	// --------- DELETE FUNCTION

	static testmethod void testDeletion_Renault_TooManyAccounts() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc_4();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.deleteGigyaAccounts();
		system.assertNotEquals(null, ext.LastResponseRenault);
		system.assertEquals( system.Label.GIGYA_520 ,ext.LastResponseRenault.code);
		List<User> listUsers = [SELECT Id FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( 1, listUsers.size() );
		Test.stopTest();
	}

	static testmethod void testDeletion_Renault_DeletionFails() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
		mock.setFunction_OK();
		mock.setFunction_ErrorCode();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.deleteGigyaAccounts();
		system.assertNotEquals(null, ext.LastResponseRenault);
		system.assertEquals( system.Label.GIGYA_599 ,ext.LastResponseRenault.code);
		List<User> listUsers = [SELECT Id FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( 1, listUsers.size() );
		Test.stopTest();
	}

	static testmethod void testDeletion_Renault_DeletionOK() {
		insertPersonalAccount();
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		User usr = [SELECT Id, isActive FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( true, usr.isActive );
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
		mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		Test.startTest();
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaRenault();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Renault.name();
		ext.deleteGigyaAccounts();
		Test.stopTest();
		system.assertNotEquals(null, ext.LastResponseRenault);
		system.assertEquals( system.Label.GIGYA_000 ,ext.LastResponseRenault.code);
		system.assertEquals( true, ext.DeletionInProgress);
		system.assertEquals( 1, ext.DeletionCountPoller);
		usr = [SELECT Id, isActive FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( false, usr.isActive );
	}

	static testmethod void testDeletion_Dacia_TooManyAccounts() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc_4();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDAcia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.deleteGigyaAccounts();
		system.assertNotEquals(null, ext.LastResponseDacia);
		system.assertEquals( system.Label.GIGYA_520 ,ext.LastResponseDacia.code);
		List<User> listUsers = [SELECT Id FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( 1, listUsers.size() );
		Test.stopTest();
	}

	static testmethod void testDeletion_Dacia_DeletionFails() {
		insertPersonalAccount();
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
		mock.setFunction_OK();
		mock.setFunction_ErrorCode();
    	Test.setMock(HttpCalloutMock.class, mock );
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Test.startTest();
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDacia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.deleteGigyaAccounts();
		system.assertNotEquals(null, ext.LastResponseDacia);
		system.assertEquals( system.Label.GIGYA_599 ,ext.LastResponseDacia.code);
		List<User> listUsers = [SELECT Id FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( 1, listUsers.size() );
		Test.stopTest();
	}

	static testmethod void testDeletion_Dacia_DeletionOK() {
		insertPersonalAccount();
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		User usr = [SELECT Id, isActive FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( true, usr.isActive );
		//Set the mock with result OK
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
		mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		Test.startTest();
		//Load the Gigya data for Renault and Dacia
		ext.loadGigyaDacia();
		//trigger the test for Renault and check the Result
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		ext.deleteGigyaAccounts();
		Test.stopTest();
		system.assertNotEquals(null, ext.LastResponseDacia);
		system.assertEquals( system.Label.GIGYA_000 ,ext.LastResponseDacia.code);
		system.assertEquals( true, ext.DeletionInProgress);
		system.assertEquals( 1, ext.DeletionCountPoller);
		usr = [SELECT Id, isActive FROM User WHERE AccountId =:acc.Id];
		system.assertEquals( false, usr.isActive );
	}

	//DELETION: test the display of the confirmation message
	static testmethod void testDeletion_ConfirmationMessage() {
		insertPersonalAccount();
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		system.assertEquals(false, ext.DisplayRenaultDeletionPanel);
		system.assertEquals(false, ext.DisplayDaciaDeletionPanel);
		//check Renault
		ext.mBrand = 'Renault';
		ext.displayDeletePanel();
		system.assertEquals(true, ext.DisplayRenaultDeletionPanel);
		system.assertEquals(false, ext.DisplayDaciaDeletionPanel);
		ext.hideDeletePanel();
		system.assertEquals(false, ext.DisplayRenaultDeletionPanel);
		system.assertEquals(false, ext.DisplayDaciaDeletionPanel);
		//Check Dacia
		ext.mBrand = 'Dacia';
		ext.displayDeletePanel();
		system.assertEquals(false, ext.DisplayRenaultDeletionPanel);
		system.assertEquals(true, ext.DisplayDaciaDeletionPanel);
		ext.hideDeletePanel();
		system.assertEquals(false, ext.DisplayRenaultDeletionPanel);
		system.assertEquals(false, ext.DisplayDaciaDeletionPanel);
	}

	//Deletion: test the poller
	static testMethod void testDeletion_PollerCounter() {
		insertPersonalAccount();
		//Prepare the account
		Account acc = [SELECT Id, Country__c FROM Account];
		Myr_AccountGigyaRun_EXT ext = new Myr_AccountGigyaRun_EXT( new ApexPages.StandardController(acc));
		//Launch the delete and check the poller after
		ext.mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		Test.startTest();
		ext.deleteGigyaAccounts();
		Test.stopTest();
		system.assertEquals( 3, Myr_AccountGigyaRun_EXT.MAX_COUNT_DELETION_POLLER);
		//Check the poller
		system.assertEquals( true, ext.DeletionInProgress);
		system.assertEquals( 1, ext.DeletionCountPoller);
		//Check 1
		ext.refreshData();
		system.assertEquals( true, ext.DeletionInProgress);
		system.assertEquals( 2, ext.DeletionCountPoller);
		//Check 2
		ext.refreshData();
		system.assertEquals( false, ext.DeletionInProgress);
		system.assertEquals( 3, ext.DeletionCountPoller);
	}
}