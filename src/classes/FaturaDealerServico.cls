public class FaturaDealerServico {

    public static Id UNPROCESSED    = Utils.getRecordTypeId('FaturaDealer__c', 'Unprocessed');
    public static Id VALID          = Utils.getRecordTypeId('FaturaDealer__c', 'Valid');
    public static Id INVALID        = Utils.getRecordTypeId('FaturaDealer__c', 'Invalid');
    public static ID NOTELIGIGLE    = Utils.getRecordTypeId('FaturaDealer__c', 'Not_Eligible');

    private Map<String, FaturaDealer__c> tratarDuplicidade(List<FaturaDealer__c> scope) {
        System.debug('Iniciando o tratamento de duplicidade.');

        Map<String, FaturaDealer__c> mapCount = new Map<String, FaturaDealer__c>();

        for(FaturaDealer__c fatD : scope) {

            System.debug('Fat: ' + fatD);

            if(fatD.RecordTypeId == UNPROCESSED) {
                fatD.RecordTypeId = (fatD.Error_Messages__c == null) ? VALID : INVALID;

                if(fatD.NFBIREMI__c != null && fatD.NFNRONFI__c != null) {

                    Pattern pat = Pattern.compile('([0]+)');
                    Matcher matcher = pat.matcher(fatD.NFNRONFI__c);

                    String countStg = null;
                    if(matcher.matches() && fatD.NFTIPONF__c.equalsIgnoreCase('OS')) {
                        countStg = fatD.NFBIREMI__c + fatD.NFNRONOS__c + fatD.NFTIPREG__c;

                    } else {
                        countStg = fatD.NFBIREMI__c + fatD.NFNRONFI__c + fatD.NFTIPREG__c;

                    }

                    fatD.Count__c = countStg;
                    mapCount.put(countStg, fatD);
                }
            }
        }

        try {
            Database.update(scope);

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);

        }

        return mapCount;
    }

    public void tratarHistorico(Map<String, FaturaDealer__c> mapCount) {
        System.debug('Iniciando tratamento de historico');
        try {
            //obter historico de faturas
            List<FaturaDealer__c> fatHistList = [
                SELECT Id, CreatedDate, Count__c, Reprocessed__c, Corrected__c, RecordTypeId, NFDTANFI__c, NFSEQUEN__c, NFNROREG__c
                FROM FaturaDealer__c
                WHERE Count__c =: mapCount.keySet() AND RecordTypeId !=: NOTELIGIGLE AND RecordTypeId !=: UNPROCESSED
                ORDER BY Count__c LIMIT 999
            ];

            System.debug('Fat Historico: ' + fatHistList);

            if(fatHistList.isEmpty()) {
                return;
            }

            //Categorizar as faturas utilizando a chave Count__c
            Map<String, List<FaturaDealer__c>> mapOrder = new Map<String, List<FaturaDealer__c>>();
            for(FaturaDealer__c fat: fatHistList) {
                List<FaturaDealer__c> categoria = mapOrder.get(fat.Count__c);//obter categoria
                if(categoria == null) {// nao exite a categoria?
                    categoria = new List<FaturaDealer__c>();
                    mapOrder.put(fat.Count__c, categoria);//criar categoria
                }
                categoria.add(fat);//add a categoria
            }

            List< List<FaturaDealer__c> > categoriaList = mapOrder.values();
            for(List<FaturaDealer__c> cat : categoriaList) {

                List<FaturaDealerWrapper> wrapList = new List<FaturaDealerWrapper>();
                for(FaturaDealer__c item : cat) {
                    wrapList.add(new FaturaDealerWrapper(item));
                }
                wrapList.sort();//ordenar as faturas da categoria

                FaturaDealerWrapper fatNew = wrapList.remove(0);

                //A ultima fatura esta valida?
                if(fatNew.fat.RecordTypeId == VALID) {
                    for(FaturaDealerWrapper item : wrapList) {
                        //existe registro de fatura invalida?
                        if(item.fat.RecordTypeId == INVALID) {
                            fatNew.fat.Corrected__c = true;
                        }
                    }
                }

                for(FaturaDealerWrapper item : wrapList) {
                    item.fat.Reprocessed__c = true;
                }
            }

            UPDATE fatHistList;

        } catch(Exception ex) {
            System.debug('Ex REP:' + ex);

        }
    }

    public void iniciarValidacao(List<FaturaDealer__c> scope) {
        Map<String, List<FaturaDealer__c>> mapaBirO = new Map<String, List<FaturaDealer__c>>();
        Map<String, List<FaturaDealer__c>> mapaBirE = new Map<String, List<FaturaDealer__c>>();

        for(FaturaDealer__c fatD : scope) {
            System.debug('Fatura selecionada : ' + fatD);

            fatD.Error_Messages__c = null;

            if(fatD.NFTIPREG__c != null && fatD.NFBANDEI__c != null) {
                if(isValidar(fatD)) {
                    validar(fatD, mapaBirO, mapaBirE);

                } else {
                    fatD.RecordTypeId = NOTELIGIGLE;

                }

            } else {
                fatD.RecordTypeId = INVALID;

                if(fatD.NFTIPREG__c == null) {
                    msgErroCampoObrigado(fatD, 'NFTIPREG__c');
                }

                if(fatD.NFBANDEI__c == null) {
                    msgErroCampoObrigado(fatD, 'NFBANDEI__c');
                }
            }
        }

        validarBirOExiste(mapaBirO);
        validarBirEExiste(mapaBirE);

        Map<String, FaturaDealer__c> mapCount = tratarDuplicidade(scope);
        tratarHistorico(mapCount);
    }

    private Boolean isValidar(FaturaDealer__c fatD) {
        return (fatD.NFTIPREG__c.equals('10') || fatD.NFTIPREG__c.equals('99')) && fatD.NFBANDEI__c.equals('REN');
    }

    private void validar(FaturaDealer__c fatD, Map<String, List<FaturaDealer__c>> mapaBirO, Map<String, List<FaturaDealer__c>> mapaBirE) {
        Datetime dataTemp = createDate(fatD, fatD.NFDTANFI__c, 'NFDTANFI');
        if(dataTemp != null) {
            fatD.NFDTANFI_Mirror__c = dataTemp.date();
        }

        dataTemp = createDate(fatD, fatD.NFDTASIS__c, 'NFDTASIS');
        if(dataTemp != null) {
            fatD.NFDTASIS_Mirror__c = dataTemp.date();
        }

        if(String.isEmpty(fatD.NFNRONFI__c)) {
            msgErroCampoObrigado(fatD, 'Num_NF');
        }

        validarBirOrigem(fatD, mapaBirO);
        validarBirEmissao(fatD, mapaBirE);
        validarCodOperacao(fatD);
        validarTipoNota(fatD);
        validarCpfCnpj(fatD);

        if(fatD.NFCODOPR_Mirror__c != null /*&& fatD.NFTIPONF_Mirror__c != null*/) {
            validarNf(fatD);
        }
    }

    /**
     * Valida conteudo da bir origem
     */
    private void validarBirOrigem(FaturaDealer__c fat, Map<String, List<FaturaDealer__c>> mapaBir) {
        //origem do arquivo
        if(String.isEmpty(fat.NFBIRENV__c)) {
            msgErroCampoObrigado(fat, 'BIR_Origem');
            return;
        }

        if(fat.NFBIRENV__c.length() != 8 || !fat.NFBIRENV__c.startsWith('0760')) {
            msgErroConteudoInvalido(fat, 'BIR_Origem', fat.NFBIRENV__c);
            return;
        }

        addBirMapa(fat, fat.NFBIRENV__c, mapaBir);
        fat.NFBIRENV_Mirror__c = null;
    }

    /**
    * Valida conteudo da bir emissao
    */
    private void validarBirEmissao(FaturaDealer__c fat, Map<String, List<FaturaDealer__c>> mapaBir) {
        //origem do arquivo
        if(String.isEmpty(fat.NFBIREMI__c)) {
            msgErroCampoObrigado(fat, 'BIR_Emis_NF');
            return;
        }

        if(fat.NFBIREMI__c.length() != 8 || !fat.NFBIREMI__c.startsWith('0760')) {
            msgErroConteudoInvalido(fat, 'BIR_Emis_NF', fat.NFBIREMI__c);
            return;
        }

        addBirMapa(fat, fat.NFBIREMI__c, mapaBir);
        fat.NFBIREMI_Mirror__c = null;
    }

    private void addBirMapa(FaturaDealer__c fat, String birInput, Map<String, List<FaturaDealer__c>> mapaBir) {
        String bir = tratarBir(birInput);
        List<FaturaDealer__c> fatList = mapaBir.get(bir);
        if(fatList == null) {
            fatList = new List<FaturaDealer__c>();
            mapaBir.put(bir, fatList);
        }
        fatList.add(fat);
    }

    private String tratarBir(String bir) {
        return bir.substring(1, bir.length());
    }

    /**
     * Valida se as Birs origem existe
     */
    private void validarBirOExiste(Map<String, List<FaturaDealer__c>> mapaBir) {
        List<Account> accConce = obterConcessionaria(mapaBir);

        for(Account acc: accConce) {
            List<FaturaDealer__c> fatL = mapaBir.get(acc.IDBIR__c);
            for(FaturaDealer__c fat : fatL) {
                fat.NFBIRENV_Mirror__c = acc.Id;
            }
        }

        List<List<FaturaDealer__c>> v = mapaBir.values();
        for(List<FaturaDealer__c> g : v) {
            for(FaturaDealer__c fat : g) {
                if(fat.NFBIRENV_Mirror__c == null) {
                    msgErroReferenciaInvalida(fat, 'BIR_Origem', fat.NFBIRENV__c);
                }
            }
        }
    }

    /**
* Valida se as Birs emissao existe
*/
    private void validarBirEExiste(Map<String, List<FaturaDealer__c>> mapaBir) {
        List<Account> accConce = obterConcessionaria(mapaBir);

        for(Account acc: accConce) {
            List<FaturaDealer__c> fatL = mapaBir.get(acc.IDBIR__c);
            for(FaturaDealer__c fat : fatL) {
                fat.NFBIREMI_Mirror__c = acc.Id;
            }
        }
        List<List<FaturaDealer__c>> v = mapaBir.values();
        for(List<FaturaDealer__c> g : v) {
            for(FaturaDealer__c fat : g) {
                if(fat.NFBIREMI_Mirror__c == null) {
                    msgErroReferenciaInvalida(fat, 'BIR_Emis_NF', fat.NFBIREMI__c);
                }
            }
        }
    }

    private List<Account> obterConcessionaria(Map<String, List<FaturaDealer__c>> mapaBir) {
        List<String> birList = new List<String>();
        for(String bir :  mapaBir.keySet()) {
            birList.add(bir);
        }

        return [
            SELECT Id, IDBIR__c
            FROM Account
            WHERE IDBIR__c =: birList AND Dealer_Status__c = 'Active'
        ];
    }

    /**
* Validar codigo da operacao
*/
    private void validarCodOperacao(FaturaDealer__c fat) {
        fat.NFCODOPR_Mirror__c = null;

        if(String.isEmpty(fat.NFCODOPR__c)) {
            msgErroCampoObrigado(fat, 'Cod_Operacao');
            return;
        }

        Set<String> codList = new Set<String> { '19',
            '21',
            '22',
            '25',
            '26',
            '44',
            '66',
            '67',
            '98'
            };

                if(!codList.contains(fat.NFCODOPR__c)) {
                    msgErroConteudoInvalido(fat, 'Cod_Operacao', fat.NFCODOPR__c);
                    return;
                }
        fat.NFCODOPR_Mirror__c = fat.NFCODOPR__c;
    }

    /**
     * Validar tipo da nota
     */

    private void validarTipoNota(FaturaDealer__c fat) {
        if(fat.NFTIPREG__c.equals('99')) {
            return;
        }

        fat.NFTIPONF_Mirror__c = null;

        if(String.isEmpty(fat.NFTIPONF__c)) {
            msgErroCampoObrigado(fat, 'Tipo_NF');
            return;
        }

        Set<String> codList = new Set<String> { 'VN', 'vn',
            'V', 'v',
            'SC', 'sc',
            'VP', 'vp',
            'SN', 'sn',
            'CA', 'ca',
            'GA', 'ga',
            'SI','si',
            'TR', 'tr',
            'CO', 'co',
            'DM', 'dm',
            'OS', 'os',
            'DE', 'de',
            'CM', 'cm',
            'SO', 'so',
            'EN', 'en',
            'SR', 'sr',
            'OU', 'ou' };

                if(!codList.contains(fat.NFTIPONF__c)) {
                    msgErroConteudoInvalido(fat, 'Tipo_NF', fat.NFTIPONF__c);
                    return;
                }

        fat.NFTIPONF_Mirror__c = fat.NFTIPONF__c;
    }

    /**
     * Validar o campo CPF CNPJ
     */

    private void validarCpfCnpj(FaturaDealer__c fat) {
        if(String.isEmpty(fat.NFCPFCGC__c)) {
            msgErroCampoObrigado(fat, 'CPF/CNPJ');
            return;
        }

        if(String.isEmpty(fat.NFTIPCLI__c)) {
            msgErroCampoObrigado(fat, 'NFTIPCLI');
            return;
        }

        if(fat.NFTIPCLI__c.equalsIgnoreCase('F') &&
           fat.NFCPFCGC__c.equalsIgnoreCase('Estrangeiro')) {
               fat.NFCPFCGC_Mirror__c = fat.NFCPFCGC__c;
               return;
           }

        Pattern pat = Pattern.compile('([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})');
        Matcher matcher = pat.matcher(fat.NFCPFCGC__c);

        if(!matcher.matches()) {
            msgErroConteudoInvalido(fat, 'CPF/CNPJ', fat.NFCPFCGC__c);
            return;
        }

        fat.NFCPFCGC_Mirror__c = fat.NFCPFCGC__c;
    }

    private void validarNf(FaturaDealer__c fat) {

        if(!String.isEmpty(fat.NFNRONFI__c)) {
            Pattern pat = Pattern.compile('([0]+)');
            Matcher mat = pat.matcher(fat.NFNRONFI__c);

            if(mat.matches() && (String.isEmpty(fat.NFTIPONF__c) || !fat.NFTIPONF__c.equalsIgnoreCase('OS'))) {
                msgErroConteudoInvalido(fat, 'Num_NF', fat.NFNRONFI__c);
                return;
            }
        }

        Set<String> cod = new Set<String> {'19', '25'};
            if(!cod.contains(fat.NFCODOPR_Mirror__c)) {
                return;
            }
        Set<String> tip = new Set<String> { 'VN', 'V', 'GA', 'SC', 'SO'};
            if(!fat.NFTIPREG__c.equals('99') && !tip.contains(fat.NFTIPONF_Mirror__c)) {
                return;
            }
        //if(!cod.contains(fat.NFCODOPR_Mirror__c) || !tip.contains(fat.NFTIPONF_Mirror__c) ) {
        //   return;
        //}

        if(String.isEmpty(fat.NFEMAILS__c)) {
            msgErroCampoObrigado(fat, 'E-mail');

        } else {
            Pattern p1 = Pattern.compile('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$');
            Pattern p2 = Pattern.compile('^[_A-Za-z0-9-\\+]([a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+)@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$');
            Matcher mat1 = p1.matcher(fat.NFEMAILS__c);
            Matcher mat2 = p2.matcher(fat.NFEMAILS__c);

            if(!mat1.matches() || !mat2.matches()) {
                msgErroConteudoInvalido(fat, 'E-mail', fat.NFEMAILS__c);

            } else {
                fat.NFEMAILS_Mirror__c = fat.NFEMAILS__c;
            }
        }

        Integer numTel = 0;

        System.debug(fat.NFTELRES__c + ' ' + fat.NFDDDRES__c);
        System.debug(fat.NFTELCEL__c + ' ' + fat.NFDDDCEL__c);
        System.debug(fat.NFTELCOM__c + ' ' + fat.NFDDDCOM__c);
        System.debug(fat.NFNROFAX__c + ' ' + fat.NFDDDFAX__c);

        if(!String.isEmpty(fat.NFTELRES__c) && !String.isEmpty(fat.NFDDDRES__c)) {
            numTel++;
        }
        if(!String.isEmpty(fat.NFTELCEL__c) && !String.isEmpty(fat.NFDDDCEL__c)) {
            numTel++;
        }
        if(!String.isEmpty(fat.NFTELCOM__c) && !String.isEmpty(fat.NFDDDCOM__c)) {
            numTel++;
        }
        if(!String.isEmpty(fat.NFNROFAX__c) && !String.isEmpty(fat.NFDDDFAX__c)) {
            numTel++;
        }
        System.debug('Num valid: ' + numTel);
        if(numTel < 2) {
            addMsg(fat, 'Preenchimento obrigatório mínimo de 2 telefones | ');

        } else {
            fat.NFTELRES_Mirror__c = fat.NFTELRES__c;
            fat.NFTELCEL_Mirror__c = fat.NFTELCEL__c;
            fat.NFTELFAX_Mirror__c = fat.NFNROFAX__c;
            fat.NFTELCOM_Mirror__c = fat.NFTELCOM__c;

            try {fat.NFDDDRES_Mirror__c = Integer.valueOf(fat.NFDDDRES__c);
                } catch(Exception ex) {
                    System.debug('Erro:' + ex);
                    //msgErroConteudoInvalido(fat, 'DDD_Res', fat.NFDDDRES__c);
                }
            try {    fat.NFDDDCEL_Mirror__c = Integer.valueOf(fat.NFDDDCEL__c);
                } catch(Exception ex) {
                    System.debug('Erro:' + ex);
                    //msgErroConteudoInvalido(fat, 'DDD_Cel', fat.NFDDDCEL__c);
                }
            try {fat.NFDDDCOM_Mirror__c = Integer.valueOf(fat.NFDDDCOM__c);
                } catch(Exception ex) {
                    System.debug('Erro:' + ex);

                    //msgErroConteudoInvalido(fat, 'DDD_Com', fat.NFDDDCOM__c);
                }
            try {fat.NFDDDFAX_Mirror__c = Integer.valueOf(fat.NFDDDFAX__c);
                } catch(Exception ex) {
                    System.debug('Erro:' + ex);
                    //msgErroConteudoInvalido(fat, 'DDD_Com', fat.NFDDDFAX__c);
                }
        }

        /*if(String.isEmpty(fat.NFDDDRES__c)) {
            msgErroCampoObrigado(fat, 'DDD_Res');

        } else {

            try {
                fat.NFDDDRES_Mirror__c = Integer.valueOf(fat.NFDDDRES__c);

            } catch(Exception ex) {
                System.debug('Erro:' + ex);

                msgErroConteudoInvalido(fat, 'DDD_Res', fat.NFDDDRES__c);
            }
        }

        if(String.isEmpty(fat.NFTELRES__c)) {
            msgErroCampoObrigado(fat, 'Tel_Res');

        } else {
            fat.NFTELRES_Mirror__c = fat.NFTELRES__c;
        }

        if(String.isEmpty(fat.NFDDDCEL__c)) {
            msgErroCampoObrigado(fat, 'DDD_Cel');

        } else {
            try {
                fat.NFDDDCEL_Mirror__c = Integer.valueOf(fat.NFDDDCEL__c);

            } catch(Exception ex) {
                System.debug('Erro:' + ex);

                msgErroConteudoInvalido(fat, 'DDD_Cel', fat.NFDDDCEL__c);
            }

        }

        if(String.isEmpty(fat.NFTELCEL__c)) {
            msgErroCampoObrigado(fat, 'Tel_Cel');

        } else {
            fat.NFTELCEL_Mirror__c = fat.NFTELCEL__c;
        }*/

        if(String.isEmpty(fat.NFCHASSI__c)) {
            msgErroCampoObrigado(fat, 'Chassi');

        } else if(!isChassiValido(fat.NFCHASSI__c)) {
            msgErroConteudoInvalido(fat, 'Chassi', fat.NFCHASSI__c);

        } else {
            fat.NFCHASSI_Mirror__c = fat.NFCHASSI__c;

        }

        if(String.isEmpty(fat.NFANOFAB__c)) {
            msgErroCampoObrigado(fat, 'Ano_Fabric');

        } else if(fat.NFANOFAB__c.length() != 4) {
            msgErroConteudoInvalido(fat, 'Ano_Fabric', fat.NFANOFAB__c);

        } else {

            try {
                fat.NFANOFAB_Mirror__c = Integer.valueOf(fat.NFANOFAB__c);

            } catch(Exception ex) {
                System.debug('Erro:' + ex);

                msgErroConteudoInvalido(fat, 'Ano_Fabric', fat.NFANOFAB__c);
            }
        }

        if(String.isEmpty(fat.NFANOMOD__c)) {
            msgErroCampoObrigado(fat, 'Ano_Mod');

        } else if(fat.NFANOMOD__c.length() != 4) {
            msgErroConteudoInvalido(fat, 'Ano_Mod', fat.NFANOMOD__c);

        } else {

            try {
                fat.NFANOMOD_Mirror__c = Integer.valueOf(fat.NFANOMOD__c);

            } catch(Exception ex) {
                System.debug('Erro:' + ex);

                msgErroConteudoInvalido(fat, 'Ano_Mod', fat.NFANOMOD__c);
            }
        }
    }

    /**
     * Verificar se o CHASSI e valido
     */

    private Boolean isChassiValido(String chassi) {
        Pattern p = Pattern.compile( '([a-zA-Z0-9]{17})' );
        Matcher matcher = p.matcher(chassi);
        if(!matcher.matches()) {
            return false;
        }

        p = Pattern.compile('([' + chassi.substring(0, 1) + ']+)');
        matcher = p.matcher(chassi);
        if(matcher.matches()) {
            return false;
        }

        String ch = chassi.substring(11, 17);
        p = Pattern.compile( '([0-9]{1,6})' );
        matcher = p.matcher(ch);
        if(!matcher.matches()) {
            return false;
        }

        return true;
    }

    private Datetime createDate(FaturaDealer__c fat, String data, String campo) {
        if(String.isEmpty(data)) {
            msgErroCampoObrigado(fat, campo);
            return null;
        }

        try {
            Integer year    = Integer.valueOf(data.substring(0, 4));
            Integer month   = Integer.valueOf(data.substring(4, 6));
            Integer day     = Integer.valueOf(data.substring(6, 8));

            if(year == 0 || day == 0 || month == 0) {
                msgErroCampoObrigado(fat, campo);
                return null;
            }

            if(year < 2000) {
                msgErroConteudoInvalido(fat, campo, data);
                return null;
            }

            return Datetime.newInstance(year, month, day);

        } catch (Exception ex) {
            System.debug('Erro:' + ex);

            msgErroConteudoInvalido(fat, campo, data);
        }
        return null;
    }

    private void msgErroReferenciaInvalida(FaturaDealer__c fat, String campo, String conteudo) {
        String temp  = 'Preenchimento incorreto campo ' + campo + ' concessionária inexistente. Recebido: ' + conteudo + ' | ';
        addMsg(fat, temp);
    }

    private void msgErroConteudoInvalido(FaturaDealer__c fat, String campo, String conteudo) {
        String temp =
            'O conteúdo do campo ' + campo +' é inválido. Recebido: ' + conteudo + ' | ';
        addMsg(fat, temp);
    }

    private void msgErroCampoObrigado(FaturaDealer__c fat, String campo) {
        String temp =
            'Campo obrigatório ' + campo + ' não está preenchido' + ' | ';
        addMsg(fat, temp);
    }

    /**
	 * Adicionar mensagem
	 */
    private void addMsg(FaturaDealer__c fat, String msg) {
        if(fat.Error_Messages__c == null) {
            fat.Error_Messages__c = '';
        }
        fat.Error_Messages__c = fat.Error_Messages__c + msg;
    }

    public class FaturaDealerWrapper implements Comparable {
        public FaturaDealer__c fat {get; set;}
        // Constructor
        public FaturaDealerWrapper(FaturaDealer__c f) {
            this.fat = f;
        }
        public Integer compareTo(Object compareTo) {
            FaturaDealerWrapper compareToOppy = (FaturaDealerWrapper) compareTo;

            Integer returnValue = 0;

            /*if(String.isEmpty(fat.NFDTANFI__c) && !String.isEmpty(compareToOppy.fat.NFDTANFI__c)) {
                return 1;

            } else if(!String.isEmpty(fat.NFDTANFI__c) && String.isEmpty(compareToOppy.fat.NFDTANFI__c)) {
                return -1;

            } else*/ if(!String.isEmpty(fat.NFDTANFI__c) && !String.isEmpty(compareToOppy.fat.NFDTANFI__c)) {
                try {
                    Integer a = Integer.valueof(fat.NFDTANFI__c);
                    Integer b = Integer.valueof(compareToOppy.fat.NFDTANFI__c);

                    if(a > b ) {
                        return -1;
                    } else if(a < b) {
                        return 1;
                    }
                } catch(Exception ex) {
                    System.debug('Ex:'+ ex);
                }
            }

            /*if(String.isEmpty(fat.NFSEQUEN__c) && !String.isEmpty(compareToOppy.fat.NFSEQUEN__c)) {
                return 1;

            } else if(!String.isEmpty(fat.NFSEQUEN__c) && String.isEmpty(compareToOppy.fat.NFSEQUEN__c)) {
                return -1;

            } else*/ if(!String.isEmpty(fat.NFSEQUEN__c) && !String.isEmpty(compareToOppy.fat.NFSEQUEN__c)) {
                try {
                    Integer a = Integer.valueof(fat.NFSEQUEN__c);
                    Integer b = Integer.valueof(compareToOppy.fat.NFSEQUEN__c);
                   if(a > b ) {
                        return -1;
                    } else if(a < b) {
                        return 1;
                    }
                } catch(Exception ex) {
                    System.debug('Ex:'+ ex);
                }

            }

            /*if(String.isEmpty(fat.NFNROREG__c) && !String.isEmpty(compareToOppy.fat.NFNROREG__c)) {
                return 1;

            } else if(!String.isEmpty(fat.NFNROREG__c) && String.isEmpty(compareToOppy.fat.NFNROREG__c)) {
                return -1;

            } else*/ if(!String.isEmpty(fat.NFNROREG__c) && !String.isEmpty(compareToOppy.fat.NFNROREG__c)) {
                    try {
                    Integer a = Integer.valueof(fat.NFNROREG__c);
                    Integer b = Integer.valueof(compareToOppy.fat.NFNROREG__c);
                    if(a > b ) {
                        return -1;
                    } else if(a < b) {
                        return 1;
                    }
                } catch(Exception ex) {
                    System.debug('Ex:' + ex);
                }

            }

            Long a = fat.CreatedDate.getTime();
            Long b = compareToOppy.fat.CreatedDate.getTime();

            if(a > b) {
                return -1;

            } else if(a < b) {
                return 1;

            }

            return returnValue;
        }
    }
}