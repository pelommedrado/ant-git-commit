@isTest
public class MonthlyGoalsTest {
    
    @isTest public static void testaGoalGroup(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(thisUser){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        acc.Dealer_Matrix__c = '6868';
        acc.Active_to_PPO__c = true;
        acc.IDBIR__c = '9875';
        insert(acc);
        
        Account acc1 = moc.criaAccountDealer();
        acc1.Dealer_Matrix__c = '6767';
        acc1.Active_to_PPO__c = true;
        acc1.IDBIR__c = '8789';
        insert(acc1);
        
        Account acc2 = moc.criaAccountDealer();
        acc2.Dealer_Matrix__c = '6767';
        acc2.Active_to_PPO__c = true;
        acc2.IDBIR__c = '0982';
        insert(acc2);
        
        Contact cnt1 = moc.criaContato();
        cnt1.AccountId = acc.id;
        insert(cnt1);
        
        Contact cnt2 = moc.criaContato();
        cnt2.AccountId = acc1.id;
        insert(cnt2);
        
        User u = moc.criaUser();
        u.ContactId = cnt1.Id;
        u.ProfileId = Utils.getProfileId('SFA - Seller');
        insert(u);
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
		psa.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'].Id;
        psa.AssigneeId = u.id;
		insert(psa);
            
        test.stopTest();
            
        }
        
    }
    
    @isTest public static void testaGoalGroupUser(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(thisUser){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        acc.Dealer_Matrix__c = '6868';
        acc.Active_to_PPO__c = true;
        acc.IDBIR__c = '9875';
        insert(acc);
        
        Contact cnt1 = moc.criaContato();
        cnt1.AccountId = acc.id;
        insert(cnt1);
        
        User u = moc.criaUser();
        u.ContactId = cnt1.Id;
        u.ProfileId = Utils.getProfileId('SFA - Seller');
        insert(u);
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
		psa.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'].Id;
        psa.AssigneeId = u.id;
		insert(psa);
            
        Monthly_Goal_Group__c mgc = new Monthly_Goal_Group__c();
        mgc.Matrix_Bir__c = '6868';
        mgc.Year__c = System.today().year();
        mgc.Month__c = System.now().format('MMMMM');
        insert mgc;
            
        test.stopTest();
            
        }
        
    }
    
    public static testMethod void testCreateDealerSellerGoal(){
        
        Test.startTest();
        
        Account a = new Account();
        a.Dealer_Matrix__c = '6767';
        a.DealershipStatus__c = 'Active';
        a.Country__c = 'Brazil';
        a.RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
        a.IDBIR__c = '2323';
        a.Name = 'Dealer';
        Insert a;
        
        List<Monthly_Goal_Group__c> groupGoals = new List<Monthly_Goal_Group__c>();
        
        Monthly_Goal_Group__c mg = new Monthly_Goal_Group__c();
        mg.Year__c = System.today().year();
        mg.Matrix_Bir__c = '6767';
        Insert mg;
        
        Monthly_Goal_Dealer__c mgd = new Monthly_Goal_Dealer__c();
        mgd.Monthly_Goal_Group__c = mg.Id;
        Insert mgd;
        
        groupGoals.add(mg);
        
        MonthlyGoalGroup_Controller.createDealerSellerGoal(groupGoals);
        
        Test.stopTest();
        
    }
    
    public static testMethod void testaBatch(){
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        acc.Dealer_Matrix__c = '6868';
        acc.Active_to_PPO__c = true;
        acc.IDBIR__c = '9875';
        insert acc;
        
        Account acc1 = moc.criaAccountDealer();
        acc1.Dealer_Matrix__c = '6767';
        acc1.Active_to_PPO__c = true;
        acc1.IDBIR__c = '8789';
        insert acc1;
        
        Account acc2 = moc.criaAccountDealer();
        acc2.Dealer_Matrix__c = '6767';
        acc2.Active_to_PPO__c = true;
        acc2.IDBIR__c = '0982';
        insert acc2;
        
        Contact cnt1 = moc.criaContato();
        cnt1.AccountId = acc.id;
        insert cnt1;
        
        Contact cnt2 = moc.criaContato();
        cnt2.AccountId = acc1.id;
        
        GroupGoalsScheduler ggs = new GroupGoalsScheduler();
        System.schedule('jobName', '0 0 1 1 * ?', ggs);
        
        test.stopTest();
        
    }

}