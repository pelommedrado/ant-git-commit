/** MyR BackEnd: Insert MyRenault Notifications linked to personal account
	A return code is given for eahc message in entry
	This class is usable outside from the webservice Myr_CreateCustMessage_WS.
	=> The main function returns a list of status code in the same order than the inputs
	
	V1.0: S. Ducamp / Atos / 08.07.2012 - initial revision 
	V2.0: D. Veron  / Atos / 13.07.2016 - Field Lifetime from object "Cust Mess Setting" to "Cust Mess Template"
	@version 16.10: SDP / F1415-US3696 / Clarify merge functionnality
**/
global without sharing class Myr_CreateCustMessage_Cls {  
	
	global enum Status {OK, KO}
	//Inner class to manage exception
	public class TestException extends Exception{} 
	
	// Inner class to manage the link between the message and the corresponding cell in the response table 
	global class SyncStatusMsg {
		public Integer index; //index in the status responses list (in the same order than the input messages)
		public Myr_CustMessage_Cls msg;
		//required constructor
		public SyncStatusMsg(Integer idx, Myr_CustMessage_Cls iMsg) {
			index = idx;
			msg = iMsg;
		}
	}
	
	// Inner class for the global response of the message creation
	global class CreateMsgResponse {
		public Status status; 
		public String message;
		public List<Myr_CustMessage_Cls.Status> listStatus;  
	}
	
	// Check that the strings are considered as valid html content for customer messages. Only few tags are accepted, there are listed into the setting
	//	@param String, the list of strings to check
	//	@return true if the string is valid for Customer Message 
	global static Boolean checkHtmlTags(String iHtmlContent) {
		system.debug('### Myr_CreateCustMessage_Cls - <checkHtmlTags> BEGIN: ' + iHtmlContent);
		String content = iHtmlContent;
		//first remove the tags from the string
		String strTags = CS04_MYR_Settings__c.getInstance().Myr_Customer_Message_HtmlTags__c;
		if( String.isBlank(strTags) ) return true;
		List<String> listTags = strTags.split(';', 0);
		content = content.unescapeHtml4();
		for( String tag : listTags) {
			content = content.replaceAll(tag, '');
		}
		system.debug('### Myr_CreateCustMessage_Cls - <checkHtmlTags> - after removing authorized html tags: ' + content);
		//then remove the input labels from the string
		content = content.replace(system.Label.Customer_Message_Input1, '');
		system.debug('### Myr_CreateCustMessage_Cls - <checkHtmlTags> - after removing input tags: ' + content);
		//finally, if it remains some html tags (<>), then it is not normal !!
		Boolean isOK = !Pattern.matches(system.Label.Customer_Message_Pattern_Html, content); 
		system.debug('### Myr_CreateCustMessage_Cls - <checkHtmlTags> - END: ' + isOK);
		return isOK;
	}
	
	//Insert the customer messages within salesforce
	//	 @param List<Myr_CustMessage_Cls>, the list of messages to insert
	//	 @return CreateMsgResponse, the global response of the function
	public static CreateMsgResponse createMessages(List<Myr_CustMessage_Cls> listMessages) {
		system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - BEGIN');
		CreateMsgResponse response = new CreateMsgResponse();
		Myr_CustMessage_Cls.GlobalFailureTest toTest = null; //for test purposes and code coverage
		Id idParentLog = null; //Id Parent Log to fill
		
		try {
			//INSERT LOG: do not interrupt the process if we can not insert the logs !  
			try {
				idParentLog = INDUS_Logger_CLS.addLog(INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateCustMessage_Cls', 'createMessages', String.valueOf(listMessages), null, 'inputs', INDUS_Logger_CLS.ErrorLevel.Info);
			} catch (Exception e) {
				//continue ....
			}
			
			//---CONTROL---: test the messages list
			if( listMessages == null || (listMessages != null && listMessages.size() == 0)) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_NoMessages;
				return returnResponse(response, idParentLog);
			}
			
			//---CONTROL---: if more than 100 messages ==> reject (global setting, could be reviewed)
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Check number of messages: ' + listMessages.size());
			Integer maxMessage = 200; //default value ...
			if( CS04_MYR_Settings__c.getInstance().Myr_Customer_Message_MaxMessages__c != null ) {
				Integer lMax = Integer.valueOf(CS04_MYR_Settings__c.getInstance().Myr_Customer_Message_MaxMessages__c);
				maxMessage = (lMax < maxMessage) ? lMax : maxMessage; //no more than 200 messages
			}
			system.debug('>>>>>>>> listMessages.size()='+listMessages.size()+', maxMessage='+maxMessage);
			if( listMessages.size() > maxMessage ) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_TooManyMessages.replace('{0}', String.ValueOf(maxMessage));
				return returnResponse(response, idParentLog);
			}
			
			//----STEP----: create the Status table
			response.listStatus = new List<Myr_CustMessage_Cls.Status>();
			List<SyncStatusMsg> listSyncStatusMsgs = new List<SyncStatusMsg>(); //"indirection" list to point the right cell in the final status table for the given message 
			
			//----STEP----: Check the validity of the inputs and initialize the status. 
			//        Prepare only the valid messages for insertion in a map with the index in the status table as a key (for later update).
			//		  Prepare the list of accounts to search for.
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Check validity of the inputs: ' + listMessages.size());
			Set<String> setAccSfdcId = new Set<String>();
			Set<String> setCustId = new Set<String>();
			Set<String> setTypeTempl = new Set<String>();
			Set<String> setVins = new Set<String>();
			for( Myr_CustMessage_Cls msg : listMessages ) {
				Myr_CustMessage_Cls.Status status = msg.isValid();
				response.listStatus.add(status);
				//if the status is ok, then the object is added to the map for later update
				//and we get accountIds and goldenId for search into salesforce
				if( status.isOK() ) {
					listSyncStatusMsgs.add( new SyncStatusMsg(response.listStatus.size() - 1, msg) );
					if( !String.isBlank(msg.accountSfdcId) ) {
						setAccSfdcId.add(msg.accountSfdcId);
					} else if ( !String.isBlank(msg.goldenId) ) {
						setCustId.add(msg.goldenId);
					}
					setTypeTempl.add(msg.typeId);
					setVins.add(msg.vin);
				}
				//check test global failure if needed
				if( Test.isRunningTest() && msg.getTestFailure() != null) {
					toTest = msg.getTestFailure();
				}
			}
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Check validity of the inputs - listSyncStatusMsgs.size(): ' + listSyncStatusMsgs.size());
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Check validity of the inputs - listSyncStatusMsgs=: ' + listSyncStatusMsgs);
			
			//----STEP----: prepare the check of the accounts
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the check of the accounts');
			Map<Id, Account> mapAcc = new Map<Id, Account>();
			try {
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_ACCOUNT ) throw new TestException('TEST ERROR SEARCH ACCOUNT');
	
				mapAcc = new Map<Id, Account> (
											[SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c 
											FROM Account
											WHERE (MyR_Status__c != '' OR MyD_Status__c != '')
											AND (
												Id IN :setAccSfdcId OR Tech_ACCExternalID__c IN :setCustId OR Bcs_Id__c IN :setCustId 
												OR Bcs_Id_Dacia__c IN :setCustId OR PeopleId_CN__c IN :setCustId
												)
											]);
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_SchAcc + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			Map<String, Account> mapTechAccExtId = new Map<String, Account>();
			Map<String, Account> mapBcsId = new Map<String, Account>();
			Map<String, Account> mapBcsDaciaId = new Map<String, Account>();
			Map<String, Account> mapPeopleId = new Map<String, Account>();
			for( Account acc : mapAcc.values() ) {
				if( !String.isBlank(acc.Tech_ACCExternalID__c) ) mapTechAccExtId.put(acc.Tech_ACCExternalID__c, acc);
				if( !String.isBlank(acc.Bcs_Id__c) ) mapBcsId.put(acc.Bcs_Id__c, acc);
				if( !String.isBlank(acc.Bcs_Id_Dacia__c) ) mapBcsDaciaId.put(acc.Bcs_Id_Dacia__c, acc);
				if( !String.isBlank(acc.PeopleId_CN__c) ) mapPeopleId.put(acc.PeopleId_CN__c, acc);
			} 
			
			//----STEP----: prepare the check of the vehicles: goal is simply to create a set containing the VIN concatenated with the accountId if they are linked.
			//				This set will be used later to check the relation and make faster the search of an existing relation
			Map<String, VRE_VehRel__c> mapVinsAccIdVre = new Map<String, VRE_VehRel__c>();
			try {
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_VEHREL ) throw new TestException('TEST ERROR SEARCH VEHICLE RELATIONS');
				
				List<VRE_VehRel__c> listRelations = [SELECT Id, Account__c, VIN__c, VIN__r.Name 
											FROM VRE_VehRel__c
											WHERE Account__c IN :mapAcc.keySet()
											AND VIN__r.Name IN :setVins]; //Limit also the account to search to the ones given in parameter (mandatory) to avoid accounts with thousands of query
				for( VRE_VehRel__c rel : listRelations ) {
					mapVinsAccIdVre.put( rel.Account__c + rel.VIN__r.Name, rel);
				}
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_SchVehRel  + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			
			//----STEP----: prepare the check of the users
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the check of the users');
			Map<String, User> mapAccIdUser = new Map<String, User>();
			try {
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_USER ) throw new TestException('TEST ERROR SEARCH USERS');
				
				List<User> listUsers = [SELECT Id, AccountId, LanguageLocaleKey, Country, RecordDefaultCountry__c 
										FROM User
										WHERE Profile.Name=:CS04_MYR_Settings__c.getInstance().Myr_Profile_Community_User__c
										AND AccountId IN :mapAcc.keySet()];
				for( User usr : listUsers) {
					mapAccIdUser.put(usr.AccountId, usr);
				}
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_SchUsers + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			
			//----STEP----: prepare the check of the templates
			//one template per type, country, language, brand, so the key will be a uppercase concatenation of all these field in order to accelerate later research !
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the check of the templates. setTypeTempl' + setTypeTempl); 
			Map<String, Customer_Message_Templates__c> mapStrTemplates = new Map<String, Customer_Message_Templates__c>();
			try {
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_TEMPL ) throw new TestException('TEST ERROR SEARCH TEMPLATES');
				
				List<Customer_Message_Templates__c> listTemplates = 
					[SELECT Id, Body__c, Brand__c, Country__c, CtaHref__c, CtaHrefMobile__c, CtaLabel__c, Language__c, 
							MessageType__c, Summary__c, Title__c, Criticity__c,
							MessageType__r.Name, Lifetime__c, 
							MessageType__r.MergeKeys__c, MessageType__r.MergeAction__c, MessageType__r.MergeRefresXDaysValue__c, 
							MessageType__r.NbInputs__c, MessageType__r.Type__c, isActive__c, Name, Channel__c
					 FROM Customer_Message_Templates__c
					 WHERE MessageType__r.Name IN :setTypeTempl
					];
				Map<String, Schema.SObjectField> mapFieldsCustMess = Schema.SObjectType.Customer_Message__c.fields.getMap();
				for( Customer_Message_Templates__c templ : listTemplates ) {
					Boolean isValid = true;
					//check the template and reject it if it's not proper/ not valid matching keys
					if( !String.isBlank(templ.MessageType__r.MergeKeys__c) ) {
						//check the existence of the keys
						List<String> matchingKeys = templ.MessageType__r.MergeKeys__c.split(';', 0);
						for( String kStr : matchingKeys ) {
							if( !mapFieldsCustMess.containsKey(kStr) ) {
								isValid = false;
								break;
							}	
						}
					}
					//add the template
					if( isValid ) {
						String key = (templ.MessageType__r.Name + templ.Brand__c + templ.Country__c + templ.Language__c).toUpperCase();
						mapStrTemplates.put(key, templ);
					}				
				}
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_SchTmpl + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the check of the templates, mapStrTemplates.size()='+mapStrTemplates.values().size());
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the check of the templates, mapStrTemplates.keys()='+mapStrTemplates.keySet());
			
			if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.CR_GLOBAL ) throw new TestException('TEST ERROR UNEXPECTED ERROR OUTSIDE FROM TRY-CATCH');
			//---STEP---: For each remaining message:
			//	          1. check that we have found the account, otherwise the message is rejected
			//			  2. if the vin of a vehicle is filled, check that this vehicle is really linked to the given account, otherwise the message is rejected
			//            3. check that the country and language are found either in inputs either on the user, otherwise rejzect the message
			//            4. check that the message type exists (for the given brand, language and country), otherwise the message is rejected
			//            5. check that the couple country/language exists for the found message type otherwise the message is rejected. 
			//               If country and language are not filled in the input, then get these information from the associated HeliosCommunity user.
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Prepare the messages and check, listSyncStatusMsgs.size()='+listSyncStatusMsgs.size());
			Set<String> setAccIdMergeAction  = new Set<String>(); //list of the accounts linked to a valid message to insert and with a potential merge action
			Set<String> setTemplIdMergeAction  = new Set<String>(); //list of the templates linked to a valid message to insert and with a potential merge action
			for( Integer i = 0; i < listSyncStatusMsgs.size(); ++i ) {
				SyncStatusMsg syncMsg = listSyncStatusMsgs[i];
				//check account
				if( !String.isBlank(syncMsg.msg.accountSfdcId) && mapAcc.containsKey(syncMsg.msg.accountSfdcId) ) { 
					syncMsg.msg.account = syncMsg.msg.accountSfdcId;
				} else if( !String.isBlank(syncMsg.msg.goldenId) && mapTechAccExtId.containsKey(syncMsg.msg.goldenId) ) {
					syncMsg.msg.account = mapTechAccExtId.get(syncMsg.msg.goldenId).Id;
				} else if( !String.isBlank(syncMsg.msg.goldenId) && mapBcsId.containsKey(syncMsg.msg.goldenId) ) {
					syncMsg.msg.account = mapBcsId.get(syncMsg.msg.goldenId).Id;
				} else if( !String.isBlank(syncMsg.msg.goldenId) && mapBcsDaciaId.containsKey(syncMsg.msg.goldenId) ) {
					syncMsg.msg.account = mapBcsDaciaId.get(syncMsg.msg.goldenId).Id;
				} else if( !String.isBlank(syncMsg.msg.goldenId) && mapPeopleId.containsKey(syncMsg.msg.goldenId) ) {
					syncMsg.msg.account = mapPeopleId.get(syncMsg.msg.goldenId).Id;
				} else {
					response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS504;
					response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS504_Msg;
					listSyncStatusMsgs.remove(i);
					--i; //stay on the same index to go to the following message.
					continue; //skip to the next iteration
				}
				//check if the vin filled is really associated to the given account
				if( !String.isBLank(syncMsg.msg.vin) ) {
					if( mapVinsAccIdVre.containsKey(syncMsg.msg.account + syncMsg.msg.vin) ) {
						system.debug('search ' + syncMsg.msg.account + syncMsg.msg.vin + ' found');
						VRE_VehRel__c rel = mapVinsAccIdVre.get(syncMsg.msg.account + syncMsg.msg.vin);
						syncMsg.msg.setVehicle( new VEH_Veh__c(Id=rel.VIN__c, Name=rel.VIN__r.Name) ); //affect a fake vehicle as it is not inserted later
					} else {
						system.debug('search ' + syncMsg.msg.account + syncMsg.msg.vin +' not found');
						response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS505;
						response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS505_Msg.replace('{0}', syncMsg.msg.vin).replace('{1}', syncMsg.msg.account);
						listSyncStatusMsgs.remove(i);
						--i; //stay on the same index to go to the following message.
						continue; //skip to the next iteration 
					}
				}
				//check country and language settings
				if( String.isBlank(syncMsg.msg.country) && mapAccIdUser.containsKey(syncMsg.msg.account) ) {
					syncMsg.msg.country = mapAccIdUser.get(syncMsg.msg.account).RecordDefaultCountry__c;
				}
				if( String.isBlank(syncMsg.msg.language) && mapAccIdUser.containsKey(syncMsg.msg.account) ) {
					syncMsg.msg.language = mapAccIdUser.get(syncMsg.msg.account).LanguageLocaleKey;
				}
				if( String.isBlank(syncMsg.msg.country) || String.isBlank(syncMsg.msg.language) ) {
					response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS502;
					response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS502_Msg + ' country and/or language not given in inputs or not found on the user linked to the account';
					listSyncStatusMsgs.remove(i);
					--i; //stay on the same index to go to the following message.
					continue; //skip to the next iteration
				}
				//check template
				String key = (syncMsg.msg.typeId + syncMsg.msg.brand.name() + syncMsg.msg.country + syncMsg.msg.language).toUpperCase();
				if( mapStrTemplates.containsKey(key) ) {
					syncMsg.msg.setTemplate(mapStrTemplates.get(key)); 
					//update country and language with proper values to be able to query salesforce with the right values
					syncMsg.msg.country = syncMsg.msg.getTemplate().Country__c;
					syncMsg.msg.language = syncMsg.msg.getTemplate().Language__c;
					//check that we have the right name of inputs to use the template
					if( syncMsg.msg.getNbInputsForTemplate() < syncMsg.msg.getTemplate().MessageType__r.NbInputs__c) {
						response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS503;
						response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS503_Msg + ' ' 
							+ syncMsg.msg.getTemplate().MessageType__r.Name + '(' + syncMsg.msg.getTemplate().MessageType__r.NbInputs__c + ' required, '
							+ syncMsg.msg.getNbInputsForTemplate() + ' given)';
						listSyncStatusMsgs.remove(i);
						--i; //stay on the same index to go to the following message.
						continue; //skip to the next iteration
					}
					//If the template is not activated, an error is returned
					if( ! syncMsg.msg.getTemplate().isActive__c) {
						response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS506; 
						response.listStatus[syncMsg.index].message = 'Template "' + syncMsg.msg.getTemplate().Name + '" not activated for the country "' + syncMsg.msg.getTemplate().Country__c + '" and the language "' + syncMsg.msg.getTemplate().Language__c + '"';
						listSyncStatusMsgs.remove(i);
						--i; //stay on the same index to go to the following message.
						continue; //skip to the next iteration
					}
				} else {
					response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS502;
					response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS502_Msg + ' template not found (' + syncMsg.msg.typeId + ',' + syncMsg.msg.brand.name() 
																+ ',' + syncMsg.msg.country + ',' + syncMsg.msg.language + ')';
					listSyncStatusMsgs.remove(i);
					--i; //stay on the same index to go to the following message.
					continue; //skip to the next iteration
				}
				//Prepare the Merge Action
				//Specificity: if we encounter the merge action keep, then we exclude the message from future upsert with the right success code !
				if( !String.isBlank(syncMsg.msg.getTemplate().MessageType__r.MergeAction__c) ) {
					setAccIdMergeAction.add(syncMsg.msg.account);
					setTemplIdMergeAction.add(syncMsg.msg.getTemplate().Id);
				}
				syncMsg.msg.prepareSfdcMessage();//prepare the message to upsert
			}
			
			//---STEP---: For the remaining message (account found and template found !)
			//		      search the existing messages matching the remaining messages only if we have a mergeaction set
			Map<String, Map<String, List<Customer_Message__c>>> mapAccTempDup = new Map<String, Map<String, List<Customer_Message__c>>>(); 
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Check duplicates, listSyncStatusMsgs.size()='+listSyncStatusMsgs.size());
			//get the potential duplicates	  
			try {
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_DUP ) throw new TestException('TEST ERROR SEARCH DUPLICATES');
				
				String queryAll = Myr_MyRenaultTools.buildSelectAllQuery(Customer_Message__c.class.getName(), null).Query;
				queryAll += ' WHERE Account__c IN :setAccIdMergeAction';
	            queryAll += ' AND Template__c IN :setTemplIdMergeAction';
	            List<Customer_Message__c> listDuplicates = Database.query(queryAll);
	            for( Customer_Message__c msg : listDuplicates ) {
	            	if( mapAccTempDup.containsKey(msg.Account__c) ) {
	            		if( mapAccTempDup.get(msg.Account__c).containsKey(msg.Template__c) ) {
	            			mapAccTempDup.get(msg.Account__c).get(msg.Template__c).add(msg);
	            		} else {
	            			mapAccTempDup.get(msg.Account__c).put(msg.Template__c, new List<Customer_Message__c>{msg});          			
	            		}
	            	} else {
	            		Map<String, List<Customer_Message__c>> mapTempDup = new Map<String, List<Customer_Message__c>>();
	            		mapTempDup.put( msg.Template__c, new List<Customer_Message__c>{msg} );
	            		mapAccTempDup.put( msg.Account__c, mapTempDup );
	            	}
	            }
	            
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_SchDup + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			//identify the duplicates and do what has to be done (refresh or replace ....)
			List<Customer_Message__c> listUpsertMsgs = new List<Customer_Message__c>(); 
			for( Integer i = 0; i < listSyncStatusMsgs.size(); ++i ) {
				SyncStatusMsg syncMsg = listSyncStatusMsgs[i];
				if( mapAccTempDup.containsKey(syncMsg.msg.account) ) {
					if( mapAccTempDup.get(syncMsg.msg.account).containsKey(syncMsg.msg.getTemplate().Id) ) {
						List<Customer_Message__c> listPotentialDuplicates = mapAccTempDup.get(syncMsg.msg.account).get(syncMsg.msg.getTemplate().Id);
						syncMsg.msg.applyDupAction(listPotentialDuplicates);
					}
				}
				//if( syncMsg.msg.getDupAction() != system.Label.Customer_Message_Tpl_Keep ) { //Dup found with keep action: do
				if( !syncMsg.msg.keepMsgInSfdcAsItIs() ) {
					listUpsertMsgs.add(syncMsg.msg.getSfdcMessage()); 
				} else {
					response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS003;
					response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS003_Msg;
					listSyncStatusMsgs.remove(i);
					--i; //stay on the same index to go to the following message.
				}
			}
			
			//FINALLY UPSERT THE DATABASE !!!!
			system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Upsert the database, listUpsertMsgs.size()='+listUpsertMsgs.size());
			try { 
				if( Test.isRunningTest() && toTest == Myr_CustMessage_Cls.GlobalFailureTest.DB_UPSERT ) throw new TestException('TEST ERROR DATABASE UPSERT');
				
				Database.UpsertResult[] db_results = Database.upsert(listUpsertMsgs, false);
				//transverse the results and set the code
				for(Integer i = 0; i < db_results.size(); ++i) {
					SyncStatusMsg syncMsg = listSyncStatusMsgs[i];
					Database.UpsertResult res = db_results[i];
					String dupAction = syncMsg.msg.getDupAction();
					system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Complete result for syncstatus=' + syncMsg + ', res=' + res + ', dupAction=' + dupAction);
					if( db_results[i].isSuccess() ) { //SUCCESS
						if( dupAction == system.Label.Customer_Message_Tpl_Replace ) {
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS001;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS001_Msg;
						} else if( dupAction == system.Label.Customer_Message_Tpl_Refresh ) {
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS002;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS002_Msg;
						} else if( dupAction == system.Label.Customer_Message_Tpl_Keep ) {
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS003;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS003_Msg;
						} else if( dupAction == system.Label.Customer_Message_Tpl_RefreshXDays ) {
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS004;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS004_Msg;
						} else { //insertion
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS000;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS000_Msg;
						}
					} else { //FAILED
						if( dupAction == system.Label.Customer_Message_Tpl_Replace ) {
							response.listStatus[syncMsg.index].code =system.Label.Customer_Message_WS06MS521;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS521_Msg;
						} else if( dupAction == system.Label.Customer_Message_Tpl_Refresh ) {
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS522;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS522_Msg;
						} else if( dupAction == system.Label.Customer_Message_Tpl_Keep ) {
							//should not occur as we are doing nothing
						} else { //insertion
							response.listStatus[syncMsg.index].code = system.Label.Customer_Message_WS06MS599;
							response.listStatus[syncMsg.index].message = system.Label.Customer_Message_WS06MS599_Msg;
						}
						//add the messages
						String errorMsg = '';
						for(Database.Error err : res.getErrors() ) {
							errorMsg += (errorMsg.length() > 0) ? ', ' : '';
							errorMsg += err.getMessage();
						}
						response.listStatus[syncMsg.index].message += (': ' + errorMsg);
					}
				}
			} catch (Exception e) {
				response.status = Status.KO;
				response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_DbUpsert + ' ' + e.getMessage();
				return returnResponse(response, idParentLog);
			}
			
			response.status = Status.OK;
			response.message = system.Label.Customer_Message_WS06MS000_Msg_WS;
			return returnResponse(response, idParentLog);
		
		} catch (Exception e ) { //GLOBAL CATCH FOR UNEXPECTED ERROR
			
			System.debug(e.getStackTraceString());
			response.status = Status.KO;
			response.message = system.Label.Customer_Message_WS06MS501_Msg_WS_UnexErr + ' ' + e.getMessage(); 
			return returnResponse(response, idParentLog);
		} 
	} 
	
	//Centralize the log and logger before returning the response
	global static CreateMsgResponse returnResponse(CreateMsgResponse response, Id idParentLog) {
		system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - response.status' + response.status);
		system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - response.message' + response.message);
		system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - Transverse the status: ');
		if( response.listStatus != null ) {
			for( Integer i = 0; i < response.listStatus.size(); ++i ) {
				system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - message ' + i + ': code=' + response.listStatus[i].code + ', message=' + response.listStatus[i].message);
			}
		}
		system.debug('### Myr_CreateCustMessage_Cls - <createMessages> - END');
		if( idParentLog != null ) {
			//INSERT LOG: do not interrupt the process if we can not insert the logs !  
			try {
				INDUS_Logger_CLS.addLogChild(idParentLog, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateCustMessage_Cls', 'createMessages', String.valueOf(response), null, 'outputs', INDUS_Logger_CLS.ErrorLevel.Info);
			} catch (Exception e) {
				//continue ....
			}
		}
		return response;
	}
	
}