/**
* Classe que será disparada pela trigger do objeto Opportunity após a inserção dos registros.
* @author Felipe Jesus Silva.
*/
public class VFC38_OpportunityAfterInsertExecution implements VFC37_TriggerExecution
{
    public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
    {
        List<Opportunity> lstSObjOpportunity = (List<Opportunity>) lstNewtData;
        
       // this.createTasks(lstSObjOpportunity);
        
    }
    
    
    /*Comentado por @Edvaldo - {kolekto} por não estar mais em uso. (chamada esta nesta classe e está comentada)
    
    private void createTasks(List<Opportunity> lstSObjOpportunity)
    {
        for(Opportunity sObjOpportunity : lstSObjOpportunity)
        {
            if(sObjOpportunity.RecordTypeId != Label.OPPRecordTypeId)
            VFC40_TaskBO.getInstance().createTasksFromOpportunities(lstSObjOpportunity);
            }
    }*/
}