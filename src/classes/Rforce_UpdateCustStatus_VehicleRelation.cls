// ---------------------------------------------------------------------------
// Project: RForce
// Name: Rforce_UpdateCustStatus_VehicleRelation
// Desc: Customer Status of Account
// CreatedBy: Sumanth(RNTBCI)
// CreatedDate: 05/12/2014
// ---------------------------------------------------------------------------
public class Rforce_UpdateCustStatus_VehicleRelation{
    public static void updatecustomerstatus(list <VRE_VehRel__c> listVRE){
        Set<Id> accids=new Set<Id>();
        //Getting List of accounts that the status has to be updated
        for(VRE_VehRel__c vr:listVRE){
            accids.add(vr.Account__c);          
        }
        try{
        //Getting all the accounts and vehicle relations of every account
           List<Account> aclist=[Select Id,CustmrStatus__c,(Select Id,Status__c from Vehicle_Relations__r) from Account where Id IN :accids]; 
           
           for(Account a: aclist){
               String custstatus=System.label.Acc_Prospect;
               for(VRE_VehRel__c vre : a.Vehicle_Relations__r){
                   if(vre.Status__c==system.label.VRE_Inactive){
                       custstatus=system.label.Acc_OldCustomer;
                   }else if(vre.Status__c==system.label.VRE_Active){
                       custstatus=system.label.Acc_Customer;
                       break;    
                   } 
                   system.debug('custstatus-->'+custstatus);                   
               }
               a.CustmrStatus__c=custstatus ;              
           }
           update aclist;
        }catch(Exception e){
            system.debug(e);
        }     
    }
    public static void updatecustomerstatus_ondelete(list <VRE_VehRel__c> listVRE){
        Set<Id> accids=new Set<Id>();
        Set<Id> delids=new Set<Id>();
        //Getting List of accounts that the status has to be updated
        for(VRE_VehRel__c vr:listVRE){
            accids.add(vr.Account__c); 
            delids.add(vr.Id);         
        }
        try{
        //Getting all the accounts and vehicle relations of every account
           List<Account> aclist=[Select Id,CustmrStatus__c,(Select Id,Status__c from Vehicle_Relations__r) from Account where Id IN :accids]; 
           
           for(Account a: aclist){
               String custstatus=System.label.Acc_Prospect;
               for(VRE_VehRel__c vre : a.Vehicle_Relations__r){
                   if(!delids.contains(vre.Id)){
                       if(vre.Status__c==system.label.VRE_Inactive){
                           custstatus=system.label.Acc_OldCustomer;
                       }else if(vre.Status__c==system.label.VRE_Active){
                           custstatus=system.label.Acc_Customer;
                           break;    
                       } 
                   }   
                   system.debug('custstatus-->'+custstatus);                   
               }
               a.CustmrStatus__c=custstatus ;              
           }
           update aclist;
        }catch(Exception e){
            system.debug(e);
        }     
    }         
}