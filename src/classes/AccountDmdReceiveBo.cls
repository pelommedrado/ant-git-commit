public class AccountDmdReceiveBo {
    
    public static final Set<String> setInterfaceProfiles = 
		new Set<String> { '<Renault> - Interface' };
            
    public static Id recordTypeDealer = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
    
    public void execInsert(List<Account> newList) {
        System.debug('AccountDmdReceiveBo.execInsert...');
        
        Set<Account> accDealerList = new Set<Account>();
        System.debug('trigger List:'+newList);
        for(Account acc : newList) {
            System.debug('acc:'+acc);
            System.debug('teste:'+(acc.Dealer_Status__c == 'Active'));
            if(acc.RecordTypeId == recordTypeDealer && acc.Dealer_Status__c == 'Active') {
                System.debug('!!!Executou');
                accDealerList.add(acc); 
            }
        }
        
        if(accDealerList.isEmpty()) {
            return;
        }
        
        for(Account ac : accDealerList) {
            ac.Dealer_Status__c = null;
            configAccount(ac);
        }
    }
    
    public void execUpdate(List<Account> newList, Map<Id, Account> oldMap) {
        System.debug('AccountDmdReceiveBo.execUpdate...');
        
        Set<Account> accDealerList = new Set<Account>();
        for(Account acc : newList) {
            if(acc.RecordTypeId == recordTypeDealer && acc.Dealer_Status__c == 'Active') {
                accDealerList.add(acc); 
            }
        }
        
        if(accDealerList.isEmpty()) {
            return;
        }
        
        for(Account acc: accDealerList) {
            final Account acOld = oldMap.get(acc.Id);
            //System.debug('acOld:' + acOld);
            //System.debug('acc:' + acc);
            if(acOld.RecordTypeId == recordTypeDealer) {
                //acc.Name = acc.UsualName__c;
                //acc.ShortndName__c = acc.UsualName__c;
                
                //acc.IDBIR__c = acOld.IDBIR__c;
                //acc.Tech_ACCExternalID__c = acOld.Tech_ACCExternalID__c;
                
                acc.Dealer_Status__c = acOld.Dealer_Status__c;
                //acc.NameZone__c = acOld.NameZone__c;
                acc.CompanyID__c = acOld.CompanyID__c;
                
                configAccount(acc);
            }
        }
    }
    
    private void configAccount(Account ac) {
        System.debug('!!!AccountDmdReceiveBo.configAccount');
        if(!String.isEmpty(ac.UsualName__c)) {
            ac.ShortndName__c = ac.UsualName__c;
            ac.Name	= ac.UsualName__c;
        }
        
        if(!String.isEmpty(ac.Tech_ACCExternalID__c)) {
            ac.Tech_ACCExternalID__c = removerZero(ac.Tech_ACCExternalID__c);
        }
        
        if(!String.isEmpty(ac.IDBIR__c)) {
            ac.IDBIR__c = removerZero(ac.IDBIR__c);
        }
        

        if(String.isNotEmpty(ac.NameZone__c) && ac.NameZone__c.length() > 5) {
            ac.NameZone__c = ac.NameZone__c.substring(4, 6);
        }
    }
    private String removerZero(String bir) {
        if(String.isEmpty(bir)) {
            return null;
        }
        Integer len = bir.length();
        if(bir.startsWith('0')) {
            return bir.subString(1, len);
        }
        return bir;
    }
}