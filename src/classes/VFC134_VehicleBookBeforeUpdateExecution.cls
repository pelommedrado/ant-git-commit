public class VFC134_VehicleBookBeforeUpdateExecution implements VFC37_TriggerExecution{

	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{	
		List<VehicleBooking__c> lstSObjCurrentVehicleBooking = (List<VehicleBooking__c>) lstNewtData;
		
		this.checkStatus(lstSObjCurrentVehicleBooking);
	}
	
	private void checkStatus(List<VehicleBooking__c> lstSObjCurrentVehicleBooking)
	{
		System.debug('### INICIANDO ###');
		
		for(VehicleBooking__c sObjCurrentVehicleBooking : lstSObjCurrentVehicleBooking)
		{
			
			System.debug('VBK STATUS:' + sObjCurrentVehicleBooking.Status__c );			
			
			if((String.isNotEmpty(sObjCurrentVehicleBooking.Status__c)) && (sObjCurrentVehicleBooking.Status__c.equals('Canceled')) && (sObjCurrentVehicleBooking.Quote__c != null))
			{
				System.debug('QUOTE ID:' + sObjCurrentVehicleBooking.Quote__c );
				
                try{
                
				Quote quote = VFC35_QuoteDAO.getInstance().fetchById(sObjCurrentVehicleBooking.Quote__c);

				System.debug('QUOTE STATUS:' + quote.Status );

				if(quote.Status == 'Pre Order Requested' ||
					quote.Status == 'Pre Order Sent' ||
					quote.Status == 'Billed' ){
					
					sObjCurrentVehicleBooking.addError('Veículo está relacionado a uma cotação com o status que não permite a alteração da reserva.');
                    }
                }catch(QueryException qe){
                	sObjCurrentVehicleBooking.addError('Reserva de veículo indisponível para cancelamento. Favor entrar em contato com o responsável pela reserva.');
                }
                
			}
		}
	}

}