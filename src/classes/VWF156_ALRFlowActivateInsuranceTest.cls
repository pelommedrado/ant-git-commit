@isTest
private class VWF156_ALRFlowActivateInsuranceTest {

    static testMethod void myUnitTest() {
        
        
        
        User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
    	Database.insert( manager );
        
        Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concession�ria teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
   		Database.insert( dealerAcc );
        
        contact contato = new contact(
         	AccountId = dealerAcc.id,
        	FirstName = 'teste',
        	LastName = 'test',
        	Email = 'teste@test.com', 
        	Phone =  '551122334455', 
        	PersMobPhone__c = '551122334455', 
        	ProPhone__c = '551122334455'
        );
        Database.insert(contato);
        
        //System.runAs(contato){
	        VWF156_ALRFlowActivateInsurance plugin = new VWF156_ALRFlowActivateInsurance();
	        Map<String,Object> inputParams = new Map<String,Object>();
	
	        string feedSubject = 'Flow is alive';
	        InputParams.put('subject', feedSubject);
	
	        Process.PluginRequest request = new Process.PluginRequest(inputParams);           
	        
	        plugin.invoke(request);
	        plugin.describe();
	        VWF156_ALRFlowActivateInsurance.getContact(''+contato.id);
        //}
    }
}