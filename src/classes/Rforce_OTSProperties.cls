public with sharing class Rforce_OTSProperties {
    public String OTSType {get;set;}
    public String OTSNo {get;set;}
    public String OTSDescription {get;set;}
    public String OTSTechnicalNoteNo {get;set;}
}