public class ValidationUtils {

	public static Boolean isCpfCnpj(String cpfCnpj) {
		if(String.isEmpty(cpfCnpj)) return false;
		String regex = '([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})';
		Pattern pat = Pattern.compile(regex);
		Matcher matcher = pat.matcher(cpfCnpj);
		return matcher.matches();
	}

	public static Boolean isEmail(String email) {
		if(String.isEmpty(email)) return false;
		Pattern p1 = Pattern.compile('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$');
		Pattern p2 = Pattern.compile('^[_A-Za-z0-9-\\+]([a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+)@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$');
		Matcher mat1 = p1.matcher(email);
		Matcher mat2 = p2.matcher(email);

		return mat1.matches() && mat2.matches();
	}

	public static Boolean isNumeroTelefone(String telefone) {
		final String pat = '^(?:(?:\\+|00)?(55)\\s?)?(?:\\(?([1-9][0-9])\\)?\\s?)?(?:((?:9\\d|[2-9])\\d{3})\\-?(\\d{4}))$';
		return Pattern.matches(pat, telefone);
	}

	public static Boolean isChassiValido(String chassi) {
			Pattern p = Pattern.compile( '([a-zA-Z0-9]{17})' );
			Matcher matcher = p.matcher(chassi);
			if(!matcher.matches()) {
					return false;
			}

			p = Pattern.compile('([' + chassi.substring(0, 1) + ']+)');
			matcher = p.matcher(chassi);
			if(matcher.matches()) {
					return false;
			}
			//nao e mais necessario que os ultimos 6 digitos sejam numeros
			/*String ch = chassi.substring(11, 17);
			p = Pattern.compile( '([0-9]{1,6})' );
			matcher = p.matcher(ch);
			if(!matcher.matches()) {
					return false;
			}*/

			return true;
	}
}