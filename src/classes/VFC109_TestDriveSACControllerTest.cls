/**
* Classe de teste da classe VFC109_TestDriveSACController.
* @author Felipe Jesus Silva.
*/
@isTest(SeeAllData = true)
private class VFC109_TestDriveSACControllerTest 
{
    static Id getRecordTypeId(String developerName)
    {
        RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
        
        return sObjRecordType.Id;
    }
    
    static Account createDealerAccount(String bir)
    {
        /*obtém o record type de concessionária*/
        Id recordTypeId = getRecordTypeId('Network_Site_Acc');
        
        /*cria e insere a conta que representa a concessionária*/
        Account sObjAccDealer = new Account();
        sObjAccDealer.Name = 'Dealer Test';
        sObjAccDealer.Phone='1000';
        sObjAccDealer.IDBIR__c = bir;
        sObjAccDealer.RecordTypeId = recordTypeId; 
        sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com'; 
        sObjAccDealer.ShippingCity = 'Paris';
        sObjAccDealer.ShippingCountry = 'France';
        sObjAccDealer.ShippingState = 'IDF';
        sObjAccDealer.ShippingPostalCode = '75013';
        sObjAccDealer.ShippingStreet = 'my street';
        
        return sObjAccDealer;
    }
    
    static Account createPersonalAccount()
    {
        /*obtém o record type de concessionária*/
        Id recordTypeId = getRecordTypeId('Personal_Acc');
        
        /*cria e insere a conta que representa a concessionária*/
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        sObjAccPersonal.PersLandline__c = '33334444';
        sObjAccPersonal.PersMobPhone__c = '55556666';
        
        return sObjAccPersonal;
    }
    
    static VEH_Veh__c createVehicle()
    {
        VEH_Veh__c sObjVehicle = new VEH_Veh__c();
        sObjVehicle.Name = 'TESTCHASSISANDERO';
        sObjVehicle.Model__c = 'SANDERO';
        sObjVehicle.Version__c = 'Stepway 1.6 8V';
        sObjVehicle.VersionCode__c = 'S1';
        sObjVehicle.Color__c = 'Black';
        sObjVehicle.VehicleRegistrNbr__c = 'ABC-1234';
        
        return sObjVehicle;
    }
    
    static TDV_TestDrive__c createTestDrive()
    {
        TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
        sObjTestDrive.Status__c = 'Scheduled';
        sObjTestDrive.Opportunity__c = null;
        
        return sObjTestDrive;
    }
    
    /**
    * Testa o método initialize.
    */
    testMethod
    static void initializeTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('1109');      
        insert sObjAccDealer;
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller para testar o método de inicialização*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal)); 
        
        Test.startTest();
        
        /*chama o método que será testado*/
        controller.initialize();
        
        /*certifica de que os dados inicializados no VO estão corretos*/
        System.assert(controller.testDriveVO.lstSelOptionVehicleInterest.size() > 0);       
        System.assertEquals(controller.testDriveVO.lstSelOptionVehiclesAvailable.size(), 1);
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5);
        System.assertEquals(controller.testDriveVO.dateBookingNavigationFmt, Date.today().format());
    
        Test.stopTest();        
    }
    
    /**
    * Testa o método processDealerInterestChange.
    */
    testMethod
    static void processDealerInterestChangeTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccDealerAux = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11109');      
        insert sObjAccDealer;
        
        /*obtém o id do proprietário dessa conta*/
        sObjAccDealerAux = [SELECT OwnerId FROM Account WHERE Id =: sObjAccDealer.Id];
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
        controller.testDriveVO.sObjAccount.DealerInterest_BR__c = sObjAccDealer.Id;
        controller.testDriveVO.vehicleInterest = sObjVehicle.Model__c;
        
        Test.startTest();
    
        /*chama o método que será testado*/
        controller.processDealerInterestChange();
        
        /*certifica de que os dados inicializados no VO estão corretos*/
        System.assertEquals(controller.testDriveVO.lstSelOptionVehiclesAvailable.size(), 1);
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5); 
        System.assertEquals(controller.testDriveVO.model, sObjVehicle.Model__c);
        System.assertEquals(controller.testDriveVO.version, sObjVehicle.Version__c);
        System.assertEquals(controller.testDriveVO.color, sObjVehicle.Color__c);
        System.assertEquals(controller.testDriveVO.plaque, sObjVehicle.VehicleRegistrNbr__c);
        System.assertEquals(controller.testDriveVO.dateBookingNavigation, Date.today());
        System.assertEquals(controller.testDriveVO.dealerOwnerId, sObjAccDealerAux.OwnerId);
            
        Test.stopTest();        
    }
    
    /**
    * Testa o método processVehicleInterestChange.
    */
    testMethod
    static void processVehicleInterestChangeTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccDealerAux = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11209');      
        insert sObjAccDealer;
        
        /*obtém o id do proprietário dessa conta*/
        sObjAccDealerAux = [SELECT OwnerId FROM Account WHERE Id =: sObjAccDealer.Id];
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
        controller.testDriveVO.sObjAccount.DealerInterest_BR__c = sObjAccDealer.Id;
        controller.testDriveVO.vehicleInterest = sObjVehicle.Model__c;
        
        Test.startTest();
        
        /*chama o método que será testado*/
        controller.processVehicleInterestChange();
        
        /*certifica de que os dados inicializados no VO estão corretos*/
        System.assertEquals(controller.testDriveVO.lstSelOptionVehiclesAvailable.size(), 1);
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5); 
        System.assertEquals(controller.testDriveVO.model, sObjVehicle.Model__c);
        System.assertEquals(controller.testDriveVO.version, sObjVehicle.Version__c);
        System.assertEquals(controller.testDriveVO.color, sObjVehicle.Color__c);
        System.assertEquals(controller.testDriveVO.plaque, sObjVehicle.VehicleRegistrNbr__c);
        System.assertEquals(controller.testDriveVO.dateBookingNavigation, Date.today());
        System.assertEquals(controller.testDriveVO.dealerOwnerId, sObjAccDealerAux.OwnerId);
            
        Test.stopTest();        
    }
    
    /**
    * Testa o método processVehicleAvailableChange.
    */
    testMethod
    static void processVehicleAvailableChangeTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
                
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11309');      
        insert sObjAccDealer;
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
        controller.testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
        controller.testDriveVO.dateBookingNavigation = Date.today();
                            
        Test.startTest();
        
        /*chama o método que será testado*/
        controller.processVehicleAvailableChange();
        
        /*certifica de que as informações setadas no VO de resposta estão corretas*/
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5); 
            
        Test.stopTest();
    }
    
    /**
    * Testa o método getAgendaNextDay.
    */
    testMethod
    static void getAgendaNextDayTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        Date dateBookingNavigation = null;
                
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11409');      
        insert sObjAccDealer;
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
        controller.testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
        
        /*seta a data do dia atual para que o método procure a agenda do próximo dia*/
        dateBookingNavigation = Date.today();
        controller.testDriveVO.dateBookingNavigation = dateBookingNavigation;
                    
        Test.startTest();
                
        /*chama o método que será testado*/
        controller.getAgendaNextDay();
        
        /*certifica de que as informações setadas no VO de resposta estão corretas*/
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5); 
        System.assertEquals(controller.testDriveVO.dateBookingNavigationFmt, dateBookingNavigation.addDays(1).format());
            
        Test.stopTest();
    }
    
    /**
    * Testa o método getAgendaPreviousDay.
    */
    testMethod
    static void getAgendaPreviousDayTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        Date dateBookingNavigation = null;
                
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11509');      
        insert sObjAccDealer;
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC114_ScheduleTestDriveSACVO>>();
        controller.testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
        
        /*seta a data do dia seguinte para que o método procure a agenda do dia anterior*/
        dateBookingNavigation = Date.today() + 1;
        controller.testDriveVO.dateBookingNavigation = dateBookingNavigation;
                    
        Test.startTest();
        
        /*chama o método que será testado*/
        controller.getAgendaPreviousDay();
        
        /*certifica de que as informações setadas no VO de resposta estão corretas*/
        System.assertEquals(controller.testDriveVO.lstAgendaVehicle.size(), 5); 
        System.assertEquals(controller.testDriveVO.dateBookingNavigationFmt, dateBookingNavigation.addDays(-1).format());
            
        Test.stopTest();
    }
    
    /**
    * Testa o método confirmSchedulingTestDrive.
    */
    testMethod
    static void confirmSchedulingTestDriveTest()
    {
        Account sObjAccDealer = null;
        Account sObjAccDealerAux = null;
        Account sObjAccPersonal = null;
        VEH_Veh__c sObjVehicle = null;
        TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
        VFC109_TestDriveSACController controller = null;
        PageReference pageRef = null;
    
        /*cria e insere a conta que representa a concessionária*/
        sObjAccDealer = createDealerAccount('11609');      
        insert sObjAccDealer;
        
        /*obtém o id do proprietário dessa conta*/
        sObjAccDealerAux = [SELECT OwnerId FROM Account WHERE Id =: sObjAccDealer.Id];
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;
        
        /*cria e insere o veículo*/
        sObjVehicle = createVehicle();
        insert sObjVehicle;
        
        /*cria a insere o objeto TestVehicle*/
        sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
        insert sObjTestDriveVehicle;
            
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();       
        controller.testDriveVO.accountId = sObjAccPersonal.Id;
        controller.testDriveVO.dealerOwnerId = sObjAccDealerAux.OwnerId;
        controller.testDriveVO.sObjAccount.DealerInterest_BR__c = sObjAccDealerAux.Id;
        controller.testDriveVO.vehicleInterest = 'SANDERO';
        controller.testDriveVO.customerName = 'CUSTOMER TEST';
        controller.testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
        controller.testDriveVO.selectedTime = '20:00';
        
        /*define a data de agendamento de 1 dia atrás para provocar erro no test*/
        controller.testDriveVO.dateBookingNavigation = Date.today() - 1;
        
        Test.startTest();
        
        /*chama o método que será testado*/
        pageRef = controller.confirmSchedulingTestDrive();
        
        /*certifica de que o page reference está null, o que indica que houve erro na confirmação do test drive e que será redirecionado para a mesma página*/
        //System.assertEquals(pageRef, null);
        
        /*muda a data de agendamento para que agora haja sucesso no agendamento do test drive*/
        controller.testDriveVO.dateBookingNavigation = Date.today() + 1;
        
        /*chama o método que será testado novamente (dessa vez deve ter sucesso)*/
        pageRef = controller.confirmSchedulingTestDrive();
        
        /*certifica de que o page reference aponta para o id da conta, o que indica que o test drive foi agendado e que será redirecionado para o layout padrão dessa conta*/
        System.assertEquals(pageRef.getURL(), ('/' + sObjAccPersonal.Id));
            
        Test.stopTest();        
    }
    
    /**
    * Testa o método confirmSchedulingTestDrive.
    */
    testMethod
    static void cancelSchedulingTest()
    {
        Account sObjAccPersonal = null;
        VFC109_TestDriveSACController controller = null;
        PageReference pageRef = null;
        
        /*cria a conta pessoal e vincula a concessionária*/
        sObjAccPersonal = createPersonalAccount();
        insert sObjAccPersonal;
        
        /*instancia o controller e seta os dados necessários para testar o método*/
        controller = new VFC109_TestDriveSACController(new ApexPages.StandardController(sObjAccPersonal));
        controller.testDriveVO = new VFC110_TestDriveSACVO();
        controller.testDriveVO.accountId = sObjAccPersonal.Id;
        
        Test.startTest();
        
        /*chama o método que será testado*/
        pageRef = controller.cancelScheduling();
        
        /*certifica de que o page reference aponta para o id da conta, o que indica que será redirecionado para o layout padrão dessa conta*/
        System.assertEquals(pageRef.getURL(), ('/' + sObjAccPersonal.Id));
        
        Test.stopTest();
    }
    
}