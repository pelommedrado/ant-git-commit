/** Mock WebService for MDM
  * Used for test purposes 
  **/
@isTest
global class AsyncTRVwsdlmdm implements WebServiceMock {
    
    public static final Integer CASERESP_EMPTYRESPONSE = 0;
    public static final Integer CASERESP_REPONSEOK     = 1;
    public static final Integer CASERESP_RESPONSEERROR = 2;
    
    public Integer numberOfResponse {get; set;}
    public Integer caseOfResponse {get; set;}
    public Boolean businessAccount {get; set;}
    
    
    
    private Boolean MODE;
    private Boolean LMT;

    /** @constructor **/    
    global AsyncTRVwsdlmdm() {
        MODE = false;
        LMT = false;
    }
    
    /** @constructor **/    
    global AsyncTRVwsdlmdm(Boolean MyRenaultMode) {
        MODE = MyRenaultMode;
    }
    global AsyncTRVwsdlmdm(Integer caseOfResp, Integer numberOfResp, Boolean isbusiness) {
        caseOfResponse = caseOfResp;
        numberOfResponse = numberOfResp;
        businessAccount = isbusiness;
        LMT = true; 
			    
    }
        /** Constructor. default value for Number of response is 1 */
    global AsyncTRVwsdlmdm(Integer caseOfResp, Boolean isbusiness)
    {
        this(caseOfResp, 1, isbusiness);
    }
    /** Constructor. default value for business account is false */
    global AsyncTRVwsdlmdm(Integer caseOfResp, Integer numberOfResp)
    {
        this(caseOfResp, numberOfResp, false);
    }
    /** Constructor. default values for Number of response and business account */
    global AsyncTRVwsdlmdm(Integer caseOfResp)
    {
        this(caseOfResp, false);
    }

    /** Simulates inovkation **/
    global void doInvoke( Object stub, Object request, Map<String, Object> response,
                            String endpoint, String soapAction, String requestName,
                            String responseNS, String responseName, String responseType) {
    
        system.debug('#### AsyncTRVwsdlmdm - doInvoke - BEGIN request=' + request);
        if( requestName == 'SearchPartyRequest' && LMT == false)
        {
            response.put( 'response_x', buildSearchPartyResponse() );
        } 
        else if ( requestName == 'GetPartyRequest') 
        {
            response.put( 'response_x', buildDetailedResponse() );
        }
        else if ( requestName == 'GetCustomerOrderRequest') 
        {
            response.put('response_x', buildCustomerOrderResponse()); 
        }
        else //LMT 
        {
        	system.debug('##Ludo passage lmt mock');
            TRVwsdlMdm.SearchPartyRequest_element req = (TRVwsdlMdm.SearchPartyRequest_element) request;
            TRVwsdlMdm.SearchPartyResponse_element resp = new TRVwsdlMdm.SearchPartyResponse_element();
            resp.ResponseMessage = 'Search Party OK';
            resp.PartyOverview = null; 
            
            if (caseOfResponse == CASERESP_RESPONSEERROR){
                resp.ResponseMessage = 'ERROR';
            }else if (caseOfResponse == CASERESP_REPONSEOK){
                resp.PartyOverview = buildResponse(req);
            }
            response.put('response_x', resp);
            System.debug('#### LMT_MDM_SearchParty_MK - doInvoke - END resp=' + resp);
        }
        
        system.debug('#### AsyncTRVwsdlmdm - doInvoke - END');                          
    }
    
    /** Build a response with several persons **/
    public TRVwsdlMdm.SearchPartyResponse_element buildSearchPartyResponse() {
        TRVwsdlMdm.SearchPartyResponse_element resp = new TRVwsdlMdm.SearchPartyResponse_element();
        resp.ResponseMessage = 'Search Party OK';
        List<TRVwsdlMdm.PartyOverview> listParty = new List<TRVwsdlMdm.PartyOverview>();
        //In MyRenault mode, returns only one response
        //response 1
        TRVwsdlMdm.PartyOverview party1 = new TRVwsdlMdm.PartyOverview();
        party1.PartyId = '345678';
        party1.PartyType = 'O';
        party1.MatchScore = 99;
        party1.FirstName = 'JONATHAN';
        party1.LastName = 'LAMBERT';
        party1.DateOfBirth = Date.newInstance(1980,1,1);
        party1.Sex = '1';
        party1.OccupationalCategoryCode = 'EMPLOYE';
        party1.Language = 'FR';
        party1.AddressLine1 = '71 RUE DES AMANDIERS';
        party1.AddressLine2 = '';
        party1.City = 'PARIS';
        party1.Region = '';
        party1.Postcode = '75000';
        party1.Country = 'FRANCE';
        party1.FixedPhoneNumber = new List<String>();
        party1.FixedPhoneNumber.add('+33 3 29 66 53 42');
        party1.MobilePhoneNumber = new List<String>();
        party1.MobilePhoneNumber.add('+33 6 85 29 09 96');
        party1.EmailAddress = new List<String>();
        party1.EmailAddress.add('mag.perrin21@free.fr');
        listParty.add(party1);
        if( !MODE ) {
            //response 2
            TRVwsdlMdm.PartyOverview party2 = new TRVwsdlMdm.PartyOverview();
            party2.PartyId = '1082019';
            party2.PartyType = '';
            party2.MatchScore = 90;
            party2.FirstName = 'JONATHAN';
            party2.LastName = 'LAMBERT';
            party2.DateOfBirth = Date.newInstance(1980,1,1);
            party2.Sex = '1';
            party2.Language = 'FR';
            party2.AddressLine1 = '4 IMPASSE SAMUEL DE CHAMPLAIN';
            party2.AddressLine2 = '';
            party2.City = 'PARIS';
            party2.Region = '';
            party2.Postcode = '75000';
            party2.Country = 'FRANCE';
            party2.FixedPhoneNumber = new List<String>();
            party2.FixedPhoneNumber.add('+33 3 29 66 53 42');
            party2.MobilePhoneNumber = new List<String>();
            party2.MobilePhoneNumber.add('+33 6 85 29 09 96');
            party2.EmailAddress = new List<String>();
            party2.EmailAddress.add('mag.perrin21@free.fr');
            listParty.add(party2);
        }
        resp.PartyOverview = listParty;
        return resp;
    }
    
    /** Build the detailed response for one person **/
    public  TRVwsdlMdm.GetPartyResponse_element buildDetailedResponse() {
        TRVwsdlMdm.Party party = new TRVwsdlMdm.Party();
        party.PartyType = 'I';
        party.PartySubType = 'AC';
        party.PartyStatus = 'A';
        party.FirstName1 = 'JONATHAN';
        party.FirstName2 = 'JONATHAN2';
        party.LastName1 = 'LAMBERT';
        party.LastName2 = 'LAMBERT2';
        party.FullName = 'JONATHAN LAMBERT';
        party.DateOfBirth = Date.newInstance(1979, 1, 1);
        party.Civility = 'M';
        party.Sex = 'M';
        party.Title = 'M';
        party.FinancialStatus = 'ACT';
        party.MaritalStatus ='Single';
        party.Language = 'FRE';
        party.OccupationalCategoryCode = 'EMPLOYE';
        party.OccupationalCategoryDescription = 'B1';
        party.PartySegment = 'Others';
        party.PartySubType = 'AC'; 
        party.OrganisationName = '?'; 
        party.RenaultGroupStaff ='?';
        party.NumberOfChildrenAtHome = 10; 
        party.deceased = '?';
        party.PreferredMedia ='SMS';
        party.commercialName = '?';
        party.NumberOfEmployees = 10;
        party.CompanyActivityDescription = '?'; 
        party.legalNature = '?';
        
        party.PartyIdentification = new List<TRVwsdlMdm.PartyIdentification>();
        TRVwsdlMdm.PartyIdentification pi = new TRVwsdlMdm.PartyIdentification();
        pi.IdentificationType = '21534';
        party.PartyIdentification.add(pi);
        
        party.CommunicationAgreement = new List<TRVwsdlMdm.CommunicationAgreement>();
        TRVwsdlmdm.CommunicationAgreement comm  = new TRVwsdlMdm.CommunicationAgreement();
        comm.CommunicationAgreementType = '?';
        comm.Brand = '?';
        comm.CommunicationAgreement= '?';
        comm.CommunicationAgreementOrigin= '?';
        comm.UpdatedDate= system.today();
        party.CommunicationAgreement.add(comm);
        
        party.StopCommunication = new List<TRVwsdlMdm.StopCommunication>();
        TRVwsdlmdm.StopCommunication stop  = new TRVwsdlMdm.StopCommunication();
        stop.StopCommunicationType = '?';
        stop.StopCommunication = '?';
        stop.UpdatedDate= system.today();
        party.StopCommunication.add(stop);
        
        party.PreferredDealer = new List<TRVwsdlMdm.PreferredDealer>();
        TRVwsdlmdm.PreferredDealer pdealer  = new TRVwsdlMdm.PreferredDealer();
        pdealer.DealerCode = '?';
        party.PreferredDealer.add(pdealer);
        
        party.address = new List<TRVwsdlMdm.Address>();
        TRVwsdlMdm.Address pa = new TRVwsdlMdm.Address();
        pa.city = '?';
        pa.postcode = '?';
        pa.AddressLine1 = '?';
        pa.AddressLine2 = '?';
        pa.AddressLine3 = '?'; 
        pa.AddressLine4 = '?';
        pa.region = '?';
        party.address.add(pa);
        
        party.ElectronicAddress = new List<TRVwsdlMdm.ElectronicAddress>();
        TRVwsdlMdm.ElectronicAddress email = new TRVwsdlMdm.ElectronicAddress();
        email.ElectronicAddressValue = 'mag.perrin21@free.fr';
        email.ElectronicAddressType = 'EMAIL';
        party.ElectronicAddress.add(email);
        
        party.PhoneNumber = new List<TRVwsdlMdm.PhoneNumber>();
        TRVwsdlMdm.PhoneNumber land = new TRVwsdlMdm.PhoneNumber();
        land.PhoneNumberType = 'LAND';
        land.PhoneNumberValue = '+33 3 29 66 53 42';
        land.Valid = 'Y';
        //land.LastUpdateDate = Date.newInstance(2014, 7, 29);
        party.PhoneNumber.add(land);
        TRVwsdlMdm.PhoneNumber mobile = new TRVwsdlMdm.PhoneNumber();
        mobile.PhoneNumberType = 'MOBILE';
        mobile.PhoneNumberValue = '+33 6 85 29 09 96';
        mobile.Valid = 'Y';
//      mobile.LastUpdateDate = Date.newInstance(2014, 7, 29);      
        party.PhoneNumber.add(mobile);
     
        TRVwsdlMdm.Vehicle veh = new TRVwsdlMdm.Vehicle();      
        veh.VehicleType = 'VP';
        veh.VehicleIdentificationNumber = 'VF1BMSE0637005985';
        veh.Brand = 'DACIA';
        veh.BrandLabel = 'DACIA';
        veh.Model = 'TW2';
        veh.ModelLabel = 'TWINGO II';
        veh.CartecId = '63468';
        veh.RegistrationDate = Date.newInstance(2006, 12, 12);
        veh.CurrentMileage = 0;
        veh.CurrentMileageDate = Date.newInstance(1900, 1, 1);
        veh.PreviousRegistrationNumber = '7938VH88';
        veh.PartyVehicleType = 'OWN';
        veh.DeliveryDate = Date.newInstance(2006, 12, 12);
        veh.OrderDate = Date.newInstance(2007, 1, 2);
        veh.PurchaseDate = Date.newInstance(2007, 1, 2);
        veh.RegistrationNumber = '7938VH88';
        veh.PurchaseMileage = 0;
        veh.NewVehicle = 'Y';
        veh.StartDate = Date.newInstance(2014, 7, 29);
        party.Vehicle = new List<TRVwsdlMdm.Vehicle>();
        party.Vehicle.add(veh);
        
        TRVwsdlMdm.Contract con = new TRVwsdlMdm.Contract();  
        con.Number_x = '9911883073';
        con.Status = 'ACTIVE';
        con.VIN ='VF15RFL0H49290783';
        con.Category = 'SERVICES';
        con.SubCategory = 'MAINTENANCE';
        con.Offer ='PACK ZEN RENAULT';
        con.BeginningDate ='2015-01-06';
        con.Duration = '60';    
        party.Contract = new List<TRVwsdlMdm.Contract>();
        party.Contract.add(con);
        
        TRVwsdlMdm.GetPartyResponse_element mdmResponse = new TRVwsdlMdm.GetPartyResponse_element();
        mdmResponse.ResponseMessage = ':: Get Party OK ::';
        mdmResponse.Party = party;
        return mdmResponse;
    }
    public TRVwsdlMdm.GetCustomerOrderResponse_element buildCustomerOrderResponse()
    {
        
        TRVwsdlMdm.CustomerOrderOverview party = new TRVwsdlMdm.CustomerOrderOverview();
        list <TRVwsdlMdm.CustomerOrderOverview> partylst = new list <TRVwsdlMdm.CustomerOrderOverview>();
        
        party.PartyType = '?';
        party.PartySubType = '?';
        party.PartyStatus= '?';
        party.FirstName1= 'Francois';
        party.FirstName2= '?';
        party.LastName1= 'Toto';
        party.LastName2= '?';
        party.FullName= '?';
        party.DateOfBirth = system.today(); 
        party.YearOfBirth= '?';
        party.Civility= '?';
        party.Title= '?';
        party.Sex= '?';
        party.MaritalStatus= '?';
        party.Language= '?';
        party.RenaultGroupStaff= '?';
        party.NumberOfChildrenAtHome= 10;
        party.Deceased= '?';
        party.EmailRental= '?';
        party.PreferredMedia= '?';
        party.OccupationalCategoryCode= '?';
        party.PartySegment= '?';
        party.OrganisationName= '?';
        party.CommercialName= '?';
        party.ProtocolName= '?';
        party.NumberOfVehicles= 10;
        party.NumberOfEmployees= 10;
        party.FinancialRisk= '?';
        party.FinancialStatus= '?';
        party.CompanyActivityCode= '?';
    
        party.Order = new List<TRVwsdlMdm.Order>();
        TRVwsdlMdm.Order po = new TRVwsdlMdm.Order();
        po.OrderID = '?';
        po.OrderNumber = '?';
        po.OrderType = '?';
        po.OrderStatus = '?';
        po.SellingDealer = '?'; 
        po.DeliveryDealer = '?';
        po.VIN = '?';
        po.Brand = '?';
        po.Model = '?';
        po.VersionCode = '?';
        po.OrderDate = system.today();
        po.OrderCancellationDate = system.today();
        po.DesiredDeliveryDate = system.today();
        po.ManufacturingDate = system.today();
        po.ShippingDate = system.today();
        po.DealerDeliveryDate = system.today();
        po.DeliveryDate = system.today();
        po.DeliveryCancellationDate = system.today();
        po.CreatedDate = system.today();
        po.UpdatedDate = system.today();
        po.TechnicalUpdatedDate = system.today();
        party.Order.add(po); 
        
        partylst.add(party); 
        TRVwsdlMdm.GetCustomerOrderResponse_element mdmResponse = new TRVwsdlMdm.GetCustomerOrderResponse_element();
        mdmResponse.ResponseMessage = ':: Get Customer Order OK ::';
        mdmResponse.CustomerOrderOverview = partylst;

        return mdmResponse;
    }
    /**
    *builds the web service response. if a criteria is set, response returns its value otherwise returns a hardcoded value.
    */
    private TRVwsdlMdm.PartyOverview[] buildResponse(TRVwsdlMdm.SearchPartyRequest_element req){
    
            TRVwsdlMdm.PartyOverview[] pos = new List<TRVwsdlMdm.PartyOverview>();
            for (Integer i = 0; i < numberOfResponse; i++){
                TRVwsdlMdm.PartyOverview po = new TRVwsdlMdm.PartyOverview();
                po.PartyId = 'ID' + i;
                
                po.MatchScore = 1;
                if (!String.isEmpty(req.SearchCriteria.LastName)){
                    po.LastName = req.SearchCriteria.LastName;
                }else{
                    po.LastName = 'TOTO';
                }
                if (!businessAccount){
                    po.PartyType = 'I';
                    po.FirstName = req.SearchCriteria.FirstName;
                    po.DateOfBirth = Date.newInstance (19, 4, 1969);
                    po.Sex = '';
                    po.Language = '';
                }else{
                    po.PartyType = 'O';
                    po.Siret = new List<String>();
                    if (!String.isEmpty(req.SearchCriteria.IdentValue1)){
                        po.Siret.add(req.SearchCriteria.IdentValue1);
                    }else{
                        po.Siret.add('18473478452345');
                    }
                }
                po.AddressLine1 = '1 rue des fleurs';
                po.AddressLine2 = 'Bat B';
                po.Region = '';
                po.Country = 'FRA';
                po.FixedPhoneNumber = new List<String>(); //{'+33 2 99 88 77 66', '+33 2 99 88 77 99'};
                po.MobilePhoneNumber = new List<String>(); //{'+33 6 99 88 77 66'};
                po.EmailAddress = new List<String>();
                
                //-- add real values if set
                if (!String.isEmpty(req.SearchCriteria.ElectronicAddress)){
                    po.EmailAddress.add(req.SearchCriteria.ElectronicAddress);
                }else{
                    po.EmailAddress.add('a.talon@testlmtsfa.com');
                }
                if (!String.isEmpty(req.SearchCriteria.PhoneNumber)){
                    if (req.SearchCriteria.PhoneNumber.replaceAll(' ', '').startsWith('+336')){
                        po.MobilePhoneNumber.add(req.SearchCriteria.PhoneNumber);
                        po.FixedPhoneNumber.add('+33 2 99 88 77 66');
                        po.FixedPhoneNumber.add('+33 2 99 88 77 99');
                    }else{
                        po.FixedPhoneNumber.add(req.SearchCriteria.PhoneNumber);
                        po.MobilePhoneNumber.add('+33 6 99 88 77 66');
                    }
                }else{
                    po.FixedPhoneNumber.add('+33 2 99 88 77 66');
                    po.FixedPhoneNumber.add('+33 2 99 88 77 99');
                    po.MobilePhoneNumber.add('+33 6 99 88 77 66');
                }
                if (!String.isEmpty(req.SearchCriteria.City)){
                    po.City = req.SearchCriteria.City;
                }else{
                    po.City = 'RENNES'; 
                }
                if (!String.isEmpty(req.SearchCriteria.PostCode)){
                    po.PostCode = req.SearchCriteria.PostCode;
                }else{
                    po.PostCode = '35700'; 
                }
                
                pos.add(po);
            }
            
            // to check that party type is correctly filtered add another account
            TRVwsdlMdm.PartyOverview po = new TRVwsdlMdm.PartyOverview();
            po.PartyId = 'ID999';
            po.LastName = 'TOTORO';
            if (!businessAccount){
                // insert a party type not compliant with query to check it is discarded
                po.PartyType = 'O';
            }else{
                po.PartyType = 'I';
            }
            po.Country = 'FRA';
            pos.add(po);
            
            return pos;
    }
}