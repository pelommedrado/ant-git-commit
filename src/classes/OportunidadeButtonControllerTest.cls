@IsTest
private class OportunidadeButtonControllerTest {
  static User userSeller = null;
  private static Account accDealer;
  static {
			User usr = [Select id from User where Id = :UserInfo.getUserId()];
			UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
			usr.UserRoleId = r.Id;
			Update usr;

			System.RunAs(usr) {
				accDealer = MyOwnCreation.getInstance().criaAccountDealer();
        accDealer.ReturnPeriod__c = 10;
				INSERT accDealer;

				Contact ctt = new Contact(
					CPF__c 		=  '44476157114',
					Email 		= 'manager1@org1.com',
					FirstName 	= 'User',
					LastName 	= 'Manager1',
					MobilePhone = '11223344556',
					AccountId 	= accDealer.Id
				);

				INSERT ctt;

				userSeller = new User(
					ContactId 			= ctt.Id,
					ProfileId 			= Utils.getProfileId('BR - Renault + Cliente'),
					FirstName 			= 'User',
					LastName 			= 'Manager1',
					Username 			= 'manager1@org1.com.teste',
					Email 				= 'manager1@org1.com',
					Alias 				= 'ma1',
					RecordDefaultCountry__c = 'Brazil',
					BIR__c 				= '123456',
					EmailEncodingKey	='UTF-8',
					LanguageLocaleKey	='en_US',
					LocaleSidKey		='en_US',
					TimeZoneSidKey		='America/Los_Angeles'
				);

				INSERT userSeller;
			}
	}

  static testMethod void testOportunidadeExpirou() {
    Test.startTest();
    MyOwnCreation moc = new MyOwnCreation();
    System.runAs(userSeller) {
      Opportunity opp = moc.criaOpportunity();
      INSERT opp;

      ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
      OportunidadeButtonController ct = new OportunidadeButtonController(st);
      ct.oportunidadeExpirou();

      List<Apexpages.Message> msgs = ApexPages.getMessages();
      Boolean result = false;
      for(Apexpages.Message msg : msgs) {
        final String resultString = 'Oportunidade fora do período de retorno!';
        if(msg.getDetail().contains(resultString)) {
          result = true;
        }
      }
      System.assert(result);
    }
    Test.stopTest();
  }

  static testMethod void testOportunidadeAceita(){
    Test.startTest();

    MyOwnCreation moc = new MyOwnCreation();
    System.runAs(userSeller) {
      Opportunity opp = moc.criaOpportunity();
      INSERT opp;

      ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
      OportunidadeButtonController ct = new OportunidadeButtonController(st);
      ct.actionButtonAtendimento();
      ct.oportunidadeAceita();
      TST_Transaction__c trans = [SELECT Id, Type__c
        FROM TST_Transaction__c WHERE Opportunity__c =: opp.Id AND Type__c = 'Treatment' LIMIT 1];
      System.assertEquals('Treatment', trans.Type__c);
    }
    Test.stopTest();
  }

    static testMethod void testOportunidadeReabrir() {
      Test.startTest();
      MyOwnCreation moc = new MyOwnCreation();
      System.runAs(userSeller) {
        Opportunity opp = moc.criaOpportunity();
        INSERT opp;

        ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
        OportunidadeButtonController ct = new OportunidadeButtonController(st);

        ct.oportunidadeReabrir();

        Opportunity oppIdent = [SELECT Id, StageName FROM Opportunity LIMIT 1];
        System.assertEquals('Identified', oppIdent.StageName);
      }
      Test.stopTest();
    }

    static testMethod void testOportunidadeNaoReabre(){

      Test.startTest();

      MyOwnCreation moc = new MyOwnCreation();
      System.runAs(userSeller) {
          Opportunity opp = moc.criaOpportunity();
          Insert opp;

          ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
          OportunidadeButtonController ct = new OportunidadeButtonController(st);

          ct.oportunidadeNaoReabre();

          List<Apexpages.Message> msgs = ApexPages.getMessages();
          boolean b = false;
          for(Apexpages.Message msg:msgs){
              if (msg.getDetail().contains('Esta Oportunidade foi marcada como Perdida e não pode ser reaberta.')) b = true;
          }
          system.assert(b);
      }

      Test.stopTest();
    }

    static testMethod void testOportunidadeNaoReabreFaturada(){
        Test.startTest();
        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            Insert opp;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            ct.oportunidadeNaoReabreFaturada();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('A oportunidade já está faturada e não pode ser reaberta.')) b = true;
            }
            system.assert(b);
        }

        Test.stopTest();
    }

    static testMethod void TestActionButtonAtendimento(){
        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            opp.StageName = 'Quote';
            Insert opp;

            Quote q = moc.criaQuote();
            q.OpportunityId = opp.Id;
            Insert q;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            PageReference redirect = ct.actionButtonAtendimento();

            String oppId = opp.Id;

            System.assertEquals('/apex/ClienteOportunidadeDetalheSfa2?Id=' + oppId.substring(0,15) , redirect.getUrl() );
        }

        Test.stopTest();
    }

    static testMethod void TestActionButtonAtendimentoOppLost(){
        Test.startTest();

        accDealer.ReturnPeriod__c = 10;
        accDealer.ReturnActiveToSFA__c = true;
        UPDATE accDealer;

        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            opp.ReasonLossCancellation__c = 'Attendance';
            opp.StageName = 'Lost';
            Insert opp;

            Quote q = moc.criaQuote();
            q.OpportunityId = opp.Id;
            Insert q;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            PageReference redirect = ct.actionButtonAtendimento();

            String oppId = opp.Id;

            System.assertEquals('/apex/ClienteOportunidadeSfa2?Id=' + oppId.substring(0,15) , redirect.getUrl() );
        }

        Test.stopTest();
    }
    static testMethod void testActionDetalheOportunidade(){
        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            Insert opp;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            PageReference redirect = ct.actionDetalheOportunidade();

            String oppId = opp.Id;

            System.assertEquals('/cac/' + oppId.substring(0,15) , redirect.getUrl() );
        }

        Test.stopTest();
    }

    static testMethod void testActionNovaOportunidade(){
        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Account acc = [SELECT Id FROM Account WHERE IDBIR__c = '123456' LIMIT 1];

            Opportunity opp = moc.criaOpportunity();
            opp.AccountId = acc.Id;
            Insert opp;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            PageReference redirect = ct.actionNovaOportunidade();

            System.assertEquals('/PesquisaSfa2?conta=' + acc.Id , redirect.getUrl() );
        }

        Test.stopTest();
    }

    // Testa botao retorno quando "NAO" está ativo para a concessionaria
    static testMethod void testActionButtonReturn1(){
        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            Insert opp;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            ct.actionButtonReturn();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('Retorno é o conceito de reaproveitamento de oportunidades abertas '+
                                             'dentro de um período pré determinado pela sua gestão de vendas, mas não está ativado '+
                                             'para a sua concessionária. Caso a equipe de vendas tenha interesse em utilizar '+
                                             'essa funcionalidade, favor entrar em contato com a TI local.')) b = true;
            }
            //system.assert(b);
        }

        Test.stopTest();
    }

    // Testa o botao retorno quando está ativo para a concessionaria
    static testMethod void testActionButtonReturn2(){
        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = [SELECT Id, ReturnActiveToSFA__c FROM Account WHERE IDBIR__c = '123456' LIMIT 1];
        acc.ReturnActiveToSFA__c = true;
        Update acc;

        System.runAs(userSeller) {

            Opportunity opp = moc.criaOpportunity();
            INSERT opp;
            opp.StageName = 'Quote';
            UPDATE opp;

            Quote q = moc.criaQuote();
            q.OpportunityId = opp.Id;
            Insert q;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            PageReference redirect = ct.actionButtonReturn();

            String oppId = opp.Id;

            System.assertEquals('/apex/ClienteOportunidadeDetalheSfa2?Id=' + oppId.substring(0,15) , redirect.getUrl() );

        }

        Test.stopTest();
    }

    // Testa o botao retorno quando está ativo para a concessionaria e usuario é recepcionista
    static testMethod void testActionButtonReturn3(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        User hostess = createHostess();

        System.runAs(hostess) {

            Opportunity opp = moc.criaOpportunity();
            opp.StageName = 'Quote';
            Insert opp;

            Quote q = moc.criaQuote();
            q.OpportunityId = opp.Id;
            Insert q;

            ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp);
            OportunidadeButtonController ct = new OportunidadeButtonController(st);

            ct.actionButtonReturn();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('Esta funcionalidade está disponível somente para vendedores. '+
                                             'Clique em "Atender / Encaminhar Oportunidade"')) b = true;
            }
            system.assert(b);

        }

        Test.stopTest();
    }


    static User createHostess(){

        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User user;
        Account account;

        MyOwnCreation moc = new MyOwnCreation();

        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {

            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;

            accDealer.ReturnActiveToSFA__c = true;
            UPDATE accDealer;

            Contact contact = new Contact(
              CPF__c 		=  '44476157116',
              Email 		= 'seller@org1.com',
              FirstName 	= 'User',
              LastName 	= 'Seller',
              MobilePhone = '11223344556',
              AccountId 	= accDealer.Id
            );
            insert(contact);

            user = moc.criaUser();
            user.ProfileId = Utils.getProfileId('SFA - Receptionist');
            user.ContactId = contact.Id;
            insert(user);
        }
        return user;
    }
}