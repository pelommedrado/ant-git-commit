/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class SetMessageStatusRequest {
    @InvocableVariable(label='Error Message' description='Optional error message' required=false)
    global String ErrorMessage;
    @InvocableVariable(label='Message Id' description='Identifier for previously-queued message.' required=true)
    global Id MessageId;
    @InvocableVariable(label='Message Status' description='Status of message - should be one of Queued, Sent, or Error' required=true)
    global String MessageStatus;
    global SetMessageStatusRequest() {

    }
}
