/*****************************************************************************************************
  Purge of german myRenault accounts
******************************************************************************************************
31 Mar 2016 : Creation 
******************************************************************************************************/
global class Myr_Batch_Germany2_User_BAT extends INDUS_Batch_AbstractBatch_CLS { 
    
    List<Id> L_I = new List<Id>();
    global String Global_Query=null; 

    global Myr_Batch_Germany2_User_BAT() {
        String RetentionDay = Country_Info__c.getInstance('Germany2').Myr_Deleting_Retention__c;
        String restriction='';
        //Account_Ids
        String query = 'Select Id from Account where country__c=\'Germany2\' and ((';
        query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
        query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MyD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay;
        query += ') OR (';
        query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
        query += 'Myd_Status__c=null';
        query += ') OR (';
        query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
        query += 'Myr_Status__c=null';
        query += '))';
        query += Myr_Batch_Germany2_ToolBox.Add_Governor_Limit();
        System.debug('Myr_Batch_Germany2_ToolBox.Get_Account_Ids : query=' + query);
        List<Account> L_A = Database.query(query);

        if (L_A.size()==0){
            Global_Query='select Id from user where Profile.Name like \'HeliosCommunityDontExist\'';
            return;
        }

        for (Account a : L_A) {
            if (!restriction.equalsIgnoreCase('')){
                restriction+='\', \'';
            } 
            restriction+=a.Id; 
        }
        Global_Query='select Id, AccountId from user where AccountId in (\'' + restriction + '\')  and Profile.Name like \'HeliosCommunity\'';
    }
    
    global Database.QueryLocator start(Database.BatchableContext info){
        System.debug('Global_Query : ' + Global_Query);
        return Database.getQueryLocator(Global_Query);
    }

    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> records){ 
        System.debug('Myr_Batch_Germany2_User_BAT.doExecute : info=' + info + ', records=' + records);
        List<User> dbUserToUpdateList = new List<User>();
        for( sObject record : records )
        {
            User dbUserToUpdate = (User) record;
            dbUserToUpdate.username=Myr_Users_Utilities_Class.generateCancelUsername(dbUserToUpdate.AccountId);//Not really usefull, normally the username is already cancelled
            dbUserToUpdate.isActive=false;
            dbUserToUpdate.IsPortalEnabled=false;
            dbUserToUpdate.email=Myr_Users_Utilities_Class.generateCancelUsername(dbUserToUpdate.AccountId);
            dbUserToUpdate.FirstName='ANONYM';
            dbUserToUpdate.LastName='ANONYM';
            dbUserToUpdateList.add(dbUserToUpdate);
        }
        return new DatabaseAction( dbUserToUpdateList, Action.ACTION_UPDATE, false );
    }
    global override void doFinish(Database.BatchableContext info){ 
        System.debug( '#### Myr_Batch_Germany2_User_BAT - Finish ');
    }
    global override String getClassName(){
        return Myr_Batch_Germany2_User_BAT.class.GetName();
    }
}