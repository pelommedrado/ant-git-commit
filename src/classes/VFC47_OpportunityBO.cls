/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto Opportunity.
* @author Felipe Jesus Silva.
*/
public class VFC47_OpportunityBO 
{
	private static final VFC47_OpportunityBO instance = new VFC47_OpportunityBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC47_OpportunityBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC47_OpportunityBO getInstance()
	{
		return instance;
	}
	
	public Opportunity findById(String opportunityId)
	{
		Opportunity sObjOpportunity = null;
		
		try
		{
			sObjOpportunity = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(opportunityId); 
		}
		catch(QueryException ex)
		{			
		}
		
		return sObjOpportunity;
	}
	
	public List<Opportunity> findById(Set<String> setId)
	{
		List<Opportunity> lstSObjOpportunity = VFC23_OpportunityDAO.getInstance().findById(setId);
		
		if(lstSObjOpportunity.isEmpty())
		{
			throw new VFC89_NoDataFoundException('Nenhuma oportunidade foi encontrada para os ids especificados');
		}
		
		return lstSObjOpportunity;
	}
	
	public void createOpportunity(Opportunity sObjOpportunity)
	{
		try
		{
			VFC23_OpportunityDAO.getInstance().insertData(sObjOpportunity);
		}
		catch(DMLException ex)
		{
			throw new VFC115_CreateOpportunityException(ex, ex.getDMLMessage(0));
		}
	}
	
	public void updateOpportunityAndAccount(Opportunity sObjOpportunity, Account sObjAccount)
	{
		try
		{
			VFC23_OpportunityDAO.getInstance().updateData(sObjOpportunity, sObjAccount);
		}
		catch(DMLException ex)
		{
			throw new VFC91_UpdateOpportunityException(ex, ex.getDMLMessage(0));
		}
	}
	
	public void updateOpportunity(Opportunity sObjOpportunity)
	{
		try
		{
			VFC23_OpportunityDAO.getInstance().updateData(sObjOpportunity);
		}
		catch(DMLException ex)
		{
			throw new VFC91_UpdateOpportunityException(ex, ex.getDMLMessage(0));
		}
	}
	
	public void updateOpportunity(Opportunity sObjOpportunity, TDV_TestDrive__c sObjTestDrive)
	{
		try
		{
			VFC23_OpportunityDAO.getInstance().updateData(sObjOpportunity, sObjTestDrive);
		}
		catch(DMLException ex)
		{
			throw new VFC91_UpdateOpportunityException(ex, ex.getDMLMessage(0));
		}
	}
	
	public void updateOpportunities(List<Opportunity> lstSObjOpportunity)
	{
		try
		{
            System.debug('lstSObjOpportunity:' + lstSObjOpportunity);
			VFC23_OpportunityDAO.getInstance().updateData(lstSObjOpportunity);
		}
		catch(DMLException ex)
		{
			throw new VFC91_UpdateOpportunityException(ex, ex.getDMLMessage(0));
		}
	}
}