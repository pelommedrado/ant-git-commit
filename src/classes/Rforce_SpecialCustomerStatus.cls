/*****************************************************************************************
    Name    : Account_BeforeUpdate_Trigger
    Desc    : Update 
    Author  : Sumanth(RNTBCI)
    Project : Rforce
    Created Date: 02-04-2015
******************************************************************************************/
public class Rforce_SpecialCustomerStatus{
    public static void updatespecialcustomerstatus(List <Account> listAccount){
        //Added by sumanth on 20/03/2015 for Intaile Paris
        Set<Id> acids=new Set<Id>();
        List<Task> tasklist=new List<Task>();
        for(Account a :listAccount){
            if(a.CustmrStatus__c==system.label.Acc_Customer){
                acids.add(a.id);
            }
        }
        system.debug('Customer account-->'+acids);
        try{
            
            List<Account> specialCustomer=[Select Id,CustmrStatus__c,SpecialCustmr__c,(Select Id,Status__c,VIN__r.Initiale_Paris__c from Vehicle_Relations__r where Status__c=:system.label.VRE_Active) from Account where Id IN :acids];
            for(Account a : specialCustomer){
                if(a.SpecialCustmr__c!=system.label.Acc_SpecialCustomer_InitialeParis){
                     for(VRE_VehRel__c vre : a.Vehicle_Relations__r){
                         if(vre.VIN__r.Initiale_Paris__c==True){
                             a.SpecialCustmr__c=system.label.Acc_SpecialCustomer_InitialeParis;
                             // Added to Create the Task 
                             tasklist.add(new Task(IsReminderSet = true,WhatId=a.id, Type=system.label.TSK_Type_OutBound,Subject=system.label.TSK_Type_OutBound,ActivityDate=system.today(),Priority=system.label.TSK_Priority_Normal,Status=system.label.TSK_Status_ToDo,ReminderDateTime=system.now().addHours(2),OwnerId=UserInfo.getuserid()));                                                                           
                             system.debug('## tasklist value is..::'+ tasklist);
                         }
                     }
                }    
            }
            update specialCustomer;
            insert tasklist;
        }catch(Exception e){
            system.debug('Exception occured::'+e);
        }   
    }
}