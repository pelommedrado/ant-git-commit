/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class DeleteSmsScheduleItem {
    @InvocableVariable(label='Object Id' description='Identifier for SMS_Schedule__c object record to be deleted.' required=true)
    global Id ObjectId;
    @InvocableVariable(label='Reference ID' description='Identifier for SMS_Schedule__c object record to be deleted.' required=true)
    global Id ReferenceId;
    global DeleteSmsScheduleItem() {

    }
}
