@IsTest
public class ContactBuild {
    
    public static ContactBuild instance = null;
    
    public static ContactBuild getInstance() {
        if(instance == null) {
            instance = new ContactBuild();
        }
        return instance;
    }
    
    private ContactBuild() {
    }
    
    public Contact createContact(Id accId) {
        Contact ctt = new Contact();
        ctt.CPF__c 		= '44476157114';
        ctt.Email 		= 'contact@test.com';
        ctt.FirstName  	= 'User';
        ctt.LastName  	= 'Test';
        ctt.MobilePhone = '1123344556';
        ctt.AccountId  	= accId;
        return ctt;
    }
}