public class WebServiceIPCallout {

    @future (callout=true)
    public static void sendNotification() {
 
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
 
        req.setEndpoint('http://api.hostip.info/get_html.php');

        req.setMethod('GET');
        
        try {
            res = http.send(req);
            System.debug(res.toString());
            System.debug('#### : ' + res.getBody());

            System.debug('STATUS:'+res.getStatus());
            System.debug('STATUS_CODE:'+res.getStatusCode());
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
 
    }

}