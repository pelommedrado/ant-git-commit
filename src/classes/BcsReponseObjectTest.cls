@isTest
public with sharing class BcsReponseObjectTest{
    
    static testMethod void testMet() {
        BcsReponseObject tobj = new BcsReponseObject();
    	tobj.email					= 'email';
        tobj.phoneNum3 				= 'phoneNum3';
        tobj.possessionBegin		= 'possessionBegin';
        tobj.registration			= 'registration';
        tobj.firstRegistrationDate 	= 'firstRegistrationDate';
        tobj.versionLabel			= 'versionLabel';
        tobj.modelLabel				= 'modelLabel';
        
        tobj.modelCode				= 'modelCode';
        tobj.brandCode				= 'brandCode';
        tobj.vin					= 'vin';
        tobj.City					= 'City';
        tobj.Zip					= 'Zip';
        
        tobj.CountryCode			= 'CountryCode';
        tobj.Compl2					= 'Compl2';
        tobj.StrName				= 'StrName';
        tobj.phoneNum3				= 'phoneNum3';
        tobj.phoneNum2				= 'phoneNum2';
        tobj.phoneNum1				= 'phoneNum1';
        
        tobj.EmailCommAgreement		= 'EmailCommAgreement';
        tobj.FaxCommAgreement		= 'FaxCommAgreement';
        tobj.SMSCommAgreement		= 'SMSCommAgreement';
        tobj.TelCommAgreemen		= 'TelCommAgreemen';
        tobj.PostCommAgreement		= 'PostCommAgreement';
        tobj.GlobalCommAgreement	= 'GlobalCommAgreement';
        
        tobj.typeperson				= 'typeperson';
        tobj.FaxCommAgreement		= 'FaxCommAgreement';
        tobj.SMSCommAgreement		= 'SMSCommAgreement';
        tobj.TelCommAgreemen		= 'TelCommAgreemen';
        tobj.PostCommAgreement		= 'PostCommAgreement';
        tobj.GlobalCommAgreement	= 'GlobalCommAgreement';
        
        tobj.IdClient				= 'IdClient';
        tobj.LastName				= 'LastName';
        tobj.Title					= 'Title';
        tobj.Lang					= 'Lang';
        tobj.FirstName				= 'FirstName';
    }

}