@isTest
public with sharing class Rforce_SpecialCustomerStatus_Test{
        private static testMethod void testCustStat() {
            Test.startTest();
            Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Account Acc = new Account(CustmrStatus__c='Customer',FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert Acc;
            VEH_Veh__c veh = new VEH_Veh__c(Name='VF1KMRF0533061370',VehicleBrand__c='Renault',Initiale_Paris__c=True);
            insert veh;
            VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=Acc.Id,VIN__c=veh.id,Status__c='Active',TypeRelation__c='Owner');
            insert vre; 
            List<Account> aclist=new List<Account>();
            Rforce_SpecialCustomerStatus.updatespecialcustomerstatus(aclist);            
            Test.stopTest();
        }
}