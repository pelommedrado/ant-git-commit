public with sharing class newLiveagentcontroller{
  
    public String Casesubject { get; set; }
    public String placa { get; set; }
    public String phone { get;set; }
    public String cpf { get; set; }
    public String email { get; set; }
    public String lastName { get; set; }
    public String firstName { get; set; }
   
    public PageReference redirect() {
        Pagereference pagetwoVFP=Page.newPrechatredirectPage;
        return pagetwoVFP;
    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecione um assunto'));
        //options.add(new SelectOption('Produtos e Promocoes','Produtos e Promoções'));
        options.add(new SelectOption('Interagir com o SAC','Interagir com o SAC'));
        options.add(new SelectOption('Financiamento ou Consorcio Renault','Financiamento ou Consórcio Renault'));
        options.add(new SelectOption('Renault Assistance','Renault Assistance'));
        return options;
    }

    public void createOwnerShipRelation(Case caso){

        //Veiculos e relação de posses do banco
        List<VEH_Veh__c> lsVeiculos = [Select Id, VehicleRegistrNbr__c, Name From VEH_Veh__c Where VehicleRegistrNbr__c =: caso.Vehicle_Plate__c];
        List<VRE_VehRel__c> lsVeiculosRel = [Select Id, VIN__c From VRE_VehRel__c 
                                            Where VehicleRegistrNbr__c =: caso.Vehicle_Plate__c And Account__c =: caso.AccountId];
        
        system.debug('Veiculo = '+ lsVeiculos);
        system.debug('Veiculo Relacionado = '+ lsVeiculosRel);

        // cria a relação de posse
        if(!lsVeiculos.isEmpty() && lsVeiculosRel.isEmpty()){
            Id veiculoId = lsVeiculos.get(0).Id;
            createOwnerShip(veiculoId, caso.AccountId);
        }

        // relaciona o veículo com o caso
        if(!lsVeiculos.isEmpty())
        	caso.VIN__c = lsVeiculos.get(0).Id;
    }

    public void createOwnerShip(Id veiculoId, Id accountId){
        VRE_VehRel__c vr = new VRE_VehRel__c();
        vr.VIN__c = veiculoId;
        vr.Account__c = accountId;
        vr.TypeRelation__c = 'User';
        vr.Status__c = 'Active';
        vr.StartDateRelation__c = system.today();
        Insert vr;
        system.debug('Relação de posse gerada = '+ vr );
    }

}