@isTest
public class VFC112_TestDriveVehicle_Import_BOTest {
    
    public static testMethod void myTest(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        insert dealer;
        
        VEH_Veh__c veiculo = moc.criaVeiculo();
        veiculo.IDBIRSellingDealer__c = '123456';
        veiculo.Status__c = 'TEST DRIVE';
        insert veiculo;
        
        List<VEH_Veh__c> lsVeiculos = new List<VEH_Veh__c>();
        VFC112_TestDriveVehicle_Import_BO.getInstance().AfterInsertOn_TestDriveVehicle_Import(lsVeiculos);
        
    }

}