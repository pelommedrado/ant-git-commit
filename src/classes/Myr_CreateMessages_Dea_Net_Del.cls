/****************************************************************************************************************************************
 Name    : Myr_CreateMessages_Dea_Net_Del
 Desc    : Notification DEALER_NETWORK_DELETED
 Project : MyRenault
*****************************************************************************************************************************************
2016 Aug, Author=D. Veron (AtoS) : R16.10 F1854-US3654 - DEALER_NETWORK_DELETED
2016 Nov, Author=D. Veron (AtoS) : Patch on channels
*****************************************************************************************************************************************/
public without sharing class Myr_CreateMessages_Dea_Net_Del{
	public enum Mode_Launch {ASYNC,SYNC}

	//Called from Login Handler
	public static void Dealer_Network_Deleted(Id Account_Id, String Brand, String Country) {
		System.debug('###Myr_CreateMessages_Dea_Net_Del.Dealer_Network_Deleted:BEGIN');
		if (Country_Info__c.getInstance(Country).Myr_Customer_Message_DND_Asynchronous__c){
			Manage_Asynchronously(Account_Id, Brand);
		}else{
			Manage(Account_Id, Brand, Mode_Launch.SYNC); 
		}  
	}
	
	//Manage the notification asynchronously
	@future
	public static void Manage_Asynchronously(Id Account_Id, String Brand) {
		System.debug('###Myr_CreateMessages_Dea_Net_Del.Manage_Asynchronously:BEGIN');
    	Manage(Account_Id, Brand, Mode_Launch.ASYNC);
    }
	
	//Prepare and fire the actions
	public static void Manage(Id Account_Id, String Brand, Myr_CreateMessages_Dea_Net_Del.Mode_Launch Launch_Mode) {
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;

		INDUS_Logger_CLS.addLogChild(null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateMessages_Dea_Net_Del', 'Manage', 'Launch_Mode='+Launch_Mode, null, 'Launch_Mode='+Launch_Mode, INDUS_Logger_CLS.ErrorLevel.Info);

		M_H = Prepare(Account_Id, Brand);

		Launch_Actions(M_H);

		DEALER_NO_DEALER(M_H, Account_Id, system.today());

		
    }

	//Prepare data
	public static Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler Prepare(Id Account_Id, String Brand){
		List<Account> L_A;
		Map<String, Account> Dealers_List;
		Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H;
		String SOQL_QUERY;
		
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Prepare:BEGIN';
				
		System.debug(Prefx+'Account_Id=<'+Account_Id+'>, Brand=<'+Brand+'>');
		SOQL_QUERY= '  SELECT Myr_Status__c, Myd_Status__c, MyR_Dealer_1_BIR__c, MyD_Dealer_1_BIR__c, Firstname';
		SOQL_QUERY+= ' , SecondSurname__c, LastName ';
		if(Schema.SObjectType.Account.fields.getMap().keySet().contains('second_last_name__pc')) {
		    SOQL_QUERY+=' ,Second_Last_Name__pc ';
		}
		SOQL_QUERY+=' FROM Account WHERE Id=\'' + Account_Id + '\'';
		System.debug(Prefx+'SOQL_QUERY=<'+SOQL_QUERY+'>');

		try {
			L_A=Database.query(SOQL_QUERY);
		} catch (Exception e) {
        	return null;
      	}

		if (L_A.size()!=1){
			System.debug(Prefx+'No account found, Account_Id=<'+Account_Id+'>');
			return null;
		}

		M_H = new Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler(L_A[0], Brand);

		return M_H;
	}

	//Possible actions (depending on status and favorite dealer BIR id) are : 
	// - Reset Favorite Dealer Id
	// - Set notif to unread
	// - Create a notif DEALER_NETWORK DELETED
	public static void Launch_Actions(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H){
		String Acc_Status;
		String Acc_Fav_Deal_BIR;
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Launch_Actions:';
		
		System.debug(Prefx+'BEGIN : Brand=<'+M_H.Brand+'>, Account=<'+M_H.My_Account+'>');

		if (M_H!=null){
			if(M_H.Brand.equalsIgnoreCase('Renault')){
				Acc_Status=M_H.My_Account.Myr_Status__c;
				Acc_Fav_Deal_BIR=M_H.My_Account.MyR_Dealer_1_BIR__c;
			}else{
				Acc_Status=M_H.My_Account.Myd_Status__c;
				Acc_Fav_Deal_BIR=M_H.My_Account.MyD_Dealer_1_BIR__c;
			}
		
			if(!String.isEmpty(Acc_Status) && Acc_Status.equalsIgnoreCase(system.Label.Myr_Status_Activated)){
				System.debug(Prefx+'1. Acc_Status=<'+ Acc_Status + '>');
				if (String.isEmpty(Acc_Fav_Deal_BIR)){
					System.debug(Prefx+'2. Prefered dealer is empty => gonna set the status of the message to "read"');
					Set_Status_To_Read(M_H);
				}else if(Acc_Fav_Deal_BIR.startsWithIgnoreCase(System.Label.Myr_Notif_Dealer_Network_Deleted_Prefix)){
					System.debug(Prefx+'3. Prefered dealer begins with DEL_ => generate a notification DEALER_NETWORK_DELETED');
					M_H.Dnd_Generated=true;
					M_H.Dealer_Bir_Id=Acc_Fav_Deal_BIR.substring(4);
					Search_Dealer(M_H);

					Create_A_Message(M_H);
				 
					Set_Dealer_To_Empty(M_H);
				}
			}
			System.debug(Prefx+'Brand=<'+M_H.Brand+'>');
			System.debug(Prefx+'Account=<'+M_H.My_Account+'>');
			System.debug(Prefx+'Dealer=<'+M_H.My_Dealer+'>');
		}else{
			System.debug(Prefx+'M_H is null');
		} 
	}

    public static void Create_A_Message(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H) {
		Myr_CustMessage_Cls New_Msg;
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Create_A_Message:';
		
		System.debug(Prefx+'BEGIN');
		New_Msg				  = new Myr_CustMessage_Cls();
		New_Msg.accountSfdcId = M_H.My_Account.Id;
		New_Msg.brand		  = Myr_CustMessage_Cls.BrandValueOf(M_H.Brand);
		New_Msg.typeid		  = System.Label.Myr_Notif_Dealer_Network_Deleted;
		New_Msg.channels	  = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.popin, Myr_CustMessage_Cls.Channel.message};
		New_Msg.input1		  = M_H.Brand;
		if (String.isEmpty(M_H.My_Dealer.Name)){
			New_Msg.input2    = ' ';
		}else{
			New_Msg.input2    = M_H.My_Dealer.Name;
		}
		System.debug(Prefx+'New_Msg.input2='+New_Msg.input2);
		M_H.listMessages.add(New_Msg);
		System.debug(Prefx+'M_H.listMessages=<'+M_H.listMessages+'>');

		Myr_CreateCustMessage_Cls.CreateMsgResponse response = Myr_CreateCustMessage_Cls.createMessages(M_H.listMessages);
    }

	public static void Set_Dealer_To_Empty(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H) {
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Set_Dealer_To_Empty:';
		Account M_A = M_H.My_Account;
		
		System.debug(Prefx+'BEGIN');
		if(M_H.Brand.equalsIgnoreCase('Renault')){
			M_A.MyR_Dealer_1_BIR__c=null;
		}else if(M_H.Brand.equalsIgnoreCase('Dacia')){
			M_A.MyD_Dealer_1_BIR__c=null;
		}
		
		try{
			update M_A;
			System.debug(Prefx+'M_A=<'+M_A+'>');
		}catch (Exception e) {
			System.debug(Prefx + 'Error = ' + e.getStackTraceString());
		}
    }

	//Get a dealer using his Bir Id
	public static void Set_Status_To_Read(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H) {
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Set_Status_To_Read:';
		List<Customer_Message__c> M_C;
		String SOQL_QUERY;

		SOQL_QUERY='SELECT id, status__c FROM Customer_Message__c WHERE account__c=\'';
		SOQL_QUERY+=M_H.My_Account.Id + '\' and brand__c=\''+M_H.Brand+'\' and Template__r.Name like \'DEALER_NETWORK_DELETED%\'';
		System.debug(Prefx+'SOQL_QUERY=<'+SOQL_QUERY+'>');
		try{
			M_C=Database.query(SOQL_QUERY); 
			if (M_C.size()==1){
				M_C[0].status__c='read';
				update M_C[0];
			}
		}catch(Exception e){
			System.debug(Prefx+'Error = ' + e.getStackTraceString() + ' # ' + e);
      	}	
		System.debug(Prefx+'END=<'+SOQL_QUERY+'>');
	}

	//Get a dealer using his Bir Id
	public static void Search_Dealer(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H) {
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.Search_Dealer:';
		String SOQL_QUERY;
		List<Account> L_A;
		String Bir_Id=M_H.Dealer_Bir_Id;
		
		if (!String.isEmpty(Bir_Id)){
			SOQL_QUERY='SELECT IDBIR__c, Name FROM Account WHERE IDBIR__c=\''+Bir_Id+'\'';
			System.debug(Prefx+'SOQL_QUERY='+SOQL_QUERY);
			try{
				L_A=Database.query(SOQL_QUERY);
			}catch(Exception e){
				System.debug(Prefx+'Error = ' + e.getStackTraceString() + ' # ' + e);
      		}

			if (L_A.size()==1){
				M_H.My_Dealer=L_A[0];
			}
		}
		System.debug(Prefx+'END=<'+SOQL_QUERY+'>');
	}

	//DEALER_NO_DEALER
	public static void DEALER_NO_DEALER(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H, String accountId, Date dateReference) {
		List<Customer_Message__c> List_Notif_Dealer;
		Boolean existDealer;
		String Prefx='###Myr_CreateMessages_Dea_Net_Del.DEALER_NO_DEALER';

		System.debug(Prefx+'BEGIN M_H.Dnd_Generated=<'+M_H.Dnd_Generated+'>');

		if(!M_H.Dnd_Generated){
		
			try{
				if (M_H.Brand.equalsIgnoreCase('Renault')) { existDealer = !String.isBlank(M_H.My_Account.MyR_Dealer_1_BIR__c);}
				if (M_H.Brand.equalsIgnoreCase('Dacia')) { existDealer =  !String.isBlank(M_H.My_Account.MyD_Dealer_1_BIR__c);}

				List_Notif_Dealer = [SELECT Id, Status__c, LastModifiedDate 
									FROM Customer_Message__c 
									WHERE Template__r.Name like 'DEALER_NO_DEALER%' 
									and Brand__c = :M_H.Brand and account__c = :accountId];
			
				if(existDealer) {
					if(List_Notif_Dealer!=null && List_Notif_Dealer.size()>0){
						System.debug(Prefx+'CAS1 There is existing preferred dealer and existing notification');
						List_Notif_Dealer[0].Status__c = 'read';
						update List_Notif_Dealer[0];
					}
				} else {
					System.debug(Prefx+'CAS3 there is no preferred dealer and no notification	');			
    				 if(M_H.Brand.equalsIgnoreCase('Renault') && !String.isBlank(M_H.My_Account.Myr_Status__c) && M_H.My_Account.Myr_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Activated)){
						System.debug(Prefx+'Brand='+Myr_CustMessage_Cls.Brand.Renault);
						LH_Add_DEALER_NO_DEALER(M_H, Myr_CustMessage_Cls.Brand.Renault);
					}
					if(M_H.Brand.equalsIgnoreCase('Dacia') && !String.isBlank(M_H.My_Account.Myd_Status__c) && M_H.My_Account.Myd_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Activated)){
						System.debug(Prefx+'Brand='+Myr_CustMessage_Cls.Brand.Dacia);
						LH_Add_DEALER_NO_DEALER(M_H, Myr_CustMessage_Cls.Brand.Dacia);
					}
				}
			}catch (Exception e) {
				System.debug(Prefx+'Error = ' + e.getStackTraceString() + ' # ' + e);
			}

			Myr_CreateCustMessage_Cls.CreateMsgResponse response = Myr_CreateCustMessage_Cls.createMessages(M_H.listMessages);
		}
	}

    //Add DEALER_NO_DEALER notification (call LH_Messages)
    public static void LH_Add_DEALER_NO_DEALER(Myr_CreateMessages_TriggerHandler_Cls.Msg_Handler M_H, Myr_CustMessage_Cls.Brand Brand) {
		Myr_CustMessage_Cls New_Msg;
		
		New_Msg=new Myr_CustMessage_Cls();
		
		Myr_CreateMessages_TriggerHandler_Cls.LH_Messages(M_H, system.Label.Myr_Notif_Dealer_No_Dealer, Brand, New_Msg);
		M_H.listMessages.add(New_Msg);
    }
}