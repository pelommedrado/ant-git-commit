global class SurveyAnswerScheduled implements Schedulable {
	global void execute(SchedulableContext sc) {
		ID batchprocessid = Database.executeBatch(new SurveyAnswerBatch(), 50);
		System.debug(batchprocessid);
	}
}