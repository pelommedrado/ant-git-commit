public with sharing class VFC01_CaseView {
    public String caseView {get;set;}
    public String userId {get;set;}
    public String profileid{get;set;}
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    
    public VFC01_CaseView(ApexPages.StandardController controller) {
        system.debug('Test Case View');
        caseView='1';
        userId ='test';
        sortExp ='CreatedDate';
        sortDirection ='ASC';
        CaseViewList();        
    }
    
    public PageReference redirectToPage() {        
        PageReference pageRef=new PageReference('/');
        return pageRef;
    }
    public ApexPages.StandardSetController setCon {
       get{
        if(setCon == null){
            size = 5;
            setCon = new ApexPages.StandardSetController(caselist);
            setCon.setPageSize(size);
            noOfRecords = setCon.getResultSize(); 
        }
            return setCon;
        }
        set;
    }
 //Ticket-No 4552   
    private String recordTypeCaseView = '( RecordType.DeveloperName = \'BR_RecType\' or'+
        ' RecordType.DeveloperName = \'Deal_RecType\' or'+' RecordType.DeveloperName = \'Core_Community_RecType\' or'+
        ' RecordType.DeveloperName = \'CORE_CAS_Case_RecType\')';
    
    public List<Case> caselist{get;set;}
    public String ErrorMessage ;
    //For Sorting
    public String sortname{get;set;}
    private String sortDirection = 'ASC';
    private String sortExp = 'name';
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
    
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
     public List<Case> getCasePageList(){
        List<Case> c = new List<Case>();
        for(Case ca : (List<Case>)setCon.getRecords()){
        c.add(ca);
        }
        return c;
    }
    
    
       public void first() {
        setCon.first();
    }
 
    public void last() {
        setCon.last();
    }
 
    public void previous() {
        setCon.previous();
    }
 
    public void next() {
        setCon.next();
        system.debug('####setCon next:' + setCon.getHasNext());
    }
    
    public Boolean hasNext {
     get {
         return setCon.getHasNext();
       }
     set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
 
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
    
    public PageReference CaseViewList(){
        Boolean Success ;
        PageReference pageRef=new PageReference('/');
        try
        {    
            system.debug('User Id: '+UserInfo.getUserId());
            system.debug('Case View Value: ' + caseView  );
            userId =UserInfo.getUserId();
            
            String name=UserInfo.getName();
            
            system.debug('#### userId:::: '+userId);
            system.debug('#### NAME:::: '+name);
            
            string sortFullExp = sortExp + ' ' + sortDirection;
            
            system.debug('##SortExp'+sortFullExp);
            try {
            Id ContactId=[select Id,ContactId from User where Id=:UserInfo.getUserId()].contactId;
            system.debug('ContactId'+ContactId);
            Contact contact=[select account.id from contact where Id=:ContactId limit 1];
            
            system.debug('#### contact:::: '+contact.id);
            String id=contact.account.id; 
            system.debug('#### id:::: '+id);
            }catch (Exception ex ) {}
            //Getting Case List for my case
            if(caseView.equals('1'))
            {
                system.debug('#### My Case ####');
                //SOQL Query
                // caselist =[select CaseNumber,Contact.name,VIN__c,VehicleRegistrNbr__c,Subject,Status,Priority,CreatedDate,Owner.alias from case where createdby.id=:userId order by CaseNumber ];
                caselist= Database.query('select id,CaseNumber,Contact.name,VIN__c,VehicleRegistrNbr__c,DealerBizName__c,Type,Subject,Dealer__c,Account.Name,status,Description,CreatedDate,Priority,Owner.alias from case where  createdby.id=:userId and '+recordTypeCaseView+' and (status=\'Open\' or status=\'New\' or status=\'Waiting for reply\' or status=\'Escalated\' or status=\'Answered\' or status=\'Resolved\' or status=\'Reopen\' or status=\'Draft\') order by '+ sortFullExp);
                
                for(Case c:caselist)
                {
                    
                    system.debug('#### MYcaselist #CaseNumber:::: '+c.CaseNumber);
                    system.debug('#### MYcaselist #Owner.alias:::: '+c.Owner.alias);
                    
                }
                
            }else if (caseView.equals('2')) {
                system.debug('#### MY Dealer Case ####' );
                
                // caselist= [select CaseNumber,Contact.name,Subject,Status,Priority,CreatedDate,Owner.alias from case where account.id=:contact.account.id order by CaseNumber];
                caselist= Database.query('select id,CaseNumber,Contact.name,VIN__c,VehicleRegistrNbr__c,DealerBizName__c,Type,Subject,Dealer__c,Account.Name,status,Description,CreatedDate,Priority,Owner.alias from case where '+recordTypeCaseView+' and (Dealer__c=:id OR createdby.id=:userId) and (status=\'Open\' or status=\'New\' or status=\'Waiting for reply\' or status=\'Escalated\' or status=\'Answered\' or status=\'Resolved\' or status=\'Reopen\' or status=\'Draft\') order by '+sortFullExp);
                                
                
                for(Case c:caselist)
                {       
                    system.debug('#### caselist #CaseNumber:::: '+c.CaseNumber);
                    system.debug('#### caselist #Owner.alias:::: '+c.Owner.alias);        
                }
            }else{              
                system.debug('#### Closed Case ####' );                
                caselist= Database.query('select id,CaseNumber,Contact.name,VIN__c,VehicleRegistrNbr__c,DealerBizName__c,Type,Subject,Dealer__c,Account.Name,status,Description,CreatedDate,Priority,Owner.alias from case where '+recordTypeCaseView+' and (Dealer__c=:id OR createdby.id=:userId) and status=\'Closed\' order by '+sortFullExp+'LIMIT 1000');
                for(Case c:caselist)
                {
                    
                    system.debug('#### caselist #CaseNumber:::: '+c.CaseNumber);
                    system.debug('#### caselist #Owner.alias:::: '+c.Owner.alias);
                    
                }
                
            }
            
        }
        catch(exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Test');
            ApexPages.addMessage(myMsg);
            ErrorMessage = String.valueOf(e);
            system.debug('erreur : ' + e.getMessage());
        } 
        
        pageRef.setRedirect(true);
        setCon = null;
        return null;
    }
    
}