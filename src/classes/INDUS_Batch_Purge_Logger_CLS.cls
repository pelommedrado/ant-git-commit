/***************************************************************************************************
  Name    : INDUS_Batch_Purge_Logger_CLS    					                     Creation : 2015             
  Desc    : Purge of logs   											         Project : myRenault
  **************************************************************************************************
  17 May 2016, 16.07 BackEagle, D. Veron (AtoS) : Purge retention is set by project and type of logs
  *************************************************************************************************/
global class INDUS_Batch_Purge_Logger_CLS extends INDUS_Batch_AbstractBatch_CLS {
    //Starting the batch: initialize the query
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String Quer;
        CS14_Logger_Settings__c setting;
        Id Record_Type_Application_Id;
        Id Record_Type_Supervision_Id;
        
        //Initialisations
        setting = CS14_Logger_Settings__c.getInstance();
        Record_Type_Application_Id = (Myr_Datasets_Test.getRecordTypeByAPIName('Logger__c','Application')).Id;
        Record_Type_Supervision_Id = (Myr_Datasets_Test.getRecordTypeByAPIName('Logger__c','Supervision')).Id;
         
        String MYR_App = (DateTime.now() - Integer.valueOf(setting.Retention_MYR_Application__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String MYR_Sup = (DateTime.now() - Integer.valueOf(setting.Retention_MYR_Supervision__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String LMTSFA_App = (DateTime.now() - Integer.valueOf(setting.Retention_LMTSFA_Application__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String LMTSFA_Sup = (DateTime.now() - Integer.valueOf(setting.Retention_LMTSFA_Supervision__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String TRV_App = (DateTime.now() - Integer.valueOf(setting.Retention_TRV_Application__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String TRV_Sup = (DateTime.now() - Integer.valueOf(setting.Retention_TRV_Supervision__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        
        Quer ='SELECT Id FROM Logger__c WHERE ';
        Quer+='   (RecordTypeId=\''+Record_Type_Application_Id+'\' and Project_Name__c=\'MYR\'    and CreatedDate<='+MYR_App+') ';
        Quer+='or (RecordTypeId=\''+Record_Type_Supervision_Id+'\' and Project_Name__c=\'MYR\'    and CreatedDate<='+MYR_Sup + ') ';
        Quer+='or (RecordTypeId=\''+Record_Type_Application_Id+'\' and Project_Name__c=\'LMTSFA\' and CreatedDate<='+LMTSFA_App + ') ';
        Quer+='or (RecordTypeId=\''+Record_Type_Supervision_Id+'\' and Project_Name__c=\'LMTSFA\' and CreatedDate<='+LMTSFA_Sup + ') ';
        Quer+='or (RecordTypeId=\''+Record_Type_Application_Id+'\' and Project_Name__c=\'TRV\'    and CreatedDate<='+TRV_App + ') ';
        Quer+='or (RecordTypeId=\''+Record_Type_Supervision_Id+'\' and Project_Name__c=\'TRV\'    and CreatedDate<='+TRV_Sup + ') ';
      
        System.debug('##### INDUS_Batch_Purge_Logger_CLS.start Query = <' + Quer + '>');
        return Database.getQueryLocator(Quer);
    }
    
    //Execute the batch : the things to be done one the given scope
    global override DatabaseAction doExecute( Database.BatchableContext info, List<sObject> records){
        System.debug('##### INDUS_Batch_Purge_Logger_CLS.doExecute records=<' + records + '>');
        return new DatabaseAction(records, Action.ACTION_DELETE, false);
    }
    //Finishing the batch
    global override void doFinish(Database.BatchableContext info){ 
        System.debug('##### INDUS_Batch_Purge_Logger_CLS.doFinish - Finish ');
    }
    //Get class name
    global override String getClassName(){
        return INDUS_Batch_Purge_Logger_CLS.class.GetName();
    }
}