/**
* Classe que representa a agenda do Test Drive SAC.
* @author Felipe Jesus Silva 
*/
public class VFC114_ScheduleTestDriveSACVO 
{
	public String hour {get;set;}
	public String schedulingStatus {get;set;}
	
	public VFC114_ScheduleTestDriveSACVO()
	{
		
	}
}