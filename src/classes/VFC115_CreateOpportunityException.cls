/**
* Classe que representa exceções geradas na criação da oportunidade.
* @author Felipe Jesus Silva.
*/
public class VFC115_CreateOpportunityException extends Exception 
{
	public VFC115_CreateOpportunityException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}