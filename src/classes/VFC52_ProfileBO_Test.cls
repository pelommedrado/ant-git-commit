@isTest
private class VFC52_ProfileBO_Test {

    static testMethod void testVFC52_ProfileBO() {
    	
    	Profile prof = [Select Id,Name from Profile where Name = 'Gold Partner User'];    	
        System.assert(VFC52_ProfileBO.getInstance().isProfile(prof.Id, 'Gold Partner User'));
        
    }
}