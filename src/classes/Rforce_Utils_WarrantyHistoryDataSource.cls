public with sharing class Rforce_Utils_WarrantyHistoryDataSource {

    public Boolean Test;
     
         public Rforce_ApvGetDonIran1.ApvGetDonIran1Response getWarrantyHistoryData(Rforce_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
         
        // --- PRE TREATMENT ----
        Rforce_ApvGetDonIran1.ApvGetDonIran1Request request = new Rforce_ApvGetDonIran1.ApvGetDonIran1Request();       
        Rforce_ApvGetDonIran1.ServicePreferences servicePref = new Rforce_ApvGetDonIran1.ServicePreferences();
        servicePref.vin = VehController.getVin();   
        servicePref.codPays = System.Label.codPays;   //servicePref.codPays = 'FR'  
        //servicePref.codlanguage = System.Label.CodLanguage; //servicePref.codlanguage = 'FRA'
        System.debug('####### getLanguage '+UserInfo.getLanguage().substring(0, 2).toUpperCase());
        servicePref.codlanguage = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_ApvGetDonIran1.ApvGetDonIran1 WHWS = new Rforce_ApvGetDonIran1.ApvGetDonIran1();
        WHWS.endpoint_x = System.label.VFP05_WarrantyHistoryURL;   
        WHWS.clientCertName_x = System.label.RenaultCertificate;   
        WHWS.timeout_x=40000;
        
        Rforce_ApvGetDonIran1.ApvGetDonIran1Response WH_WS = new Rforce_ApvGetDonIran1.ApvGetDonIran1Response();    
    
        if (Test==true) {
            WH_WS = Rforce_Utils_Stubs.WarrantyHistoryStub();
            system.debug('test = true' +  WH_WS);  
        } else {

            WH_WS = WHWS.getApvGetDonIran1(request);
                        system.debug('test = false' +  WH_WS);
        }
         return WH_WS;  
     
    }
}