/**
* @author: Vetrivel Sundararajan
* @date: 07/08/2014
* @description: This apex class is a POJO class which will contains the values of PartyOverview and Party
* Vehicles values in it.
* @author Sundaramurthy.T
* @date 13/11/2014
* @see Issue ID - BlueHorse-001
* Issue due to Mappping of the Petit soucis de mapping
*/

public class Rforce_MDMPartyOverview_CLS {

    public String strPartyID{get;set;}
    
    // Party
    public String strAccoutType{get;set;}
    public String strPartySubType{get;set;}
    public String strFirstName{get;set;}
    public String strSecondFirstName{get;set;}
    public String strLastName{get;set;}
    public String strSecondLastName{get;set;}
    public String strDOB{get;set;}
    public String strCivility{get;set;}
    public String strTitle{get;set;}
    public String strGender{get;set;}
    public String strMartialStatus{get;set;}
    public String strLanguage{get;set;}
    public String strRenaultEmployee{get;set;}
    public Integer intNumberofChildren{get;set;}
    public String strDeceased{get;set;}
    public String strPreferredMedia{get;set;}
    public String strOccupationalCategoryCode{get;set;}
    
    // Added for MDM Enanchements on 14th Jan 2015
    
    public String strCompanyId{get;set;}
    public String strTypeofActivity{get;set;}
    public String strCodeofActivity{get;set;}
    
    // Ended for MDM Enanchements on 14th Jan 2015

    //BlueHorse-001
    public String strOccupationalCategoryDescription{get;set;}
    public String strPartySegment{get;set;}
    public String strOrganisationName{get;set;}
    public String strCommercialName{get;set;}
    public Integer intNumberOfEmployees{get;set;}
    public String strFinancialStatus{get;set;}
    public String strCompanyActivityCode{get;set;}
    //BlueHorse-001
    public String strCompanyActivityDescription{get;set;}   
    public String strAccountLabel{get;set;}
    
    public String strLegalNature{get;set;}
    
    // Address
    public String strAddressLine1{get;set;}
    public String strAddressLine2{get;set;}
    public String strAddressLine3{get;set;}
    public String strAddressLine4{get;set;}
    public String strCity{get;set;}
    public String strRegion{get;set;}
    public String strPostCode{get;set;}
    public String strCountry{get;set;}
    
    // ElectronicAddress
    public String strEmail{get;set;}
    public String strEmail2{get;set;}

    // PhoneNumber
    public String strFixeLandLine{get;set;}
    public String strFixeLandLine2{get;set;}
    public String strMobile{get;set;}
    public String strMobile2{get;set;}

    //PartyIdentification
    public String strPartyIdentificationType{get;set;}
    public String strPartyIdentificationValue{get;set;}
    public String strSiretValue{get;set;}
 
    //PreferredDealer
    public String strDealerCode{get;set;}
    
    //StopCommunication
    public String strStopCommunicationType{get;set;}
    public String strStopCommunication{get;set;}

     
    
    public List<Rforce_MDMPartyOverview_CLS.Vehicle> vcle {get;set;}
    
    public class Vehicle{
    public String vin {get;set;}
    public String IdClient{get;set;}
    public String brandCode {get;set;}
    
    public String brandLabel{get;set;}
    public String modelCode{get;set;}
    public String modelLabel{get;set;}
    public String firstRegistrationDate{get;set;}

    public String currentMileage{get;set;}
    public String currentMileageDate{get;set;}
    public String vehicleType{get;set;}
    public String deliveryDate{get;set;}
    public String registration{get;set;}
    public String newVehicle{get;set;}

    public String startDate{get;set;}
    public String endDate{get;set;}
    
    public String versionLabel {get;set;}
    public String possessionBegin{get;set;}
      
  }

}