/*  Test class of Myr_AccountStatus_WS web service
*************************************************************************************************************
03.08.2016  : Creation
*************************************************************************************************************/
@isTest
private class Myr_AccountStatus_WS_Test {

	@testsetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, true) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    }

	/****************************************************************************/
	/******* UNIT TESTS *********************************************************/

	//Failed: invalid parameters
	//WS09MS501
	static testMethod void test_invalidParameters()  {
		//1 Prepare data
		
		//Check blank email
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = '';
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS501', response.info.code );

		//Check null email
		input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = null;
		response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS501', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//Failed: too long parameters
	//WS09MS502
	static testMethod void test_tooLongParameters()  {
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = 'emailcontaininigmorethan80charactersssssssssssssssssssssssssssssssss@fakemail.com';
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS502', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//Failed: invalid email
	//WS09MS503
	//the goal of this test is to check the response of the webservice not the format of the email
	//as this part is already tested in Myr_MyRenaultTools_Test.checkEmailPattern
	static testMethod void test_invalidEmail()  {
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = 'test-32_yahoo@test';
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS503', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//Failed: more than one personal account found
	//WS09MS504
	static testMethod void test_MoreThan1Account()  {
		String email = 'patrice_dupont@unit.atos.test';
		
		//1 Prepare the data: insert 2 accounts with the same id
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
			List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[0].renaultId = email;
			listQueries[0].myr_status = null; //available for MyRenault
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[1].daciaid = email;
			listQueries[1].myd_status = system.Label.Myr_Status_Activated; //already activated for MyDacia
			List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		}

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS504', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//Failed: no account found, username not available althrough salesforce org
	//WS09MS505
	static testMethod void test_NoAccount_NotAvailableUsername()  {
		//Insert a user
		String email = 'testFakeAtos.1234AnyEmail@atos.net';
		User dealerUser = Myr_Datasets_Test.getDealerUser('rhelios_testFakeAtos', '1234AnyEmail', 'France');
		insert dealerUser;
		system.assertEquals( 1, [SELECT Id FROM User WHERE Username=:dealerUser.username].size() );

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS505', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//OK: no account found, username available althrough salesforce org
	//WS09MS000
	static testMethod void test_NoAccount_AvailableUsername()  {
		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = 'availableEmail@atos.unittest.com';
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS000', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( system.Label.Myr_AccountStatus_UserStatus_NotExist, response.output.userStatus );
	}

	//OK: no account found, username available althrough salesforce org
	//WS09MS000
	static testMethod void test_NoAccount_AvailableUsername_WithPrefix()  {
		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = 'rhelios_availableEmail@atos.unittest.com';
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS000', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( system.Label.Myr_AccountStatus_UserStatus_NotExist, response.output.userStatus );
	}

	//OK With Warning: account found, user found
	//WS09MS002
	static testMethod void test_Account_ActiveUser()  {
		String email = 'patrice_dupont@unit.atos.test';
		Account insertedAccount = new Account();
		
		//1 Prepare the data: insert 2 accounts with the same id
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.ACTIVE_USERS;
			List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[0].renaultId = email;
			listQueries[0].myr_status = null; //available for MyRenault
			listQueries[0].daciaid = email;
			listQueries[0].myd_status = system.Label.Myr_Status_Activated; //already activated for MyDacia
			List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
			insertedAccount = listAccounts[0];
		}

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS002', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( system.Label.Myr_Status_Activated, response.output.myd_status );
		system.assertEquals( insertedAccount.Id, response.output.accountSfdcId );
		system.assertEquals( system.Label.Myr_AccountStatus_UserStatus_Exist, response.output.userStatus );
	}

	//OK With Warning: account found, user found
	//WS09MS002
	static testMethod void test_Account_InActiveUser()  {
		String email = 'patrice_dupont@unit.atos.test';
		Account insertedAccount = new Account();
		
		//1 Prepare the data: insert 2 accounts with the same id
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.INACTIVE_USERS;
			List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[0].renaultId = email;
			listQueries[0].myr_status = system.Label.Myr_Status_Deleted; //available for MyRenault
			listQueries[0].daciaid = email;
			listQueries[0].myd_status = system.Label.Myr_Status_Activated; //already activated for MyDacia
			List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
			insertedAccount = listAccounts[0];
		}

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS002', response.info.code );
		system.assertEquals( system.Label.Myr_Status_Deleted, response.output.myr_status );
		system.assertEquals( system.Label.Myr_Status_Activated, response.output.myd_status );
		system.assertEquals( insertedAccount.Id, response.output.accountSfdcId );
		system.assertEquals( system.Label.Myr_AccountStatus_UserStatus_Exist, response.output.userStatus );
	}

	//Failed With Warning: account found, no user, username not available althrough salesforce org
	//WS09MS505
	static testMethod void test_Account_NoUser_NotAvailableUsername()  {
		Account insertedAccount = new Account();
		
		//1 Prepare the data: insert 2 accounts with the same id
		//prepare user
		String email = 'patrice.dupont_unittestatos@atos.net';
		User dealerUser = Myr_Datasets_Test.getDealerUser('rhelios_patrice', 'dupont_unittestatos', 'France');
		insert dealerUser;
		system.assertEquals( 1, [SELECT Id FROM User WHERE Username=:dealerUser.username].size() );
		//prepare account
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
			List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[0].renaultId = email;
			listQueries[0].myr_status = null; //available for MyRenault
			listQueries[0].daciaid = email;
			listQueries[0].myd_status = system.Label.Myr_Status_Activated; //already activated for MyDacia
			List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
			insertedAccount = listAccounts[0];
		}

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS505', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

	//OK With Warning: account found, no user,  username available althrough salesforce org
	//WS09MS001
	static testMethod void test_Account_NoUser_AvailableUsername()  {
		Account insertedAccount = new Account();
		String email = 'patrice.dupont_unittestatos@atos.net';
		
		//1 Prepare the data: insert 2 accounts with the same id
		//prepare account
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
			List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
			listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
			listQueries[0].renaultId = email;
			listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
			listQueries[0].daciaid = email;
			listQueries[0].myd_status = system.Label.Myr_Status_PreRegistrered; //already activated for MyDacia
			List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
			insertedAccount = listAccounts[0];
		}

		//2 Trigger the test
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = email;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );
		system.assertEquals( 'WS09MS001', response.info.code );
		system.assertEquals( system.Label.Myr_Status_Activated, response.output.myr_status );
		system.assertEquals( system.Label.Myr_Status_PreRegistrered, response.output.myd_status );
		system.assertEquals( insertedAccount.Id, response.output.accountSfdcId );
		system.assertEquals( system.Label.Myr_AccountStatus_UserStatus_NotExist, response.output.userStatus );
	}

	//Failed: general exception 
	//WS09MS999
	static testMethod void test_GeneralException()  {
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input input = new Myr_AccountStatus_WS.Myr_AccountStatus_WS_Input();
		input.emailAddress = Myr_AccountStatus_WS.GENERAL_EXCEPTION_SIMULATION;
		Myr_AccountStatus_WS.Myr_AccountStatus_WS_Response response = Myr_AccountStatus_WS.checkEmail( input );

		system.assertEquals( 'WS09MS999', response.info.code );
		system.assertEquals( null, response.output.myr_status );
		system.assertEquals( null, response.output.myd_status );
		system.assertEquals( null, response.output.accountSfdcId );
		system.assertEquals( null, response.output.userStatus );
	}

}