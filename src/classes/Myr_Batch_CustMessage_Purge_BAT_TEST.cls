/** TestClass dedicated to purge of customer messages
  @author Sébastien Ducamp
  @version 1.0
  @date 23.07.2015
**/
@isTest
private class Myr_Batch_CustMessage_Purge_BAT_TEST { 
  
  @testSetup static void setCustomSettings() {
    Myr_Datasets_Test.prepareRequiredCustomSettings();
    Myr_Datasets_Test.prepareCustomerMessageSettings();
    //prepare basic data sets
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts( 5, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
  } 
  
  //@return Customer_Message__c with regardless fields filled with static information 
  private static Customer_Message__c getBatchFakeMsg(Id accId, Id tempId, String status, Date expDate) {
    return new Customer_Message__c(
      Account__c = accId, 
      Template__c = tempId, 
      Status__c = status,
      ExpirationDate__c = expDate,
      Channel__c = 'message', 
      Title__c = 'fake title', 
      Summary__c = 'fake summary', 
      Body__c = 'fake body'
    );
  }
  
  //TEST: CORE function used to test different history retentions
  private static void test_purge_with_different_retentions(Integer retHist) {
    //Prepare a list of accounts
        List<Account> listAcc = [SELECT Id FROM Account LIMIT 5];
        //take a template with no merge option
        Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
        system.AssertNotEquals(null, template);
        String read = system.Label.Customer_Message_Read;
        String unread = system.Label.Customer_Message_UnRead;
        String del = system.Label.Customer_Message_Deleted;
        //Prepare a list of messages to delete: status=read/unread/delete AND expirationdate < today
        List<Customer_Message__c> listDel_01 = new List<Customer_Message__c>();
        listDel_01.add( getBatchFakeMsg(listAcc[0].Id, template.Id, read, system.today().addDays(-1)) );
        listDel_01.add( getBatchFakeMsg(listAcc[1].Id, template.Id, unread, system.today().addDays(-1)) );
        listDel_01.add( getBatchFakeMsg(listAcc[2].Id, template.Id, del, system.today().addDays(-1)) );
        insert listDel_01;
        //Prepare a list of messages to delete: statusdate = deleted AND statusupdatedate < today - retentionhistory
        //WARNING: STATUS UPDATE DATE TO MODIFY AFTER CREATION BECAUSE OF WORKFLOW INITIALIZING THIS FIELD WHEN NEW !!!
        List<Customer_Message__c> listDel_02 = new List<Customer_Message__c>();
        listDel_02.add( getBatchFakeMsg(listAcc[0].Id, template.Id, del, null) );
        listDel_02.add( getBatchFakeMsg(listAcc[1].Id, template.Id, del, null) );
        insert listDel_02;
        listDel_02 = [SELECT Account__c, Template__c, Status__c, StatusUpdateDate__c, ExpirationDate__c, Channel__c, Title__c, Summary__c, Body__c FROM Customer_Message__c WHERE Id=:listDel_02];
        listDel_02[0].StatusUpdateDate__c = system.now().addDays(-1*(retHist+1));
        listDel_02[1].StatusUpdateDate__c = system.now().addDays(-1*(retHist+10));
        update listDel_02;
        //Prepare a list of messages to delete: statusdate = deleted AND statusupdatedate < today - retentionhistory and expiration date in the future
        List<Customer_Message__c> listDel_03 = new List<Customer_Message__c>();
        listDel_03.add( getBatchFakeMsg(listAcc[0].Id, template.Id, del, system.today().addDays(10)) );
        insert listDel_03;
        listDel_03 = [SELECT Account__c, Template__c, Status__c, StatusUpdateDate__c, ExpirationDate__c, Channel__c, Title__c, Summary__c, Body__c FROM Customer_Message__c WHERE Id=:listDel_03];
        listDel_03[0].StatusUpdateDate__c = system.now().addDays(-1*(retHist+1));
        update listDel_03;
        //Prepare a list of messages to keep  : status=read/unread/deleted expiration date not set
        List<Customer_Message__c> listKeep_01 = new List<Customer_Message__c>();
        listKeep_01.add( getBatchFakeMsg(listAcc[0].Id, template.Id, read, null) );
        listKeep_01.add( getBatchFakeMsg(listAcc[0].Id, template.Id, unread, null) );
        listKeep_01.add( getBatchFakeMsg(listAcc[0].Id, template.Id, del, null) );
        listKeep_01.add( getBatchFakeMsg(listAcc[1].Id, template.Id, read, null) );
        listKeep_01.add( getBatchFakeMsg(listAcc[1].Id, template.Id, unread, null) );
        listKeep_01.add( getBatchFakeMsg(listAcc[1].Id, template.Id, del, null) );
        insert listKeep_01;
        //Prepare a list of messages to keep  : status=read/unread/deleted expiration date >= today
        List<Customer_Message__c> listKeep_02 = new List<Customer_Message__c>();
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, read, system.today()) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, read, system.today().addDays(1)) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, unread, system.today()) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, unread, system.today().addDays(1)) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, del, system.today()) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, del, system.today().addDays(1)) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, unread, system.today().addDays(100)) );
        listKeep_02.add( getBatchFakeMsg(listAcc[2].Id, template.Id, del, system.today().addDays(100)) );
        insert listKeep_02;
        //Prepare a list of messages to keep  : statusdate = deleted and statusupdatedate >= today - retentionhistory
        List<Customer_Message__c> listKeep_03 = new List<Customer_Message__c>();
        listKeep_03.add( getBatchFakeMsg(listAcc[1].Id, template.Id, del, null) );
        listKeep_03.add( getBatchFakeMsg(listAcc[1].Id, template.Id, del, null) );
        listKeep_03.add( getBatchFakeMsg(listAcc[1].Id, template.Id, del, null) );
        insert listKeep_03;
        listKeep_03 = [SELECT Account__c, Template__c, Status__c, StatusUpdateDate__c, ExpirationDate__c, Channel__c, Title__c, Summary__c, Body__c FROM Customer_Message__c WHERE Id=:listKeep_03];
        listKeep_03[0].StatusUpdateDate__c = system.now();
        listKeep_03[1].StatusUpdateDate__c = system.now().addDays(-1 * retHist);
        listKeep_03[2].StatusUpdateDate__c = system.now().addDays(-1 * retHist + 1);
        update listKeep_03;      
        //Prepare a list of messages to keep  : statusdate = read/unread and statusupdatedate < today - retentionhistory
        List<Customer_Message__c> listKeep_04 = new List<Customer_Message__c>();
        listKeep_04.add( getBatchFakeMsg( listAcc[0].Id, template.Id, read, null ));
        listKeep_04.add( getBatchFakeMsg( listAcc[0].Id, template.Id, read, null ));
        listKeep_04.add( getBatchFakeMsg( listAcc[0].Id, template.Id, read, null ));
        insert listKeep_04;
        listKeep_04 = [SELECT Account__c, Template__c, Status__c, StatusUpdateDate__c, ExpirationDate__c, Channel__c, Title__c, Summary__c, Body__c FROM Customer_Message__c WHERE Id=:listKeep_04];
        listKeep_04[0].StatusUpdateDate__c = system.now().addDays(-1*retHist);
        listKeep_04[1].StatusUpdateDate__c = system.now().addDays(-1*retHist-1);
        listKeep_04[2].StatusUpdateDate__c = system.now().addDays(-1*retHist-100);
        update listKeep_04;  
        
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listDel_01.size()=' + listDel_01.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listDel_02.size()=' + listDel_02.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listDel_03.size()=' + listDel_03.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listKeep_01.size()=' + listKeep_01.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listKeep_02.size()=' + listKeep_02.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listKeep_03.size()=' + listKeep_03.size());
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> listKeep_04.size()=' + listKeep_04.size());
        Integer totalToKeep = listKeep_01.size() + listKeep_02.size() + listKeep_03.size() + listKeep_04.size();
        Integer totalToDel = listDel_01.size() + listDel_02.size() + listDel_03.size();
        Integer totalMsgBeforePurge =  totalToKeep + totalToDel;
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> total=' + totalMsgBeforePurge);
        system.debug('### Myr_Batch_CustMessage_Purge_BATch_Test - <test_OK_PurgeMessages> total to keep ='+totalToKeep+', total to del='+totalToDel+', total='+totalMsgBeforePurge);
        
        //---CONTROL BEFORE LAUNCHING
        List<Customer_Message__c> listBefore = [SELECT Id FROM Customer_Message__c];
        system.assertEquals( totalMsgBeforePurge, listBefore.size() );
        
        //---LAUNCH THE PURGE
        Myr_Batch_CustMessage_Purge_BAT batch = new Myr_Batch_CustMessage_Purge_BAT();
        system.AssertEquals(retHist, batch.m_retention);
        Test.startTest();
        Database.executeBatch(batch, 200);
        Test.stopTest();
        
        //---CONTROL AFTER THE PURGE  
        List<Customer_Message__c> listKeep01_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listKeep_01];
        //system.assertEquals(listKeep_01.size(), listKeep01_after.size());
        List<Customer_Message__c> listKeep02_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listKeep_02];
        //system.assertEquals(listKeep_02.size(), listKeep02_after.size());
        List<Customer_Message__c> listKeep03_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listKeep_03];
        //system.assertEquals(listKeep_03.size(), listKeep03_after.size());
        List<Customer_Message__c> listKeep04_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listKeep_04];
        system.assertEquals(listKeep_04.size(), listKeep04_after.size());
        List<Customer_Message__c> listDel01_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listDel_01];
        system.assertEquals(0, listDel01_after.size());
        List<Customer_Message__c> listDel02_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listDel_02];
        system.assertEquals(0, listDel02_after.size());
        List<Customer_Message__c> listDel03_after = [SELECT Id FROM Customer_Message__c WHERE Id IN :listDel_03];
        system.assertEquals(0, listDel03_after.size());
        List<Customer_Message__c> listAfter = [SELECT Id FROM Customer_Message__c];
        //system.assertEquals(totalToKeep, listAfter.size()); 
  }
    /**/
    //TEST: purge is OK
    static testMethod void test_OK_PurgeMessages() {
      //Set RetentionHistory
      Integer retHist = 100;
      CS04_MYR_Settings__c setting = [SELECT Id, Myr_Customer_Message_PurgeRetention__c FROM CS04_MYR_Settings__c];
      setting.Myr_Customer_Message_PurgeRetention__c = retHist;
      update setting;
      test_purge_with_different_retentions(retHist);
    }
    
    //TEST: purge is OK without specifying the deleted message retention (by default=30)
    static testMethod void test_OK_PurgeMessagesDefaultRetention() {
      CS04_MYR_Settings__c setting = [SELECT Id, Myr_Customer_Message_PurgeRetention__c FROM CS04_MYR_Settings__c];
      setting.Myr_Customer_Message_PurgeRetention__c = null; //YES, it's exactly what I want to do !!!!
      update setting;
      test_purge_with_different_retentions(30); //should be 30 as it is hardcoded in the batch in case the setting is null
    }
	
    
    //TEST: fatal error with query locator (bad query)
    static testMethod void test_KO_BadQuery() {
        Myr_Batch_CustMessage_Purge_BAT batch = new Myr_Batch_CustMessage_Purge_BAT();
        //push a bad query to provoke the crash
        batch.m_query = 'TRUC';
        Boolean crash = false;
        try {
          Test.startTest();
          Database.executeBatch(batch, 200);
          Test.stopTest(); 
        } catch (Myr_Batch_CustMessage_Purge_BAT.Myr_Batch_CustMessage_Purge_BAT_Exception e) {
		  system.debug( '### Myr_Batch_CustMessage_Purge_BAT_TEST - <test_KO_BadQuery> exception = ' + e.getMessage());
          system.AssertEquals(true, e.getMessage().containsIgnoreCase(Myr_Batch_CustMessage_Purge_BAT.class.getName()+': unable to initialize the query locator='));
          crash = true;
        }
        system.AssertEquals(true, crash);
    }
  
	 //TEST: fatal error with query locator (bad query)
    static testMethod void test_KO_FailLogInStart() {
        Myr_Batch_CustMessage_Purge_BAT batch = new Myr_Batch_CustMessage_Purge_BAT();
		batch.TestFailureSimulation = Myr_Batch_CustMessage_Purge_BAT.TEST_FAILURE.FAIL_START_TEST;
        Boolean crash = false;
        try {
          Test.startTest();
          Database.executeBatch(batch, 200);
          Test.stopTest(); 
        } catch (Myr_Batch_CustMessage_Purge_BAT.Myr_Batch_CustMessage_Purge_BAT_Exception e) {
          crash = false;
        }
		//the crash simulation on the log insertion in the start method does not provoke the crash of the batch
        system.AssertEquals(false, crash);
	}
	
}