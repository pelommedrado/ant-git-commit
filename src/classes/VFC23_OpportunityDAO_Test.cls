/**
    Class   -   VFC23_OpportunityDAO_Test
    Author  -   RameshPrabu
    Date    -   31/10/2012
    
    #01 <RameshPrabu> <31/10/2012>
        Created this class using test for VFC23_OpportunityDAO.
**/
@isTest 
private class VFC23_OpportunityDAO_Test {

    public static Opportunity opty;
    public static Account acc;
    public static TDV_TestDrive__c td;

    public static void setup(){

        opty = new Opportunity();
        opty.StageName = 'Identified';
        opty.CloseDate = System.today();
        opty.Name = 'opp Test';
        Insert opty;

        acc = new Account();
        acc.FirstName = 'test';
        acc.LastName = 'Sobrenome';
        acc.PersEmailAddress__c = 'test@teste.com';
        acc.RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc');
        acc.PersMobPhone__c = '45214524';
        acc.PersLandline__c = '45214578';
        Insert acc;

        td = new TDV_TestDrive__c();
        Insert td;

    }

    static testMethod void VFC23_OpportunityDAO_Test() {
        // TO DO: implement unit test
        List<Account> lstNWSiteAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        List<Opportunity> lstOpp = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToOpportunity(lstNWSiteAccounts);
        Test.startTest();
        Opportunity Opp = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(lstOpp[0].Id);
        Test.stopTest();
        System.debug('Oppp...' +Opp);
        System.assert(Opp != null);
        List<Opportunity> lstOps = VFC23_OpportunityDAO.getInstance().fetchListOpportunityUsingAccountId(lstOpp[0].AccountId);
        
        List<Opportunity> oppListBySet = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityIds(new Set<Id>{lstOpp[0].Id});
        
        // List<Opportunity> fetchListOpportunityUsingAccountAndDealer( Id accountId, Id dealerId )
        
        List<Opportunity> lstOppUsingCustomerAndDealer = VFC23_OpportunityDAO.getInstance().fetchListOpportunityUsingAccountAndDealer(lstOpp[2].AccountId, lstOpp[2].Dealer__c);
        
        //List<Opportunity> fetchOpportunity_UsingCustomerID( Set<Id> customerIDs )
        Set<Id> setCustomerIDs = new Set<Id>();
        Set<Id> setDealerIDs = new Set<Id>();
        
        for( Opportunity opportu : lstOpp ){
            if( opportu.AccountId != null ){
                setCustomerIDs.add( opportu.AccountId );
            }
            if( opportu.Dealer__c != null ){
                setDealerIDs.add( opportu.Dealer__c );
            }
        }
        List<Opportunity> lstOppUsingCustomers = VFC23_OpportunityDAO.getInstance().fetchOpportunity_UsingCustomerID(setCustomerIDs);
        //List<Opportunity> fetchListOpportunityUsingDealer(Set<Id> setDealerId ){
        
        List<Opportunity> lstOppUsingDealerIds = VFC23_OpportunityDAO.getInstance().fetchListOpportunityUsingDealer(setDealerIDs);
        
        
    }

    public static testMethod void testUpdateData(){

        setup();

        Test.startTest();

        VFC23_OpportunityDAO.getInstance().updateData(opty,acc);

        Test.stopTest();

    }

    public static testMethod void testUpdateData2(){

        setup();

        Test.startTest();

        VFC23_OpportunityDAO.getInstance().updateData(opty,td);

        Test.stopTest();

    }
}