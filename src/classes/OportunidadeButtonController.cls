public class OportunidadeButtonController extends OportunidadeHistoricoAbstract
  implements OportunidadeHistoricoListener {
  private String oppId;
  private Boolean buttonRet;
  private static final String recordTypeTransaction
    = Utils.getRecordTypeId('TST_Transaction__c', 'SFA');
  private static final String transactionStatusNew 	= 'New';

  public OportunidadeButtonController(ApexPages.StandardController ctrl) {
    init(ctrl.getId());
    this.oppId = ctrl.getId();
    this.buttonRet = false;
  }

  public void oportunidadeExpirou() {
    ApexPages.addMessage(new ApexPages.Message(
      ApexPages.Severity.WARNING,
      'Oportunidade fora do período de retorno!'));
  }

  public void oportunidadeAceita() {
    String tipo = buttonRet ? 'Return' : 'Treatment';
    registrarTransactionOpp('Created', tipo);
  }

  private void registrarTransactionOpp(String action, String tipo) {
    System.debug('registrarTransactionOpp()');
    final TST_Transaction__c trans = createTransactionSfa(action, tipo);
    if(isTransactionType(trans, 'Treatment')) {
      final List<TST_Transaction__c> transList = obterTransactionTreatment();
      if(transList.isEmpty()) {
        System.debug('INSERT TRANS ' + trans);
        INSERT trans;
        return;
      }
      final TST_Transaction__c transUpdate = transList.get(0);
      updateTreatmentCount(transUpdate);
      UPDATE transUpdate;
    } else if(isTransactionType(trans, 'Return')) {
      INSERT trans;
      this.opp.DateTimeSellerUpdatedStageOpp__c = System.now();
      this.opp.return__c = true;
      UPDATE this.opp;
    }
  }

  private TST_Transaction__c createTransactionSfa(String action, String type) {
    final TST_Transaction__c trans = new TST_Transaction__c();
    trans.RecordTypeId         = recordTypeTransaction;
    trans.Type__c	             = type;
    trans.Action__c	           = action;
    trans.TransactionStatus__c = transactionStatusNew;
    trans.ReturnTreatmentStatus__c = this.opp.StageName;
    trans.Account__c           = this.opp.AccountId;
    trans.Opportunity__c       = this.opp.Id;
    trans.Treatment_Count__c   = 1;
    if(action == 'Updated') {
      trans.Reason__c = 'Reopening';
    }
    return trans;
  }

  private Boolean isTransactionType(TST_Transaction__c trans, String type) {
    return trans.Type__c.equalsIgnoreCase(type);
  }

  private List<TST_Transaction__c> obterTransactionTreatment() {
    final List<TST_Transaction__c> transList = [
      SELECT Id, Treatment_Count__c FROM TST_Transaction__c
      WHERE RecordTypeId =:recordTypeTransaction AND TransactionStatus__c =:transactionStatusNew
        AND Opportunity__c =: this.opp.Id AND Type__c = 'Treatment'
      ORDER BY CreatedDate DESC LIMIT 1
    ];
    return transList;
  }

  private void updateTreatmentCount(TST_Transaction__c transUpdate) {
    if(transUpdate.Treatment_Count__c == null) {
      transUpdate.Treatment_Count__c = 1;
    }
    transUpdate.Treatment_Count__c += 1;
  }

  public void oportunidadeReabrir() {
    final Integer intDays = countDaysLastModified();
    final String tipo = intDays != 0 ? 'Return' : 'Treatment';
    registrarTransactionOpp('Created', tipo);
    this.opp.DateTimeSellerUpdatedStageOpp__c = System.now();
    this.opp.StageName = 'Identified';
    UPDATE this.opp;
	}

  public void oportunidadeNaoReabre() {
    ApexPages.addMessage(new ApexPages.Message(
      ApexPages.Severity.WARNING,
      'Esta Oportunidade foi marcada como Perdida e não pode ser reaberta.'));
  }

  public void oportunidadeNaoReabreFaturada() {
    ApexPages.addMessage(new ApexPages.Message(
      ApexPages.Severity.WARNING,
      'A oportunidade já está faturada e não pode ser reaberta.'));
  }

  public PageReference actionButtonAtendimento() {
    buttonRet = false;
    if(!verificarOportunidade(this)) {
      return null;
    }
    return redirect();
  }

  public PageReference actionButtonReturn() {
    if(!acc.ReturnActiveToSFA__c) {
      ApexPages.addMessage(new ApexPages.Message(
        ApexPages.Severity.WARNING,
        'Retorno é o conceito de reaproveitamento de oportunidades abertas dentro de um período pré determinado pela sua gestão de vendas, mas não está ativado para a sua concessionária. Caso a equipe de vendas tenha interesse em utilizar essa funcionalidade, favor entrar em contato com a TI local.'));
      return null;
    }
    if(isRecepcionista()) {
      ApexPages.addMessage(new ApexPages.Message(
        ApexPages.Severity.WARNING,
        'Esta funcionalidade está disponível somente para vendedores. Clique em "Atender / Encaminhar Oportunidade"'));
      return null;
    }
    buttonRet = true;
    if(!verificarOportunidade(this)) {
        return null;
    }
    return redirect();
  }

  public PageReference actionDetalheOportunidade() {
    PageReference pageRef = new PageReference('/cac/' + this.oppId);
    return pageRef;
  }

  public PageReference actionNovaOportunidade() {
    PageReference pageRef = new PageReference('/PesquisaSfa2?conta=' + this.opp.AccountId);
    return pageRef;
  }
  
  private PageReference redirect() {
    PageReference pageRef = null;

    if(this.oppId == null || this.oppId == ''){
        return new PageReference('/apex/VFP03_SearchForAccountAndLead');
    }

    VFC53_OpportunityDestinationType opportunityDestination =
        VFC51_OppOrchestrationBusinessDelegate.getInstance().getOpportunityDestination(this.oppId);


    /*verifica o destino da oportunidade, ou seja, para qual página que deve ser direcionada*/
    if(opportunityDestination == VFC53_OpportunityDestinationType.QUALIFYING_ACCOUNT) {
        pageRef = new PageReference('/apex/ClienteOportunidadeSfa2?Id=' + this.oppId);

    }

    List<Quote> quoteList = Sfa2Utils.obterCotacao(this.oppId);
    String quoteId = null;

    if(!quoteList.isEmpty()) {
        quoteId = quoteList.get(0).Id;
    }

    if(opportunityDestination == VFC53_OpportunityDestinationType.TEST_DRIVE) {
        pageRef = new PageReference('/apex/ClienteTestDriveSfa2?Id=' + this.oppId + '&quoteId=' + quoteId);

    } else if(opportunityDestination == VFC53_OpportunityDestinationType.QUOTE) {
        pageRef = new PageReference('/apex/ClienteOportunidadeDetalheSfa2?Id=' + this.oppId);

    }

    return pageRef;
  }

  private Boolean isRecepcionista() {
    String profileName = [
      SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId() LIMIT 1
    ].Name;

    Set<String> permissionByUserList =
        new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());

    if(profileName == 'SFA - Receptionist'  ||
       (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess'))) {
       return true;
    }
    return false;
  }
}