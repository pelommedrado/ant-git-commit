/**
* Classe controladora da página de orçamento.
* @author Christian Ranha.
*         Elvia Serpa.
*/
public class VFC57_QuoteController 
{
        private String opportunityId;
        public List<Quote> orcamentos {get;set;}
        public VFC60_QuoteVO quoteVO {get;set;}
        public VFC60_QuoteVO quoteItemVO {get;set;}
        public VFC61_QuoteDetailsVO quoteDetailsVO {get;set;}
        private String idQuote;
        
        public VFC57_QuoteController (ApexPages.StandardController controller)
        {
                this.opportunityId = controller.getId();
                //this.opportunityId = '006M0000004wPYqIAM';
                this.quoteVO = new VFC60_QuoteVO();
        }

        public PageReference initialize()
        {
                this.orcamentos = null;
                System.debug('>>> this.opportunityId >>> ' + this.opportunityId);
                this.quoteVO = VFC63_QuoteBusinessDelegate.getInstance().getOpportunity(this.opportunityId);
                System.debug('>>> this.quoteVO >>> ' + this.quoteVO);
                this.buildPicklists();
                
                /* Monta a Grid Orçamento */
                quoteItemVO = VFC63_QuoteBusinessDelegate.getInstance().getQuote(this.opportunityId);
                
                return null;
        }
        
        /**
        * Constrói os picklists da tela.
        * Para o picklist de status, inicialmente só 2 opções estarão disponíveis: a primeira será o valor do campo Status e a segunda a opção Não Realizado.
        */
        private void buildPicklists()
        {
                /*cria os picklists que deverão ser exibidos assim que a tela for carregada*/
                this.quoteVO.lstSelOptionInterestVehicle = VFC49_PickListUtil.buildPickList();
        }
        
        /**
        * Trata a ação do botão cancelar oportunidade.
        * Constroi o picklist com as opções de cancelamento da oportunidade para que o pop-up possa ser aberto.
        */
        public void openPopUpCancellation()
        {
                this.quoteVO.reasonLossCancellationOpportunity = '';
                this.quoteVO.btnCancelOpportunityDisabled = true;        
                
                /*verifica se o picklist ainda não foi construído*/
                if(this.quoteVO.lstSelOptionReasonLossCancellationOpp == null) 
                {
                        /*cria o picklist que exibe as opções de cancelamento da oportunidade*/
                        this.quoteVO.lstSelOptionReasonLossCancellationOpp = VFC49_PickListUtil.buildPickList(Opportunity.ReasonLossCancellation__c, '');
                }
        }
        
        /**
        * Trata o evento onchange do campo razão de cancelamento da oportunidade (Pop-Up de cancelamento).
        */
        public void processReasonCancellationChange()
        {
                this.quoteVO.btnCancelOpportunityDisabled = String.isEmpty(this.quoteVO.reasonLossCancellationOpportunity);
        }       
        
        /**
        * Trata a ação do botão cancelar oportunidade.
        */
        public PageReference cancelOpportunity()
        {
                PageReference pageRef = null; 
                ApexPages.Message message = null;
                
                try 
                {
                        VFC63_QuoteBusinessDelegate.getInstance().cancelOpportunity(this.quoteVO);
                        
                        pageRef = new PageReference('/home/home.jsp'); 
                }
                catch(Exception ex)
                {
                        /*TODO: verificar tratamento dessa exceção*/
                        
                        message = new ApexPages.Message(ApexPages.Severity.Confirm, 'Não foi possível cancelar a oportunidade. Motivo: \n' + ex.getMessage());
                        
                        ApexPages.addMessage(message);
                }
                
                return pageRef;
        }
        /*
        public String getStatusQuote(){
                String status = '';             
                if((quoteVO.status == 'Billed'))                        
                {
                        status = 'Billed';
                        
                }else if(quoteVO.status == 'Pre Order Sent')
                {
                        status = 'Order';
                        
                }else
                {
                        for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes)
                        {
                                if(auxQuote.isCheckOpp == true)
                                {
                                        if(auxQuote.vehicle != null)
                                                status = 'Alocado';
                                        else
                                                status = 'Não alocado';                          
                                }
                        }       
                }
                return status;          
        }
        */
        
        public boolean getIsReadOnly(){
                return (quoteVO.status == 'Lost') || (quoteVO.status == 'Billed') || (quoteVO.status == 'Order');
        }
        
        public void sync()
        {
            synchronizeQuote(true);
        }
        
        /**
        * 
        */
        public PageReference synchronizeQuote(Boolean showError)
        {
            String strMessage = null;
            ApexPages.Message message = null;
            Boolean onceWasChecked = false;
                            
            /* Atualiza o campo:SyncedQuoteId da oportunidade com o Id da cotação. Este processo ativa a sincronização */
            for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes)
            {
                if(auxQuote.isCheckOpp == true)
                {
                    onceWasChecked = true;
                    
                    try
                    {
                        VFC63_QuoteBusinessDelegate.getInstance().synchronizationOpportunity(opportunityId, auxQuote.id);
                        this.quoteVO.status = VFC63_QuoteBusinessDelegate.getInstance().getOpportunity(this.opportunityId).status;
                        
                        strMessage = 'O orçamento foi sincronizado com a Oportunidade';  
                        message = new ApexPages.Message(ApexPages.Severity.CONFIRM, strMessage);
                        ApexPages.addMessage(message);                          
                    }
                    catch(DMLException ex)
                    {
                        strMessage = ex.getMessage();
                        message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                        ApexPages.addMessage(message);
                    }
                        
                }
            }       
            
            if((!onceWasChecked && showError))
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione um Orçamento para realizar a sincronização.'));                            
                    
            return null;            
        }
        
        public boolean hasSyncedQuote()
        {      
    	    boolean onceAtLeast = false;
            for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes)
            {
                if(auxQuote.isCheckOpp == true)
                        onceAtLeast = true;
            }
            return onceAtLeast;     
        }
        
        /**
        * 
        */
        public PageReference sendToRequest()
        { 
        	   Boolean isCheck = false;
               String strMessage = null;
               ApexPages.Message message = null;

               for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes)
               {
                    if(auxQuote.status == 'Canceled' && auxQuote.isCheckOpp == true)
                    {
                        strMessage = 'O Orçamento selecionado esta com o status Cancelado e não pode ser sincronizado.'; 
                        message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                        ApexPages.addMessage(message);  
                        return null;                            
                    }
           		}        	
        	      
                synchronizeQuote(false);
                    
                for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes)
                {
                    if(auxQuote.isCheckOpp == true)
                    {
                        idQuote = auxQuote.id;
                        isCheck = true;
                        
                        if(auxQuote.VehicleRule == '' || auxQuote.VehicleRule == null)
                        {
                            strMessage = 'Existem veículos sem chassi definido no orçamento. Favor cadastrar estes chassis antes de gerar o pedido.'; 
                            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                            ApexPages.addMessage(message);  
                            return null;                            
                        }
                    }
           		}

	            if(isCheck == false)        
	            {
                    strMessage = 'Selecione um orçamento para gerar a pré ordem.';       
                    message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                    ApexPages.addMessage(message);  
                    return null;    
	            }
            
		       PageReference pageRef = new PageReference('/apex/VFP15_SendToRequest?Id='+idQuote);
		       pageRef.setRedirect(true);
		       return pageRef; 
        }
        
        /**
        * 
        */
        public PageReference newQuote()
        {       
           PageReference pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+this.opportunityId+'&idCotacao='+idQuote);
           pageRef.setRedirect(true);
           return pageRef; 
        }       
}