public class BcsParsingEngine{

public List<BcsReponseObject> parseXML(String xml) 
{
Integer start=xml.indexOf('<response>'); 
Integer endIndex=xml.indexOf('</response>');  
String formattedxml=xml.SubString(start,endIndex+11);    
List<BcsReponseObject> bcsxmlDataList=new List<BcsReponseObject>();

Integer count=1;

Dom.Document doc = new Dom.Document();
 doc.load(formattedxml);
 Dom.XMLNode rootElement = doc.getRootElement();


for (Dom.XmlNode assets : rootElement.getChildElements()) {
  
  

if(assets.getname()=='clientList')
{
  
   System.debug('Parsing the '+count+'Client >>>>>>');
   
   count++;
   
   System.debug('assets.getChildElements()>>>>>>>>>>>'+assets.getChildElements());
   

   BcsReponseObject bcsxmlData=new BcsReponseObject();
  
for (Dom.XmlNode child : assets.getChildElements()) {
  

  if (child.getname() == 'idClient') {
                          bcsxmlData.IdClient=child.getText();
                        } 
                         if (child.getname() == 'lastName') {
                         
                        bcsxmlData.LastName=child.getText();
                        
                        } 
                         if (child.getname() == 'title') {
                        
                        bcsxmlData.Title=child.getText();
                        
                        } 
                         if (child.getname() == 'lang') {
                        
                             
                        bcsxmlData.Lang=child.getText();
                        
                        } 
                         if (child.getname() == 'firstName') {
                        
                       
                             
                        bcsxmlData.FirstName=child.getText();
                        
                        } 
                      
                      
                      ///BCScommAgreement List
                        if (child.getname() == 'BCScommAgreement') {
                        
                       
                          for (dom.XmlNode BCScommAgrechild: child.getchildren() ) {
                          
                              
                        if (BCScommAgrechild.getname() == 'Global.Comm.Agreement') {
                        
                         
                            
                             bcsxmlData.GlobalCommAgreement=BCScommAgrechild.getText();
                            
                        }
                       if (BCScommAgrechild.getname() == 'Post.Comm.Agreement') {
                        
                            system.debug('Posttttttttt'+BCScommAgrechild.getText());
                             bcsxmlData.PostCommAgreement=BCScommAgrechild.getText();
                        }
                          if (BCScommAgrechild.getname() == 'Tel.Comm.Agreement') {
                        
                            system.debug('Telllllllllll'+BCScommAgrechild.getText());
                             bcsxmlData.TelCommAgreemen=BCScommAgrechild.getText();
                        }
                          if (BCScommAgrechild.getname() == 'SMS.Comm.Agreement') {
                        
                            system.debug('SMSsssssssssssssssss'+BCScommAgrechild.getText());
                             bcsxmlData.SMSCommAgreement=BCScommAgrechild.getText();
                        }
                          if (BCScommAgrechild.getname() == 'Fax.Comm.Agreement') {
                        
                            system.debug('Faxxxxxxxxxxxxxxxxxxxxxx'+BCScommAgrechild.getText());
                             bcsxmlData.FaxCommAgreement=BCScommAgrechild.getText();
                        }
                        if (BCScommAgrechild.getname() == 'Email.Comm.Agreement') {
                            bcsxmlData.EmailCommAgreement=BCScommAgrechild.getText();
                        }
                        
                        } 
                      
        
                            
                      }
                      
                      
                      
                      
                  if (child.getname() == 'contact') {
                       
                      
                      
                          for (dom.XmlNode contactchild: child.getchildren() ) {
                       
                       if (contactchild.getname() == 'phoneNum1') {
                            bcsxmlData.phoneNum1=contactchild.getText();
                        }
                        if (contactchild.getname() == 'phoneNum2') {
                            bcsxmlData.phoneNum2=contactchild.getText();
                        }
                        if (contactchild.getname() == 'phoneNum3') {
                            bcsxmlData.phoneNum3=contactchild.getText();
                        }   
  
                        if (contactchild.getname() == 'email') {
                            bcsxmlData.email=contactchild.getText();
                        }     
                       
                     }
 
                       
                       }

                          if (child.getname() == 'address') {
                          
                          

                       
                       for (dom.XmlNode addresschild: child.getchildren() ) {
                       
                       if (addresschild.getname() == 'strName') {
                        
                            system.debug('strNameeeeeee'+addresschild.getText());
                            bcsxmlData.StrName=addresschild.getText();
                        }
                        if (addresschild.getname() == 'compl2') {
                        
                            system.debug('compl2222222222'+addresschild.getText());
                            bcsxmlData.Compl2=addresschild.getText();
                        }
                        if (addresschild.getname() == 'countryCode') {
                        
                            system.debug('countryCodeeeeeeeeee'+addresschild.getText());
                            bcsxmlData.CountryCode=addresschild.getText();
                        } 
                        if (addresschild.getname() == 'zip') {
                        
                            system.debug('zippppppppppp'+addresschild.getText());
                            bcsxmlData.Zip=addresschild.getText();
                        }  
                        if (addresschild.getname() == 'city') {
                        
                            system.debug('cityyyyyyyyyyyyyy'+addresschild.getText());
                            bcsxmlData.City=addresschild.getText();
                        }              
       
                   }
                           
                 }
                        //typeperson
                         if (child.getname() == 'typeperson') {
                          system.debug('typepersonnnnnn'+child.getText());
                            bcsxmlData.typeperson=child.getText();
                          
                         }

                        //vehicleList


                        if (child.getname() == 'vehicleList') {
                        
                    
                            
                       
                       for (dom.XmlNode vehicleListchild: child.getchildren() ) {
                       
                       if (vehicleListchild.getname() == 'vin') {
                        
                            system.debug('vinnnnnnnnnn'+vehicleListchild.getText());
                              bcsxmlData.vin=vehicleListchild.getText();
                            
                        }
                        if (vehicleListchild.getname() == 'brandCode') {
                        
                            system.debug('brandCodeeeeeeeeee'+vehicleListchild.getText());
                              bcsxmlData.brandCode=vehicleListchild.getText();
                        }
                        if (vehicleListchild.getname() == 'modelCode') {
                        
                            system.debug('modelCodeeeeeeeeeee'+vehicleListchild.getText());
                              bcsxmlData.modelCode=vehicleListchild.getText();
                        } 
                        if (vehicleListchild.getname() == 'modelLabel') {
                        
                            system.debug('modelLabellllllllllll'+vehicleListchild.getText());
                              bcsxmlData.modelLabel=vehicleListchild.getText();
                        }  
                        if (vehicleListchild.getname() == 'versionLabel') {
                        
                            system.debug('versionLabelllllllllllll'+vehicleListchild.getText());
                              bcsxmlData.versionLabel=vehicleListchild.getText();
                        }     
                         if (vehicleListchild.getname() == 'firstRegistrationDate') {
                        
                            system.debug('firstRegistrationDateeeeeeeeeeeeee'+vehicleListchild.getText());
                              bcsxmlData.firstRegistrationDate=vehicleListchild.getText();
                        }  
                         if (vehicleListchild.getname() == 'registration') {
                        
                            system.debug('registrationnnnnnnnnnnnn'+vehicleListchild.getText());
                              bcsxmlData.registration=vehicleListchild.getText();
                        }    
                         if (vehicleListchild.getname() == 'possessionBegin') {
                        
                            system.debug('possessionBeginnnnnnnnnnnnnnnnn'+vehicleListchild.getText());
                              bcsxmlData.possessionBegin=vehicleListchild.getText();
                        } 


                        }
                        }

                                  
       
       
                       

}
bcsxmlDataList.add(bcsxmlData);
}
}
return bcsxmlDataList;
}
}