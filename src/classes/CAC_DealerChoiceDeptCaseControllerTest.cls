@isTest
public class CAC_DealerChoiceDeptCaseControllerTest {
	

    
    static testMethod void teste1(){
        CAC_DealerChoiceDeptCaseController controller = new CAC_DealerChoiceDeptCaseController();
        
        Case caso = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_Financing_CreditRecovery'));
        
        ApexPages.CurrentPage().getparameters().put('id', caso.id);
        ApexPages.StandardController sc = new ApexPages.standardController(caso);
        CAC_DealerChoiceDeptCaseController sic = new CAC_DealerChoiceDeptCaseController(sc);
        sic.newCase();
        sic.getAvailableRecordType();
        sic.setTypeCaseSelect('');
        sic.newValuesToFilter();
        sic.setTypeCaseSelect(Utils.getRecordTypeId('Case', 'Cacweb_Financing_CreditRecovery'));
        sic.newValuesToFilter();
        sic.getTypeCaseSelect();
        
        
        sic.directionCase = 'Financial';
        sic.directionCa();
        sic.directionCase = null;
        sic.directionCa();
    
    	sic.departmentCase = 'Crédito e Cobrança';
        sic.departmentCa();
        sic.directionCase = 'Financial';   
        sic.departmentCase = null;
        sic.departmentCa();
    }
    
    
    private static Case returnNewCase(Id recordType){
        Case cas = new Case(
            Status = 'new',
            CAC_Direction__c= 'Financial',
            CAC_Department__c = 'Crédito e Cobrança',
            Invoice_Value__c = 123,
			documentNumber__c = '12345678912345678909',
            Subject__c = 'Solicitação de Documentos - ND Vendas Diretas Cobrança',
            Description = 'teste',
            Dealer_Contact_Name__c = 'teste',
            Contact_Phone__c  = '1122334455',
            Reference__c = 'teste',
            Order_Number__c = '1234567',
            RecordTypeId = recordType
        );
        Database.insert(cas,true);
        return cas;
    }
    
}