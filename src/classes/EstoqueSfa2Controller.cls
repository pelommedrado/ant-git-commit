public with sharing class EstoqueSfa2Controller extends EstoqueAbstractController {

    public Boolean iniciarNeg { get; set; }
    
    /**
     * Contrutor da classe.
     */
    public EstoqueSfa2Controller () {
        Account acc = Sfa2Utils.obterContaUserComunidade();
        
        this.iniciarNeg = acc.Sell_from_stock__c;   
        this.vehicleList = new List<EstoqueAbstractController.ObjVeiculo>();
    }
}