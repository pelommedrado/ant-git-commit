@IsTest
private class DashboardAtividadeControllerTest {

    private static User userManager 	= null;
    private static Account accDealer 	= null;

    private static Task task 		= null;
    private static Event event 		= null;
    private static Opportunity opp 	= null;

    static {
        Test.startTest();

        User usr = [Select id from User where Id = :UserInfo.getUserId()];

  			//necessario usuario possuir papel
  			UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
  			usr.UserRoleId = r.Id;
  			Update usr;
        System.RunAs(usr) {

            //criar conta Dealer
            accDealer = AccountBuild.getInstance()
                .createAccountDealer();
            INSERT accDealer;

            //criar contato para a conta Dealer
            final Contact ctt = ContactBuild.getInstance()
                .createContact(accDealer.Id);
            INSERT ctt;

            //criar um usuario
            final Id profileId = Utils.getProfileId('BR - Renault + Cliente Manager');
            userManager = UserBuild.getInstance()
                .createUser(ctt.Id, profileId);
            INSERT userManager;

            final Account accPerson = AccountBuild.getInstance()
                .createAccountPersoal();
            INSERT accPerson;

            opp = OpportunityBuild.getInstance()
                .createOpportunity(userManager.Id, accPerson.Id, accDealer.Id);
            INSERT opp;

            task = TaskBuild.getInstance()
                .createTask(userManager.Id, opp.id);
            INSERT task;

            event = EventBuild.getInstance()
                .createEvent(userManager.Id, opp.id);
            //INSERT event;
        }
        Test.stopTest();
    }

    static testMethod void deveCriarControllerPorTask() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
    }
    static testMethod void deveChangeStatusTaskCanceled() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
   		ctl.act.Status = 'Canceled';
        ctl.changeStatus();
    }
    static testMethod void deveChangeStatusTaskExtended() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Extended';
       	ctl.changeStatus();
    }
    static testMethod void deveChangeStatusTaskCompletedConfirm() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Completed';
        ctl.act.Subject = 'Confirm Test Drive';
        ctl.changeStatus();
    }
    static testMethod void deveChangeStatusTaskCompletedPersecution() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Completed';
        ctl.act.Subject = 'Persecution';
        ctl.changeStatus();
    }
    static testMethod void deveSelecionarTemperatura() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        System.currentPageReference().getParameters().put('temp', 'hot');
        ctl.selectTemperatura();
    }
    static testMethod void deveSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.save();
    }
    static testMethod void deveRetornoErroDadosSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.acc.FirstName = '';
        ctl.save();
    }
    static testMethod void deveRetornoErroDataFuturaSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Extended';
        ctl.act.activityDate = '12/02/2017';
        ctl.act.activityHour = '12:45';
        ctl.save();
    }
    static testMethod void deveRetornoErroNextDataNullSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Completed';
        ctl.act.Subject = 'Contact';
        ctl.nextAct.activityDate = '';
        ctl.nextAct.activityHour = '';
        ctl.save();
    }
    static testMethod void deveRetornoErroNextDataFuturaSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.nextAct.Status = 'Extended';
        ctl.nextAct.activityDate = '12/02/2017';
        ctl.nextAct.activityHour = '12:45';
        ctl.save();
    }
    static testMethod void deveRetornoErroStatuCancelarSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.act.Status = 'Canceled';
        ctl.save();
    }
    static testMethod void deveRetornoErroDadosContatoSalvaAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.acc.PersMobPhone__c = '';
        ctl.acc.PersLandline__c = '';
        ctl.acc.PersEmailAddress__c = '';
        ctl.save();
    }
    static testMethod void deveCancelarAtividade() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.cancelar();
    }
    static testMethod void deveObterMotivoTestDrive() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        List<SelectOption> selList = ctl.motivoTestDrive;
    }
    static testMethod void deveObterStatusNextList() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        List<SelectOption> selList = ctl.statusNextList;
    }
    static testMethod void deveObterStatusList() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        List<SelectOption> selList = ctl.statusList;
    }
    static testMethod void deveObterOptionConfirm() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        List<SelectOption> selList = ctl.optionConfirm;
    }
    static testMethod void deveObterMotivoFaltou() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        List<SelectOption> selList = ctl.motivoFaltou;
    }
    static testMethod void deveChangeNextStatus() {
        System.currentPageReference().getParameters().put('actId', task.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
        ctl.nextAct.subject = 'Confirm Test Drive';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Perform Test Drive';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Confirm Visit';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Perform Visit';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Customer Service';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Persecution';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Rescue';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Delivery Confirmation';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Contact';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Negotiation';
        ctl.chanceNextStatus();
        ctl.nextAct.subject = 'Pre-Order';
        ctl.chanceNextStatus();
    }
    static testMethod void deveCriarControllerPorOpp() {
        System.currentPageReference().getParameters().put('oppId', opp.Id);
        DashboardAtividadeController ctl = new DashboardAtividadeController();
    }
}