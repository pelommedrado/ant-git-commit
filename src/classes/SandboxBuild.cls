public with sharing class SandboxBuild {

	public static void createData(){

		Id dealerRecordTypeId = Utils.getRecordTypeId('Account','Network_Site_Acc');
		Id contactRecordTypeId = Utils.getRecordTypeId('Contact','Network_Ctc');
		Id gerenteProfileId = Utils.getProfileId('BR - Renault + Cliente Manager');
		Id sellerProfileId = Utils.getProfileId('BR - Renault + Cliente');

		Id parentId = criaConcessionariaGrupo(dealerRecordTypeId);

		Account dealer = criaConcessionaria(parentId, dealerRecordTypeId);

		Contact vendedor = criaContatoConcessionaria(dealer.Id, contactRecordTypeId, 'Vendedor');
		criaUsuarioComunidade(vendedor.Id, dealer.IDBIR__c, 'Vendedor', sellerProfileId, 'BR_SFA_Seller');

		Contact gerente = criaContatoConcessionaria(dealer.Id, contactRecordTypeId, 'Gerente');
		criaUsuarioComunidade(gerente.Id, dealer.IDBIR__c, 'Gerente', gerenteProfileId, 'BR_SFA_Dealer_Manager');

		Contact gerenteGrupo = criaContatoConcessionaria(dealer.Id, contactRecordTypeId, 'Gerente Grupo');
		criaUsuarioComunidade(gerenteGrupo.Id, dealer.IDBIR__c, 'Gerente Grupo', gerenteProfileId,'BR_SFA_Dealer_Executive');

		List<VEH_Veh__c> veiculoList = criarVeiculos();

		criarRelacaoPosse(dealer.Id, veiculoList);
        
        List<User> userToInsert = new List<User>();
        userToInsert.add(criaUsuariosAdmin('Edvaldo','Fernandes','edvaldo@kolekto.com.br'));
        userToInsert.add(criaUsuariosAdmin('Lucas','Andrade','landrade@kolekto.com.br'));
        Insert userToInsert;

	}
    
    public static User criaUsuariosAdmin(String firstName, String lastName, String email){
        
        String nameSandbox = UserInfo.getUserName().substringAfterLast('.');
        
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Username = email + '.' + nameSandbox,
            Email = email,
            Alias = firstName.substring(0,3),
            RecordDefaultCountry__c = 'Brazil',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            TimeZoneSidKey='America/Sao_Paulo',
            ProfileId = Utils.getSystemAdminProfileId()
        );
        
        Database.DMLOptions dmlo = new Database.DMLOptions();
        dmlo.EmailHeader.triggerUserEmail = true;
        dmlo.EmailHeader.triggerAutoResponseEmail= true;
        u.setOptions(dmlo);
        
        return u;
        
    }

	public static void criarRelacaoPosse(Id accountId, List<VEH_Veh__c> veiculoList){

		List<VRE_VehRel__c> veiculoRelList = new List<VRE_VehRel__c>();

		for(Integer i=0; i < 10; i++){

			VRE_VehRel__c veiculoRel = new VRE_VehRel__c(
				Account__c = accountId,
				VIN__c = veiculoList[i].Id
			);

			veiculoRelList.add(veiculoRel);

		}

		Insert veiculoRelList;

	}

	public static List<VEH_Veh__c> criarVeiculos(){

		List<VEH_Veh__c> veiculoList = new List<VEH_Veh__c>();
		List<String> modelos = new List<String>{'CLIO','SANDERO','DUSTER','LOGAN','CAPTUR','FLUENCE','MASTER','KANGOO','CLIO','SANDERO'};

		for(Integer i=0; i < 10; i++){

			VEH_Veh__c veiculo = new VEH_Veh__c(
				Name = '3425GSVFAGS2346' +  String.valueOf( 37 + i ),
				Version__c = 'Authentic 1.0',
				Price__c = 35000,
				Model__c = modelos[i],
				Is_Available__c = true,
				Status__c = 'Stock'
			);

			veiculoList.add(veiculo);

		}

		Insert veiculoList;

		return veiculoList;

	}

	public static Id obterConjPermissao(String name){

		List<PermissionSet> conjPerms = [SELECT Id FROM PermissionSet WHERE Name =: name];

		Id conjPermsId = conjPerms.get(0).Id != null ? conjPerms.get(0).Id : null;

		return conjPermsId;

	}

	public static void criarPermissao(Id userId, Id permSetId){

		PermissionSetAssignment psa = new PermissionSetAssignment(
			AssigneeId = userId,
			PermissionSetId = permSetId
		);
		Insert psa;

	}

	public static Id criaConcessionariaGrupo(Id dealerRecordTypeId){

		Account dealer = new Account(
			Name = 'Concessionaria Grupo',
			RecordTypeId = dealerRecordTypeId,
			IDBIR__c = '123451'
		);

		Insert dealer;

		return dealer.Id;

	}

	public static Account criaConcessionaria(Id parentId, Id dealerRecordTypeId){

		Account dealer = new Account(
			Name = 'Concessionaria',
			RecordTypeId = dealerRecordTypeId,
			IDBIR__c = '12341',
			ParentId = parentId,
			NameZone__c = 'R1'
		);

		Insert dealer;

		return dealer;

	}

	public static Contact criaContatoConcessionaria(Id accountId, Id contactRecordTypeId, String firstName){

		Contact dealerContact = new Contact(
			FirstName = firstName,
			LastName = 'Teste ' ,
			RecordTypeId = contactRecordTypeId,
			AccountId = accountId
		);

		Insert dealerContact;

		return dealerContact;

	}

	@Future
	public static void criaUsuarioComunidade(Id contactId, String bir, String firstName, Id profileId, String conjPerm){

		System.debug('firstName: '+firstName);

		System.debug('firstName.substring(): ' + firstName.substring( firstName.length() - 4,firstName.length() ));


        User u = new User(
            FirstName = firstName,
            LastName = 'Test',
            Username = 'usuario'+ firstName.replace(' ','') +'@test.com',
            Email = 'usuario' + firstName.replace(' ','') + '@test.com',
            Alias = firstName.substring(firstName.length() - 4,firstName.length()),
            RecordDefaultCountry__c = 'Brazil',
            BIR__c = bir,
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            ProfileId = profileId,
            ContactId = contactId
        );

        Insert u;

		criarPermissao(u.Id, obterConjPermissao(conjPerm));
    }

}