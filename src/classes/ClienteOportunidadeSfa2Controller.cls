public class ClienteOportunidadeSfa2Controller extends AbstractOportunidadeController {
	private static final String optionNotVehicle = 'Não possui veículo';
	private List<String> brandsDetailList;
    private List<String> modelDetailList;
    private List<Campaign> lstCampaign;

    public Boolean cancelButtonDisabled { get;set; }
    public String selectedBrand { get;set; }
    public String selectedModel { get;set; }

    public VFC46_QualifyingAccountVO qualifyingAccountVO { get;set; }
    public VRE_VehRel__c veiculoRel { get; set; }

    public List<SelectOption> motivoList { get;set; }
    public LIst<SelectOption> motivoCancelList { get;set; }
    List<PVVersion__c> versions { get;set; }
    List<PVVersion__c> secversions { get;set; }

    public String motivoCancelamento { get;set; }

	/** Controller constructor */
    public ClienteOportunidadeSfa2Controller(ApexPages.Standardcontroller controller){
		this.initialize(controller.getId());

        String veiculoId = ApexPages.currentPage().getParameters().get('veh');
        this.veiculoRel = Sfa2Utils.obterVeiculo(veiculoId);

        this.motivoList =
            VFC49_PickListUtil.buildPickList(TDV_TestDrive__c.ReasonCancellation__c, '');
        this.motivoCancelList =
            VFC49_PickListUtil.buildPickList(Opportunity.ReasonLossCancellation__c, '');
    }

    private void initialize(String opportunityId) {
        System.debug('initialize() : ' + opportunityId);

        initAction(opportunityId);

        this.qualifyingAccountVO = VFC133_QualifyingAccountBOWOS.getInstance()
            .getAccountByOpportunityId(opportunityId);

        if(this.qualifyingAccountVO.sObjCampaign == null) {
            this.qualifyingAccountVO.sObjCampaign = new Campaign();
            this.qualifyingAccountVO.promoCode = 'Teste';
        }

        //set Brazil as default country
        qualifyingAccountVO.shippingCountry = 'Brasil';

        selectedBrand = qualifyingAccountVO.veiculoAtual;
        
        
        if(this.qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c != null) {
        //    selectedBrand = this.qualifyingAccountVO.vehicleInterestBRBrand;
            selectedModel = this.qualifyingAccountVO.vehicleInterestBRModel;
        }

        this.getBrandDetails();
        this.getModelDetails();
        this.getVersionsDetail();
        this.getSecVersionsDetail();
    }

    public void verifyRequiredFields(){

           if(String.isBlank(qualifyingAccountVO.vehicleInterestBR)) {

                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR,
                    'CPF Inválido!'));

               //return null;
        }
    }

    public void getVersionsDetail(){
        String src = 'SELECT Id, Name FROM PVVersion__c WHERE Model__r.Name LIKE \'%'
            + qualifyingAccountVO.vehicleInterestBR+'%\' ORDER BY Name';

        system.debug('query: ' + src);
        versions = Database.query(src);
    }

    public List<SelectOption> getVersions(){

        List<SelectOption> versionOption = new List<SelectOption>();
        versionOption.add(new SelectOption('', ''));

        if(!versions.isEmpty() && versions != null){
            for(PVVersion__c option : versions){
                versionOption.add(new SelectOption(option.id, option.Name));
            }
        }

        return versionOption;
    }

    public void getSecVersionsDetail(){
        String src = 'SELECT Id, Name FROM PVVersion__c WHERE Model__r.Name LIKE \'%'
            + qualifyingAccountVO.secondVehicleOfInterest + '%\' ORDER BY Name';

        system.debug('query: ' + src);
        secversions = Database.query(src);
    }

    public List<SelectOption> getSecVersions(){

        List<SelectOption> versionOption = new List<SelectOption>();
        versionOption.add(new SelectOption('', ''));

        if(!secversions.isEmpty() && secversions != null){
            for(PVVersion__c option : secversions){
                versionOption.add(new SelectOption(option.id, option.Name));
            }
        }

        return versionOption;
    }

    public PageReference performTestDrive() {
        if(saveOpportunity() == null) {
            return null;
        }
        List<MLC_Molicar__c> lstMolicarDetId = null;
        PageReference pageRef = null;
        ApexPages.Message message = null;

        try {
            lstMolicarDetId = VFC11_MolicarDAO.getInstance().
                findMolicarByBrandAndModel(selectedBrand, selectedModel);
                
                System.debug('@@@@ lstMolicarDetId: ' + lstMolicarDetId);

            if(!lstMolicarDetID.isEmpty()) {
                qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c = lstMolicarDetID[0].Id;
            }

            VFC48_QualifyingAccountBusinessDelegate.getInstance().
                updateDataForTestDriveStage(this.qualifyingAccountVO);

            String quoteId = null;
            Quote temp = obterQuoteOpp(this.qualifyingAccountVO.opportunityId);
            if(temp == null) {
                VFC61_QuoteDetailsVO orc = Sfa2Utils.criarOrcamento(
                    this.qualifyingAccountVO.opportunityId,
                    this.veiculoRel);
                quoteId = orc.Id;

            } else {
                quoteId = temp.Id;

            }

            pageRef = new PageReference('/apex/ClienteTestDriveSfa2?Id=' +
                                        this.qualifyingAccountVO.opportunityId + '&quoteId=' + quoteId);

        } catch(Exception e) {
            message = new ApexPages.Message(
                ApexPages.Severity.Confirm,
                'Erro ao salvar os dados: ' + e.getMessage());

            ApexPages.addMessage(message);
        }

        return pageRef;
    }

    public PageReference saveOpportunity() {
		if(!isValidCPF()) {
            return null;
        }

        /* Medida provisória - retirada a obrigatoriedade da pagina até que a Captur venha versões da AOC

        if(String.isBlank(qualifyingAccountVO.vehicleInterestBR)
          || String.isBlank(qualifyingAccountVO.versionInterest)) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR,
                    'As informações de veículo e versão de interesse devem ser preenchidas!'));

            return null;
        }*/

        if(String.isBlank(qualifyingAccountVO.persMobPhone)) {
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR,
                'O campo celular é obrigatório!'));
            return null;
        }

				if(!isSelectedBrandValid()) {
					Apexpages.addMessage(new Apexpages.Message(
								ApexPages.Severity.ERROR,
								'As informações de veículo atual devem ser preenchidas!'));
					return null;
				}

        try {
            if(this.veiculoRel != null) {
                Sfa2Utils.criarOrcamento(this.qualifyingAccountVO.opportunityId,
                                         this.veiculoRel);
            }
            
            List<MLC_Molicar__c> lstMolicarDetId = VFC11_MolicarDAO.getInstance().
                findMolicarByBrandAndModel(selectedBrand, selectedModel);
                
            System.debug('@@@@ lstMolicarDetId: ' + lstMolicarDetId);

            if(!lstMolicarDetID.isEmpty()) {
                qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c = lstMolicarDetID[0].Id;
            }
            
            qualifyingAccountVO.oppApproach = oppApproach();
            qualifyingAccountVO.veiculoAtual = this.selectedBrand;
            qualifyingAccountVO.versaoAtual = this.selectedModel;
            VFC48_QualifyingAccountBusinessDelegate.getInstance().updateData(this.qualifyingAccountVO);

            return redirect();

        } catch(Exception ex) {
            System.debug(ex);
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Erro ao salvar os dados: ' + ex.getMessage()));
        }

        return null;
    }

		private Boolean isSelectedBrandValid() {
			if(String.isBlank(selectedBrand)) {
				return false;
			}
			//TODO ainda falta adicionar a regra de validacao completa.
			//TODO Falta informacao referente a regra de negocio.
			/*if(String.isBlank(selectedBrand)
				|| String.isBlank(selectedModel)
				|| String.isBlank(qualifyingAccountVO.yrReturnVehicleBR)) {
							Apexpages.addMessage(new Apexpages.Message(
									ApexPages.Severity.ERROR,
									'As informações de veículo atual devem ser preenchidas!'));

					return null;
			}*/
			if(!selectedBrand.equalsIgnoreCase(optionNotVehicle)
				&& String.isBlank(selectedModel)) {
				return false;
			}
			return true;
		}

    public Boolean isValidCPF() {
        final String cpf = qualifyingAccountVO.numCL;
        final List<Opportunity> oppList = [
            SELECT Id, SourceMedia__c FROM Opportunity
            WHERE Id =: qualifyingAccountVO.opportunityId
        ];

        if(oppList.isEmpty()) {
            return false;
        }
        final Opportunity opp = oppList.get(0);
        //Opportunity opp = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(qualifyingAccountVO.opportunityId);

        //if(!String.isEmpty(opp.SourceMedia__c) && opp.SourceMedia__c.equalsIgnoreCase('Telephone')) {
            if(String.isEmpty(cpf)) {
                return true;
            }
        //}

        if(Sfa2Utils.isCpfValido(cpf)) {
            return true;
        }
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.Error, 'CPF inválido') );

        return false;
    }

    private String oppApproach() {
        return this.selectedBrand.equalsIgnoreCase('RENAULT') ?
            'Loyalty' : 'Acquisition';
    }

    private PageReference redirect() {
        String page = ProfileUtils.isUserAgenda(UserInfo.getUserId()) ?
            '/DashboardTask?aba=quote' : '/' + this.qualifyingAccountVO.opportunityId;
        return new PageReference(page);
    }

    public PageReference gerarPreOrdem() {
        if(saveOpportunity() == null) {
            return null;
        }
        PageReference pageRef = null;
        String strMessage = null;
        ApexPages.Message message = null;

        if(!isValidCPF()) {
            return null;
        }

        /* Medida provisória - retirada a obrigatoriedade da pagina até que a Captur venha versões da AOC

        if(String.isBlank(qualifyingAccountVO.vehicleInterestBR)
          || String.isBlank(qualifyingAccountVO.versionInterest)) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR,
                    'As informações de veículo e versão de interesse devem ser preenchidas!'));

            return null;
        }*/

        if(String.isBlank(qualifyingAccountVO.persMobPhone)) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR,
                    'O campo celular é obrigatório!'));

            return null;
        }

        motivoCancelamento =
            ApexPages.currentPage().getParameters().get('motivo');

        System.debug('### ' + motivoCancelamento);

        if(String.isEmpty(motivoCancelamento)) {
            return pageRef;
        }

        try {
            String oppId = this.qualifyingAccountVO.opportunityId;
            Sfa2Utils.criarTestDrive(oppId, motivoCancelamento);

            String quoteId = null;
            Quote temp = obterQuoteOpp(oppId);
            if(temp == null) {
                VFC61_QuoteDetailsVO orc =
                    Sfa2Utils.criarOrcamento(oppId, this.veiculoRel);
                quoteId = orc.Id;

            } else {
                quoteId = temp.Id;

            }

            pageRef = new PageReference('/apex/ClientePreOrdemSfa2?Id='
                + quoteId + '&oppId=' + oppId);

        } catch(Exception ex) {
            strMessage = 'Não foi possível alterar a oportunidade para a fase de orçamento. Motivo: \n' + ex.getMessage();
            message = new ApexPages.Message(ApexPages.Severity.Confirm, strMessage);

            ApexPages.addMessage(message);
        }
        return pageRef;
    }

    public PageReference cancelOpportunity() {
        PageReference pageRef = null;
        ApexPages.Message message = null;

        motivoCancelamento =
            ApexPages.currentPage().getParameters().get('motivo');
        System.debug('### ' + motivoCancelamento);

        qualifyingAccountVO.reasonLossCancellation = motivoCancelamento;
        try {
            VFC48_QualifyingAccountBusinessDelegate.getInstance().
                updateDataForCancellationStage(qualifyingAccountVO);

            pageRef = new PageReference('/' + this.qualifyingAccountVO.opportunityId);

            return pageRef;

        } catch(Exception e) {
            message = new ApexPages.Message(
                ApexPages.Severity.Confirm,
                'Erro ao salvar os dados: ' + e.getMessage());

            ApexPages.addMessage(message);
        }

        return pageRef;
    }

    private Quote obterQuoteOpp(String oppId) {
        List<Quote> quoteList = Sfa2Utils.obterCotacao(oppId);
        String quoteId = null;

        if(quoteList.isEmpty()) {
            return null;
        }

        Quote quote = quoteList.get(0);
        if(quote.Status != 'Canceled') {
            return quote;
        }
        return null;
    }

    /*
     * Add values to Lead State field in visualforce page.
     * @return option - return the State result and to store values form record
     */
    public List<SelectOption> getStates() {
        List<SelectOption> state_Option = new List<SelectOption>();
        state_Option.add(new SelectOption('', ''));

        // Hard coded state values.
        state_Option.add(new SelectOption('AC', 'AC'));
        state_Option.add(new SelectOption('AL', 'AL'));
        state_Option.add(new SelectOption('AP', 'AP'));
        state_Option.add(new SelectOption('AM', 'AM'));
        state_Option.add(new SelectOption('BA', 'BA'));
        state_Option.add(new SelectOption('CE', 'CE'));
        state_Option.add(new SelectOption('DF', 'DF'));
        state_Option.add(new SelectOption('ES', 'ES'));
        state_Option.add(new SelectOption('GO', 'GO'));
        state_Option.add(new SelectOption('MA', 'MA'));
        state_Option.add(new SelectOption('MT', 'MT'));
        state_Option.add(new SelectOption('MS', 'MS'));
        state_Option.add(new SelectOption('MG', 'MG'));
        state_Option.add(new SelectOption('PR', 'PR'));
        state_Option.add(new SelectOption('PB', 'PB'));
        state_Option.add(new SelectOption('PA', 'PA'));
        state_Option.add(new SelectOption('PE', 'PE'));
        state_Option.add(new SelectOption('PI', 'PI'));
        state_Option.add(new SelectOption('RJ', 'RJ'));
        state_Option.add(new SelectOption('RN', 'RN'));
        state_Option.add(new SelectOption('RS', 'RS'));
        state_Option.add(new SelectOption('RO', 'RO'));
        state_Option.add(new SelectOption('RR', 'RR'));
        state_Option.add(new SelectOption('SC', 'SC'));
        state_Option.add(new SelectOption('SE', 'SE'));
        state_Option.add(new SelectOption('SP', 'SP'));
        state_Option.add(new SelectOption('TO', 'TO'));

        return state_Option;
    }

    public List<Selectoption> getBrands() {
        final List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', '-- Nenhum --'));
        for(String molicar : brandsDetailList) {
          option.add(new SelectOption(molicar, molicar));
        }
        option.add(new SelectOption(optionNotVehicle, optionNotVehicle));
        return option;
    }

    /*
     * get Model values from Molicar object
     * @return option - return the molicar  model result and to store values form record
     */
    public List<Selectoption> getModels() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', ''));

        //check Brand name must not be null and the model names list also not equalto null
        if( (selectedBrand != null || selectedBrand != '') && modelDetailList != null) {
            if(!modelDetailList.isEmpty()) {
                for (String model : this.modelDetailList) {
                    option.add(new SelectOption(model, model));
                }
            }
        }

        return option;
    }

    /**
     * get Brand  values from Molicar object using selected brand
     * @return - dont have any value return
     */
    public void getBrandDetails() {
        brandsDetailList = new List<String>();
        AggregateResult[] agrMolicarBrandDetails =
            VFC11_MolicarDAO.getInstance().findMolicarBrand();

        if(!agrMolicarBrandDetails.isEmpty()) {
            for(AggregateResult Brand : agrMolicarBrandDetails) {
                // To avoid duplication add Brand values to set list of Brands.
                brandsDetailList.add(String.valueOf(Brand.get('Brand__c')));
            }
        }

        brandsDetailList.sort();
    }

	/**
     * get Model values from Molicar object using selected Id
     * @return - dont have any value return
     */
    public void getModelDetails() {
        this.modelDetailList = new List<String>();
        List<MLC_Molicar__c> lstMolicarModels = new List<MLC_Molicar__c>();

        //get Brand values from Molicar object using selected Brand Id
        lstMolicarModels =
            VFC11_MolicarDAO.getInstance().findMolicarByBrand(this.selectedBrand);

        if(!lstMolicarModels.isEmpty()) {
            for (MLC_Molicar__c molicar : lstMolicarModels) {
                this.modelDetailList.add(molicar.Model__c);
            }
        }
    }

    /** Get the address information based on the zip code. */
    public PageReference getAddressGivenZipCode() {
        System.debug('*** getAddressGivenZipCode()');

        String zipCodeNumber = qualifyingAccountVO.shippingPostalCode;

        if(zipCodeNumber != null && zipCodeNumber.length() == 8) {
            ZipCodeBase__c zipCode =
                VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);

            System.debug('*** zipCode='+zipCode);

            if (zipCode != null) {
                qualifyingAccountVO.shippingStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
                qualifyingAccountVO.shippingCity = zipCode.City__c;
                qualifyingAccountVO.shippingState = zipCode.State__c;
            }
        }

        return null;
    }
}