@isTest
private class VFC94_VehicleItemVOTest {

    static testMethod void deveCriarVFC94_VehicleItemVO() {
        VFC94_VehicleItemVO vo = new VFC94_VehicleItemVO();
        vo.anoFabricacao = 2015;
        vo.anoModelo = 1;
        vo.bir = 'bir';
        vo.chassi = 'chassi';
        vo.CheckVehicodisabled = true;
        vo.cor = 'cor';
        vo.formattedPriceVehicle = 'formattedPriceVehicle';
        vo.isCheckVehico = true;
        vo.modelo = 'modelo';
        vo.opcionais = 'opcionais';
        vo.priceVehicle = 1;
        vo.status = 'status';
        vo.tempoEstoque = 'tempoEstoque';
        vo.versao = 'versao';
        
        System.assertEquals(2015, vo.anoFabricacao);
    }
}