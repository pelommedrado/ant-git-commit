@isTest(seeAllData=true)
private class ClienteOportDetalheSfa2ControllerTest {
    
    static Opportunity opportunity1;
    static Quote quote;
    static Product2 prd ;
    static VEH_Veh__c v;
    
    static {
        Account account1 = new Account(
            Name = 'Account Test 01',
            RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id,
            IDBIR__c = '1001');
        insert account1;
        
        Id recordTypeId = getRecordTypeId('Personal_Acc');
        
        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.RgStateTexto__c = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone = '1133334444';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.Sex__c = 'Man';
        sObjAccPersonal.MaritalStatus__c = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate = System.today();
        insert sObjAccPersonal;
        
        opportunity1 = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            AccountId = sObjAccPersonal.Id);
        insert opportunity1;
        
        
        MyOwnCreation moc = new MyOwnCreation();
        
        prd = moc.criaProduct2();
        Insert prd;
        
        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;
        
        Product2 prd2 = moc.criaProduct2();
        prd2.ModelSpecCode__c = 'Name';
        prd2.Model__c = prd.Id;
        Insert prd2;
        
        PricebookEntry pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = pb.Id;
        pe.Product2Id = prd.Id;
        Insert pe;
        
        v = moc.criaVeiculo();
        v.Model__c = 'CLIO';
        v.Price__c = 1;
        v.DateofManu__c = system.today();
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        vr.Account__c = account1.Id;
        Insert vr;
        
        VRE_VehRel__c veiculo = [
            SELECT Id, VIN__r.Model__c, Account__c, VIN__r.Price__c
            FROM VRE_VehRel__c
            WHERE ID =: vr.id
        ];
        VFC61_QuoteDetailsVO orc = 
            Sfa2Utils.criarOrcamento(
                opportunity1.Id, 
                null);
        
        quote = [
            SELECT ID
            FROM Quote
            WHERE ID =: orc.Id
        ];
        
    }
    
    static Id getRecordTypeId(String developerName) {
        RecordType sObjRecordType = [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName =: developerName];
        
        return sObjRecordType.Id;
    }
    
    static testMethod void unitTest01() {
        
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        ct.initialize();
    }
    
    static testMethod void unitTest02() {
        
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        ct.cancelOpportunity();
    }
    
    static testMethod void unitTest03() {
        
        Test.startTest();
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        ct.initialize();
        ct.cancelarPreOrdem();
        Test.stopTest();
    }
    
    static testMethod void unitTest04() {
        
        Test.startTest();
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        ct.initialize();
        ct.sendToRequest();
        Test.stopTest();
    }
    
    static testMethod void unitTest05() {
        
        Test.startTest();
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        ct.initialize();
        ct.getIsReadOnly();
        ct.updateVehicleInterest();
        Test.stopTest();
    }
    
    static testMethod void unitTest06() {
        
        Test.startTest();
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        ClienteOportunidadeDetalheSfa2Controller ct = 
            new ClienteOportunidadeDetalheSfa2Controller(stdCont);
        
        Lead lead = new Lead();
        lead.LastName = 'Last';
        lead.RecordTypeId = Utils.getRecordTypeId('Lead', 'Voucher');
        
        INSERT lead;
        
        AbstractOportunidadeController.verificarCampanha('08233350540', opportunity1.Id);
        AbstractOportunidadeController.cancelarAlertVoucher(opportunity1.Id);
        AbstractOportunidadeController.resgatarVoucher('08233350540', lead.Id, opportunity1.Id);
        Test.stopTest();
    }
}