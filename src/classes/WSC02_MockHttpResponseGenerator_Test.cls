/**
*	Class	-	WSC02_MockHttpResponseGenerator_Test
*	Author	-	Suresh Babu
*	Date	-	01/11/2012
*
*	#01 <Suresh Babu> <01/11/2012>
*		This class was used to test Http callout.
**/
@isTest
global class WSC02_MockHttpResponseGenerator_Test implements HttpCalloutMock{

	global HTTPResponse respond(HTTPRequest req) {
		
        System.assertEquals('GET', req.getMethod());
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        					   //Estrada da Aldeinha,220 - Alphaville Emp Barueri SP  Brasil
        res.setBody('{"name": "Estrada da Aldeinha,220 - Alphaville Emp Barueri SP  Brasil","Status": {"code": 200,"request": "geocode"},"Placemark": [ {"id": "p1","address": "Barueri - São Paulo, Brazil","AddressDetails": {"Accuracy" : 4,"Country" : {"AdministrativeArea" : {"AdministrativeAreaName" : "São Paulo","Locality" : {"LocalityName" : "Barueri"}},"CountryName" : "Brazil","CountryNameCode" : "BR"}},"ExtendedData": {"LatLonBox": {"north": -23.4691139,"south": -23.5529690,"east": -46.7988690,"west": -46.9653380}},"Point": {"coordinates": [ -46.8767748, -23.5113015, 0 ]}} ]}');
        res.setStatusCode(200);
        return res;
	}
}