/**
*   Class   -   Rforce_TestClassDataGenerator
*   Author  -   Praneel PIDIKITI
*   Date    -   27/12/2013
*   Project -   Rforce
*/
public with sharing class Rforce_Utils_OTSDataSource {

    public Boolean Test;
     
         public Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse getOTSData(Rforce_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
         
        // --- PRE TREATMENT ----
        Rforce_otsIcmApvBserviceRenault.ApvGetListOtsRequest request = new Rforce_otsIcmApvBserviceRenault.ApvGetListOtsRequest();       
        Rforce_otsIcmApvBserviceRenault.ApvGetListOtsRequestParms requestParameters = new Rforce_otsIcmApvBserviceRenault.ApvGetListOtsRequestParms();
        Rforce_otsIcmApvBserviceRenault.ServicePreferences servicePref = new Rforce_otsIcmApvBserviceRenault.ServicePreferences();
        requestParameters.vin = VehController.getVin();
        requestParameters.countryLanguage = '?';
        request.requestParms = requestParameters;    
        servicePref.country = System.label.CountryCode2;  // servicePref.country = 'FR'
        servicePref.language = UserInfo.getLanguage().substring(0, 2).toUpperCase(); // servicePref.language = 'FR'
        
        servicePref.userid = '?';
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_otsIcmApvBserviceRenault.ApvGetListOts OTSWS = new Rforce_otsIcmApvBserviceRenault.ApvGetListOts();
        OTSWS.endpoint_x = System.label.VFP05_OTS_URL;
        OTSWS.clientCertName_x = System.label.RenaultCertificate;     
        OTSWS.timeout_x=10000;
        
        Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse OTSRWS = new Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse();    
    
        if (Test==true) {
            OTSRWS = Rforce_Utils_Stubs.OTSStub();    
        } else {
            OTSRWS = OTSWS.getListOts(request);
        }
         return OTSRWS;  
     
    }
}