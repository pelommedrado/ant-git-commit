/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class UpdateInboundItem {
    @InvocableVariable(label='ITR Context' description='Optional ITR Context to set for this ITR Message. If not specified, ITR Context will not be modified.' required=false)
    global String ITRContext;
    @InvocableVariable(label='Object Id' description='Identifier for ITR Message object to be updated.' required=true)
    global Id ObjectId;
    @InvocableVariable(label='Queue Name' description='Optional Queue Name to set for this ITR Message.  If not specified, Queue Name will not be modified.' required=false)
    global String QueueName;
    global UpdateInboundItem() {

    }
}
