public with sharing class Rforce_CaseCommentTriggerHandler_CLS {

    public static void onBeforeInsert(list < Casecomment > listcomment) {

        if (listcomment.size() > 0) {
        
            sendEmail(listcomment);
        
        }
        
    }
    public static void sendEmail(list < Casecomment > commentlist) {
        
        if (commentlist.size() == 1 && commentlist[0].ParentId != null) {
            
            Case cas1 = [Select Id, casenumber, Dealer__c, CountryCase__c from Case where Id = :commentlist[0].ParentId];
            
            if (cas1.Dealer__c != null && cas1.CountryCase__c == 'Colombia') {
                
                Account acc2 = [Select Id, country__c, ProfEmailAddress__c from Account where  Id = :cas1.Dealer__c];
                
                if (acc2 != null) {
                    
                    if (acc2.ProfEmailAddress__c != null) {
                        
                        System.debug('###In SenEmailHelper2 Class### with CaseComment value =' + commentlist[0].Id);
                        
                        Messaging.reserveSingleEmailCapacity(1);
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        
                        // Strings to hold the email addresses to which you are sending the email.
                        String[] toAddresses = new String[] {acc2.ProfEmailAddress__c};
                        
                        // Assign the addresses for the To and CC lists to the mail object.
                        mail.setToAddresses(toAddresses);
                        
                        // Specify the name used as the display name.
                        mail.setSenderDisplayName('Salesforce Support');
                        
                        // Specify the subject line for your email address.
                        mail.setSubject('New Casecomment Added to the Case : ' + cas1.casenumber );
                        
                        // Set to True if you want to BCC yourself on the email.·
                        mail.setBccSender(false);
                        
                        // Optionally append the salesforce.com email signature to the email.
                        // The email address of the user executing the Apex Code will be used.
                        mail.setUseSignature(false);
                        
                        // Specify the text content of the email.
                        mail.setPlainTextBody('Notification');
                        mail.setHtmlBody('A casecomment is added to the case:<b>' + cas1.casenumber + '  </b><p>' +
                                         'To view your comment related to the case <a href=System.label.Rforce_loginURL' + cas1.Id + '>click here.</a>');
                        
                        // Send the email you have created.
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    
                    }
                
                }
                
            }
        
        }
    
    }

}