/**
*	Class	-	VFC27_TestDriveVehicleDAO_Test
*	Author	-	RameshPrabu
*	Date	-	16/11/2012
*
*	#01 <RameshPrabu> <16/11/2012>
*		This test class called for VFC27_TestDriveVehicleDAO
**/
@isTest
private class VFC27_TestDriveVehicleDAO_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        List<VEH_Veh__c> lstVehs = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
       	List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
       	List<TDV_TestDriveVehicle__c> lstTestDriveVehicle =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
       	
       	Test.startTest();

		// Test01
       	List<TDV_TestDriveVehicle__c> lstTestDriveVehicles = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingTestDriveVehicle(lstTestDriveVehicle[0]);

		// Test02
       	Id AccountId = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicleAccount_UsingId(lstTestDriveVehicle[0].Id);
    	String model = lstVehs[0].Model__c;    	
		TDV_TestDriveVehicle__c TestDriveVehicles = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingDealerId(lstDealerRecords[0].Id, model);

		// Test03
    	String town = 'Curitiba';
    	String stateCode = 'PR';
    	List<TDV_TestDriveVehicle__c> lstTestDriveVehs = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicleRecordsUsingCriteria(model,town,stateCode);
    	
    	// Test04
    	String tdvId = lstTestDriveVehicle[0].Id;
    	TDV_TestDriveVehicle__c test04 = VFC27_TestDriveVehicleDAO.getInstance().findById(tdvId);

		// Test05
		String account = '';
		Boolean available = True;
		String vehicleModel = '';
    	List<TDV_TestDriveVehicle__c> test05 = VFC27_TestDriveVehicleDAO.getInstance().findByCriteria(account, available, vehicleModel);

    	Test.stopTest();
    }
}