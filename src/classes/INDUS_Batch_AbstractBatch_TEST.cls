/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class INDUS_Batch_AbstractBatch_TEST {
     
     class AbstractBatchTestException extends Exception {}     

     static void activeLog(){
        CS22_INDUS_Logger__c setting = new CS22_INDUS_Logger__c(Name=INDUS_Batch_AbstractBatchMock_TEST.class.Getname(), Active__c=true);
        insert setting;
     }
     
     static testMethod void dbDeleteExceptionInactiveLog() {
         dbExceptionInactiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE);
     }

     static testMethod void dbUpdateExceptionInactiveLog() {
         dbExceptionInactiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_UPDATE);
     }

     static testMethod void dbDeleteExceptionActiveLog() {
         dbExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE);
     }

     static testMethod void dbUpdateExceptionActiveLog() {
         dbExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_UPDATE);
     }

     static testMethod void dbDeleteNoExceptionActiveLog() {
         dbNoExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE);
     }

     static testMethod void dbUpdateNoExceptionActiveLog() {
         dbNoExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action.ACTION_UPDATE);
     }

	 /** Cover EmailSending .... no assertions done, just code coverage. Not enough time **/
	 static testMethod void dbSendEmail() {
		dbExceptionInactiveLogAndSendEmail(null);
	 }

	 /**
      *launch test to generate an exception during delete/update with no log.
      */
     private static void dbExceptionInactiveLog(INDUS_Batch_AbstractBatch_CLS.Action action) { 
        dbException(action, true, false);
        List<Logger__c> ll = [SELECT Id, Error_Level__c, Exception_Type__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST'];
        System.AssertEquals(0, ll.size()); 
     }

     /**
      *launch test to generate an exception during delete/update with no log.
      */
     private static void dbExceptionInactiveLogAndSendEmail(INDUS_Batch_AbstractBatch_CLS.Action action) { 
        dbException(action, true, true);
        List<Logger__c> ll = [SELECT Id, Error_Level__c, Exception_Type__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST'];
        System.AssertEquals(0, ll.size()); 
     }
     
     /**
      *launch test to generate an exception during delete/update with log.
      */
     private static void dbExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action action) { 
        activeLog();
        dbException(action, true, false);
        List<Logger__c> llparent = [SELECT Id, Error_Level__c, Exception_Type__c, Message__c, Total_Scope__c, Nb_Records_Created__c, Nb_Records_Updated__c, Nb_Records_Deleted__c, Nb_Error_Insert__c, Nb_Error_Update__c, Nb_Error_Delete__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST' AND ParentLog__c = NULL];
        System.debug('#### INDUS_Batch_AbstractBatch_TEST - dbExceptionActiveLog - list Logger__c:' + llparent);
        System.AssertEquals(1, llparent.size());
        System.AssertEquals('Info', llparent[0].Error_Level__c);
        System.AssertEquals('Failed', llparent[0].Exception_Type__c); 
        System.AssertEquals(2, llparent[0].Total_Scope__c);
        System.AssertEquals(0, llparent[0].Nb_Records_Created__c);
        System.AssertEquals(0, llparent[0].Nb_Records_Updated__c);
        System.AssertEquals(0, llparent[0].Nb_Records_Deleted__c);
        System.AssertEquals(0, llparent[0].Nb_Error_Insert__c);
        if (action == INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE){
           System.AssertEquals(0, llparent[0].Nb_Error_Update__c);
           System.AssertEquals(2, llparent[0].Nb_Error_Delete__c);
        }else{
           System.AssertEquals(2, llparent[0].Nb_Error_Update__c);
           System.AssertEquals(0, llparent[0].Nb_Error_Delete__c);
        }
        List<Logger__c> llchild = [SELECT Id, Error_Level__c, Exception_Type__c, Message__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST' AND ParentLog__c = :llparent[0].Id];
        System.AssertEquals(1, llchild.size());
        System.AssertEquals('Critical', llchild[0].Error_Level__c);
        System.AssertNotEquals(null, llchild[0].Exception_Type__c);
        System.AssertEquals(llchild[0].Exception_Type__c, 'External entry point');
        System.AssertEquals(llchild[0].Message__c, 'ERROR :Exception:TEST DB EXCEPTION');
     }

     /**
      *launch test to generate an exception during delete/update with log.
      */
     private static void dbNoExceptionActiveLog(INDUS_Batch_AbstractBatch_CLS.Action action) { 
        activeLog();
        dbException(action, false, false);
        List<Logger__c> llparent = [SELECT Id, Error_Level__c, Exception_Type__c, Message__c, Total_Scope__c, Nb_Records_Created__c, Nb_Records_Updated__c, Nb_Records_Deleted__c, Nb_Error_Insert__c, Nb_Error_Update__c, Nb_Error_Delete__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST' AND ParentLog__c = NULL];
        System.debug('#### INDUS_Batch_AbstractBatch_TEST - dbExceptionActiveLog - list Logger__c:' + llparent);
        System.AssertEquals(1, llparent.size());
        System.AssertEquals('Info', llparent[0].Error_Level__c);
        System.AssertEquals('Success', llparent[0].Exception_Type__c); 
        System.AssertEquals(2, llparent[0].Total_Scope__c);
        System.AssertEquals(0, llparent[0].Nb_Records_Created__c);
        System.AssertEquals(0, llparent[0].Nb_Error_Insert__c);
        System.AssertEquals(0, llparent[0].Nb_Error_Update__c);
        System.AssertEquals(0, llparent[0].Nb_Error_Delete__c);
        if (action == INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE){
	        System.AssertEquals(0, llparent[0].Nb_Records_Updated__c);
	        System.AssertEquals(2, llparent[0].Nb_Records_Deleted__c);
        }else{
	        System.AssertEquals(2, llparent[0].Nb_Records_Updated__c);
	        System.AssertEquals(0, llparent[0].Nb_Records_Deleted__c);
        }
        List<Logger__c> llchild = [SELECT Id, Error_Level__c, Exception_Type__c, Message__c FROM Logger__c WHERE Apex_Class__c = :'INDUS_Batch_AbstractBatchMock_TEST' AND ParentLog__c = :llparent[0].Id];
        System.AssertEquals(0, llchild.size());
     }

     /**
      *launch test to generate an exception during delete/update.
      */
     private static void dbException(INDUS_Batch_AbstractBatch_CLS.Action action, Boolean isexception, Boolean sendemail) {
        INDUS_Batch_AbstractBatchMock_TEST btch = new INDUS_Batch_AbstractBatchMock_TEST();
        //Lead l1 = new Lead(Id='00Q11000006bt4t');  
        //Lead l2 = new Lead(Id='00Q11000006bt7x');
		//SDP, determine the record type to use depending on the org
		String leadRTName = '';
		if( Myr_Datasets_Test.isEuropeanOrg() ) {
			leadRTName = 'LEA_VN_VO';
		} else if( Myr_Datasets_Test.isBrazilOrg() ) {
			leadRTName = 'LDD_Internal';
		} else if( Myr_Datasets_Test.isRussianOrg() ) {
			leadRTName = 'LeadCoreRecType';
		}
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='Lead' AND DeveloperName = :leadRTName];
        List<Lead> lleads = new List<Lead>();
        lleads.add(new Lead(lastname='talon', firstname='achille',email='a.talon@test.com', recordTypeId=rt.Id));
        lleads.add(new Lead(lastname='lefuneste', firstname='hilarion',email='h.lefuneste@test.com', recordTypeId=rt.Id));
		insert lleads;          
        btch.iter = lleads;
        btch.requiredAction = action; //INDUS_Batch_AbstractBatch_CLS.Action.ACTION_DELETE;  
		btch.sendEmail = sendemail;
        if (isexception){
        	btch.dbException = new AbstractBatchTestException('TEST DB EXCEPTION'); 
        }
        Test.startTest(); 
        Database.executeBatch(btch);
        Test.stopTest();

        ApexClass aC = [SELECT Id, Name FROM ApexClass WHERE Name = 'INDUS_Batch_AbstractBatchMock_TEST'];
        List<AsyncApexJob> la = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, ExtendedStatus, CompletedDate FROM AsyncApexJob WHERE ApexClassID=:aC.Id];
        System.AssertEquals(1, la.size());
        System.AssertEquals(0, la[0].NumberOfErrors);
        System.AssertEquals('Completed', la[0].Status);
     }
}