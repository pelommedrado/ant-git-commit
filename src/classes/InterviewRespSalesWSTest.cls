@isTest
public with sharing class InterviewRespSalesWSTest {

	@TestSetup static void setup(){
		Account newAcc = new Account();
		newAcc.Name = 'accTeste';
		newAcc.Phone = '12345678';
		Database.insert(newAcc);

		Contact newContact = new Contact();
		newContact.LastName = 'testeContact';
		newContact.AccountId = newAcc.Id;
		Database.insert(newContact);

		Interview__c newInterview = new Interview__c();
		newInterview.Name = 'Pesquisa VN';
		Database.insert(newInterview);

		InterviewResponseBuild instance = InterviewResponseBuild.getInstance();
		instance.answerLink = 'www.teste.com.br';
		instance.returnedToRepair = 'Repair shop';

		InterviewResponse__c interviewRes = instance.newInterviewResp(newAcc.Id, newContact.Id, newInterview.Id);
		Database.insert(interviewRes);
    }

    @isTest static void shouldGetInterviewResById() {
		Id interviewRes = [SELECT Id FROM InterviewResponse__c LIMIT 1].Id;

		RestRequest request = new RestRequest();
		request.requestUri = URL.getSalesforceBaseUrl()+'/services/apexrest/sales/'+ interviewRes;
		request.httpMethod = 'GET';
		RestContext.request = request;

		InterviewResponse__c check = InterviewRespSalesWS.getSalesInterview();
		System.assert(check != null);
		System.assertEquals('Facebook', check.Origin__c);
		System.assertEquals('www.teste.com.br', check.AnswerLink__c);
		System.assertEquals('Repair shop', check.CustomerReturnedRenaultRepairShop__c);
    }

     @isTest static void shouldCreateInterviewRes() {

		Id accId        = [SELECT Id FROM Account LIMIT 1].Id;

		Id checkId;
		InterviewRespSalesWS.ResponseInterviewSales innerClass;

		innerClass = InterviewRespSalesWS.createSalesInterview(true, true, true, 10, 'test', true, accId);

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.interviewResponseId == null);

		checkId = innerClass.interviewResponseId;

		System.assert(checkId != null);
		InterviewResponse__c check = InterviewResponseDAO.getInterviewResById(checkId);
		System.assert(check != null);
		System.assertEquals('Facebook', check.Origin__c);
		System.assertEquals(accId, check.AccountId__c);
		System.assertEquals('test', check.CustomerExperience__c);
		System.assertEquals('Sim. E realizei.', check.TestDriveOffered__c);

		innerClass = InterviewRespSalesWS.createSalesInterview(true, false, true, 10, 'test', true, accId);

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.interviewResponseId == null);

		checkId = innerClass.interviewResponseId;

		System.assert(checkId != null);
		check = InterviewResponseDAO.getInterviewResById(checkId);
		System.assert(check != null);
		System.assertEquals('Facebook', check.Origin__c);
		System.assertEquals(accId, check.AccountId__c);
		System.assertEquals('test', check.CustomerExperience__c);
		System.assertEquals('Sim. Mas optei por não realizar.', check.TestDriveOffered__c);

		innerClass = InterviewRespSalesWS.createSalesInterview(false, false, true, 10, 'test', true, accId);

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.interviewResponseId == null);

		checkId = innerClass.interviewResponseId;

		System.assert(checkId != null);
		check = InterviewResponseDAO.getInterviewResById(checkId);
		System.assert(check != null);
		System.assertEquals('Facebook', check.Origin__c);
		System.assertEquals(accId, check.AccountId__c);
		System.assertEquals('test', check.CustomerExperience__c);
		System.assertEquals('Não.', check.TestDriveOffered__c);
    }

}