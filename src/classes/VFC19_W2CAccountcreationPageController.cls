public with sharing class VFC19_W2CAccountcreationPageController{
    
    Id caseId=ApexPages.currentPage().getParameters().get('caseId');
    Id profileId=userinfo.getProfileId();
    String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
    String getProfiletrim=profileName.subString(0,3);
    List<case> caseRec=new List<case>();
    
    public string country;
    public String accFirstName;
    public String accLastName;
    public String CPF;
    public String billingStreet;
    public String billingCity;
    public String billingState;
    public String personTitle;
    public String phone;
    public String cellPhone;
    public String email;
    public String VIN;
    public String licenseNo;
    public String caseNumber;
    public String Language;
    public String Source;
    public String Sub_Source;
    public String Detail;
    public String SubDetail;
    ID accRecTypeId;
    
    public VFC19_W2CAccountcreationPageController( ){}
    
    //This method is used to redirect users to New Account Page
    public PageReference accountRedirectPage(){
        try{  
            caseRec=[select Id,SuppliedEmail,Origin, CaseSubSource__c, Type, SubType__c, CaseNumber,Address_Web__c,City_Web__c,Cell_Phone_Web__c,SuppliedName,
                     SuppliedPhone,Email_Web__c,CPF_Web__c,CPF_Not_Found_Web__c,Email_Opt_In_Web__c,FirstName_Web__c,LastName_Web__c,
                     License_Number_Web__c,State_Web__c,Title_Web__c,VIN_Web__c,CountryCase__c from case where Id=:caseId];    
        }catch(System.Stringexception e){  
            System.debug(e.getMessage());
        }catch(System.Listexception e){
            System.debug(e.getMessage());
        }
        caseNumber=caseRec[0].CaseNumber;
        accFirstName=caseRec[0].FirstName_Web__c;
        accLastName=caseRec[0].LastName_Web__c;
        personTitle=caseRec[0].Title_Web__c;
        CPF=caseRec[0].CPF_Web__c;
        if(caseRec[0].Address_Web__c!=null){
            billingStreet=caseRec[0].Address_Web__c;
        }
        if(caseRec[0].City_Web__c!=null){
            billingCity=caseRec[0].City_Web__c;
        }
        if(caseRec[0].State_Web__c!=null){
            billingState=caseRec[0].State_Web__c;
        }
        phone=caseRec[0].SuppliedPhone;
        cellPhone=caseRec[0].Cell_Phone_Web__c;
        // HQREQ-01878 
        if(caseRec[0].CountryCase__c!='Brazil' && getProfiletrim!='LMT')
            email=caseRec[0].SuppliedEmail;
        else
            email=caseRec[0].Email_Web__c;
        VIN=caseRec[0].VIN_Web__c;
        if(caseRec[0].License_Number_Web__c!=null){
            licenseNo=caseRec[0].License_Number_Web__c;
        }
        accRecTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
        
        //Start Code - To LMT-Origins - @Edvaldo - kolekto
        
        if(String.isNotBlank(caseRec[0].Origin) && caseRec[0].Origin.equalsIgnoreCase('Renault Site') &&
           String.isNotBlank(caseRec[0].CaseSubSource__c) && caseRec[0].CaseSubSource__c.equalsIgnoreCase('Webform') && 
           String.isNotBlank(caseRec[0].Type) && caseRec[0].Type.equalsIgnoreCase('Other')){
            Source = 'CCS';
            Sub_Source = 'RENAULT SITE';
            Detail = 'SUGGESTION';
        }    
        else if(String.isNotBlank(caseRec[0].Origin) && caseRec[0].Origin.equalsIgnoreCase('Renault Site') &&
                String.isNotBlank(caseRec[0].CaseSubSource__c) && caseRec[0].CaseSubSource__c.equalsIgnoreCase('Webform') &&
                String.isNotBlank(caseRec[0].Type) && caseRec[0].Type.equalsIgnoreCase('Service Request') &&
                String.isNotBlank(caseRec[0].SubType__c) && caseRec[0].SubType__c.equalsIgnoreCase('Communication & Events')){
                    Source = 'CCS';
                    Sub_Source = 'RENAULT SITE';
                    Detail = 'NEWS';
                }
        else if(String.isNotBlank(caseRec[0].Origin) && caseRec[0].Origin.equalsIgnoreCase('Renault Site') &&
                String.isNotBlank(caseRec[0].CaseSubSource__c) && caseRec[0].CaseSubSource__c.equalsIgnoreCase('Webform') &&
                String.isNotBlank(caseRec[0].Type) && caseRec[0].Type.equalsIgnoreCase('Service Request') &&
                String.isNotBlank(caseRec[0].SubType__c) && caseRec[0].SubType__c.equalsIgnoreCase('Brochure Request')){
                    Source = 'CCS';
                    Sub_Source = 'RENAULT SITE';
                    Detail = 'BROCHURES';
                }
        else if(String.isNotBlank(caseRec[0].Origin) && caseRec[0].Origin.equalsIgnoreCase('Renault Site') &&
                String.isNotBlank(caseRec[0].CaseSubSource__c) && caseRec[0].CaseSubSource__c.equalsIgnoreCase('Chat')){
            Source = 'CCS';
            Sub_Source = 'RENAULT SITE';
            Detail = 'CHAT - LIVE AGENT';
        }
        else{
            Source = 'CCS';
            Sub_Source = 'PHONE - RECEPTIVE';
        }
        
        return new PageReference('/001/e?RecordTypeId='+accRecTypeId+'&name_firstacc2='+accFirstName+'&name_lastacc2='+accLastName+'&00ND0000004qc8t='+
                                 phone+'&acc17street='+billingStreet+'&acc17city='+billingCity+'&acc17state='+billingState+'&00ND0000004qc9K='+email
                                 +'&00ND0000004qc8R='+CPF+'&00ND0000005cxiA='+caseNumber+'&00ND0000004qc9M='+cellPhone+'&00ND0000004qc8K='+country
                                 +'&00ND0000004qc91='+Language+'&00ND0000004qc7e='+Sub_Source+'&00ND0000004qc9i='+Source+'&00ND0000005eIx0='+Detail);
        
        //Finish Code - To LMT-Origins - @Edvaldo - kolekto
        
        /*return new PageReference('/001/e?RecordTypeId='+accRecTypeId+'&name_firstacc2='+accFirstName+'&name_lastacc2='+accLastName
        +'&00ND0000004qc8t='+phone+'&name_salutationacc2='+personTitle+'&acc17street='+billingStreet+'&acc17city='+billingCity+'&acc17state='
    +billingState+'&00ND0000004qc9K='+email+'&00ND0000004qc8R='+CPF+'&00ND0000005cxiA='+caseNumber+'&00ND0000004qc9M='+cellPhone
    +'&00ND0000004qc8K='+country+'&00ND0000004qc91='+Language);*/
    }
    
}