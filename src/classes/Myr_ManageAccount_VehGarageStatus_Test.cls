@isTest
private class Myr_ManageAccount_VehGarageStatus_Test { 

	 @testsetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
	}

	//Vehicle Rejected: not modified
	static testmethod void test_VehRejected_NotModified() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultId = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='rejected');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'rejected', checkVre.My_Garage_Status__c );
	}

	//Vehicle Taken: not modified
	static testmethod void test_VehTaken_NotModified() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultId = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='taken');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'taken', checkVre.My_Garage_Status__c );
	}

	//Vehicle Confirmed, even for another brand, not modified
	static testmethod void test_VehConfirmed_NotModified() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='confirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault Unconfirmed for account renault, pass it to confirmed
	static testmethod void test_VehUnConfirmed_Confirmed() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault with blank status for account renault, pass it to confirmed
	static testmethod void test_VehBlank_Confirmed() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle with No Brand, no garage status, Account Renault: do not treat the vehicle relation
	static testmethod void test_VehNoBrand_AccountRenault() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; //available for MyRenault
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( null, checkVre.My_Garage_Status__c );
	}

	//Vehicle with No brand, no garage status, Account Dacia: do not treat the vehicle
	static testmethod void test_VehNoBrand_AccountDacia() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Activated;
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( null, checkVre.My_Garage_Status__c );
	}

	//Vehicle Renault, garage status unconfirmed, but account nor created, nor preregistered, nor activated
	static testmethod void test_VehRenault_AccountNotValidAccount() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Deleted; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'unconfirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault, garage status unconfirmed, account activated for nissan
	static testmethod void test_VehRenault_AccountNissan() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'nimportequoi@atos.net';
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'unconfirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault, garage status blank, account created for renault
	static testmethod void test_VehRenault_AccountRenault() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Created; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle dacia, garage status empty, account renault created
	static testmethod void test_VehDacia_AccountRenault() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Created; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Dacia');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'unconfirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault, garage status empty, account dacia preregistered
	static testmethod void test_VehRenault_AccountDacia() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Preregistrered; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'unconfirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle Dacia, garage status empty, account dacia activated
	static testmethod void test_VehDacia_AccountDacia() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Dacia');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault, garage status unconfirmed, account created for renault AND dacia
	static testmethod void test_VehRenault_AccountRenaultDacia() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Created; 
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Created; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle renault, garage status unconfirmed, account created for renault AND dacia
	static testmethod void test_VehDacia_AccountRenaultDacia() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Created; 
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Created; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Dacia');
		insert veh;
		VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=listAccounts[0].Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vre;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( listAccounts[0].Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vre.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
	}

	//Vehicle Renault already linked as rejected to another account
	static testmethod void test_VehRenault_AccountRenault_VehRejectedForAccB() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		Account accB = listAccounts[1];
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vreAccA = new VRE_VehRel__c(Account__c=accA.Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vreAccA;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=veh.Id, My_Garage_Status__c='rejected');
		insert vreAccB;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccA.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
		VRE_VehRel__c checkVreB = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id];
		system.assertEquals( 'rejected', checkVreB.My_Garage_Status__c );
	}

	//Vehicle Renault already linked as taken to another account
	static testmethod void test_VehRenault_AccountRenault_VehTakenForAccB() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		Account accB = listAccounts[1];
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vreAccA = new VRE_VehRel__c(Account__c=accA.Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vreAccA;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=veh.Id, My_Garage_Status__c='taken');
		insert vreAccB;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccA.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
		VRE_VehRel__c checkVreB = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id];
		system.assertEquals( 'taken', checkVreB.My_Garage_Status__c );
	}

	//Vehicle Renault already linked as confirmed to another account
	static testmethod void test_VehRenault_AccountRenault_VehConfirmedForAccB() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		Account accB = listAccounts[1];
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vreAccA = new VRE_VehRel__c(Account__c=accA.Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vreAccA;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=veh.Id, My_Garage_Status__c='confirmed');
		insert vreAccB;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccA.Id];
		system.assertEquals( null, checkVre.My_Garage_Status__c );
		VRE_VehRel__c checkVreB = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id];
		system.assertEquals( 'confirmed', checkVreB.My_Garage_Status__c );
	}

	//Vehicle Renault already linked as unconfirmed to another account
	static testmethod void test_VehRenault_AccountRenault_VehUnConfirmedForAccB() {	
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		Account accB = listAccounts[1];
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vreAccA = new VRE_VehRel__c(Account__c=accA.Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vreAccA;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=veh.Id, My_Garage_Status__c='unconfirmed');
		insert vreAccB;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVreA = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccA.Id];
		system.assertEquals( 'confirmed', checkVreA.My_Garage_Status__c );
		VRE_VehRel__c checkVreB = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id];
		system.assertEquals( 'unconfirmed', checkVreB.My_Garage_Status__c );
	}

	//Vehicle Renault already linked with blank status to another account
	static testmethod void test_VehRenault_AccountRenault_VehBlankForAccB() {
		//Prepare data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		Account accB = listAccounts[1];
		VEH_Veh__c veh = new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault');
		insert veh;
		VRE_VehRel__c vreAccA = new VRE_VehRel__c(Account__c=accA.Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vreAccA;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=veh.Id, My_Garage_Status__c='');
		insert vreAccB;
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the result
		VRE_VehRel__c checkVre = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccA.Id];
		system.assertEquals( 'confirmed', checkVre.My_Garage_Status__c );
		VRE_VehRel__c checkVreB = [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id];
		system.assertEquals( null, checkVreB.My_Garage_Status__c );
	}

	//5 vehicles with different situations linked to Renault account
	static testmethod void test_SeveralVehicles_AccountRenault() {
		//Prepare data
		//Accounts
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].renaultid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0]; //Renault account
		Account accB = listAccounts[1];//Renault account
		//Vehicles
		List<VEH_Veh__c> vehicles = new List<VEH_Veh__c>();
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Nissan'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST2', VehicleBrand__c='Renault'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST3', VehicleBrand__c='Renault'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST4', VehicleBrand__c='Renault'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST5', VehicleBrand__c='Renault'));
		insert vehicles;
		//Relations
		List<VRE_VehRel__c> relations = new List<VRE_VehRel__c>();
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[0].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[1].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[2].Id, My_Garage_Status__c='taken'));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[3].Id, My_Garage_Status__c='unconfirmed'));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[4].Id, My_Garage_Status__c=''));
		insert relations;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=vehicles[4].Id, My_Garage_Status__c='confirmed');
		insert vreAccB;
		relations = [SELECT Id, VIN__r.Name, VIN__c, Account__c, My_Garage_Status__c, VIN__r.VehicleBrand__c
					 FROM VRE_VehRel__c WHERE Id IN :relations ORDER BY Id];
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the results
		//Linked to account A but Nissan vehicle => do not touch 
		system.assertEquals( null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[0].Id].My_Garage_Status__c );
		//Linked to account A with blank status => confirmed
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[1].Id].My_Garage_Status__c );
		//Linked to account A with taken status => do not touch 
		system.assertEquals( 'taken', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[2].Id].My_Garage_Status__c );
		//Linked to account A with unconfirmed status => confirmed
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[3].Id].My_Garage_Status__c );
		//Linked to account A with blank status and with account B with confirmed status => do not touch
		system.assertEquals( null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[4].Id].My_Garage_Status__c );
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id].My_Garage_Status__c );
	}

	//5 vehicles with different situations linked to Dacia account
	static testmethod void test_SeveralVehicles_AccountDacia() {	
		//Prepare data
		//Accounts
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.NO_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Patrice', 'Dupont', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[0].myd_status = system.Label.Myr_Status_Activated; 
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'Convenant', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[1].daciaid = 'patrice_dupont@unit.atos.test';
		listQueries[1].myd_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0]; //Dacia account
		Account accB = listAccounts[1];//Dacia account
		//Vehicles
		List<VEH_Veh__c> vehicles = new List<VEH_Veh__c>();
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Nissan'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST2', VehicleBrand__c='Dacia'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST3', VehicleBrand__c='Dacia'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST4', VehicleBrand__c='Dacia'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST5', VehicleBrand__c='Dacia'));
		insert vehicles;
		//Relations
		List<VRE_VehRel__c> relations = new List<VRE_VehRel__c>();
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[0].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[1].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[2].Id, My_Garage_Status__c='taken'));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[3].Id, My_Garage_Status__c='unconfirmed'));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[4].Id, My_Garage_Status__c=''));
		insert relations;
		VRE_VehRel__c vreAccB = new VRE_VehRel__c(Account__c=accB.Id, VIN__c=vehicles[4].Id, My_Garage_Status__c='confirmed');
		insert vreAccB;
		relations = [SELECT Id, VIN__r.Name, VIN__c, Account__c, My_Garage_Status__c, VIN__r.VehicleBrand__c
					 FROM VRE_VehRel__c WHERE Id IN :relations ORDER BY Id];
		//Trigger the test
		Myr_ManageAccount_VehGarageStatus modifier = new Myr_ManageAccount_VehGarageStatus( accA.Id );
		modifier.setGaragetStatusAndCommit();
		//Check the results
		//Linked to account A but Nissan vehicle => do not touch 
		system.assertEquals( null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[0].Id].My_Garage_Status__c );
		//Linked to account A with blank status => confirmed
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[1].Id].My_Garage_Status__c );
		//Linked to account A with taken status => do not touch 
		system.assertEquals( 'taken', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[2].Id].My_Garage_Status__c );
		//Linked to account A with unconfirmed status => confirmed
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[3].Id].My_Garage_Status__c );
		//Linked to account A with blank status and with account B with confirmed status => do not touch
		system.assertEquals( null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[4].Id].My_Garage_Status__c );
		system.assertEquals( 'confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:vreAccB.Id].My_Garage_Status__c );
	}




}