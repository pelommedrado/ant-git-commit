/*****************************************************************************************
    Name    : VFC13_UpdateVehicleCalloutr
    Desc    : This is to make Webservice callout to Update vehicle Details
    Approach: 
                
                                                
 Modification Log : 
---------------------------------------------------------------------------------------
Developer                       Date                Description
---------------------------------------------------------------------------------------
Praveen Ravula                  4/22/2013           Created 

******************************************************************************************/


global class VFC13_UpdateVehicleCallout{


    public static String countryCode {get;set;}
 //constructor
    VFC13_UpdateVehicleCallout() {
       countryCode ='';    
    }

  webservice static veh_veh__c  getVehicleDetails(String VINNO,String VID){
    
        veh_veh__c vehicleobject;
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse rep;
      
       
        if (VINNO != null){
    
           rep = searchBVM(VINNO);
        }
        if (rep != null && rep.detVeh != null) {
           vehicleobject = updateVehicleData(rep,VINNO,VID);   
            return vehicleobject;
        }
        
        // else update vehicle record with uncheck bvm data upto date? checkbox and details piclist with not found value.
          else { 
           VEH_Veh__c veh = new VEH_Veh__c();
           veh.Name =VINNO;
           veh.Id=VID;  
           Veh.BVM_data_Upto_date__c=false;
           veh.Details__c='Not Found';
          
           return veh;
       }   
  }


   public static WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse searchBVM(String VINNO){
    
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();               
        WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
        WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
       
        try{  
            servicePref.bvmsi25SocEmettrice  = 'CCB';
            servicePref.bvmsi25Vin = VINNO;
            servicePref.bvmsi25CodePaysLang = countryCode; //FRA 
            servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
            request.ServicePreferences = servicePref;
            VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
            VehWS.clientCertName_x = System.label.clientCertNamex;           
            VehWS.timeout_x=50000;
            System.debug('req***********'+request); 
            System.debug('servicePref ***********'+servicePref); 
            return (WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse)VehWS.getApvGetDetVehXml(request);
        }            
        catch (Exception e){                    
            system.debug(VEH_WS);
            system.debug('erreur*********** : ' + String.valueOf(e));
            return null;
        }
    }
    
    public static VEH_Veh__c  updateVehicleData(WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse vehBVM,String VIN,String VID){
             VEH_Veh__c veh = new VEH_Veh__c();
             veh.Name = VIN;
             veh.Id=VID;              
        //VersionCode__c p460:version
        if (vehBVM.detVeh.version != null){
            veh.VersionCode__c = vehBVM.detVeh.version;
        }
     
        //AfterSalesType__c p460:tvv
        if (vehBVM.detVeh.tvv != null) {
            veh.AfterSalesType__c = vehBVM.detVeh.tvv;
        }
         
         //EngineType__c p460:typeMot 
        if (vehBVM.detVeh.typeMot != null){
            veh.EngineType__c = vehBVM.detVeh.typeMot;         
        }
        
        //EngineIndex__c p460:indMot
        if (vehBVM.detVeh.indMot != null){
            veh.EngineIndex__c = vehBVM.detVeh.indMot; 
        }
        
         //EngineManuNbr__c p460:NMot
        if (vehBVM.detVeh.NMot != null){
            veh.EngineManuNbr__c = vehBVM.detVeh.NMot;
        }
           
        //GearBoxType__c p460:typeBoi
        if (vehBVM.detVeh.typeBoi != null){
            veh.GearBoxType__c = vehBVM.detVeh.typeBoi;
        } 
 
       //GearBoxIndex__c p460:indBoi 
        if (vehBVM.detVeh.indBoi != null){
            veh.GearBoxIndex__c = vehBVM.detVeh.indBoi; 
        }  
        
      //GearBoxManuNbr__c p460:NBoi
        if (vehBVM.detVeh.NBoi != null){
            veh.GearBoxManuNbr__c = vehBVM.detVeh.NBoi;   
        } 
         
        //DateofManu__c p460:dateTcmFab 
        if (vehBVM.detVeh.dateTcmFab != null && vehBVM.detVeh.dateTcmFab.length() > 7) {
            veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(0, 2)));
        }  
      
         // updating the EnergyType from BVM service when the value is null
    
        if (veh.EnergyType__c == null) {
            if (vehBVM.detVeh.criteres != null) {
              for (WS01_ApvGetDetVehXml.DetVehCritere crit : vehBVM.detVeh.criteres){
                //EnergyType__c Objet 019
                if (crit.cdObjOf == '019')
                    veh.EnergyType__c = crit.critereOf;
              }
            }
        }
      
          
        if ( veh.DeliveryDate__c == null) {
            if (vehBVM.detVeh.dateLiv != null && vehBVM.detVeh.dateLiv.length() > 7)
                veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(0, 2)));
        }
              
        if (veh.Model__c == null) {
             if (vehBVM.detVeh.libModel != null)
             veh.Model__c = vehBVM.detVeh.libModel;
        }
        
        
        if (veh.BodyType__c == null) {
            if (vehBVM.detVeh.libCarrosserie != null){
                veh.BodyType__c = vehBVM.detVeh.libCarrosserie;
           }
       }     
       
       
  
              
    // Update the new Criteria Field   
    //here is the  criteres attempt to derefference a nullpoint
       if (vehBVM.detVeh.criteres != null){
        WS01_ApvGetDetVehXml.DetVehCritere[] criteres=vehBVM.detVeh.criteres;

        String ModelRangeValue;
        String ModelCode;
        for(integer i=0;i<criteres.size();i++){

         System.debug('Criteria Label Value>>>>>>>>>>>'+Criteres[i].libCritOf);
         System.debug('Criteria Value>>>>>>>>>>>'+Criteres[i].critereOf);
         System.debug('Criteria Value cdCritOf>>>>>>>>>>>'+Criteres[i].cdCritOf);
         System.debug('Criteria Value cdObjOf>>>>>>>>>>>'+Criteres[i].cdObjOf);
     
        if(criteres[i].cdObjOf =='008'){
           ModelCode=criteres[i].critereOf;  
           System.debug('ModelCode>>>>>>>>>>>'+ModelCode);
        }     
        if(criteres[i].libCritOf=='MODELE FICHE GAMME'){
         ModelRangeValue=criteres[i].critereOf;    
         System.debug('ModelRangeValue>>>>>>>>>>>'+ModelRangeValue);
        }
     }  
       
        if (veh.Model_Range__c == null) {
               if (ModelRangeValue != null){
               veh.Model_Range__c = ModelRangeValue;
                          
               }
        }     
       
       
       
        if (veh.ModelCode__c == null) {
           if (ModelCode != null){
             veh.ModelCode__c = ModelCode;
             }
        }     
       System.debug('res*********'+vehBVM);              
   }
      if( Veh.BVM_data_Upto_date__c==false){
            Veh.BVM_data_Upto_date__c=true;
            veh.Details__c='Found';
      } 
        
      return veh;
   }

    
}