global class SurveyAnswerBatch implements Database.Batchable<sObject> {
	
	String query;
	Date hoje = System.today();
	DateTime daysBeforeIni = System.today().addDays(-2);
	DateTime daysBeforeFim = DateTime.newInstance(hoje.year(), hoje.month(), hoje.addDays(-2).day(), 23, 59, 59);
	
	global SurveyAnswerBatch() {

		this.query 	= 'SELECT Id, Name, Account__c, BIR__c, FaturaDealer__c, FaturaDealer2__c, InvoiceDate__c, '
					+ 'InvoiceNumber__c, MessageText__c, MobileNumber__c, Region1__c, Region2__c, SendDate__c, '
					+ 'SendSurvey__c, Status__c, SurveyAnswered__c, VIN__c '
					+ 'FROM SurveySent__c '
					+ 'WHERE SendDate__c >=: daysBeforeIni AND SendDate__c <=: daysBeforeFim  AND Status__c = \'Delivered\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('!!!'+Database.query(this.query));
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		SurveyAnswer instance = new SurveyAnswer(scope);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}	
}