/**
    Class   -   VFC03_InsertSObjectsRecordsForTest_Test
    Author  -   RameshPrabu
    Date    -   27/09/2012
    
    #01 <RameshPrabu> <27/09/2012>
        Created this class using test for VFC03_InsertSObjectsRecordsForTestClass.
**/
@isTest 
private class VFC03_InsertSObjectsRecordsForTest_Test {

/**    static testMethod void unitTest01()
*    {
*         Map<String,ID> recTypeIds = new Map<String,ID>();
*        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where 
*                            (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc','Personal_Acc'))]) 
*        {
*            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
*        }
*        List<RCM_Recommendation__c> recomm = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRecommendationObject();
*        List<RDT_RelationshipDecisionTree__c> RelationshipTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRelationshipDecisionTreeObject();
*        List<NCT_NonCustomerDecisionTree__c> NonCustomerTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToNonCustomerRelationshipObjects();
*        List<CDT_CustomerDecisionTree__c> CustomerTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToCustomerRelationshipObjects();
*        List<AST_AfterSalesDecisionTree__c> AfterSales = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToAfterSalesDecisionTreeObjects();
*        
*        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
*        
*        List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
*        List<Contact> lstContact =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToContact(lstDealerRecords);
*        
*        // Já é chamado detro do método insertRecordsToVehRelObjects
*        //List<VEH_Veh__c> lstvehicles =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehicleObjects();
*        
*        //Aqui
*        List<VRE_VehRel__c> lstVehRelations =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehRelObjects(lstDealerRecords);
*        List<Product2> lstProducts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
*        
*        List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
*       // system.debug('Account Info....' +lstAccounts[0]);
*        
*
*        
*        List<TST_Transaction__c> lstTransactions = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToTransactionObject();
*        Account acc =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().singleAccountInsertion();
*        
*        // Aqui
*        List<Opportunity> lstOpportunity =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToOpportunity(lstDealerRecords);
*        List<Opportunity> lstOpps = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsTo_Opportunity( lstAccounts[0].Id, lstAccounts[0].Name, '' );
*        List<VEH_Veh__c> lstVehs = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
*        
*        
*        //List<TDV_TestDriveVehicle__c> lstTestDrive =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
*        List<VRE_VehRel__c> lstVehRels = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRelationRecordsInsertion(lstAccounts[0], lstVehs[0]);
*       
*        List<TDV_TestDriveVehicle__c> lstTestDriveVehicleRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
*        
*         
*        //List<TDV_TestDrive__c> lstTDVs =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDrive_RecordsInsertion( lstDealerRecords[0], lstTestDriveVehicleRecords[0] );
*        Test.startTest();
*        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
*        Test.stopTest();
*        System.assert(leads.size() > 0);
*       // System.assert(acconts.size() > 0);
*       VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead(leads[0].Id);
*       User user = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertUserObjects();
*      
*    }
**/

    static testMethod void unitTest02()
    {
        // Prepare test data
        VFC03_InsertSObjectsRecordsForTestClass vfc03 = VFC03_InsertSObjectsRecordsForTestClass.getInstance();      

        // Start test
        Test.startTest();

        List<CS_Models__c> test01 = vfc03.insertCSModels();
        List<Vehicle_Item__c> test02 = vfc03.insertRecordToVehicleItem();

        // Stop test
        Test.stopTest();

        // Valdidate data
        system.assert(test01.size() > 0, 'Test#01: list is empty');
        system.assert(test02.size() > 0, 'Test#02: list is empty');
    }
}