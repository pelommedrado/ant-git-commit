/**
* Classe que representa exceções geradas na atualização de veículo.
* @author Christian Ranha.
*/
public class VFC93_UpdateVehicleException extends Exception {

	public VFC93_UpdateVehicleException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}