/*****************************************************************************************
    Name            : Rforce_CommercialOffersPush_CTR
    Description     : This class is used to Push data to Adobe Campaign and update the "RequesttoAdobe" Flag.
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Implemented By  : Ashok Muntha
 
    Description     : Commercial Offer V3 changes as a part of 16.1 sprint.
    Release Number  : 9.1
    Modified Date   : 08/12/2015
    Modified By     : Venkatesh Kumar
    
 
******************************************************************************************/
global class Rforce_CommercialOffersPush_CTR {

/**
 * @author Venkatesh Kumar
 * @date 08/12/2015
 * @description This method is used to push the offer related values to adobe from the Send to Adobe button using Java Script.
 * @Param strEmailId the email id received from Send to Adobe button using Java Script
 * @Param strImageUrl the image url received from Send to Adobe button using Java Script
 * @Param strCommId the Offer Id received from Send to Adobe button using Java Script
 * @Param strAccId the Account Id received from Send to Adobe button using Java Script
 * @Param strCaseId the Case Id url received from Send to Adobe button using Java Script
 * @return strOfferResponseID
 */
 
 webservice static String pushOffer(String strEmailId, String strImageUrl,String strCommId,String strAccId,String strCaseId, String strExternalId){

     String strResponse;     
     String strEventType;
     String strMyRMyD;
    // List<Rforce_CommercialOfferDetails_CLS> list_OfferDetailsLst = new List<Rforce_CommercialOfferDetails_CLS>();
     Rforce_CommercialOfferDetails_CLS OfferDetails = new Rforce_CommercialOfferDetails_CLS();
     
     system.debug('### Rforce_CommercialOffersPush_CTR : strEmailId -> '+ strImageUrl);
     system.debug('### Rforce_CommercialOffersPush_CTR : strImageUrl -> '+ strImageUrl);
     system.debug('### Rforce_CommercialOffersPush_CTR : strCommId -> '+ strCommId);
     system.debug('### Rforce_CommercialOffersPush_CTR : strAccId -> '+ strAccId);
     system.debug('### Rforce_CommercialOffersPush_CTR : strCaseId -> '+ strCaseId);
     system.debug('### Rforce_CommercialOffersPush_CTR : externalId -> ' + strExternalId);
     
     Commercial_Offer__c cOfferDetails = [Select Name,Offer_Validity_Date__c,OfferLabel__c,Offer_Id__c,OfferDescriptionMkt__c,OfferDescriptionLeg__c,Image_URL__c,Brand__c,Target__c,Offer_Level__c,MyR_Offers__c,OfferType__c,AS_Offer_Detail__c,OfferDescriptionEli__c from Commercial_Offer__c where Id =: strCommId];
     Account accountDetails = [Select ID,Firstname,LastName,PersonTitle,Salutation,CustmrStatus__c,MyDaciaID__c,MyRenaultID__c,CompanyID__c,CustomerIdentificationNbr__c,PersMobPhone__c from Account where id =:strAccId];
              
     
     OfferDetails.strAccFirstName = accountDetails.Firstname;
     OfferDetails.strAccLastName = accountDetails.LastName;
     OfferDetails.strAccOfferTitle = accountDetails.PersonTitle;
     OfferDetails.strAccOfferSalutation = accountDetails.Salutation;
     OfferDetails.strAcctCompanyID = accountDetails.CompanyID__c;
     OfferDetails.strAccountID = accountDetails.ID;
     OfferDetails.strAcctCustomerIdentificationNumber = accountDetails.CustomerIdentificationNbr__c;
     OfferDetails.strAcctPhonenumber = accountDetails.PersMobPhone__c;
     OfferDetails.dteCommofferValidityDate = cOfferDetails.Offer_Validity_Date__c;
     OfferDetails.strComofferMarketingDesc = cOfferDetails.OfferDescriptionMkt__c;
     OfferDetails.strCommofferName = cOfferDetails.Offer_Id__c;
     OfferDetails.strLegalConditions = cOfferDetails.OfferDescriptionLeg__c;
     OfferDetails.strOfferLabel = cOfferDetails.OfferLabel__c;
     OfferDetails.strEligibility = cOfferDetails.OfferDescriptionEli__c;
     
     System.debug('cOfferDetails.OfferDescriptionEli__c--------->'+cOfferDetails.OfferDescriptionEli__c);
     
    If (cOfferDetails.AS_Offer_Detail__c == '' || cOfferDetails.AS_Offer_Detail__c == null ){
        OfferDetails.strOffercode = (cOfferDetails.Brand__c + cOfferDetails.Target__c + cOfferDetails.Offer_Level__c + cOfferDetails.MyR_Offers__c + cOfferDetails.OfferType__c);
    }else{
        OfferDetails.strOffercode = (cOfferDetails.Brand__c + cOfferDetails.Target__c + cOfferDetails.Offer_Level__c + cOfferDetails.MyR_Offers__c + cOfferDetails.OfferType__c + cOfferDetails.AS_Offer_Detail__c);    
    }
    If (strCaseId != '') {
        Case caseDetails =[Select VIN__c from Case where CaseNumber =: strCaseId] ;
         System.debug('caseDetails---------------->'+caseDetails);
        If (caseDetails.VIN__c != null){
            System.debug('caseDetails.VIN__c---------------->'+caseDetails.VIN__c);
         VEH_Veh__c cVehicleModList=[Select Name,YearofManufacture__c,Model__c From VEH_Veh__c where Id IN (Select VIN__c from Case where CaseNumber=: strCaseId) ];
        OfferDetails.strVenhVIN = cVehicleModList.Name;
        OfferDetails.strVenhVehicleModel = cVehicleModList.Model__c;
        OfferDetails.strVenhYearModel = cVehicleModList.YearofManufacture__c;
                }        
      }
    else{
        OfferDetails.strVenhVIN = '';
        OfferDetails.strVenhVehicleModel = '';
        OfferDetails.strVenhYearModel = '';
    }
    
    System.debug('OfferDetails--------->'+OfferDetails);
    if((accountDetails.MyRenaultID__c != null && accountDetails.MyRenaultID__c !='') || (accountDetails.MyDaciaID__c != null && accountDetails.MyDaciaID__c !='')){
    strMyRMyD = System.Label.Rforce_OFF_MyRMyD_OK;
    } else {
    strMyRMyD = System.Label.Rforce_OFF_MyRMyD_KO;
    }
     
    if(accountDetails.CustmrStatus__c == System.Label.Rforce_ACC_Status_Customer && strMyRMyD == System.Label.Rforce_OFF_MyRMyD_KO) {
      // strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterMyRcustomer;
        strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterNMNcustomer;
    }
    else if(strMyRMyD == System.Label.Rforce_OFF_MyRMyD_OK){
    strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterMyRcustomer;
    }
    else if((accountDetails.CustmrStatus__c == System.Label.Rforce_ACC_Status_OldCustomer || accountDetails.CustmrStatus__c == System.Label.Rforce_ACC_Status_Prospect)){
      strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterProspect;
    }
    System.debug('event type==>'+strEventType);
   /* if((accountDetails.CustmrStatus__c == System.Label.Rforce_ACC_Status_OldCustomer || accountDetails.CustmrStatus__c == System.Label.Rforce_ACC_Status_Prospect) && strMyRMyD == 'ko'){
        strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterNMNcustomer;
    }
    else{
        strEventType = System.Label.Rforce_OFF_EventType_renMsgSFCallCenterProspect;
    }*/
    system.debug('OfferDetails-------------------------->'+OfferDetails);
    strResponse = Rforce_CommercialOffers_WS.pushOffer(strEmailId, strImageUrl, strEventType, OfferDetails, strExternalId); 
    system.debug('response--->'+strResponse);
     
    // THis response will return as xml
     
    Commercial_Offer__c cObj = new Commercial_Offer__c();
    Commercial_Offer__c cSelectObj = new Commercial_Offer__c();
     
    Rforce_CommercialOffersParsing_CLS obj = new Rforce_CommercialOffersParsing_CLS();
    String strOfferResponseID = obj.parsePushEvents(strResponse);
 
    if(strAccId != null && strCommId != null){
        cSelectObj = [Select RequesttoAdobe__c from Commercial_Offer__c where Id =:strCommId and Account__c =:accountDetails.Id];
            try{  
                if(cSelectObj != null){         
                    if(!String.isEmpty(strOfferResponseID)){             
                         cSelectObj.RequesttoAdobe__c = System.Label.Rforce_CommercialOffer_Yes;
                         System.debug('strOfferResponseID ==>'+strOfferResponseID);
                    }else{
                         cSelectObj.RequesttoAdobe__c = System.Label.Rforce_CommercialOffer_No;
                    }
                    update cSelectObj;
                }
            }catch(Exception ex) {
              system.debug('Rforce_CommercialOffersPush_CTR-->METHOD:init Exception : :'+ ex.getMessage());
            }    
    }     
     return strOfferResponseID;
   }
   
    /**
    * @Author : Chandra Kanth
    * @Date : 28/11/2016
    * @Description : Release 17.01 - This method is used to delete the offer history record if offer failed to sent to adobe from the Send to Adobe button using Java Script.
    * @Param strOfferHistoryId the OfferHistory record id which should be deleted and received from Send to Adobe button using Java Script
    */
    webservice static void deleteOfferHistory(String strOfferHistoryId){
        try {   
            List<OfferHistory__c> offHistory = [SELECT id FROM OfferHistory__c WHERE id =:strOfferHistoryId];
            delete offHistory;
        } catch(DmlException de) {
            System.debug('### Rforce_CommercialOffersPush_CTR : deleteOfferHistory Exception -> '+ de.getMessage());
        }
    }

}