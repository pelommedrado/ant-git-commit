@isTest
private class VFC142_RMCOpportunity_ListingControlTest {
  
  private static User loadObjects (String userType) {
    Account acc = new Account( Name = 'AR Motors Alphaville' );
    acc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
    acc.ShippingCity = 'Barueri';
    acc.ShippingState = 'SP';
    acc.ShippingCountry = 'Brasil';
    acc.ShippingPostalCode = '';
    acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'Network_Site_Acc' limit 1].Id;
    acc.IDBIR__c = '123';
    acc.NameZone__c = 'R1';
    Database.insert( acc );
    
    Contact cnt1 = new Contact( FirstName = 'Test', LastName = 'Contact1', AccountId = acc.Id ),
      cnt2 = new Contact( FirstName = 'Test', LastName = 'Contact2', AccountId = acc.Id );
    Database.insert( new List< Contact >{ cnt1, cnt2 } );
    
    Profile sellerProfile = [select Id from Profile where Name = 'SFA - Seller'],
      receptionistProfile = [select Id from Profile where Name = 'SFA - Receptionist'];
      
    if( userType == 'Seller' ){
	    User seller = new User();
	    seller.Email = 'test@org.com';
	    seller.LastName = 'TestingUSer';
	    seller.Username = 'test@org1.com';
	    seller.Alias = 'tes';
	    seller.ProfileId = sellerProfile.Id;
	    seller.EmailEncodingKey = 'UTF-8';
	    seller.LanguageLocaleKey = 'pt_BR';
	    seller.LocaleSidKey = 'pt_BR';
	    seller.TimeZoneSidKey = 'America/Los_Angeles';
	    seller.CommunityNickname = 'testing';
	    seller.BIR__c = '123';
	    seller.contactId = cnt1.id;
        seller.isCac__c = true;
	    Database.insert( seller );
	    
	    return seller;
    }else if( userType == 'Receptionist' ){
	    User receptionist = new User();
	    receptionist.Email = 'recptionist.test@org.com';
	    receptionist.LastName = 'TestingReceptionist';
	    receptionist.Username = 'recptionist.test@org1.com';
	    receptionist.Alias = 'recep';
	    receptionist.ProfileId = receptionistProfile.Id;
	    receptionist.EmailEncodingKey = 'UTF-8';
	    receptionist.LanguageLocaleKey = 'pt_BR';
	    receptionist.LocaleSidKey = 'pt_BR';
	    receptionist.TimeZoneSidKey = 'America/Los_Angeles';
	    receptionist.CommunityNickname = 'recptionist.testing';
	    receptionist.BIR__c = '123';
	    receptionist.contactId = cnt2.id;
        receptionist.isCac__c = true;
	    Database.insert( receptionist );
	    
      return receptionist;
    }
    return null;
  }
  static testmethod void runAsReceptionist(){
    System.runAs( loadObjects( 'Receptionist' ) ){
      VFC142_RMCOpportunity_ListingController controller = new VFC142_RMCOpportunity_ListingController();
      List< String > tabLabels = VFC142_RMCOpportunity_ListingController.tabLabels;
      String welcomeMessage = controller.welcomeMessage;
    }
  }
  static testmethod void runAsSeller(){
    System.runAs( loadObjects( 'Seller' ) ){
      VFC142_RMCOpportunity_ListingController controller = new VFC142_RMCOpportunity_ListingController();
      controller.reloadTabs();
      List< String > tabLabels = VFC142_RMCOpportunity_ListingController.tabLabels;
      String welcomeMessage = controller.welcomeMessage;
    }
  }
  
}