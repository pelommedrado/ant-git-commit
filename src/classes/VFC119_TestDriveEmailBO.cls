public class VFC119_TestDriveEmailBO
{
  private static final VFC119_TestDriveEmailBO instance = new VFC119_TestDriveEmailBO();

    private VFC119_TestDriveEmailBO(){}

    public static VFC119_TestDriveEmailBO getInstance(){
        return instance;
    }


  //
  // TODO: description
  //
  public String sendTestDriveEmail(String emailType, Account dealerAccount, String name, String emailCustomer, String phone, String cep, 
        String vehicle, Datetime DateHour, Id custumerId)
    {
        String errorMessage = '';

        try
        {
            String emailTo = '';
            if (emailType == 'template06' || emailType == 'template07')
                emailTo = dealerAccount.Email__c;
            else if (emailType == 'template04' || emailType == 'template05')
                emailTo = emailCustomer;

            String template = '';
            if (emailType ==  'template04')
                template = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate04(dealerAccount, name, phone, cep, vehicle, DateHour);
            else if (emailType ==  'template05')
                template = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate05(dealerAccount, name, phone, cep, vehicle, DateHour);
            else if (emailType ==  'template06')
                template = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate06(dealerAccount, name, phone, cep, vehicle, DateHour);
            else if (emailType ==  'template07'){
                template = VFC117_TestDriveEmailTemplate.getInstance().buildTemplate07(dealerAccount, name, emailCustomer, phone, cep, vehicle, DateHour);
              
              errorMessage = saveTestDriveEmail(dealerAccount.Id, custumerId, DateHour, vehicle);
              
            }
    
        errorMessage = VFC118_UTIL_Email.getInstance().sendEmailHtml(emailTo, Label.EmailTDSubject, template);
        }
        catch (Exception e) {
            errorMessage = e.getMessage();
        }
        return errorMessage;
    }
    
     private static String saveTestDriveEmail(Id dealer, Id account, Datetime dateOfBooking,  String vehicleBooking){
    
    System.debug('Registrando Test Drive Email' );
    TDE_TestDriveEmail__c sObjTestDriveEmail = new TDE_TestDriveEmail__c();
    
    /* fetch testdrivevehicle records using dealer id and car model */
        TDV_TestDriveVehicle__c testDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingDealerId( dealer, vehicleBooking );
        if (testDriveVehicle == null) {
            return 'Unhandled error: Veículo não encontrado';
        }
                 
       sObjTestDriveEmail.Dealer__c = dealer;
       sObjTestDriveEmail.Account__c= account;
       sObjTestDriveEmail.DateBooking__c = dateOfBooking;
       sObjTestDriveEmail.Test_Drive_Vehicle__c = testDriveVehicle.Id;
       
       Database.SaveResult sr = null;
        
            sr = Database.insert(sObjTestDriveEmail);
        try 
        {
          System.debug('********** sObjTestDriveEmail: ' + sObjTestDriveEmail);
        } 
        catch(DMLException ex) 
        {
            for (Integer i = 0; i < ex.getNumDml(); i++) {
            // Process exception here
            System.debug(ex.getDmlMessage(i)); 
            return ex.getDmlMessage(i);
        }
        }
       
       return '';
    
  }
}