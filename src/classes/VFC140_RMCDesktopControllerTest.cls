@isTest
private class VFC140_RMCDesktopControllerTest {
	//Profile Seller
    static testMethod void myUnitTest() {
        Profile SellerProfile = [select id from Profile where Name= 'SFA - Seller' limit 1];
        
        Account parentAcc = new Account (Name ='ParentAcc');
        parentAcc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        parentAcc.ShippingCity = 'Barueri';
        parentAcc.ShippingState = 'SP';
        parentAcc.ShippingCountry = 'Brasil';
        parentAcc.ShippingPostalCode = '';
        parentAcc.Phone = '11999999999';
		parentAcc.ProfEmailAddress__c ='teste@teste.com'; 
		parentAcc.RecordTypeId = [select id from RecordType where developerName='Network_Acc' limit 1 ].id;
        database.insert(parentAcc);
        
        Account acc = new Account(Name = 'AR Motors Alphaville', parentID = parentAcc.id);
        acc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        acc.ShippingCity = 'Barueri';
        acc.ShippingState = 'SP';
        acc.ShippingCountry = 'Brasil';
        acc.ShippingPostalCode = '';
        acc.RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id;
        acc.IDBIR__c ='123';
        acc.NameZone__c = 'R1';
        database.insert(acc);
        
        Contact contato = new Contact (FirstName = 'Test', LastName='Teste', AccountID=acc.id);
        database.insert(contato);
        
        User u = new User();
        u.Email = 'test@org.com';
        u.LastName = 'TestingUSer';
        u.Username = 'test@org1.com';
        u.Alias = 'tes';
        u.ProfileId = SellerProfile.id;
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.CommunityNickname = 'testing';
        u.isCac__c = true;
        u.BIR__c ='123';
        u.contactID=contato.id;
        u.isCac__c = true;
        database.insert(u);
        
        system.runAs(u){
        	VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsTo_Opportunity(acc.id, acc.Name, 'Veiculo de Test');
        	PageReference pageRef = Page.VFP20_RMCDesktop;
	        Test.setCurrentPage(pageRef);
	        VFC140_RMCDesktopController controller = new VFC140_RMCDesktopController();
        }
    }
    //Profile Receptionist
    static testMethod void myUnitTest2() {
        VFP20_DesktopMessages__c msg = new VFP20_DesktopMessages__c();
        msg.Welcome_message_R1_en_US__c = 'Test Message';
        msg.Welcome_message_R1_pt_BR__c = 'Mensagem de Teste';
        database.insert(msg);
        
        Profile SellerProfile = [select id from Profile where Name= 'SFA - Receptionist' limit 1];
        
        Account parentAcc = new Account (Name ='ParentAcc');
        parentAcc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        parentAcc.ShippingCity = 'Barueri';
        parentAcc.ShippingState = 'SP';
        parentAcc.ShippingCountry = 'Brasil';
        parentAcc.ShippingPostalCode = '';
        parentAcc.Phone = '11999999999';
		parentAcc.ProfEmailAddress__c ='teste@teste.com'; 
		parentAcc.RecordTypeId = [select id from RecordType where developerName='Network_Acc' limit 1 ].id;
        database.insert(parentAcc);
        
        Account acc = new Account(Name = 'AR Motors Alphaville', parentID = parentAcc.id);
        acc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        acc.ShippingCity = 'Barueri';
        acc.ShippingState = 'SP';
        acc.ShippingCountry = 'Brasil';
        acc.ShippingPostalCode = '';
        acc.RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id;
        acc.IDBIR__c ='123';
        acc.NameZone__c = 'R1';
        database.insert(acc);
        
        Contact contato = new Contact (FirstName = 'Test', LastName='Teste', AccountID=acc.id);
        database.insert(contato);
        
        User u = new User();
        u.Email = 'test@org.com';
        u.LastName = 'TestingUSer';
        u.Username = 'test@org1.com';
        u.Alias = 'tes';
        u.ProfileId = SellerProfile.id;
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.CommunityNickname = 'testing';
        u.ProfileId = SellerProfile.id;
        u.BIR__c ='123';
        u.contactID=contato.id;
        u.isCac__c = true;
        database.insert(u);
        Task tsk = new Task( ownerID = u.id);
        database.insert(tsk);
        
        system.runAs(u){
        	VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsTo_Opportunity(acc.id, acc.Name, 'Veiculo de Test');
        	PageReference pageRef = Page.VFP20_RMCDesktop;
	        Test.setCurrentPage(pageRef);
	        VFC140_RMCDesktopController controller = new VFC140_RMCDesktopController();
	        String taskPrefix = controller.taskPrefix;
	        String message = controller.welcomeMessage;
	        System.debug('Taks Prefix: ' + taskPrefix);
	        system.debug('Message: ' + message);
        }
    }
}