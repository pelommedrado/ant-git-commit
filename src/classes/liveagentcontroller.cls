public with sharing class liveagentcontroller {
  
  
    public String Casesubject { get; set; }
    public String telephone { get; set; }
    public String cpf { get; set; }
    public String email { get; set; }
    public String lastName { get; set; }
    public String firstName { get; set; }
    public String[] countries = new String[]{};
   
    public PageReference redirect() {
    Pagereference pagetwoVFP=Page.PrechatredirectPage;
    return pagetwoVFP;
    }
    
     public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Promoções','Promoções'));
            options.add(new SelectOption('Reclamações','Reclamações'));
            options.add(new SelectOption('Informações','Informações'));
             options.add(new SelectOption('Sugestões','Sugestões'));
            return options;
        }
        
           public String[] getCountries() {
           return countries;
        }
            
        public void setCountries(String[] countries) {
            this.countries = countries;
        }

}