global class VFC125_CampaignSchedule implements Schedulable
{ 
	global void execute(SchedulableContext ctx)
	{
		VFC124_CampaignBatch batch = new VFC124_CampaignBatch();
        Database.executeBatch(batch, 200);
	}
}