@isTest
public class SFA_PassingPurchaseControllerTest{
    
    static testMethod void MyTest(){
        Objects();
        SFA_PassingPurchaseController p = new SFA_PassingPurchaseController();
        List<PassingAndPurchaseOrder__c> pa = p.lsPurchase;
        PageReference pg = p.novo();      
    }
    
    static void Objects(){
        account a = new account(
            Name = 'teste',
            RecordTypeId = [select id from RecordType where Name =: 'Network Site Account'].Id,
            IDBIR__c = '145263'
        );
        Database.insert(a);
        
        Contact c = new Contact(
            AccountId = a.Id,
            FirstName = 'Nome',
            LastName = 'TestContact'
        );
        Database.insert(c);
        
        User u = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            IsActive = true,
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert(u);
        
        User partnerUser = new User(
            FirstName = 'TestPartner',
            LastName = 'LastNameUser',
            Email = 'test@orgPartner.com',
            Username = 'test@org2.com',
            Alias = 'tAlias',
            IsActive = true,
            EmailEncodingKey='UTF-8',
            ContactId = c.Id,
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'test2',
            ProfileId = [select Id from Profile where Name = 'ALR - Dealer'].Id,
            BIR__c ='123ABC123',
           	isCac__c = true
        );
        Database.insert(partnerUser);
        
        system.runAs(partnerUser){      
            PassingAndPurchaseOrder__c pa = new PassingAndPurchaseOrder__c(
                Account__c = a.Id
            );
            Database.insert(pa);
            
        } 
    }
}