public class Utils 
{
    private static Map< String, Map< String, Id > > recordTypeMap;
    private static Map< String, Id > profileMap;
    private static Map< String, Id > userRoleMap;
    private static Map< String, Schema.SObjectType > mapGlobalDescribe = Schema.getGlobalDescribe();

    public static String getResourceURL(String resourceName) 
    {
        List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp 
                                             FROM StaticResource 
                                             WHERE Name = :resourceName];
        
        if (resourceList.size() == 1) 
        {
            String namespace = resourceList[0].NamespacePrefix;
            
            String response ='/resource/' 
                + resourceList[0].SystemModStamp.getTime() + '/' 
                + (namespace != null && namespace != '' ? namespace + '__' : '') 
                + resourceName;
            System.debug('Utils.getResourceURL Response>>>>>>>'+response);        
            return response; 
        } 
        else 
            return '';
    }
    
    public static String baseURL{
        get{
            return Utils.baseURL();
        }
    }
    
    /**
* Remover a formatacao de telefone
*/
    public static String limparFormatacaoTel(String param) {
        return param.replaceAll('[)(-]', '');
    }
    
    /**
* Remover a formatacao do CPF ou CNPJ
*/
    public static String limparFormatacaoCpfCnpj(String param) {
        return param.replaceAll('[/\\.-]', '');
    }
    
    public static String parseErrorMessage (Exception e){
        return Utils.parseErrorMessage( e.getMessage() );
    }
    
    public static String parseErrorMessage (String msg){
        Pattern p = Pattern.compile( '.* [A-Z|_]+, (.*): \\[(.*)]' );
        Matcher m = p.matcher( msg );
        
        return m.matches() && m.find( 1 ) ? m.group( 1 ) : msg;
        
    }
    
    public static String valueOrBlank (String value){
        return String.isEmpty( value ) ? '' : value;
    }
    
    public static Id getRecordTypeId (String SObjectType, String DeveloperName){
        if( String.isEmpty( SObjectType ) || String.isEmpty( DeveloperName ) )
            throw new FetchException( 'You must provide valid SObject type and developer name arguments.' );
        
        if( recordTypeMap == null ){
            recordTypeMap = new Map< String, Map< String, Id > >();
            for( RecordType recType : [select Id, SObjectType, DeveloperName from RecordType] ){
                Map< String, Id > SObjectMap = recordTypeMap.get( recType.SObjectType );
                if( SObjectMap == null ){
                    SObjectMap = new Map< String, Id >{recType.DeveloperName => recType.Id};
                        recordTypeMap.put( recType.SObjectType, SObjectMap );
                }else SObjectMap.put( recType.DeveloperName, recType.Id );
            }
        }

        Map< String, Id > SObjectMap = recordTypeMap.get( SObjectType );
        if( SObjectMap == null ) throw new FetchException( 'Invalid SObject type.' );

        Id recTypeId = SObjectMap.get( DeveloperName );
        if( recTypeId == null ) throw new FetchException( 'Invalid developer name "' + DeveloperName + '" for SObject type "' + SObjectType + '".' );

        return recTypeId;
    }
    
    public static Id getProfileId (String profileName){
        if( String.isEmpty( profileName ) )
            throw new FetchException( 'You must provide a valid profile name argument.' );
        
        if( profileMap == null ){
            profileMap = new Map< String, Id >();
            for( Profile prof : [select Id, Name from Profile] )
                profileMap.put( prof.Name, prof.Id );
        }
        
        Id profileId = profileMap.get( profileName );
        if( profileId == null ) throw new FetchException( 'Invalid profile name.' );
        
        return profileId;
    }
    
    
    public static Id getUserRoleId (String userRoleName){
        if( String.isEmpty( userRoleName ) )
            throw new FetchException( 'You must provide a valid user role name argument.' );
        
        if( userRoleMap == null ){
            userRoleMap = new Map< String, Id >();
            for( UserRole role : [select Id, Name from UserRole] )
                userRoleMap.put( role.Name, role.Id );
        }
        
        Id userRoleId = userRoleMap.get( userRoleName );
        if( userRoleId == null ) throw new FetchException( 'Invalid user role name.' );
        
        return userRoleId;
    }
    
    public static Id getSystemAdminProfileId (){
        try{
            return [select Id from Profile where UserType = 'Standard' and PermissionsAuthorApex = true order by CreatedDate asc limit 1].Id;
        }catch (Exception e){
            throw new FetchException( 'Could not determine system admin profile. This is bad.' );
        }
    }
    
    public static String baseURL(){
        return URL.getCurrentRequestUrl().toExternalForm().substringBefore( '/apex' );
    }
    
    
    
    public static List<RecordType> getRecordType (String SObjectType, String developerName){
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType = \''+SObjectType+'\' and DeveloperName LIKE \'%'+developerName+'%\' order by  Name limit 300 ';
        return Database.query(url);
    }
    public static List<RecordType> getRecordType (String SObjectType){
        return [ select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType =:SObjectType ];
    }
    
    public static List<RecordType> getRecordType(Id idRecordType){
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where Id = \''+idRecordType+'\'';
        return Database.query(url);        
    }
    
    public static Map<Id,RecordType> getMapRecordType(String sObjectT){
        String url = 'select Id, SObjectType, DeveloperName, Name from RecordType where SObjectType = \''+sObjectT+'\' ';
        List<RecordType> lsRec =Database.query(url);
        Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>(lsRec);
        return mapRecordType;
    }
    
    
    public static List< SelectOption > getPicklistValues (String SObjectName, String fieldName){
        if( String.isBlank( SObjectName ) || String.isBlank( fieldName ) )
            throw new FetchException( 'You must provide valid SObject name and field name arguments.' );
        List< SelectOption > retList = new List< SelectOption >();
        Schema.SObjectType SObjType = Schema.getGlobalDescribe().get( SObjectName );
        
        if( SObjType == null ) throw new FetchException( 'Could not get describe information for SObject "' + SObjectName + '".' );
        Schema.SObjectField field = SObjType.getDescribe().fields.getMap().get( fieldName );
        
        if( field == null ) throw new FetchException( 'Could not get describe information for "' + fieldName + '" field of "' + SObjectName + '" SObject.' );
        
        for( Schema.PicklistEntry entry : field.getDescribe().getPicklistValues() )
            retList.add( new SelectOption( entry.getValue(), entry.getLabel() ) );
        
        return retList;
    }

    public static List<SelectOption> buildSelectOptions(List<SObject> sObjList, String value, String label){
        List<SelectOption> retList = new List<SelectOption>();

        if(sObjList == null) return retList;

        for(SObject sObj : sObjList)
            retList.add(new SelectOption(String.valueOf(sObj.get(value)), String.valueOf(sObj.get(label))));
        return retList;
    }
    
    public static Map<Object, SObject> getSObjectMap(String objeto, String campoChave, String whereCondition){
        Map<Object, SObject> sObjectMap = new Map<Object, SObject>();
        
        Map<String, Schema.SObjectField> fieldMap = mapGlobalDescribe.get(objeto).getDescribe().fields.getMap();
        
        String query = 'SELECT ';
        
        for(String fieldName : fieldMap.keySet()){
            query += fieldName + ',';
        }
        
        query = query.substring(0, query.length()-1) + ' FROM ' + objeto + ' ' + whereCondition;
        
        system.debug('@@@ getSObjectMap query: ' + query);
        
        
        for(SObject obj : Database.query(query))
            sObjectMap.put(obj.get(fieldMap.get(campoChave)), obj);
        
        
        return sObjectMap;
    }
    
    public static Map<Object, SObject> getSObjectMap(String objeto, String campoChave, String idField, Set<Id> setIds){
       Map<Object, SObject> sObjectMap = new Map<Object, SObject>();
       
       Map<String, Schema.SObjectField> fieldMap = mapGlobalDescribe.get(objeto).getDescribe().fields.getMap();
       
       String query = 'SELECT ';
       
       for(String fieldName : fieldMap.keySet()){
           query += fieldName + ',';
       }
       
       String whereCondition = '';
       
       if(!setIds.isEmpty()){
           whereCondition = 'WHERE ' + idField + ' in (';
           
           for(Id i : setIds){
               whereCondition += '\''+i+'\',';
           }
           
           whereCondition = whereCondition.substring(0, whereCondition.length()-1) + ')';
       }
       
       query = query.substring(0, query.length()-1) + ' FROM ' + objeto + ' ' + whereCondition;
       
       system.debug('@@@ getSObjectMap query: ' + query);
       
       
       for(SObject obj : Database.query(query))
           sObjectMap.put(obj.get(fieldMap.get(campoChave)), obj);
       
       
       return sObjectMap;
   }
    
    /* BY @PUDIM */
    public static Map<Id, List<PermissionSet>> getUserPermissionSetsMap(Set<Id> usersIds){
        Map<Id, List<PermissionSet>> mapPermissions = new Map<Id, List<PermissionSet>>();
        Map<Object, SObject> mapPermissionSets = getSObjectMap('PermissionSet', 'Id', 'Id', getUsersPermissionSetsIds(usersIds));
        
        List<PermissionSetAssignment> listPermissionAssigment = [
            SELECT AssigneeId, PermissionSetId 
            FROM PermissionSetAssignment 
            WHERE AssigneeId in :usersIds
        ];
        
        for(Id userId : usersIds){
            List<PermissionSet> userPermissions = new List<PermissionSet>();
            
            for(PermissionSetAssignment psa : listPermissionAssigment){
                if(userId == psa.AssigneeId && mapPermissionSets.containsKey(psa.PermissionSetId))
                    userPermissions.add( (PermissionSet) mapPermissionSets.get(psa.PermissionSetId) );
            }
            
            mapPermissions.put(userId, userPermissions);
        }
        
        
        return mapPermissions;
    }
    
    public static Set<Id> getUsersPermissionSetsIds(Set<Id> usersIds){
        Set<Id> permissionSetsIds = new Set<Id>();
        
        List<PermissionSetAssignment> listPermissionAssigment = [
            SELECT AssigneeId, PermissionSetId 
            FROM PermissionSetAssignment 
            WHERE AssigneeId in :usersIds
        ];
        
        for(PermissionSetAssignment psa : listPermissionAssigment)
            permissionSetsIds.add(psa.PermissionSetId);
        
        return permissionSetsIds;
    }
    
    public static Datetime now(){
        Datetime now = Datetime.now();
        return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
    }
    
    
    //Monta Map Valor / Label de campos picklist
    public static Map<String, String> getMapLabelPicklist(String objeto, String campo){
        Map<String, String> mapLabel = new Map<String, String>();
        
        try {
            //Schema.DescribeFieldResult fieldDescribe
            Map<String, Schema.SObjectField> fieldMap = mapGlobalDescribe.get(objeto).getDescribe().fields.getMap();
            
            List<Schema.PicklistEntry> pickList = fieldMap.get(campo).getDescribe().getPicklistValues();
            
            //Schema.getGlobalDescribe().get(objeto).getDescribe().fields.getMap().get(campo).getDescribe().getPicklistValues()
            
            for(Schema.PicklistEntry pick : pickList){
                mapLabel.put(pick.getValue(), pick.getLabel());
            }
            
        } catch(Exception e){
            system.debug('@@@ getMapLabelPicklist fail: ' + e.getMessage());
        }
        
        return mapLabel;
    }
    
    //Monta Map API / Label dos campos de um objeto
    public static Map<String, String> getMapLabelFields(String objeto){
        Map<String, String> mapLabel = new Map<String, String>();
        
        try {
            Map<String, Schema.SObjectField> mapSObjectFields = mapGlobalDescribe.get(objeto).getDescribe().fields.getMap();
            
            for(String nomeCampo : mapSObjectFields.keySet())
                mapLabel.put(nomeCampo, mapSObjectFields.get(nomeCampo).getDescribe().getLabel());
            
            
        } catch(Exception e){
            system.debug('@@@ getMapLabelFields fail: ' + e.getMessage());
        }
        
        return mapLabel;
    }
    
    
    public static String generateCpf(){
        return getCpfVerifyingDigits(randomNumberAsString(9));
    }
    
    public static Boolean isCpfValid(String cpf){
        if(String.isEmpty(cpf) || cpf.length() != 11)
            return false;
        
        return getCpfVerifyingDigits(cpf.substring(0, 9)) == cpf;
    }
    
    private static String getCpfVerifyingDigits(String cpfNumber){
        List<String> cpfAsList = new List<String>();
        List<Integer> multiplyNumber;
        Integer cpfSum;
        Integer verificatorX;
        Integer verificatorY;
        
        
        
        system.debug('@@@ cpfNumber 9: ' + cpfNumber);
        
        cpfAsList = cpfNumber.split('');
        for(Integer i=0 ; i<cpfAsList.size() ; i++){
            if(!cpfAsList[i].isNumeric())
                cpfAsList.remove(i);
        }
        system.debug('@@@ cpfAsList in X: ' + cpfAsList);
        
        multiplyNumber = new List<Integer>{10, 9, 8, 7, 6, 5, 4, 3, 2};
        cpfSum = 0;
        
        if(cpfAsList.size() != multiplyNumber.size())
            throw new Utils.GenericException('Algo de errado não está certo');
        
        for(Integer i=0 ; i<9 ; i++){
            cpfSum += Integer.valueOf(cpfAsList[i]) * multiplyNumber[i];
        }
        system.debug('@@@ cpfSum in X: ' + cpfSum);
        
        Integer modCpfSum = Math.mod(cpfSum, 11);
        system.debug('@@@ modCpfSum in X: ' + modCpfSum);
        
        verificatorX = modCpfSum < 2 ? 0 : 11 - modCpfSum;
        system.debug('@@@ verificatorX: ' + verificatorX);
        
        
        
        cpfNumber += String.valueOf(verificatorX);
        system.debug('@@@ cpfNumber 10: ' + cpfNumber);
        
        cpfAsList = cpfNumber.split('');
        for(Integer i=0 ; i<cpfAsList.size() ; i++){
            if(!cpfAsList[i].isNumeric())
                cpfAsList.remove(i);
        }
        system.debug('@@@ cpfAsList in Y: ' + cpfAsList);
        
        multiplyNumber = new List<Integer>{11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
        cpfSum = 0;
        
        if(cpfAsList.size() != multiplyNumber.size())
            throw new Utils.GenericException('Algo de errado não está certo');
        
        for(Integer i=0 ; i<10 ; i++){
            cpfSum += Integer.valueOf(cpfAsList[i]) * multiplyNumber[i];
        }
        system.debug('@@@ cpfSum in Y: ' + cpfSum);
        
        modCpfSum = Math.mod(cpfSum, 11);
        system.debug('@@@ modCpfSum in Y: ' + modCpfSum);
        
        verificatorY = modCpfSum < 2 ? 0 : 11 - modCpfSum;
        system.debug('@@@ verificatorY: ' + verificatorY);
        
        
        
        cpfNumber += String.valueOf(verificatorY);
        system.debug('@@@ cpfNumber 11: ' + cpfNumber);
        
        
        
        return cpfNumber;
    }
    
    public static String generateCnpj(){
        return getCnpjVerifyingDigits(randomNumberAsString(8) + '0001');
    }
    
    public static Boolean isCnpjValid(String cnpj){
        if(String.isEmpty(cnpj) || cnpj.length() != 14)
            return false;
        
        return getCnpjVerifyingDigits(cnpj.substring(0, 12)) == cnpj;
    }
    
    private static String getCnpjVerifyingDigits(String cnpjNumber){
        List<String> cnpjAsList = new List<String>();
        List<Integer> multiplyNumber;
        Integer cnpjSum;
        Integer verificatorX;
        Integer verificatorY;
        
        
        
        system.debug('@@@ cnpjNumber 12: ' + cnpjNumber);
        
        cnpjAsList = cnpjNumber.split('');
        for(Integer i=0 ; i<cnpjAsList.size() ; i++){
            if(!cnpjAsList[i].isNumeric())
                cnpjAsList.remove(i);
        }
        system.debug('@@@ cnpjAsList in X: ' + cnpjAsList);
        
        multiplyNumber = new List<Integer>{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
        cnpjSum = 0;
        
        if(cnpjAsList.size() != multiplyNumber.size())
            throw new Utils.GenericException('Algo de errado não está certo');
        
        for(Integer i=0 ; i<12 ; i++){
            cnpjSum += Integer.valueOf(cnpjAsList[i]) * multiplyNumber[i];
        }
        system.debug('@@@ cnpjSum in X: ' + cnpjSum);
        
        Integer modCnpjSum = Math.mod(cnpjSum, 11);
        system.debug('@@@ modCnpjSum in X: ' + modCnpjSum);
        
        verificatorX = modCnpjSum < 2 ? 0 : 11 - modCnpjSum;
        system.debug('@@@ verificatorX: ' + verificatorX);
        
        
        
        cnpjNumber += String.valueOf(verificatorX);
        system.debug('@@@ cnpjNumber 13: ' + cnpjNumber);
        
        cnpjAsList = cnpjNumber.split('');
        for(Integer i=0 ; i<cnpjAsList.size() ; i++){
            if(!cnpjAsList[i].isNumeric())
                cnpjAsList.remove(i);
        }
        system.debug('@@@ cnpjAsList in Y: ' + cnpjAsList);
        
        multiplyNumber = new List<Integer>{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
        cnpjSum = 0;
        
        if(cnpjAsList.size() != multiplyNumber.size())
            throw new Utils.GenericException('Algo de errado não está certo');
        
        for(Integer i=0 ; i<13 ; i++){
            cnpjSum += Integer.valueOf(cnpjAsList[i]) * multiplyNumber[i];
        }
        system.debug('@@@ cnpjSum in Y: ' + cnpjSum);
        
        modCnpjSum = Math.mod(cnpjSum, 11);
        system.debug('@@@ modCnpjSum in Y: ' + modCnpjSum);
        
        verificatorY = modCnpjSum < 2 ? 0 : 11 - modCnpjSum;
        system.debug('@@@ verificatorY: ' + verificatorY);
        
        
        
        cnpjNumber += String.valueOf(verificatorY);
        system.debug('@@@ cnpjNumber 14: ' + cnpjNumber);
        
        
        
        return cnpjNumber;
    }
    
    public static String randomNumberAsString(Integer size){
        
        String randomNumber = '';
        Boolean validSize = false;
        
        while(!validSize){
            String randomGenerator = String.valueOf(Math.random());
            system.debug('### randomGenerator generated -> ' + randomGenerator);
            
            randomGenerator = randomGenerator.removeStart('0');
            randomGenerator = randomGenerator.removeStart('.');
            system.debug('### randomGenerator removed -> ' + randomGenerator);
            
            randomNumber += randomGenerator.isNumeric() ? randomGenerator : '';
            
            if(randomNumber.length() >= size){
                randomNumber = randomNumber.substring(0, size);
                validSize = true;
            }
        }
        
        system.debug('@@@ randomNumber: ' + randomNumber);
        return randomNumber;
    }
    

    public static String searchCEP(String CEP){
        
        String message = '', rua = '', bairro = '', cidade = '', uf = '', complemento2 = '';
        try{
            System.debug('### CEP '+CEP);
            if(String.isNotEmpty(CEP) && CEP.isNumeric() && CEP.length() == 8){
                
                // xml de chamada webservice dos Correios
                String xml = '<soapenv:Envelope xmlns:soapenv='+
                    '"http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">' +
                    '<soapenv:Header/>' +
                    '<soapenv:Body>' +
                    '<cli:consultaCEP>' +
                    '<cep>'+CEP+'</cep>' +
                    '</cli:consultaCEP>' +
                    '</soapenv:Body>' +
                    '</soapenv:Envelope>';
                
                // Método de envio de requisição
                HttpRequest req = new HttpRequest();
                //verificar as configurações abaixo pois são a que estão no soapui
                req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
                req.setHeader('Content-Length','324');
                req.setHeader('SOAPAction', '');
                req.setHeader('Host','apps.correios.com.br');
                //selecionar a url do servidor
                req.setEndpoint('https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente');
                req.setMethod('POST');
                //setar o xml
                req.setBody(xml);
                
                //envio da requisição e obtenção da resposta
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                res = http.send(req); 
                
                // Log the XML content
                System.debug('### res.getBody(): ' + res.getBody());
                
                if(!res.getBody().contains('CEP NAO ENCONTRADO')){
                    
                    String body = res.getBody();
                    
                    rua = body.split('<end>')[1].split('</end>')[0];
                    bairro = body.split('<bairro>')[1].split('</bairro>')[0];
                    cidade = body.split('<cidade>')[1].split('</cidade>')[0];
                    uf = body.split('<uf>')[1].split('</uf>')[0];
                    complemento2 = body.split('<complemento2>')[1].split('</complemento2>')[0];
                    
                    JSONGenerator generator = JSON.createGenerator(true);
                    generator.writeStartObject();
                    generator.writeStringField('rua', rua);
                    generator.writeStringField('bairro', bairro);
                    generator.writeStringField('cidade', cidade);
                    generator.writeStringField('uf', uf);
                    generator.writeStringField('cep', CEP);
                    generator.writeEndObject();
                    message = generator.getAsString();
                    
                }else{
                    message = 'CEP NÃO ENCONTRADO';
                }
                
            }else{
                message = 'DIGITE UM CEP VÁLIDO';
            }
        }catch(Exception e){
            
        }
        
        
        return message;
    }
    
    private class FetchException extends Exception{}
    public class GenericException extends Exception{}
    
}