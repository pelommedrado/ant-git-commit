public with sharing class Utils_OTSDataSource {

    public Boolean Test;
     
         public WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse getOTSData(VFC05_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
         
        // --- PRE TREATMENT ----
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequest request = new WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequest();       
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequestParms requestParameters = new WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequestParms();
        WS02_otsIcmApvBserviceRenault.ServicePreferences servicePref = new WS02_otsIcmApvBserviceRenault.ServicePreferences();
        requestParameters.vin = VehController.getVin();
        requestParameters.countryLanguage = '?';
        request.requestParms = requestParameters;    
        servicePref.country = System.label.codCountry;  // servicePref.country = 'FR'
        servicePref.language = UserInfo.getLanguage().substring(0, 2).toUpperCase(); // servicePref.language = 'FR'
        
        servicePref.userid = '?';
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        WS02_otsIcmApvBserviceRenault.ApvGetListOts OTSWS = new WS02_otsIcmApvBserviceRenault.ApvGetListOts();
        OTSWS.endpoint_x = System.label.VFP05_OTS_URL;
        OTSWS.clientCertName_x = System.label.RenaultCertificate;     
        OTSWS.timeout_x=10000;
        
        WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse OTSRWS = new WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse();    
    
        if (Test==true) {
            OTSRWS = Utils_Stubs.OTSStub();    
        } else {
            OTSRWS = OTSWS.getListOts(request);
        }
         return OTSRWS;  
     
    }
}