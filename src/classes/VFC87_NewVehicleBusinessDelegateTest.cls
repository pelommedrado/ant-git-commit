@isTest(seeAllData=true)
private class VFC87_NewVehicleBusinessDelegateTest {

    public static Quote q;
    public static Model__c modelC;
    public static PVVersion__c version;
    public static Optional__c opt, opt2, opt3;
    public static Product2 prd2;
    public static Product2 prd;
    public static VEH_Veh__c v;
    public static Account dealer;
    public static Account dealer2;

    public static void setup(){

         //Classe para criar os objetos
        MyOwnCreation moc = new MyOwnCreation();
        
        dealer = moc.criaAccountDealer();
        dealer.IDBIR__c = '9876543';
        
        dealer2 = moc.criaAccountDealer();
        dealer2.IDBIR__c = '1234567';
        dealer2.ParentId = dealer.Id;
        
        Account a = moc.criaPersAccount();

        List<Account> accList = new List<Account>();
        accList.add(dealer);
        accList.add(dealer2);
        accList.add(a);
        Insert accList;
        
        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = a.Id;
        opp.Dealer__c = dealer2.Id;
        Insert opp;
        
        prd = moc.criaProduct2();
        
        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;
        
        q = moc.criaQuote();
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb.Id;
        Insert q;
        
        prd2 = moc.criaProduct2();
        prd2.Name = 'CLIO';
        prd2.ModelSpecCode__c = 'Name';
        prd2.Model__c = prd.Id;

        List<Product2> prdList = new List<Product2>();
        prdList.add(prd);
        prdList.add(prd2);
        Insert prdList;

        v = moc.criaVeiculo();
        v.DateofManu__c = system.today();
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        vr.Account__c = dealer2.Id;
        Insert vr;
        
        PricebookEntry pe2 = moc.criaPricebookEntry();
        pe2.Pricebook2Id = pb.Id;
        pe2.Product2Id = prd.Id;
        Insert pe2;
        
        PricebookEntry pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = pb.Id;
        pe.Product2Id = prd2.Id;
        Insert pe;
        
        modelC = new Model__c();
        modelC.Name = 'CLIO';
        modelC.Status_SFA__c = 'Active';
        modelC.Model_PK__c = 'VVV-3';
        insert modelC;
        
        version = new PVVersion__c();
        version.Price__c = 1;
        version.Model__c = modelC.Id;
        version.PVC_Maximo__c = 1;
        version.PVR_Minimo__c = 1;
        version.Version_Id_Spec_Code__c = 'VEC199_BRES';
        insert version;

        opt = new Optional__c(
            Name = 'Azul',
            Type__c = 'Cor',
            Optional_Code__c = '3N4',
            Version__c = version.Id
        );
        
        opt2 = new Optional__c(
            Name = 'harmonia',
            Type__c = 'Harmonia',
            Optional_Code__c = '3U4',
            Version__c = version.Id
        );
        
        opt3 = new Optional__c(
            Name = 'Trim',
            Type__c = 'Trim',
            Optional_Code__c = '3NT',
            Version__c = version.Id
        );

        List<Optional__c> optList = new List<Optional__c>();
        optList.add(opt);
        optList.add(opt2);
        optList.add(opt3);
        Insert optList;

    }
    
    public static testMethod void testIncludeVersionTable(){

        setup();

        Test.startTest();
        
        List<String> stgList = new List<String>();
        stgList.add(opt.Id);

        VFC87_NewVehicleBusinessDelegate.getInstance().includeVersionTable(q.Id, modelC.Id, version.Id, opt.Id, opt3.Id, opt2.Id, stgList);

        Test.stopTest();
        
    }

    public static testMethod void testGetAccountModel(){

        setup();

        Test.startTest();
        
        VFC87_NewVehicleBusinessDelegate.getInstance().getAccountModel(q.Id);

        Test.stopTest();
       
        
    }

    public static testMethod void testSelectModel(){

        setup();

        Test.startTest();
        
        VFC87_NewVehicleBusinessDelegate.getInstance().selectModel();

        Test.stopTest();
        
    }

    public static testMethod void testSelectVersion(){

        setup();

        Test.startTest();
        
        VFC87_NewVehicleBusinessDelegate.getInstance().selectVersion(prd2.Id);

        Test.stopTest();
        
    }

    public static testMethod void testSelectColor(){

        setup();
        
        Test.startTest();

        VFC87_NewVehicleBusinessDelegate.getInstance().selectColor(prd2.Id);
        
        Test.stopTest();
        
    }

    public static testMethod void testIncludeItemVehicle(){

        setup();
        
        Test.startTest();

        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, q.Id, 1000);
        
        Test.stopTest();
        
    }

    public static testMethod void testIncludeItemGeneric(){

        setup();
        
        Test.startTest();

        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemGeneric(prd.Id, q.Id);
        
        Test.stopTest();
    }

    public static testMethod void testVehiclesAvailableGrid(){

        setup();

        dealer2.ParentId = dealer.Id;
        Update dealer2;
        
        Test.startTest();

        VFC80_VehicleVO veiculo = new VFC80_VehicleVO();
        veiculo.itemModel = prd.Id;
        VFC87_NewVehicleBusinessDelegate.getInstance().vehiclesAvailableGrid(q.Id, veiculo);

        Test.stopTest();
        
    }

    public static testMethod void TestConvertStatus(){

        setup();

        Test.startTest();
                    
        System.assertEquals('Estoque',VFC87_NewVehicleBusinessDelegate.getInstance().convertStatus('Stock'));
        System.assertEquals('Em trânsito',VFC87_NewVehicleBusinessDelegate.getInstance().convertStatus('In Transit'));
        System.assertEquals('Bloqueio de Qualidade',VFC87_NewVehicleBusinessDelegate.getInstance().convertStatus('Quality Blockade'));
        System.assertEquals('Bloqueio Comercial',VFC87_NewVehicleBusinessDelegate.getInstance().convertStatus('Comercial Blockade'));
        System.assertEquals('Faturado',VFC87_NewVehicleBusinessDelegate.getInstance().convertStatus('Billed'));

        Test.stopTest();
        
    }

}