@isTest(SeeAllData=true)
private class VehicleBoTest {

	private static Account account;
	private static Opportunity opp;
	private static User userCommunity;
	private static VEH_Veh__c vehicle;
	private static User localUser;
	private static MyOwnCreation moc;
	// static {
	// 			moc = new MyOwnCreation();
	//
	// 			localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
	// 			System.runAs(localUser) {
	// 					userCommunity = moc.CriaUsuarioComunidade();
	// 					// final PermissionSetAssignment psa = new PermissionSetAssignment();
	// 					// psa.PermissionSetId = '0PS57000000XEsW';
	// 					// psa.AssigneeId = userCommunity.id;
	// 					// INSERT psa;
	// 			}
	// 	}

		@isTest
		static void itShould() {
				Test.startTest();
					moc = new MyOwnCreation();
				///System.runAs(localUser) {
					account = moc.criaPersAccount();
					INSERT account;

					opp = new Opportunity();
					opp.Name = 'Test';
					opp.StageName = 'Identified';
					opp.AccountId = account.Id;
					opp.CloseDate = System.today();
					//opp.OwnerId = localUser.Id;
					INSERT opp;

					vehicle = new VEH_Veh__c();
					vehicle.Name = '87342098463872416';
					//vehicle.OwnerId = localUser.Id;
					vehicle.Status__c = 'Billed';
					INSERT vehicle;

					Product2 prod = new Product2(Name = 'CLIO');
        	insert prod;

	        Id pricebookId = Test.getStandardPricebookId();

	        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId,
	            Product2Id = prod.Id, UnitPrice = 10000, IsActive = true);
	        insert standardPrice;

	        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
	        insert customPB;

	        PricebookEntry customPrice = new PricebookEntry(
	            Pricebook2Id = customPB.Id,
	            Product2Id = prod.Id,
	            UnitPrice = 12000,
	            IsActive = true
	        );
	        insert customPrice;

					Quote quote = moc.criaQuote();
					quote.Pricebook2Id = customPB.Id;
					quote.OpportunityId = opp.Id;
					quote.Status = 'Billed';
					insert quote;

					QuoteLineItem ql = moc.criaQuoteLineItem();
					ql.QuoteId = quote.Id;
        	ql.PricebookEntryId = customPrice.Id;
					ql.Vehicle__c = vehicle.Id;
					ql.Quantity = 1;
					ql.UnitPrice = 35000;
					insert ql;

					// final List<Quote> quot = [
					// 	SELECT Id, Status, OpportunityId FROM Quote
					// 	WHERE Id IN(SELECT QuoteId FROM QuoteLineItem WHERE Vehicle__c =: vehicle.Id) //AND Status = 'Billed'
					// ];
					// System.assertEquals('Billed', quot.get(0).Status);

					vehicle.Status__c = 'In Transit';
					UPDATE vehicle;
					// vehicle.Status__c = 'Billed';
					// UPDATE vehicle;
					//vehicle.Status__c = 'In Transit';
					//UPDATE vehicle;
				//}

				Test.stopTest();
		}
}