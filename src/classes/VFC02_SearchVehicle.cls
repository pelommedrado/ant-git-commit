public with sharing class VFC02_SearchVehicle 
{
    public String VIN {get;set;}
    public String VRN {get;set;}
    public String LanguageCode {get;set;}
    public String CountryCode {get;set;}
    public String urlForLink {get;set;}
    public String vehiclePrefix {get;set;}

    public VFC02_SearchVehicle()
    {
        VIN = '';
        VRN = '';
        LanguageCode = '';
        CountryCode = '';
        urlForLink = URL.getSalesforceBaseUrl().toExternalForm();        
        Schema.DescribeSObjectResult descrVehicle = Schema.SObjectType.VEH_Veh__c;
        vehiclePrefix = descrVehicle.getKeyPrefix();
    }

    public PageReference searchVehicle()
    {
        String query = 'Select Id from VEH_Veh__c ';
        String whereClause = '';
        VEH_Veh__c vehicleFound;
        PageReference pageRef;
        
        VIN = VIN.trim();
        VRN = VRN.trim();
        LanguageCode = LanguageCode.trim();
        CountryCode = CountryCode.trim();
        
        if (VIN != null && VIN != '')
            whereClause = ' where Name = \'' + VIN + '\'';
        
        if (VRN != null && VRN != '')
        {
            if (whereClause != '')
                whereClause = whereClause + ' AND VehicleRegistrNbr__c = \'' + VRN + '\'';
            else
                whereClause = whereClause + ' WHERE VehicleRegistrNbr__c = \'' + VRN + '\'';
        }
        
        if (whereClause == '')      
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.searchVehicleFilterMandatory);
            ApexPages.addMessage(myMsg);
            return null;
        }
        else
        {
            query = query + whereClause + ' LIMIT 1';
            
            try 
            {
                vehicleFound = Database.query(query);
                pageRef = new PageReference('/' + vehicleFound.Id);
            }
            catch (QueryException qe)
            {
                if (VIN != null && VRN == '')
                {
                    WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse rep = searchBVM();
                     
                    if (rep != null && rep.detVeh != null)
                    {
                        pageRef = createVehicle(rep);
                    }
                    else
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, System.label.noVehicleFound  + ' ' + System.label.CreateVehicleManually + ' :<b><a href=\'' + urlForLink + '/' + vehiclePrefix +  '/e' + '\'>' + System.label.CreateVehicle + '</a></b>');
                        ApexPages.addMessage(myMsg);
                    }
                }
                else
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, System.label.noVehicleFound   + ' ' + System.label.CreateVehicleManually + ' :<b><a href=\'' + urlForLink + '/' + vehiclePrefix + '/e' +  '\'>' + System.label.CreateVehicle + '</a></b>');
                    ApexPages.addMessage(myMsg);
                }
            }
            
            return pageRef;
        }
    }
    
    public WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse searchBVM()
    {
        Boolean Success ;
        string strDebut = 'Début Trace Réponse WS';
        string strFin = 'Fin Trace Réponse WS';
        //WS04_CustdataCrmBserviceRenault.CrmGetCustData callout = new WS04_CustdataCrmBserviceRenault.CrmGetCustData() ;
        //WS04_CustdataCrmBserviceRenault.CustDataRequest info = new WS04_CustdataCrmBserviceRenault.CustDataRequest();
        //WS04_CustdataCrmBserviceRenault.GetCustDataRequest request = new WS04_CustdataCrmBserviceRenault.GetCustDataRequest();
        //WS04_CustdataCrmBserviceRenault.GetCustDataResponse result = new WS04_CustdataCrmBserviceRenault.GetCustDataResponse();
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();               
        WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
        WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
       
        try
        {  
            servicePref.bvmsi25SocEmettrice  = System.Label.SocieteEmettrice;
            servicePref.bvmsi25Vin = VIN;
            servicePref.bvmsi25CodePaysLang = CountryCode; //FRA 
            servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
            request.ServicePreferences = servicePref;
              
            VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
            VehWS.clientCertName_x = System.label.RenaultCertificate;           
            VehWS.timeout_x=40000;
            
            return (WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse)VehWS.getApvGetDetVehXml(request);
        }            
        catch (Exception e)
        {                    
            system.debug(VEH_WS);
            system.debug('erreur : ' + String.valueOf(e));
            return null;
        }
    }
    
    public PageReference createVehicle(WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse vehBVM)
    {
        VEH_Veh__c veh = new VEH_Veh__c();
        veh.Name = VIN;
               
        //AfterSalesType__c p460:tvv
        if (vehBVM.detVeh.tvv != null)
            veh.AfterSalesType__c = vehBVM.detVeh.tvv;
        
        //BodyType__c p460:libCarrosserie
        if (vehBVM.detVeh.libCarrosserie != null)
            veh.BodyType__c = vehBVM.detVeh.libCarrosserie;
        
        //DateofManu__c p460:dateTcmFab 
        if (vehBVM.detVeh.dateTcmFab != null && vehBVM.detVeh.dateTcmFab.length() > 7)
            veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(0, 2)));
        
        //DeliveryDate__c p460:dateLiv
        if (vehBVM.detVeh.dateLiv != null && vehBVM.detVeh.dateLiv.length() > 7)
            veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(0, 2)));
        
        //Description__c p460:libModel + p460:ligne2P12 + p460:ligne3P12 + p460:ligne4P12
            veh.Description__c = vehBVM.detVeh.libModel + ' ' + vehBVM.detVeh.ligne2P12 + ' ' + vehBVM.detVeh.ligne3P12 + ' ' + vehBVM.detVeh.ligne4P12;
        
        //EngineIndex__c p460:indMot
        if (vehBVM.detVeh.indMot != null)
            veh.EngineIndex__c = vehBVM.detVeh.indMot;
            
        //EngineManuNbr__c p460:NMot
        if (vehBVM.detVeh.NMot != null)
            veh.EngineManuNbr__c = vehBVM.detVeh.NMot;
                
        //EngineType__c p460:typeMot 
        if (vehBVM.detVeh.typeMot != null)
            veh.EngineType__c = vehBVM.detVeh.typeMot;
            
        //GearBoxIndex__c p460:indBoi 
        if (vehBVM.detVeh.indBoi != null)
            veh.GearBoxIndex__c = vehBVM.detVeh.indBoi;
            
        //GearBoxManuNbr__c p460:NBoi
        if (vehBVM.detVeh.NBoi != null)
            veh.GearBoxManuNbr__c = vehBVM.detVeh.NBoi;
            
        //GearBoxType__c p460:typeBoi
        if (vehBVM.detVeh.typeBoi != null)
            veh.GearBoxType__c = vehBVM.detVeh.typeBoi;
        
        //Model__c p460:libModel 
        if (vehBVM.detVeh.libModel != null)
            veh.Model__c = vehBVM.detVeh.libModel;
            
        //VehicleBrand__c p460:marqCom 
        if (vehBVM.detVeh.marqCom != null)
            veh.VehicleBrand__c = vehBVM.detVeh.marqCom;
            
        //VehicleManuNbr__c p460:NFab
        if (vehBVM.detVeh.NFab != null)
            veh.VehicleManuNbr__c = vehBVM.detVeh.NFab;
            
        //VersionCode__c p460:version
        if (vehBVM.detVeh.version != null)
            veh.VersionCode__c = vehBVM.detVeh.version;
            
        if (vehBVM.detVeh.criteres != null)
        {
            for (WS01_ApvGetDetVehXml.DetVehCritere crit : vehBVM.detVeh.criteres)
            {
                //EnergyType__c Objet 019
                if (crit.cdObjOf == '019')
                    veh.EnergyType__c = crit.critereOf;
                //ModelCode__c Objet 008
                else if (crit.cdObjOf == '008')
                    veh.ModelCode__c = crit.critereOf;
            }
        }
        
        try
        {
            insert veh;
        }
        catch(DMLException dmle)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSaveVehicle);
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        return new PageReference('/' + veh.Id);
    }


public class thecontrollerTests {
             
    }
}