public class MonthlyGoalGroup_Controller {
    
    public static void createDealerSellerGoal(List<Monthly_Goal_Group__c> groupGoals){
        
        System.debug('***MonthlyGoalGroup_Controller.createDealerSellerGoal()');
        System.debug('groupGoals: ' + groupGoals);
        
        List<Monthly_Goal_Dealer__c> mgdList = new List<Monthly_Goal_Dealer__c>();
        List<Monthly_Goal_Dealer__c> mgdExisting = new List<Monthly_Goal_Dealer__c>();
        List<Account> groups = new List<Account>();
        
        Set<Id> dealerGoalsId = new Set<Id>();
        Set<String> matrixBirsDouble = new Set<String>();
        Set<String> matrixBirs = new Set<String>();
        
        for(Monthly_Goal_Group__c g : groupGoals){
            matrixBirs.add(g.Matrix_Bir__c);
            matrixBirsDouble.add(g.Matrix_Bir__c);
        }
        
        System.debug('matrixBirs: ' + matrixBirs);
        
        groups = [
            SELECT Id, Dealer_Matrix__c
            FROM Account
            WHERE Dealer_Matrix__c IN : matrixBirs
            AND DealershipStatus__c = 'Active'
            AND Country__c = 'Brazil'
            AND RecordTypeId =: Utils.getRecordTypeId('Account', 'Network_Site_Acc')
            LIMIT 999
        ];
        
        mgdExisting = [SELECT Id, Dealer__c
                       FROM Monthly_Goal_Dealer__c
                       WHERE Monthly_Goal_Group__r.Matrix_Bir__c IN : matrixBirsDouble];
        
        for(Monthly_Goal_Dealer__c mgd : mgdExisting){
            dealerGoalsId.add(mgd.Dealer__c);
        }
        
        for(Monthly_Goal_Group__c mgg : groupGoals){
            for(Account acc : groups){
                
                if(acc.Dealer_Matrix__c == mgg.Matrix_Bir__c
                   && !dealerGoalsId.contains(acc.Id)){
                    
                    Monthly_Goal_Dealer__c mgd = new Monthly_Goal_Dealer__c();
                    mgd.Monthly_Goal_Group__c = mgg.Id;
                    mgd.Dealer__c = acc.Id;
                    mgdList.add(mgd);
                    
                    System.debug('mgd: ' + mgd);
                    
                }
            }
            
            Savepoint sp1 = Database.setSavepoint();
            
            try{
                
                Database.upsert(mgdList);
                
            } catch(Exception e){
                
                System.debug('############################# erro dealer: ' + e);
                Database.rollback(sp1);
                
            }
        }
        
    }
    
}