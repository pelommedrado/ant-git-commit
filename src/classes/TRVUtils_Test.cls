@isTest
private class TRVUtils_Test {

    static testMethod void myUnitTestMDM() 
    {
		/*
      	Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
      	User u = new User(); //user MDM
        u.ProfileId = profileCus.Id;
        u.FirstName = 'testFirstName3';
        u.LastName = 'testLastName3';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France2' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
        insert u;  
		*/

		User u = Myr_Datasets_Test.getSysAdminUser();
            
        system.runas(u)
        {
	          test.startTest();
				TRVResponseWebservices.InfoClient info =new TRVResponseWebservices.InfoClient();
				TRVResponseWebservices.Vehicle vcle1 =new TRVResponseWebservices.Vehicle();
			
							
				list<TRVResponseWebservices.InfoClient> infolst = new list<TRVResponseWebservices.InfoClient>();
				info.name = 'toto';
				info.email = 't54oto@totot.com';
				info.emailPro = 't54oto@totot.com';
				infolst.add(info);
				TRVUtils.createAccountWS(infolst,true);
			
			
				info = prepopulateInfo(info); 
				vcle1 = prepopulatevcle(vcle1); 
				vcle1.vnvo = 'O';
				list<TRVResponseWebservices.Vehicle>lstvcle = new list<TRVResponseWebservices.Vehicle>();
				lstvcle.add(vcle1);
				
				info.vcle = lstvcle;
				TRVUtils.createAccountWSPage(info);
				TRVUtils.createAccountWSPage(info);
								
				info.companyID = '313134';
				info.secondCompanyID = '313314';
				info.localCustomerID1 = '313134';
				info.localCustomerID2 = '313134';
				info.vcle = null;
				
				info.FirstName1 = ''; 
				TRVUtils.createAccountWSPage(info);
				
				info.companyID = '31313';
				info.secondCompanyID = '31331';
				info.localCustomerID1 = '31313';
				info.localCustomerID2 = '31313';
				
				info.accountBrand = 'Dacia';
				TRVUtils.createAccountWSPage(info);//run
				
				
				
				
				
				
				info =new TRVResponseWebservices.InfoClient();
								
				info.name = 'toto';
				info.email = 't54oto@totot.com';
				//info = prepopulateInfo(info); 
				info.companyID = '313134';
				info.secondCompanyID = '313314';
				info.localCustomerID1 = '313134';
				info.localCustomerID2 = '313134';
				info.vcle = null;
				info.strAccountType = 'Company';
				
				TRVUtils.createAccountWSPage(info);//run 		
			
			
			test.stopTest();
          
        }
        
    }
    static testMethod void myUnitTestRBX() 
    {
		/*
      	Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
      	User u = new User(); //user MDM
        u.ProfileId = profileCus.Id;
        u.FirstName = 'testFirstName34';
        u.LastName = 'testLastName34';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France2' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
        insert u;  */

		User u = Myr_Datasets_Test.getSysAdminUser();
            
        system.runas(u)
        {
	          test.startTest();
				TRVResponseWebservices.InfoClient info =new TRVResponseWebservices.InfoClient();
				TRVResponseWebservices.Vehicle vcle1 =new TRVResponseWebservices.Vehicle();
			
				info = prepopulateInfo(info); 
				vcle1 = prepopulatevcle(vcle1); 
				info.datasource = 'RBX';
				
				vcle1.vin = 'test0';
				vcle1.vinExternal = 'test0';
			
				list<TRVResponseWebservices.Vehicle>lstvcle = new list<TRVResponseWebservices.Vehicle>();
				lstvcle.add(vcle1);
				
				info.vcle = lstvcle;
				TRVUtils.createAccountWSPage(info);
				TRVUtils.createAccountWSPage(info);
				
				info.companyID = '3131340';
				info.secondCompanyID = '3133140';
				info.localCustomerID1 = '3131340';
				info.localCustomerID2 = '3131340';
				info.vcle = null;
				

				
				
				info.countrycode = ''; // test country code empty
				info.renaultEmployee = 'N'; 
				info.deceased = 'Y';
				
				TRVUtils.createAccountWSPage(info);
				info.accountBrand = 'Dacia';
				TRVUtils.createAccountWSPage(info);
				info.accountBrand = 'NISSAN';
				TRVUtils.createAccountWSPage(info); // run
				
			/*	info.strAccountType = 'Company';
				info.accountBrand = 'Renault';
				TRVUtils.createAccountWSPage(info);	// run 
				info.prefDealer = ''; 
				info.strAccountType = 'Company';
				info.accountBrand = 'Dacia';
				TRVUtils.createAccountWSPage(info);	// run 
				info.strAccountType = 'Company';
				info.accountBrand = 'NISSAN';
				TRVUtils.createAccountWSPage(info);	// run
				*/
			test.stopTest();
          
        }
        
    }
    static testMethod void myUnitTestBCS() 
    {
		/*
      	Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
      	User u = new User(); //user MDM
        u.ProfileId = profileCus.Id;
        u.FirstName = 'testFirstName35';
        u.LastName = 'testLastName35';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France2' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
        insert u;  
		*/

		User u = Myr_Datasets_Test.getSysAdminUser();
            
        system.runas(u)
        {
	          test.startTest();
				TRVResponseWebservices.InfoClient info =new TRVResponseWebservices.InfoClient();
				TRVResponseWebservices.Vehicle vcle1 =new TRVResponseWebservices.Vehicle();
			
				info = prepopulateInfo(info); 
				vcle1 = prepopulatevcle(vcle1); 
				
				info.datasource = 'BCS';
				info.strPartyID = '54541';
				vcle1.vin = 'test1';
				vcle1.vinExternal = 'test1';
				
				
				list<TRVResponseWebservices.Vehicle>lstvcle = new list<TRVResponseWebservices.Vehicle>();
				lstvcle.add(vcle1);
				
				info.vcle = lstvcle;
				 
				TRVUtils.createAccountWSPage(info); //run 
				TRVUtils.createAccountWSPage(info);
								
				info.companyID = '3131341';
				info.secondCompanyID = '3133141';
				info.localCustomerID1 = '3131341';
				info.localCustomerID2 = '3131341';
				info.vcle = null;
				
				/*list<TRVResponseWebservices.InfoClient>infolst = new list<TRVResponseWebservices.InfoClient>();
				infolst.add(info);
				TRVUtils.createAccountWS(infolst,true); // run bulk*/ 
				
				
				info.companyID = '313131';
				info.secondCompanyID = '313311';
				info.localCustomerID1 = '313131';
				info.localCustomerID2 = '313131';
				info.accountBrand = 'Renault';
				info.vcle = null;
				
				TRVUtils.createAccountWSPage(info);
			
			
				
				 
				
				
			
			
				info.countrycode = ''; // test country code empty
				//with Dacia account			
				info.accountBrand = 'Dacia';
				TRVUtils.createAccountWSPage(info); // run
				info.accountBrand = 'NISSAN';
				TRVUtils.createAccountWSPage(info); // run
				
		/*		info.strAccountType = 'Company';
				info.accountBrand = 'Renault';
				TRVUtils.createAccountWSPage(info);	// run 
				info.strAccountType = 'Company';
				info.accountBrand = 'Dacia';
				TRVUtils.createAccountWSPage(info);	// run 
				info.strAccountType = 'Company';
				info.accountBrand = 'NISSAN';
				TRVUtils.createAccountWSPage(info);	// run */
				
			test.stopTest();
          
        }
        
    }
    static testMethod void myUnitTestAustria() 
    {
		/*
      	Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
      	User u = new User(); //user MDM
        u.ProfileId = profileCus.Id;
        u.FirstName = 'testFirstName35';
        u.LastName = 'testLastName35';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'Austria' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
        insert u;  */
		User u = Myr_Datasets_Test.getSysAdminUser();
            
        system.runas(u)
        {
	          test.startTest();
				TRVResponseWebservices.InfoClient info =new TRVResponseWebservices.InfoClient();
				TRVResponseWebservices.Vehicle vcle1 =new TRVResponseWebservices.Vehicle();
			
				info = prepopulateInfo(info); 
				vcle1 = prepopulatevcle(vcle1); 
				
				info.datasource = 'BCS';
				info.strPartyID = '54541';
				vcle1.vin = 'test1';
				vcle1.vinExternal = 'test1';
				
				
				list<TRVResponseWebservices.Vehicle>lstvcle = new list<TRVResponseWebservices.Vehicle>();
				lstvcle.add(vcle1);
				
				info.vcle = lstvcle;
				 
				TRVUtils.createAccountWSPage(info); //run 
			test.stopTest();
          
        }
        
    }
    static testMethod void myUnitTestSwitzerland() 
    {
		/*
      	Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
      	User u = new User(); //user MDM
        u.ProfileId = profileCus.Id;
        u.FirstName = 'testFirstName35';
        u.LastName = 'testLastName35';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'Switzerland' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
        insert u;  */

		User u = Myr_Datasets_Test.getSysAdminUser();
            
        system.runas(u)
        {
	          test.startTest();
				TRVResponseWebservices.InfoClient info =new TRVResponseWebservices.InfoClient();
				TRVResponseWebservices.Vehicle vcle1 =new TRVResponseWebservices.Vehicle();
			
				info = prepopulateInfo(info); 
				vcle1 = prepopulatevcle(vcle1); 
				
				info.datasource = 'BCS';
				info.strPartyID = '54541';
				vcle1.vin = 'test1';
				vcle1.vinExternal = 'test1';
				
				
				list<TRVResponseWebservices.Vehicle>lstvcle = new list<TRVResponseWebservices.Vehicle>();
				lstvcle.add(vcle1);
				
				info.vcle = lstvcle;
				 
				TRVUtils.createAccountWSPage(info); //run 
			test.stopTest();
          
        }
        
    }
    static public TRVResponseWebservices.InfoClient prepopulateInfo(TRVResponseWebservices.InfoClient info)
    {
    		info.datasource = 'MDM'; //by default
		
			info.strPartyID = '5454';
			
			info.strAccountType = 'Personal';
			info.FirstName1 = 'Test';
			info.LastName1 = 'Test';
			info.FirstName2 = 'Test';
			info.LastName2 = 'Test';
			info.name = 'Test';
			info.strDOB = '1988-10-01';
			info.title = 'Test';
			info.civility = 'Test';
			info.sex = 'Test';
			info.marital = 'Test';
			info.Lang = 'Test';
			info.renaultEmployee = 'Y';
			info.nbrChildrenHome = '10';
			info.deceased = 'Test';
			info.preferredMedia = 'Test';
			info.OccupationalCategoryCodeP = 'Test';
			info.OccupationalCategoryCode = 'Test';
			info.partySegment = 'Test';
			info.commercialName = 'Test';
			info.numberOfEmployees = 10;
			info.financialStatus = 'Test';
			info.codeNAF = 'Test';
			info.legalNature = 'Test';
			info.partySub = 'Test';
		
			info.countrySF = 'France';
			info.AdriaticCountrySF = 'France';
			info.MiDCECountrySF = 'France';
			info.NRCountrySF = 'France';
			info.PlCountrySF = 'France';
			info.UkIeCountrySF = 'France';
		
		
			//blockadress
			info.addressLine1 = 'Test';
			info.addressLine2 = 'Test';
			info.strNum = 'Test';
			info.strType = 'Test';
			info.compl1 = 'Test';
			info.compl3 = 'Test';
			info.city = 'Test';
			info.region = 'Test';
			info.zip = 'Test';
			info.CountryCode = 'Test';
		
			info.PersonOtherStreet = 'Test';
			info.PersonOtherCity = 'Test';
			info.PersonOtherState = 'Test';
			info.PersonOtherPostalCode = 'Test';
			info.PersonOtherCountry = 'Test';
		
		
		
			info.email = 'toto@totot.com';
			info.emailPro = 'toto@totot.com';
			info.strFixeLandLine = '0101010101';
			info.strFixeLandLinePro = '1011010101';
			info.strMobile = '1010101010';
			info.strMobilePro = '1010101010';
				
		    Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Company_Account_RecType' limit 1].Id;
			Account Acc2 = new Account(Name = 'TestL 0', Phone = '1000', RecordTypeId = RTID_COMPANY, PersEmailAddress__c = 'test@renault.com', ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingPostalCode = '75013', ShippingStreet = 'my street');
			insert Acc2; 
		
			info.prefDealer=Acc2.id;
			info.prefDealerDacia=Acc2.id;
		
		
		/*	info.companyID = '3131';
			info.secondCompanyID = '3131';
			info.localCustomerID1 = '3131';
			info.localCustomerID2 = '3131';*/
		
			info.GlobalCommAgreement = 'Y';
			info.TelCommAgreement = 'Y';
			info.TelCommAgreementDate = date.today();
			info.EmailCommAgreement = 'Y';
			info.EmailCommAgreementDate = date.today();
			info.SMSCommAgreement = 'Y';
			info.SMSCommAgreementDate = date.today();
			info.PostCommAgreement = 'Y';
			info.PostCommAgreementDate = date.today();
			info.stopComFlag = 'Y';
			info.stopComFlagDate = date.today();
			info.stopComSMDFlag = 'Y';
			info.stopComSMDFlagDate = date.today();
		
			info.accountBrand = 'Renault';
		
			//technicalfield
			//info.datasource = 'Test';
			info.countryCode2 = 'Test';
			
			return info; 
        	
    }
    static public TRVResponseWebservices.Vehicle prepopulatevcle(TRVResponseWebservices.Vehicle vcle1)
    {
		vcle1.vin = 'test';
		vcle1.vinExternal = 'test';
		vcle1.brandCode = 'Renault';
		vcle1.model = 'test';
		vcle1.modelCode = 'test';
		vcle1.modelLabel = 'test';
		vcle1.versionLabel = 'test';
		vcle1.deliveryDate = '1988-10-01';
		vcle1.firstRegistrationDate = String.valueOf(system.today().format());
		vcle1.lastRegistrationDate = String.valueof(system.today().format());
		vcle1.registrationNumber = 'test';
		vcle1.technicalControlDate = 'tets';
		
		vcle1.dataCompletionBVM = 'test';
		vcle1.dataCompletionETICOM = 'test';
		vcle1.dataCompletionSLK = 'test';
		vcle1.dataBVMUpto = 'test';
			
		//VehicleRelation
		vcle1.vehicleType = 'test';
		vcle1.possessionBegin = String.valueof(system.today().format());
		vcle1.possessionEnd = String.valueof(system.today().format());
		vcle1.vnvo = 'N';
		vcle1.garageStatus = 'test';
		vcle1.status = 'test';
		vcle1.IdClient = 'test';
		//limit
    	
    	return vcle1;
    }

}