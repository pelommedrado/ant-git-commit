/**
* Classe responsável pela manipulação dos dados do objeto Attachment.
* @author Felipe Jesus Silva.
*/
public class VFC107_AttachmentDAO extends VFC01_SObjectDAO
{
	private static final VFC107_AttachmentDAO instance = new VFC107_AttachmentDAO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC107_AttachmentDAO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC107_AttachmentDAO getInstance()
	{
		return instance;
	}

	public List<Attachment> findByProductName(String productName)
	{
        system.debug('***productName: '+productName);
		List<Attachment> lstSObjAttachment = new List<Attachment>(); 
		List<Product2> p = [SELECT Id From Product2 where Name =:productName];
        
        system.debug('***p: '+p);
		
		/*if(p != null && p.size() >0){
			lstSObjAttachment = [SELECT Id, Name FROM Attachment WHERE Parent.Id =: p.get(0).Id ORDER By Name LIMIT 10];
		}else{
			lstSObjAttachment = [SELECT Id, Name FROM Attachment WHERE Parent.Name =: productName ORDER By Name LIMIT 10];
		}*/
		
		
		return lstSObjAttachment;
	}
}