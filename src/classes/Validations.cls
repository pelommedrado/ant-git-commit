public class Validations {
    
    public static final List<Integer> pesoCPF = new List<Integer> {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
        
    /* Para validar CPF */
    public static boolean isValidCPF(String cpf) {
        if ((cpf==null) || (cpf.length()!=11)) return false;
            
        Integer digito1 = calcularDigito(cpf.substring(0,9), pesoCPF);
        Integer digito2 = calcularDigito(cpf.substring(0,9) + digito1, pesoCPF);
        return cpf.equals(cpf.substring(0,9) + String.valueOf(digito1) + String.valueOf(digito2));
    }
    
    private static Integer calcularDigito(String str, Integer[] peso) {
        Integer soma = 0;
        for (Integer indice=str.length() - 1, digito; indice >= 0; indice-- ) {
            digito = Integer.valueOf(str.substring(indice,indice + 1));
            Integer aux = peso.size() - str.length() + indice;
            soma += digito * peso[aux];
        }
        soma = 11 - math.mod(soma, 11);
        return soma > 9 ? 0 : soma;
    }
    
    /* Para validação de email */
    public static Boolean isValidateEmail (String semail) {
        
        String regex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern myPattern = Pattern.compile(regex);	
        Matcher myMatcher = myPattern.matcher(semail);
        
        if (!myMatcher.matches()) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
}