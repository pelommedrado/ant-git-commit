/*****************************************************************************************
Name    : Rforce_AccountCascadingSearch_CLS
Desc    : This is to update a newly created case with its desired account name , after doing cascading search for account with the field values entered during the case creation.
Approach: Calling this apex class on Before Insert trigger for case.
Project : Rforce
CreatedBy: Venkatesh Kumar Sakthivel(RNTBCI)
CreatedDate: 22/09/2015

******************************************************************************************/

public class Rforce_AccountCascadingSearch_CLS {

//Update case with account number by doing a cascading search based on LastName,Address,Email,country,phone on Account

/**
* @Author Venkatesh Kumar Sakthivel(RNTBCI)
* @date 22/09/2015
* @description Get input value from Before insert trigger for to Update case with account number by doing a cascading search based on LastName,Address,Email,country,phone on Account
* Project Rforce
*/ 

public static void searchAccount(list <Case> lstCase){

Map<String,Case> mlastname=new Map<String,Case>();
Map<String,Case> maddress=new Map<String,Case>();
Map<String,Case> memail=new Map<String,Case>();
Map<String,Case> mcountry=new Map<String,Case>();
Map<String,Case> mphone=new Map<String,Case>();
Map<String,Case> mcellphone=new Map<String,Case>();
Map<Id,Case> maccountId=new Map<Id,Case>();

List<Account> lstLastnameAddressCountry = new List<Account>();
List<Account> lstLastnameAddressEmailCountry = new List<Account>();
List<Account> lstLastnameAddressPhoneCountry = new List<Account>();
List<Account> lstLastnameAddressEmailPhoneCountry = new List<Account>();
List<Account> lstLastNamePhoneCountry = new List<Account>();
List<Account> lstLastnameEmailPhoneCountry = new List<Account>();
List<Account> lstLastnameEmailCountry = new List<Account>();

Id RTID_PERSON = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
System.debug('RTID_PERSON----->'+RTID_PERSON);

//get the list of values for case
system.debug('lstcasevale>>>'+lstCase);

for(Case caselist:lstCase){
    mlastname.put(caselist.LastName_Web__c,caselist);
    maddress.put(caselist.Address_Web__c,caselist);
    mcountry.put(caselist.CountryCase__c,caselist);
    memail.put(caselist.SuppliedEmail,caselist);
    mphone.put(caselist.SuppliedPhone,caselist);
    mcellphone.put(caselist.Cell_Phone_Web__c,caselist);
   }
    system.debug('mlastname-->'+mlastname);
    system.debug('maddress-->'+maddress);
    system.debug('memail-->'+memail);
    system.debug('mcountry-->'+mcountry);
    system.debug('mphone-->'+mphone);
    system.debug('mphone-->'+mcellphone);

    // Search for LastName,Address and Country
    lstLastnameAddressCountry=[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON  and accnt.LastName IN : mlastname.keyset() and accnt.Billingstreet IN : maddress.keyset() and accnt.country__c IN : mcountry.keyset() ];
    // Search for LastName,Address,Email and Country
    if(lstLastnameAddressCountry.size() > 1){
    lstLastnameAddressEmailCountry=[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.Billingstreet IN : maddress.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.PersEmailAddress__c IN : memail.keyset() ];
    // Search for LastName,Address,phone and Country
    if(lstLastnameAddressEmailCountry.size() == 0){
    lstLastnameAddressPhoneCountry =[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.Billingstreet IN : maddress.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.HomePhone__c IN : mphone.keyset()];
    }
    // Search for LastName,Address,Email,phone and Country
    else if(lstLastnameAddressEmailCountry.size() > 1){
    lstLastnameAddressEmailPhoneCountry=[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.Billingstreet IN : maddress.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.PersEmailAddress__c IN : memail.keyset() and accnt.HomePhone__c IN : mphone.keyset()];
    }
    }  

    // search for Lastname , email and country
    else if(lstLastnameAddressCountry.size() == 0){
    lstLastnameEmailCountry =[select accnt.Id, accnt.PersonContactId,accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.PersEmailAddress__c IN : memail.keyset() ];
    
    // search for Lastname , phone and country
    if(lstLastnameEmailCountry.size() == 0){
    lstLastNamePhoneCountry =[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.HomePhone__c IN : mphone.keyset()];
    System.debug('lstLastNamePhoneCountry-->'+lstLastNamePhoneCountry);
    }
    // search for Lastname , email , phone and country
    else if(lstLastnameEmailCountry.size() > 1){
    lstLastnameEmailPhoneCountry=[select accnt.Id, accnt.PersonContactId, accnt.PersMobPhone__c,accnt.PersEmailAddress__c,accnt.BillingStreet,accnt.BillingCity,accnt.BillingState ,accnt.HomePhone__c,accnt.FirstName,accnt.LastName from Account accnt where IsPersonAccount=True and recordtypeid =: RTID_PERSON and accnt.LastName IN : mlastname.keyset() and accnt.country__c IN : mcountry.keyset() and accnt.PersEmailAddress__c IN : memail.keyset() and accnt.HomePhone__c IN : mphone.keyset()];
    }
    }
    
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameAddressCountry Size - ' + lstLastnameAddressCountry.size());
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameAddressEmailCountry Size - ' + lstLastnameAddressEmailCountry.size());
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameAddressPhoneCountry Size - ' + lstLastnameAddressPhoneCountry.size());
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameAddressEmailPhoneCountry Size - ' + lstLastnameAddressEmailPhoneCountry.size());
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameEmailCountry Size - ' + lstLastnameEmailCountry.size());
    system.debug('Rfroce_Accountcascadingsearch : lstLastNamePhoneCountry Size - ' + lstLastNamePhoneCountry.size()); 
    system.debug('Rfroce_Accountcascadingsearch : lstLastnameEmailPhoneCountry Size - ' + lstLastnameEmailPhoneCountry.size()); 
   
for (Case cas: lstCase){
   if(lstLastnameAddressCountry.size()== 1) { // Condition for LastName, Address and Country
       for (Account accnt: lstLastnameAddressCountry) {
        cas.AccountId = accnt.Id;
        cas.ContactId = accnt.PersonContactId;
        WebtoCase_UpdateAccount(lstCase);
        
       }
       }
    else if(lstLastnameAddressCountry.size()== 0)   {
        if(lstLastnameEmailCountry.size()== 1){ // Condition for Lastname , email and country
            for(Account accnt: lstLastnameEmailCountry){
                     cas.AccountId = accnt.Id;
                     cas.ContactId = accnt.PersonContactId;
                     WebtoCase_UpdateAccount(lstCase);
                }
                }
        else if (lstLastnameEmailCountry.size()== 0){
            if(lstLastNamePhoneCountry.size()== 1){ // Condition for Lastname , phone and country
                for (Account accnt: lstLastNamePhoneCountry){
                    cas.AccountId = accnt.Id;
                    cas.ContactId = accnt.PersonContactId;
                    WebtoCase_UpdateAccount(lstCase);
                }
                }
            else if(lstLastNamePhoneCountry.size()== 0){
                // CMDM Call              
                }
                }
                
        else if (lstLastnameEmailCountry.size() > 1){
            if(lstLastnameEmailPhoneCountry.size()== 1){ // Condition for Lastname , email , phone and country
                for (Account accnt: lstLastnameEmailPhoneCountry){
                    cas.AccountId = accnt.Id;
                    cas.ContactId = accnt.PersonContactId;
                    WebtoCase_UpdateAccount(lstCase);
                }
                }
            else if(lstLastnameEmailPhoneCountry.size()== 0){
                // CMDM Call
                }
                }   
            
        }        
    else if(lstLastnameAddressCountry.size() > 1){
        if(lstLastnameAddressEmailCountry.size()== 1){ // Condition for LastName,Address,email and Country
            for(Account accnt: lstLastnameAddressEmailCountry){
                     cas.AccountId = accnt.Id;
                     cas.ContactId = accnt.PersonContactId;
                    WebtoCase_UpdateAccount(lstCase);
                }
                }
        else if(lstLastnameAddressEmailCountry.size()== 0){
            if(lstLastnameAddressPhoneCountry.size()==1){ // Condition for LastName,Address,phone and Country
                for (Account accnt: lstLastnameAddressPhoneCountry){
                    cas.AccountId = accnt.Id;
                    cas.ContactId = accnt.PersonContactId;
                    WebtoCase_UpdateAccount(lstCase);
                }
                }
            else if(lstLastnameAddressPhoneCountry.size()==0){
                // CMDM Call
                }
                }
         else if(lstLastnameAddressEmailCountry.size() > 1){
             if(lstLastnameAddressEmailPhoneCountry.size()==1){ // Condition for LastName,Address,Email,phone and Country
                for (Account accnt: lstLastnameAddressEmailPhoneCountry){
                    cas.AccountId = accnt.Id;
                    cas.ContactId = accnt.PersonContactId;
                    WebtoCase_UpdateAccount(lstCase);
                }
                }
            else if(lstLastnameAddressEmailPhoneCountry.size()==0){
                // CMDM Call
                }
                }
    }           
    }   
    }
 public static void WebtoCase_UpdateAccount(list <Case> listCase){
        Map<String,Case> emailmap=new Map<String,Case>();
        Map<Id,Case> accountidmap=new Map<Id,Case>();
        for(Case c:listcase){
        
            if(c.SuppliedEmail!=null && (c.Origin==System.label.Rforce_CAS_Webform || c.CaseSubSource__c==System.label.Rforce_CAS_Webform || c.Origin==System.label.Rforce_CAS_VOCD_Origin )){
                c.SuppliedName = c.FirstName_Web__c + ' ' + c.LastName_Web__c;
                emailmap.put(c.SuppliedEmail,c);    
            }
        }
        for(Case c : listCase){
             system.debug('Rfroce_WebtoCaseUtil :  listCase - c.AccountId - ' + c.AccountId);
            if(c.AccountId != null && c.SuppliedEmail != null){
                  accountidmap.put(c.AccountId,c);   
                  
                    system.debug('Rfroce_WebtoCaseUtil :  listCase -2 c.AccountId - ' + c.AccountId);
                      system.debug('Rfroce_WebtoCaseUtil :  listCase -2 c.SuppliedEmail - ' +c.SuppliedEmail);
            }
        } 
        try{
            /* REQ-01294: ACCOUNT CONFICT : To disable the Account Conflict symbol and Not map the Account to Case for the Multi Countries  */
            /* HQREQ-01294: ACCOUNT CONFICT : To Enable/Display the Create Account symbol from the Web2Case for the Multi Countiries  */
           List<Account> acclist=[Select Id, Name,HomePhone__c,PersEmailAddress__c, PersMobPhone__c, Phone, Country__c, BillingStreet, BillingCity, BillingState From account where id IN :accountidmap.keyset()];
            if(acclist.size()>0){
                for(Account a: acclist){
                    Case cas = accountidmap.get(a.Id);
                    if(cas != null){
                           system.debug('cas.AccountConflicts_web__c-->'+cas.AccountConflicts_web__c);
                           system.debug(a.PersMobPhone__c+'=='+ cas.SuppliedPhone +'&&'+ a.PersEmailAddress__c+' ==' +cas.SuppliedEmail +'&&' +a.BillingStreet +'=='+ cas.Address_Web__c+ '&&'+ a.BillingCity +'=='+ cas.City_Web__c+ '&&'+ a.BillingState +'=='+ cas.State_Web__c);
                        if ((a.PersMobPhone__c== cas.Cell_Phone_Web__c) && (a.PersEmailAddress__c == cas.SuppliedEmail) && (a.BillingStreet == cas.Address_Web__c) && (a.BillingCity == cas.City_Web__c) && (a.BillingState == cas.State_Web__c)
                             && (a.HomePhone__c==cas.SuppliedPhone) && (a.FirstName==cas.FirstName_Web__c) && (a.LastName==cas.LastName_Web__c)) {                           
                            cas.AccountConflicts_web__c = false;
                            system.debug('cas.AccountConflicts_web__c-->'+cas.AccountConflicts_web__c);
                        }  else if (a.Country__c != cas.CountryCase__c && (cas.Origin == 'Webform' || cas.CaseSubSource__c == 'Webform')
                                    ) {
                            system.debug('Rfroce_WebtoCaseUtil : cas.AccountConflicts_web__c - '+cas.AccountConflicts_web__c);
                            cas.AccountConflicts_web__c = false;
                            cas.AccountId = null;
                            cas.ContactId = null;  
                        }
                        else if(cas.subtype__c== System.label.RForce_VOC_Call_Center) {
                            cas.AccountConflicts_web__c = false;
                            system.debug('Rfroce_WebtoCaseUtil : fi'+cas.AccountConflicts_web__c);
                        }
                         else {
                            cas.AccountConflicts_web__c = true;
                            system.debug('Rfroce_WebtoCaseUtil : final else - cas.AccountConflicts_web__c - '+cas.AccountConflicts_web__c);
                        }
                    }                    
                }
            }   
        }catch(Exception e){
            system.debug(e);
        }          
    }
}