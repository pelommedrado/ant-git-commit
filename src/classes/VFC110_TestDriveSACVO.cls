/**
* Classe que representa os dados da página de confirmação de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC110_TestDriveSACVO 
{
	/*atributos utilizados somente para controle interno, não são exibidos na tela*/
	public String accountId {get;set;}
	public String dealerOwnerId {get;set;} 
	public List<TDV_TestDrive__c> lstSObjTestDriveAgenda {get;set;}
	public List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicleAvailable{get;set;}
	public List<Product2> lstSObjModel {get;set;}
	
	/*atributos referentes a seção informações do test drive*/
	public Account sObjAccount {get;set;}
	public String vehicleInterest {get;set;}
	
	/*atributos referentes a seção informações do cliente*/
	public String customerName {get;set;}
	public String currentVehicle {get;set;}
	public String persLandline {get;set;}
	public Integer yearCurrentVehicle {get;set;}
	public String persMobPhone {get;set;}
	public String persEmail {get;set;}
	
	/*atributos referentes a seção agenda de veículos*/
	public String testDriveVehicleId {get;set;}
	public String plaque {get;set;}
	public String model {get;set;}
	public String version {get;set;}
	public String color {get;set;}
	public DateTime dateBooking {get;set;}
	public Date dateBookingNavigation {get;set;}
	public String dateBookingNavigationFmt {get;set;}
	public String selectedTime {get;set;}
	
	/*picklists que serão exibidos na tela*/
	public List<SelectOption> lstSelOptionVehicleInterest {get;set;}
	public List<SelectOption> lstSelOptionVehiclesAvailable {get;set;}
	
	/*matriz que contém a agenda do veículo (foi criado uma lista de listas para poder exibir a agenda na tela em formato tabular - matriz)*/
	public List<List<VFC114_ScheduleTestDriveSACVO>> lstAgendaVehicle {get;set;}
	
	public VFC110_TestDriveSACVO()
	{
		this.sObjAccount = new Account();
	}
}