/*
* @Project Name : Rforce
* @Name : Rforce_CaseCMDMSync_Test
* @Author: RNTBCI
* @Date : 29/06/2017
* @Description: Case and synchronisation CMDM - StopRcomFrom & StopRcomTo sending CMDM Flag
*/
@isTest
public class Rforce_CaseCMDMSync_Test {
    static testMethod void createNewCase(){
        system.debug('### Rforce_CaseCMDMSync_Test : createNewCase Starts...' );
        List<Case> lstCase = new List<Case>();
        Id cId;
        Country_Info__c ctr = new Country_Info__c (Name = 'Brazil', Country_Code_2L__c = 'BR', LanguageLocaleCode__c = 'en_US', Language__c = 'Français', Case_RecordType__c='BR_Case_RecType');
        insert ctr;

        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Brazil', alias = 'lro', Email = 'lrotondo@rotondo.com.test', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();            
            //Id recTypeId = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Account account7 = new Account(PersMobPhone__c='+111111111',CustmrStatus__c='Customer',FirstName = 'Ashwin', LastName = 'Maddala', 
            PersEmailAddress__c='test@renault.com', Country__c='France', BillingStreet='BillingStreet', BillingCity='BillingCity', BillingState='BillingState',
            Phone = '1234', ProfEmailAddress__c = 'maddalavr@mail.com', ShippingCity = 'city', ShippingCountry = 'Country1', 
            ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert account7;
           
            Case case1 = new Case(Origin = 'Webform',CaseSubSource__c = 'Webform', Type = 'Complaint', SubType__c = 'Booking', Status = 'New', Priority = 'Normal',
                        SuppliedPhone='1234', Address_Web__c='BillingStreet', City_Web__c='BillingCity', State_Web__c='BillingState',
                        CountryCase__c='Brazil', Description = 'Description', From__c = 'Customer', CatalogFuncCode__c='123456789',
                        SuppliedEmail = 'test@renault.com', AccountId = account7.Id,Dealer__c=account7.id);
           insert case1;
           case1.Status='Closed'; 
           Case1.CountryCase__c='Colombia';                     
           Update case1;                          
                              
           Test.stopTest();
      
        }
    }
    
    static testMethod void deleteCaseTest(){
        system.debug('### Rforce_CaseCMDMSync_Test : deleteCaseTest Starts...' );
        List<Case> lstCase = new List<Case>();
        Id cId;
        Country_Info__c ctr = new Country_Info__c (Name = 'Brazil', Country_Code_2L__c = 'BR', LanguageLocaleCode__c = 'en_US', Language__c = 'Français', Case_RecordType__c='BR_Case_RecType');
        insert ctr;

        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Brazil', alias = 'lro', Email = 'lrotondo@rotondo.com.test', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
           
            Account account7 = new Account(PersMobPhone__c='+111111111',CustmrStatus__c='Customer',FirstName = 'Ashwin', LastName = 'Maddala', 
            PersEmailAddress__c='test@renault.com', Country__c='France', BillingStreet='BillingStreet', BillingCity='BillingCity', BillingState='BillingState',
            Phone = '1234', ProfEmailAddress__c = 'maddalavr@mail.com', ShippingCity = 'city', ShippingCountry = 'Country1', 
            ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert account7;                        
           
           Case case2 = new Case(Origin = 'Webform',CaseSubSource__c = 'Webform', Type = 'Complaint', SubType__c = 'Booking', Status = 'New', Priority = 'Normal',
                        SuppliedPhone='12345', Address_Web__c='BillingStreet', City_Web__c='BillingCity', State_Web__c='BillingState',
                        CountryCase__c='Brazil', Description = 'Description', From__c = 'Customer', CatalogFuncCode__c='123456789',
                        SuppliedEmail = 'test@renault.com', AccountId = account7.Id);                     
           
           insert case2;
           
           Case case3 = new Case(Origin = 'Webform',CaseSubSource__c = 'Webform', Type = 'Complaint', SubType__c = 'Booking', Status = 'New', Priority = 'Normal',
                        SuppliedPhone='123456', Address_Web__c='BillingStreet', City_Web__c='BillingCity', State_Web__c='BillingState',
                        CountryCase__c='Brazil', Description = 'Description', From__c = 'Customer', CatalogFuncCode__c='123456789',
                        SuppliedEmail = 'test@renault.com', AccountId = account7.Id);                             
           
           insert case3;
           
           delete case2;                     
                              
           Test.stopTest();
      
        }
    }
        
}