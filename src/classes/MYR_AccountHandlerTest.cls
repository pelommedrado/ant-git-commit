@isTest
private class MYR_AccountHandlerTest {
	
	private static testMethod void testeOne(){

		test.startTest();

		MyOwnCreation moc = new MyOwnCreation();

		List<Id> newAccs = new List<Id>(); 

        Account account;
        Account acc;

        acc = moc.criaPersAccount();
		acc.ShippingPostalCode = '09820220';
		acc.BillingPostalCode = '09820220';
		insert acc;

		newAccs.add(acc.Id);

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('correiostest');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');
        
        Test.setMock(HttpCalloutMock.class, mock);

		MYR_AccountHandler.getBrazilAddressFromZipCode(newAccs, Utils.getProfileId('HeliosCommunity'));

		test.stopTest();

	}
	
}