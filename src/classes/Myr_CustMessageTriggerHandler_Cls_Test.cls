/** Apex class used to test the triggers on customer message tables **/
@isTest
private class Myr_CustMessageTriggerHandler_Cls_Test {
	
	@testSetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
        //initialize a list of customer message settings and customer message templates for tests
        Myr_Datasets_Test.prepareCustomerMessageSettings();
    }

	//Test the template validity when trying to insert it
    static testMethod void testTemplateValidityOnInsert() {
        Customer_Message_Settings__c custSetting = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3);
        insert custSetting;
        //BAD HTML TAGS IN TITLE (not included in the list on CS04_MYR_Settings) ==> OK (not checked)
        Customer_Message_Templates__c template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS <div>' + Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel '+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.', 
    		Summary__c='OTS ' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2, Channel__c='email');
    	Boolean lPassedThroughException = false;
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: TITLE NOT CHECKED ! SHOULD NOT OCCUR !');
    	}
    	//BAD HTML TAGS IN Summary (not included in the list on CS04_MYR_Settings) ==> KO
    	template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS ' + Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.', 
    		Summary__c='OTS <div color=>' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2, Channel__c='email');
    	lPassedThroughException = false;
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: SUMMARY NOT CHECKED ! SHOULD NOT OCCUR !');
    	}
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS ' + Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel <p>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.', 
    		Summary__c='OTS ' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2, Channel__c='email');
    	lPassedThroughException = false;
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS ' + Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel <div>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.', 
    		Summary__c='OTS ' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2, Channel__c='email');
    	lPassedThroughException = false;
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS ' + Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel <i color=>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.', 
    		Summary__c='OTS ' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2, Channel__c='email');
    	lPassedThroughException = false;
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//ONINSERT: PROPER HTML TAGS ==> OK
    	template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='OTS ' + system.Label.Customer_Message_Input1, 
    		Body__c='Suite au rappel<i>'+system.Label.Customer_Message_Input1+'</i>, merci d\'amener votre véhicule <b>'+system.Label.Customer_Message_Input2+'</b> chez votre concessionaire le plus proche.', 
    		Summary__c='OTS ' + system.Label.Customer_Message_Input1+' sur votre '+system.Label.Customer_Message_Input2, Channel__c='email');
    	system.debug('>>>>> template.Body__c='+template.Body__c);
    	system.debug('>>>>> template.Body__c unescape='+template.Body__c.unescapeHtml4());
    	try {
    		insert template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: PROPER HTML TAGS ! SHOULD NOT OCCUR !');
    	}
    	Customer_Message_Templates__c templateInserted = [SELECT Id, Body__c FROM Customer_Message_Templates__c WHERE Id=:template.Id];
    	system.assertEquals('Suite au rappel<i>'+system.Label.Customer_Message_Input1+'</i>, merci d\'amener votre véhicule <b>'+system.Label.Customer_Message_Input2+'</b> chez votre concessionaire le plus proche.', templateInserted.Body__c.unescapeHtml4());
    }
    
    //Test the template validity when trying to update it
    static testMethod void testTemplateValidityOnUpdate() {
        Customer_Message_Settings__c custSetting = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3);
        insert custSetting;
        Customer_Message_Templates__c template = new Customer_Message_Templates__c( MessageType__c=custSetting.Id, Country__c='France', Brand__c='Renault', Language__c='fr', 
    		Title__c='simple title', 
    		Body__c='simple body', 
    		Summary__c='simple summary'
			, Channel__c='email');
        insert template;
        //BAD HTML TAGS IN TITLE (not included in the list on CS04_MYR_Settings) ==> OK (not checked)
        template.Title__c='OTS <div>' + Label.Customer_Message_Input1; 
    	Boolean lPassedThroughException = false;
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: TITLE NOT CHECKED ! SHOULD NOT OCCUR !');
    	}
    	//BAD HTML TAGS IN Summary (not included in the list on CS04_MYR_Settings) ==> KO
    	template.Summary__c='OTS <div color=>' + Label.Customer_Message_Input1+' sur votre '+Label.Customer_Message_Input2;
    	lPassedThroughException = false;
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: SUMMARY NOT CHECKED ! SHOULD NOT OCCUR !');
    	}
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template.Body__c='Suite au rappel <p>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.'; 
    	lPassedThroughException = false;
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template.Body__c='Suite au rappel <div>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.'; 
    	lPassedThroughException = false;
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//BAD HTML TAGS IN Body (not included in the list on CS04_MYR_Settings) ==> KO
    	template.Body__c='Suite au rappel <i color=>'+Label.Customer_Message_Input1+', merci d\'amener votre véhicule '+Label.Customer_Message_Input2+' chez votre concessionaire le plus proche.'; 
    	lPassedThroughException = false;
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(true, e.getMessage().contains(system.Label.Customer_Message_Tpl_ErrHtmlTags));
    		lPassedThroughException = true;
    	}
    	system.assertEquals(true, lPassedThroughException);
    	//ONINSERT: PROPER HTML TAGS ==> OK
    	template.Body__c='Suite au rappel<i>'+system.Label.Customer_Message_Input1+'</i>, merci d\'amener votre véhicule <b>'+system.Label.Customer_Message_Input2+'</b> chez votre concessionaire le plus proche.'; 
    	try {
    		update template;
    	} catch( Exception e ) {
    		system.assertEquals(null, 'ERROR: PROPER HTML TAGS ! SHOULD NOT OCCUR !');
    	}
    	Customer_Message_Templates__c templateUpd = [SELECT Id, Body__c FROM Customer_Message_Templates__c WHERE Id=:template.Id];
    	system.assertEquals('Suite au rappel<i>'+system.Label.Customer_Message_Input1+'</i>, merci d\'amener votre véhicule <b>'+system.Label.Customer_Message_Input2+'</b> chez votre concessionaire le plus proche.', templateUpd.Body__c.unescapeHtml4());
    }
    
    //Test the template validity when trying to update it
    static testMethod void testSettingsValidityOnInsert() {
    	//try insertion without mergeaction and mergekeys
    	Customer_Message_Settings__c custSetting1 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3);
    	insert custSetting1;
		system.assertEquals( null, [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );
		
		//try insertion without mergekeys
		Customer_Message_Settings__c custSetting2 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3, MergeAction__c='keep');
		insert custSetting2;
		system.assertEquals( 'account__c;template__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting2.Id][0].MergeKeys__c );

		//try insertion with only account__c and other fields
		Customer_Message_Settings__c custSetting3 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3, MergeAction__c='keep', MergeKeys__c='account__c;input_1__c');
		insert custSetting3;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting3.Id][0].MergeKeys__c );

		//try insertion with only template__c and other fields
		Customer_Message_Settings__c custSetting4 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3, MergeAction__c='keep', MergeKeys__c='template__c;input_1__c');
		insert custSetting4;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting4.Id][0].MergeKeys__c );

		//try insertion with only other fields
		Customer_Message_Settings__c custSetting5 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3, MergeAction__c='keep', MergeKeys__c='input_1__c');
		insert custSetting5;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting5.Id][0].MergeKeys__c );
    }
    
    //Test the template validity when trying to update it
    static testMethod void testSettingsValidityOnUpdate() {
    	//try insertion without mergeaction and mergekeys
    	Customer_Message_Settings__c custSetting1 = new Customer_Message_Settings__c(Name='Case online', Type__c='CUS_CASE', NbInputs__c=3);
    	insert custSetting1;
		system.assertEquals( null, [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );

		//update to a mergeaction
		custSetting1.MergeAction__c = 'keep';
		update custSetting1;
		system.assertEquals( 'account__c;template__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c  );

		//update to keep only account and other fields
		custSetting1.MergeKeys__c = 'account__c;input_1__c';
		update custSetting1;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );

		//update to keep only type and other fields
		custSetting1.MergeKeys__c = 'template__c;input_1__c';
		update custSetting1;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );

		//update to keep only other fields
		custSetting1.MergeKeys__c = 'input_1__c';
		update custSetting1;
		system.assertEquals( 'account__c;template__c;input_1__c', [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );

		//update to remove the mergeaction
		custSetting1.MergeAction__c = '';
		update custSetting1;
		system.assertEquals( null, [SELECT MergeKeys__c FROM Customer_Message_Settings__c WHERE Id=:custSetting1.Id][0].MergeKeys__c );

    }
    
}