@isTest
public class BcsParsingEngineTest{
    
    static User usr;
    
    static {
        usr = new User (LastName = 'Rotondo', 
                        alias = 'lro', 
                        Email = 'lrotondo@rotondo.com', 
                        EmailEncodingKey = 'UTF-8', 
                        LanguageLocaleKey = 'en_US', 
                        LocaleSidKey = 'en_US', 
                        ProfileId = Label.PROFILE_SYSTEM_ADMIN, 
                        TimeZoneSidKey = 'America/Los_Angeles', 
                        UserName = 'lrotondo@lrotondo.com', 
                        BypassVR__c = true, 
                        BypassWF__c = true);
        
        insert usr;
    }
    
    static testMethod void testmet() {
        BcsParsingEngine bcpe=new BcsParsingEngine();
        
        List<BcsReponseObject> bcsList = new List<BcsReponseObject>();
        String formattedtext = '<response><clientList><idClient>1-9805-35812</idClient><lastName>LAPORTE</lastName><lang>FRA</lang><firstName>ALAIN</firstName><BCScommAgreement><Global.Comm.Agreement/><Preferred.Communication.Method/><Post.Comm.Agreement/><Tel.Comm.Agreement/><SMS.Comm.Agreement/><Fax.Comm.Agreement/><Email.Comm.Agreement/></BCScommAgreement><contact/><address><strName>4 PASSAGE JOUFFROY</strName><countryCode>FR</countryCode><zip>75009</zip><city>PARIS</city></address><typeperson>P</typeperson></clientList><clientList><idClient>1-98EM-37711</idClient><lastName>LAPORTE</lastName><title>1</title><lang>FRA</lang><firstName>HENRI</firstName><BCScommAgreement><Global.Comm.Agreement/><Preferred.Communication.Method/><Post.Comm.Agreement/><Tel.Comm.Agreement/><SMS.Comm.Agreement/><Fax.Comm.Agreement/><Email.Comm.Agreement/></BCScommAgreement><contact><phoneNum1>+330145781138</phoneNum1></contact><address><strName>65 RUE DE JAVEL</strName><countryCode>FR</countryCode><zip>75015</zip><city>PARIS</city></address><typeperson>P</typeperson></clientList><clientList><idClient>1-ACY6-12318</idClient><lastName>LAPORTE</lastName><title>1</title><lang>FRA</lang><firstName>JEAN MARC</firstName><BCScommAgreement><Global.Comm.Agreement/><Preferred.Communication.Method/><Post.Comm.Agreement/><Tel.Comm.Agreement/><SMS.Comm.Agreement/><Fax.Comm.Agreement/><Email.Comm.Agreement/></BCScommAgreement><contact><phoneNum1>+330142723266</phoneNum1></contact><address><strName>36  rue du Temple</strName><countryCode>FR</countryCode><zip>75004</zip><city>PARIS</city></address><typeperson>P</typeperson></clientList><clientList><idClient>1-ACWC-32847</idClient><lastName>LAPORTE</lastName><title>1</title><lang>FRA</lang><firstName>PHILIPPE</firstName><BCScommAgreement><Global.Comm.Agreement/><Preferred.Communication.Method/><Post.Comm.Agreement/><Tel.Comm.Agreement/><SMS.Comm.Agreement/><Fax.Comm.Agreement/><Email.Comm.Agreement/></BCScommAgreement><contact><phoneNum1>+330142801966</phoneNum1><phoneNum2>+330611117959</phoneNum2></contact><address><strName>2 rue de Londres</strName><countryCode>FR</countryCode><zip>75009</zip><city>PARIS</city></address><typeperson>P</typeperson><vehicleList> <vin>VF7867687978897</vin></vehicleList></clientList><nbReplies>4</nbReplies></response>';
        
        bcsList = bcpe.parseXML(formattedtext);            
    }

    @isTest
    static void testmet2() {
        Id RTID_COMPANY = [select Id 
                           from RecordType 
                           where sObjectType = 'Account' and DeveloperName = 'Company_Acc' 
                           limit 1].Id;

        Account Acc = new Account(Name = 'Test1', 
                                  Phone = '1000', 
                                  RecordTypeId = RTID_COMPANY, 
                                  ProfEmailAddress__c = 'addr1@mail.com', 
                                  ShippingCity = 'Paris', 
                                  ShippingCountry = 'France', 
                                  ShippingState = 'IDF', 
                                  ShippingPostalCode = '75013', 
                                  ShippingStreet = 'my street');
        insert Acc;
        
        Contact Con = new Contact(FirstName = 'Mandatory', 
                                  LastName = 'Test Contact', 
                                  Salutation = 'Mr.', 
                                  ProEmail__c = 'lro@lro.com', 
                                  AccountId = Acc.Id);
        insert Con;

        VEH_Veh__c v = new VEH_Veh__c (Name = '45678934567234567', 
                                       VehicleBrand__c = 'Renault', 
                                       DeliveryDate__c = date.parse('04/06/2010'));
        insert v;

        Case c = new Case (Origin = 'Mobile', 
                           Type = 'Information Request', 
                           SubType__c = 'Booking', 
                           VIN__c = v.Id, 
                           Status = 'Open', 
                           CaseBrand__c = 'RNO', 
                           Priority = 'Urgent', 
                           Kilometer__c = 100000, 
                           Description = 'Description ', 
                           From__c = 'Customer', 
                           AccountId = Acc.Id, 
                           ContactId = Con.Id);
        insert c;
        
        Id RecordId = [select Id from RecordType where sObjectType = 'Goodwill__c' and Name = 'CAR RENTAL' limit 1].Id;

        Goodwill__c g = new Goodwill__c (case__c = c.Id, ResolutionCode__c = 'Voucher', RecordTypeId = RecordId, ORDate__c = date.parse('02/02/2010'), ExpenseCode__c = 'OTS Issue', BudgetCode__c = 'SRC', Organ__c = 'BATTERY', SRCPartRate__c = 0.5, QuotWarrantyRate__c = 1000, Country__c = 'Brazil');
        Goodwill_Grid_Line__c ggl = new Goodwill_Grid_Line__c (Country__c = 'Slovakia', VehicleBrand__c = 'Renault', Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c = 800000);

        System.runAs(usr) {
            Test.startTest();
            Utils_VehicleDataSource uv = new Utils_VehicleDataSource();
            uv.Test = true;

            ApexPages.StandardController controller1=new ApexPages.StandardController(v); 

            VFC04_VehicleData vehcon = new VFC04_VehicleData(controller1);

            WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse response = uv.getVehicleData(vehcon);

            Test.stopTest();

        }

    }

}