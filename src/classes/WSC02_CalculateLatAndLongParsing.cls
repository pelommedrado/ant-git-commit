/**
* Class - WSC02_CalculateLatAndLongParsing
*   #01 <Suresh Babu> <20/10/2012>
*       Web service class to get Latitude and Longitude using Address information from Account.
*       Use json parsing to get Latitude and Longitude for given address.
*/
public class WSC02_CalculateLatAndLongParsing {
    
    public static boolean called = false;
    public static String latitude;
    public static String longitude;
    public static List<String> lstLatAndLng = new List<String>();
    /* constructor */
    public WSC02_CalculateLatAndLongParsing(){}
    /**
    *   This webservices Method was used to get LatAndLong from google using some fields
    *   @param  accountId   -   use Account Id and update lat and lang fields in account
    *   @param  street  -   use Account street field and get lat and lang
    *   @param  city    -   use Account city field and get lat and lang
    *   @param  state   -   use Account state field and get lat and lang
    *   @param  pinCode -   use Account pinCode field and get lat and lang
    *   @param  country -   use Account country field and get lat and lang 
    */
    @Future ( callout = true )
    public static void calloutToGoogleToGetLatAndLong(Id accountId, String street, String city, String state, String pinCode, String country){
        
        String endPointURL;
        /* google api url assi set in endPointUrl String */
        endPointURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        /* if Street is contains empty asn replace the value of "%20" */
        if( street.contains(' ') ){
            street = street.replace(' ', '%20');
            endPointURL += street + '+';
        }else{
            endPointURL += street+ '+';
        }
        /* if city is contains empty asn replace the value of "%20" */
        if( city.contains(' ') ){
            city = city.replace(' ', '%20');
            endPointURL += city + '+';
        }else{
            endPointURL += city + '+';
        }
        /* if state is contains empty asn replace the value of "%20" */
        if( state.contains(' ') ){
            state = state.replace(' ', '%20');
            endPointURL += state + '+';
        }else{
            endPointURL += state + '+';
        }
        /* if pinCode is contains empty asn replace the value of "%20" */
        if( pinCode.contains(' ') ){
            pinCode = pinCode.replace(' ', '%20');
            endPointURL += pinCode + '+';
        }else{
            endPointURL += pinCode + '+';
        }
        /* if country is contains empty asn replace the value of "%20" */
        if( country.contains(' ') ){
            country = country.replace(' ', '%20');
            endPointURL += country;
        }else{
            endPointURL += country;
        }
        /* concatinate the endpoint url and pass the json value in url */
        endPointURL += '&output=json&sensor=false';
        
        system.Debug(' end point URL -->'+endPointURL);
        /* This method called lat and lang value update in account using accountId and endpointurl */
        combineCalloutMethods( endPointURL, accountId );
        system.Debug('lstLatAndLng ---->'+lstLatAndLng);
        
    }
    /*  This method using validate to the new account and set the value in street, city, state, postalcode and country */
    public static void validateNewAccount( List<Account> accTrigger ){
        /* get accountrecordtype id from RecordType object using Developer Name */  
        Id accountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Network_Site_Acc' );
        /* Iterate the Account trigger */
        for( Account acc : accTrigger ) {
            /* if check new account recordtype Id equal to query returned account record type Id */
            if( acc.RecordTypeId == accountRecordTypeId ){
                String Street, City, State, PostalCode, Country;
                /* check and assign Account street, state, city,country to tempary values */ 
                Street      =   ( acc.ShippingStreet == null ? '' : acc.ShippingStreet );
                City        =   ( acc.ShippingCity == null ? '' : acc.ShippingCity );
                State       =   ( acc.ShippingState == null ? '' : acc.ShippingState );
                PostalCode  =   ( acc.ShippingPostalCode == null ? '' : acc.ShippingPostalCode );
                Country     =   ( acc.ShippingCountry == null ? '' : acc.ShippingCountry );
                if( Street != '' && City != '' && Country != '' ){
                    /* calculate the lat and lang field using Account Id, Street, city, state, postal code and country */
                    WSC02_CalculateLatAndLongParsing.calloutToGoogleToGetLatAndLong( acc.Id, Street, City, State, PostalCode, Country);
                }
            }
        }
    }
    /* This method using validate and update the  Account */
    public static void validateUpdateAccount( List<Account> arrAccountNew, List<Account> arrAccountOld){
        /* get accountrecordtype id from RecordType object using Developer Name */ 
        Id accountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Network_Site_Acc' );
        /* Iterate the New Account List */
        for (Integer idx = 0; idx < arrAccountNew.size(); idx++)  {
            Account newAcc = arrAccountNew[idx];
            Account oldAcc = arrAccountOld[idx];
            /* if check New account Shipping street, city, state, country, postalcode is not equal to Old Account Shipping Street, State, Country, city values */
            if ( newAcc.ShippingStreet != oldAcc.ShippingStreet
                ||  newAcc.ShippingCity != oldAcc.ShippingCity
                ||  newAcc.ShippingState != oldAcc.ShippingState
                ||  newAcc.ShippingPostalCode != oldAcc.ShippingPostalCode
                ||  newAcc.ShippingCountry != oldAcc.ShippingCountry ){
                /* if check new account recordtype Id equal to query returned account record type Id */
                if( newAcc.RecordTypeId == accountRecordTypeId ){
                    String Street, City, State, PostalCode, Country;
                    /* check and assign Account street, state, city,country to tempary values */ 
                    Street      =   ( newAcc.ShippingStreet == null ? '' : newAcc.ShippingStreet );
                    City        =   ( newAcc.ShippingCity == null ? '' : newAcc.ShippingCity );
                    State       =   ( newAcc.ShippingState == null ? '' : newAcc.ShippingState );
                    PostalCode  =   ( newAcc.ShippingPostalCode == null ? '' : newAcc.ShippingPostalCode );
                    Country     =   ( newAcc.ShippingCountry == null ? '' : newAcc.ShippingCountry );
                    if( Street != '' && City != '' && Country != '' ){
                        system.debug('*** calloutToGoogleToGetLatAndLong()');
                        /* calculate the lat and lang field using New Account Id, Street, city, state, postal code and country */
                        WSC02_CalculateLatAndLongParsing.calloutToGoogleToGetLatAndLong( newAcc.Id, Street, City, State, PostalCode, Country);
                    }
                }
            }
        }
    }


    /* This method using call google api and get response then update account */
    public static void combineCalloutMethods( String endPoint, Id accountId) {
        if(accountId != null || test.isRunningTest())
        {
        	system.debug('*** called='+called);
            if (called == false)
            {
            	called = true;
            	system.debug('*** Calling: '+endPoint);
            	
	            Http h = new Http();
	            try {
	                /* return request using endpoint url assign to http request */
	                HttpRequest req = formRequest( endPoint );
	                /* return response using Request and http method using send callout */
	                
	                String JSONContent = ';';
	                if(test.isRunningTest()){
	                    JSONContent = '{"results":[{"address_components":[{"long_name":"5001","short_name":"5001","types":["street_number"]},{"long_name":"Avenida das Américas","short_name":"Av. das Américas","types":["route"]},{"long_name":"Barra da Tijuca","short_name":"Barra da Tijuca","types":["sublocality","political"]},{"long_name":"Rio de Janeiro","short_name":"Rio de Janeiro","types":["locality","political"]},{"long_name":"Rio de Janeiro","short_name":"RJ","types":["administrative_area_level_1","political"]},{"long_name":"Brazil","short_name":"BR","types":["country","political"]},{"long_name":"22631-004","short_name":"22631-004","types":["postal_code"]}],"formatted_address":"Avenida das Américas, 5001 - Barra da Tijuca, Rio de Janeiro, 22631-004, Brazil","geometry":{"bounds":{"northeast":{"lat":-23.00028070,"lng":-43.36205340},"southwest":{"lat":-23.00029760,"lng":-43.36205550}},"location":{"lat":-23.00029760,"lng":-43.36205550},"location_type":"RANGE_INTERPOLATED","viewport":{"northeast":{"lat":-22.99894016970850,"lng":-43.36070546970850},"southwest":{"lat":-23.00163813029150,"lng":-43.36340343029149}}},"types":["street_address"]}],"status":"OK"}';
	                }else{
	                    HttpResponse res = sendCallout(h, req);
	                    if( res.getStatusCode() == 200 ){
	                        //system.Debug(' REsponse -->'+res.getBody());
	                        /* get Response result using Json Parser */
	                        JSONContent = String.valueOf( res.getBody() );
	                    }
	                }
	                
	                system.debug('*** JSONContent='+JSONContent);
	
	                /* This method called get response using response */
	                getResponse(JSONContent );
	                /* update Account using Account Id */
	                Account acc = new Account( Id = accountId );
	                acc.Latitude__c = latitude;
	                acc.Longitude__c = longitude;
	                
	                system.Debug('*** account -->'+acc);
	                /* update account using VFC12_AccountDAO sobject */
	                VFC12_AccountDAO.getInstance().updateData( acc );
	            } catch(Exception e){
	                system.debug('*** Failed to execute callout!' + e.getMessage());
	            }
            } else {
            	system.debug('*** Already called: '+endPoint);
            }
        }
        
        
    }
    /* This method using request values assign in http request */
    public static HttpRequest formRequest( String endPoint){
        Httprequest req = new Httprequest();
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setEndpoint( endPoint );
        return req;
    }
     /* This method using request sent to http response */
    public static HttpResponse sendCallout(Http h, HttpRequest req){
         HttpResponse res = h.send(req);
         return res;
     }
     /* This method using get response values and add lat and lang list*/
    public static void getResponse( String res ){
        JSONParser parser = JSON.createParser( res );
        /*  if check parser token value is not null */
        while (parser.nextToken() != null) {
            
            if( parser.getCurrentName() == 'location' ){
                parser.nextToken();
                parser.nextToken();
                if( parser.getCurrentName() == 'lat' ){
                    parser.nextToken();
                    latitude = parser.getText();
                }
                parser.nextToken();
                if( parser.getCurrentName() == 'lng' ){
                    parser.nextToken();
                    longitude = parser.getText();
                }
            }
        }
    }
}