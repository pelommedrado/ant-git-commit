public class DashboardAtividadeService {

  public static Map<String, List<String>> mapValueSubject {
    get {
      Map<String, String[]> mapValues = new Map<String, String[]>();

      final String canceled = 'Canceled:Oportunidade Perdida';
      final String completed = 'Completed:Atividade Realizada';
      final String extended = 'Extended:Atividade Prorrogada';

      mapValues.put(ActivityManagerBo.confirmTestDrive, new String[] {
        'Completed:Test Drive Confirmado', canceled,
        'Extended:Test Drive Prorrogado'
      });
      mapValues.put(ActivityManagerBo.performTestDrive, new String[] {
        'Completed:Test Drive Realizado', canceled,
        'Extended:Test drive Prorrogado'
      });
      mapValues.put(ActivityManagerBo.confirmVisit, new String[] {
        'Completed:Vista Confirmada', canceled,
        'Extended:Visita Prorrogada'
      });
      mapValues.put(ActivityManagerBo.performVisit, new String[] {
        'Completed:Visita Realizada', canceled,
        'Extended:Visita Prorrogada'
      });

      mapValues.put(ActivityManagerBo.custemerService, new String[] {
        completed, canceled, extended
      });
      mapValues.put(ActivityManagerBo.contact, new String[] {
        completed, canceled, extended
      });
      mapValues.put(ActivityManagerBo.negotiation, new String[] {
        'Completed:Seguir Negociação', canceled
      });

      mapValues.put(ActivityManagerBo.persecution, new String[] {
        'Completed:Perseguição Realizada', canceled, extended
      });
      mapValues.put(ActivityManagerBo.rescue, new String[] {
        'Completed:Oportunidade Resgatada', extended
      });
      mapValues.put(ActivityManagerBo.deliveryConfirm, new String[] {
        completed, extended
      });

      return mapValues;
    }
  }

  public static List<SelectOption> optionActivity(String status, Boolean isNext) {
      final List<SelectOption> optionList = new List<SelectOption>();

      if(String.isEmpty(status)) {
        return optionList;
      }

      final String[] values = mapValueSubject.get(status);

      if(values == null) {
        return optionList;
      }

      if(isNext) {
        optionList.add(new SelectOption('In Progress', 'Pendente'));
      }

      for(String val : values) {
        String[] atrr = val.split(':');
        optionList.add(new SelectOption(atrr[0], atrr[1]));
      }

      return optionList;
  }
}