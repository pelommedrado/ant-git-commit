@isTest
private class VFC44_AssigningOpportunityControllerTest {

	static testMethod void unitTest01()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                      IDBIR__c = '1000');
		insert account1;

		Contact contact1 = new Contact(FirstName='Test FirstName', LastName='Test LastName',Salutation='Mr.',ProEmail__c ='test01@test.com',AccountId=account1.Id);
		insert contact1;

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id);
		insert opportunity1;
		
    	Task newTask = new Task();
		newTask.WhatId = opportunity1.Id;
		newTask.Subject = Label.OPPCustomerService;
		newTask.OwnerId = UserInfo.getUserId();
		newTask.ActivityDate = system.today();
		newTask.Description = Label.OPPCustomerWaiting;
		newTask.Priority = 'High';
		//newTask.ReminderDateTime = currentDateTime;
		insert newTask;

    	Apexpages.currentPage().getParameters().put('taskId', newTask.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(newTask);

    	// Start Test
    	Test.startTest();

        VFC44_AssigningOpportunityController controller = new VFC44_AssigningOpportunityController();
 		controller.sendToSeller();

		// Stop Test
		Test.stopTest();
    }
}