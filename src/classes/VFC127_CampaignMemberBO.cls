public with sharing class VFC127_CampaignMemberBO
{
	private static final VFC127_CampaignMemberBO instance = new VFC127_CampaignMemberBO();
    
	private VFC127_CampaignMemberBO(){}

    public static VFC127_CampaignMemberBO getInstance(){
        return instance;
    }
    
    
    /**
     * [before insert]
     * Check if the user profile are allowed to include campaing members
     */
    public void checkIfProfileCanInsert(List<CampaignMember> lstNewCampaignMember)
    {
    	system.debug(LoggingLevel.INFO, '*** checkIfProfileCanInsert()');
    	
		// Check if the user profile must be checked for inseting data	
    	Profile userProfile = VFC32_ProfileDAO.getInstance().findById(UserInfo.getProfileId());
    	if (VFC126_CampaignConfig.setMarketingProfiles.contains(userProfile.Name)
    	 || VFC126_CampaignConfig.setDealerProfiles.contains(userProfile.Name)
    	 || VFC126_CampaignConfig.setInterfaceProfiles.contains(userProfile.Name))
		{
    		Set<Id> setCampaign = new Set<Id>();
	    	//  Get all campaigns Ids
	    	for (CampaignMember cm : lstNewCampaignMember)
	    	{
	    		setCampaign.add(cm.CampaignId);
	    	}
	
			// Fetch campaign data
			Map<Id, Campaign> mapCampaign = VFC121_CampaignDAO.getInstance().
				fetchCampaignByIdSet(setCampaign);
	    	
			for (CampaignMember cm : lstNewCampaignMember)
	    	{
	    		Campaign campaign = mapCampaign.get(cm.CampaignId);
	    		if (campaign.ManualInclusionAllowed__c == false
	    		 && VFC126_CampaignConfig.setMarketingProfiles.contains(userProfile.Name))
	    		{
	    			cm.addError('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha.');
	    		}
	    		else if (campaign.DealerInclusionAllowed__c == false
	    		      && VFC126_CampaignConfig.setDealerProfiles.contains(userProfile.Name))
				{
					cm.addError('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha.');
				}
	    	}
		}
    }

    /**
     * [before delete]
     * Check if the user profile are allowed to delete campaing members
     */
    public void checkIfProfileCanDelete(List<CampaignMember> lstCampaignMember)
    {
    	system.debug(LoggingLevel.INFO, '*** checkIfProfileCanDelete()');
    	
    	// Check if the user profile must be checked for inseting data
    	CS_Permission__c cs = CS_Permission__c.getInstance();
    	if (cs.CanDeleteCampaignMember__c != 'TRUE')
    	{
	    	for (CampaignMember cm : lstCampaignMember)
	    	{
	    		cm.addError('Campanha: o perfil do seu usuário não permite excluir membros de campanha.');
	    	}
		}
    }
    
    /**
     * Returns a list of non-existing CM based in the campaignId and LeadId.
     */
    public List<CampaignMember> removeExistingCM(List<CampaignMember> lstNewCM)
    {
    	List<CampaignMember> lstCleanedCM = new List<CampaignMember>();

    	// Get data to query DB for CM
    	Set<Id> setCampaignId = new Set<Id>();
		Set<Id> setLeadId = new Set<Id>();
		for (CampaignMember cm : lstNewCM) {
			setCampaignId.add(cm.CampaignId);
			setLeadId.add(cm.LeadId);
		}
		
		// Retrieve existing CM by campaignId and LeadId
		// The string is: CampaignId + '-' + LeadId
		Set<String> setCM = VFC128_CampaignMemberDAO.getInstance().
			fetchCMByLeadIdAndCampaignId(setCampaignId, setLeadId);
		
		for (CampaignMember cm : lstNewCM) {
			if (setCM.contains(cm.CampaignId+'-'+cm.LeadId)) {
				// Remove this CM
				system.debug(LoggingLevel.INFO, '*** CM removed: campaignId['+cm.CampaignId+'], LeadId['+cm.LeadId+']');
			}
			else {
				lstCleanedCM.add(cm);
			}
		}
		return lstCleanedCM;
    }
    
    /**
     * [before insert]
     * Check for a Web2toLead insertion (Lead and CampaignMember), we should convert
	 * the Lead if the CM is associated do a Campaign that allows the convertion
	 * and the Lead has a dealer associated
     */
    public void checkIfShouldConvertLeadIntoOpportunity(List<CampaignMember> lstNewCampaignMember)
    {
    	system.debug(LoggingLevel.INFO, '*** checkIfShouldConvertLeadIntoOpportunity()');
    	
    	Set<Id> setCampaign = new Set<Id>();
    	Set<String> setLead = new Set<String>();

    	for (CampaignMember cm : lstNewCampaignMember) {
    		setCampaign.add(cm.CampaignId);
    		if (cm.LeadId != null)
    			setLead.add(cm.LeadId);
    	}
	
		// Fetch campaign data
		Map<Id, Campaign> mapCampaign = VFC121_CampaignDAO.getInstance().fetchCampaignByIdSet(setCampaign);
    	
    	// Fetch lead data
    	Map<Id, Lead> mapLead = VFC09_LeadDAO.getInstance().returnMapWithIdAndLeadRecord(setLead);
    	
    	for (CampaignMember cm : lstNewCampaignMember)
    	{
            System.debug('cm.LeadId: ' + cm.LeadId);
            
    		if (cm.LeadId != null)
    		{
    			Lead aLead = mapLead.get(cm.LeadId);
    			Campaign aCampaign = mapCampaign.get(cm.CampaignId);
                
                System.debug('aLead.DealerOfInterest__c: ' + aLead.DealerOfInterest__c);
                System.debug('aCampaign.WebToOpportunity__c: ' + aCampaign.WebToOpportunity__c);
                System.debug('ChecaSeDeveConverterLead(aLead): ' + ChecaSeDeveConverterLead(aLead));
                
    			if (aCampaign.WebToOpportunity__c == true && aLead.DealerOfInterest__c != null && ChecaSeDeveConverterLead(aLead) ) {
    				
    				// Convert lead and create an opportunity
    				system.debug('*** Convert lead and create an opportunity ...');
    				VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(aLead, aCampaign);
    				
    				// In this case, change the cm Status
    				cm.Status = 'Responded';
    			}
    		}
    	}
    }
    
    public Boolean ChecaSeDeveConverterLead(Lead aLead){
        Boolean toConvert = true;
        
        if(aLead.TransactionCode__c != null){
            return true;
        }
        
        List<ManageLeadConversion__c> customSettings = ManageLeadConversion__c.getAll().values();
        for(ManageLeadConversion__c cs: customSettings){
            String field = (String)aLead.get(cs.Field__c);
            if( field != null && field.equalsIgnoreCase(cs.Value__c)){
                toConvert = false;
                break;
            }
        }
        return toConvert;
    }
}