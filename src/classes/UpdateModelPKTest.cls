@isTest
private class UpdateModelPKTest {
    
    @isTest static void test_method_one() {
        Model__c model = new Model__c( Model_PK__c = 'ABC-0', Model_Spec_Code__c = 'ABC', Phase__c = '0' );
        insert model;

        PV_ModelMilesime__c milesime = new PV_ModelMilesime__c( Model__c = model.Id, ENS__c = 'ENS', Milesime_PK__c = 'ABC-0ENS' );
        insert milesime;

        PVVersion__c version = new PVVersion__c( 
            Model__c = model.Id, 
            Milesime__c = milesime.Id, 
            Name = 'ABC', 
            Version_PK__c = 'ABC-0ENSVSC', 
            Version_Id_Spec_Code__c = 'VSC',
            PVC_Maximo__c = 0, 
            PVR_Minimo__c = 0, 
            Price__c = 0
        );
        insert version;

        Optional__c optional = new Optional__c( Version__c = version.Id, Optional_Code__c = 'OC', Optional_PK__c = 'ABC-0ENSVSCOC' );
        insert optional;


        Test.startTest();

        Database.executeBatch( new UpdateModelPK(), 1 );

        Test.stopTest();
    }
    
}