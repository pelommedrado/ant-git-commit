/**
	Class   -   VFC01_SObjectDAO_Test
    Author  -   RameshPrabu
    Date    -   13/09/2012
    
    #01 <RameshPrabu> <13/09/2012>
        Created this class using test for VFC01_SObjectDAO.
**/
@isTest
private class VFC01_SObjectDAO_Test {

    static testMethod void VFC01_SObjectDAO_Test() {
        // TO DO: implement unit test
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        VFC09_LeadDAO.getInstance().findLeadByLeadId(leads[0].Id);
        List<String> deleteData = new List<String>();
        deleteData.add(leads[0].Id);
        List<Lead> lstLeadInsert = new List<Lead>();
        Lead leadInsert = new Lead();
        leadInsert.LastName = 'Ramesh';
        lstLeadInsert.add(leadInsert);
        
        Lead leadInsert1 = new Lead();
        leadInsert1.LastName = 'Ramesh';
        
        Test.startTest();
        List<Database.SaveResult> insertList = VFC09_LeadDAO.getInstance().insertData(lstLeadInsert);
        Test.stopTest();
        System.assert(insertList.size() > 0);
        
        VFC09_LeadDAO.getInstance().insertData(leadInsert1);
        VFC09_LeadDAO.getInstance().updateData(leads[0]);
        VFC09_LeadDAO.getInstance().updateData(leads);
        VFC09_LeadDAO.getInstance().upsertData(leads);
        VFC09_LeadDAO.getInstance().deleteData(deleteData);
        VFC09_LeadDAO.getInstance().deleteData(leads[1].Id);
        
    }
}