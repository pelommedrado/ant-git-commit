/**
*	Class	-	VFC31_TestDriveVehicleBO
*	Author	-	RameshPrabu
*	Date	-	15/11/2012
*
*	#01 <RameshPrabu> <15/11/2012>
*		BO class for Test Drive object to logical function.
**/
public with sharing class VFC31_TestDriveVehicleBO {
	private static final VFC31_TestDriveVehicleBO instance = new VFC31_TestDriveVehicleBO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC31_TestDriveVehicleBO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC31_TestDriveVehicleBO getInstance(){
        return instance;
    }
    /**
    * 	This Method was used to some validate the  Test Drive vehicle Object fields before insertion(like vehicle is already reserved  or not in this time period.)
    * 	@param	testDriveVehTrigger	-	New Test Drive vehicle record passed  and validate Test Drive vehicle Object fields.
    */
    public void validateNewTestDriveVehicle( List<TDV_TestDriveVehicle__c> testDriveVehTrigger ){
    	
    	for( TDV_TestDriveVehicle__c testDriveVehicleRecord : testDriveVehTrigger ){
    		/* New TestDriveVehicle record pass and return the list of TestDrive vehicle records */
	    	List<TDV_TestDriveVehicle__c> lstNewTestDriveVehicles = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingTestDriveVehicle( testDriveVehicleRecord );
	    	/*  if check lstNewTestDriveVehicles is not equal to empty */
	    	if(!lstNewTestDriveVehicles.isEmpty()){
	    		/* add error message in New page */
	    		String errorMessage = Label.ERRTDVMessage1;
		    	testDriveVehicleRecord.addError( errorMessage );
	    	}
    	}
    }
    /**
    * 	This Method was used to some validate the  Test Drive vehicle Object fields before updation (like vehicle is already reserved  or not in this time period.)
    * 	@param	newTestDriveVehTrigger	-	New Test Drive vehicle record passed  and validate Test Drive vehicle Object fields.
    * 	@param	oldTestDriveVehTrigger	-	old Test Drive vehicle record passed  and validate Test Drive vehicle Object fields.
    */
    public void validateEditTestDriveVehicle( List<TDV_TestDriveVehicle__c> newTestDriveVehTrigger, List<TDV_TestDriveVehicle__c> oldTestDriveVehTrigger){
    	
    	for( TDV_TestDriveVehicle__c newTestDriveVehicles : newTestDriveVehTrigger ){
    		for( TDV_TestDriveVehicle__c oldTestDriveVehicles : oldTestDriveVehTrigger ){
    			/* if check value changed of AccountId, VehicleId, Opening date  and closing date */ 
				if(newTestDriveVehicles.Account__c != oldTestDriveVehicles.Account__c || newTestDriveVehicles.Vehicle__c != oldTestDriveVehicles.Vehicle__c ||
					newTestDriveVehicles.AgendaOpeningDate__c != oldTestDriveVehicles.AgendaOpeningDate__c || newTestDriveVehicles.AgendaClosingDate__c != oldTestDriveVehicles.AgendaClosingDate__c ||
					newTestDriveVehicles.Available__c != oldTestDriveVehicles.Available__c){
					/* New TestDriveVehicle record pass and return the list of TestDrive vehicle records */
					List<TDV_TestDriveVehicle__c> lstNewTestDriveVehicles = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingTestDriveVehicle(newTestDriveVehicles);
			    	/*  if check lstNewTestDriveVehicles is not equal to empty */
			    	if(!lstNewTestDriveVehicles.isEmpty()){
			    		/* add error message in Edit page */
			    		String errorMessage = Label.ERRTDVMessage1;
				    	newTestDriveVehicles.addError(errorMessage);
			    	}
				}
    		}
    	}
    }
}