public class RenaultMaisCliente_Utils{
    private static Id userId = UserInfo.getUserId();
    
	public static List<String> permissionSetAvailable(){
        
        List<String> permisionNameList = new List<String>();
        
        List<SFA_PermissionSetAvailable__c> permissoesDisponiveisList = SFA_PermissionSetAvailable__c.getAll().values();
        for(SFA_PermissionSetAvailable__c permission : permissoesDisponiveisList)
            permisionNameList.add(permission.permissionSet_Name__c);

        Set<Id> permissionSetAssingnmentIdSet = new Set<Id>();
        for(PermissionSetAssignment permissionSetAssi : [SELECT PermissionSetId FROM PermissionSetAssignment where  AssigneeId =: userId])
            permissionSetAssingnmentIdSet.add(permissionSetAssi.PermissionSetId);

        List<String> permissionSetList = new List<String>();
        for(PermissionSet permissionSet : [select Name,id from PermissionSet where id in: (permissionSetAssingnmentIdSet) and Name in:(permisionNameList)]){
			permissionSetList.add(permissionSet.Name);
        }
        return permissionSetList;
    }
    
    

    public static Map<String,Profile> getProfileMap(String profileName){
        Map<String,Profile> profileMap = new Map<String,Profile>();
        for(Profile perfil : [select id, Name from Profile where Name =: profileName])
            profileMap.put(perfil.Name, perfil);
        return profileMap;
    }
    
}