/*****************************************************************************************
    Name    : BatchToUpdatevehicle_Schedule
    Desc    : This is to schedule BatchToUpdatevehicle class using Standard UI
    Approach: Standard UI
                
                                                
 Modification Log : 
-----------------------------------------------------------------------------------------
Developer                        Date               Description
----------------------------------------------------------------------------------------
Praveen Ravula                  4/24/2013           Created 

******************************************************************************************/



global class  BatchToUpdatevehicle_Schedule implements Schedulable{
  global void execute(SchedulableContext ctx) {
      BatchToUpdatevehicle bvm = new BatchToUpdatevehicle();
       Database.executeBatch(bvm, Limits.getLimitCallouts());
  }
}