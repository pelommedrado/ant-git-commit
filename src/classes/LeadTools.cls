public class LeadTools{

    public static Account gerDealerByBIR(String stringBIR)
    {
        List<Account> lstAccountsByCriteria = null;
        Account ret = null;
        String stringBIRWithZero = '0'+stringBIR;
        
        lstAccountsByCriteria = [SELECT Id, Name, IDBIR__c 
                                   FROM Account 
                                  WHERE IDBIR__c =: stringBIR
                                     OR IDBIR__c =: stringBIRWithZero
                                  order by IDBIR__c desc 
                                  limit 1];
        if(lstAccountsByCriteria != null && lstAccountsByCriteria.size() > 0){
            ret = lstAccountsByCriteria.get(0);
        }                                  
        return ret;
    }
}