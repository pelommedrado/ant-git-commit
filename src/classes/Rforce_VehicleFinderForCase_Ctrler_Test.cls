/**
 * An apex page controller that exposes the change password functionality
 */
@IsTest public class  Rforce_VehicleFinderForCase_Ctrler_Test{
    
      public static testMethod  void testSearchVin() {
         
      Test.startTest();
      
      list<Account> accLst= new list<Account>();
      Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Company_Account_RecType' limit 1].Id;
      Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingPostalCode = '75013', ShippingStreet = 'my street');
      insert Acc;
      VEH_Veh__c veh = new VEH_Veh__c(Name='VF1KMRF0533061370',VehicleBrand__c='Renault');
      insert veh;
      VRE_VehRel__c vre = new VRE_VehRel__c(Account__c=Acc.Id,VIN__c=veh.id,Status__c='Active',TypeRelation__c='Owner');
            insert vre;  
      PageReference PageRef = Page.Rforce_VehicleFinderForCase_Page;
      PageReference PageRef2 = Page.Rforce_VehicleFinderForCase_Page;
      
      Test.setCurrentPage(pageRef);
      
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Acc);
      
      Apexpages.currentPage().getParameters().put('Id',Acc.Id);
      //pageRef.getParameters().put('IdCase','1-244507361');
      
      Rforce_VehicleFinderForCase_Controller svfatc = new Rforce_VehicleFinderForCase_Controller();
      svfatc.getvehiclePageList();
      Rforce_BVM_WS.ApvGetDetVehXmlResponse ws = new Rforce_BVM_WS.ApvGetDetVehXmlResponse();
      Rforce_BVM_WS.DetVeh d = new Rforce_BVM_WS.DetVeh();
      
      Rforce_BVMComplet_WS.ApvGetDetVehResponse ws2 = new Rforce_BVMComplet_WS.ApvGetDetVehResponse();
      Rforce_BVMComplet_WS.DetVeh d2 = new Rforce_BVMComplet_WS.DetVeh();
      
      d.tvv = 'test';
      d.libCarrosserie='test';
      d.dateTcmFab='01/01/2000';
      d.dateLiv='01/01/2000';
      d.indMot='test';
      d.NMot='test';
      d.typeMot='tset';
      d.indBoi='test';
      d.NBoi='test';
      d.typeBoi='test';
      d.libModel='test';
      d.marqCom='test';
      d.NFab='test';
      d.version='test';
      ws.detVeh = d;
      
      d2.bvmso24Tvv = 'test';
      d2.bvmso24LibCarrosserie='test';
      d2.bvmso24DateTcmFab='01/01/2000';
      d2.bvmso24DateLiv='01/01/2000';
      d2.bvmso24IndMot='test';
      d2.bvmso24NMot='test';
      d2.bvmso24TypeMot='tset';
      d2.bvmso24IndBoi='test';
      d2.bvmso24NBoi='test';
      d2.bvmso24TypeBoi='test';
      d2.bvmso24LibModel='test';
      d2.bvmso24MarqCom='test';
      d2.bvmso24NFab='test';
      d2.bvmso24Version='test';
      d2.bvmso24NRecCom='test';
      d2.bvmso24PaysImmat='FR';
      d2.bvmso24RefBoi='test';
      d2.bvmso24RefMot='test';
      d2.bvmso24LibUsimot='test';
      d2.bvmso24LibUsiboi='test';
      d2.bvmso24LibUsiveh='test';
      d2.bvmso24Ligne2P12='test';
      d2.bvmso24PlacSpec='test';
      d2.bvmso24Ligne3P12='test';
      d2.bvmso24Ligne4P12='test';
      d2.bvmso24Ptmaav='test';
      d2.bvmso24Ptr='test';
      d2.bvmso24Ptma='test';
      d2.bvmso24Ptmaar='test';
      
      ws2.detVeh = d2;
      
      VEH_Veh__c Vehicule = new VEH_Veh__c (Name = '21342312323123456',  VehicleBrand__c = 'Renault', KmCheck__c=100 , KmCheckDate__c = date.today());
      insert Vehicule;
      svfatc.VIN='21342312323123456';
      svfatc.VRN='21342312323123456';
      Apexpages.currentPage().getParameters().put('Id','111');
      
      svfatc.searchVehicle();
      pageRef = svfatc.createVehicleBVM(ws2);
      pageRef= svfatc.SelectVehicle();  
      svfatc.Skip();  
      pageRef= svfatc.Cancel();    
      ws = svfatc.searchBVM();
      
      svfatc.searchVehicle();
      pageRef2 = svfatc.createVehicleBVM(ws2);
      pageRef2= svfatc.SelectVehicle();  
      svfatc.Skip();  
      pageRef2= svfatc.Cancel();    
      ws2 = svfatc.searchBVMComplet();
      Test.stopTest();       

     }
     
           public static testMethod  void Vinnull() {
         
      Test.startTest();
      
      list<Account> accLst= new list<Account>();
      Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Company_Account_RecType' limit 1].Id;
      Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingPostalCode = '75013', ShippingStreet = 'my street');
      insert Acc;
     
      PageReference PageRef = Page.Rforce_VehicleFinderForCase_Page;
      PageReference PageRef2 = Page.Rforce_VehicleFinderForCase_Page;
      
      Test.setCurrentPage(pageRef);
      
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Acc);
      
      Apexpages.currentPage().getParameters().put('Id',Acc.Id);
      //pageRef.getParameters().put('IdCase','1-244507361');
      
      Rforce_VehicleFinderForCase_Controller svfatc = new Rforce_VehicleFinderForCase_Controller();
     
      Rforce_BVM_WS.ApvGetDetVehXmlResponse ws = new Rforce_BVM_WS.ApvGetDetVehXmlResponse();
      Rforce_BVM_WS.DetVeh d = new Rforce_BVM_WS.DetVeh();
      
      Rforce_BVMComplet_WS.ApvGetDetVehResponse ws2 = new Rforce_BVMComplet_WS.ApvGetDetVehResponse();
      Rforce_BVMComplet_WS.DetVeh d2 = new Rforce_BVMComplet_WS.DetVeh();
      
      d.tvv = 'test';
      d.libCarrosserie='test';
      d.dateTcmFab='01/01/2000';
      d.dateLiv='01/01/2000';
      d.indMot='test';
      d.NMot='test';
      d.typeMot='tset';
      d.indBoi='test';
      d.NBoi='test';
      d.typeBoi='test';
      d.libModel='test';
      d.marqCom='test';
      d.NFab='test';
      d.version='test';
      ws.detVeh = d;
      
      d2.bvmso24Tvv = 'test';
      d2.bvmso24LibCarrosserie='test';
      d2.bvmso24DateTcmFab='01/01/2000';
      d2.bvmso24DateLiv='01/01/2000';
      d2.bvmso24IndMot='test';
      d2.bvmso24NMot='test';
      d2.bvmso24TypeMot='tset';
      d2.bvmso24IndBoi='test';
      d2.bvmso24NBoi='test';
      d2.bvmso24TypeBoi='test';
      d2.bvmso24LibModel='test';
      d2.bvmso24MarqCom='test';
      d2.bvmso24NFab='test';
      d2.bvmso24Version='test';
      d2.bvmso24NRecCom='test';
      d2.bvmso24PaysImmat='FR';
      d2.bvmso24RefBoi='test';
      d2.bvmso24RefMot='test';
      d2.bvmso24LibUsimot='test';
      d2.bvmso24LibUsiboi='test';
      d2.bvmso24LibUsiveh='test';
      d2.bvmso24Ligne2P12='test';
      d2.bvmso24PlacSpec='test';
      d2.bvmso24Ligne3P12='test';
      d2.bvmso24Ligne4P12='test';
      d2.bvmso24Ptmaav='test';
      d2.bvmso24Ptr='test';
      d2.bvmso24Ptma='test';
      d2.bvmso24Ptmaar='test';
      
      ws2.detVeh = d2;
      
      VEH_Veh__c Vehicule = new VEH_Veh__c (Name = '21342312323123456',  VehicleBrand__c = 'Renault', KmCheck__c=100 , KmCheckDate__c = date.today());
      insert Vehicule;
      
      svfatc.vehId = Vehicule.Id;
      svfatc.vehName = Vehicule.Name;
      svfatc.vehicleRelationExists = '0';
      
      svfatc.VIN='';
      svfatc.VRN='';
      Apexpages.currentPage().getParameters().put('Id','');
      
      svfatc.searchVehicle();
      pageRef = svfatc.createVehicleBVM(ws2);
      pageRef= svfatc.SelectVehicle();  
      svfatc.Skip();  
      pageRef= svfatc.Cancel();    
      ws = svfatc.searchBVM();
      
      svfatc.searchVehicle();
      pageRef2 = svfatc.createVehicleBVM(ws2);
      pageRef2= svfatc.SelectVehicle();  
      svfatc.Skip();  
      pageRef2= svfatc.Cancel();    
      ws2 = svfatc.searchBVMComplet();
      Test.stopTest();       

     }
}