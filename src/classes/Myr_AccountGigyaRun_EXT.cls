/** Extension controller used to control the Gigya function for MyRenaulmt BackEnd RUN team
* @author sebastien.ducamp@atos.net.hqmyrdev
* @description  
*/
public class Myr_AccountGigyaRun_EXT { 

	public static final Integer MAX_COUNT_DELETION_POLLER = 3;

	/** input data **/
	public Id mAccountId {get; set;}
	public String mBrand {get; set;}
	public Account mAcc {get; private set;}
	public String mGigUID {get; set;}

	public String newEmail {get; set;}
	public User usr {get; set;}

	//Authorization for the current user
	//CLD (06.06.2016) : Add the new Authorization Level 3
	public Boolean AuthLevel_1 {get; private set;}
	public Boolean AuthLevel_2 {get; private set;}
	public Boolean AuthLevel_3 {get; private set;}
	public Boolean DisplayDeleteKO {get; private set;}

	/* SDP: I try to use visualforce component (call the vf component for each breand) but this does not work
		as the second component crushed the first one. There is only a single instance of the vf component controller
		where I expected to have 2 different instances (one for each brand).. Thats's simply bullshit ...
	/** Gigya Caller and results **/
	public Myr_GigyaFunctions_Cls GigyaRenault {get; private set;}
	public Myr_GigyaFunctions_Cls GigyaDacia {get; private set;}
	public Boolean FoundGigyaRenault {get; private set;}
	public Boolean FoundOnlyOneGigyaRenault {get; private set;}
	public Boolean FoundGigyaDacia {get; private set;}
	public Boolean FoundOnlyOneGigyaDacia {get; private set;}
	public String ErrorGigyaRenault {get; private set; }
	public String ErrorGigyaDacia {get; private set; }
	public Myr_GigyaParser_Cls.GigyaAccountParser GigyaResultRenault {get; private set;}
	public Myr_GigyaParser_Cls.GigyaAccountParser GigyaResultDacia {get; private set;}
	public Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response LastResponseRenault {get; private set;}
	public Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response LastResponseDacia {get; private set;}

	/** Deletion case **/ 
	public Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response LastMultipResponseRenault {get; private set;}
	public Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response LastMultipResponseDacia {get; private set;}
	public Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response Deactivation{get; private set;}
	public Boolean DisplayRenaultDeletionPanel {get; private set;}
	public Boolean DisplayDaciaDeletionPanel {get; private set;}
	public Boolean DeletionInProgress {get; private set;}
	public Integer DeletionCountPoller {get; private set;}
	public String  Message_Gigya_DeleteAccount_Confirmation  {get; private set;}

	public Boolean getDeactivationIsOk() {
		return ( Deactivation.info.code.endsWith('001') || Deactivation.info.code.endsWith('000') ) ? true : false;
	}

	public String getDeactivationMsg() {
		return Deactivation.info.message;
	}

	/** @constructor **/
	public Myr_AccountGigyaRun_EXT(ApexPages.StandardController stdController) {
		DeletionInProgress = false;
		mBrand = '';
		mGigUID = '';
		Message_Gigya_DeleteAccount_Confirmation = '';
		mAccountId = stdController.getId();
		refreshData();
		checkDelete();
		DisplayRenaultDeletionPanel= false;
		DisplayDaciaDeletionPanel= false;
		Myr_GigyaFunctions_Cls checkGigya = new Myr_GigyaFunctions_Cls();
		AuthLevel_1 = checkGigya.getAuthLevel1();
		AuthLevel_2 = checkGigya.getAuthLevel2();
		AuthLevel_3 = checkGigya.getAuthLevel3();
	}

	/** @description  Refresh the data used in the visualforce page. Do not use the standard controller
		to be able to reload the data after a deletion for instance
	**/
	public void refreshData() {
		system.debug('### Myr_AccountGigyaRun_EXT - <refreshData> BEGIN' );
		mAcc = [SELECT Id, Country__c, MyRenaultID__c, MYR_Status__c, MyDaciaID__c, MyD_Status__c
						, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate
						, AccountSource, AccSubSource__c, Account_Sub_Sub_Source__c
						, IsPersonAccount
				FROM Account
				WHERE Id = :mAccountId];
		try {
			usr = [SELECT Id, Username, LastLoginDate, isActive FROM User WHERE AccountId = :mAccountId];
		} catch (Exception e) {
			//nothing to do
		}

		if( DeletionCountPoller != null ) {
			DeletionCountPoller++;
			if( DeletionCountPoller >= MAX_COUNT_DELETION_POLLER ) {
				DeletionInProgress = false;
			}
		}
		
		system.debug('### Myr_AccountGigyaRun_EXT - <refreshData> Account='+mAcc );
		system.debug('### Myr_AccountGigyaRun_EXT - <refreshData> User='+usr );
	}

	/* Check that Delete function will work properly, that is a community user, User.recorddefaultcountry = Account.Country
	   Or User.PAD_BypassTrigger__c = Account.UpdateCountryWithUser **/
	private void checkDelete() {
		//Control that the user could be able to use the delete function
		//if the country is not the same than the country of the account of if the user has not the permission
		//to bypass the trigger then forbid the function delete
		User currentUser = [SELECT Id, RecordDefaultCountry__c, PAD_BypassTrigger__c FROM User WHERE Id=:UserInfo.getUserId()];
		Myr_GigyaFunctions_Cls test = new Myr_GigyaFunctions_Cls();
		String bypassTrg = (String.isBlank(currentUser.PAD_BypassTrigger__c)?'':currentUser.PAD_BypassTrigger__c);
		Boolean canByPassCountry = bypassTrg.containsIgnoreCase('Account.UpdateCountryWithUser');
		DisplayDeleteKO = 
			test.getAuthLevel2() 
			&& !currentUser.RecordDefaultCountry__c.equalsIgnoreCase(mAcc.Country__c) 
			&& !canByPassCountry;
	}

	public void loadGigya() {
		loadGigyaRenault();
		loadGigyaDacia();
	}

	/** Load Renault Dacia: function splitted so that actionStatus will work properly ... **/
	public void loadGigyaRenault() {
		system.debug('### Myr_AccountGigyaRun_EXT - <loadGigyaRenault> - BEGIN' );
		//init Renault
		GigyaRenault = new Myr_GigyaFunctions_Cls(Myr_GigyaFunctions_Cls.GigyaLogType.ASYNCHRONOUS);
		Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response response = GigyaRenault.viewData( mAccountId , Myr_MyRenaultTools.Brand.Renault.name() );
		system.debug('### Myr_AccountGigyaRun_EXT - <loadGigyaRenault> response=' + response + ', isOK=' + response.isOK() );
		FoundOnlyOneGigyaRenault =false;
		if( response.isOK() ) {
			FoundGigyaRenault = true;
			GigyaResultRenault = GigyaRenault.getGigyaData();
			FoundOnlyOneGigyaRenault = GigyaResultRenault.results.size()==1;		
		} else {
			FoundGigyaRenault = false;
			ErrorGigyaRenault = 'Account not found on Gigya: ' + response.msg;
		}
		
	}

	/** Load Gigya Dacia: function splitted so that actionStatus will work properly ... **/
	public void loadGigyaDacia() {
		system.debug('### Myr_AccountGigyaRun_EXT - <loadGigyaDacia> - BEGIN' );
		//init DAcia
		GigyaDacia = new Myr_GigyaFunctions_Cls(Myr_GigyaFunctions_Cls.GigyaLogType.ASYNCHRONOUS);
		Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response response = GigyaDacia.viewData( mAccountId , Myr_MyRenaultTools.Brand.Dacia.name() );
		system.debug('### Myr_AccountGigyaRun_EXT - <loadGigyaDacia> response=' + response + ', isOK=' + response.isOK() );
		FoundOnlyOneGigyaDacia =false;
		if( response.isOK() ) {
			FoundGigyaDacia = true;
			GigyaResultDacia = GigyaDacia.getGigyaData();
			FoundOnlyOneGigyaDacia = GigyaResultDacia.results.size()==1;
		} else {
			FoundGigyaDacia = false;
			ErrorGigyaDacia = 'Account not found on Gigya: ' + response.msg;
		}
	}

	/** @description  Reset the password for thr given gigya account on the given brand **/
	public void resetPasswordRenault() {
		mBrand = 'Renault';
		resetPassword();
	}

	/** @description  Reset the password for thr given gigya account on the given brand **/
	public void resetPasswordDacia() {
		mBrand = 'Dacia';
		resetPassword();
	}

	/** @description  Reset the password for thr given gigya account on the given brand **/
	public void resetPassword() {
		system.debug('### Myr_AccountGigyaRun_EXT - <Reset Password> - mBrand='+mBrand+', mAccountId='+mAccountId);
		system.debug('### Myr_AccountGigyaRun_EXT - <Reset Password> - GigyaRenault='+GigyaRenault);
		system.debug('### Myr_AccountGigyaRun_EXT - <Reset Password> - GigyaDacia='+GigyaDacia);		
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			LastResponseRenault =  GigyaRenault.resetPassword(mAccountId, mBrand);
			system.debug('### Myr_AccountGigyaRun_EXT - <Reset Password> Renault=' + LastResponseRenault);
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			LastResponseDacia =  GigyaDacia.resetPassword(mAccountId, mBrand);
			system.debug('### Myr_AccountGigyaRun_EXT - <Reset Password> Dacia=' + LastResponseDacia);
		}
	}

	/** @description  Resend the activation email for the given gigya account on the given brand **/
	public void resendActivationEmail() {
		system.debug('### Myr_AccountGigyaRun_EXT - <Resend Activation Email> mBrand=' + mBrand);
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			LastResponseRenault =  GigyaRenault.resendActivationEmail(mAccountId, mBrand);
			system.debug('### Myr_AccountGigyaRun_EXT - <Resend Activation Email> Renault=' + LastResponseRenault);
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			LastResponseDacia =  GigyaDacia.resendActivationEmail(mAccountId, mBrand);
			system.debug('### Myr_AccountGigyaRun_EXT - <Resend Activation Email> Dacia=' + LastResponseDacia);
		}
	}

	/** @description  Change the Gigya Email for the given gigya account on the given brand **/
	public void changeEmail() {
		//LastResponse = GigyaWrapper.changeEmail(mAccountId, mBrand, newEmail);
		system.debug('### Myr_AccountGigyaRun_EXT - <Change Email> mBrand=' + mBrand + ', Email=' + newEmail);
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			LastResponseRenault =  GigyaRenault.changeEmail(mAccountId, mBrand, newEmail);
			system.debug('### Myr_AccountGigyaRun_EXT - <Change Email> Renault=' + LastResponseRenault);
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			LastResponseDacia =  GigyaDacia.changeEmail(mAccountId, mBrand, newEmail);
			system.debug('### Myr_AccountGigyaRun_EXT - <Change Email> Dacia=' + LastResponseDacia);
		}
		refreshData();
	}

	/** @description  Display the delete panel confirmation **/
	public void displayDeletePanel() {
		system.debug('### Myr_AccountGigyaRun_EXT - <Change Email> displayDeletePanel for Brand =' + mBrand + ', Email=' + newEmail);
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			DisplayRenaultDeletionPanel= true;
			system.debug('### CGS > DisplayRenaultDeletionPanel=' + DisplayRenaultDeletionPanel + ', Email=' + newEmail);		
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			DisplayDaciaDeletionPanel= true;
			system.debug('### CGS > DisplayDaciaDeletionPanel=' + DisplayDaciaDeletionPanel + ', Email=' + newEmail);	
		}
		// Set the confirmation message
		if ((mGigUID!=null) && (mGigUID!='')){
			Message_Gigya_DeleteAccount_Confirmation = system.Label.Myr_Gigya_DeleteOneAccount_Confirmation.replace('{0}', mGigUID);

		} else {
			Message_Gigya_DeleteAccount_Confirmation = system.Label.Myr_Gigya_DeleteAccounts_Confirmation;
		}
	}	


	/** @description  Display the delete panel confirmation **/
	public void hideDeletePanel() {
		mGigUID = '';
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			DisplayRenaultDeletionPanel= false;
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			DisplayDaciaDeletionPanel= false;
		}
	}

	/* Delete the Gigya Accounts  associated to the current account and for the given brand **/
	public void deleteGigyaAccounts() {		
		system.debug('### Myr_AccountGigyaRun_EXT - <Delete> mBrand=' + mBrand + ', accountSfdcId=' + mAccountId);
		// Check if it's an unitary or a global deletion
		if ((mGigUID!=null) && (mGigUID!='')){
			deleteOneGigyaAccount();
		} else {
			deleteAllGigyaAccounts();
		}
	}

	/* Delete the One Gigya Account associated to the current account and for the given brand and the UID filled in parameters**/
	private void deleteOneGigyaAccount() {		
		
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			DisplayRenaultDeletionPanel = false;
			system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> GigyaResultRenault=' + GigyaResultRenault);
			if( GigyaResultRenault != null && GigyaResultRenault.results.size() > 0 ) {
				LastResponseRenault =  GigyaRenault.deleteOneAccount(mAccountId, mBrand, mGigUID);				
				system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Response=' + LastResponseRenault);
			}
			system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Call loadGigyaData');
			refreshData();
			system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Deactivation Result = ');
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			DisplayDaciaDeletionPanel= false;
			if( GigyaResultDacia != null && GigyaResultDacia.results.size() > 0 ) {
				LastResponseDacia =  GigyaDacia.deleteOneAccount(mAccountId, mBrand, mGigUID);				
				system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Response=' + LastResponseDacia);
			}			
			system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Deletion OK, call loadGigyaData');
			refreshData();
			system.debug('### Myr_AccountGigyaRun_EXT - deleteOneGigyaAccount - <Delete> Deactivation Result = ');
		}
		mGigUID = '';
	}
	/* Delete the Gigya Accounts associated to the current account and for the given brand **/
	private void deleteAllGigyaAccounts() {		
		DeletionInProgress = true;
		DeletionCountPoller = 0;
		//First check that we do not have too many accounts to delete
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(mBrand) ) {
			DisplayRenaultDeletionPanel = false;
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> GigyaResultRenault=' + GigyaResultRenault);
			if( GigyaResultRenault != null && GigyaResultRenault.results.size() > 0 ) {
				LastMultipResponseRenault =  GigyaRenault.deleteAccounts(mAccountId, mBrand);
				LastResponseRenault = new Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response(
					LastMultipResponseRenault.gigFunction,
					LastMultipResponseRenault.code, LastMultipResponseRenault.label, LastMultipResponseRenault.msg
				);
				system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Response=' + LastMultipResponseRenault);
			}
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Call UserDeactivation');
			deactivateUser( mBrand );
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Call loadGigyaData');
			refreshData();
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Deactivation Result = ');
		} else if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(mBrand) ) {
			DisplayDaciaDeletionPanel= false;
			if( GigyaResultDacia != null && GigyaResultDacia.results.size() > 0 ) {
				LastMultipResponseDacia =  GigyaDacia.deleteAccounts(mAccountId, mBrand);
				LastResponseDacia = new Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response(
					LastMultipResponseDacia.gigFunction,
					LastMultipResponseDacia.code, LastMultipResponseDacia.label, LastMultipResponseDacia.msg
				);
				system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Response=' + LastMultipResponseDacia);
			}
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Deletion OK, call UserDeactivation');
			deactivateUser( mBrand );
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Deletion OK, call loadGigyaData');
			refreshData();
			system.debug('### Myr_AccountGigyaRun_EXT - <Delete> Deactivation Result = ');
		}
	}


	/** Deactivate the Helios Community User **/
	private void deactivateUser(String brand) {
		Deactivation= new Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response();
		system.debug('### Myr_AccountGigyaRun_EXT - <Deactivate User> - BEGIN');
		Deactivation = Myr_UserDeActivation_WS.deActivateUser( mAccountId, brand );
		system.debug('### Myr_AccountGigyaRun_EXT - <Deactivate User> - END - ' + Deactivation);
	}

}