/*  Test class for Myr_ManageSetting_WS
*************************************************************************************************************
18 March 2016, R16.01 9.1 ChocolateCamelBaby, D. Veron (AtoS) : Creation
************************************************************************************************************/
@isTest 
private class Myr_ManageSetting_WS_Test{   
  @testsetup static void setCustomSettings() {
    Myr_Datasets_Test.prepareRequiredCustomSettings();
  }

  //get logs
  static Boolean isLogged(String Code_Log, String Label_Log) 
  {  
    List<Logger__c> listLog = [SELECT Message__c, Exception_Type__c FROM Logger__c];
	if (listLog.size()!=1){
		return false;	
	}
	for(Logger__c myLog : listLog){
		if (myLog.Exception_Type__c.equalsIgnoreCase(Code_Log)){
			String M_Message = myLog.Message__c;
			if (M_Message==null || !M_Message.equalsIgnoreCase(Label_Log)){
				return false;	
			}
		}
	}
	return true;
  }
  
  //LanguageLocaleKey is mandatory (WS07MS501)
  static testMethod void test010_Patch_Language() 
  {  
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
	User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', '');
    RestContext.request = req; 

	Test.startTest(); 
	system.runAs(communityUser) { 
        resp=Myr_ManageSetting_WS.Patch_Language(); 
    }
	Test.stopTest(); 
    system.assertNotEquals(null, resp);
    system.assertEquals('WS07MS501',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('Value missing : Parameter LanguageLocaleKey'));
	system.assertEquals(true,isLogged('WS07MS501','Value missing : Parameter LanguageLocaleKey'));
  }
  
  //LanguageLocaleKey max lenght = 5 (WS07MS502)
  static testMethod void test020_Patch_Language() 
  {
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
	User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', '123456');
    RestContext.request = req;

	Test.startTest();
	system.runAs(communityUser) {
        resp=Myr_ManageSetting_WS.Patch_Language();
    }
	Test.stopTest(); 
    system.assertNotEquals(null,resp);
    system.assertEquals('WS07MS502',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('Value(s) too long : Parameter LanguageLocaleKey'));
	system.assertEquals(true,isLogged('WS07MS502','Value(s) too long : Parameter LanguageLocaleKey'));
  }
  
  //The user must be a community one (WS07MS504)
  static testMethod void test030_Patch_Language() 
  {
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
	User NoCommunityUser = Myr_Datasets_Test.getUser('firstname', 'lastname', 'France', 'fr', Myr_Users_Utilities_Class.getHeliosReadOnlyProfile().Id);
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', '12345');
    RestContext.request = req;

	Test.startTest(); 
	system.runAs(NoCommunityUser) {
        resp=Myr_ManageSetting_WS.Patch_Language();
    }
	Test.stopTest(); 
    system.assertNotEquals(null,resp);
    system.assertEquals('WS07MS504',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('The user found is not a HeliosCommunity one. Impossible to change the language of the user'));
	system.assertEquals(true,isLogged('WS07MS504','The user found is not a HeliosCommunity one. Impossible to change the language of the user'));
  }

  //The user must be active (WS07MS505)
  static testMethod void test040_Patch_Language() 
  {
    List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
	User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', 'FAKEL');
    RestContext.request = req;

	Test.startTest();
	system.runAs(communityUser) {
        resp=Myr_ManageSetting_WS.Patch_Language();
    }
	Test.stopTest(); 
    system.assertNotEquals(null,resp);
    system.assertEquals('WS07MS505',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('An issue occurred during the change of the language :'));
	system.assertEquals(true,isLogged('WS07MS505','An issue occurred during the change of the language :'));
  }
  
  
  //Same Language (WS07MS001)
  static testMethod void test050_Patch_Language() 
  {
    Account listAcc = Myr_Datasets_Test.insertPersonalAccountAndUser('firstname', 'lastname', '35000', 'test.test@test.com', 'rhelios_test.test@test.com', '', 'USER_ACTIF', '', 'France', '', '', '', '');
	User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc.Id];
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', 'fr');
    RestContext.request = req;

	Test.startTest();
	system.runAs(communityUser) {
        resp=Myr_ManageSetting_WS.Patch_Language();
    }
	Test.stopTest(); 
    system.assertNotEquals(null,resp);
    system.assertEquals('WS07MS001',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('The language was already the one provided'));
	system.assertEquals(true,isLogged('WS07MS001','The language was already the one provided'));
  }

  //Nominal (WS07MS000)
  static testMethod void test060_Patch_Language() 
  {
    Account listAcc = Myr_Datasets_Test.insertPersonalAccountAndUser('firstname', 'lastname', '35000', 'test.test@test.com', 'rhelios_test.test@test.com', '', 'USER_ACTIF', '', 'France', '', '', '', '');
	User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc.Id];
	Myr_ManageSetting_WS.Myr_ManageSetting_WS_Response resp = null;
	RestRequest req = new RestRequest();
	req.requestURI = '<a target="_blank" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/" rel="nofollow">' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/Myr_ManageSetting_WS/Patch_Language/</a>';
    req.httpMethod = 'PATCH';
	req.addParameter('LanguageLocaleKey', 'en');
    RestContext.request = req;

	Test.startTest();
	system.runAs(communityUser) {
        resp=Myr_ManageSetting_WS.Patch_Language();
    }
	Test.stopTest(); 
    system.assertNotEquals(null,resp);
    system.assertEquals('WS07MS000',resp.Code);
    system.assertEquals(true,resp.MessageLabel.contains('The change of the language of the user performed OK'));
	system.assertEquals(true,isLogged('WS07MS000','The change of the language of the user performed OK'));
  }
}