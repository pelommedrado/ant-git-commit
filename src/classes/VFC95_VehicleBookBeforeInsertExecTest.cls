/**
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC95_VehicleBookBeforeInsertExecTest {

    static testMethod void myUnitTestBeforeInsert() {
        
        Boolean isInsertedVehiclleBooking = false;
        
        Opportunity opp = new Opportunity();
        Quote q = new Quote();        
        PricebookEntry pBookEntry;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;

        // criar nova cotação
        q.Name = 'QuoteNameTest_1';
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb2.Id;
        insert q;
        
        // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = '12345678902234567';
        v.Status__c = 'Available';
        insert v;
        
		// selecionar pricebookentry (seeAllData)
        pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];

        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = q.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;
           
        VehicleBooking__c vBooking = new VehicleBooking__c();
        vBooking.Quote__c = q.Id;
        vBooking.Vehicle__c = v.Id;
        vBooking.Status__c = 'Active';
    
        Test.startTest();
        
        try
        {
            insert vBooking;
            isInsertedVehiclleBooking = true;
        }
        catch(DMLException ex) {
        }
        
        system.assertEquals(isInsertedVehiclleBooking, true);
        
        //=============================================
        // insert next vehicle booking
        //=============================================


        Opportunity opp2 = new Opportunity();
        Quote q2 = new Quote();        
        PricebookEntry pBookEntry2;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb22 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp2.Name = 'OppNameTest_1';
        opp2.StageName = 'Identified';
        opp2.CloseDate = Date.today();
        opp2.Pricebook2Id = pb22.Id;
        opp2.CurrencyIsoCode = 'BRL';
        insert opp2;

        // criar nova cotação
        q2.Name = 'QuoteNameTest_1';
        q2.OpportunityId = opp.Id;
        q2.Pricebook2Id = pb2.Id;
        insert q2;

        // criar Vehicle
        VEH_Veh__c v2 = new VEH_Veh__c();
        v2.Name = '12345678901234568';
        v2.Status__c = 'Available';
        insert v2;
        
		// selecionar pricebookentry (seeAllData)
        pBookEntry2 = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];

        // criar item de cotação
        QuoteLineItem qLineItem2 = new QuoteLineItem();
        qLineItem2.QuoteId = q2.Id;
        qLineItem2.Vehicle__c = v2.Id;
        qLineItem2.PricebookEntryId = pBookEntry2.Id;
        qLineItem2.Quantity = 1;
        qLineItem2.UnitPrice = pBookEntry2.UnitPrice;                       
        insert qLineItem2;
           
        VehicleBooking__c vBooking2 = new VehicleBooking__c();
        vBooking2.Quote__c = q2.Id;
        vBooking2.Vehicle__c = v.Id;
        vBooking2.Status__c = 'Active';
       
        try
		{
            insert vBooking2;          
        }
        catch(DmlException e) {
            isInsertedVehiclleBooking = false;
        }
       
        system.assertEquals(isInsertedVehiclleBooking, false);
       
        Test.stopTest();
    }
}