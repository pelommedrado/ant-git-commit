/**
 * 
 */
global class EmailService implements Messaging.InboundEmailHandler {    				
    /*   --- Nome alterado por @Edvaldo --- */
    
    /** Regex do campo **/
    
    public static final String RX_NOME 			= 'Nome:';
    public static final String RX_SOBRENOME 	= 'Sobrenome:';
    public static final String RX_EMAIL 		= 'E-mail:';
    public static final String RX_TELEFONE		= 'Telefone:';
    public static final String RX_CELULAR		= 'Celular:';
    public static final String RX_CIDADE		= 'Cidade:';
    public static final String RX_ESTADO		= 'Estado:';
    public static final String RX_CONCESSIO		= 'Concessionária de Interesse:';
    public static final String RX_MODELO		= 'Modelo de Interesse:';
    public static final String RX_CPF			= 'CPF:';
    public static final String RX_RECEBE_EMAIL 	= 'Desejo receber informações por SMS:';
    public static final String RX_RECEBE_SMS	= 'Desejo receber informações por E-mail:';
    public static final String RX_DETALHE      	= 'Detalhe:';
    public static final String RX_SUB_DETALHE  	= 'Sub Detalhe:';
    
    public static final String RX_ID 			= 'Id:';
    public static final String RX_ORIGEM 		= 'Origem:';
    public static final String RX_SUBORIGEM 	= 'Sub Origem:';
    
    /** Lista de regex dos campos */
    private static final List<String> listaRegex = new List<String>();
    
    static {
        listaRegex.add(RX_NOME);
        listaRegex.add(RX_SOBRENOME);
        listaRegex.add(RX_EMAIL);
        listaRegex.add(RX_TELEFONE);
        listaRegex.add(RX_CELULAR);
        listaRegex.add(RX_CIDADE);
        listaRegex.add(RX_ESTADO);
        listaRegex.add(RX_CONCESSIO);
        listaRegex.add(RX_MODELO);
        listaRegex.add(RX_CPF);
        listaRegex.add(RX_RECEBE_EMAIL);
        listaRegex.add(RX_RECEBE_SMS);
        listaRegex.add(RX_DETALHE);
        listaRegex.add(RX_SUB_DETALHE);
        listaRegex.add(RX_ID);
        listaRegex.add(RX_ORIGEM);
        listaRegex.add(RX_SUBORIGEM);
    }
    
    /**
     * Metodo principal da classe
     */
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        String body = email.plainTextBody;
        
        Map<String, String> mapaValor = extrairInfo(body);
        
        try {
            if(isValidCampos(mapaValor)) {
                Lead lead = salvarLead(mapaValor, body);
                VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity( lead, null );     
                //  -- inclui classe de conversão de Lead @Edvaldo -- 
                
            } else {
                salvarIncomLead(mapaValor, body);
            }  
            
        } catch (Exception e) {
            System.debug('Ex:' + e.getMessage());
            System.debug('Ex:' + e.getCause());
        }
        
        result.success = true;
        
        return result;
    }
    
    /**
	 * 
	 */
    public Lead salvarLead(Map<String, String> mapaValor, String body) {
        Lead lead = criarLead(mapaValor);
        
        System.debug('Criando Lead completo:' + lead);
        
        if(obterContaConcessionaria(lead.BIR_Dealer_of_Interest__c) != null) {
            System.debug('Adicionando o lead ao usuario...');
            
            User user = obterUsuarioRenauld();
            lead.OwnerId = user.Id;
            
        } else {
            System.debug('Adicionando o lead a fila...');
            
            lead.OwnerId = obterFila();
        
        }
        
        lead.W2E_Body__c = body;
        
        //Pattern p = Pattern.compile( '"(\n|\r|.)*"' );
        Pattern p = Pattern.compile('"(.*|\n)+"');
        Matcher m = p.matcher( body );
        if(m.find()) {
            String descricao = m.group();
            System.debug('Descricao: ' + descricao);
            
            lead.Description = descricao.trim();
        }
        
        System.debug('Lead: ' + lead);
        
        
        Database.upsert( lead ); 
        
        return lead;
    }
    
    /**
     * Deve obter a fila
     */
    private Id obterFila() {
        return [
            SELECT Id 
            FROM Group 
            WHERE Type = 'queue' AND DeveloperName = 'Leads_Concessionaria'
        ].Id;
    }
    
    /**
     * 
     */
    public void salvarIncomLead(Map<String, String> mapaValor, String body) {
        Incomplete_Lead__c inLead = criarLeadIncompleto(mapaValor);
        
        System.debug('Criando Lead incompleto:' + inLead);
        
        User user = obterUsuarioRenauld();
        inLead.OwnerId = user.Id;
        
        inLead.W2E_Body__c = body;
        
        Database.insert( inLead );
         
    }
    
    /**
     * Verificar se a concessionaria existe
     */
    private Account obterContaConcessionaria(String bir) {
        if(bir == null) {
            return null;
        }
        
        List<Account> accList = [
            SELECT Id 
            FROM Account
            WHERE IDBIR__c =: bir
        ];
        
        if(accList.isEmpty()) {
            return null;
        }
        return accList.get(0);
    }
    
    /**
     * Deve obter o usuario padrao
     */
    private User obterUsuarioRenauld() {
        User user = [
            SELECT Id 
            FROM User 
            WHERE Name = 'Renault do Brasil Rede'
        ];
        
        /* --- alterado de busca de Id por Name @Edvaldo --- */
        return user;
    }
    
    /**
     * Deve validar os campos obrigatorios
     */
    public Boolean isValidCampos(Map<String, String> mapa) {
        
        if(!mapa.containsKey(RX_NOME)) {
           System.debug('Nome nao indentificado.');
           return false; 
        }
        
        if(!mapa.containsKey(RX_SOBRENOME)) {
           System.debug('Sobrenome nao indentificado.');
           return false;  
        }
        
        //if(!mapa.containsKey(RX_CONCESSIO)) {
        //   System.debug('Concessionaria nao indentificada.');
        //   return false;  
        //}
        
        if(!mapa.containsKey(RX_EMAIL) && 
           !mapa.containsKey(RX_TELEFONE) && 
           !mapa.containsKey(RX_CELULAR)) {
               System.debug('Nenhum contato indentificado.');
               return false;  
        }
        
        return true;
    }
    
    /**
     * Deve criar um lead a partir das informacoes recebidas
     */
    public Lead criarLead(Map<String, String> mapaValor) {
        Lead lead = new Lead();
        
        lead.Id = mapaValor.get(RX_ID);
        
        if(mapaValor.get(RX_ID) != null) {
            lead.Offer__c = [
                SELECT Offer__c 
                FROM Lead
                WHERE Id =: mapaValor.get(RX_ID) 
            ].Offer__c;  /* -- add Offer Id @Marcelino -- */
        }
        
        lead.FirstName = mapaValor.get(RX_NOME);
        lead.LastName = mapaValor.get(RX_SOBRENOME);
        
        if(mapaValor.get(RX_CONCESSIO) != null) {
            lead.BIR_Dealer_of_Interest__c = mapaValor.get(RX_CONCESSIO);
            lead.DealerOfInterest__c  = [
                SELECT Id 
                FROM Account 
                WHERE IDBIR__c =: lead.BIR_Dealer_of_Interest__c 
            ].Id;  /* -- add Id da Concessionaria @Edvaldo -- */
        }
        
        if(mapaValor.get(RX_EMAIL) != null) {
            lead.Email = mapaValor.get(RX_EMAIL);
        }
        
        if(mapaValor.get(RX_TELEFONE) != null) {
            lead.Home_Phone_Web__c = limparTelefone(mapaValor.get(RX_TELEFONE));
            lead.HomePhone__c = lead.Home_Phone_Web__c;									
            /* -- add campo de Lead para mapeamento  @Edvaldo -- */
        }
        
        if(mapaValor.get(RX_CELULAR) != null) {
            lead.Mobile_Phone__c = limparTelefone(mapaValor.get(RX_CELULAR));
            lead.MobilePhone = lead.Mobile_Phone__c;									
            /* -- add campo de Lead para mapeamento  @Edvaldo -- */
        }
        
        if(mapaValor.get(RX_CIDADE) != null) {
            lead.City = mapaValor.get(RX_CIDADE);
        }
        
        if(mapaValor.get(RX_ESTADO) != null) {
            lead.State = mapaValor.get(RX_ESTADO);
        }
        
        if(mapaValor.get(RX_MODELO) != null) {
            lead.VehicleOfInterest__c = mapaValor.get(RX_MODELO);
        }
        
        if(mapaValor.get(RX_CPF) != null) {
            lead.CPF_CNPJ__c = limparCpf(mapaValor.get(RX_CPF));
        }
        
        if(mapaValor.get(RX_DETALHE) != null) {
            lead.Detail__c = mapaValor.get(RX_DETALHE);
        }
        
        if(mapaValor.get(RX_SUB_DETALHE) != null) {
            lead.Sub_Detail__c = mapaValor.get(RX_SUB_DETALHE);
        }
        
        if(mapaValor.get(RX_RECEBE_EMAIL) != null) {
            String valor = mapaValor.get(RX_RECEBE_EMAIL).trim();
            lead.OptinEmail__c = (valor.equalsIgnoreCase('Y') ? 'Y' : 'N');
        }
        
        if(mapaValor.get(RX_RECEBE_SMS) != null) {
            String valor = mapaValor.get(RX_RECEBE_SMS).trim();
            lead.OptinSMS__c = (valor.equalsIgnoreCase('Y') ? 'Y' : 'N');
        }
        
        if(mapaValor.get(RX_ORIGEM) != null) {
            lead.LeadSource = mapaValor.get(RX_ORIGEM).trim();
        }
        
        if(mapaValor.get(RX_SUBORIGEM) != null) {
            lead.SubSource__c = mapaValor.get(RX_SUBORIGEM).trim();
        }
        
        if(lead.LeadSource == null) {
            lead.LeadSource 	= 'DEALER';    
        }
        
        if(lead.SubSource__c == null) {
            lead.SubSource__c 	= 'INTERNET';     
        }
       
        lead.RecordTypeId 	=  Utils.getRecordTypeId('Lead', 'DVR');     
        /* --- alterado de Id para classe que pega Id por mapa @Edvaldo --- */
        lead.Country__c		= 'Brazil';
        lead.CurrencyIsoCode= 'BRL';
        
        return lead;
    }
    
    /**
     * 
     */
    public Incomplete_Lead__c criarLeadIncompleto(Map<String, String> mapaValor) {
        Incomplete_Lead__c inLead = new Incomplete_Lead__c();
        
        if(mapaValor.get(RX_NOME) != null) {
            inLead.First_Name__c = mapaValor.get(RX_NOME);
        }
        
        if(mapaValor.get(RX_SOBRENOME) != null) {
            inLead.Last_Name__c = mapaValor.get(RX_SOBRENOME);
        }
        
        if(mapaValor.get(RX_CONCESSIO) != null) {
            String valor = mapaValor.get(RX_CONCESSIO);
            Account acc = obterContaConcessionaria(valor);
            if(acc != null) {
                inLead.Dealer_of_Interest__c = acc.Id;    
            }   
        }
        
        if(mapaValor.get(RX_EMAIL) != null) {
            inLead.Email__c = mapaValor.get(RX_EMAIL);
        }
        
        if(mapaValor.get(RX_TELEFONE) != null) {
            inLead.Phone__c = limparTelefone(mapaValor.get(RX_TELEFONE));
        }
        
        if(mapaValor.get(RX_CELULAR) != null) {
            inLead.Mobile_Phone__c = limparTelefone(mapaValor.get(RX_CELULAR));
        }
        
        if(mapaValor.get(RX_CIDADE) != null) {
            inLead.City__c = mapaValor.get(RX_CIDADE);
        }
        
        if(mapaValor.get(RX_ESTADO) != null) {
            inLead.State__c = mapaValor.get(RX_ESTADO);
        }
        
        if(mapaValor.get(RX_MODELO) != null) {
            inLead.Vehicle_of_interest__c = mapaValor.get(RX_MODELO);
        }
        
        if(mapaValor.get(RX_CPF) != null) {
            inLead.CPF__c = limparCpf(mapaValor.get(RX_CPF));
        }
        
        if(mapaValor.get(RX_DETALHE) != null) {
            inLead.Detail__c = mapaValor.get(RX_DETALHE);
        }
        
        if(mapaValor.get(RX_SUB_DETALHE) != null) {
            inLead.Sub_Detail__c = mapaValor.get(RX_SUB_DETALHE);
        }
        
        inLead.Type__c = 'Email-to-Lead';
        
        if(mapaValor.get(RX_RECEBE_EMAIL) != null) {
            String valor = mapaValor.get(RX_RECEBE_EMAIL).trim();
            inLead.Receive_News_by_Email__c = (valor.equalsIgnoreCase('Y') ? 'Y' : 'N');
        }
        
        if(mapaValor.get(RX_RECEBE_SMS) != null) {
            String valor = mapaValor.get(RX_RECEBE_SMS).trim();
            inLead.Receive_News_by_SMS__c = (valor.equalsIgnoreCase('Y') ? 'Y' : 'N');
        }
        
        return inLead;
    }
    
    /**
     * Deve extrair todas as informacoes necessarias para
     * a criacao do LEad
     */
    public Map<String, String> extrairInfo(final String body) {
        Map<String, String> mapa = new Map<String, String>();
        
        //deve transformar o conteudo do email 
        //em uma lista a partir das linhas
        List<String> linhas = body.split('\n');
        
        for(String lin: linhas) {
            System.debug('Verificando linha:' + lin);
            
            for(String key: listaRegex) {
                
                if(lin.startsWithIgnoreCase(key)) {
                    //obter conteudo da linha do email
                    String conteudo = tratarLinha(lin, key);
                    
                    System.debug('Key:' + key + ' Valor:' + conteudo);
                    
                    //existe conteudo?
                    if(String.isNotBlank(conteudo)) {
                        mapa.put(key, conteudo);
                    }
                }
                
            }
        }
        
        return mapa;
    }
    
    /**
     * Deve remover a formatacao do CPF
     */
    private String limparCpf(String cpf) {
        return cpf.replaceAll('[- \\.]', '');
    }
    
    /**
     * Deve remover a formatacao do telefone
     */
    private String limparTelefone(String tel) {
        return tel.replaceAll('[- )(]', '');
    }
    
    /**
     * Deve obter o conteudo do label
     */
    private String tratarLinha(String linha, String regex) {
        return linha.replace(regex, '').replace('*', '').trim();
    }   
}