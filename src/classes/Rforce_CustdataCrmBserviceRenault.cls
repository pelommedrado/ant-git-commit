//Generated by wsdl2apex

public class Rforce_CustdataCrmBserviceRenault {
    public class Survey {
        public String surveyOK;
        public String value;
        public String loyMessage;
        private String[] surveyOK_type_info = new String[]{'surveyOK','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] value_type_info = new String[]{'value','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] loyMessage_type_info = new String[]{'loyMessage','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'surveyOK','value','loyMessage'};
    }
    public class getCustData_element {
        public Rforce_CustdataCrmBserviceRenault.GetCustDataRequest request;
        private String[] request_type_info = new String[]{'request','http://custdata.crm.bservice.renault','GetCustDataRequest','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'request'};
    }
    public class Contact {
        public String phoneCode1;
        public String phoneNum1;
        public String phoneCode2;
        public String phoneNum2;
        public String phoneCode3;
        public String phoneNum3;
        public String email;
        public String preferredCom;
        public String optin;
        private String[] phoneCode1_type_info = new String[]{'phoneCode1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] phoneNum1_type_info = new String[]{'phoneNum1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] phoneCode2_type_info = new String[]{'phoneCode2','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] phoneNum2_type_info = new String[]{'phoneNum2','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] phoneCode3_type_info = new String[]{'phoneCode3','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] phoneNum3_type_info = new String[]{'phoneNum3','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] email_type_info = new String[]{'email','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] preferredCom_type_info = new String[]{'preferredCom','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] optin_type_info = new String[]{'optin','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'phoneCode1','phoneNum1','phoneCode2','phoneNum2','phoneCode3','phoneNum3','email','preferredCom','optin'};
    }
    public class LogsCustData {
        public Integer mode;
        public String country;
        public String brand;
        public String demander;
        public String vin;
        public String lastName;
        public String firstName;
        public String city;
        public String zip;
        public String ident1;
        public String idClient;
        public String idMyr;
        public String firstRegistrationDate;
        public String registration;
        public String email;
        public String logDate;
        public String ownedVehicles;
        public String nbReplies;
        public String wsVersion;
        public String ipAdress;
        public String codErreur;
        public String dbCible;
        public String wsHostname;
        public Long wsTexec;
        private String[] mode_type_info = new String[]{'mode','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] brand_type_info = new String[]{'brand','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] demander_type_info = new String[]{'demander','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] vin_type_info = new String[]{'vin','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] zip_type_info = new String[]{'zip','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ident1_type_info = new String[]{'ident1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idClient_type_info = new String[]{'idClient','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idMyr_type_info = new String[]{'idMyr','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstRegistrationDate_type_info = new String[]{'firstRegistrationDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] registration_type_info = new String[]{'registration','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] email_type_info = new String[]{'email','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] logDate_type_info = new String[]{'logDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ownedVehicles_type_info = new String[]{'ownedVehicles','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] nbReplies_type_info = new String[]{'nbReplies','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] wsVersion_type_info = new String[]{'wsVersion','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ipAdress_type_info = new String[]{'ipAdress','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] codErreur_type_info = new String[]{'codErreur','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] dbCible_type_info = new String[]{'dbCible','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] wsHostname_type_info = new String[]{'wsHostname','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] wsTexec_type_info = new String[]{'wsTexec','http://www.w3.org/2001/XMLSchema','long','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'mode','country','brand','demander','vin','lastName','firstName','city','zip','ident1','idClient','idMyr','firstRegistrationDate','registration','email','logDate','ownedVehicles','nbReplies','wsVersion','ipAdress','codErreur','dbCible','wsHostname','wsTexec'};
    }
    public class GetCustDataError {
        public String fault;
        private String[] fault_type_info = new String[]{'fault','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'fault'};
    }
    public class Contract {
        public String idContrat;
        public String type_x;
        public String techLevel;
        public String serviceLevel;
        public String productLabel;
        public String initKm;
        public String maxSubsKm;
        public String subsDate;
        public String initContractDate;
        public String endContractDate;
        public String status;
        public String updDate;
        public String idMandator;
        public String idCard;
        private String[] idContrat_type_info = new String[]{'idContrat','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] techLevel_type_info = new String[]{'techLevel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] serviceLevel_type_info = new String[]{'serviceLevel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] productLabel_type_info = new String[]{'productLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] initKm_type_info = new String[]{'initKm','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] maxSubsKm_type_info = new String[]{'maxSubsKm','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] subsDate_type_info = new String[]{'subsDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] initContractDate_type_info = new String[]{'initContractDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] endContractDate_type_info = new String[]{'endContractDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] status_type_info = new String[]{'status','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] updDate_type_info = new String[]{'updDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idMandator_type_info = new String[]{'idMandator','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idCard_type_info = new String[]{'idCard','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'idContrat','type_x','techLevel','serviceLevel','productLabel','initKm','maxSubsKm','subsDate','initContractDate','endContractDate','status','updDate','idMandator','idCard'};
    }
    public class GetCustDataResponse {
        public Rforce_CustdataCrmBserviceRenault.PersonnalInformation[] clientList;
        public Rforce_CustdataCrmBserviceRenault.wsInfos wsInfos;
        public String responseCode;
        public String nbReplies;
        private String[] clientList_type_info = new String[]{'clientList','http://custdata.crm.bservice.renault','PersonnalInformation','0','-1','true'};
        private String[] wsInfos_type_info = new String[]{'wsInfos','http://custdata.crm.bservice.renault','wsInfos','0','1','true'};
        private String[] responseCode_type_info = new String[]{'responseCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] nbReplies_type_info = new String[]{'nbReplies','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'clientList','wsInfos','responseCode','nbReplies'};
    }
    public class Dealer {
        public String[] birId;
        private String[] birId_type_info = new String[]{'birId','http://www.w3.org/2001/XMLSchema','string','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'birId'};
    }
    public class CustDataRequest {
        public String mode;
        public String country;
        public String brand;
        public String demander;
        public String vin;
        public String lastName;
        public String firstName;
        public String city;
        public String zip;
        public String ident1;
        public String idClient;
        public String idMyr;
        public String firstRegistrationDate;
        public String registration;
        public String email;
        public String sinceDate;
        public String ownedVehicles;
        public String nbReplies;
        private String[] mode_type_info = new String[]{'mode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] brand_type_info = new String[]{'brand','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] demander_type_info = new String[]{'demander','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] vin_type_info = new String[]{'vin','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] zip_type_info = new String[]{'zip','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ident1_type_info = new String[]{'ident1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idClient_type_info = new String[]{'idClient','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idMyr_type_info = new String[]{'idMyr','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstRegistrationDate_type_info = new String[]{'firstRegistrationDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] registration_type_info = new String[]{'registration','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] email_type_info = new String[]{'email','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] sinceDate_type_info = new String[]{'sinceDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ownedVehicles_type_info = new String[]{'ownedVehicles','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] nbReplies_type_info = new String[]{'nbReplies','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'mode','country','brand','demander','vin','lastName','firstName','city','zip','ident1','idClient','idMyr','firstRegistrationDate','registration','email','sinceDate','ownedVehicles','nbReplies'};
    }
    public class Desc_x {
        public String type_x;
        public String typeCode;
        public String subType;
        public String subTypeCode;
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] typeCode_type_info = new String[]{'typeCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] subType_type_info = new String[]{'subType','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] subTypeCode_type_info = new String[]{'subTypeCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','typeCode','subType','subTypeCode'};
    }
    public class PersonnalInformation {
        public String idClient;
        public String lastName;
        public String title;
        public String lang;
        public String middleName;
        public String firstName;
        public String distinction1;
        public String distinction2;
        public String initiales;
        public String typeIdent1;
        public String ident1;
        public String ident2;
        public String idMyr;
        public String typeperson;
        public String birthDay;
        public String sex;
        public String soc;
        public String legalCategory;
        public Rforce_CustdataCrmBserviceRenault.Contact contact;
        public Rforce_CustdataCrmBserviceRenault.Address address;
        public String hasRenaultCard;
        public String flagMyR;
        public Rforce_CustdataCrmBserviceRenault.Vehicle[] vehicleList;
        public Rforce_CustdataCrmBserviceRenault.Dealer[] dealerList;
        public Rforce_CustdataCrmBserviceRenault.Survey survey;
        private String[] idClient_type_info = new String[]{'idClient','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] title_type_info = new String[]{'title','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] lang_type_info = new String[]{'lang','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] middleName_type_info = new String[]{'middleName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] distinction1_type_info = new String[]{'distinction1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] distinction2_type_info = new String[]{'distinction2','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] initiales_type_info = new String[]{'initiales','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] typeIdent1_type_info = new String[]{'typeIdent1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ident1_type_info = new String[]{'ident1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ident2_type_info = new String[]{'ident2','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] idMyr_type_info = new String[]{'idMyr','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] typeperson_type_info = new String[]{'typeperson','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] birthDay_type_info = new String[]{'birthDay','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] sex_type_info = new String[]{'sex','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] soc_type_info = new String[]{'soc','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] legalCategory_type_info = new String[]{'legalCategory','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] contact_type_info = new String[]{'contact','http://custdata.crm.bservice.renault','Contact','0','1','true'};
        private String[] address_type_info = new String[]{'address','http://custdata.crm.bservice.renault','Address','0','1','true'};
        private String[] hasRenaultCard_type_info = new String[]{'hasRenaultCard','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] flagMyR_type_info = new String[]{'flagMyR','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] vehicleList_type_info = new String[]{'vehicleList','http://custdata.crm.bservice.renault','Vehicle','0','-1','true'};
        private String[] dealerList_type_info = new String[]{'dealerList','http://custdata.crm.bservice.renault','Dealer','0','-1','true'};
        private String[] survey_type_info = new String[]{'survey','http://custdata.crm.bservice.renault','Survey','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'idClient','lastName','title','lang','middleName','firstName','distinction1','distinction2','initiales','typeIdent1','ident1','ident2','idMyr','typeperson','birthDay','sex','soc','legalCategory','contact','address','hasRenaultCard','flagMyR','vehicleList','dealerList','survey'};
    }
    public class CrmGetCustData {
        public String endpoint_x = 'https://webservices.renault.fr/rs2/4/CrmGetQuest';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://common.bservice.renault', 'Rforce_CustdataCrmBserviceRenault', 'http://custdata.crm.bservice.renault', 'Rforce_CustdataCrmBserviceRenault'};
        public Rforce_CustdataCrmBserviceRenault.GetCustDataResponse getCustData(Rforce_CustdataCrmBserviceRenault.GetCustDataRequest request) {
            Rforce_CustdataCrmBserviceRenault.getCustData_element request_x = new Rforce_CustdataCrmBserviceRenault.getCustData_element();
            Rforce_CustdataCrmBserviceRenault.getCustDataResponse_element response_x;
            request_x.request = request;
            Map<String, Rforce_CustdataCrmBserviceRenault.getCustDataResponse_element> response_map_x = new Map<String, Rforce_CustdataCrmBserviceRenault.getCustDataResponse_element>();
            response_map_x.put('response_x', response_x);
            // RV adjusted start
            if (Test.isRunningTest()){
                response_x = new Rforce_CustdataCrmBserviceRenault.getCustDataResponse_element();
                response_x.response = Rforce_Utils_Stubs.ContractStub();
            } else {
                System.debug('endpoint_x >>>>>>>>>'+endpoint_x);
                System.debug('request_x>>>>>>>>'+request_x);
                
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  'getCustData',
                  'http://custdata.crm.bservice.renault' ,  
                  'getCustData',
                  'http://custdata.crm.bservice.renault',  
                  'getCustDataResponse',
                  'Rforce_CustdataCrmBserviceRenault.getCustDataResponse_element'}
                );
                response_x = response_map_x.get('response_x');
            }
            System.debug('response_x>>>>>>>>'+response_x);
            return response_x.response;
        } // RV adjusted end
    }
    public class Address {
        public String strName;
        public String strNum;
        public String compl1;
        public String compl2;
        public String compl3;
        public String strType;
        public String strTypeLabel;
        public String countryCode;
        public String zip;
        public String city;
        public String qtrCode;
        public String dptCode;
        public String sortCode;
        public String numberPB;
        public String zipPB;
        public String cityPB;
        public String areaCode;
        public String areaLabel;
        public String addressUpdateDate;
        private String[] strName_type_info = new String[]{'strName','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] strNum_type_info = new String[]{'strNum','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] compl1_type_info = new String[]{'compl1','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] compl2_type_info = new String[]{'compl2','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] compl3_type_info = new String[]{'compl3','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] strType_type_info = new String[]{'strType','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] strTypeLabel_type_info = new String[]{'strTypeLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] zip_type_info = new String[]{'zip','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] qtrCode_type_info = new String[]{'qtrCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] dptCode_type_info = new String[]{'dptCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] sortCode_type_info = new String[]{'sortCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] numberPB_type_info = new String[]{'numberPB','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] zipPB_type_info = new String[]{'zipPB','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] cityPB_type_info = new String[]{'cityPB','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] areaCode_type_info = new String[]{'areaCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] areaLabel_type_info = new String[]{'areaLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] addressUpdateDate_type_info = new String[]{'addressUpdateDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'strName','strNum','compl1','compl2','compl3','strType','strTypeLabel','countryCode','zip','city','qtrCode','dptCode','sortCode','numberPB','zipPB','cityPB','areaCode','areaLabel','addressUpdateDate'};
    }
    public class wsInfos {
        public String wsVersion;
        public String wsEnv;
        public String wsDb;
        private String[] wsVersion_type_info = new String[]{'wsVersion','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] wsEnv_type_info = new String[]{'wsEnv','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] wsDb_type_info = new String[]{'wsDb','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'wsVersion','wsEnv','wsDb'};
    }
    public class GetCustDataRequest {
        public Rforce_CustdataCrmBserviceRenault.ServicePreferences servicePrefs;
        public Rforce_CustdataCrmBserviceRenault.CustDataRequest custDataRequest;
        private String[] servicePrefs_type_info = new String[]{'servicePrefs','http://common.bservice.renault','ServicePreferences','1','1','true'};
        private String[] custDataRequest_type_info = new String[]{'custDataRequest','http://custdata.crm.bservice.renault','CustDataRequest','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'servicePrefs','custDataRequest'};
    }
    public class getCustDataResponse_element {
        public Rforce_CustdataCrmBserviceRenault.GetCustDataResponse response;
        private String[] response_type_info = new String[]{'response','http://custdata.crm.bservice.renault','GetCustDataResponse','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class WorkShop {
        public String date_x;
        public String km;
        public String birId;
        public Rforce_CustdataCrmBserviceRenault.Desc_x description;
        private String[] date_x_type_info = new String[]{'date','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] km_type_info = new String[]{'km','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] birId_type_info = new String[]{'birId','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] description_type_info = new String[]{'description','http://custdata.crm.bservice.renault','Desc','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'date_x','km','birId','description'};
    }
    public class Vehicle {
        public String vin;
        public String brandCode;
        public String brandLabel;
        public String modelCode;
        public String modelLabel;
        public String versionLabel;
        public String vnvo;
        public String registrationDate;
        public String firstRegistrationDate;
        public String registration;
        public String new_x;
        public String energy;
        public String fiscPow;
        public String dynPow;
        public String doorNum;
        public String transmissionType;
        public String capacity;
        public String colorCode;
        public String possessionBegin;
        public String paymentMethod;
        public String technicalInspectionDate;
        public String possessionEnd;
        public String purchaseNature;
        public Rforce_CustdataCrmBserviceRenault.WorkShop[] workshopList;
        public Rforce_CustdataCrmBserviceRenault.Contract[] contractList;
        private String[] vin_type_info = new String[]{'vin','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] brandCode_type_info = new String[]{'brandCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] brandLabel_type_info = new String[]{'brandLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] modelCode_type_info = new String[]{'modelCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] modelLabel_type_info = new String[]{'modelLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] versionLabel_type_info = new String[]{'versionLabel','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] vnvo_type_info = new String[]{'vnvo','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] registrationDate_type_info = new String[]{'registrationDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] firstRegistrationDate_type_info = new String[]{'firstRegistrationDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] registration_type_info = new String[]{'registration','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] new_x_type_info = new String[]{'new','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] energy_type_info = new String[]{'energy','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] fiscPow_type_info = new String[]{'fiscPow','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] dynPow_type_info = new String[]{'dynPow','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] doorNum_type_info = new String[]{'doorNum','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] transmissionType_type_info = new String[]{'transmissionType','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] capacity_type_info = new String[]{'capacity','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] colorCode_type_info = new String[]{'colorCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] possessionBegin_type_info = new String[]{'possessionBegin','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] paymentMethod_type_info = new String[]{'paymentMethod','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] technicalInspectionDate_type_info = new String[]{'technicalInspectionDate','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] possessionEnd_type_info = new String[]{'possessionEnd','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] purchaseNature_type_info = new String[]{'purchaseNature','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] workshopList_type_info = new String[]{'workshopList','http://custdata.crm.bservice.renault','WorkShop','0','-1','true'};
        private String[] contractList_type_info = new String[]{'contractList','http://custdata.crm.bservice.renault','Contract','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'vin','brandCode','brandLabel','modelCode','modelLabel','versionLabel','vnvo','registrationDate','firstRegistrationDate','registration','new_x','energy','fiscPow','dynPow','doorNum','transmissionType','capacity','colorCode','possessionBegin','paymentMethod','technicalInspectionDate','possessionEnd','purchaseNature','workshopList','contractList'};
    }
    public class ServicePreferences {
        public String irn;
        public String sia;
        public String reqid;
        public String userid;
        public String language;
        public String country;
        private String[] irn_type_info = new String[]{'irn','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] sia_type_info = new String[]{'sia','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] reqid_type_info = new String[]{'reqid','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] userid_type_info = new String[]{'userid','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] language_type_info = new String[]{'language','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://common.bservice.renault','false','false'};
        private String[] field_order_type_info = new String[]{'irn','sia','reqid','userid','language','country'};
    }
}