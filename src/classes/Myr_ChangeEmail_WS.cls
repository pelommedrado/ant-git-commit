/*  Change of Email (Webservice) 
    Error codes :   OK           : WS04MS000 
            OK WITH WARNING : WS04MS001 -> WS04MS500
            CRITICAL ERROR  : WS04MS501 -> WS04MS999
*************************************************************************************************************
27 Jan 2015 : Creation
30 Apr 2015 : Improve the log systems, merge the future function addLog and updateAccount to be sure to get any information regarding changeEmail linked to the same parent log
17 Sep 2015 : Synchro ATC
16 Nov 2015 : R16.01 9.1 ChocolateCamelBaby, D. Veron (AtoS) : User's login and account's personal email become independent
18 Mar 2016 : R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
26 Sep 2016 : R16.11 (S. Ducamp - AtoS) : Refactoring changeEmail for MyR/MyD coexistence
*************************************************************************************************************/ 
global without sharing class Myr_ChangeEmail_WS {

	public enum TEST_EXCEPTION {GENERAL_EXCEPTION}
	public class Myr_ChangeEmail_WS_Exception extends Exception{}
    
	//WebService used to change email of a given account
    //@param accountSfdcId, the SFDC id of the personal account on which we want change the email address
    //@param emailAddress, the new email address  
    //@return a msg
    WebService static Myr_ChangeEmail_WS_Response changeEmail(String accountSfdcId, String emailAddress) { 

		try {
			if( Test.isRunningTest() && emailAddress.startsWithIgnoreCase( TEST_EXCEPTION.GENERAL_EXCEPTION.name() ) ) {
				throw new Myr_ChangeEmail_WS_Exception( 'Test Exception' );
			}

			Myr_ChangeEmail_Cls changeEmail = new Myr_ChangeEmail_Cls( accountSfdcId, emailAddress );
			Myr_ChangeEmail_Cls.Myr_ChangeEmail_Response innerResp = changeEmail.process();

			//Map the response in the webservice response to keep the same format for our prefered partner ....
			Myr_ChangeEmail_WS_Response response = new Myr_ChangeEmail_WS_Response();
			response.info.code = innerResp.info.code;
			response.info.message = innerResp.info.message;
			return response;
		} catch (Exception e) {
			//oups ! Should not occur !!!
			Myr_ChangeEmail_WS_Response response = new Myr_ChangeEmail_WS_Response();
			response.info.code = system.Label.Myr_ChangeEmail_WS04MS504;
			response.info.message = String.format( system.Label.Myr_ChangeEmail_WS04MS504_Msg, new List<String>{e.getMessage()} );
			return response;
		}
    }  
    
    //Global format of the response
    global class Myr_ChangeEmail_WS_Response {
        WebService Myr_ChangeEmail_WS_Response_Msg info;
		//@inner constructor
		public Myr_ChangeEmail_WS_Response() {
			info = new Myr_ChangeEmail_WS_Response_Msg();
		}
    }
    global class Myr_ChangeEmail_WS_Response_Msg {
        WebService String code;
        WebService String message;
    }
    
}