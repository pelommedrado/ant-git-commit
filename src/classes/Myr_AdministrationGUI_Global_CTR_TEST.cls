/**
* @author sebastien.ducamp@atos
* @description Test class for Myr_AdministrationGUI_Global_CTR
* @version 1.0 / 16.07 / Sébastien Ducamp / Initial revision

*/
@isTest
private class Myr_AdministrationGUI_Global_CTR_TEST {

	//Prepare CS04_MYR_Settings__c
	@testsetup static void setCustomSettings() {
		CS04_MYR_Settings__c setting = new CS04_MYR_Settings__c();
		setting.Myr_Prefix_Unique__c = 'rhelios_';
		setting.Myr_Profile_Community_User__c = 'HeliosCommunity';
		setting.Myr_Personal_Account_Record_Type__c = 'CORE_ACC_Personal_Account_RecType';
		setting.Myr_Profile_Helios_ReadOnly__c = 'HeliosTechnique';
		insert setting; 
	}

	//Test that we get properly the MyRenault Settings
	static testMethod void testInit() {
		//Prepare a HeliosLocalAdministrator admin
		User adminUser = Myr_Datasets_Test.getHeliosAdminUser('Helios', 'Admin User', 'France');
		//Trigger the test
		system.runAs( adminUser ) {
			Myr_AdministrationGUI_Global_CTR controller = new Myr_AdministrationGUI_Global_CTR();
			system.assertEquals( false, controller.EditMode );
			system.assertEquals( 'rhelios_', controller.InnerSettings.Myr_Prefix_Unique__c );
			system.assertEquals( 'HeliosCommunity', controller.InnerSettings.Myr_Profile_Community_User__c );
		}
	}

	//Test the save functionnality
	static testMethod void testSave() {
		//Prepare a HeliosLocalAdministrator admin
		User adminUser = Myr_Datasets_Test.getHeliosAdminUser('Helios', 'Admin User', 'France');
		//Trigger the test
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		system.runAs( adminUser ) {
			Myr_AdministrationGUI_Global_CTR controller = new Myr_AdministrationGUI_Global_CTR();
			system.assertEquals( 'rhelios_', controller.InnerSettings.Myr_Prefix_Unique__c );
			system.assertEquals( 'rhelios_', controller.MyrSettings.Fields.get('myr_prefix_unique__c').getValue() );
			controller.MyrSettings.Fields.get('myr_prefix_unique__c').value = 'toto';
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.CONFIRM ) );
			system.assertEquals( 'toto', CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c );
		}
	}

	//Test the save functionnality with exception
	static testMethod void testSaveException() {
		//Prepare a HeliosLocalAdministrator admin
		User adminUser = Myr_Datasets_Test.getHeliosAdminUser('Helios', 'Admin User', 'France');
		//Trigger the test
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		system.runAs( adminUser ) {
			Myr_AdministrationGUI_Global_CTR controller = new Myr_AdministrationGUI_Global_CTR();
			system.assertEquals( 'rhelios_', controller.InnerSettings.Myr_Prefix_Unique__c );
			controller.MyrSettings.Fields.get('myr_prefix_unique__c').value = 'longstringggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg';
			controller.save();
			system.assertEquals( true, ApexPages.hasMessages( ApexPages.Severity.ERROR ) );
		}
	}

	//Test the switch to edit mode
	static testMethod void testEditCancelMode() {
		//Prepare a HeliosLocalAdministrator admin
		User adminUser = Myr_Datasets_Test.getHeliosAdminUser('Helios', 'Admin User', 'France');
		//Trigger the test
		Test.setCurrentPage( Page.Myr_AdministrationGUI );
		system.runAs( adminUser ) {
			Myr_AdministrationGUI_Global_CTR controller = new Myr_AdministrationGUI_Global_CTR();
			system.assertEquals( false, controller.EditMode );
			controller.edit();
			system.assertEquals( true, controller.EditMode );
			controller.cancel();
		}
	}

}