/** 
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC64_QuoteDetailsController_Test
{
	static testMethod void unitTest01()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                       IDBIR__c = '10640');
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id,
							                       Pricebook2Id = pb2.Id);
		insert opportunity1;

        Quote aQuote = new Quote (Name = 'QuoteNameTest_1',
        					 OpportunityId = opportunity1.Id,
        					 Pricebook2Id = pb2.Id);
        insert aQuote;
		
		 // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
				                     FROM PricebookEntry 
				                     WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
				                     AND Pricebook2Id =: pb2.Id
				                     AND IsActive = true
				                     AND CurrencyIsoCode = 'BRL'
				                     limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = aQuote.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;

    	Apexpages.currentPage().getParameters().put('id', aQuote.Id);
    	Apexpages.currentPage().getParameters().put('opportunityId', opportunity1.Id);
    	Apexpages.currentPage().getParameters().put('quoteLineItemId', qLineItem.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(aQuote);

    	// Start Test
    	Test.startTest();

        VFC64_QuoteDetailsController controller = new VFC64_QuoteDetailsController(stdCont);
        controller.initialize();
        controller.generatePDF();
        controller.generatePreOrder();
        controller.getBrandDetails();
        controller.getBrands();
        controller.getIsReadOnly();
        controller.getModelDetails();
        controller.getModels();
        controller.getValueTraded();
        controller.reserveVehicle();
        controller.updateQuoteDetails();
        controller.cancelQuotation();
        controller.deleteQuoteLineItem();
        controller.redirectToNewAccessory();
        controller.redirectToNewVehico();
        controller.redirectToNewAccessoryEdit();

        controller.quoteDetailsVO.sObjQuote.ExpirationDate = Date.today();
        controller.reserveVehicle();
        controller.generatePreOrder();
        controller.checkCDC();
        controller.checkLSG();
        
		// Stop Test
		Test.stopTest();
    }
}