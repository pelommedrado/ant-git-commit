public class CAC_CaseUtilsNormaly extends CAC_CaseUtilsMaster{
    
    public CAC_CaseUtilsNormaly(){
        super();
    }
    public CAC_CaseUtilsNormaly(List<Case> caseList, Map<Id,Case>oldCase){
        super(caseList,oldCase);
    }
    
    public void statusAnswersWaiting(){
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Analyst')){
            
            
            Set<Id> setIdCase = new Set<ID>();
            for(Case caseI: lsCase)
                if(caseI.Status.equals('Waiting for reply'))
                setIdCase.add(caseI.Id);
            
            
            //mapa com os ultimos comentário do caso
            Map<Id,CaseComment> mapCaseCommentId = new Map<Id,CaseComment>();
            for(CaseComment caseCom : [select Id, ParentId,CreatedById,IsPublished  from CaseComment where ParentId in:(setIdCase) and IsPublished = true order by id asc ])
                mapCaseCommentId.put(caseCom.ParentId, caseCom);
            
            
            for(Case caseI: lsCase){
                
                if(caseI.Status.equals('Waiting for reply') && mapCaseCommentId.containsKey(caseI.id)){
                    if(!mapCaseCommentId.get(caseI.id).CreatedById.equals(UserInfo.getUserId())){
                        caseI.addError('Insira um comentário');
                    }
                }else if(caseI.Status.equals('Waiting for reply')){
                    caseI.addError('Insira um comentário');
                }
            }
            
        }
    }
    
    public void statusAnswers(){
        if(userInfos.isCac__c && userInfos.roleInCac__c.equals('Analyst')){
            List<Case> caseIdList = new List<Case>();
            for(Case caseCac : lsCase){
                
                if(mapOldCase.get(caseCac.id).Answer__c <> caseCac.Answer__c && !caseCac.Status.equalsIgnoreCase('Answered'))
                    caseCac.addError('O campo status deve ser alterado para Respondido');
                
                if(String.isNotEmpty(mapOldCase.get(caseCac.id).Answer__c)  
                   && !mapOldCase.get(caseCac.id).Answer__c.equals(caseCac.Answer__c) )
                    caseCac.addError('A reposta não pode ser alterada');
                
                if(caseCac.Status.equalsIgnoreCase('Answered') && String.isNotEmpty(mapOldCase.get(caseCac.id).Answer__c))
                    caseIdList.add(caseCac);
            }
            
            Map<String,CaseComment> mapCaseComem = new Map<String,CaseComment>();   
            for(CaseComment casec: [select id,ParentId,CreatedById, CreatedDate,IsPublished from CaseComment where ParentId in: caseIdList order by CreatedDate asc] )
                mapCaseComem.put(casec.ParentId, casec);
            
            for(Case caseCac : caseIdList){
                if(mapCaseComem.containsKey(caseCac.Id) && (!mapCaseComem.get(caseCac.Id).CreatedById.equals(UserInfo.getUserId()) || !mapCaseComem.get(caseCac.Id).IsPublished))
                    caseCac.addError('Insira um comentário');
            }
            
        }
    }
    
    public void updateField(){
        
        Map<String,cacweb_settingTime__c> mapExpired = getExpiredDate();
        
        for(Case caseUp: lsCase){
            
            if(String.isNotBlank(caseUp.Status) && caseUp.Status.equals('New') &&
                caseUp.OwnerId != UserInfo.getUserId() && userInfos.isCac__c && 
                String.isNotEmpty(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer') &&
                !String.isEmpty(caseUp.Subject__c) && mapExpired.containsKey(caseUp.Subject__c) && 
                !caseUp.Expired__c){
                    
                    caseUp.Expiration_Date__c = corretDayReturn(
                        Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).hoursExpire__c)),
                        Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                        Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                    );
                    
                    caseUp.beforeExpiredDate__c = corretDayReturn(
                        Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).alertExpire__c)),
                        Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                        Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                    );
                }
            
            else if(String.isNotBlank(caseUp.Status) && (caseUp.Status.equals('Waiting for reply') || caseUp.Status.equals('Answered'))
                   && !String.isEmpty(caseUp.Subject__c) && mapExpired.containsKey(caseUp.Subject__c)){
                integer valueStartDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c);
                integer valueFinishedDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c);
                
                /*caseUp.CAC_nearAutomaticClosure__c = corretDayReturn(25,
                                                                     valueStartDate,
                                                                     valueFinishedDate);
                caseUp.CAC_automaticClosure__c = corretDayReturn(24,
                                                                 valueStartDate,
                                                                 valueFinishedDate);
                caseUp.CAC_HourExpired8__c = corretDayReturn(8,
                                                             valueStartDate,
                                                             valueFinishedDate);
                
                caseUp.CAC_HourExpired16__c = corretDayReturn(16,
                                                              valueStartDate,
                                                              valueFinishedDate);*/
                caseUp.CAC_HourExpired24__c = corretDayReturn(24,
                                                              valueStartDate,
                                                              valueFinishedDate);
                
            }else{
                caseUp.CAC_nearAutomaticClosure__c = null;
                caseUp.CAC_automaticClosure__c = null;
                caseUp.CAC_HourExpired8__c = null;
                caseUp.CAC_HourExpired16__c = null;
                caseUp.CAC_HourExpired24__c  = null;
            }    
        }
    }
    
    public static void alterOwner(List<Case> listaCaso, Map<Id,Case> mapOldCaso){
        
        User userInf = [select Id, isCac__c, roleInCac__c from User where id =: UserInfo.getUserId()];
        if(userInf.isCac__c && String.isNotEmpty(userInf.roleInCac__c) && userInf.roleInCac__c.equals('Analyst')){
            for(Case casea: listaCaso){
                if(casea.OwnerId != UserInfo.getUserId()){
                    if((casea.Status.equals('Waiting for reply')||
                        casea.Status.equals('Answered')||
                        casea.Status.equals('Under Analysis')
                       ) && 
                       mapOldCaso.get(casea.id).Status.equals('New')){
                           casea.OwnerId = UserInfo.getUserId();
                           if(casea.Status.equals('Waiting for reply')||casea.Status.equals('Answered') ){
                               casea.Status = 'Under Analysis';
                           }
                       }
                }
            }
            
            //Para status respondido
            List<Id> lsIdOldOwner = new List<Id>();
            for(Case casea: listaCaso){//Waiting for reply
                if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered')) || 
                   (!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply'))){
                      lsIdOldOwner.add(mapOldCaso.get(casea.Id).OwnerId);
                  }
            }
            
            Map<String,User>mapUser = new Map<String,User>([select id , email from User where id in:(lsIdOldOwner)]);
            
            for(Case casea: listaCaso){    
                casea.Owner_CAC__c = casea.Owner.Name;
                if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered') )||
                   (!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply'))){
                      if(mapUser.containsKey(casea.OwnerId)){
                          casea.CAC_EmailAnalyst__c = mapUser.get(casea.OwnerId).Email;
                          casea.OwnerId = casea.CreatedById;
                      }
                  }
            }
            
        }else if(userInf.isCac__c && String.isNotEmpty(userInf.roleInCac__c) && userInf.roleInCac__c.equals('Dealer')){
            
            Map<id,RecordType> mapRecordType =  new Map<id,RecordType>(Utils.getRecordType('Case'));
            
            List<String>lsNameQueue = new List<String>();
            Map<String,cacweb_Mapping__c> mapCacMap = new Map<String,cacweb_Mapping__c>();
            for(cacweb_Mapping__c cacMap: cacweb_Mapping__c.getAll().values()){
                lsNameQueue.add(cacMap.Name_Queue__c);
                mapCacMap.put(cacMap.Record_Type_Name__c, cacMap);
            }
            
            Map<String,Group> mapGroup = new Map<String,Group>();
            for( Group groupQueue : [select Id, DeveloperName from Group where Type = 'Queue' and DeveloperName in:(lsNameQueue)])
                mapGroup.put(groupQueue.DeveloperName, groupQueue);
            
            Map<String,CaseComment> mapCaseComem = new Map<String,CaseComment>();   
            for(CaseComment casec: [select id,ParentId, CreatedDate from CaseComment where ParentId in:(listaCaso) and CreatedById =: UserInfo.getUserId()])
                mapCaseComem.put(casec.ParentId, casec);
            
            for(Case caso : listaCaso){
                if(mapOldCaso != null && !mapOldCaso.isEmpty() && String.isNotBlank(caso.Status) && 
                   caso.Status.equals('New') && mapRecordType.containsKey(caso.RecordTypeId) &&
                   mapCacMap.containsKey(mapRecordType.get(caso.RecordTypeId).DeveloperName) &&
                   mapGroup.containsKey(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c)){
                       if(mapCaseComem.containsKey(caso.Id) && mapCaseComem.get(caso.Id).CreatedDate> mapOldCaso.get(caso.Id).LastModifiedDate){
                           caso.OwnerId =mapGroup.get(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c).Id;  
                       }
                       if(!mapOldCaso.get(caso.Id).Status.equals('New'))
                           caso.Case_Return__c  = true;
                }
            }
        }
    }
    
    public void blockStatus(){
        
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c)){
            
            for(Case caso: lsCase){
                if( 
                    (mapOldCase.get(caso.id).Status.equals('Closed Dealer') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Automatically Answered') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Automatically Waiting Answer') 
                    )
                ){
                    caso.addError('O caso  não pode ser modificado porque está fechado.');  
                }
                
            }
        }
        /*if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Analyst')){
            for(Case caso: lsCase){
                
                if((mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                    mapOldCase.get(caso.Id).Status.equals('Answered'))&&
                   mapOldCase.get(caso.Id).Status != caso.Status
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }
                if(caso.Status.equals('New') && (mapOldCase.get(caso.Id).Status.equals('Under Analysis')||
                                                 mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                                                 mapOldCase.get(caso.Id).Status.equals('Answered')
                                                )
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }else{
                      if(!(caso.Status.equals('New')||
                           caso.Status.equals('Under Analysis')||
                           caso.Status.equals('Waiting for reply')||
                           caso.Status.equals('Answered')
                          )){
                              caso.addError('O caso não pode ser modificado');
                          }
                      
                  }
            }
        }
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer')){
            for(Case caso: lsCase){
                if(!(caso.Status.equals('New')||
                     caso.Status.equals('Closed Dealer')||
                     caso.Status.equals('Closed Automatically Answered')||
                     caso.Status.equals('Closed Automatically Waiting Answer')
                    )){
                        caso.addError('O caso não pode ser modificado');
                    }
            }
        }*/
    }
    
}