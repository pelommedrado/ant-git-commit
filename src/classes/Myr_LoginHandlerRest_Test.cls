/** Apex Class used to test the LoginHandler MyRenault
  @author S. Ducamp
  @date 26.08.2015
  @version 1.0
 */
@isTest
private class Myr_LoginHandlerRest_Test {

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, true) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
	}

	//Check the response of the webservice (always OK);
	private static void checkResponse(String iResponse) {
		system.assertEquals('OK', iResponse);
	}

	/** Check the elements inlcuded in the logs
	 * @param List<String>, the list of records to check in Record__c
	 * @param List<String>, the list of messages to check in the field Message__c
	 * @return void
	 **/ 
	private static void checkLogs(List<String> records, List<String> messages) {
		system.debug('### Myr_LoginHandlerRest_Test - <checkLogs> - BEGIN');
		List<Logger__c> logs = [SELECT Id, RunId__c, Error_Level__c, Exception_Type__c, Function__c, Message__c, Project_Name__c, Record__c FROM Logger__c];
		//Prepare the results vectors
		Boolean[] checkRecords = new Boolean[records.size()];
		for (Integer i = 0; i < records.size();++ i) {
			checkRecords[i] = false;
		}
		Boolean[] checkMessages = new Boolean[messages.size()];
		for (Integer j = 0; j < messages.size();++ j) {
			checkMessages[j] = false;
		}
		//Transverse the logs
		for (Logger__c log : logs) {
			system.debug('### Myr_LoginHandlerRest_Test - <checkLogs> - log=' + log);
			for (Integer i = 0; i < records.size();++ i) {
				if (!String.isBlank(log.Record__c) && log.Record__c.containsIgnoreCase(records[i])) {
					checkRecords[i] = true;
				}
			}
			for (Integer j = 0; j < messages.size();++ j) {
				if (!String.isBlank(log.Message__c) && log.Message__c.containsIgnoreCase(messages[j])) {
					checkMessages[j] = true;
				}
			}
		}
		//Check the results
		for (Integer i = 0; i < checkRecords.size();++ i) {
			if (!checkRecords[i]) {
				system.assertEquals(null, 'not found record: ' + records[i]);
			}
		}
		for (Integer j = 0; j < checkMessages.size();++ j) {
			if (!checkMessages[j]) {
				system.assertEquals(null, 'not found message: ' + messages[j]);
			}
		}

	}

	//-------------------------------------------------------------------------------------------------
	//----- BASIC TEST

	//Test that the brand treated in Login Handler is MyRenault
	static testMethod void testLoginHandler_CheckMyRBrand() {
		//Login Handler
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler
	}

	//Test that the brand treated in Login Handler is MyDacia
	static testMethod void testLoginHandler_CheckMyDBrand() {
		//Login Handler
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyDaciaRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler
	}

	//Test that nothing has been done for the give n uaser as he is not an Helios Community user
	static testMethod void testLoginHandler_CheckNotProperUser_1() {
		//Login Handler
		User techUser = Myr_Datasets_Test.getTechnicalUser('France');
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(techUser) {
			String response = Myr_LoginHandlerMyDaciaRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Log
		checkLogs(
		          new List<String> { 
					/*'UserId=' + techUser.Id + ', brand=Dacia', */'UserId=' + techUser.Id + ', brand=Dacia' },
		          new List<String> { 
					/*'entry in login handler for the user ' + techUser.Username,*/
			        'The user is not an Helios Community User: ' + techUser.Username }
		);
		//Just 2 logs: input and error ouput
		List<Logger__c> listLogs = [SELECT Id FROM Logger__c];
		system.assertEquals(1, listLogs.size());
	}

	//Test that nothing has been done for the give n uaser as he is not an Helios Community user
	static testMethod void testLoginHandler_CheckNotProperUser_2() {
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS_BADPROFILE);
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		//Login Handler
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyDaciaRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Log
		checkLogs(
		          new List<String> { 'UserId=' + communityUser.Id + ', brand=Dacia' },
		          new List<String> { 'The user is not an Helios Community User: ' + communityUser.Username }
		);
		//Just 2 logs: input and error ouput
		List<Logger__c> listLogs = [SELECT Id FROM Logger__c];
		system.assertEquals(1, listLogs.size());
	}

	//-------------------------------------------------------------------------------------------------
	//----- ACCOUNT UPDATE

	//Check that the account information are updated (last connection date)
	static testMethod void testLoginHandler_AccountUpdate_Renault() {
		//Login Handler
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Log
		checkLogs(
		          new List<String> { 'UserId=' + communityUser.Id + ', brand=Renault' },
		          new List<String> { 'update the last connection dates' }
		);

		//Check Account Information
		Account acc = [SELECT Id, MyR_Last_Connection_Date__c, MyD_Last_Connection_Date__c FROM Account WHERE Id = :listAcc[0].Id];
		system.assertEquals(true, Myr_Datasets_Test.checkEqualDates(system.today(), acc.MyR_Last_Connection_Date__c));
		system.assertEquals(null, acc.MyD_Last_Connection_Date__c);
	}

	//Check that the account information are updated (last connection date)
	static testMethod void testLoginHandler_AccountUpdate_Dacia() {
		//Login Handler
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyDaciaRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Log
		checkLogs(
		          new List<String> { 'UserId=' + communityUser.Id + ', brand=Dacia' },
		          new List<String> { 'update the last connection dates' }
		);

		//Check Account Information
		Account acc = [SELECT Id, MyR_Last_Connection_Date__c, MyD_Last_Connection_Date__c FROM Account WHERE Id = :listAcc[0].Id];
		system.assertEquals(true, Myr_Datasets_Test.checkEqualDates(system.today(), acc.MyD_Last_Connection_Date__c));
		system.assertEquals(null, acc.MyR_Last_Connection_Date__c);
	}
	

	//Problem when updating the account
	static testMethod void testLoginHandler_AccountUpdate_Exception() {
		//Login Handler
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Lastname = Myr_LoginHandlerRest_Cls.TEST_FAILURE.ACC_UPDATE.name();
		update listAcc[0]; 
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		Test.startTest(); //asynchronous methods launched by login handler
		system.runAs(communityUser) {
			String response = Myr_LoginHandlerMyDaciaRest_WS.loginHandler();
			checkResponse(response);
		}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Log
		checkLogs(
		          new List<String> { 'UserId=' + communityUser.Id + ', brand=Dacia' },
		          new List<String> { 'Problem when trying to update the last connection date for the user: Test Exception' }
		);

		//Check Account Information
		Account acc = [SELECT Id, MyR_Last_Connection_Date__c, MyD_Last_Connection_Date__c FROM Account WHERE Id = :listAcc[0].Id];
		system.assertEquals(null, acc.MyD_Last_Connection_Date__c);
		system.assertEquals(null, acc.MyR_Last_Connection_Date__c);
	}

	//-------------------------------------------------------------------------------------------------
	//----- PERSONAL DATA: RUBIcS Add Vehicle when the user is connecting to MyRenault

	//Get Vehicle: KO From MDM (even if activated)
	static testMethod void testPersData_AddVehicle_MDM_KO() {
		//pre-control
		Country_Info__c franceSetting = [SELECT Id, DataSource__c, Myr_EnablePersDataCall__c FROM Country_Info__c WHERE Name='France'];
		system.assertEquals( 'MDM', franceSetting.DataSource__c );
		franceSetting.Myr_EnablePersDataCall__c = true;
		franceSetting.Myr_Enable_Login_GetVehicle__c = true;
		update franceSetting;
		//prepare the data
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'France';
		update listAcc;
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];

		//Trigger the test
		Test.startTest(); //asynchronous methods launched by login handler
			system.runAs(communityUser) {
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Personal Data
		List<Logger__c> logs = [SELECT Id FROM Logger__c WHERE Function__c = :Myr_ManageAccount_PersDataUpdate_Cls.LOG_FUNCTION];
		system.assertEquals( 0, logs.size());
	}

	//Get Vehicle: KO From BCS (even if activated)
	static testMethod void testPersData_AddVehicle_BCS_KO() {
		//pre-control
		Country_Info__c russiaSetting = Country_Info__c.getInstance('Russia');
		system.assertEquals( 'BCS', russiaSetting.DataSource__c );
		system.assertEquals( true, russiaSetting.Myr_EnablePersDataCall__c );
		system.assertEquals( true, russiaSetting.Myr_Enable_Login_GetVehicle__c );
		//prepare the data
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'Russia';
		update listAcc;
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];

		//Trigger the test
		Test.startTest(); //asynchronous methods launched by login handler
			system.runAs(communityUser) {
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Personal Data
		List<Logger__c> logs = [SELECT Id FROM Logger__c WHERE Function__c = :Myr_ManageAccount_PersDataUpdate_Cls.LOG_FUNCTION];
		system.assertEquals( 0, logs.size());
	}

	//Get Vehicle: OK from SIC
	static testMethod void testPersData_AddVehicle_SIC_OK() {
		//pre-control
		Country_Info__c italySetting = Country_Info__c.getInstance('Italy');
		system.assertEquals( 'SIC', italySetting.DataSource__c );
		system.assertEquals( true, italySetting.Myr_EnablePersDataCall__c );
		system.assertEquals( true, italySetting.Myr_Enable_Login_GetVehicle__c );
		//prepare the data
		/*List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'Italy';
		listAcc[0].Bcs_Id_Dacia__c = 'xxxIDSICxxx';*/
		Account myAcc = new Account();
		system.runAs( Myr_Datasets_Test.getTechnicalUserWithRole('Italy') ) {
			myAcc = Myr_Datasets_Test.insertPersonalAccountAndUser('testPersData', 'AddVehicle_SIC_OK', '', 'testPersData.AddVehicle_SIC_OK@testunit.atos.net', '', '', 'USER_ACTIF','', 'Italy', '', '', '', '');
			myAcc.Bcs_Id_Dacia__c = 'xxxIDSICxxx';
			update myAcc;
		}
		system.assertEquals('Italy', [SELECT Id, Country__c FROM Account WHERE Id = :myAcc.Id][0].Country__c);
		//pre-control
		List<VRE_VehRel__c> listRelations = [SELECT Id FROM VRE_VehRel__c WHERE Account__c = :myAcc.Id];
		system.assertEquals( 0, listRelations.size() );

		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :myAcc.Id];
		//Trigger the test
		system.runAs(communityUser) {
			Test.startTest(); //asynchronous methods launched by login handler
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			Test.stopTest(); //asynchronous methods launched by login handler
		}
		

		//Check Personal Data
		Myr_ManageAccount_PersDataCall_Test.checkPersonalDataTries( 'PID', 1, 'idClient=xxxIDSICxxx' );
		//Check Vehicle
		List<VEH_Veh__c> listVehicles = [SELECT Id FROM VEH_Veh__c WHERE Name='VF17R5A0H48447489'];
		system.assertEquals( 1, listVehicles.size() );
		//Check Relation
		listRelations = [SELECT Id FROM VRE_VehRel__c
								WHERE Account__c=:myAcc.Id AND VIN__c=:listVehicles[0].Id];
		system.assertEquals(1, listRelations.size() );
	}

	//Get Vehicle: KO From SIC because deactivated
	static testMethod void testPersData_AddVehicle_SIC_Disabled() {
		//pre-control
		Country_Info__c italySetting = [SELECT Id, DataSource__c, Myr_EnablePersDataCall__c, Myr_Enable_Login_GetVehicle__c FROM Country_Info__c WHERE Name='Italy'];
		system.assertEquals( 'SIC', italySetting.DataSource__c );
		system.assertEquals( true, italySetting.Myr_Enable_Login_GetVehicle__c );
		italySetting.Myr_EnablePersDataCall__c = false;
		update italySetting;
		//prepare the data
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'Italy';
		update listAcc;
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];

		//Trigger the test
		Test.startTest(); //asynchronous methods launched by login handler
			system.runAs(communityUser) {
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Personal Data
		List<Logger__c> logs = [SELECT Id FROM Logger__c WHERE Function__c = :Myr_ManageAccount_PersDataUpdate_Cls.LOG_FUNCTION];
		system.assertEquals( 0, logs.size());
	}

	//Get Vehicle: KO From SIC because no id
	static testMethod void testPersData_AddVehicle_SIC_NoId() {
		//pre-control
		Country_Info__c italySetting = Country_Info__c.getInstance('Italy');
		system.assertEquals( 'SIC', italySetting.DataSource__c );
		system.assertEquals( true, italySetting.Myr_EnablePersDataCall__c );
		system.assertEquals( true, italySetting.Myr_Enable_Login_GetVehicle__c );
		//prepare the data
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'Italy';
		update listAcc;
		//pre-control
		List<VRE_VehRel__c> listRelations = [SELECT Id FROM VRE_VehRel__c WHERE Account__c = :listAcc[0].Id];
		system.assertEquals( 0, listRelations.size() );

		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];
		//Trigger the test
		Test.startTest(); //asynchronous methods launched by login handler
			system.runAs(communityUser) {
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Personal Data
		Myr_ManageAccount_PersDataCall_Test.checkPersonalDataTries( null, 0, null );
		//Check Vehicle
		List<VEH_Veh__c> listVehicles = [SELECT Id FROM VEH_Veh__c];
		system.assertEquals( 0, listVehicles.size() );
		//Check Relation
		listRelations = [SELECT Id FROM VRE_VehRel__c];
		system.assertEquals(0, listRelations.size() );
	}

	//Get Vehicle: KO From SIC because deactivated
	static testMethod void testPersData_AddVehicle_GetVehicle_Disabled() {
		//pre-control
		Country_Info__c italySetting = [SELECT Id, DataSource__c, Myr_EnablePersDataCall__c FROM Country_Info__c WHERE Name='Italy'];
		system.assertEquals( 'SIC', italySetting.DataSource__c );
		system.assertEquals( true, italySetting.Myr_EnablePersDataCall__c );
		italySetting.Myr_Enable_Login_GetVehicle__c = false;
		update italySetting;
		//prepare the data
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		listAcc[0].Country__c = 'Italy';
		update listAcc;
		User communityUser = [SELECT Id, Username FROM User WHERE AccountId = :listAcc[0].Id];

		//Trigger the test
		Test.startTest(); //asynchronous methods launched by login handler
			system.runAs(communityUser) {
				String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
				checkResponse(response);
			}
		Test.stopTest(); //asynchronous methods launched by login handler

		//Check Personal Data
		List<Logger__c> logs = [SELECT Id FROM Logger__c WHERE Function__c = :Myr_ManageAccount_PersDataUpdate_Cls.LOG_FUNCTION];
		system.assertEquals( 0, logs.size());
	} 

	//-------------------------------------------------------------------------------------------------
	//----- VEHICLE GARAGE STATUS: F1854 / US3654: check that vehicle garage status is updated synchronously
	//----- at login step

	//Check vehicles garage status
	static testmethod void test_Login_VehicleGarageStatus() {
		//Prepare the data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.ACTIVE_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'UnitTestAtos', '75000', '', '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'Jean-Claude.UnitTestAtos@atos.net';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0]; //Dacia account
		User userA = [SELECT Id, Username FROM User WHERE AccountId=:accA.Id];
		//Vehicles
		List<VEH_Veh__c> vehicles = new List<VEH_Veh__c>();
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST2', VehicleBrand__c='Dacia'));
		insert vehicles;
		//Relations
		List<VRE_VehRel__c> relations = new List<VRE_VehRel__c>();
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[0].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[1].Id, My_Garage_Status__c=''));
		insert relations;

		//Trigger the test
		system.runAs( userA ) {
			String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
			checkResponse(response);
		}

		//Check vehicle relations
		system.assertEquals('confirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[0].Id].My_Garage_Status__c);
		system.assertEquals('unconfirmed', [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[1].Id].My_Garage_Status__c);
	}

	//Check vehicles garage status in case of exception
	static testmethod void test_Login_VehicleGarageStatus_Exception() {
		//Prepare the data
		Myr_Datasets_Test.UserOptions uOpt = Myr_Datasets_Test.UserOptions.ACTIVE_USERS;
		List<Myr_Datasets_Test.TestPersAccount> listQueries = new List<Myr_Datasets_Test.TestPersAccount>();
		listQueries.add( new Myr_Datasets_Test.TestPersAccount('Jean-Claude', 'UnitTestAtos', '75000', Myr_LoginHandlerRest_Cls.UPDVEHREL_USREMAIL_FAILURE, '', '', uOpt, 'France', '', '', '', '') );
		listQueries[0].renaultid = 'Jean-Claude.UnitTestAtos@atos.net';
		listQueries[0].myr_status = system.Label.Myr_Status_Activated; 
		List<Account> listAccounts = Myr_Datasets_Test.insertPersonalAccandUsers(listQueries);
		Account accA = listAccounts[0];
		User userA = [SELECT Id, Username FROM User WHERE AccountId=:accA.Id];
		//Vehicles
		List<VEH_Veh__c> vehicles = new List<VEH_Veh__c>();
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST1', VehicleBrand__c='Renault'));
		vehicles.add( new VEH_Veh__c(Name='VFDEMO1IUNITTEST2', VehicleBrand__c='Dacia'));
		insert vehicles;
		//Relations
		List<VRE_VehRel__c> relations = new List<VRE_VehRel__c>();
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[0].Id, My_Garage_Status__c=''));
		relations.add( new VRE_VehRel__c(Account__c=accA.Id, VIN__c=vehicles[1].Id, My_Garage_Status__c=''));
		insert relations;

		//Trigger the test
		system.runAs( userA ) {
			String response = Myr_LoginHandlerMyRenaultRest_WS.loginHandler();
			checkResponse(response);
		}

		//Check vehicle relations
		system.assertEquals(null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[0].Id].My_Garage_Status__c);
		system.assertEquals(null, [SELECT Id, My_Garage_Status__c FROM VRE_VehRel__c WHERE Id=:relations[1].Id].My_Garage_Status__c);
	}

}