public with sharing class Rforce_Utils_MaintDataSource {

    public Boolean Test;
    
      public Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse getMaintenanceData(Rforce_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
         
         return getMaintenanceData(VehController.getVin());
      }
         
      public Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse getMaintenanceData(String vin) {     
        // --- PRE TREATMENT ----
        Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullRequest request = new Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullRequest();       
        Rforce_fullRepApvBserviceRenault.ServicePreferences servicePref = new Rforce_fullRepApvBserviceRenault.ServicePreferences();

        //servicePref.vin = VehController.getVin();
        servicePref.vin = vin;    
        //servicePref.codLanguage = label.CodLanguage; //servicePref.codLanguage = 'FRA'; 
        servicePref.codLanguage = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFull MaintWS = new Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFull();
        MaintWS.endpoint_x = System.label.VFP05_MaintenanceURL;
        MaintWS.clientCertName_x = System.label.RenaultCertificate;     
        MaintWS.timeout_x=40000;
        
        Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse Maint_WS = new Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse();    
    
        if (Test==true) {
            Maint_WS = Rforce_Utils_Stubs.MaintStub();     
        } else {
            Maint_WS = MaintWS.getApvGetDonPgmEntVinFull(request);
        }
       
       
        
         return Maint_WS;  
     
    }
}