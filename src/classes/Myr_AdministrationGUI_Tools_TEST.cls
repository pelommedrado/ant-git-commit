@isTest
private class Myr_AdministrationGUI_Tools_TEST { 

	@testsetup static void setCustomSettings() {
		CS04_MYR_Settings__c setting = new CS04_MYR_Settings__c();
		setting.Myr_Prefix_Unique__c = 'rhelios_';
		setting.Myr_Profile_Community_User__c = 'HeliosCommunity';
		setting.Myr_Personal_Account_Record_Type__c = 'CORE_ACC_Personal_Account_RecType';
		setting.Myr_Profile_Helios_ReadOnly__c = 'HeliosTechnique';
		setting.Myr_GUI_GroupingFields__c = '_GUI_:GUI';
		insert setting; 
	}

	static testmethod void testInit_OK() {
		CS04_MYR_Settings__c setting = [SELECT ID, Myr_GUI_GroupingFields__c FROM CS04_MYR_Settings__c];
		setting.Myr_GUI_GroupingFields__c = '_GUI_:GUI';
		update setting;
		Myr_AdministrationGUI_Tools GUITools = new Myr_AdministrationGUI_Tools();
		List<String> systemFields = GUITools.getSystemFields();
		system.assertEquals( systemFields.size() + 1, GUITools.Groups.size() );
		system.assertEquals( true, GUITools.Groups.containsKey('_GUI_') );
	}

	static testmethod void testInit_BadCouple() {
		CS04_MYR_Settings__c setting = [SELECT ID, Myr_GUI_GroupingFields__c FROM CS04_MYR_Settings__c];
		setting.Myr_GUI_GroupingFields__c = '_GUI_-GUI';
		update setting;
		Myr_AdministrationGUI_Tools GUITools = new Myr_AdministrationGUI_Tools();
		system.assertEquals( false, GUITools.Groups.containsKey('GUI') );
	}

	static testmethod void testPrepareGroup() {
		CS04_MYR_Settings__c setting = [SELECT ID, Myr_GUI_GroupingFields__c FROM CS04_MYR_Settings__c];
		setting.Myr_GUI_GroupingFields__c = '_GUI_:GUI';
		update setting;
		Myr_AdministrationGUI_Tools GUITools = new Myr_AdministrationGUI_Tools();
		Map<String, List<String>> groups = GUITools.buildGroups(new List<String>{'myr_gui__c','myr_other','createddate'});
		system.assertEquals( true, groups.containsKey(system.Label.Myr_GUI_SystemFields) );
		system.assertEquals( true, groups.containsKey(system.Label.Myr_GUI_OtherFields) );
		system.assertEquals( true, groups.containsKey('GUI') );
	}
}