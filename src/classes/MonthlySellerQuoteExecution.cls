public class MonthlySellerQuoteExecution {
    
    private List<Monthly_Goal_Seller_Relation__c> mgsr = new List<Monthly_Goal_Seller_Relation__c>();
    private List<Monthly_Goal_Seller__c> goalSellers = new List<Monthly_Goal_Seller__c>();
    private List<Monthly_Goal_Seller__c> sellerToInsert = new List<Monthly_Goal_Seller__c>();
    private List<Monthly_Goal_Seller__c> sellerToUpdate = new List<Monthly_Goal_Seller__c>();
    private List<Monthly_Goal_Dealer__c> mgdExisting = new List<Monthly_Goal_Dealer__c>();
    private List<Account> dealerToInsert = new List<Account>();
    
    private Set<Id> sellersAvailable = new Set<Id>();
    
    private Set<Id> sellers = new Set<Id>();
    private Set<Id> dealers = new Set<Id>();
    
    private Set<Id> oppsId = new Set<Id>();
    private Set<Id> oldSellers = new Set<Id>();
    private Set<Id> oldDealers = new Set<Id>();
    private Set<Id> newDealers = new Set<Id>();
    private Set<Id> existingGoalDealers = new Set<Id>();
    private Set<Id> newGoalDealers = new Set<Id>();
    private Set<Id> oppOwners = new Set<Id>();
    
    private Map<Id, Id> newSellers = new Map<Id, Id>();
    private Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
    private Map<Id, Monthly_Goal_Seller__c> relMap = new Map<Id, Monthly_Goal_Seller__c>();
    
    public void createMonthlyGoalSellerRelation(List<Quote> newQuote) {
        //seta valores de vendedor e concessionária
        for(Quote q : newQuote) {
            if(q.Status == 'Billed') {
                oppsId.add(q.OpportunityId);
            }
        }
        
        if(oppsId.isEmpty()) {
            return;
        }
        
        List<Opportunity> opps = [
            SELECT Id, OwnerId, Dealer__c 
            FROM Opportunity WHERE Id IN : oppsId 
        ];

        System.debug('***OPPS: ' + opps);
        
        for(Opportunity opp : opps){
            oppOwners.add(opp.OwnerId);
        }
        
        //get users from permission set BR - SFA - Sellers
        List<PermissionSetAssignment> psets = [
            SELECT AssigneeId
            FROM PermissionSetAssignment
            WHERE PermissionSetId IN (SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller')
            AND AssigneeId IN :oppOwners
        ];

        System.debug('***psets: ' + psets);
        
        for(PermissionSetAssignment psa : psets){
            sellersAvailable.add(psa.AssigneeId);
        }

        System.debug('sellersAvailable: ' + sellersAvailable);
        
        for(Opportunity opp : opps) {
            System.debug('OPPs ' + opp);
            if(sellersAvailable.contains(opp.OwnerId)){
                sellers.add(opp.OwnerId);
                dealers.add(opp.Dealer__c);
                oppMap.put(opp.Id, opp);
            }
        }
        
        System.debug('sellers: ' + sellers);
        System.debug('dealers: ' + dealers);
        
        //busca registros de objetivo VENDEDOR existentes 
        goalSellers = [
            SELECT Id, Seller__c, Monthly_Goal_Dealer__r.Dealer__c
            FROM Monthly_Goal_Seller__c
            WHERE Monthly_Goal_Dealer__r.Monthly_Goal_Group__r.Month__c =: System.now().format('MMMMM')
            AND Monthly_Goal_Dealer__r.Monthly_Goal_Group__r.Year__c =: System.now().year()
            AND Seller__c IN : sellers
            AND Monthly_Goal_Dealer__r.Dealer__c IN : dealers
        ];
        
        System.debug('goalSellers: ' + goalSellers);
        
        //seta vendedores e concessionárias com registros EXISTENTES
        for(Monthly_Goal_Seller__c s : goalSellers) {
            oldSellers.add(s.Seller__c);
            oldDealers.add(s.Monthly_Goal_Dealer__r.Dealer__c);
        }
        
        System.debug('oldSellers: ' + oldSellers);
        System.debug('oldDealers: ' + oldDealers);
        
        //seta vendedores e concessionárias SEM REGISTRO DE OBJETIVO
        for(Opportunity opp : opps) {
            if(!oldSellers.contains(opp.OwnerId) && !oldDealers.contains(opp.Dealer__c)) {
                newSellers.put(opp.Dealer__c, opp.OwnerId);
                newDealers.add(opp.Dealer__c);
            }
        }
        
        System.debug('newSellers: ' + newSellers);
        System.debug('newDealers: ' + newDealers);
        
        //Busca objetivos de CONCESSIONÁRIA caso existam vendedores sem registros de objetivos
        if(!newSellers.isEmpty()) {
            
            mgdExisting = [
                SELECT Id, Dealer__c
                FROM Monthly_Goal_Dealer__c
                WHERE Dealer__c IN : newDealers
                AND Monthly_Goal_Group__r.Month__c =: System.now().format('MMMMM')
                AND Monthly_Goal_Group__r.Year__c =: System.now().year()
            ];
            
            System.debug('mgdExisting: ' + mgdExisting);
            
            for(Monthly_Goal_Dealer__c m : mgdExisting) {
                
                existingGoalDealers.add(m.Dealer__c);
                
                Monthly_Goal_Seller__c s = new Monthly_Goal_Seller__c();
                s.Seller__c = newSellers.get(m.Dealer__c);
                s.Monthly_Goal_Dealer__c = m.Id;
                sellerToInsert.add(s);
            }
            
            System.debug('sellerToInsert: ' + sellerToInsert);
            System.debug('existingGoalDealers: ' + existingGoalDealers);
            
            Savepoint sp1 = Database.setSavepoint();
            try {
                Database.insert(sellerToInsert);
                
            } catch(Exception e) {
                Database.rollback(sp1);
                System.debug('Ex: ' + e);
            }
            
            goalSellers.addAll(sellerToInsert);
            
            for(Id d : newDealers) {
                if(!existingGoalDealers.contains(d)){
                    newGoalDealers.add(d);
                }
            }
            
            System.debug('newGoalDealers: ' + newGoalDealers);
            
            //busca contas SEM registro
            if(!newGoalDealers.isEmpty() && newGoalDealers != null){
                
                dealerToInsert = [
                    SELECT Id, RecordTypeId, Dealer_Matrix__c
                    FROM Account
                    WHERE Id IN : newGoalDealers
                ];

                System.debug('***dealerToInsert: ' + dealerToInsert);
                
                //cria registros de OBJETIVO DEALER
                MonthlyGoalDealer_Controller.createFromAcc(dealerToInsert);
            }
            
        }
        
        //APÓS INSERIR REGISTROS FALTANTES, INSERE REGISTROS DO OBJETO DE RELACIONAMENTO
        sellerToUpdate = [
            SELECT Id, Seller__c, Monthly_Goal_Dealer__r.Dealer__c
            FROM Monthly_Goal_Seller__c
            WHERE Monthly_Goal_Dealer__r.Monthly_Goal_Group__r.Month__c =: System.now().format('MMMMM')
            AND Monthly_Goal_Dealer__r.Monthly_Goal_Group__r.Year__c =: System.now().year()
            AND Seller__c IN : sellers
            AND Monthly_Goal_Dealer__r.Dealer__c IN : dealers
        ];
        
        for(Monthly_Goal_Seller__c goals : sellerToUpdate){
            relMap.put(goals.Seller__c, goals);
        }
        
        System.debug('sellerToUpdate: ' + sellerToUpdate);
        
        for(Quote q : newQuote) {
            System.debug('Q>:' + q);
            Opportunity oppBilled = oppMap.get(q.OpportunityId);
            System.debug('PB>:' + oppBilled);
            Monthly_Goal_Seller__c s = oppBilled != null && relMap.get(oppBilled.OwnerId) != null ? relMap.get(oppBilled.OwnerId) : null;
            System.debug('Monthly_Goal_Seller__c : ' + s);
            if(s != null && sellers.contains(s.Seller__c)){
                Monthly_Goal_Seller_Relation__c mgs = createSellerRelation(s.Id, q.id);
                mgsr.add(mgs);
            }
        }
        
        System.debug('mgsr: ' + mgsr);
        
        Savepoint sp2 = Database.setSavepoint();
        try {
            Database.insert(mgsr);
            
        } catch(Exception e) {
            Database.rollback(sp2);
            System.debug('Ex: ' + e);
        }   
        
    }
    
    public Monthly_Goal_Seller_Relation__c createSellerRelation(Id sellerId, Id quoteId) {
        Monthly_Goal_Seller_Relation__c mgs = new Monthly_Goal_Seller_Relation__c();
        mgs.Monthly_Goal_Seller__c = sellerId;
        mgs.Quote__c = quoteId;
        return mgs;
    }
}