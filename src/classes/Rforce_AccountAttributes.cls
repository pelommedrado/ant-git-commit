/**
*    @Author PraneelPidikiti
*    @ModifiedBy RNTBCI(Ramamoorthy Dakshinamoorthy)
*    @Date 22-07-2015
*    @Description This class describes each and every attributes of the oAccount based on the oAccount recoedtype
*    Project Rforce
*/
public with sharing class Rforce_AccountAttributes {
    public String sToParse {get; set;}
    public Rforce_ArchivesXMLData xmlData {get; set;}
    public Rforce_Utils08_RCArchivage CasesDataSource;
    public Rforce_ArchivesProperties.ArchivesCases[] ArchCasesList {get; set;}
    public Rforce_ArchivesProperties.ArchivesCases caseDetails {get; set;}
    public Rforce_ArchivesProperties.ArchivesAttachments[] ArchAttachmentsList {get; set;}
    private Account oAcc;
    public String IdCase  {get; set;}
    public String sAttachmentURL {get; set;}
    String sBusinessName = '';
    String LastName  = '';
    String FirstName = '';
    String City = '';
    String ZipCode = '';
    String PhoneNumber = '';
    String BusinessCustomId = '';
    String PersonCustomId = '';
    String strCountryCode = '';
    String strLocalId = '';
    String strEmail = '';
    public Integer showDetail {
        get { if (showDetail == null) {
                showDetail = 0;
                return showDetail;
            } else return showDetail;
        }
        set { showDetail = value; }
    }
    public Integer showValue {
        get { if (showValue == null) {
                showValue = 0;
                return showValue;
            } else return showValue;
        }
        set { showValue = value; }
    }
    public Integer showError {
        get { if (showError == null) {
                showError = 0;
                return showError;
            } else return showError;
        }
        set { showError = value; }
    }
    public Rforce_AccountAttributes(ApexPages.StandardController stdController) {        
        CasesDataSource = new Rforce_Utils08_RCArchivage();
        Account tmp = (Account)stdController.getRecord();
        this.oAcc = [select id, Country__c,name, FirstName, LastName , ShippingPostalCode, BillingPostalCode, ShippingCity, BillingCity, HomePhone__c, PersMobPhone__c, ProfMobPhone__c, ProfLandLine__c, Phone, RecordTypeId, Bcs_Id__c, Bcs_Id_Dacia__c,PeopleId_CN__c,PersEmailAddress__c from Account where id = :tmp.id];
        if (oAcc.name == '') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,System.Label.Rforce_AccFieldEmpty));
        }
    }
/**
*    @Author PraneelPidikiti
*    @ModifiedBy RNTBCI(Ramamoorthy Dakshinamoorthy)
*    @Date 22-07-2015
*    @Description To set the details of the oAccount details based on the recordtype of the oAccount
*/    
    public void getArchives() {
        Rforce_RcArchivageEnhanced.caseDetailsList[] resultCaseList = null;       
        String RecordtypeName='';
        try{
          RecordtypeName =[Select Id,Name,DeveloperName from Recordtype WHERE SObjectType = 'Account' and Id=:oAcc.RecordTypeId].DeveloperName;
          if(oAcc.Country__c!=null){
          strCountryCode = Country_Info__c.getInstance(oAcc.Country__c).Country_Code_3L__c;
          }
        }catch(QueryException e){
         System.debug('Error'+e);
         }        
          if(RecordtypeName.equals('CORE_ACC_Personal_Account_RecType') && oAcc.Bcs_Id__c != null && oAcc.Bcs_Id__c != ''){
           PersonCustomId = oAcc.Bcs_Id__c;
           BusinessCustomId = '';
          }else if(RecordtypeName.equals('CORE_ACC_Personal_Account_RecType') && oAcc.Bcs_Id_Dacia__c != null && oAcc.Bcs_Id_Dacia__c != ''){
               PersonCustomId = oAcc.Bcs_Id_Dacia__c;
               BusinessCustomId = '';
          }else if(RecordtypeName.equals('CORE_ACC_Company_Account_RecType') && oAcc.Bcs_Id__c != null && oAcc.Bcs_Id__c != ''){
                 BusinessCustomId = oAcc.Bcs_Id__c;
                 PersonCustomId = '';
           }else if(RecordtypeName.equals('CORE_ACC_Company_Account_RecType') && oAcc.Bcs_Id_Dacia__c != null && oAcc.Bcs_Id_Dacia__c != ''){
                   BusinessCustomId = oAcc.Bcs_Id_Dacia__c;
                   PersonCustomId = '';
             }        
        if(RecordtypeName.equals('CORE_ACC_Personal_Account_RecType') && oAcc.Bcs_Id__c != null && oAcc.Bcs_Id__c != ''){
               PersonCustomId = oAcc.Bcs_Id__c;
               BusinessCustomId = '';
        }
        if(RecordtypeName.equals('CORE_ACC_Personal_Account_RecType') && oAcc.Bcs_Id_Dacia__c != null && oAcc.Bcs_Id_Dacia__c != ''){
               PersonCustomId = oAcc.Bcs_Id_Dacia__c;
               BusinessCustomId = '';
          }        
          if (RecordtypeName.equals('CORE_ACC_Company_Account_RecType') && oAcc.name != null && oAcc.name != '') {
            sBusinessName = oAcc.name;
        }        
        if(strCountryCode != null && strCountryCode !=''){
        }else{
        strCountryCode='';
        }            
        if (oAcc.LastName  != null && oAcc.LastName  != '') {
            LastName  = oAcc.LastName ;
        }
        if (oAcc.FirstName != null && oAcc.FirstName != '') {
            FirstName = oAcc.FirstName;
        }
        if (oAcc.ShippingPostalCode != null && oAcc.ShippingPostalCode != '') {
            ZipCode = oAcc.ShippingPostalCode;
        } else if (oAcc.BillingPostalCode != null && oAcc.BillingPostalCode != '') {
            ZipCode = oAcc.BillingPostalCode;
        }
        if (oAcc.ShippingCity != null && oAcc.ShippingCity != '') {
            City = oAcc.ShippingCity;
        } else if (oAcc.BillingCity != null && oAcc.BillingCity != '') {
            City = oAcc.BillingCity;
        }
        if (oAcc.Phone != null && oAcc.Phone != '') {
            PhoneNumber = oAcc.Phone;
        } else if (oAcc.ProfLandLine__c != null && oAcc.ProfLandLine__c != '') {
            PhoneNumber = oAcc.ProfLandLine__c;
        } else if (oAcc.ProfMobPhone__c != null && oAcc.ProfMobPhone__c != '') {
            PhoneNumber = oAcc.ProfMobPhone__c;
        } else if (oAcc.PersMobPhone__c != null && oAcc.PersMobPhone__c != '') {
            PhoneNumber = oAcc.PersMobPhone__c;
        } else if (oAcc.HomePhone__c != null && oAcc.HomePhone__c != '') {
            PhoneNumber = oAcc.HomePhone__c;
        }        
        if (PhoneNumber != null && PhoneNumber !='' && PhoneNumber.length() > 4){           
            if (PhoneNumber.startsWith('+')) { 
            PhoneNumber=PhoneNumber.SubString(4,PhoneNumber.length());
            }else if (PhoneNumber.startsWith('00')) {
              PhoneNumber=PhoneNumber.SubString(5,PhoneNumber.length());
            }            
        }        
        if (PersonCustomId !='' || BusinessCustomId !='') {
            PhoneNumber='';
        } 
        if(oAcc.PeopleId_CN__c != null && oAcc.PeopleId_CN__c != '') {
            strLocalId = oAcc.PeopleId_CN__c;
        }
        if(oAcc.PersEmailAddress__c != null && oAcc.PersEmailAddress__c != ''){
            strEmail = oAcc.PersEmailAddress__c;
        }
        try {
            if (sBusinessName == '' && LastName  == '' && FirstName == '' && City == '' && ZipCode == '' && PhoneNumber == '' && BusinessCustomId== '' && PersonCustomId== '') {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, System.Label.Rforce_ParamFilled));
            } else if (sBusinessName == '' && City == '' && ZipCode == '' && PhoneNumber == '' && BusinessCustomId== '' && PersonCustomId== '') {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, System.Label.Rforce_ParamFilled));
            } else if (sBusinessName != '' && City == '' && ZipCode == '' && PhoneNumber == '' && BusinessCustomId== '' && PersonCustomId== '') {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, System.Label.Rforce_ParamFilled));
            } else {
            resultCaseList = CasesDataSource.getCaseIdListAccountConcat(LastName , FirstName, ZipCode, City, PhoneNumber, sBusinessName,BusinessCustomId, PersonCustomId,strCountryCode,strLocalId,strEmail);
            }
            if (resultCaseList == null) {
                showDetail = 99;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, system.Label.Error_Rnull));
            } else {
                showDetail = 11;
                ArchCasesList = new Rforce_ArchivesProperties.ArchivesCases[] {};
                Rforce_ArchivesProperties.ArchivesCases Arch;
            for(Rforce_RcArchivageEnhanced.caseDetailsList myCaseId : resultCaseList) {              
                    Arch = new Rforce_ArchivesProperties.ArchivesCases();
                    Arch.IdCase  = myCaseId.caseId;
                    Arch.country = myCaseId.countryCode;
                    Arch.build = myCaseId.build;
                    Arch.lastName = myCaseId.lastName;
                    Arch.firstName = myCaseId.firstName;
                    Arch.serialNum = myCaseId.serialNum;
                    Arch.createdDate = myCaseId.createdDate;
                    System.debug('CaseIdretrieved for' + oAcc.Id + 'is >>>>>>>>>>>>>' + Arch.IdCase );
                    ArchCasesList.add(Arch);
                }
            }
        } catch (System.CalloutException e) {
            showDetail = 99;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, System.Label.Error_NoCaseFound));
            System.debug('Error message in the caselist method>>>>>>>>>>>' + e);
          }
    }
/**
*    @Author PraneelPidikiti
*    @ModifiedBy RNTBCI(Ramamoorthy Dakshinamoorthy)
*    @Date 22-07-2015
*    @Description To display the error message if the results retrieved is null
*    @Returntype null
*    Project Rforce
*/    
    public void getCaseDetails () {
        try {
            String IdCase  = ApexPages.currentPage().getParameters().get('IdCase');
            String country = ApexPages.currentPage().getParameters().get('country');
            String Output = CasesDataSource.getXmlDetails(country, IdCase );
            if (Output == null) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, system.Label.Error_Rnull));
            }else {
                showDetail = 12;
                Rforce_ArchivageXMLParseEngine parsingEngine = new Rforce_ArchivageXMLParseEngine ();
                xmlData = parsingEngine.getXMLData(Output);
             }
        } catch (CalloutException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, system.Label.ERR_ARC_CASEDETAILS));
        }
    }
/**
*    @Author PraneelPidikiti
*    @ModifiedBy RNTBCI(Ramamoorthy Dakshinamoorthy)
*    @Date 22-07-2015
*    @Description To display the error message if the attachment retrieved is null
*    @Returntype null
*    Project Rforce
*/    
    public void getAttachements() {
        try {
            IdCase  = ApexPages.currentPage().getParameters().get('IdCase ');
            String country = ApexPages.currentPage().getParameters().get('country');
            Rforce_RcArchivage.attachmentDetailsList[] resultAttachment = CasesDataSource.getAttachmentDetailsList(country, IdCase );
            if (resultAttachment == null) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, system.Label.Error_Rnull));
            } else {
                showDetail = 13;
                ArchAttachmentsList = new Rforce_ArchivesProperties.ArchivesAttachments[] {};
                Rforce_ArchivesProperties.ArchivesAttachments Arch;
                for (Rforce_RcArchivage.attachmentDetailsList myAttList : resultAttachment) {
                    Arch = new Rforce_ArchivesProperties.ArchivesAttachments();
                    Arch.attachmentName = myAttList.attachmentName;
                    Arch.extension = myAttList.extension;
                    Arch.createdDate = myAttList.createdDate;
                    Arch.Idcase=IdCase ;
                    Arch.country=country;
                    ArchAttachmentsList.add(Arch);
                }
            }
        }catch (CalloutException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error, system.Label.ERR_ARC_NOATTACHMENT));
         }
    }
/**
*    @Author PraneelPidikiti
*    @ModifiedBy RNTBCI(Ramamoorthy Dakshinamoorthy)
*    @Date 22-07-2015
*    @Description To get the details of the attachment  based on the URL
*    @ReturnType null
*/    
  public void getAttachementData(){   
    try{
        IdCase  = ApexPages.currentPage().getParameters().get('IdCase');
        String country = ApexPages.currentPage().getParameters().get('country');         
        String attachmentName = System.currentPagereference().getParameters().get('attachmentName');
        String strContentType=System.currentPagereference().getParameters().get('contentType');
        Blob BlobDec;
        String resultURL = CasesDataSource.getAttachmentContent(country, IdCase , attachmentName);
        String stUrlUTF8 = EncodingUtil.urlEncode(resultURL, 'UTF-8');
        BlobDec = Blob.valueOf(stUrlUTF8);
        String BlobDec1=EncodingUtil.base64Encode(BlobDec);
        BlobDec = EncodingUtil.base64Decode(resultURL);
        if (resultURL == null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.Error_Rnull));
         }else {
               showDetail = 14;     
               sAttachmentURL=resultURL;
               Document currDoc= new Document(); 
               currDoc.AuthorId = UserInfo.getUserId();                   
               currDoc.Name = attachmentName;                 
               currDoc.FolderId = UserInfo.getUserId();
               currDoc.Body=BlobDec;
               currDoc.Type = strContentType;
               insert currDoc;                  
               sAttachmentURL=currDoc.Id;                  
             }
          } catch (CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,system.Label.ERR_ARC_NOATTACHMENT));        
             }  
    }    
    public Void getCaseDetailXML() {
    }
    public PageReference OpenXMLPage() {
        return Page.Rforce_OpenXMLAttachmentAccount;
    }
    public String getAccount() {
        if (Test.isRunningTest()) {
            return '1 TEMPS DARET';
        }
        return oAcc.name;
    }
}