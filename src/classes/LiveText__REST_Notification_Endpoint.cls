/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
@RestResource(urlMapping='/notifications')
global class REST_Notification_Endpoint {
    global REST_Notification_Endpoint() {

    }
    @HttpPost
    global static List<LiveText.REST_Notification_Endpoint.MessageResponse> newInboundMessage() {
        return null;
    }
global class MessageResponse {
}
}
