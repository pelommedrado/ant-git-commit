public class DistrinetCallServico {
    public static final String endPoint         = Label.endpointDistrinet;
    public static final String clientCertficate = Label.clientCertNamex;
    
    public static final Id personalAccount = 
        VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
    
    public static final Id companyAccount = 
        VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Company_Acc');
        
    public static void call(String quoteId){
        
        List<QuoteLineItem> quoteItemList = [
            SELECT Id, Vehicle__c, Vehicle__r.Name, Model_AOC__r.IsNewRelease__c,
            Model_AOC__r.Semiclair__c, Model_AOC__r.TypeVehicle__c, Model_AOC__r.Name, Version_AOC__r.Semiclair__c, 
            Color_AOC__c, Upholstery_AOC__c, Harmony_AOC__c, Optionals_AOC__c, Model_AOC__c, Version_AOC__c
            FROM QuoteLineItem
            WHERE QuoteId =: quoteId
        ];
        
        DistrinetCallServico.call(quoteId, 
                                  quoteItemList[0].Color_AOC__c,
                                  quoteItemList[0].Upholstery_AOC__c,
                                  quoteItemList[0].Harmony_AOC__c,
                                  quoteItemList[0].Model_AOC__r.Name,
                                  quoteItemList[0].Model_AOC__c,
                                  quoteItemList[0].Version_AOC__c,
                                  quoteItemList[0].Optionals_AOC__c);
        
    }
    
    @Future(callout=true)
    public static void call(String orderId, String color, String upho, String harmo, String modelName, String modelId, String versionId, String optionalSemiclairs) {
        
        QuoteLineItem quoteLineItem = instanceQuoteLineItem(orderId, color, upho, harmo, modelName, modelId, versionId, optionalSemiclairs);
        
        String xml = gerarBody(orderId, quoteLineItem);
        
        if(String.isEmpty(xml)) {
            System.debug('Nao foi possivel gerar o xml');
            return;
        }
        
        System.debug('***XML:' + xml);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setMethod('POST');
        req.setClientCertificateName(clientCertficate);
        req.setBody(xml.trim());
        
        try {
            Http http = new Http();
            HTTPResponse res = http.send(req);
            
            System.debug(res);
            System.debug('***RESPONSE:\n\n' + res.getBody().replaceAll('><' , '>\n<') + '\n\n');
            
            //if(res.getStatus().equalsIgnoreCase('OK') && res.getStatusCode() == 200) {
            // Logic to parse the response
            //}
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
            return;
        }
    }
    
    public static QuoteLineItem instanceQuoteLineItem(Id quoteId, String color, String upho, String harmo, String modelName, String modelId, String versionId, String optionalsIds){
        
        Lead lead = new Lead(SemiclairColor__c=color,
                          SemiclairUpholstered__c=upho,
                          SemiclairHarmony__c=harmo,
                          SemiclairOptional__c=optionalsIds);
        
        
        Model__c model = new Model__c(Id=modelId, Name=modelName);
        PVVersion__c version = new PVVersion__c(Id=versionId);
        
        String[] modelos = model.Name.split(' ');
        
        Product2 produto = [
            SELECT id, Name, Version__c, Model__r.Name, ModelSpecCode__c
            FROM Product2
            WHERE Name IN: modelos LIMIT 1
        ];
        
        List<String> optionalSemiclairs = new List<String>
            { lead.SemiclairColor__c, lead.SemiclairUpholstered__c, lead.SemiclairHarmony__c };

        if(lead.SemiclairOptional__c != null) {
            List<String> items = lead.SemiclairOptional__c.split(' ');
            for(String it: items) {
                optionalSemiclairs.add(it);
            }
        }
        
        List<Optional__c> opList = [
            SELECT Id, Name, Optional_Code__c, Amount__c, Type__c
            FROM Optional__c
            WHERE Optional_Code__c IN: optionalSemiclairs
            AND Version__c =: version.Id
        ];

        System.debug('***opList: '+opList);
        
        List<String> optionalCodes = new List<String>();
        List<String> optionalNames = new List<String>();
        String colorName, upholsteryName, harmonyName, colorCode, upholsteryCode, harmonyCode;
        
        for(Optional__c o : opList) {
            if(o.Type__c != 'Cor' && o.Type__c != 'Trim' && o.Type__c != 'Harmonia'){
                optionalNames.add(o.Name);
                optionalCodes.add(o.Optional_Code__c);
            }
            if(o.Type__c == 'Cor'){
                colorName = o.Name;
                colorCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Trim'){
                upholsteryName = o.Name;
                upholsteryCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Harmonia'){
                harmonyName = o.Name;
                harmonyCode = o.Optional_Code__c;
            }
        }
        
        PricebookEntry sObjPricebookEntry = VFC131_LeadBO.getInstance().getPricebookEntry(produto.Id);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quoteId;
        quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 0;
        
        quoteLineItem.Model_AOC__c      = model.Id;
        quoteLineItem.Version_AOC__c    = version.Id;
        quoteLineItem.Color_AOC__c      = colorCode;
        quoteLineItem.ColorName__c      = colorName;
        quoteLineItem.Upholstery_AOC__c = upholsteryCode;
        quoteLineItem.UpholsteryName__c = upholsteryName;
        quoteLineItem.Harmony_AOC__c    = harmonyCode;
        quoteLineItem.HarmonyName__c    = harmonyName;
        quoteLineItem.Optionals_AOC__c  = String.join(optionalCodes, ' ');
        quoteLineItem.OptionalsNames__c = String.join(optionalNames, ' ');

        System.debug('***quoteLineItem: '+quoteLineItem);
        
        return quoteLineItem;
        
    }
    
    public static String gerarBody(String orderId, QuoteLineItem qli) {
        
        List<Quote> quoteList = [
            SELECT Id, DeliveryDate__c, QuoteNumber, External_Id__c, 
            BillingAccount__r.RecordTypeId, BillingAccount__r.ProfEmailAddress__c, BillingAccount__r.Phone,
            BillingAccount__c, BillingAccount__r.Name, BillingAccount__r.CustomerIdentificationNbr__c,
            BillingAccount__r.LastName, BillingAccount__r.FirstName, BillingAccount__r.Shipping_Complement__c,
            BillingAccount__r.PersonBirthdate, BillingAccount__r.PersonEmail, BillingAccount__r.Fax,
            BillingAccount__r.PersLandline__c, BillingAccount__r.PersMobPhone__c, BillingAccount__r.RgStateTexto__c,
            BillingAccount__r.ProfLandline__c, Opportunity.Owner.Contact.CPF__c, BillingAccount__r.Shipping_Number__c,
            BillingAccount__r.ShippingStreet, BillingAccount__r.ShippingPostalCode,
            BillingAccount__r.ShippingState, BillingAccount__r.ShippingCity, BillingAccount__r.Sex__c,
            Opportunity.Dealer__r.IdBir__c, Opportunity.Dealer__r.Dealer_Matrix_ICB__c
            
            FROM Quote
            WHERE Id =: orderId
        ];
        
        System.debug('quoteList: ' + quoteList);
        
        if(quoteList.isEmpty()) {
            return null;
        }
        
        Quote quote = quoteList.get(0);
        
        return creadBody(quote, qli);
    }
    
    public static String creadBody(Quote quote, QuoteLineItem item) {
        
        StaticResource distWsCreate = [
            select Body
            from StaticResource
            where Name = 'dist_ws_create'
        ];
        
        String body = /*xmlModel;*/distWsCreate.Body.toString(); 
        //body = body.replaceAll('\r?\n|\r', '');
        if(quote.BillingAccount__r.RecordTypeId == personalAccount) {
            body = blindValue(body, '\\{razaosocial\\}', '');
            body = blindValue(body, '\\{tipoCliente\\}', '1');
            
            body = blindValue(body, '\\{email\\}',      quote.BillingAccount__r.PersonEmail);
            body = blindPhone(body, '\\{comercial\\}',  quote.BillingAccount__r.ProfLandline__c);
            
        } else if(quote.BillingAccount__r.RecordTypeId == companyAccount) {
            body = blindValue(body, '\\{razaosocial\\}',    quote.BillingAccount__r.Name);
            body = blindValue(body, '\\{tipoCliente\\}',    '2');
        
            body = blindValue(body, '\\{email\\}',     quote.BillingAccount__r.ProfEmailAddress__c);
            body = blindPhone(body, '\\{comercial\\}', quote.BillingAccount__r.Phone);
        }
           
        body = blindBir(body, '\\{account\\}', quote.Opportunity.Dealer__r.Dealer_Matrix_ICB__c);

        if(quote.Opportunity.Dealer__r.IdBir__c == quote.Opportunity.Dealer__r.Dealer_Matrix_ICB__c){
            body = blindBir(body, '\\{filial\\}', '');
        }else{
            body = blindBir(body, '\\{filial\\}', quote.Opportunity.Dealer__r.IdBir__c); 
        } 
        
        body = blindValue(body, '\\{quoteExtId\\}', quote.External_Id__c);
        //body = blindValue(body, '\\{cpfVendedor\\}', '');
        body = blindValue(body, '\\{complemento\\}', quote.BillingAccount__r.Shipping_Complement__c );
        body = blindValue(body, '\\{numero\\}',     quote.BillingAccount__r.Shipping_Number__c);
        body = blindValue(body, '\\{bairro\\}',     quote.BillingAccount__r.ShippingCity);
        
        body = blindValue(body, '\\{firstName\\}',  quote.BillingAccount__r.FirstName);
        body = blindValue(body, '\\{lastName\\}',   quote.BillingAccount__r.LastName);        
        body = blindValue(body, '\\{cpfcnpj\\}',    quote.BillingAccount__r.CustomerIdentificationNbr__c);
        body = blindValue(body, '\\{dateOfBirth\\}',quote.BillingAccount__r.PersonBirthdate);
        body = blindValue(body, '\\{rua\\}',        quote.BillingAccount__r.ShippingStreet);
        body = blindValue(body, '\\{cep\\}',        quote.BillingAccount__r.ShippingPostalCode);
        body = blindValue(body, '\\{estado\\}',     quote.BillingAccount__r.ShippingState);
        body = blindPhone(body, '\\{celular\\}',    quote.BillingAccount__r.PersMobPhone__c);
        body = blindPhone(body, '\\{telefone\\}',   quote.BillingAccount__r.PersLandline__c);
        body = blindPhone(body, '\\{fax\\}',        quote.BillingAccount__r.Fax);
        body = blindValue(body, '\\{rg\\}',         quote.BillingAccount__r.RgStateTexto__c);
        body = blindValue(body, '\\{sexo\\}',       quote.BillingAccount__r.Sex__c);
        body = blindValue(body, '\\{agreedDeliveryDate\\}', quote.DeliveryDate__c);
       
        /*List<QuoteLineItem> quoteItemList = [
            SELECT Id, Vehicle__c, Vehicle__r.Name, Model_AOC__r.IsNewRelease__c,
            Model_AOC__r.Semiclair__c, Model_AOC__r.TypeVehicle__c, Version_AOC__r.Semiclair__c, 
            Color_AOC__c, Upholstery_AOC__c, Harmony_AOC__c, Optionals_AOC__c, Version_AOC__c
            FROM QuoteLineItem
            WHERE QuoteId =: quote.Id
        ];
        
        if(quoteItemList.isEmpty()) {
            return null; 
        }
        
        
        QuoteLineItem item = quoteItemList.get(0);*/
        
        Model__c modelAOC = [SELECT Id, IsNewRelease__c, Semiclair__c, TypeVehicle__c FROM Model__c WHERE Id =: item.Model_AOC__c ];
        PVVersion__c versionAOC = new PVVersion__c();
        
        if(!String.isEmpty(item.Version_AOC__c)){
            versionAOC = [SELECT Id, Semiclair__c FROM PVVersion__c WHERE Id =: item.Version_AOC__c];
        }

        System.debug('***ITEM: '+item);
        
        if(!String.isEmpty(item.Vehicle__c)) {

            body = blindValue(body, '\\{kind\\}', '');
            body = blindValue(body, '\\{version\\}', '');
            body = blindValue(body, '\\{options\\}', '');
            body = blindValue(body, '\\{model\\}', '');
            body = blindValue(body, '\\{VIN\\}', item.Vehicle__r.Name);
            body = blindValue(body, '\\{accessories\\}', '');
            body = blindValue(body, '\\{colour\\}', '');
            body = blindValue(body, '\\{trim\\}', '');
            body = blindValue(body, '\\{trimColour\\}', '');

        } else if(modelAOC.IsNewRelease__c && String.isEmpty(item.Version_AOC__c)) {
        
            body = blindValue(body, '\\{kind\\}', modelAOC.TypeVehicle__c);
            body = blindValue(body, '\\{version\\}', 'XXXXX');
            body = blindValue(body, '\\{options\\}', 'XXXXX');
            body = blindValue(body, '\\{model\\}', modelAOC.Semiclair__c);
            body = blindValue(body, '\\{VIN\\}', '');
            body = blindValue(body, '\\{accessories\\}', '');
            body = blindValue(body, '\\{colour\\}', 'XXXXX');
            body = blindValue(body, '\\{trim\\}', 'XXXXXX');
            body = blindValue(body, '\\{trimColour\\}', 'XXXXX');
            
        } else{
        
            // Separa os ids dos opcionais do campo de "Opcionais" separados por ";"
            List<String> opList = new List<String>();
            if(!String.isEmpty(item.Optionals_AOC__c)) {
                opList = item.Optionals_AOC__c.split(' ');
            }

            // adiciona os ids de cor, estofado e harmonia
            opList.add(item.Color_AOC__c);
            opList.add(item.Upholstery_AOC__c);
            opList.add(item.Harmony_AOC__c);
            
            // recupera os codigos referente aos opcionais
            List<Optional__c> optionalList = [
                SELECT Id, Type__c, Optional_Code__c
                FROM Optional__c
                WHERE Optional_Code__c IN: opList
                AND Version__c =: item.Version_AOC__c
            ];

            System.debug('***optionalList: '+optionalList);
            
            // monta a lista de codigos de opcionais
            List<String> optionalCodeList = new List<String>();
            Set<String> optionalTypesList = new Set<String>{'Cor','Trim','Harmonia','Pintura'};
            for(Optional__c opt : optionalList) {
                if(!optionalTypesList.contains(opt.Type__c))
                    optionalCodeList.add(opt.Optional_Code__c);
            }
            
            for(Optional__c o: optionalList) {
                if(o.Type__c == 'Cor') {
                    body = blindValue(body, '\\{colour\\}', o.Optional_Code__c);
                }
                if(o.Type__c == 'Trim') {
                    body = blindValue(body, '\\{trim\\}', o.Optional_Code__c);
                }
                if(o.Type__c == 'Harmonia') {
                    body = blindValue(body, '\\{trimColour\\}', o.Optional_Code__c);
                }
            }
            
            body = blindValue(body, '\\{kind\\}', modelAOC.TypeVehicle__c);
            body = blindValue(body, '\\{version\\}', versionAOC.Semiclair__c);
            body = blindValue(body, '\\{options\\}', String.join(optionalCodeList, ' '));
            body = blindValue(body, '\\{model\\}', modelAOC.Semiclair__c);
            body = blindValue(body, '\\{VIN\\}', '');
            body = blindValue(body, '\\{accessories\\}', '');

        }
        
        if(String.isNotEmpty(quote.Opportunity.Owner.Contact.CPF__c)){
            body = blindValue(body, '\\{cpfVendedor\\}', quote.Opportunity.Owner.Contact.CPF__c);
        
        } else{
            body = blindValue(body, '\\{cpfVendedor\\}', '43586725825');
        }
        
        return body;
    }

    private static String blindPhone(String body, String key, String phone){
        String valuePhone = '';
        if(!String.isEmpty(phone) && phone.length() == 8){
            valuePhone = '      ' + phone;
        }else if(!String.isEmpty(phone) && phone.length() == 9){
            valuePhone = '     ' + phone;
        }else if(!String.isEmpty(phone) && phone.length() == 10){
            valuePhone = phone.substring(0,2) + '    ' + phone.substring(2);
        }else if(!String.isEmpty(phone) && phone.length() == 11){
            valuePhone = phone.substring(0,2) + '   ' + phone.substring(2);
        }
        return blindValue(body, key, valuePhone);
    }
    
    private static String blindBir(String body, String key, String bir) {
        String valueBir = '';
        if(!String.isEmpty(bir) && bir.length() > 4) {
            valueBir = '0' + bir.substring(bir.length()-5, bir.length());   
        }
        
        return blindValue(body, key, valueBir);
    }
    
    private static String blindValue(String body, String key, Date data) {
        String dataSt = '';
        
        if(data != null) {
            Datetime datat = 
                Datetime.newInstance(data.Year(), data.month(), data.day());
            
            dataSt = datat.format('ddMMYYYY');
        }
        
        return blindValue(body, key, dataSt);
    }
    
    private static String blindValue(String body, String key, String value) {
        if(String.isEmpty(value)) {
            value = '';
        }
        return body.replaceAll(key, value);
    }
}