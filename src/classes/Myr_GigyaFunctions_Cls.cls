/** Apex Class containing the main functions to call Gigya
	The following functions are available yet
	- resetPassword
	- resendActivationMail
	
	@author S. Ducamp
	@date 28.09.2015

	@version 1.0 : resetPassword + resendActivationMail 
	@version 16.11 (SDP / 21.09.2016): introduce forceLoginId function
**/

public with sharing class Myr_GigyaFunctions_Cls {

	private Myr_GigyaCaller_Cls GigyaCaller;
	private Myr_GigyaParser_Cls.GigyaAccountParser gigyaAccountResult;
	private CS04_MYR_Settings__c myrSettings;
	private Account sfdcAccount;
	private User sfdcUser;
	private GigyaLogType TypeLog = GigyaLogType.ASYNCHRONOUS;
	private GigyaMode mMode = GigyaMode.NORMAL;
	private Myr_GigyaCaller_Cls.ENVIRONMENT mEnv;

	/** Information on current user **/
	private Boolean AuthLevel_1;
	private Boolean AuthLevel_2;
	private Boolean AuthLevel_3;
		
	public enum GigyaFunction {
		Myr_Gigya_ResetPassword,
		Myr_Gigya_ResendActivationMail,
		Myr_Gigya_ViewData,
		Myr_Gigya_ChangeEmail,
		Myr_Gigya_DeleteAccounts,
		Myr_Gigya_ForceNewLoginId
	}

	public enum GigyaLogType {
		SYNCHRONOUS, ASYNCHRONOUS
	}

	//Either normal mode (by default), either no verification email mode: should be used only by changeEmail webservice 
	public enum GigyaMode {
		NORMAL, NOVERIFICATIONEMAIL
	}

	/* @return Boolean, true if the user is authorized to use reset password and resend activation email **/
	public Boolean getAuthLevel1() {
		return AuthLevel_1;
	}

	/* @return Boolean, true if the user is authorized to use change email **/
	public Boolean getAuthLevel2() {
		return AuthLevel_2;
	}
	
	/* @return Boolean, true if the user is authorized to use the delete functions **/
	public Boolean getAuthLevel3() {
		return AuthLevel_3;
	}
	
	/** @constructor
		@throw MYR_Gigya_Caller_Exception 
	*/
	public Myr_GigyaFunctions_Cls() {
		 innerConstructor();		
	}

	/** @constructor **/
	public Myr_GigyaFunctions_Cls(GigyaLogType typeLog) {
		this.TypeLog = typeLog;
		innerConstructor();
	}

	/** @constructor **/
	public Myr_GigyaFunctions_Cls(GigyaLogType typeLog, GigyaMode iMode) {
		this.TypeLog = typeLog;
		this.mMode = iMode;
		innerConstructor();
	} 

	/** common initializations for constructors **/
	private void innerConstructor() {
		system.debug('### Myr_GigyaFunctions_Cls - <innerConstructor> - BEGIN');
		myrSettings = CS04_MYR_Settings__c.getInstance();
		//try to find the authorization for the current user
		CS04_MYRAuthorization_Settings__c profAuth = CS04_MYRAuthorization_Settings__c.getInstance(UserInfo.getProfileId());
		CS04_MYRAuthorization_Settings__c userAuth = CS04_MYRAuthorization_Settings__c.getInstance(UserInfo.getUserId());
		AuthLevel_3 = profAuth.Gigya_Level3__c || userAuth.Gigya_Level3__c;
		//if the user is authorized on the level 3 then he is authorized on the level2
		AuthLevel_2 = profAuth.Gigya_Level2__c || userAuth.Gigya_Level2__c; 
		//if the user is authorized on the level 2 or 3 then he is authorized on the level1
		AuthLevel_1 = profAuth.Gigya_Level1__c || userAuth.Gigya_Level1__c || AuthLevel_2 || AuthLevel_3;

		//Set the environment to use
		if( GigyaMode.NORMAL == mMode ) {
			if( [select IsSandbox from Organization].IsSandbox ) {
				mEnv = Myr_GigyaCaller_Cls.ENVIRONMENT.NOPROD;
			} else {
				mEnv = Myr_GigyaCaller_Cls.ENVIRONMENT.PROD;
			}
		} else if (GigyaMode.NOVERIFICATIONEMAIL == mMode) {
			if( [select IsSandbox from Organization].IsSandbox ) {
				mEnv = Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_NOPROD;
			} else {
				mEnv = Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_PROD;
			}
		}
		system.debug('### Myr_GigyaFunctions_Cls - <innerConstructor> - mEnv=' + mEnv.name() );
	}
	
	/** Call Gigya to retrieve the token for the given brand and the given country
		@return the error message if encountered, null otherwise	
	**/ 
	public String getGigyaToken(String country, String brand) {
		try  {
			GigyaCaller = new Myr_GigyaCaller_Cls(country, brand, mEnv);
			if( String.isBlank(GigyaCaller.getToken()) ) {
				return 'Connection to Gigya failed (no access token)';
			}
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}
	
	/** @return Myr_GigyaParser_Cls.GigyaAccountParser, the Gigya account found 
				using searchAccount function, null otherwise
		This function is usable only after at least one searchAccount function .... **/
	public Myr_GigyaParser_Cls.GigyaAccountParser getGigyaData() {
		return gigyaAccountResult;
	}

	private enum GIGYA_SEARCH_MODE {
		FULL, //return all the accounts found to display them in the GUI
		ONE //return ok only of we found a single account
	}
	
	/** Search the account with the given accountSfdcId
		@return MYR_Gigya_Functions_Response in case of prolems, null otherwise
	**/
	private Myr_GigyaFunctions_Response searchAccount(String accountSfdcId, String brand, GIGYA_SEARCH_MODE mode) {
		//Check the account format
		if( String.isBlank(accountSfdcId) || (!String.isBlank(accountSfdcId) && accountSfdcId.length() != 15 && accountSfdcId.length() != 18 ) ) {
			return new Myr_GigyaFunctions_Response(system.Label.GIGYA_501, 'GIGYA_501_Msg', system.Label.GIGYA_501_Msg);
		}
		//Check the brand
		if( String.isBlank(brand) || (!String.isBlank(brand) && !brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name()) && !brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name())) ) {
			return new Myr_GigyaFunctions_Response(system.Label.GIGYA_511, 'GIGYA_511_Msg', system.Label.GIGYA_511_Msg.replace('{0}', ((brand==null)?'<null>':brand)));
		} 
		//Search for the account 
		List<Account> listAccount = [SELECT Id, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c, Country__c, MyR_Status__c, MyD_Status__c FROM Account WHERE Id=:accountSfdcId];
		if( listAccount.size() != 1 ) {
			return new Myr_GigyaFunctions_Response(system.Label.GIGYA_502, 'GIGYA_502_Msg', system.Label.GIGYA_502_Msg);
		}
		sfdcAccount = listAccount[0];
		system.debug('### Myr_GigyaFunctions_Cls - <searchAccount> - account found in SFDC=' + sfdcAccount);
		//Prepare GIGYA 
		String errorMsg = getGigyaToken(sfdcAccount.Country__c, brand);			 
		if( !String.isBlank(errorMsg) ) {
			return new Myr_GigyaFunctions_Response(system.Label.GIGYA_599, 'GIGYA_599_Msg', system.Label.GIGYA_599_Msg + ' ' + errorMsg);
		}
		//Search for the account within Gigya
		gigyaAccountResult = null; 
		try {
			gigyaAccountResult = GigyaCaller.searchAccount(accountSfdcId);
			//do not test null object, as it will be catched then throwed ...
			if( gigyaAccountResult.errorCode != '0') {
				return new Myr_GigyaFunctions_Response(system.Label.GIGYA_599, 'GIGYA_599_Msg', 
					system.Label.GIGYA_599_Msg + ' code=' + gigyaAccountResult.errorCode + ' ' + gigyaAccountResult.errorMessage + '(' + gigyaAccountResult.errorDetails + ')');
			} else if ( gigyaAccountResult.results == null ) {
				return new Myr_GigyaFunctions_Response(system.Label.GIGYA_505, 'GIGYA_505_Msg', system.Label.GIGYA_505_Msg.replace('{0}', accountSfdcId));
			} else if (gigyaAccountResult.results != null ) {
				if( (GIGYA_SEARCH_MODE.ONE == mode && gigyaAccountResult.results.size() != 1)
					|| (GIGYA_SEARCH_MODE.FULL == mode && gigyaAccountResult.results.size() == 0) ) {
						return new Myr_GigyaFunctions_Response(system.Label.GIGYA_505, 'GIGYA_505_Msg', system.Label.GIGYA_505_Msg.replace('{0}', accountSfdcId));
				}
			}
		} catch (Exception e) {
			return new Myr_GigyaFunctions_Response(system.Label.GIGYA_599, 'GIGYA_599_Msg', system.Label.GIGYA_599_Msg + ' ' + e.getMessage());
		}
		return null;
	}
	
	/** Reset the Gigya password of the given Personal Account
		@param Id, the salesforce id of the personal account 
		@return MYR_Gigya_Functions_Response, the response 
	**/
	public Myr_GigyaFunctions_Response resetPassword(String accountSfdcId, String brand) {
		//first check authorization
		if( !AuthLevel_1 ) {
			Myr_GigyaFunctions_Response response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_590, 'GIGYA_590_Msg', system.Label.GIGYA_590_Msg);			
			return logAndExit( response, accountSfdcId );
		}
		//Search the Account
		Myr_GigyaFunctions_Response response = searchAccount(accountSfdcId, brand, GIGYA_SEARCH_MODE.ONE);
		if( response != null ) {
			//oups ! problem !
			response.gigFunction = GigyaFunction.Myr_Gigya_ResetPassword;
			return logAndExit( response, accountSfdcId );
		}
		//Check the email address
		//HQREQ-04191: reset password should be based on MyRenault Id or MyDaciaId depending on the brand given in parameter
		String emailBrand = null;
		if( Myr_MyRenaultTools.checkRenaultBrand(brand) ) {
			emailBrand = sfdcAccount.MyRenaultID__c;
		} else if (Myr_MyRenaultTools.checkDaciaBrand(brand) ) {
			emailBrand = sfdcAccount.MyDaciaID__c;
		}
		if( String.isBlank(emailBrand) ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_503, 'GIGYA_503_Msg', system.Label.GIGYA_503_Msg);
			return logAndExit( response, accountSfdcId );
		}
		//Check that the SalesForce values are coherent
		List<User> listUsers = [SELECT Id, Username FROM User WHERE AccountId = :accountSfdcId];
		if( listUsers.size() == 1 ) {
			sfdcUser = listUsers[0];
			//check that the values are coherent
			String emailAddress = myrSettings.Myr_Prefix_Unique__c + emailBrand;
			if( ! emailAddress.toLowerCase().equalsIgnoreCase(sfdcUser.Username.toLowerCase()) ) {
				response = new Myr_GigyaFunctions_Response(
							GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_509, 'GIGYA_509_Msg', 
							system.Label.GIGYA_509_Msg.replace('{0}', emailBrand).replace('{1}', sfdcUser.Username));
				return logAndExit( response, accountSfdcId );
			}
		}
		//Check that the email is included into Gigya verified logins
		if( !gigyaAccountResult.results[0].loginIDs.emails.contains(emailBrand) ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_510, 'GIGYA_510_Msg', system.Label.GIGYA_510_Msg);
			return logAndExit( response, accountSfdcId );
		} 
		//Launch the reset password
		try {
			Myr_GigyaParser_Cls.GigyaAccountParser resetResp = GigyaCaller.resetPassword(emailBrand);
			if( resetResp.errorCode != '0') {
				response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_599, 'GIGYA_599_Msg', 
							system.Label.GIGYA_599_Msg + ' code=' + resetResp.errorCode + ' ' + resetResp.errorMessage + '(' + resetResp.errorDetails + ')');
				return logAndExit( response, accountSfdcId );
			}
		} catch (Exception e) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_599, 'GIGYA_599_Msg', system.Label.GIGYA_599_Msg + ' ' + e.getMessage()); 
			return logAndExit( response, accountSfdcId );
		}
		
		response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResetPassword, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
		return logAndExit( response, accountSfdcId );
	}
	
	/** Resend the activation mail of the given Personal Account
		@param Id, the salesforce id of the personal account 
		@return MYR_Gigya_Functions_Response, the response 
	**/
	public Myr_GigyaFunctions_Response resendActivationEmail(String accountSfdcId, String brand) {
		//first check authorization
		if( !AuthLevel_1 ) {
			Myr_GigyaFunctions_Response response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_590, 'GIGYA_590_Msg', system.Label.GIGYA_590_Msg);			
			return logAndExit( response, accountSfdcId );
		}
		//Search the Account
		Myr_GigyaFunctions_Response response = searchAccount(accountSfdcId, brand, GIGYA_SEARCH_MODE.ONE);
		if( response != null ) {
			//oups ! problem !
			response.gigFunction = GigyaFunction.Myr_Gigya_ResendActivationMail;
			return logAndExit( response, accountSfdcId );
		}
		//Get the email
		String email = null;
		if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase( brand) ) {
			email = sfdcAccount.MyRenaultID__c;
		} else if ( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase( brand ) ) {
			email = sfdcAccount.MyDaciaID__c;
		}

		//Check the email address depending on the given brand
		if( String.isBlank(email) ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_503, 'GIGYA_503_Msg', system.Label.GIGYA_503_Msg);
			return logAndExit( response, accountSfdcId );
		}
		//Check if we have an Helios community User whatever the status
		List<User> listUsers = [SELECT Id, Username FROM User WHERE AccountId = :accountSfdcId];
		if( listUsers.size() == 1 ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_504, 'GIGYA_504_Msg', system.Label.GIGYA_504_Msg.replace('{0}', listUsers[0].Id).replace('{1}', accountSfdcId));
			return logAndExit( response, accountSfdcId );
		}
		//Check if the email is present in the Gigya verified logins
		try {
			if( !gigyaAccountResult.results[0].emails.unverified.contains(email) ) {
				//the mail is not in the unverified emails, check if it is in the verified emails to precise the error message
				if( gigyaAccountResult.results[0].emails.verified.contains(email) ) {
					response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_506, 'GIGYA_506_Msg', system.Label.GIGYA_506_Msg.replace('{0}', email));
					return logAndExit( response, accountSfdcId );
				} else {
					response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_507, 'GIGYA_507_Msg', system.Label.GIGYA_507_Msg.replace('{0}', email));
					return logAndExit( response, accountSfdcId );
				}
			}
			//Ok, we can try to execute again the activation email
			Myr_GigyaParser_Cls.GigyaAccountParser resendResp = GigyaCaller.resendActivationMail(gigyaAccountResult.results[0].UID, email); 
			if( resendResp.errorCode != '0') {
				response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_599, 'GIGYA_599_Msg', 
							system.Label.GIGYA_599_Msg + ' code=' + resendResp.errorCode + ' ' + resendResp.errorMessage + '(' + resendResp.errorDetails + ')');
				return logAndExit( response, accountSfdcId );
			}
		} catch (Exception e) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_599, 'GIGYA_599_Msg', system.Label.GIGYA_599_Msg + ' ' + e.getMessage()); 
			return logAndExit( response, accountSfdcId );
		}
		
		response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ResendActivationMail, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
		return logAndExit( response, accountSfdcId );
	}
	
	/** View Gigya Data for the given Personal Account
		After this call the function getGigyaData could be called to retrieve the Gigya data
		@param Id, the salesforce id of the personal account
		@param String, the brand of the given account
		@return MYR_Gigya_Functions_Response, the response
	**/
	public Myr_GigyaFunctions_Response viewData(String accountSfdcId, String brand) {
		//Search the Account
		Myr_GigyaFunctions_Response response = searchAccount(accountSfdcId, brand, GIGYA_SEARCH_MODE.FULL);
		if( response == null ) {
			//searchAccount was OK, so we initialize a OK response otherwise it is already initialized
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ViewData, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
			//return logAndExit( response, accountSfdcId );
			return response;
		}
		response.gigFunction = GigyaFunction.Myr_Gigya_ViewData;
		//return logAndExit( response, accountSfdcId );
		return response;
	}

	/** Change Gigya Email for the given account and the given brand
		@param Id, the salesforce id of the personal account
		@param String, the brand of the given account
		@param String, the newEmail of the given account
		@return MYR_Gigya_Functions_Response, the response

		CAUTION: do not change the MyRenault_Id__c, MyDacia_Id__c or PErsEmailAddress___c fields as these
		changes will be done by the Salesforce changeEmail triggered by Gigya
	**/
	public Myr_GigyaFunctions_Response changeEmail(String accountSfdcId, String brand, String newEmail) {
		Myr_GigyaFunctions_Response response = null;
		//first check authorization
		if( !AuthLevel_2 ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ChangeEmail, system.Label.GIGYA_590, 'GIGYA_590_Msg', system.Label.GIGYA_590_Msg);			
			return logAndExit( response, accountSfdcId );
		}
		//Check the email entered
		if( !Myr_MyRenaultTools.isEmailValid(newEmail) ) {
			response = new Myr_GigyaFunctions_Response( 
				GigyaFunction.Myr_Gigya_ChangeEmail, 
				system.Label.GIGYA_512, 
				'GIGYA_512_Msg', 
				String.format(system.Label.GIGYA_512_Msg, new List<String>{newEmail}));
			return logAndExit( response, accountSfdcId );
		}
		
		//Search the Account
		response = searchAccount(accountSfdcId, brand, GIGYA_SEARCH_MODE.ONE);
		if( response != null ) {
			//oups ! problem !
			response.gigFunction = GigyaFunction.Myr_Gigya_ChangeEmail;
			return logAndExit( response, accountSfdcId );
		}
		response = new Myr_GigyaFunctions_Response( GigyaFunction.Myr_Gigya_ChangeEmail, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);

		//Check the status of the Account
		Boolean myrActivated = Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(brand) 
			&& system.Label.Myr_Status_Activated.equalsIgnoreCase( sfdcAccount.MyR_Status__c );
		Boolean mydActivated = Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(brand) 
			&& system.Label.Myd_Status_Activated.equalsIgnoreCase( sfdcAccount.MyD_Status__c );
		if( !(myrActivated || mydActivated) ) {
			response.code = system.Label.GIGYA_519;
			response.label = 'GIGYA_519_Msg'; 
			response.msg = system.Label.GIGYA_519_Msg;
			return logAndExit( response, accountSfdcId );
		}

		try {
			//Check that newEmail is not already used by another activated or created account
			Myr_ManageAccount_MatchingKeys_Cls matcher = new Myr_ManageAccount_MatchingKeys_Cls(brand, newEmail, accountSfdcId);
			Myr_ManageAccount_MatchingKeys_Cls.Response respMatcher = matcher.checkEmailAvailability();
			if( !respMAtcher.isOk() ) {
				response.code = system.Label.GIGYA_518;
				response.label = 'GIGYA_518_Msg'; 
				response.msg = String.format(system.Label.GIGYA_518_Msg, new List<String>{newEmail});
				return logAndExit( response, accountSfdcId );
			}

			//Check that newEmail is not already in verified logins of Gigya
			if( gigyaAccountResult.results[0].loginIDs.emails.contains(newEmail) ) {
				response.code = system.Label.GIGYA_516;
				response.label = 'GIGYA_516_Msg'; 
				response.msg = String.format( system.Label.GIGYA_516_Msg, new List<String>{newEmail} );
				return logAndExit( response, accountSfdcId );
			}

			//Check that a changeEmail is not in progress on Gigya side
			String gigNewEmail = gigyaAccountResult.results[0].data.newEmail;
			if( !String.isBlank(gigNewEmail) ) {
				response.code = system.Label.GIGYA_517;
				response.label = 'GIGYA_517_Msg'; 
				response.msg = String.format( system.Label.GIGYA_517_Msg, new List<String>{gigNewEmail} );
				return logAndExit( response, accountSfdcId );
			}
		
			Myr_GigyaParser_Cls.GigyaAccountParser changeGigMail = GigyaCaller.changeGigyaEmail(gigyaAccountResult.results[0].UID, newEmail); 
			if( changeGigMail.errorCode != '0') {
				response = new Myr_GigyaFunctions_Response(
							GigyaFunction.Myr_Gigya_ChangeEmail, 
							system.Label.GIGYA_599, 'GIGYA_599_Msg', 
							system.Label.GIGYA_599_Msg + ' code=' + changeGigMail.errorCode + ' ' + changeGigMail.errorMessage + '(' + changeGigMail.errorDetails + ')');
				return logAndExit( response, accountSfdcId );
			}
		} catch (Exception e) {
			system.debug('### Myr_GigyaFunctions_Cls - <changeEmail> - exception:' +  e.getMessage() + ' stack=' + e.getStackTraceString() );
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ChangeEmail, system.Label.GIGYA_599, 'GIGYA_599_Msg', system.Label.GIGYA_599_Msg + ' ' + e.getMessage()); 
			return logAndExit( response, accountSfdcId );
		}

		response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_ChangeEmail, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
		return logAndExit( response, accountSfdcId );
	}
	  
	/** Delete All the Gigya accounts linked to the given salesforce id and the given brand
		@param Id, the salesforce id of the personal account
		@param String, the brand of the given account
		@return List<Myr_GigyaFunctions_Response>, return the list of response 
		(one for each account deleted or if we have a general problem, only one response)

		CAUTION: This treatment will trigger the Myr_UserDeactivation_WS !
		06.06.06 : New authorization level created (AuthLevel_3)	
	**/
	public Myr_GigyaFunctions_MultipResponse deleteAccounts(String accountSfdcId, String brand) {
		Myr_GigyaFunctions_MultipResponse response = null;
		//first check authorization
		if( !AuthLevel_3 ) {
			response = new Myr_GigyaFunctions_MultipResponse(GigyaFunction.Myr_Gigya_DeleteAccounts, system.Label.GIGYA_590, 'GIGYA_590_Msg', system.Label.GIGYA_590_Msg);			
			return logAndExit( response, accountSfdcId );
		}
		//Search the Accounts
		Myr_GigyaFunctions_Response schResponse = searchAccount(accountSfdcId, brand, GIGYA_SEARCH_MODE.FULL);
		if( schResponse != null ) {
		  //oups ! problem !
		  response = new Myr_GigyaFunctions_MultipResponse( GigyaFunction.Myr_Gigya_DeleteAccounts, schResponse.code, schResponse.label, schResponse.msg );
		  return logAndExit( response, accountSfdcId );
		}
		Myr_GigyaParser_Cls.GigyaAccountParser listGigyaAccounts = getGigyaData();
		//Check the number of Gigya accounts to delete
		Integer maxAcc = Integer.valueOf(CS_Gigya_Settings__c.getInstance().Max_Account_Deletion__c);
		if( listGigyaAccounts.results.size() > maxAcc ) {
			String msg = String.format( 
				system.Label.GIGYA_520_Msg,
				new List<String>{ String.valueOf(listGigyaAccounts.results.size()), 
								  String.valueOf(maxAcc) 
				}
			);
			response = new Myr_GigyaFunctions_MultipResponse( GigyaFunction.Myr_Gigya_DeleteAccounts, system.Label.GIGYA_520, 'GIGYA_520_Msg', msg );
			return logAndExit( response, accountSfdcId );
		}

		//Ok, we can try to execute the gigya accounts deletion
		response = new Myr_GigyaFunctions_MultipResponse( GigyaFunction.Myr_Gigya_DeleteAccounts, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
		for( Myr_GigyaParser_Cls.GigyaAccounts gigAcc : listGigyaAccounts.results ) {
			try {
				Myr_GigyaParser_Cls.GigyaAccountParser deleteAcc = GigyaCaller.deleteGigyaAccounts(gigAcc.uid); 
				Myr_GigyaFunctions_Response resp = null;
				if( deleteAcc.errorCode != '0') {
					resp = new Myr_GigyaFunctions_Response(
						GigyaFunction.Myr_Gigya_DeleteAccounts, 
						system.Label.GIGYA_599, 'GIGYA_599_Msg', 
						system.Label.GIGYA_599_Msg + ' code=' + deleteAcc.errorCode + ' ' + deleteAcc.errorMessage + '(' + deleteAcc.errorDetails + ')' );
					//modify the global response
					response.code = resp.code;
					response.label = resp.label;
					response.msg = resp.msg;
				} else {
					resp = new Myr_GigyaFunctions_Response(
						GigyaFunction.Myr_Gigya_DeleteAccounts, 
						system.Label.GIGYA_000, 'GIGYA_000_Msg', 
						system.Label.GIGYA_000_Msg);
				}
				response.addResponse( resp );
			} catch (Exception e) {
				Myr_GigyaFunctions_Response resp = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_DeleteAccounts, 
					system.Label.GIGYA_599, 'GIGYA_599_Msg', 
					system.Label.GIGYA_599_Msg + ' ' + e.getMessage() );
				response.addResponse( resp );
				//modify the global response
				response.code = resp.code;
				response.label = resp.label;
				response.msg = resp.msg;
			}
		}

		return logAndExit( response, accountSfdcId );
	}

		/** Delete One Gigya account linked to the given salesforce id and the given brand
		@param Id, the salesforce id of the personal account
		@param String, the brand of the given account
		@param String UID, the Gigya UID to be deleted
		@return <Myr_GigyaFunctions_Response>, return the response 

		CAUTION: This treatment will trigger the Myr_UserDeactivation_WS !
		06.06.06 : New authorization level created (AuthLevel_3)	
	**/
	public Myr_GigyaFunctions_Response deleteOneAccount(String accountSfdcId, String brand, String uid) {
		Myr_GigyaFunctions_Response response = null;

		//first check authorization
		if( !AuthLevel_3 ) {
			response = new Myr_GigyaFunctions_Response(GigyaFunction.Myr_Gigya_DeleteAccounts, system.Label.GIGYA_590, 'GIGYA_590_Msg', system.Label.GIGYA_590_Msg);			
			return logAndExit( response, accountSfdcId );
		}	

		//Ok, we can try to execute the deletion of one Gigya account
		response = new Myr_GigyaFunctions_Response( GigyaFunction.Myr_Gigya_DeleteAccounts, system.Label.GIGYA_000, 'GIGYA_000_Msg', system.Label.GIGYA_000_Msg);
		try {
			Myr_GigyaParser_Cls.GigyaAccountParser deleteAcc = GigyaCaller.deleteGigyaAccounts(uid);
			if( deleteAcc.errorCode != '0') {
				response = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_DeleteAccounts, 
					system.Label.GIGYA_599, 'GIGYA_599_Msg', 
					system.Label.GIGYA_599_Msg + ' code=' + deleteAcc.errorCode + ' ' + deleteAcc.errorMessage + '(' + deleteAcc.errorDetails + ')' );
			} else {
				response = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_DeleteAccounts, 
					system.Label.GIGYA_000, 'GIGYA_000_Msg', 
					system.Label.GIGYA_000_Msg);
			}
			
		} catch (Exception e) {
			response = new Myr_GigyaFunctions_Response(
				GigyaFunction.Myr_Gigya_DeleteAccounts, 
				system.Label.GIGYA_599, 'GIGYA_599_Msg', 
				system.Label.GIGYA_599_Msg + ' ' + e.getMessage() );	
		}

		return logAndExit( response, accountSfdcId );
	}

	/** Function used to force a new Login ID on Gigya side without sending an activation email to the customer
		This function should be called only in the context of a changeEmail and SHOULD NOT BE EXPOSED
		within Gigya function
	**/
	public Myr_GigyaFunctions_Response forceNewLoginId(String uid, String accId, String newLoginId, String oldLoginId) {
		system.debug(' ### Myr_GigyaFunctions_Cls - <forceNewLoginId> - uid='+uid+', newLoginID='+newLoginId+', oldLoginId='+oldLoginId+', mEnv' + mEnv.name());
		Myr_GigyaFunctions_Response response = null;
		if( Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_NOPROD != mEnv && Myr_GigyaCaller_Cls.ENVIRONMENT.NOVERIFEMAIL_PROD != mEnv ) {
			response = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_ForceNewLoginId, 
					system.Label.GIGYA_530, 'GIGYA_530_Msg', system.Label.GIGYA_530_Msg);	
			return response;
		}
		try {
			Myr_GigyaParser_Cls.GigyaAccountParser forceResp = GigyaCaller.forceGigyaEmail(uid, accId, newLoginId, oldLoginId);
			if( forceResp.errorCode != '0') {
				response = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_ForceNewLoginId, 
					system.Label.GIGYA_599, 'GIGYA_599_Msg', 
					system.Label.GIGYA_599_Msg + ' code=' + forceResp.errorCode + ' ' + forceResp.errorMessage + '(' + forceResp.errorDetails + ')' );
			} else {
				response = new Myr_GigyaFunctions_Response(
					GigyaFunction.Myr_Gigya_ForceNewLoginId, 
					system.Label.GIGYA_000, 'GIGYA_000_Msg', 
					system.Label.GIGYA_000_Msg);
			}
		} catch (Exception e) {
			response = new Myr_GigyaFunctions_Response(
				GigyaFunction.Myr_Gigya_ForceNewLoginId, 
				system.Label.GIGYA_599, 'GIGYA_599_Msg', 
				system.Label.GIGYA_599_Msg + ' ' + e.getMessage() );	
		}
		//do not log in this case
		return response;
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ----- LOGGER SYSTEM

	//SDP / 26.02.2016: cannot use interface for logAndExit because Rforce is also using
	//this section of code :-(
	/* Insert an information in the logger and exit from the current function **/
	public Myr_GigyaFunctions_Response logAndExit(Myr_GigyaFunctions_Response response, String accountId) {
		addLog( response, accountId );
		return response;
	}
	/* Insert an information in the logger and exit from the current function **/
	public Myr_GigyaFunctions_MultipResponse logAndExit(Myr_GigyaFunctions_MultipResponse response, String accountId) {
		Myr_GigyaFunctions_Response simpleResponse = new Myr_GigyaFunctions_Response(
			response.gigFunction, response.code, response.label, response.msg
		);
		addLog( simpleResponse, accountId );
		return response;
	}

	@future
	public static void insertASyncLog(String function, String accId, String code, String message, Boolean isOk) {
		system.debug('### Myr_GigyaFunctions_Cls - <addLog> - Asynchronous log - function='+function+', message='+message);
		INDUS_Logger_CLS.addLog(
			INDUS_Logger_CLS.ProjectName.MYR //project
			, String.valueOf(Myr_GigyaFunctions_Cls.class)//ApexClass
			, function//Function
			, accId
			, code //ExceptType
			, message
			, (isOK?INDUS_Logger_CLS.ErrorLevel.Info:INDUS_Logger_CLS.ErrorLevel.Warn) //ErrorLevel
		);
	}

	/* Add a Log **/
	public void addLog(Myr_GigyaFunctions_Response response, String accountId) {
		Profile profile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
		String message = 'User=' + UserInfo.getUserId() 
			+ ', Username=' + UserInfo.getUserName() 
			+ ', Profile=' + profile.Name
			+ ', Message=' + response.msg;
		if( TypeLog == GigyaLogType.SYNCHRONOUS ) {
			system.debug('### Myr_GigyaFunctions_Cls - <addLog> - synchronous log');
			INDUS_Logger_CLS.addLog(
				INDUS_Logger_CLS.ProjectName.MYR //project
				, String.valueOf(Myr_GigyaFunctions_Cls.class)//ApexClass
				, response.gigFunction.name()//Function
				, accountId
				, response.code //ExceptType
				, message
				, (response.isOK()?INDUS_Logger_CLS.ErrorLevel.Info:INDUS_Logger_CLS.ErrorLevel.Warn) //ErrorLevel
			);
		} else {
			insertASyncLog( response.gigFunction.name(), accountId, response.code, message, response.isOK() );
		}
	}


	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ----- INNER STRUCTURE

	//Inner class to return the response : code + label
	public virtual class Myr_GigyaFunctions_Response {

		public GigyaFunction gigFunction {get; private set;}
		public String code {get; private set;}
		public String label {get; private set;}
		public String msg {get; private set;}
		
		//inner @constructor
		public Myr_GigyaFunctions_Response(GigyaFunction gigFunction, String code, String label, String msg) {
			this.gigFunction = gigFunction;
			this.code = code;
			this.label = label;
			this.msg = msg;
		}
		//inner @constructor
		public Myr_GigyaFunctions_Response(String code, String label, String msg) {
			this.code = code;
			this.label = label;
			this.msg = msg;
		}
		//@return Boolean, ture if the feedback from Gigya is ok
		public virtual Boolean isOK() {
			return (!String.isBlank(code) && code.endsWith('000'));
		}
		//@return Boolean, ture if the feedback from Gigya is ok
		public Boolean getIsOK() {
			return isOK();
		}
		//@return String, the name of the function used
		public String getFunctionName() {
			return gigFunction.name();
		}
	}

	//inner class to use in case of multiple treatments (delete case)
	public class Myr_GigyaFunctions_MultipResponse extends Myr_GigyaFunctions_Response {
		
		public List<Myr_GigyaFunctions_Response> listResponses;

		//@inner constructor
		public Myr_GigyaFunctions_MultipResponse(GigyaFunction gigFunction, String code, String label, String msg) {
			super( gigFunction, code, label, msg );
			listResponses = new List<Myr_GigyaFunctions_Response>();
		}

		//@inner constructor
		public Myr_GigyaFunctions_MultipResponse(String code, String label, String msg) {
			super( code, label, msg );
			listResponses = new List<Myr_GigyaFunctions_Response>();
		}

		//add a specific response for the current deletion
		public void addResponse( Myr_GigyaFunctions_Response response ) {
			system.debug('#### addResponse response=' + response);
			listResponses.add( response );
		}

		//check if all the responses are OK
		public override Boolean isOK() {
			if( listResponses.size() > 0 ) {
				for( Myr_GigyaFunctions_Response innerResponse : listResponses ) {
					if( !innerResponse.isOk() ) {
						return false;
					}
				}
			} 
			return super.isOk();
		}
	}
	
}