@isTest
private class VFC56_TestDriveBusinessDelegateTest {
    
    public static Account dealer;
    public static Account cliente;
    public static VEH_Veh__c veiculo;
    public static Opportunity opp;
    public static TDV_TestDriveVehicle__c veiculoTestDrive;
    public static TDV_TestDrive__c testDrive;
    public static Product2 prd;
    
    public static void criaObjetos(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        dealer = moc.criaAccountDealer();
		Insert dealer;

		cliente = moc.criaPersAccount();
		cliente.VehicleInterest_BR__c = 'SANDERO';
		Insert cliente;

		veiculo = moc.criaVeiculo();
		veiculo.Model__c = 'SANDERO';
		Insert veiculo;

		opp = moc.criaOpportunity();
		opp.Dealer__c = dealer.Id;
		opp.AccountId = cliente.Id;
		Insert opp;

		veiculoTestDrive = new TDV_TestDriveVehicle__c(); //moc.criaTestDriveVeiculo();
		veiculoTestDrive.Account__c = dealer.Id;
		veiculoTestDrive.Available__c = true;
		veiculoTestDrive.Vehicle__c = veiculo.Id;
		Insert veiculoTestDrive;

		testDrive = new TDV_TestDrive__c(); //moc.criaTestDrive();
		testDrive.Opportunity__c = opp.Id;
		testDrive.TestDriveVehicle__c = veiculoTestDrive.Id;
		testDrive.DateBooking__c = System.today().addDays(2);
		Insert testDrive;

		prd = moc.criaProduct2();
		prd.Name = 'SANDERO';
		prd.RecordTypeId = Utils.getRecordTypeId('Product2','PDT_Model');
		Insert prd;

	}
	
	@isTest 
	static void testMethodGetTestDriveByOpportunity() {

		Test.startTest();

        criaObjetos();
        testDrive.Status__c = 'Open';
        Update testDrive;

		VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().getTestDriveByOpportunity(opp.Id);

		// verifica se esta retornando um Test Drive
		System.assertEquals(testDriveVO.id,testDrive.Id);

		Test.stopTest();

	}

	@isTest 
	static void testMethodGetVehiclesAvailable() {

		Test.startTest();
        
        criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;

		/*cria ou obtém um test drive existente para essa oportunidade*/
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opp.Id);
		
		/*obtém a lista de modelos*/
		List<Product2> lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
        
        VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
        testDriveVO.id = testDrive.Id;
        testDriveVO.dealerId = dealer.Id;
        testDriveVO.vehicleInterest = cliente.VehicleInterest_BR__c;

		//VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().mapToValueObject(sObjTestDrive, lstSObjModel);

		VFC54_TestDriveVO testDriveVO2 = VFC56_TestDriveBusinessDelegate.getInstance().getVehiclesAvailable(testDriveVO);

		String veiculoModel = [SELECT Model__c FROM VEH_Veh__c].Model__c;

		// verifica se esta retornando veiculos disponiveis
		System.assertEquals(veiculoModel, testDriveVO2.model);
        
		Test.stopTest();

	}

	@isTest 
	static void testMethodGetVehicleDetails() {

		Test.startTest();

		criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;
        
        VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
        testDriveVO.testDriveVehicleId = veiculoTestDrive.Id;

		/*cria ou obtém um test drive existente para essa oportunidade*/
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opp.Id);
		
		/*obtém a lista de modelos*/
		List<Product2> lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');

		//VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().mapToValueObject(sObjTestDrive, lstSObjModel);

		testDriveVO.dateBookingNavigation = System.today().addDays(2);

		VFC54_TestDriveVO testDriveVO2 = VFC56_TestDriveBusinessDelegate.getInstance().getVehicleDetails(testDriveVO);

		List<VEH_Veh__c> veiculos = [SELECT Model__c, Version__c, Color__c, VehicleRegistrNbr__c FROM VEH_Veh__c];

		// verifica se esta retornando detalhes dos veiculos disponiveis
		System.assertEquals(testDriveVO2.model, veiculos[0].Model__c);
		System.assertEquals(testDriveVO2.version, veiculos[0].Version__c );
		System.assertEquals(testDriveVO2.color, veiculos[0].Color__c );
		System.assertEquals(testDriveVO2.plaque, veiculos[0].VehicleRegistrNbr__c );

		Test.stopTest();

	}
    /*

	@isTest 
	static void testMethodUpdateTestDriveAndCreateNew() {

		Test.startTest();

		criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;
        
        VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
        testDriveVO.id = testDrive.Id;
        testDriveVO.dateBookingNavigation = System.today().addDays(5);
        
        System.debug('***testDriveVO.dateBookingNavigation: '+testDriveVO.dateBookingNavigation);

		/*cria ou obtém um test drive existente para essa oportunidade
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opp.Id);
		
		/*obtém a lista de modelos
		List<Product2> lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');

		//VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().mapToValueObject(sObjTestDrive, lstSObjModel);

		VFC54_TestDriveVO testDriveVO2 = VFC56_TestDriveBusinessDelegate.getInstance().updateTestDriveAndCreateNew(testDriveVO);

		/*  
			verifica se criou outro testDrive e compara os ids - tem que ser diferentes para provar que temos 2 test Drives,
			ou seja foi criado outro a partir do primeiro passado como parametro
		
		System.assertNotEquals(testDriveVO2.id, testDriveVO.id );
		System.assertEquals(testDriveVO2.model, testDriveVO.model);

		Test.stopTest();

	}
	*/

	@isTest 
	static void testMethodCancelOpportunity() {

		Test.startTest();

		criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;

		/*cria ou obtém um test drive existente para essa oportunidade*/
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opp.Id);
		
		/*obtém a lista de modelos*/
		List<Product2> lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
        
        VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
        testDriveVO.opportunityId = opp.Id;

		//VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().mapToValueObject(sObjTestDrive, lstSObjModel);

		testDriveVO.dateBookingNavigation = System.today().addDays(2);

		VFC56_TestDriveBusinessDelegate.getInstance().cancelOpportunity(testDriveVO);

		// verifica os status da oporunidade foi cancelado
		String oppStageName = [SELECT Id, StageName FROM Opportunity].StageName;
		System.assertEquals('Lost', oppStageName);

		Test.stopTest();

	}

	@isTest 
	static void testMethodUpdateOpportunityForQuoteStage() {

		Test.startTest();

		criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;

		VFC56_TestDriveBusinessDelegate.getInstance().updateOpportunityForQuoteStage(opp.Id);

		// verifica os status da oporunidade foi para orçamento "Quote"
		String oppStageName = [SELECT Id, StageName FROM Opportunity].StageName;
		System.assertEquals( 'Quote', oppStageName);

		Test.stopTest();

	}

	@isTest 
	static void testMethodCancelTestDriveAndUpdateOpportunityForQuoteStage() {

		Test.startTest();

		criaObjetos();
        testDrive.Status__c = 'Scheduled';
        Update testDrive;

		/*cria ou obtém um test drive existente para essa oportunidade*/
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opp.Id);
		
		/*obtém a lista de modelos*/
		List<Product2> lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
        
        VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
        testDriveVO.opportunityId = opp.Id;
        testDriveVO.id = testDrive.Id;
        testDriveVO.reasonCancellation = 'reasonCancellation';

		//VFC54_TestDriveVO testDriveVO = VFC56_TestDriveBusinessDelegate.getInstance().mapToValueObject(sObjTestDrive, lstSObjModel);

		testDriveVO.dateBookingNavigation = System.today().addDays(2);

		System.debug('*** testDriveVO: '+testDriveVO);

		VFC56_TestDriveBusinessDelegate.getInstance().cancelTestDriveAndUpdateOpportunityForQuoteStage(testDriveVO);

		String testDriveStatus = [SELECT Status__c FROM TDV_TestDrive__c].Status__c;
		String oppStageName = [SELECT StageName FROM Opportunity].StageName;

		// verifica se o Test Drive foi cancelado e a oportunidade atualizada para Quote
		system.assertEquals( 'Not Performed', testDriveStatus);
		system.assertEquals( 'Quote', oppStageName);

		Test.stopTest();

	}
    
    static testMethod void testCreateNewTestDrive(){
        
        Test.startTest();
        
        criaObjetos();

		VFC56_TestDriveBusinessDelegate.getInstance().createNewTestDrive(opp.Id);
        
        Test.stopTest();
    }
	
}