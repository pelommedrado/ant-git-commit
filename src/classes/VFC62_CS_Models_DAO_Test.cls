@isTest 
private class VFC62_CS_Models_DAO_Test
{
	static testMethod void unitTest1()
	{
		// Prepare test data
		CS_Models__c csmodel1 = new CS_Models__c(Name ='XXX', Document_URL__c = 'http://renault.com');
		insert csmodel1;

		// Start test
        Test.startTest();

        // Test 01
        List<Id> test01 = VFC62_CS_Models_DAO.getInstance().fetch_All_Models();

        // Stop test
        Test.stopTest();

        // Validate data
        system.assert(test01.size() > 0, 'Error in test01');
	}
	
	static testMethod void unitTest2()
	{
		// Start test
        Test.startTest();

        // Test 01
        List<Id> test01 = VFC62_CS_Models_DAO.getInstance().fetch_All_Models();

        // Stop test
        Test.stopTest();

        // Validate data
        system.assert(test01 == null, 'Error in test01');
	}
}