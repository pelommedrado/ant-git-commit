/**
* @author: Vetrivel Sundararajan
* @date: 07/08/2014
* @description: This apex class is a helper class which will be used to create Account,Vehicle and Vehicle Relations. 
* @createMDMAccount
* @ MDM Evolution ID : 39
* @ Description : Even if the vehicle is already existing in Rforce,Vehicle Relation should be created for  
*   all the available Vehicles.
* @author: Vetrivel.s
* date: 1/12/2014 
*/
public class Rforce_MDMCreateAccountUtils_CLS {

 public Rforce_MDMCreateAccountUtils_CLS() {}
 
 /**
 * @author Vetrivel Sundararajan
 * @date 07/08/2014
 * @description This method will iterate the mdmxmlDataList and insert the values in Accout, Vehicle and VehicleRelation Objects.
*/

 public String createMDMAccount(Rforce_MDMPartyOverview_CLS mdmPartyVehicleObject) {
 
 system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount() -->');
 
 Id PERSONAL_ID = [select Id from RecordType where sObjectType='Account' and DeveloperName='CORE_ACC_Personal_Account_RecType' limit 1].Id;
 Id COMPANY_ID  = [select Id from RecordType where sObjectType='Account' and DeveloperName='CORE_ACC_Company_Account_RecType' limit 1].Id;        
 
 Map<String,Id> allvinmap= new Map<String,Id>();
 
 Account acct   = new Account();
 VEH_Veh__c     vehicle; 
 VRE_VehRel__c  vehRelation;
 
 String strAcctAddress1='';
 String strAcctAddress2='';
 String strAcctAddress3='';
 String strAcctAddress4='';

 // retrieve the vehcile data from mdmxmlDataList
  
  List<Rforce_MDMPartyOverview_CLS.Vehicle> vehicleDataList=new List<Rforce_MDMPartyOverview_CLS.Vehicle>();
  Rforce_MDMPartyOverview_CLS.Vehicle vehicleData=new Rforce_MDMPartyOverview_CLS.Vehicle();
  
  //Check if the account already exists
  List<Account> accs = new List<Account>();
  
  system.debug('## Party ID is..::'+ mdmPartyVehicleObject.strPartyID);
  system.debug('## Account ID is..::'+mdmPartyVehicleObject.strAccoutType);
   String mdmPartyID ='';
   if (mdmPartyVehicleObject.strPartyID!=null && mdmPartyVehicleObject.strPartyID!=''){
       mdmPartyID ='MDM-'+mdmPartyVehicleObject.strPartyID;
   } 
   
  if(mdmPartyVehicleObject.strAccoutType=='Personal'){
       accs = [Select Id, Lastname, Firstname, PersEmailAddress__c from Account where Tech_ACCExternalID__c = :mdmPartyID  LIMIT 1]; 
  }
   else{
        accs = [Select Id, Name from Account where Tech_ACCExternalID__c = :mdmPartyID  LIMIT 1]; 
   }
   
   if(accs != null && accs.size()!= 0) {
      acct = accs[0];
   }
   system.debug('acct size..::'+ acct);
   
   if( mdmPartyVehicleObject.strAccoutType=='Personal') {
    acct.RecordTypeId=PERSONAL_ID;
    //CAS 20150409 : HQREQ-01326 we fill the firsname with a . if its empty to "bypass" the VR (=>firstname mandatory)
    if(mdmPartyVehicleObject.strFirstName!=null && mdmPartyVehicleObject.strFirstName!=''){
        acct.FirstName = mdmPartyVehicleObject.strFirstName;
    }
    else{
        acct.FirstName = '.';
    }
        
       acct.LastName  = mdmPartyVehicleObject.strLastName;
       
       acct.SecondFirstName__c = mdmPartyVehicleObject.strSecondFirstName;
       acct.SecondSurname__c   = mdmPartyVehicleObject.strSecondLastName;
       
    } else {
       acct.RecordTypeId=COMPANY_ID;
       acct.Name=mdmPartyVehicleObject.strOrganisationName;
   }
   
      acct.AccountBrand__c=Label.ACC_MDM;
   
   // Added for passing PartyID value into Tech_ACCExternalID__c field
 
      if(mdmPartyVehicleObject.strPartyID!=null && mdmPartyVehicleObject.strPartyID!='') {
         acct.Tech_ACCExternalID__c='MDM-'+mdmPartyVehicleObject.strPartyID;     
      }

      if(mdmPartyVehicleObject.strPartySubType!=null && mdmPartyVehicleObject.strPartySubType!='') {
         acct.Sub__c=mdmPartyVehicleObject.strPartySubType;     
      }
      
      if(mdmPartyVehicleObject.strDOB!=null && mdmPartyVehicleObject.strDOB!='') {
         Date birthDate=date.valueOf(mdmPartyVehicleObject.strDOB);
         acct.PersonBirthdate=birthDate;
      }
     
      if(mdmPartyVehicleObject.strCivility!=null && mdmPartyVehicleObject.strCivility!='') {
         acct.Salutation=mdmPartyVehicleObject.strCivility;
      }

      if(mdmPartyVehicleObject.strTitle!=null && mdmPartyVehicleObject.strTitle!='') {
         acct.Title__c=mdmPartyVehicleObject.strTitle;
      }
     
     
     if(mdmPartyVehicleObject.strGender!=null && mdmPartyVehicleObject.strGender!='') {
       system.debug('mdmPartyVehicleObject.strGender is..::'+ mdmPartyVehicleObject.strGender); 
       acct.Sex__c=mdmPartyVehicleObject.strGender;
      }
     
      if(mdmPartyVehicleObject.strMartialStatus!=null && mdmPartyVehicleObject.strMartialStatus!='') {
         acct.MaritalStatus__c=mdmPartyVehicleObject.strMartialStatus;
      }
     
      if(mdmPartyVehicleObject.strLanguage!=null && mdmPartyVehicleObject.strLanguage!='') {
         acct.Language__c=mdmPartyVehicleObject.strLanguage;
      }
     
      if(mdmPartyVehicleObject.strRenaultEmployee!=null && mdmPartyVehicleObject.strRenaultEmployee!='') {
         acct.Renault_Employee__c=Boolean.valueOf(mdmPartyVehicleObject.strRenaultEmployee);
      }
     
      if(mdmPartyVehicleObject.intNumberofChildren!=null ) {
         acct.NbrChildrenHome__c=String.valueOf(mdmPartyVehicleObject.intNumberofChildren);
      }
     
      if(mdmPartyVehicleObject.strDeceased!=null && mdmPartyVehicleObject.strDeceased!='') {
         acct.Deceased__c=mdmPartyVehicleObject.strDeceased;
      }
     
      if(mdmPartyVehicleObject.strPreferredMedia!=null && mdmPartyVehicleObject.strPreferredMedia!='') {
         acct.PreferedMedia__c=mdmPartyVehicleObject.strPreferredMedia;
      }
      
      // To check for the picklist values
      if(mdmPartyVehicleObject.strOccupationalCategoryCode!=null && mdmPartyVehicleObject.strAccoutType=='Personal') {
         acct.JobClass__c=mdmPartyVehicleObject.strOccupationalCategoryDescription;
      }else if(mdmPartyVehicleObject.strOccupationalCategoryCode!=null && mdmPartyVehicleObject.strAccoutType=='Company') {
         acct.Industry=mdmPartyVehicleObject.strOccupationalCategoryDescription;
      }
        
      if(mdmPartyVehicleObject.strPartySegment!=null && mdmPartyVehicleObject.strPartySegment!='') {
         acct.SpecialCustmr__c=mdmPartyVehicleObject.strPartySegment;
      }
      
   // This is for company account and Company Commercial name
      if(mdmPartyVehicleObject.strAccoutType=='Company')
      { 
              if(mdmPartyVehicleObject.strCommercialName!=null && mdmPartyVehicleObject.strCommercialName!='') {
                 acct.UsualName__c=mdmPartyVehicleObject.strCommercialName;
              }   
      }
   
      if(mdmPartyVehicleObject.intNumberOfEmployees!=null ) {
         acct.NumberOfEmployees=mdmPartyVehicleObject.intNumberOfEmployees;
      }
   
      if(mdmPartyVehicleObject.strFinancialStatus!=null && mdmPartyVehicleObject.strFinancialStatus!='') {
         acct.Financial_Status__c=mdmPartyVehicleObject.strFinancialStatus;
      }
   
     /* if(mdmPartyVehicleObject.strCompanyActivityCode!=null && mdmPartyVehicleObject.strCompanyActivityCode!='') {
         acct.Code_NAF__c=mdmPartyVehicleObject.strCompanyActivityCode;
      } */
      if(mdmPartyVehicleObject.strCompanyActivityDescription!=null && mdmPartyVehicleObject.strCompanyActivityDescription!='') {
         acct.Code_NAF__c=mdmPartyVehicleObject.strCompanyActivityDescription;
      }
      
   
      if(mdmPartyVehicleObject.strLegalNature!=null && mdmPartyVehicleObject.strLegalNature!='') {
         acct.LegalForm__c=mdmPartyVehicleObject.strLegalNature;
      } 
   
     // Address field mapping
         
      if(mdmPartyVehicleObject.strAddressLine1!=null ) {
         strAcctAddress1=mdmPartyVehicleObject.strAddressLine1;
      }
      if(mdmPartyVehicleObject.strAddressLine2!=null ) {
         strAcctAddress2=mdmPartyVehicleObject.strAddressLine2;
      }
      if(mdmPartyVehicleObject.strAddressLine3!=null ) {
         strAcctAddress3=mdmPartyVehicleObject.strAddressLine3;     
      }   
      if(mdmPartyVehicleObject.strAddressLine4!=null ) {
         strAcctAddress4=mdmPartyVehicleObject.strAddressLine4;     
      }
   
      acct.BillingStreet=strAcctAddress1+' '+strAcctAddress2+' '+strAcctAddress3+' '+strAcctAddress4;
   

      if (mdmPartyVehicleObject.strCity!=null && mdmPartyVehicleObject.strCity!='') {
          acct.BillingCity=mdmPartyVehicleObject.strCity;
      }
      if (mdmPartyVehicleObject.strRegion!=null && mdmPartyVehicleObject.strRegion!='') {
          acct.BillingState=mdmPartyVehicleObject.strRegion;
      }
      if (mdmPartyVehicleObject.strPostCode!=null && mdmPartyVehicleObject.strPostCode!='') {
          acct.BillingPostalCode=mdmPartyVehicleObject.strPostCode;
      }
      if (mdmPartyVehicleObject.strCountry!=null && mdmPartyVehicleObject.strCountry!='') {
          acct.BillingCountry=mdmPartyVehicleObject.strCountry;
      }
 
     // ElectronicAddress field mapping
       if (mdmPartyVehicleObject.strAccoutType=='Company') {
           if(mdmPartyVehicleObject.strEmail!=null && mdmPartyVehicleObject.strEmail!='' ){ 
             acct.ProfEmailAddress__c=mdmPartyVehicleObject.strEmail;             
           }
           
       } else if (mdmPartyVehicleObject.strAccoutType=='Personal') {
           if(mdmPartyVehicleObject.strEmail!=null && mdmPartyVehicleObject.strEmail!='' ){ 
              acct.PersEmailAddress__c=mdmPartyVehicleObject.strEmail;     
           }
       }

  
     // PhoneNumber field mapping
  
       /*
       if(String.isBlank(mdmPartyVehicleObject.strMobile) && String.isBlank(mdmPartyVehicleObject.strFixeLandLine)){ 
          acct.PersMobPhone__c='0000000000';  
          acct.Phone='0000000000';   
       } 
       else if(mdmPartyVehicleObject.strMobile==null && mdmPartyVehicleObject.strFixeLandLine==null) { 
              acct.PersMobPhone__c='0000000000';  
              acct.Phone='0000000000';   
       } 
       else if(mdmPartyVehicleObject.strMobile!=null && mdmPartyVehicleObject.strMobile!='') {
              String strMobileNo=mdmPartyVehicleObject.strMobile;
              strMobileNo=strMobileNo.replace(' ',''); 
              acct.PersMobPhone__c =strMobileNo;
              acct.Phone=strMobileNo;
       } 
       else if(mdmPartyVehicleObject.strFixeLandLine!=null && mdmPartyVehicleObject.strFixeLandLine!='') {
               String strFixeLandLine=mdmPartyVehicleObject.strFixeLandLine.trim();
               strFixeLandLine=strFixeLandLine.replace(' ',''); 
               //acct.PersMobPhone__c =strFixeLandLine;
               acct.HomePhone__c=strFixeLandLine;
               acct.Phone=strFixeLandLine;
       } 
 */
    
    if (mdmPartyVehicleObject.strAccoutType=='Personal') {
        system.debug('## inside personal ');
     if(mdmPartyVehicleObject.strMobile!=null && mdmPartyVehicleObject.strMobile!='') {
              String strMobileNo=mdmPartyVehicleObject.strMobile;
              system.debug('## strMobileNo is..::'+ strMobileNo);
              strMobileNo=strMobileNo.replace(' ',''); 
              acct.PersMobPhone__c =strMobileNo;
              //acct.Phone=strMobileNo;
       } 

     if(mdmPartyVehicleObject.strFixeLandLine!=null && mdmPartyVehicleObject.strFixeLandLine!='') {
              String strFixeLandLine=mdmPartyVehicleObject.strFixeLandLine.trim();
              strFixeLandLine=strFixeLandLine.replace(' ',''); 
              //acct.PersMobPhone__c =strFixeLandLine;
              system.debug('strFixeLandLine is..::'+ strFixeLandLine);
              acct.HomePhone__c=strFixeLandLine;
              acct.Phone=strFixeLandLine;
       } 
     }else if(mdmPartyVehicleObject.strAccoutType=='Company') {
          if(mdmPartyVehicleObject.strFixeLandLine!=null && mdmPartyVehicleObject.strFixeLandLine!='') {
                  String strFixeLandLine=mdmPartyVehicleObject.strFixeLandLine.trim();
                  strFixeLandLine=strFixeLandLine.replace(' ',''); 
                  acct.Phone=strFixeLandLine;
           }     
     }
       
      /*
       else if(mdmPartyVehicleObject.strFixeLandLine!=null && mdmPartyVehicleObject.strFixeLandLine!='') {
               String strFixeLandLine=mdmPartyVehicleObject.strFixeLandLine.trim();
               strFixeLandLine=strFixeLandLine.replace(' ',''); 
               //acct.PersMobPhone__c =strFixeLandLine;
               acct.HomePhone__c=strFixeLandLine;
               acct.Phone=strFixeLandLine;
       } */
 
     // DealerCode Field Mapping
      if(mdmPartyVehicleObject.strDealerCode!=null && mdmPartyVehicleObject.strDealerCode!=''){ 
         acct.PrefdDealerDacia__c=mdmPartyVehicleObject.strDealerCode;     
      }

      // Party Idenfication Field Mapping
      /*
      if(mdmPartyVehicleObject.strPartyIdentificationType!=null && mdmPartyVehicleObject.strPartyIdentificationType=='SIRET'){
         acct.SIRET__c=Integer.valueOf(mdmPartyVehicleObject.strPartyIdentificationValue);      
      }*/  
      
      
      if(mdmPartyVehicleObject.strPartyIdentificationValue!=null && mdmPartyVehicleObject.strPartyIdentificationValue!='') {
          acct.Second_account_id__c=mdmPartyVehicleObject.strPartyIdentificationValue;
      }
      if(mdmPartyVehicleObject.strSiretValue!=null && mdmPartyVehicleObject.strSiretValue!='') {
          acct.CompanyID__c=mdmPartyVehicleObject.strSiretValue;
      }
      
   // Stop Communication Filed Mapping
   //acct.ComAgreemt__c='Partial'; 
  try{
  if(accs.size() != 0){
    system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount() --> Before calling Update Acct ');
    update acct;
   }
   if(accs.size() == 0){
     system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount() --> Before calling Insert Acct ');
     insert acct;
   }    
  }catch(DMLException dmlEx){
    system.debug('DMLException occured While Inserting/Updating the Account :'+ dmlEx.getMessage());
  }
  catch(Exception ex) {
      system.debug('Exception occured While Inserting/Updating the Account :'+ ex.getMessage());
  }            
   system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount() AccoutID :'+acct.id);
   
   
   // update into vehicle insert vin and brand vales
   String strVin;
   String strBrand;
   String strModelLabel;
   String strModelCode;
   String strRegistrationDate;
   String strCurrentMileage;
   String strCurrentMileageDate;
   String strDeliveryDate;
   String strRegistrationNumber;
   String strNewVehicle;
   
   Rforce_MDMPartyOverview_CLS.Vehicle vehicleObj = new Rforce_MDMPartyOverview_CLS.Vehicle();
   List <VEH_Veh__c> vehList = new List<VEH_Veh__c>();
   
   // Created for MDM Evolution ID No :39
   List <VEH_Veh__c> allVehicleList = new List<VEH_Veh__c>();

   Set<String> allVehicleSet = new Set<String>();
   List <VRE_VehRel__c> vehRelationList = new List<VRE_VehRel__c>();
                                        
   vehicleDataList = mdmPartyVehicleObject.vcle;
   system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount():vehicleDataList Size:'+ vehicleDataList.size());
   system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount():vehicleDataList:'+ vehicleDataList);
  
   Map<String,VEH_Veh__c> vinnumbers=new Map<String,VEH_Veh__c>();
   for (Integer j=0; j<vehicleDataList.size(); j++) 
   {
      vehicleObj = new Rforce_MDMPartyOverview_CLS.Vehicle();
      vehicleObj = (Rforce_MDMPartyOverview_CLS.Vehicle)vehicleDataList.get(j);
      vehicle= new VEH_Veh__c();

      strVin        = vehicleObj.vin;
      strBrand      = vehicleObj.brandCode;
      strModelCode  = vehicleObj.modelCode;
      strModelLabel = vehicleObj.modelLabel; 
      
      strRegistrationDate  = vehicleObj.firstRegistrationDate;
      strCurrentMileageDate= vehicleObj.currentMileageDate;
      strDeliveryDate      = vehicleObj.deliveryDate;
      strCurrentMileage    = vehicleObj.currentMileage; 
      strRegistrationNumber= vehicleObj.registration;   
      strNewVehicle        = vehicleObj.newVehicle;
      
      system.debug('strNewVehicle value is..::'+ strNewVehicle);
      
      vehicle.Name             = strVin;
      vehicle.Model_Code2__c   = strModelCode;
     // Added for passing mode code to Model__c field on 19thNov2014
      vehicle.Model__c=strModelCode;
      vehicle.VehicleBrand__c  = strBrand;
      
      vehicle.Commercial_Model__c= strModelLabel;
      
      if (strDeliveryDate!=null && strDeliveryDate.length()>0) {
          Date deliveryDate = date.valueOf(strDeliveryDate);
          vehicle.DeliveryDate__c  = deliveryDate;
      }
      
      if (strRegistrationDate!=null && strRegistrationDate.length()>0) {
          Date registeredDate = date.valueOf(strRegistrationDate);
          vehicle.FirstRegistrDate__c  = registeredDate;
      }
      
      vehicle.VehicleRegistrNbr__c = strRegistrationNumber;
         
      String existingVin='';
      system.debug('## strBrand is ..::'+strBrand);
      // EMC - if MDM do not provide Brand in webservice -- HQREQ-01305
      if(strBrand != null) {
           if(strBrand.equalsIgnoreCase('Renault') || strBrand.equalsIgnoreCase('Dacia'))
           {            
         // Added for MDM ID No :39 to add all the Vehicles in the List to create VehicleRelations 
          allVehicleList.add(vehicle);
          vinnumbers.put(vehicle.name,vehicle);      
         } // end of if loop for strBrand check            
       }
    }   // end of for loop
      for(VEH_Veh__c veh: [Select Id,Name from VEH_Veh__c WHERE Name IN : vinnumbers.keyset()]){
          vinnumbers.remove(veh.Name );    
      }
      for(VEH_Veh__c veh : allVehicleList){
          VEH_Veh__c vh= vinnumbers.get(veh.name);
          if(vh!=null){
              vehList.add(vh);
          }
      }
      system.debug('map of vehiles to be inserted-->'+vinnumbers);
      system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount():vehList:'+vehList);
      system.debug('## Available VIN in allVehicleSet ##'+ allVehicleSet);
      system.debug('## All VehicleList ##'+ allVehicleList);

      try{
       insert vehList;
      }catch(DMLException dmlEx){
       system.debug('DMLException occured While Inserting/Updating the Vehicle List :'+ dmlEx.getMessage());
      }
      catch(Exception ex) {
       system.debug('Exception occured While Inserting the Vehicles List :'+ ex.getMessage());
      } 
      
      system.debug('## After inserting values into Vehicles ##'); 
      // Commented for the MDM Evolution ID: 39
      // Adding all the Vin Number and ID to check in Vehicle Relations
      
      if (vehList!=null) {
         for(VEH_Veh__c vn: vehList){
           allvinmap.put(vn.name,vn.id);
         }
      }
      
     // Added for the MDM Evolution ID :39 to fetch Vehicle ID's for all the available Vehicles
      /*
      if (allVehicleList !=null) {
           for(VEH_Veh__c vn: allVehicleList){
               Id vehid=[Select ID from VEH_Veh__c where Name=:vn.name].Id;
               allvinmap.put(vn.name,vehid);
         }

      }  
      */
      system.debug('# allvinmap size is..::'+ allvinmap.size());    
      system.debug('# allvinmap Value is..::'+ allvinmap);    

      if (strVin!=null) {
          for (Integer j=0; j<vehicleDataList.size(); j++) 
            {
             vehRelation=new VRE_VehRel__c();
             vehicleObj = new Rforce_MDMPartyOverview_CLS.Vehicle();
             vehicleObj = (Rforce_MDMPartyOverview_CLS.Vehicle)vehicleDataList.get(j);
             
             strVin= vehicleObj.vin;
             String vehicleType=vehicleObj.vehicleType;
             String startDate=vehicleObj.startDate;
             String endDate=vehicleObj.endDate;
             
             strNewVehicle= vehicleObj.newVehicle;
             
              //if (allVehicleSet.contains(strVin))
              //{
                 //Id vehid=[Select ID from VEH_Veh__c where Name=:strVin].Id;
                   Id vehid=allvinmap.get(strVin);
                   
                 system.debug('## vehid is..::'+vehid);
                 
                 vehRelation.VIN__c=vehid;
                 vehRelation.Account__c=acct.id;
                 
                 if (strNewVehicle!=null && strNewVehicle.equals('Y')){
                     vehRelation.VNVO__c = Label.ACC_New;
                 }else if (strNewVehicle!=null && strNewVehicle.equals('N')){
                     vehRelation.VNVO__c = Label.ACC_SecondHand;
                 }
                 
                 // Commented below conditions for MDM Evolution ID:39 and 40 to retrive the values for VehicleType from Custom Settings
                 /*
                 if (vehicleType=='OWN') {
                     vehRelation.TypeRelation__c=Label.ACC_Owner;
                  }else{
                     vehRelation.TypeRelation__c=vehicleObj.vehicleType;
                  } 
                  */
             
                  // Added for MDM Evolution ID:39 and 40 to retrive the values for VehicleType from Custom Settings
                  
                   Map<String, MDMVehicleRelationType__c> mdmVehicleRelationType= MDMVehicleRelationType__c.getAll();                 
                   List<String> vehicleRelationsNames = new List<String>();                 
                   vehicleRelationsNames.addAll(mdmVehicleRelationType.keySet());                 
                   vehicleRelationsNames.sort();
                  
                   for (String vehicleName: vehicleRelationsNames) {                     
                         MDMVehicleRelationType__c vehicleRelationValue = mdmVehicleRelationType.get(vehicleName);                     
                         if (vehicleRelationValue.Name.equalsIgnoreCase(vehicleType)) {                         
                              vehRelation.TypeRelation__c= vehicleRelationValue.VehicleRelationType__c;                      
                         }                 
                   }      
                  
                  if(startDate!=null && startDate.length()>0) {
                     Date myDate = date.valueOf(startDate);
                     vehRelation.StartDateRelation__c=myDate;
                  }
                  
                  if(endDate!=null && endDate.length()>0) {
                     Date endDateRelation= date.valueOf(endDate); 
                     vehRelation.EndDateRelation__c=endDateRelation;
                  }
                  if (vehid!=null)  {
                      vehRelationList.add(vehRelation);
                   }   
              // }   
            }
            
         system.debug('CLASS:Rforce_MDMCreateAccountUtils_CLS-->METHOD:createMDMAccount():Before inserting record inito VehicleRelation:');      
         try{
              insert vehRelationList;
            }catch(DMLException dmlEx){
              system.debug('DMLException occured While Inserting/Updating the VehicleRelations :'+ dmlEx.getMessage());
            }
             catch(Exception ex) {
              system.debug('Exception occured While Inserting/Updating the VehicleRelations :'+ ex);
          }     
         }
    return acct.id;
 
 }



}