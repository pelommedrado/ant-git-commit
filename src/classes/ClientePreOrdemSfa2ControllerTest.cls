@isTest
private class ClientePreOrdemSfa2ControllerTest {

    static Opportunity opportunity1;
    static Quote quote;
    static Product2 prd ;
    static VEH_Veh__c v;
    static VRE_VehRel__c vr;
    static PricebookEntry pe;

    static {
        Account account1 = new Account(
            Name = 'Account Test 01',
            RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id,
            IDBIR__c = '1001');
        insert account1;

        Id recordTypeId = getRecordTypeId('Personal_Acc');

        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId;
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com';
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.RgStateTexto__c = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone = '1133334444';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.Sex__c = 'Man';
        sObjAccPersonal.MaritalStatus__c = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate = System.today();
        insert sObjAccPersonal;

        opportunity1 = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            AccountId = sObjAccPersonal.Id);
        insert opportunity1;

        MyOwnCreation moc = new MyOwnCreation();

        prd = moc.criaProduct2();
        Insert prd;

        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;

        Product2 prd2 = moc.criaProduct2();
        prd2.ModelSpecCode__c = 'Name';
        prd2.Model__c = prd.Id;
        Insert prd2;

        pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = pb.Id;
        pe.Product2Id = prd.Id;
        Insert pe;

        v = moc.criaVeiculo();
        v.Model__c = 'CLIO';
        v.Price__c = 1;
        v.DateofManu__c = system.today();
        Insert v;

        vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        vr.Account__c = account1.Id;
        Insert vr;

        VRE_VehRel__c veiculo = [
            SELECT Id, VIN__r.Model__c, Account__c, VIN__r.Price__c
            FROM VRE_VehRel__c
            WHERE ID =: vr.id
        ];
        VFC61_QuoteDetailsVO orc =
            Sfa2Utils.criarOrcamento(
                opportunity1.Id,
                null);

        quote = [
            SELECT ID
            FROM Quote
            WHERE ID =: orc.Id
        ];

        /*VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);

        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quote.Id;
        quoteLineItem.PricebookEntryId = pe.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 1;

        insert quoteLineItem;*/

    }

    static Id getRecordTypeId(String developerName) {
        RecordType sObjRecordType = [
            SELECT Id
            FROM RecordType
            WHERE DeveloperName =: developerName];

        return sObjRecordType.Id;
    }

    static testMethod void unitTest01() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        ct.initializeQuote();
        ct.addVeiculo();
        Test.stopTest();
    }

    static testMethod void unitTest02() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);
        ct.initializeQuote();
        ct.removerVeiculo();
        Test.stopTest();
    }

    static testMethod void unitTest03() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        ct.mobilePhoneQuote = '';
        ct.customerEmailQuote = '';
        ct.homePhoneQuote = '';
        ct.initializeQuote();
        ct.salvar();
        Test.stopTest();
    }

    static testMethod void unitTest04() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        ct.initializeQuote();
        ct.quoteVO.accountZipCode = '04410060';
        ct.getAddressGivenZipCode();
        ct.quoteVO.billPJZipCode = '04410060';
        ct.getAddressGivenZipCodeCNPJ();
        ct.quoteVO.billZipCode = '04410060';
        ct.getAddressGivenZipCodeCPF();

        ct.getStates();
        ct.useExistBillingAccount = true;
        ct.useAccountInfo();
        Test.stopTest();
    }

    static testMethod void unitTest05() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);

        List<Selectoption> brands = ct.brands;

        ct.initializeQuote();
        ct.useExistBillingAccount = false;
        ct.validateBooking();
        ct.saveRequest();
        ct.changeBillingAccount();
        ct.getAccount();
        ct.changeTipoCliente();
        ct.refresh();

        ct.quoteVO.FKQuote.BillingAccount__c = ct.quoteVO.sObjQuote.Opportunity.Account.Id;
        ct.changeBillingAccount();
        Test.stopTest();
    }

    static testMethod void unitTest06() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        ct.initializeQuote();

        ct.quoteVO.accountCnpjCpf = '08233350540';
        ct.getAccount();

        ct.quoteVO.accountFirstName = '';
        ct.quoteVO.accountLastName = '';
        ct.salvar();
        ct.quoteVO.accountFirstName = 'accountFirstName';
        ct.quoteVO.accountLastName = 'accountLastName';
        ct.salvar();

        ct.quoteVO.FKQuote.BillingAccount__c = ct.quoteVO.sObjQuote.Opportunity.Account.Id;
        ct.salvar();

        ct.quoteVO.accountResPhone = '';
        ct.quoteVO.accountComPhone = '';
        ct.quoteVO.accountEmail = '';
        ct.quoteVO.accountCelPhone = '';
        ct.salvar();

        Test.stopTest();
    }

    static testMethod void unitTest07() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        //VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);
        ct.initializeQuote();
        ct.useExistBillingAccount = false;
        ct.quoteVO.billFirstName = 'accountFirstName';
        ct.quoteVO.billLastName = 'accountLastName';
        ct.quoteVO.billCpf = 'accountLastName';
        ct.quoteVO.billEmail = 'accountLastName';

        ct.quoteVO.accountFirstName = 'accountFirstName';
        ct.quoteVO.accountLastName = 'accountLastName';
        ct.quoteVO.accountCnpjCpf = 'accountLastName';
        ct.quoteVO.accountEmail = 'accountLastName';
        ct.quoteVO.accountResPhone = 'phone';
        ct.quoteVO.accountZipCode = 'zipCode';
        ct.quoteVO.accountStreet = 'accuntStreet';
        ct.quoteVO.accountCity = 'accountCity';
        ct.quoteVO.accountState = 'accountState';
        ct.quoteVO.accountRG = 'accountRG';
        ct.quoteVO.sObjQuote.Opportunity.Account.Sex__c = 'Man';
        ct.quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c = '1-MARRIED';
		ct.quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate = System.today();

        ct.quoteVO.billResPhone = 'billResPhone';
        ct.quoteVO.billStreet = 'accountLastName';
        ct.quoteVO.billZipCode = 'accountLastName';
        ct.quoteVO.billState = 'accountLastName';

        ct.quoteVO.billCity = 'accountLastName';
        ct.quoteVO.billRG = 'accountLastName';
        ct.quoteVO.billState = 'accountLastName';
        ct.quoteVO.sObjQuote.BillingAccount__r.Sex__c = 'Man';
        ct.quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c = '1-MARRIED';
        ct.quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate = System.today();
        ct.saveRequest();

        ct.quoteVO.tipoCliente = 'cnpj';
        ct.saveRequest();
        ct.salvar();

        ct.addVeiculo();
        ct.saveRequest();

        Test.stopTest();
    }

    static testMethod void unitTest08() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);
        ct.initializeQuote();
        Test.stopTest();
    }

    static testMethod void unitTest09() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        Test.startTest();
        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);

        Delete vr;
        Delete v;

        Model__c modelo = new Model__c(
            Name = 'modelo',
            Model_PK__c = '2342'
        );
        Insert modelo;

        PVVersion__c versao = new PVVersion__c(
            Name = 'Versao',
            Model__c = modelo.Id,
            PVC_Maximo__c = 2,
            PVR_Minimo__c = 1,
            Price__c = 1,
            Version_Id_Spec_Code__c = '234werferg'
        );
        Insert versao;

        Optional__c opt = new Optional__c(
            Name = 'Azul',
            Type__c = 'Cor',
            Optional_Code__c = '3N4',
            Version__c = versao.Id
        );
        Insert opt;

        Optional__c opt2 = new Optional__c(
            Name = 'harmonia',
            Type__c = 'Harmonia',
            Optional_Code__c = '3U4',
            Version__c = versao.Id
        );
        Insert opt2;

        Optional__c opt3 = new Optional__c(
            Name = 'Trim',
            Type__c = 'Trim',
            Optional_Code__c = '3NT',
            Version__c = versao.Id
        );
        Insert opt3;

        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quote.Id;
        quoteLineItem.PricebookEntryId = pe.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 1;
        quoteLineItem.Optionals_AOC__c = 'optional1;optional2';
        quoteLineItem.Harmony_AOC__c = opt2.Id;
        quoteLineItem.Upholstery_AOC__c = opt3.Id;
        quoteLineItem.Color_AOC__c = opt.Id;
        Insert quoteLineItem;

        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, NULL, quote.Id, 1000);

        ct.initializeQuote();
        ct.salvar();
        Test.stopTest();
    }

    /*static testMethod void unitTest03() {

        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(quote);

        ClientePreOrdemSfa2Controller ct = new ClientePreOrdemSfa2Controller(stdCont);
        ct.initializeQuote();
        ct.addVeiculo();
        ct.removerVeiculo();
        ct.salvar();
        ct.quoteVO.accountZipCode = '04410060';
        ct.getAddressGivenZipCode();
        ct.quoteVO.billPJZipCode = '04410060';
        ct.getAddressGivenZipCodeCNPJ();
        ct.quoteVO.billZipCode = '04410060';
        ct.getAddressGivenZipCodeCPF();

        ct.getStates();
        ct.useExistBillingAccount = true;
        ct.useAccountInfo();
        ct.useExistBillingAccount = false;
        ct.saveRequest();
        ct.changeBillingAccount();
        ct.getAccount();
        ct.changeTipoCliente();
        ct.refresh();

        ct.quoteVO.accountFirstName = '';
        ct.quoteVO.accountLastName = '';
        ct.salvar();
        ct.quoteVO.accountFirstName = 'accountFirstName';
        ct.quoteVO.accountLastName = 'accountLastName';
        ct.salvar();

        ct.quoteVO.accountResPhone = '';
        ct.quoteVO.accountComPhone = '';
        ct.quoteVO.accountEmail = '';
        ct.quoteVO.accountCelPhone = '';
        ct.salvar();

        VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(prd.Id, v.Id, quote.Id, 1000);
        ct.initializeQuote();
        ct.saveRequest();
    }
*/
}