public with sharing class VFC122_CampaignBO
{
    private static final VFC122_CampaignBO instance = new VFC122_CampaignBO();
    
    private VFC122_CampaignBO(){}

    public static VFC122_CampaignBO getInstance(){
        return instance;
    }
    
    
    /**
     * [before insert, before update]
     * This method will check, only for SingleCampaign recordType, some field values
     * If a error is found, the addError method is used.
     */
    public void validateSingleCampaign(List<Campaign> lstNewCampaign)
    {
        system.debug(LoggingLevel.INFO, '*** validateSingleCampaign()');

        Boolean isRunningViaDataloader = this.isRunningViaDataloader();
        VFC123_CampaignValidation campaignValidation = new VFC123_CampaignValidation();
        if (isRunningViaDataloader)
            campaignValidation.validadeCampaignForDataLoader(lstNewCampaign);
        else
            campaignValidation.validadeCampaignForDataEnteredManually(lstNewCampaign);
    }

    /**
     * Determine if the running user is the data loder
     */
    private Boolean isRunningViaDataloader()
    {
        String userName = UserInfo.getName();
        if (VFC126_CampaignConfig.setDataLoaderUsers.contains(userName))
        {
            system.debug(LoggingLevel.INFO, '*** isRunningViaDataloader = true');
            return true;
        }
        else
        {
            system.debug(LoggingLevel.INFO, '*** isRunningViaDataloader = false');
            return false;
        }
    }
    
    /**
     * This method will process importDBM.CampaignCode__c that contains the campaign code
     * [Campaign.DBMCampaignCode__c] separeted by |.
     * One of these fields below must be filled out in order to get a lead by Id or external Id
     * - importDBM.Id_Salesforce__c
     * - importDBM.CPF_CNPJ__c
     * - importDBM.NUMDBM__c
     */
    public void processCampaignMember(DBM_Import__c importDBM, Lead aLead, Map<String, Campaign> mapCampaignSingle,
        List<CampaignMember> lstCampaignMemberToInsert)
    {
        system.debug(LoggingLevel.INFO, '*** processCampaignMember()');

        if (importDBM.CampaignCode__c != null && importDBM.CampaignCode__c != '')
        {
            List<String> lstCampaignCode = importDBM.CampaignCode__c.split(VFC126_CampaignConfig.CAMPAIGN_CHAR_SEPARATOR);
            
            for (String code : lstCampaignCode)
            {
                Campaign campaign = mapCampaignSingle.get(code);
                // Check if all single campaign codes exists to allow the association with campaign member
                if (campaign == null)
                {
                	system.debug(LoggingLevel.ERROR, '*** DBMImport: o código da campanha pai não foi encontrado ['+code+'].');
                    importDBM.addError('DBMImport: o código da campanha pai não foi encontrado ['+code+'].');
                }
                else
                {
                	system.debug(LoggingLevel.INFO, '*** Preparing to insert CM in campaignId=['+campaign.Id+']');

                    CampaignMember cm = new CampaignMember(CampaignId = campaign.Id,
                                                           Status = 'Sent',
                                                           Source__c = 'DBM',
                                                           Transferred__c = false);
                    if (aLead.Id != null) {
                        cm.LeadId = aLead.Id;
                    }
                    /*
                    else if (aLead.NUMDBM__c != null)
                    {
                        // Create the Parent
                        //Lead parentLead = new Lead();
                        //parentLead.NUMDBM__c = aLead.NUMDBM__c;
                        //cm.Lead = parentLead;
                        
                        // Inser as a Parent
                        cm.Lead = aLead;
                        system.debug(LoggingLevel.INFO, '***  CM Inserting as aLead.NUMDBM__c');
                    }
                    else if (aLead.CPF_CNPJ__c != null && aLead.CPF_CNPJ__c != '')
                    {
                        // Create the Parent
                        //Lead parentLead = new Lead();
                        //parentLead.CPF_CNPJ__c = aLead.CPF_CNPJ__c;
                        //cm.Lead = parentLead;
                        cm.Lead = aLead;
                        
                        system.debug(LoggingLevel.INFO, '***  CM Inserting as aLead.CPF_CNPJ__c');
                    }
                    */
                    else {
                        // Was not possible to determine the lead. Rise an error.
                        importDBM.addError('CampaignMember: Não foi possível localizar o lead para associar a um campanha.');
                    }
                    lstCampaignMemberToInsert.add(cm);
                }
            }
        }
    }
    
    /**
     * [after update]
     * This code should run only for ParentCampaign recordType that doesn't have a parent
     * This method will check if the parent campaign was changed and if these changes
     * should be propagated to their children
     */
    public void checkParentCampaignChangesToForward(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld)
    {
        system.debug(LoggingLevel.INFO, '*** checkParentCampaignChangesToForward()');

        // Get all recordTypes Ids necessary, where the key is the developer name
        Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
            getRecordTypeIdByDevNameSet(new Set<String>{'ParentCampaign', 'SingleCampaign'});

        Set<Id> setCampaignParentId = new Set<Id>();
        
        // Get all children for each campaign
        for (Campaign campaignNew : lstTriggerNew)
        {
            //if (campaignNew.recordTypeId == mapRecordTypes.get('ParentCampaign') 
            // && campaignNew.parentId == null)
            setCampaignParentId.add(campaignNew.Id);
        }
        
        // Fetch all children campaigns
        Map<Id, List<Campaign>> mapCampaign = VFC121_CampaignDAO.getInstance().
            fetchChildrenCampaignListByIdSet(setCampaignParentId);

        List<Campaign> lstChildCampaignToUpdate = new List<Campaign>();

        // Check if the parent was modified
        for (Campaign campaignNew : lstTriggerNew)
        {
            //if (campaignNew.recordTypeId == mapRecordTypes.get('ParentCampaign')
            // && campaignNew.parentId == null)
            //{
                Boolean changedActive = false;
                Boolean changedStatus = false;
                Boolean changedDate = false;
                Boolean changedFlags = false;
    
                Campaign campaignOld = mapTriggerOld.get(campaignNew.Id);
                
                // Check if the parent campaign was deactivated, so all children campaign
                // should be deactivated too.           
                if (campaignNew.isActive == false && campaignOld.isActive == true)
                    changedActive = true;
                
                // Check if the parent Campaign had changed their Status to propagate the same
                // status do all children.
                if (campaignNew.Status != campaignOld.Status)
                    changedStatus = true;
                
                // Check if the parent campaign had changed the start or end Dates to propagate
                // the same dates to all children.
                if (campaignNew.StartDate != campaignOld.StartDate || campaignNew.EndDate != campaignOld.EndDate)
                    changedDate = true;
                    
                // Check if the parent campaign had changed the Flags
                if (campaignNew.DealerShareAllowed__c != campaignOld.DealerShareAllowed__c 
                 || campaignNew.PAActiveShareAllowed__c != campaignOld.PAActiveShareAllowed__c
                 || campaignNew.DealerInclusionAllowed__c != campaignOld.DealerInclusionAllowed__c
                 || campaignNew.ManualInclusionAllowed__c != campaignOld.ManualInclusionAllowed__c)
                    changedFlags = true;
                
                if (changedActive || changedStatus || changedDate || changedFlags)
                {
                    // Parent campaign was changed, so propagate the changes to children
                    List<Campaign> lstChildCampaign = mapCampaign.get(campaignNew.Id);
                    if (lstChildCampaign != null)
                    {
                        // this parent campaign has children
                        for (Campaign childCampaign : lstChildCampaign)
                        {
                            childCampaign.isActive = campaignNew.isActive;
                            childCampaign.Status = campaignNew.Status;
                            childCampaign.StartDate = campaignNew.StartDate;
                            childCampaign.EndDate = campaignNew.EndDate;
                            childCampaign.DealerShareAllowed__c = campaignNew.DealerShareAllowed__c;
                            childCampaign.PAActiveShareAllowed__c = campaignNew.PAActiveShareAllowed__c;
                            childCampaign.DealerInclusionAllowed__c = campaignNew.DealerInclusionAllowed__c;
                            childCampaign.ManualInclusionAllowed__c = campaignNew.ManualInclusionAllowed__c;

                            lstChildCampaignToUpdate.add(childCampaign);
                            system.debug(LoggingLevel.INFO, '*** added childCampaign.Id='+childCampaign.Id);
                        }
                    }
                }
            //}
        }
        
        if (lstChildCampaignToUpdate.size() > 0)
        {
            system.debug(LoggingLevel.INFO, '*** lstChildCampaignToUpdate='+lstChildCampaignToUpdate);
            update lstChildCampaignToUpdate;
        }
    }

    /**
     * [before insert, update]
     * Enforce that all children campaign will inherit some attributes values from their parent.
     * This should be applied for Single and Parent recordtypes that have a parent.
     */
    public void enforceInheritance(List<Campaign> lstTriggerNew)
    {
        system.debug(LoggingLevel.INFO, '*** enforceInheritance()');

        Set<Id> setCampaignParentId = new Set<Id>();
        
        // Get all parents
        for (Campaign campaignNew : lstTriggerNew)
        {
            if (campaignNew.parentId != null)
                setCampaignParentId.add(campaignNew.parentId);
        }
        
        // Fetch all children campaigns
        Map<Id, Campaign> mapCampaignParent = VFC121_CampaignDAO.getInstance().
            fetchCampaignByIdSet(setCampaignParentId);
        
        for (Campaign campaignNew : lstTriggerNew)
        {
            if (campaignNew.parentId != null)
            {
                Campaign campaignParent = mapCampaignParent.get(campaignNew.parentId);
                if (campaignParent == null)
                {
                    campaignNew.addError('Campaign: Não foi possível localizar a Campanha Pai informada.');
                }
                else
                {
                    // Use the same values from parent for these fields
                    campaignNew.IsActive = campaignParent.IsActive;
                    campaignNew.Status = campaignParent.Status;
                    campaignNew.StartDate = campaignParent.StartDate;
                    campaignNew.EndDate = campaignParent.EndDate;
                    campaignNew.DealerInclusionAllowed__c = campaignParent.DealerInclusionAllowed__c;
                    campaignNew.DealerShareAllowed__c = campaignParent.DealerShareAllowed__c;
                    campaignNew.ManualInclusionAllowed__c = campaignParent.ManualInclusionAllowed__c; 
                    campaignNew.PAActiveShareAllowed__c = campaignParent.PAActiveShareAllowed__c;
                    
                    system.debug(LoggingLevel.INFO, '*** copied from parent, campaignNew.Id='+campaignNew.Id);
                }
            }   
        }   
    }
    
    
	/**
      * [before insert, update]
      * Enforce that IsActive field is in sync with the Status field
      */
    public void enforceStatusIsActiveRules(List<Campaign> lstTriggerNew)
    {
        system.debug(LoggingLevel.INFO, '*** enforceStatusIsActiveRules()');
        
        for (Campaign campaignNew : lstTriggerNew)
        {
        	if (campaignNew.Status == 'In Progress')
        		campaignNew.IsActive = true;
        	else
        		campaignNew.IsActive = false;
        }
    }
    
    /**
     * Deduplicate campaign members in the same list
     */
    public List<CampaignMember> deduplicatePossibleCampaignMember(List<CampaignMember> lstCampaignMemberToInsert)
    {
    	List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
    	Map<String, CampaignMember> mapCampaignMember = new Map<String, CampaignMember>();
    	String key = '';
    	
    	for (CampaignMember cm : lstCampaignMemberToInsert)
		{
			if (cm.LeadId != null)
				key = cm.CampaignId + '-' + cm.LeadId;
			else
				key = '';

			if (key != '') {
				if (!mapCampaignMember.containsKey(key)) {
					mapCampaignMember.put(key, cm);
				}
				else {
					// duplicate cm, discard
					system.debug(LoggingLevel.INFO, '*** Duplicated CM: Lead.Id=' + cm.LeadId + ' with campaignId='+ cm.CampaignId);
				}
			}
			else {
				lstCampaignMember.add(cm);
			}
		}
    
    	// Join the list and the map
    	lstCampaignMember.addAll(mapCampaignMember.values());
    
    	return lstCampaignMember;
    }
}