/**
*	Class   -   VFC07_NonCustomerDecisionTreeDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*    
*   #01 <RameshPrabu> <31/08/2012>
*        Created this Class to handle record from Non Customer Decision Tree related Queries.
*/
public with sharing class VFC07_NonCustomerDecisionTreeDAO {
	
	private static final VFC07_NonCustomerDecisionTreeDAO instance = new VFC07_NonCustomerDecisionTreeDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC07_NonCustomerDecisionTreeDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC07_NonCustomerDecisionTreeDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get Non Customer Decision Tree Record based on some lead fields
    * @param clusterMD - This field using from Market data's cluster of lead object             
    * @param currentVehicle - This field using from current Vehicle of lead object
    * @param preApprovedCredit - This field using from preApproved Credit of lead object
    * @param vehicleOfInterest - This field using from vehicle Of Interest of lead object
    * @param vehicleOfCampaign - This field using from vehicle Of Campaign of lead object
    * @return lstNonCustomer - fetch and return the result in lstAfterSales list
    */
    public List<NCT_NonCustomerDecisionTree__c> findNonCustomerDecisionTree(String clusterMD, Boolean currentVehicle, String preApprovedCredit, Boolean vehicleOfInterest, Boolean vehicleOfCampaign){
        List<NCT_NonCustomerDecisionTree__c> lstNonCustomer = null;
        	
    	String query = 'Select Id, Name, Recommendation1__c, Recommendation2__c  From NCT_NonCustomerDecisionTree__c WHERE ClusterMD__c = '+ClusterMD+'  and CurrentVehicle__c = '+CurrentVehicle+' and PreApprovedCredit__c = '+PreApprovedCredit+' and VehicleOfInterest__c ='+VehicleOfInterest+'';
    	system.debug('Query....' +query);
        lstNonCustomer = [select 
        					 Id, 
        					 Name, 
        					 Recommendation1__c, 
        					 Recommendation2__c, 
        					 CurrentVehicle__c, 
        					 VehicleOfInterest__c
    				 	  from 
        				 	 NCT_NonCustomerDecisionTree__c
            			  where 
                			 ClusterMD__c =: clusterMD 
                			 And CurrentVehicle__c =:currentVehicle 
                			 And PreApprovedCredit__c =:preApprovedCredit 
                			 And VehicleOfInterest__c =:vehicleOfInterest 
                			 And VehicleOfCampaign__c =: vehicleOfCampaign];
        
        return lstNonCustomer;
    }
}