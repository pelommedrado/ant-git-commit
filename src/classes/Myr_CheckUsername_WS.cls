/*  Check is the username requested is available
    Error codes :   OK              : WS05MS000 
                    OK WITH WARNING : WS05MS001 -> WS05MS500
                    CRITICAL ERROR  : WS05MS501 -> WS05MS999
*************************************************************************************************************
13.02.2014  : Creation
19.09.2015  : New message codes : WS05MS001, WS05MS506
18 Mar 2016 : R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
*************************************************************************************************************/
global without sharing class Myr_CheckUsername_WS {
	
    //WebService used to check if the given username is available for the given brand
    //@param String username, the username requested
    //@param String brand, the brand requested Value = Renault, Dacia
    //@return a msg and the activated user

	private static Long Begin_Time=DateTime.now().getTime();
	private static String logRunId = Myr_CheckUsername_WS.class.getName() + Begin_Time;

    WebService static Myr_CheckUsername_WS_Response isAvailable(String username, String brand) {
        String inputs = 'username='+username+', brand='+brand;
         
        try { 
            //--- CHECK PARAMETERS    
            if( !Pattern.matches(Myr_MyRenaultTools.USERNAME_REGEX, username ) ) {
                return prepareResponse(
                        system.Label.Myr_CheckUsername_WS05MS502, 
                        system.Label.Myr_CheckUsername_WS05MS502_Msg.replace('{0}', username).replace('{1}', CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c + '<...>@<...>.<...>'),
                        logRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Critical, inputs); 
            }
            if( username.length() > User.Username.getDescribe().getLength() ) {
                return prepareResponse(system.Label.Myr_CheckUsername_WS05MS507, 
                                        system.Label.Myr_CheckUsername_WS05MS507_Msg + ' ' + User.Username.getDescribe().getLength(),
                                        logRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Critical, inputs);
            }
            if( String.isBlank(brand) ) { //Renault by default
                brand = Myr_MyRenaultTools.Brand.Renault.name();
            }
            if( !Myr_MyRenaultTools.authorizedBrand(brand) ) {
                return prepareResponse(system.Label.Myr_CheckUsername_WS05MS004, system.Label.Myr_CheckUsername_WS05MS004_Msg,
                                        logRunId, 'brand='+brand, INDUS_Logger_CLS.ErrorLevel.Critical, inputs); 
            }
            //--- CHECK AVAILABILITY
            List<User> listUser = [SELECT Id, AccountId, Username FROM User WHERE Username = :username];
            if( listUser.size() > 0) {
                //check if the username is already registered for another brand
                List<Account> listAcc = [SELECT Id, MYR_Status__c, MYD_Status__c FROM Account WHERE Id = :listUser[0].AccountId];
                if( listAcc.size() > 0 ) { 
                    Account acc = listAcc[0];
                    if(!isBrandActive(acc, brand)) { 
                        return prepareResponse(
                                  system.Label.Myr_CheckUsername_WS05MS001
                                , system.Label.Myr_CheckUsername_WS05MS001_Msg.replace('{0}', username)
                                , logRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Info, inputs);
                    } else {
                        return prepareResponse(
                                  system.Label.Myr_CheckUsername_WS05MS506
                                , system.Label.Myr_CheckUsername_WS05MS506_Msg.replace('{0}', username)
                                , logRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Info, inputs);
                    }
                } else {
                    //oups username is registered from someone else than a helios user !
                    return notAvailable(username, logRunId, inputs);
                }
            } else {
				Boolean isAvailable = Myr_Users_Utilities_Class.isUsernameAvailableAlThroughOrgs(username);
				if( isAvailable ) {
					return available(username, logRunId, inputs);
				} else {
					return notAvailable(username, logRunId, inputs);
				}
            }
        } catch (Exception e) {
            return prepareResponse(system.Label.Myr_CheckUsername_WS05MS505, system.Label.Myr_CheckUsername_WS05MS505_Msg + ' ' + e.getMessage(),
                                    logRunId, 'username='+username+'brand='+brand, INDUS_Logger_CLS.ErrorLevel.Critical, inputs);
        }
    }
    
    //Username is available: prepare the response
    private static Myr_CheckUsername_WS_Response available(String username, String thelogRunId, String inputs) {
        return prepareResponse(system.Label.Myr_CheckUsername_WS05MS000, system.Label.Myr_CheckUsername_WS05MS000_Msg.replace('{0}', username),
                                thelogRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Info, inputs);
    }  
    
    //Username is NOT available: prepare the response
    private static Myr_CheckUsername_WS_Response notAvailable(String username, String thelogRunId, String inputs) {
        return prepareResponse(system.Label.Myr_CheckUsername_WS05MS505, system.Label.Myr_CheckUsername_WS05MS505_Msg_NotAvai.replace('{0}', username),
                                thelogRunId, 'username='+username, INDUS_Logger_CLS.ErrorLevel.Info, inputs);
    }
    
    //Check if the brand is activated
    private static Boolean isBrandActive(Account acc, String brand) {
        return (brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Renault.name()) && acc.MYR_Status__c == system.Label.Myr_Status_Activated)
            || (brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.Dacia.name()) && acc.MYD_Status__c == system.Label.Myd_Status_Activated);
    }
    
    //@return the response in any situation
    private static Myr_CheckUsername_WS_Response prepareResponse(String code, String message, String thelogRunId, String logRecord, INDUS_Logger_CLS.ErrorLevel logErrLvl, String inputs) {
        addLog(thelogRunId, 'checkUsername', logRecord, code, message, logErrLvl.name(), inputs);
        Myr_CheckUsername_WS_Response response = new Myr_CheckUsername_WS_Response();
        response.info = new Myr_CheckUsername_WS_Response_Msg();
        response.info.code = code;
        response.info.message = message;
        system.debug('#### Myr_CheckUsername_WS - <prepareResponse> - code='+code+', message='+message);
        return response;
    }
  
    //Global format of the response
    global class Myr_CheckUsername_WS_Response {
        WebService Myr_CheckUsername_WS_Response_Msg info;
    }
  
    //Global format of the message into the response
    global class Myr_CheckUsername_WS_Response_Msg {
        WebService String code;
        WebService String message;
    }
    
    //Add the logs for this webservice in synchronous mode: no risk of a mixed_dml exception
    private static void addLog(String runLogId, String myMethod, String myId, String exceptType, String myMessage, String myLevel, String inputs) {
        Id id = INDUS_Logger_CLS.addGroupedLogFull(runLogId, null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CheckUsername_WS', 'Myr_CheckUsername_WS Output', exceptType, myMessage, myLevel, myId, null, null, Begin_Time, DateTime.now().getTime(), 'Inputs: <' + inputs + '>');
    } 

}