/*****************************************************************************************
    Name            : Rforce_CommercialOffersResponse_CLS
    Description     : This apex class is a POJO class which will contains the Helios Request values for Email and Vin Search.
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Imnplemented By : Ashok Muntha
 
******************************************************************************************/

public class Rforce_CommercialOffersPropose_CLS {

 public String strEmailAndCountry{get;set;}
 public String strVinAndCountry{get;set;}
 public String strMaxCount{get;set;}
 public String strUid{get;set;}
 public String strNlid{get;set;}
 public String strNoProp{get;set;}
 public String strCategories{get;set;}
 public String strThemes{get;set;}
 
 
 public Rforce_CommercialOffersPropose_CLS() {}
 
 public Rforce_CommercialOffersPropose_CLS(String strEmailAndCountry, String strVinAndCountry, String maxCount, String uid, String nlid, String noProp, String categories, String themes) {
     this.strEmailAndCountry=strEmailAndCountry;
     this.strVinAndCountry=strVinAndCountry;
     this.strMaxCount=maxCount;
     this.strUid=uid;
     this.strNlid=nlid;
     this.strNoProp=noProp;
     this.strCategories=categories;
     this.strThemes=themes;
 }    
  
}