/**
* Classe que possue os tipos de transição da oportunidade.
* Os tipos de transição estão definidos em constantes e cada constante está relacionada a uma entrada do picklist OpportunityTransition do objeto Opportunity.
* @author Felipe Jesus Silva.
*/
public class VFC43_OpportunityTransitionType 
{
	public static final String HOT_LEAD_TO_MANAGER = 'Hot Lead To Manager';
	
	public static final String TEST_DRIVE_TO_MANAGER = 'Test Drive To Manager';
	
	public static final String HOT_LEAD_MANAGER_TO_SELLER = 'Hot Lead - Manager to Seller';
	
	public static final String TEST_DRIVE_MANAGER_TO_SELLER = 'Test Drive - Manager to Seller';
	
	public static final String HOT_LEAD_RECEPTIONIST_TO_SELLER = 'Hot Lead - Receptionist to Seller';
	
	public static final String TEST_DRIVE_RECEPTIONIST_TO_SELLER = 'Test Drive - Receptionist to Seller';
	
	public static final String PASSING_RECEPTIONIST_TO_SELLER = 'Passing - Receptionist to Seller';
	
	public static final String IN_ATTENDANCE_SELLER = 'In Attendance - Seller';
	
	public static void init()
	{
		String t1 = VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER;
        String t2 = VFC43_OpportunityTransitionType.HOT_LEAD_RECEPTIONIST_TO_SELLER;
        String t3 = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        String t4 = VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER;
        String t5 = VFC43_OpportunityTransitionType.PASSING_RECEPTIONIST_TO_SELLER;
        String t6 = VFC43_OpportunityTransitionType.TEST_DRIVE_MANAGER_TO_SELLER;
        String t7 = VFC43_OpportunityTransitionType.TEST_DRIVE_RECEPTIONIST_TO_SELLER;
        String t8 = VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER;
	}
}