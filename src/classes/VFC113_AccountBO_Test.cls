@isTest
private class VFC113_AccountBO_Test {
	
	static Id getRecordTypeId(String developerName)
	{
		RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
		
		return sObjRecordType.Id;
	}
	
	static Account createDealerAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Network_Site_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccDealer = new Account();
		sObjAccDealer.Name = 'Dealer Test';
		sObjAccDealer.Phone='1000';
        sObjAccDealer.IDBIR__c='1000';
		sObjAccDealer.RecordTypeId = recordTypeId; 
		sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com'; 
		sObjAccDealer.ShippingCity = 'Paris';
		sObjAccDealer.ShippingCountry = 'France';
		sObjAccDealer.ShippingState = 'IDF';
		sObjAccDealer.ShippingPostalCode = '75013';
		sObjAccDealer.ShippingStreet = 'my street';
		
		return sObjAccDealer;
	}
	
    static testMethod void testAccountBO() {
        
        Account acc = createDealerAccount();
        insert acc;
        
        VFC113_AccountBO.getInstance().getById(acc.Id);
        
        delete acc;
        
        try{
        	VFC113_AccountBO.getInstance().getById(acc.Id);	
        }catch(Exception e){
        	System.assert(e.getMessage().contains('A conta não foi encontrada.'));
        }
        
        
    }
}