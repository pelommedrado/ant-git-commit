@isTest
public with sharing class TestVFC05_ArchivesXMLData{ 
static testMethod void VFC05_ArchivesMethod(){
Test.startTest();
VFC05_ArchivesXMLData mp = new VFC05_ArchivesXMLData();
VFC05_ArchivesXMLData.Attachment attachmentList=new VFC05_ArchivesXMLData.Attachment();
    VFC05_ArchivesXMLData.ActivityList activityList= new VFC05_ArchivesXMLData.ActivityList();   
    VFC05_ArchivesXMLData.ContactList contactList=new VFC05_ArchivesXMLData.ContactList();
    VFC05_ArchivesXMLData.Vehicle vehicleData=new VFC05_ArchivesXMLData.Vehicle();
    VFC05_ArchivesXMLData.Concerned concernedList=new VFC05_ArchivesXMLData.Concerned(); 
    VFC05_ArchivesXMLData.EffectPerception epList=new VFC05_ArchivesXMLData.EffectPerception();  
    VFC05_ArchivesXMLData.CustomerCompensation customercomensationList=new VFC05_ArchivesXMLData.CustomerCompensation();
    VFC05_ArchivesXMLData.Employee employeeList =new VFC05_ArchivesXMLData.Employee();
    VFC05_ArchivesXMLData.Paying payingList = new VFC05_ArchivesXMLData.Paying();  
    VFC05_ArchivesXMLData.OwnerOrganization ownerOrgList=new VFC05_ArchivesXMLData.OwnerOrganization();
    VFC05_ArchivesXMLData.FleetOrganization fleetorgList=new VFC05_ArchivesXMLData.FleetOrganization();
    VFC05_ArchivesXMLData.Contact_PersonalAddress contactpersonaddrList=new VFC05_ArchivesXMLData.Contact_PersonalAddress();
	VFC05_ArchivesXMLData.Concerned_BusinessAddress concernedbusinessList=new VFC05_ArchivesXMLData.Concerned_BusinessAddress();
    VFC05_ArchivesXMLData.Paying_BusinessAddress payingbusinessadressList=new VFC05_ArchivesXMLData.Paying_BusinessAddress();
    VFC05_ArchivesXMLData.BusinessAddress businessAddressList=new VFC05_ArchivesXMLData.BusinessAddress();
    
Test.stopTest();
}
}