/**
* @author: Vetrivel Sundararajan
* @date: 07/08/2014
* @description: This apex class is a POJO class which will contains the values of Search Party.
* @author: Vetrivel Sundararajan
* @date: 06/02/2015
* @description: Added strCompanyID field and modified the constructor to add companyID.
*/

public class Rforce_MDMSearchParty_CLS {

    public String strVin{get;set;}
    public String strRegistration{get;set;}
    public String strCountry{get;set;}
    public String strLastName{get;set;}
    public String strFirstName{get;set;}
    public String strCity{get;set;}
    public String strEmail{get;set;}
    public String strZipCode{get;set;}
    public String strLocalID{get;set;}
    public String strFixeLandLine{get;set;}
    public String strMobile{get;set;}
    public String strPartyID{get;set;}
    public String strCompanyID{get;set;}
    
    public Rforce_MDMSearchParty_CLS() {}
    
    public Rforce_MDMSearchParty_CLS(String vin, String registration, String country, String lname, String fname, String city, String email,String zipCode, String landLineNo, String mobileNo,String companyID)
    {
      this.strVin=vin;
      this.strRegistration=registration;
      this.strCountry=country;
      this.strLastName=lname;
      this.strFirstName=fname;
      this.strCity=city;
      this.strEmail=email;
      this.strZipCode=zipCode;
      this.strFixeLandLine=landLineNo;
      this.strMobile=mobileNo;
      this.strCompanyID=companyID;
    } 
 }