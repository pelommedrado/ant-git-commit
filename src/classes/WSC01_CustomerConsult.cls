global class WSC01_CustomerConsult {

    global static list<sObject> accountSearch {get;set;}
    global static list<sObject> contactSearch {get;set;}
    global static list<Cliente> customerList {get;set;}
    global static list<sObject> accountList {get;set;}
    global static list<sObject> contactList {get;set;}
    global static Map<String, Cliente> mapAccount {get;set;}
    global static Map<String, Veiculo> mapVehicle {get;set;}
    global static Cliente cliente {get;set;}
	global static Caso caso {get;set;}
    global static Veiculo veiculo {get;set;}

    //WS - Telephone Consultation
    webservice static list<Cliente> customerRequestPhone(String phone){
        phone = convertStringToNumber(phone);
        return customerConsultParam(1, phone);
    }

    //WS - Customer Identification Nbr Consultation
    webservice static list<Cliente> customerRequestCPF(String cpf){
        cpf = convertStringToNumber(cpf);
        return customerConsultParam(2, cpf);
    }

    //Customer Consultation
    public static list<Cliente> customerConsultParam(Integer operation, String param){        
        accountSearch = new list<sObject>();
        contactSearch = new list<sObject>();
        accountList = new list<sObject>();
        contactList = new list<sObject>();
        customerList = new list<Cliente>();
        String searchQuery;
        String searchStr = '*' + param + '*';
		String[] premiumCarsList;

        //Custom configuration premium cars
        if(cti_premium_cars__c.getValues('default') != null){            
            cti_premium_cars__c premiumCars = cti_premium_cars__c.getValues('default');
            if(premiumCars.id_premium_vehicles__c == null){
                premiumCars.id_premium_vehicles__c = 'Nenhum';
            }
            premiumCarsList = premiumCars.id_premium_vehicles__c.split(';');            
        }

		if(param.length() > 1){
            if(operation == 1){
                searchQuery = 'FIND \'' + searchStr + '\' IN PHONE FIELDS RETURNING Account, Contact';
            }else if(operation ==2){
                searchQuery = 'FIND \'' + searchStr + '\' IN ALL FIELDS RETURNING Account, Contact';
            }

            List<List<SObject>> searchList = Search.query(searchQuery);
            accountSearch = ((List<Account>)searchList[0]);
            contactSearch = ((List<Contact>)searchList[1]);
            
            accountList = [SELECT Id, Name, CompanyId__c, CustomerIdentificationNbr__c, Phone, ShippingStreet, ShippingCity, ShippingState,
                           (SELECT Id, CaseNumber, Status FROM Cases WHERE isClosed = false),   //Open cases
                           (SELECT Id, VIN__c, VIN__r.Name, VIN__r.Model__c, VIN__r.Version__c, VIN__r.Year_Model_Version__c, VIN__r.ModelYear__c
                            FROM Vehicle_Relations__r)		//Related vehicles
                           FROM Account
                           WHERE Id IN :accountSearch];
            
            contactList = [SELECT Id, Name, MobilePhone, HomePhone, CPF__c, MailingStreet, MailingCity, MailingState,
                           (SELECT Id, CaseNumber, Status FROM Cases WHERE isClosed = false)	//Open cases
                           FROM Contact 
                           WHERE Id IN :contactSearch];            
        	}

			mapAccount = new Map<String, Cliente>();
        	mapVehicle = new Map<String, Veiculo>();    

            if(accountList.size() > 0){
                for(SObject a : accountList){
                    cliente = new Cliente();
                    cliente.listaVeiculo = new list<Veiculo>();
                    cliente.listaCaso = new list<Caso>();
                    
                    cliente.id = (String)a.get(account.Id);
                    cliente.nome = (String)a.get(account.Name);
                    cliente.cpfCnpj = (String)a.get(account.CustomerIdentificationNbr__c);
                    cliente.telefone = (String)a.get(account.Phone);
                    cliente.logradouro = (String)a.get(account.ShippingStreet);
                    cliente.cidade = (String)a.get(account.ShippingCity);
                    cliente.estado = (String)a.get(account.ShippingState);
                    cliente.isPremium = false;

                    for(Case c : a.getSObjects('Cases')){
                        caso = new Caso();
                        caso.Id = c.Id;
                        caso.numeroCaso = c.CaseNumber;
                        caso.status = c.Status;
                        cliente.listaCaso.add(caso);
                    }

                    for(VRE_VehRel__c v : a.getSObjects('Vehicle_Relations__r')){
                        veiculo = new Veiculo();
                        veiculo.Id = v.Id;
                        veiculo.modelo = v.VIN__r.Model__c;
                        veiculo.versao = v.VIN__r.Version__c;
                        veiculo.anoModelo = String.valueOf(v.VIN__r.ModelYear__c);
                        veiculo.anoVersao = v.VIN__r.Year_Model_Version__c;

                        cliente.listaVeiculo.add(veiculo);
                        mapVehicle.put(veiculo.modelo.toLowerCase(), veiculo);
                    }

                    //Validates that client has premium service
                    if(cti_premium_cars__c.getValues('default') != null && mapVehicle.size() > 0){
                        for(String s : premiumCarsList){
                            if(mapVehicle.containsKey(s.toLowerCase())){
                                cliente.isPremium = true;
                            }
                        }
                    }
                    
                    //Map used to validate whether there is personal account in the contacts return
                    mapAccount.put((String)a.get(account.Name), cliente);
                    
                    customerList.add(cliente);
                }            
            }

            if(contactList.size() > 0){
                for(SObject a : contactList){
                    if(!mapAccount.containsKey((String)a.get(contact.Name))){                        
                        cliente = new Cliente();
                        cliente.listaVeiculo = new list<Veiculo>();
                        cliente.listaCaso = new list<Caso>();
                        
                        cliente.id = (String)a.get(contact.Id);
                        cliente.nome = (String)a.get(contact.Name);
                        cliente.cpfCnpj = (String)a.get(contact.CPF__c);
                        cliente.telefone = (String)a.get(contact.HomePhone);
                        cliente.celular = (String)a.get(contact.MobilePhone);
                        cliente.logradouro = (String)a.get(contact.MailingStreet);
                        cliente.cidade = (String)a.get(contact.MailingCity);
                        cliente.estado = (String)a.get(contact.MailingState);
                        
                        for(Case c : a.getSObjects('Cases')){
                            caso = new Caso();
                            caso.Id = c.Id;
                            caso.numeroCaso = c.CaseNumber;
                            caso.status = c.Status;
                            cliente.listaCaso.add(caso);
                        }
            
                        customerList.add(cliente);
                    }                   
                }           
            }
        return customerList;
    }
    
    /*Converts characters and numbers only numbers*/
    private static String convertStringToNumber(String str){
        String numericString = '';
        integer strLength = str.length();

        for(integer i = 0; i < str.length(); i++){
            String s = str.mid(i,1);
            if(s.isNumeric()){
                numericString += s;
            }
        }       
        return numericString;
    }
    
    /*Client*/
    global class Cliente{
        webservice String id {get;set;}
        webservice String nome {get;set;}
        webservice String cpfCnpj {get;set;}
        webservice String telefone {get;set;}
        webservice String celular {get;set;}
        webservice String logradouro {get;set;}
        webservice String cidade {get;set;}
        webservice String estado {get;set;}
        webservice boolean isPremium {get;set;}
        webservice list<Veiculo> listaVeiculo {get;set;}
        webservice list<Caso> listaCaso {get;set;}
    }
    /*Vehicle*/
    global class Veiculo{
        webservice String id {get;set;}
        webservice String modelo {get;set;}
        webservice String versao {get;set;}
        webservice String anoModelo {get;set;}
        webservice String anoVersao {get;set;}
    }
	/*Cases*/
    global class Caso{
        webservice String id {get;set;}
        webservice String numeroCaso {get;set;}
        webservice String status {get;set;}
    }
}