/**
* Classe responsável por fazer o papel de business delegate da página de confirmação de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC79_TestDriveConfirmBusinessDelegate 
{
	private static final VFC79_TestDriveConfirmBusinessDelegate instance = new VFC79_TestDriveConfirmBusinessDelegate();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC79_TestDriveConfirmBusinessDelegate()
	{		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC79_TestDriveConfirmBusinessDelegate getInstance()
	{
		return instance;
	}
	
	public VFC77_TestDriveConfirmationVO getTestDriveByOpportunity(String opportunityId)
	{
		TDV_TestDrive__c sObjTestDrive = null;
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicleAux = null;
		VFC77_TestDriveConfirmationVO testDriveVO = null; 
		VFC77_TestDriveConfirmationVO testDriveVOAux = null;
		
		/*cria ou obtém um test drive existente para essa oportunidade*/
		sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveSchedule(opportunityId);
		
		/*mapeia os dados do objeto para o VO*/
		testDriveVO = this.mapToValueObject(sObjTestDrive);	 
				 
		/*obtém a lista de veículos que estão disponíveis para test drive (parâmetros: id da concessionária e veículo de interesse)*/			                                                                    
		lstSObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getVehiclesAvailable(testDriveVO.dealerId, testDriveVO.vehicleInterest);
																							  
		if(!lstSObjTestDriveVehicle.isEmpty())
		{
			testDriveVO.lstSObjTestDriveVehicleAvailable = lstSObjTestDriveVehicle;
		
			testDriveVO.dateBookingNavigation = Date.newInstance(testDriveVO.dateBooking.year(), testDriveVO.dateBooking.month(), testDriveVO.dateBooking.day());
																							  																						  		
			/*obtém a agenda do veículo*/
			testDriveVOAux = this.getAgendaVehicle(testDriveVO);																				  																							  
			 
			testDriveVO.lstSObjTestDriveAgenda = testDriveVOAux.lstSObjTestDriveAgenda;
		}
							 	
		return testDriveVO;
	}
	
	public VFC77_TestDriveConfirmationVO getAgendaVehicle(VFC77_TestDriveConfirmationVO testDriveVO)
	{
		List<TDV_TestDrive__c> lstSObjTestDrive = null;
		VFC77_TestDriveConfirmationVO testDriveVOResponse = new VFC77_TestDriveConfirmationVO();
		
		lstSObjTestDrive = VFC30_TestDriveBO.getInstance().getTestsDriveScheduledOrConfirmed(testDriveVO.testDriveVehicleId, testDriveVO.dateBookingNavigation);
		
		testDriveVOResponse.lstSObjTestDriveAgenda = lstSObjTestDrive; 
		
		return testDriveVOResponse; 
	}
	
	public VFC77_TestDriveConfirmationVO getVehiclesAvailable(VFC77_TestDriveConfirmationVO testDriveVO)
	{
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicleAux = null;
		VFC77_TestDriveConfirmationVO testDriveVOAux = new VFC77_TestDriveConfirmationVO();
		
		/*obtém a lista de veículos que estão disponíveis para test drive (parâmetros: veículo de interesse e id da concessionária)*/		
		lstSObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getVehiclesAvailable(testDriveVO.dealerId, testDriveVO.vehicleInterest);
		
		/*verifica se a busca retornou registros*/
		if(!lstSObjTestDriveVehicle.isEmpty())
		{
			/*obtém o primeiro veículo da lista*/
			sObjTestDriveVehicleAux = lstSObjTestDriveVehicle.get(0);
			
			testDriveVOAux.testDriveVehicleId = sObjTestDriveVehicleAux.Id;
			testDriveVOAux.dateBookingNavigation = Date.today();
			
			/*obtém a agenda desse veículo*/
			testDriveVOAux = this.getAgendaVehicle(testDriveVOAux);
			
			testDriveVOAux.model = sObjTestDriveVehicleAux.Vehicle__r.Model__c;
			testDriveVOAux.version = sObjTestDriveVehicleAux.Vehicle__r.Version__c;
			testDriveVOAux.color = sObjTestDriveVehicleAux.Vehicle__r.Color__c;		
			testDriveVOAux.plaque = sObjTestDriveVehicleAux.Vehicle__r.VehicleRegistrNbr__c;
			testDriveVOAux.dateBookingNavigation = Date.today();
			
			testDriveVOAux.lstSObjTestDriveVehicleAvailable = lstSObjTestDriveVehicle;
		}
		                                                                    	
		return testDriveVOAux;
	}
	
	public VFC77_TestDriveConfirmationVO getVehicleDetails(VFC77_TestDriveConfirmationVO testDriveVO)
	{
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC77_TestDriveConfirmationVO testDriveVOResponse = null;
		
		/*obtém o veículo pelo id*/	
		sObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getById(testDriveVO.testDriveVehicleId); 
			
		/*obtém o VO contendo a lista de tests drive agendados/confirmados para esse veículo*/
		testDriveVOResponse = this.getAgendaVehicle(testDriveVO);
			
		/*seta o restante dos dados no objeto de resposta*/
		testDriveVOResponse.model = sObjTestDriveVehicle.Vehicle__r.Model__c;
		testDriveVOResponse.version = sObjTestDriveVehicle.Vehicle__r.Version__c;
		testDriveVOResponse.color = sObjTestDriveVehicle.Vehicle__r.Color__c;		
		testDriveVOResponse.plaque = sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c;

		return testDriveVOResponse;
	}
	
	public void updateTestDrive(VFC77_TestDriveConfirmationVO testDriveVO)
	{
		TDV_TestDrive__c sObjTestDrive = this.mapToTestDriveObject(testDriveVO);
		
		VFC30_TestDriveBO.getInstance().updateTestDrive(sObjTestDrive);
	}
		
	private VFC77_TestDriveConfirmationVO mapToValueObject(TDV_TestDrive__c sObjTestDrive)
	{
		VFC77_TestDriveConfirmationVO testDriveVO = new VFC77_TestDriveConfirmationVO();
		
		/*popula o VO*/
		testDriveVO.id = sObjTestDrive.Id;
		testDriveVO.opportunityId = sObjTestDrive.Opportunity__c;
		testDriveVO.dealerId = sObjTestDrive.Opportunity__r.Dealer__c;
		testDriveVO.testDriveVehicleId = sObjTestDrive.TestDriveVehicle__c;			
		testDriveVO.model = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c;
		testDriveVO.version = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Version__c;
		testDriveVO.color = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Color__c;
		testDriveVO.plaque = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.VehicleRegistrNbr__c;
		testDriveVO.dateBooking = sObjTestDrive.DateBooking__c;
		testDriveVO.name = sObjTestDrive.Name;
		testDriveVO.sellerName = UserInfo.getName();
		testDriveVO.opportunityName = sObjTestDrive.Opportunity__r.Name; 
		testDriveVO.status = sObjTestDrive.Status__c;	
		testDriveVO.customerName = sObjTestDrive.Opportunity__r.Account.Name; 
		testDriveVO.persLandline = sObjTestDrive.Opportunity__r.Account.PersLandline__c;
		testDriveVO.persMobPhone = sObjTestDrive.Opportunity__r.Account.PersMobPhone__c;
		
		if(String.isNotEmpty(sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c))
		{
			testDriveVO.vehicleInterest = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c;
		}
		else
		{
			testDriveVO.vehicleInterest = sObjTestDrive.Opportunity__r.Account.VehicleInterest_BR__c;
		}
		
		if(sObjTestDrive.Opportunity__r.Account.YrReturnVehicle_BR__c != null)
		{
			testDriveVO.yearCurrentVehicle = sObjTestDrive.Opportunity__r.Account.YrReturnVehicle_BR__c.intValue();
		}
		
		return testDriveVO;
	}
	
	private TDV_TestDrive__c mapToTestDriveObject(VFC77_TestDriveConfirmationVO testDriveVO)
	{
		TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c(Id = testDriveVO.Id);
		
		sObjTestDrive.TestDriveVehicle__c = testDriveVO.testDriveVehicleId;		
		sObjTestDrive.Status__c = testDriveVO.status;
		sObjTestDrive.ReasonCancellation__c = testDrivevO.reasonCancellation;
		sObjTestDrive.DateBooking__c = testDriveVO.dateBooking;

		return sObjTestDrive;
	}
	
	
}