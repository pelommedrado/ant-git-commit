@isTest
private class VFC157_VehicleTrackingControllerTest {
  
  @isTest static void test_method_one() {

    ALR_Tracking__c tracking = new ALR_Tracking__c();
    Database.insert( tracking );

    ApexPages.StandardController stdController = new ApexPages.StandardController( tracking );
    VFC157_VehicleTrackingController controller = new VFC157_VehicleTrackingController( stdController );
    controller = new VFC157_VehicleTrackingController();

    List< SelectOption > dummyList = controller.deliveryStatusOptionList;
    dummyList = controller.lockStatusOptionList;
    dummyList = controller.modelOptionList;
    dummyList = controller.vehiclePositionOptionList;

    List< ALR_Tracking__c > trackingList = controller.vehicleTrackingList;
    controller.arrival.ALR_Date_MADP__c = System.today();
    controller.selectedTrackingsIdListAsString = JSON.serialize( new List< Id >{ trackingList[0].Id } );
    controller.setArrivalDate();

    Database.delete( tracking );
    controller.setArrivalDate();

    String dummyStr = controller.selectedTrackingsIdListAsString;

    controller.exportExcel();
  }

}