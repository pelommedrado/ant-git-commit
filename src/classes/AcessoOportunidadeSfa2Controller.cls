public with sharing class AcessoOportunidadeSfa2Controller {
    
    private List<Id> selectedOppIdList { get;set; }
    private Id accDealerId { get;set; }
    
    private Map<Id, Opportunity> oppMap { get;set; }
    private Map<Id, Id> oppOwnerMap { get;set; }
    
    public AcessoOportunidadeSfa2Controller() {
        String userBIR = 
            Sfa2Utils.obterBirConcessionaria(Userinfo.getUserId());
        
        if(userBIR != null) {
            Account acc = [
                SELECT Id 
                FROM Account
                WHERE RecordType.DeveloperName = 'Network_Site_Acc'
                AND IDBIR__c =: userBIR
            ];
            
            if(acc != null) {
                this.accDealerId = acc.Id;    
            }     
        }

        system.debug('### userBIR: '+userBIR);
        
        loadOppMap();
        
        this.selectedOppIdList = new List<Id>();
    }
    
    public List<SelectOption> sellerSelectList {
        get {
            String bir = Sfa2Utils.obterBirConcessionaria(Userinfo.getUserId());
            List<User> userList = Sfa2Utils.obterVendedoresConcessionaria(bir);
            userList.sort();
            List<SelectOption> sellerSelectList = new List<SelectOption>();
            sellerSelectList.add(new SelectOption('', '--' + Label.OPPNone + '--' ));
            for(User u : userList) {
                sellerSelectList.add(new SelectOption(u.Id, u.Name));
            }      
            return sellerSelectList;
        }
    }
    
    public List<SolicitacaoOpp> oppList {
        get {
            List<SolicitacaoOpp> solicList = new List<SolicitacaoOpp>();
            List<Opportunity> oppList = oppMap.values();
            if(oppList.isEmpty()) return solicList;
            
            Set<Id> solicitantesId = new Set<Id>();
            for(Opportunity op : oppList) {
                solicitantesId.add(op.Ownership_requested_by_ID__c);
            }
            List<User> userList = [
                SELECT Id, LastName, FirstName
                FROM User
                WHERE Id =: solicitantesId
            ];
            Map<Id, User> userMap = new Map<Id, User>(userList);
            
            for(Opportunity op : oppList) {
                User userSolic = userMap.get(op.Ownership_requested_by_ID__c);
                solicList.add(new SolicitacaoOpp(userSolic, op));
            }
            return solicList;
        }
    }
    
    public String oppIdListAsString {
        get {
            return JSON.Serialize( selectedOppIdList );
        }
        set { 
            //selectedOppIdList = (List< Id >)Json.Deserialize( value, List< Id >.class );
            oppOwnerMap = (Map< Id, Id >) Json.Deserialize(value, Map< Id, Id >.class );
        }
    }
  
    public void loadOppMap() {
        List<Opportunity> oppList = Database.query( 
            'SELECT Id, Name, StageName, CreatedDate, Vehicle_of_Interest__c, VehicleInterest__c, ' + 
            'OpportunitySubSource__c, Campaign_Name__c, OwnerId, Ownership_requested_by_ID__c ' +
            'FROM Opportunity ' +
            'WHERE Ownership_requested_by_ID__c <> null ' +
            //'AND OwnerId = \'' + Userinfo.getUserId() + '\' ' +
            //'AND StageName = \'Identified\' ' +
            //(!String.isBlank(campaignFilter) ? 'AND Campaign_Name__c = \'' + campaignFilter + '\' ' : '') +
            //'AND Dealer__c = \'' + accDealerId + '\' ' +
            'ORDER BY CreatedDate ASC'
        );

        system.debug('### oppList: '+oppList);
        
        this.oppMap = new Map<Id, Opportunity>(oppList);
    }
  
    public void forward() {
        
        try {
            List<Opportunity> modifiedOppList = new List<Opportunity>();
            
            for(Id oppId : new List<Id>(oppOwnerMap.keySet())) {
                modifiedOppList.add(new Opportunity(
                    Id = oppId, 
                    OwnerId = (Id) oppOwnerMap.get(oppId),
                    Ownership_requested_by_ID__c = null));
            }
            
            System.debug('@@@ BEFORE UPDATE: ' + Json.SerializePretty(modifiedOppList));
            Database.update(modifiedOppList);
            
            System.debug('@@@ AFTER UPDATE: ' + Json.SerializePretty(modifiedOppList));
            
            Apexpages.addMessage(new Apexpages.Message( 
                ApexPages.Severity.CONFIRM, 
                modifiedOppList.size() + ' Oportunidade(s) encaminha(s) com sucesso'));
            
        } catch(Exception ex) {
            System.debug('Ex:' + ex);
            Apexpages.addMessage(
                new Apexpages.Message(ApexPages.Severity.ERROR, ex.getMessage() ) );
        }
        
        loadOppMap();
    }
    
    public class SolicitacaoOpp {
        /** Oportunidade de solicitacao de acesso **/
        public Opportunity opp { get;set;}
        /** Usuario solicitante **/
        public User user {get;set;}
        
        public SolicitacaoOpp(User user, Opportunity opp) {
            this.user = user;
            this.opp = opp;
        }
    }
}