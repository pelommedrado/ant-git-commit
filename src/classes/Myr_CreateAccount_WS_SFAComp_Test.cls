/*  Test class for Myr_Create_Account_WS
*************************************************************************************************************
23.002.2016: Dedicated class to test compatibility with SFA
************************************************************************************************************/
@isTest 
private class Myr_CreateAccount_WS_SFAComp_Test {

	private static final String PARTNER_EMAIL = 'sfa.partnerFrance@test.com';

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, true) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
		//Prepare a partner user to test the SFA compatibility
		User partnerUser = Myr_Datasets_Test.getPartnerUser('sfa', 'partnerFrance', PARTNER_EMAIL, 'France');
		insert partnerUser;
	}

	//------------------------------------------------------------------------------------------------------------------------
	//------ LMT/SFA COMPATIBILITY

	//Change the ownership when the owner has not a SalesForce standard license to be able to create the community user
	static testMethod void test_LMTSFA_ChangeOwnership() {
		//1 --- prepare the data
		//Get the tek user
		User tekUser = Myr_Datasets_Test.getTechnicalUserWithRole('France');

		String firstname = 'sfa';
		String lastname = 'personalAcc';
		String email = 'sfa.personalAcc@testu.com';
		Account persAcc = new Account();
		//Get the partner user created in the test method
		User partnerUser = [SELECT Id FROM User WHERE Username = :PARTNER_EMAIL];
		//Very hard to do implement a case in SAlesForce test method : inser ta personal account as a partner community user
		//Other requirement: this should work on ALL the orgs. As a consequence we cannot use LMT implementation
		//1. Insert the partner user (Account > Contact > OPartner User) in the testsetup
		//2. Insert the personal account in a runAs techUser and Specify the Owner as the paterne user
		//     -> impossible to directly use runAs partner because the standard license Partneer Community USer has no 
		//			access to the proper record types :-(
		//     -> forced to use a runAs tekuser to avoid fu... MIXED_DML_EXCEPTION !
		system.runAs( tekUser ) {
			Account acc = new Account();
			persAcc.RecordTypeId = Myr_Users_Utilities_Class.getPersonalAccountRecordType();
			persAcc.FirstName = firstname;
			persAcc.LastName = lastname;
			persAcc.Country__c = 'France';
			persAcc.MyRenaultID__c = email;
			persAcc.MyDaciaID__c = email;
			persAcc.PersEmailAddress__c = email;
			persAcc.OwnerId = partnerUser.Id;
			insert persAcc; 
		} 
		//Check that the ownerId is the partner user
		persAcc = [SELECT Id, OwnerId, Country__c FROM Account WHERE Id = :persAcc.Id];
		system.assertEquals(partnerUser.Id, persAcc.OwnerId);
		//2 --- trigger the test: call CreateAccount and try to create a community user
		Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question req = new Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question();
	    req.country='France';
	    req.accountSource='MYR';
	    req.accountSubSource='STANDARD';
	    req.emailAddress = email;
	    req.firstname = firstname;
	    req.lastname = lastname;
	    req.accountBrand='Renault';
	    req.city='Paris';

		Test.startTest();
		system.runAs( tekUser ) {
			Myr_CreateAccount_WS.Myr_CreateAccount_WS_Response res = Myr_CreateAccount_WS.createAccount(req);
			system.AssertEquals( 'WS01MS001', res.info.code );
			system.AssertEquals( persAcc.Id, res.accountSfdcId );
			persAcc = [SELECT Id, OwnerId, Country__c FROM Account WHERE Id = :res.accountSfdcId];
			//now the owner is the myr tek user
			system.assertEquals( tekUser.Id, persAcc.OwnerId );
			//try to create the community user now
			Myr_UserActivation_WS.Myr_UserActivation_WS_Response response = 
					Myr_UserActivation_WS.activateUser(res.accountSfdcId, '1', 'GMT', 'fr_FR_EURO', 'fr','EUR', '');
			system.AssertEquals( 'WS02MS000', response.info.code );
			
		}
		Test.stopTest();
	}

	//DO NOT change the ownership when the owner has a SalesForce standard license
	static testMethod void test_LMTSFA_DoNotChangeOwnership() {
		//1 --- prepare the data
		//Get standard users
		User tekUser = Myr_Datasets_Test.getTechnicalUserWithRole('France');
		Profile updProfile = Myr_Users_Utilities_Class.getProfile('HeliosMyRImport');
		User updUser = Myr_Datasets_Test.getUser('myr', 'importFrance', 'France', '', updProfile.Id);
		updUser.UserRoleId = Myr_Datasets_Test.getValidRole( 'France' );
		String firstname = 'sfa';
		String lastname = 'personalAcc';
		String email = 'sfa.personalAcc@testu.com';
		Account persAcc = new Account();
		//Get the partner user created in the test method
		system.runAs( updUser ) {
			persAcc = Myr_Datasets_Test.insertPersonalAccountAndUser(firstname, lastname, '', email, '', '', 'NO_USER','', '', '', '', '', '');
			persAcc = [SELECT Id, OwnerId, Country__c FROM Account WHERE Id = :persAcc.Id];
			system.assertEquals(updUser.Id, persAcc.OwnerId);
		}
		//2 --- trigger the test: call CreateAccount and try to create a community user
		Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question req = new Myr_CreateAccount_WS.Myr_CreateAccount_WS_Question();
	    req.country='France';
	    req.accountSource='MYR';
	    req.accountSubSource='STANDARD';
	    req.emailAddress = email;
	    req.firstname = firstname;
	    req.lastname = lastname;
	    req.accountBrand='Renault';
	    req.city='Paris';

		Test.startTest();
		system.runAs( tekUser ) {
			Myr_CreateAccount_WS.Myr_CreateAccount_WS_Response res = Myr_CreateAccount_WS.createAccount(req);
			system.AssertEquals( 'WS01MS001', res.info.code );
			system.AssertEquals( persAcc.Id, res.accountSfdcId );
			persAcc = [SELECT Id, OwnerId, Country__c FROM Account WHERE Id = :res.accountSfdcId];
			//the owner has not been changed
			system.assertEquals( updUser.Id, persAcc.OwnerId );
			//try to create the community user now
			Myr_UserActivation_WS.Myr_UserActivation_WS_Response response = 
					Myr_UserActivation_WS.activateUser(res.accountSfdcId, '1', 'GMT', 'fr_FR_EURO', 'fr','EUR', '');
			system.debug('### test_LMTSFA_DoNotChangeOwnership - response = ' + response );
			system.AssertEquals( 'WS02MS000', response.info.code );
			
		}
		Test.stopTest();
	}
}