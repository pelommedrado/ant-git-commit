// Test Class for Update the conflicts Account - Reena J
@isTest
public with sharing class TestVFC19_W2CAccountupdatePageController {
    
    static testMethod void myUnitTest() {    
        User usr = new User (LastName='ts1',
                             FirstName='Rotondo1', 
                             alias='lro',
                             Email='lrotondo@rotondo.com',
                             EmailEncodingKey='UTF-8',
                             LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US',
                             ProfileId='00eD0000001PhWZIA0',
                             RecordDefaultCountry__c = 'Brazil',
                             TimeZoneSidKey='America/Los_Angeles', 
                             UserName='lrotondo@lrotondo.com');
        
        Test.startTest();
        
        System.runas(usr) {
            Account Acc = new Account(Name='Test1',
                                      Phone='0000',
                                      RecordTypeId='012D0000000KAoFIAW', 
                                      ProfEmailAddress__c = 'addr1@mail.com', 
                                      ShippingCity = 'Paris', 
                                      ShippingCountry = 'France', 
                                      ShippingState = 'IDF', 
                                      ShippingPostalCode = '75013', 
                                      ShippingStreet = 'my street');
            insert Acc;
            
            Contact Con = new Contact(LastName='Test Contact',
                                      FirstName='Test', 
                                      Salutation='Mr.',
                                      ProEmail__c='lro@lro.com',
                                      AccountId=Acc.Id);
            insert Con;
            
            Case c = new Case(Origin='Renault Site',
                              Type='Information Request',
                              SubType__c ='Booking',
                              Status='New',Priority='Medium',
                              Description='Description',
                              From__c='Customer',
                              AccountId=Acc.Id,
                              ContactId=Con.Id);
            try {
                insert c;
                
            } catch(Exception e){
            }
            
            ApexPages.currentPage().getParameters().put('caseId',c.Id); 
            ApexPages.currentPage().getParameters().put('upFirstName','reenaj');
            //ApexPages.StandardController controller1=new ApexPages.StandardController(c);
            
            VFC19_W2CAccountupdatePageController tst1 = new VFC19_W2CAccountupdatePageController();
            
            tst1.upFirstName 	= 'upFirstName';
            tst1.upLastName 	= 'upLastName';
            tst1.upPhone 		= 'upPhone';
            tst1.upEmail 		= 'upEmail';
            tst1.upStreet 		= 'upStreet';
            tst1.upCity 		= 'upCity';
            tst1.upName			= 'upName';
            tst1.upState        = 'upState';
            
            tst1.getAccountIds();
            tst1.getCaseAccList();
            tst1.getAccDetail();
            tst1.bckToCase();
            tst1.getRecordsToDisplay();
            tst1.processSelected();
            tst1.processSelectedcmd();
            
            
        }
        Test.StopTest();
    }
}