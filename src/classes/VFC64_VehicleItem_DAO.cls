/**
*	Class		-   VFC64_VehicleItem_DAO
*   Author 		-   Suresh Babu
*   Date   		-   12/03/2013
*   Description	-	DAO class for Vehcile Item object to query records.
*/

public class VFC64_VehicleItem_DAO extends VFC01_SObjectDAO{
	private static final VFC64_VehicleItem_DAO instance = new VFC64_VehicleItem_DAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC64_VehicleItem_DAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC64_VehicleItem_DAO getInstance(){
        return instance;
    }
    
    /**
    	Method used to fetch all Item records.
    **/
    public List<Vehicle_Item__c> fetchAll_VehicleItem_Records(){
    	List<Vehicle_Item__c> lstAllVehicleItems = new List<Vehicle_Item__c>();
    	
    	lstAllVehicleItems = [SELECT 
    								isActive__c, Category__c, Item__c, ModelVersion__c, Name, Id 
    							FROM 
    								Vehicle_Item__c
    							WHERE
    								Name != null
    							];
    	return lstAllVehicleItems;
    }
    
    /**
    	Method used to return Map Key as Item -> Name and List of Vehicle Item records.
    	This method returns only the Color record type records..
    **/
    public Map<String, List<Vehicle_Item__c>> return_Map_ItemKey_and_VehicleItem(){
    	Id Color_RecordTypeID = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Color' );
    	Map<String, List<Vehicle_Item__c>> map_ItemKey_Vitem = new Map<String, List<Vehicle_Item__c>>();
    	List<Vehicle_Item__c> lstAllVehicleItems = new List<Vehicle_Item__c>();
    	
    	lstAllVehicleItems = [SELECT 
    								isActive__c, Category__c, Item__c, Item__r.Name,
    								ModelVersion__c, ModelVersion__r.ProductCode, Name, Id 
    							FROM 
    								Vehicle_Item__c 
    							WHERE 
    								Item__r.RecordTypeId =: Color_RecordTypeID
    							];
    	for( Vehicle_Item__c VItem : lstAllVehicleItems ){
    		List<Vehicle_Item__c> tempVehicles = new List<Vehicle_Item__c>();
    		if( map_ItemKey_Vitem.containsKey( VItem.Item__r.Name )){
    			tempVehicles = map_ItemKey_Vitem.get( VItem.Item__r.Name );
    			tempVehicles.add( VItem );
    			map_ItemKey_Vitem.put( VItem.Item__r.Name, tempVehicles );
    		}
    		else{
    			tempVehicles.add( VItem );
    			map_ItemKey_Vitem.put( VItem.Item__r.Name, tempVehicles );
    		}
    	}
    	return map_ItemKey_Vitem;
    }
    
    
    /**
    	Method returns the Vehicle Item records,which combained with the Product Version and its Item.
    	This method only returns the Option Record type records..
    **/
    public Map<String, Map<String, Vehicle_Item__c>> return_Map_ModelCode_ItemCode_VehicleItems( String modelKey ){
    	Id option_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Option' );
		Map<String, Map<String, Vehicle_Item__c>> map_AllRecords = new Map<String, Map<String, Vehicle_Item__c>>();
		List<Vehicle_Item__c> lstAllVehicleItems = new List<Vehicle_Item__c>();
    	
    	lstAllVehicleItems = [SELECT 
    								isActive__c, Category__c, Item__c, Item__r.Name,
    								ModelVersion__c, ModelVersion__r.ProductCode, Name, Id 
    							FROM 
    								Vehicle_Item__c 
    							WHERE 
    								Item__r.RecordTypeId =: option_RecordTypeId
    								AND ModelVersion__r.ProductCode LIKE : modelKey
    							];
    	for( Vehicle_Item__c VItem : lstAllVehicleItems ){
			Map<String, Vehicle_Item__c> inner_Map = new Map<String, Vehicle_Item__c>();
			if( map_AllRecords.containsKey( VItem.ModelVersion__r.ProductCode )){
				inner_Map = map_AllRecords.get( VItem.ModelVersion__r.ProductCode );
				if( !inner_Map.containsKey( VItem.Item__r.Name )){
					inner_Map.put( VItem.Item__r.Name, VItem );
				}
				system.Debug(' inner_Map- -->'+inner_Map);
			}
			else{
				inner_Map.put( VItem.Item__r.Name, VItem );
				
				map_AllRecords.put( VItem.ModelVersion__r.ProductCode, inner_Map );
			}
			system.Debug(' in for loop map_AllRecords -->'+map_AllRecords);
		}
		system.Debug(' overa all map_AllRecords -->'+map_AllRecords);
		return map_AllRecords;
	}
	
	
	public Map<String, List<String>> return_VehicleItem_With_ItemName(){
		Id option_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Option' );
		Map<String, List<String>> map_all_VehicleRecords = new Map<String, List<String>>();
		List<Vehicle_Item__c> lstAllVehicleItems = new List<Vehicle_Item__c>();
    	
    	lstAllVehicleItems = [SELECT 
    								isActive__c, Category__c, Item__c, Item__r.Name,
    								ModelVersion__c, ModelVersion__r.ProductCode, Name, Id 
    							FROM 
    								Vehicle_Item__c 
    							WHERE 
    								Item__r.RecordTypeId =: option_RecordTypeId
    							];
    	for( Vehicle_Item__c vehicle : lstAllVehicleItems ){
    		List<String> temp_String = new List<String>();
    		if( map_all_VehicleRecords.containsKey( vehicle.ModelVersion__r.ProductCode )){
    			temp_String = map_all_VehicleRecords.get( vehicle.ModelVersion__r.ProductCode );
    			temp_String.add( vehicle.Item__r.Name );
    			map_all_VehicleRecords.put( vehicle.ModelVersion__r.ProductCode, temp_String );
    		}
    		else{
    			temp_String.add( vehicle.Item__r.Name );
    			map_all_VehicleRecords.put( vehicle.ModelVersion__r.ProductCode, temp_String );
    		}
    	}
		return map_all_VehicleRecords;
	}

	/**
	* 
	*/		
    public List<Vehicle_Item__c> fetchVehicleItemByModel(String model)
    {
   		Id colorRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Color' );
   		
   		return [ Select 
						Id ,
						ModelVersion__r.Model__c , 
						ModelVersion__c , 
						Item__r.Name , 
						Item__r.Label__c,
						Item__c 
					From 
						Vehicle_Item__c 
					Where
						Item__r.RecordTypeId = :colorRecordTypeId
					And
						ModelVersion__r.Name = :model ];
   }
}