/**
	Class   -   VFC18_VehicleRelationDAO_Test
    Author  -   RameshPrabu
    Date    -   29/10/2012
    
    #01 <RameshPrabu> <29/10/2012>
        Created this class using test for VFC18_VehicleRelationDAO.
**/
@isTest
private class VFC18_VehicleRelationDAO_Test {

	static testMethod void VFC18_VehicleRelationDAO_Test()
	{
		// Prepare data
        //List<VEH_Veh__c> lstVehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehicleObjects();
        
        List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        List<VRE_VehRel__c> lstVehRel = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehRelObjects(lstDealerRecords);
        Id dealerId = lstVehRel[3].Account__c;
        
        List<VEH_Veh__c> lstVehicle = VFC21_VehicleDAO.getInstance().fetchVehicleRecords();

        // Start test
        Test.startTest();
        
        system.debug('*** lstVehRel[0].VehicleRegistrNbr__c='+lstVehRel[0].VehicleRegistrNbr__c);
        system.debug('*** lstVehicle[0].Name='+lstVehicle[0].Name);

        List<VRE_VehRel__c> vehicleTest1 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationRecords(lstVehRel[0].VehicleRegistrNbr__c, lstVehicle[0].Name);
        List<VRE_VehRel__c> vehicleTest2 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationRecords(lstVehRel[0].VehicleRegistrNbr__c, '');
        List<VRE_VehRel__c> vehicleTest3 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationRecords('', lstVehicle[0].Name);
        List<VRE_VehRel__c> vehicleTest4 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationRecords('XXX-0000', '0123456789');
        List<VRE_VehRel__c> vehicleTest5 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationRecords('XXX-0000', null);

		List<VRE_VehRel__c> test4 = VFC18_VehicleRelationDAO.getInstance().fetchVREUsingAccountIDBIR('1111');

		Map<String, VRE_VehRel__c> test5 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationUsingVIN(new Set<String>{'12345678'}, dealerId);

	    Map<String, VRE_VehRel__c> test6 = VFC18_VehicleRelationDAO.getInstance().fetchVehicleRelationUsingVehicleIds(dealerId, new List<Id>{lstVehRel[0].Id});

        // Stop test
        Test.stopTest();
    }
}