/**
* Classe que provê funcionalidades de negócio relacionadas ao objeto Profile.
* @author Felipe Jesus Silva.
*/
public class VFC52_ProfileBO 
{
	private static final VFC52_ProfileBO instance = new VFC52_ProfileBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC52_ProfileBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC52_ProfileBO getInstance()
	{
		return instance;
	}
	
	public Boolean isProfile(String profileId, String profileName)
	{
		Boolean isProfile = false;
		Profile sObjProfile = VFC32_ProfileDAO.getInstance().findById(profileId);
		
		/*verifica se o perfil foi localizado*/
		if(sObjProfile != null)
		{
			isProfile = sObjProfile.Name.equalsIgnoreCase(profileName);
		}
		
		return isProfile;	
	}
}