public with sharing class WSC06_DMSPreOrderBO
{
    private static final WSC06_DMSPreOrderBO instance = new WSC06_DMSPreOrderBO();
        
    /*private constructor to prevent the creation of instances of this class*/
    private WSC06_DMSPreOrderBO(){}

    /**
     * Method responsible for providing the instance of this class..
     */  
    public static WSC06_DMSPreOrderBO getInstance(){
        return instance;
    }
    
    public List<Account> fetch_DealerAccountsUsindIDBIR( String stringBIR ){
        List<Account> lstDealerAccounts = VFC12_AccountDAO.getInstance().checkAccountsWithIDBIR(stringBIR);
        return lstDealerAccounts;
    }

    public List<Account> fetchRecordsUsingIDBIRNumber( Set<Id> setDealerIds ){
        List<Account> lstAccountRecords = VFC12_AccountDAO.getInstance().fetchAccountForPreOrderWS( setDealerIds );
        return lstAccountRecords;
    }
    
    //Return PreOrderVO objects
    public List<WSC04_PreOrderDMSVO> returnPreOrderDMS(String stringBIR)
    {
        // Returned list
        List<WSC04_PreOrderDMSVO> lstPreOrder = new List<WSC04_PreOrderDMSVO>();

        // Fetch Dealer
        Account dealer = fetch_DealerAccountsUsindIDBIR(stringBIR)[0];      
        Set<Id> setDealerIds = new Set<Id>();
        setDealerIds.add(dealer.Id);
        
        system.debug('*** dealer.Id:'+dealer.id);
        
        List<Quote> lstQuotes = VFC35_QuoteDAO.getInstance().fetchQuoteRecords_UsingDealerId(dealer.id);
        system.debug('*** lstQuotes.size():'+lstQuotes.size());

        // Get Customer data
        Set<Id> setAccountCustomerId = new Set<Id>();
        Set<Id> setUserSalespersonId = new Set<Id>();
        Set<Id> setQuoteId = new Set<Id>();
        Set<Id> setMolicarId = new Set<Id>();
        for (Quote aQuote : lstQuotes) {
            setAccountCustomerId.add(aQuote.BillingAccount__c);
            setUserSalespersonId.add(aQuote.Opportunity.OwnerId);
            setQuoteId.add(aQuote.Id);
            setMolicarId.add(aQuote.UsedVehicle__c);
        }
        
        system.Debug('*** setAccountCustomerId ---> '+setAccountCustomerId);
        system.Debug('*** setUserSalespersonId ---> '+setUserSalespersonId);
        system.Debug('*** setQuoteId ---> '+setQuoteId);
        system.Debug('*** setMolicarId ---> '+setMolicarId);
        
        List<Account> lstAccountCustomer = VFC12_AccountDAO.getInstance().fetchCustomersUsing_SetIds(setAccountCustomerId);
        
        Map<Id, Account > mapAccountCustomer = new Map<Id, Account>();
        for (Account anAccount : lstAccountCustomer) {
            mapAccountCustomer.put(anAccount.Id, anAccount);
        }

        // Get Salesperson data
        List<User> lstUserSalesperson = VFC25_UserDAO.getInstance().fetchUserRecords_UsingSalesPersonIds(setUserSalespersonId);
        system.debug('*** lstUserSalesperson' + lstUserSalesperson);
                
        Map<Id, User> mapUserSalesperson = new Map<Id, User>();
        for (User anUser : lstUserSalesperson) {
            mapUserSalesperson.put(anUser.Id, anUser);
        }

        system.debug('*** setQuoteId=' + setQuoteId);

        //  Get Product data
        List<QuoteLineItem> lstQuoteLineItem = VFC36_QuoteLineItemDAO.getInstance().fetchQuoteLineItems_UsingQuoteIds(setQuoteId);
        
        Map<Id, QuoteLineItem> mapQuoteLineItem = new Map<Id, QuoteLineItem>();
        for (QuoteLineItem aQuoteLine : lstQuoteLineItem) {
            if (aQuoteLine.Vehicle__c != null)
                mapQuoteLineItem.put(aQuoteLine.QuoteId, aQuoteLine);
        }
        
        system.debug('*** lstQuoteLineItem=' + lstQuoteLineItem.size());
        system.debug('*** mapQuoteLineItem=' + mapQuoteLineItem.size());
        
        //  Get Molicar data
        List<MLC_Molicar__c> lstMolicar = VFC11_MolicarDAO.getInstance().fetchMolicarRecords_UsingCustomerIDs(setMolicarId);
        
        Map<Id, MLC_Molicar__c> mapMolicar = new Map<Id, MLC_Molicar__c>();
        for (MLC_Molicar__c molicar : lstMolicar) {
            mapMolicar.put(molicar.Id, molicar);
        }
        
        system.Debug('*** lstQuotes --->'+lstQuotes);
        
        // Create return List
        for (Quote aQuote : lstQuotes)
        {
            system.debug('*** OpportunityId:'+aQuote.Opportunity.Id);
            System.debug('*** aQuote:' + aQuote);
             
            WSC04_PreOrderDMSVO preOrderVO = new WSC04_PreOrderDMSVO();

            // Quote Record information
            if( aQuote.QuoteNumber != null || aQuote.QuoteNumber != '' ){
                preOrderVO.orderNumber = aQuote.QuoteNumber;
            }
            if( aQuote.Status != null || aQuote.Status != '' ){
                preOrderVO.status = aQuote.Status;
            }
            if( aQuote.InvoiceNumber__c != null || aQuote.InvoiceNumber__c != '' ){
                preOrderVO.invoiceNumber = aQuote.InvoiceNumber__c;
            }
            if( aQuote.DMSNumber__c != null ){
                preOrderVO.orderNumberDMS = String.valueOf(aQuote.DMSNumber__c);
            }
            if( aQuote.CreatedDate != null ){
                preOrderVO.creationDate = date.newinstance(aQuote.CreatedDate.year(), aQuote.CreatedDate.month(), aQuote.CreatedDate.day());
            }
            if( aQuote.ExpirationDate != null ){
                preOrderVO.expirationDate = aQuote.ExpirationDate;
            }
            if( aQuote.TotalPrice != null ){
                preOrderVO.value = aQuote.TotalPrice;
            }
            if( aQuote.PaymentMethods__c != null || aQuote.PaymentMethods__c != '' ){
                preOrderVO.paymentType = aQuote.PaymentMethods__c;
            }
            if( aQuote.Entry__c != null ){
                preOrderVO.entryValue = aQuote.Entry__c;
            }
            if( aQuote.YearUsedVehicle__c != null ){
                preOrderVO.usedCarYear = Integer.valueOf( aQuote.YearUsedVehicle__c );
            }
            if( aQuote.PriceUsedVehicle__c != null ){
                preOrderVO.usedCarValue = aQuote.PriceUsedVehicle__c;
            }
            if( aQuote.UsedVehicleLicensePlate__c != null || aQuote.UsedVehicleLicensePlate__c != '' ){
                preOrderVO.usedCarLicencePlate = aQuote.UsedVehicleLicensePlate__c;
            }
            if( aQuote.FinancingAgent__c != null || aQuote.FinancingAgent__c != '' ){
                preOrderVO.fundingAgent = aQuote.FinancingAgent__c;
            }
            if( aQuote.FinancingType__c != null || aQuote.FinancingType__c != '' ){
                preOrderVO.fundingType = aQuote.FinancingType__c;
            }
            if( aQuote.AmountFinanced__c != null ){
                preOrderVO.fundingValue = aQuote.AmountFinanced__c;
            }
            if( aQuote.NumberOfInstallments__c != null ){
                preOrderVO.installmentsNumber = Integer.valueOf(aQuote.NumberOfInstallments__c);
            }
            if( aQuote.ValueOfInstallments__c != null ){
                preOrderVO.installmentsValue = aQuote.ValueOfInstallments__c;
            }
            if( aQuote.LastModifiedDate != null ){
                preOrderVO.lastUpdateDate = aQuote.LastModifiedDate;
            }
            if( aQuote.PaymentDetails__c!= null ){
               preOrderVO.paymentDetails = aQuote.PaymentDetails__c;
            }


            // Customer Record Informations
            System.debug('>>>>>>> mapAccountCustomer' + mapAccountCustomer);
            System.debug('>>>>>>> aQuote.BillingAccount__c' + aQuote.BillingAccount__c);
            Account customerAccount = mapAccountCustomer.get(aQuote.BillingAccount__c);
            preOrderVO.CPF_CNPJ = customerAccount.CustomerIdentificationNbr__c;
            
            //preOrderVO.customerLastName = customerAccount.LastName;
            //preOrderVO.customerName = customerAccount.FirstName;
            preOrderVO.customerName = customerAccount.Name;

            //if(dealer.NewWebServiceAddress__c){

                //preOrderVO.street = customerAccount.Shipping_Street__c;
                //preOrderVO.complement = customerAccount.Shipping_Complement__c;
                //preOrderVO.addressNumber = customerAccount.Shipping_Number__c;
                //preOrderVO.neighborhood = customerAccount.Shipping_Neighborhood__c;

            //} else {

                preOrderVO.streetAndComplement = customerAccount.Shipping_Street__c;
                if( customerAccount.Shipping_Number__c != null ) { preOrderVO.streetAndComplement += ', ' + customerAccount.Shipping_Number__c; }
                if( customerAccount.Shipping_Complement__c != null) { preOrderVO.streetAndComplement += ' - ' + customerAccount.Shipping_Complement__c; }
                if( customerAccount.Shipping_Neighborhood__c != null) { preOrderVO.streetAndComplement += ' - ' + customerAccount.Shipping_Neighborhood__c; }

            //}
                

            preOrderVO.city = customerAccount.Shipping_City__c;
            preOrderVO.zipCode = customerAccount.Shipping_PostalCode__c;
            preOrderVO.state = customerAccount.Shipping_State__c;
            if(customerAccount.Shipping_Country__c != null){
                preOrderVO.country = customerAccount.Shipping_Country__c;
            }else{
                preOrderVO.country = 'BRASIL';
            }

            
            preOrderVO.maritalStatus = customerAccount.MaritalStatus__c;
            //preOrderVO.sex = customerAccount.Sex__c;
            preOrderVO.birthDate = customerAccount.PersonBirthdate != null ? customerAccount.PersonBirthdate : Date.newInstance(1990,1,1);
            //preOrderVO.RG_StateRegistration = String.valueOf(customerAccount.RGStateRegistration__c);
            preOrderVO.RG_StateRegistration = customerAccount.RGStateTexto__c;
                
            preOrderVO.residencialLandline = customerAccount.PersLandline__c;
            
            if(preOrderVO.CPF_CNPJ.length() == 11) {
                preOrderVO.commercialLandline = customerAccount.ProfLandline__c;    
            
            } else {
                preOrderVO.commercialLandline = customerAccount.Phone;
            }
            
            preOrderVO.cellphone = customerAccount.PersMobPhone__c;
            
            //Converte o retorno campo Sexo
            if(customerAccount.Sex__c == 'Man'){
                preOrderVO.sex = 'M';
            }else if(customerAccount.Sex__c == 'Woman'){
                preOrderVO.sex = 'F';
            }else{
                preOrderVO.sex = 'O';
            }
            if(preOrderVO.CPF_CNPJ.length() > 11){
                preOrderVO.customerEmail = customerAccount.ProfEmailAddress__c;
            }else{
                preOrderVO.customerEmail = customerAccount.PersEmailAddress__c;
            }

            // Contact Record information
            User salespersonUser = mapUserSalesperson.get(aQuote.Opportunity.OwnerId);
            preOrderVO.nameSalesperson = salespersonUser.Contact.Name;
            preOrderVO.emailSalesperson = salespersonUser.Contact.Email;
            preOrderVO.phoneSalesperson = salespersonUser.Phone;
            preOrderVO.CPFSalesperson = salespersonUser.Contact.CPF__c;

            // QuoteLineItem Record information
            QuoteLineItem aQuoteLineRecord = mapQuoteLineItem.get(aQuote.Id);
            if (aQuoteLineRecord != NULL)
            {
                // PDT_Accessory -> Accessory Product.
                String product_Accessory = 'PDT_Accessory';
                Id product_AccessoryRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName(product_Accessory);
                List<String> lstQuoteLineItemDescription = new List<String>();
                
                for (QuoteLineItem aQuoteLine : lstQuoteLineItem)
                {
                    if (aQuoteLine.Description != null 
                        && aQuoteLine.PricebookEntry.Product2.RecordTypeId == product_AccessoryRecordTypeId
                        && aQuote.Id == aQuoteLine.QuoteId
                       ) {
                        lstQuoteLineItemDescription.add(aQuoteLine.Description);
                    }
                }
                
                if (lstQuoteLineItemDescription.size() > 0) {
                    preOrderVO.accessoriesList = String.join( lstQuoteLineItemDescription, ', ');
                }
                
                if (aQuoteLineRecord.Vehicle__r.Name != null || aQuoteLineRecord.Vehicle__r.Name != '') {
                    preOrderVO.VIN = aQuoteLineRecord.Vehicle__r.Name;
                }
            }

            // Molicar Record information
            MLC_Molicar__c molicar = mapMolicar.get(aQuote.UsedVehicle__c);
            if (molicar != NULL) {
                if( molicar.Brand__c != null || molicar.Brand__c != '' ){
                    preOrderVO.usedCar = molicar.Brand__c;
                }
                if( molicar.Model__c != null || molicar.Model__c != '' ){
                    preOrderVO.usedCar = preOrderVO.usedCar + '|' + molicar.Model__c;
                }
                if( molicar.Configuration__c != null || molicar.Configuration__c != '' ){
                    preOrderVO.usedCar = preOrderVO.usedCar + '|' + molicar.Configuration__c;
                }
            }

            lstPreOrder.add(preOrderVO);
        }

        system.debug('*** lstPreOrder --->>'+lstPreOrder);
        
        return lstPreOrder;
    }
    
    // update Pre Order records in SFDC
    public List<WSC04_PreOrderDMSVO> updateLstPreOrderDMS(String stringBIR, List<WSC04_PreOrderDMSVO> lstPreOrderDMSVO)
    {
        List<WSC04_PreOrderDMSVO> lstPreOrderDMSWithStatus = new List<WSC04_PreOrderDMSVO>();
        
        List<Account> lstAccount_Records = new List<Account>();
        List<Quote> lstQuote_Records = new List<Quote>(); 
        List<VEH_Veh__c> lstVehicle_Records = new List<VEH_Veh__c>();
        List<Contact> lstContact_Records = new List<Contact>();
        List<Product2> lstProcuct_Records = new List<Product2>();
        
        List<Account> lstAccount_Update = new List<Account>();
        List<Quote> lstQuote_Update = new List<Quote>();
        
        system.Debug('*** input lstPreOrderDMSVO --> '+lstPreOrderDMSVO);
        system.Debug('*** input stringBIR --> '+stringBIR);
        
        Set<String> setQuoteNumber = new Set<String>();
        
        for( WSC04_PreOrderDMSVO validationPreOrder : lstPreOrderDMSVO ){
            if( validationPreOrder.orderNumber != null || validationPreOrder.orderNumber != '' ){
                setQuoteNumber.add( validationPreOrder.orderNumber );
            }
        }
        
        system.debug('**** setQuoteNumber: ' + setQuoteNumber);
        
        Account dealer = fetch_DealerAccountsUsindIDBIR(stringBIR)[0];
        
        system.debug('**** dealer.id: ' + dealer.id);
        
        // Get Quote Records.
        lstQuote_Records = VFC35_QuoteDAO.getInstance().fetchQuoteRecords_UsingQuoteNumber_DealerId(setQuoteNumber, dealer.id);
        
        system.debug('*** lstQuotes :'+lstQuote_Records);
        
        // Get Customer data
        Set<Id> setAccountCustomerId = new Set<Id>();
        Set<Id> setUserSalespersonId = new Set<Id>();
        Set<Id> setQuoteId = new Set<Id>();
        Set<Id> setMolicarId = new Set<Id>();
        for (Quote aQuote : lstQuote_Records) {
            setAccountCustomerId.add(aQuote.BillingAccount__c);
            setUserSalespersonId.add(aQuote.Opportunity.OwnerId);
            setQuoteId.add(aQuote.Id);
            setMolicarId.add(aQuote.UsedVehicle__c);
        }
        
        system.Debug('*** setAccountCustomerId ---> '+setAccountCustomerId);
        system.Debug('*** setUserSalespersonId ---> '+setUserSalespersonId);
        system.Debug('*** setQuoteId ---> '+setQuoteId);
        system.Debug('*** setMolicarId ---> '+setMolicarId);
        
        // Get customer Account Record.
        lstAccount_Records = VFC12_AccountDAO.getInstance().fetchCustomersUsing_SetIds(setAccountCustomerId);
        
        system.Debug('*** lstAccount_Records ---> '+lstAccount_Records);
        system.Debug('*** lstPreOrderDMSVO ---> '+lstPreOrderDMSVO);
        system.Debug('*** lstQuote_Records ---> '+lstQuote_Records);
        
        for (WSC04_PreOrderDMSVO preOrderVO : lstPreOrderDMSVO)
        {
            for (Quote quote_Record : lstQuote_Records)
            {
                if (quote_Record.QuoteNumber == preOrderVO.orderNumber)
                {
                    Quote quoteUpdate = new Quote(Id = quote_Record.Id);
                    
                    quoteUpdate.Status = preOrderVO.status;
                    if(quoteUpdate.InvoiceNumber__c != null)
                        quoteUpdate.InvoiceNumber__c = preOrderVO.invoiceNumber;
                    
                    if (preOrderVO.orderNumberDMS != null)
                        system.debug('*** OrderNumberDMS=' + preOrderVO.orderNumberDMS);
                    
                    if(quoteUpdate.ExpirationDate != null)
                        quoteUpdate.ExpirationDate =  preOrderVO.expirationDate.dateGMT();
                    if(quoteUpdate.PaymentMethods__c != null)
                        quoteUpdate.PaymentMethods__c = preOrderVO.paymentType;
                    if(quoteUpdate.Entry__c != null)
                        quoteUpdate.Entry__c = preOrderVO.entryValue;
                    if(quoteUpdate.YearUsedVehicle__c != null)
                        quoteUpdate.YearUsedVehicle__c = preOrderVO.usedCarYear;
                    if(quoteUpdate.PriceUsedVehicle__c != null)
                        quoteUpdate.PriceUsedVehicle__c = preOrderVO.usedCarValue;
                    if(quoteUpdate.UsedVehicleLicensePlate__c != null)
                        quoteUpdate.UsedVehicleLicensePlate__c = preOrderVO.usedCarLicencePlate;
                    if(quoteUpdate.FinancingAgent__c != null)
                        quoteUpdate.FinancingAgent__c = preOrderVO.fundingAgent;
                    if(quoteUpdate.FinancingType__c != null)
                        quoteUpdate.FinancingType__c = preOrderVO.fundingType;
                    if(quoteUpdate.AmountFinanced__c != null)
                        quoteUpdate.AmountFinanced__c = preOrderVO.fundingValue;
                    if(quoteUpdate.NumberOfInstallments__c != null)
                        quoteUpdate.NumberOfInstallments__c = preOrderVO.installmentsNumber;
                    if(quoteUpdate.ValueOfInstallments__c != null)
                        quoteUpdate.ValueOfInstallments__c = preOrderVO.installmentsValue;
                    if(quoteUpdate.PaymentDetails__c != null)
                        quoteUpdate.PaymentDetails__c = preOrderVO.paymentDetails;
                    
                    lstQuote_Update.add(quoteUpdate);
                    
                    system.Debug('*** lstAccount_Records --> '+lstAccount_Records);
                    
                    for (Account acc : lstAccount_Records)
                    {
                        if (quote_Record.BillingAccount__c == acc.Id)
                        {
                            system.debug('*** Xquote_Record.Opportunity.AccountId='+quote_Record.Opportunity.AccountId);
                            system.debug('*** acc.Id='+acc.Id);
                            
                            Account accUpdate = new Account(Id = acc.Id);
                            accUpdate.CustomerIdentificationNbr__c = preOrderVO.CPF_CNPJ;
                            
                            //accUpdate.LastName = preOrderVO.customerLastName;
                            //accUpdate.LastName = preOrderVO.customerName;
                            //accUpdate.FirstName = preOrderVO.customerName;
                            
                            if(accUpdate.CustomerIdentificationNbr__c.length() > 11){
                                System.debug('%%%CNPJ');
                                accUpdate.Name = preOrderVO.customerName;
                                accUpdate.ProfEmailAddress__c = preOrderVO.customerEmail;
                                
                                accUpdate.Phone = preOrderVO.commercialLandline;
                                
                            } else {
                                // @Hugo Medrado {kolekto} removed required field Customer Name
                                if(preOrderVO.customerName != null && preOrderVO.customerName != ''){
                                    System.debug('%%%CPF');
                                    List<String> sObjLstName = preOrderVO.customerName.split(' ');
                                    
                                    accUpdate.FirstName = sObjLstName.get(0);
                                    accUpdate.LastName = '';
                                    
                                    for (Integer pos = 1; pos <= (sObjLstName.size() - 1) ; pos++) {
                                        accUpdate.LastName = accUpdate.LastName + ' ' + sObjLstName.get(pos);                               
                                    }   
                                }
                                accUpdate.PersEmailAddress__c = preOrderVO.customerEmail;
                                
                                accUpdate.ProfLandline__c = preOrderVO.commercialLandline;
                            }    

                            //if(dealer.NewWebServiceAddress__c){

                                //accUpdate.Shipping_Street__c = preOrderVO.street;
                                //accUpdate.Shipping_Number__c = preOrderVO.addressNumber;
                                //accUpdate.Shipping_Complement__c = preOrderVO.complement;
                                //accUpdate.Shipping_Neighborhood__c = preOrderVO.neighborhood; 

                            //} else{

                                accUpdate.Shipping_Street__c = preOrderVO.streetAndComplement;

                            //}              

                            accUpdate.Shipping_City__c = preOrderVO.city;
                            accUpdate.Shipping_PostalCode__c = preOrderVO.zipCode;
                            accUpdate.Shipping_State__c = preOrderVO.state;
                            accUpdate.Shipping_Country__c = preOrderVO.country;

                            accUpdate.MaritalStatus__c = preOrderVO.maritalStatus;
                            //accUpdate.Sex__c = preOrderVO.sex;
                            accUpdate.PersonBirthdate = preOrderVO.birthDate;
                            accUpdate.PersLandline__c = preOrderVO.residencialLandline;
                            accUpdate.ProfLandline__c = preOrderVO.commercialLandline;
                            accUpdate.PersMobPhone__c = preOrderVO.cellphone;
                            
                            if(preOrderVO.RG_StateRegistration != null && !preOrderVO.RG_StateRegistration.trim().equals('')){                          
                                //accUpdate.RGStateRegistration__c = Double.valueOf(preOrderVO.RG_StateRegistration );
                                accUpdate.RGStateTexto__c = preOrderVO.RG_StateRegistration;
                            }
                            
                            //Converte o retorno campo Sexo
                            if(preOrderVO.sex == 'M'){
                                accUpdate.Sex__c = 'Man';
                            } else if(preOrderVO.sex == 'F'){
                                accUpdate.Sex__c = 'Woman';
                            } else if(preOrderVO.sex == 'O'){
                                accUpdate.Sex__c = 'Undefined';
                            }
                            
                            lstAccount_Update.add(accUpdate);
                        }
                    }
                }
            }
        }
        
        system.Debug('*** lstAccount_Update --> '+lstAccount_Update);
        system.Debug('*** lstQuote_Update --> '+lstQuote_Update);
        
        List<Database.Saveresult> account_results = Database.update(lstAccount_Update, false);
        
        for( Database.Saveresult accountUpdate : account_results ){
            if( !accountUpdate.isSuccess() ){
                WSC04_PreOrderDMSVO errorDMS = new WSC04_PreOrderDMSVO();
                errorDMS.errorMessage = accountUpdate.getErrors() + ' - ' + accountUpdate.getId();
                lstPreOrderDMSWithStatus.add( errorDMS );
            }
        }
        
        List<Database.Saveresult> quote_results = Database.update(lstQuote_Update);
        for( Database.Saveresult quoteUpdate : quote_results ){
            if( quoteUpdate.isSuccess() ){
                WSC04_PreOrderDMSVO errorDMS = new WSC04_PreOrderDMSVO();
                errorDMS.errorMessage = ' All Records are successfully updated Quote ID - ' + quoteUpdate.getId();
                lstPreOrderDMSWithStatus.add( errorDMS );
            } else{
                WSC04_PreOrderDMSVO errorDMS = new WSC04_PreOrderDMSVO();
                errorDMS.errorMessage = quoteUpdate.getErrors() + ' - ' + quoteUpdate.getId();
                lstPreOrderDMSWithStatus.add( errorDMS );
            }
        }
        return lstPreOrderDMSWithStatus;
    }
}