/*  Logger__c management
**************************************************************************************************************************
25 Aug 2014                           : Creation
24 Aug 2015 (R15.09, PinkBullBaby  8.1) : Supervision logs
18 Mar 2016 (R16.04, PurpleDragon 10.1) : Management of Logger__c.lookups (account__c, Vehicle__c, Vehicle_Relationship__c
**************************************************************************************************************************/
public without sharing class INDUS_Logger_CLS {  
	
	public enum ErrorLevel {Info, Warn, Critical}
	public enum ProjectName {RFORCE, MYR, LMTSFA, INDUS}
	static Id Logger_RecordType_Id_Application; 
	static Id Logger_RecordType_Id_Supervision;
	
	static {
        Logger_RecordType_Id_Application = [select Id from recordtype where SobjectType='Logger__c' and developername='Application'].Id;
		Logger_RecordType_Id_Supervision = [select Id from recordtype where SobjectType='Logger__c' and developername='Supervision'].Id;
    }
	
	/** Insert the given log
		@param parentLogId is nullable: in this case, the id is the parent of potential other logs
		@returns the id of the inserted log **/
	private static Id insertLog(String runId, Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, ErrorLevel error){
		Logger__c log = new Logger__c(
			  RecordTypeId=Get_RecordType(ApexClass)
			, Project_Name__c=String.valueOf(project)
			, Apex_Class__c=ApexClass  
			, Function__c=Function
			, Record__c=Record
			, Exception_Type__c=ExceptType 
			, Message__c=Message
			, Error_Level__c=String.valueOf(error)
			, RunId__c = runId
			, ParentLog__c = parentLogId
		);
		try{ 
    		insert log;
    		return log.Id;
    	} catch(Exception e) {
    		//Caution : we can have some problems to insert a log and by calling recursively the same function we can go into an infinite loop (Too many dml in our case !)
    		if( ApexClass != INDUS_Logger_CLS.class.getName() ) {
    			insertLog(null, null, INDUS_Logger_CLS.ProjectName.INDUS, INDUS_Logger_CLS.class.getName(), 'addLog', String.valueOf(log), e.getTypeName(), e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Warn);
    		}
    	}
    	return null;
	}
	
	/** Insert the given log
		@param parentLogId is nullable: in this case, the id is the parent of potential other logs
		@returns the id of the inserted log **/
	private static Id insertLog(Id recordType, String runId, Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, ErrorLevel error){
		Logger__c log = new Logger__c( 
			  RecordTypeId=Get_RecordType(ApexClass)
			, Project_Name__c=String.valueOf(project) 
			, Apex_Class__c=ApexClass  
			, Function__c=Function
			, Record__c=Record
			, Exception_Type__c=ExceptType 
			, Message__c=Message
			, Error_Level__c=String.valueOf(error)
			, RunId__c = runId
			, ParentLog__c = parentLogId
		);
		try{ 
    		insert log;
    		return log.Id;
    	} catch(Exception e) {
    		//Caution : we can have some problems to insert a log and by calling recursively the same function we can go into an infinite loop (Too many dml in our case !)
    		if( ApexClass != INDUS_Logger_CLS.class.getName() ) {
    			insertLog(runId, parentLogId, INDUS_Logger_CLS.ProjectName.INDUS, INDUS_Logger_CLS.class.getName(), 'addLog', String.valueOf(log), e.getTypeName(), e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Warn);
    		}
    	}
    	return null;
	}
	
	/** @return the error level given a string **/
	private static ErrorLevel getErrorFromString(String iError) {
		for( ErrorLevel err : ErrorLevel.values() ) {
			if( err.name().equalsIgnoreCase(iError) ) return err;
		}
		return null;
	}
	
	/** Insert a log
		@return the id of the log to be used to reference others messages to this log **/
	public static Id addLog (ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, ErrorLevel error){
		return insertLog(null, null, project, ApexClass, Function, Record, ExceptType, Message, error);
	}
	
	/** Insert a log as the child of another one
		@return the id of the inserted log **/
	public static Id addLogChild (Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, ErrorLevel error){
		return insertLog(null, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, error);
	}
	
	/** Alternative to the Parent/Child logs which cannot be used in the context of update on user (Logs are always writtent asynchronously to avoid MIXED_DML_OPERATION exception)
		@param runId is used to group the logs of a same run together
		@return the Id of the written log 
	**/ 
	public static Id addGroupedLog (String runId, Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, String error){
		return insertLog(runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, getErrorFromString(error));
	}

	/* Supervion logs - parent*/ 
	public static Id addGroupedLogSupParent (String runId, Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, String error, String Job_Id, dateTime Completed_Date, String Status, Integer Total_Job_Items, Integer Job_Items_Processed, Integer Number_Of_Errors, String Extended_Status, Datetime Dt_Start_Time, Integer Total_Scope, Integer Nb_Records_Created, Integer Nb_Records_Updated, Integer Nb_Records_Deleted, Integer Nb_Error_Insert, Integer Nb_Error_Update, Integer Nb_Error_Delete, Integer Nb_Email_Sent, Integer Nb_Error_Email){
		Logger__c log = new Logger__c( 
			  RecordTypeId=Get_RecordType(ApexClass)
			, Project_Name__c=String.valueOf(project)
			, Apex_Class__c=ApexClass
			, Function__c=Function
			, Record__c=Record
			, Exception_Type__c=ExceptType 
			, Message__c=Message
			, Error_Level__c=String.valueOf(error)
			, RunId__c = runId
			, ParentLog__c = parentLogId
			, Job_Id__c = Job_Id
			, Completed_Date__c = Completed_Date
			, Status__c = Status
			, Total_Job_Items__c = Total_Job_Items
			, Job_Items_Processed__c = Job_Items_Processed
			, Number_Of_Errors__c = Number_Of_Errors
			, Extended_Status__c = Extended_Status
			, Dt_Start_Time__c = Dt_Start_Time
			, Total_Scope__c = Total_Scope
			, Nb_Records_Created__c = Nb_Records_Created
			, Nb_Records_Updated__c = Nb_Records_Updated
			, Nb_Records_Deleted__c = Nb_Records_Deleted
			, Nb_Error_Insert__c = Nb_Error_Insert
			, Nb_Error_Update__c = Nb_Error_Update
			, Nb_Error_Delete__c = Nb_Error_Delete
			, Nb_Email_Sent__c = Nb_Email_Sent
			, Nb_Error_Email__c = Nb_Error_Email
		);
		try{ 
    		insert log;
    		return log.Id;
    	} catch(Exception e) {
    		//Caution : we can have some problems to insert a log and by calling recursively the same function we can go into an infinite loop (Too many dml in our case !)
    		if( ApexClass != INDUS_Logger_CLS.class.getName() ) {
    			insertLog(runId, parentLogId, INDUS_Logger_CLS.ProjectName.INDUS, INDUS_Logger_CLS.class.getName(), 'addLog', String.valueOf(log), e.getTypeName(), e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Warn);
    		}
    	}
    	return null;
	}	
	
	/* Supervion logs - fils*/ 
	public static Id addGroupedLogSupFils (String runId, Id parentLogId, ProjectName project, String ApexClass, String Function, String Record, String ExceptType, String Message, String error){
		return insertLog(Get_RecordType(ApexClass), runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, getErrorFromString(error));
	}

	//Alternative methods to log with lookup Account__c, Vehicle__c, Vehicle_Relationship__c
	//Called from classes
	public static Id addGroupedLogFull (String runId, String parentLogId, ProjectName project, String ApexClass, String Function, String ExceptType, String Message, String error, String accountId, String relationId, String vehicleId, Long Begin_Time, Long End_Time, String Record){ 
		Log_Input L_I = new Log_Input ();
		L_I.RecordTypeId=Get_RecordType(ApexClass);
		L_I.Run_Id=runId; 
		L_I.Parent_Log_Id=parentLogId; 
		L_I.Project=project;
		L_I.ApexClass=ApexClass; 
		L_I.Function=Function;
		L_I.ExceptType=ExceptType;
		L_I.Message=Message;
		L_I.Error=error;
		L_I.Account_Id=((!Myr_MyRenaultTools.isValidId(accountId))?null:accountId);
		L_I.Vehicle_Id=((!Myr_MyRenaultTools.isValidId(vehicleId))?null:vehicleId);
		L_I.Record=Record; 
		L_I.Vehicle_Relationship_Id=((!Myr_MyRenaultTools.isValidId(relationId))?null:relationId);
		if (Begin_Time!=null && End_Time!=null)
		{
			L_I.Duration = End_Time - Begin_Time;
		}
		return insertLogFull(L_I);
	}
	
	//Insert log
	private static Id insertLogFull(Log_Input L_I){
		Logger__c log = new Logger__c( 
			Project_Name__c=String.valueOf(L_I.Project) 
			, Apex_Class__c=L_I.ApexClass  
			, Function__c=L_I.Function
			, Record__c=L_I.Record
			, Exception_Type__c=L_I.ExceptType 
			, Message__c=L_I.Message
			, Error_Level__c=String.valueOf(L_I.Error)
			, RunId__c = L_I.Run_Id
			, ParentLog__c = L_I.Parent_Log_Id
			, Account__c = L_I.Account_Id
			, Vehicle__c = L_I.Vehicle_Id
			, Vehicle_Relationship__c = L_I.Vehicle_Relationship_Id
			, Duration__c = L_I.Duration
		);
		try{ 
			system.debug('### INDUS_Logger_CLS - <insertLogFull> - log=' + log);
    		insert log;
    		return log.Id; 
    	} catch(Exception e) {
			//Caution : we can have some problems to insert a log and by calling recursively the same function we can go into an infinite loop (Too many dml in our case !)
			//DO NOT COMMENT THIS PART OF CODE otherwise we can crash some code just because we were unable to log something !!!!!
    		if( log.Apex_Class__c != INDUS_Logger_CLS.class.getName() ) {
    			insertLog(log.RunId__c, log.ParentLog__c, INDUS_Logger_CLS.ProjectName.INDUS, INDUS_Logger_CLS.class.getName(), 'addLog', String.valueOf(log), e.getTypeName(), e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Warn);
    		}
    	}
    	return null;
	}

	//Class for inputs to log
	public class Log_Input { 
		public ProjectName Project {get; set;}
		public String ApexClass {get; set;}  
		public String Function {get; set;} 
		public String Record {get; set;}
		public String ExceptType {get; set;} 
		public String Message {get; set;}
		public String Error {get; set;}
		public String Run_Id {get; set;}
		public String Parent_Log_Id {get; set;}
		public String Account_Id {get; set;}
		public String Vehicle_Id {get; set;}
		public String Vehicle_Relationship_Id {get; set;} 
		public Long Duration {get; set;} 
		public Id RecordTypeId {get; set;}
	}

	//Get the suitable recordId, depending on the name of the ApexClass
	private static Id Get_RecordType(String ApexClass){
		Id Record_Type_Id;
		if (ApexClass.containsIgnoreCase('batch')){
			Record_Type_Id=Logger_RecordType_Id_Supervision;
		}else{
			Record_Type_Id=Logger_RecordType_Id_Application;
		}
		return Record_Type_Id;
	}
}