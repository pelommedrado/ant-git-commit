@isTest
public class AcessoOportunidadeSfa2ControllerTest {

    static testMethod void myUnitTest() {
        
        MyOwnCreation moc = new MyOwnCreation();
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User communityUser;
        Account dealer;
                
        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {
            
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            dealer = moc.criaAccountDealer();
            Insert(dealer);
            
            Contact contact = moc.criaContato();
            contact.AccountId = dealer.Id;
            Insert(contact);
            
            communityUser = moc.criaUser();
            communityUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            communityUser.ContactId = contact.Id;
            Insert(communityUser);  
        }
        
        //Execução com usuario comunidade
        System.runAs( communityUser ){
            
            Opportunity opp = moc.criaOpportunity();
            opp.Dealer__c = dealer.Id;
            opp.Ownership_requested_by_ID__c = communityUser.Id;
            opp.OwnerId = communityUser.Id;
            opp.StageName = 'Identified';
            Insert opp;
            
            AcessoOportunidadeSfa2Controller controller = new AcessoOportunidadeSfa2Controller();
            
            controller.oppIdListAsString = '{"'+String.valueOf(opp.Id)+'":"'+String.valueOf(communityUser.Id)+'"}';
            String oppIdListAsString = controller.oppIdListAsString;
            
            List<SelectOption> sellerSelectList = controller.sellerSelectList;
            
            List<AcessoOportunidadeSfa2Controller.SolicitacaoOpp> neeOppList = controller.oppList;
            
            controller.forward();
        }
    }
}