/**
* @Felipe Livino - {kolekto}
* Classe responsavel pelo objeto Lead. Quando gerado um novo lead deve se verificar o campoPV_OfferNotFound
* estiver ativado para transformalo em uma quarentena
*/
public class VFC152_PV_LEAD {

  public VFC152_PV_LEAD(){}
  
  private  Quarantine__c quarentena;
    
  
  // método responsável por converter o lead para quarentena quando marcado o campo PV_OfferNotFound
  
   public void geraQuarentena(Lead lead){ 
            quarentena = new Quarantine__c();
              quarentena.Lead__c = lead.Id;
              quarentena.CPF_CNPJ__c= lead.CPF_CNPJ__c;
              quarentena.State__c = lead.State;
              quarentena.City__c = lead.City;
              quarentena.Email__c = lead.Email;
              quarentena.FirstName__c = lead.FirstName;
              quarentena.LastName__c = lead.LastName;
              quarentena.Phone__c = lead.Phone;
              quarentena.Origin__c = lead.LeadSource;
              quarentena.SubOrigin__c = lead.SubSource__c;
              quarentena.Model_Interest__c = lead.VehicleOfInterest__c;
              quarentena.Second_Model_Interest__c = lead.SecondVehicleOfInterest__c;
          insert(quarentena);
   }
}