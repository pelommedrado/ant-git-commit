@isTest
public class Test_VFC05ArchivageXMLParseEngine{
    
    static User getEnvSetup(){
        // Get the context of a user
        Profile p = [SELECT Id FROM Profile WHERE Name='CCBIC - Agent Front Office']; 
        User u = new User(Alias = 'rvtes_1', Email='rvtestuser_1@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
                          LocaleSidKey='fr', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='rvtestuser_1@testorg.com',
                          Country = 'GRIDADMINISTRATOR');
        VFC05_LanguageMapping__c mycs = VFC05_LanguageMapping__c.getValues('Short_Code__c');
        if(mycs == null) {
            mycs = new VFC05_LanguageMapping__c(Name= 'French');
            mycs.Short_Code__c = 'FR';
            insert mycs;
        }
        System.debug('Current User: ' + UserInfo.getUserName());
        System.debug('Current Profile: ' + UserInfo.getProfileId()); 
        system.debug('Userlanguage: ' + u.LanguageLocaleKey);
        return u;
    }
    
    
    static testMethod void testVehicleAttributes(){
        
        // Set up the envioronment 
        User u = getEnvSetup();
        //   User u = getEnvSetup();
        // Create a vehicle
        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766');
        insert veh;
        
        String toParse='<SiebelMessage>	<ListOfRcServiceRequest-Archivage>		<ServiceRequest>			<SRNumber><![CDATA[1-216147610]]></SRNumber>			<ListOfAction>				<Action>					<Type2>gg</Type2>					<ListOfActionAttachment>						<ActionAttachment>							<ActivityFileName>ActivityFileName</ActivityFileName>						</ActionAttachment>					</ListOfActionAttachment>				</Action>			</ListOfAction>			<ListOfContact>				<Contact>					<TitleSIC>TitleValue</TitleSIC>				</Contact>				<ListOfContact_PersonalAddress>					<Contact_PersonalAddress>						<PersonalCompleteStreetAddress><![CDATA[Potters Mews Technical Street ]]></PersonalCompleteStreetAddress>					</Contact_PersonalAddress>				</ListOfContact_PersonalAddress>			</ListOfContact>			<ListOfRenaultRcDealers>				<RenaultRcDealers>					<DealerNumber><![CDATA[82651200]]></DealerNumber>					<ListOfRenaultRCDealers_BusinessAddress>						<RenaultRCDealers_BusinessAddress>							<CompleteAddress><![CDATA[75-80 HIGH STREETDIGBETH]]></CompleteAddress>						</RenaultRCDealers_BusinessAddress>					</ListOfRenaultRCDealers_BusinessAddress>				</RenaultRcDealers>			</ListOfRenaultRcDealers>			<ListOfAssetMgmt-AssetArchivage>				<AssetMgmt-AssetArchivage>					<Build>builtList</Build>				</AssetMgmt-AssetArchivage>			</ListOfAssetMgmt-AssetArchivage>			<ListOfRenaultRcDealers-Compensateur>				<RenaultRcDealers-Compensateur>					<DealerNumber>DealerNumber</DealerNumber>				</RenaultRcDealers-Compensateur>			</ListOfRenaultRcDealers-Compensateur>			<ListOfEmployee>				<Employee>					<FirstName><![CDATA[CHRIS]]></FirstName>				</Employee>			</ListOfEmployee>			<ListOfAccount2>				<Account2>					<Name2>Name</Name2>					<ListOfAccount_BusinessAddress>						<Account_BusinessAddress>							<CompleteAddress>Address</CompleteAddress>						</Account_BusinessAddress>					</ListOfAccount_BusinessAddress>				</Account2>			</ListOfAccount2>						<RenaultRcDealers-Compensateur>				<DealerNumber>DealerNumber</DealerNumber>			</RenaultRcDealers-Compensateur></ServiceRequest>	</ListOfRcServiceRequest-Archivage></SiebelMessage>';
        // Get ready to run the methods
        VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(veh));
        VFC05ArchivageXMLParseEngine parsingEngine=new VFC05ArchivageXMLParseEngine();
        VFC05_ArchivesXMLData xmlData=new VFC05_ArchivesXMLData();
        Test.startTest();          
        System.runAs(u) {
            xmlData=parsingEngine.getXMLData(toParse);     
        }
        Test.stopTest();
    }
    
    
}