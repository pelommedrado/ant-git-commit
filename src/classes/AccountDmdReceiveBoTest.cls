@isTest
public class AccountDmdReceiveBoTest {
	//This method must follow the Main Flow, meeting every condition
	@isTest static void shouldFollowMainFlow(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('<Renault> - Interface');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/Los_Angeles', UserName='meuusuario@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'NotExpectedName';
			tstAccount1.ShortndName__c        = 'NESN';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = 'Conta001';
			tstAccount1.Tech_ACCExternalID__c = '012';
			tstAccount1.IDBIR__c              = '012';
			tstAccount1.NameZone__c           = 'testeteste';
			tstAccount1.Dealer_Status__c      = 'Active';

			System.debug('!!! tstAccount1: '+tstAccount1);

			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);

			System.assertEquals('et'      , selectedAcc.NameZone__c          );
			System.assertEquals('Conta001', selectedAcc.ShortndName__c       );
			System.assertEquals('Conta001', selectedAcc.Name                 );
			System.assertEquals('12'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('12'      , selectedAcc.IDBIR__c             );
		}
	}

	@isTest static void shouldFollowMainFlowOtherUser(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('<Renault> - Interface');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'padrao', Email='padraouser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testando', LanguageLocaleKey='pt_BR',LocaleSidKey='pt_BR', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/New_York', UserName='meuusuario2@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'NotExpectedName2';
			tstAccount1.ShortndName__c        = 'NESN';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = 'Conta001';
			tstAccount1.Tech_ACCExternalID__c = '013';
			tstAccount1.IDBIR__c              = '013';
			tstAccount1.NameZone__c           = 'testeteste';	
			tstAccount1.Dealer_Status__c      = 'Active';

			System.debug('!!! tstAccount1: '+tstAccount1);
			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);		

			System.assertEquals('et'      , selectedAcc.NameZone__c          );
			System.assertEquals('Conta001', selectedAcc.ShortndName__c       );
			System.assertEquals('Conta001', selectedAcc.Name                 );
			System.assertEquals('13'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('13'      , selectedAcc.IDBIR__c             );

			tstAccount1.NameZone__c = 'testeteste';	
			update tstAccount1;
			System.assertEquals('et', selectedAcc.NameZone__c);
		}
	}
		
	//This method execution must follow only a alternative Flow
	@isTest static void shouldFollowAlternativeFlow(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('<Renault> - Interface');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/Los_Angeles', UserName='meuusuario@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'ExpectedName';
			tstAccount1.ShortndName__c        = 'ExpectedShort';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = '';
			tstAccount1.Tech_ACCExternalID__c = '12';
			tstAccount1.IDBIR__c              = '12';
			tstAccount1.NameZone__c           = 'this';

			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);

			System.assertEquals('this'      , selectedAcc.NameZone__c          );
			System.assertEquals('ExpectedShort', selectedAcc.ShortndName__c       );
			System.assertEquals('ExpectedName', selectedAcc.Name                 );
			System.assertEquals('12'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('12'      , selectedAcc.IDBIR__c             );

			selectedAcc.Dealer_Status__c  = 'Active';
			selectedAcc.IDBIR__c = '';
			selectedAcc.NameZone__c = 'testeteste';	
			update selectedAcc;
		}
	}

	//This method execution shouldn't trigger the class execution
	@isTest static void shouldFollowAlternativeFlowAdminUser(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('Administrador do sistema');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/Los_Angeles', UserName='meuusuario@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'ExpectedName';
			tstAccount1.ShortndName__c        = 'ExpectedShort';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = 'Should not trigger';
			tstAccount1.Tech_ACCExternalID__c = '012';
			tstAccount1.IDBIR__c              = '012';
			tstAccount1.NameZone__c           = 'testeteste';

			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);

			System.assertEquals('testeteste'      , selectedAcc.NameZone__c          );
			System.assertEquals('ExpectedShort', selectedAcc.ShortndName__c       );
			System.assertEquals('ExpectedName', selectedAcc.Name                 );
			System.assertEquals('012'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('012'      , selectedAcc.IDBIR__c             );
		}
	}

	//This method execution shouldn't trigger the class execution
	@isTest static void shouldFollowAlternativeFlowInactiveDealerStatus(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('<Renault> - Interface');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/Los_Angeles', UserName='meuusuario@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'ExpectedName';
			tstAccount1.ShortndName__c        = 'ExpectedShort';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = 'Should not trigger';
			tstAccount1.Tech_ACCExternalID__c = '012';
			tstAccount1.IDBIR__c              = '012';
			tstAccount1.NameZone__c           = 'testeteste';
			tstAccount1.Dealer_Status__c      = 'Inactive';

			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);

			System.assertEquals('testeteste'      , selectedAcc.NameZone__c          );
			System.assertEquals('ExpectedShort', selectedAcc.ShortndName__c       );
			System.assertEquals('ExpectedName', selectedAcc.Name                 );
			System.assertEquals('012'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('012'      , selectedAcc.IDBIR__c             );
		}
	}
	
	//This method execution shouldn't trigger the class execution
	@isTest static void shouldFollowAlternativeFlowNullDealerStatus(){
		//This is the only Profile whose Users trigger the behavior of AccountDmdReceiveBo 
		Profile prfl = VFC32_ProfileDAO.getInstance().findByName('<Renault> - Interface');

		//User, Profile->'<Renault> - Interface' 
		User user = new User(
			Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', 
			LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prfl.Id, 
	  		TimeZoneSidKey='America/Los_Angeles', UserName='meuusuario@notduplicated.com'
  		);

		Account tstAccount1;

		System.runAs(user){
			tstAccount1                       = new Account();
			tstAccount1.Name                  = 'ExpectedName';
			tstAccount1.ShortndName__c        = 'ExpectedShort';
			tstAccount1.RecordTypeId          = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
			tstAccount1.UsualName__c          = 'Should not trigger';
			tstAccount1.Tech_ACCExternalID__c = '012';
			tstAccount1.IDBIR__c              = '012';
			tstAccount1.NameZone__c           = 'testeteste';
			tstAccount1.Dealer_Status__c      = null;

			insert tstAccount1;

			Account selectedAcc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(tstAccount1.Id);

			System.assertEquals('testeteste'      , selectedAcc.NameZone__c          );
			System.assertEquals('ExpectedShort', selectedAcc.ShortndName__c       );
			System.assertEquals('ExpectedName', selectedAcc.Name                 );
			System.assertEquals('012'      , selectedAcc.Tech_ACCExternalID__c);
			System.assertEquals('012'      , selectedAcc.IDBIR__c             );
		}
	}	
}