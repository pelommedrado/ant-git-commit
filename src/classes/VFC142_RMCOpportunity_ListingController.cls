public with sharing class VFC142_RMCOpportunity_ListingController{
    private String usrID;
    private String usrProfileID;
    private final static String OPP_QUERY = 'select id,StageName, Name, Vehicle_Of_Interest__c, VehicleInterest__c, CreatedDate, Owner.Name,'
        + ' OpportunitySource__c, OpportunitySubSource__c, Campaign.name, Campaign_Name__c, LastModifiedDate from Opportunity where ';
    
    public VFP22_Opp_ListingMessages__c messages{get;set;}
    public String region{get;set;}
    
    public static List< String > tabLabels{
        get{
            Set<String> permissionByUserList = new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());
            String profileName =
                [select Name from Profile where Id = :Userinfo.getProfileId() limit 1].Name;
            system.debug('$$$ ProfileName: '+profileName);
            if( !permissionByUserList.isEmpty() && 
                (permissionByUserList.contains('BR_SFA_Dealer_Executive') || permissionByUserList.contains('BR_SFA_Dealer_Manager'))){
                List<String> manager = new List<String>{Label.VFP22_seller_tab1, Label.VFP22_seller_tab2, Label.VFP22_seller_tab3,
                    Label.VFP22_seller_tab4, Label.VFP22_seller_tab5, 'Em Resgate'};
                        return manager;
            }
            else if( profileName == 'SFA - Receptionist' || (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess'))){
                List<String> receptionist = new List<String>{Label.VFP22_Receptionist_tab1, Label.VFP22_Receptionist_tab2, 
                    Label.VFP22_Receptionist_tab3, Label.VFP22_Receptionist_tab4, Label.VFP22_Receptionist_tab5};
                        return receptionist;
            }
            else if( profileName == 'SFA - Seller' ||(!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Seller'))){
                List<String> seller = new List<String>{Label.VFP22_seller_tab1, Label.VFP22_seller_tab2, Label.VFP22_seller_tab3,
                    Label.VFP22_seller_tab4, Label.VFP22_seller_tab5};
                        return seller;
            }
            else return new List<String>{'A', 'B', 'C', 'D', 'E'};
        }
    }
    
    private String selectedTabName;
    
    public String selectedTab{
        get{
            return String.isBlank( selectedTabName ) ? (String)Apexpages.CurrentPage().getParameters().get( 'tab' ) : selectedTabName;
        }
        set{
            selectedTabName = value;
        }
    }
    
    public Boolean isReceptionist{get;set;}
    public Boolean isSeller{get;set;}
    public Boolean isManager{get;set;}
    public Boolean isGerenteComunidade{get;set;}
    public list<Opportunity> tab1 {get;set;}
    public list<Opportunity> tab2 {get;set;}
    public list<Opportunity> tab3 {get;set;}
    public list<Opportunity> tab4 {get;set;}
    public list<Opportunity> tab5 {get;set;}
    public list<Opportunity> tab6 {get;set;}
    
    public String title {get;set;}
    public list<String> tabNames {get;set;}
    
    public String welcomeMessage{
        get{
            try{
                return (String)messages.get( 'Welcome_message_' + region + '_' + Userinfo.getLanguage() + '__c' );
            }catch( Exception e ){
                return messages.Welcome_message_R1_pt_BR__c;
            }
        }
    }
    public String tabReloadPeriod{get;set;}
    
    public VFC142_RMCOpportunity_ListingController() {
        Set<String> permissionByUserList = new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());
        system.debug('$$$ permissionByUserList: '+permissionByUserList);
        isReceptionist = isSeller = isManager = isGerenteComunidade = false;
        String profileName = [select Name from Profile where Id = :Userinfo.getProfileId() limit 1].Name;
        system.debug('$$$ profileName: '+profileName);
        if(!permissionByUserList.isEmpty() && 
           (permissionByUserList.contains('BR_SFA_Dealer_Executive') || permissionByUserList.contains('BR_SFA_Dealer_Manager'))){
            isGerenteComunidade = true;
               system.debug('$$$ isGerenteComunidade: '+isGerenteComunidade);}
        else if( profileName == 'SFA - Receptionist' || (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess')))
            isReceptionist = true;
        else if( profileName == 'SFA - Seller' ||(!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Seller')))
            isSeller = true;

        else if( profileName == 'SFA - Dealer VN Manager' )
            isManager = true;
        
        system.debug('$$$ isGerenteComunidade: '+isGerenteComunidade);
        system.debug('$$$ isReceptionist: '+isReceptionist);
        system.debug('$$$ isSeller: '+isSeller);

        System.assert( isReceptionist || isSeller || isManager || isGerenteComunidade, Label.VFP20_Unauthorized_access);
        
        usrID = System.Userinfo.getUserId();
        
        tab1= new list<Opportunity>();
        tab2= new list<Opportunity>();
        tab3= new list<Opportunity>();
        tab4= new list<Opportunity>();
        tab5= new list<Opportunity>();
        tab6= new list<Opportunity>();
        
        String profileNameToQuery = (isSeller?'SFA - Seller':isReceptionist?'SFA - Receptionist':isGerenteComunidade?'BR - Renault + Cliente':'');
        system.debug('$$$ profileNameToQuery: '+profileNameToQuery);
        if(String.isNotBlank(profileNameToQuery) && renaultMaisCliente_Utils.getProfileMap(profileNameToQuery).containsKey(profileNameToQuery))
            messages = VFP22_Opp_ListingMessages__c.getInstance(  RenaultMaisCliente_Utils.getProfileMap(profileNameToQuery).get(profileNameToQuery).Id );
        else   
            messages = VFP22_Opp_ListingMessages__c.getInstance( Userinfo.getProfileId() );
        
        
        try{
            User currUser =
                [select Contact.Account.NameZone__c from User where Id = :Userinfo.getUserId()];
            region = currUser.Contact.Account.NameZone__c;
        }catch( Exception e ){
            region = 'R1';
        }
        
        //Carrega o nome das Tabs
        tabNames = VFC142_RMCOpportunity_ListingController.tabLabels;
        
        system.debug('$$$ tabNames: '+tabNames);
        
        tabReloadPeriod = 'NONE';
        
        if(isSeller){
			//Carrega Tab1
			loadOpps(' StageName = \'Identified\'', 
                     null, 'order by CreatedDate asc', tab1);
            //Carrega tab2
            loadOpps(' (StageName = \'Test Drive\' OR StageName = \'Quote\' OR StageName = \'In Attendance\')', 
                     null,'order by CreatedDate desc', tab2);
            //Carrega tab3
            loadOpps(' StageName = \'Order\'', 
                     ' LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab3);
            //Carrega tab4
            loadOpps(' StageName = \'Billed\'', 
                     '  LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab4);
            //Carrega tab5
            loadOpps(' StageName = \'Lost\'', 
                     ' LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab5); 
		}
        //Gerente Comunidade
		
		else if(isGerenteComunidade){
            //Carrega Tab1
            loadOpps(' StageName = \'Identified\'', null,'order by CreatedDate desc', tab1);
            //Carrega tab2
            loadOpps(' (StageName = \'Test Drive\' OR StageName = \'Quote\' OR StageName = \'In Attendance\')', null,'order by CreatedDate desc', tab2);
            //Carrega tab3
            loadOpps(' StageName = \'Order\'', ' LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab3);
            //Carrega tab4
            loadOpps(' StageName = \'Billed\'', '  LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab4);
            //Carrega tab5
            loadOpps(' StageName = \'Lost\'', ' LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab5);
            //Carrega tab6
            loadOpps(' StageName != \'Lost\' AND StageName != \'Billed\' AND Rescue__c = \'Rescued\'', ' LastModifiedDate = THIS_MONTH ','order by LastModifiedDate desc', tab6);
        }
        //Recepcionista
        else{
            //Carrega Tab1
            loadOpps(null, ' CreatedDate = THIS_MONTH ','order by CreatedDate desc', tab1);
            //Carrega tab2
            loadOpps(null, ' CreatedDate = THIS_WEEK ','order by CreatedDate desc', tab2);
            //Carrega tab3
            loadOpps(null, ' CreatedDate = YESTERDAY ','order by CreatedDate desc', tab3);
            //Carrega tab4
            loadOpps(null, '  CreatedDate = TODAY ','order by CreatedDate desc', tab4);
            //Carrega tab5
            loadOpps(' OwnerId = \'' + usrID + '\' and OpportunitySource__c = \'NETWORK\'', ' CreatedDate = THIS_MONTH ','order by CreatedDate desc', tab5);
        }
    }
    
    public void reloadTabs (){
        tab1 = new List< Opportunity >();
        tab2 = new List< Opportunity >();
        tab3 = new List< Opportunity >();
        tab4 = new List< Opportunity >();
        tab5 = new List< Opportunity >();
        tab6 = new List< Opportunity >();
        
        //Carrega Tab1
        loadOpps(' StageName = \'Identified\'', getReloadQueryPeriod( 'CreatedDate'),' order by CreatedDate asc', tab1);
        //Carrega tab2
        loadOpps(' (StageName = \'Test Drive\' OR StageName = \'Quote\' OR StageName = \'In Attendance\')', getReloadQueryPeriod( 'CreatedDate' ),' order by CreatedDate desc', tab2);
        //Carrega tab3
        loadOpps(' StageName = \'Order\'', getReloadQueryPeriod( 'LastModifiedDate' ),' order by LastModifiedDate desc', tab3);
        //Carrega tab4
        loadOpps(' StageName = \'Billed\'', getReloadQueryPeriod( 'LastModifiedDate' ),' order by LastModifiedDate desc', tab4);
        //Carrega tab5
        loadOpps(' StageName = \'Lost\'', getReloadQueryPeriod( 'LastModifiedDate' ),' order by LastModifiedDate desc', tab5);
        //Carrega tab6
        loadOpps(' StageName != \'Lost\' AND StageName != \'Billed\' AND Rescue__c = \'Rescued\'', 
                 getReloadQueryPeriod( 'LastModifiedDate' ),' order by LastModifiedDate desc', tab6);
    }
    
    private String getReloadQueryPeriod (String fieldName){
        return tabReloadPeriod != 'NONE' ? ' ' + fieldName + ' = ' + tabReloadPeriod + ' ' : null;
    }
    
    public void loadOpps (String condition, string period, String orderBy, list<Opportunity> oppList){
        String[] queryTerms = new List< String >();
        if( isSeller ) queryTerms.add( ' OwnerId = \'' + usrID + '\' ' );
        if( !String.isBlank( condition ) ) queryTerms.add( condition );
        if( !String.isBlank( period ) ) queryTerms.add( period );
        String queryStr = OPP_QUERY + String.join( queryTerms, ' and ' ) + (!String.isBlank( orderBy ) ? orderBy : '') + ' LIMIT 200';
        system.debug('$$$ queryStr: '+queryStr);
        oppList.addAll( (List< Opportunity >)Database.query( queryStr ) );
        System.debug( ' @@@ ' + condition + ':\n' + Json.serializePretty( oppList ) );
    }
}