public class ActivityManagerBo {
    public List<Opportunity> oppNewList { get;set; }
    private Map<Id, Opportunity> oppOldMap  { get;set; }

    private Set<Id>     userPermissionSeller { get;set; }

    public static String confirmTestDrive   = 'Confirm Test Drive';
    public static String confirmVisit       = 'Confirm Visit';
    public static String contact            = 'Contact';
    public static String custemerService    = 'Customer Service';//Atendimento Passante
    public static String deliveryConfirm    = 'Delivery Confirmation';//Confirmação de Entrega
    public static String negotiation        = 'Negotiation';//Negociação
    public static String performTestDrive   = 'Perform Test Drive';//Realizar Test Drive
    public static String performVisit       = 'Perform Visit';//Realizar Visita
    public static String persecution        = 'Persecution';//Perseguição
    public static String preOrder           = 'Pre-Order';//Pré-Order
    public static String prospection        = 'Prospection';//Prospecção
    public static String rescue             = 'Rescue';//Resgate

    public List<ProprietarioOportunidadeAlterado> proprietarioOppList { get; set;}

    public ActivityManagerBo(List<Opportunity> oppNewList, Map<Id, Opportunity> oppOldMap) {
      this.oppNewList     = oppNewList;
      this.oppOldMap      = oppOldMap;
      this.userPermissionSeller =
            ProfileUtils.userPermission(ProfileUtils.permissionSeller);
      if(oppOldMap != null) {
        this.proprietarioOppList = obterProprietarioOportunidadeAlteradoList();
      }
    }

    private List<ProprietarioOportunidadeAlterado> obterProprietarioOportunidadeAlteradoList() {
        System.debug('obterOppListOwner() ...');

        List<ProprietarioOportunidadeAlterado> proprietarioOportunidadeList = new List<ProprietarioOportunidadeAlterado>();
        for(Opportunity opp : oppNewList) {
            final Opportunity oppOld = oppOldMap.get(opp.Id);
            if(isProprietarioMudou(opp, oppOld)) {
                ProprietarioOportunidadeAlterado proprietarioOpp = createProprietarioOportunidade(opp, oppOld);
                proprietarioOportunidadeList.add(proprietarioOpp);
            }
        }
        return proprietarioOportunidadeList;
    }

    private Boolean isProprietarioMudou(Opportunity opp, Opportunity oppOld) {
      if(oppOld != null && opp.OwnerId != null && opp.OwnerId != oppOld.OwnerId) {
          return true;
      }
      return false;
    }

    private ProprietarioOportunidadeAlterado createProprietarioOportunidade(Opportunity opp, Opportunity oppOld) {
      ProprietarioOportunidadeAlterado oUo = new ProprietarioOportunidadeAlterado();
      oUo.opp     = opp;
      oUo.oppOld  = oppOld;
      oUo.newOwnerSeller = userPermissionSeller.contains(opp.OwnerId);
      oUo.oldOwnerSeller = userPermissionSeller.contains(oppOld.OwnerId);
      return oUo;
    }

    public void execInsert() {
        System.debug('execInsert()');

        List<Event> eventNewList = new List<Event>();
        for(Opportunity opp : oppNewList) {
            if(isProprietarioVendedor(opp)) {
                final Event event = criarAtividadeParaVendedor(opp);
                if(event != null) {
                  eventNewList.add(event);
                }
            }
        }

        try {
            INSERT eventNewList;

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
        }
    }

    private Boolean isProprietarioVendedor(Opportunity opp) {
      return !String.isEmpty(opp.OwnerId) && userPermissionSeller.contains(opp.OwnerId);
    }

    private Event criarAtividadeParaVendedor(Opportunity opp) {
        System.debug('criarAtividade() opp: ' + opp);

        if(isFaseInvalidaParaCriacao(opp)) {
            System.debug('Fase invalida para criar tarefa');
            return null;
        }

        if(isNaoTemOpportunitySource(opp)) {
          System.debug('Nao possui o campo OpportunitySource');
            return null;
        }

        if(isDetailIgualTestDrive(opp)) {
            return createEvent(confirmTestDrive, opp);

        } else if(!isOpportunitySourceIgualDealer(opp)) {
            return createEvent(confirmVisit, opp);

        }

        if(isNaoTemOpportunitySubSource(opp)) {
          System.debug('Nao possui o campo OpportunitySubSource');
          return null;
        }

        if(isOpportunitySourceIgualDealer(opp)) {
          String atividade = isOpportunitySubSourceIgualInternet(opp) ? confirmVisit : custemerService;
          return createEvent(atividade, opp);
        }
        return null;
    }

    private Boolean isFaseInvalidaParaCriacao(Opportunity opp) {
      final Set<String> fasesValidas = new Set<String> { 'Identified', 'In Attendance' };
      return !fasesValidas.contains(opp.StageName);
    }

    private Boolean isNaoTemOpportunitySource(Opportunity opp) {
      return String.isEmpty(opp.OpportunitySource__c);
    }

    private Boolean isDetailIgualTestDrive(Opportunity opp) {
      return !String.isEmpty(opp.Detail__c) && opp.Detail__c.equalsIgnoreCase('TEST DRIVE');
    }

    private Boolean isOpportunitySourceIgualDealer(Opportunity opp) {
      return opp.OpportunitySource__c.equalsIgnoreCase('DEALER');
    }

    private Boolean isNaoTemOpportunitySubSource(Opportunity opp) {
      return String.isEmpty(opp.OpportunitySubSource__c);
    }

    private Boolean isOpportunitySubSourceIgualInternet(Opportunity opp) {
      return opp.OpportunitySubSource__c.equalsIgnoreCase('INTERNET');
    }

    private static Event createEvent(String assunto, Opportunity opp) {
        final Event event = new Event();
        event.Subject                = assunto;
        event.Status__c              = 'In Progress';
        event.ReminderDateTime       = opp.Expiration_Time__c.addMinutes(-15);
        event.ActivityDatetime__c    = opp.Expiration_Time__c.addMinutes(-15);
        event.IsReminderSet          = true;
        event.IsVisibleInSelfService = true;
        event.OwnerId                = opp.OwnerId;
        event.WhatId                 = opp.Id;
        event.StartDateTime          = opp.Expiration_Time__c;
        event.EndDateTime            = opp.Expiration_Time__c.addMinutes(15);
        return event;
    }

    public void execUpdate() {
      System.debug('execUpdate()');

      execTransferenciaTask();
      execCriacaoAtividadeParaVendedor();
      execAnaliseAlteracaoFase();
    }

    private void execTransferenciaTask() {
      System.debug('execTransferenciaTask()');
      final Map<Id, ProprietarioOportunidadeAlterado> proprietarioOppMap = new Map<Id, ProprietarioOportunidadeAlterado>();
      for(ProprietarioOportunidadeAlterado ou: proprietarioOppList) {
          if(ou.isTransferirTask()) {
              proprietarioOppMap.put(ou.opp.Id, ou);
          }
      }
      try {
          transferirTasksDeProprietario(proprietarioOppMap);
      } catch(Exception ex) {
          System.debug('Ex: ' + ex);
          throw ex;
      }
    }

    private void transferirTasksDeProprietario(Map<Id, ProprietarioOportunidadeAlterado> mapOppUpOwner) {
        System.debug('transferirTasksDeProprietario() mapOppUpOwner: ' + mapOppUpOwner);
        if(mapOppUpOwner.isEmpty()) {
          return;
        }
        List<Task> taskTransfList = [
            SELECT Id, WhatId, OwnerId FROM Task WHERE WhatId IN:(mapOppUpOwner.keySet()) ];
        for(Task t: taskTransfList) {
            ProprietarioOportunidadeAlterado oUo = mapOppUpOwner.get(t.WhatId);
            t.OwnerId = oUo.opp.OwnerId;
            System.debug('task: ' + t + ' ProprietarioOportunidadeAlterado: ' + oUo);
        }
        UPDATE taskTransfList;

        List<Event> eventTransfList = [
            SELECT Id, WhatId, OwnerId FROM Event WHERE WhatId IN:(mapOppUpOwner.keySet())];
        for(Event e: eventTransfList) {
            ProprietarioOportunidadeAlterado oUo = mapOppUpOwner.get(e.WhatId);
            e.OwnerId = oUo.opp.OwnerId;
            System.debug('event: ' + e + ' ProprietarioOportunidadeAlterado: ' + oUo);
        }
        UPDATE eventTransfList;
    }

    private void execCriacaoAtividadeParaVendedor() {
      System.debug('execCriacaoAtividadeParaVendedor()');

      final List<Event> eventNewList = new List<Event>();
      for(ProprietarioOportunidadeAlterado ou: proprietarioOppList) {
        if(!ou.isTransferirTask() && ou.isNovoDonoVendedor()) {
          final Event event = criarAtividadeParaVendedor(ou.opp);
          if(event != null) {
            eventNewList.add(event);
          }
        }
      }

      try {
          if(!eventNewList.isEmpty()) {
            INSERT eventNewList;
          }


      } catch(Exception ex) {
        System.debug('Ex: ' + ex);
        throw ex;

      }
    }

    public void execAnaliseAlteracaoFase() {
      System.debug('execAnaliseAlteracaoFase()');
        
      Set<Id> oppSetStageOrder = new Set<Id>();
      Set<Id> oppSetStageBilled = new Set<Id>();

      final List<Task> newTaskList = new List<Task>();
      final List<Opportunity> oppListStageQuote = obterOppListFaseAlteradaPara('Quote');
      for(Opportunity opp : oppListStageQuote) {
        final Task tas = createTask('Negotiation', opp);
        newTaskList.add(tas);
      }

      final List<Opportunity> oppListStageOrder = obterOppListFaseAlteradaPara('Order');
      for(Opportunity opp : oppListStageOrder) {
        final Task tas = createTask('Pre-Order', opp);
        oppSetStageOrder.add(opp.Id);
        newTaskList.add(tas);
      }

      final List<Opportunity> oppListStageBilled = obterOppListFaseAlteradaPara('Billed');
      for(Opportunity opp : oppListStageBilled) {
          oppSetStageBilled.add(opp.Id);
      }

      final List<Opportunity> oppListStageAll = new List<Opportunity>();
      oppListStageAll.addAll(oppListStageQuote);
      oppListStageAll.addAll(oppListStageOrder);
      oppListStageAll.addAll(oppListStageBilled);
      
      Set<Id> oppsIds = new Set<Id>();
      
      for(Opportunity opp : oppListStageAll){
        oppsIds.add(opp.Id);
      }
        

      try {
        
        if(System.isBatch() || System.isFuture()){
          completarAtividade(oppListStageAll);
        
        } else{
          completarAtividade(oppsIds);
        }
        
        Set<Id> oppsStageAllSet = new Set<Id>();

        List<String> fases = new List<String>();
        fases.add('Negotiation');
        //fecharListaAtividades(fases, oppSetStageOrder);

        fases.add('Pre-Order');
        //fecharListaAtividades(fases, oppSetStageBilled);

        oppsStageAllSet.addAll(oppSetStageOrder);
        oppsStageAllSet.addAll(oppSetStageBilled);

        fecharListaAtividades(fases, oppsStageAllSet);

        if(!newTaskList.isEmpty()) {
          INSERT newTaskList;
        }

      } catch(Exception ex) {
          System.debug('Ex: ' + ex);
      }
    }

    private List<Opportunity> obterOppListFaseAlteradaPara(String stageName) {
      System.debug('obterListOppStageChange(): ' + stageName);

      final List<Opportunity> oppStageList = new List<Opportunity>();
      for(Opportunity opp : oppNewList) {
          final Opportunity oppOld = oppOldMap.get(opp.Id);
          if(isFaseMudou(opp, oppOld) && opp.StageName.equalsIgnoreCase(stageName)) {
              oppStageList.add(opp);
          }
      }
      return oppStageList;
    }

    private Boolean isFaseMudou(Opportunity opp, Opportunity oppOld) {
      if(oppOld != null && opp.StageName != oppOld.StageName) {
          return true;
      }
      return false;
    }
    
    @future
    private static void completarAtividade(Set<Id> oppIds){
      
      
      if(oppIds.isEmpty()) {
          return;
        }
        List<String> assuntoWhereList2 = new List<String> {
          'Confirm Visit', 'Perform Visit', 'Confirm Test Drive', 'Perform Test Drive', 'Customer Service'
        };
        List<String> statusWhereList2 = new List<String> {
          'Completed', 'Deferred', 'Canceled'};

        List<Task> tList = [ SELECT Id FROM Task WHERE whatId IN: oppIds AND
                            Subject IN: assuntoWhereList2 AND Status Not In: statusWhereList2 ];
        for(Task t : tList) { t.Status = 'Completed'; }
        if(!tList.isEmpty()) {
          UPDATE tList;
        }

        List<Event> eList = [ SELECT Id FROM Event WHERE whatId IN: oppIds AND
                             Subject IN: assuntoWhereList2 AND Status__c Not In: statusWhereList2 ];
        for(Event t : eList) { t.Status__c = 'Completed'; }
        if(!eList.isEmpty()) {
          UPDATE eList;
        }
      
    }

    public void completarAtividade(List<Opportunity> opList) {
        System.debug('completarAtividade()');
        if(opList.isEmpty()) {
          return;
        }
        final  List<String> assuntoWhereList = new List<String> {
          confirmVisit, performVisit, confirmTestDrive, performTestDrive, custemerService
        };
        final  List<String> statusWhereList = new List<String> {
          'Completed', 'Deferred', 'Canceled'};

        List<Task> tList = [ SELECT Id FROM Task WHERE whatId IN: opList AND
                            Subject IN: assuntoWhereList AND Status Not In: statusWhereList ];
        for(Task t : tList) { t.Status = 'Completed'; }
        if(!tList.isEmpty()) {
          UPDATE tList;
        }

        List<Event> eList = [ SELECT Id FROM Event WHERE whatId IN: opList AND
                             Subject IN: assuntoWhereList AND Status__c Not In: statusWhereList ];
        for(Event t : eList) { t.Status__c = 'Completed'; }
        if(!eList.isEmpty()) {
          UPDATE eList;
        }
    }
    
    public static Task createTask(String assunto, Opportunity opp) {
        final Task task = new Task();
        task.OwnerId  = opp.OwnerId;
        task.WhatId = opp.Id;
        task.Subject = assunto;
        task.Status = 'In Progress';
        task.ActivityDate = date.today();
        task.IsReminderSet = true;
        task.ReminderDateTime = Utils.now();
        task.ActivityDatetime__c = Utils.now();
        task.IsVisibleInSelfService = true;
        return task;
    }

    public static Task createBdcTask(Id oppId, String subject) {
        System.debug('createBdcTask() subject:' + subject + 'oppId: ' + oppId);

        List<Opportunity> oppList = [
            SELECT Id, RescueOpportunity__c, Accept__c, ChaseOpportunity__c, ConfirmDeliveryOpportunity__c
            FROM Opportunity WHERE Id =: oppId
        ];
        if(oppList.isEmpty()) {
            return null;
        }
        final Opportunity opp = oppList.get(0);
        opp.OwnerId = UserInfo.getUserId();
        opp.RescueOpportunity__c          = false;
        opp.ChaseOpportunity__c           = false;
        opp.ConfirmDeliveryOpportunity__c = false;
        opp.Accept__c                     = 'Yes Accept';
        UPDATE opp;


        final Task tas = createTask(subject, opp);
        INSERT tas;

        return tas;
    }

    public static void gerarPreOrder(Opportunity op) {
        final List<String> fasesList = ActivityManagerBo.obterFasePreOrder();
        ActivityManagerBo.fecharAtividade(fasesList, op);
        //Opportunity opp = [SELECT ID, OwnerId FROM Opportunity WHERE Id =: op.ID];
        //Task task = createTask(negotiation, opp);
        //INSERT task;
    }

    public static void updateAtividadeRealizarTest(Opportunity opp, Datetime data) {
        System.debug('updateAtividadeRealizarTest() ');
        System.debug('opp: ' + opp);
        System.debug('data: ' + data);

        opp = [SELECT Id, OwnerId, CreatedDate, Expiration_Time__c FROM Opportunity WHERE Id =: opp.Id];
        List<Event> eventL = [
            SELECT Id FROM Event WHERE Subject =: performTestDrive
            AND WhatId =: opp.Id
        ];

        if(eventL.isEmpty()) {
            final Event event = createEvent(performTestDrive, opp);
            event.StartDateTime = data;
            event.EndDateTime   = data.addMinutes(15);
            event.ReminderDateTime = data.addMinutes(-15);
            event.ActivityDatetime__c = data.addMinutes(-15);
            event.Status__c = 'In Progress';
            INSERT event;

        } else if(!eventL.isEmpty()) {
            for(Event e : eventL) {
                e.StartDateTime = data;
                e.EndDateTime   = data.addMinutes(15);
                e.ReminderDateTime = data.addMinutes(-15);
                e.ActivityDatetime__c = data.addMinutes(-15);
                e.Status__c = 'Completed';
            }
            UPDATE eventL;
        }
    }

    public static void fecharAtividade(List<String> fases, Opportunity opp) {
        System.debug('fecharAtividade() ' + fases);
        if(fases.isEmpty()) {
          return;
        }
        List<Task> taskL = [
            SELECT Id FROM Task WHERE Subject In: fases AND Status != 'Completed'
            AND WhatId =: opp.Id
        ];
        for(Task t : taskL) {
            t.Status = 'Completed';
        }
        List<Event> eventL = [
            SELECT Id FROM Event WHERE Subject In: fases AND Status__c != 'Completed'
            AND WhatId =: opp.Id
        ];
        for(Event e : eventL) {
            e.Status__c = 'Completed';
        }
        if(!taskL.isEmpty()) {
          UPDATE taskL;
        }
        if(!eventL.isEmpty()) {
          UPDATE eventL;
        }
    }
	
    public static void executeFecharAtividades(List<Opportunity> oppList) {
        List<String> fases = new List<String>();
        fases.add('Confirm Test Drive');
        fases.add('Confirm Visit');
        fases.add('Contact');
        fases.add('Customer Service');
        fases.add('Delivery Confirmation');
        fases.add('Negotiation');
        fases.add('Perform Test Drive');
        fases.add('Perform Visit');
        fases.add('Persecution');
        fases.add('Pre-Order');
        fases.add('Prospection');
        fases.add('Rescue');
        
        fecharListaAtividades(fases, oppList);
    }
    
    @future
    public static void fecharListaAtividades(List<String> fases, Set<Id> oppsIds){
        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Id oppId : oppsIds){
            oppList.add( new Opportunity(Id=oppId) );
        }
        
        ActivityManagerBo.fecharListaAtividades(fases, oppList);
    }
    
    public static void fecharListaAtividades(List<String> fases, List<Opportunity> oppList) {
        System.debug('fecharListaAtividades() ' + fases);
        if(fases.isEmpty() || oppList.isEmpty()) {
          return;
        }
        final List<Task> taskList = [
            SELECT Id FROM Task WHERE Subject In: fases AND Status != 'Completed'
            AND WhatId IN: oppList
        ];
        for(Task task : taskList) {
            task.Status = 'Completed';
        }

        final List<Event> eventList = [
            SELECT Id FROM Event WHERE Subject In: fases AND Status__c != 'Completed'
            AND WhatId in: oppList
        ];
        for(Event event : eventList) {
            event.Status__c = 'Completed';
        }

        try {
          if(!taskList.isEmpty()) {
            UPDATE taskList;
          }
          if(!eventList.isEmpty()) {
            UPDATE eventList;
          }
        } catch(Exception ex) {
          System.debug(ex);
        }
    }

    public static List<String> obterFasePreOrder() {
        List<String> fases = obterFaseTestDrive();
        fases.add(performTestDrive);
        return fases;
    }

    public static List<String> obterFaseTestDrive() {
        List<String> fases = new List<String>();
        fases.add(confirmVisit);
        fases.add(confirmTestDrive);
        fases.add(custemerService);
        fases.add(performVisit);
        return fases;
    }

    private class ProprietarioOportunidadeAlterado {
        private Opportunity opp         { get;set; }
        private Opportunity oppOld      { get;set; }
        private Boolean newOwnerSeller  { get;set; }
        private Boolean oldOwnerSeller  { get;set; }
        public Boolean isTransferirTask() {
          return isNovoDonoVendedor() && isAntigoDonoVendedor();
        }
        public Boolean isNovoDonoVendedor() {
          return newOwnerSeller;
        }
        public Boolean isAntigoDonoVendedor() {
          return oldOwnerSeller;
        }
    }
}