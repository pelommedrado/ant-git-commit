/**
*   Class   -   VFC28_TestDriveDAO
*   Author  -   Surseh Babu
*   Date    -   29/10/2012
*
*   #01 <Suresh Babu> <29/10/2012>
*       DAO class for Test Drive object to Query records.
**/
public with sharing class VFC28_TestDriveDAO extends VFC01_SObjectDAO
{
    private static final VFC28_TestDriveDAO instance = new VFC28_TestDriveDAO();

    /*private constructor to prevent the creation of instances of this class*/
    private VFC28_TestDriveDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC28_TestDriveDAO getInstance() {
        return instance;
    }
        
    /**
    *   This Method was used to get Product Record 
    *   @param  dealerId - Dealer Id for fetch records.
    *   @param  carModel    -   get carModel to refine search criteria.
    *   @param  dateOfBooking   -   Date wants to book.
    *   @return lstTestDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    */
    public List<TDV_TestDrive__c> fetchTestDriveRecordsUsingCriteria( Id dealerId, String carModel, Date dateOfBooking ){
        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        
        lstTestDriveRecords = [SELECT 
                                    AGENDA__c, BuyingInterestAfterTestDrive__c, Comments__c, 
                                    CurrentModelVehicle__c, CurrentVehicle__c, Customer__c, 
                                    CustomerEmail__c, CustomerPhone__c, DateBooking__c, Dealer__c, 
                                    DepartureTime__c, Design__c, EquipmentLevel__c, FuelOutputLevel__c, 
                                    FuelReturnLevel__c, Handling__c, InternalSpace__c, Model__c, 
                                    Opportunity__c, OutputKm__c, PanelCommands__c, Performance__c, 
                                    ReasonCancellation__c, Id, ReturnKm__c, ReturnTime__c, Security__c, 
                                    Status__c, Name, TestDriveVehicle__c, TestDriveVersion__c, YearCurrentVehicle__c 
                                FROM 
                                    TDV_TestDrive__c
                                WHERE
                                    Dealer__c =: dealerId AND
                                    Model__c =: carModel AND
                                    DAY_ONLY(DateBooking__c) =: dateOfBooking
                                ];
        return lstTestDriveRecords;
    }
    
    /**
    *   This Method was used to get TestDrive Record 
    *   @param  dealerId           - dealer Id for fetch records.
    *   @param  carModel           - get carModel to refine search criteria.
    *   @param  startDateOfBooking - start date wants to book.
    *   @param  endDateOfBooking   - end date wants to book.
    *   @return lstTestDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    */
    public List<TDV_TestDrive__c> fetchTestDriveRecordsUsingCriteriaByRange(Id dealerId, String carModel, 
    	Date startDateOfBooking, Date endDateOfBooking)
    {
        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        
        lstTestDriveRecords = [SELECT AGENDA__c, BuyingInterestAfterTestDrive__c, Comments__c, 
                                      CurrentModelVehicle__c, CurrentVehicle__c, Customer__c, 
                                      CustomerEmail__c, CustomerPhone__c, DateBooking__c, Dealer__c, 
                                      DepartureTime__c, Design__c, EquipmentLevel__c, FuelOutputLevel__c, 
                                      FuelReturnLevel__c, Handling__c, InternalSpace__c, Model__c, 
                                      Opportunity__c, OutputKm__c, PanelCommands__c, Performance__c, 
                                      ReasonCancellation__c, Id, ReturnKm__c, ReturnTime__c, Security__c, 
                                      Status__c, Name, TestDriveVehicle__c, TestDriveVersion__c, YearCurrentVehicle__c 
                                 FROM TDV_TestDrive__c
                                WHERE Dealer__c =: dealerId 
                                  AND Model__c =: carModel
                                  AND DAY_ONLY(DateBooking__c) >= :startDateOfBooking
                                  AND DAY_ONLY(DateBooking__c) <= :endDateOfBooking];
        return lstTestDriveRecords;
    }
    
    /**
    *   This Method was used to get Product Record 
    *   @param  dealerId - Dealer Id for fetch records.
    *   @param  carModel    -   get carModel to refine search criteria.
    *   @param  dateTimeOfBooking   -   Date and Time wants to book.
    *   @return lstTestDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    */
    public List<TDV_TestDrive__c> fetchTestDriveRecordsUsing_DateAndTime( Id dealerId, String carModel, DateTime dateOfBooking ){
        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        
        lstTestDriveRecords = [SELECT 
                                    AGENDA__c, BuyingInterestAfterTestDrive__c, Comments__c, 
                                    CurrentModelVehicle__c, CurrentVehicle__c, Customer__c, 
                                    CustomerEmail__c, CustomerPhone__c, DateBooking__c, Dealer__c, 
                                    DepartureTime__c, Design__c, EquipmentLevel__c, FuelOutputLevel__c, 
                                    FuelReturnLevel__c, Handling__c, InternalSpace__c, Model__c, 
                                    Opportunity__c, OutputKm__c, PanelCommands__c, Performance__c, 
                                    ReasonCancellation__c, Id, ReturnKm__c, ReturnTime__c, Security__c, 
                                    Status__c, Name, TestDriveVehicle__c, TestDriveVersion__c, YearCurrentVehicle__c 
                                FROM 
                                    TDV_TestDrive__c
                                WHERE
                                    Dealer__c =: dealerId AND
                                    Model__c =: carModel AND
                                    DateBooking__c =: dateOfBooking
                                ];
        return lstTestDriveRecords;
    }
    
    /**
    *   This Method was used to get TestDrive Record 
    *   @param  TestDriveVehId - Test Drive Vehicle Id for fetch records.
    *   @param  StartDateTime   -   Date and Time wants to book.
    *   @return lstTestDriveRecords - fetch and return the List Test Drive Records from Test Drive object.
    */
    public List<TDV_TestDrive__c> fetchTestDriveRecordsUsingTestDriveVehId( Id TestDriveVehId, DateTime StartDateTime){
        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        
        lstTestDriveRecords = [SELECT 
                                    AGENDA__c, BuyingInterestAfterTestDrive__c, Comments__c, 
                                    CurrentModelVehicle__c, CurrentVehicle__c, Customer__c, 
                                    CustomerEmail__c, CustomerPhone__c, DateBooking__c, Dealer__c, 
                                    DepartureTime__c, Design__c, EquipmentLevel__c, FuelOutputLevel__c, 
                                    FuelReturnLevel__c, Handling__c, InternalSpace__c, Model__c, 
                                    Opportunity__c, OutputKm__c, PanelCommands__c, Performance__c, 
                                    ReasonCancellation__c, Id, ReturnKm__c, ReturnTime__c, Security__c, 
                                    Status__c, Name, TestDriveVehicle__c, TestDriveVersion__c, YearCurrentVehicle__c 
                                FROM 
                                    TDV_TestDrive__c
                                WHERE
                                    TestDriveVehicle__c =: TestDriveVehId And
                                    DateBooking__c = : StartDateTime
                                ];
        return lstTestDriveRecords;
    }
    
    public List<TDV_TestDrive__c> findByOpportunityId(String opportunityId, Set<String> setStatus) {
        List<TDV_TestDrive__c> lstSObjTestDrive = [
            SELECT DateBooking__c, Id, Name, Car_Engine__c, Car_Exchange__c,                                                     
            Opportunity__r.Account.Name,
            Opportunity__r.Account.PersLandline__c,
            Opportunity__r.Account.PersMobPhone__c,
            Opportunity__r.Account.VehicleInterest_BR__c,
            Opportunity__r.Account.YrReturnVehicle_BR__c,
            Opportunity__r.Account.CurrentVehicle_BR__c,
            Opportunity__r.Account.CurrentVehicle_BR__r.Model__c,
            Opportunity__r.Dealer__c,                                                       
            Opportunity__r.Name,                              
            Status__c,
            TestDriveVehicle__c,                                                        
            TestDriveVehicle__r.Vehicle__r.Color__c,
            TestDriveVehicle__r.Vehicle__r.Model__c,
            TestDriveVehicle__r.Vehicle__r.VehicleRegistrNbr__c,
            TestDriveVehicle__r.Vehicle__r.VersionCode__c,
            TestDriveVehicle__r.Vehicle__r.Version__c
            FROM TDV_TestDrive__c
            WHERE Opportunity__c =: opportunityId
            AND Status__c IN : setStatus
            ORDER BY CreatedDate DESC
        ];
        
        return lstSObjTestDrive;
    }
    
    public TDV_TestDrive__c insertData(TDV_TestDrive__c sObjTestDrive)
    {
        TDV_TestDrive__c sObjTestDriveAux = null;
    
        super.insertData(sObjTestDrive);
            
        sObjTestDriveAux = this.findById(sObjTestDrive.Id);
        
        return sObjTestDriveAux;
    }
    
    public TDV_TestDrive__c findById(String testDriveId)
    {
        TDV_TestDrive__c sObjTestDrive = null;
        
        sObjTestDrive = [SELECT BuyingInterestAfterTestDrive__c,
                                Comments__c, Car_Engine__c, Car_Exchange__c,   
                                CurrentModelVehicle__c,
                                CurrentVehicle__c,
                                Customer__c,
                                CustomerEmail__c,
                                CustomerPhone__c,
                                Dealer__c,
                                DateBooking__c,
                                DepartureTime__c,
                                Design__c,
                                EquipmentLevel__c,
                                FuelOutputLevel__c,
                                FuelReturnLevel__c,
                                Handling__c,
                                InternalSpace__c,
                                Model__c,
                                Name,
                                Opportunity__r.Account.Name,
                                Opportunity__r.Account.PersLandline__c,
                                Opportunity__r.Account.PersMobPhone__c,
                                Opportunity__r.Account.VehicleInterest_BR__c,
                                Opportunity__r.Account.YrReturnVehicle_BR__c,
                                Opportunity__r.Account.CurrentVehicle_BR__c,
                                Opportunity__r.Account.CurrentVehicle_BR__r.Model__c,
                                Opportunity__r.Dealer__c,
                                Opportunity__r.Name,                                                                
                                OutputKm__c,
                                OwnerId,
                                PanelCommands__c,
                                Performance__c,
                                ReasonCancellation__c,
                                ReturnKm__c,
                                ReturnTime__c,
                                Security__c,
                                Status__c,
                                TestDriveVehicle__c,
                                TestDriveVehicle__r.Vehicle__r.Color__c,
                                TestDriveVehicle__r.Vehicle__r.Model__c,
                                TestDriveVehicle__r.Vehicle__r.VehicleRegistrNbr__c,
                                TestDriveVehicle__r.Vehicle__r.VersionCode__c,
                                TestDriveVehicle__r.Vehicle__r.Version__c,
                                TestDriveVersion__c,
                                YearCurrentVehicle__c
                         FROM TDV_TestDrive__c     
                         WHERE Id =: testDriveId];
        
        return sObjTestDrive;
    }
    
    public List<TDV_TestDrive__c> findByStatusVehicleDatebooking(Set<String> setStatus, String testDriveVehicleId, Date dateBooking)
    {
        List<TDV_TestDrive__c> lstSObjTestDrive = [SELECT DateBooking__c,
                                                          Status__c
                                                   FROM TDV_TestDrive__c
                                                   WHERE Status__c IN : setStatus
                                                   AND TestDriveVehicle__c =: testDriveVehicleId
                                                   AND CALENDAR_YEAR(convertTimezone(DateBooking__c)) =: dateBooking.year()
                                                   AND CALENDAR_MONTH(convertTimezone(DateBooking__c)) =: dateBooking.month()
                                                   AND DAY_IN_MONTH(convertTimezone(Datebooking__c)) =: dateBooking.day()
                                                   ORDER BY DateBooking__c];
                                                   
                                                   
        return lstSObjTestDrive;
    }
        
}