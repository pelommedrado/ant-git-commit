/**
* Classe que representa os dados da página de qualificação de conta.
* @author Felipe Jesus Silva.
*/
public class VFC46_QualifyingAccountVO 
{
	public String accountId {get;set;}
	public String opportunityId {get;set;}
	public String firstName {get;set;}
	public String lastName {get;set;}
	public String numCL {get;set;}
	public Account sObjAccount {get;set;}
	public String email {get;set;}
	public Boolean optionPhone {get;set;} 
	public Boolean optionEmail {get;set;}
	public Boolean optionSMS {get;set;}
	public String persLandline {get;set;}
	public String profLandline {get;set;}
	public String profMobPhone {get;set;}
	public String persMobPhone {get;set;}
	public String typeOfInterest {get;set;}
	public String campaignBR {get;set;}
	public String vehicleInterestBR {get;set;}
    public String versionInterest {get;set;}
	public String secondVehicleOfInterest {get;set;}
    public String secondVersionInterest {get;set;}
	public String yrReturnVehicleBR {get;set;}
	public String shippingStreet {get;set;}
	public String shippingCity {get;set;}
	public String shippingState {get;set;}
	public String shippingPostalCode {get;set;}
	public String shippingCountry {get;set;}	
	public String reasonLossCancellation {get;set;}
	public String vehicleInterestBRBrand {get;set;} 
	public String vehicleInterestBRModel {get;set;}
	public String description {get;set;}
    public String sourcemedia { get;set; }
    public String oppApproach { get;set; }
    public String veiculoAtual { get;set; }
    public String versaoAtual { get;set; }
    
	public Id       campaignId  {get; set;}
	public Campaign sObjCampaign {get; set;}
	
	/* @Hugo Medrado {kolekto} Incusao de PromoCode */
	public String promoCode {get;set;}	
	
	/*picklists que serão exibidos na tela*/
	public List<SelectOption> lstSelOptionTypeOfInterest {get;set;}
	public List<SelectOption> lstSelOptionCampaign {get;set;}
	public List<SelectOption> lstSelOptionVehicleInterest {get;set;}
	public List<SelectOption> lstSelOptionSecondVehicleInterest {get;set;}
    public List<SelectOption> lstSelOptionsSourceMedia {get;set;}
	
	/*esse picklist será exibido somente quando o vendedor desejar cancelar a oportunidade, por isso não é inicializado no construtor*/
	public List<SelectOption> lstSelOptionReasonLossCancellation {get;set;}

	public VFC46_QualifyingAccountVO()
	{
		this.initialize();
	}
	
	private void initialize()
	{
		this.sObjAccount = new Account(); 
		this.sObjCampaign = new Campaign();
		
		/*cria os picklists que deverão ser exibidos assim que a tela for carregada*/
		this.lstSelOptionTypeOfInterest = VFC49_PickListUtil.buildPickList(Account.TypeOfInterest__c, '');
		this.lstSelOptionCampaign = VFC49_PickListUtil.buildPickList(Account.Campaign_BR__c, '');
		this.lstSelOptionVehicleInterest = VFC49_PickListUtil.buildPickList(Opportunity.Vehicle_Of_Interest__c, '');
		this.lstSelOptionSecondVehicleInterest = VFC49_PickListUtil.buildPickList(Account.SecondVehicleOfInterest__c, '');
        this.lstSelOptionsSourceMedia = VFC49_PickListUtil.buildPickList(Opportunity.SourceMedia__c, '');
	}
	
}