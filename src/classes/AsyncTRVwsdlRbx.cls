/**
 * Mock Test Class to test the RBX/SIC WebService 
 */
@isTest
global class AsyncTRVwsdlRbx implements WebServiceMock {
	
	public TRVwsdlrbx.getCustDataResponse_element sicResponse;

    /** @constructor **/
    global AsyncTRVwsdlRbx() {
        sicResponse = buildSICResponse();
    }
    
    /** Simulates invokation of the webservice **/
    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        system.debug('#### AsyncTRVwsdlRbx - doInvoke - BEGIN request=' + request);
        response.put('response_x', sicResponse);
        system.debug('#### AsyncTRVwsdlRbx - doInvoke - END');                                  
    }
    
    
    private TRVwsdlrbx.getCustDataResponse_element buildSICResponse() {
    	
    	TRVwsdlrbx.wsInfos wsInfos = new TRVwsdlrbx.wsInfos();
        wsInfos.wsVersion='Test';
        wsInfos.wsEnv='Test';
        wsInfos.wsDb='Test';
    	
    	TRVwsdlrbx.Survey pSurveyTest = new TRVwsdlrbx.Survey();
        pSurveyTest.surveyOK='Test';
        pSurveyTest.value='Test';
        pSurveyTest.loyMessage='Test';
    	
        TRVwsdlrbx.GetCustDataResponse pTestData = new TRVwsdlrbx.GetCustDataResponse();
        pTestData.clientList = new List<TRVwsdlrbx.PersonnalInformation>();
        
        TRVwsdlrbx.LogsCustData logsCust =  new TRVwsdlrbx.LogsCustData();
        logsCust.brand='Renault';
        logsCust.mode=1;
        logsCust.country='test';
        logsCust.brand='test';
        logsCust.demander='test';
        logsCust.vin='test';
        logsCust.lastName='test';
        logsCust.firstName='test';
        logsCust.city='test';
        logsCust.zip='test';
        logsCust.ident1='test';
        logsCust.idClient='test';
        logsCust.idMyr='test';
        logsCust.firstRegistrationDate='test';
        logsCust.registration='test';
        logsCust.email='test';
        logsCust.logDate='test';
        logsCust.ownedVehicles='test';
        logsCust.nbReplies='test';
        logsCust.wsVersion='test';
        logsCust.ipAdress='test';
        logsCust.codErreur='test';
        logsCust.dbCible='test';
        logsCust.wsHostname='test';
        logsCust.wsTexec=200001;
        
        TRVwsdlrbx.RepDynamicComponent repDynamicComp = new TRVwsdlrbx.RepDynamicComponent();
        repDynamicComp.repCode='test';
        repDynamicComp.typeCalc='test';
        repDynamicComp.ddo='test';
        repDynamicComp.kdo='test';
        repDynamicComp.dp3_0='test';
        repDynamicComp.dp3_1='test';
        repDynamicComp.dp3_2='test';
        repDynamicComp.dp3_3='test';
        repDynamicComp.dp3_4='test';
        repDynamicComp.dp3_5='test';
        repDynamicComp.dp3_6='test';
        repDynamicComp.dp3_7='test';
        repDynamicComp.dp3_8='test';
        repDynamicComp.dp3_9='test';
        TRVwsdlrbx.VehicleRepDynamic repDynamic = new TRVwsdlrbx.VehicleRepDynamic();
        repDynamic.repdynList = new List<TRVwsdlrbx.RepDynamicComponent>();
        repDynamic.repdynList.add(repDynamicComp);
        
        TRVwsdlrbx.Dealer dealer = new TRVwsdlrbx.Dealer();
        String birId1 = 'test1';
        String birId2 = 'test2';
        dealer.birId = new List<String>();
        dealer.birId.add(birId1);
        dealer.birId.add(birId2);
        
        TRVwsdlrbx.CustDataRequest custDataRequest = new TRVwsdlrbx.CustDataRequest();
        custDataRequest.mode='test';
        custDataRequest.country='test';
        custDataRequest.brand='test';
        custDataRequest.demander='test';
        custDataRequest.vin='test';
        custDataRequest.lastName='test';
        custDataRequest.firstName='test';
        custDataRequest.city='test';
        custDataRequest.zip='test';
        custDataRequest.ident1='test';
        custDataRequest.idClient='test';
        custDataRequest.typeperson='test';
        custDataRequest.idMyr='test';
        custDataRequest.firstRegistrationDate='test';
        custDataRequest.registration='test';
        custDataRequest.email='test';
        custDataRequest.sinceDate='test';
        custDataRequest.ownedVehicles='test';
        custDataRequest.nbReplies='test';
        
        TRVwsdlrbx.ServicePreferences servPref = new TRVwsdlrbx.ServicePreferences();
        servPref.irn='test';
        servPref.sia='test';
        servPref.reqid='test';
        servPref.userid='test';
        servPref.language='test';
        servPref.country='test';
        
        TRVwsdlrbx.GetCustDataRequest getCustData = new TRVwsdlrbx.GetCustDataRequest();
        getCustData.custDataRequest = custDataRequest;
        getCustData.servicePrefs = servPref;
        
        TRVwsdlrbx.getCustData_element custDataElement = new TRVwsdlrbx.getCustData_element();
        custDataElement.request = getCustData;        
        
        TRVwsdlrbx.GetCustDataError custDataError = new TRVwsdlrbx.GetCustDataError();
        custDataError.fault='test';
        
        TRVwsdlrbx.Desc_x descrip = new TRVwsdlrbx.Desc_x();
        descrip.type_x='test';
        descrip.typeCode='test';
        descrip.subType='test';
        descrip.subTypeCode='test';
        
        TRVwsdlrbx.WorkShop workshop = new TRVwsdlrbx.WorkShop();
        workshop.date_x='test';
        workshop.km='test';
        workshop.birId='test';
        workshop.origin='test';
        workshop.description=descrip;
        
        TRVwsdlrbx.contract contract = new TRVwsdlrbx.contract();
        contract.idContrat='test';
        contract.type_x='test';
        contract.techLevel='test';
        contract.serviceLevel='test';
        contract.productLabel='test';
        contract.initKm='test';
        contract.maxSubsKm='test';
        contract.subsDate='test';
        contract.initContractDate='test';
        contract.endContractDate='test';
        contract.status='test';
        contract.updDate='test';
        contract.idMandator='test';
        contract.idCard='test';
        
        TRVwsdlrbx.PersonnalInformation client = new TRVwsdlrbx.PersonnalInformation();
        client.IdClient = '7026526';
        client.LastName = 'FERRONI';
        client.middlename = 'TOTO'; 
        client.FirstName = 'ANTONIO';
        client.Lang = 'ENG';
        client.sex = 'F';
        client.title = 'M'; 
        client.birthDay = '1948-12-17';
        client.typeperson = 'P';
        client.typeIdent1 = '1';
        client.ident1 = 'FRRNTN48T17F156W';
        client.ident2 = 'FRRNTN48T17F156W1';
        client.contact = new TRVwsdlrbx.Contact();
        client.contact.phoneNum1 = '8136853';
        client.contact.phonecode1 = '+33';
        client.contact.phoneNum2 = '8136853';
        client.contact.phonecode2 = '+33';
        client.contact.phoneNum3 = '4858210';
        client.contact.phonecode3 = '+33';
        client.contact.email = 'ROSY.PARLAPARLA@GMAIL.COM';
        client.contact.optin = '0';
        client.address = new TRVwsdlrbx.Address();
        client.address.strName = 'FAMAGOSTA';
        client.address.compl2 = 'FAMAGOSTA';
        client.address.compl1 = 'FAMAGOSTA';
        client.address.compl3 = 'FAMAGOSTA';
       
        client.address.strType = 'VLE';
        client.address.strTypeLabel = 'VIALE';
        client.address.strNum = '34';
        client.address.countryCode = 'FRA';
        client.address.zip = '20142';
        client.address.city = 'MILANO';
        client.address.qtrCode = '015';
        client.address.dptCode = '146';
        client.address.sortCode = '0003332';
        client.address.areaCode = 'MI';
        
        
        client.CommunicationAgreement = new List<TRVwsdlRbx.CommunicationAgreement>();
        TRVwsdlRbx.CommunicationAgreement comm  = new TRVwsdlRbx.CommunicationAgreement();
        
        
        comm.CommunicationAgreementType = 'EMAIL';
        comm.Brand = '?';
        comm.CommunicationAgreement= 'Y';
        comm.CommunicationAgreementOrigin= '?';
        comm.UpdatedDate = String.valueOf(system.today());
        client.CommunicationAgreement.add(comm);
		comm.CommunicationAgreementType = 'PHONE';
		comm.Brand = '?';
        comm.CommunicationAgreement= 'Y';
        comm.CommunicationAgreementOrigin= '?';
        comm.UpdatedDate = String.valueOf(system.today());
        client.CommunicationAgreement.add(comm);
        comm.CommunicationAgreementType = 'SMS';
		comm.Brand = '?';
        comm.CommunicationAgreement= 'Y';
        comm.CommunicationAgreementOrigin= '?';
        comm.UpdatedDate = String.valueOf(system.today());
        client.CommunicationAgreement.add(comm);
        comm.CommunicationAgreementType = 'POSTAL ADD';
		comm.Brand = '?';
        comm.CommunicationAgreement= 'Y';
        comm.CommunicationAgreementOrigin= '?';
        comm.UpdatedDate = String.valueOf(system.today());
        client.CommunicationAgreement.add(comm);
        
        client.StopCommunication = new List<TRVwsdlRbx.StopCommunication>();
        TRVwsdlRbx.StopCommunication stop  = new TRVwsdlRbx.StopCommunication();
        stop.StopCommunicationType = 'ROB';
        stop.StopCommunication = 'Y';
        stop.UpdatedDate= String.valueOf(system.today());
        client.StopCommunication.add(stop);
		stop.StopCommunicationType = 'SMD';
        stop.StopCommunication = 'Y';
        stop.UpdatedDate= String.valueOf(system.today());
        client.StopCommunication.add(stop);
        
        
        
        client.vehicleList = new List<TRVwsdlrbx.Vehicle>();
        TRVwsdlrbx.Vehicle vehicle = new TRVwsdlrbx.Vehicle();
        vehicle.vin = 'VF17R5A0H48447489';
        vehicle.brandCode = 'RENAULT';
        vehicle.modelCode = 'CK4';
        vehicle.modelLabel = 'CLIO SPORTER';
        vehicle.firstRegistrationDate = '2013-09-26';
        vehicle.registration = 'ES847JH';
        vehicle.possessionBegin = '2013-09-30';
        vehicle.possessionEnd = '2013-09-30';
        vehicle.registrationDate = '2013-09-30';
        
        vehicle.paymentMethod = 'F';
        vehicle.purchaseNature = 'Own';
        vehicle.capacity = '898';
        vehicle.doorNum = '4';
        vehicle.versionLabel = 'DYR 09S E5';
        vehicle.new_x = 'o';
        
        //add contract
        vehicle.contractList = new List<TRVwsdlrbx.Contract>();
        TRVwsdlrbx.Contract cont = new TRVwsdlrbx.Contract();
       
        cont.idContrat='?';
	    cont.type_x = '?';
        cont.techLevel= '?';
        cont.serviceLevel= '?';
        cont.productLabel= '?';
        cont.initKm = '?';
        cont.maxSubsKm= '?';
        cont.subsDate = '?';
        cont.initContractDate= '?';
        cont.endContractDate = '?';
        cont.status= '?';
        cont.updDate= '?';
        cont.idMandator= '?';
        cont.idCard = '?';
        
        
        vehicle.contractList.add(cont);
        
        
        client.vehicleList.add(vehicle);
        pTestData.clientList.add(client);
        TRVwsdlrbx.getCustDataResponse_element sicResponse = new TRVwsdlrbx.getCustDataResponse_element();
		 
		TRVwsdlrbx.CrmGetCustData crm = new TRVwsdlrbx.CrmGetCustData();
		TRVwsdlrbx.GetCustDataResponse res = new TRVwsdlrbx.GetCustDataResponse();
		//sicResponse = crm.getCustData(getCustData);
		sicResponse.response = pTestData;

        return sicResponse; 
    }
}