/**
Last Modified : 20/04/2016 by Ludovic Marboutie 
Tested by : TRVWebservicesManager_Test
**/

public with sharing class TRVWebservicesManager 
{
        
        public static TRVResponseWebservices requestSearchParty(TRVRequestParametersWebServices.RequestParameters eparams)
        {
            TRVResponseWebservices response;
            
            eparams = getcountryInfo(eparams);
            //eparams.mDataSourceType
            system.debug('##Ludo check datasource : ' + eparams.mDataSourceType);
            
            system.debug('#### TRVWebservicesManager - requestSearchParty - TRV - BEGIN'); 
            system.debug('#### DataSource is : ' + eparams.mDataSourceType);
            
            if (eparams.mDataSourceType == 'MDM')
				response = TRVWebservicesMDM.getAccountSearchParty(eparams);
            else if (eparams.mDataSourceType == 'BCS')
                response = TRVWebservicesBCS.getAccountSearch(eparams);
            else if (eparams.mDataSourceType == 'SIC' || eparams.mDataSourceType == 'RBX') 
                response = TRVWebservicesRBX.getAccountSearch(eparams);
            
            system.debug('#### TRVWebservicesManager - requestSearchParty - TRV - RESPONSE : ' + response);
            system.debug('#### TRVWebservicesManager - requestSearchParty - TRV - END'); 
            
            return response;
        }
        public static TRVResponseWebservices requestGetParty(TRVRequestParametersWebServices.RequestParameters eparams)
        {
            //method is only activate for mdm webservices because BCS and SIC don't have getParty in wsdl 
            TRVResponseWebservices response;
            
            system.debug('#### TRVWebservicesManager - requestGetParty - TRV - BEGIN');
            
            eParams = getcountryInfo(eparams);
            response = TRVWebservicesMDM.getAccountGetParty(eParams); 
            
            system.debug('#### TRVWebservicesManager - requestGetParty - TRV - END');
            return response;
        }
        
        
        
        public static TRVRequestParametersWebServices.RequestParameters getcountryInfo(TRVRequestParametersWebServices.RequestParameters eParams)
        {
            system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - BEGIN');
            
            
            Country_Info__c c; 
            User u = [Select Id, RecordDefaultCountry__c, UserType__c from user where Id = :UserInfo.getUserId()];
            system.debug('##Ludo pays getcountryInfo : ' + u.RecordDefaultCountry__c);
            if (u != null)
                c = [Select Datasource__c, Call_mode__c, DataSourceOrigin__c, Country_Code_2L__c, Country_Code_3L__c, Business_Id1__c, Business_Id2__c, Personal_Id1__c, Personal_Id2__c from Country_Info__c where Name = :u.RecordDefaultCountry__c LIMIT 1];
            else 
               system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - User is null [Warning]');
            
            system.debug('##Ludo result custom setting country info : ' + c);
            system.debug('##Ludo technicalCountry : ' + eParams.technicalCountry);
            
            if (c != null) 
            {
                if (String.isEmpty(c.Datasource__c) == false && String.isEmpty(eParams.technicalCountry) == true) // it's not region
                {
                	
                   system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - Country DataSource : ' + c.Datasource__c);
                    eparams.mDataSourceType = c.Datasource__c;
                    eparams.CountrySF = u.RecordDefaultCountry__c; //Save country for SF 
                    if (c.Datasource__c =='MDM')
                    {
                        eparams.country = c.Country_Code_3L__c;
                        eparams.BusinessId1 = c.Business_Id1__c;
                        eparams.BusinessId2 = c.Business_Id2__c;
                        eparams.PersonalId1 = c.Personal_Id1__c;
                        eparams.PersonalId2 = c.Personal_Id2__c;
                        if (eparams.callmode == null)
                        	eparams.callmode = c.Call_mode__c;
                    }
                    else if(c.Datasource__c =='BCS')
                    {
                        eparams.country = c.Country_Code_2L__c;
                        if (eparams.callmode == null)
                        	eparams.callmode = c.Call_mode__c; 
                    }
                    else if((c.Datasource__c =='SIC' || c.Datasource__c =='RBX'))
                    {
                        eparams.country = c.Country_Code_3L__c;
                        eparams.country2L = c.Country_Code_2L__c;
                        if (eparams.callmode == null)
                        	eparams.callmode = c.Call_mode__c; 
                    }
                    eparams.DataSourceOrigin = c.DataSourceOrigin__c;                
                    return eparams; 
                }
                else //region 
                {
                    //see if the datasource is empty 
                    //eParams.technicalCountry is value selected with picklist in controller Rforce
                    c = [Select Datasource__c, Call_mode__c, DataSourceOrigin__c, Country_Code_2L__c, Country_Code_3L__c, Business_Id1__c, Business_Id2__c, Personal_Id1__c, Personal_Id2__c from Country_Info__c where Name = :eParams.technicalCountry LIMIT 1]; 
                    if (c != null) 
                    {
                       system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - Country Region DataSource : ' + c.Datasource__c);
                     
                        
                        //region 
                        if (u.RecordDefaultCountry__c == 'Adriatic')
                            eparams.AdriaticCountrySF = eParams.technicalCountry;
                        else if (u.RecordDefaultCountry__c == 'MidCE')
                            eparams.MiDCECountrySF = eParams.technicalCountry;
                        else if (u.RecordDefaultCountry__c == 'Nordic')
                            eparams.NRCountrySF = eParams.technicalCountry;
                        else if (u.RecordDefaultCountry__c == 'Poland')
                            eparams.PlCountrySF = eParams.technicalCountry;
                        else if (u.RecordDefaultCountry__c == 'UK-IE')
                            eparams.UkIeCountrySF = eParams.technicalCountry; 
                        else if (u.RecordDefaultCountry__c == 'CS')
                            eparams.countrySF = eParams.technicalCountry;
                        
                        eparams.mDataSourceType = c.Datasource__c;
                        if (c.Datasource__c =='MDM')
                        {
                            eparams.country = c.Country_Code_3L__c;
                            eparams.BusinessId1 = c.Business_Id1__c;
                            eparams.BusinessId2 = c.Business_Id2__c;
                            eparams.PersonalId1 = c.Personal_Id1__c;
                            eparams.PersonalId2 = c.Personal_Id2__c;
                             
                            if (eparams.callmode == null)
                            	eparams.callmode = c.Call_mode__c; 
                        }
                        else if(c.Datasource__c =='BCS')
                        {
                            eparams.country = c.Country_Code_2L__c;
                            if (eparams.callmode == null)
                            	eparams.callmode = c.Call_mode__c; 
                        }
                        else if((c.Datasource__c =='RBX' || c.Datasource__c =='SIC'))
                        {
                            eparams.country = c.Country_Code_3L__c;
                            eparams.country2L = c.Country_Code_2L__c;
                            if (eparams.callmode == null)
                            	eparams.callmode = c.Call_mode__c; 
                        }
                        eparams.DataSourceOrigin = c.DataSourceOrigin__c;    
                        return eparams;
                    }
                    else 
                    {
                       	system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - NO Country DataSource [Warning]');
                        return null;
                    }           
                    
                    return null;
                }
            }
           system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - Global Error Country DataSource [Warning]');
           system.debug('#### TRVWebservicesManager - getcountryInfo - TRV - END');
            return null; 
        }
        
		public static Id createAccountWSPage(TRVResponseWebservices.InfoClient response)
		{
			return TRVUtils.createAccountWSPage(response); 
		}
        
        
        
         
}