@isTest
private class Rforce_Rbx_WS_Test implements HttpCalloutMock {
	
	 private HttpResponse resp;
	 
     public Rforce_Rbx_WS_Test(String testBody) {
       resp = new HttpResponse();
       resp.setBody(testBody);
       resp.setStatusCode(200);
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        String formattedtext='<response><clientList><idClient>7026526</idClient><lastName>FERRONI</lastName><title xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><lang>ITA</lang><middleName xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><firstName>ANTONIO</firstName><distinction1 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><distinction2 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><initiales xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><typeIdent1>1</typeIdent1><ident1>FRRNTN48T17F156W</ident1><ident2 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><idMyr xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><typeperson>P</typeperson><birthDay>1948-12-17</birthDay><sex>1</sex><soc xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><legalCategory xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><contact><phoneCode1>02</phoneCode1><phoneNum1>8136853</phoneNum1><phoneCode2>02</phoneCode2><phoneNum2>8136853</phoneNum2><phoneCode3>338</phoneCode3><phoneNum3>4858210</phoneNum3><email>ROSY.PARLAPARLA@GMAIL.COM</email><preferredCom xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><optin>O</optin></contact><address><strName>FAMAGOSTA</strName><strNum>34</strNum><compl1 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><compl2 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><compl3 xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><strType>VLE</strType><strTypeLabel>VIALE</strTypeLabel><countryCode>ITA</countryCode><zip>20142</zip><city>MILANO</city><qtrCode>015</qtrCode><dptCode>146</dptCode><sortCode>0003332</sortCode><numberPB xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><zipPB xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><cityPB xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><areaCode>MI</areaCode><areaLabel xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><addressUpdateDate xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/></address><hasRenaultCard xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><flagMyR xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><vehicleList><vin>VF17R5A0H48447489</vin><brandCode>RENAULT</brandCode><brandLabel>Renault</brandLabel><modelCode>CK4</modelCode><modelLabel>CLIO SPORTER</modelLabel><versionLabel>DYR 09S E5</versionLabel><registrationDate xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><firstRegistrationDate>2013-09-26</firstRegistrationDate><registration>ES847JH</registration><new>N</new><energy>1</energy><fiscPow xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><dynPow xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><doorNum>4</doorNum><transmissionType xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><capacity>898</capacity><colorCode xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><possessionBegin>2013-09-30</possessionBegin><paymentMethod>F</paymentMethod><technicalInspectionDate xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><possessionEnd xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><purchaseNature>POS</purchaseNature></vehicleList><dealerList><birId>38002860</birId></dealerList><survey xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/></clientList><wsInfos><wsVersion>3.8.3</wsVersion><wsEnv>PROD</wsEnv><wsDb>DWS</wsDb></wsInfos><responseCode xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><nbReplies>1</nbReplies></response>';
        
         HttpCalloutMock mock = new Rforce_Rbx_WS_Test(formattedtext);
         Test.setMock(HttpCalloutMock.class, mock);
        
        Rforce_Rbx_WS testgetInfo = new Rforce_Rbx_WS();
        String testget = testgetInfo.getCustData('VF17R5A0H48447489', '', 'ITA', 'FERRONI', 'ANTONIO', 'RENAULT', '', '', '', '');
       
       System.debug('Resultat en durrrrrr'+formattedtext);
       System.debug('Resultat attendu'+testget);
       
       // System.assertEquals(formattedtext, testget);
    }
}