/**
Class   -   VFC09_LeadDAO_Test
Author  -   RameshPrabu
Date    -   05/09/2012

#01 <RameshPrabu> <05/09/2012>
Created this class using test for VFC09_LeadDAO.
**/
@isTest 
public with sharing class VFC09_LeadDAO_Test {
    
    static testMethod void VFC09_LeadDAO_Test() {
        // TO DO: implement unit test
        Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where 
                                  (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc','Personal_Acc'))])
        {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        // List<Account> lstAccount = [Select Id, FirstName, LastName from Account where recordTypeId= :recTypeIds.get('Personal_Acc')];
        List<Account> lstAccount = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        Test.startTest();
        Lead lead = VFC09_LeadDAO.getInstance().findLeadByLeadId(leads[0].Id);
        Test.stopTest();
        system.debug('lead Details...' +lead);
        System.assert(lead != null);
        
        List<Lead> leadSearch = VFC09_LeadDAO.getInstance().fetchLeadUsingNameOrCPF(leads[1].FirstName, leads[1].LastName, leads[1].CPF_CNPJ__c);
        String leadQuery = 'Select Id, FirstName, LastName from Lead';
        List<Lead> leadQuerySearch = VFC09_LeadDAO.getInstance().fetchLeadsByQueryString(leadQuery);
        Id leadSearchAcc = VFC09_LeadDAO.getInstance().fetchLeadUsingByAccoundRecord(lstAccount[0].FirstName, lstAccount[0].LastName);
        Id leadSearchAcc1 = VFC09_LeadDAO.getInstance().fetchLeadUsingByAccoundId(lstAccount[1].Id);
        
        VFC09_LeadDAO.getInstance().updateData(leads[0]);
        
        //modificado felipe livino
        Model__c model = new Model__c(
            //ENS__c = 'abc',
            Market__c = 'abc',
            Status__c = 'Active',
            Model_Spec_Code__c = 'ABC',
            Phase__c = '1',
            Model_PK__c = 'ABC-1'
        );
        Database.insert( model );
        
        PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
            Model__c = model.Id,
            Start_Date__c = System.today().addMonths( -1 ),
            End_Date__c = System.today().addMonths( 1 ),
            Type_of_Action__c = 'abc',
            Status__c = 'Active'
        );
        Database.insert( commercialAction );
        
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
            Commercial_Action__c = commercialAction.Id,
            Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
            Call_Offer_End_Date__c = System.today().addMonths( 1 ),
            Minimum_Entry__c = 100.0,
            Period_in_Months__c = 5,
            Month_Rate__c = 0.99
        );
        Database.insert( callOffer );
        
        User manager = new User(
            
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
            BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
            Name = 'Concessionária teste',
            IDBIR__c = '123ABC123',
            RecordTypeId = '012D0000000KAoH',
            Country__c = 'Brazil',
            NameZone__c = 'R2',
            ShippingState = 'SP',
            ShippingCity = 'São Paulo',
            OwnerId = manager.Id,
            Active_PV__c = true,
            isDealerActive__c = true
        );
        Database.insert( dealerAcc );
        
        
        
        Group_Offer__c group_offer = new Group_Offer__c(
            Status__c = 'Active',
            Type_of_Action__c ='TV',
            Type_of_Offer__c = 'Cooperada',
            Number_Offer__c = '2'
        );
        Database.insert(group_offer);
        
        Offer_Item__c OfferItem = new Offer_Item__c(
        Code__c = 'Code'
        );
        insert OfferItem;
        
        Offer__c offer = new Offer__c(
            Status__c = 'Active',
            Group_Offer__c = group_offer.ID,
            Stage__c = 'Approved',
            Featured_In_Offer__c = 'Complete',
            Total_Inventory_Vehicle__c = 3,
            Minimum_Input__c = 50,
            Number_Of_Installments__c = 60,
            Monthly_Tax__c = 0.99,
            Coefficient__c = 0.0225,
            Value_From__c = 30000,
            ValueTo__c = 30000,
            Entry_Value__c = 15000,
            Installment_Value__c = 545.00,
            Pricing_Template_Lookup__c = OfferItem.Id
        );
        Database.insert(offer);
        //---------------------------------     
        
        Lead leadInsert = new Lead(
            LastName = 'Ramesh',
            //RecordTypeId = recTypeIds.get('Marketing'),
            FirstName = 'Prabu',
            SubSource__c = 'RCI',
            Status = 'Identified',
            Phone = '9876262282',
            BusinessPhone__c = '873773',
            BusinessMobilePhone__c= '8348844',
            OptinEmail__c = 'Y',
            OptinPhone__c = 'Y',
            OptinSMS__c = 'Y',
            CPF_CNPJ__c = '24433278769',
            VehicleOfInterest__c = 'Duster',
            SecondVehicleOfInterest__c = 'Logan',
            IdDealer__c = dealerAcc.Id,
            IdOffer__c = offer.Id,
            Offer__c = offer.Id,
            NUMDBM__c = 1);
        VFC09_LeadDAO.getInstance().insertData(leadInsert);
        
        Campaign aCampaign = new Campaign(
            name='SITE OFERTAS',
            DBMCampaignCode__c = '1234',
            Type = 'Internet',
            Status = 'Planned',
            DealerInclusionAllowed__c = true,
            WebToOpportunity__c = true,
            ManualInclusionAllowed__c = true,
            ExpectedResponse = 0.00,
            NumberSent = 0
            //RecordType = 'Single Campaign'
        );
        Database.insert(aCampaign);
        
        Lead lea = new Lead(
            LastName = 'Ramesh',
            //RecordTypeId = recTypeIds.get('Marketing'),
            FirstName = 'Prabu',
            SubSource__c = 'RCI',
            Status = 'Identified',
            Phone = '9876262282',
            BusinessPhone__c = '873773',
            BusinessMobilePhone__c= '8348844',
            OptinEmail__c = 'Y',
            OptinPhone__c = 'Y',
            OptinSMS__c = 'Y',
            CPF_CNPJ__c = '24433278769',
            VehicleOfInterest__c = 'Duster',
            SecondVehicleOfInterest__c = 'Logan',
            IdDealer__c = dealerAcc.Id,
            NUMDBM__c = 1);
        VFC09_LeadDAO.getInstance().insertData(lea);
        
        List<Lead> leadAccList = 
            VFC09_LeadDAO.getInstance().returnLeadsWithAccountId(dealerAcc.Id);
        
        //VFC131_LeadBO le = new VFC131_LeadBO();
        //VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(leadInsert,aCampaign);
        //VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lea,aCampaign);
        
        
        // Tests
        Map<Id, Lead> test01 = VFC09_LeadDAO.getInstance().returnMapWithIdAndLeadRecord(new Set<String>{leadInsert.Id});
        Map<Decimal, Lead> test02 = VFC09_LeadDAO.getInstance().returnLeadRecordsUsingNUMDBMField(new Set<Decimal>{1});
        Map<String, List<Lead>> test03 = VFC09_LeadDAO.getInstance().returnLeadsUsingCPF_CNPJ(new Set<String>{'24433278769'});
        List<Lead> test04 = VFC09_LeadDAO.getInstance().returnLeadsWithAccountId(lstAccount[1].Id);        
    }
}