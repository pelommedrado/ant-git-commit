@isTest
private class BatchExpiredOpportunityTest {
    static testMethod void batchTest(){
        Id dealerRecordTypeId   = Utils.getRecordTypeId('Account','Network_Site_Acc');
        Id parentId             = SandboxBuild.criaConcessionariaGrupo(dealerRecordTypeId);
        Account dealer          = SandboxBuild.criaConcessionaria(parentId, dealerRecordTypeId);
        
        dealer.DealerDirectorPhone__c = '5511987623411';
        dealer.DealerDirectorFstName__c = 'Luiz Prandini';
        
        dealer.DealerManagerPhone__c = '5511987623411';
        dealer.DealerManagerFstName__c = 'Joaquim Gonzales';
        
        dealer.OwnerPhone__c = '5511987623411';
        dealer.DealerOwner__c =  'Maria Gonzales';
        
        update dealer;
        
        List<Account> lstAccount = new List<Account>();
        lstAccount.add(dealer);
        
        Test.startTest();
            BatchExpiredOpportunity batch = new BatchExpiredOpportunity();
            Database.executeBatch(batch);
           
            String cronExpr = '0 0 0 15 3 ? 2022'; 
        
            Id batchInstanceId = Database.executeBatch(new BatchExpiredOpportunity());
            batch.execute(null, lstAccount);
            System.schedule('myJobTestJobName', cronExpr, new BatchExpiredOpportunity());
        
        Test.stopTest();
    }

}