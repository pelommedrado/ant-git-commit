public class TriggerAdministration {

  //List of triggers that can be bypassed
  public static final String PAD_BypassTrigger;
  public static final Boolean PAD_DebugMode { get; set; }

  public static final String userRoleName;  //User Role Name
  public static final String userProfileName;//User Profile Name

  static {
    final User user = [
      SELECT UserRole.Name, Profile.Name,
        UserPreferencesApexPagesDeveloperMode, PAD_BypassTrigger__c
      FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1
    ];

    PAD_BypassTrigger = ';' + user.PAD_BypassTrigger__c + ';';
    PAD_DebugMode     = user.UserPreferencesApexPagesDeveloperMode;
    userRoleName      = user.UserRole.Name;
    userProfileName   = user.Profile.Name;
  }

  //List of Apex codes that should run only once. Add any code to the list
  private static final Set<String> requiredOnce = new Set<String> {
    'Task.fillTaskType', 'Task.fillTaskSubject',
    'CORE-APEX', 'ADM-APEX', 'Account.onBeforeUpdate', 'Account.onBeforeInsert',
    'Account.onAfterInsert', 'Account.onAfterUpdate', 'Task.onAfterInsert',
    'Task.onAfterUpdate','Task.onBeforeInsert','Task.onBeforeUpdate',
    'Case.onAfterInsert','Case.onAfterUpdate','Case.onBeforeInsert',
    'Case.onBeforeUpdate','CaseComment.onAfterInsert','Attachment.onAfterInsert',
    'VehicleRelation.onAfterUpdate','VehicleRelation.onAfterInsert',
    'Attachment.onAfterInsert','EmailMessage.onBeforeInsert'
  };

  //List of Apex code that has already been run. Keep this list empty.
  private static Set<String> hasRun = new Set<String>();

  public static boolean canTrigger(String apexName) { //If no bypass
    //If it should run Once
    if(requiredOnce.contains(apexName)) {
        //Already run, should not run
        if(hasRun.contains(apexName)) { 
          return false;
        }
        //Never run, can run only if not bypassed
        hasRun.add(apexName);
    }
    return PAD_BypassTrigger.indexOf(';' + apexName + ';') == -1;
  }
}