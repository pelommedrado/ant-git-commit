/**
* Classe controladora da página de orquestração de oportunidade.
* Essa classe é responsável por verificar qual página relacionada a oportunidade que deve ser chamada.
* @author Felipe Jesus Silva.
*/
public class VFC50_OpportunityOrchestrationController {
	private String opportunityId;
	
	public VFC50_OpportunityOrchestrationController(ApexPages.StandardController controller) {
		this.opportunityId = controller.getId();
    }
	
	public PageReference redirect() {
		PageReference pageRef = null;
		
		if(this.opportunityId == null || this.opportunityId == ''){
			return new PageReference('/apex/VFP03_SearchForAccountAndLead');
		}
		
		VFC53_OpportunityDestinationType opportunityDestination = 
            VFC51_OppOrchestrationBusinessDelegate.getInstance().getOpportunityDestination(this.opportunityId);
        
        
        /*verifica o destino da oportunidade, ou seja, para qual página que deve ser direcionada*/	
        if(opportunityDestination == VFC53_OpportunityDestinationType.QUALIFYING_ACCOUNT) {
            pageRef = new PageReference('/apex/ClienteOportunidadeSfa2?Id=' + this.opportunityId);
            
		} 
        
        List<Quote> quoteList = obterCotacao(this.opportunityId);
        String quoteId = null;
        
        if(!quoteList.isEmpty()) {
            quoteId = quoteList.get(0).Id;
        }
        
        if(opportunityDestination == VFC53_OpportunityDestinationType.TEST_DRIVE) {
            //pageRef = new PageReference('/apex/VFP08_TestDrive?Id=' + this.opportunityId);
            pageRef = new PageReference('/apex/ClienteTestDriveSfa2?Id=' + this.opportunityId + '&quoteId=' + quoteId);
            
        } else if(opportunityDestination == VFC53_OpportunityDestinationType.QUOTE) {
			pageRef = new PageReference('/apex/ClienteOportunidadeDetalheSfa2?Id=' + this.opportunityId);
		
        }
				
		return pageRef;
	}
    
    private List<Quote> obterCotacao(String oppId) {
        return [
            SELECT Id
            FROM Quote
            WHERE OpportunityId =: oppId
        ];
    }
}