@IsTest
public class AccountBuild {
    
    public static AccountBuild instance = null;
    
    public static AccountBuild getInstance() {
        if(instance == null) {
            instance = new AccountBuild();
        }
        return instance;
    }
    
    private AccountBuild() {
    }
    
    public Account createAccountPersoal() {
        final Id recordType = 
            Utils.getRecordTypeId('Account', 'Personal_Acc');
        
        final Account acc = createAccount(recordType);
        acc.FirstName 				= 'Account';
        acc.LastName 				= 'Test';
        acc.PersEmailAddress__c 	= 'account@test.com';
        acc.VehicleInterest_BR__c 	= 'CLIO';
        acc.PersMobPhone__c 		= '45214524';
        acc.PersLandline__c 		= '45214578';
        return acc;
    }
    
    public Account createAccountDealer() {
        final Id recordType = 
            Utils.getRecordTypeId('Account', 'Network_Site_Acc');
        
        final Account acc = createAccount(recordType);
        acc.Name 		 = 'Account Dealer';
        acc.IDBIR__c 	 = '1234';
        return acc;
    }
    
    private Account createAccount(Id recordType) {
        Account acc = new Account();
        acc.RecordTypeId = recordType;
        return acc;
    }
}