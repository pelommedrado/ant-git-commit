/**
 * Superclass for batches. It provides:
 *   - generic logging inside Logger__c custom object
 */
global abstract with sharing class INDUS_Batch_AbstractBatch_CLS implements Database.Batchable<sObject>, Database.Stateful {
	/**
     *Whether or not logs are active. 
    */
    private Boolean activeLog ;
    /** 
     *Batch Class name
     */
    private String batchClassName;
    
    /**
     *Liste of errors to log at the end of the execution.
     *Warning: remains null when activeLog is false.
     */
    private List<RecordError> lErrors;
    
    private Datetime dtStartTime; 

	/**
     *Returns class name.
     */
    global abstract String getClassName(); 

    /**
     *Processes a chunk of data.
     */     
    global abstract DatabaseAction doExecute(Database.BatchableContext info, List<sObject> scope);

    /**
     *Finish.
     */     
    global abstract void doFinish(Database.BatchableContext info);
       
    /**
     *Total scope
     */
    private Integer totalScope = 0;

    /**
     *Number of records created
     */
    private Integer nbRecordsCreated = 0;

    /**
     *Number of records updated
     */
    private Integer nbRecordsUpdated = 0;

    /**
     *Number of records deleted
     */
    private Integer nbRecordsDeleted = 0;

    /**
     *Number of email sent
     */
    private Integer nbEmailSent = 0;

    /**
     *Number of error of insert
     */
    private Integer nbErrorInsert = 0;

    /**
     *Number of error of update
     */
    private Integer nbErrorUpdate = 0;

    /**
     *Number of error deletion
     */
    private Integer nbErrorDelete = 0;
    
    /**
     *Number of error on email sending
     */
    private Integer nbErrorEmail = 0;

    /**
     *Project Name
     */
    private INDUS_Logger_CLS.ProjectName projectName;
    
    /**
    *Wraps a database error
    */
    class RecordError {
        SObject record; 
        String methode;
        String message;
        Exception except;
        
        RecordError(SObject rec, String met, String msg, Exception exc){
            record = rec;
            methode = met;
            message = msg;
            except = exc;
        }
    }
    
    @TestVisible
    private Exception dbexception;  
    
    /**
    *Database actions
    */
    @TestVisible
    public enum Action { 
        ACTION_CREATE, 
        ACTION_UPDATE, 
        ACTION_DELETE
    }
    
    global class DatabaseAction{
        /**
         *list of SObjects to process. 
         */
        List<SObject> lSObjects;
        /**
         *Required action
         */
        Action action;
        /**
        *None or all mode
        */
        Boolean noneOrAll;
        
        /**
        *Constructor.
        */
        public DatabaseAction(List<SObject> lSObjects, Action action, Boolean noneOrAll){
            this.lSObjects = lSObjects;
            this.action = action;
            this.noneOrAll = noneOrAll;
        }
    }

	/**
     *Constructor.
     */
    public INDUS_Batch_AbstractBatch_CLS(){
        String str;
        this.batchClassName = getClassName();
        str=this.batchClassName.substring(0, this.batchClassName.indexOf('_'));
        if(str.equalsIgnoreCase('MYR')){
        	projectName=INDUS_Logger_CLS.ProjectName.MYR;
        }else if (str.equalsIgnoreCase('LMT')){
	        projectName=INDUS_Logger_CLS.ProjectName.LMTSFA;
        }else if (str.equalsIgnoreCase('SFA')){
	        projectName=INDUS_Logger_CLS.ProjectName.LMTSFA;
	    }else if (str.equalsIgnoreCase('RFORCE')){
		    projectName=INDUS_Logger_CLS.ProjectName.RFORCE;
	    }else if (str.equalsIgnoreCase('INDUS')){
		    projectName=INDUS_Logger_CLS.ProjectName.INDUS;
        }
        System.debug( '#### INDUS_Batch_AbstractBatch_CLS - <init> - Launching batch:' + batchClassName);
        CS22_INDUS_Logger__c setting = CS22_INDUS_Logger__c.getInstance(batchClassName);
        if(setting != null){
            activeLog = setting.Active__c; 
        }else{
            activeLog = false;
        }
        dtStartTime = Datetime.now();
        if (activeLog){
            lErrors = new List<RecordError>();
        }
    }
       
    global void execute(Database.BatchableContext info, List<sObject> scope){
        try{
            totalScope += scope.size();
            DatabaseAction dbAction = doExecute(info, scope);
            executeActionWithoutTryCatch(dbAction);
        }catch(Exception e){
            if (activeLog){
                lErrors.add(new RecordError(null, 'execute', 'Exception:' + e.getMessage(), e));
            }
            throw e; 
        }
    }   

    global void finish(Database.BatchableContext info) {
        System.debug('#### INDUS_Batch_AbstractBatch_CLS - finish - JobId:' + info.getJobId());
        if (activeLog){
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, ExtendedStatus, CompletedDate FROM AsyncApexJob WHERE Id = :info.getJobId()];
            // check whether batch has succeeded or failed
            String status;
            if (a.NumberOfErrors + nbErrorInsert + nbErrorUpdate + nbErrorDelete > 0){
                status = 'Failed';
            }else{
                status = 'Success';
            } 
            String logRunId = batchClassName + DateTime.now().getTime();
            Id idParentLog = INDUS_Logger_CLS.addGroupedLogSupParent ( 
            	logRunId
            	, null
            	, projectName
            	, batchClassName
            	, 'finish'
            	, null
            	, status
            	, a.Id
            	, INDUS_Logger_CLS.ErrorLevel.Info.name()
				, a.Id
				, a.CompletedDate
				, a.Status
				, a.TotalJobItems
				, a.JobItemsProcessed
				, a.NumberOfErrors
            	, a.ExtendedStatus
            	, dtStartTime
            	, totalScope
            	, nbRecordsCreated
            	, nbRecordsUpdated
            	, nbRecordsDeleted
            	, nbErrorInsert
            	, nbErrorUpdate
            	, nbErrorDelete
            	, NbEmailSent
            	, NbErrorEmail
            	);
            for (RecordError err : lErrors){  
                INDUS_Logger_CLS.addGroupedLogSupFils( 
                	logRunId
                	, idParentLog
                	, projectName
                	, batchClassName
                	, err.methode
                	, String.valueOf(err.record), err.except != null ? err.except.getStackTraceString() : null
                	, 'ERROR :' + err.message
                	, INDUS_Logger_CLS.ErrorLevel.Critical.name()
                );
            }
        }
        doFinish(info);
    }
    
    /**
     *Insert a chunk of data in database.
     */
    private Integer dbInsertOrUpdate(List<sObject> lSObjects, Boolean noneOrAll, Action actione){
        Integer nbError = 0;
        try{
            if (Test.isRunningTest() && dbexception != null){
                throw dbexception;
            }
            Database.SaveResult[] lsr;
            Integer cpt = 0; 
            
            if (actione == Action.ACTION_CREATE){ 
                lsr = Database.insert(lSObjects, noneOrAll);
            }else{
                lsr = Database.update(lSObjects, noneOrAll);
            }
            
            for (Database.SaveResult sr : lsr){
                if (!sr.IsSuccess()){
                    Database.Error err = sr.getErrors()[0];
                    System.debug('#### INDUS_Batch_AbstractBatch_CLS - dbInsertOrUpdate - Batch:' + batchClassName + ' - First error:' + err.getStatusCode() + ' - ' + err.getMessage());
                    // increment number of error during insertion
                    nbError++;
                    if (activeLog){
                        lErrors.add(new RecordError(lSObjects[cpt], actione == Action.ACTION_CREATE ? 'Database.insert' : 'Database.update', 'First error:' + err.getStatusCode() + ' - ' + err.getMessage(), null));
                    }
                }
                cpt++;
            }
        }catch(Exception e){
            System.debug('#### INDUS_Batch_AbstractBatch_CLS - dbInsert - Batch:' + batchClassName + ' - Exception:' + e);
            // increment number of error during insertion
            nbError = lSObjects.size();
            if (activeLog){
                lErrors.add(new RecordError(null, actione == Action.ACTION_CREATE ? 'Database.insert' : 'Database.update', 'Exception:' + e.getMessage(), e));
            }
        }
        return nbError;
    }

	/**
     *Deletes a chunk of data in database.
     */
    private Integer dbDelete(List<sObject> lSObjects, Boolean noneOrAll){
        Integer nbError = 0;
        try{
            if (Test.isRunningTest() && dbexception != null){
                throw dbexception;
            }
            Integer cpt = 0;
            Database.DeleteResult[] lsr = Database.delete(lSObjects, noneOrAll);
            for (Database.DeleteResult sr : lsr){
                if (!sr.IsSuccess()){
                    Database.Error err = sr.getErrors()[0];
                    System.debug('#### INDUS_Batch_AbstractBatch_CLS - dbDelete - Batch:' + batchClassName + ' - First error:' + err.getStatusCode() + ' - ' + err.getMessage());
                    // increment number of error during deletion
                    nbError++;
                    if (activeLog){
                        lErrors.add(new RecordError(lSObjects[cpt], 'Database.delete', 'First error:' + err.getStatusCode() + ' - ' + err.getMessage(), null));
                    }
                }
                cpt++;
            }
        }catch(Exception e){
            System.debug('#### INDUS_Batch_AbstractBatch_CLS - dbDelete - Batch:' + batchClassName + ' - Exception:' + e);
            // increment number of error during deletion
            nbError = lSObjects.size();
            if (activeLog){
                lErrors.add(new RecordError(null, 'Database.delete', 'Exception:' + e.getMessage(), e));
            }
        }
        return nbError;
    }
    
    protected void executeAction(DatabaseAction dbAction){
    	try{
            executeActionWithoutTryCatch(dbAction);
        }catch(Exception e){
            if (activeLog){
                lErrors.add(new RecordError(null, 'execute', 'Exception:' + e.getMessage(), e));
            }
            throw e; 
        }
    }
    
    private void executeActionWithoutTryCatch(DatabaseAction dbAction){
    	Integer nbError;
        if (dbAction != null){
            if (dbAction.action == Action.ACTION_DELETE){
                nbError = dbDelete(dbAction.lSObjects, dbAction.noneOrAll);
                nbErrorDelete += nbError;
                nbRecordsDeleted += (dbAction.lSObjects.size() - nbError);
            }else{
                nbError = dbInsertOrUpdate(dbAction.lSObjects, dbAction.noneOrAll, dbAction.action);
                if (dbAction.action == Action.ACTION_UPDATE){
                    nbErrorUpdate += nbError;
                    nbRecordsUpdated += (dbAction.lSObjects.size() - nbError);
                }else{
                    nbErrorInsert += nbError;
                    nbRecordsCreated += (dbAction.lSObjects.size() - nbError);
                }
            }
        }
    }
    
    global class EmailSending{
    	public EmailSending(Id targetObject, Id template, Id what, Id orgWideEmailAddressId, Boolean saveAs){
    		this.targetObjectId = targetObject;
    		this.templateId = template;
    		this.WhatId = what;
    		this.saveAsActivity = saveAs;
            this.orgWideEmailAddressId = orgWideEmailAddressId;
    	}
    	Id targetObjectId;
    	Id templateId;
    	Id WhatId;
        Id orgWideEmailAddressId;
    	Boolean saveAsActivity;
    }
    
    /**
     *Send a list of emails.
     */
     protected void sendEmail(List<EmailSending> lemailSending){
    	Integer nbError = 0;

        try{
	        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            for (EmailSending emailSend: lemailSending)
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(emailSend.targetObjectId); 
                email.setSaveAsActivity(emailSend.saveAsActivity); 
                email.setTemplateId(emailSend.templateId); 
                email.setWhatId(emailSend.WhatId); 
                if (emailSend.orgWideEmailAddressId!=null)
                {
                	email.setOrgWideEmailAddressId(emailSend.orgWideEmailAddressId);
                }
                System.debug('#### INDUS_Batch_AbstractBatch_CLS - EmailSending:' + emailSend);
               	System.debug('#### INDUS_Batch_AbstractBatch_CLS - sendEmailTo:' + email); 
            	emails.add(email);
            }
            
            List<Messaging.SendEmailResult> lsmr = Messaging.sendEmail(emails);   
            for (Messaging.SendEmailResult smr : lsmr){
            	if (!smr.isSuccess()) {
            		nbError++;
            	}
            }  
                
        } catch(Exception e) {
        	nbError = lemailSending.size();
            System.debug('#### INDUS_Batch_AbstractBatch_CLS - sendEmail - Exception:' + e.getStackTraceString());
             if (activeLog){
                lErrors.add(new RecordError(null, 'sendEmail', 'Exception:' + e.getMessage(), e));
            }
        }
        nbErrorEmail += nbError;
        nbEmailSent += (lemailSending.size() - nbError);
    }
}