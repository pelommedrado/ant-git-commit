/**
	Class   -   VFC05_CustomerDecisionTreeDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC05_CustomerDecisionTreeDAO.
**/
@isTest
public with sharing class VFC05_CustomerDecisionTreeDAO_Test {

	static testMethod void VFC05_CustomerDecisionTreeDAO_Test() {
        // TO DO: implement unit test
        List<CDT_CustomerDecisionTree__c> lstCustomerDecisionTreeInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToCustomerRelationshipObjects();
        String BehaviorCustomer = 'Price';
		String CadastralCustomer = 'Young / Low Income';
		Boolean CurrentVehicle = false;
		String PreApprovedCredit = 'Y';
		Boolean VehicleOfCampaign =true;
		Boolean VehicleOfInterest = true;
		Test.startTest();
		//List<CDT_CustomerDecisionTree__c> lstCustomerDecisionTree = VFC05_CustomerDecisionTreeDAO.getInstance().findCustomerDecisionTree(BehaviorCustomer, CadastralCustomer, CurrentVehicle, PreApprovedCredit, VehicleOfInterest, VehicleOfCampaign);
		List<CDT_CustomerDecisionTree__c> lstCustomerDecisionTree = VFC05_CustomerDecisionTreeDAO.getInstance().findCustomerDecisionTree(lstCustomerDecisionTreeInsert[0].BehaviorCustomer__c, lstCustomerDecisionTreeInsert[0].CadastralCustomer__c, lstCustomerDecisionTreeInsert[0].CurrentVehicle__c, lstCustomerDecisionTreeInsert[0].PreApprovedCredit__c, lstCustomerDecisionTreeInsert[0].VehicleOfInterest__c, lstCustomerDecisionTreeInsert[0].VehicleOfCampaign__c);
   		Test.stopTest();
   		System.assert(lstCustomerDecisionTree.size() > 0);
    }
}