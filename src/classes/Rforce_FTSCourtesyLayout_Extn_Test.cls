/**
*    
*    @Author RNTBCI(Vetrivel Sundararajan)
*    @Date 21-07-2015
*    @Description This test class is used for code coverage of Rforce_FTSCourtesyLayout_Extn class
*    @Project Rforce
*/    

@isTest
public with sharing class Rforce_FTSCourtesyLayout_Extn_Test  
{
 static testMethod void testFTSGoodwillWarantySpain()
    {
        Country_Info__c ctr = new Country_Info__c (Name = 'Spain', Country_Code_2L__c = 'ES', Language__c = 'Spanish', Case_RecordType__c = 'ES_Case_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', alias = 'lro', RecordDefaultCountry__c = 'France', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
        Test.startTest();

        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        RCourtesyGrid__c obj = new RCourtesyGrid__c();
        obj.CarSegmentName__c='vehículo particular';
        obj.Country__c='Spain';
        obj.MaxiDailyRate__c=22;
        obj.StandardDailyRate__c=22;      
        insert obj;    
        
        Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;
        system.debug('Acc--->'+Acc);
    
        Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        system.debug('Con--->'+Con);

        VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRG0633285766',DeliveryDate__c=date.parse('02/02/2007'));
        insert veh;     
        Case c = new Case(Origin='Email',Type='Dealer Support',SubType__c ='DLPA',Status='Closed',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id,VIN__c=veh.Id,CaseBrand__c='Renault',Kilometer__c=3333);
        insert c;
        system.debug('Case--->'+c);
  
        Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and DeveloperName='Replacement_Car_Goodwill' limit 1].Id;
        system.debug('Acc--->'+RecordId);
      
       Goodwill__c g = new Goodwill__c (CarSegment__c=obj.CarSegmentName__c,Replacement_Car_Negociated_Fare__c=22,case__c = c.Id,BudgetCode__c='Dealer',ExpenseCode__c='Car Rental',ResolutionCode__c='Warranty - Car Rental Warranty',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2012'), Organ__c= 'BATTERY', QuotWarrantyRate__c=1000,Country__c= 'Spain',PVI_Number__c='PVI123',Decided_start_date__c=date.parse('02/05/2015'),Decided_end_date__c=date.parse('05/05/2015'),GoodwillStatus__c='Approved',DeviationReasDesc__c='Decription');
        FTS__c ft=new  FTS__c();
        ft.Case__c=c.Id;
        ft.ORDate__c= date.parse('02/02/2012');
        ft.Total_Amount_Labour__c=67;
        ft.Dealer_Labour_Rate__c=69;
        ft.Total_Amount_Of_Parts__c=354;
        ft.PVI_Number__c='PVI123';
        ft.Car_Segment__c='vehículo particular';
        ft.Replacement_Start_Date__c=date.parse('02/05/2015');
        ft.Replacement_End_Date__c=date.parse('07/05/2015');
            
        insert ft;
   
        List<Goodwill__c> addgodwil=new  List<Goodwill__c>();
        try{
            //g.ExpenseCode__c='Car Rental';
            //g.BudgetCode__c='LPG';
            addgodwil.add(g);
            upsert addgodwil;
            System.debug('GW******'+addgodwil);
        }
        catch(DmlException d){
        system.debug('here we get dml err message'+d.getMessage());
        }
        catch(Exception e){
        system.debug('here we get generic err message'+e.getMessage());
        }
    ApexPages.currentPage().getParameters().put('id',c.id);
    ApexPages.currentPage().getParameters().put('ftsIds',ft.id);
    ApexPages.currentPage().getParameters().put('RecordType',g.Id);
    ApexPages.currentPage().getParameters().put('strCaseCountry','Spain');
    Rforce_FTSCourtesyLayout_Extn ctrl =new Rforce_FTSCourtesyLayout_Extn(new ApexPages.StandardSetController(addgodwil));
    ctrl.initialize();  
    ctrl.saveGoodwill();
    Test.stopTest();
}

}

}