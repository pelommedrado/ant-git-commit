/**
* Classe que representa exceções geradas na atualização dos itens de orçamento.
* @author Christian Ranha.
*/
public with sharing class VFC93_UpdateQuoteLineItemException extends Exception {

	public VFC93_UpdateQuoteLineItemException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}