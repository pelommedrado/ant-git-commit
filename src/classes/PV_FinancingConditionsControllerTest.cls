@isTest
private class PV_FinancingConditionsControllerTest {
    
    //inativar o documento
    /*static testMethod void inactiveFinancingConditions() {
        Document document = new Document(
            Body = blob.valueOf('TWVzZXM7VGF4YTtDb2VmaWNpZW50ZTtUaXBvIERlIFBlc3NvYTtGbGFnDQoxMjswLDk5OzAsMjM0NTtQRjsxMjA5OVBG'),
            ContentType = '	text/csv',
            DeveloperName = 'Layout_Upload_FinancingConditions',
            Name = 'Layout_Upload_FinancingConditions',
            FolderId = UserInfo.getUserId()
        );
        Database.insert(document);
        
        PV_FinancingConditionsController.inactiveFinancingConditions(document.id);
    }*/
    
    static testMethod void createDocument(){
        PV_FinancingConditionsController financingConditions = new PV_FinancingConditionsController();
        //financingConditions.csvFileBody = Blob.valueOf('TWVzZXM7VGF4YTtDb2VmaWNpZW50ZTtUaXBvIERlIFBlc3NvYTtGbGFnDQoxMjswLDk5OzAsMjM0NTtQRjsxMjA5OVBG');
        financingConditions.csvFileBody = 
            Blob.valueOf('\n10;3,14;5,1;a;b;c;\n');
        
        PageReference pg = new PageReference('/apex/PV_FinancingConditions');
        financingConditions.csvAsString = 'Document';
        financingConditions.importCSVFile();
        financingConditions.inactiveOldFinancingConditions();
        
        List<PV_FinancingConditions__c> pvList = financingConditions.lsFinancingConditions;
         
        Id id = [SELECT id 
                 FROM PV_FinancingConditions__c 
                 WHERE Status__c = 'ACTIVE'
                 LIMIT 1].Id;
        PV_FinancingConditionsController.inactiveFinancingConditions(id);
    }
}