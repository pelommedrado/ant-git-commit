public class MonthlyGoalSellerExecution {

    public Set<Id> obterSeller() {
        List<PermissionSet> pSList = [
            SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'
        ];
        
        //get users from permission set BR - SFA - Sellers
        List<PermissionSetAssignment> psets = [
            SELECT Id, AssigneeId
            FROM PermissionSetAssignment
            WHERE PermissionSetId IN: pSList
        ];
        
        Set<Id> sellersIds = new Set<Id>();
        for(PermissionSetAssignment p : psets) {
            sellersIds.add(p.AssigneeId);
        }
        return sellersIds;
    }
    
    public Monthly_Goal_Seller__c create(Id mgdId, Id sellerId) {
        Monthly_Goal_Seller__c mgs = new Monthly_Goal_Seller__c();
        mgs.Monthly_Goal_Dealer__c = mgdId;
        mgs.Seller__c = sellerId;
        return mgs;
    }
    
    public void createFromGoalDealer(List<Monthly_Goal_Dealer__c> mgdList) {
        List<Monthly_Goal_Seller__c> mgsList = new List<Monthly_Goal_Seller__c>();
        
        Set<Id> sellersIds = obterSeller();
        Set<Id> dealerGoalsId = new Set<Id>();
        Set<Id> groups = new Set<Id>();
        
        for(Monthly_Goal_Dealer__c  mgd : mgdList) {
            groups.add(mgd.Dealer__c);
        }
        
        //get sellers
        List<User> sellers = [
            SELECT Id, Contact.Account.Dealer_Matrix__c, Contact.AccountId
            FROM User
            WHERE Contact.AccountId IN: groups
            AND IsActive = true
            AND Id IN : sellersIds
        ];
        
        System.debug('sellers: ' + sellers);
        
        for(Monthly_Goal_Dealer__c mgdv : mgdList) {
            for(User u : sellers) {
                if(u.Contact.AccountId == mgdv.Dealer__c && sellersIds.contains(u.Id)){
                    mgsList.add(create(mgdv.Id, u.Id));
                }
            }
        }
        
        //insert goals
        Savepoint sp2 = Database.setSavepoint();
        try {    
            Database.upsert(mgsList);
            
        } catch(Exception ex) {
            Database.rollback(sp2); 
            System.debug('Ex: ' + ex);
        }
    }
}