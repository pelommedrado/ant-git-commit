/* Purge of accounts
*************************************************************************************************************
18 Mar 2016 : Creation - purge of german accounts
*************************************************************************************************************/
@isTest
private class Myr_Batch_Germany2_SCH_TEST {
	@testSetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		Myr_Datasets_Test.prepareCustomerMessageSettings();
		List<Account> listAcc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		Account a  = [select Id from Account];
		Logger__c l = new Logger__c(); 
		l.Account__c=a.Id; 
		l.Apex_Class__c='Class';
		l.Function__c='Function';
		l.Project_Name__c='Project_Name'; 
		insert l;

		List<Customer_Message_Templates__c> C_T = [select id from Customer_Message_Templates__c];
		
		Customer_Message__c c = new Customer_Message__c();
		c.Account__c=a.Id;
		c.Body__c='Body';
		c.Channel__c='Channel';
		c.Template__c=C_T[0].Id;
		c.Title__c='Title';
		insert c;
		
		Synchro_ATC__c s = new Synchro_ATC__c();
		s.Account__c=a.Id;
		insert s;
    }

    static testMethod void test_schedulable_batch() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany2_SCH schedulableBat = new Myr_Batch_Germany2_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany2_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_20() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany20_SCH schedulableBat = new Myr_Batch_Germany20_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany20_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_21() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany21_SCH schedulableBat = new Myr_Batch_Germany21_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany21_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_22() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany22_SCH schedulableBat = new Myr_Batch_Germany22_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany20_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_23() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany23_SCH schedulableBat = new Myr_Batch_Germany23_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany20_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_24() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany24_SCH schedulableBat = new Myr_Batch_Germany24_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany20_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }


    static testMethod void test_schedulable_batch_25() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany25_SCH schedulableBat = new Myr_Batch_Germany25_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany20_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_26() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		Myr_Batch_Germany26_SCH schedulableBat = new Myr_Batch_Germany26_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany26_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_27() {
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);

		a.Myr_Status__c='Deleted';
		a.Myd_Status__c='Activated';
		update a;

		a.MYR_Status_UpdateDate__c=Date.newInstance(1960, 2, 17);
		update a;

		Myr_Batch_Germany27_SCH schedulableBat = new Myr_Batch_Germany27_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany27_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }

    static testMethod void test_schedulable_batch_28() {  
		Account a  = [select Id from Account];
		Synchro_ATC__c s  = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(a.Id,s.account__c);
		Logger__c l  = [select Id, account__c from Logger__c];
		System.assertEquals(a.Id,l.account__c);
		Customer_Message__c c  = [select Id, account__c from Customer_Message__c];
		System.assertEquals(a.Id,c.account__c);
		
		a.Myd_Status__c='Deleted';
		a.Myr_Status__c='Activated';
		update a;

		a.MYD_Status_UpdateDate__c=Date.newInstance(1960, 2, 17);
		update a;

		

		Myr_Batch_Germany28_SCH schedulableBat = new Myr_Batch_Germany28_SCH();
		String schedule  = '0 25 10 * * ?';
        String jobId = system.schedule('Myr_Batch_Germany28_SCH', schedule, schedulableBat);
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId ];
        System.assertEquals( schedule, ct.CronExpression );
    }


}