public class VEH_VehController {
    
    public static void canceledStatus(List<VEH_Veh__c> newveh, List<VEH_Veh__c> oldveh){
        
        system.debug('new:' + newveh);
        system.debug('old: ' + oldveh);
        
        List<Id> vehIds = new List<Id>();
        List<Opportunity> opps = new List<Opportunity>();
        List<Id> oppIds = new List<Id>();
        List<Quote> quotes = new List<Quote>();
        
        
        for(VEH_Veh__c v : newveh){
            for(VEH_Veh__c vold : oldveh){
                
                System.debug('newveh: ' + v.Status__c);
        		System.debug('oldveh: ' + vold.Status__c);
                
                if(v.Status__c != vold.Status__c){
                    if(vold.Status__c == 'Billed'){
                        vehIds.add(vold.Id);
                    }
                }
            }
        }
        
        quotes = [SELECT Id, Status, OpportunityId FROM Quote 
                  WHERE Id IN (SELECT QuoteId FROM QuoteLineItem WHERE Vehicle__c IN: vehIds)
                  AND Status = 'Billed'];
        
        System.debug('####################### -- quotes: ' + quotes);
        
        for(Integer i = 0; i<quotes.size(); i++){
            quotes[i].Status = 'Canceled';
            oppIds.add(quotes[i].OpportunityId);
        }
        
        System.debug('################### -- oppIds: ' + oppIds);
        
        opps = [SELECT Id, StageName FROM Opportunity
                WHERE Id IN: oppIds];
        
        System.debug('###################-- opps: ' + opps);
        
        for(Integer i = 0; i<opps.size();i++){
            opps[i].StageName = 'Lost';
        }
        
        System.debug('################-- opps AFTER: ' + opps);
        
        Savepoint sp1 = Database.setSavepoint();
        
        try{
            Database.update(quotes);
            Database.update(opps);
        } catch(Exception e){
            System.debug('erro: ' + e);
            
            Database.rollback(sp1);
            
        }
        
    }

}