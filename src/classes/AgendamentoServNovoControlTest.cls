@isTest
private class AgendamentoServNovoControlTest {
    
    public static Account acc { get;set; }
    public static Contact cont { get;set; }
    public static VEH_Veh__c vehicle1 { get;set; }
    
    static testMethod void unitTest01() {
        User user = criarUser();
        
        System.runAs(user) {
            ApexPages.currentPage().getParameters().put('Id', acc.Id); 
            
            AgendamentoServicoNovoController ct = new AgendamentoServicoNovoController();
            
            ct.salvar();
            ct.acc.FirstName = 'Test';
            ct.acc.LastName = ' Test';
            ct.salvar();
            ct.acc.CustomerIdentificationNbr__c = '13551258716';
            ct.salvar();
            ct.acc.PersonEmail = 'test@gmail.com';
            ct.salvar();
            ct.opp.ProposalScheduleDate__c = Date.today();
            ct.salvar();
            ct.opp.ProposalScheduleHour__c = '12:00';
            ct.salvar();
            ct.opp.ScheduledPeriod__c = 'EARLY MORNING';
            ct.salvar();
            ct.opp.Dealer__c = acc.Id;
            ct.salvar();
            ct.opp.ServiceType__c = 'RETURN';
            ct.salvar();
            ct.veiculo = vehicle1;
            ct.salvar();
            
            ct.tipoRelText = 'User';
            ct.salvar();
           
            
            ct.textSearch = '123';
            ct.buscarVeiculo();
            ct.cancelar();
        }
    }
    
    static User criarUser() {
        acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '79856523',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_Service__c = 'Available'
        );
        
        Database.insert( cont );
        
        Profile p = [SELECT Id FROM Profile WHERE Name='BR - Renault + Cliente'];
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = p.Id,
            ContactId = cont.Id,
            BIR__c ='79856523',
            isCac__c = true
        );
        
        vehicle1 = new VEH_Veh__c(Name = '12111111111111119',
                                  VehicleRegistrNbr__c = '1000ZB',
                                  KmCheckDate__c = Date.today(),
                                  DeliveryDate__c = Date.today() + 30,
                                  VehicleBrand__c= 'Active',
                                  KmCheck__c = 100,
                                  Tech_VINExternalID__c = '9000000009');
        
        Database.insert( vehicle1 );
        
        return manager;
    }
}