/**
*	Class   -   VFC23_OpportunityDAO
*   Author  -   SureshBabu
*   Date    -   23/10/2012
*    
*   #01 <Suresh Babu> <23/10/2012>
*      Created this Class to handle records from Opportunity related Queries.
*/

// A DAO must inherit the sharing rules from the caller class. 
public class VFC23_OpportunityDAO extends VFC01_SObjectDAO
{
	private static final VFC23_OpportunityDAO instance = new VFC23_OpportunityDAO();
    
    private VFC23_OpportunityDAO(){}

    public static VFC23_OpportunityDAO getInstance(){
        return instance;
    }

    
    /** 
	 * This Method was used to get Opportunity Record based on opportunityId 
	 * @param opportunityId - This field using get Opportunity details by opportunity Id
     * @return oppotunityRecordById - fetch and return the result in oppotunityRecordById.
     */
    public Opportunity fetchOpportunityUsingOpportunityId(Id opportunityId)
    {
    	Opportunity opportunity 
    		= [SELECT AccountId, Amount, CloseDate, VehicleInterest__c, Dealer__c, Description, LeadSource__c, LeadSource, 
    				  LeadSubsource__c, Name, SyncedQuoteId, Id, OpportunitySource__c, OpportunitySubSource__c, 
    				  Type, OwnerId, Owner.Email, Owner.Name, Owner.Phone, PeriodNextPurchase__c,
    				  Pricebook2Id, IsPrivate, Probability, ReasonLossCancellation__c, RecordTypeId,
    				  StageName, IsWon, SourceMedia__c, OpportunityTransition__c, CampaignId,
    				  Account.FirstName, Account.LastName, Account.Name, Account.PersonBirthdate,
    				  Account.CustomerIdentificationNbr__c, Account.PersEmailAddress__c, Account.PersonEmail,
					  Account.PersLandline__c, Account.ProfLandline__c, Account.ProfMobPhone__c, 
					  Account.PersMobPhone__c, Account.OptinPhone__c, Account.OptinEmail__c, Account.OptinSMS__c,
    				  Account.TypeOfInterest__c, Account.Campaign_BR__c, Account.VehicleInterest_BR__c,
               		  Account.Version_of_Interest__c, Current_Vehicle__c,
					  Account.SecondVehicleOfInterest__c, Account.CurrentVehicle_BR__c, 
					  Account.CurrentVehicle_BR__r.Brand__c, Account.CurrentVehicle_BR__r.Model__c,
    				  Account.YrReturnVehicle_BR__c, Account.ShippingStreet,
				      Account.ShippingCity, Account.ShippingState, Account.ShippingPostalCode, 
				      Account.ShippingCountry,Account.Id, Account.CompanyID__c, PromoCode__c 
			     FROM Opportunity    								
                WHERE Id =: opportunityId];					  							
    	return opportunity;
    }
    
    public List<Opportunity> fetchOpportunityUsingOpportunityIds(Set<Id> oppIds)
    {
       return [SELECT AccountId, Amount, CloseDate, Dealer__c, VehicleInterest__c, Dealer__r.Name, Dealer__r.ParentId, Description, LeadSource__c, LeadSource, 
                      LeadSubsource__c, Name, SyncedQuoteId, Id, OpportunitySource__c, OpportunitySubSource__c, 
                      Type, OwnerId, Owner.Email, Owner.Name, Owner.Phone, PeriodNextPurchase__c,
                      Pricebook2Id, IsPrivate, Probability, ReasonLossCancellation__c, RecordTypeId,
                      StageName, IsWon, SourceMedia__c, OpportunityTransition__c, CampaignId,
                      Account.FirstName, Account.LastName, Account.Name, Account.PersonBirthdate,
                      Account.CustomerIdentificationNbr__c, Account.PersEmailAddress__c, Account.PersonEmail,
                      Account.PersLandline__c, Account.ProfLandline__c, Account.ProfMobPhone__c, 
                      Account.PersMobPhone__c, Account.OptinPhone__c, Account.OptinEmail__c, Account.OptinSMS__c,
                      Account.TypeOfInterest__c, Account.Campaign_BR__c, Account.VehicleInterest_BR__c,
                      Account.Version_of_Interest__c, Current_Vehicle__c,
                      Account.SecondVehicleOfInterest__c, Account.CurrentVehicle_BR__c, 
                      Account.CurrentVehicle_BR__r.Brand__c, Account.CurrentVehicle_BR__r.Model__c,
                      Account.YrReturnVehicle_BR__c, Account.ShippingStreet,
                      Account.ShippingCity, Account.ShippingState, Account.ShippingPostalCode, 
                      Account.ShippingCountry,Account.Id, Account.CompanyID__c, PromoCode__c, Vehicle_Of_Interest__c
                 FROM Opportunity                                   
                WHERE Id IN: oppIds]; 
    }
    
    /** 
	* This Method was used to get List of Opportunity Record based on accountId 
	* @param accountId - This field using get Opportunity details by Account Id
    * @return oppotunityRecordByAccount - fetch and return the result in oppotunityRecordByAccount.
    */
    public List<Opportunity> fetchListOpportunityUsingAccountId(Id accountId)
    {
    	List<Opportunity> oppotunityRecordByAccount = 
    		[SELECT AccountId, Amount, CloseDate, Dealer__c, Description, LeadSource__c, LeadSource, 
    		        LeadSubsource__c, Name, Id, OpportunitySource__c, OpportunitySubSource__c, Type, 
    		        OwnerId, PeriodNextPurchase__c, Pricebook2Id, IsPrivate, Probability, 
    		        ReasonLossCancellation__c, RecordTypeId, StageName, IsWon , SourceMedia__c
    		   FROM Opportunity
    		  WHERE AccountId =: accountId];
    	return oppotunityRecordByAccount;
    }
    
    /** 
	 * This Method was used to get List of Opportunity Record based on accountId and User.AccountID
	 * 	@param accountId - This field using get Opportunity details by account Id
	 *	@param dealerId	- This Id used to fetch the Opportunity using Dealer Id.
     * 	@return oppotunityRecordByAccount - fetch and return the result in oppotunityRecordByAccount.
     */
    public List<Opportunity> fetchListOpportunityUsingAccountAndDealer(Id accountId, Id dealerId)
    {
    	List<Opportunity> oppotunityRecordByAccount = 
    		[SELECT Id, Name, AccountId, Amount, CloseDate, Dealer__c, Description,
					LeadSource__c, LeadSource, LeadSubsource__c, OpportunitySource__c, 
					OpportunitySubSource__c, Type, OwnerId, PeriodNextPurchase__c,
					Pricebook2Id, IsPrivate, Probability, ReasonLossCancellation__c, RecordTypeId,
					StageName, IsWon, SourceMedia__c, OpportunityTransition__c, PromoCode__c, 
					CampaignId, Campaign.Name
			   FROM Opportunity
			  WHERE AccountId =: accountId 
			    AND Dealer__c =: dealerId];
    	return oppotunityRecordByAccount;
    }

    /**
    *	This method was used to get the Opportunity records using Customer Account ID.
    *	@param	customerIDs			-	customer Ids to fetch Opportunities.
    *	@return	lstOpportunities	-	List of Opportunities.
    **/
    public List<Opportunity> fetchOpportunity_UsingCustomerID(Set<Id> customerIDs)
    {
    	List<Opportunity> lstOpportunities = 
    		[SELECT AccountId, Amount, CloseDate, Dealer__c, Description, LeadSource__c, LeadSource, 
    			    LeadSubsource__c, Name, Id, OpportunitySource__c, OpportunitySubSource__c, Type, 
    			    OwnerId, PeriodNextPurchase__c, Pricebook2Id, IsPrivate, Probability, 
    			    ReasonLossCancellation__c, RecordTypeId, StageName, IsWon , SourceMedia__c, PromoCode__c
			   FROM Opportunity
			  WHERE AccountId IN : customerIDs];
    	return lstOpportunities;
    }
    
    /** 
	* This Method was used to get List of Opportunity Record based on DealerId
	*	@param dealerId	- This Id used to fetch the Opportunity using Dealer Id.
    * 	@return List<Opportunity> - fetch and return the result in oppotunityRecordByAccount.
    */
    public List<Opportunity> fetchListOpportunityUsingDealer(Set<Id> setDealerId)
    {
    	List<Opportunity> lstOpportunities = 
    		[SELECT AccountId, Amount, CloseDate, Dealer__c, Description, LeadSource__c, LeadSource, 
    				LeadSubsource__c, Name, Id, OpportunitySource__c, OpportunitySubSource__c, Type, 
    				OwnerId, PeriodNextPurchase__c, Pricebook2Id, IsPrivate, Probability, 
    				ReasonLossCancellation__c, RecordTypeId, StageName, IsWon , SourceMedia__c, PromoCode__c
			   FROM Opportunity
    		  WHERE Dealer__c IN :setDealerId];
    	return lstOpportunities;
    }
    
    /**
     *
     */
    public void updateData(Opportunity sObjOpportunity, Account sObjAccount)
    {
    	Savepoint objSavePoint = null;
    	
    	try
    	{
    		//obtém uma transação
            objSavePoint = Database.setSavepoint();
            
            // tenta atualizar a oportunidade
            super.updateData(sObjOpportunity);
            
            // tenta atualizar a conta
            super.updateData(sObjAccount);
    	}
    	catch (DMLException ex)
    	{
    		Database.rollback(objSavePoint);
            throw ex;  		
    	}
    }
    
    /**
     *
     */
    public void updateData(Opportunity sObjOpportunity, TDV_TestDrive__c sObjTestDrive)
    {
    	Savepoint objSavePoint = null;
    	
    	try
    	{
    		// obtém uma transação
            objSavePoint = Database.setSavepoint();
            
            // tenta atualizar a oportunidade
            super.updateData(sObjOpportunity);
            
            // tenta atualizar a conta
            super.updateData(sObjTestDrive);
    	}
    	catch (DMLException ex)
    	{
    		Database.rollback(objSavePoint);
            throw ex;  		
    	}
    }
    
    /**
     *
     */
    public List<Opportunity> findById(Set<String> setId)
    {
    	List<Opportunity> lstSObjOpportunity = [SELECT OpportunityTransition__c
    	                                        FROM Opportunity
    	                                        WHERE Id IN : setId];
    	
    	return lstSObjOpportunity;                                        
    }
}