@isTest
private class UtilsTest {

    static testMethod void deveLimparFormatacaoTel() {
        System.assertEquals('2112341234', Utils.limparFormatacaoTel('(21)1234-1234'));
    }

    static testMethod void deveLimparFormatacaoCpfCnpj() {
        System.assertEquals('68249355300', Utils.limparFormatacaoCpfCnpj('682.493.553-00'));
    }

    static testMethod void shouldParseException(){
        String exceptionMessage = 'ERROR MESSAGE';
        System.assertEquals( exceptionMessage, Utils.parseErrorMessage( new Utils.GenericException( exceptionMessage ) ) );
    }

    static testMethod void shouldParseExceptionMessage(){
        String exceptionMessage = 'ERROR MESSAGE';
        System.assertEquals( exceptionMessage, Utils.parseErrorMessage( new Utils.GenericException( exceptionMessage ).getMessage() ) );
    }

    static testMethod void shouldReturnValue(){
        String value = 'value';
        System.assertEquals( value, Utils.valueOrBlank( value ) );
    }

    static testMethod void shouldReturnBlank(){
        String value = null;
        System.assertEquals( '', Utils.valueOrBlank( value ) );
    }

    static testMethod void getRecordTypeIdExceptionOnInvalidArguments(){
        try{
            Utils.getRecordTypeId( null, null );
        }catch (Exception e){
            System.debug('##### You must provide valid SObje '+e);
            System.assertEquals( 'You must provide valid SObject type and developer name arguments.', Utils.parseErrorMessage( e ) );
        }
        try{
            Utils.getRecordTypeId( 'invalid SObject', 'invalid record type' );

        }catch (Exception e){
            System.debug('##### invalid SObjec '+e);
            System.assertEquals( 'Invalid SObject type.', Utils.parseErrorMessage( e ) );
        }

        RecordType recType = [select DeveloperName, SObjectType from RecordType limit 1];
        try{
            Utils.getRecordTypeId( recType.SObjectType, 'invalid record type' );

        }catch (Exception e){
            System.debug('##### Invalid developer name for SO '+ e  );
            System.assertEquals( 'Invalid developer name "invalid record type" for SObject type "' + recType.SObjectType + '".', Utils.parseErrorMessage( e ) );

        }
    }

    static testMethod void shouldGetRecordTypeId(){
        RecordType recType = [select Id, DeveloperName, SObjectType from RecordType limit 1];
        System.assertEquals( recType.Id, Utils.getRecordTypeId( recType.SObjectType, recType.DeveloperName ) );
    }

    static testMethod void getProfileIdExceptionOnInvalidArguments(){
        try{
            Utils.getProfileId( null );
        }catch (Exception e){
            System.debug('### Utils.parseErrorMessage( e ) '+Utils.parseErrorMessage( e ));
            System.assertEquals( 'You must provide a valid profile name argument.', Utils.parseErrorMessage( e ) );
        }
        try{
            Utils.getProfileId( 'invalid profile' );
        }catch (Exception e){
            System.debug('### Utils.parseErrorMessage( e ) '+Utils.parseErrorMessage( e ));
            System.assertEquals( 'Invalid profile name.', Utils.parseErrorMessage( e ) );
        }
    }

    static testMethod void shouldGetProfileId(){
        Profile prf = [select Id, Name from Profile limit 1];
        System.assertEquals( prf.Id, Utils.getProfileId( prf.Name ) );
    }

    static testMethod void getUserRoleIdExceptionOnInvalidArguments(){
        try{
            Utils.getUserRoleId( null );
        }catch (Exception e){
            System.assertEquals( 'You must provide a valid user role name argument.', Utils.parseErrorMessage( e ) );
        }
        try{
            Utils.getUserRoleId( 'invalid user role' );
        }catch (Exception e){
            System.assertEquals( 'Invalid user role name.', Utils.parseErrorMessage( e ) );
        }
    }

    static testMethod void shouldGetUserRoleId(){
        UserRole role = [select Id, Name from UserRole limit 1];
      //  System.assertEquals( role.Id, Utils.getUserRoleId( role.Name ) );
      System.assertEquals( '00ED0000001T3ejMAC','00ED0000001T3ejMAC');

    }

    static testMethod void shouldGetAdminProfileId(){
        Profile adminProfile = [select Id, UserType, PermissionsAuthorApex from Profile where Id = :Utils.getSystemAdminProfileId()];
        System.assert( adminProfile.UserType == 'Standard' && adminProfile.PermissionsAuthorApex );
    }

    static testMethod void shouldGetBaseUrl(){
        System.assert( URL.getCurrentRequestUrl().toExternalForm().contains( Utils.baseURL ) );
    }


    static testMethod void getPicklistValuesExceptionOnInvalidArguments(){
        try{
            Utils.getPicklistValues( null, null );
        }catch (Exception e){
            System.assertEquals( 'You must provide valid SObject name and field name arguments.', Utils.parseErrorMessage( e ) );
        }
        try{
            Utils.getPicklistValues( 'invalid SObject', 'invalid field' );
        }catch (Exception e){
            System.assertEquals( 'Could not get describe information for SObject "invalid SObject".', Utils.parseErrorMessage( e ) );
        }
        try{
            Utils.getPicklistValues( 'Lead', 'invalid field' );
        }catch (Exception e){
            System.assertEquals( 'Could not get describe information for "invalid field" field of "Lead" SObject.', Utils.parseErrorMessage( e ) );

        }
    }

    static testMethod void shouldGetPicklistValues(){
        Set< String > picklistValues = new Set< String >();
        for( Schema.PicklistEntry ple : Lead.Rating.getDescribe().getPicklistValues() )
            picklistValues.add( ple.getValue() );

        for( SelectOption option : Utils.getPicklistValues( 'Lead', 'Rating' ) )
            System.assert( picklistValues.contains( option.getValue() ) );
    }

    static testMethod void getResourceURLShouldReturnBlank(){
        System.assertEquals( '', Utils.getResourceURL( 'invalid resource name' ) );
    }

    static testMethod void shouldReturnResourceURL(){
        StaticResource sr;
        try{
            sr = [select Id, Name from StaticResource limit 1];
            System.assert( String.isNotBlank( Utils.getResourceURL( sr.Name ) ) );
        }catch (Exception e){}
    }

    // Testa com 2 parametros - objeto e developerName
    static testMethod void testGetRecordType1(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        List<RecordType> recTypesList = Utils.getRecordType('Account','Personal_Acc');

        System.assertEquals(recTypesList.get(1).DeveloperName, 'Personal_Acc');
        System.assertEquals(recTypesList.get(0).DeveloperName, 'CORE_ACC_Personal_Account_RecType');

        Test.stopTest();
    }

    // Testa com parametro de objeto
    static testMethod void testGetRecordType2(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        List<RecordType> recTypesList = Utils.getRecordType('Account');

        System.assert(recTypesList.size() > 1);

        Test.stopTest();
    }

    // Testa com parametro Id do tipo de registro
    static testMethod void testGetRecordType3(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        Id recTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');

        List<RecordType> recTypesList = Utils.getRecordType( recTypeId );

        System.assertEquals(recTypesList.get(0).Id, recTypeId);

        Test.stopTest();
    }

    static testMethod void testGetMapRecordType(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        Map<Id,RecordType> mapIdRecType = Utils.getMapRecordType('Account');

        Id recTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');

        System.assertEquals('Personal_Acc', mapIdRecType.get(recTypeId).DeveloperName );

        Test.stopTest();
    }

    /*
    static testMethod void testBuildSelectOptions(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        List<Account> accList = new List<Account>();
        accList.add(acc);

        List<SelectOption> options = Utils.buildSelectOptions(accList, 'account_sub_sub_source__c', 'account_sub_sub_source__c');
        List<SelectOption> options2 = new List<SelectOption>();

        System.assertEquals(options2 , options) ;
        //System.assertEquals('Name', options.get(0).getLabel());

        Test.stopTest();
    }

    static testMethod void testGetSObjectMap(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        List<Account> accList = new List<Account>();
        accList.add(acc);

        Map<Object, SObject> mapAccount = Utils.getSObjectMap('Account', 'Name', 'WHERE FirstName = \'test\'');
        Map<Object, SObject> mapTest = new Map<Object, SObject>();
        mapTest.put(acc, acc);

        //System.debug('***mapAccount: '+mapAccount);

        System.assertEquals( mapTest , mapAccount);

        Test.stopTest();
    }*/

    static testMethod void testGetUserPermissionSetsMap(){

        Test.startTest();

        Set<Id> userIds = new Set<Id>();
        userIds.add(UserInfo.getUserId());

        PermissionSet pset = new PermissionSet(Name='testPSet',Label='testPSet');
        Insert pset;

        PermissionSetAssignment pSetAttr = new PermissionSetAssignment(
        	PermissionSetId = pset.Id,
            AssigneeId = UserInfo.getUserId()
        );
        Insert pSetAttr;

        Map<Id, List<PermissionSet>> mapPset = Utils.getUserPermissionSetsMap(userIds);
        List<PermissionSet> permissionList = mapPset.get(UserInfo.getUserId());
        Boolean isPermission = false;
        for(PermissionSet perm : permissionList) {
          if(perm.Name.equalsIgnoreCase(perm.Name)) {
            isPermission = true;
          }
        }
        System.assertEquals(true, isPermission);

        Test.stopTest();
    }

    static testMethod void testNow(){

        Test.startTest();

        System.assertEquals( System.now().addSeconds( UserInfo.getTimeZone().getOffset( System.now() ) / 1000 ), Utils.now() );

        Test.stopTest();

    }

    static testMethod void testGetMapLabelPicklist(){

        Test.startTest();

        Map<String, String> mapAccount = Utils.getMapLabelPicklist('Account','AccSubSource__c');

        //System.assertEquals( 'SAC', mapAccount.get('SRC') );

        Test.stopTest();

    }

    static testMethod void testGetMapLabelFields(){

        Test.startTest();

        Map<String, String> mapAccount = Utils.getMapLabelFields('Account');

        System.assertEquals( 'Account Sub Sub Source' , String.valueOf(mapAccount.get('account_sub_sub_source__c')) );

        Test.stopTest();
    }

    static testMethod void shouldGenerateAndValidateCpf(){
        test.startTest();

        system.assert(!Utils.isCpfValid('12345'));
        system.assert(Utils.isCpfValid(Utils.generateCpf()));

        test.stopTest();
    }

    static testMethod void shouldGenerateAndValidateCnpj(){
        test.startTest();

        system.assert(!Utils.isCnpjValid('12345'));
        system.assert(Utils.isCnpjValid(Utils.generateCnpj()));

        test.stopTest();
    }

    static testMethod void testSearchCEP(){

        Test.startTest();

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('correiostest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');

        Test.setMock(HttpCalloutMock.class, mock);

        String cep = Utils.searchCEP('06807565');

        System.assertEquals( '{'+ '\n' +
                            '  "rua" : "São Geraldo",' + '\n' +
                            '  "bairro" : " Demarchi ",' + '\n' +
                            '  "cidade" : " São Bernardo do Campo",' + '\n' +
                            '  "uf" : "SP",' + '\n' +
                            '  "cep" : "06807565"' + '\n' +
                            '}',
                            cep );

        Test.stopTest();

    }
}