public with sharing class CAC_SupportCaseController {
    

    //private static final String ENDPOINT = 'https://test.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
    private static final String ENDPOINT = W2CParams__c.getInstance( 'PRD' ).URLW2C__c;

    public Id orgId{
        get{
            return W2CParams__c.getInstance( 'PRD' ).Org_Id__c;
        }
    }
    public Id recordTypeId{
        get{
            return W2CParams__c.getInstance( 'PRD' ).RecordTypeId__c;
        }
    }

    public String serializedForm{get;set;}

    public CAC_SupportCaseController() {
    }

    public void submitForm(){
        HttpRequest request = new HttpRequest();
        request.setEndpoint(CAC_SupportCaseController.ENDPOINT);
        request.setMethod('POST');
        request.setBody(this.serializedForm);

        HttpResponse response = new Http().send(request);
    }
    
    public String emailUser {get{
        return UserInfo.getUserEmail();
    }}
    public String userId{get{return UserInfo.getUserId();}}
    public String userName{get{return UserInfo.getFirstName() +' '+ UserInfo.getLastName();}}
    public String idUserToURL{get{return UserInfo.getUserId();}}
    public String userAccount{get{
      	User usuario =  [select ContactId,BIR__c from User where id =: UserInfo.getUserId()];
        if(usuario.ContactId != null){
            Contact contato = [select AccountId from Contact where id =: usuario.ContactId];
            if(contato!=null){
                return [select Name from Account where id =: contato.AccountId].get(0).Name;
            }
        }else if(usuario.BIR__c != null){
            Account conta = [select Name from Account where IDBIR__c =: usuario.BIR__c ];
            if(conta != null)
            	return conta.Name;
        }
        return '';
    }}

    public Static boolean supportUser{ 
        get{
            System.debug('@@@ CAC_SupportCaseController.supportUser - BEGIN');
            boolean x = false;
            User usuario =  [select ContactId,BIR__c from User where id =: UserInfo.getUserId()];
            if(usuario.ContactId != null){
                Contact contato = [select AccountId from Contact where id =: usuario.ContactId];
                if(contato!=null){
                    x = [select Name, SuporteOnline__c from Account where id =: contato.AccountId].get(0).SuporteOnline__c;
                }
            }else if(usuario.BIR__c != null){
                Account conta = [select Name, SuporteOnline__c from Account where IDBIR__c =: usuario.BIR__c ];
                if(conta != null)
                    return conta.SuporteOnline__c;
            }
            System.debug('@@@ CAC_SupportCaseController.supportUser - FINISH ' + x);
            return x;
    	}
    }
    
}