//Use to simulate a response from the WebService
@isTest
global class Test_WS_RcArch_WebServiceMock implements WebServiceMock  {
    
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        

        if (responseName.equals('getAttachmentDetailsListResponse')){
          WebserviceRcArchivageV2.attachmentDetailsList myAttach = new WebserviceRcArchivageV2.attachmentDetailsList();
          myAttach.attachmentName='test';
          myAttach.createdDate=datetime.now();
          myAttach.extension='pdf';
          
          // Send the response
          WebserviceRcArchivageV2.getAttachmentDetailsListResponse response_AttDetails = new WebserviceRcArchivageV2.getAttachmentDetailsListResponse();
          response_AttDetails.attachmentDetails = new WebserviceRcArchivageV2.attachmentDetailsList[0];
          response_AttDetails.attachmentDetails.add(myAttach);
          response.put('response_x', response_AttDetails); 
        }
        
        if (responseName.equals('getCaseIdListResponse')){
          WebserviceRcArchivageV2.getCaseIdList req = ((WebserviceRcArchivageV2.getCaseIdList)request);
          WebserviceRcArchivageV2.caseId myCase = new WebserviceRcArchivageV2.caseId();
          // Create a case
          myCase.caseId = '';
          myCase.countryCode = '';
          // Send the response
          WebserviceRcArchivageV2.getCaseIdListResponse response_caseId = new WebserviceRcArchivageV2.getCaseIdListResponse();
          response_caseId.caseIdList = new WebserviceRcArchivageV2.caseId[0];
          response_caseId.caseIdList.add(myCase);
          response.put('response_x', response_caseId);                
        }
        
          if (responseName.equals('getAttachmentContentResponse')){
          WebserviceRcArchivageV2.getAttachmentContentResponse res = new WebserviceRcArchivageV2.getAttachmentContentResponse();
          res.attachmentContent='This is attachment Content';
          response.put('response_x',res);                
        }
        
         if (responseName.equals('getXmlDetailsResponse')){
          WebserviceRcArchivageV2.getXmlDetailsResponse res = new WebserviceRcArchivageV2.getXmlDetailsResponse();
          response.put('response_x','');                
        }

        if (responseName.equals('getCaseDetailsResponse')){
          // Cases Details request **/
          WebserviceRcArchivageV2.getCaseDetails caseDetailsReq = ((WebserviceRcArchivageV2.getCaseDetails)request);
          
          // Create case details       
          WebserviceRcArchivageV2.caseDetails caseDetails = new WebserviceRcArchivageV2.caseDetails();
          caseDetails.activityCloseDate = datetime.now();
          caseDetails.accountId = 'test';
          caseDetails.assetId = 'test';
          caseDetails.brandInfoExchangeFlag = 'test';
          caseDetails.buId = 'test';
          caseDetails.build = 'test';
          caseDetails.city = 'test';
          caseDetails.clientAgreement = 'test';
          caseDetails.conflictId = 'test';
          caseDetails.contactCellPhNum = 'test';
          caseDetails.contactHomePhNum = 'test';
          caseDetails.contactId = 'test';
          caseDetails.contactWorkPhNum = 'test';
          caseDetails.country = 'test';
          caseDetails.created = datetime.now();
          caseDetails.createdBy = 'test';
          caseDetails.createdDate = datetime.now();
          caseDetails.descText = 'test';
          caseDetails.detail = 'test';
          caseDetails.firstName = 'test';
          caseDetails.from_x = 'test';
          caseDetails.lastName = 'test';
          caseDetails.lastUpdated = datetime.now();
          caseDetails.lastUpdatedBy = 'test';
          caseDetails.login = 'test';
          caseDetails.modificationNumber = 'test';
          caseDetails.name = 'test';
          caseDetails.negoResult = 'test';
          caseDetails.orgId = 'test';
          caseDetails.purchaseDate = datetime.now();
          caseDetails.purchaseIntention = 'test';
          caseDetails.quoteType = 'test';
          caseDetails.rcName = 'test';
          caseDetails.rcRowId = 'test';
          caseDetails.resAuthor = 'test';
          caseDetails.resDate = datetime.now();
          caseDetails.rowId = 'test';
          caseDetails.serialNum = 'test';
          caseDetails.srArea = 'test';
          caseDetails.srAreaCode = 'test';
          caseDetails.srBrand = 'test';
          caseDetails.srCustNum = 'test';
          caseDetails.srNum = 'test';
          caseDetails.srSubtypeCode = 'test';
          caseDetails.state = 1;
          caseDetails.subArea = 'test';
          caseDetails.traitment = 'test';
          caseDetails.zipCode = 'test';
          

          // Send the response
          WebserviceRcArchivageV2.getCaseDetailsResponse response_casedetails = new WebserviceRcArchivageV2.getCaseDetailsResponse();
          response_casedetails.caseDetails = caseDetails;
          response.put('response_x', response_casedetails); 
        }
  
  
  
       /* request = new WebserviceRcArchivageV2.getXmlDetails();
        response = new Map<String, WebserviceRcArchivageV2.getXmlDetailsResponse>();
        WebserviceRcArchivageV2.getXmlDetails xmlDetailReq = ((WebserviceRcArchivageV2.getXmlDetails)request);
        String subresp_xml = 'xml details';
        
        // Send the response
        WebserviceRcArchivageV2.getXmlDetailsResponse response_xml = new WebserviceRcArchivageV2.getXmlDetailsResponse();
        response_xml.xmlData = subresp_xml;
        response.put('response_x', response_xml);
        
        
        */
        
        
   }
}