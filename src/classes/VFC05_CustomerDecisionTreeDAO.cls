/**
*	Class   -   VFC05_CustomerDecisionTreeDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*   
*   #01 <RameshPrabu> <31/08/2012>
*       Created this Class to handle record from Customer Decision Tree related Queries.
*/
public with sharing class VFC05_CustomerDecisionTreeDAO {
	
	private static final VFC05_CustomerDecisionTreeDAO instance = new VFC05_CustomerDecisionTreeDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC05_CustomerDecisionTreeDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC05_CustomerDecisionTreeDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get Customer Decision Tree Record based on some lead fields
    * @param behaviorCustomer - This field using from behavior Customer of lead object             
    * @param cadastralCustomer - This field using from cadastral Customer of lead object
    * @param currentVehicle - This field using from current Vehicle of lead object
    * @param preApprovedCredit - This field using from preApproved Credit of lead object
    * @param vehicleOfInterest - This field using from vehicle Of Interest of lead object
    * @param vehicleOfCampaign - This field using from vehicle Of Campaign of lead object
    * @return lstCustomerTree - fetch and return the result in lstCustomerTree list
    */
    public List<CDT_CustomerDecisionTree__c> findCustomerDecisionTree(String behaviorCustomer, String cadastralCustomer, Boolean currentVehicle, String preApprovedCredit, Boolean vehicleOfInterest, Boolean vehicleOfCampaign){
        
        List<CDT_CustomerDecisionTree__c> lstCustomerTree = null;
        
        lstCustomerTree = [select 
        					Id, 
        					Name, 
        					Recommendation1__c, 
        					Recommendation2__c 
    				 	 from 
    				 	 	CDT_CustomerDecisionTree__c 
    				 	 where 
	    				 	 BehaviorCustomer__c =:behaviorCustomer
	    				 	 And CadastralCustomer__c =:cadastralCustomer 
	    				 	 And CurrentVehicle__c =:currentVehicle 
	    				 	 And PreApprovedCredit__c =:preApprovedCredit 
	    				 	 And VehicleOfInterest__c =:vehicleOfInterest 
	    				 	 And VehicleOfCampaign__c =:vehicleOfCampaign];
	    				 	 
        return lstCustomerTree;
    }
}