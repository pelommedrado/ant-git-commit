/**
* @Author Bic Admin
* @Modifiedby RNTBCI(Ramamoorthy Dakshinamoorthy)
* @date 29/02/2016
* @description This util class is used to get the contract information from RUBICS_WS
* Project Rforce
*/

public with sharing class Rforce_Utils_ContractDataSource {

    // Boolean variable used to specify if the dummy implementation must be used or the real one
    public Boolean Test;
    //public String strCountry;
    //public String strCountryVal3L;
    public User oUsr {get;set;}
    
  /** @author: Bic Admin
* @Modified by : Ramamoorthy Dakshinamoorthy
* @date : 29/02/2016
* @description:to send request by calling the external webservice using contract endpoint URL
* @param: Rforce_VehicleAttributes instance
* @return: contract WS info
*/    
    public Rforce_CustdataCrmBserviceRenault.GetCustDataResponse getContractData(Rforce_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
        // --- PRE TREATMENT ---- 
        system.debug('## Inside Rforce_Utils_ContractDataSource :: getContractData method ##');     
        Rforce_CustdataCrmBserviceRenault.GetCustDataRequest request = new Rforce_CustdataCrmBserviceRenault.GetCustDataRequest();
        Rforce_CustdataCrmBserviceRenault.ServicePreferences servicePref = new Rforce_CustdataCrmBserviceRenault.ServicePreferences();
        Rforce_CustdataCrmBserviceRenault.CustDataRequest custPref = new Rforce_CustdataCrmBserviceRenault.CustDataRequest();
        oUsr = [Select RecordDefaultCountry__c from User where Name =:UserInfo.getName()];        
           // strCountry = oUsr.RecordDefaultCountry__c;           
        
        Country_Info__c Country3L = [Select Country_Code_3L__c from Country_Info__c where Name =:oUsr.RecordDefaultCountry__c];
        //strCountryVal3L = Country3L.Country_Code_3L__c;
        custPref.vin = VehController.getVin(); 
        custPref.mode = System.Label.Rforce_ContractMode;
        custPref.country = Country3L.Country_Code_3L__c;
       custPref.demander = System.Label.Rforce_ContractDemander;
        request.servicePrefs = servicePref; 
        request.custDataRequest = custPref;
    
        // ---- WEB SERVICE CALLOUT -----    
        Rforce_CustdataCrmBserviceRenault.CrmGetCustData ConWS = new Rforce_CustdataCrmBserviceRenault.CrmGetCustData();
        ConWS.endpoint_x = System.label.VFP05_ContractURL;
        ConWS.clientCertName_x = System.label.RenaultCertificate;
        ConWS.timeout_x=40000;
        
        Rforce_CustdataCrmBserviceRenault.GetCustDataResponse CON_WS = new Rforce_CustdataCrmBserviceRenault.GetCustDataResponse();    
        system.debug('## After retriving the values using GetCustDataResponse ##'); 
         
        if (Test==true) {
            CON_WS = Rforce_Utils_Stubs.ContractStub();    
        } else { 
            system.debug('## Inside else part ##');
            CON_WS = ConWS.getCustData(request);
            system.debug('## CON_WS value is..::'+ CON_WS);
        }
         return CON_WS;  
    }
    @TestVisible
    private String getUserLanguage(){
        return UserInfo.getLanguage().substring(0, 2).toUpperCase();
    /*  system.debug(UserInfo.getName());
        Map<String, Rforce_LanguageMapping__c> cs = new Map<String, Rforce_LanguageMapping__c>{};
        cs = Rforce_LanguageMapping__c.getAll();
        system.debug('User language is:' + UserInfo.getLanguage() + UserInfo.getName());
        GetUserInfoResult userinforesult = new GetUserInfoResult();
        userinforesult = UserInfo.getUserInfo();
        return userinforesult.userLanguage;*/
        //return cs.get(UserInfo.getLanguage()).Short_Code__c;
    }
}