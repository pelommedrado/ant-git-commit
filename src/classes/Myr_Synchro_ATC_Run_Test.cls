/************************************************************
Name    : Myr_Synchro_ATC_Run_Test
Desc    : Test class about Synchro ATC visualforce page
Project : MyRenault
*************************************************************/
@isTest
private class Myr_Synchro_ATC_Run_Test {
	@testSetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		String byPassTrg = 'Account.SynchroATC,VehicleRelation.SynchroATC,Vehicle.SynchroATC';
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser> ();
		listReqTechUser.add(new Myr_Datasets_Test.RequestedTechUser('Italy', byPassTrg, true));
		listReqTechUser.add(new Myr_Datasets_Test.RequestedTechUser('UK', null, false));
		Myr_Datasets_Test.insertTechnicalUsers(listReqTechUser);
	}

	static void runCommonAssert(Account acc, Myr_Synchro_ATC_Run ext) {
		system.AssertEquals(acc.Id, ext.P_H.Account_Id);
		system.AssertEquals(3, ext.P_H.L_H.size());
	}

	static void Update_CS04(){
		CS04_MYR_Settings__c setting;
		setting = [select Myr_ATC_Activated__c from CS04_MYR_Settings__c];
		setting.Myr_ATC_Activated__c=true;
		update setting;
	}
	
	//Myr Status <> Activated
	static testmethod void Test_010() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));

		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id = acc.Id;
			ext.Init_General();
		}
		Test.stopTest();

		runCommonAssert(acc, ext);
		system.AssertNotEquals(true, ext.P_H.Buttn_Acc_Rnd); //Synchronization of the account is unavailable (button disabled)
	}
	
	//Get_Static_Ressource
	static testmethod void Test_020_Get_Static_Ressource() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;
		Myr_Synchro_ATC_Run.Page_Handler P_H;

		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', '');
		acc= [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));

		Test.startTest();
		system.runAs(u1) {
			P_H = new Myr_Synchro_ATC_Run.Page_Handler();
			ext.Get_Static_Ressource(P_H);
		}
		Test.stopTest(); 
		system.AssertEquals(true, P_H.imgAutomatic.contains('/resource/'));
		system.AssertEquals(true, P_H.imgAutomatic.contains('/Automatic'));
		system.AssertEquals(true, P_H.imgManual.contains('/resource/'));
		system.AssertEquals(true, P_H.imgManual.contains('/Manual'));
	}
	 
	//Init_SynchroATC_List : Zero records
	static testmethod void Test_030_Init_SynchroATC_List() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext; 
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', '');
		acc = [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));

		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id=acc.Id;
			ext.Init_General();
		}
		Test.stopTest(); 

		runCommonAssert(acc, ext);
		//system.AssertEquals(false, ext.P_H.AtLeastOneSynchro); //"Synchro ATC" section not visible 
	}
	 
	//Init_SynchroATC_List : At least one record
	static testmethod void Test_040_Init_SynchroATC_List() {
		User u1; 
		Account acc;
		Myr_Synchro_ATC_Run ext;
		
		u1= Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		acc = [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));

		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id=acc.Id;
			ext.F07_Init_SynchroATC_List(acc.Id);
		}
		Test.stopTest(); 

		system.AssertEquals(1, ext.P_H.L_H_Synchro.size());
	}

	//Init_Veh_List
	static testmethod void Test_050_Init_Veh_List() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		acc = [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));

		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id=acc.Id;
			ext.F02_Init_Veh_List(acc.Id);
		}
		Test.stopTest(); 
	}
	 
	//Init_Account
	static testmethod void Test_060_Init_Account() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;

		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		acc = [SELECT Id, Myr_Status__c, MyRenaultID__c, CSDB_Account_Activated__c, Salutation_Digital__c, Firstname, Lastname, HomePhone__c, BillingStreet, BillingPostalCode, BillingCity, PersMobPhone__c, country__c FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));

		system.AssertEquals(acc.Myr_Status__c, ext.P_H.Account_Status);
		system.AssertEquals(acc.MyRenaultID__c, ext.P_H.MyRenaultID);
		system.AssertEquals(acc.CSDB_Account_Activated__c, ext.P_H.CSDB_Account_Activated);
		system.AssertEquals(acc.Salutation_Digital__c, ext.P_H.Salutation_Digital);
		system.AssertEquals(acc.Firstname, ext.P_H.Firstname);
		system.AssertEquals(acc.Lastname, ext.P_H.Lastname);
		system.AssertEquals(acc.HomePhone__c, ext.P_H.HomePhone);
		system.AssertEquals(acc.BillingStreet, ext.P_H.Street);
		system.AssertEquals(acc.BillingPostalCode, ext.P_H.Zip_Code);
		system.AssertEquals(acc.BillingCity, ext.P_H.City);
		system.AssertEquals(acc.PersMobPhone__c, ext.P_H.Mobile_Phone);
		system.AssertEquals(acc.country__c, ext.P_H.country);
	}

	//Run_Synchro_VIN
	static testmethod void Test_070_Run_Synchro_VIN() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;
		VRE_VehRel__c My_Veh_Rel;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		acc = [SELECT Id, Myr_Status__c, MyRenaultID__c, CSDB_Account_Activated__c, Salutation_Digital__c, Firstname, Lastname, HomePhone__c, BillingStreet, BillingPostalCode, BillingCity, PersMobPhone__c, country__c FROM Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(acc));
		My_Veh_Rel = [select Id, vin__c from VRE_VehRel__c limit 1];
	 
		Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.PAGE_VIN_OK));
	 
		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id=acc.Id;
			ext.P_H.Call_CSDB_Vehicle=true;
			ext.P_H.Country='France';
			ext.P_H.MyRenaultID='do.cc@ff.com';
			ext.P_H.Vehicle_Vin='VF1JZ0NA643766053';
			ext.P_H.Vehicle_Model='Model';
			ext.P_H.Relation_Id=My_Veh_Rel.Id;
			ext.P_H.Vehicle_Id=My_Veh_Rel.vin__c;
			ext.F03_Run_Synchro_Vehicle(ext.P_H);
		}
		Test.stopTest(); 
		//system.AssertEquals('0 VIN OK', ext.P_H.returnMessage_Sync_Vin);
	}

	//Run_Synchro_Account
	static testmethod void Test_080_Run_Synchro_Account() {
		User u1;
		Account a1;
		Myr_Synchro_ATC_Run ext;
		Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.PAGE_VIN_OK));
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		a1 = [select Id from Account];
		ext = new Myr_Synchro_ATC_Run( new ApexPages.StandardController(a1));

		Test.startTest();
		system.runAs(u1) {
			ext.P_H.Account_Id=a1.Id;
			ext.P_H.Call_CSDB_Account=true;
			ext.F04_Run_Synchro_Account(ext.P_H);  
		}
		Test.stopTest(); 
	}
	 
	//Get_Csdb_Profile
	static testmethod void Test_090_Get_Csdb_Profile() {
		User u1;
		Account a1;
		Myr_Synchro_ATC My_Synchro;
		Myr_Synchro_ATC.Csdb_Profile Cs_Prof;
		
		Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.PAGE_VIN_OK));
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		a1 = [select Id from Account];
		My_Synchro = new Myr_Synchro_ATC();
	 
		Test.startTest();
		system.runAs(u1) {
			Cs_Prof = My_Synchro.Get_Csdb_Profile('don@rr.com', 'France', a1.Id);  
		}
		Test.stopTest(); 
	}
	 
	//Parse_Xml
	static testmethod void Test_100_Parse_Xml() {
		User u1;
		Account a1;
		Myr_Synchro_ATC My_Synchro;
		HttpResponse Res;
		Myr_Synchro_ATC.Csdb_Profile p;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Activated', 'ONE');
		a1= [select Id from Account];
		My_Synchro = new Myr_Synchro_ATC();
		Res = new HttpResponse();
		Res.setBody('<?xml version="1.0" encoding="UTF-8"?><customerVehicles><customer><address>303f79f528af997019f436053c905394d214f91f9f7b728518a7b0d7e1c70bf5</address><addressType>INDIVIDUAL</addressType><city>c468212ddf8d851136ece324c5bd02d6e03e59a55d375d1bdbcf2461a8ff874c</city><civility /><countryIsoCode>GB</countryIsoCode><email>b1d31eb895a29695f9aac6efe743aa17aca9d799f47f9c6cc79a4d816d1e3edd</email><firstName>85debe77d106863639fe02e7ac9b6e527705cb3b5b037de20e95501019bc05ad</firstName><lastName>5c19cedcddeda21f25ffcfc06d5710cde6baed4f7c30f683e7cc7ee3fca711a4</lastName><optInATCProfile>00000</optInATCProfile><portalCountryCode>GB</portalCountryCode><zipCode>e846a3867ee2598139c1957e15fafe06f2357059c177aaa944cb5769bf1a5ae1</zipCode></customer><vehicles><vin>111</vin><vin>111</vin><vin>111</vin></vehicles><status><responseCode>0</responseCode><responseDiag>CSDB-0000 : Success</responseDiag><value>OK</value></status></customerVehicles>');
		Test.setMock(HttpCalloutMock.class, new Myr_Synchro_ATC_MK(Myr_Synchro_ATC_MK.MODE.PAGE_VIN_OK));
		
		Test.startTest();
		system.runAs(u1) {
			p = My_Synchro.Parse_Xml(Res);  
		}
		Test.stopTest();
		
		system.AssertEquals('303f79f528af997019f436053c905394d214f91f9f7b728518a7b0d7e1c70bf5', p.My_Address);
		system.AssertEquals('INDIVIDUAL', p.addressType);
		system.AssertEquals('c468212ddf8d851136ece324c5bd02d6e03e59a55d375d1bdbcf2461a8ff874c', p.city);
		system.AssertEquals('', p.civility);
		system.AssertEquals('GB', p.countryIsoCode);
		system.AssertEquals('b1d31eb895a29695f9aac6efe743aa17aca9d799f47f9c6cc79a4d816d1e3edd', p.My_Email);
		system.AssertEquals('85debe77d106863639fe02e7ac9b6e527705cb3b5b037de20e95501019bc05ad', p.firstName);
		system.AssertEquals('5c19cedcddeda21f25ffcfc06d5710cde6baed4f7c30f683e7cc7ee3fca711a4', p.lastName);
		system.AssertEquals('00000', p.optInATCProfile);
		system.AssertEquals('0', p.responseCode);
		system.AssertEquals('CSDB-0000 : Success', p.responseDiag);
		system.AssertEquals('OK', p.value);
		String Compile_Vin='';
		for(Myr_Synchro_ATC.Csdb_Profile_Veh myV : p.VehList){
			Compile_Vin+=myV.vin;
		}
	}
	 
	//Encode_String
	static testmethod void Test_110_Encode_String() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run ext;
		String str1;
		String str2;

		u1= Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		ext = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));

		str1= ext.Encode_String('123eed');  
		str2= ext.Encode_String('123eed');  
		system.AssertEquals(str1, str2);
		
		system.AssertEquals(null, ext.Encode_String(null));
		system.AssertEquals(null, ext.Encode_String(''));
	}
	 
	//Myr_Synchro_ATC.Run_Synchro
	static testmethod void Test_120_Run_Synchro() {
		User u1;
		Account acc;
		List<VRE_VehRel__c> rel;
		List<VEH_Veh__c> veh; 
		Myr_Synchro_ATC.In_Param I_N;

		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		rel = [SELECT Id FROM VRE_VehRel__c];
		veh = [SELECT Id FROM VEH_Veh__c];

		I_N = new Myr_Synchro_ATC.In_Param();
		I_N.Account_Id=acc.Id;
		I_N.Account_Country='France';
		I_N.Account_MyRenaultID='don.dd@ff.com';
		I_N.Vehicle_Vin='CC';
		I_N.Vehicle_Model='MODEL';
		I_N.Relation_Id=rel[0].Id;
		I_N.Vehicle_Id=veh[0].Id;
		I_N.Synchro_Type='Vehicle';
		I_N.Action_On_Update='Vehicle_On_Account';
		I_N.Method='Update_On_Account';
	 
		Myr_Synchro_ATC.Run_Synchro(I_N);
		//TODO : check
	}
	 
	//Insert_SynchroATC
	static testmethod void Test_130_Insert_SynchroATC() {
		User u1;
		Account acc;
		List<VRE_VehRel__c> rel;
		List<VEH_Veh__c> veh;
		Myr_Synchro_ATC_Run ext;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Acc;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Veh;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		rel = [SELECT Id FROM VRE_VehRel__c];
		veh = [SELECT Id FROM VEH_Veh__c];
		ext = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));
		Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();
		Resp_Veh.Synchro_Ok=false;
		Resp_Veh.code='1';
		Resp_Veh.Msg='Test_veh';
		Resp_Veh.Sync_Atc = new Myr_Synchro_ATC.Synchro_Call_Response_Sync_Atc(acc.Id, null, null, 'Update_On_Account', null, 'Body', '<responseCode>1</responseCode><responseDiag>Response</responseDiag>', null, 1, null, null, null, null, 'OK', 'Manual');
		Resp_Veh.Log = new Myr_Synchro_ATC.Synchro_Call_Response_Log('ee', null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_Synchro_ATC', 'POST', acc.Id, 'OK', null, INDUS_Logger_CLS.ErrorLevel.Info.name(), 'CCC');
		Resp_Acc = new Myr_Synchro_ATC.Synchro_Call_Response();
		Resp_Acc.Synchro_Ok=true;
		Resp_Acc.code='1';
		Resp_Acc.Msg='Test';
		Resp_Acc.Sync_Atc = new Myr_Synchro_ATC.Synchro_Call_Response_Sync_Atc(acc.Id, null, null, 'Update_On_Account', null, 'Body', '<responseCode>1</responseCode><responseDiag>Response</responseDiag>', null, 1, null, null, null, null, 'OK', 'Manual');
		Resp_Acc.Log = new Myr_Synchro_ATC.Synchro_Call_Response_Log('ee', null, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_Synchro_ATC', 'POST', acc.Id, 'OK', null, INDUS_Logger_CLS.ErrorLevel.Info.name(), 'CCC');
		
		ext.F06_Insert_SynchroATC(Resp_Acc, Resp_Veh); 
	}

	static testmethod void Test_140_Rendering_Low_Levels() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);

		P_H.List_SynchroAtc_Rnd=true;
		P_H.Buttn_Acc_Rnd=true;
		P_H.Buttn_Veh_Rnd=true;
		P_H.ErrMe_Acc_Rnd=true;
		P_H.ErrMe_Veh_Rnd=true;

		M_A.Rendering_Low_Levels();

		system.AssertEquals(false,P_H.List_SynchroAtc_Rnd);
		system.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		system.AssertEquals(false,P_H.Buttn_Veh_Rnd);
		system.AssertEquals(false,P_H.ErrMe_Acc_Rnd);
		system.AssertEquals(false,P_H.ErrMe_Veh_Rnd);
	}

	static void test_140_Common(Myr_Synchro_ATC_Run.Page_Handler P_H, Boolean My_Status) { 
		P_H.ErrMe_Veh_Sev='';
		P_H.ErrMe_Veh_Rnd=false;
		P_H.ErrMe_Acc_Rnd=true;
		P_H.ErrMe_Veh_Lbl='';
		P_H.Status_3flags=My_Status;
	} 
	 
	static testmethod void Test_150_Rendering_3flags_OK() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.Prefix_Mes_Veh='PREFIX';	
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		
		test_140_Common(P_H, true);
	 
		M_A.Rendering_3flags();

		system.AssertEquals('info',P_H.ErrMe_Veh_Sev);
		system.AssertEquals(true,P_H.ErrMe_Veh_Rnd);
		system.AssertEquals(false,P_H.ErrMe_Acc_Rnd);
		system.AssertEquals('PREFIX'+Label.Myr_Synchro_ATC_Data_Completion_OK,P_H.ErrMe_Veh_Lbl);

		test_140_Common(P_H, false);

		M_A.Rendering_3flags();

		system.AssertEquals('error',P_H.ErrMe_Veh_Sev);
		system.AssertEquals(true,P_H.ErrMe_Veh_Rnd);
		system.AssertEquals(false,P_H.ErrMe_Acc_Rnd);
		system.AssertEquals('PREFIX'+P_H.Status_3flags_Lbl,P_H.ErrMe_Veh_Lbl);

		test_140_Common(P_H, null);

		M_A.Rendering_3flags(); 

		system.AssertEquals('info',P_H.ErrMe_Veh_Sev);
		system.AssertEquals(false,P_H.ErrMe_Veh_Rnd);
		system.AssertEquals(true,P_H.ErrMe_Acc_Rnd);
		system.AssertEquals('',P_H.ErrMe_Veh_Lbl);
	}
	 
	static testmethod void Test_160_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.All_Account_Fields_Synchronized=false;
		P_H.country='France';
		Update_CS04();
		
		M_A.Rendering_Synchro();

		system.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		//system.AssertEquals(false,P_H.Buttn_Veh_Rnd);
		//system.AssertEquals(Label.Myr_Synchro_ATC_Disabled_Org,P_H.ErrMe_Acc_Lbl);
		//system.AssertEquals(true,P_H.ErrMe_Acc_Rnd);
		//system.AssertEquals('error',P_H.ErrMe_Acc_Sev);
	}

	static testmethod void Test_170_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.All_Account_Fields_Synchronized=false;
		P_H.country='Argentina';
		Update_CS04();
		
		M_A.Rendering_Synchro();

		system.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		system.AssertEquals(Label.Myr_Synchro_ATC_Disabled_Country,P_H.ErrMe_Acc_Lbl);
		system.AssertEquals(true,P_H.ErrMe_Acc_Rnd);
		system.AssertEquals('error',P_H.ErrMe_Acc_Sev);
	}
	 

	static testmethod void Test_180_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;

		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.All_Account_Fields_Synchronized=false;
		P_H.country='France';
		Update_CS04();
		
		M_A.Rendering_Synchro();

		system.AssertEquals('The account is not activated for brand Renault. Myr Status :null',P_H.ErrMe_Acc_Lbl);
	}
	 
	static testmethod void Test_190_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Veh;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh= new Myr_Synchro_ATC.Synchro_Call_Response();
		Update_CS04();
		
		P_H.Resp_Veh.code='MYCODE'; 
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.All_Account_Fields_Synchronized=true;
		P_H.country='France';
		P_H.Call_CSDB_Vehicle=true;
		P_H.Account_Status='';
	 
		M_A.Rendering_Synchro();

		System.AssertEquals(false,P_H.Buttn_Acc_Rnd); 
		System.AssertEquals(Label.Myr_Synchro_ATC_No_Activated+P_H.Account_Status,P_H.ErrMe_Acc_Lbl);
	}

	static testmethod void Test_200_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H; 
		Myr_Synchro_ATC_Run.Availability M_A;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Veh;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();

		Update_CS04();
		
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.All_Account_Fields_Synchronized=true;
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Created;
	 
		M_A.Rendering_Synchro();

		System.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		System.AssertEquals(Label.Myr_Synchro_ATC_No_Activated+P_H.Account_Status,P_H.ErrMe_Acc_Lbl);
	}

	static testmethod void Test_210_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Veh;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();
	 
		Update_CS04();
				
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.All_Account_Fields_Synchronized=true;
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=true;
	 
		M_A.Rendering_Synchro();

		System.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		System.AssertEquals('error',P_H.ErrMe_Veh_Sev);
		System.AssertEquals('PREFIXMYCODE MYMESS',P_H.ErrMe_Veh_Lbl);
	}
	 
	static testmethod void Test_220_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh= new Myr_Synchro_ATC.Synchro_Call_Response();
	 	Update_CS04();
				
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.All_Account_Fields_Synchronized=true;
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=false;
	 
		M_A.Rendering_Synchro();

		System.AssertEquals(false,P_H.Buttn_Acc_Rnd); 
	}
	 
	static testmethod void Test_230_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Veh;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();
		Update_CS04();
		
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=false;
		P_H.All_Account_Fields_Synchronized=false;
		P_H.Is_One_Vehicle_Good=false;
	 
		M_A.Rendering_Synchro();
	}
	 
	static testmethod void Test_240_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
	 	Update_CS04();
				
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=true;
		P_H.All_Account_Fields_Synchronized=false;
		P_H.Is_One_Vehicle_Good=true;
	 
		M_A.Rendering_Synchro(); 

		System.AssertEquals(true,P_H.ErrMe_Veh_Rnd);
		System.AssertEquals('error',P_H.ErrMe_Veh_Sev);
		System.AssertEquals('PREFIXMYCODE MYMESS',P_H.ErrMe_Veh_Lbl);
	}
	 
	static testmethod void Test_250_Rendering_Synchro_KO() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		Myr_Synchro_ATC.Synchro_Call_Response Resp_Acc;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		P_H.Resp_Acc = new Myr_Synchro_ATC.Synchro_Call_Response();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
	 
		Update_CS04();
		
		P_H.Resp_Acc.code='MYCODE';
		P_H.Resp_Acc.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=false;
		P_H.All_Account_Fields_Synchronized=false;
		P_H.Is_One_Vehicle_Good=true;
	 
		M_A.Rendering_Synchro();

		System.AssertEquals('error', P_H.ErrMe_Acc_Sev);
		System.AssertEquals('MYCODE MYMESS', P_H.ErrMe_Acc_Lbl);
	}

	static testmethod void Test_260_Rendering_Level0() {
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		Myr_Synchro_ATC_Run.Availability M_A;
		
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		M_A = new Myr_Synchro_ATC_Run.Availability(P_H);
		P_H.Resp_Veh = new Myr_Synchro_ATC.Synchro_Call_Response();
		Update_CS04();
		
		P_H.Resp_Veh.code='MYCODE';
		P_H.Resp_Veh.Msg='MYMESS';
		P_H.Prefix_Mes_Veh='PREFIX';
		P_H.country='France';
		P_H.Account_Status=Label.Myr_Status_Activated;
		P_H.Call_CSDB_Vehicle=false;
		P_H.All_Account_Fields_Synchronized=false;
		P_H.Is_One_Vehicle_Good=false;
	 
		P_H.Buttn_3fl_Rnd=false;
		P_H.List_SynchroAtc_Rnd=false;
		P_H.Buttn_Acc_Rnd=false;
		P_H.Buttn_Veh_Rnd=false;
		P_H.ErrMe_Acc_Rnd=false;
		P_H.ErrMe_Veh_Rnd=false; 

		system.runAs(Myr_Datasets_Test.getTechnicalUser('UK')){ 
			M_A.Display_Buttons();
		}
	 
		System.AssertEquals(false,P_H.Buttn_3fl_Rnd);
		System.AssertEquals(false,P_H.List_SynchroAtc_Rnd);
		System.AssertEquals(false,P_H.Buttn_Acc_Rnd);
		System.AssertEquals(false,P_H.Buttn_Veh_Rnd);
		System.AssertEquals(false,P_H.ErrMe_Acc_Rnd);
		System.AssertEquals(false,P_H.ErrMe_Veh_Rnd);
	}

	static testmethod void Test_270_Rendering_Level0() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		List<CS04_MYRAuthorization_Settings__c> settings;
		CS04_MYRAuthorization_Settings__c setting;
		Myr_Synchro_ATC_Run tt;
		List<Profile> L_P;  
		Map<String, Id> m;
		 
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		tt = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));
		L_P = [select Name, Id from Profile where Name='HeliosTechnique'];  
		m = new Map<String, Id>();

		for (Profile p : L_P){
			m.put(p.Name, p.Id);
		}
	 
		settings = [select Id from CS04_MYRAuthorization_Settings__c];
		delete settings;

		setting=new CS04_MYRAuthorization_Settings__c(Name='HeliosTechnique', ATC_Level1__c=true, ATC_Level2__c=false, ATC_Level3__c=false);
		insert setting;
		
		system.runAs(Myr_Datasets_Test.getTechnicalUser('UK')){ 
			tt.F01_Init_Authorizations(P_H);
		}
	}

	static testmethod void Test_280_Rendering_Level0() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		List<CS04_MYRAuthorization_Settings__c> settings;
		CS04_MYRAuthorization_Settings__c setting;
		Myr_Synchro_ATC_Run tt;
		List<Profile> L_P;  
		Map<String, Id> m;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		tt = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));
		L_P = [select Name, Id from Profile where Name='HeliosTechnique'];  
		m = new Map<String, Id>();

		for (Profile p : L_P){
			m.put(p.Name, p.Id);
		}
	 
		settings = [select Id from CS04_MYRAuthorization_Settings__c];
		delete settings;

		setting=new CS04_MYRAuthorization_Settings__c(Name='HeliosTechnique', ATC_Level1__c=false, ATC_Level2__c=true, ATC_Level3__c=false);
		insert setting;
		
		system.runAs(Myr_Datasets_Test.getTechnicalUser('UK')){ 
			tt.F01_Init_Authorizations(P_H);
		}
	}

	static testmethod void Test_290_Rendering_Level0() {
		User u1; 
		Account acc;
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		List<CS04_MYRAuthorization_Settings__c> settings;
		CS04_MYRAuthorization_Settings__c setting;
		Myr_Synchro_ATC_Run tt;
		List<Profile> L_P;  
		Map<String, Id> m;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		tt = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));
		L_P = [select Name, Id from Profile where Name='HeliosTechnique'];
		m = new Map<String, Id>();

		for (Profile p : L_P){
			m.put(p.Name, p.Id);
		}
	 
		settings = [select Id from CS04_MYRAuthorization_Settings__c];
		delete settings;

		setting=new CS04_MYRAuthorization_Settings__c(Name='HeliosTechnique', ATC_Level1__c=false, ATC_Level2__c=false, ATC_Level3__c=true);
		insert setting;
		
		system.runAs(Myr_Datasets_Test.getTechnicalUser('UK')){ 
			tt.F01_Init_Authorizations(P_H);
		}
	} 

	static testmethod void Test_300_Rendering_Level0() {
		User u1;
		Account acc;
		Myr_Synchro_ATC_Run.Page_Handler P_H;
		List<CS04_MYRAuthorization_Settings__c> settings;
		CS04_MYRAuthorization_Settings__c setting;
		Myr_Synchro_ATC.Csdb_Profile Cs_Prof;
		Myr_Synchro_ATC_Run tt;
		List<Profile> L_P;  
		Map<String, Id> m;
		
		u1 = Myr_Datasets_Test.Synchro_ATC_PrepareData('123', 'Created', '');
		acc = [SELECT Id FROM Account];
		P_H = new Myr_Synchro_ATC_Run.Page_Handler();
		tt = new Myr_Synchro_ATC_Run(new ApexPages.StandardController(acc));
		L_P = [select Name, Id from Profile where Name='HeliosTechnique'];
		m = new Map<String, Id>();

		for (Profile p : L_P){
			m.put(p.Name, p.Id);
		}
	 
		settings = [select Id from CS04_MYRAuthorization_Settings__c];
		delete settings;

		setting= new CS04_MYRAuthorization_Settings__c(Name='HeliosTechnique', ATC_Level1__c=false, ATC_Level2__c=false, ATC_Level3__c=true);
		insert setting;

		P_H.Account_Synchronized=true;
		Cs_Prof = new Myr_Synchro_ATC.Csdb_Profile();
		
		system.runAs(Myr_Datasets_Test.getTechnicalUser('UK')){ 
			//tt.F05_Init_Csdb_Profile(P_H);
		}
	}
}