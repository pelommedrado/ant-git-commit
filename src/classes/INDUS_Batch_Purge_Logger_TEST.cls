/*******************************************************************************************
  Name    : INDUS_Batch_Purge_Logger_TEST                        Creation date : 2015
  Desc    : TestClass dedicated to test the purge of Logger__c
  Author  : Donatien Veron (AtoS)
  Project : myRenault
  ******************************************************************************************
         2015                                   : Creation
  17 May 2016, 16.07 BackEagle, D. Veron (AtoS) : Update
  ******************************************************************************************/
  
@isTest(seeAllData=false)
private class INDUS_Batch_Purge_Logger_TEST {
	
	@testSetup static void Init_Test(){
		Id Record_Type_Application_Id;
		Id Record_Type_Supervision_Id;
		List<Logger__c> lstLogs;
		Logger__c log;
		
		//Initialisations
		Record_Type_Application_Id = (Myr_Datasets_Test.getRecordTypeByAPIName('Logger__c','Application')).Id;
        Record_Type_Supervision_Id = (Myr_Datasets_Test.getRecordTypeByAPIName('Logger__c','Supervision')).Id;
		lstLogs = new List<Logger__c>();

		//Creation of the Dataset
		for (Integer i=0; i<28; i++){
	    	log = new Logger__c();
	    	if(i>=0 && i<9){
	    		System.debug('#### Application i=' + i);
	    		log.RecordTypeId=Record_Type_Application_Id;
	    		if(i>=0 && i<2){
	    			System.debug('#### Application MYR i=' + i);
	    			log.Project_Name__c='MYR';
	    		}
	    		if(i>=2 && i<5){
	    			System.debug('#### Application LMTSFA i=' + i);
	    			log.Project_Name__c='LMTSFA'; 
	    		}
	    		if(i>=5 && i<9){
	    			System.debug('#### Application TRV i=' + i);
	    			log.Project_Name__c='TRV';
	    		}
	    	}else{
	    		System.debug('#### Supervision i=' + i);
	    		log.RecordTypeId=Record_Type_Supervision_Id;
	    		if(i>=9 && i<14){
	    			System.debug('#### Supervision MYR i=' + i);
	    			log.Project_Name__c='MYR';
	    		}
	    		if(i>=14 && i<20){
	    			System.debug('#### Supervision LMTSFA i=' + i);
	    			log.Project_Name__c='LMTSFA';
	    		}
	    		if(i>=20 && i<28){
	    			System.debug('#### Supervision TRV i=' + i);
	    			log.Project_Name__c='TRV';
	    		}
	    	}
	    	
	    	log.Apex_Class__c='TEST.cls';
	    	log.Function__c='TEST';
	    	log.Record__c='TEST';
	    	log.Exception_Type__c='TEST';
	    	log.Message__c='TEST';
	    	log.Error_Level__c='TEST';
	    	lstLogs.add(log);
	    }
	    insert lstLogs;
	    lstLogs = [select Id, RecordTypeId, Project_Name__c from Logger__c];
        System.assertEquals(28, lstLogs.size());
	}

	//MYR - Application
	static testMethod void test010_MYR_Application(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_MYR_Application__c from CS14_Logger_Settings__c];
		setting[0].Retention_MYR_Application__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(26, lstLogs.size());
	}
	
	//MYR - Supervision
	static testMethod void test020_MYR_Supervision(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_MYR_Supervision__c from CS14_Logger_Settings__c];
		setting[0].Retention_MYR_Supervision__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(23, lstLogs.size());
	}
	
	//LMTSFA - Application
	static testMethod void test030_LMTSFA_Application(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_LMTSFA_Application__c from CS14_Logger_Settings__c];
		setting[0].Retention_LMTSFA_Application__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(25, lstLogs.size());
	}
	
	//LMTSFA - Supervision
	static testMethod void test040_LMTSFA_Supervision(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_LMTSFA_Supervision__c from CS14_Logger_Settings__c];
		setting[0].Retention_LMTSFA_Supervision__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(22, lstLogs.size());
	}
	
	//TRV - Application
	static testMethod void test050_TRV_Application(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_TRV_Application__c from CS14_Logger_Settings__c];
		setting[0].Retention_TRV_Application__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(24, lstLogs.size());
	}
	
	//TRV - Supervision
	static testMethod void test060_TRV_Supervision(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_TRV_Supervision__c from CS14_Logger_Settings__c];
		setting[0].Retention_TRV_Supervision__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(20, lstLogs.size());
	}

	//TRV - Both Application & Supervision
	static testMethod void test070_TRV_Both(){
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		INDUS_Batch_Purge_Logger_CLS batch; 
		List<Logger__c> lstLogs;
		
		List<CS14_Logger_Settings__c> setting = [select Retention_TRV_Supervision__c from CS14_Logger_Settings__c];
		setting[0].Retention_TRV_Application__c='-1';
		setting[0].Retention_TRV_Supervision__c='-1';
		update setting;
		
	    //Purge of logs
	    batch = new INDUS_Batch_Purge_Logger_CLS();
        Test.startTest();
       		Database.executeBatch(batch);
        Test.stopTest();  
        
        //Checks
        lstLogs = [select Id from Logger__c];
        System.assertEquals(16, lstLogs.size());
	}
}