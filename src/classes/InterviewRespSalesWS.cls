@RestResource(urlMapping='/sales/*')
global class InterviewRespSalesWS {

    @HttpGet
    global static InterviewResponse__c getSalesInterview() {
        RestRequest request = RestContext.request;
        String interviewResId = request.requestURI.substring(
        request.requestURI.lastIndexOf('/')+1);
        InterviewResponse__c result = InterviewResponseDAO.getInterviewResById(interviewResId);
        return result;
    }

    global class ResponseInterviewSales{
        public String message;
        public String error;
        public Id interviewResponseId;
    }

    @HttpPost
    global static ResponseInterviewSales createSalesInterview(Boolean has_seller_offered_test_drive, Boolean has_user_permormed_test_drive,
                                         Boolean has_vehicle_delivered_on_correct_date_and_time,
                                         Integer dealership_rating, String user_experience,
                                         Boolean has_doubt_suggestion_complaint, String client_user_id) {

        System.debug(client_user_id);

        List<Interview__c> interviewToAssign =  [SELECT Id, Name, Description__c 
                                                FROM Interview__c 
                                                WHERE Name  = 'Pesquisa VN'
                                                LIMIT 1];

        List<Account> accountToAssing = [SELECT Id FROM Account WHERE Id =: client_user_id];

        String testDriveOfferedText = setTestDriveOffered(has_seller_offered_test_drive, has_user_permormed_test_drive);
        String vehicleDelivery      = has_vehicle_delivered_on_correct_date_and_time ? 'Sim.' : 'Não.';
        //Common
        Integer dealerRate          = dealership_rating;
        String userExperience       = user_experience;
        Boolean getInTouch          = has_doubt_suggestion_complaint;

        InterviewResponse__c newInterviewRes = new InterviewResponse__c();

        newInterviewRes.TestDriveOffered__c                     = testDriveOfferedText;
        newInterviewRes.VehicleDeliveriedDateCombined__c        = vehicleDelivery;
        
        newInterviewRes.ReccomendDealerNote__c                  = dealerRate;
        newInterviewRes.CustomerExperience__c                   = userExperience;
        newInterviewRes.CustumerGetInTouch__c                   = getInTouch;

        //FIXED
        if(!accountToAssing.isEmpty()){
            newInterviewRes.AccountId__c                        = accountToAssing[0].id;
        }
        newInterviewRes.InterviewId__c                          = interviewToAssign[0].Id;
        newInterviewRes.Origin__c                               = 'Facebook';
        newInterviewRes.AnswerDate__c                           = System.now();
        //UNUSED
        //newInterviewRes.AnswerDate__c                           = answerDate;
        //newInterviewRes.AnswerLink__c                           = answerLink;
        //newInterviewRes.ContactId__c                            = contactId;
        //newInterviewRes.ContactChannel__c                       = contactChannel;
        //newInterviewRes.CustomerReturnedRenaultRepairShop__c    = customerReturnedRenaultRepairShop;
        //newInterviewRes.DealerGetInTouchAfterService__c         = dealerGetInTouchAfterService;
        //newInterviewRes.ReasonReturn__c                         = reasonReturn;

        ResponseInterviewSales salesReturn = new ResponseInterviewSales();

        try{
            Database.insert(newInterviewRes);
        }
        catch(Exception ex){
            salesReturn.message = 'Error';
            salesReturn.error = ex.getMessage();
            return salesReturn;
        }
        salesReturn.message = 'Sucess';
        salesReturn.error = '';
        salesReturn.interviewResponseId = newInterviewRes.Id;
        return salesReturn;
    }

    public static String setTestDriveOffered(Boolean hasOffered, Boolean hasPerformed){
        if(hasOffered && hasPerformed){
            return 'Sim. E realizei.';
        }
        if(hasOffered && !hasPerformed){
            return 'Sim. Mas optei por não realizar.';
        }
        return 'Não.';
    }

}