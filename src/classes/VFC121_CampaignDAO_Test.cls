@isTest
private class VFC121_CampaignDAO_Test
{
	private static testMethod void unitTest01()
	{
		// Test Data
		Account dealer = VFC129_UTIL_TestData.createAccountDealer();
		insert dealer;

		Lead lead = VFC129_UTIL_TestData.createLead();
		insert lead;

		Campaign campaignParent1 = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_RENAULT');
		insert campaignParent1;
		
		Campaign campaignSingle1 = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_RENAULT_1', null, campaignParent1.Id);
		insert campaignSingle1;
		
		Campaign campaignSingle2 = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_RENAULT_2', null, campaignParent1.Id);
		insert campaignSingle2;

		// Start test
	    Test.startTest();
	    
	    Map<String, Campaign> test01 = VFC121_CampaignDAO.getInstance().fetchCampaignParentByDBMCampaignCodeSet(new Set<String>{'CAMPANHA_RENAULT'});
	    
	    Map<String, Campaign> test02 = VFC121_CampaignDAO.getInstance().fetchCampaignSingleByDBMCampaignCodeSet(new Set<String>{'CAMPANHA_RENAULT_1'});
	    
	    Map<Id, Campaign> test03a = VFC121_CampaignDAO.getInstance().fetchCampaignByIdSet(new Set<Id>{campaignParent1.Id});
	    
	    Map<Id, Campaign> test03b = null;
	    try {
	    	test03b = VFC121_CampaignDAO.getInstance().fetchCampaignByIdSet(new Set<Id>{lead.Id, null});
	    }
	    catch (Exception e) {}

	    Campaign test04 = VFC121_CampaignDAO.getInstance().fetchCampaignById(campaignParent1.Id);
	    
	    Map<Id, List<Campaign>> test05 = VFC121_CampaignDAO.getInstance().fetchChildrenCampaignListByIdSet(new Set<Id>{campaignParent1.Id, campaignSingle2.Id});

	    String test06 = VFC121_CampaignDAO.getInstance().buildCampaignQueryForBatchProcess();

	    List<Campaign> test07 = VFC121_CampaignDAO.getInstance().fetchCampaignForDealer(dealer.Id);
	    
	    List<Campaign> test08 = VFC121_CampaignDAO.getInstance().fetchCampaignForPA(dealer.Id);

	    // Stop test
        Test.stopTest();
        
        // Verify data
        system.assert(test01 != null);
        system.assert(test02 != null);
        system.assert(test03a != null);
        system.assert(test03b != null);
        system.assert(test04 != null);
        system.assert(test05 != null);
        system.assert(test06 != null);
        system.assert(test07 != null);
        system.assert(test08 != null);
	}
}