/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class LiveTextController {
    @Deprecated
    @RemoteAction
    global static LiveText__SMS_Text__c createSMSTextRecord(String message, String conversationId) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String getMessagePayloadResponse(String message, Integer messageId, String conversationId, String originatingNumber, String supportNumber) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static LiveText__SMS_Text__c getSMSTextRecord(String smsTextRecord) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String sendSMS2(SObject smsObject, String smsText) {
        return null;
    }
}
