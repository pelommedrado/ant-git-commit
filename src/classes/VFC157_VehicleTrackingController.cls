public with sharing class VFC157_VehicleTrackingController{

  private static List< SelectOption > getPicklistValues (SObject obj, String field){
    List< SelectOption > options = new List< SelectOption >();

    List< Schema.PicklistEntry > values = obj.getSObjectType().getDescribe().fields.getMap().get( field ).getDescribe().getPickListValues();

    for( Schema.PicklistEntry a : values ) 
     options.add( new SelectOption( a.getValue(), a.getLabel() ) ); 

    return options;
   }

  public String deliveryStatus{get;set;}
  public List< SelectOption > deliveryStatusOptionList{
    get{
      /*List< SelectOption > retList = new List< SelectOption >();
      for( AggregateResult aggRes : [
        select ALR_Status_System_Delivery__c 
        from ALR_Tracking__c 
        where ALR_Status_System_Delivery__c <> null
          and (
            ALR_Date_MADP__c > LAST_N_DAYS:30
            or ALR_Date_MADP__c = null
          )
        group by ALR_Status_System_Delivery__c
        order by ALR_Status_System_Delivery__c 
      ] ) retList.add( new SelectOption( (String)aggRes.get( 'ALR_Status_System_Delivery__c' ), (String)aggRes.get( 'ALR_Status_System_Delivery__c' ) ) );
      return retList;*/
      return getPicklistValues( arrival, 'ALR_Status_System_Delivery__c' );
    }
  }

  public String lockStatus{get;set;}
  public List< SelectOption > lockStatusOptionList{
    get{
      /*List< SelectOption > retList = new List< SelectOption >();
      for( AggregateResult aggRes : [
        select ALR_Status_System_Lock__c 
        from ALR_Tracking__c 
        where ALR_Status_System_Lock__c <> null
          and (
            ALR_Date_MADP__c > LAST_N_DAYS:30
            or ALR_Date_MADP__c = null
          )
        group by ALR_Status_System_Lock__c
        order by ALR_Status_System_Lock__c 
      ] ) retList.add( new SelectOption( (String)aggRes.get( 'ALR_Status_System_Lock__c' ), (String)aggRes.get( 'ALR_Status_System_Lock__c' ) ) );
      return retList;*/
      return getPicklistValues( arrival, 'ALR_Status_System_Lock__c' );
    }
  }

  public String model{get;set;}
  public List< SelectOption > modelOptionList{
    get{
      List< SelectOption > retList = new List< SelectOption >();
      for( AggregateResult aggRes : [
        select ALR_Chassi__r.Model__c model
        from ALR_Tracking__c
        where ALR_Chassi__r.Model__c <> null
          and (
            ALR_Date_MADP__c > LAST_N_DAYS:30
            or ALR_Date_MADP__c = null
          )
        group by ALR_Chassi__r.Model__c
        order by ALR_Chassi__r.Model__c 
      ] ) retList.add( new SelectOption( (String)aggRes.get( 'model' ), (String)aggRes.get( 'model' ) ) );
      return retList;
    }
  }

  public String vehiclePosition{get;set;}
  public List< SelectOption > vehiclePositionOptionList{
    get{
      /*List< SelectOption > retList = new List< SelectOption >();
      for( AggregateResult aggRes : [
        select ALR_Vehicle_Position__c 
        from ALR_Tracking__c 
        where ALR_Vehicle_Position__c <> null
          and (
            ALR_Date_MADP__c > LAST_N_DAYS:30
            or ALR_Date_MADP__c = null
          )
        group by ALR_Vehicle_Position__c
        order by ALR_Vehicle_Position__c
      ] ) retList.add( new SelectOption( (String)aggRes.get( 'ALR_Vehicle_Position__c' ), (String)aggRes.get( 'ALR_Vehicle_Position__c' ) ) );
      return retList;*/
      return getPicklistValues( arrival, 'ALR_Vehicle_Position__c' );
    }
  }

  public List< ALR_Tracking__c > vehicleTrackingList{
    get{
      return Database.query( 
        'select Id, ALR_Chassi__r.Name, ALR_Model__c, ALR_Invoice_Number_Serie__c, ALR_Pilot_Delivery__c, ALR_Forecast_Delivery__c, ALR_Date_MADP__c, ' +
          'ALR_Vehicle_Position__c, ALR_Status_System_Delivery__c, ALR_Status_System_Lock__c, ALR_Status_Delivery__c, ALR_Status_Lock__c ' +
        'from ALR_Tracking__c ' +
        'where (ALR_Date_MADP__c = LAST_N_DAYS:30 ' +
        'or ALR_Date_MADP__c = null) ' +
        (
          String.isNotBlank( deliveryStatus ) ?
            deliveryStatus.equalsIgnoreCase( 'On time' ) ?
              'and ALR_Pilot_Delivery__c >= TODAY ' :
              deliveryStatus.equalsIgnoreCase( 'Delayed' ) ? 
                'and ALR_Pilot_Delivery__c < TODAY ' :
                '' :
            '') +
        (String.isNotBlank( lockStatus ) ? 'and ALR_Status_System_Lock__c = :lockStatus ' : '') +
        (String.isNotBlank( model ) ? 'and ALR_Chassi__r.Model__c = :model ' : '') +
        (String.isNotBlank( vehiclePosition ) ? 'and ALR_Vehicle_Position__c = :vehiclePosition ' : '') +
        'limit 999'
      );
    }
  }

  public ALR_Tracking__c arrival{get;set;}
  
  public String selectedTrackingsIdListAsString{
    get{
      return JSON.serialize( this.selectedTrackingsIdList );
    }
    set{
      this.selectedTrackingsIdList = (List< Id >)JSON.deserialize( value, List< Id >.class );
    }
  }
  List< Id > selectedTrackingsIdList;

  public void setArrivalDate(){
    System.debug( '@@@ Arrival Date: \n' + JSON.serializePretty( arrival ) + '\n Selected Trackings: ' + JSON.serializePretty( this.selectedTrackingsIdList ) );
    List< ALR_Tracking__c > trackingList = new List< ALR_Tracking__c >();
    for( Id trackingId : this.selectedTrackingsIdList )
      trackingList.add( new ALR_Tracking__c( Id = trackingId, ALR_Date_MADP__c = arrival.ALR_Date_MADP__c, ALR_Vehicle_Position__c = 'On dealer stock' ) );
 
    try{
      Database.update( trackingList );
      }catch( Exception e ){
        String errorMsg = e.getMessage();
        Matcher regex = Pattern.compile( '^.*[A-Z_]{2,}, (.*)$' ).matcher( errorMsg );

        if( regex.matches() )
          errorMsg = regex.group( 1 );

        ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, errorMsg ) );
      }
  }

  public PageReference exportExcel(){
    return new PageReference( '/apex/VFP33_VehicleTrackingExcel' );
  }

  public VFC157_VehicleTrackingController (ApexPages.StandardController stdController) {
    arrival = new ALR_Tracking__c( Id = null, OwnerId = UserInfo.getUserId(), ALR_Date_MADP__c = null );
  }
  public VFC157_VehicleTrackingController () {
    arrival = new ALR_Tracking__c( Id = null, OwnerId = UserInfo.getUserId(), ALR_Date_MADP__c = null );
  }
}