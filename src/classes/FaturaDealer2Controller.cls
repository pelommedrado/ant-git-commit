public class FaturaDealer2Controller {
    private static final String CSV_SEPARATE = ';';

	public Boolean typeSwitch					{get;set;} 

	public String invoiceFrom					{get;set;}
	public String invoiceTo						{get;set;}

	public String sendFrom						{get;set;}
	public String sendTo						{get;set;}

	public String issuerBir						{get;set;}
	public String matrixBir						{get;set;}
	public String customerIdentification 		{get;set;}

	public String regionalSelected		 		{get;set;}
	public String statusSelected				{get;set;}
	public String setorSelected					{get;set;}

	public List<SelectOption> regional 			{get;set;}
	public List<SelectOption> status 			{get;set;}
	public List<SelectOption> setor 			{get;set;}

	public List<FaturaDealer2__c> invoiceList	{get;set;}

	public DateTime dataIni;
  	public DateTime dataFim;

  	public Id invoiceType = Utils.getRecordTypeId('FaturaDealer2__c', 'Invoice');
	public Id registerType = Utils.getRecordTypeId('FaturaDealer2__c', 'Register');

	public class InvoiceLabel{
		public String Address 		 					{get{return FaturaDealer2__c.Address__c.getDescribe().getLabel();} set;}
		public String Address_Complement				{get{return FaturaDealer2__c.Address_Complement__c.getDescribe().getLabel();}set;}
		public String BIR_Group_Issuer					{get{return FaturaDealer2__c.BIR_Group_Issuer__c.getDescribe().getLabel();}set;}
		public String BIR_Name_NFE_Issuer				{get{return FaturaDealer2__c.BIR_Name_NFE_Issuer__c.getDescribe().getLabel();}set;}
		public String BIR_NFE_Issuer					{get{return FaturaDealer2__c.BIR_NFE_Issuer__c.getDescribe().getLabel();}set;}
		public String BIR_Issuer_Register 				{get{return FaturaDealer2__c.BIR_Issuer_Register__c.getDescribe().getLabel();}set;}//*NEW*
		public String BIR_Matrix_Register 				{get{return FaturaDealer2__c.BIR_Matrix_Register__c.getDescribe().getLabel();}set;}//*NEW*
		public String Birth_Date						{get{return FaturaDealer2__c.Birth_Date__c.getDescribe().getLabel();}set;}
		public String isCancelled						{get{return FaturaDealer2__c.isCancelled__c.getDescribe().getLabel();}set;}
		public String City								{get{return FaturaDealer2__c.City__c.getDescribe().getLabel();}set;}
		public String CustomerAccount 					{get{return FaturaDealer2__c.CustomerAccount__c.getDescribe().getLabel();}set;}//*NEW*
		public String Customer_Email					{get{return FaturaDealer2__c.Customer_Email__c.getDescribe().getLabel();}set;}
		public String Customer_Email_Mirror 			{get{return FaturaDealer2__c.Customer_Email_Mirror__c.getDescribe().getLabel();}set;}
		public String Customer_Identification_Number 	{get{return FaturaDealer2__c.Customer_Identification_Number__c.getDescribe().getLabel();}set;}
		public String Customer_Name 			 		{get{return FaturaDealer2__c.Customer_Name__c.getDescribe().getLabel();}set;}
		public String Customer_Type 			 		{get{return FaturaDealer2__c.Customer_Type__c.getDescribe().getLabel();}set;}
		public String CNPJ_Issuer_Register 				{get{return FaturaDealer2__c.CNPJ_Issuer_Register__c.getDescribe().getLabel();}set;}//*NEW*
		public String CNPJ_Matrix_Register 				{get{return FaturaDealer2__c.CNPJ_Matrix_Register__c.getDescribe().getLabel();}set;}//*NEW*
		public String DDD1								{get{return FaturaDealer2__c.DDD1__c.getDescribe().getLabel();}set;}
		public String DDD2								{get{return FaturaDealer2__c.DDD2__c.getDescribe().getLabel();}set;}
		public String DDD3								{get{return FaturaDealer2__c.DDD3__c.getDescribe().getLabel();}set;}
		public String DealerAccount 					{get{return FaturaDealer2__c.DealerAccount__c.getDescribe().getLabel();}set;}//*NEW*
		public String Delivery_Date 			 		{get{return FaturaDealer2__c.Delivery_Date__c.getDescribe().getLabel();}set;}
		public String Error_Messages 			 		{get{return FaturaDealer2__c.Error_Messages__c.getDescribe().getLabel();}set;}
		public String External_Key 				 		{get{return FaturaDealer2__c.External_Key__c.getDescribe().getLabel();}set;}
		public String Group_Name_Issuer 	 			{get{return FaturaDealer2__c.Group_Name_Issuer__c.getDescribe().getLabel();}set;}
		public String Integration_Date 		 			{get{return FaturaDealer2__c.Integration_Date__c.getDescribe().getLabel();}set;}
		public String Integration_Protocol 				{get{return FaturaDealer2__c.Integration_Protocol__c.getDescribe().getLabel();}set;}
		public String Invoice_Data 						{get{return FaturaDealer2__c.Invoice_Data__c.getDescribe().getLabel();}set;}
		public String Invoice_Number 					{get{return FaturaDealer2__c.Invoice_Number__c.getDescribe().getLabel();}set;}
		public String Invoice_Serie 					{get{return FaturaDealer2__c.Invoice_Serie__c.getDescribe().getLabel();}set;}
		public String Invoice_Type 						{get{return FaturaDealer2__c.Invoice_Type__c.getDescribe().getLabel();}set;}
		public String Manufacturing_Year 				{get{return FaturaDealer2__c.Manufacturing_Year__c.getDescribe().getLabel();}set;}
		public String MatrixAccount 					{get{return FaturaDealer2__c.MatrixAccount__c.getDescribe().getLabel();}set;}//*NEW*
		public String Model_Year 						{get{return FaturaDealer2__c.Model_Year__c.getDescribe().getLabel();}set;}
		public String Neighborhood 						{get{return FaturaDealer2__c.Neighborhood__c.getDescribe().getLabel();}set;}
		public String TrackNumber 						{get{return FaturaDealer2__c.Number__c.getDescribe().getLabel();}set;}
		public String Payment_Type 						{get{return FaturaDealer2__c.Payment_Type__c.getDescribe().getLabel();}set;}
		public String Phone_1 							{get{return FaturaDealer2__c.Phone_1__c.getDescribe().getLabel();}set;}
		public String Phone_2 							{get{return FaturaDealer2__c.Phone_2__c.getDescribe().getLabel();}set;}
		public String Phone_3 							{get{return FaturaDealer2__c.Phone_3__c.getDescribe().getLabel();}set;}
		public String Postal_Code 						{get{return FaturaDealer2__c.Postal_Code__c.getDescribe().getLabel();}set;}
		public String Region 							{get{return FaturaDealer2__c.Region__c.getDescribe().getLabel();}set;}
		public String isReturned 						{get{return FaturaDealer2__c.isReturned__c.getDescribe().getLabel();}set;}
		public String Sector 							{get{return FaturaDealer2__c.Sector__c.getDescribe().getLabel();}set;}
		public String Sequential_File 					{get{return FaturaDealer2__c.Sequential_File__c.getDescribe().getLabel();}set;}
		public String Shipping_BIR 						{get{return FaturaDealer2__c.Shipping_BIR__c.getDescribe().getLabel();}set;}
		public String State 							{get{return FaturaDealer2__c.State__c.getDescribe().getLabel();}set;}
		public String StatusInvoice	 					{get{return FaturaDealer2__c.Status__c.getDescribe().getLabel();}set;}
		public String VIN 								{get{return FaturaDealer2__c.VIN__c.getDescribe().getLabel();}set;}
	}

	public final static String FILE_NAME = 'Fatura2';

	public static Id folderId {
		get {
			final List<Folder> folderList = [SELECT Id FROM Folder WHERE Name = 'Shared Documents'];
			if(folderList.isEmpty()) {
				System.debug('Folder not found');
				return null;
			}
			return folderList.get(0).Id;
		}
	}

	public void typeChange(){
		typeSwitch = !typeSwitch;
		invoiceList.clear();
		cleanFilter();
	}

	public FaturaDealer2Controller() {
		System.debug('execução');

		typeSwitch = True;

		invoiceList = new List<faturaDealer2__c>();

		regional = new List<SelectOption>();
		regional.add(new SelectOption('1', 'R1'));
		regional.add(new SelectOption('2', 'R2'));
		regional.add(new SelectOption('3', 'R3'));
		regional.add(new SelectOption('4', 'R4'));
		regional.add(new SelectOption('5', 'R5'));
		regional.add(new SelectOption('6', 'R6'));

		//Melhorias Fatura Dealer 2
		//ITEM 1 - Limitar a pesquisa à notas do tipo "New Vehicle" Validas ou Inválidas.
		status = new List<SelectOption>();
		status.add(new SelectOption('\'Invalid\'', 'Inválidos'));
        status.add(new SelectOption('\'Valid\'', 'Válidos'));
		status.add(new SelectOption('\'Invalid\' OR Status__c = \'Valid\' ', 'Todos'));

		setor = new List<SelectOption>();
		setor.add(new SelectOption('1', 'S1'));
		setor.add(new SelectOption('2', 'S2'));
		setor.add(new SelectOption('3', 'S3'));
		setor.add(new SelectOption('4', 'S4'));
		setor.add(new SelectOption('5', 'S5'));

		cleanFilter();
	}

	public void cleanFilter(){
		invoiceFrom	 			= null;
		invoiceTo				= null;
		sendFrom				= null;
		sendTo					= null;
		issuerBir				= null;
		matrixBir				= null;
		customerIdentification	= null;
		regionalSelected		= null;
		statusSelected			= null;
		setorSelected			= null;

		invoiceList.clear();
	}

	public PageReference searchInvoice() {
		System.debug('searchInvoice()');

		if(typeSwitch){

			/*if(String.isNotEmpty(invoiceFrom)) {
			    Date data = Datetime.now().date().addMonths(-3);
			    System.debug(data);
			    System.debug(toDate(invoiceFrom));
			    if(data > toDate(invoiceFrom)) {
			    	Apexpages.addMessage(new Apexpages.Message(
				      ApexPages.Severity.WARNING,
				      'A data mínima para início da análise é '+ Datetime.now().addMonths(-3).format('dd/MM/YYYY')));
				  return null;
	  		    }
			}*/

			if(String.isNotEmpty(invoiceTo)) {
			  Integer d = toDate(invoiceTo).daysBetween(Datetime.now().date());
			  if(d < 0) {
			      Apexpages.addMessage(new Apexpages.Message(
			          ApexPages.Severity.WARNING,
			          'A data máxima para término da análise é '
										+ Datetime.now().format('dd/MM/YYYY')));
			  }
			}

			if(!String.isEmpty(invoiceFrom)) {
				Date data = toDate(invoiceFrom);
				dataIni = DateTime.newInstance(data.year(), data.month(), data.day(), 0, 0 ,0);
			}

			if(!String.isEmpty(invoiceTo)) {
				Date data = toDate(invoiceTo);
				dataFim = DateTime.newInstance(data.year(), data.month(), data.day(), 23, 59 ,59);
			}

			System.debug('dataIni:' + dataIni);
			System.debug('dataFim:' + dataFim);
		}

		final String query = buildQuery();

		System.debug('Query:' + query);

		this.invoiceList = Database.Query(query);
		return null;
	}

	public PageReference searchAllInvoice() {
		System.debug('searchAllInvoice()');
      	Pagereference page = buildCsv();
      	return page;
	}

	private String bodyBuild(List<FaturaDealer2__c> invoices){
		InvoiceLabel lbl = new InvoiceLabel();
		String str = line(new String[] {
      		format(lbl.Address),
			format(lbl.Address_Complement),
			format(lbl.BIR_Group_Issuer),
			format(lbl.BIR_Name_NFE_Issuer),
			format(lbl.BIR_NFE_Issuer),
			format(lbl.BIR_Issuer_Register),
			format(lbl.BIR_Matrix_Register),
			format(lbl.Birth_Date),
			format(lbl.City),
			format(lbl.CustomerAccount),
			format(lbl.Customer_Email),
			format(lbl.Customer_Email_Mirror),
			format(lbl.Customer_Identification_Number),
			format(lbl.Customer_Name),
			format(lbl.Customer_Type),
			format(lbl.CNPJ_Issuer_Register),
			format(lbl.CNPJ_Matrix_Register),
			format(lbl.DDD1),
			format(lbl.DDD2),
			format(lbl.DDD3),
			format(lbl.DealerAccount),
			format(lbl.Delivery_Date),
			format(lbl.Error_Messages),
			format(lbl.External_Key),
			format(lbl.Group_Name_Issuer),
			format(lbl.Integration_Date),
			format(lbl.Integration_Protocol),
			format(lbl.Invoice_Data),
			format(lbl.Invoice_Number),
			format(lbl.Invoice_Serie),
			format(lbl.Invoice_Type),
			format(lbl.Manufacturing_Year),
			format(lbl.Model_Year),
			format(lbl.MatrixAccount),
			format(lbl.Neighborhood),
			format(lbl.TrackNumber),
			format(lbl.Payment_Type),
			format(lbl.Phone_1),
			format(lbl.Phone_2),
			format(lbl.Phone_3),
			format(lbl.Postal_Code),
			format(lbl.Region),
			format(lbl.Sector),
			format(lbl.Sequential_File),
			format(lbl.Shipping_BIR),
			format(lbl.State),
			format(lbl.StatusInvoice),
			format(lbl.VIN)
      	});

      for(FaturaDealer2__c fatura : invoices) {
          str += line(new String[] {
              	format(fatura.Address__c),
				format(fatura.Address_Complement__c),
				format(fatura.BIR_Group_Issuer__c),
				format(fatura.BIR_Name_NFE_Issuer__c),
				format(fatura.BIR_NFE_Issuer__c),
				format(fatura.BIR_Issuer_Register__c),
				format(fatura.BIR_Matrix_Register__c),
				format(fatura.Birth_Date__c),
				format(fatura.City__c),
				format(fatura.CustomerAccount__c),
				format(fatura.Customer_Email__c),
				format(fatura.Customer_Email_Mirror__c),
				format(fatura.Customer_Identification_Number__c),
				format(fatura.Customer_Name__c),
				format(fatura.Customer_Type__c),
				format(fatura.CNPJ_Issuer_Register__c),
				format(fatura.CNPJ_Matrix_Register__c),
				format(fatura.DDD1__c),
				format(fatura.DDD2__c),
				format(fatura.DDD3__c),
				format(fatura.DealerAccount__c),
				format(fatura.Delivery_Date__c),
				format(fatura.Error_Messages__c),
				format(fatura.External_Key__c),
				format(fatura.Group_Name_Issuer__c),
				format(fatura.Integration_Date__c),
				format(fatura.Integration_Protocol__c),
				format(fatura.Invoice_Data__c),
				format(fatura.Invoice_Number__c),
				format(fatura.Invoice_Serie__c),
				format(fatura.Invoice_Type__c),
				format(fatura.Manufacturing_Year__c),
				format(fatura.MatrixAccount__c),
				format(fatura.Model_Year__c),
				format(fatura.Neighborhood__c),
				format(fatura.Number__c),
				format(fatura.Payment_Type__c),
				format(fatura.Phone_1__c),
				format(fatura.Phone_2__c),
				format(fatura.Phone_3__c),
				format(fatura.Postal_Code__c),
				format(fatura.Region__c),
				format(fatura.Sector__c),
				format(fatura.Sequential_File__c),
				format(fatura.Shipping_BIR__c),
				format(fatura.State__c),
				format(fatura.Status__c),
				format(fatura.VIN__c)
          });
      }
      return str;
	}

	private String line(String[] cells) {
	    return String.join(cells, CSV_SEPARATE) + '\n';
	}
	@TestVisible private String format(Date d)    {
	    return d != null ? '"' + d.format().escapeCsv() + '"' : '""';
	}
	@TestVisible private String format(String s) {
	   	return s != null ? '"' + s + '"' : '""';
	}
	@TestVisible private String format(Decimal d, Integer scale) {
	   return d != null ? String.valueOf(d.setScale(scale)) : '';
	}
	@TestVisible private String format(Decimal d) {
	   return format(d, 2);
	}

	private PageReference buildCsv() {
		System.debug('buildCsv()');

		if(!String.isEmpty(invoiceFrom)) {
			Date data = toDate(invoiceFrom);
			dataIni = DateTime.newInstance(data.year(), data.month(), data.day(), 0, 0 ,0);
		}

		if(!String.isEmpty(invoiceTo)) {
		 	Date data = toDate(invoiceTo);
			dataFim = DateTime.newInstance(data.year(), data.month(), data.day(), 23, 59 ,59);

		}

		System.debug('dataIni:' + dataIni);
		System.debug('dataFim:' + dataFim);

		String query = buildFullQuery();

	    System.debug('Query Full:' + query);

	    List<FaturaDealer2__c> faturas =  Database.Query(query);

	    String body = bodyBuild(faturas);

	    final List<Document> docsDelete = [SELECT Id FROM Document WHERE Name =: FILE_NAME];
	    DELETE docsDelete;

	    final Document doc = new Document(
	        Name = FILE_NAME, FolderId = folderId, Body = Blob.valueOf(body),
	        ContentType = 'text/csv;charset=UTF-8', Type = 'csv'
	    );
	    INSERT doc;

	    final PageReference page = new PageReference('/servlet/servlet.FileDownload?file=' + doc.Id);
	    page.setRedirect(true);

	    return page;
	}

	private String buildFullQuery() {
		String query 	= 	'SELECT Id, '
						+	'Name, Address__c, Address_Complement__c, BIR_Group_Issuer__c, BIR_Name_NFE_Issuer__c, '
						+	'BIR_NFE_Issuer__c, Birth_Date__c, isCancelled__c, City__c, Customer_Email__c, '
						+	'Customer_Email_Mirror__c, Customer_Identification_Number__c, Customer_Name__c, '
						+	'Customer_Type__c, DDD1__c, DDD2__c, DDD3__c, Delivery_Date__c, Delivery_Date_Mirror__c,'
						+	'External_Key__c, Group_Name_Issuer__c, Integration_Date__c, Integration_Protocol__c, CNPJ_Issuer_Register__c, ' 
						+	'CNPJ_Matrix_Register__c, Invoice_Data__c, Invoice_Date_Mirror__c, Invoice_Number__c, Invoice_Serie__c, '
						+	'Invoice_Type__c,  Manufacturing_Year__c, Model_Year__c, Neighborhood__c, Number__c, Payment_Type__c, '
						+	'Phone_1__c, Phone_2__c, Phone_3__c, Postal_Code__c, Region__c, isReturned__c, '
						+ 	'DealerAccount__c, MatrixAccount__c, BIR_Issuer_Register__c, '
						+	'BIR_Matrix_Register__c, CustomerAccount__c, '
						+	'Sector__c, Sequential_File__c, Shipping_BIR__c, State__c, Status__c, Error_Messages__c,  VIN__c '
						+	'FROM FaturaDealer2__c '
						+	'WHERE Id != null ';
		//Melhorias Fatura Dealer 2
		//ITEM 1 - Limitar a pesquisa à notas do tipo "New Vehicle" Validas ou Inválidas.
		if(typeSwitch){
			query 	+= 	'AND RecordTypeId = \''+ invoiceType +'\' '
					+	'AND Invoice_Type__c != \'Cancellation\' '
					+   'AND Invoice_Type__c != \'Devolution\' '
					+	'AND (Invoice_Type__c = \'New Vehicle\' '
					+	'AND isCancelled__c != true '
					+ 	'AND isReturned__c != true '
					+	'AND (Status__c = \'Valid\' OR Status__c = \'Invalid\')) '

					+ 	(String.isNotBlank(customerIdentification) ? 'AND Customer_Identification_Number__c LIKE \'%' 
					+ 	customerIdentification   + '%\'' : '' )
					+ 	(String.isNotBlank(issuerBir) ? 'AND BIR_NFE_Issuer__c LIKE \'%' + issuerBir   + '%\'' : '' )
					+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' )
					+	(String.isNotBlank(regionalSelected) ? 'AND Region__c = \''  + regionalSelected  + '\' ' : '' )
					+	(String.isNotBlank(setorSelected) ? 'AND Sector__c = \''  + setorSelected      + '\' ' : '' )
					+ 	(String.isNotEmpty(invoiceFrom) && String.isNotEmpty(invoiceTo) ?
					+	'AND Invoice_Date_Mirror__c >=: dataIni '
					+ 	'AND Invoice_Date_Mirror__c <=: dataFim ' : '')
					+ 	(String.isNotEmpty(sendFrom) && String.isNotEmpty(sendTo) ?
					+	'AND Delivery_Date_Mirror__c >= ' + onlyDate(sendFrom) + ' '
					+ 	'AND Delivery_Date_Mirror__c <= ' + onlyDate(sendTo) + ' ' : '' );
		}
		else{
			query 	+= 	'AND RecordTypeId = \''+ registerType +'\' '
					+ 	(String.isNotBlank(issuerBir) ? 'AND BIR_Issuer_Register__c LIKE \'%' + issuerBir   + '%\'' : '' )
					+ 	(String.isNotBlank(matrixBir) ? 'AND BIR_Matrix_Register__c LIKE \'%' + matrixBir   + '%\'' : '' )
					+ 	(String.isNotBlank(customerIdentification) ? 'AND Customer_Identification_Number__c LIKE \'%' 
					+ 	customerIdentification   + '%\'' : '' )
					+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' );
		}

		query 	+= 'LIMIT 1500';

		System.debug(query);

		return query;
	}

	private String buildQuery() {
		String query 	= 	'SELECT Id,'
						+	'Name, Delivery_Date__c, BIR_NFE_Issuer__c, Integration_Date__c,Invoice_Type__c, Invoice_Data__c, '
						+	'Invoice_Number__c, VIN__c, Customer_Identification_Number__c, Error_Messages__c, '
						+	'Delivery_Date_Mirror__c, Region__c, Sector__c, DDD1__c, Phone_1__c, DDD2__c, Phone_2__c, DDD3__c, '
						+	'Phone_3__c, Status__c, CustomerAccount__r.Email__c, LastModifiedDate, Invoice_Date_Mirror__c '
						+	'FROM FaturaDealer2__c '
						+	'WHERE Id != null ';

		//Melhorias Fatura Dealer 2
		//ITEM 1 - Limitar a pesquisa à notas do tipo "New Vehicle" Validas ou Inválidas.
		if(typeSwitch){
			query 	+= 	'AND RecordTypeId = \''+ invoiceType +'\' '
					+	'AND Invoice_Type__c != \'Cancellation\' '
					+   'AND Invoice_Type__c != \'Devolution\' '
					+	'AND (Invoice_Type__c = \'New Vehicle\' '
					+	'AND isCancelled__c != true '
					+ 	'AND isReturned__c != true '
					+	'AND (Status__c = \'Valid\' OR Status__c = \'Invalid\')) '
					
					+ 	(String.isNotBlank(customerIdentification) ? 'AND Customer_Identification_Number__c LIKE \'%' 
					+ 	customerIdentification   + '%\'' : '' )
					+ 	(String.isNotBlank(issuerBir) ? 'AND BIR_NFE_Issuer__c LIKE \'%' + issuerBir   + '%\'' : '' )
					+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' )
					+	(String.isNotBlank(regionalSelected) ? 'AND Region__c = \''  + regionalSelected  + '\' ' : '' )
					+	(String.isNotBlank(setorSelected) ? 'AND Sector__c = \''  + setorSelected      + '\' ' : '' )
					+ 	(String.isNotEmpty(invoiceFrom) && String.isNotEmpty(invoiceTo) ?
					+	'AND Invoice_Date_Mirror__c >=: dataIni '
					+ 	'AND Invoice_Date_Mirror__c <=: dataFim ' : '')
					+ 	(String.isNotEmpty(sendFrom) && String.isNotEmpty(sendTo) ?
					+	'AND Delivery_Date_Mirror__c >= ' + onlyDate(sendFrom) + ' '
					+ 	'AND Delivery_Date_Mirror__c <= ' + onlyDate(sendTo) + ' ' : '' );
		}
		else{
			query 	+= 	'AND RecordTypeId = \''+ registerType +'\' '
					+ 	(String.isNotBlank(issuerBir) ? 'AND BIR_Issuer_Register__c LIKE \'%' + issuerBir   + '%\'' : '' )
					+ 	(String.isNotBlank(matrixBir) ? 'AND BIR_Matrix_Register__c LIKE \'%' + matrixBir   + '%\'' : '' )
					+ 	(String.isNotBlank(customerIdentification) ? 'AND Customer_Identification_Number__c LIKE \'%' 
					+ 	customerIdentification   + '%\'' : '' )
					+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' );
		}

		query 	+=	'Limit 700';

		System.debug(query);

		return query;
	}

	@testVisible private Date toDate(String data) {
	  final List<String> arrayDate = data.split('/');
	  return Date.valueOf(arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0]);
	}

	@testVisible private String onlyDate(String data) {
		final List<String> arrayDate = data.split('/');
	  return arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0];
	}
}