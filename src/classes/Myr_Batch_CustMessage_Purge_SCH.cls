/** Schedulable Interface to allow the scheduling of the Customer Messages Purge Batch
  @author S. Ducamp
  @date 27.07.2015
  @version 1.0
**/
global class Myr_Batch_CustMessage_Purge_SCH implements Schedulable {
  //Execute the batch
  global void execute(SchedulableContext sc) {
    Myr_Batch_CustMessage_Purge_BAT batch = new Myr_Batch_CustMessage_Purge_BAT();
    Database.executebatch(batch);
  }
}