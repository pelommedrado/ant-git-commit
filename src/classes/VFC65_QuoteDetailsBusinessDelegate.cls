/**
* Classe responsável por fazer o papel de business delegate da página de detalhe do orçamento.
* @author Christian Ranha.
*/
public class VFC65_QuoteDetailsBusinessDelegate 
{
	private static final VFC65_QuoteDetailsBusinessDelegate instance = new VFC65_QuoteDetailsBusinessDelegate();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC65_QuoteDetailsBusinessDelegate()
	{
		
	}
	 
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC65_QuoteDetailsBusinessDelegate getInstance()
	{
		return instance;
	}
	
	public VFC61_QuoteDetailsVO getQuoteDetails(String quoteId, String opportunityId)
	{
		Quote sObjQuote = null;
		VehicleBooking__c sObjVehicleBooking = null;
		VFC61_QuoteDetailsVO quoteDetailsVO = null;
		
		/*obtém a oportunidade pelo id*/
		sObjQuote = VFC81_QuoteBO.getInstance().getQuoteCreatedOrOpenFromOpportunity(quoteId, opportunityId);
		
		/*mapeia os dados do objeto para o QuoteDetailVO*/
		quoteDetailsVO = this.mapToQuoteDetailValueObject(sObjQuote);
		
		/*obtêm os itens de orçamento*/
		quoteDetailsVO.lstQuoteLineItemVO = this.getQuoteLineItems(quoteId);
		
		/*desabilita ação para adicionar veiculo se já existe um veiculo adicionado*/
		quoteDetailsVO.addvehicleDisabled = this.hasAddedVehicle(quoteDetailsVO.lstQuoteLineItemVO);
		
		try
		{
			/*faz a busca pelo vehiclebooking ativo relacionado a esse orçamento (se houver)*/
			sObjVehicleBooking = VFC86_VehicleBookingBO.getInstance().getVehicleBookingActiveByQuote(sObjQuote.Id);
			
			/*armazena no VO principal o id da reserva*/
			quoteDetailsVO.vehicleBookingId = sObjVehicleBooking.Id;
			
			/*sinaliza que já existe reserva para o veículo desse orçamento*/
			quoteDetailsVO.vehicleWasReserved = true;
		}
		catch(VFC89_NoDataFoundException ex)
		{
			/*se entrar aqui indica que não existe nenhuma reserva ativa para esse orçamento, logo, não há nenhuma ação a ser tomada*/
		}
						
		return quoteDetailsVO; 
	}
	
	public List<VFC83_QuoteLineItemVO> getQuoteLineItems(String quoteId)
	{
		List<VFC83_QuoteLineItemVO> lstQuoteLineItem = null;
		List<QuoteLineItem> lstSObjQuoteLineItem = null;
		
		/*obtêm os itens de orçamento pelo id do orçamento e tipo de registro do produto*/
		lstSObjQuoteLineItem = VFC84_QuoteLineItemBO.getInstance().getQuoteLineItem(quoteId, new set<String> { 'PDT_ModelVersion', 'PDT_Accessory', 'PDT_Model' });
		
		/*mapeia os dados do objeto para o QuoteLineItemVO*/
		lstQuoteLineItem = this.mapToQuoteLineItemValueObject(lstSObjQuoteLineItem);
		
		return lstQuoteLineItem;
	}
	
	public String reserveVehicle(VFC61_QuoteDetailsVO quoteDetailsVO)
	{
		VehicleBooking__c sObjVehicleBooking = new VehicleBooking__c();
		
		/*seta os dados no objeto*/	
		sObjVehicleBooking.Quote__c = quoteDetailsVO.id;
		sObjVehicleBooking.Vehicle__c = quoteDetailsVO.idReservedVehicle;
		sObjVehicleBooking.Status__c = 'Active';
		
		/*esse método poderá lançar uma exception, se ocorrer, será tratada pelo controller*/
		VFC86_VehicleBookingBO.getInstance().insertVehicleBooking(sObjVehicleBooking);
		
		return sObjVehicleBooking.Id;
	}
	
	public void deleteQuoteLineItem(VFC61_QuoteDetailsVO quoteDetailsVO, String quoteLineItemId)
	{
		/*exclui item de orçamento*/
		VFC84_QuoteLineItemBO.getInstance().deleteQuoteLineItem(quoteLineItemId);
		
		/*obtêm os itens de orçamento*/
		quoteDetailsVO.lstQuoteLineItemVO = this.getQuoteLineItems(quoteDetailsVO.Id);
		
		/*desabilita ação para adicionar veiculo se já existe um veiculo adicionado*/
		quoteDetailsVO.addvehicleDisabled = this.hasAddedVehicle(quoteDetailsVO.lstQuoteLineItemVO);
	}
	
	public void updateQuoteDetails(VFC61_QuoteDetailsVO quoteDetailsVO)
	{
        System.debug('&+++:' + quoteDetailsVO);
        
		Quote sObjQuote = null;
		List<QuoteLineItem> lstSObjQuoteLineItem = null;
		Opportunity sObjOpportunity = null;
		VehicleBooking__c sObjVehicleBooking = null;
		
		/*mapeia os campos do quoteDetailsVO para o objeto de orçamento*/
		sObjQuote = this.mapToQuoteDetailSObject(quoteDetailsVO);
		System.debug('sObjQuote:' + sObjQuote);
        
		/*mapeia os campos do objQuoteLineItemVO para o objeto de itens do orçamento*/
		lstSObjQuoteLineItem = this.mapToQuoteLineItemSObject(quoteDetailsVO.lstQuoteLineItemVO);
		System.debug('lstSObjQuoteLineItem:' + lstSObjQuoteLineItem);
        
		/*mapeia os campos do VO para o objeto Opportunity*/
		sObjOpportunity = new Opportunity(Id = quoteDetailsVO.opportunityId, SyncedQuoteId = quoteDetailsVO.id);
		System.debug('sObjOpportunity:' + sObjOpportunity);
        
		/*obtém um objeto vehiclebooking*/
        sObjVehicleBooking = this.mapToVehicleBookingSObject(quoteDetailsVO);
        System.debug('sObjVehicleBooking:' + sObjVehicleBooking);
        
        //obtém o save point para controlar a transação
		SavePoint objSavePoint = Database.setSavepoint();
        
		try {
			/*grava informações de orçamento*/
			VFC81_QuoteBO.getInstance().updateQuote(sObjQuote);
		
			/*grava informações dos itens de orçamento*/
			//VFC84_QuoteLineItemBO.getInstance().updateQuoteLineItem(lstSObjQuoteLineItem);
				
			/*sincroniza a oportunidade no objeto de orçamento*/
			VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity);
			
            
            if(sObjVehicleBooking != null) {
                
                User user = [
                    SELECT Id, Name, Contact.CPF__c, Contact.AccountId
                    FROM User
                    WHERE Id =: UserInfo.getUserId()
                ];
                
                Quote qu = [
                    SELECT Id,
                    Opportunity.Account.FirstName,
                    Opportunity.Account.LastName,
                    Opportunity.Account.PersPhone__pc,
                    Opportunity.Account.PersEmailAddress__c,
                    Opportunity.Account.PersMobPhone__c,
                    Opportunity.Account.CustomerIdentificationNbr__c
                  	FROM Quote
                    WHERE Id =: sObjQuote.Id
                ];
                
                sObjVehicleBooking.ClientName__c = qu.Opportunity.Account.FirstName;
                sObjVehicleBooking.ClientSurname__c = qu.Opportunity.Account.LastName;
                sObjVehicleBooking.ClientEmail__c = qu.Opportunity.Account.PersEmailAddress__c;
                sObjVehicleBooking.MobilePhone__c = qu.Opportunity.Account.PersMobPhone__c;
                sObjVehicleBooking.HomePhone__c = qu.Opportunity.Account.PersPhone__pc;
                sObjVehicleBooking.CustomerIdentificationNbr__c = qu.Opportunity.Account.CustomerIdentificationNbr__c;
                
                sObjVehicleBooking.SellerName__c 		= user.Name;
                sObjVehicleBooking.SellerCPF__c 		= user.Contact.CPF__c;
                sObjVehicleBooking.Dealer__c			= user.Contact.AccountId ;
                sObjVehicleBooking.IntegrationStatus__c = 'VEHICLE BOOKING SENT';
                
                if(sObjVehicleBooking.Id == null) {
                    VFC86_VehicleBookingBO.getInstance().insertVehicleBooking(sObjVehicleBooking);
                    
                } else {
                    VFC86_VehicleBookingBO.getInstance().updateVehicleBooking(sObjVehicleBooking);
                }
            }
            
        } catch(Exception ex) {
            System.debug('Ex' + ex);
            
			Database.rollback(objSavePoint);
			
			throw ex;	
		}	
	}
	
	public void updateQuoteItemsDetails(List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO)
	{
		List<QuoteLineItem> lstSObjQuoteLineItem = null;
	
		/*mapeia os campos do objQuoteLineItemVO para o objeto de itens do orçamento*/
		lstSObjQuoteLineItem = this.mapToQuoteLineItem(lstQuoteLineItemVO);
	
		try
		{
			/*grava informações dos itens de orçamento*/
			VFC84_QuoteLineItemBO.getInstance().updateQuoteLineItem(lstSObjQuoteLineItem);
		}
		catch(Exception ex)
		{
			throw ex;	
		}
	}
	
	private List<QuoteLineItem> mapToQuoteLineItem(List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO)
	{
		List<QuoteLineItem> lstSObjQuoteLineItem = new List<QuoteLineItem>(); 
		QuoteLineItem sObjQuoteLineItem = null;
		
		for (VFC83_QuoteLineItemVO objQuoteLineItemVO : lstQuoteLineItemVO)
		{
			sObjQuoteLineItem = new QuoteLineItem(Id=objQuoteLineItemVO.Id);
			sObjQuoteLineItem.UnitPrice = VFC69_Utility.priceFormatToDouble(objQuoteLineItemVO.unitPriceMask);
			
			lstSObjQuoteLineItem.add(sObjQuoteLineItem);
		}
		
		return lstSObjQuoteLineItem;
	}		
	
	private VFC61_QuoteDetailsVO mapToQuoteDetailValueObject(Quote sObjQuote)
	{
		VFC61_QuoteDetailsVO quoteDetailsVO = new VFC61_QuoteDetailsVO();
		
		/*mapeia os campos do objeto opportunity para os campos do VO*/
		/*atributos usados para controle interno, não são utilizados na tela*/
		quoteDetailsVO.id = sObjQuote.Id;
		quoteDetailsVO.opportunityId = sObjQuote.OpportunityId;
		quoteDetailsVO.oppStatus = sObjQuote.Opportunity.StageName;
		
		/*atributos referentes a seção informações do Orçamento*/
		quoteDetailsVO.quoteName = sObjQuote.Name;
		quoteDetailsVO.quoteNumber = sObjQuote.QuoteNumber;
		quoteDetailsVO.createdDate = sObjQuote.CreatedDate; 
		quoteDetailsVO.status = sObjQuote.Status; 
		quoteDetailsVO.sObjQuote.ExpirationDate = sObjQuote.ExpirationDate; 
		
		/*atributos referentes a seção cliente*/
		quoteDetailsVO.customerName = sObjQuote.Opportunity.Account.Name; 
		quoteDetailsVO.customerPersonEmail = sObjQuote.Opportunity.Account.PersonEmail;
		quoteDetailsVO.customerPersLandline = sObjQuote.Opportunity.Account.PersLandline__c;
		quoteDetailsVO.customerPersMobPhone = sObjQuote.Opportunity.Account.PersMobPhone__c;
		
		/*atributos referentes a seção vendedor*/
		quoteDetailsVO.sallerName = sObjQuote.Opportunity.Owner.Name;
		quoteDetailsVO.sallerEmail = sObjQuote.Opportunity.Owner.Email;
		quoteDetailsVO.sallerPhone = sObjQuote.Opportunity.Owner.Phone;
		
		/*atributos referentes a seção sintese*/
        quoteDetailsVO.signalValueMask = String.valueOf(sObjQuote.SignalValue__c);
		quoteDetailsVO.entryMask = String.valueOf(sObjQuote.Entry__c); 
		quoteDetailsVO.amountFinancedMask = String.valueOf(sObjQuote.AmountFinanced__c);
		quoteDetailsVO.sObjQuote.UsedVehicle__c = sObjQuote.UsedVehicle__c;
		if(sObjQuote.YearUsedVehicle__c == 0.0){
			quoteDetailsVO.yearUsedVehicle = '';
		}else{
			quoteDetailsVO.yearUsedVehicle = String.valueOf(sObjQuote.YearUsedVehicle__c);
		}
		quoteDetailsVO.priceUsedVehicleMask = String.valueOf(sObjQuote.PriceUsedVehicle__c);
		quoteDetailsVO.cdcCFinancing = sObjQuote.CDCFinancing__c;
		quoteDetailsVO.lsgFinancing = sObjQuote.LSGFinancing__c;
		if(sObjQuote.NumberOfInstallments__c == 0.0){
			quoteDetailsVO.numberOfParcels = '';
		}else{
			quoteDetailsVO.numberOfParcels = String.valueOf(sObjQuote.NumberOfInstallments__c);
		} 
		quoteDetailsVO.valueOfParcelMask = String.valueOf(sObjQuote.ValueOfInstallments__c);
		quoteDetailsVO.paymentMethods = sObjQuote.PaymentMethods__c;
		quoteDetailsVO.paymentDetails = sObjQuote.PaymentDetails__c;
		quoteDetailsVO.usedVehicleLicensePlate = sObjQuote.UsedVehicleLicensePlate__c;
		quoteDetailsVO.usedVehicleBrand = sObjQuote.UsedVehicle__r.Brand__c;
		quoteDetailsVO.usedVehicleModel = sObjQuote.UsedVehicle__r.Model__c;
		quoteDetailsVO.signalValueCheck = sObjQuote.SignalValueCk__c;
        
		/*atributos referentes a seção documentos básicos necessários - pessoa física*/
		quoteDetailsVO.identityCard = sObjQuote.IdentityCard__c;
		quoteDetailsVO.cpf = sObjQuote.CPF_BR__c;
		quoteDetailsVO.proofIncome = sObjQuote.ProofIncome__c;
		quoteDetailsVO.proofResidence = sObjQuote.ProofResidence__c;
		quoteDetailsVO.lastStatementIncomeTax = sObjQuote.LastStatementIncomeTax__c;
		quoteDetailsVO.lastPaycheckStatement = sObjQuote.LastPaycheckStatement__c;
		quoteDetailsVO.lastStatementBank = sObjQuote.LastStatementBank__c;
		quoteDetailsVO.driversLicense = sObjQuote.DriversLicense__c;
		
		/*atributos referentes a seção documentos básicos necessários - empresa*/
		quoteDetailsVO.socialContract = sObjQuote.SocialContract__c;
		quoteDetailsVO.lastStatementIncomeTaxEnterprise = sObjQuote.LastStatementIncomeTaxEnterprise__c;
		quoteDetailsVO.identityCardAllMembers = sObjQuote.IdentityCardAllMembers__c;
		quoteDetailsVO.cpfAllMembers = sObjQuote.CPFAllMembers__c;
		quoteDetailsVO.registrationFormSignedPartners = sObjQuote.RegistrationFormSignedPartners__c;
		quoteDetailsVO.lastBalanceSheet = sObjQuote.LastBalanceSheet__c;
		quoteDetailsVO.balanceLastTwoYears = sObjQuote.BalanceLastTwoYears__c;
		quoteDetailsVO.relationshipFleetDebt = sObjQuote.RelationshipFleetDebt__c;
		quoteDetailsVO.totalPriceMask = VFC69_Utility.priceFormats(sObjQuote.TotalPrice);
		quoteDetailsVO.sObjQuote = sObjQuote;
		
		/*atributos referentes a seção documentos básicos necessários - empresa*/
		quoteDetailsVO.dut = sObjQuote.DUT__c;
		quoteDetailsVO.procuration = sObjQuote.Procuration__c;
		quoteDetailsVO.inspection = sObjQuote.Inspection__c;
		quoteDetailsVO.finesPaid = sObjQuote.FinesPaid__c;
		quoteDetailsVO.publicProcuration = sObjQuote.PublicProcuration__c;
		quoteDetailsVO.licensing = sObjQuote.Licensing__c;
		quoteDetailsVO.leaseTermination = sObjQuote.LeaseTermination__c;
		quoteDetailsVO.ManualSpareKey = sObjQuote.ManualAndSpareKey__c;
        
        //atributos referentes a seção de acessórios
        quoteDetailsVO.accessory1 = sObjQuote.Accessory1__c;
        quoteDetailsVO.accePrice1 = VFC69_Utility.priceFormats(sObjQuote.Accessory1Price__c);
        quoteDetailsVO.accessory2 = sObjQuote.Accessory2__c;
        quoteDetailsVO.accePrice2 = VFC69_Utility.priceFormats(sObjQuote.Accessory2Price__c);
        quoteDetailsVO.accessory3 = sObjQuote.Accessory3__c;
        quoteDetailsVO.accePrice3 = VFC69_Utility.priceFormats(sObjQuote.Accessory3Price__c);
				
		return quoteDetailsVO;	
	}
	
	private List<VFC83_QuoteLineItemVO> mapToQuoteLineItemValueObject(List<QuoteLineItem> lstSObjQuoteLineItem)
	{
		List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO = new List<VFC83_QuoteLineItemVO>();  
		VFC83_QuoteLineItemVO objQuoteLineItemVO = null;
		
		for (QuoteLineItem sObjQuoteLineItem : lstSObjQuoteLineItem)
		{
			objQuoteLineItemVO = new VFC83_QuoteLineItemVO();
			objQuoteLineItemVO.id = sObjQuoteLineItem.Id; 
			objQuoteLineItemVO.item = sObjQuoteLineItem.PricebookEntry.Product2.Name;
			objQuoteLineItemVO.vehicleName = sObjQuoteLineItem.Vehicle__r.Name;
			objQuoteLineItemVO.vehicleId = sObjQuoteLineItem.Vehicle__r.Id;	
			objQuoteLineItemVO.isReserved = isReserved(sObjQuoteLineItem);//(sObjQuoteLineItem.Vehicle__r.Status__c == 'Reserved');	
			objQuoteLineItemVO.unitPrice = sObjQuoteLineItem.UnitPrice;
			objQuoteLineItemVO.unitPriceMask = String.valueOf(sObjQuoteLineItem.UnitPrice);
			objQuoteLineItemVO.description = sObjQuoteLineItem.Description;
			lstQuoteLineItemVO.add(objQuoteLineItemVO); 		
		}	
			
		return lstQuoteLineItemVO;
	}
	
	private Boolean isReserved(QuoteLineItem sObjQuoteLineItem){
		
		if(sObjQuoteLineItem.Vehicle__r != null){
			Set<String> setVehicleId = new Set<String>();
		
			setVehicleId.add(sObjQuoteLineItem.Vehicle__r.Id);
		
			List<VehicleBooking__c> vbk = VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(setVehicleId, 'Active');
		
			if(vbk == null || vbk.size() == 0){
				return false;
			}else{
				if(sObjQuoteLineItem.QuoteId == vbk.get(0).Quote__c){
					return true;
				}	
			}
		}
		
		return false;
	}
	
	private Quote mapToQuoteDetailSObject(VFC61_QuoteDetailsVO quoteDetailsVO) 
	{
		Quote sObjQuote = new Quote(Id=quoteDetailsVO.Id);
		
		/*atributos referentes a seção informações do Orçamento*/
		sObjQuote.ExpirationDate = quoteDetailsVO.sObjQuote.ExpirationDate;	
		
		sObjQuote.Status = quoteDetailsVO.status;	
		
		/*atributos referentes a seção sintese*/
        System.debug('quoteDetailsVO.entryMask: ' + quoteDetailsVO.entryMask);
        System.debug('quoteDetailsVO.entryMask: ' + VFC69_Utility.priceFormatToDouble(quoteDetailsVO.entryMask));
		
        sObjQuote.SignalValue__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.signalValueMask);
        sObjQuote.SignalValueCk__c = sObjQuote.SignalValue__c != 0;
        sObjQuote.Entry__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.entryMask); 
		sObjQuote.AmountFinanced__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.amountFinancedMask); 
		sObjQuote.UsedVehicle__c = quoteDetailsVO.sObjQuote.UsedVehicle__c; 
		if(quoteDetailsVO.yearUsedVehicle == '' || quoteDetailsVO.yearUsedVehicle == null) {
			sObjQuote.YearUsedVehicle__c = 0;
		}else{
			sObjQuote.YearUsedVehicle__c = Double.valueOf(quoteDetailsVO.yearUsedVehicle);  
		}		
		sObjQuote.PriceUsedVehicle__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.priceUsedVehicleMask); 
		sObjQuote.CDCFinancing__c = quoteDetailsVO.cdcCFinancing; 
		sObjQuote.LSGFinancing__c = quoteDetailsVO.lsgFinancing;
		if(quoteDetailsVO.numberOfParcels == '' || quoteDetailsVO.numberOfParcels == null) {
			sObjQuote.NumberOfInstallments__c = 0;
		}else{
			sObjQuote.NumberOfInstallments__c = Double.valueOf(quoteDetailsVO.numberOfParcels);
		} 
		sObjQuote.ValueOfInstallments__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.valueOfParcelMask); 
		sObjQuote.PaymentMethods__c = quoteDetailsVO.paymentMethods; 
		sObjQuote.PaymentDetails__c = quoteDetailsVO.paymentDetails; 
		sObjQuote.UsedVehicleLicensePlate__c = quoteDetailsVO.usedVehicleLicensePlate; 
		
		/*atributos referentes a seção documentos básicos necessários - pessoa física*/
		sObjQuote.IdentityCard__c = quoteDetailsVO.identityCard;
		sObjQuote.CPF_BR__c = quoteDetailsVO.cpf;
		sObjQuote.ProofIncome__c = quoteDetailsVO.proofIncome;
		sObjQuote.ProofResidence__c = quoteDetailsVO.proofResidence;
		sObjQuote.LastStatementIncomeTax__c = quoteDetailsVO.lastStatementIncomeTax;
		sObjQuote.LastPaycheckStatement__c = quoteDetailsVO.lastPaycheckStatement;
		sObjQuote.LastStatementBank__c = quoteDetailsVO.lastStatementBank;
		sObjQuote.DriversLicense__c = quoteDetailsVO.driversLicense;
		
		/*atributos referentes a seção documentos básicos necessários - empresa*/
		sObjQuote.SocialContract__c = quoteDetailsVO.socialContract;
		sObjQuote.LastStatementIncomeTaxEnterprise__c = quoteDetailsVO.lastStatementIncomeTaxEnterprise;
		sObjQuote.IdentityCardAllMembers__c = quoteDetailsVO.identityCardAllMembers;
		sObjQuote.CPFAllMembers__c = quoteDetailsVO.cpfAllMembers;
		sObjQuote.RegistrationFormSignedPartners__c = quoteDetailsVO.registrationFormSignedPartners;
		sObjQuote.LastBalanceSheet__c = quoteDetailsVO.lastBalanceSheet;
		sObjQuote.BalanceLastTwoYears__c = quoteDetailsVO.balanceLastTwoYears;
		sObjQuote.RelationshipFleetDebt__c = quoteDetailsVO.relationshipFleetDebt;
		
		
		/*atributos referentes a seção documentos básicos necessários - empresa*/
		sObjQuote.DUT__c = quoteDetailsVO.dut;
		sObjQuote.Procuration__c = quoteDetailsVO.procuration;
		sObjQuote.Inspection__c = quoteDetailsVO.inspection;
		sObjQuote.FinesPaid__c = quoteDetailsVO.finesPaid;
		sObjQuote.PublicProcuration__c = quoteDetailsVO.publicProcuration;
		sObjQuote.Licensing__c = quoteDetailsVO.licensing;
		sObjQuote.LeaseTermination__c = quoteDetailsVO.leaseTermination;
		sObjQuote.ManualAndSpareKey__c = quoteDetailsVO.ManualSpareKey;
        
        //atributos referente a seção de acessórios
        sObjQuote.Accessory1__c = quoteDetailsVO.accessory1;
        sObjQuote.Accessory1Price__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice1);
        sObjQuote.Accessory2__c = quoteDetailsVO.accessory2;
        sObjQuote.Accessory2Price__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice2);
        sObjQuote.Accessory3__c = quoteDetailsVO.accessory3;
        sObjQuote.Accessory3Price__c = VFC69_Utility.priceFormatToDouble(quoteDetailsVO.accePrice3);
		
        System.debug('sObjQuote: ' + sObjQuote);
         System.debug('quoteDetailsVO: ' + quoteDetailsVO);
		return sObjQuote;	
	}
	
	private List<QuoteLineItem> mapToQuoteLineItemSObject(List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO)
	{
		List<QuoteLineItem> lstSObjQuoteLineItem = new List<QuoteLineItem>(); 
		QuoteLineItem sObjQuoteLineItem = null;
		
		for (VFC83_QuoteLineItemVO objQuoteLineItemVO : lstQuoteLineItemVO)
		{
			sObjQuoteLineItem = new QuoteLineItem(Id=objQuoteLineItemVO.Id);
			sObjQuoteLineItem.UnitPrice = VFC69_Utility.priceFormatToDouble(objQuoteLineItemVO.unitPriceMask);
			
			lstSObjQuoteLineItem.add(sObjQuoteLineItem);
		}
		
		return lstSObjQuoteLineItem;
	}
	
	private VehicleBooking__c mapToVehicleBookingSObject(VFC61_QuoteDetailsVO quoteDetailsVO)
	{
		VFC83_QuoteLineItemVO quoteLineItemVOAux = null;
		VehicleBooking__c sObjVehicleBooking = null;
		
		/*percorre a lista para procurar o item da cotação que é um veículo*/
        for(VFC83_QuoteLineItemVO quoteLineItemVO : quoteDetailsVO.lstQuoteLineItemVO)
        {
        	/*se o id do veículo estiver preenchido, indica que o item foi encontrado*/
            if(String.isNotEmpty(quoteLineItemVO.vehicleId))
            {
            	quoteLineItemVOAux = quoteLineItemVO;
                                
                break;
       		}
        }
        
        if(quoteLineItemVOAux != null)
        {
        	// Efetuar uma consulta no VBK e ver como esta antes de verificar
        	System.debug('###### -> quoteLineItemVOAux ->'+quoteLineItemVOAux);
        	System.debug('###### -> vehicleWasReserved ->'+quoteDetailsVO);
        	if(quoteDetailsVO.vehicleWasReserved)
        	{
        		sObjVehicleBooking = new VehicleBooking__c(Id = quoteDetailsVO.vehicleBookingId);  
        		if(quoteDetailsVO.status != 'Canceled')       	
        			sObjVehicleBooking.Status__c = quoteLineItemVOAux.isReserved ? 'Active' : 'Canceled';
        	}
        	else
        	{
        		sObjVehicleBooking = new VehicleBooking__c();	
        		sObjVehicleBooking.Quote__c = quoteDetailsVO.id;
				sObjVehicleBooking.Vehicle__c = quoteLineItemVOAux.vehicleId;
        	}	
        }
        
        return sObjVehicleBooking;
	}
	
	private Boolean hasAddedVehicle(List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO)
	{
		Boolean hasAddedVehicle = false;
		
		for (VFC83_QuoteLineItemVO objQuoteLineItemVO : lstQuoteLineItemVO)
		{
			if (objQuoteLineItemVO.item != 'Acessório')
			{
				hasAddedVehicle = true;
				break;			
			}			
		}
		 
		return hasAddedVehicle;  
	}
}