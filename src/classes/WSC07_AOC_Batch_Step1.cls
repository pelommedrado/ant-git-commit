/**
*   WebService Class    -   WSC07_AOC_Batch_Step1
*   Author              -   Suresh Babu
*   Date                -   08/03/2013
*   
*   #01 <Suresh Babu> <08/03/2013>
*       This AOC Batch class to fetch every values from Webservice and do the callouts and save the values in SFDC.
**/

global class WSC07_AOC_Batch_Step1 implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    public List<Wrapper_Model_DOCS> lstWrapperModel = new List<Wrapper_Model_DOCS>();
    public List<CS_Models__c> lstCSModels = new List<CS_Models__c>();
    
    public List<Wrapper_Model> lstModelRecords = new List<Wrapper_Model>();
    
    public List<Wrapper_presentationGroup> lst_presentaionGroup = new List<Wrapper_presentationGroup>();
    public List<Wrapper_versionsPresentation> lstVersionRecords = new List<Wrapper_versionsPresentation>();
    public List<Wrapper_PresentationItem> lstColorPresentation = new List<Wrapper_PresentationItem>();
    public List<Product2> lstProduct_Models = new List<Product2>();
    public Set<Product2> set_Product_Versions = new Set<Product2>();
    public Map<String, List<Wrapper_model_Items>> map_WrapperModelItems = new Map<String, List<Wrapper_model_Items>>();
    
    public Map<String , Product2> map_ProductModel_with_ModelSpec = new Map<String, Product2>();
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.Debug('*** AOC_Batch_Step1.start()');

        // Delete OLD model and NEW Models from JSON..
        RetrieveModels();
        
        // Build a map with all models using the prod.ModelSpecCode__c
        map_ProductModel_with_ModelSpec = VFC22_ProductDAO.getInstance().fetchAll_Model_Products();
        
        String query = 'SELECT Name, Id, Document_URL__c FROM CS_Models__c WHERE Name != null';
        return Database.getQueryLocator(query);
    }
    
    public void RetrieveModels(){
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Accept', 'application/json');
        
        String endpoint = 'http://br.co.rplug.renault.com/docs';
        req.setEndpoint(endpoint);
        
        Http http = new Http();
        String resBody = '';
        if (!Test.isRunningTest())
        {
            HttpResponse res = http.send(req);
            resBody = res.getBody();
        }
        system.debug('****** resBody=' + resBody);
        
        Parse_RetrievedModel(resBody);
    }
    
    public void Parse_RetrievedModel( String JSONModel ){
        // Parse entire JSON response.
        JSONParser parser = JSON.createParser(JSONModel);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        Wrapper_Model_DOCS model = (Wrapper_Model_DOCS)parser.readValueAs(Wrapper_Model_DOCS.class);
                        
                        lstWrapperModel.add( model );
                        
                        parser.skipChildren();
                    }
                }
            }
        }
        
        for( Wrapper_Model_DOCS models : lstWrapperModel ){
            CS_Models__c ModelCustomSetting = new CS_Models__c();
            ModelCustomSetting.Document_URL__c = models.doc;

            Wrapper_SpecCode spec = models.docId;
            if (spec != null) {
                ModelCustomSetting.Name = spec.modelSpecCode;
                lstCSModels.add( ModelCustomSetting );
            }
        }
        
        if( lstCSModels.size() > 0 ){
            // Delete Previous records.. 
            List<Id> lstCSModelId = new List<Id>();
            lstCSModelId = VFC62_CS_Models_DAO.getInstance().fetch_All_Models();
            
            if( lstCSModelId != null )
                VFC62_CS_Models_DAO.getInstance().deleteData( lstCSModelId );
                
            VFC62_CS_Models_DAO.getInstance().insertData( lstCSModels );
        }
    }
    
    // Wrapper classes used to get MODELS from JSON..
    
    public class Wrapper_Model_DOCS{
        public String doc {get;set;}
        Wrapper_SpecCode docId;
        
        public Wrapper_Model_DOCS(String docURL, Wrapper_SpecCode spec ){
            doc = docURL;
            docId = spec;
        }
    }
    
    public class Wrapper_SpecCode{
        public String modelSpecCode {get;set;}
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.Debug('*** AOC_Batch_Step1.execute()');

        for( sObject modelRecord : scope ){
            CS_Models__c model = (CS_Models__c)modelRecord;
            system.Debug( ' model -->'+model);
            
            String docURL = model.Document_URL__c;
            String persistURL;
            String categoryURL;
            String priseListURL;
            String urlCode;
            
            if( docURL.contains( 'http://br.co.rplug.renault.com/doc/' )){
                urlCode = docURL.replace('http://br.co.rplug.renault.com/doc/', '');
                system.Debug(' URL Code ->'+urlCode);
                
                if( docURL.contains('/doc/') ){
                    persistURL = docURL.replace( '/doc/', '/pres/');
                    categoryURL = docURL.replace( '/doc/', '/category/');
                    priseListURL = docURL.replace( '/doc/', '/pricelist/');
                    
                    system.Debug( ' persistURL ->'+persistURL);
                    system.Debug( ' categoryURL ->'+categoryURL);
                    system.Debug( ' priseListURL ->'+priseListURL);
                    
                    connectToWS( persistURL, 'pres', model.Name, urlCode );
                }
            }
        }
    }
    
    global void connectToWS(String uri, String AOCClient, String key, String urlCode ){
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(uri);
        
        Http http = new Http();
        HttpResponse res = http.send(req);
        String response = res.getBody();
        
        if( AOCClient == 'pres' ){
            parsePres( response, key, urlCode );
        }
    }
    
    public void parsePres( String presJSON, String keyValue, String URLCode ){
        // Clear the Version Map..
        map_WrapperModelItems.clear();
        
        System.JSONParser parser = JSON.createParser(presJSON);
        while (parser.nextToken() != null) {
            if( parser.getCurrentName() == 'map' ){
                Boolean isModelComplete = false;
                while( isModelComplete == false && parser.nextToken() != null ){
                    if( parser.getCurrentName() == 'colors'){
                        isModelComplete = true;
                    }
                    if( parser.getCurrentName() == keyValue ){
                        parser.nextToken();
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            Wrapper_Model model = (Wrapper_Model)parser.readValueAs(Wrapper_Model.class);
                            model.modelURL = URLCode;
                            system.Debug(' model ------>'+model);
                            lstModelRecords.add( model );
                        }
                    }
                }
            }
            
            // Get Colors of Models..
            if( parser.getCurrentName() == 'colors'){
                
                while( parser.nextToken() != JSONToken.END_ARRAY ){
                    if( parser.getCurrentName() == 'presentationItem' ){
                        parser.nextToken();
                        if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
                            Wrapper_PresentationItem colorPresentation = (Wrapper_PresentationItem)parser.readValueAs(Wrapper_PresentationItem.class);
                            colorPresentation.modelKey = keyValue;
                            system.Debug( 'colorPresentation -->'+colorPresentation);
                            lstColorPresentation.add( colorPresentation );
                        }
                    }
                }
            }
            
            
            //Get Versions of Model..
            if( parser.getCurrentName() == 'versionsPresentation' ){
                /**
                while( parser.nextToken() != null && parser.getCurrentName() != 'equipementsPresentation' ){
                    if( parser.getCurrentName() == 'presentationGroup' ){
                        parser.nextToken();
                        /**
                        // This if condition to get values for Stepway model versions..!!
                        
                        if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
                            Wrapper_presentationGroup presentationGroup = (Wrapper_presentationGroup)parser.readValueAs(Wrapper_presentationGroup.class);
                            presentationGroup.model_Code = keyValue;
                            lst_presentaionGroup.add( presentationGroup );
                            system.Debug(' presentationGroup -->'+presentationGroup);
                        }
                        
                        if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
                            Wrapper_versionsPresentation versionPresentation = (Wrapper_versionsPresentation)parser.readValueAs(Wrapper_versionsPresentation.class);
                            versionPresentation.model_Code = keyValue;
                            lstVersionRecords.add( versionPresentation );
                            system.Debug(' versionPresentation -->'+versionPresentation);
                        }
                    }
                }
                */
                
                while( parser.nextToken() != null && parser.getCurrentName() != 'equipementsPresentation' ){
                    
                    if( parser.getCurrentName() == 'presentationGroup' ){
                        parser.nextToken();
                        
                        if( keyValue == 'B9M' || keyValue == 'L9M' || keyValue == 'CBM' || keyValue == 'KEM' ){
                            if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
                                Wrapper_presentationGroup presentationGroup = (Wrapper_presentationGroup)parser.readValueAs(Wrapper_presentationGroup.class);
                                presentationGroup.model_Code = keyValue;
                                
                                Wrapper_model_Items model_Item = new Wrapper_model_Items( presentationGroup );
                                
                                List<Wrapper_model_Items> temp = new List<Wrapper_model_Items>();
                                if( map_WrapperModelItems.containsKey( keyValue )){
                                    temp = map_WrapperModelItems.get( keyValue );
                                    temp.add( model_Item );
                                    map_WrapperModelItems.put( keyValue, temp );
                                }
                                else{
                                    temp.add( model_Item );
                                    map_WrapperModelItems.put( keyValue, temp );
                                }
                            }
                        }
                        else{
                            if( parser.getCurrentToken() == JSONToken.START_OBJECT ){
                                Wrapper_versionsPresentation versionPresentation = (Wrapper_versionsPresentation)parser.readValueAs(Wrapper_versionsPresentation.class);
                                versionPresentation.model_Code = keyValue;
                                lstVersionRecords.add( versionPresentation );
                                system.Debug(' versionPresentation -->'+versionPresentation);
                            }
                        }
                    }
                }
            }
            
            if( keyValue == 'B9M' || keyValue == 'L9M' || keyValue == 'CBM' || keyValue == 'KEM'){
                if( parser.getCurrentName() == 'equipementsPresentation' ){
                    Wrapper_Model_Label m_label = new Wrapper_Model_Label();
                    if ( map_ProductModel_with_ModelSpec.containsKey( keyValue ) ){
                        m_label.pt = map_ProductModel_with_ModelSpec.get( keyValue ).Name;
                    }
                    
                    Wrapper_versionsPresentation versionPresentation = new Wrapper_versionsPresentation( map_WrapperModelItems.get( keyValue ), '', m_label );
                    versionPresentation.model_Code = keyValue;
                    lstVersionRecords.add( versionPresentation );
                    system.Debug(' versionPresentation -->'+versionPresentation);
                }
            }
            
        }
    }
    
    // To get Color informations..
    
    public class Wrapper_PresentationItem{
        public String modelKey {get;set;}
        public String key {get;set;}
        Wrapper_ColorLabel label;
        public Wrapper_PresentationItem( String wrap_key, Wrapper_ColorLabel wrap_label ){
            key = wrap_key;
            label = wrap_label;
        }
    }
    
    public class Wrapper_ColorLabel{
        public String pt {get;set;}
    }
    
    // To get Version of Products..
    
    public class Wrapper_versionsPresentation{
        public String model_Code {get;set;}
        public List<Wrapper_model_Items> items;
        public String key {get;set;}
        public Wrapper_Model_Label label;
        
        public Wrapper_versionsPresentation(List<Wrapper_model_Items> wrap_items, String wrap_key, Wrapper_Model_Label wrap_label ){
            items = wrap_items.clone();
            key = wrap_key;
            label = wrap_label;
        }
    }
    
    public class Wrapper_model_Items{
        public Wrapper_presentationGroup presentationGroup;
        
        public Wrapper_model_Items(Wrapper_presentationGroup wrap_presentationGroup){
            presentationGroup = wrap_presentationGroup;
        }
    }
    
    public class Wrapper_Model_Label{
        public String pt {get;set;}
    }
    
    public class Wrapper_presentationGroup{
        public String model_Code {get;set;} // Testing may use..?!
        public String key {get;set;}
        public Wrapper_presentationGroup_Label label;
        public List<Wrapper_items> items;
        public Wrapper_presentationGroup( List<Wrapper_items> wrap_items, Wrapper_presentationGroup_Label wrap_label, String wrap_key){
            items = wrap_items.clone();
            label = wrap_label;
            key = wrap_key;
        }
    }
    
    public class Wrapper_items{
        public Wrapper_versionItem versionItem;
        
        public Wrapper_items( Wrapper_versionItem wrap_versionItem ){
            versionItem = wrap_versionItem;
        }
    }
    
    public class Wrapper_presentationGroup_Label{
        public String pt {get;set;}
    }
    
    public class Wrapper_versionItem{
        public String versionIdSpecCode {get;set;}
        public String key {get;set;}
        public Wrapper_Version_Label label;
        
        public Wrapper_versionItem( Wrapper_Version_Label wrap_label, String spec, String wrap_key ){
            versionIdSpecCode = spec;
            key = wrap_key;
            label = wrap_label;
        }
    }
    
    public class Wrapper_Version_Label{
        public String pt {get;set;}
    }
    
    
    public void Test_WrapperValues(){
        Wrapper_Version_Label v_label = new Wrapper_Version_Label();
        v_label.pt = 'GT 2.0 Turbo';
        
        Wrapper_versionItem v_verItem = new Wrapper_versionItem( v_label, 'VEC055_BRES', 'VEC055_BRES' );
        
        Wrapper_presentationGroup_Label v_prGrp = new Wrapper_presentationGroup_Label();
        v_prGrp.pt = 'Fluence GT 2012/2013';
        
        Wrapper_items v_item = new Wrapper_items(v_verItem);
        List<Wrapper_items> lst_v_item = new List<Wrapper_items>();
        lst_v_item.add( v_item );
        
        Wrapper_presentationGroup m_prGrp = new Wrapper_presentationGroup( lst_v_item, v_prGrp, 'ENS_163' );
        //List<Wrapper_presentationGroup> lst_m_presGrp = new List<Wrapper_presentationGroup>();
        //lst_m_presGrp.add( m_prGrp );
        
        Wrapper_Model_Label m_label = new Wrapper_Model_Label();
        m_label.pt = 'Fluence';
        
        
        Wrapper_model_Items m_items = new Wrapper_model_Items( m_prGrp );
        List<Wrapper_model_Items> lst_m_Items = new List<Wrapper_model_Items>();
        lst_m_Items.add( m_items );
        
        Wrapper_versionsPresentation M_V_Pres = new Wrapper_versionsPresentation( lst_m_Items, 'ENS_162', m_label );
        
        
        system.Debug(' M_V_Pres ---> '+M_V_Pres);
    }
    
    
    
    public class Wrapper_Model{
        public String modelURL {get;set;}
        public Wrapper_Presentation presentationItem;
        
        public Wrapper_Model( Wrapper_Presentation pres, String wrap_modelURL ){
            presentationItem = pres;
            modelURL = wrap_modelURL;
        }
    }
    
    public class Wrapper_Presentation{
        public String key {get;set;}    
        public Wrapper_Label label;
        
        public Wrapper_Presentation( Wrapper_Label wrapLabel, String wrap_key ){
            label = wrapLabel;
            key = wrap_key;
        }
    }
    
    public class Wrapper_Label{
        public String pt {get;set;}
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.Debug('*** AOC_Batch_Step1.finish()');   

        //
        // Process the Models
        //
        
        List<Product2> lst_Product_Model_Upsert = new List<Product2>();
        List<Product2> lst_Product_Version_Upsert = new List<Product2>();
        
        Set<Product2> set_Version_Product = new Set<Product2>();
        
        Id model_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'PDT_Model' );
        Id version_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'PDT_ModelVersion' );
        
        
        // Create a list with all models fetched in the execute()
        for( Wrapper_Model models : lstModelRecords ){
            
            Product2 model_Product = new Product2();
            Wrapper_Presentation pres = models.presentationItem;
            Wrapper_Label label = pres.label;
            
            model_Product.ModelUrlCode__c = models.modelURL;
            model_Product.Name = label.pt;
            model_Product.ModelSpecCode__c = pres.key;
            model_Product.RecordTypeId = model_RecordTypeId;
            model_Product.IsActive = true;
            
            lstProduct_Models.add( model_Product );
        }
        
        system.Debug(' lstProduct_Models -->'+lstProduct_Models);
        
        // For each model in the previews list
        for( Product2 product_Model : lstProduct_Models ){
            if ( map_ProductModel_with_ModelSpec.containsKey( product_Model.ModelSpecCode__c ) ){
                
                // Do the update
                Product2 update_Product = map_ProductModel_with_ModelSpec.get( product_Model.ModelSpecCode__c );
                
                update_Product.ModelUrlCode__c = product_Model.ModelUrlCode__c;
                update_Product.Name = product_Model.Name;
                update_Product.ModelSpecCode__c = product_Model.ModelSpecCode__c;
                update_Product.RecordTypeId = model_RecordTypeId;
                update_Product.IsActive = true;
                
                // Add it to the list to update
                lst_Product_Model_Upsert.add( update_Product );
                
                // Remove it from the map
                map_ProductModel_with_ModelSpec.remove( product_Model.ModelSpecCode__c );
            }
            else{
                lst_Product_Model_Upsert.add( product_Model );
            }
        }
        
        List<Product2> lst_All_Models = new List<Product2>();
        lst_All_Models = map_ProductModel_with_ModelSpec.values();
        system.Debug( 'lst_All_Models  ----->'+lst_All_Models);
        
        /*
        // Suresh - Commented as mentioned by Renault
        // All remaining models in map should be disabled
        for( Product2 disabled_Models : lst_All_Models ){
            disabled_Models.IsActive = false;
            lst_Product_Model_Upsert.add( disabled_Models );
        }
        */
        // Here we need to Upsert the PRODUCT MODEL records..
        
        // Remove possible duplicates?
        Set<Product2> set_Model_Product = new Set<Product2>();
        set_Model_Product.addAll( lst_Product_Model_Upsert );
        lst_Product_Model_Upsert.clear();
        lst_Product_Model_Upsert.addAll( set_Model_Product );

        system.Debug(' lst_Product_Model_Upsert -->'+lst_Product_Model_Upsert);
        system.Debug( ' Model size -->'+lst_Product_Model_Upsert.size());

        // Upsert the models
        VFC22_ProductDAO.getInstance().upsertData( lst_Product_Model_Upsert );
        
        // Build a map with all models using the key=Id
        Map<Id, Product2> map_Model_Id = new Map<Id, Product2>();
        map_Model_Id = VFC22_ProductDAO.getInstance().fetchAll_Model_Id_In_Map();
        
        // Build a map with all models using the key=ModelSpecCode__c
        Map<String, Product2> map_Product_Model_AfterSaved = new Map<String, Product2>();
        map_Product_Model_AfterSaved = VFC22_ProductDAO.getInstance().fetchAll_Model_Products();

        //
        // Process the Versions
        //

        for( Wrapper_versionsPresentation version : lstVersionRecords ){
            
            // Hold the current model Id
            Id Product_Model_ID;
            system.Debug(' version.model_Code  ---->'+ version.model_Code );

            if( map_Product_Model_AfterSaved.containsKey( version.model_Code )){
                Product_Model_ID = map_Product_Model_AfterSaved.get( version.model_Code ).Id;
            }

            Wrapper_Model_Label wrap_Model_Label = version.label;
            List<Wrapper_model_Items> lst_Wrap_Model_Items = new List<Wrapper_model_Items>();
            lst_Wrap_Model_Items = version.items;
            
            system.Debug( 'lst_Wrap_Model_Items --->'+lst_Wrap_Model_Items);
            
            if( lst_Wrap_Model_Items != null ){
                for( Wrapper_model_Items model_Item : lst_Wrap_Model_Items ){
                    
                    if( model_Item.presentationGroup != null ){
                        Wrapper_presentationGroup wrap_PresentationGRP = model_Item.presentationGroup;
                        
                        List<Wrapper_items> lst_Wrap_Version_Items = new List<Wrapper_items>();
                        lst_Wrap_Version_Items = wrap_PresentationGRP.items;
                        Wrapper_presentationGroup_Label wrap_VersionGroup_Label = wrap_PresentationGRP.label;
                        
                        system.Debug( 'lst_Wrap_Version_Items --->'+lst_Wrap_Version_Items);
                        if( lst_Wrap_Version_Items != null ){
                            for( Wrapper_items wrap_version : lst_Wrap_Version_Items ){

                                // Build new Product Version..
                                Product2 new_Product_Version = new Product2();
                                Wrapper_versionItem wrap_version_Record = wrap_version.versionItem;
                                
                                Wrapper_Version_Label wrap_Version_Name = wrap_version_Record.label;
                                
                                new_Product_Version.Model__c = Product_Model_ID;      // Id of the related model
                                new_Product_Version.Name = wrap_VersionGroup_Label.pt;
                                new_Product_Version.ProductModelCode__c = wrap_PresentationGRP.key;
                                new_Product_Version.ProductCode = version.model_Code + '-' + wrap_version_Record.key; // As mentioned in Doc, for ex, "LCM-VEC162_BRES".
                                new_Product_Version.Version__c = wrap_Version_Name.pt;
                                new_Product_Version.VersionIdSpecCode__c = wrap_version_Record.versionIdSpecCode;
                                new_Product_Version.ModelVersionKey__c = version.model_Code + '-' + wrap_version_Record.key; // As mentioned in Doc, for ex, "LCM-VEC162_BRES".
                                new_Product_Version.IsActive = true;
                                new_Product_Version.RecordTypeId = version_RecordTypeId;
                                
                                set_Product_Versions.add( new_Product_Version );
                            }
                        }
                    }
                }
            }
        }
        
        // Build a map with all versions using the key=ProductCode
        Map<String, Product2> map_VersionRecord_with_ProductCode = new Map<String, Product2>();
        map_VersionRecord_with_ProductCode = VFC22_ProductDAO.getInstance().fetch_All_Versions_Map_with_ProductCode();
        
        for( Product2 product_version : set_Product_Versions ){
            String prodCode = product_version.ProductCode;
            if( map_VersionRecord_with_ProductCode.containsKey( prodCode ) ){
                
                Product2 Update_Product = new Product2();
                Update_Product = map_VersionRecord_with_ProductCode.get( product_version.ProductCode );
                Update_Product.IsActive = true;
                Update_Product.ProductModelCode__c = product_version.ProductModelCode__c;
                Update_Product.VersionIdSpecCode__c = product_version.VersionIdSpecCode__c;
                Update_Product.ModelVersionKey__c = product_version.ModelVersionKey__c;
                Update_Product.ProductModelCode__c = product_version.ProductModelCode__c;
                
                set_Version_Product.add( Update_Product );
                
                //to set disable to the product version records..
                map_VersionRecord_with_ProductCode.remove( prodCode );
            }
            else{
                // INSERT new version from JSON..
                set_Version_Product.add( product_Version );
            }
        }
        
        /**
        //  Suresh : Commented as mentioned by Renault
        // Disable the remaining versions
        List<Product2> lst_All_Versions = new List<Product2>();
        lst_All_Versions = map_VersionRecord_with_ProductCode.values();
        for( Product2 ver : lst_All_Versions ){
            ver.IsActive = false;
            set_Version_Product.add( ver );
        }
        */
        
        // Remove possible duplicates?
        lst_Product_Version_Upsert.clear();
        lst_Product_Version_Upsert.addAll( set_Version_Product );
        system.Debug(' lst_Product_Version_Upsert ---->'+lst_Product_Version_Upsert);
        
        // Upsert the Product Version records..
        VFC22_ProductDAO.getInstance().upsertData( lst_Product_Version_Upsert );

        
        //
        // Process the Colors
        //
        system.Debug( ' lstColorPresentation --->'+lstColorPresentation);

        Id color_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Color' );
        List<Item__c> lst_Item_Insert = new List<Item__c>();
        List<Vehicle_Item__c> lst_Vehicle_Item_Upsert = new List<Vehicle_Item__c>();
        Set<Vehicle_Item__c> set_Vehicle_Item_Insert = new Set<Vehicle_Item__c>();

        // Build a map of Item__c Colors using the key=Name
        Map<String, Item__c> map_Item_with_Name = new Map<String, Item__c>();
        map_Item_with_Name = VFC63_Item_DAO.getInstance().fetch_Color_Items_Map_Name();
        system.Debug( 'map_Item_with_Name -->'+map_Item_with_Name);
        
        // Build a map of Vehicle_Item__c using as key Item -> Name ???
        Map<String, List<Vehicle_Item__c>> map_ItemKey_VehicleItems = new Map<String, List<Vehicle_Item__c>>();
        map_ItemKey_VehicleItems = VFC64_VehicleItem_DAO.getInstance().return_Map_ItemKey_and_VehicleItem();
        system.Debug( 'map_ItemKey_VehicleItems -->'+map_ItemKey_VehicleItems);
        
        
        Map<String, List<Id>> map_ColorKey_with_ProductID = new Map<String, List<Id>>();
        
        // Iterate for all Colors from JSON
        for( Wrapper_PresentationItem model_color : lstColorPresentation )
        {
            if( map_Product_Model_AfterSaved.containsKey( model_color.modelKey )){
                List<Id> lst_Temp_IDs = new List<Id>();
                if( map_ColorKey_with_ProductID.containsKey( model_color.key )){
                    lst_Temp_IDs = map_ColorKey_with_ProductID.get( model_color.key );
                    lst_Temp_IDs.add( map_Product_Model_AfterSaved.get( model_color.modelKey ).Id );
                    map_ColorKey_with_ProductID.put( model_color.key, lst_Temp_IDs );
                }
                else{
                    lst_Temp_IDs.add( map_Product_Model_AfterSaved.get( model_color.modelKey ).Id );
                    map_ColorKey_with_ProductID.put( model_color.key, lst_Temp_IDs );
                }
            }
            
            if( !map_Item_with_Name.containsKey( model_color.key ) )
            {
                // Create a new record to save the new color
                Item__c new_Item = new Item__c();
                new_Item.Name = model_color.key;
                new_Item.Key__c = model_color.key;
                new_Item.Label__c = model_color.label.pt;
                new_Item.RecordTypeId = color_RecordTypeId;
                
                lst_Item_Insert.add( new_Item );
            }
            else{
	            Vehicle_Item__c new_VehicleItem_Insert = new Vehicle_Item__c();
				new_VehicleItem_Insert.ModelVersion__c = map_Product_Model_AfterSaved.get( model_color.modelKey ).Id;
				new_VehicleItem_Insert.Item__c = map_Item_with_Name.get( model_color.key ).Id;
				new_VehicleItem_Insert.isActive__c = true;
				set_Vehicle_Item_Insert.add( new_VehicleItem_Insert );
            }
            
            // Prepare for disabling the color
            if( map_ItemKey_VehicleItems.containsKey( model_color.key ) ){
                List<Vehicle_Item__c> lstUpdate_TRUE = map_ItemKey_VehicleItems.get( model_color.key );
                
                for( Vehicle_Item__c veh : lstUpdate_TRUE ){
                    veh.isActive__c = true;
                    lst_Vehicle_Item_Upsert.add( veh );
                }
                map_ItemKey_VehicleItems.remove( model_color.key );
            }
        }

        // Remove possible duplicates?
        Set<Item__c> set_Item = new Set<Item__c>();
        set_Item.addAll( lst_Item_Insert );
        lst_Item_Insert.clear();
        lst_Item_Insert.addAll( set_Item );
        
        system.Debug(' map_ColorKey_with_ProductID --->'+map_ColorKey_with_ProductID);
        system.Debug(' map_ColorKey_with_ProductID key set ->'+map_ColorKey_with_ProductID.keySet());
        
        // INSERT Item records...
        List<Database.Saveresult> lst_Item_SaveResult =  VFC63_Item_DAO.getInstance().insertData( lst_Item_Insert );
        
        for( Integer i = 0; i < lst_Item_SaveResult.size(); i++ ){
            Database.Saveresult insert_Result = lst_Item_SaveResult[i];
            if( insert_Result.isSuccess() ){
                Item__c Inserted_Item = lst_Item_Insert[i];
                
                system.Debug(' Inserted_Item ----->'+Inserted_Item);
                system.Debug(' Inserted_Item ----->'+Inserted_Item.Name);
                List<Id> lst_Products = new List<Id>();
                lst_Products = map_ColorKey_with_ProductID.get( Inserted_Item.Name );
                
                for( Id productId : lst_Products ){
                    Vehicle_Item__c new_Vehicle_Item = new Vehicle_Item__c();
                    new_Vehicle_Item.Item__c = insert_Result.getId();
                    new_Vehicle_Item.ModelVersion__c = productId; // Map returns the Product2 ID..
                    new_Vehicle_Item.isActive__c = true;
                    
                    lst_Vehicle_Item_Upsert.add( new_Vehicle_Item );
                }
            }
        }
        
        /**
        //  Suresh : Commented as mentioned by Renault
        // Deactivate the remaining Vehicle Items
        for( List<Vehicle_Item__c> lst_vehicleItem : map_ItemKey_VehicleItems.values() ){
            for( Vehicle_Item__c vehicleItem : lst_vehicleItem ){
                vehicleItem.isActive__c = false;
                lst_Vehicle_Item_Upsert.add( vehicleItem );
            }
        }
        */
        
        // Insert New Vehicle Items.
        for( Vehicle_Item__c vehItem : lst_Vehicle_Item_Upsert ){
        	for( Vehicle_Item__c insertVeh : set_Vehicle_Item_Insert ){
        		
        		if( vehItem.ModelVersion__c == insertVeh.ModelVersion__c &&
        			vehItem.Item__c == insertVeh.Item__c ){
        			set_Vehicle_Item_Insert.remove(insertVeh);
        		}
        	}
        }
        
        
        // Remove possible duplicates?
        Set<Vehicle_Item__c> set_Vehicle_Item = new Set<Vehicle_Item__c>();
        set_Vehicle_Item.addAll( lst_Vehicle_Item_Upsert );
        lst_Vehicle_Item_Upsert.clear();
        lst_Vehicle_Item_Upsert.addAll( set_Vehicle_Item );
        lst_Vehicle_Item_Upsert.addAll( set_Vehicle_Item_Insert );
        
        //UPSERT Vehicle Item Records...
        VFC64_VehicleItem_DAO.getInstance().upsertData( lst_Vehicle_Item_Upsert );
        
        system.Debug( 'lst_Item_Insert -->'+lst_Item_Insert);
        system.Debug(' lst_Vehicle_Item_Upsert --->'+lst_Vehicle_Item_Upsert);
        
        
        if (!Test.isRunningTest())
        {
            /**
            *   Schedule the Batch2 from here..
            **/
            Datetime sysTime = System.now();
            sysTime = sysTime.addSeconds(20);
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            system.debug(chron_exp);
            
            WSC08_AOC_Scheduler_Step2 AOCBatch2Schedule = new WSC08_AOC_Scheduler_Step2();
            
            Id idBatch = System.schedule('AOC-Batch2-Schedule' + sysTime.getTime(), chron_exp, AOCBatch2Schedule);
        }
    }
}