/*****************************************************************************************
 Name    : Myr_Synchro_ATC_TriggerHandler                      Creation date : 07 Aug 2015
 Desc    : Handler dedicated to CSDB synchronizations
 Author  : Donatien Veron (AtoS)
 Project : myRenault
******************************************************************************************/
public class Myr_Synchro_ATC_TriggerHandler  { 
    public static void On_Account_After_Update(list<Account> listAcc, boolean isUpdate, Map<Id,Account> oldMap) {
    	Map<Id, String> myMap = new Map<Id, String>();
        for (Account myA: listAcc){
        	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update - Size of listAcc : <' + listAcc.size() +'>');
            if(String.isBlank(myA.country__c) || Country_Info__c.getInstance(myA.country__c)==null || !Country_Info__c.getInstance(myA.country__c).Myr_ATC_Synchronisable__c){
               	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update - Country=<' + myA.country__c + '> not synchronisable');
            }else{
            	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update - MYR Status, New=<' + myA.MYR_Status__c + '>,Old=<' + oldMap.get(myA.Id).MYR_Status__c + '>, CSDB_Account_Activated, New=<' + myA.CSDB_Account_Activated__c + '>, Old=<' + oldMap.get(myA.Id).CSDB_Account_Activated__c + '>, Firstname=<' + myA.Firstname + '>,Old=<' + oldMap.get(myA.Id).Firstname + '>, Lastname,New=<' + myA.Lastname + '>,Old=<' + oldMap.get(myA.Id).Lastname + '} , HomePhone__c = {' + myA.HomePhone__c + ',' + oldMap.get(myA.Id).HomePhone__c + '} , PersMobPhone__c = {' + myA.PersMobPhone__c + ',' + oldMap.get(myA.Id).PersMobPhone__c  + '} , BillingStreet = {' + myA.BillingStreet + ',' + oldMap.get(myA.Id).BillingStreet  + '} , BillingPostalCode = {' + myA.BillingPostalCode + ',' + oldMap.get(myA.Id).BillingPostalCode + '} , BillingCity = {' + myA.BillingCity + ',' + oldMap.get(myA.Id).BillingCity + '}');
              	if (myA.CSDB_Account_Activated__c && ! oldMap.get(myA.Id).CSDB_Account_Activated__c){
                	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update : Block execution of the trigger stack');
              	}else{
                	if(String.isBlank(myA.MyRenaultID__c)){
                  	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update : Empty value for field "My Renault Username (MyRenaultID__c)", accountId = <' + myA.Id + '>');
                	}else{
                  		if(!String.isBlank(myA.MYR_Status__c) && myA.MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Activated) && (String.isBlank(oldMap.get(myA.Id).MYR_Status__c) || !oldMap.get(myA.Id).MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Activated))){
                      		myMap.put(myA.Id, 'Activation_On_Account');
                  		}
                  		if(! (isSynchro(myA.Salutation_Digital__c,oldMap.get(myA.Id).Salutation_Digital__c)
		                      && isSynchro(myA.Firstname,oldMap.get(myA.Id).Firstname)
		                      && isSynchro(myA.Lastname,oldMap.get(myA.Id).Lastname)
		                      && isSynchro(myA.HomePhone__c,oldMap.get(myA.Id).HomePhone__c)
		                      && isSynchro(myA.PersMobPhone__c,oldMap.get(myA.Id).PersMobPhone__c)
		                      && isSynchro(myA.BillingStreet,oldMap.get(myA.Id).BillingStreet)
		                      && isSynchro(myA.BillingPostalCode,oldMap.get(myA.Id).BillingPostalCode)
		                      && isSynchro(myA.BillingCity,oldMap.get(myA.Id).BillingCity))
						){
                      		if(myMap.containskey(myA.Id)){
                        		myMap.put(myA.Id,'Activation_and_Update_On_Account');
                      		}else{
                        		myMap.put(myA.Id,'Update_On_Account');
                      		}
                  		}
                  		if(!String.isBlank(oldMap.get(myA.Id).MyRenaultID__c) && !myA.MyRenaultID__c.equalsIgnoreCase(oldMap.get(myA.Id).MyRenaultID__c)){
                      		myMap.put(myA.Id,'Update_On_Account_ChangeEmail:' + oldMap.get(myA.Id).MyRenaultID__c);
                  		}
                  		System.debug('Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update : myA.MYR_Status__c=<' + myA.MYR_Status__c + '>, system.Label.Myr_Status_Deleted=<' + system.Label.Myr_Status_Deleted + '>,  oldMap.get(myA.Id).MYR_Status__c=<' + oldMap.get(myA.Id).MYR_Status__c + '>, myA.CSDB_Account_Activated__c=<' + myA.CSDB_Account_Activated__c+'>');
                  		if(    !String.isBlank(myA.MYR_Status__c) 
		                     && myA.MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Deleted) 
		                     && (String.isBlank(oldMap.get(myA.Id).MYR_Status__c) || !oldMap.get(myA.Id).MYR_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Deleted))
		                     && myA.CSDB_Account_Activated__c
                		){
                  			myMap.put(myA.Id, 'Delete_On_Account');
                 		}
                	}
              	}
      		}
        }
        if (myMap.size()>0){
			System.debug('Myr_Synchro_ATC_TriggerHandler : myMap.size()=<' + myMap.size()+'>');
			List<Id> myIds = new List<Id>();
			for (Id key : myMap.keySet()){
            	myIds.add(key);
          	}
          	List<VRE_VehRel__c> lVr = [select Id, account__c, vin__c, account__r.CSDB_Account_Activated__c, account__r.Firstname, account__r.Lastname, vin__r.Name from VRE_VehRel__c where My_Garage_Status__c = 'confirmed' and account__r.Id in :myIds and vin__r.rlinkEligibility__c='Y' ];
            for (VRE_VehRel__c myVr: lVr){
                 System.debug('Myr_Synchro_ATC_TriggerHandler : call' + myMap.get(myVr.account__c) + '. Firstname=' + myVr.account__r.Firstname + ', Lastname = ' + myVr.account__r.Lastname + ', VIN = ' + myVr.vin__r.Name + ', Id = ' + myVr.Id);
                 Myr_Synchro_ATC.send_Request_Create_Customer(myVr.account__c, myVr.vin__c, myVr.Id, myMap.get(myVr.account__c));
            }
        }
    }

	private static Boolean isSynchro(String CsdbValue, String SfdcValue){
		//Return True if the value of a specific field retrieved from the CSDB is equal to the one stored in SFDC
		
		Boolean res = false;						
		if (String.isBlank(CsdbValue) && String.isBlank(SfdcValue)) {
			res = true; 
		} else if (CsdbValue!=null && SfdcValue!=null) { 
			res = CsdbValue.equalsignoreCase(SfdcValue);
		}		

		return res;		
	}
    
    public static void On_VehicleRelation_After_Update(list<VRE_VehRel__c> listVR, boolean isUpdate, Map<Id,VRE_VehRel__c> oldMap) {
        System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Update : BEGIN');
        List<Id> myList = new List<Id>();
        for (VRE_VehRel__c myVR: listVR){
            if(!String.isBlank(myVR.My_Garage_Status__c) && myVR.My_Garage_Status__c.equalsIgnoreCase('confirmed') && !myVR.My_Garage_Status__c.equalsIgnoreCase(oldMap.get(myVR.Id).My_Garage_Status__c)){
                myList.add(myVR.Id);
            }
        }
        if (myList.size()>0){
            List<VRE_VehRel__c> lVr = [select Id, account__c, vin__c, account__r.country__c from VRE_VehRel__c where account__r.Myr_Status__c = :system.Label.Myr_Status_Activated and vin__r.rlinkEligibility__c='Y' and Id in :myList ];
            for (VRE_VehRel__c myVr: lVr){
                if((Country_Info__c.getInstance(myVr.account__r.country__c)).Myr_ATC_Synchronisable__c){
                    System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Update : call of send_Request_Create_Customer : myVr.account__c=<' + myVr.account__c + '>, myVr.vin__c=<'+myVr.vin__c+'>, myVr.Id=<'+ myVr.Id + '>, Update_On_Relation');
                    Myr_Synchro_ATC.send_Request_Create_Customer(myVr.account__c, myVr.vin__c, myVr.Id, 'Update_On_Relation');
                }else{
                	System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Update : the country <'+myVr.account__r.country__c+'> of the account :<'+myVr.account__c+'> is not synchronizable. See on custom setting Country Info');
                }
            }
        }
    }
    
    public static void On_VehicleRelation_After_Insert(list<VRE_VehRel__c> listVR) {
        List<Id> myList = new List<Id>();
        for (VRE_VehRel__c myVR: listVR){
            if(!String.isBlank(myVR.My_Garage_Status__c) && myVR.My_Garage_Status__c.equalsIgnoreCase('confirmed')){
                myList.add(myVR.Id);
            }
        }
        if (myList.size()>0){
            List<VRE_VehRel__c> lVr = [select Id, account__r.Firstname, account__r.Lastname, account__c, vin__c, account__r.country__c from VRE_VehRel__c where account__r.Myr_Status__c = :system.Label.Myr_Status_Activated and vin__r.rlinkEligibility__c='Y' and Id in :myList ];
            for (VRE_VehRel__c myVr: lVr){
                System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Insert : myVr.account__r.country__c=<' + myVr.account__r.country__c + ', myVr.account__r.Firstname+Lastname=<' + myVr.account__r.Firstname + myVr.account__r.Lastname + '>');
                if((Country_Info__c.getInstance(myVr.account__r.country__c)).Myr_ATC_Synchronisable__c){
                    System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Insert : call of send_Request_Create_Customer : myVr.account__c=<' + myVr.account__c + '>, myVr.vin__c=<'+myVr.vin__c+'>, myVr.Id=<'+ myVr.Id + '>, Update_On_Relation');
                    Myr_Synchro_ATC.send_Request_Create_Customer(myVr.account__c, myVr.vin__c, myVr.Id, 'Insert_On_Relation');
                }else{
                	System.debug('Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Insert : the country <'+myVr.account__r.country__c+'> of the account :<'+myVr.account__c+'> is not synchronizable. See on custom setting Country Info');
                }
            }
        }
    }
    
    public static void On_Vehicle_After_Update(list<VEH_Veh__c> listVeh, boolean isUpdate, Map<Id,VEH_Veh__c> oldMap) {
        List<Id> myList = new List<Id>();
        for (VEH_Veh__c myV: listVeh){
            if(!String.isBlank(myV.rlinkEligibility__c) && myV.rlinkEligibility__c.equalsIgnoreCase('Y') && ( String.isBlank(oldMap.get(myV.Id).rlinkEligibility__c) || !oldMap.get(myV.Id).rlinkEligibility__c.equalsIgnoreCase('Y'))){
                myList.add(myV.Id); 
            }
        }
        if (myList.size()>0){
            List<VRE_VehRel__c> lVr = [select Id, account__c, vin__c, account__r.country__c from VRE_VehRel__c where My_Garage_Status__c = 'confirmed' and account__r.Myr_Status__c = :system.Label.Myr_Status_Activated and vin__r.Id in :myList ];
            for (VRE_VehRel__c myVr: lVr){
                if((Country_Info__c.getInstance(myVr.account__r.country__c)).Myr_ATC_Synchronisable__c){
                     System.debug('Myr_Synchro_ATC_TriggerHandler.On_Vehicle_After_Update : call of send_Request_Create_Customer : myVr.account__c=<' + myVr.account__c + '>, myVr.vin__c=<'+myVr.vin__c+'>, myVr.Id=<'+ myVr.Id + '>, Update_On_Relation');
                     Myr_Synchro_ATC.send_Request_Create_Customer(myVr.account__c, myVr.vin__c, myVr.Id, 'Update_On_Vehicle');
                }else{
                	System.debug('Myr_Synchro_ATC_TriggerHandler.On_Vehicle_After_Update : the country <'+myVr.account__r.country__c+'> of the account :<'+myVr.account__c+'> is not synchronizable. See on custom setting Country Info');
                }
            }
        }
    }
}