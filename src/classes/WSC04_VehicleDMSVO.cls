global class WSC04_VehicleDMSVO {
    
    webservice String color                 {get;set;}      // required: Cor [Text:100]
    //webservice Datetime entranceDateInStock {get;set;}      // NOT USED
    webservice Date entryInventoryDate      {get;set;}      // optional: Data Entrada no Estoque
    webservice String errorMessage          {get;set;}      // optional
    webservice Datetime lastUpdateDate      {get;set;}      // optional: Data Última Atualização
    webservice Integer manufacturingYear    {get;set;}      // required: Ano Fabricação [4]
    webservice String model                 {get;set;}      // required: Modelo [Text:100]
    webservice Integer modelYear            {get;set;}      // required: Ano Modelo [4]
    webservice String optionals             {get;set;}      // optional: Opcionais [Text:400]
    webservice Double price                 {get;set;}      // optional : Preço [10,2]
    webservice String status                {get;set;}      // required: Status : EN: [Available / To be billed / Reserved / Billed]
    webservice String version               {get;set;}      // required: Versão [Text:255]
    webservice String VIN                   {get;set;}      // required: Chassi [Text:17]

	// to delete
	//webservice Boolean error                {get;set;}      // optional: False: no error ; True: error

    global WSC04_VehicleDMSVO(){} 
}