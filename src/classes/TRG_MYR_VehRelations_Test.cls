@isTest
public class TRG_MYR_VehRelations_Test {

    public static testMethod void myUnitTest(){
        
        User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
        Database.insert( manager );
        
        Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concessionária teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
        
        Database.insert( dealerAcc );
        
        VEH_Veh__c veiculo = new VEH_Veh__c();
        veiculo.Name = '93Y5SRD04FJ727745';
        
        Database.insert( veiculo );
        
        VRE_VehRel__c veh = new VRE_VehRel__c();
        veh.Account__c = dealerAcc.Id;
        veh.VIN__c = veiculo.Id;
        Database.insert( veh );
        
        Database.delete( veh );
    }
}