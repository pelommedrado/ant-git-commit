/**
    SDP / 26.08.2014
    Apex class containing main functions for managing myrenault users
    DVN : add currency on createUser method 

@version 16.10: SDP / F1991-US3659 / Centralize check username functionnality
**/
global class Myr_Users_Utilities_Class {
    
    global class Myr_Users_Utilities_Exception extends Exception {}
    
    public static String DEF_TIMEZONESIDKEY         = 'GMT';
    public static String DEF_LOCALESIDKEY           = 'fr_FR_EURO';
    public static String DEF_LANGUAGELOCALKEY       = 'fr';
    public static String DEF_EMAILENCODINGKEY       = 'ISO-8859-1';
    
     

    /** @return the Profile Salesforce for the given profile name **/
    public static Profile getProfile(String iProfileName) {
        try { 
            return [SELECT Id, Name FROM Profile WHERE Name=:iProfileName];
        } catch (Exception e) {
            throw new Myr_Users_Utilities_Exception ('Profile <' + iProfileName + '> does not exist or it hasnot been set in the Custom setting CS04_MYR_Settings');
        }
    }
    
    /** @return the Read Only Technical Profile for Helios **/
    global static Profile getHeliosReadOnlyProfile() {
        return getProfile(CS04_MYR_Settings__c.getInstance().Myr_Profile_Helios_ReadOnly__c);
    }
    
    /** @return the profile for the myrenault technical user **/
    global static Profile getHeliosTechniqueProfile() {
        return getProfile(CS04_MYR_Settings__c.getInstance().Myr_Profile_Helios_Technique__c);
    }
    
    /** @return the profile for the myrenault community user **/
    global static Profile getHeliosCommunityProfile() {
        return getProfile(CS04_MYR_Settings__c.getInstance().Myr_Profile_Community_User__c);
    }
    
    /** @return the profile for the Customer Message user **/
    global static Profile getHeliosCustomerMessageProfile() {
        return getProfile(CS04_MYR_Settings__c.getInstance().Myr_Profile_Helios_Customer_Messages__c);
    }
    
    /** @return the profile for the Dealer user **/
    global static Profile getHeliosDealerProfile() {
        return getProfile(CS04_MYR_Settings__c.getInstance().Myr_Profile_Dealer__c);
    }
    
    //@return the profile for the Dealer user
    global static Profile getHeliosViewerBusinessProfile() {
        return getProfile('HeliosViewerBusiness');
    }
    
    //@return the profile for the Dealer user
    global static Profile getHeliosUpdateBusinessProfile() {
        return getProfile('HeliosUpdateBusiness');
    }

	//@return the profile for the Dealer user
    global static Profile getHeliosLocalAdministratorProfile() {
        return getProfile('HeliosLocalAdministrator');
    }

	//@return the permission set required to run HeliosTechnique user
	global static PermissionSet getHeliosTechniquePermSet() {
		String permSetName = 'MYR_WebServices_Access';
		if( !String.isBlank(CS04_MYR_Settings__c.getInstance().Myr_PermissionSet_HeliosTechnique__c) ) {
			permSetName = CS04_MYR_Settings__c.getInstance().Myr_PermissionSet_HeliosTechnique__c;
		} 
		try {
            return [SELECT Id, Name FROM PermissionSet WHERE Name=:permSetName];
        } catch (Exception e) {
            throw new Myr_Users_Utilities_Exception ('PermissionSet <' + permSetName + '> does not exist or it hasnot been set in the Custom setting CS04_MYR_Settings');
        }
	}
    
    /** @return the default record type to use with inserted personal accounts **/
    global static Id getPersonalAccountRecordType() { 
        try {
            //SDP avoid the use of a query. 
            //Use Developer Name as the label could change depending on the language
            RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = :CS04_MYR_Settings__c.getInstance().Myr_Personal_Account_Record_Type__c];
            return rt.Id;
        } catch (Exception e) {
            throw new Myr_Users_Utilities_Exception ('<' + CS04_MYR_Settings__c.getInstance().Myr_Personal_Account_Record_Type__c + '> does not exist or it has not been set in the Custom setting CS04_MYR_Settings');
        }
    }

    /** create the community user associated to the given account
        @param the account on which we have to create a user
        @return the created user
        
        @TODO user exists with a different profile ????
        @TODO does RecordDefaultCountry__c, TimeZoneSidKey, LocaleSidKey and LanguageLocaleKey have a real importance in our case ?
        @TODO set the user password and return it
    **/ 
    global static User createUser(Account a, String brand, String timeZoneSidKey, String LocaleSidKey, String LanguageLocaleKey, String currencyCode) {
        List<User> listUsers = [SELECT Id FROM User WHERE Contact.AccountId = :a.Id];
        if( listUsers.size() > 0 ) throw new Myr_Users_Utilities_Exception ('User Already Exists');//user already exists, this is not normal
        //Create the user and associate it to the given account.
        User u = new User();
		u = prepareUsername( u, a, brand );
        u.ContactId = a.PersonContactId;
        u.Title = a.Salutation;
        u.CommunityNickname = createCommunityNickName(a.Firstname, a.Lastname);               //REQUIRED
        u.Alias = a.FirstName.left(1)+a.LastName.left(3); //no more than 8 cars             //REQUIRED
        u.firstname = a.FirstName;
        u.lastname = a.LastName;
        u.IsActive = true; // useless as we cannot set a password on a inactive user
        u.RecordDefaultCountry__c = a.country__c; 
        u.TimeZoneSidKey = timeZoneSidKey;                                                  //REQUIRED
        u.localeSidKey= localeSidKey;                                                           //REQUIRED
        u.languageLocaleKey = languageLocaleKey;
        u.CURRENCYISOCODE = currencyCode;                                                           //REQUIRED
        u.EmailEncodingKey = (String.isBlank(Country_Info__c.getInstance(a.Country__c).EmailEncodingCode__c) ? DEF_EMAILENCODINGKEY : Country_Info__c.getInstance(a.Country__c).EmailEncodingCode__c);        //REQUIRED
        u.ProfileId = getHeliosCommunityProfile().Id;
        u.UserType__c='MYR';
        try {
            insert u;
        } catch (Dmlexception dmlE) {
            throw new Myr_Users_Utilities_Exception('PROBLEM WHEN CREATING USER: ' + dmlE.getMessage());
        }
        return u;
    }

	/** Prepare the email and the username of the given user for the given brand
		@param u, User, the user to prepare
		@param a, Account, the Account on which the user is based
		@param brand, String, the brand on which the user is created
		@return User, the user modified
	**/
	global static User prepareUsername(User u, Account a, String brand) {
		 String email;
		if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(brand) ) {
			email = a.MyDaciaID__c;
		} else {
			email = a.MyRenaultID__c;
		}
        u.Email = email;
        u.Username = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c + email;
		return u;
	}

	/** Check if the given username is available althrough all the salesforce organizations. This is done by 
		simulating the insertion of a user (set a savepoint then rollback in case of insertion success
		@param the username (including rhelios prefix )
		@return true if the username is available, false otherwise
	**/
	global static Boolean isUsernameAvailableAlThroughOrgs( String username ) {
		//try to insert the username to check a global availability across all the salesforce organization
        String prefix = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c;
        String email = username.substring(prefix.length(), username.length());
        System.Savepoint savePoint = Database.setSavepoint();
        User u = new User();
        try {
            u.firstname= 'check purposes';
            u.lastname = 'helios';
            u.email = username;
            u.Username = username;
            u.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName('', email );     //REQUIRED
            u.Alias = email.left(8);         //no more than 8 cars  /REQUIRED
            u.TimeZoneSidKey = Myr_Users_Utilities_Class.DEF_TIMEZONESIDKEY ;           //REQUIRED
            u.localeSidKey= Myr_Users_Utilities_Class.DEF_LOCALESIDKEY;                 //REQUIRED
            u.languageLocaleKey = Myr_Users_Utilities_Class.DEF_LANGUAGELOCALKEY;       //REQUIRED
            u.EmailEncodingKey = Myr_Users_Utilities_Class.DEF_EMAILENCODINGKEY;        //REQUIRED
            u.ProfileId = Myr_Users_Utilities_Class.getHeliosReadOnlyProfile().Id;
            insert u;
        } catch (DmlException e) {
            if( e.getMessage().contains('DUPLICATE_USERNAME') ) {
                return false;
            } 
        }
        Database.rollback(savePoint);
		return true;
	}

	 /** Create a valid community nickname for salesforce. To insure compatibility and avoid modification in all the developments
        @return the community nickname depending on the firstname and the lastname of the account **/
	public static String createCommunityNickName(String firstName, String lastName){
		return createCommunityNickName( firstName, lastName, null);
	}

    /** Create a valid community nickname for salesforce
        @return the community nickname depending on the firstname and the lastname of the account **/
    public static String createCommunityNickName(String firstName, String lastName, Integer idName){
        String communityNickname = firstName+'.'+lastName;
        communityNickname.toUpperCase();
        communityNickname.deleteWhitespace();
        // La taille du CommunityNickname ne doit pas d?passer 30 caract?res
        if( communityNickname.length()>32 ){
            communityNickname=communityNickname.substring(0, 31);
        } 
        //check communityNickName
		if( idName != null ) {
			communityNickname += idName;
		} else {
			List<User> listUsers = [SELECT Id, Name FROM User WHERE CommunityNickname LIKE :communityNickname + '%'];
			if (listUsers.size() > 0) {
				communityNickname += String.valueOf(listUsers.size());
			}
		}
        return communityNickname;
    }

	/** Generates a cancel name that is unique for a given user **/
	public static String generateCancelUsername(String accountId) {
	    String timestamp = String.valueOf(datetime.now().getTime());
	    String prefix = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c;
	    return prefix + '.' + accountId + '@' + timestamp + '.com';
	}
    
    /** Check the format of the customer identification number per country
  		@return null if all is ok, the error otherwise.
  		Some countries require a specific check:
  			- Romania, 13 digits SAALLZZJJNNNC 
  						1 digit for sex and century of birth (S): 1, 3, and 5 = male; 2, 4, and 6 = female. 1 and 2 = 20th century, 3 and 4 = 19th century, and 5 and 6 = 21st century. (For legal residents the numbers 7 and 8 are used to encode the sex of the person having the residence permit);
						6 digits for birth date in the format YYMMDD (AALLZZ)
						2 digits for place of birth(JJ) (the code for the county) (For legal residents the numbers encode the region where the person having the residence permit comes from);
						3 other digits (NNN)
						1 control digit
  					AND ==> (S*2+A*7+A*9+L*1+L*4+Z*6+Z*3+J*5+J*8+N*2+N*7+N*9) MOD 11 = C 	
	**/
	private static Integer[] ROM_CNP = new Integer[]{2,7,9,1,4,6,3,5,8,2,7,9};
	
    public static String checkCustomerIdentNbr(String country, String CIN) {
    	system.debug('### Myr_Users_Utilities_Class - <checkCustomerIdentNbr> - BEGIN country='+country+', CIN='+CIN);
		Country_Info__c countryInf = Country_Info__c.getInstance(country);
		if( countryInf != null && !String.isBlank(countryInf.Myr_CustomerIdentificationNbr_Pattern__c) ) {
			system.debug('### Myr_Users_Utilities_Class - <checkCustomerIdentNbr> - Pattern='+countryInf.Myr_CustomerIdentificationNbr_Pattern__c);
			if( String.isBlank(CIN) ) return 'mandatory field for ' + country;
			if( !Pattern.matches(countryInf.Myr_CustomerIdentificationNbr_Pattern__c, CIN) ) {
				return 'not the proper format ['+countryInf.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected';
			} else {
				//specific check for some countries
				if( 'Romania'.equalsIgnoreCase(country) ) {
					try {
						system.debug('### Myr_Users_Utilities_Class - <checkCustomerIdentNbr> - Romania checker');
						Integer check = 0;
						for( Integer i = 0; i < CIN.length() - 1; ++i) {
							system.debug('>>>> i='+i+', CIN.substring(i, i+1)='+CIN.substring(i, i+1));
							check += Integer.valueOf(CIN.substring(i, i+1)) * ROM_CNP[i];
						}
						system.debug('### Myr_Users_Utilities_Class - <checkCustomerIdentNbr> - Romania check='+check+', modulo='+Math.mod(check, 11) + ', check=' + CIN.substring(CIN.length()-2, CIN.length()-1));
						Integer modulo = Math.mod(check, 11);
						modulo = (modulo == 10) ? 1 : modulo;
						if( ! (modulo == Integer.valueOf(CIN.substring(CIN.length()-1, CIN.length()))) ) {
							return 'not the proper format [control digit is not correct]'; 
						}
					} catch (Exception e) {
						return 'not the proper format [only digits accepted] ' + e.getMessage();
					}
				}
			}
		}
	  	return null;
    }

	/** @return String, the name of the record type **/
	public static String getPersAccRecordType() {
		if( !String.isBlank(CS04_MYR_Settings__c.getInstance().Myr_Personal_Account_Record_Type__c) ) {
			return CS04_MYR_Settings__c.getInstance().Myr_Personal_Account_Record_Type__c;
		} else if( Myr_Datasets_Test.isBrazilOrg() ) {
			return 'Personal_Acc';
		} else {
			return 'CORE_ACC_Personal_Account_RecType';
		}
	}

	/** @return String, the name of the record type **/
	public static String getDealerRecordType() {
		if( !String.isBlank(CS04_MYR_Settings__c.getInstance().Myr_Dealer_Record_Type__c) ) {
			return CS04_MYR_Settings__c.getInstance().Myr_Dealer_Record_Type__c;
		} else {
			return 'Network_Site_Acc';
		}
	}

	/** @return String, the name of the record type **/
	public static Id getPersAccRecordTypeId() {
		//impossible to use Id persAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(perAccRTName).getRecordTypeId();
		//as this is based on Name and not on DeveloperName (API name) => multi language problem !
		List<RecordType> listRTs = [SELECT Id FROM RecordType WHERE DeveloperName = :getPersAccRecordType()];
		if( listRTs.size() > 0 ) {
			return listRTs[0].Id;
		}
		return null;
	}

	/** @return Id, the id the record type **/
	public static Id getDealerRecordTypeId() {
		List<RecordType> listRTs = [SELECT Id FROM RecordType WHERE DeveloperName = :getDealerRecordType()];
		if( listRTs.size() > 0 ) {
			return listRTs[0].Id;
		}
		return null;
	}

}