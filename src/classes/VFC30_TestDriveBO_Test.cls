/**
*	Class	-	VFC30_TestDriveBO_Test
*	Author	-	RameshPrabu
*	Date	-	16/11/2012
*
*	#01 <RameshPrabu> <16/11/2012>
*		This test class called for VFC30_TestDriveBO
**/
@isTest 
public with sharing class VFC30_TestDriveBO_Test {
	static testMethod void VFC30_TestDriveBO_Test() {
		List<VEH_Veh__c> lstVehs = VFC03_InsertSObjectsRecordsForTestClass.getInstance().vehicleRecordsInsertion();
       	List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
       	List<TDV_TestDriveVehicle__c> lstTestDriveVehicle =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDriveVehicleRecordsInsertion(lstDealerRecords[0], lstVehs[0]);
        List<TDV_TestDrive__c> lstTestDrives = VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDrive_RecordsInsertion(lstDealerRecords[0], lstTestDriveVehicle[0]);
        
        try{
       		List<TDV_TestDrive__c> lstTestDrives1 = VFC03_InsertSObjectsRecordsForTestClass.getInstance().testDrive_RecordsInsertion(lstDealerRecords[0], lstTestDriveVehicle[0]);
       	}catch(System.DMLException e){
       		String errorMessage = Label.ERRTDVMessage2;
          	System.assert(e.getMessage().contains(errorMessage),e.getMessage());
      	}
      	List<TDV_TestDrive__c> lstTestDrivesUpdate = new List<TDV_TestDrive__c>();
      	for(TDV_TestDrive__c updateTestDrive : lstTestDrives){
      		DateTime dateOfBooking = Datetime.newInstanceGmt( system.now().yearGmt(), system.now().monthGmt(), system.now().dayGmt(), system.now().hourGmt() + 1, 0,0);
      		updateTestDrive.DateBooking__c = dateOfBooking.addMinutes(30);
      		lstTestDrivesUpdate.add(updateTestDrive);
      	}
      	try{
       		VFC28_TestDriveDAO.getInstance().updateData(lstTestDrivesUpdate);
       	}catch(System.DMLException e){
       		String errorMessage = Label.ERRTDVMessage2;
          	System.assert(e.getMessage().contains(errorMessage),e.getMessage());
      	}
	}
}