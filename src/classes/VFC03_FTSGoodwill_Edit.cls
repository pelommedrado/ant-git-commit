/* This class is to display Technical Goodwill Edit Page - Reena J */

public with sharing class VFC03_FTSGoodwill_Edit {

    public String getTotalPercentage { get; set; }
       
    //Variables
    Id gudwillId=ApexPages.currentPage().getParameters().get('id');
    Goodwill__c gudwill=new Goodwill__c();
    List<case> caseRec=new List<case>();
    String retURL=ApexPages.currentPage().getParameters().get('retURL');
    List<Goodwill_Grid_Line__c> gwGridLines=new List<Goodwill_Grid_Line__c>();  
    String caseBrand; 
    date OrdateGw;
    Integer VehicleAg;
    Integer km;
    Integer SRCAmt;
    integer numberDaysDue;
  
    //This method is to return Goodwill Record
    public Goodwill__c getgudwill() {
        return gudwill;
    }
     
    public VFC03_FTSGoodwill_Edit() {
        gudwill=[SELECT AgreementNbr__c,Agreement_date__c, BankName__c, Brand__c, BudgetCode__c, BackUp_Car__c, Car_Rental_Amount__c,
        Car_Rental_Negociated_fare__c, Case__c, CatalogFunctionCode__c, Catalog__c, CheckNbr__c, Country__c,
        CreditCode__c, CurrencyCode__c, CurrencyIsoCode, CustomerScore__c, RemainingBalance__c, DealerPartAmount__c,
        DealerName__c, DealerPartRate__c, DebtorName__c, SRCDecidedRate__c, Decisiondescription__c, IsDeleted,
        DeliveryDate__c, Description__c, DeviationReasDesc__c, DeviationReason__c, ExpenseCode__c, FTS__c,
        SRC_Decided_Rate__c, GlobalPartRate__c, GoodwillAmount__c, Goodwill_Amount_Calculated__c, Name,
        GoodwillStatus__c, GoodwillAmount2__c, ICMPartAmount__c, ICMPartRate__c, Date_of_Vehicle_retention__c,
        Organ__c, Kilometer__c, LastActivityDate, Number_of_Days__c,
        ORDate__c, Part_Inventory_Status__c, Part_order_number__c, Part_order_type__c, PaymentDealerID__c,
        PaymentMode__c, Id, RecordTypeId, RenaultPartRate__c, QuotClientRate__c, QuotWarrantyRate__c, 
        ResolutionCode__c, SRCCustomerValPartRate__c, Amount__c, SRCPartAmount__c, SRCRetainedRate__c,
        SRCPartAmount2__c, SRCPartRate__c, SystemModstamp, TestAmountValue__c, Total_Goodwill_Amount__c,
        VIN__c, VehicleAgeInMonth__c, GlobalPartAmount__c FROM Goodwill__c where Id=:gudwillId];
     
    }
    
    //This method is used for Cancel button
    public PageReference cancel() {
        PageReference canPg=null;
        return canPg= new PageReference('/'+retURL);
     }

    
    //This method is used to save the Goodwill Record
    public PageReference saveGoodwill()
    {
        PageReference gw=null;
         try{
            gudwill.AgreementNbr__c=gudwill.AgreementNbr__c;
            gudwill.Agreement_date__c=gudwill.Agreement_date__c;
            
            gudwill.GoodwillStatus__c=gudwill.GoodwillStatus__c;
            gudwill.ResolutionCode__c=gudwill.ResolutionCode__c;
            gudwill.Country__c=gudwill.Country__c;
            gudwill.CurrencyCode__c=gudwill.CurrencyCode__c;
            gudwill.Description__c=gudwill.Description__c;
            gudwill.ORDate__c=gudwill.ORDate__c;
            gudwill.Organ__c=gudwill.Organ__c;
            gudwill.QuotWarrantyRate__c=gudwill.QuotWarrantyRate__c;
            gudwill.ICMPartRate__c=gudwill.ICMPartRate__c;
            gudwill.QuotClientRate__c=gudwill.QuotClientRate__c;
            gudwill.DealerPartRate__c=gudwill.DealerPartRate__c;
            gudwill.SRCPartRate__c=gudwill.SRCPartRate__c;
            gudwill.SRCDecidedRate__c=gudwill.SRCDecidedRate__c;
            gudwill.DeviationReasDesc__c=gudwill.DeviationReasDesc__c;
            gudwill.DealerName__c=gudwill.DealerName__c;
            gudwill.BankName__c=gudwill.BankName__c;
            gudwill.CreditCode__c=gudwill.CreditCode__c;
            
            if (gudwill.ResolutionCode__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Resolution Code'));
                return null;
            } 
            if (gudwill.ExpenseCode__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Expense Code'));
                return null;
            } 
            gudwill.ExpenseCode__c=gudwill.ExpenseCode__c;
            if (gudwill.BudgetCode__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Budget Code'));
                return null;
            } 
            if ((gudwill.GoodwillStatus__c == 'Approved') &(gudwill.DeviationReason__c==null)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Deviation Reason if Goodwill Approved'));
                return null;
            }  
            
            gudwill.BudgetCode__c=gudwill.BudgetCode__c;
            gudwill.DeviationReason__c=gudwill.DeviationReason__c;
            gudwill.Id=gudwillId;
            upsert gudwill;
        }
        catch(Exception e){
            System.debug('Error'+e);
        }
        
        return gw= new PageReference('/'+gudwill.Id);
        
    }
    
    

    
    //This method is used for Goodwill Calculation
    public void CalculateSRC(){
    try{
        caseRec=[select CaseBrand__c,DeliveryDate__c,Kilometer__c from Case where Id=:gudwill.Case__c];
       }
    catch(Exception e){
    System.debug('Error'+e);
    }
    if(caseBrand==null)
    {
        caseBrand='Renault';
    }
    else{
        caseBrand=caseRec[0].CaseBrand__c;
    }
        
    System.debug('LINE 114>>>>>>>>And the caseBrand is >>>>'+caseBrand);
    OrdateGw=caseRec[0].DeliveryDate__c;
    if((gudwill.ORDate__c!=null) & (caseRec[0].DeliveryDate__c!=null)){
        Date myDate = date.valueOf(gudwill.ORDate__c);
        Date myDate1 = date.valueOf(caseRec[0].DeliveryDate__c);
        numberDaysDue =myDate1.daysBetween(myDate);
    }
    else{
        numberDaysDue=0; 
    }
    VehicleAg=Integer.valueOf(numberDaysDue/30.5);
    km=Integer.valueOf(caseRec[0].Kilometer__c);
    System.debug('LINE 126>>>>>>>>And the VehicleAg is >>>>'+VehicleAg);
    if(gudwill.Country__c==null ||caseBrand==null || VehicleAg==null || km==null) {
    SRCAmt=0;
    }
    else{
        gwGridLines = [Select Age_interval__c, Max_Age__c, Mileage_Interval__c, Max_Mileage__c, Row_Number__c, f1__c, f2__c, f3__c, f4__c, f5__c, f6__c, f7__c, f8__c, f9__c, f10__c, f11__c, f12__c, f13__c, f14__c, f15__c, f16__c, f17__c, f18__c, f19__c, f20__c, f21__c, f22__c, f23__c, f24__c, f25__c, f26__c, f27__c, f28__c, f29__c, f30__c, f31__c, f32__c, f33__c, f34__c, f35__c, f36__c, f37__c, f38__c, f39__c, f40__c, f41__c, f42__c, f43__c, f44__c, f45__c, f46__c, f47__c, f48__c, f49__c, f50__c, f51__c, f52__c, f53__c, f54__c, f55__c, f56__c, f57__c, f58__c, f59__c, f60__c, f61__c from Goodwill_Grid_Line__c where Country__c = :gudwill.Country__c AND VehicleBrand__c = :caseBrand AND Organ__c = :gudwill.Organ__c order by Row_Number__c];
        if (gwGridLines.size() == 0 || VehicleAg > gwGridLines.get(0).Max_Age__c || km > gwGridLines.get(0).Max_Mileage__c)
        { 
            System.debug('LINE 134>>>>>>>>gwGridLines.size() == 0 || VehicleAg > gwGridLines.get(0).Max_Age__c || km > gwGridLines.get(0).Max_Mileage__c<<<<<<<<<<<<<<<<<');
            SRCAmt= 0;
        }
        else
        {
            Integer mileageLine = Integer.valueOf(km / gwGridLines.get(0).Mileage_Interval__c)+1;
            Integer ageColumn = Integer.valueOf(VehicleAg / gwGridLines.get(0).Age_interval__c)+1;
            SRCAmt= 0;
        for (Goodwill_Grid_Line__c gw : gwGridLines)
        { 
            if (gw.Row_Number__c == mileageLine) 
            {
                
           System.debug('LINE 147>>>>>>>>gw.Row_Number__c= MileageLine<<<<<<<<<<<<<<<<<');
           System.debug('LINE 149>>>>>>>>ageColumn<<<<<<<<<<<<<<<<<'+ageColumn);
            SRCAmt= Integer.valueOf(gw.get('f'+ageColumn+'__c')); 
            
            System.debug('And the Final Value is >>>>'+SRCAmt);
            }
        }
        }
   
    }
    gudwill.SRCPartRate__c=SRCAmt;
  
    }
 
}