/**
Last Modified : 20/04/2016 by Ludovic Marboutie 
Tested by : TRVWebservicesManager_Test
**/

public with sharing class TRVWebservicesBCS 
{

    //Members: webservice and request made
    public static TRVwsdlBcs.BCS_spcCrmGetCustDataService bcsDataSource;
    public static TRVwsdlBcs.getCustData getCustDataReq;

    

      /** CALL The BCS Personal Data WebService and @return a Myr_PersonalDataResponse_Cls response **/
    public static TRVResponseWebservices getAccountSearch(TRVRequestParametersWebServices.RequestParameters eParams) 
    {
        TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element bcsResponse = getBCSData(eParams);
        TRVResponseWebservices response = mapPersonalDataFields(bcsResponse, eParams);
        
        system.debug('#### TRVWebservicesBCS - getAccountSearch - TRV - END'); 
        return response;
    }   
    
    private static TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element getBCSData(TRVRequestParametersWebServices.RequestParameters eParams) 
    {
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - BEGIN'); 
        
        if( eParams.brand == null )
            system.debug('#### TRVWebservicesBCS - getBCSData - TRV - Brand is Mandatory [Warning][Error]');
             
        if( eParams.vin == null && eParams.Registration == null && eParams.ident1 == null & eParams.idClient == null && eParams.email == null && ( (eParams.lastname == null && eParams.zip == null) || (eParams.lastname == null && eParams.city == null) ) ) 
        {
            system.debug('#### TRVWebservicesBCS - getBCSData - TRV - Argument or combinations missing [Warning]');
            system.debug('##Ludo The minimum parameters for BCS should one of the following combinations: vin or registration or ident1 or idClient or email or lastname + city or lastname + zip');
        }
        //Prepare the header
        bcsDataSource = new TRVwsdlBcs.BCS_spcCrmGetCustDataService();
        
        // Map Webservice;Url
        list<String> WsInfo = new list<String>(); 
        User u = [Select Id, RecordDefaultCountry__c, UserType__c from user where Id = :UserInfo.getUserId()];
        map<String, String> mapWS = new map<String, String>();
        map<String, String> mapLogin = new map<String, String>(); 
        map<String, String> mapPassword = new map<String, String>();  
        for (WebServicesInfo__c ws : [select name, Login__c, Password__c, EndPoint__c from WebServicesInfo__c])
        {
            if (ws.Login__c != null || ws.Login__c != '')
                mapLogin.put(ws.Name, ws.login__c);
            if (ws.Password__c != null || ws.Password__c != '')
                mapPassword.put(ws.Name, ws.password__c);
            mapWs.put(ws.Name, ws.EndPoint__c);
        }
        try //TODO clean hard code
        {
            if (u.RecordDefaultCountry__c == 'Austria') 
            {
                bcsDataSource.endpoint_x = mapWs.get('R2');
                //bcsDataSource.endpoint_x='http://r2soap.contact360.at/';
                
            }
            else if (u.RecordDefaultCountry__c == 'Switzerland')
            {
              //bcsDataSource.endpoint_x = 'http://requestb.in/194oy2a1';
                bcsDataSource.endpoint_x = mapWs.get('PDC');
            } 
            else 
                bcsDataSource.endpoint_x = mapWs.get('BCS');
        }
        catch(exception e)
        {
            system.debug('Error Endpoint Exception :'+ e.getMessage());
            return null;
        }
        system.debug('##Ludo endpoint bcs : ' + bcsDataSource.endpoint_x);
        
        //bcsDataSource.endpoint_x = System.label.BcsWsUrl;
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - EndPoint BCS : ' + bcsDataSource.endpoint_x); 
        // TODO LUDO voir pour remplacer par un custom setting
        bcsDataSource.clientCertName_x = System.label.clientcertnamex;
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - clientCertName_x BCS : ' + bcsDataSource.clientCertName_x);
        bcsDataSource.timeout_x = 120000; 
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - TimeOut BCS : ' + bcsDataSource.timeout_x);
         
        bcsDataSource.inputHttpHeaders_x = new Map<String, String>();
         //bcsDataSource.inputHttpHeaders_x.put('Content-Type', 'application/soap+xml; charset=utf-8');
        bcsDataSource.inputHttpHeaders_x.put('Content-Type', ' text/xml; charset=utf-8');
        bcsDataSource.inputHttpHeaders_x.put('Keep-Alive', '1115');
        //Header particulier pour l'autriche. 
        if (u.RecordDefaultCountry__c == 'Austria')
        {
            Blob loginPassword = blob.valueof(mapLogin.get('R2') + ':' + mapPassword.get('R2')); 
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(loginPassword);
            bcsDataSource.inputHttpHeaders_x.put('Authorization', authorizationHeader); 
        } 
        
        
        //Prepare the request parameters: 
        TRVwsdlBcs.custDataRequest custDataRequest = new TRVwsdlBcs.custDataRequest();
        custDataRequest.mode = '';
        custDataRequest.country = (eParams.country == null) ? '' : eParams.country;
        custDataRequest.brand = (eParams.brand == null) ? '' : eParams.brand;
        custDataRequest.demander = eParams.demander; //by default
        custDataRequest.vin = (eParams.vin == null ) ? '' : eParams.vin;
        custDataRequest.lastName = (eParams.LastName == null ) ? '' : eParams.LastName;
        custDataRequest.firstName = (eParams.firstName == null ) ? '' : eParams.firstName;
        custDataRequest.city = (eParams.City == null ) ? '' : eParams.City;
        custDataRequest.zip = (eParams.Zip == null ) ? '' : eParams.Zip;
        custDataRequest.ident1 = (eParams.ident1 == null ) ? '' : eParams.ident1;
        custDataRequest.idClient = (eParams.idClient == null ) ? '' : eParams.idClient;
        custDataRequest.idMyr = '';
        custDataRequest.firstRegistrationDate = '';
        custDataRequest.registration = (eParams.Registration == null ) ? '' : eParams.Registration;
        custDataRequest.email = (eParams.email == null ) ? '' : eParams.email;
        custDataRequest.ownedVehicles = '';
        
        if (eParams.nbReplies == null)
      custDataRequest.nbReplies = '20';
        else 
         custDataRequest.nbReplies = String.valueOf(eParams.nbReplies);
          
        TRVwsdlBcs.servicePrefs servicesPrefs = new TRVwsdlBcs.servicePrefs();
        servicesPrefs.irn = '?';
        servicesPrefs.sia = '?';
        servicesPrefs.reqid = '?';
        servicesPrefs.userid = '?';
        servicesPrefs.language = '?';
        servicesPrefs.country = '?';
        
        TRVwsdlBcs.request request = new TRVwsdlBcs.request();
        
        request.servicePrefs =  servicesPrefs;
        request.custDataRequest = custDataRequest;
        getCustDataReq = new TRVwsdlBcs.getCustData();
        getCustDataReq.request = request;
        
        //Call BCS
        bcsDataSource.inputHttpHeaders_x.put('Content-Length', String.ValueOf(getCustDataReq.toString().length()));
        
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - input Http Headers BCS : ' + bcsDataSource.inputHttpHeaders_x);
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - cust Data Request BCS : ' + custDataRequest);
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - service Prefs BCS : ' + servicesPrefs);
        system.debug('#### TRVWebservicesBCS - getBCSData - TRV - request BCS : ' + getCustDataReq);
        system.debug('\n #### TRVWebservicesBCS - getBCSData - TRV - Request BSC BEGIN');
        TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element responseBCS = bcsDataSource.getCustData(getCustDataReq);
        system.debug('\n #### TRVWebservicesBCS - getBCSData - TRV - Request BSC END');
        
        system.debug('\n #### TRVWebservicesBCS - getBCSData - TRV - return response : ' + responseBCS); 
        system.debug('\n #### TRVWebservicesBCS - getBCSData - TRV - END');
        return responseBCS;
  }
  
   /** Map the BCS response into a PersonalData response **/
    public static TRVResponseWebservices mapPersonalDataFields(TRVwsdlBcs.BCS_spcCrmGetCustDataService_getCustData_Output_element iBCSResponse, TRVRequestParametersWebServices.RequestParameters eParams) 
    {
        system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - BEGIN');
        TRVResponseWebservices pDataResponse = new TRVResponseWebservices();
        pDataResponse.response.dataSource = 'BCS';
        if( iBCSResponse == null ) 
        {
            system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - Argument is null [Warning]');
            return pDataResponse;
        }
        if( iBCSResponse.getCustDataResponse == null ) 
        {
            system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - Argument.getCustDataResponse is null [Warning]');
            return pDataResponse;
        }
        if( iBCSResponse.getCustDataResponse.response == null ) 
        {
            system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - Argument.response is null [Warning]');
            return pDataResponse;
        }
        if( iBCSResponse.getCustDataResponse.response.clientList != null ) 
        {
            system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - Result parsing BEGIN');
            
            //for transco
            map<String, MDMCivillity__c> mdmCivility = MDMCivillity__c.getAll(); //Civility
            List<String> civilityNames = new List<String>();
            civilityNames.addAll(mdmCivility.keySet());
            civilityNames.sort();

            map<String, Gender__c> mdmGender= Gender__c.getAll(); //Gender/Sex
            List<String> genderNames = new List<String>();
            genderNames.addAll(mdmGender.keySet());
            genderNames.sort();

            Map<String, Language__c> mdmLanguage= Language__c.getAll(); //language
            List<String> languageNames = new List<String>();
            languageNames.addAll(mdmLanguage.keySet());
            languageNames.sort();       
                
            Map<String, MDMCountry__c> mdmCountry= MDMCountry__c.getAll();  //country
            List<String> countryNames = new List<String>();
            countryNames.addAll(mdmCountry.keySet());
            countryNames.sort();
            
            Map<String, MDMVehicleRelationType__c> mdmVehicleRelationType= MDMVehicleRelationType__c.getAll(); //vehicle relation type
            List<String> vehicleRelationsNames = new List<String>();
            vehicleRelationsNames.addAll(mdmVehicleRelationType.keySet());
            vehicleRelationsNames.sort();
            
            for( TRVwsdlBcs.clientList bcsClient : iBCSResponse.getCustDataResponse.response.clientList ) 
            {
                //Personal Information
                TRVResponseWebservices.InfoClient infoClient = new TRVResponseWebservices.InfoClient();
                
                infoClient.datasource = 'BCS';
                infoClient.DataSourceOrigin = eParams.DataSourceOrigin;     //DataSource origin
                	
                infoClient.accountBrand = eParams.brand;
                
                if(String.isEmpty(bcsClient.idClient) == false)
                    infoClient.strPartyID = bcsClient.idClient.trim();   //MDM like IdClient for BCS or RBX
                if(String.isEmpty(bcsClient.lastName) == false)
                {
                    infoClient.LastName1 = bcsClient.lastName.trim();
                    infoClient.name = bcsClient.lastName.trim();
            }
                if(String.isEmpty(bcsClient.middleName) == false)
                    infoClient.LastName2 = bcsClient.middleName.trim();
                if(String.isEmpty(bcsClient.birthDay) == false)
                    infoClient.strDOB = bcsClient.birthDay.trim();
                    
                //Country SF
                if (String.isEmpty(eparams.countrySF) == false) 
                    infoClient.countrySF = eparams.countrySF;
                if (String.isEmpty(eparams.AdriaticCountrySF) == false) 
                    infoClient.AdriaticCountrySF = eparams.AdriaticCountrySF;
                if (String.isEmpty(eparams.MiDCECountrySF) == false) 
                    infoClient.MiDCECountrySF = eparams.MiDCECountrySF;
                if (String.isEmpty(eparams.NRCountrySF) == false) 
                    infoClient.NRCountrySF = eparams.NRCountrySF;
                if (String.isEmpty(eparams.PlCountrySF) == false) 
                    infoClient.PlCountrySF = eparams.PlCountrySF;
                if (String.isEmpty(eparams.UkIeCountrySF) == false) 
                    infoClient.UkIeCountrySF = eparams.UkIeCountrySF;
                
  
                    

                
                if (String.isEmpty(bcsClient.title) == false)   
                {
                    for (String civilityName : civilityNames) {
                        MDMCivillity__c civility = mdmCivility.get(civilityName);
                        if (String.isEmpty(civility.value__c) == false)
                            if (civility.Value__c.equalsIgnoreCase(bcsClient.title) && civility.dataSource__c == 'RBX') 
                                infoClient.civility = civility.Civility__c;
                    }
                    if(String.isEmpty(infoClient.civility) == true)
                        infoClient.civility = bcsClient.title; //value by default
                }
                if (String.isEmpty(bcsClient.lang) == false)
                {
                    for (String languageName: languageNames) 
                    {
                        Language__c language = mdmLanguage.get(languageName);
                        if (String.isEmpty(language.value__c) == false)
                            if (language.Value__c.equalsIgnoreCase(bcsClient.lang) && language.dataSource__c == 'RBX') 
                                infoClient.Lang = language.MDMLanguage__c;
                    }
                    if(String.isEmpty(infoClient.Lang) == true)
                        infoClient.Lang = bcsClient.lang;
                }
                if (String.isEmpty(bcsClient.sex) == false)
                {
                    for (String genderName : genderNames) 
                    {
                        Gender__c gender = mdmGender.get(genderName);
                        if (String.isEmpty(gender.value__c) == false)
                            if (gender.Value__c.equalsIgnoreCase(bcsClient.sex) && gender.dataSource__c == 'RBX') 
                                infoClient.sex  = gender.Gender__c;
                    }
                    if(String.isEmpty(infoClient.sex) == true)
                        infoClient.sex  = bcsClient.sex;
                }
            
                
                if(String.isEmpty(bcsClient.firstName) == false)
                    infoClient.FirstName1 = bcsClient.firstName.trim();
                if(String.isEmpty(bcsClient.typeperson) == false)
                    infoClient.strAccountType = bcsClient.typeperson.trim();

                if (infoClient.strAccountType != '' && infoClient.strAccountType == 'P')
                {
                    if(String.isEmpty(bcsClient.ident1) == false)
                        infoClient.localCustomerID1 = bcsClient.ident1.trim();
                    if(String.isEmpty(bcsClient.ident2) == false)
                        infoClient.localCustomerID2 = bcsClient.ident2.trim();
                }
                else
                {
                    if(String.isEmpty(bcsClient.ident1) == false)
                        infoClient.companyID = bcsClient.ident1.trim();
                    if(String.isEmpty(bcsClient.ident2) == false)
                        infoClient.secondCompanyID = bcsClient.ident2.trim();
                }
                
                if(String.isEmpty(bcsClient.ident1) == false)
                    infoClient.siret = bcsClient.ident1;
                if(String.isEmpty(bcsClient.ident2) == false)
                    infoClient.siren = bcsClient.ident2;
        		if(String.isEmpty(bcsClient.soc) == false)
                    infoClient.soc = bcsClient.soc;
                    
                    
                if( bcsClient.BCScommAgreement != null && bcsClient.BCScommAgreement.size() > 0 ) 
                {
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].Global_Comm_Agreement) == false)
                      if (bcsClient.BCScommAgreement[0].Global_Comm_Agreement.equalsIgnoreCase('Y'))
                          infoClient.GlobalCommAgreement = 'Yes'; 
                        else 
                          infoClient.GlobalCommAgreement = 'No';
                          
                    
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].Post_Comm_Agreement) == false)
                      if (bcsClient.BCScommAgreement[0].Post_Comm_Agreement.equalsIgnoreCase('Y'))
                          infoClient.PostCommAgreement = 'Yes';
                        else 
                          infoClient.PostCommAgreement = 'No';
                    
                    
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].Tel_Comm_Agreement) == false)
                      if (bcsClient.BCScommAgreement[0].Tel_Comm_Agreement.equalsIgnoreCase('Y'))
                          infoClient.TelCommAgreement = 'Yes';
                        else 
                          infoClient.TelCommAgreement = 'No';
                    
                    
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].SMS_Comm_Agreement) == false)
                      if (bcsClient.BCScommAgreement[0].SMS_Comm_Agreement.equalsIgnoreCase('Y'))
                          infoClient.SMSCommAgreement = 'Yes';
                        else
                          infoClient.SMSCommAgreement = 'No';
                        
                    //infoClient.FaxCommAgreement = bcsClient.BCScommAgreement[0].Fax_Comm_Agreement;
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].Email_Comm_Agreement) == false)
                      if (bcsClient.BCScommAgreement[0].Email_Comm_Agreement.equalsIgnoreCase('Y'))
                          infoClient.EmailCommAgreement = 'Yes';
                        else 
                          infoClient.EmailCommAgreement = 'No';
                          
                        
                    if(String.isEmpty(bcsClient.BCScommAgreement[0].Preferred_Communication_Method) == false)
                        infoClient.preferredMedia = bcsClient.BCScommAgreement[0].Preferred_Communication_Method; 
                }
                if( bcsClient.contact != null) 
                {
                	system.debug('##Ludo mail : ' + bcsClient.contact.email);
                    if(String.isEmpty(bcsClient.contact.email) == false)
                        infoClient.email = bcsClient.contact.email.trim();
                    infoClient.emailPro = bcsClient.contact.BCSemailPro; 
                    if(String.isEmpty(bcsClient.contact.phoneNum1) == false)
                        infoClient.strFixeLandLine = bcsClient.contact.phoneNum1.trim();
                    if(String.isEmpty(bcsClient.contact.phoneNum2) == false)
                        infoClient.strMobile = bcsClient.contact.phoneNum2.trim();
                    if(String.isEmpty(bcsClient.contact.phoneNum3) == false)
                        infoClient.strFixeLandLinePro = bcsClient.contact.phoneNum3.trim();
                }
                if(bcsClient.address != null) 
                {      
                    if(String.isEmpty(bcsClient.address.strName) == false)
                        infoClient.AddressLine1 = bcsClient.address.strName.trim();          //MDM: strAddressLine
                    if(String.isEmpty(bcsClient.address.compl1) == false)
                        infoClient.compl1 = bcsClient.address.compl1.trim();
                    if(String.isEmpty(bcsClient.address.strNum) == false)          
                        infoClient.StrNum = bcsClient.address.strNum.trim();
                    if(String.isEmpty(bcsClient.address.strType) == false)
                        infoClient.StrType = bcsClient.address.strType.trim();
                    if(String.isEmpty(bcsClient.address.compl2) == false)        
                        infoClient.AddressLine2 = bcsClient.address.compl2.trim();            //MDM/ strAddressLine2
                    
                    
                    if (String.isEmpty(bcsClient.address.countryCode) == false)
                    {
                        for (String countryName : countryNames) 
                        {
                            MDMCountry__c country = mdmCountry.get(countryName);
                            if (String.isEmpty(country.value__c) == false)              
                                if (country.Value__c.equalsIgnoreCase(bcsClient.address.countryCode) && country.dataSource__c == 'RBX') 
                                    infoClient.CountryCode = country.Country__c;
                        }
                        if(String.isEmpty(infoClient.CountryCode ) == true)
                            infoClient.CountryCode = bcsClient.address.countryCode; //value by default
                    }   
                    
                    if( String.isEmpty(bcsClient.address.zip) == false)
                        infoClient.Zip = bcsClient.address.zip.trim();
                    if(String.isEmpty(bcsClient.address.city) == false)
                        infoClient.City = bcsClient.address.city.trim();
                    if(String.isEmpty(bcsClient.address.areaCode) == false)
                        infoClient.region = bcsClient.address.areaCode.trim();
                        
                }
                
                //Vehicle informations
                infoClient.vcle = new List<TRVResponseWebservices.Vehicle>();
                if( bcsClient.vehicleList != null ) 
                {
                    for( TRVwsdlBcs.vehicleList bcsVehicle : bcsClient.vehicleList ) 
                    {
                        TRVResponseWebservices.Vehicle vehicle = new TRVResponseWebservices.Vehicle();
                        if(String.isEmpty(bcsVehicle.vin) == false)
                            vehicle.vin = bcsVehicle.vin.trim();
                        if( String.isEmpty(bcsClient.idClient) == false)
                            vehicle.IdClient = bcsClient.idClient.trim();
                        
                    	if (String.isEmpty(bcsVehicle.brandCode) == false)
                    		vehicle.brandCode = bcsVehicle.brandCode.trim();
                        
                        if(String.isEmpty(bcsVehicle.modelCode) == false)
                            vehicle.modelCode = bcsVehicle.modelCode.trim();
                        if(String.isEmpty(bcsVehicle.modelLabel) == false)
                            vehicle.modelLabel = bcsVehicle.modelLabel.trim();
                        if(String.isEmpty(bcsVehicle.versionLabel) == false)
                            vehicle.versionLabel = bcsVehicle.versionLabel.trim();
                            
                        if(String.isEmpty(bcsVehicle.firstRegistrationDate) == false)
                            vehicle.firstRegistrationDate = Date.valueof(bcsVehicle.firstRegistrationDate.trim()).format();
                        if(String.isEmpty(bcsVehicle.possessionBegin) == false)
                            vehicle.possessionBegin = Date.valueof(bcsVehicle.possessionBegin.trim()).format();
                        if(String.isEmpty(bcsVehicle.possessionEnd) == false)
                            vehicle.possessionEnd = Date.valueof(bcsVehicle.possessionEnd.trim()).format();
                        if(String.isEmpty(bcsVehicle.registrationDate) == false)
                            vehicle.lastRegistrationDate = Date.valueof(bcsVehicle.registrationDate.trim()).format();
                            
                        
                        if(String.isEmpty(bcsVehicle.registration) == false)
                            vehicle.registrationNumber = bcsVehicle.registration.trim();

                        
                        if (String.isEmpty(bcsVehicle.new_x) == false)
                            vehicle.vnvo = bcsVehicle.new_x.trim();
                            
                        if (String.isEmpty(bcsVehicle.purchaseNature) == false)
                        {
                            for (String vehicleName: vehicleRelationsNames) {
                                MDMVehicleRelationType__c vehicleRelationValue = mdmVehicleRelationType.get(vehicleName);
                                if (String.isEmpty(vehicleRelationValue.value__c) == false) 
                                        vehicle.vehicleType = vehicleRelationValue.VehicleRelationType__c;
                            }
                            if(String.isEmpty(vehicle.vehicleType ) == true)
                                vehicle.vehicleType = bcsVehicle.purchaseNature; //value by default
                        }
                        

                        
                        
                        infoClient.vcle.add(vehicle);
                    }
                }
                pDataResponse.response.infoClients.add(infoClient);
                pDataResponse.response.rawResponse = iBCSResponse.toString();
                try
                {
                	pDataResponse.response.rawQuery = bcsDataSource.toString() + ' ------- ' + getCustDataReq.toString();
                }
                catch(Exception e)
                {
                	pDataResponse.response.rawQuery = 'error';
                }
            }
            system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - Result parsing END');
        }
        system.debug('#### TRVWebservicesBCS - mapPersonalDataFields - TRV - END');
        return pDataResponse;
    }

}