/*  Close the previous relation (launched by triggers) 
****************************************************************************
       2014 : Initial revision 
12 Oct 2015 : 15.11 (ChocolateCamel) : Notification
13 Jan 2016 : Patch. Avoid the update of a 
***************************************************************************/
public without sharing class Myr_VehRelEndPreviousRelation_Cls {

	//@return the list of countries to exclude
	private static Set<String> getExcludedCountries() {
		CS04_MYR_Settings__c myrSettings = CS04_MYR_Settings__c.getInstance();
		String countryLimited = '';
		Set<String> setCountries = new Set<String>();
		if( myrSettings != null && !String.isBlank(myrSettings.Myr_LimitClosePrevious_Country__c) ) {
			setCountries.addAll( myrSettings.Myr_LimitClosePrevious_Country__c.split(';') );
		}
		return setCountries;
	}
    
    public static void closePrevious(list <VRE_VehRel__c> listVRE, boolean isInsert, Map <Id, VRE_VehRel__c> oldMap) {
		system.debug('### Myr_VehRelEndPreviousRelation_Cls - <closePrevious> - BEGIN');
        Set<Id> setV = new Set<Id>();
        Set<Id> setRL = new Set<Id>();
        for(VRE_VehRel__c vr : listVRE){
            if (vr.My_Garage_Status__c!=null && vr.My_Garage_Status__c.equalsIgnoreCase('confirmed')){
                setV.add(vr.VIN__c);
                setRL.add(vr.Id);
            }
        }

        if(setRL.size()>0){ 
            List<VRE_VehRel__c> listVREsToClose = [
                SELECT
                    Status__c
                    , My_Vehicle_Delete_Reason__c
                    , My_Vehicle_Delete_Date__c
                    , Account__c 
					, Account__r.Country__c
                    , VIN__r.VehicleBrand__c
                    , VIN__r.Name
                FROM 
                    VRE_VehRel__c
                WHERE 
                    VIN__c IN :setV 
                    AND Id NOT IN :setRL 
                    AND My_Garage_Status__c='confirmed'
					
                ];

			//SDP / 10.02.2016 Do not take into account the given list of countries in the closeprevious function
			// Do not use this list in a NOT IN clause as this is not optimized
			Set<String> setExcludedCountries = getExcludedCountries();
			List<VRE_VehRel__c> listVREsToCloseCleaned = new List<VRE_VehRel__c>();
            for(VRE_VehRel__c vr : listVREsToClose) {
				if( !setExcludedCountries.contains(vr.Account__r.Country__c) ) {
					vr.My_Garage_Status__c='taken';
					vr.My_Vehicle_Delete_Reason__c='10';
					listVREsToCloseCleaned.add(vr);
				}
            }
            update listVREsToCloseCleaned;
        }
		system.debug('### Myr_VehRelEndPreviousRelation_Cls - <closePrevious> - END');
    } 
    
    public static void closePreviousUpdate(list <VRE_VehRel__c> listVRE, boolean isInsert, Map <Id, VRE_VehRel__c> oldMap) {
		system.debug('### Myr_VehRelEndPreviousRelation_Cls - <closePreviousUpdate> - BEGIN');
        Set<Id> setV = new Set<Id>();
        Set<Id> setRL = new Set<Id>();
        String old;
        String newR;
        for(VRE_VehRel__c vr : listVRE){
            old=oldMap.get(vr.Id).My_Garage_Status__c;
            newR=vr.My_Garage_Status__c;
            if (old==null){
                old='';
            }
            if (newR==null){
                newR='';
            }
            if( !old.equalsIgnoreCase('confirmed') &&  newR.equalsIgnoreCase('confirmed')){
                setV.add(vr.vin__c);
                setRL.add(vr.Id);
            }
        }
        if (setRL.size()>0){
            List<VRE_VehRel__c> listRelVeh = 
                [
                SELECT Id, My_Garage_Status__c, Account__c, VIN__r.VehicleBrand__c , VIN__r.Name, Account__r.Country__c
                FROM VRE_VehRel__c 
                WHERE Id NOT IN :setRL
                AND vin__c IN :setV 
                AND My_Garage_Status__c='confirmed'
                ];
            
			//SDP / 10.02.2016 Do not take into account the given list of countries in the closeprevious function
			// Do not use this list in a NOT IN clause as this is not optimized
			Set<String> setExcludedCountries = getExcludedCountries();
			List<VRE_VehRel__c> listVREsToCloseCleaned = new List<VRE_VehRel__c>();
            for(VRE_VehRel__c lr : listRelVeh){
				if( !setExcludedCountries.contains(lr.Account__r.Country__c) ) {
					lr.My_Garage_Status__c='taken';
					lr.My_Vehicle_Delete_Reason__c='10';
					listVREsToCloseCleaned.add(lr);
				}
            }
            update listRelVeh;
        }
		system.debug('### Myr_VehRelEndPreviousRelation_Cls - <closePreviousUpdate> - END');
    }
}