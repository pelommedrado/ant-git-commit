/** Apex Class used to test Myr_CreateCustMessage_Cls
  @author S. Ducamp / Atos
  @date 10.07.2015
 
  Note: 
  Date comparison: Salesforce is simply unable to compare 2 dates !!!! 
  By simply testing the equality between 2 dates (and not datetime), SalesForce is able to say that they are not equal 
  because it is not the same second !!!! (Assertion Failed: Expected: 2015-07-21 08:25:47, Actual: 2015-07-21 08:25:46)
  => so test the date equality with 30 seconds of error margin
 
  ==> this test is dedicated to test massively the synchronous state of the response.
  **/
@isTest
private class Myr_CreateCustMessage_Sync_Test {
	
	private static final Integer maxAcc = 38;

	@testSetup static void setCustomSettings() {
		Test.startTest();
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		//initialize a list of customer message settings and customer message templates for tests
		Myr_Datasets_Test.prepareCustomerMessageSettings();
		//Deactivate synchro ATC to gain more queries
		List<Country_Info__c> countInfos = [SELECT Id, Myr_ATC_Synchronisable__c FROM Country_Info__c];
		for (Country_Info__c cInf : countInfos) {
			cInf.Myr_ATC_Synchronisable__c = false;
		}
		update countInfos;
		//Prepare a list of accounts with or without users, with or without vehicles
		List<Account> listAccUsers = Myr_Datasets_Test.insertPersonalAccounts(maxAcc / 2, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		List<Account> listAccNoUsers = Myr_Datasets_Test.insertPersonalAccounts(maxAcc / 2, Myr_Datasets_Test.UserOptions.NO_USERS);
		//prepare Renault or Dacia Account
		listAccUsers = [SELECT Id, MyR_Status__c, MyD_Status__c FROM Account WHERE Id IN :listAccUsers];
		listAccNoUsers = [SELECT Id, MyR_Status__c, MyD_Status__c FROM Account WHERE Id IN :listAccNoUsers];
		system.AssertEquals(listAccUsers.size(), listAccNoUsers.size());
		for (Integer i = 0; i < listAccUsers.size();++ i) {
			if (Math.mod(i, 2) == 0) {
				listAccUsers[i].MyR_Status__c = system.Label.Myr_Status_Activated;
				listAccNoUsers[i].MyR_Status__c = system.Label.Myr_Status_Created;
			} else {
				listAccUsers[i].MyD_Status__c = system.Label.Myd_Status_Activated;
				listAccNoUsers[i].MyD_Status__c = system.Label.Myd_Status_Created;
			}
		}
		update listAccUsers;
		update listAccNoUsers;
		//Prepare linked vehicles
		List<VEH_Veh__c> listVehicles = Myr_Datasets_Test.getVehicles('CRMSGTSTCL', maxAcc, true);
		system.debug('setCustomSettings - listVehicles.size()=' + listVehicles.size());
		List<VRE_VehRel__c> listRelations = new List<VRE_VehRel__c> ();
		Integer j = 0;
		for( Integer i =0; i < (maxAcc/2); ++i ) {
			listRelations.add(new VRE_VehRel__c(Account__c = listAccUsers[i].Id, VIN__c = listVehicles[j].Id));
			j++;
			listRelations.add(new VRE_VehRel__c(Account__c = listAccNoUsers[i].Id, VIN__c = listVehicles[j].Id));
			j++;
		}
		//while (i < (maxAcc/2)) {
		//	listRelations.add(new VRE_VehRel__c(Account__c = listAccUsers[i].Id, VIN__c = listVehicles[i].Id));
		//	++ i;
		//	listRelations.add(new VRE_VehRel__c(Account__c = listAccNoUsers[i].Id, VIN__c = listVehicles[i].Id));
		//	++ i;
		//}
		insert listRelations;
		Test.stopTest();
	}

	//TEST: Try ot insert 50 messages in different cases: missing parameters, invalid parameters, keep, replace, refresh
	//Check the syncrhonization of the responses code compared to the messages in input
	static testMethod void test_OK_MassiveCreationSyncStatusCode() {
		Test.startTest();
		//Prepare the list of accounts
		List<VRE_VehRel__c> listRelations = [SELECT Id, Account__c, Account__r.Tech_ACCExternalID__c, Account__r.Bcs_Id__c, Account__r.Bcs_Id_Dacia__c, Account__r.PeopleId_CN__c,
		                                     VIN__c, VIN__r.Name, Account__r.isCustomerPortal
											 FROM VRE_VehRel__c];
		List<VRE_VehRel__c> listAccUsers = new List<VRE_VehRel__c> ();
		List<VRE_VehRel__c> listAccNoUsers = new List<VRE_VehRel__c> ();
		List<VRE_VehRel__c> listRelAccNoUsers = new List<VRE_VehRel__c> ();
		List<VRE_VehRel__c> listRelAccUsers = new List<VRE_VehRel__c> ();

		for (VRE_VehRel__c vre : listRelations) {
			if (vre.Account__r.isCustomerPortal) {
				if (listAccUsers.size() < (maxAcc / 4) ) {
					listAccUsers.add(vre);
				} else {
					listRelAccUsers.add(vre);
				}
			} else {
				if (listAccNoUsers.size() < (maxAcc / 4) ) {
					listAccNoUsers.add(vre);
				} else {
					listRelAccNoUsers.add(vre);
				}
			}
		}

		system.debug('### test_OK_MassiveCreationSyncStatusCode - listAccUsers.size()=' + listAccUsers.size());
		system.debug('### test_OK_MassiveCreationSyncStatusCode - listRelAccUsers.size()=' + listRelAccUsers.size());
		system.debug('### test_OK_MassiveCreationSyncStatusCode - listAccNoUsers.size()=' + listAccNoUsers.size());
		system.debug('### test_OK_MassiveCreationSyncStatusCode - listRelAccNoUsers.size()=' + listRelAccNoUsers.size());

		//Get the templates
		List<Customer_Message_Templates__c> listTempl = [SELECT Id, Body__c, Brand__c, Country__c, CtaHref__c, CtaHrefMobile__c, CtaLabel__c, Language__c,
		                                                 MessageType__c, Summary__c, Title__c,
		                                                 MessageType__r.Name, Lifetime__c, MessageType__r.MergeKeys__c, MessageType__r.MergeAction__c,
		                                                 MessageType__r.NbInputs__c, MessageType__r.Type__c,
		                                                 Criticity__c
		                                                 FROM Customer_Message_Templates__c
		                                                 WHERE MessageType__r.Name IN('VEH_OTS', 'GAR_VEH_ADDED', 'DIV_WELCOME')
		                                                 AND Country__c = 'France'
		                                                 AND Language__c = 'fr'
		                                                 AND Brand__c = 'Renault'];
		Customer_Message_Templates__c templateVEHOTS = null;
		Customer_Message_Templates__c templateGARVEHADDED = null;
		Customer_Message_Templates__c templateDIVWELCOME = null;
		for (Customer_Message_Templates__c templ : listTempl) {
			if (templ.MessageType__r.Name.equalsIgnoreCase('VEH_OTS')) {
				templateVEHOTS = templ;
			} else if (templ.MessageType__r.Name.equalsIgnoreCase('GAR_VEH_ADDED')) {
				templateGARVEHADDED = templ;
			} else if (templ.MessageType__r.Name.equalsIgnoreCase('DIV_WELCOME')) {
				templateDIVWELCOME = templ;
			}
		}
		system.assertNotEquals(null, templateVEHOTS);
		system.assertNotEquals(null, templateGARVEHADDED);
		system.assertNotEquals(null, templateDIVWELCOME);

		//Customer_Message_Templates__c templateVEHOTS = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('VEH_OTS', 'France', 'fr', 'Renault');
		//Customer_Message_Templates__c templateGARVEHADDED = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('GAR_VEH_ADDED', 'France', 'fr', 'Renault');
		//Customer_Message_Templates__c templateDIVWELCOME = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DIV_WELCOME', 'France', 'fr', 'Renault');

		//********** PREPARE THE CONTEXT ****************************************************
		List<Customer_Message__c> listExistingMessages = new List<Customer_Message__c> ();
		//====> Prepare existing customer messgers for duplicate research
		listExistingMessages.add(new Customer_Message__c(Account__c = listRelAccNoUsers[0].Account__c, VIN__c = listRelAccNoUsers[0].VIN__c, Template__c = templateVEHOTS.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		listExistingMessages.add(new Customer_Message__c(Account__c = listRelAccNoUsers[1].Account__c, VIN__c = listRelAccNoUsers[1].VIN__c, Template__c = templateVEHOTS.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		listExistingMessages.add(new Customer_Message__c(Account__c = listRelAccNoUsers[3].Account__c, VIN__c = listRelAccNoUsers[3].VIN__c, Template__c = templateGARVEHADDED.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		listExistingMessages.add(new Customer_Message__c(Account__c = listRelAccNoUsers[4].Account__c, VIN__c = listRelAccNoUsers[4].VIN__c, Template__c = templateGARVEHADDED.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		listExistingMessages.add(new Customer_Message__c(Account__c = listAccUsers[6].Account__c, VIN__c = null, Template__c = templateDIVWELCOME.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		listExistingMessages.add(new Customer_Message__c(Account__c = listAccUsers[7].Account__c, VIN__c = null, Template__c = templateDIVWELCOME.Id,
		                                                 Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary', Body__c = 'fakebody'));
		//insert the existing messages
		insert listExistingMessages;
		//====> Prepare wrong messages to induce an exception when trygin to update them (refresh ko)
		system.runAs(Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', true)) {
			List<Customer_Message__c> listCrashMessage = new List<Customer_Message__c> ();
			//Insert a message with no body to induce the crash of the validation CM_VR01_BodyIsMandatory when trying to refresh
			listCrashMessage.add(new Customer_Message__c(Account__c = listRelAccNoUsers[5].Account__c, VIN__c = listRelAccNoUsers[5].VIN__c, Template__c = templateVEHOTS.Id,
			                                             Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary'));
			listCrashMessage.add(new Customer_Message__c(Account__c = listRelAccNoUsers[6].Account__c, VIN__c = listRelAccNoUsers[6].VIN__c, Template__c = templateVEHOTS.Id,
			                                             Channel__c = 'message', Title__c = 'faketitle', Summary__c = 'fakesummary'));
			insert listCrashMessage;
		}

 
		//********** PREPARE THE MSG TO INSERT ***********************************************
		//Prepare the list of messages for test
		List<Myr_CustMessage_Cls> listTestMessages = new List<Myr_CustMessage_Cls> ();
		//====> 5 customer messages are inserted with success
		for (Integer i = 0; i < 5;++ i) {
			Myr_CustMessage_Cls msg = new Myr_CustMessage_Cls();
			msg.accountSfdcId = listAccNoUsers[i].Account__c;
			msg.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
			msg.typeId = 'DEA_EMPTY'; //template with no inputs
			msg.country = 'France';
			msg.language = 'fr';
			listTestMessages.add(msg);
		}
		//=====> 2 following ones are missing parameters
		Myr_CustMessage_Cls msg1 = new Myr_CustMessage_Cls();
		msg1.accountSfdcId = listAccUsers[0].Account__c;
		msg1.typeId = 'DEA_EMPTY';
		msg1.country = 'France';
		msg1.language = 'fr';
		listTestMessages.add(msg1); //missing channels => no more blocking now, channel on template arez inserted by default in this case
		Myr_CustMessage_Cls msg2 = new Myr_CustMessage_Cls();
		msg2.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg2.typeId = 'DEA_EMPTY';
		msg2.country = 'France';
		msg2.language = 'fr';
		listTestMessages.add(msg2); //missing account id
		//=====> 2 following ones are refreshed with success => template VEH_OTS with 2 inputs, matching on VINs
		Myr_CustMessage_Cls msg3 = new Myr_CustMessage_Cls();
		msg3.accountSfdcId = listRelAccNoUsers[0].Account__c;
		msg3.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg3.typeId = 'VEH_OTS';
		msg3.country = 'France';
		msg3.language = 'fr';
		msg3.input1 = 'OTS 12345';
		msg3.input2 = 'CLio IV';
		msg3.vin = listRelAccNoUsers[0].VIN__r.Name;
		listTestMessages.add(msg3);
		Myr_CustMessage_Cls msg4 = new Myr_CustMessage_Cls();
		msg4.accountSfdcId = listRelAccNoUsers[1].Account__c;
		msg4.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg4.typeId = 'VEH_OTS';
		msg4.country = 'France';
		msg4.language = 'fr';
		msg4.input1 = 'OTS 123456';
		msg4.input2 = 'Laguna V2';
		msg4.vin = listRelAccNoUsers[1].VIN__r.Name;
		listTestMessages.add(msg4);
		//=====> following one has a vin not associated to this account
		Myr_CustMessage_Cls msg5 = new Myr_CustMessage_Cls();
		msg5.accountSfdcId = listRelAccNoUsers[2].Account__c;
		msg5.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg5.typeId = 'VEH_OTS';
		msg5.country = 'France';
		msg5.language = 'fr';
		msg5.input1 = 'OTS 123456';
		msg5.input2 = 'Laguna V2';
		msg5.vin = listRelAccNoUsers[1].VIN__r.Name; //not an error: we add an existing vin not associated to this account
		listTestMessages.add(msg5);
		//=====> following one has a not existing template
		Myr_CustMessage_Cls msg6 = new Myr_CustMessage_Cls();
		msg6.accountSfdcId = listAccUsers[1].Account__c;
		msg6.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg6.typeId = 'TRUC'; // fake template
		msg6.country = 'France';
		msg6.language = 'fr';
		listTestMessages.add(msg6);
		//=====> following one has a missing template
		Myr_CustMessage_Cls msg7 = new Myr_CustMessage_Cls();
		msg7.accountSfdcId = listAccUsers[2].Account__c;
		msg7.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg7.country = 'France';
		msg7.language = 'fr';
		listTestMessages.add(msg7); //missing template
		//=====> 2 following ones are replaced with success => template GAR_VEH_ADDED with 1 inputs, matching on VINs
		Myr_CustMessage_Cls msg8 = new Myr_CustMessage_Cls();
		msg8.accountSfdcId = listRelAccNoUsers[3].Account__c;
		msg8.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg8.typeId = 'GAR_VEH_ADDED';
		msg8.country = 'France';
		msg8.language = 'fr';
		msg8.input1 = 'Clio IV';
		msg8.vin = listRelAccNoUsers[3].VIN__r.Name;
		listTestMessages.add(msg8);
		Myr_CustMessage_Cls msg9 = new Myr_CustMessage_Cls();
		msg9.accountSfdcId = listRelAccNoUsers[4].Account__c;
		msg9.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg9.typeId = 'GAR_VEH_ADDED';
		msg9.country = 'France';
		msg9.language = 'fr';
		msg9.input1 = 'Clio IV';
		msg9.vin = listRelAccNoUsers[4].VIN__r.Name;
		listTestMessages.add(msg9);
		//======> following one is missing country in input with no valid associated user
		Myr_CustMessage_Cls msg10 = new Myr_CustMessage_Cls();
		msg10.accountSfdcId = listAccNoUsers[5].Account__c;
		msg10.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg10.typeId = 'DEA_EMPTY';
		msg10.language = 'fr';
		listTestMessages.add(msg10); //missing country
		//======> following one is missing country in input with a user including no country 
		Myr_CustMessage_Cls msg11 = new Myr_CustMessage_Cls();
		msg11.accountSfdcId = listAccUsers[3].Account__c;
		msg11.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg11.typeId = 'DEA_EMPTY';
		msg11.language = 'fr';
		listTestMessages.add(msg11); //missing country and user with no recorddefaultcountry
		//=====> 2 following messages have not enough inputs for templates
		Myr_CustMessage_Cls msg12 = new Myr_CustMessage_Cls();
		msg12.accountSfdcId = listAccUsers[4].Account__c;
		msg12.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg12.typeId = 'CUS_CASE'; //3 inputs required, only 1 given
		msg12.country = 'France';
		msg12.language = 'fr';
		msg12.input1 = 'Case 12345';
		listTestMessages.add(msg12); //2 inputs missing
		Myr_CustMessage_Cls msg13 = new Myr_CustMessage_Cls();
		msg13.accountSfdcId = listAccUsers[5].Account__c;
		msg13.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg13.typeId = 'CUS_CASE'; //3 inputs required, only 2 given
		msg13.country = 'France';
		msg13.language = 'fr';
		msg13.input1 = 'Case 12345';
		msg13.input2 = 'Clio';
		listTestMessages.add(msg13); //1 input missing
		//=====> 2 following ones are kept with success => template DIV_WELCOME with no inputs, no matching
		Myr_CustMessage_Cls msg14 = new Myr_CustMessage_Cls();
		msg14.accountSfdcId = listAccUsers[6].Account__c;
		msg14.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg14.typeId = 'DIV_WELCOME';
		msg14.country = 'France';
		msg14.language = 'fr';
		listTestMessages.add(msg14);
		Myr_CustMessage_Cls msg15 = new Myr_CustMessage_Cls();
		msg15.accountSfdcId = listAccUsers[7].Account__c;
		msg15.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg15.typeId = 'DIV_WELCOME';
		msg15.country = 'France';
		msg15.language = 'fr';
		listTestMessages.add(msg15);
		//=====> 2 following ones are refreshed FAILED (FIELD_CUSTOM_VALIDATION_EXCEPTION) => template VEH_OTS with 2 inputs, matching on VINs
		Myr_CustMessage_Cls msg16 = new Myr_CustMessage_Cls();
		msg16.accountSfdcId = listRelAccNoUsers[5].Account__c;
		msg16.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg16.typeId = 'VEH_OTS';
		msg16.country = 'France';
		msg16.language = 'fr';
		msg16.input1 = 'OTS 12345';
		msg16.input2 = 'Clio IV';
		msg16.vin = listRelAccNoUsers[5].VIN__r.Name;
		listTestMessages.add(msg16);
		Myr_CustMessage_Cls msg17 = new Myr_CustMessage_Cls();
		msg17.accountSfdcId = listRelAccNoUsers[6].Account__c;
		msg17.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.email };
		msg17.typeId = 'VEH_OTS';
		msg17.country = 'France';
		msg17.language = 'fr';
		msg17.input1 = 'OTS 123456';
		msg17.input2 = 'Laguna V2';
		msg17.vin = listRelAccNoUsers[6].VIN__r.Name; 
		listTestMessages.add(msg17);
 

		//********** LAUNCH THE TEST **********************************************************
		Myr_CreateCustMessage_Cls.CreateMsgResponse response = null;
		system.runAs(Myr_Datasets_Test.getCustMsgUser('Myr2', 'CreateCustMsgTst', false)) {
			response = Myr_CreateCustMessage_Cls.createMessages(listTestMessages);
		}
		Test.stopTest();

		//********** CHECK THE RESULTS ********************************************************
		system.assertNotEquals(null, response);
		//system.assertEquals(Myr_CreateCustMessage_Cls.Status.OK, response.status);
		//system.assertEquals('OK, check messageStatus list for more details', response.message);
		system.assertEquals(listTestMessages.size(), response.listStatus.size());

		//====> 5 first customer messages are inserted
		system.assertEquals('WS06MS000', response.listStatus[0].code);
		system.assertEquals('Notification created with success', response.listStatus[0].message);
		system.assertEquals( listAccNoUsers[0].Account__c, response.listStatus[0].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[0].input.typeid );
		system.assertEquals( 'France', response.listStatus[0].input.country );
		system.assertEquals( 'fr', response.listStatus[0].input.language );
		system.assertEquals( 'Renault', response.listStatus[0].input.brand );

		system.assertEquals('WS06MS000', response.listStatus[1].code);
		system.assertEquals('Notification created with success', response.listStatus[1].message);
		system.assertEquals( listAccNoUsers[1].Account__c, response.listStatus[1].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[1].input.typeid );
		system.assertEquals( 'France', response.listStatus[1].input.country );
		system.assertEquals( 'fr', response.listStatus[1].input.language );
		system.assertEquals( 'Renault', response.listStatus[1].input.brand );

		system.assertEquals('WS06MS000', response.listStatus[2].code);
		system.assertEquals('Notification created with success', response.listStatus[2].message);
		system.assertEquals( listAccNoUsers[2].Account__c, response.listStatus[2].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[2].input.typeid );
		system.assertEquals( 'France', response.listStatus[2].input.country );
		system.assertEquals( 'fr', response.listStatus[2].input.language );
		system.assertEquals( 'Renault', response.listStatus[2].input.brand );

		system.assertEquals('WS06MS000', response.listStatus[3].code);
		system.assertEquals('Notification created with success', response.listStatus[3].message);
		system.assertEquals( listAccNoUsers[3].Account__c, response.listStatus[3].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[3].input.typeid );
		system.assertEquals( 'France', response.listStatus[3].input.country );
		system.assertEquals( 'fr', response.listStatus[3].input.language );
		system.assertEquals( 'Renault', response.listStatus[3].input.brand );

		system.assertEquals('WS06MS000', response.listStatus[4].code);
		system.assertEquals('Notification created with success', response.listStatus[4].message);
		system.assertEquals( listAccNoUsers[4].Account__c, response.listStatus[4].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[4].input.typeid );
		system.assertEquals( 'France', response.listStatus[4].input.country );
		system.assertEquals( 'fr', response.listStatus[4].input.language );
		system.assertEquals( 'Renault', response.listStatus[4].input.brand );

		system.assertEquals('WS06MS000', response.listStatus[5].code);
		system.assertEquals('Notification created with success', response.listStatus[5].message);
		system.assertEquals( listAccUsers[0].Account__c, response.listStatus[5].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[5].input.typeid );
		system.assertEquals( 'France', response.listStatus[5].input.country );
		system.assertEquals( 'fr', response.listStatus[5].input.language );
		system.assertEquals( 'Renault', response.listStatus[5].input.brand );

		//=====> Following one is missing parameters
		system.assertEquals('WS06MS501', response.listStatus[6].code);
		system.assertEquals('Missing parameter: accountSfdcId or goldenId have to be specified to retrieve the account', response.listStatus[6].message);
		system.assertEquals( null, response.listStatus[6].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[6].input.typeid );
		system.assertEquals( 'France', response.listStatus[6].input.country );
		system.assertEquals( 'fr', response.listStatus[6].input.language );
		system.assertEquals( 'Renault', response.listStatus[6].input.brand );

		//=====> 2 following ones are refreshed with success parameters
		system.assertEquals('WS06MS002', response.listStatus[7].code);
		system.assertEquals('Notification refreshed with success', response.listStatus[7].message);
		system.assertEquals( listRelAccNoUsers[0].Account__c, response.listStatus[7].input.accountSfdcId );
		system.assertEquals( 'VEH_OTS', response.listStatus[7].input.typeid );
		system.assertEquals( 'France', response.listStatus[7].input.country );
		system.assertEquals( 'fr', response.listStatus[7].input.language );
		system.assertEquals( 'Renault', response.listStatus[7].input.brand );

		system.assertEquals('WS06MS002', response.listStatus[8].code);
		system.assertEquals('Notification refreshed with success', response.listStatus[8].message);
		system.assertEquals( listRelAccNoUsers[1].Account__c, response.listStatus[8].input.accountSfdcId );
		system.assertEquals( 'VEH_OTS', response.listStatus[8].input.typeid );
		system.assertEquals( 'France', response.listStatus[8].input.country );
		system.assertEquals( 'fr', response.listStatus[8].input.language );
		system.assertEquals( 'Renault', response.listStatus[8].input.brand );

		//=====> following one has a vin not associated to this account
		system.assertEquals('WS06MS505', response.listStatus[9].code);
		system.assertEquals('Vin ' + listRelAccNoUsers[1].VIN__r.Name + ' is not linked to the account ' + listRelAccNoUsers[2].Account__c + ' or this vin does not exist', response.listStatus[9].message);
		system.assertEquals( listRelAccNoUsers[2].Account__c, response.listStatus[9].input.accountSfdcId );
		system.assertEquals( 'VEH_OTS', response.listStatus[9].input.typeid );
		system.assertEquals( 'France', response.listStatus[9].input.country );
		system.assertEquals( 'fr', response.listStatus[9].input.language );
		system.assertEquals( 'Renault', response.listStatus[9].input.brand );

		//=====> following one has a not existing template
		system.assertEquals('WS06MS502', response.listStatus[10].code);
		system.assertEquals('Invalid parameter: template not found (TRUC,Renault,France,fr)', response.listStatus[10].message);
		system.assertEquals( listAccUsers[1].Account__c, response.listStatus[10].input.accountSfdcId );
		system.assertEquals( 'TRUC', response.listStatus[10].input.typeid );
		system.assertEquals( 'France', response.listStatus[10].input.country );
		system.assertEquals( 'fr', response.listStatus[10].input.language );
		system.assertEquals( 'Renault', response.listStatus[10].input.brand );

		//=====> following one has a missing template 
		system.assertEquals('WS06MS501', response.listStatus[11].code);
		system.assertEquals('Missing parameter: typeId is mandatory', response.listStatus[11].message);
		system.assertEquals( listAccUsers[2].Account__c, response.listStatus[11].input.accountSfdcId );
		system.assertEquals( null, response.listStatus[11].input.typeid );
		system.assertEquals( 'France', response.listStatus[11].input.country );
		system.assertEquals( 'fr', response.listStatus[11].input.language );
		system.assertEquals( 'Renault', response.listStatus[11].input.brand );

		//=====> 2 following ones are replaced with success => template GAR_VEH_ADDED with 1 inputs, matching on VINs
		system.assertEquals('WS06MS001', response.listStatus[12].code);
		system.assertEquals('Notification replaced with success', response.listStatus[12].message);
		system.assertEquals( listRelAccNoUsers[3].Account__c, response.listStatus[12].input.accountSfdcId );
		system.assertEquals( 'GAR_VEH_ADDED', response.listStatus[12].input.typeid );
		system.assertEquals( 'France', response.listStatus[12].input.country );
		system.assertEquals( 'fr', response.listStatus[11].input.language );
		system.assertEquals( 'Renault', response.listStatus[12].input.brand );

		system.assertEquals('WS06MS001', response.listStatus[13].code);
		system.assertEquals('Notification replaced with success', response.listStatus[13].message);
		system.assertEquals( listRelAccNoUsers[4].Account__c, response.listStatus[13].input.accountSfdcId );
		system.assertEquals( 'GAR_VEH_ADDED', response.listStatus[13].input.typeid );
		system.assertEquals( 'France', response.listStatus[13].input.country );
		system.assertEquals( 'fr', response.listStatus[13].input.language );
		system.assertEquals( 'Renault', response.listStatus[13].input.brand );

		//======> following one is missing country in input with no valid associated user
		system.assertEquals('WS06MS502', response.listStatus[14].code);
		system.assertEquals('Invalid parameter: country and/or language not given in inputs or not found on the user linked to the account', response.listStatus[14].message);
		system.assertEquals( listAccNoUsers[5].Account__c, response.listStatus[14].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[14].input.typeid );
		system.assertEquals( null, response.listStatus[14].input.country );
		system.assertEquals( 'fr', response.listStatus[14].input.language );
		system.assertEquals( 'Renault', response.listStatus[14].input.brand );

		//======> following one is missing country in input with a user including no country 
		system.assertEquals('WS06MS502', response.listStatus[15].code);
		system.assertEquals('Invalid parameter: country and/or language not given in inputs or not found on the user linked to the account', response.listStatus[15].message);
		system.assertEquals( listAccUsers[3].Account__c, response.listStatus[15].input.accountSfdcId );
		system.assertEquals( 'DEA_EMPTY', response.listStatus[15].input.typeid );
		system.assertEquals( null, response.listStatus[15].input.country );
		system.assertEquals( 'fr', response.listStatus[15].input.language );
		system.assertEquals( 'Renault', response.listStatus[15].input.brand );

		//=====> 2 following messages have not enough inputs for templates
		system.assertEquals('WS06MS503', response.listStatus[16].code);
		system.assertEquals('Missing input fields for the corresponding template: CUS_CASE(3 required, 1 given)', response.listStatus[16].message);
		system.assertEquals( listAccUsers[4].Account__c, response.listStatus[16].input.accountSfdcId );
		system.assertEquals( 'CUS_CASE', response.listStatus[16].input.typeid );
		system.assertEquals( 'France', response.listStatus[16].input.country );
		system.assertEquals( 'fr', response.listStatus[16].input.language );
		system.assertEquals( 'Renault', response.listStatus[16].input.brand );

		system.assertEquals('WS06MS503', response.listStatus[17].code);
		system.assertEquals('Missing input fields for the corresponding template: CUS_CASE(3 required, 2 given)', response.listStatus[17].message);
		system.assertEquals( listAccUsers[5].Account__c, response.listStatus[17].input.accountSfdcId );
		system.assertEquals( 'CUS_CASE', response.listStatus[17].input.typeid );
		system.assertEquals( 'France', response.listStatus[17].input.country );
		system.assertEquals( 'fr', response.listStatus[17].input.language );
		system.assertEquals( 'Renault', response.listStatus[17].input.brand );

		//=====> 2 following ones are kept with success => template DIV_WELCOME with no inputs, no matching
		system.assertEquals('WS06MS003', response.listStatus[18].code);
		system.assertEquals('Notification kept with success', response.listStatus[18].message);
		system.assertEquals( listAccUsers[6].Account__c, response.listStatus[18].input.accountSfdcId );
		system.assertEquals( 'DIV_WELCOME', response.listStatus[18].input.typeid );
		system.assertEquals( 'France', response.listStatus[18].input.country );
		system.assertEquals( 'fr', response.listStatus[18].input.language );
		system.assertEquals( 'Renault', response.listStatus[18].input.brand );

		system.assertEquals('WS06MS003', response.listStatus[19].code);
		system.assertEquals('Notification kept with success', response.listStatus[19].message);
		system.assertEquals( listAccUsers[7].Account__c, response.listStatus[19].input.accountSfdcId );
		system.assertEquals( 'DIV_WELCOME', response.listStatus[19].input.typeid );
		system.assertEquals( 'France', response.listStatus[19].input.country );
		system.assertEquals( 'fr', response.listStatus[19].input.language );
		system.assertEquals( 'Renault', response.listStatus[19].input.brand );

		//=====> 2 following ones are refreshed FAILED (FIELD_CUSTOM_VALIDATION_EXCEPTION) => template VEH_OTS with 2 inputs, matching on VINs
		system.assertEquals('WS06MS522', response.listStatus[20].code);
		system.assertEquals('Failed to refresh notification: Body is mandatory', response.listStatus[20].message);
		system.assertEquals( listRelAccNoUsers[5].Account__c, response.listStatus[20].input.accountSfdcId );
		system.assertEquals( 'VEH_OTS', response.listStatus[20].input.typeid );
		system.assertEquals( 'France', response.listStatus[20].input.country );
		system.assertEquals( 'fr', response.listStatus[20].input.language );
		system.assertEquals( 'Renault', response.listStatus[20].input.brand );

		system.assertEquals('WS06MS522', response.listStatus[21].code);
		system.assertEquals('Failed to refresh notification: Body is mandatory', response.listStatus[21].message);
		system.assertEquals( listRelAccNoUsers[6].Account__c, response.listStatus[21].input.accountSfdcId );
		system.assertEquals( 'VEH_OTS', response.listStatus[21].input.typeid );
		system.assertEquals( 'France', response.listStatus[21].input.country );
		system.assertEquals( 'fr', response.listStatus[21].input.language );
		system.assertEquals( 'Renault', response.listStatus[21].input.brand );
	}
	
}