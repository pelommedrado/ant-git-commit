/**
 * Apex Class to test MDM webservice
 */
@isTest
private class Myr_PersonalDataMDM_Cls_Test {

	@testsetup 
    static void prepareCustomSettings() {
        Myr_Datasets_Test.prepareMyrCustomSettings();
		list<WebServicesInfo__c> listws= new list<WebServicesInfo__c>();
		WebServicesInfo__c wsinfo = new WebServicesInfo__c();  
		wsinfo.name = 'MDM'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CMDM_Renault_uat_1_6';  
		listws.add(wsinfo); 
		wsinfo = new WebServicesInfo__c(); 
		wsinfo.name = 'BCS'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/BCS_CrmGetCustDataService';
		listws.add(wsinfo);
		wsinfo = new WebServicesInfo__c(); 
		wsinfo.name = 'RBX'; 
		wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CrmGetCustData_4_1';
		listws.add(wsinfo);
		insert listws; 
		MDMCivillity__c civility = new MDMCivillity__c();
		civility.Name = 'MDM-M';
		civility.value__c = 'M';
		civility.Civility__c = 'Mr.';
		civility.dataSource__c='MDM';
		insert civility;
		Gender__c genderM = new Gender__c();
		genderM.Name = 'MDM-M';
		genderM.Country__c = '';
		genderM.value__c = 'M';
		genderM.Gender__c = 'Man';
		genderM.datasource__c='MDM';
		insert genderM;
		MDMPartySubType__c subType = new MDMPartySubType__c();
		subType.Name = 'MDM-AC';
		subType.dataSource__c = 'MDM';
		subType.sub__c = 'Craftsman/Retailer';
		subType.value__c = 'AC';
		insert subType;
		MDMFinancialStatus__c finance = new MDMFinancialStatus__c();
		finance.Name = 'MDM-ACT';
		finance.datasource__c = 'MDM';
		finance.value__c = 'ACT';
		finance.FinancialStatus__c = 'ACT';
		insert finance;
		MaritalStatus__c marital = new MaritalStatus__c();
		marital.Name = 'MDM-Single';
		marital.value__c = 'Single';
		marital.datasource__c = 'MDM';
		marital.MaritalStatus__c = 'Single';
		insert marital;
		MDMCategory__c category = new MDMCategory__c();
		category.Name = 'MDM-B1';
		category.value__c = 'B1';
		category.datasource__c = 'MDM';
		category.JobClass__c = 'Employee';
		insert category;
		MDMPartySegment__c partySeg = new MDMPartySegment__c();
		partySeg.Name = 'MDM-Others';
		partySeg.value__c = 'Others';
		partySeg.datasource__c = 'MDM';
		partySeg.PartySegment__c = 'Others';
		insert partySeg;
		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
    } 

	/** Test search party **/
    static testMethod void testSearchParty() { 
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'France';
        c2.Country_Code_2L__c = 'FR';
        c2.Country_Code_3L__c = 'FRA';
        c2.DataSource__c = 'MDM';
        c2.Language__c = 'FR';
        insert c2;

    	Test.setMock(WebServiceMock.class, new Myr_PersonalDataMDM_MK(null));
    	//test the search party
        Myr_PersonalData_WS.RequestParameters request = new Myr_PersonalData_WS.RequestParameters();
        request.lastname = 'LAMBERT';
       	request.firstname = 'JONATHAN';
       	request.email='jonathan.lambert@yopmail.com';
       	request.idClient='';
       	request.country='France';
		system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			Test.startTest();
       			TRVResponseWebservices response = TRVWebservicesManager.requestSearchParty(Myr_PersonalDataMDM_Cls.prepareTRVMDMParameters(request));
			Test.stopTest();
       		system.assertNotEquals(null, response);
       		system.assertEquals( Myr_PersonalData_WS.DataSourceType.MDM.name(), response.response.dataSource);
       		system.assertEquals( 2, response.response.infoclients.size() );
       		system.assertEquals( 'LAMBERT', response.response.infoclients[0].LastName1 );
       		system.assertEquals( 'LAMBERT', response.response.infoclients[1].LastName1 );
		}
    }
    
    /** Test Detailed response */
    static testMethod void testGetParty() {
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'France';
        c2.Country_Code_2L__c = 'FR';
        c2.Country_Code_3L__c = 'FRA';
        c2.DataSource__c = 'MDM';
        c2.Language__c = 'FR';
        insert c2;
    	Test.setMock(WebServiceMock.class, new Myr_PersonalDataMDM_MK(Myr_PersonalDataMDM_MK.MODE.MYRENAULT));
    	//test the get party
		Myr_PersonalData_WS.RequestParameters request = new Myr_PersonalData_WS.RequestParameters();
       	request.strPartyID = '456789';
		system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			Test.startTest();
				TRVResponseWebservices response = TRVWebservicesManager.requestGetParty(Myr_PersonalDataMDM_Cls.prepareTRVMDMParameters(request));
			Test.stopTest();
       		system.assertNotEquals(null, response);
       		system.assertEquals( Myr_PersonalData_WS.DataSourceType.MDM.name(), response.response.dataSource);
       		system.assertEquals( 1, response.response.infoclients.size() );
       		system.assertEquals('LAMBERT' + 'JONATHAN',response.response.infoclients[0].lastname1 + response.response.infoclients[0].firstname1);
			system.assertEquals( 'mag.perrin21@free.fr', response.response.infoclients[0].email);
		}
    }
    
     /** Test MyRenault mode */
    static testMethod void testMyRenault() {
		Country_Info__c c2 = new Country_Info__c();
        c2.Name = 'France';
        c2.Country_Code_2L__c = 'FR';
        c2.Country_Code_3L__c = 'FRA';
        c2.DataSource__c = 'MDM';
        c2.Language__c = 'FR';
        insert c2;
    	Test.setMock(WebServiceMock.class, new Myr_PersonalDataMDM_MK(Myr_PersonalDataMDM_MK.MODE.MYRENAULT));
    	//test the myrenault mode
    	TRVRequestParametersWebServices.RequestParameters request = new TRVRequestParametersWebServices.RequestParameters();
        request.lastname = 'LAMBERT';
       	request.firstname = 'JONATHAN';
       	request.email='jonathan.lambert@yopmail.com';
       	request.idClient='';
       	request.country='France';
		system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			Test.startTest();
    			TRVResponseWebservices response = Myr_PersonalData_WS.getPersonalData(request); 
			Test.stopTest();
       		system.assertNotEquals(null, response);
       		system.assertEquals( 'MDM', response.response.dataSource);
       		system.assertEquals( 1, response.response.infoclients.size() );
       		TRVResponseWebservices.InfoClient info = response.response.infoclients[0];
       		system.assertEquals('LAMBERT' + 'JONATHAN',info.lastname1 + info.firstname1);
			system.assertEquals( 'mag.perrin21@free.fr', info.email);
			system.assertEquals( 3, info.vcle.size() );
			system.assertEquals( 'VF1BMSE0637005985', info.vcle[0].vin );
		}
    }
}