/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class JBintFireBulkEvent {
    global JBintFireBulkEvent() {

    }
    @InvocableMethod(label='Fire Bulk JB Event' description='Fires off a bulk event for the given set of params')
    global static void fireBulkEvent(List<et4ae5.JBIntFireEventParams> fireEventParamList) {

    }
}
