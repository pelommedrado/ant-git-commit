public with sharing class FDealer2SimpleController {

    private static final String CSV_SEPARATE = ';';

	public String invoiceFrom					{get;set;}
	public String invoiceTo						{get;set;}

	public String sendFrom						{get;set;}
	public String sendTo						{get;set;}

	public String birEmissao					{get;set;}

	public String registerTypeSelected			{get;set;}
	public String statusSelected				{get;set;}

	public List<SelectOption> registerType 		{get;set;}
	public List<SelectOption> status 			{get;set;}

	public List<FaturaDealer2__c> invoiceList	{get;set;}

	public DateTime dataIni {get;set;}
	public DateTime dataFim {get;set;}

	public String Address 							= FaturaDealer2__c.Address__c.getDescribe().getLabel();
	public String Address_Complement				= FaturaDealer2__c.Address_Complement__c.getDescribe().getLabel();
	public String BIR_Group_Issuer					= FaturaDealer2__c.BIR_Group_Issuer__c.getDescribe().getLabel();
	public String BIR_Name_NFE_Issuer				= FaturaDealer2__c.BIR_Name_NFE_Issuer__c.getDescribe().getLabel();
	public String BIR_NFE_Issuer					= FaturaDealer2__c.BIR_NFE_Issuer__c.getDescribe().getLabel();
	public String Birth_Date						= FaturaDealer2__c.Birth_Date__c.getDescribe().getLabel();
	public String isCancelled						= FaturaDealer2__c.isCancelled__c.getDescribe().getLabel();
	public String City								= FaturaDealer2__c.City__c.getDescribe().getLabel();
	public String Customer_Email					= FaturaDealer2__c.Customer_Email__c.getDescribe().getLabel();
	public String Customer_Email_Mirror 			= FaturaDealer2__c.Customer_Email_Mirror__c.getDescribe().getLabel();
	public String Customer_Identification_Number	= FaturaDealer2__c.Customer_Identification_Number__c.getDescribe().getLabel();
	public String Customer_Name 					= FaturaDealer2__c.Customer_Name__c.getDescribe().getLabel();
	public String Customer_Type 					= FaturaDealer2__c.Customer_Type__c.getDescribe().getLabel();
	public String DDD1								= FaturaDealer2__c.DDD1__c.getDescribe().getLabel();
	public String DDD2								= FaturaDealer2__c.DDD2__c.getDescribe().getLabel();
	public String DDD3								= FaturaDealer2__c.DDD3__c.getDescribe().getLabel();
	public String Delivery_Date 					= FaturaDealer2__c.Delivery_Date__c.getDescribe().getLabel();
	public String Error_Messages 					= FaturaDealer2__c.Error_Messages__c.getDescribe().getLabel();
	public String External_Key 						= FaturaDealer2__c.External_Key__c.getDescribe().getLabel();
	public String Group_Name_Issuer 				= FaturaDealer2__c.Group_Name_Issuer__c.getDescribe().getLabel();
	public String Integration_Date 					= FaturaDealer2__c.Integration_Date__c.getDescribe().getLabel();
	public String Integration_Protocol 				= FaturaDealer2__c.Integration_Protocol__c.getDescribe().getLabel();
	public String Invoice_Data 						= FaturaDealer2__c.Invoice_Data__c.getDescribe().getLabel();
	public String Invoice_Number 					= FaturaDealer2__c.Invoice_Number__c.getDescribe().getLabel();
	public String Invoice_Serie 					= FaturaDealer2__c.Invoice_Serie__c.getDescribe().getLabel();
	public String Invoice_Type 						= FaturaDealer2__c.Invoice_Type__c.getDescribe().getLabel();
	public String Manufacturing_Year 				= FaturaDealer2__c.Manufacturing_Year__c.getDescribe().getLabel();
	public String Model_Year 						= FaturaDealer2__c.Model_Year__c.getDescribe().getLabel();
	public String Neighborhood 						= FaturaDealer2__c.Neighborhood__c.getDescribe().getLabel();
	public String TrackNumber 						= FaturaDealer2__c.Number__c.getDescribe().getLabel();
	public String Payment_Type 						= FaturaDealer2__c.Payment_Type__c.getDescribe().getLabel();
	public String Phone_1 							= FaturaDealer2__c.Phone_1__c.getDescribe().getLabel();
	public String Phone_2 							= FaturaDealer2__c.Phone_2__c.getDescribe().getLabel();
	public String Phone_3 							= FaturaDealer2__c.Phone_3__c.getDescribe().getLabel();
	public String Postal_Code 						= FaturaDealer2__c.Postal_Code__c.getDescribe().getLabel();
	public String Region 							= FaturaDealer2__c.Region__c.getDescribe().getLabel();
	public String isReturned 						= FaturaDealer2__c.isReturned__c.getDescribe().getLabel();
	public String Sector 							= FaturaDealer2__c.Sector__c.getDescribe().getLabel();
	public String Sequential_File 					= FaturaDealer2__c.Sequential_File__c.getDescribe().getLabel();
	public String Shipping_BIR 						= FaturaDealer2__c.Shipping_BIR__c.getDescribe().getLabel();
	public String State 							= FaturaDealer2__c.State__c.getDescribe().getLabel();
	public String StatusInvoice	 					= FaturaDealer2__c.Status__c.getDescribe().getLabel();
	public String VIN 								= FaturaDealer2__c.VIN__c.getDescribe().getLabel();

	public FDealer2SimpleController() {
		System.debug('execução');

		invoiceList = new List<faturaDealer2__c>();

		registerType = new List<SelectOption>();
		registerType.add(new SelectOption(Utils.getRecordTypeId('FaturaDealer2__c','Invoice'), 'Notas Fiscais'));
		registerType.add(new SelectOption(Utils.getRecordTypeId('FaturaDealer2__c','Register'), 'Dados Cadastrais'));

		status = new List<SelectOption>();
		status.add(new SelectOption('\'Unprocessed\'', 'Não processados'));
		status.add(new SelectOption('\'Invalid\'', 'Inválidos'));
        status.add(new SelectOption('\'Valid\'', 'Válidos'));
		status.add(new SelectOption('\'Invalid\' OR Status__c = \'Unprocessed\' OR Status__c = \'Valid\'', 'Todos'));

		cleanFilter();
	}

	public void cleanFilter(){
		invoiceFrom	 			= null;
		invoiceTo				= null;
		sendFrom				= null;
		sendTo					= null;
		birEmissao				= null;
		registerTypeSelected	= null;
		statusSelected			= null;

		invoiceList.clear();
	}

	public PageReference searchInvoice(){

        /*if(String.isNotEmpty(invoiceFrom)) {
		    Date data = Datetime.now().date().addMonths(-3);
		    System.debug(data);
		    System.debug(toDate(invoiceFrom));

			    if(data > toDate(invoiceFrom)) {
			    	Apexpages.addMessage(new Apexpages.Message(
				      ApexPages.Severity.WARNING,
				      'A data mínima para início da análise é '+ Datetime.now().addMonths(-3).format('dd/MM/YYYY') ));
				  	return null;
					}
        }*/

        if(String.isNotEmpty(invoiceTo)) {
            Integer d = toDate(invoiceTo).daysBetween(Datetime.now().date());
            if(d < 0) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.WARNING,
                    'A data máxima para término da análise é ' + Datetime.now().format('dd/MM/YYYY')));
            }
        }

		if(!String.isEmpty(invoiceFrom)) {
			Date data = toDate(invoiceFrom);
			dataIni = DateTime.newInstance(data.year(), data.month(), data.day(), 0, 0 ,0);
		}

		if(!String.isEmpty(invoiceTo)) {
		 	Date data = toDate(invoiceTo);
			dataFim = DateTime.newInstance(data.year(), data.month(), data.day(), 23, 59 ,59);
		}

		System.debug('dataIni:' + dataIni);
		System.debug('dataFim:' + dataFim);

        String query = buildQuery();

        System.debug('Query:' + query);

        this.invoiceList = Database.Query(query);

        return null;
	}

	public PageReference searchAllInvoice(){

        Pagereference page;

        page = buildCsv();

        return page;
	}

	private String bodyBuild(List<FaturaDealer2__c> invoices){

        String str = line(new String[] {
            format(Address),
			format(Address_Complement),
			format(BIR_Group_Issuer),
			format(BIR_Name_NFE_Issuer),
			format(BIR_NFE_Issuer),
			format(Birth_Date),
			format(City),
			format(Customer_Email),
			format(Customer_Email_Mirror),
			format(Customer_Identification_Number),
			format(Customer_Name),
			format(Customer_Type),
			format(DDD1),
			format(DDD2),
			format(DDD3),
			format(Delivery_Date),
			format(Error_Messages),
			format(External_Key),
			format(Group_Name_Issuer),
			format(Integration_Date),
			format(Integration_Protocol),
			format(Invoice_Data),
			format(Invoice_Number),
			format(Invoice_Serie),
			format(Invoice_Type),
			format(Manufacturing_Year),
			format(Model_Year),
			format(Neighborhood),
			format(TrackNumber),
			format(Payment_Type),
			format(Phone_1),
			format(Phone_2),
			format(Phone_3),
			format(Postal_Code),
			format(Region),
			format(Sector),
			format(Sequential_File),
			format(Shipping_BIR),
			format(State),
			format(StatusInvoice),
			format(VIN)
        });
        for (FaturaDealer2__c fatura : invoices) {
            str += line(new String[] {
                format(fatura.Address__c),
				format(fatura.Address_Complement__c),
				format(fatura.BIR_Group_Issuer__c),
				format(fatura.BIR_Name_NFE_Issuer__c),
				format(fatura.BIR_NFE_Issuer__c),
				format(fatura.Birth_Date__c),
				format(fatura.City__c),
				format(fatura.Customer_Email__c),
				format(fatura.Customer_Email_Mirror__c),
				format(fatura.Customer_Identification_Number__c),
				format(fatura.Customer_Name__c),
				format(fatura.Customer_Type__c),
				format(fatura.DDD1__c),
				format(fatura.DDD2__c),
				format(fatura.DDD3__c),
				format(fatura.Delivery_Date__c),
				format(fatura.Error_Messages__c),
				format(fatura.External_Key__c),
				format(fatura.Group_Name_Issuer__c),
				format(fatura.Integration_Date__c),
				format(fatura.Integration_Protocol__c),
				format(fatura.Invoice_Data__c),
				format(fatura.Invoice_Number__c),
				format(fatura.Invoice_Serie__c),
				format(fatura.Invoice_Type__c),
				format(fatura.Manufacturing_Year__c),
				format(fatura.Model_Year__c),
				format(fatura.Neighborhood__c),
				format(fatura.Number__c),
				format(fatura.Payment_Type__c),
				format(fatura.Phone_1__c),
				format(fatura.Phone_2__c),
				format(fatura.Phone_3__c),
				format(fatura.Postal_Code__c),
				format(fatura.Region__c),
				format(fatura.Sector__c),
				format(fatura.Sequential_File__c),
				format(fatura.Shipping_BIR__c),
				format(fatura.State__c),
				format(fatura.Status__c),
				format(fatura.VIN__c)
            });
        }
        return str;
	}

    private String line(String[] cells) {
        return String.join(cells, CSV_SEPARATE) + '\n';
    }
    private String format(Date d)    {
        return d != null ? '"' + d.format().escapeCsv() + '"' : '""';
    }
    private String format(String s) {
        return s != null ? '"' + s + '"' : '""';
    }
    private String format(Decimal d, Integer scale) {
        return d != null ? String.valueOf(d.setScale(scale)) : '';
    }
    private String format(Decimal d) {
        return format(d, 2);
    }

	private PageReference buildCsv(){

		if(!String.isEmpty(invoiceFrom)) {
			Date data = toDate(invoiceFrom);
			dataIni = DateTime.newInstance(data.year(), data.month(), data.day(), 0, 0 ,0);
		}

		if(!String.isEmpty(invoiceTo)) {
			Date data = toDate(invoiceTo);
			dataFim = DateTime.newInstance(data.year(), data.month(), data.day(), 23, 59 ,59);
		}

		System.debug('dataIni:' + dataIni);
		System.debug('dataFim:' + dataFim);

		String query = buildFullQuery();

        System.debug('Query Full:' + query);

        List<FaturaDealer2__c> faturas =  Database.Query(query);

        String body = bodyBuild(faturas);

        Id pasta = [Select Id from Folder where Name = 'Shared Documents'].Id;

        List<Document> docsDelete = [SELECT Id FROM Document WHERE Name = 'Fatura'];
        Delete docsDelete;

        Document doc = new Document(
            Name = 'Fatura',
            FolderId = pasta,
            Body = Blob.valueOf(body),
            ContentType = 'text/csv;charset=UTF-8',
            Type = 'csv'
        );
        Insert doc;

        PageReference page = new PageReference('/servlet/servlet.FileDownload?file=' + doc.Id);
        page.setRedirect(true);

        return page;
	}

	private String buildFullQuery(){

		List<Account> accList = null;
        User currUser = Sfa2Utils.obterUsuarioLogado();
        Account parentAcc =  Sfa2Utils.obterContaParente(currUser);
        accList = Sfa2Utils.obterListaConta(parentAcc);

		return 	'SELECT Id, '
				+	'Name, Address__c, Address_Complement__c, BIR_Group_Issuer__c, BIR_Name_NFE_Issuer__c, '
				+	'BIR_NFE_Issuer__c, Birth_Date__c, isCancelled__c, City__c, Customer_Email__c, '
				+	'Customer_Email_Mirror__c, Customer_Identification_Number__c, Customer_Name__c, '
				+	'Customer_Type__c, DDD1__c, DDD2__c, DDD3__c, Delivery_Date__c, Delivery_Date_Mirror__c,'
				+	'External_Key__c, Group_Name_Issuer__c, Integration_Date__c, Integration_Protocol__c, '
				+	'Invoice_Data__c, Invoice_Date_Mirror__c, Invoice_Number__c, Invoice_Serie__c, Invoice_Type__c, '
				+	'Manufacturing_Year__c, Model_Year__c, Neighborhood__c, Number__c, Payment_Type__c, '
				+	'Phone_1__c, Phone_2__c, Phone_3__c, Postal_Code__c, Region__c, isReturned__c, '
				+	'Sector__c, Sequential_File__c, Shipping_BIR__c, State__c, Status__c, Error_Messages__c,  VIN__c '
				+	'FROM FaturaDealer2__c '
				+	'WHERE Id != null '
				+	(String.isNotBlank(birEmissao) ? 'AND BIR_NFE_Issuer__c LIKE \'%' + birEmissao   + '%\'' : '' )
				+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' )
				+	(String.isNotBlank(registerTypeSelected) ? 'AND RecordTypeId = \'' + registerTypeSelected    + '\' ' : '' )

            	+ 	(String.isNotEmpty(invoiceFrom) && String.isNotEmpty(invoiceTo) ?
				+	'AND Invoice_Date_Mirror__c >=: dataIni '
            	+ 	'AND Invoice_Date_Mirror__c <=: dataFim ' : '')

            	+ 	(String.isNotEmpty(sendFrom) && String.isNotEmpty(sendTo) ?
				+	'AND Delivery_Date_Mirror__c >= ' + onlyDate(sendFrom) + ' '
				+ 	'AND Delivery_Date_Mirror__c <= ' + onlyDate(sendTo) + ' ' : '' )
				+	buildQueryInAccount(accList)
				+	'LIMIT 1500';
	}

	private String buildQuery(){
		List<Account> accList = null;
        User currUser = Sfa2Utils.obterUsuarioLogado();
        Account parentAcc =  Sfa2Utils.obterContaParente(currUser);
        accList = Sfa2Utils.obterListaConta(parentAcc);

		return 	'SELECT Id,'
				+	'Name, Delivery_Date__c, BIR_NFE_Issuer__c, Integration_Date__c,Invoice_Type__c, Invoice_Data__c, '
				+	'Invoice_Number__c, VIN__c, Customer_Identification_Number__c, Error_Messages__c, Delivery_Date_Mirror__c,'
				+	'Status__c, Invoice_Date_Mirror__c '
				+	'FROM FaturaDealer2__c '
				+	'WHERE Id != null '
				+	(String.isNotBlank(birEmissao) ? 'AND BIR_NFE_Issuer__c LIKE \'%' + birEmissao   + '%\'' : '' )
				+	(String.isNotBlank(statusSelected) ? 'AND (Status__c = ' + statusSelected    + ') ' : '' )
				+	(String.isNotBlank(registerTypeSelected) ? 'AND RecordTypeId = \'' + registerTypeSelected    + '\' ' : '' )

            	+ 	(String.isNotEmpty(invoiceFrom) && String.isNotEmpty(invoiceTo) ?
				+	'AND Invoice_Date_Mirror__c >=: dataIni '
             	+ 	'AND Invoice_Date_Mirror__c <=: dataFim ' : '')

            	+ 	(String.isNotEmpty(sendFrom) && String.isNotEmpty(sendTo) ?
				+	'AND Delivery_Date_Mirror__c >= ' + onlyDate(sendFrom) + ' '
				+ 	'AND Delivery_Date_Mirror__c <= ' + onlyDate(sendTo) + ' ' : '' )
				+	buildQueryInAccount(accList)
				+	'Limit 700';
	}

	private String buildQueryInAccount(List<Account> accList) {
        if(accList == null) {
            return ' ';
        }

        String inStg = ' AND BIR_NFE_Issuer__c in(';

        for(Account acc : accList) {
            if(accList.get(accList.size()-1).Id == acc.Id) {
                inStg += '\'' + acc.IDBIR__c + '\')';
            }
            else {
                inStg += '\'' + acc.IDBIR__c + '\',';

            }
        }

        return inStg;
    }

	private Date toDate(String data) {
	  final List<String> arrayDate = data.split('/');
	  return Date.valueOf(arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0]);
	}

	private String onlyDate(String data) {
		final List<String> arrayDate = data.split('/');
	  return arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0];
	}
}