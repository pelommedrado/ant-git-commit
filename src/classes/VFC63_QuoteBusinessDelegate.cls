/**
* Classe responsável por fazer o papel de business delegate da página de orçamento.
* @author Christian Ranha.
*/
public with sharing class VFC63_QuoteBusinessDelegate {

    private static final VFC63_QuoteBusinessDelegate instance = new VFC63_QuoteBusinessDelegate();
    private VFC67_QuoteItemVO quoteItemVO = new VFC67_QuoteItemVO();
    private String idOpp;
    
    /**
    * Construtor privado para impedir a criação de instancias dessa classe.
    */
    private VFC63_QuoteBusinessDelegate()
    {   
    }
    
    /**
    * Método responsável por prover a instância dessa classe.
    */  
    public static VFC63_QuoteBusinessDelegate getInstance()
    {
        return instance;
    }
    
    public VFC60_QuoteVO getOpportunity(String opportunityId)
    {
        Opportunity sObjOpportunity = null;
        VFC60_QuoteVO quoteVO = null;
        
        /*obtém a oportunidade pelo id*/
        sObjOpportunity = VFC47_OpportunityBO.getInstance().findById(opportunityId);
        
        /*mapeia os dados do objeto para o VO*/
        quoteVO = this.mapToValueObject(sObjOpportunity);
        
        return quoteVO; 
    }
    
    private VFC60_QuoteVO mapToValueObject(Opportunity sObjOpportunity)
    {
        VFC60_QuoteVO quoteVO = new VFC60_QuoteVO();
        
        /*mapeia os campos do objeto opportunity para os campos do VO*/
        quoteVO.opportunityId = sObjOpportunity.Id;
        quoteVO.opportunityName = sObjOpportunity.Name;
        quoteVO.customerName = sObjOpportunity.Account.Name;
        quoteVO.sellerName = sObjOpportunity.Owner.Name;
        quoteVO.status = sObjOpportunity.StageName;
        quoteVO.reasonCancellation = sObjOpportunity.ReasonLossCancellation__c;
        quoteVO.vehicleInterest = sObjOpportunity.Account.VehicleInterest_BR__c;
        quoteVO.customerId = sObjOpportunity.AccountId;
        quoteVo.sObjOpp = sObjOpportunity;
        
        return quoteVO; 
    }
    
    public void cancelOpportunity(VFC60_QuoteVO quoteVO)
    {
        Opportunity sObjOpportunity = new Opportunity(Id = quoteVO.opportunityId);

        // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
        

        sObjOpportunity.StageName = 'Lost';
        sObjOpportunity.ReasonLossCancellation__c = quoteVO.reasonLossCancellationOpportunity;
        
        List<Quote> quoteList = [SELECT Id, Status, QuoteNumber 
                                 FROM Quote 
                                 WHERE OpportunityId = :sObjOpportunity.Id 
                                 AND Status = 'Open' ];
        
        System.debug('quoteList:' + quoteList);
        
        if(!quoteList.isEmpty()) {
            List<Quote> quoteUpList = new List<Quote>();
            for( Quote quote : quoteList ) {
                
                Quote q1 = new Quote(Id = quote.Id);
                q1.Status = 'Canceled';
                
                quoteUpList.add(q1);
            }
            
            update quoteUpList;
        }
        
        VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity);
    }

    /**
    * Método responsável por montar a Grid do Orçameto da pagina VFP11_Quote.
    * @param: Id da Oportunidade
    * @return:  VFC60_QuoteVO
    * @autor: Elvia Serpa
    */  
    public VFC60_QuoteVO getQuote(String opportunityId)
    {  
        VFC60_QuoteVO quoteVO = new VFC60_QuoteVO();
        
        /*obtém a Quote pelo id*/
        List<Quote> listQuote = VFC35_QuoteDAO.getInstance().findByOpportunityId(opportunityId); 
        System.debug('>>>> listQuote'+listQuote);
        
        /*popula os dados do objeto para o VO*/
        for(Quote auxQuote : listQuote) 
        {
            quoteItemVO = new VFC67_QuoteItemVO();
            
            for(QuoteLineItem auxQuoteLine : auxQuote.QuoteLineItems )
            {
                // Verifica se existe um veiculo cadastrado 
                if(auxQuoteLine.Vehicle__r.Model__c != null)
                {
                    quoteItemVO.vehicle = auxQuoteLine.Vehicle__r.Model__c;
                    quoteItemVO.vehicleRule = auxQuoteLine.Vehicle__r.Model__c;
                } 
                else//Senão exisitir veiculo adiona o produto
                {
                    quoteItemVO.vehicle = auxQuoteLine.PricebookEntry.Name;
                    quoteItemVO.vehicleRule = auxQuoteLine.Vehicle__r.Model__c;
                }
            }
            
            quoteItemVO.isCheckOpp = auxQuote.IsSyncing; 
            quoteItemVO.nameItem = auxQuote.Name;
            quoteItemVO.quoteNumber = auxQuote.quoteNumber;
            quoteItemVO.totalPrice = VFC69_Utility.priceFormats(auxQuote.totalPrice);
            quoteItemVO.status = auxQuote.Status;
            quoteItemVO.id = auxQuote.Id;
            quoteItemVO.sobjQuote = auxQuote;
            
            System.debug('*** quoteItenVO'+quoteItemVO.nameItem);
            quoteVO.lstQuotes.add(quoteItemVO);
        }       
        
        return quoteVO; 
    }
    
    /**
    * Método responsável por atualizar o campo:SyncedQuoteId da oportunidade com o Id da cotação. 
    * Este processo ativa a sincronização
    * @param: Id da Oportunidade e id Orçamento
    * @autor: Elvia Serpa
    */      
    public void synchronizationOpportunity(String opportunityId, String quoteId)
    {
        Opportunity sObjOpportunity = new Opportunity(id = opportunityId);
        sObjOpportunity.SyncedQuoteId = quoteId;
        
        VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity);   
    }
    
    
    /**
    * PAGINA - VFP15_SendToRequest
    */      
    public VFC60_QuoteVO getQuoteById(String quoteId)
    {
        Quote sObjQuote = null;
        VFC60_QuoteVO quoteVO = null;
        
        /*obtém o orçamento pelo id*/
        sObjQuote =  VFC35_QuoteDAO.getInstance().findById(quoteId);                                    
        
        /*mapeia os dados do objeto para o VO*/
        quoteVO = this.mapToValueObject(sObjQuote);
        
        /*armazena a conta original da quote.*/
        quoteVO.mergeId = quoteVO.sObjQuote.Opportunity.Account.Id;
                        
        return quoteVO; 
    }

    /**
    * PAGINA - VFP15_SendToRequest
    */      
    private VFC60_QuoteVO mapToValueObject(Quote sObjQuote)
    {
        VFC60_QuoteVO quoteVO = new VFC60_QuoteVO(); 
        
        /*mapeia os campos do objeto Quote para os campos do VO*/
        quoteVO.quoteName = sObjQuote.Name; 
        quoteVO.sObjQuote = sObjQuote;
        
        for(QuoteLineItem auxQuoteLine : sObjQuote.QuoteLineItems )
        {
            // Verifica se existe um veiculo cadastrado
            if(auxQuoteLine.Vehicle__r.Model__c != null)
            {
                quoteVO.vehicle = auxQuoteLine.Vehicle__r.Model__c;
                quoteVo.vehicleVin = auxQuoteLine.Vehicle__r.Name;
            } 
            else//Senão exisitir veiculo adiona o produto
            {
                quoteVO.vehicle = auxQuoteLine.PricebookEntry.Name;
                quoteVo.vehicleVin = null;
            }
        }
        
        quoteVO.valueTotal = VFC69_Utility.priceFormats(sObjQuote.TotalPrice);
        quoteVO.customerName =  sObjQuote.Opportunity.Account.Name;
        quoteVO.numberQuote = sObjQuote.QuoteNumber;
        quoteVO.idCustomer = sObjQuote.Opportunity.AccountId;
        quoteVO.opportunityId = sObjQuote.OpportunityId;    
        quoteVO.id = sObjQuote.Id;
        idOpp =  sObjQuote.OpportunityId;
        
        /* Account field mapping*/
        quoteVo.accountFirstName = sObjQuote.Opportunity.Account.FirstName;
        quoteVo.accountLastName = sObjQuote.Opportunity.Account.LastName;
        quoteVo.accountCnpjCpf = sObjQuote.Opportunity.Account.CustomerIdentificationNbr__c;
        quoteVo.accountEmail = sObjQuote.Opportunity.Account.PersEmailAddress__c;
        quoteVo.accountCelPhone = sObjQuote.Opportunity.Account.PersMobPhone__c;
        quoteVo.accountResPhone = sObjQuote.Opportunity.Account.PersLandline__c;
        quoteVo.accountComPhone = sObjQuote.Opportunity.Account.ProfLandline__c;
        quoteVo.accountFax = sObjQuote.Opportunity.Account.Fax;
        quoteVo.accountStreet = sObjQuote.Opportunity.Account.Shipping_Street__c;
        quoteVo.accountNumber = sObjQuote.Opportunity.Account.Shipping_Number__c;
        quoteVo.accountComplement = sObjQuote.Opportunity.Account.Shipping_Complement__c;
        quoteVo.accountNeighborhood = sObjQuote.Opportunity.Account.Shipping_Neighborhood__c;
        quoteVo.accountCountry = sObjQuote.Opportunity.Account.Shipping_Country__c;
        quoteVo.accountZipCode = sObjQuote.Opportunity.Account.Shipping_PostalCode__c;
        quoteVo.accountState = sObjQuote.Opportunity.Account.Shipping_State__c;
        quoteVo.accountCity = sObjQuote.Opportunity.Account.Shipping_City__c;
        quoteVo.accountMarStatus = sObjQuote.Opportunity.Account.MaritalStatus__c;
        quoteVo.accountBirthday = sObjQuote.Opportunity.Account.PersonBirthdate;
        quoteVo.accountGender = sObjQuote.Opportunity.Account.Sex__c;
        //quoteVo.accountRG = (sObjQuote.Opportunity.Account.RGStateRegistration__c > 0) ? String.valueOF(sObjQuote.Opportunity.Account.RGStateRegistration__c) : '';
        quoteVo.accountRG = sObjQuote.Opportunity.Account.RgStateTexto__c;
        
        
        /* BillingAccount field mapping*/
        Id personalAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
        Id companyAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Company_Acc');
        
        quoteVo.FKQuote.BillingAccount__c = sObjQuote.BillingAccount__c; 
        if(quoteVO.sObjQuote.BillingAccount__r.RecordTypeId == personalAccount){
            quoteVO.tipoCliente =  'cpf';
            
            quoteVo.billFirstName = sObjQuote.BillingAccount__r.FirstName;
            quoteVo.billLastName = sObjQuote.BillingAccount__r.LastName;
            quoteVo.billCpf = sObjQuote.BillingAccount__r.CustomerIdentificationNbr__c;
            quoteVo.billEmail = sObjQuote.BillingAccount__r.PersEmailAddress__c;
            quoteVo.billCelPhone = sObjQuote.BillingAccount__r.PersMobPhone__c;
            quoteVo.billResPhone = sObjQuote.BillingAccount__r.PersLandline__c;
            quoteVo.billComPhone = sObjQuote.BillingAccount__r.ProfLandline__c;
            quoteVo.billFax = sObjQuote.BillingAccount__r.Fax;
            quoteVo.billStreet = sObjQuote.BillingAccount__r.Shipping_Street__c;
            quoteVO.billNumber = sObjQuote.BillingAccount__r.Shipping_Number__c;
            quoteVo.billComplement = sObjQuote.BillingAccount__r.Shipping_Complement__c;
            quoteVo.billNeighborhood = sObjQuote.BillingAccount__r.Shipping_Neighborhood__c;
            quoteVo.billCountry = sObjQuote.BillingAccount__r.Shipping_Country__c;
            quoteVo.billZipCode = sObjQuote.BillingAccount__r.Shipping_PostalCode__c;
            quoteVo.billState = sObjQuote.BillingAccount__r.Shipping_State__c;
            quoteVo.billCity = sObjQuote.BillingAccount__r.Shipping_City__c;
            quoteVo.billMarStatus = sObjQuote.BillingAccount__r.MaritalStatus__c;
            quoteVo.billBirthday = sObjQuote.BillingAccount__r.PersonBirthdate;
            quoteVo.billGender = sObjQuote.BillingAccount__r.Sex__c;
            //quoteVo.billRG = (sObjQuote.BillingAccount__r.RGStateRegistration__c > 0) ? String.valueOf(sObjQuote.BillingAccount__r.RGStateRegistration__c) : '';
            quoteVo.billRG = sObjQuote.BillingAccount__r.RGStateTexto__c;
            
        }else if(quoteVO.sObjQuote.BillingAccount__r.RecordTypeId == companyAccount){
            quoteVO.tipoCliente =  'cnpj';
           
            quoteVO.billPJCnpj = sObjQuote.BillingAccount__r.CustomerIdentificationNbr__c;
            quoteVO.billPJEmail = sObjQuote.BillingAccount__r.ProfEmailAddress__c ;
            quoteVO.billPJName = sObjQuote.BillingAccount__r.Name;
            quoteVO.billPJPhone = sObjQuote.BillingAccount__r.Phone;
            quoteVO.billPJState = sObjQuote.BillingAccount__r.Shipping_State__c;
            quoteVO.billPJCity = sObjQuote.BillingAccount__r.Shipping_City__c;
            quoteVO.billPJStreet = sObjQuote.BillingAccount__r.Shipping_Street__c;
            quoteVO.billPJZipCode = sObjQuote.BillingAccount__r.Shipping_PostalCode__c;
            quoteVO.billPJNumber = sObjQuote.BillingAccount__r.Shipping_Number__c;
            quoteVO.billPJComplement = sObjQuote.BillingAccount__r.Shipping_Complement__c;
            quoteVO.billPJNeighborhood = sObjQuote.BillingAccount__r.Shipping_Neighborhood__c;
            quoteVO.billPJCountry = sObjQuote.BillingAccount__r.Shipping_Country__c;
            quoteVO.billPJFax = sObjQuote.BillingAccount__r.Fax;
            
        }
        
        
        /*atributos referentes a seção sintese*/
        quoteVo.entry = VFC69_Utility.priceFormats(sObjQuote.Entry__c);  
        quoteVo.amountFinanced = VFC69_Utility.priceFormats(sObjQuote.AmountFinanced__c);
        if(sObjQuote.YearUsedVehicle__c == 0.0){
            quoteVo.yearUsedVehicle = '';
        }else{
            quoteVo.yearUsedVehicle = String.valueOf(sObjQuote.YearUsedVehicle__c);
        }
        quoteVo.priceUsedVehicle = VFC69_Utility.priceFormats(sObjQuote.PriceUsedVehicle__c);
        quoteVo.cdcCFinancing = sObjQuote.CDCFinancing__c;
        quoteVo.lsgFinancing = sObjQuote.LSGFinancing__c;
        if(sObjQuote.NumberOfInstallments__c == 0.0){
            quoteVo.numberOfParcels = '';
        }else{
            quoteVo.numberOfParcels = String.valueOf(sObjQuote.NumberOfInstallments__c);
        } 
        quoteVo.valueOfParcel = VFC69_Utility.priceFormats(sObjQuote.ValueOfInstallments__c);
        quoteVo.paymentMethods = sObjQuote.PaymentMethods__c;
        quoteVo.paymentDetails = sObjQuote.PaymentDetails__c;
        quoteVo.usedVehicleLicensePlate = sObjQuote.UsedVehicleLicensePlate__c;
        quoteVo.usedVehicleBrand = sObjQuote.UsedVehicle__r.Brand__c;
        quoteVo.usedVehicleModel = sObjQuote.UsedVehicle__r.Model__c;
        
        
        return quoteVO; 
    }

    private Account accountMappingToBillingAccount(VFC60_QuoteVO quoteVO, Quote sObjQuote)
    {
        /* BillingAccount field mapping */
        Account account = new Account(id = quoteVO.sObjQuote.BillingAccount__c);
        
        Id personalAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
        Id companyAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Company_Acc');
        
        
        if(quoteVo.tipoCliente == 'cnpj' ){
            account.RecordTypeId = companyAccount;
            
            account.Name = quoteVo.billPJName;
            account.ProfEmailAddress__c = quoteVo.billPJEmail;
            account.CustomerIdentificationNbr__c = quoteVo.billPJCnpj;
            account.Shipping_City__c = quoteVO.billPJCity;
            account.Shipping_Number__c = quoteVO.billPJNumber;
            account.Shipping_Complement__c = quoteVO.billPJComplement;
            account.Shipping_Neighborhood__c = quoteVO.billPJNeighborhood;
            account.Shipping_Country__c = quoteVO.billPJCountry;
            account.Phone = quoteVO.billPJPhone;
            account.Shipping_State__c = quoteVO.billPJState;
            account.Shipping_Street__c = quoteVO.billPJStreet ;
            account.Shipping_PostalCode__c = quoteVO.billPJZipCode ;
            account.Fax = quoteVo.billPJFax;
        }else if(quoteVo.tipoCliente == 'cpf' ){
            account.FirstName = quoteVo.billFirstName; 
            account.LastName = quoteVo.billLastName;
            account.PersonBirthdate = quoteVo.sObjQuote.BillingAccount__r.PersonBirthdate;
            account.MaritalStatus__c = quoteVo.sObjQuote.BillingAccount__r.MaritalStatus__c;
            account.Sex__c = quoteVo.sObjQuote.BillingAccount__r.Sex__c;
            account.PersEmailAddress__c = quoteVo.billEmail;
            account.CustomerIdentificationNbr__c = quoteVo.billCpf;
            account.PersMobPhone__c = quoteVo.billCelPhone;
            account.PersLandline__c = quoteVo.billResPhone;
            account.ProfLandline__c = quoteVo.billComPhone;
            //account.RGStateRegistration__c = VFC69_Utility.getDecimal(quoteVo.billRG);
            account.RgStateTexto__c = quoteVo.billRG;
            
            account.Fax = quoteVo.billFax;
            account.Shipping_Street__c = quoteVo.billStreet;
            account.Shipping_Number__c = quoteVo.billNumber;
            account.Shipping_Complement__c = quoteVo.billComplement;
            account.Shipping_Neighborhood__c = quoteVo.billNeighborhood;
            account.Shipping_Country__c = quoteVo.billCountry;
            account.Shipping_PostalCode__c = quoteVo.billZipCode;
            account.Shipping_State__c = quoteVo.billState;
            account.Shipping_City__c = quoteVo.billCity;
        }
        return account;
    }
    
    private Account accountMappingToAccount(VFC60_QuoteVO quoteVO, Quote sObjQuote)
    {
        /* Account field mapping*/      
        Account account = new Account(id = sObjQuote.Opportunity.Account.Id);
        
        account.FirstName = quoteVo.accountFirstName;
        account.LastName = quoteVo.accountLastName;
        account.CustomerIdentificationNbr__c = quoteVo.accountCnpjCpf;
        account.PersEmailAddress__c = quoteVo.accountEmail;
        account.PersMobPhone__c = quoteVo.accountCelPhone;
        account.PersLandline__c = quoteVo.accountResPhone;
        account.ProfLandline__c = quoteVo.accountComPhone;
        account.Fax = quoteVo.accountFax;
        account.Shipping_Street__c = quoteVo.billStreet;
        account.Shipping_Number__c = quoteVo.billNumber;
        account.Shipping_Complement__c = quoteVo.billComplement;
        account.Shipping_Neighborhood__c = quoteVo.billNeighborhood;
        account.Shipping_Country__c = quoteVo.billCountry;
        account.Shipping_PostalCode__c = quoteVo.billZipCode;
        account.Shipping_State__c = quoteVo.billState;
        account.Shipping_City__c = quoteVo.billCity;
        account.MaritalStatus__c = quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c;
        account.PersonBirthdate = quoteVo.sObjQuote.Opportunity.Account.PersonBirthdate;
        account.Sex__c = quoteVo.sObjQuote.Opportunity.Account.Sex__c;
        account.RGStateRegistration__c = VFC69_Utility.getDecimal(quoteVo.accountRG);
        account.RgStateTexto__c = quoteVo.accountRG;
        
        return account;
    }
    
    public VFC60_QuoteVO updateQuoteVOByAccount(Account account, VFC60_QuoteVO quoteVO)
    {
        if(quoteVO.tipoCliente == 'cnpj'){
            quoteVO.billPJCnpj = account.CustomerIdentificationNbr__c;
            quoteVO.billPJName = account.Name;
            
            quoteVO.billPJCity = account.Shipping_City__c;
            quoteVO.billPJEmail = account.ProfEmailAddress__c ;
            quoteVO.billPJPhone = account.Phone;
            quoteVO.billPJState = account.Shipping_State__c;
            quoteVO.billPJStreet = account.Shipping_Street__c;
            quoteVO.billPJZipCode = account.Shipping_PostalCode__c;
            quoteVO.billPJNumber = account.Shipping_Number__c;
            quoteVO.billPJComplement = account.Shipping_Complement__c;
            quoteVO.billPJNeighborhood = account.Shipping_Neighborhood__c;
            quoteVO.billPJCountry = account.Shipping_Country__c;
            
            
        }else if(quoteVO.tipoCliente == 'cpf'){
            quoteVO.billCpf = account.CustomerIdentificationNbr__c;
            quoteVO.billFirstName = account.FirstName;
            quoteVO.billLastName = account.LastName; 
            quoteVO.billEmail = account.PersEmailAddress__c;
            quoteVO.billCelPhone = account.PersMobPhone__c;
            quoteVO.billResPhone = account.PersLandline__c;
            quoteVO.billComPhone = account.ProfLandline__c;
            quoteVO.billFax = account.Fax;
            quoteVO.billStreet = account.Shipping_Street__c;
            quoteVO.billNumber = account.Shipping_Number__c;
            quoteVO.billComplement = account.Shipping_Complement__c;
            quoteVO.billNeighborhood = account.Shipping_Neighborhood__c;
            quoteVO.billCountry = account.Shipping_Country__c;
            quoteVO.billZipCode = account.Shipping_PostalCode__c;
            quoteVO.billState = account.Shipping_State__c;
            quoteVO.billCity = account.Shipping_City__c;
            quoteVO.billMarStatus = account.MaritalStatus__c;
            quoteVO.billBirthday = account.PersonBirthdate;
            quoteVO.billGender = account.Sex__c;
            //quoteVO.billRG = String.valueOf(account.RGStateRegistration__c);
            quoteVO.billRG = account.RGStateTexto__c;
            
            if(quoteVO.sObjQuote.BillingAccount__r == null){
                quoteVO.sObjQuote.BillingAccount__r = new Account();
            }
            
            quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate = account.PersonBirthdate;
            quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c = account.MaritalStatus__c;
            quoteVO.sObjQuote.BillingAccount__r.Sex__c = account.Sex__c;
        }
        
        return quoteVO;
    }
    
    /**
    * PAGINA - VFP15_SendToRequest
    */          
    public void saveQuote(VFC60_QuoteVO quoteVO) {
        salvarVFC60_QuoteVO(quoteVO, true);
    }
    
    public void salvarVFC60_QuoteVO(VFC60_QuoteVO quoteVO, Boolean enviarPreOrdem) {
        Account account = accountMappingToAccount(quoteVO, quoteVO.sObjQuote);
        System.debug('Acc:' + account);
        Database.update( account );
        
        Account billingAccount = null;
        //se for selecionada uma billingaccount já existente
        if(quoteVO.FKQuote.BillingAccount__c != null) {
            //atualiza quote com novo billing account
            quoteVO.sObjQuote.BillingAccount__c = quoteVO.FKQuote.BillingAccount__c;
            //atualiza o billingaccount com os valores do vo
            billingAccount = accountMappingToBillingAccount(quoteVO, quoteVO.sObjQuote);    
            //somente atualiza os dados se conta de faturamento for diferente da conta da oportunidade
            if(quoteVO.sObjQuote.BillingAccount__c != account.Id) {
                VFC12_AccountDAO.getInstance().updateData(billingAccount);
            }
            //caso o lookup de billign account não esteja preenchido
            //entao cria registro de conta  
        } else {
            if(!String.isEmpty(quoteVO.billFirstName) && !String.isEmpty(quoteVO.billLastName)) {
                if(!String.isEmpty(quoteVO.billResPhone) || !String.isEmpty(quoteVO.billCelPhone) || 
                   String.isEmpty(quoteVO.billComPhone) || !String.isEmpty(quoteVO.billEmail) ) {
                       quoteVO.sObjQuote.BillingAccount__c = quoteVO.FKQuote.BillingAccount__c;
                       billingAccount = accountMappingToBillingAccount(quoteVO, quoteVO.sObjQuote);
                       VFC12_AccountDAO.getInstance().insertData(billingAccount);           
                       //atualiza campo de conta de faturamento
                       quoteVO.sObjQuote.BillingAccount__r = null;          
                       quoteVO.sObjQuote.BillingAccount__c = billingAccount.Id;
                   }
                
            } else if(quoteVO.tipoCliente == 'cnpj') {
                if(!String.isEmpty(quoteVO.billPJName)      && 
                   !String.isEmpty(quoteVO.billPJPhone)     &&
                   !String.isEmpty(quoteVO.billPJCnpj)      &&
                   !String.isEmpty(quoteVO.billPJEmail)     &&
                   !String.isEmpty(quoteVO.billPJStreet)    &&
                   !String.isEmpty(quoteVO.billPJZipCode)   &&
                   !String.isEmpty(quoteVO.billPJCity)      &&
                   !String.isEmpty(quoteVO.billPJState)) {
                       quoteVO.sObjQuote.BillingAccount__c = quoteVO.FKQuote.BillingAccount__c;
                       billingAccount = accountMappingToBillingAccount(quoteVO, quoteVO.sObjQuote);
                       VFC12_AccountDAO.getInstance().insertData(billingAccount);           
                       //atualiza campo de conta de faturamento
                       quoteVO.sObjQuote.BillingAccount__r = null;          
                       quoteVO.sObjQuote.BillingAccount__c = billingAccount.Id;       
                   }
                
            }
        }
        
        //um insert não pode setar o Id e FK __r
        quoteVO.sObjQuote.BillingAccount__r = null;
        if(enviarPreOrdem) {
            
            System.debug('@@quoteVO.vehicleVin> ' + quoteVO.vehicleVin);
            
            if(quoteVO.vehicle.equals(Label.VehicleToDistrinet) && quoteVO.vehicleVin == null){
                quoteVO.sObjQuote.Status = 'Pre order sent to Distrinet';
                quoteVO.sObjQuote.PC_DmsOrderNumber__c = quoteVO.sObjQuote.QuoteNumber; 
            
            } else{
                quoteVO.sObjQuote.Status = 'Pre Order Requested';
            }
             
        }
        quoteVO.sObjQuote.PaymentDetails__c = quoteVO.paymentDetails;
        
        System.debug('ATUALIZANDO ORÇAMENTO ---->' + quoteVO.sObjQuote);
        VFC23_OpportunityDAO.getInstance().updateData(quoteVO.sObjQuote);
    }
    
    /**
    * PAGINA - VFP15_SendToRequest
    */      
    public Account getCustomerById(String idCustomer)
    {
        System.debug('*** Entrou no método'+idCustomer);
        
        Account sObjAccount = null;
        
        /*obtém o orçamento pelo id*/
        sObjAccount = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(idCustomer);                                    
        
        return sObjAccount;
    }           
}