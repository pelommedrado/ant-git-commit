public with sharing class VFC01_Goodwill {

    /* constructor */
    public VFC01_Goodwill() {}
    
    public static Double getPrice(String typeOfBackupCar) {
        Map<String, Car_Mapping__c> prices = Car_Mapping__c.getAll();
        if (prices.containsKey(typeOfBackupCar)) {
            Double price= prices.get(typeOfBackupCar).Amount__c;
            return price;
        }
        return null;      
    }
    
    public static Double getGoodwillAmount(Goodwill__c iGdw) {
        if (iGdw.GoodwillStatus__c == 'Refused') {
            return null;
        }
        String typeOfBackupCar = iGdw.BackUp_Car__c;
        Double  price = getPrice(typeOfBackupCar);
        Double globalAmount = null;
        
        
        if (iGdw.Amount__c != null) {
            globalAmount = iGdw.Amount__c;
        } else if (iGdw.SRCPartAmount__c != null) {
            globalAmount = iGdw.SRCPartAmount__c ;
        }
        
        else{
        globalAmount = 0;
        
        }
        if( globalAmount == null|| iGdw.Number_of_Days__c == null|| price == null) {
            return null;
        }
        Double goodAmount = (iGdw.Number_of_Days__c * price);
        system.debug ('VFC01.getGoodwillAmount : ' + goodAmount );
        return goodAmount;
    }
 
}