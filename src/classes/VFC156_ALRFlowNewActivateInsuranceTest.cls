@isTest
private class VFC156_ALRFlowNewActivateInsuranceTest {

    static testMethod void myUnitTest() { 
         
        FileUploaderController target = new FileUploaderController();
        
        Account dealerAcc = new Account(
              Name = 'Concessionaria teste',
              IDBIR__c = '123ABC123',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc );
        System.debug('######## conta inserido '+dealerAcc);
        
        Account dealerAcc2 = new Account(
              Name = 'Concessionaria teste2',
              IDBIR__c = '123ABC124',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc2 );
        System.debug('######## conta inserido '+dealerAcc2);
        
        Account dealerAcc3 = new Account(
              Name = 'Concessionaria teste3',
              IDBIR__c = '123ABC125',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        Database.insert( dealerAcc3 );
        System.debug('######## conta inserido '+dealerAcc3);
        
        
        
        Account css1 = dealerAcc;
        Account css2 = dealerAcc2;
        Account css3 = dealerAcc3;
        /*List<Account> lsCss = [Select id,IDBIR__c From Account where Active_PV__c = true and  Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH'];
          for( Account conta : lsCss){
            if(css1==null){
              if(css2==null){
                if(css3==null){
                  break;
                }else{
                  css3=conta;
                }
              }else{
                css2 = conta;
              }
            }else{
              css1 = conta;
            }
          }
          
            */
        VEH_Veh__c vehicle = new VEH_Veh__c(
          ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            IDBIRSellingDealer__c = css1.IDBIR__c,
            Id_Bir_BuyDealer__c = css2.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'fffffffffffffffff',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'fffffffffffffffff',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        System.debug('################ css1.IDBIR__c '+css1.IDBIR__c);
        System.debug('################ css2.IDBIR__c '+css2.IDBIR__c);
        Database.insert(vehicle);
        System.debug('################ vehicle '+vehicle);
        
        List<ALR_Tracking__c> listTracking = [select id,name from ALR_Tracking__c where ALR_Chassi__c =:vehicle.id] ;
       
       List<ALR_Activate_Insurance__c> listActivateInsurance = new List<ALR_Activate_Insurance__c>();//lista para armazenar o acionamento do seguro
       
       System.debug('###### listTracking '+listTracking);
       for(ALR_Tracking__c track : listTracking){
       	System.debug('###### track '+track);
         ALR_Activate_Insurance__c activateInsurance = new ALR_Activate_Insurance__c(
             ALR_Amount_Damage__c = 990.00,
             ALR_Approved_Value_Parts__c = 790.00,
             ALR_Approved_Value_Salvado__c = 300.00,
             ALR_Approved_Value_Service__c = 200.00,
             ALR_Arrival_Dealership__c = date.today(),
             ALR_Carrier__c = 'BRAZUL TRANSPORTE DE VEICULOS',
             ALR_Chassi__c = track.id,
             ALR_Check_Caveat__c = true,
             ALR_Check_CTe__c = true,
             ALR_Check_Invoice_Carrier__c = true,
             ALR_Check_Invoice_Combined__c = true,
             ALR_Check_Invoice_Parts__c = true,
             ALR_Check_Invoice_Salvado__c = true,
             ALR_Check_Invoice_Service__c = true,
             ALR_Check_Photo_Chassi__c = true,
             ALR_City_Occurrence__c = 'São Paulo',
             ALR_Claim_Number__c = '789109',
             ALR_Comercial_Phone__c = '22223333',
             ALR_Contact_Email__c = 'teste@test.com',
             ALR_Contact_Name__c = 'test',
             ALR_CTe__c = '123123',
             ALR_Date_Docs_Received__c = date.today(),
             ALR_Date_Docs_Refused__c = date.today().addDays(-3),
             ALR_Date_Error_Insurance__c = date.today().addDays(-3),
             ALR_Date_Estimate_Received__c = date.today().addDays(-1),
             ALR_Date_Estimate_Requested__c = date.today(),
             ALR_Date_Finalized_Process__c = date.today(),
             ALR_Date_Inspection_Received__c = date.today(),
             ALR_Date_Inspection_Requested__c = date.today().addDays(-1),
             ALR_Date_Insurance_Ended__c = date.today().addDays(-1), 
             ALR_Date_No_Send_Insurer__c = date.today(),
             ALR_Date_Occurrence__c = date.today().addDays(-15),
             ALR_Date_Open_Insurance__c = date.today().addDays(-2),
             ALR_Date_Pending_Processing__c  = date.today().addDays(-2),
             ALR_Date_Process__c = date.today().AddMonths(-1),
             ALR_Date_Received_Insurer__c = date.today(),
             ALR_Date_Repair_Released__c = date.today().addDays(-3),
             ALR_Date_Reparation_Requested__c = date.today().addDays(-2),
             ALR_Date_Total_Loss_Defined__c = date.today().addDays(-2),
             ALR_DDD_Comercial_Phone__c = '11',
             ALR_DDD_Mobile__c = '11',
             ALR_DDD__c = '11',
             ALR_Description_Pending__c = 'teste teste teste teste',
             ALR_Driver_Name__c = 'teste teste',
             ALR_Error_Description__c = 'testando os testes com a classe de teste',
             ALR_Estimated_Loss_Value__c = 12.4,
             ALR_Event_Phase_Process__c = '107',
             ALR_Flag_Caveat_CTe__c = true,
             ALR_Flag_Caveat__c = true,
             ALR_Flag_CTe__c = true,
             ALR_Flag_Invoice_Carrier__c = true,
             ALR_Flag_Invoice_Combined__c = true,
             ALR_Flag_Invoice_Parts_Refused__c = true,
             ALR_Flag_Invoice_Parts__c = true,
             ALR_Flag_Invoice_Salvado__c = true,
             ALR_Flag_Invoice_Service_Refused__c = true,
             ALR_Flag_Invoice_Service__c = true,
             ALR_indemnification_of_parts__c = true,
             ALR_Indemnification_of_Service__c = true,
             ALR_Indemnification_total_loss__c = true,
             ALR_Inspection_Type__c = 'Imagem',
             ALR_Inspector_Email__c = 'teste@teste.com',
             ALR_Inspector_Name__c = 'Teste',
             ALR_Invoice_Combined_Refused__c =true,
             ALR_Invoice_Parts_Refused__c =true,
             ALR_Invoice_Salavado_Refused__c =true,
             ALR_Invoice_Service_Refused__c =true,
             ALR_lastUpdateTime__c = System.now(),
             ALR_Local_Ocorrence__c = 'Pais Leme',
             ALR_Mobile__c = '111199992',
             ALR_Myself__c = false,
             ALR_Necessary_Repair_TL_Docs__c = 'Nota Fiscal de Serviços',
             ALR_Neighborhood_Occurrence__c = 'Pinheiros',
             ALR_Observation__c = 'teste',
             ALR_Pending_Docs_Received__c =   'teste teste',
             ALR_Pending_Docs_Refused__c = 'teste teste',
             ALR_Pending_Estimate__c = 'teste',
             ALR_Pending_Inspection__c = 'teste teste',
             ALR_Pending_Insurance_Ended__c = 'testando com a classe de testes',
             ALR_Pending_Insurance_Open__c = 'testando com a classe de testes',
             ALR_Pending_Released_Repair__c = 'testando com a classe de testes',
             ALR_Pending_Reparation__c = 'teste testando',
             ALR_Phase_Process__c = '100',
             ALR_Phone__c = '11112222',
             ALR_Reason_Refusal__c = 'testando com a classe de testes',
             ALR_Send_Email__c = true,
             ALR_Send_SMS__c = true,
             ALR_Sinister_Nature__c = '187',
             ALR_Status_System__c = 'Pending processing',
             ALR_Status__c = 'Sent',
             ALR_Trunck_Position_Number__c = '5',
             ALR_UF_Occurrence__c = 'SP',
             ALR_Zip_Code_Occurrence__c = '07910090',
             The_Damaged_Vehicle_Region__c = 'lateral direita'
    //         ALR_CTe_refused__c = true,
    //         ALR_Caveat_CTe_refused__c = true,
    //         ALR_Flag_Photo_Chassi_Marking_on_glass__c = true,
     //        ALR_Photo_Chassi_Marking_glass_refused__c = true,
     //        ALR_Reason_Refusal_Document__c = 'teste'
             
         );
         listActivateInsurance.add(activateInsurance);
       }
       
       Database.upsert(listActivateInsurance);
        
        
        ALR_Damage__C damage = new ALR_Damage__C(ALR_Activate_Insurance__c =listActivateInsurance.get(0).Id, ALR_Position__c = '1', ALR_Cod_Quadrant__c ='A', ALR_Damage_Area__c ='A', ALR_Damage_Type__c ='A');
        Database.insert(damage);

        
        ApexPages.StandardController stdController = new ApexPages.StandardController( damage );
        VFC156_ALRFlowNewActivateInsurance controller1,controller2;
        controller1 = new VFC156_ALRFlowNewActivateInsurance( stdController );
        controller2 = new VFC156_ALRFlowNewActivateInsurance();
        
        String a = controller1.getUrlImg();
        //String b = controller1.getPicklistVazio();
        boolean c = controller1.getAuxPanel();
        PageReference returnoPage = controller1.getFinish();
        controller1.fileUploaderCallback(target);
        controller1.registerController(target);
      //  controller1.verificarUpload();
       // controller1.envioSeguradora();
       // 
        ApexPages.StandardController stdControllerDamage = new ApexPages.StandardController( damage );
        VFC156_ALRDamageController controller1Damage;
        controller1Damage = new VFC156_ALRDamageController( stdControllerDamage );
        
        String var = controller1Damage.varResourceImagem;
        String idDan = controller1Damage.idDamage;
        String varDocumentID= controller1Damage.varDocumentID;
        String varFaseUpload  = controller1Damage.varFaseUpload;
        FileUploaderCaller self = controller1Damage.self;
        Id parentId = controller1Damage.parentId;
        String msg = controller1Damage.getMsgErro();
        
        String aDamage = controller1Damage.getUrlImg();
       // String vazio = controller1Damage.getPicklistVazio();
        boolean cDamage = controller1Damage.getAuxPanel();
        PageReference returnoPageDamage = controller1Damage.getFinish();
        controller1Damage.fileUploaderCallback(target);
        controller1Damage.registerController(target);
    }
}