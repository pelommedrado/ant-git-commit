global class VFC150_CloneComercialAction {
  

  WebService static String clonarAcaoComercial(String idAcao){
  	
	  	PVCommercial_Action__c acaoComercial = [select Id, Name, End_Date__c,Start_Date__c,Model__c,Status__c,Type_of_Action__c  from PVCommercial_Action__c where Id =:idAcao];
	  	List<PVCall_Offer__c> chamadaOferta = [select id, name,Call_Offer_End_Date__c,Call_Offer_Start_Date__c,Coefficient__c,
	         Commercial_Action__c,Description__c,Financing_Condition__c,Minimum_Entry__c,Model__c,Month_Rate__c,Period_in_Months__c,
	         Status__c,Template_Price__c from PVCall_Offer__c where Commercial_Action__c =: idAcao];
	    
	    acaoComercial.Id = null;
	    PVCommercial_Action__c novaAC = acaoComercial;
	    try{
	     Database.insert(novaAC);
	    }catch(Exception e){}
	    novaAC.Name = acaoComercial.Name;
	    Database.update(novaAC);
	    List<PVCall_Offer__c> novasChamadas = new List<PVCall_Offer__c>();
	    for(PVCall_Offer__c ch2: chamadaOferta){
	    	   ch2.Status__c= 'Inactive';
	         ch2.id=null;
	         ch2.Commercial_Action__c = novaAC.Id;
	         novasChamadas.add(ch2);
	    }
	    try{
	     Database.insert(novasChamadas);     
	    }catch(Exception e){}
	    return novaAC.Id;
  	
  	
  }
   	
}