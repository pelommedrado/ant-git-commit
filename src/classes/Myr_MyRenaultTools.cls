/** General Tools used by MyRenault
    Creation: 13.02.2015
    
**/ 
public class Myr_MyRenaultTools {
	
	/** List of all the possible accents (lower case) **/
  	private static String ACCENTS = 'àáâãäåèéêëìíîïðòóôõöùúûüýÿ';
  	/** list of the possible vowels (lower case) **/
  	private static String VOWELS = 'aeiouy';
    
    public static String USERNAME_REGEX = 
        CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c + '([\\w-\\.]*)@([\\w-\\.]*)\\.(\\w*)';
    
    public enum Brand {
        Renault,
        Dacia,
        Nissan
    }
    
    public enum accountBrand { 
        MYR,
        MYD
    }

    public enum Mode {
        UpdateMode,
        InsertMode
    }

    /** Check if this is the proper Renault Brand
        @return true if it's ok
    **/
    public static Boolean checkRenaultBrand( String brand ) {
        return (!String.isBlank(brand) && brand.equalsIgnoreCase( Myr_MyRenaultTools.Brand.RENAULT.name()));
    }
    
    /** Check if this is the proper Dacia Brand
        @return true if it's ok
    **/
    public static Boolean checkDaciaBrand(String brand) {
        return (!String.isBlank(brand) && brand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name()));
    }

	/** @return the other brand given the current brand **/
	public static String getOtherBrand( String brand ) {
		String otherBrand = null;
		if( Myr_MyRenaultTools.checkRenaultBrand( brand ) ) {
			otherBrand = Myr_MyRenaultTools.Brand.Dacia.name();
		} else if( Myr_MyRenaultTools.checkDaciaBrand( brand ) ) {
			otherBrand = Myr_MyRenaultTools.Brand.Renault.name();
		}
		return otherBrand;
	}

	/** Returns the id field depending on the given brand 
		@return the id field, either MyRenaultID__c, either MyDacia_ID__c **/
	public static String getIdField( String brand ) {
		if( checkRenaultBrand( brand ) ) {
			return 'myrenaultid__c';
		} else if ( checkDaciaBrand( brand ) ) {
			return 'mydaciaid__c';
		}
		return '';
	}

	/** Returns the status field depending on the given brand 
		@return the status field, either Myr_Status__c, either Myd_Status__c **/
	public static String getStatusField( String brand ) {
		if( checkRenaultBrand( brand ) ) {
			return 'myr_status__c';
		} else if ( checkDaciaBrand( brand ) ) {
			return 'myd_status__c';
		}
		return '';
	}

	/**
	* @description Check if the given email is a valid email
	* @param iEmail 
	* @return Boolean 
	*/
	public static Boolean isEmailValid(String iEmail) {
		return Pattern.matches(system.Label.Myr_Patterns_Email, iEmail);
	}
    
    /** Check if this is an authorized Brand for MyRenault
        @return true if it's ok
    **/
    public static Boolean authorizedBrand(String brand) {
        if( String.isBlank(brand) ) return false;
        List<Brand> listBrands = Myr_MyRenaultTools.Brand.values();
        for( Brand br : listBrands ) {
            system.debug('>>>>>> brand='+brand+' and br='+br.name());
            if( brand.equalsIgnoreCase(br.name()) ) {
                return true;
            }
        }
        return false;
    }

	/* Check that the given string is a valid salesforce ID
	  @param the string to test
	  @return Boolean, true if it's a valid id
	**/
	public static Boolean isValidId(String testId) {
		try {
			if( String.isBlank(testId) ) {
				return false;
			}
			Id validId = testId;
		} catch( Exception e ) {
			return false;
		}
		return true;
	}
    
    /** Replace all the characters with accents or vowels with the given character
     @param the string in which we want to replace the accents
     @param the character to use for replacement
     @return the string with replaced characters
    **/
    public static String replaceAccentsAndVowels( String iStr, String iChar ) {
   	 	String regExp= '['+ACCENTS + ACCENTS.toUpperCase() + VOWELS + VOWELS.toUpperCase() + ']';
    	return iStr.replaceAll(regExp, iChar);
    }

	/** Remove all the spaces from the given string
		 @param the string in which we want to remove the spaces
		 @return the cleaned string
   **/
	public static String removeAllSpaces(String iStr ) {
		if( !String.isBlank( iStr ) ) {
			iStr = iStr.replaceAll('[\\s]*','');
		}
		return iStr;
	}

	/* Select All Query **/
	public class SelectAll {
		public String sObjectName;
		public String Query;
		public List<String> Fields;
		//@inner constructor
		public SelectAll() {
			Fields = new List<String>();
		}

	}
    
    /** Build a Select All Query on the given object
		@param String, the name of the object on which the select * should be build
		@param String, the filter to apply on field names
    	@return the SelectAll, the selectAll query containing the select * query and the list of field queried
    **/
    public static SelectAll buildSelectAllQuery(String sObjectName, String filter) {
    	system.debug('#### Myr_MyRenaultTools - <buildSelectAllQuery> - BEGIN');
		SelectAll queryall = new SelectAll();
		queryAll.sObjectName = sObjectName;
		if( !Schema.getGlobalDescribe().containsKey( sObjectName ) ) {
			return queryAll;
		}
		queryAll.Query = 'SELECT ';
		Set<String> objectFields =  Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().keySet();
		List<String> listFields = new List<String>();
		listFields.addAll( objectFields );
		listFields.sort();
		for( String field : listFields ) {
			if( (!String.isBlank(filter) && field.containsIgnoreCase(filter)) || String.isBlank(filter) ) {
				queryAll.Query += field + ',';
				queryAll.Fields.add( field );
			}
		}
		// Trim last comma
		queryAll.Query = queryAll.Query.subString(0, queryAll.Query.length() - 1);
		queryAll.Query += ' FROM ' + queryAll.sObjectName;
		system.debug('#### Myr_MyRenaultTools - <buildSelectAllQuery> - END:' + queryAll);
		return queryAll;
    }

}