public with Sharing class CAC_DealerChoiceDeptCaseController {
    
    //public Case case{get;set}
    private final String userId = UserInfo.getUserId();
    private final String profileId = UserInfo.getProfileId();
    public static List<Case> showListCase{get;set;}
    public Case caseType{get;set;}
    private final List<String> recordTypeDontSeeList = new List<String>{'BR_RecType',
        'Deal_RecType','Core_Community_RecType','CORE_CAS_Case_RecType','CN_Case_RecType','CO_Case_RecType','ZA_Case_RecType'
        };
    
    
    public String typeCaseSelect='';
    public String getTypeCaseSelect(){return typeCaseSelect;}
    public void setTypeCaseSelect(String value){
        typeCaseSelect = value;
    }
    public PageReference  typecase(){
        newValuesToFilter();
        return null;
    }
    
    public String directionCase{ get; set;}
    public PageReference  directionCa(){
        System.debug('#### directionCase '+directionCase);
        filtroDirecao();
        return null;
    }
    
    
    public void filtroDirecao(){
        List<id> goupMemberIdList = groupMember();
        if(String.isEmpty(directionCase)){
            //showListCase = new List<Case>();
		    showListCase = [select id, Type, Subject__c,Description, Expiration_Date__c, CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   		from Case 
                        where RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                         (CreatedById =: UserInfo.getUserId() or 
                         OwnerId =: UserInfo.getUserId() or 
                         OwnerId in:(goupMemberIdList)) 
                         order by LastModifiedDate Desc 
                        limit 999 ];   
      
        }else{
            showListCase = [select id, Type, Subject__c,Description, Expiration_Date__c, CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   		from Case 
                        where CAC_Direction__c =:directionCase and
                            RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                        (CreatedById =: UserInfo.getUserId() or 
                         OwnerId =: UserInfo.getUserId() or 
                         OwnerId in:(goupMemberIdList)) 
                        order by LastModifiedDate Desc 
                        limit 999 ];
            
        }
        
    }
    
    
    
    public String departmentCase{ get; set;}
    public PageReference  departmentCa(){
        filtroDepartamento();
        return null;
    }
    public void filtroDepartamento(){
        List<id> goupMemberIdList = groupMember();
        showListCase = new List<Case>();
        if(String.isNotBlank(departmentCase))
		    showListCase = [select id, Type, Subject__c,Description, Expiration_Date__c, CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   		from Case 
                        where CAC_Department__c =:departmentCase and
                            RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                        (CreatedById =: UserInfo.getUserId() or 
                         OwnerId =: UserInfo.getUserId() or 
                         OwnerId in:(goupMemberIdList)) 
                        order by LastModifiedDate Desc 
                        limit 999 ];   
        else
           showListCase = [select id, Type, Subject__c,Description, Expiration_Date__c, CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   		from Case 
                        where CAC_Direction__c =:directionCase and 
                           RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                        (CreatedById =: UserInfo.getUserId() or 
                         OwnerId =: UserInfo.getUserId()or 
                         OwnerId in:(goupMemberIdList)) 
                        order by LastModifiedDate Desc 
                        limit 999 ];  
        
    }
    
    
    
    public CAC_DealerChoiceDeptCaseController(){}
    public CAC_DealerChoiceDeptCaseController(ApexPages.StandardController controller){
        List<id> goupMemberIdList = groupMember();
        System.debug('#### goupMemberIdList '+goupMemberIdList);
        showListCase = [select id, Type, Subject__c,Description, Expiration_Date__c, CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   from Case where RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                        (CreatedById =: UserInfo.getUserId() or 
                         OwnerId =: UserInfo.getUserId() or 
                         OwnerId in:(goupMemberIdList))   order by LastModifiedDate Desc limit 999 ];
    }
    
    public PageReference newCase(){
        PageReference pg;
        pg = new PageReference('/apex/CAC_01_ChoiceCaseByDept?retURL=/apex/CAC_DealerChoiceDeptCase');
        pg.setRedirect(true);
        return pg;
    }
    
    
    public  void newValuesToFilter(){
        String opcao =typeCaseSelect;
        showListCase = new List<Case>();
        if(String.isEmpty(opcao) ||  String.isBlank(opcao)){
            showListCase = [select id, Subject__c,Description, Expiration_Date__c, Type,CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status 
                   from Case 
                   where 
                            RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                            (CreatedById =: UserInfo.getUserId() or OwnerId =: UserInfo.getUserId())  order by LastModifiedDate Desc  limit 999 ];    
        }else{
            showListCase = [select id, Subject__c,Description, Expiration_Date__c, Type,CaseNumber ,CreatedDate ,VIN__r.Name,VIN__r.VehicleRegistrNbr__c,VIN__r.Model__c,Status  
                   from Case 
                   where RecordTypeId =:opcao and 
                            RecordType.DeveloperName not in:(recordTypeDontSeeList) and
                            (CreatedById =: UserInfo.getUserId() or OwnerId =: UserInfo.getUserId()) order by LastModifiedDate Desc  limit 999 ];
        }
    }
    
    
    
    public String recordTypeWereSelected{get;set;}
    public List<SelectOption> getAvailableRecordType() {
        Schema.SObjectType objType = Case.SObjectType;
        List<SelectOption> names = new List<SelectOption>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        // If there are 2 or more RecordTypes...
        if (infos.size() > 1) {
            //infos.sort();
            for (RecordTypeInfo i : infos) {
               if (i.isAvailable() && !i.getName().equalsIgnoreCase('Mestre') && !i.getName().equalsIgnoreCase('Master'))
               // Ignore the Master Record Type, whose Id always ends with 'AAA'.
               // We check the Id because Name can change depending on the user's language.
               // && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                   names.add( new SelectOption(i.getRecordTypeId(),i.getName()));
            }
        } 
        // Otherwise there's just the Master record type,
        // so add it in, since it MUST always be available
        else names.add(new SelectOption(infos[0].getRecordTypeId(),infos[0].getName()));
        names.sort();
        return names;
    }
    
  
    
    
    public List<Id> groupMember(){
        List<Id> groupIdList = new List<Id>();
        for(GroupMember groupMember : [SELECT GroupId,Id,SystemModstamp,UserOrGroupId 
                                       FROM GroupMember 
                                       WHERE UserOrGroupId =: UserInfo.getUserRoleId() or 
                                       UserOrGroupId =: UserInfo.getUserId()]){
             groupIdList.add(groupMember.GroupId);
         }
        return groupIdList;
    }
    
}