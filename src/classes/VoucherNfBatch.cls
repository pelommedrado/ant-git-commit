global class VoucherNfBatch implements Schedulable, Database.Batchable<sObject> {
    
    global void execute(SchedulableContext sc) {
        System.debug('Iniciando a execucao do Schedulable...');
        
        VoucherNfBatch fatBatch = new VoucherNfBatch();
        Database.executeBatch(fatBatch);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(
            'SELECT Lead.Id, Campaign.Name, ' 		+ 
            'Lead.Name, Lead.CPF_CNPJ__c, ' 		+ 
            'Lead.Voucher_Protocol__c,' 			+ 
            'Lead.Description, Campaign.EndDate, ' 	+ 
            'Lead.Status, Lead.VIN__c, Lead.Vehicle_Invoice__c ' + 
            
            'FROM CampaignMember ' 					+ 
            'WHERE Lead.RecordTypeId = \'' + Utils.getRecordTypeId('Lead', 'Voucher') + 
            '\' AND Lead.Status = \'Rescued\' AND Campaign.Billing_Required__c = true ' + 
            ' AND Lead.CPF_CNPJ__c != null AND Lead.VIN__c != null AND Lead.Vehicle_Invoice__c != null'
        );
        
    }
    
    global void execute(Database.BatchableContext BC, List<CampaignMember> scope) {
        System.debug('Iniciando a execucao do Batch...: ' + scope);
        
        Set<String> cpfList 	= new Set<String>();
        Set<String> chassiList	= new Set<String>();
        Set<String> nfList		= new Set<String>();
        
        try {
            System.debug('Scope: ' + scope);
            
            Map<String, Lead> leadMap = new Map<String, Lead>();
            
            for(CampaignMember cm : scope) {
                cpfList.add(cm.Lead.CPF_CNPJ__c);
                chassiList.add(cm.Lead.VIN__c);
                nfList.add(cm.Lead.Vehicle_Invoice__c);
                
                Lead lead = new Lead(Id= cm.Lead.Id, Status = 'Valid');
                lead.CPF_CNPJ__c = cm.Lead.CPF_CNPJ__c;
                lead.VIN__c = cm.Lead.VIN__c;
                lead.Vehicle_Invoice__c = cm.Lead.Vehicle_Invoice__c;
                
                leadMap.put(cm.Lead.CPF_CNPJ__c + cm.Lead.VIN__c + Lead.Vehicle_Invoice__c, lead);
            }
            
            System.debug('leadMap:' 	+ leadMap.values());
            System.debug('cpfList:' 	+ cpfList);
            System.debug('chassiList:' 	+ chassiList);
            System.debug('nfList:' 		+ nfList);
            
            List<FaturaDealer__c> fatDealerList = [
                SELECT Id, NFCPFCGC__c, NFCHASSI__c, NFNRONFI__c
                FROM FaturaDealer__c 
                WHERE NFCHASSI__c IN:(chassiList) AND 
                NFNRONFI__c IN:(nfList) AND NFCPFCGC__c IN: (cpfList) AND 
                Reprocessed__c = false AND NFTIPREG__c = '10' AND NFBANDEI__c = 'REN' AND
                NFTIPONF__c = 'VN' AND RecordTypeId =: FaturaDealerServico.VALID
            ];
            
            System.debug('fatDealerList:' + fatDealerList);
            
            List<Lead> leadList = new List<Lead>();
            
            for(FaturaDealer__c fatD : fatDealerList) {
                Lead temp = leadMap.get(fatD.NFCPFCGC__c + fatD.NFCHASSI__c + fatD.NFNRONFI__c);
                
                if(temp != null) {
                    leadList.add(temp);
                }
            }
            
            if(!leadList.isEmpty()) {
                UPDATE leadList;
            }
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
}