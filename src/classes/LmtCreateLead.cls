public class LmtCreateLead {
    
    public static Lead criarLead(String nome, 
                                 String sobrenome, 
                                 String email, 
                                 String telefone, 
                                 String celular, 
                                 String cidade, 
                                 String veiculo, 
                                 String concessionaria,
                                 String leadSource, 
                                 String detalhe, 
                                 String subDetalhe, 
                                 String novSms, 
                                 String novEmail, 
                                 String campanha,
                                 String cpf,
                                 String estado,
                                 String tipoLead,
                                 String origem,
                                 String moeda,
                                 String country,
                                 String leadProvider,
                                 String brandOfInterest) 
    {
        
        System.debug('Criando novo lead...');        
        Lead lead = new Lead();
        
        lead.FirstName                  = nome;
        lead.LastName                   = sobrenome;
        lead.Email                      = email;
        lead.Home_Phone_Web__c          = telefone;
        lead.Mobile_Phone__c            = celular;
        lead.City                       = cidade;
        lead.VehicleOfInterest__c       = veiculo;
        lead.BIR_Dealer_of_Interest__c  = concessionaria;
        lead.LeadSource                 = leadSource;
        //lead.Detail__c                  = detalhe;
        //lead.Sub_Detail__c              = concessionaria;
        lead.OptinSMS__c                = novSms;
        lead.OptinEmail__c              = novEmail;
        
        lead.CPF_CNPJ__c                = cpf;
        lead.State                      = estado;
        lead.RecordTypeId               = tipoLead;
        lead.SubSource__c               = origem;
        lead.CurrencyIsoCode            = moeda;
        lead.ownerId = '00GD0000004SfZq';
        //lead.Campaign_ID              = campanha;
        lead.Country__c                 = country;
        
        return lead;
    }
}