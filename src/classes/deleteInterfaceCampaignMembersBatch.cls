global class deleteInterfaceCampaignMembersBatch implements Database.Batchable<sObject> {
    
    global final String Query;
    
    global deleteInterfaceCampaignMembersBatch(String q){
        Query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<InterfaceCampaignMembers__c> scope) {
        
        List<Id> InterfaceCampaignMembersIds = new List<Id>();
        for(InterfaceCampaignMembers__c cm: scope){
            InterfaceCampaignMembersIds.add(cm.Id);
        }
        
        Database.Delete(InterfaceCampaignMembersIds,false);
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        
        String message = 'Seu processo de exclusão de InterfaceCampaignMembers foi concluído. \n'
            + 'Status: ' + a.Status;
        
        enviaEmail('Processo de exclusão de InterfaceCampaignMembers', 'Status', message);
    }
    
    public void enviaEmail(String senderDisplayName, String subject, String message ){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});
        //mail.setReplyTo('batch@acme.com');
        mail.setSenderDisplayName(senderDisplayName);
        mail.setSubject(subject);
        mail.setPlainTextBody(message);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}