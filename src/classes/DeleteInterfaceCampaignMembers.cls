global class DeleteInterfaceCampaignMembers implements Database.Batchable<sObject> {
    
    public String query {get;set;}
    
    public DeleteInterfaceCampaignMembers() {
        this.query = 'SELECT Id from InterfaceCampaignMembers__c where CreatedDate < LAST_N_DAYS:30';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<InterfaceCampaignMembers__c> scope) { 
        List<Database.DeleteResult> result;
        if(scope != null && !scope.isEmpty())
            result = Database.Delete(scope,false);
        for(Database.DeleteResult dr: result){
            if(!dr.isSuccess()){
                system.debug('Error: '+dr.getErrors());   
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}