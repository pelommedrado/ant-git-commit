@isTest(seeAllData=true)
private class ClienteOportunidadeSfa2ControllerTest {

    static Opportunity criaObjetos(){

        //cria e insere a conta que representa a concessionária
        Account sObjAccDealer = new Account();
        sObjAccDealer.Name = 'Dealer Test';
        sObjAccDealer.IDBIR__c = '10450';
        sObjAccDealer.Phone='1000';
        sObjAccDealer.RecordTypeId = Utils.getRecordTypeId('Account','Network_Site_Acc');
        sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com';
        sObjAccDealer.Shipping_City__c = 'Paris';
        sObjAccDealer.Shipping_Country__c = 'France';
        sObjAccDealer.Shipping_State__c = 'IDF';
        sObjAccDealer.Shipping_PostalCode__c = '75013';
        sObjAccDealer.Shipping_Street__c = 'my street';
        insert sObjAccDealer;

        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.CustomerIdentificationNbr__c = '15221385872';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc');
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com';
        sObjAccPersonal.Shipping_City__c = 'Paris';
        sObjAccPersonal.Shipping_Country__c = 'France';
        sObjAccPersonal.Shipping_State__c = 'IDF';
        sObjAccPersonal.Shipping_PostalCode__c = '75013';
        sObjAccPersonal.Shipping_Street__c = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
        insert sObjAccPersonal;

        /*cria e insere o veículo*/
        VEH_Veh__c sObjVehicle = new VEH_Veh__c();
        sObjVehicle.Name = 'TESTCHASSISANDERO';
        sObjVehicle.Model__c = 'Sandero';
        sObjVehicle.Version__c = 'Stepway 1.6 8V';
        sObjVehicle.VersionCode__c = 'S1';
        sObjVehicle.Color__c = 'Black';
        sObjVehicle.VehicleRegistrNbr__c = 'ABC-1234';
        insert sObjVehicle;

        TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
        sObjTestDrive.Status__c = 'Scheduled';
        sObjTestDrive.Opportunity__c = null;
        Insert sObjTestDrive;

        Opportunity opp1 = new Opportunity();
        boolean isCreatedOpportunity1 = false;

        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb21 = [select id from Pricebook2 where IsStandard = true limit 1];

        // criar nova oportunidade
        opp1.AccountId = sObjAccPersonal.Id;
        opp1.Name = 'OppNameTest_1';
        opp1.StageName = 'Identified';
        opp1.CloseDate = Date.today();
        opp1.Pricebook2Id = pb21.Id;
        opp1.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opp1.CurrencyIsoCode = 'BRL';
        opp1.SourceMedia__c = 'Telephone';
        insert opp1;

        MLC_Molicar__c molicar = new MLC_Molicar__c(Brand__c = 'MARCA OD VEICULO');
        Insert molicar;

        return opp1;

    }



    static testMethod void test() {

        Opportunity opp1 = criaObjetos();

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);

        PageReference pageRef = Page.ClienteOportunidadeSfa2;
        pageRef.getParameters().put('id',opp1.id);
        Test.setCurrentPage(pageRef);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        Test.startTest();

        cont.getStates();
        cont.getBrandDetails();
        cont.getBrands();
        cont.getModels();
        cont.selectedBrand ='RENAULT';
        cont.getModelDetails();

        Test.stopTest();
    }

    static testMethod void deveGerarPreOrdem() {

        Opportunity opp1 = criaObjetos();

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);

        PageReference pageRef = Page.ClienteOportunidadeSfa2;
        pageRef.getParameters().put('id',opp1.id);
        Test.setCurrentPage(pageRef);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        Test.startTest();
        cont.qualifyingAccountVO.persMobPhone = '22222222';
        cont.selectedBrand = 'Não possui veículo';
        pageRef.getParameters().put('motivo', 'Lost');
        cont.gerarPreOrdem();
        cont.cancelButtonDisabled = false;

        Test.stopTest();
    }

    static testMethod void deveGerarPerformTestDrive() {

        Opportunity opp1 = criaObjetos();

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);

        PageReference pageRef = Page.ClienteOportunidadeSfa2;
        pageRef.getParameters().put('id',opp1.id);
        Test.setCurrentPage(pageRef);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        Test.startTest();
        cont.qualifyingAccountVO.persMobPhone = '22222222';
        cont.selectedBrand = 'Não possui veículo';
        cont.performTestDrive();
        cont.cancelButtonDisabled = false;

        Test.stopTest();
    }

    static testMethod void test2() {

        Opportunity opp1 = criaObjetos();

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);

        PageReference pageRef = Page.ClienteOportunidadeSfa2;
        pageRef.getParameters().put('id',opp1.id);
        Test.setCurrentPage(pageRef);

        VEH_Veh__c vr = [SELECT Id FROM VEH_Veh__c Limit 1];

        pageRef.getParameters().put('veh', vr.id);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        Test.startTest();

        cont.cancelOpportunity();
        cont.performTestDrive();
        cont.saveOpportunity();

        Test.stopTest();
    }

    static testMethod void test3(){

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('correiostest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml; charset=utf-8');
        mock.setHeader('Content-Length','189');

        Test.setMock(HttpCalloutMock.class, mock);

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller([SELECT Id FROM Opportunity LIMIT 1]);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        Test.startTest();

        cont.qualifyingAccountVO.shippingPostalCode = '09820220';
        cont.getAddressGivenZipCode();

        cont.qualifyingAccountVO.shippingPostalCode = '0982022-';
        cont.getAddressGivenZipCode();

        Test.stopTest();

    }

    static testMethod void test4(){

        Test.startTest();

        Model__c m = new Model__c();
        m.Model_PK__c = 'cod123';
        m.Name = 'SANDERO';
        Insert m;

        PVVersion__c v = new PVVersion__c();
        v.Model__c = m.Id;
        v.PVC_Maximo__c = 3;
        v.PVR_Minimo__c = 1;
        v.Price__c = 35000;
        v.Version_Id_Spec_Code__c = 'codv';
        Insert v;

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller([SELECT Id FROM Opportunity LIMIT 1]);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        cont.qualifyingAccountVO = new VFC46_QualifyingAccountVO();

        cont.verifyRequiredFields();

        cont.qualifyingAccountVO.vehicleInterestBR = 'SANDERO';

        cont.getVersionsDetail();

        cont.getVersions();

        cont.getSecVersions();

        Test.stopTest();

    }

    static testMethod void test5(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        Model__c m = new Model__c();
        m.Model_PK__c = 'cod123';
        m.Name = 'SANDERO';
        Insert m;

        PVVersion__c v = new PVVersion__c();
        v.Model__c = m.Id;
        v.PVC_Maximo__c = 3;
        v.PVR_Minimo__c = 1;
        v.Price__c = 35000;
        v.Version_Id_Spec_Code__c = 'codv';
        Insert v;

        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = acc.Id;
        Insert opp;

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller([SELECT Id FROM Opportunity LIMIT 1]);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);

        cont.qualifyingAccountVO = new VFC46_QualifyingAccountVO();

        cont.qualifyingAccountVO.numCL = '05451886288';
        cont.qualifyingAccountVO.opportunityId = opp.Id;
        cont.qualifyingAccountVO.vehicleInterestBR = 'SANDERO';
        cont.qualifyingAccountVO.versionInterest = 'SANDERO';
        cont.saveOpportunity();

        Test.stopTest();

    }

        static testMethod void test6(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        Account acc = moc.criaPersAccount();
        Insert acc;

        Model__c m = new Model__c();
        m.Model_PK__c = 'cod123';
        m.Name = 'SANDERO';
        Insert m;

        PVVersion__c v = new PVVersion__c();
        v.Model__c = m.Id;
        v.PVC_Maximo__c = 3;
        v.PVR_Minimo__c = 1;
        v.Price__c = 35000;
        v.Version_Id_Spec_Code__c = 'codv';
        Insert v;

        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = acc.Id;
        Insert opp;

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller([SELECT Id FROM Opportunity LIMIT 1]);

        ClienteOportunidadeSfa2Controller cont = new ClienteOportunidadeSfa2Controller(controller);
        //cont.qualifyingAccountVO = new VFC46_QualifyingAccountVO();
        cont.qualifyingAccountVO.numCL = '05451886288';
        cont.qualifyingAccountVO.opportunityId = opp.Id;
        //cont.qualifyingAccountVO.vehicleInterestBR = 'SANDERO';
        //cont.qualifyingAccountVO.versionInterest = 'SANDERO';
        cont.qualifyingAccountVO.persMobPhone = '22222222';
        cont.selectedBrand = 'Não possui veículo';
        //cont.veiculoRel = new VRE_VehRel__c();
        cont.saveOpportunity();

        Test.stopTest();

    }
}