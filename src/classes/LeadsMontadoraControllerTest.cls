@isTest
public class LeadsMontadoraControllerTest {

    static testMethod void myUnitTest() {
        
        MyOwnCreation moc = new MyOwnCreation();
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User communityUser;
        Account dealer;
                
        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {
            
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            dealer = moc.criaAccountDealer();
            Insert(dealer);
            
            Contact contact = moc.criaContato();
            contact.AccountId = dealer.Id;
            Insert(contact);
            
            communityUser = moc.criaUser();
            communityUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            communityUser.ContactId = contact.Id;
            Insert(communityUser);  
        }
        
        //Execução com usuario comunidade
        System.runAs( communityUser ){
            
            Opportunity opp = moc.criaOpportunity();
            opp.Dealer__c = dealer.Id;
            opp.OwnerId = communityUser.Id;
            opp.StageName = 'Identified';
            Insert opp;
            
            LeadsMontadoraController controller = new LeadsMontadoraController();
            String welcomeMessage = controller.welcomeMessage;
            
            controller.oppIdListAsString = '{"'+String.valueOf(opp.Id)+'":"'+String.valueOf(communityUser.Id)+'"}';
            String oppIdListAsString = controller.oppIdListAsString;
            
            List<SelectOption> campaignSelectList = controller.campaignSelectList;
            List<SelectOption> sellerSelectList = controller.sellerSelectList;
            List<SelectOption> dealerSelectList = controller.dealerSelectList;
            
            LeadsMontadoraController.OppWrapper wrapper = new LeadsMontadoraController.OppWrapper();        
            List<LeadsMontadoraController.OppWrapper> oppList = controller.lsOppWrapper;
            
            controller.forward();
        }
    }
}