@istest
class FaturaDealerPilotoBatchTest {
    
    static testmethod void test() {
        Test.startTest();
        
        String cron = '0 0 0 3 9 ? 2022';
        
        String jobId = System.schedule('FaturaDealerPilotoBatch',
                                       cron, new FaturaDealerPilotoBatch());
        
        CronTrigger ct = [
            SELECT Id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
        
        System.assertEquals(cron, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
        
    }
}