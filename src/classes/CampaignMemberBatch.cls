global class CampaignMemberBatch implements Database.Batchable<sObject> {
    
    global final String Query;
    
    global CampaignMemberBatch(String q){
        Query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<InterfaceCampaignMembers__c> scope) {
        
        List<InterfaceCampaignMembers__c> lsICM = [select Id from InterfaceCampaignMembers__c where CreatedDate < LAST_N_DAYS:30 limit 9900];
        
        if(!lsICM.isEmpty())
            Database.delete(lsICM,false);
        
        Id unprocessedRecordType = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Unprocessed');
        
        for(InterfaceCampaignMembers__c icm: scope){
            icm.RecordTypeId = unprocessedRecordType;
            icm.ErrorMessage__c = null;
        }
        
        Database.Update(scope);
        
        LMT_CampaignsMembersLoadController service = new LMT_CampaignsMembersLoadController();
        List<InterfaceCampaignMembers__c> icmList = service.importCSVFile(scope);
        
        try {
            Database.update(scope);
            Database.update(icmList);
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            enviaEmail('Processamento de Batch','Erro de processamento de Batch: ',ex.getMessage());
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email 
                          FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        
        Id validRecordType = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Valid');
        Id invalidRecordType = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Invalid');
        
        Integer valid = [select count() from InterfaceCampaignMembers__c where RecordTypeId =: validRecordType limit 24000];
        Integer invalid = [select count() from InterfaceCampaignMembers__c where RecordTypeId =: invalidRecordType limit 24000];
        
        String message = returnMessage(valid,invalid);
                
        if(message == null){
            message = 'Seu processo foi finalizado com êxito. \n' +
            'Registros Válidos: ' + valid + '; \n' +
            'Registros Inválidos: '+ invalid +'; \n' +
            'Total do Registros Processados: '+ (valid + invalid) +';';  
        }
        
        enviaEmail('Processamento de Batch', 'Status do Batch', message);
        
    }
    
    public void enviaEmail(String senderDisplayName, String subject, String message ){

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});
        //mail.setReplyTo('batch@acme.com');
        mail.setSenderDisplayName(senderDisplayName);
        mail.setSubject(subject);
        mail.setPlainTextBody(message);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public String returnMessage(Integer valid, Integer invalid){
        String message = null;
        if(valid == 24000){
            message = 'Seu processo foi finalizado com êxito. \n' + 'Registros Válidos: ' + valid + ' ou mais registros; \n' +
                'Registros Inválidos: '+ invalid +'; \n' + 'Total aproximado de registros processados: '+ (valid + invalid) +';';
        }else if(invalid == 24000){
            message = 'Seu processo foi finalizado com êxito. \n' + 'Registros Válidos: ' + valid + '; \n' +
                'Registros Inválidos: '+ invalid +' ou mais registros; \n' + 'Total aproximado de registros processados: '+ (valid + invalid) +';';
        }
        return message;
    }
}