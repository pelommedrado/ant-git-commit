@isTest
private class VFC100_VehicleQueryBuilderTest {

    static testMethod void deveSearch() {
        VFC100_VehicleQueryBuilder vfc = VFC100_VehicleQueryBuilder.getInstance();
        List<VRE_VehRel__c> lista = vfc.search('dealer', 'model', 'version', 2015, 'Color');
        System.assert(lista == null);
        lista = vfc.search('dealer', 'model', 'version', null, 'Color');
    }
}