public class WS02_otsIcmApvBserviceRenault{
//Generated by wsdl2apex
// RV Note: Add "extends Exception" to any class that is an exception

    public class getListOtsResponse_element {
        public WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse getListOtsReturn;
        private String[] getListOtsReturn_type_info = new String[]{'getListOtsReturn','http://ots.icm.apv.bservice.renault','ApvGetListOtsResponse','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'getListOtsReturn'};
    }
    public class ApvGetListOtsRequest {
        public WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequestParms requestParms;
        public WS02_otsIcmApvBserviceRenault.ServicePreferences servicePreferences;
        private String[] requestParms_type_info = new String[]{'requestParms','http://ots.icm.apv.bservice.renault','ApvGetListOtsRequestParms','1','1','true'};
        private String[] servicePreferences_type_info = new String[]{'servicePreferences','http://ots.icm.apv.bservice.renault','ServicePreferences','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'requestParms','servicePreferences'};
    }
    public class Ots {
        public String codeCouleur;
        public String descOts;
        public String libCouleur;
        public String libOts;
        public String numChif;
        public String numNoteTech;
        public String numOts;
        public String topAgre;
        private String[] codeCouleur_type_info = new String[]{'codeCouleur','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] descOts_type_info = new String[]{'descOts','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libCouleur_type_info = new String[]{'libCouleur','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] libOts_type_info = new String[]{'libOts','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] numChif_type_info = new String[]{'numChif','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] numNoteTech_type_info = new String[]{'numNoteTech','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] numOts_type_info = new String[]{'numOts','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] topAgre_type_info = new String[]{'topAgre','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'codeCouleur','descOts','libCouleur','libOts','numChif','numNoteTech','numOts','topAgre'};
    }
    public class SiUnavailableException extends Exception {
        public WS02_otsIcmApvBserviceRenault.OtsInfoMsg otsInfoMsgs;
        private String[] otsInfoMsgs_type_info = new String[]{'otsInfoMsgs','http://ots.icm.apv.bservice.renault','OtsInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'otsInfoMsgs'};
    }
    public class OtsInfoMsg {
        public String code;
        public String text;
        private String[] code_type_info = new String[]{'code','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] text_type_info = new String[]{'text','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'code','text'};
    }
    public class getListOts_element {
        public WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequest request;
        private String[] request_type_info = new String[]{'request','http://ots.icm.apv.bservice.renault','ApvGetListOtsRequest','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'request'};
    }
    public class NoDataFoundException extends Exception {
        public WS02_otsIcmApvBserviceRenault.OtsInfoMsg otsInfoMsgs;
        private String[] otsInfoMsgs_type_info = new String[]{'otsInfoMsgs','http://ots.icm.apv.bservice.renault','OtsInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'otsInfoMsgs'};
    } 
    public class ApvGetListOtsResponse {
        public WS02_otsIcmApvBserviceRenault.Ots[] apvGetListOts;
        public WS02_otsIcmApvBserviceRenault.OtsInfoMsg[] apvGetListOtsMsg;
        private String[] apvGetListOts_type_info = new String[]{'apvGetListOts','http://ots.icm.apv.bservice.renault','Ots','0','-1','true'};
        private String[] apvGetListOtsMsg_type_info = new String[]{'apvGetListOtsMsg','http://ots.icm.apv.bservice.renault','OtsInfoMsg','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'apvGetListOts','apvGetListOtsMsg'};
    }
    public class ApvGetListOtsRequestParms {
        public String codeTrace;
        public String countryLanguage;
        public String typeSel;
        public String vin;
        private String[] codeTrace_type_info = new String[]{'codeTrace','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] countryLanguage_type_info = new String[]{'countryLanguage','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] typeSel_type_info = new String[]{'typeSel','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] vin_type_info = new String[]{'vin','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'codeTrace','countryLanguage','typeSel','vin'};
    }
    public class ApvGetListOts {
        public String endpoint_x = 'http://icm.intra.renault.fr/icm/services/ApvGetListOts';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://ots.icm.apv.bservice.renault', 'WS02_otsIcmApvBserviceRenault'};
        public WS02_otsIcmApvBserviceRenault.ApvGetListOtsResponse getListOts(WS02_otsIcmApvBserviceRenault.ApvGetListOtsRequest request) {
            WS02_otsIcmApvBserviceRenault.getListOts_element request_x = new WS02_otsIcmApvBserviceRenault.getListOts_element();
            WS02_otsIcmApvBserviceRenault.getListOtsResponse_element response_x;
            request_x.request = request;
            Map<String, WS02_otsIcmApvBserviceRenault.getListOtsResponse_element> response_map_x = new Map<String, WS02_otsIcmApvBserviceRenault.getListOtsResponse_element>();
            response_map_x.put('response_x', response_x);
              
            // RV adjusted start
            if (Test.isRunningTest()) {
                response_x = new WS02_otsIcmApvBserviceRenault.getListOtsResponse_element();
                response_x.getListOtsReturn = Utils_Stubs.OTSStub();
            } else {
            	
            	System.debug('endpoint_x >>>>>>>>>'+endpoint_x);
            	System.debug('request_x>>>>>>>>'+request_x);
    
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  '',
                  'http://ots.icm.apv.bservice.renault',
                  'getListOts',
                  'http://ots.icm.apv.bservice.renault',
                  'getListOtsResponse',
                  'WS02_otsIcmApvBserviceRenault.getListOtsResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                
                System.debug('response_x>>>>>>>>'+response_x);
                
            }
            return response_x.getListOtsReturn;
        } // RV adjusted end
    }
    public class BadRequestException extends Exception {
        public WS02_otsIcmApvBserviceRenault.OtsInfoMsg otsInfoMsgs;
        private String[] otsInfoMsgs_type_info = new String[]{'otsInfoMsgs','http://ots.icm.apv.bservice.renault','OtsInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'otsInfoMsgs'};
    }
    public class SiErrorException extends Exception {
        public WS02_otsIcmApvBserviceRenault.OtsInfoMsg otsInfoMsgs;
        private String[] otsInfoMsgs_type_info = new String[]{'otsInfoMsgs','http://ots.icm.apv.bservice.renault','OtsInfoMsg','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'otsInfoMsgs'};
    }
    public class ServicePreferences {
        public String country;
        public String language;
        public String reqid;
        public String sia;
        public String userid;
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] language_type_info = new String[]{'language','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] reqid_type_info = new String[]{'reqid','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] sia_type_info = new String[]{'sia','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] userid_type_info = new String[]{'userid','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://ots.icm.apv.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'country','language','reqid','sia','userid'};
    }

}