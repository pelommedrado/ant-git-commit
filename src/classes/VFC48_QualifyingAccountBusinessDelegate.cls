/**
* Classe responsável por fazer o papel de business delegate da página de qualificação de conta.
* @author Felipe Jesus Silva.
*/
public class VFC48_QualifyingAccountBusinessDelegate 
{
    private static final VFC48_QualifyingAccountBusinessDelegate instance = new VFC48_QualifyingAccountBusinessDelegate();
    
    /**
    * Construtor privado para impedir a criação de instancias dessa classe.
    */
    private VFC48_QualifyingAccountBusinessDelegate()
    {
        
    }
    
    /**
    * Método responsável por prover a instância dessa classe.
    */  
    public static VFC48_QualifyingAccountBusinessDelegate getInstance()
    {
        return instance;
    }
    
    public VFC46_QualifyingAccountVO getAccountByOpportunityId(String opportunityId)
    {
        // obtém a oportunidade pelo id
        Opportunity sObjOpportunity = VFC47_OpportunityBO.getInstance().findById(opportunityId);

        // obtém o value object já mapeado
        VFC46_QualifyingAccountVO qualifyingAccountVO = this.mapToValueObject(sObjOpportunity);

        // Fetch the campaign object associated to the opportunity
        // If there's no campaignId associated, the campaign object will be null
        if (sObjOpportunity != null)
            qualifyingAccountVO.sObjCampaign = VFC121_CampaignDAO.getInstance().fetchCampaignById(sObjOpportunity.campaignId);
        
        return qualifyingAccountVO;
    }
    
    public void updateDataForTestDriveStage(VFC46_QualifyingAccountVO qualifyingAccountVO)
    {
        Account sObjAccount = null;
        Opportunity sObjOpportunity = null;
        
        /*mapeia os dados do VO para o objeto account*/
        sObjAccount = this.mapToAccountObject(qualifyingAccountVO);
        
        /*cria a oportunidade e seta os dados que devem ser atualizados*/
        sObjOpportunity = new Opportunity(Id = qualifyingAccountVO.opportunityId);
        
        // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
        
        sObjOpportunity.StageName = 'Test Drive';
        sObjOpportunity.Description = qualifyingAccountVO.description;
        
        /*atualiza a oportunidade e a conta*/
        VFC47_OpportunityBO.getInstance().updateOpportunityAndAccount(sObjOpportunity, sObjAccount);
    }
    
    public void updateData(VFC46_QualifyingAccountVO qualifyingAccountVO)
    {
        Account sObjAccount = null;
        Opportunity sObjOpportunity = null;
        
        /*mapeia os dados do VO para o objeto account*/
        sObjAccount = this.mapToAccountObject(qualifyingAccountVO);
        
        /*cria a oportunidade e seta os dados que devem ser atualizados*/
        sObjOpportunity = new Opportunity(Id = qualifyingAccountVO.opportunityId);
        
        Opportunity op = [
            SELECT Id, StageName
            FROM Opportunity
            Where Id =: sObjOpportunity.Id
        ];
        
        if(op.StageName != 'In Attendance') {
            
            // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        	sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
            
            sObjOpportunity.StageName = 'Identified';
        }
        
        sObjOpportunity.Description = qualifyingAccountVO.description;
        sObjOpportunity.Approach__c = qualifyingAccountVO.oppApproach;
        sObjOpportunity.SourceMedia__c = qualifyingAccountVO.sourcemedia;
        sObjOpportunity.Current_Vehicle__c = qualifyingAccountVO.veiculoAtual;
        sObjOpportunity.CurrentVehicle__c = qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c;
        sObjOpportunity.IsVehicleOwner__c = String.isEmpty(sObjOpportunity.Current_Vehicle__c) ? 'N': 'Y';
        /*atualiza a oportunidade e a conta*/
        VFC47_OpportunityBO.getInstance().updateOpportunityAndAccount(sObjOpportunity, sObjAccount);
    }
    
    public void updateDataForCancellationStage(VFC46_QualifyingAccountVO qualifyingAccountVO)
    {
        Account sObjAccount = null;
        Opportunity sObjOpportunity = null;
        
        /*List<Task> listTask = VFC33_TaskDAO.getInstance().findOpenRecurrencebyOpportunityId(qualifyingAccountVO.opportunityId); 
                    
        if(listTask != null && listTask.size() > 0){
            throw new VFC34_ApplicationException('Você deve encerrar as tarefas recursivas desta oportunidade antes de cancelar.');
        } */ 
        
        /*mapeia os dados do VO para o objeto account*/
        sObjAccount = this.mapToAccountObject(qualifyingAccountVO);
        
        /*cria a oportunidade e seta os dados que devem ser atualizados*/
        sObjOpportunity = new Opportunity(Id = qualifyingAccountVO.opportunityId);
        
        // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
        
        sObjOpportunity.StageName = 'Lost';
        sObjOpportunity.ReasonLossCancellation__c = qualifyingAccountVO.reasonLossCancellation;
        
        /*atualiza a oportunidade e a conta*/
        VFC47_OpportunityBO.getInstance().updateOpportunityAndAccount(sObjOpportunity, sObjAccount);
    }
    
    private VFC46_QualifyingAccountVO mapToValueObject(Opportunity sObjOpportunity)
    {
        VFC46_QualifyingAccountVO qualifyingAccountVO = new VFC46_QualifyingAccountVO();
        
        /*mapeia os campos do objeto opportunity para os campos do VO*/
        qualifyingAccountVO.opportunityId = sObjOpportunity.Id;
        qualifyingAccountVO.accountId = sObjOpportunity.AccountId;  
        qualifyingAccountVO.firstName = sObjOpportunity.Account.FirstName;
        qualifyingAccountVO.lastName = sObjOpportunity.Account.LastName;
        qualifyingAccountVO.numCL =  sObjOpportunity.Account.CustomerIdentificationNbr__c;
        qualifyingAccountVO.sObjAccount.PersonBirthdate = sObjOpportunity.Account.PersonBirthdate;
        qualifyingAccountVO.email = sObjOpportunity.Account.PersEmailAddress__c;
        qualifyingAccountVO.optionPhone = (sObjOpportunity.Account.OptinPhone__c == 'N') ? false : true;
        qualifyingAccountVO.optionEmail = (sObjOpportunity.Account.OptinEmail__c == 'N') ? false : true;
        qualifyingAccountVO.optionSMS = (sObjOpportunity.Account.OptinSMS__c == 'N') ? false : true;
        qualifyingAccountVO.persLandline = sObjOpportunity.Account.PersLandline__c;
        qualifyingAccountVO.profLandline = sObjOpportunity.Account.ProfLandline__c;
        qualifyingAccountVO.profMobPhone = sObjOpportunity.Account.ProfMobPhone__c;
        qualifyingAccountVO.persMobPhone = sObjOpportunity.Account.PersMobPhone__c;
        qualifyingAccountVO.typeOfInterest = sObjOpportunity.Account.TypeOfInterest__c;
        qualifyingAccountVO.campaignBR = sObjOpportunity.Account.Campaign_BR__c;
        qualifyingAccountVO.vehicleInterestBR = sObjOpportunity.Account.VehicleInterest_BR__c;
        qualifyingAccountVO.versionInterest = sObjOpportunity.Account.Version_of_Interest__c;
        
        /* @Hugo Medrado {kolekto} PromoCode */
        qualifyingAccountVO.promoCode = sObjOpportunity.PromoCode__c;
        
        qualifyingAccountVO.secondVehicleOfInterest = sObjOpportunity.Account.SecondVehicleOfInterest__c;
        qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c = sObjOpportunity.Account.CurrentVehicle_BR__c;
        qualifyingAccountVO.vehicleInterestBRBrand = sObjOpportunity.Account.CurrentVehicle_BR__r.Brand__c;
        qualifyingAccountVO.vehicleInterestBRModel = sObjOpportunity.Account.CurrentVehicle_BR__r.Model__c;
        qualifyingAccountVO.description = sObjOpportunity.Description;
        if(sObjOpportunity.Account.YrReturnVehicle_BR__c != null) {
            qualifyingAccountVO.yrReturnVehicleBR = 
                String.valueOf(sObjOpportunity.Account.YrReturnVehicle_BR__c.intValue());
        }
        
        qualifyingAccountVO.shippingStreet = sObjOpportunity.Account.ShippingStreet;
        qualifyingAccountVO.shippingCity = sObjOpportunity.Account.ShippingCity;
        qualifyingAccountVO.shippingState = sObjOpportunity.Account.ShippingState;
        qualifyingAccountVO.shippingPostalCode = sObjOpportunity.Account.ShippingPostalCode;
        qualifyingAccountVO.shippingCountry = sObjOpportunity.Account.ShippingCountry;
		qualifyingAccountVO.sourcemedia = sObjOpportunity.SourceMedia__c;
        qualifyingAccountVO.campaignId = sObjOpportunity.campaignId;

        qualifyingAccountVO.veiculoAtual = sObjOpportunity.Current_Vehicle__c;
        return qualifyingAccountVO;
    }
    
    private Account mapToAccountObject(VFC46_QualifyingAccountVO qualifyingAccountVO)
    {
        Account sObjAccount = new Account(Id = qualifyingAccountVO.accountId);
        
        /*mapeia os dados no objeto account*/
        sObjAccount.FirstName = qualifyingAccountVO.firstName;
        sObjAccount.LastName = qualifyingAccountVO.lastName;
        sObjAccount.CustomerIdentificationNbr__c = qualifyingAccountVO.numCL;
        sObjAccount.PersonBirthdate = qualifyingAccountVO.sObjAccount.PersonBirthdate;
        sObjAccount.PersEmailAddress__c = qualifyingAccountVO.email;
        sObjAccount.OptinPhone__c = qualifyingAccountVO.optionPhone ? 'Y' : 'N';
        sObjAccount.OptinEmail__c = qualifyingAccountVO.optionEmail ? 'Y' : 'N';
        sObjAccount.OptinSMS__c = qualifyingAccountVO.optionSMS ? 'Y' : 'N';
        sObjAccount.PersLandline__c = qualifyingAccountVO.persLandline;
        sObjAccount.ProfLandline__c = qualifyingAccountVO.profLandline;
        sObjAccount.ProfMobPhone__c = qualifyingAccountVO.profMobPhone;
        sObjAccount.PersMobPhone__c = qualifyingAccountVO.persMobPhone;
        sObjAccount.TypeOfInterest__c = qualifyingAccountVO.typeOfInterest;
        sObjAccount.Campaign_BR__c = qualifyingAccountVO.campaignBR;
        sObjAccount.VehicleInterest_BR__c = qualifyingAccountVO.vehicleInterestBR;
        sObjAccount.SecondVehicleOfInterest__c = qualifyingAccountVO.secondVehicleOfInterest;
        sObjAccount.CurrentVehicle_BR__c = qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c; 
        sObjAccount.Version_of_Interest__c = qualifyingAccountVO.versionInterest;
        sObjAccount.Second_Version_of_Interest__c = qualifyingAccountVO.secondVersionInterest;
        sObjAccount.SourceMedia_BR__c = qualifyingAccountVO.sourcemedia;
        
        if(String.isNotEmpty(qualifyingAccountVO.yrReturnVehicleBR))
        {
            sObjAccount.YrReturnVehicle_BR__c = Decimal.valueOf(qualifyingAccountVO.yrReturnVehicleBR);
        }
        
        sObjAccount.ShippingStreet = qualifyingAccountVO.shippingStreet;
        sObjAccount.ShippingCity = qualifyingAccountVO.shippingCity;
        sObjAccount.ShippingState = qualifyingAccountVO.shippingState;
        sObjAccount.ShippingPostalCode = qualifyingAccountVO.shippingPostalCode;
        sObjAccount.ShippingCountry = qualifyingAccountVO.shippingCountry;
        
        return sObjAccount;
    }
}