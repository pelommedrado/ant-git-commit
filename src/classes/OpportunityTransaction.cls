public class OpportunityTransaction {
    
    public static void create(List<Opportunity> newOpp, Map<Id, Opportunity> oldMap){
        
        Set<Id> oppsId = getOppsIds(newOpp);
        
        Map<String, TST_Transaction__c> tscMap = getTransactionMap(oppsId);
        
        List<TST_Transaction__c> tstToInsert = setTransactions(newOpp, oldMap, tscMap);
        
        insertTransactions(tstToInsert);
        
    }
    
    public static void insertTransactions(List<TST_Transaction__c> tstToInsert){
        try{
            Database.insert(tstToInsert);
        } catch(Exception e){
            System.debug('################# erros: ' + e);
        }
    }
    
    public static List<TST_Transaction__c> setTransactions(List<Opportunity> newOpp, Map<Id, Opportunity> oldMap, Map<String, TST_Transaction__c> tscMap){
        
        List<TST_Transaction__c> tstToInsert = new List<TST_Transaction__c>();
        Set<Id> oppsToUse = new Set<Id>();
        
        for(Opportunity opp : newOpp){
            Opportunity old = oldMap.get(opp.Id);
            if(opp.StageName != old.StageName &&
              !tscMap.containsKey(opp.Id + opp.StageName)){
                oppsToUse.add(opp.Id);
            }
        }
        
        
        Map<Id, TST_Transaction__c> lastTscMap = findLastTransactions(oppsToUse);
        
        for(Opportunity opp : newOpp){
            Opportunity old = oldMap.get(opp.Id);
            if(opp.StageName != old.StageName &&
              !tscMap.containsKey(opp.Id + opp.StageName)){
            tstToInsert.addAll(transactionToInsert(old, opp, lastTscMap));
            }
        }
        
        return tstToInsert;
        
    }
    
    public static TST_Transaction__c createInAttendance(Opportunity opp, Opportunity old){
        
        TST_Transaction__c tst = new TST_Transaction__c();
        tst.Opportunity_Last_Status__c = 'Identified';
        tst.Opportunity__c = old.Id;
        tst.Opportunity_Status__c = 'In Attendance';
        return tst;
        
    }
    
    public static TST_Transaction__c createAfterInAttendance(Opportunity opp, Opportunity old){
        
        TST_Transaction__c tst = new TST_Transaction__c();
        tst.Opportunity_Last_Status__c = 'In Attendance';
        tst.Opportunity__c = old.Id;
        tst.Opportunity_Status__c = opp.StageName;
        tst.Stage_Duration__c = Math.floor(
            Double.valueof((System.now().getTime() - old.CreatedDate.getTime()) / (1000.0*60.0*60.0)
            )
        );
        return tst;
        
    }
    
    
    public static List<TST_Transaction__c> transactionToInsert(Opportunity old, Opportunity opp, Map<Id, TST_Transaction__c> tstOld){
        
        List<TST_Transaction__c> tstToInsert = new List<TST_Transaction__c>();
        
        if(old.StageName != 'Identified' || !tstOld.isEmpty()){
            
            TST_Transaction__c tst = new TST_Transaction__c();
            
            //pega id do mapa e faz cálculo de duração da fase
            for(Id idKey : tstold.keySet()){
                if(tstold.get(idKey).Opportunity__c == old.Id){
                    tst.Stage_Duration__c = Math.floor(
                        Double.valueof(
                            (System.now().getTime() - tstOld.get(idKey).CreatedDate.getTime()) / (1000.0*60.0*60.0)
                        )
                    );
                }
            }
            
            tst.Opportunity_Last_Status__c = old.StageName;
            tst.Opportunity__c = old.Id;
            tst.Opportunity_Status__c = opp.StageName;
            
            tstToInsert.add(tst);
            
            
        } else if(old.StageName.equalsIgnoreCase('Identified') && opp.StageName.equalsIgnoreCase('Quote')){
            
            tstToInsert.add(createInAttendance(opp, old));
            tstToInsert.add(createAfterInAttendance(opp, old));
            
        } else if(old.StageName.equalsIgnoreCase('Identified') && opp.StageName.equalsIgnoreCase('Test Drive')){
            
            tstToInsert.add(createInAttendance(opp, old));
            tstToInsert.add(createAfterInAttendance(opp, old));
            
        } else{
            
            TST_Transaction__c tst = new TST_Transaction__c();
            
            //caso estiver como identificado, apenas calcula com a data de criação da opp
            tst.Stage_Duration__c = Math.floor(
                Double.valueof(
                    (System.now().getTime() - old.CreatedDate.getTime()) / (1000.0*60.0*60.0)
                )
            );
            
            tst.Opportunity_Last_Status__c = old.StageName;
            tst.Opportunity__c = old.Id;
            tst.Opportunity_Status__c = opp.StageName;
            
            tstToInsert.add(tst);
        }
        
        return tstToInsert;
        
    }
    
    public static Set<Id> getOppsIds(List<Opportunity> newOpp){
            
        Set<Id> oppsId = new Set<Id>();
        for(Opportunity ids : newOpp){
            oppsId.add(ids.id);
        }
        return oppsId;
    }
    
    public static Map<String, TST_Transaction__c> getTransactionMap(Set<Id> oppsId){
        
        Map<String, TST_Transaction__c> tscMap = new Map<String, TST_Transaction__c>();
        List<TST_Transaction__c> tscList = findTransactions(oppsId);
        
        for(TST_Transaction__c tsc : tscList){
            tscMap.put(tsc.Opportunity__c + tsc.Opportunity_Status__c, tsc);
        }
        return tscMap;
        
    }
    
    public static List<TST_Transaction__c> findTransactions(Set<Id> oppsId){
        return [SELECT Id, Opportunity__c, Opportunity_Status__c, Opportunity_Last_Status__c, CreatedDate
                FROM TST_Transaction__c 
                WHERE Opportunity__c IN :oppsId
                ORDER BY CreatedDate DESC];
    }
    
    public static Map<Id, TST_Transaction__c> findLastTransactions(Set<Id> oppsId){
        Map<Id, TST_Transaction__c> tstOld = new Map<Id, TST_Transaction__c>([SELECT Id, Opportunity__c, Opportunity_Status__c, Opportunity_Last_Status__c, CreatedDate
                                                                      FROM TST_Transaction__c 
                                                                      WHERE Opportunity__c IN :oppsId
                                                                      ORDER BY CreatedDate DESC
                                                                      ]);
        return tstOld;
    }
    
    /* ------------------- CRIA TRANSACTION VIA VEÍCULO -----------------------*/
    
    public static void createTransactionFromVeh(List<VEH_Veh__c> veh, List<VEH_Veh__c> vehold){
        
        Map<Id, TST_Transaction__c> tstOld;
        
        List<Opportunity> opp = new List<Opportunity>();
        List<TST_Transaction__c> tstToInsert = new List<TST_Transaction__c>();
        
        List<Id> vehIds = new List<Id>();
        List<Id> quoteIds = new List<Id>();
        List<Id> oppIds = new List<Id>();
        
        
        for(VEH_Veh__c v : veh){
            vehIds.add(v.Id);
        }
        
        List<QuoteLineItem> qli = [SELECT Id, QuoteId
                                   FROM QuoteLineItem
                                   WHERE Vehicle__c IN : vehIds];
        
        if(!qli.isEmpty()){
            for(QuoteLineItem q : qli){
                quoteIds.add(q.QuoteId);
            }
        }
        
        List<Quote> q = [SELECT Id, OpportunityId FROM Quote WHERE Id IN :quoteIds];
        
        if(!q.isEmpty()){
            for(Quote qid : q){
                oppIds.add(qid.OpportunityId);
            }
        }
        
        if(!oppIds.isEmpty()){
            opp = [SELECT Id, StageName, CreatedDate FROM Opportunity WHERE Id IN : oppIds ORDER BY CreatedDate DESC];
            
            
            //cria mapa com registros transaction
            tstOld = new Map<Id, TST_Transaction__c>([SELECT Id, Opportunity__c, Opportunity_Status__c, CreatedDate
                                                      FROM TST_Transaction__c 
                                                      WHERE Opportunity__c IN :oppIds
                                                      ORDER BY CreatedDate DESC
                                                      LIMIT 1]);
            
        }
        
        System.debug('veh: ' + veh);
        System.debug('vehold: ' + vehold);
        System.debug('opp: ' + opp);
        System.debug('tstOld: ' + tstOld);
        
        for(VEH_Veh__c v : veh){
            for(VEH_Veh__c vold : vehold){
                if(v.DeliveryDate__c != vold.DeliveryDate__c
                  || v.VehicleRegistrNbr__c != vold.VehicleRegistrNbr__c){
                    for(Opportunity o : opp){
                        tstToInsert.add(setTransactionFromVehicle(v, vold, o, tstOld));
                        System.debug('tstToInsert: ' + tstToInsert);
                    }
                }
            }
        }
        
        try{
            Database.insert(tstToInsert);
        } catch(Exception e){
            System.debug('######################### erro: ' + e);
        }
    }
    
    public static TST_Transaction__c setTransactionFromVehicle(VEH_Veh__c veh, VEH_Veh__c vehold, Opportunity opp, Map<Id, TST_Transaction__c> tstOld){
        
        TST_Transaction__c tst = new TST_Transaction__c();
        if(opp != null){
            
            tst.Opportunity_Last_Status__c = opp.StageName;
            tst.Opportunity__c = opp.id;
            
            if(opp.StageName != 'Identified' || !tstOld.isEmpty()){
                
                for(Id idKey : tstold.keySet()){
                    if(tstold.get(idKey).Opportunity__c == opp.Id){
                        tst.Stage_Duration__c = Math.floor(
                            Double.valueof(
                                (System.now().getTime() - tstOld.get(idKey).CreatedDate.getTime()) / (1000.0*60.0*60.0)
                            )
                        );
                    }
                }
                
            } else{
                //caso estiver como identificado, apenas calcula com a data de criação da opp
                tst.Stage_Duration__c = Math.floor(
                    Double.valueof(
                        (System.now().getTime() - opp.CreatedDate.getTime()) / (1000.0*60.0*60.0)
                    )
                );
            }
            
            if(veh.DeliveryDate__c != vehold.DeliveryDate__c){
                tst.Opportunity_Status__c = 'Delivered';
            }
            
            if(veh.VehicleRegistrNbr__c != vehold.VehicleRegistrNbr__c){
                tst.Opportunity_Status__c = 'Licensed';
            }
        }
        
        return tst;
        
    }
    
}