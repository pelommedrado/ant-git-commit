/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class ConsoleIntegrationController {
    @RemoteAction
    global static void endChat(String conversationRecordId) {

    }
    @RemoteAction
    global static String findLeadSourceVal() {
        return null;
    }
    @RemoteAction
    global static String getPhoneNumberAccounts(String phoneNumber) {
        return null;
    }
    @RemoteAction
    global static String getPhoneNumberContacts(String phoneNumber) {
        return null;
    }
    @RemoteAction
    global static String getPhoneNumberLeads(String phoneNumber) {
        return null;
    }
    @RemoteAction
    global static List<LiveText__SMS_Text__c> getSMSMessages(String conversationHeaderId) {
        return null;
    }
    @RemoteAction
    global static Boolean isConversationHeaderActive(String conversationRecordId) {
        return null;
    }
    @RemoteAction
    global static Boolean isRoutedToCurrentUser(String channelId) {
        return null;
    }
}
