@isTest
public with sharing class VFC148_ChamadaOfertaVersaoTest {
    
    static testMethod void myUnitTest(){
    Model__c model = new Model__c(
      //ENS__c = 'abc',
      Market__c = 'abc',
      Status__c = 'Active',
      Model_Spec_Code__c = 'ABC',
      Phase__c = '1',
      Model_PK__c = 'ABC-1'
    );
    Database.insert( model );
    
    PVVersion__c version = new PVVersion__c(
      Name = 'Fluence',
      Model__c = model.Id,
      Version_Id_Spec_Code__c = 'J23',
      Price__c = 33200,
      PVC_Maximo__c = 5,
      PVR_Minimo__c = 5  
    );
    Database.insert( version );

    PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
      Model__c = model.Id,
      Start_Date__c = System.today().addMonths( -1 ),
      End_Date__c = System.today().addMonths( 1 ),
      Type_of_Action__c = 'abc',
      Status__c = 'Active'
    );
    Database.insert( commercialAction );

    PVCall_Offer__c callOffer = new PVCall_Offer__c(
      Commercial_Action__c = commercialAction.Id,
      Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
      Call_Offer_End_Date__c = System.today().addMonths( 1 ),
      Minimum_Entry__c = 100.0,
      Period_in_Months__c = 5,
      Month_Rate__c = 0.99
    );
    Database.insert( callOffer );
    
     User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = [select Id from Profile where Name = 'SFA - Dealer VN Manager'].Id,
          BIR__c ='123ABC123'
        );
     Database.insert( manager );
     
     Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concession?ria teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
     Database.insert( dealerAcc );
     
     List<String> Vers = new String[2];
        vers[0] = 'Authentique 5 Portas';
        vers[1] = 'GT 2.0 Turbo';
        
     List<String> versRemovida = new String[1];
        versRemovida[0] = 'Authentique 5 Portas';
        
     List<SelectOption> vers2 = new List<SelectOption>();
        vers2.add(new SelectOption(version.Id,'Authentique 5 Portas'));
        vers2.add(new SelectOption(version.Id,'GT 2.0 Turbo'));
    
     
    VFC148_ChamadaOfertaVersao vcp = new VFC148_ChamadaOfertaVersao();
    VFC148_ChamadaOfertaVersao vfc = new VFC148_ChamadaOfertaVersao(callOffer);
    vfc.getCallOffer();
    //vfc.todasVersoes();
    vfc.getCallOffer();
    vfc.getVersao();
    vfc.getVersaoRemovida();
    vfc.getTodasVersoes();
    vfc.gettodasVersoes2();
    vfc.getVersao2();
    vfc.setVersao(Vers);
    vfc.setVersaoRemovida(Versremovida);
    vfc.setTodasVersoes(vers2);
    vfc.setVersao2(vers2);
    vfc.salvar();
    //vfc.RelacionarOferta(version.Id,callOffer.Id);  esse m?todo vai como duplicado pois o m?todo salvar j? chama esse metodo utilizando os mesmo ids
    vfc.cancelar();
    vfc.remover();
    vfc.deletarVersoes(''+callOffer.Id,null);
    vfc.init(callOffer.Id);
    //vfc.todasVersoesQuery(null);
    vfc.test();
    //vfc.adicionar();
    //vfc.getTodas_Versoes2();
    
    }

}