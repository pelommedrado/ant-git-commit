/**
 * Classe que contém métodos utilitários para o sistema.
 * Os métodos criados nessa classe possuem caráter GENÉRICO. 
 * @author Felipe Jesus Silva.
 * @ Elvia Serpa
 */
public class VFC69_Utility 
{
    /**
    * Formata um decimal em preço. Exemplo: Para o decimal 1.0 o valor de retorno será R$ 1,00.
    * @param preco Um decimal contendo um determinado valor.
    * @return Uma String representado o valor recebido formatado.
    */
    public static String priceFormats(Decimal price)
    {
        String priceAux = null;
        String cents = null;
        Decimal priceAuxDecimal = null;
        Integer indexInitial = 0;
        Integer indexFinale = 0;    
        String formattedPrice = '';
            
        if(price != null)
        {           
            /*seta o preço para 2 casas decimais*/
            priceAuxDecimal = price.setScale(2);
            
            /*faz o split para obter a parte correspondente ao preço (posição 0 do array)*/
            priceAux = String.valueOf(priceAuxDecimal).split('\\.')[0];
       
            /*faz o split para obter a parte correspondente aos centavos(posição 1 do array)*/
            cents = String.valueOf(priceAuxDecimal).split('\\.')[1];

            if(price < 1000)
            {
                formattedPrice = 'R$ ' + priceAux + ',' + cents;
            }
            else
            {
                /*inicializa os indices para obter os blocos do preço de trás para frente*/
                indexFinale = priceAux.length();
                indexInitial = indexFinale - 3;

                /*enquanto existirem caracteres*/
                while((indexFinale - indexInitial) > 0)
                {
                    /*começa a montar o preço concatenando o valor de cada bloco primeiro (substring) com o que já existia em precoFormatado*/
                    formattedPrice = priceAux.substring(indexInitial, indexFinale) + formattedPrice;

                    /*verifica se chegou o momento de colocar o ponto (o ponto será colocado a cada 3 caracteres de trás para frente
                      e o também sendo o indice inicial maior que zero)*/
                    if((indexFinale - indexInitial == 3) && (indexInitial > 0))
                    {
                        formattedPrice = '.' + formattedPrice;
                    }

                    /*atualiza os indices para pegar o próximo bloco de caracteres*/
                    indexFinale = indexInitial;
                    indexInitial = indexFinale - 3;

                    if(indexInitial < 0)
                    {
                        indexInitial = 0;
                    }
                }

                /*termina de montar o preço*/
                formattedPrice = 'R$ ' + formattedPrice + ',' + cents;
            }   
        } 
               
        return formattedPrice;
    }


    
    public static Double priceFormatToDouble(String priceStr){

        if(!String.isEmpty(priceStr)){

            System.debug(' -> old value' + priceStr);
            String formPrice = priceStr.replace('R$','').replaceAll('\\.','').replace(',','.').trim();
            System.debug(' -> newValue value' + formPrice);
            if(formPrice == '')
                formPrice = '0';
            return Double.valueOf(formPrice);

        }

        return 0;

    }
    
    /**
     * Retorna uma String a partir de um Object. 
     * Se o objeto for igual a NULL, retorna uma String vazia, caso contrário, faz a conversão e retorna uma String sem os espaços iniciais e finais(trim()). 
     * @param obj Um Object qualquer.
     * @return O objeto convertido em String.
     */
    public static String getString(Object obj)
    { 
        return (obj == null ? '' : String.valueOf(obj).trim());
    } 
    
     /**
     * Retorna o próprio Decimal recebido se ele for diferente de NULL.
     * Se for igual a NULL retorna ZERO.
     * @param valor Um Decimal qualquer.
     * @return O próprio Decimal recebido ou ZERO.
     */
    public static Decimal getDecimal(Decimal value)
    {
        return (value == null ? 0.0 : value);
    } 
    
    /**
     * Retorna um decimal a partir de uma String.
     * Esse método, antes da conversão, chama o método getString(String str).
     * Se a conversão for feita com sucesso, simplesmente será retornado a String convertida em decimal.
     * Se o parâmetro valor for NULL ou não representar um número válido, será retornado ZERO.
     * @param valor A String a ser convertida.
     * @return Um decimal >= 0.0;
     */
    public static Decimal getDecimal(String value)
    {
        Decimal valeuReturn = 0.0;

        try
        {
            valeuReturn = Decimal.valueOf(getString(value));
        }
        catch(Exception ex)
        {
        }

        return valeuReturn;
    }        
    
    /**
    * Converte um preço com a formatação de Real em um decimal.
    * @param precoComIndicadorReal Uma String representando um preço. Exemplo: R$ 150,00.
    * @return Um decimal representando o valor recebido como parâmetro (150.0 de acordo com o exemplo).
    */
    public static Decimal convertPrice (String priceIndicator)
    {
        /*chama o método geString apenas para evitar NullPointerException na linha abaixo*/
        String aux = getString(priceIndicator);
        
        /*retira o indicador de moeda, os pontos (pode conter vários pontos - mil, milhão, etc) e por fim, substitui a vírgula dos centavos por um ponto*/
        String price = aux.replace('R$ ', '').replace('.', '').replace(',', '.');
        
        /*converte para decimal e retorna o preço*/
        return getDecimal(price);
    }
    
    
    
    public static String formatDate(Date aDate)
    {
    	String year = String.valueOf(aDate.year());
    	String month = String.valueOf(aDate.month());
    	if (month.length() == 1)
			month = '0' + month;
		String day = String.valueOf(aDate.day());
		if (day.length() == 1)
			day = '0' + day;
    	
    	return year+'/'+month+'/'+day;
    }
    
    public static String formatDate(Datetime aDate)
    {
    	String year = String.valueOf(aDate.year());
    	String month = String.valueOf(aDate.month());
    	if (month.length() == 1)
			month = '0' + month;
		String day = String.valueOf(aDate.day());
		if (day.length() == 1)
			day = '0' + day;
    	
    	return year+'/'+month+'/'+day;
    }
    
    public static Date convertStringToDate(String strDate)
    {
    	Date aDate = null;
    	if (strDate !=null)
    	{
	        if (strDate.contains('/')) {
	            strDate = strDate.replace('/', '-');
	            aDate = Date.valueOf(strDate);
	        }
    	}
    	return aDate;
    }
    
    // Return YYYY/MM/DD HH:MM
    public static String formatDateYYYYMMDDHHMM(Datetime aDatetime)
    {
    	String month = String.valueOf(aDatetime.month()).length()==1 ? '0' + String.valueOf(aDatetime.month()) : String.valueOf(aDatetime.month());
		String day = String.valueOf(aDatetime.day()).length()==1 ? '0' + String.valueOf(aDatetime.day()) : String.valueOf(aDatetime.day());
		String hour = String.valueOf(aDatetime.hour()).length()==1 ? '0' + String.valueOf(aDatetime.hour()) : String.valueOf(aDatetime.hour());
		String minute = String.valueOf(aDatetime.minute()).length()==1 ? '0' + String.valueOf(aDatetime.minute()) : String.valueOf(aDatetime.minute());
    	return aDatetime.year()+'/'+month+'/'+day+' '+hour+':'+minute;
    }

    // Return YYYY/MM/DD HH:MM
    /*
    public static String formatDateYYYYMMDDHHMM(Datetime aDatetime)
    {
    	String year = String.valueOf(aDatetime.year());
    	String month = String.valueOf(aDatetime.month());
    	if (month.length() == 1)
			month = '0' + month;
		String day = String.valueOf(aDatetime.day());
		if (day.length() == 1)
			day = '0' + day;
		String hour = String.valueOf(aDatetime.hour());
		if (hour.length() == 1)
			hour = '0' + hour;
		String minute = String.valueOf(aDatetime.minute());
		if (minute.length() == 1)
			minute = '0' + minute;
    	
    	return year+'/'+month+'/'+day+' '+hour+':'+minute;
    }
    */
    
    // Return HH:MM
    public static String formatDateHHMM(Datetime aDatetime)
    {
		String hour = String.valueOf(aDatetime.hour());
		if (hour.length() == 1)
			hour = '0' + hour;
		String minute = String.valueOf(aDatetime.minute());
		if (minute.length() == 1)
			minute = '0' + minute;

    	return hour+':'+minute;
    }

    public static String removeAccent(String aux)
	{      
		if (aux == null)
	    	return '';
	    
	    //Substitui Minúsculas  
	    aux = aux.replaceAll('[âãáàä]','a');  
	    aux = aux.replaceAll('[éèêë]','e');  
	    aux = aux.replaceAll('[íìîï]','i');  
	    aux = aux.replaceAll('[óòôõö]','o');  
	    aux = aux.replaceAll('[úùûü]','u');
	    aux = aux.replaceAll('ç','c');  
	    //Substitui Maiusculas  
	    aux = aux.replaceAll('[ÂÃÁÀÄ]','A');  
	    aux = aux.replaceAll('[ÉÈÊË]','E');  
	    aux = aux.replaceAll('[ÍÌÎÏ]','I');  
	    aux = aux.replaceAll('[ÓÒÔÕÖ]','O');  
	    aux = aux.replaceAll('[ÚÙÛÜ]','U');  
  		aux = aux.replaceAll('Ç','C');
    	return aux;  
	}
	
	public static String sanitizePhoneNumber(String phoneNumber)
	{
		if (phoneNumber == null)
			return '';
		return phoneNumber.replace('-','').replace('.','').replace(' ','').replace('(','').replace(')','').left(11);
	}
}