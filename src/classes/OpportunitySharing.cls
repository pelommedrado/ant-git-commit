public class OpportunitySharing {
    
    public static final Id recordTypeDVR 		= Utils.getRecordTypeId('Opportunity','DVR');
    public static final Id recordTypeService 	= Utils.getRecordTypeId('Opportunity','Service');
    
    public static void execute(List<Opportunity> lsOpp, Map<Id, Opportunity> oldMap){
        
        Set<Id> oppsToShare = new Set<Id>();
        
        for(Opportunity opp : lsOpp){
            
            if(oldMap == null){
                oppsToShare.add(opp.Id);
                
            }else if(oldMap != null && oldMap.get(opp.Id) == null){
                oppsToShare.add(opp.Id);
                
            }else if(oldMap != null && oldMap.get(opp.Id) != null && opp.OwnerId != oldMap.get(opp.Id).OwnerId){
                oppsToShare.add(opp.Id);
            }
            
        }
        
        if(!oppsToShare.isEmpty()){
            sharing(oppsToShare);
        }
        
    }

    @future
	public static void sharing(Set<Id> oppsToShare) {
        
        System.debug('Entrou no compartilhamento!');
        
        List<Opportunity> lsOpp = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityIds(oppsToShare);
        
        //Compartilhamento individual de oportunidades para cada concessionaria - SFA
        List<Opportunity> dealerNames = getConcessionariasEGrupoConcessionarias(lsOpp, recordTypeDVR);
        Map<Id,Id> mapDealerRole = getConcessionariaPapel(dealerNames,'Partner Manager');
        compartilhaOportunidadePapel(mapDealerRole,lsOpp);
        
        //Compartilhamento de oportunidades para o grupo de concessionarias - SFA
        List<Opportunity> parentDealers = getConcessionariasEGrupoConcessionarias(lsOpp,recordTypeDVR);  
		List<Group> parentGroup = getGrupoPublico(parentDealers);      
        Map<Id,Id> mapOppGroup = criaMapaOportunidadeGrupoPublico(parentDealers,parentGroup);
        compartilhaOportunidadeGrupo(mapOppGroup,parentDealers);
        
        //Compartilhamento de oportunidades para o grupo de concessionarias - Request a Service
        List<Opportunity> grupoDealer = getConcessionariasEGrupoConcessionarias(lsOpp,recordTypeService);
		List<Group> grupoConcessionaria = getGrupoPublico(grupoDealer);
        Map<Id,Id> mapOppGrupo = criaMapaOportunidadeGrupoPublico(grupoDealer,grupoConcessionaria);
        compartilhaOportunidadeGrupo(mapOppGrupo,grupoDealer);
        
	}
    
    public static void compartilhaOportunidadePapel(Map<Id,Id> mapDealerRole,List<Opportunity> lsOpp){
        // compartilhamento de oportunidade
        List<OpportunityShare> lsOppShare = new List<OpportunityShare>();
        for(Opportunity opp: lsOpp){
            
            OpportunityShare oppShare = new OpportunityShare();
            oppShare.UserOrGroupId = mapDealerRole.get(opp.Dealer__c);
            oppShare.OpportunityId = opp.Id;
            oppShare.OpportunityAccessLevel = 'Edit';
            
            lsOppShare.add(oppShare);
        }
        List<Database.UpsertResult> results = Database.Upsert(lsOppShare,false);
        for(Database.UpsertResult r: results){
            if(!r.isSuccess()){
                system.debug('DML ERROR: '+r.getErrors());
            }
        }
    }
    
    public static Map<Id,Id> getConcessionariaPapel(List<Opportunity> dealerNames, String portalPapel) {
        Set<String> setDealerNames = new Set<String>();
        for(Opportunity opp: dealerNames) {
            setDealerNames.add(opp.Dealer__r.Name + ' ' + portalPapel);
        }
        
        // recupera papel da concessionaria
        List<UserRole> dealerRole = [
            SELECT Id, PortalAccountId 
            FROM UserRole WHERE Name IN: setDealerNames
        ];
        
        // recupera Id do grupo relacionado ao papel e cria mapa de Id<Papel> com Id<grupo relacionado ao papel>
        List<Group> grupoRelacionado = [
            SELECT Id, RelatedId 
            FROM Group WHERE RelatedId IN: dealerRole
        ];
        
        Map<Id, Id> mapGroupPortalRole = new Map<Id, Id>();
        for(Group g: grupoRelacionado) {
            mapGroupPortalRole.put(g.RelatedId, g.Id);
        }
        
        // mapa de Ids de concessionarias e seus respectivos Ids de papeis
        Map<Id,Id> mapDealerRole = new Map<Id,Id>();
        for(UserRole r: dealerRole) {
            mapDealerRole.put(r.PortalAccountId,mapGroupPortalRole.get(r.Id));
        }
        
        return mapDealerRole;   
    }
    
    public static List<Opportunity> getConcessionariasEGrupoConcessionarias(List<Opportunity> lsOpp, Id recordType) {
        // recupera os grupos de concessionarias atraves do campo Dealer da oportunidade
        List<Opportunity> parentDealers = [
            SELECT Id, Dealer__r.Name, Dealer__c, Dealer__r.ParentId, RecordTypeId 
            FROM Opportunity WHERE RecordTypeId =: recordType
            AND Dealer__c != null AND Id IN: lsOpp
        ];
        return parentDealers;        
    }
    
    public static List<Group> getGrupoPublico(List<Opportunity> parentDealers) {
        Set<String> groupName = new Set<String>();
        for(Opportunity opp: parentDealers) {
            
            if(opp.recordTypeId != null && opp.RecordTypeId.equals(recordTypeDVR))
            	groupName.add('BR - ' + opp.Dealer__r.ParentId + ' - ExecutiveUsers');
            
            if(opp.recordTypeId != null && opp.RecordTypeId.equals(recordTypeService))
            	groupName.add('BR - ' + opp.Dealer__c + ' - AllDealerUsers');
        }
        
        // recupera o grupo publico dos grupos de concessionarias
        List<Group> parentGroup = [SELECT Id, Name FROM Group WHERE Name IN: groupName];
        
        return parentGroup;
    }
    
    public static Map<Id,Id> criaMapaOportunidadeGrupoPublico(List<Opportunity> parentDealers, List<Group> parentGroup){
        // mapa de Ids de oportunidades e seus respectivos Ids de grupos publicos
        Map<Id,Id> mapOppGroup = new Map<Id,Id>();
        
        for(Opportunity opp: parentDealers){
            for(Group g: parentGroup) {
                if(g.Name.equalsIgnoreCase('BR - ' + opp.Dealer__r.ParentId + ' - ExecutiveUsers'))
                    mapOppGroup.put(opp.Id,g.Id);
                
                if(g.Name.equalsIgnoreCase('BR - ' + opp.Dealer__c + ' - AllDealerUsers'))
                    mapOppGroup.put(opp.Id,g.Id);
            }
        }
        return mapOppGroup;
    }
    
    public static void compartilhaOportunidadeGrupo(Map<Id, Id> mapOppGroup, List<Opportunity> parentDealers) {
        
        //compartilhamento de oportunidade com o grupo Executive Users
        List<OpportunityShare> oppShareGroup = new List<OpportunityShare>();
        
        for(Opportunity opp: parentDealers) {
            
            OpportunityShare oppShare = new OpportunityShare();
            oppShare.UserOrGroupId = mapOppGroup.get(opp.Id);
            oppShare.OpportunityId = opp.Id;
            oppShare.OpportunityAccessLevel = 'Edit';
            
            oppShareGroup.add(oppShare);
        }
        
        List<Database.UpsertResult> results = 
            Database.Upsert(oppShareGroup, false);
        
        for(Database.UpsertResult r: results){
            if(!r.isSuccess()){
                system.debug('DML ERROR: ' + r.getErrors());
            }
        }
    }
}