/**
*	Class   -   VFC11_MolicarDAO
*   Author  -   RameshPrabu
*   Date    -   31/08/2012
*    
*   #01 <RameshPrabu> <31/08/2012>
*        Created this Class to handle record from Molicar related Queries.
*/
public with sharing class VFC11_MolicarDAO {
	
	private static final VFC11_MolicarDAO instance = new VFC11_MolicarDAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC11_MolicarDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC11_MolicarDAO getInstance(){
        return instance;
    }
    
    /** 
    * This Method was used to get Molicar Option1, Option1 Record by MolicarId 
    * @return MolicarRecord - fetch and return the result in MolicarRecord
    */
    public MLC_Molicar__c findMolicarById(String molicarId){
        MLC_Molicar__c MolicarRecord = null;
        try{
			MolicarRecord = [select 
								Id, 
								Name, 
								Brand__c, 
								Model__c, 
								Option1__c, 
								Option2__c
							 from
								MLC_Molicar__c
							 where
								Id =: molicarId];
        } catch( QueryException ex ){}
            return MolicarRecord;
    }
    
    /** 
    * This Method was used to get Molicar Brand Record 
    * @return molicarBrand - fetch and return the result in molicarBrand
    */
    public AggregateResult[] findMolicarBrand(){
        AggregateResult[] molicarBrand = null;
            molicarBrand = [select 
            					Brand__c 
            				from 
            					MLC_Molicar__c
            				group by 
            					Brand__c];
            return molicarBrand;
    }
    
    /** 
    * This Method was used to get Molicar Record 
    * @param brand - This field using get molicar details by brand
    * @param model - This field using get molicar details by model
    * @return lstMolicar - fetch and return the result of molicar brand and model in lstMolicar list
    */
    public List<MLC_Molicar__c> findMolicarByBrandAndModel(String brand, String model){
        List<MLC_Molicar__c> lstMolicar = null;
            lstMolicar = [select 
							Id, 
							Name, 
							Brand__c, 
							Model__c 
						  from 
							MLC_Molicar__c 
						  where 
							Brand__c =: brand 
							and Model__c =:model];
            return lstMolicar;
    }
    public List<MLC_Molicar__c> findMolicarByBrandAndModelAndConfiguration(MLC_Molicar__c mlc){
        List<MLC_Molicar__c> lstMolicar = null;
            return [select 
							Id, 
							Name, 
							Brand__c, 
							Model__c,
							Configuration__c
						  from 
							MLC_Molicar__c 
						  where 
							Brand__c =: mlc.Brand__c
							and  Configuration__c =:mlc.Configuration__c
							and Model__c =:mlc.Model__c];
            
    }
    
    /** 
    * This Method was used to get Molicar Record 
    * @param brand - This field using get molicar details by brand
    * @return lstMolicarModel - fetch and return the result of molicar models in lstMolicarModel list
    */
    public List<MLC_Molicar__c> findMolicarByBrand(String brand){
        List<MLC_Molicar__c> lstMolicarModel = null;
            lstMolicarModel = [select 
            						Id, 
            						Name, 
            						Brand__c, 
            						Model__c 
            				   from 
            						MLC_Molicar__c 
            				   where 
            						Brand__c =: brand
            				   order by 
            				   		Model__c asc];
        
        return lstMolicarModel;
    }
    
    /** 
    * This Method was used to get Molicar Records using Customer Accounts. 
    * @param customerIDs		-	Customer Account Ids to fetch Molicar Records.
    * @return lstMolicarRecords	-	fetch and return the result of molicar models in lstMolicarModel list.
    */
    public List<MLC_Molicar__c> fetchMolicarRecords_UsingCustomerIDs( Set<Id> customersMolicarIDs ){
        List<MLC_Molicar__c> lstMolicarRecords = null;
            lstMolicarRecords = [SELECT
	            					Id,
	            					Name,
	            					Brand__c, 
	            					Model__c,
	            					Configuration__c
            				   	FROM 
            						MLC_Molicar__c 
            				   	WHERE
            						Id IN : customersMolicarIDs
            					];
        
        return lstMolicarRecords;
    }

    public Map<String, List<MLC_Molicar__c>> returnMolicarswithBrand ( Set<String> set_BrandNames ){
    	Map<String, List<MLC_Molicar__c>> map_MolicarWithBrand = new Map<String, List<MLC_Molicar__c>>();
        List<MLC_Molicar__c> lstMolicarRecords = [SELECT
            					Id,
            					Name,
            					Brand__c, 
            					Model__c,
            					Configuration__c
        				   	FROM 
        						MLC_Molicar__c 
        				   	WHERE
        						Brand__c IN : set_BrandNames
        					];

		system.debug('*** lstMolicarRecords.size()='+lstMolicarRecords.size());

        for( MLC_Molicar__c molicar : lstMolicarRecords ){
        	if( map_MolicarWithBrand.containsKey(molicar.Brand__c)){
        		List<MLC_Molicar__c> lst_Molicar = map_MolicarWithBrand.get(molicar.Brand__c);
        		lst_Molicar.add( molicar );
        		map_MolicarWithBrand.put( molicar.Brand__c, lst_Molicar );
        	}
        	else{
        		List<MLC_Molicar__c> lst_Molicar = new List<MLC_Molicar__c>();
        		lst_Molicar.add( molicar );
        		map_MolicarWithBrand.put( molicar.Brand__c, lst_Molicar );
        	}
        }
        return map_MolicarWithBrand;
    }
}