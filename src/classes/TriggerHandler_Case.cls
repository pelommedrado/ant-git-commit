/*****************************************************************************************
    Name    : TriggerHandler_Case
    Desc    : This class is used to handle all the functionalities of the Case Trigger
    Approach: 
    Project : Rforce

******************************************************************************************/

public class TriggerHandler_Case{
    
    public static void beforeInsert(List<Case> CaseList)
    {
     User u = [Select Id, RecordDefaultCountry__c, Division, Department,Profile.Name,ContactId,Email from User Where Id = : UserInfo.getUserId()];
     for(Case cas : CaseList)
     {
     
      if(cas.VIN__c!=null){
            Id vehid=cas.VIN__c;
            try{
                VEH_Veh__c veh=[SELECT KmCheck__c FROM VEH_Veh__c WHERE Id=:vehid];                 
                if(veh.KmCheck__c==null && cas.Kilometer__c==null){
                    veh.KmCheck__c=0;
                    veh.KmCheckDate__c=system.today();                     
                }
                else if(veh.KmCheck__c== null || (veh.KmCheck__c<cas.Kilometer__c)){
                    veh.KmCheck__c=cas.Kilometer__c;
                    veh.KmCheckDate__c=system.today();                    
                }                
                update veh;
            }
            catch(Exception e){
                system.debug(e);
            }    
        }}
     //Added for community to add the delaer details to case 24/09/2015
     if(u.Profile.Name==system.label.USR_Id_CoreServiceProfile && u.ContactId!=null){
        try{
            system.debug('Inside Community loop%%%%%');
            Contact con=[Select Id,AccountId,Account.DealershipStatus__c from Contact where ID =: u.contactId];
                for(Case cas: CaseList){
                    cas.DealerContactLN__c=con.Id;
                    cas.Dealer__c=con.AccountId;
                    cas.EmailContactDealer__c=u.Email;
                    cas.Dealer_User__c=u.Id;
                }  
                system.debug('Inside Community loop%%%%%'+CaseList[0].EmailContactDealer__c);            
        }catch(Exception e){
            system.debug(e);
        }
      }  
           
    }

}