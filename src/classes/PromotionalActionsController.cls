public class PromotionalActionsController {
    
    public List<Opportunity> oppList{get;set;}
    
    public PromotionalActionsController(){
        oppLoad();
    }
    
    public List<Opportunity> lsOpp{
        get{
            return oppList;
        }
    }
    
    public void oppLoad(){
        
        // recupera as oportunidades de acordo com o filtro escolhido na pagina
        List<Opportunity> lsOpp = Database.query( 
            'SELECT Id, Name, Account_CPF__c, LastModifiedDate, StageName, Dealer__c, Seller__c ' +
            'FROM Opportunity WHERE ' +
            '(StageName = \'In Attendance\' OR StageName = \'Billed\') ' +
            'AND CreatedDate = LAST_N_DAYS:30 ORDER BY CreatedDate ASC limit 999'
        );
        
        oppList = new List<Opportunity>();
        oppList.addAll(lsOpp);
    }
    
    public PageReference consultarVouchers(){
        String oppId = ApexPages.currentPage().getParameters().get('oppId');
        System.debug('oppId:' + oppId);
        
        Opportunity opp = [SELECT Account_CPF__c, StageName FROM Opportunity WHERE Id =: oppId limit 1];
        
        PageReference pg = new PageReference('/apex/InCourseLeads?Id=' + opp.Id + 
                                             '&CPF='+ opp.Account_CPF__c + '&stage=' + opp.StageName);
        return pg;
    }
    
    public PageReference loadPageInCourseLeads(){
        String tipoVoucher = ApexPages.currentPage().getParameters().get('tpVoucher');
        PageReference pg = new PageReference('/apex/InCourseLeads?tipoVoucher='+tipoVoucher);
        return pg;
    }
    
}