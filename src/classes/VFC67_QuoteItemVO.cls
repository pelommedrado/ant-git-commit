/**
* Classe que representa os dados dos orçamento.
* @author Elvia Serpa.
*/
public with sharing class VFC67_QuoteItemVO 
{
    /* atributos referentes a seção orçamento */
    public Boolean isCheckOpp {get;set;}
    public String nameItem {get;set;}
    public String quoteNumber {get;set;}
    public String totalPrice {get;set;}
    public String status {get;set;}
    public String vehicle {get;set;}
    public String vehicleRule {get;set;}
    public String id {get;set;}
    public Quote sobjQuote {get;set;}
    
    public VFC67_QuoteItemVO()
    {
        nameItem = '';
    }
}