public class DashboardOpportunityService {
    private static final String ACTION_QUOTE = 'quote';
    private static final String ACTION_TASK = 'task';

    public List<Id> allSellers  { get;set; }

    public String action { get;set; }

    public String paramVehicle   { get;set; }
    public String paramOppSource { get;set; }
    public String paramStatus    { get;set; }
    public String textSearch { get;set; }
    public String dataIniString { get;set; }
    public String dataFimString { get;set; }
    public String orderSelect { get;set; }
    public Boolean isData { get;set; }

    public Date data = Datetime.now().date();

    public static final Set<String> stageAttendeceSet = new Set<String> {
        'Identified', 'In Attendance', 'Test Drive', 'Quote'
    };
    public static final Set<String> stageLastDaysSet = new Set<String> {
        'Lost'
    };
    public static final List<String> stageMonthList = new List<String> {
        'Order', 'Billed'
    };
    public DashboardOpportunityService() {
      this.isData = true;
    }

    public Integer countMonth(String stage) {
      final Datetime ini = DataUtils.dataToDatetimeIniMes(data);
      final Datetime fim = DataUtils.dataToDatetimeFimMes(data);
      AggregateResult[] totalOppResults = [
          SELECT COUNT(Id) cou FROM Opportunity
          WHERE OwnerId IN:(allSellers) AND StageName =: stage
          AND CreatedDate >=: ini AND CreatedDate <=: fim
      ];
      return Integer.valueOf(totalOppResults[0].get('cou'));
    }

    public Integer countLastDays(List<String> stages) {
      AggregateResult[] totalOppResults = [
          SELECT COUNT(Id) cou FROM Opportunity
          WHERE OwnerId IN:(allSellers) AND StageName IN:(stages)
          AND LastModifiedDate >= LAST_N_DAYS:30
      ];
      return Integer.valueOf(totalOppResults[0].get('cou'));
    }

    public Integer count(List<String> stages) {
      AggregateResult[] totalOppResults = [
          SELECT COUNT(Id) cou FROM Opportunity
          WHERE OwnerId IN:(allSellers) AND StageName IN:(stages)
      ];
      return Integer.valueOf(totalOppResults[0].get('cou'));
    }

    public List<Opportunity> buscarOpp() {
        System.debug('buscarOpp()');

        String queryWhere = queryBuildOppSource();

        final List<String> paramVehicleList = DashboardUtil.paramFilter(paramVehicle);
        //final List<String> paramVehicleLikeList = DashboardUtil.paramLike(paramVehicleList);
        if(!paramVehicleList.isEmpty()) {
          queryWhere += queryBuildList('VehicleInterest__c', paramVehicleList);
          //queryWhere += 'AND VehicleInterest__c LIKE: paramVehicleList ';
        }

        final Datetime dataIniDate = createDatetimeStart();
        final Datetime dataFimDate = createDatetimeEnd();
        final List<String> stageSelectList = createStageList();

        final List<String> paramStatusList = DashboardUtil.paramFilter(paramStatus);
        String queryStage = queryBuildStage(paramStatusList);
        if(String.isEmpty(queryStage) && isData) {
          if(!isFilter() || isFilterRage()) {
            queryStage = 'AND StageName IN: stageSelectList '
                + ' AND CreatedDate >=: dataIniDate AND CreatedDate <=: dataFimDate ';
          }else {
            queryStage = 'AND StageName IN: stageSelectList ';
          }
        }

        System.debug('dataIniDate: '+ dataIniDate);
        System.debug('dataFimDate: '+ dataFimDate);
        queryWhere += queryStage;
        queryWhere += queryBuildTextSearch();

        String query = 'SELECT Id, Name, Owner.Name, VehicleInterest__c, toLabel(StageName), '
            + ' toLabel(OpportunitySource__c), toLabel(OpportunitySubSource__c), CreatedDate, LastModifiedDate '
            + ' FROM Opportunity WHERE OwnerId IN: allSellers '
            + (String.isEmpty(queryWhere) ? '' : queryWhere)
            + (orderSelect == '0' ? ' ORDER BY CreatedDate ASC' : ' ORDER BY CreatedDate DESC')
            + ' LIMIT 999';

        System.debug('buscarOpp query: ' + query);

        final List<Opportunity> oppStageLastDaysList = buscarOppStageLastDays();
        final List<Opportunity> oppStageAttendeceList = buscarOppStageAttendece();
        final List<Opportunity> allOppResultList = Database.query(query);
        allOppResultList.addAll(oppStageLastDaysList);
        allOppResultList.addAll(oppStageAttendeceList);
        List<OpportunityComparable> comparableList = new List<OpportunityComparable>();
        for(Opportunity op : allOppResultList) {
          comparableList.add(new OpportunityComparable(op, orderSelect));
        }
        comparableList.sort();
        final List<Opportunity> oppResult = new List<Opportunity>();
        for(OpportunityComparable oppCom : comparableList) {
          oppResult.add(oppCom.oppy);
        }
        return oppResult;
    }

    private class OpportunityComparable implements Comparable {
      public Opportunity oppy;
      public String order;
      public OpportunityComparable(Opportunity op, String order) {
          this.oppy = op;
          this.order = order;
      }

      // Compare opportunities based on the opportunity amount.
      public Integer compareTo(Object compareTo) {
          final OpportunityComparable compareToOppy = (OpportunityComparable)compareTo;
          Integer returnValue = 0;
          if (oppy.CreatedDate > compareToOppy.oppy.CreatedDate) {
              returnValue = order.equals('0') ? 1 : -1;
          } else if (oppy.CreatedDate < compareToOppy.oppy.CreatedDate) {
              returnValue = order.equals('0') ? -1 : 1;
          }

          return returnValue;
      }
    }

    private String queryBuildOppSource() {
        System.debug('queryBuildOpp() ' + paramOppSource);

        final List<String> oppSourceList =
            DashboardUtil.paramFilter(paramOppSource);

        return queryBuildListLike('OppSourceText__c', oppSourceList);
        /*String queryWhere = '';

        if(!oppSourceList.isEmpty() && oppSourceList.size() != 1) {
            queryWhere = 'AND ( ';
        }

        for(String param : oppSourceList) {
            final String stgParam = 'OppSourceText__c LIKE \'%' + param + '%\' ';
            if(oppSourceList.size() == 1) {
                queryWhere += 'AND ' + stgParam;
            } else {
                queryWhere += stgParam + ' OR ';
            }
        }

        // remove ultimo "OR" e acresecenta o ")"
        if(!oppSourceList.isEmpty() && oppSourceList.size() != 1) {
            queryWhere = queryWhere.substring(0, queryWhere.length() - 3) + ') ';
        }
        System.debug('queryWhere: ' + queryWhere);
        return queryWhere;*/
    }

    private String queryBuildListLike(String field, List<String> params) {
      String queryWhere = '';
      if(!params.isEmpty() && params.size() != 1) {
          queryWhere = 'AND ( ';
      }

      for(String param : params) {
          final String stgParam = field + ' LIKE \'%' + param + '%\' ';
          if(params.size() == 1) {
              queryWhere += 'AND ' + stgParam;
          } else {
              queryWhere += stgParam + ' OR ';
          }
      }

      // remove ultimo "OR" e acresecenta o ")"
      if(!params.isEmpty() && params.size() != 1) {
          queryWhere = queryWhere.substring(0, queryWhere.length() - 3) + ') ';
      }
      System.debug('queryWhere: ' + queryWhere);
      return queryWhere;
    }

    private String queryBuildList(String field, List<String> params) {
      String queryWhere = '';
      if(!params.isEmpty() && params.size() != 1) {
          queryWhere = 'AND ( ';
      }

      for(String param : params) {
          final String stgParam = field + ' = \'' + param + '\' ';
          if(params.size() == 1) {
              queryWhere += 'AND ' + stgParam;
          } else {
              queryWhere += stgParam + ' OR ';
          }
      }

      // remove ultimo "OR" e acresecenta o ")"
      if(!params.isEmpty() && params.size() != 1) {
          queryWhere = queryWhere.substring(0, queryWhere.length() - 3) + ') ';
      }
      System.debug('queryWhere: ' + queryWhere);
      return queryWhere;
    }

    private Datetime createDatetimeStart() {
      if(isFilterRage()) {
        return DataUtils.dataToDatetimeIni(DataUtils.parseDDmmYYYY(dataIniString));
      }
      return DataUtils.dataToDatetimeIniMes(data);
    }

    private Datetime createDatetimeEnd() {
      if(isFilterRage()) {
        return DataUtils.dataToDatetimeFim(DataUtils.parseDDmmYYYY(dataFimString));
      }
      return DataUtils.dataToDatetimeFimMes(data);
    }

    private List<String> createStageList() {
      final List<String> stageSelectList = new List<String>();
      if(!isFilter()) {
        stageSelectList.addAll(stageMonthList);
      } else {
        stageSelectList.addAll(stageMonthList);
        stageSelectList.addAll(stageLastDaysSet);
        stageSelectList.addAll(stageAttendeceSet);
      }
      return stageSelectList;
    }

    private Boolean isFilterRage() {
      return String.isNotEmpty(dataIniString) && String.isNotEmpty(dataFimString);
    }

    private String queryBuildStage(List<String> paramStatusList) {
      System.debug('queryBuildStage');

      if(!this.action.equals(ACTION_QUOTE)) {
        return '';
      }
      if(paramStatusList.isEmpty()) {
        return '';
      }

      final String st = paramStatusList.get(0);
      if(stageLastDaysSet.contains(st) && !isFilterRage()) {
        return  'AND StageName IN: paramStatusList '
        + '  AND LastModifiedDate >= LAST_N_DAYS:30 ';
      } else if(stageAttendeceSet.contains(st)) {
        return  'AND StageName IN: paramStatusList ';
      }
      return 'AND StageName IN: paramStatusList '
          + ' AND CreatedDate >=: dataIniDate AND CreatedDate <=: dataFimDate ';
    }

    private String queryBuildTextSearch() {
        System.debug('queryBuildTextSearch() ' + textSearch);
        if(String.isEmpty(textSearch)) {
            return '';
        }
        String queryWhere = 'AND (Account.Name LIKE \'%' + textSearch + '%\' OR '
            + ' Account.CustomerIdentificationNbr__c LIKE \'%' + textSearch + '%\' OR '
            + ' Account.PersEmailAddress__c LIKE \'%' + textSearch + '%\' OR '
            + ' Account.PersLandline__c LIKE \'%' + textSearch + '%\' OR '
            + ' Account.PersMobPhone__c LIKE \'%' + textSearch + '%\')';
        System.debug('queryWhere: ' + queryWhere);
        return queryWhere;
    }

    private List<Opportunity> buscarOppStageLastDays() {
        final Boolean filter = isFilter();
        System.debug('buscarOppStageLastDays() filter:' + filter);
        if(filter) {
            return new List<Opportunity>();
        }

        final List<Opportunity> oppStageLastDaysList = [
            SELECT Id, Name, Owner.Name, VehicleInterest__c, toLabel(StageName),
            toLabel(OpportunitySource__c), toLabel(OpportunitySubSource__c),
            CreatedDate, LastModifiedDate
            FROM Opportunity WHERE OwnerId IN:(allSellers) AND StageName IN:stageLastDaysSet
            AND LastModifiedDate >= LAST_N_DAYS:30
        ];
        return oppStageLastDaysList;
    }

    private List<Opportunity> buscarOppStageAttendece() {
      final Boolean filter = isFilter();
      System.debug('buscarOppStageAttendece() filter:' + filter);
      if(filter) {
          return new List<Opportunity>();
      }

      final List<Opportunity> oppStageAttendeceList = [
          SELECT Id, Name, Owner.Name, VehicleInterest__c, toLabel(StageName),
          toLabel(OpportunitySource__c), toLabel(OpportunitySubSource__c),
          CreatedDate, LastModifiedDate
          FROM Opportunity WHERE OwnerId IN:(allSellers) AND StageName IN:stageAttendeceSet
          LIMIT 999
      ];
      return oppStageAttendeceList;
    }

    private Boolean isFilter() {
        final List<String> paramsList = new List<String>();
        paramsList.add(paramVehicle);
        paramsList.add(paramOppSource);
        paramsList.add(paramStatus);
        paramsList.add(textSearch);
        paramsList.add(dataIniString);
        paramsList.add(dataFimString);
        for(String param : paramsList) {
            if(!String.isEmpty(param)) {
                return true;
            }
        }
        return false;
    }
}