public class TecnicoListaController {

    public List<Contact> tecnicoList { get;set; }
    public Id accId { get;set; }
    public Contact ctt { get;set; }
    
    public TecnicoListaController() {
        this.ctt = new Contact();
        this.tecnicoList = new List<Contact>();
        
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        
        this.accId  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        filtrar();
    }
    
    public PageReference filtrar() {
        System.debug('Filtrando resultados...');
        //System.debug('Tipo servico:' + tipoServico);
        
        String query = ' SELECT Id, Name, ' + 
            'Account.Name,' + 
            'Phone, ' +
            'Email, ' +
            'Available_to_Service__c ' +
            
            'FROM Contact ' +
            'WHERE AccountId =: accID ' +
            (ctt.Available_to_Service__c != null ? ' AND Available_to_Service__c = \'' + ctt.Available_to_Service__c + '\' ' : '') + 
            'ORDER BY Name';
        
        this.tecnicoList = Database.query(query);
        
        if(this.tecnicoList == null) {
            this.tecnicoList = new List<Contact>();
        }
        return null;
    }
    
    public PageReference novoContato() {
        PageReference pRef = new PageReference(
            '/cac/003/e?retURL=/cac/TecnicoLista&RecordType=012D0000000KApJ&ent=Contact');
        return pRef;
    }
    
    public PageReference editarContato() {
        String id = 
            ApexPages.currentPage().getParameters().get('cttId');
        
        PageReference pRef = new PageReference(
            '/cac/' + id + '/e?retURL=/cac/TecnicoLista&RecordType=012D0000000KApJ&ent=Contact');
        return pRef;
    }
}