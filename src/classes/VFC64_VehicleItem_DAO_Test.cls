/**
	Class   -   VFC64_VehicleItem_DAO_Test
    Author  -   Subbarao
    Date    -   13/03/2013
    
    #01 <Subbarao> <13/03/2013>
        Created this class using test for VFC64_VehicleItem_DAO.
**/
@isTest
private class VFC64_VehicleItem_DAO_Test {
	
	static testmethod void VFC64_VehicleItem_DAO_Test(){
		
		List<Vehicle_Item__c> VehicleItem = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToVehicleItem();	
		Test.startTest();
		List<Vehicle_Item__c> lstAllVehicleItems = VFC64_VehicleItem_DAO.getInstance().fetchAll_VehicleItem_Records();
		Product2 product = VFC03_InsertSObjectsRecordsForTestClass.getInstance().product_ModelRecord_Insertion();
		system.assert(lstAllVehicleItems.size()>0);
		VFC64_VehicleItem_DAO.getInstance().return_Map_ItemKey_and_VehicleItem();	
		VFC64_VehicleItem_DAO.getInstance().return_VehicleItem_With_ItemName();
		VFC64_VehicleItem_DAO.getInstance().return_Map_ModelCode_ItemCode_VehicleItems('X-01');
		VFC64_VehicleItem_DAO.getInstance().fetchVehicleItemByModel('Fluence');
		
		Test.stopTest();		
		
	}

}