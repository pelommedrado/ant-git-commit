@isTest
public class MyOwnCreation {
    private static final MyOwnCreation instance = new MyOwnCreation();
    
    public static MyOwnCreation getInstance(){
        return instance;
    }
    
    public User CriaUsuarioComunidade(){
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User user;
        Account account;
                
        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {
            
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            account = criaAccountDealer();
            account.ReturnActiveToSFA__c = true;
            insert(account);
            
            Contact contact = criaContato();
            contact.AccountId = account.Id;
            insert(contact);
            
            user = criaUser();
            user.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            user.ContactId = contact.Id;
            insert(user);  
        }
        return user;
    }
    
    public ALR_Tracking__c criaTracking(){      
        ALR_Tracking__c tracking = new ALR_Tracking__c();
        return tracking;
    }
    
    public ALR_Activate_Insurance__c criaActivateInsurance(){  
        ALR_Activate_Insurance__c ai = new ALR_Activate_Insurance__c(
            ALR_Myself__c = true,
            ALR_Inspector_Name__c = 'Nome',
            ALR_Zip_Code_Occurrence__c = '99999999'
        );
        return ai;  
    }
    
    public Campaign criaCampanha(){
        Campaign c = new Campaign(
            name='teste',
            IsActive = true,
            DBMCampaignCode__c = 'cod',
            RecordTypeId = Utils.getRecordTypeId('Campaign', 'SingleCampaign'),
            ManualInclusionAllowed__c = true,
            PAActiveShareAllowed__c = true
        );
        Return c;
    }
    
    public CampaignMember criaMembroCampanha(){
        CampaignMember cm = new CampaignMember(
        );
        Return cm;
    }
    
    public User criaUser(){
        User u = new User(
            FirstName = 'Mario',
            LastName = 'Andrade',
            Username = 'uservendedor@test.com',
            Email = 'test@test.com',
            Alias = 'tes',
            RecordDefaultCountry__c = 'Brazil',
            BIR__c = '123456',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles'
        );
        return u;
    }
    
    public Contact criaContato(){
        Contact c = new Contact(
            CPF__c =  '42616895617',
            Email = 'test@org1.com',
            FirstName = 'test',
            LastName = 'test',
            MobilePhone = '11223344556' 
        );
        return c;
    }
    
    public Lead CriaLead(){
        Lead lead = new Lead(
            FirstName = 'Test',
            LastName = 'Lead',
            CPF_CNPJ__c = '73412052612',
            LeadSource = 'Internet',
            SubSource__c = 'Marketing',
            Detail__c = 'I am Interested',
            Sub_Detail__c = 'Not Found'
        );
        Return lead;
    }

    public Account criaAccountDealer(){
        Account dealer = new Account(
            Name='Dealer',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
            IDBIR__c = '123456'
        );
        return dealer;
    }
        
    public Account criaPersAccount(){ 
        Account a = new Account(
            FirstName = 'test',
            LastName = 'Sobrenome',
            PersEmailAddress__c = 'test@teste.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            VehicleInterest_BR__c = 'test',
            PersMobPhone__c = '45214524',
            PersLandline__c = '45214578'
        );
        return a; 
    }
    
    public Opportunity criaOpportunity(){
        Opportunity opp = new Opportunity(
            Name = 'teste',
            StageName = 'test',
            CloseDate = system.today()
        );
        return opp;
    }
    
    public Product2 criaProduct2(){  
        Product2 prd = new Product2(
            Name = 'Test',
            ModelSpecCode__c = 'test'
        );
        return prd;     
    }
    
    public Quote criaQuote(){
        Quote q = new Quote(
            Name = 'teste',
            Status = 'Open'
        );
        return q;
    }
    
    public QuoteLineItem criaQuoteLineItem(){
        QuoteLineItem q = new QuoteLineItem();
        return q;
    }
    
    public Pricebook2 criaStdPricebook2(){
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 p = new Pricebook2(
            Id = pricebookId,
            IsActive = true
        );
        return p;
    }
    
    public Pricebook2 criaPricebook2(){
        Pricebook2 p = new Pricebook2(
            IsActive = true,
            Name = 'teste'
        );
        return p;
    }
    
    public PricebookEntry criaPricebookEntry(){
        PricebookEntry pe = new PricebookEntry(
            IsActive = true,
            UnitPrice = 1000
        );
        return pe;
    }
    
    public VEH_Veh__c criaVeiculo(){
        VEH_Veh__c v = new VEH_Veh__c(
            Name='12345678912458965',
            Model__c = 'Test',
            Status__c = 'Active',
            Is_Available__c = true
        );
        return v;
    }
    
    public VRE_VehRel__c criaVeiculoRelacionado(){
        VRE_VehRel__c vr = new VRE_VehRel__c(
            Status__c = 'Active'
        );
        return vr;
    }
    
    public Case criaCaso(){
        case c = new Case();
        return c;    
    }

}