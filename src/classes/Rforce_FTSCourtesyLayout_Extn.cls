/**
  * Class Name : Rforce_FTSCourtesyLayout_Extn
  * @author: Vetrivel Sundararajan
  * @date: 09/04/2015
  * @description: This apex class will be used in R-Courtesy for creating GoodWill
  * and validates the Start Date and End Date to avoid the Duplicate before saving the record into Goodwill.  
 */

public with sharing class Rforce_FTSCourtesyLayout_Extn {
    
    public Goodwill__c gudwill{get;set;}
    public FTS__c FTS {get; set;}
    public String strRecordTypeName{get;set;}
    ID FTSId;  
    public Boolean goodwillUpdateFlag=false;
    List<FTS__c> selFTS = new List<FTS__c>();
    public RCourtesyGrid__c rCourtesyGrid {get; set;} 
    
    Transient Date replacementStartDate;
    Transient Date replacementEndDate;
    Transient String carSegment;
    Transient String strPVINumber;
    public Decimal dclNegociatedFareAmt{get; set;}
    public Decimal dclMaxiDailyRate{get; set;}
    public Decimal dclStndDailyRate{get;set;}
    public String vehid;
    
    Id caseIds = System.currentPageReference().getParameters().get('CF00ND0000004qc7b_lkid');
    Id caseId = ApexPages.currentPage().getParameters().get('id');
    Id ftsRecId = ApexPages.currentPage().getParameters().get('ftsIds');
    Id GWId = System.currentPageReference().getParameters().get('RecordType'); 
    
    @TestVisible String strCaseCountry = ApexPages.currentPage().getParameters().get('CountryCase__c');
    @TestVisible String strCurrenyCode = ApexPages.currentPage().getParameters().get('CurrencyIsoCode');
    @TestVisible String strResolutionCode=ApexPages.currentPage().getParameters().get('ResolutionCode__c');
    
         
   /**
     * Constructor Name : Rforce_FTSCourtesyLayout_Extn
     * @description: This Constructor will be used to fetch the particular FTS record values
     * and assign to the local varaibles which will be used for Duplicate check and inserting the values into Goodwill. 
    */ 
    public Rforce_FTSCourtesyLayout_Extn(ApexPages.StandardSetController controller) 
    {
        system.debug('## Inside Rforce_FTSCourtesyLayout_Extn');
        selFTS = [select Id,Replacement_Car_Negociated_Fare__c,Replacement_Start_Date__c,Dealer_Id__c, Replacement_End_Date__c,Car_Segment__c, PVI_Number__c,RecordTypeId from FTS__c where Id = :ftsRecId];
        
        for (integer i = 0; i < selFTS.size(); i++) 
        {
           try 
           {    
               FTS = [select Id,Replacement_Car_Negociated_Fare__c,Replacement_Start_Date__c, Replacement_End_Date__c,Dealer_Id__c,Car_Segment__c,PVI_Number__c, RecordTypeId from FTS__c where Id = :selFTS[i].Id];
               vehid=[Select VIN__c from Case where Id =: caseId].VIN__c;
               
               carSegment           = FTS.Car_Segment__c;
               replacementStartDate = FTS.Replacement_Start_Date__c;
               replacementEndDate   = FTS.Replacement_End_Date__c;
               strPVINumber         = FTS.PVI_Number__c;
               
               dclNegociatedFareAmt = FTS.Replacement_Car_Negociated_Fare__c;
               system.debug('## dclNegociatedFareAmt is..::'+ dclNegociatedFareAmt);
               rCourtesyGrid =  [select ID,CarSegmentName__c,Country__c,MaxiDailyRate__c,StandardDailyRate__c from RCourtesyGrid__c where CarSegmentName__c=:carSegment];
                
               dclMaxiDailyRate = rCourtesyGrid.MaxiDailyRate__c;
               dclStndDailyRate = rCourtesyGrid.StandardDailyRate__c;
                
            }catch (Exception e) {
                     System.debug('## Exception Occured in Rforce_FTSCourtesyLayout_Extn Constructor::' + e);
                  }
        }     
    }
    
   /**
     * Method Name : initialize
     * @description: This method will be used to assign the values into Goodwill Object.
    */
    
    public void initialize()
    {
        system.debug('## Inside initialize Method ##');
        gudwill = new Goodwill__c();
        if (caseId != null) 
        {
            gudwill.Case__c = caseId;
            gudwill.FTS__c = ftsRecId;
            gudwill.ResolutionCode__c = strResolutionCode;
            gudwill.EndDate__c=replacementEndDate;
            gudwill.StartDate__c=replacementStartDate;
            gudwill.Decided_start_date__c=replacementStartDate;
            gudwill.Decided_end_date__c=replacementEndDate;
            
            gudwill.CarSegment__c=carSegment;
            gudwill.PVI_Number__c=strPVINumber;
            gudwill.Vehicle__c=vehid;
        }
        else 
        {
            gudwill.Case__c = caseIds;
        } 
        Id DealerId= FTS.Dealer_Id__c;
        gudwill.DealerName__c=DealerId;
        
        system.debug('## dclNegociatedFareAmt is..::'+dclNegociatedFareAmt);
       // if (dclNegociatedFareAmt!=null) {
       if(dclNegociatedFareAmt!=null && dclNegociatedFareAmt > 0) { 
           gudwill.Replacement_Car_Negociated_Fare__c=dclNegociatedFareAmt;
        }else {
          gudwill.Replacement_Car_Negociated_Fare__c=dclStndDailyRate;
        }
            
        if (strCaseCountry != null) 
        {
            gudwill.Country__c = strCaseCountry;
        }
        if (strCaseCountry == System.label.FTS_SpainCountry) {
            gudwill.recordtypeid = System.label.FTS_CountryRecordTypeID;
            gudwill.Country__c=System.label.FTS_SpainCountry;
        }
        if (strCurrenyCode != null) 
        {
            gudwill.CurrencyCode__c = strCurrenyCode;
        }
        
        try
        {
          strRecordTypeName=[Select Name from RecordType where Id =: gudwill.recordtypeid].Name;
        } catch(Exception e){
              system.debug('## Exception in Retrieving Goowill RecordType ##'+ e);
          }         
       
    } // end of initialize method
   
    /**
     * @Method Name : saveGoodwill
     * @description: This method will be used to save the  values into Goodwill Object.
    */
    
    public PageReference saveGoodwill() 
    {
        system.debug('## Inside saveGoodwill ##');
          
        set<string>vinids=new set<string>();
        list<FTS__c>FTSdate=[SELECT id,Replacement_End_Date__c,Replacement_Start_Date__c,Case_No__c,VIN__c FROM FTS__c where id=:ftsRecId]; 
        for(FTS__c vinNo:FTSdate)
        {
            vinids.add(vinNo.VIN__c);
        }
     
        list<Goodwill__c> listdate = [select id,VIN__c from Goodwill__c where VIN__c=:vinids];
        list<Goodwill__c> listStatus = [select id,VIN__c,StartDate__c,EndDate__c,Decided_start_date__c,Decided_end_date__c from Goodwill__c where VIN__c=:vinids and GoodwillStatus__c !='Refused'];
     
        PageReference pageRegGW = null;
        try 
        {
            if (gudwill.ExpenseCode__c == null && !test.isRunningTest()) 
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_ExpenseCode));
                return null;
            }
            if (gudwill.BudgetCode__c == null && !test.isRunningTest()) 
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_BudgetCodeMsg));
                return null;
            }
            if (gudwill.Decided_start_date__c ==null && !test.isRunningTest()) 
            {    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_StartDateMsg));
                return null; 
            }
            
            if (gudwill.CarSegment__c ==null && !test.isRunningTest()) 
            {    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_CarSegmentPkList));
                return null; 
            }
            
            if (gudwill.Replacement_Car_Negociated_Fare__c==null){
                gudwill.Replacement_Car_Negociated_Fare__c=dclStndDailyRate;
            } 
            if (gudwill.Replacement_Car_Negociated_Fare__c !=null && gudwill.Replacement_Car_Negociated_Fare__c > 0) {
                if (gudwill.DeviationReasDesc__c ==null && !test.isRunningTest()) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_DeviationReasonRequired));
                    return null; 
                }
            }
            // Added for changing the carsegment picklist values
            rCourtesyGrid =  [select ID,CarSegmentName__c,Country__c,MaxiDailyRate__c,StandardDailyRate__c from RCourtesyGrid__c where CarSegmentName__c=:gudwill.CarSegment__c];
            dclStndDailyRate= rCourtesyGrid.StandardDailyRate__c;
            dclMaxiDailyRate= rCourtesyGrid.MaxiDailyRate__c;
            
            system.debug('## dclStndDailyRate is..::'+ dclStndDailyRate);
            system.debug('## dclMaxiDailyRate is..::'+ dclMaxiDailyRate);
           
            if (gudwill.Replacement_Car_Negociated_Fare__c !=null && gudwill.Replacement_Car_Negociated_Fare__c == 0)
            {
                gudwill.Replacement_Car_Negociated_Fare__c=dclStndDailyRate;
            }
            else if (gudwill.Replacement_Car_Negociated_Fare__c !=null && gudwill.Replacement_Car_Negociated_Fare__c > 0)
            {
                 if(gudwill.Replacement_Car_Negociated_Fare__c < dclStndDailyRate ||  gudwill.Replacement_Car_Negociated_Fare__c  > dclMaxiDailyRate) 
                 {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_ReplacementCarFareMsg +' '+dclStndDailyRate +' and ' +dclMaxiDailyRate));
                    return null; 
                 }
            }
            
            if (( strResolutionCode!=null || test.isRunningTest())&&( test.isRunningTest() ||(strResolutionCode ==System.label.GOO_CarRentalWarranty || strResolutionCode ==System.label.GOO_CarRentalSRC))) 
            {
               system.debug('## Inside CarRental Warranty ##');
               if (gudwill.Decided_end_date__c == null) 
                {
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GOO_EndDateMsg));
                   return null;
                } 
                else if(gudwill.Decided_end_date__c !=NULL)
                {
                    if((Date.valueOf(gudwill.Decided_start_date__c) == Date.ValueOf(gudwill.Decided_end_date__c)) || (Date.valueOf(gudwill.Decided_start_date__c) < Date.ValueOf(gudwill.Decided_end_date__c)))
                    {
                        if(strResolutionCode ==System.label.GOO_CarRentalWarranty)
                        {
                            if ((gudwill.GoodwillStatus__c == System.label.GOO_Refused    ) && (test.isRunningTest() || listStatus.size()==0))
                            {
                                upsert gudwill;
                            } 
                            else if ((gudwill.GoodwillStatus__c !=System.label.GOO_Refused || test.isRunningTest()) && (test.isRunningTest() || listStatus.size()==0))
                            {
                                upsert gudwill;
                            }
                            else
                            {
                               for(Goodwill__c olddate:listStatus)
                               {  
                                 if((gudwill.Decided_start_date__c > olddate.Decided_start_date__c && gudwill.Decided_start_date__c > olddate.Decided_end_date__c) || (gudwill.Decided_start_date__c < olddate.Decided_start_date__c && gudwill.Decided_end_date__c < olddate.Decided_start_date__c ))
                                 {
                                    goodwillUpdateFlag=true;
                                 }
                                 else
                                 {
                                    goodwillUpdateFlag=false;
                                    ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_OverlappedDates);
                                    ApexPages.addmessage(msg);
                                    return null; 
                                    break;        
                                 }
                                  
                               }// For Loop End--Warranty
                            }// end of else loop 
                            
                            if (goodwillUpdateFlag || test.isRunningTest()) 
                            {
                                  system.debug('## goodwillUpdateFlag value is..:: '+ goodwillUpdateFlag);
                                  upsert gudwill;
                            }
                        }
                    
                        if(strResolutionCode == System.label.GOO_CarRentalSRC)
                        {
                          system.debug('## Inside ResolutionCode equal to SRC ##');
                          if(gudwill.GoodwillStatus__c == System.label.GOO_Refused && (listStatus.size()==0))
                          {
                             upsert gudwill;
                          }
                          else if ((gudwill.GoodwillStatus__c != System.label.GOO_Refused) && (listStatus.size()==0))
                          {
                              upsert gudwill;
                          }
                                
                           for(Goodwill__c olddate:listStatus)
                           {
                                if((gudwill.Decided_start_date__c > olddate.Decided_start_date__c && gudwill.Decided_start_date__c > olddate.Decided_end_date__c)|| (gudwill.Decided_start_date__c < olddate.Decided_start_date__c && gudwill.Decided_end_date__c < olddate.Decided_start_date__c ))
                                {
                                   goodwillUpdateFlag=true;
                                }
                                else
                                {
                                   goodwillUpdateFlag=false;
                                   ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_OverlappedDates);
                                   ApexPages.addmessage(msg);
                                   return null;
                                   break;         
                                }
                            }// For Loop End--SRC
                            if (goodwillUpdateFlag) 
                            {
                                  system.debug('## goodwillUpdateFlag value is..:: '+ goodwillUpdateFlag);
                                  upsert gudwill;
                            }
                        }     
                    }
                    else
                    {
                       ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_InvalidDateRange);
                       ApexPages.addmessage(msg);
                       return null;
                    }
                }
            }  
            else if (strResolutionCode!=null && strResolutionCode == System.label.GOO_CarRentalDLPA)  
            {
                if ((gudwill.GoodwillStatus__c == System.label.GOO_Refused) &&(listStatus.size()==0))
                { 
                    upsert gudwill;
                }
                else if ((gudwill.GoodwillStatus__c != System.label.GOO_Refused)&&(listStatus.size()==0))
                {
                   upsert gudwill;
                } 
                else 
                {
                   
                  if(gudwill.Decided_end_date__c!=null && (gudwill.Decided_start_date__c > gudwill.Decided_end_date__c)) 
                  {
                    ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_InvalidDateRange);
                    ApexPages.addmessage(msg);
                    return null;
                  }
                  else 
                  { 
                    for(Goodwill__c olddate:listStatus)
                    { 
                       if ((gudwill.Decided_start_date__c > olddate.Decided_start_date__c || (Date.valueOf(gudwill.Decided_start_date__c) == Date.ValueOf(gudwill.Decided_end_date__c))) && olddate.Decided_end_date__c!=null)
                       {
                            if (gudwill.Decided_end_date__c!=null && ((gudwill.Decided_start_date__c == olddate.Decided_start_date__c || gudwill.Decided_end_date__c == olddate.Decided_end_date__c))) 
                            { 
                                goodwillUpdateFlag=false;
                                ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_OverlappedDates);
                                ApexPages.addmessage(msg);
                                return null;
                                break;
                            } 
                            else if((gudwill.Decided_start_date__c > olddate.Decided_start_date__c && gudwill.Decided_start_date__c > olddate.Decided_end_date__c)|| (gudwill.Decided_start_date__c < olddate.Decided_start_date__c && gudwill.Decided_end_date__c < olddate.Decided_end_date__c ))
                            {
                                goodwillUpdateFlag=true;
                            }
                            else
                            { 
                                goodwillUpdateFlag=false;
                                ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_OverlappedDates);
                                ApexPages.addmessage(msg);
                                return null;    
                                break;     
                            }
                        }
                        else 
                        {
                           goodwillUpdateFlag=false;
                           ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,Label.GOO_OverlappedDates);
                           ApexPages.addmessage(msg);
                           return null;    
                           break;
                        }  
                    }// For Loop End--DLPA 
                  } 
                } 
                if (goodwillUpdateFlag) 
                {
                  system.debug('## goodwillUpdateFlag value is..:: '+ goodwillUpdateFlag);
                  upsert gudwill;
                }
            } // end of elseif loop
    
        } catch (Exception e) {
                 System.debug('Exception occured while Saving Goodwill ##' + e);
            }
             System.debug('Goodwill '+gudwill.Id);
        return pageRegGW = new PageReference('/' + gudwill.Id);
    }
 }