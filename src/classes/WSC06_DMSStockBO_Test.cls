/**
	Class   -   WSC06_DMSStockBO_Test
    Author  -   Suresh Babu
    Date    -   20/01/2013
    
    #01 <Suresh Babu> <20/01/2013>
        Created this class using test for WSC06_DMSStockBO.
**/
@isTest
private class WSC06_DMSStockBO_Test {

    static testMethod void Test_1() {

    	// Test for returnVehicleDMS method..
    	// Insert Vehicle Relation, Vehicle And Dealer Account records.
    	List<VRE_VehRel__c> vehicleRelationRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
    	
    	String stringBIR = '1000';
    	WSC06_DMSStockBO.getInstance().returnVehicleDMS(stringBIR);
        
    }
    
    static testMethod void Test_2(){
    	
    	// Test for updateLstVehicleDMSVO method..
    	// Insert Vehicle Relation, Vehicle And Dealer Account records.
    	List<VRE_VehRel__c> vehicleRelationRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
    	
    	String stringBIR = '1000';
    	
    	// 
    	List<WSC04_VehicleDMSVO> lstStockDMS = new List<WSC04_VehicleDMSVO>();
    	List<WSC04_VehicleDMSVO> lstStockDMSError = new List<WSC04_VehicleDMSVO>();
    	
    	WSC04_VehicleDMSVO stockDMS = new WSC04_VehicleDMSVO();
    	
    	stockDMS.color = 'Black';
    	stockDMS.entryInventoryDate = system.today();
    	//stockDMS.error = false;
    	stockDMS.errorMessage = '';
    	stockDMS.lastUpdateDate = system.now();
    	stockDMS.manufacturingYear = 2012;
    	stockDMS.model = 'Sandero';
    	stockDMS.modelYear = 2012;
    	stockDMS.optionals = 'test option';
    	stockDMS.price = 50000.00;
    	stockDMS.status = 'Available';
    	stockDMS.version = 'ver 1';
    	stockDMS.VIN = 'AAAA1003';
    	
    	lstStockDMS.add( stockDMS );
    	
    	WSC06_DMSStockBO.getInstance().updateLstVehicleDMSVO(stringBIR, lstStockDMS, lstStockDMSError);
    	
    }
    
    static testMethod void Test_3(){
    	
    	// Test for updateLstVehicleDMSVO method..
    	// Insert Vehicle Relation, Vehicle And Dealer Account records.
    	List<VRE_VehRel__c> vehicleRelationRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
    	
    	String stringBIR = '1000';
    	
    	List<WSC04_VehicleDMSVO> lstStockDMS = new List<WSC04_VehicleDMSVO>();
    	List<WSC04_VehicleDMSVO> lstStockDMSError = new List<WSC04_VehicleDMSVO>();
    	
    	WSC04_VehicleDMSVO stockDMS = new WSC04_VehicleDMSVO();
    	stockDMS.color = 'Black';
    	stockDMS.entryInventoryDate = system.today();
    	//stockDMS.error = false;
    	stockDMS.errorMessage = '';
    	stockDMS.lastUpdateDate = system.now() - 2; // lastmodified date wise test..
    	stockDMS.manufacturingYear = 2012;
    	stockDMS.model = 'Sandero';
    	stockDMS.modelYear = 2012;
    	stockDMS.optionals = 'test option';
    	stockDMS.price = 50000.00;
    	stockDMS.status = 'Available';
    	stockDMS.version = 'ver 1';
    	stockDMS.VIN = 'AAAA1003'; // Duplicate value to check UPDATE..
    	
    	lstStockDMS.add( stockDMS );
    	
    	WSC06_DMSStockBO.getInstance().updateLstVehicleDMSVO(stringBIR, lstStockDMS, lstStockDMSError);
    	
    }
    
    
    static testMethod void Test_4(){
    	
    	// Test for updateLstVehicleDMSVO method..
    	// Insert Vehicle Relation, Vehicle And Dealer Account records.
    	List<VRE_VehRel__c> vehicleRelationRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertVehicleRelation_DMS_WS();
    	
    	String stringBIR = '1000';
    	
    	List<WSC04_VehicleDMSVO> lstStockDMS = new List<WSC04_VehicleDMSVO>();
    	List<WSC04_VehicleDMSVO> lstStockDMSError = new List<WSC04_VehicleDMSVO>();
    	
    	WSC04_VehicleDMSVO stockDMS = new WSC04_VehicleDMSVO();
    	stockDMS.color = 'Black';
    	stockDMS.entryInventoryDate = system.today();
    	//stockDMS.error = false;
    	stockDMS.errorMessage = '';
    	stockDMS.lastUpdateDate = system.now();
    	stockDMS.manufacturingYear = 2012;
    	stockDMS.model = 'Sandero';
    	stockDMS.modelYear = 2012;
    	stockDMS.optionals = 'test option';
    	stockDMS.price = 50000.00;
    	stockDMS.status = 'Available';
    	stockDMS.version = 'ver 1';
    	stockDMS.VIN = 'AAAA1003123'; // Duplicate value to check INSERT..
    	
    	lstStockDMS.add( stockDMS );
    	
    	WSC06_DMSStockBO.getInstance().updateLstVehicleDMSVO(stringBIR, lstStockDMS, lstStockDMSError);
    }
    
    
	static testMethod void Test_5()
	{	/*
    	// Prepare test data
    	List<Account> lstNWSiteAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertDealerAccount_WithOutBillingAddress();
        List<VEH_Veh__c> lstVehicle = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToVehicleObjects();
        VRE_VehRel__c lstVehRel = new VRE_VehRel__c[] {
            new VRE_VehRel__c(
                    VIN__c = lstVehicle[0].Id,
                    StartDateRelation__c = Date.today() - 30,
                    EndDateRelation__c = Date.today() - 1,
                    Status__c= 'In Transit'),
            new VRE_VehRel__c(
                    VIN__c = lstVehicle[1].Id,
                    StartDateRelation__c = Date.today() - 30,
                    EndDateRelation__c = Date.today() - 1,
                    Status__c= 'Available')};
		insert lstVehRel;

    	String stringBIR = '1000';

    	List<WSC04_VehicleDMSVO> lstStockDMS = new List<WSC04_VehicleDMSVO>();
    	List<WSC04_VehicleDMSVO> lstStockDMSError = new List<WSC04_VehicleDMSVO>();
    	
    	WSC04_VehicleDMSVO stockDMS = new WSC04_VehicleDMSVO();
    	
    	stockDMS.color = 'Black';
    	stockDMS.entryInventoryDate = system.today();
    	stockDMS.errorMessage = '';
    	stockDMS.lastUpdateDate = system.now();
    	stockDMS.manufacturingYear = 2012;
    	stockDMS.model = 'Sandero';
    	stockDMS.modelYear = 2012;
    	stockDMS.optionals = 'test option';
    	stockDMS.price = 50000.00;
    	stockDMS.status = 'To be billed';
    	stockDMS.version = 'ver 1';
    	stockDMS.VIN = 'AAAA1003';
    	
    	lstStockDMS.add(stockDMS);
    	
    	WSC06_DMSStockBO.getInstance().updateLstVehicleDMSVO(stringBIR, lstStockDMS, lstStockDMSError);
    	*/
    }
}