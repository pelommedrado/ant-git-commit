/** @author S?bastien Ducamp
  * @date 09/10/2014
  * Apex class repsenting a response type of a personal data webservice (SIC, BCS, MDM)
  **/
public class Myr_PersonalDataResponse_Cls {
	
		/** @constructor **/
		public Myr_PersonalDataResponse_Cls() {
			InfoMsg info = new InfoMsg();
			response = new PersonalDataResponse();
			response.info = info;  
		}
		
		public PersonalDataResponse response {get;set;}
		
		/** Answer of the Personal Data webservice **/
		public class InfoMsg {
			public String codRetour {get;set;}
			public String msgRetour {get;set;}
		}
		
		/** General Response of the WebService **/
		public class PersonalDataResponse {  
			public InfoMsg info {get;set;}   
			public Myr_PersonalData_WS.DataSourceType dataSource {get;set;}
			public String rawResponse {get; set;}
			public String rawQuery {get; set;}
			public Integer searchPartyResponse {get;set;}	//MDM
			public List<InfoClient> infoClients {get;set;}
			public PersonalDataResponse() {  
				infoClients = new List<InfoClient>();
			}
		}

		public class InfoClient { 
			//Customers Id
	        public String IdClient {get;set;}
	        public String CustIdentNumber {get;set;}
	        public String CustIdentNumber2 {get;set;}
	        public String strPartyID {get;set;} 			//MDM
			public String strPartySub {get;set;} 			//MDM
	        public String strDOB {get;set;}					//MDM
	        public String strAccoutType{get;set;}			//MDM
			public String PartySegment {get; set;}				//MDM
	        public String MyrId {get; set;} 
	        //Person
	        public String Title {get;set;}
			public String LastName {get;set;}
	        public String FirstName {get;set;}
	        public String MiddleName {get;set;}
			public String FirstName2 {get;set;}
	        public String Lang {get;set;}
	        public Date DateOfBirth {get;set;}
	        public String Sex {get;set;}
	        public String typeperson {get;set;}
			public String MaritalStatus {get;set;}				//MDM
			public Boolean RenaultGroupStaff {get;set;}			//MDM
			public String NumberOfChildren {get;set;}			//MDM
			public String Deceased {get;set;}					//MDM
			public String Industry {get; set;}					//MDM
			public String JobClass {get; set;}					//MDM
			public String CommercialName {get; set;}			//MDM
			public String FinancialStatus {get; set;}			//MDM

	        //Email
	        public String email {get;set;} 
			public String emailPro {get;set;} 
	        //Communication Agreement
			public String PreferedMedia {get; set;}
	        public String GlobalCommAgreement {get;set;}
	        public String PostCommAgreement {get;set;}
	        public String TelCommAgreemen {get;set;}
	        public String SMSCommAgreement {get;set;}
	        public String FaxCommAgreement {get;set;}
	        public String EmailCommAgreement {get;set;}
			public String stopComFlag {get;set;}
			public Date stopComFlagDate {get;set;}
			public String stopComSMDFlag {get;set;}
			public Date stopComSMDFlagDate {get;set;}

	        //Phone numbers
	        public String LandLine1 {get; set;} 
	        public String LandLine2 {get; set;}
	        public String MobilePhone1 {get; set;}  
	        //Address				
	        public String StrName {get;set;}				//MDM: strAddressLine1
	        public String StrNum {get;set;}
	        public String StrType {get;set;}				
	        public String StrCompl1 {get;set;}
	        public String StrCompl2 {get;set;}
	        public String StrCompl3 {get;set;}
	        public String CountryCode {get;set;}
	        public String Zip {get;set;}
	        public String City {get;set;}
	        public String AreaCode {get;set;}
			public String PersonOtherStreet {get;set;}
			public String PersonOtherCity {get;set;}
			public String PersonOtherState {get;set;}
			public String PersonOtherPostalCode {get;set;}
			public String PersonOtherCountry {get;set;}
			
	        //Vehicles
	        public String vin {get;set;}
	        public List<Myr_PersonalDataResponse_Cls.Vehicle> vcle {get;set;}
			//Dealers
	        public List<Myr_PersonalDataResponse_Cls.Dealer> dealers {get;set;} //the order in the list is the order in the preference ...
			//inner @constructor
			public InfoClient() {
				vcle = new List<Myr_PersonalDataResponse_Cls.Vehicle>();
				dealers = new List<Myr_PersonalDataResponse_Cls.Dealer>();
			}
		}
		
		/** Inner class to define the preferred dealers of a customer **/
		public class Dealer {
			public String BirId {get; set;}
			//constructor
			public Dealer( String birId) {
				this.BirId = birId;
			}
		}
 
        public class Vehicle{
        	//Vehicle definition
        	public String vin {get;set;}
          	public String IdClient {get;set;}
          	public String brandCode {get;set;}
			public String model {get;set;}
	        public String modelCode {get;set;}
	        public String modelLabel {get;set;}
	        public String versionLabel {get;set;}
	        public Date firstRegistrationDate {get;set;}
	        public Date lastRegistrationDate {get;set;}
	        public Date TechnicalControlDate {get; set;}
			public Date DeliveryDate {get; set;}
	        public String registration {get;set;}
	        public String vehicleType{get;set;}
	        //Vehicle Relation definition
	        public Date possessionBegin {get;set;}
	        public Date possessionEnd {get;set;} 
	        public Boolean VNVO_NewVehicle {get;set;}
	        public String TypeRelationship {get;set;} 
	        //???
		    public String startDate{get;set;}				//MDM
		    public String endDate{get;set;}					//MDM
		    public String firstName{get;set;}				//MDM
		    public String lastName{get;set;} 				//MDM
    	}
}