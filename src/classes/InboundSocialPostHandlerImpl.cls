// ---------------------------------------------------------------------------
// Project: RForce
// Name: InboundSocialPostHandlerImpl
// Desc: Social Care Customer Class
// CreatedBy: Sumanth(RNTBCI)
// CreatedDate: 21/04/2015
// ------------------------------
global virtual class InboundSocialPostHandlerImpl implements Social.InboundSocialPostHandler
{
    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return 5;
    }
    global virtual String getDefaultAccountId() {
        return null;
    }
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        system.debug('post--->@@'+post.PostTags);       
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        result.setSuccess(true);
        matchPost(post);
        matchPersona(persona);
        if (post.Id != null) {
            handleExistingPost(post, persona);
        }
        setReplyTo(post, persona, rawData);
        buildPersona(persona,post);
        Case parentCase = buildParentCase(post, persona, rawData);
        setRelationshipsOnPost(post, persona, parentCase);
        upsert post;
        system.debug('--->rawData@@@@'+rawData);
        system.debug('result-->@@@@@@@');
        return result;
    }
    private void handleExistingPost(SocialPost post, SocialPersona persona) {
        system.debug('--->handleExistingPost@@@@');
        update post;
        if (persona.id != null)
            update persona;
    }
    private void setReplyTo(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        system.debug('--->setReplyTo@@@@');
        SocialPost replyTo = findReplyTo(post, persona, rawData);
        if(replyTo.id != null) {
            post.replyToId = replyTo.id;
            post.replyTo = replyTo;
        }
    }
    private SocialPersona buildPersona(SocialPersona persona,SocialPost post) {
        system.debug('--->buildPersona@@@@');
        if (persona.Id == null)
            createPersona(persona,post);
        else
            update persona;
        return persona;
    }
    private Case buildParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData){
        system.debug('--->buildParentCase@@@@');
        system.debug('post--->@@'+post.PostTags);
        Case parentCase = findParentCase(post, persona);
        if (caseShouldBeReopened(parentCase))
            reopenCase(parentCase);
        else if((post.PostTags.contains('Renault_France,NEW_CASE')) && (parentCase.id == null ||
            parentCase.isClosed))
        parentCase = createCase(post, persona);
        return parentCase;
    }
    
    private boolean caseShouldBeReopened(Case c){
        system.debug('--->caseShouldBeReopened@@@@');
        return c.id != null && c.isClosed && System.now() <
        c.closedDate.addDays(getMaxNumberOfDaysClosedToReopenCase());
    }
    
    private void setRelationshipsOnPost(SocialPost postToUpdate, SocialPersona persona,Case parentCase) {
        system.debug('--->setRelationshipsOnPost@@@@');
        if (persona.Id != null)
            postToUpdate.PersonaId = persona.Id;
        if(parentCase.id != null)
        postToUpdate.ParentId = parentCase.Id;
    }
    //Creates case with social persona and social post details
    private Case createCase(SocialPost post, SocialPersona persona) {
        system.debug('--->createCase@@@@');
        Case newCase = new Case(subject = post.Name);
        if (persona != null && persona.ParentId != null) {
            if (persona.ParentId.getSObjectType() == Account.sObjectType)
                newCase.AccountId = persona.ParentId;
        }
        newCase.Origin=system.label.CAS_Origin_SocialMedia;
        newCase.CaseSubSource__c=post.MediaType;
        newCase.From__c='Customer';
        if(post.PostTags!=null){
            List<String> actualTag=post.PostTags.split(',');
            if(actualTag!=null){
                List<String> brandcountry=actualTag[0].split('_');
                if(brandcountry!=null){
                    newCase.CaseBrand__c=brandcountry[0];
                        try{
                            String rectypename=Country_Info__c.getInstance(brandcountry[1]).Case_RecordType__c;                   
                            Id rectypeid=[Select Id from RecordType where DeveloperName=:rectypename].Id;
                            newCase.RecordTypeId=rectypeid;
                        }catch(Exception e){
                            system.debug(e);
                        }                      
                    if(brandcountry.size()==3){
                        newCase.CountryCase__c=brandcountry[2];
                    }
                }
            }
        }
        newCase.Description=post.Content;
        insert newCase;
        return newCase;
    }
    private Case findParentCase(SocialPost post, SocialPersona persona) {
        system.debug('--->findParentCase@@@@');
        Case parentCase = new Case();
        if (post.ReplyTo != null && (post.ReplyTo.IsOutbound || post.ReplyTo.PersonaId == persona.Id))
            parentCase = findParentCaseFromPostReply(post);
        return parentCase;
    }
    private Case findParentCaseFromPostReply(SocialPost post){
        system.debug('--->findParentCaseFromPostReply@@@@');
        List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Id =:post.ReplyTo.ParentId LIMIT 1];

        if(!cases.isEmpty())
            return cases[0];

        return new Case();
    }

    private void reopenCase(Case parentCase) {
        system.debug('--->reopenCase@@@@');  
        SObject[] status = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = false AND IsDefault = true];
        parentCase.Status = ((CaseStatus)status[0]).MasterLabel;
        update parentCase;
    }

    private void matchPost(SocialPost post) {
        system.debug('--->matchPost@@@@'); 
        if (post.Id != null || post.R6PostId == null) 
            return;
            List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE R6PostId =:post.R6PostId LIMIT 1];
        if (!postList.isEmpty())
            post.Id = postList[0].Id;
    }

    private SocialPost findReplyTo(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        system.debug('--->findReplyTo@@@@'); 
        if(post.replyToId != null && post.replyTo == null)
            return findReplyToBasedOnReplyToId(post);
        if(rawData.get('replyToExternalPostId') != null &&
           String.isNotBlank(String.valueOf(rawData.get('replyToExternalPostId'))))
            return findReplyToBasedOnExternalPostIdAndProvider(post,String.valueOf(rawData.get('replyToExternalPostId')));
        return new SocialPost();
    }

    private SocialPost findReplyToBasedOnReplyToId(SocialPost post){
        system.debug('--->findReplyToBasedOnReplyToId@@@@'); 
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE id = :post.replyToId LIMIT 1];
        if(posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }

    private SocialPost findReplyToBasedOnExternalPostIdAndProvider(SocialPost post, String externalPostId){
        system.debug('--->findReplyToBasedOnExternalPostIdAndProvider@@@@'); 
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE Provider = :post.provider AND ExternalPostId = :externalPostId LIMIT 1];
        if(posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }

    //Searching for existing social persona , if exists assign the parentid
    private void matchPersona(SocialPersona persona) {
        if (persona != null && persona.ExternalId != null && String.isNotBlank(persona.ExternalId)) {
            List<SocialPersona> personaList = [SELECT Id, ParentId FROM SocialPersona WHERE Provider = :persona.Provider AND
            ExternalId = :persona.ExternalId LIMIT 1];
            if ( !personaList.isEmpty()) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
            }
        }
    }
    //Create social persona and assign the creted personal account Id to parentid
    private void createPersona(SocialPersona persona,SocialPost post) {
        if (persona == null || (persona.Id != null && String.isNotBlank(persona.Id)) || !isThereEnoughInformationToCreatePersona( persona))
            return;
        SObject parent = createPersonaParent(persona,post);
        persona.ParentId = parent.Id;
        insert persona;
    }

    private boolean isThereEnoughInformationToCreatePersona(SocialPersona persona){
        return persona.ExternalId != null && String.isNotBlank(persona.ExternalId) &&
                persona.Name != null && String.isNotBlank(persona.Name) &&
                persona.Provider != null && String.isNotBlank(persona.Provider) &&
                persona.provider != 'Other';
    }

    private boolean hasSkipCreateCaseIndicator(Map<String, Object> rawData) {
        Object skipCreateCase = rawData.get('skipCreateCase');
        return skipCreateCase != null && 'true'.equalsIgnoreCase(String.valueOf(skipCreateCase));
    }
    //Creates a personal account from social persona detials
    global virtual SObject createPersonaParent(SocialPersona persona,SocialPost post) {
        String name = persona.Name;
        if (persona.RealName != null && String.isNotBlank(persona.RealName))
            name = persona.RealName;
            String firstName = '';
            String lastName = 'unknown';
        if (name != null && String.isNotBlank(name)) {
            firstName = name.substringBeforeLast(' ');
            lastName = name.substringAfterLast(' ');
            if (lastName == null || String.isBlank(lastName))
                lastName = firstName;
        }
        String country;
        String brand;
        if(post.PostTags!=null){
            List<String> actualTag=post.PostTags.split(',');
            if(actualTag!=null){
                List<String> brandcountry=actualTag[0].split('_');
                if(brandcountry!=null){
                    brand=brandcountry[0];
                    country=brandcountry[1];
                }
            }
        }                
        Account acc= new Account(LastName = lastName, FirstName = firstName,Country__c=country);
        String defaultAccountId = getDefaultAccountId();
        insert acc; 
        return acc;
    }
}