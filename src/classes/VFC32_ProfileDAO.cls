/**
*	Class	-	VFC32_ProfileDAO
*	Author	-	Surseh Babu
*	Date	-	19/11/2012
*
*	#01 <Suresh Babu> <19/11/2012>
*		DAO class for Profile object to Query records.
**/
public with sharing class VFC32_ProfileDAO 
{
	private static final VFC32_ProfileDAO instance = new VFC32_ProfileDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC32_ProfileDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC32_ProfileDAO getInstance()
    {
        return instance;
    }
    
   	public Profile findById(String profileId)
   	{
   		Profile sObjProfile = null;
   		
   		try
   		{
   			sObjProfile = [SELECT Id,
   			                      Name
   			               FROM Profile
   			               WHERE Id =: profileId];
   		}
   		catch(QueryException ex)
   		{ 			
   		}
   		
   		return sObjProfile;
   	}

    public Profile findByName(String profileName)
    {
      Profile sObjProfile = null;
      
      try
      {
        sObjProfile = [SELECT Id
                       FROM Profile
                       WHERE Name =: profileName];
      }
      catch(QueryException ex)
      {       
      }
      
      return sObjProfile;
    }
}