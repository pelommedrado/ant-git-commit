/**
* Classe que será disparada pela trigger do objeto VehicleBooking__c antes da inserção dos registros.
* Obs: Nome abreviado para não ultrapassar 40 caracteres.
* @author Felipe Jesus Silva.
*/
public class VFC95_VehicleBookBeforeInsertExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{	
		List<VehicleBooking__c> lstSObjCurrentVehicleBooking = (List<VehicleBooking__c>) lstNewtData;
		
		this.checkStatus(lstSObjCurrentVehicleBooking);
	}
	
	private void checkStatus(List<VehicleBooking__c> lstSObjCurrentVehicleBooking)
	{
		Map<String, VehicleBooking__c> mapSObjVehicleBooking = null;
		Set<String> setVehicleId = new Set<String>();
		List<VehicleBooking__c> lstSObjVehicleBookingAux = new List<VehicleBooking__c>();
		
		for(VehicleBooking__c sObjCurrentVehicleBooking : lstSObjCurrentVehicleBooking)
		{
			if((String.isNotEmpty(sObjCurrentVehicleBooking.Status__c)) && (sObjCurrentVehicleBooking.Status__c.equals('Active')))
			{
				/*adiciona o id do veículo no set para fazer a busca pelos vehicle bookings ativos*/
				setVehicleId.add(sObjCurrentVehicleBooking.Vehicle__c);
				
				/*adiciona o objeto nessa lista auxiliar para depois percorrê-la e evitar percorrer a lista original (ganho de performance na próxima percorrida)*/
				lstSObjVehicleBookingAux.add(sObjCurrentVehicleBooking); 
			}
		}
			
		if(!setVehicleId.isEmpty())
		{
			try
			{
				/*obtém o mapeamento dos vehicle bookings (a chave desse map é o id do veículo)*/
				mapSObjVehicleBooking = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesByVehicles(setVehicleId);
				
				/*percorre a lista auxiliar de veículos*/
				for(VehicleBooking__c sObjVehicleBooking : lstSObjVehicleBookingAux)
				{
					/*verifica se esse veículo está contido no map*/
					if(mapSObjVehicleBooking.containsKey(sObjVehicleBooking.Vehicle__c))
					{
						/*aborta a atualização do veículo pois já existe uma reserva para o mesmo*/
						sObjVehicleBooking.addError('Veículo indisponível para reserva.');
					}
				}
			}
			catch(VFC89_NoDataFoundException ex)
			{
				/*se cair nessa exceção significa que nenhum registro de vehicle booking foi encontrado, logo, não há ação a ser tomada, simplesmente prossegue a execução*/
			}
		}		
	}
	
}