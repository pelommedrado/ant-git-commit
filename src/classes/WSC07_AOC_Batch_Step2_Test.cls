@isTest
public  class WSC07_AOC_Batch_Step2_Test
{
	 static testmethod void myUnitTest1()
	 {
		// Prepare test data
		Item__c item = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToItem();
		List<Vehicle_Item__c> VehicleItem = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToVehicleItem();
        List<Product2> lstProducts= VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
        system.assert(lstProducts.size() > 0);
        List<CS_Models__c> lstCSModels = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertCSModels();
        WSC07_AOC_Batch_Step2 aocBatchStep2 = new WSC07_AOC_Batch_Step2();
        Database.BatchableContext BC;
        
        // Start test
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpResponseGenerator());
        
        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpAOC_Batch_Step2());
        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpAOC_Batch_Step2_Category());
        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpAOC_Batch_Step2_Pricelist());
        
        aocBatchStep2.start(BC);
        aocBatchStep2.execute(BC, lstCSModels);
        aocBatchStep2.finish(BC);
       
        // aocBatchStep2.Test_WrapperValues();
        //WSC07_AOC_Batch_Step2.Wrapper_Label wrapperlabel = new WSC07_AOC_Batch_Step2.Wrapper_Label();
        //wrapperlabel.pt = 'Test';
        //WSC07_AOC_Batch_Step2.Wrapper_Presentation wrapperPresentation = new WSC07_AOC_Batch_Step2.Wrapper_Presentation(wrapperlabel,'Test1');
    
    	// Stop test
        Test.stopTest();
    }
    
    static testmethod void myUnitTest2()
	{
		// Prepare test data
		WSC07_AOC_Batch_Step2 aocBatchStep2 = new WSC07_AOC_Batch_Step2();
		
		// Start test
        Test.startTest();
        
    	WSC07_AOC_Batch_Step2.wrapper_allInformationfrom_JSON wrapTest01 = new WSC07_AOC_Batch_Step2.wrapper_allInformationfrom_JSON();
    	WSC07_AOC_Batch_Step2.wrapper_vehicleItems wrapTest02 = new WSC07_AOC_Batch_Step2.wrapper_vehicleItems(null, null, null);
    	WSC07_AOC_Batch_Step2.wrapper_presentation_Values wrapTest03 = new WSC07_AOC_Batch_Step2.wrapper_presentation_Values(null);
    	WSC07_AOC_Batch_Step2.wrapper_presentationGroup wrapTest04 = new WSC07_AOC_Batch_Step2.wrapper_presentationGroup(null, null, null, null);
	    WSC07_AOC_Batch_Step2.wrapper_presentationGroup_Label wrapTest05 = new WSC07_AOC_Batch_Step2.wrapper_presentationGroup_Label();
    	WSC07_AOC_Batch_Step2.wrapper_presentationItem wrapTest06 = new WSC07_AOC_Batch_Step2.wrapper_presentationItem(null, null);
    	WSC07_AOC_Batch_Step2.wrapper_presentationItem_Label wrapTest07 = new WSC07_AOC_Batch_Step2.wrapper_presentationItem_Label();
    	WSC07_AOC_Batch_Step2.wrapper_priceList wrapTest08 = new WSC07_AOC_Batch_Step2.wrapper_priceList(null, null);
    	WSC07_AOC_Batch_Step2.wrapper_VersionPrice wrapTest09 = new WSC07_AOC_Batch_Step2.wrapper_VersionPrice();
    	WSC07_AOC_Batch_Step2.wrapper_PresentationItem_Array wrapTest10 = new WSC07_AOC_Batch_Step2.wrapper_PresentationItem_Array(null);
    	
    	
    	
    	aocBatchStep2.Test_Wrapper();
    	    	
    	// Stop test
        Test.stopTest();
    }
    
    
    static testmethod void myUnitTest3()
	{
		// Prepare test data
		WSC07_AOC_Batch_Step2 aocBatchStep2 = new WSC07_AOC_Batch_Step2();
		
		// Record1
		WSC07_AOC_Batch_Step2.wrapper_allInformationfrom_JSON wrap_JSON = new WSC07_AOC_Batch_Step2.wrapper_allInformationfrom_JSON();
		wrap_JSON.vehicleCode = 'X-01';
		wrap_JSON.model_version_ID = null;
		wrap_JSON.item_Key = 'key';
		wrap_JSON.item_Value = 'value';
		aocBatchStep2.set_Allvalues_from_JSON = new Set<WSC07_AOC_Batch_Step2.wrapper_allInformationfrom_JSON>();
		aocBatchStep2.set_Allvalues_from_JSON.add( wrap_JSON );
		
		
		// Start test
        Test.startTest();
       
        
    	aocBatchStep2.category_Parsing();
		
		// Stop test
        Test.stopTest();
    }


    /* static testmethod void myUnitTest2() {
        List<CS_Models__c> lstCSModels = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertCSModels();
        system.assert(lstCSModels.size() > 0);
        WSC07_AOC_Batch_Step2 aocBatchStep2 = new WSC07_AOC_Batch_Step2();
        Database.BatchableContext BC;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WSC07_MockHttpAOC_Batch_Step2());
       // aocBatchStep1.start(BC);
        aocBatchStep2.execute(BC, lstCSModels);
        aocBatchStep2.finish(BC);
        aocBatchStep2.Test_WrapperValues();
        WSC07_AOC_Batch_Step2.Wrapper_Label wrapperlabel = new WSC07_AOC_Batch_Step2.Wrapper_Label();
        wrapperlabel.pt = 'Test';
        WSC07_AOC_Batch_Step2.Wrapper_Presentation wrapperPresentation = new WSC07_AOC_Batch_Step2.Wrapper_Presentation(wrapperlabel,'Test1');
        Test.stopTest();
    } */
}