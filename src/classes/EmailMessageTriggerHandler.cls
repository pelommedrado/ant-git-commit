/*****************************************************************************************
    Name    : EmailMessageTriggerHandler
    Desc    : This is to create new Case if an Email is received in a closed case.
    Author  : Moctar BA (Atos Integration)
    Project : CS-Rforce

******************************************************************************************/
public with sharing class EmailMessageTriggerHandler {
/**
 * @author RNTBCI(Sumanth Yeluri) 
 * @date 26/08/2015
 * @description New Case is created when an email comes a closed case
 * @param list of email messages
 */   
   public static void onBeforeInsert(List<EmailMessage> Messg){
       Map<Id,EmailMessage> memailParent=new Map<Id,EmailMessage>();
       List<EmailMessage> lcaseMessage=new List<EmailMessage>();
       List<GroupMember> lmember=new List<GroupMember>();
       Map<Id,GroupMember> mGroupMember=new Map<Id,GroupMember>();
       Set<Id> scaseOwner = new Set<Id>();
       List<Case> lnewCase = new List<Case>();
       List<EmailMessage> lnewEmailMsg=new List<EmailMessage>();
       List<Task> linboundTaslList=new List<Task>();
       AssignmentRule assignRule= new AssignmentRule();
       assignRule=[Select Id from AssignmentRule where Name='Renault WW Case assignment' and Active=true];   
       Database.Dmloptions dmlOpts = new Database.Dmloptions();
       dmlOpts.assignmentRuleHeader.assignmentRuleId= assignRule.id;
          //Getting list of all incoming messages on cases
          for(EmailMessage msg:Messg){
              if(msg.ParentId.getSobjectType() == Case.SobjectType && msg.Incoming){
                  memailparent.put(msg.ParentId,msg);
                  lcaseMessage.add(msg);
              }
          }         
          if(memailparent!=null){
              try{
                  //Getting case detials of all the email messages only if the case is cloased
                  Map<Id,Case> mclosedCase=new Map<Id,Case>([SELECT id,RecordType.name, Description, AccountId, ContactId, OwnerId, CaseNumber, RecordTypeId, CountryCase__c, status,CaseBrand__c, Origin, Subject, VIN__c
                                                          FROM Case 
                                                          WHERE Id IN:memailparent.Keyset()AND Status='Closed' AND CountryCase__c!='Brazil']);
                  system.debug('mclosedCase--->&&'+mclosedCase);
                  //getting the list of cases to be created
                  if(mclosedCase!=null){
                      for(EmailMessage emg:lcaseMessage){
                          Case closedCase=mclosedCase.get(emg.ParentId);                          
                          String emailOnClosedCase='Email entering on closed case n°';
                          String transaltionOfDes=Country_Info__c.getValues(closedCase.CountryCase__c).Email_on_Closed_Case__c;
                          if(transaltionOfDes!=null)
                              emailOnClosedCase=transaltionOfDes;
                          String des=emailOnClosedCase+closedCase.CaseNumber;
                          String sub=emailOnClosedCase+closedCase.CaseNumber;                          
                          String onrId=closedCase.OwnerId;                          
                          Case newCas=new Case(ParentId=closedCase.id, Description=des,CaseBrand__c=closedCase.CaseBrand__c,Origin=closedCase.Origin, Subject=sub, AccountId=closedCase.AccountId, ContactId=closedCase.ContactId, OwnerId=closedCase.OwnerId, RecordTypeId=closedCase.RecordTypeId, VIN__c=closedCase.VIN__c, CountryCase__c=closedCase.CountryCase__c);       
                          newCas.setOptions(dmlOpts);
                          lnewCase.add(newCas);
                      }
                      system.debug('lnewCase--->&&'+lnewCase);
                      //inserting cases and email messages.
                      if(lnewCase!=null){
                          insert lnewCase;
                          for(Case c : lnewCase){
                              EmailMessage em=memailparent.get(c.ParentId); 
                              EmailMessage emsg= new EmailMessage(ParentId=c.Id, FromAddress=em.FromAddress, ToAddress=em.ToAddress, Subject=em.Subject, TextBody=em.TextBody, HtmlBody=em.HtmlBody, MessageDate=em.MessageDate, Status='0', incoming=true);
                              lnewEmailMsg.add(emsg);
                          }
                          system.debug('lnewEmailMsg--->&&'+lnewCase);
                          if(lnewEmailMsg!=null){
                              insert lnewEmailMsg;
                          } 
                      }
                  }
              }catch(Exception e){
                  system.debug(e);
              } 
             //Creating task for every incoming email on case    
               List<Case> lcasList=new List<Case>([SELECT id,CaseNumber,OwnerId,ContactId from Case where ID IN :memailparent.Keyset()]);
               for(Case c : lcasList){
                   String ownerid=c.OwnerId;
                   system.debug('-->c'+c);
                   if(ownerid.SubString(0,3)!='00G'){                   
                       Task t=new Task(WhoId=c.ContactId,IsReminderSet = true,WhatId=c.id,Type='Email - Inbound',Subject='Email - Inbound',ActivityDate=system.today(),Priority=system.label.TSK_Priority_Normal,Status=system.label.TSK_Status_ToDo,ReminderDateTime=system.now().addHours(2),OwnerId=c.OwnerId);                                              
                       linboundTaslList.add(t); 
                   }       
               }
               if(linboundTaslList!=null){
                   try{
                       system.debug('linboundTaslList&&&&'+linboundTaslList.size());
                       insert linboundTaslList;
                   }catch(Exception e){
                       system.debug(e);
                   }
               }                                    
          }
   } 
}