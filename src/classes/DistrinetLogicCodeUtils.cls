public class DistrinetLogicCodeUtils {
	
	private static final DistrinetLogicCodeUtils instance = new DistrinetLogicCodeUtils();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private DistrinetLogicCodeUtils(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static DistrinetLogicCodeUtils getInstance(){
        return instance;
    }
    
    public Map<Id, Quote> mapToQuoteDistrinet(List<DistrinetLogicCode__c> logicCodeLst, Set<String> quoteNbrSet, Boolean isCL1){
        
        Map<String, Id> quoteMapCL1 = new Map<String, Id>();
        Map<String, Quote> quoteMap = new Map<String, Quote>();
        Map<Id, Quote> quoteLstMap = new Map<Id, Quote>();
        
        //Processo diferenciado para CL1
        if(isCL1){
            
            Set<String> ownerControlSet = new Set<String>();
            Set<String> orderNumberSedreSet = new Set<String>();
            
            for(DistrinetLogicCode__c logicCode : logicCodeLst){
                ownerControlSet.add(logicCode.PC_OwnerAccountControl__c);
                orderNumberSedreSet.add(logicCode.PC_OrderNumberSedre__c);
            }
            
            List<Quote> lstQuoteCL1 = VFC35_QuoteDAO.getInstance().findBySedreNumberAndOwner(ownerControlSet, orderNumberSedreSet);
            
            for(Quote quote : lstQuoteCL1){
                quoteMapCL1.put(quote.PC_OwnerAccountControl__c + quote.PC_OrderNumberSedre__c, quote.Id);
            }
            
        //Processo para outros códigos
        } else{
            
            for(Quote quote : VFC35_QuoteDAO.getInstance().findByQuoteNumberSet(quoteNbrSet)){
                quoteMap.put(quote.External_Id__c, quote);
            }
        }
        
        for(DistrinetLogicCode__c logicCode : logicCodeLst){
            
            Quote quote = new Quote();
            
            if(isCL1){
                quote.Id = quoteMapCL1.get(logicCode.PC_OwnerAccountControl__c + logicCode.PC_OrderNumberSedre__c);
                
            } else if(!isCL1 && String.isNotEmpty(logicCode.PC_DmsOrderNumber__c) && quoteMap.get(logicCode.PC_DmsOrderNumber__c) != null){
                quote.Id = quoteMap.get(logicCode.PC_DmsOrderNumber__c).Id;
                quote.Name = quoteMap.get(logicCode.PC_DmsOrderNumber__c).Name;
            }
            
            quote.PC_LogicCode__c                         = logicCode.PC_LogicCode__c;
            
            if(String.isNotEmpty(logicCode.PC_CustomerNumberInternalSedre__c)){
                quote.PC_CustomerNumberInternalSedre__c   = logicCode.PC_CustomerNumberInternalSedre__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_ChassisNumberCurrent__c)){
                quote.PC_ChassisNumberCurrent__c          = logicCode.PC_ChassisNumberCurrent__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairModel__c)){
                quote.PC_SemiclairModel__c                = logicCode.PC_SemiclairModel__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairVersion__c)){
                quote.PC_SemiclairVersion__c              = logicCode.PC_SemiclairVersion__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairOptional__c)){
                quote.PC_SemiclairOptional__c             = logicCode.PC_SemiclairOptional__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_ColorBoxCriterion__c)){
                quote.PC_ColorBoxCriterion__c             = logicCode.PC_ColorBoxCriterion__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairUpholstered__c)){
                quote.PC_SemiclairUpholstered__c          = logicCode.PC_SemiclairUpholstered__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairHarmony__c)){
                quote.PC_SemiclairHarmony__c              = logicCode.PC_SemiclairHarmony__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_SemiclairColor__c)){
                quote.PC_SemiclairColor__c                = logicCode.PC_SemiclairColor__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_FirstName__c)){
                quote.PC_FirstName__c                     = logicCode.PC_FirstName__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_PersonalOrCompanyClientName__c)){
                quote.PC_PersonalOrCompanyClientName__c   = logicCode.PC_PersonalOrCompanyClientName__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_CustomerCompany__c)){
                quote.PC_CustomerCompany__c               = logicCode.PC_CustomerCompany__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_WayNome__c)){
            quote.PC_WayNome__c                       = logicCode.PC_WayNome__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_City__c)){
                quote.PC_City__c                          = logicCode.PC_City__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_State__c)){
                quote.PC_State__c                         = logicCode.PC_State__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_PersonalPhone__c)){
                quote.PC_PersonalPhone__c                 = logicCode.PC_PersonalPhone__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_MobilePhone__c)){
                quote.PC_MobilePhone__c                   = logicCode.PC_MobilePhone__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_ErrorMessage__c)){
                quote.PC_ErrorMessage__c                  = logicCode.PC_ErrorMessage__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_DmsOrderNumber__c)){
                quote.PC_DmsOrderNumber__c                = logicCode.PC_DmsOrderNumber__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_ExternalCustomerCodeSedre__c)){
                quote.PC_ExternalCustomerCodeSedre__c     = logicCode.PC_ExternalCustomerCodeSedre__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_OwnerAccountControl__c)){
                quote.PC_OwnerAccountControl__c           = logicCode.PC_OwnerAccountControl__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_OrderNumberSedre__c)){
                quote.PC_OrderNumberSedre__c              = logicCode.PC_OrderNumberSedre__c;
            }
            
            if(String.isNotEmpty(logicCode.PC_DmsOrderNumber__c)){
                quote.External_Id__c                      = logicCode.PC_DmsOrderNumber__c;
            }
            
            if(!isCL1 && String.isNotEmpty(logicCode.PC_DmsOrderNumber__c) && quoteMap.get(logicCode.PC_DmsOrderNumber__c) != null){
                quoteLstMap.put(quote.Id, quote);
            }
            
            System.debug('@@ quoteLstMap: ' + quoteLstMap);
        }
        
        return quoteLstMap;
    }
    
}