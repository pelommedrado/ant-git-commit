public with sharing class MYR_AccountHandler {


	/*
	 *
	 * @author LUCAS ANDRADE - [ kolekto ] - 26/10/2016
	 * Method to get account address from the given zip code and populate specific brazillian fields
	 *  - STARTS HERE -
	 *
	 */
	@future (callout = true)
	public static void getBrazilAddressFromZipCode(List<Id> newAccs, String profileId){


		List<Account> accs = VFC12_AccountDAO.getInstance().fetchAccountRecordsUsingListIDs(newAccs);
		List<Account> accToUpdate = new List<Account>();

		if(profileId == Utils.getProfileId('HeliosCommunity')){

			//Process to get address
			for(Account a : accs){

				if(a.ShippingPostalCode != null){

					String fullAddress = Utils.searchCEP(a.ShippingPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO' && fullAddress.contains('rua')){

						a.Shipping_PostalCode__c = a.ShippingPostalCode;
						a.Shipping_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_Country__c = 'Brazil';

					}
	            }

	            if(a.BillingPostalCode != null){

					String fullAddress = Utils.searchCEP(a.BillingPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO' && fullAddress.contains('rua')){

						a.Billing_PostalCode__c = a.BillingPostalCode;
						a.Billing_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_Country__c = 'Brazil';

					}

	            }

	            accToUpdate.add(a);

			}


			Database.SaveResult[] sr = Database.update(accToUpdate, false);


		}
	}

}