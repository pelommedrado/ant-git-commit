/**
*Felipe Livino {kolekto}
*Classe para alterar a data de fim da chamada de oferta e tambem o modelo.
* Caso a data da a��o comercial mude e a oferta ter a mesma data de fim ela deve ser
*alterada para a mesma data da a��o comercial
*/
public class VFC155_UpdateDateCallOffer {
    public VFC155_UpdateDateCallOffer(){}
    
    public  void atualizaModelo(PVCommercial_Action__c acao){
        List<PVCall_Offer__c> listaChamada = [select id , name from PVCall_Offer__c where Commercial_Action__c=:acao.Id ];
        List<PVCall_Offer__c> listaChamadaAux = new List<PVCall_Offer__c>();
        for(PVCall_Offer__c chamada : listaChamada){
            chamada.Model__c = acao.Model__c;
            listaChamadaAux.add(chamada);
        } 
        update listaChamadaAux;
        //atualizaVersoes(listaChamadaAux,acao.Model__c);
    }
    
    public void atualizaVersoes(List<PVCall_Offer__c> listaChamada,String model){
        List<VersionCallOffer__c> novasVersoes = new List<VersionCallOffer__c>(); 
        
        List<VersionCallOffer__c> listaPDeletar = [select id,Model__c from VersionCallOffer__c where Call_Offer__c in:(listaChamada)];
    	List<PVVersion__c> todasVersoes = [select id, name from PVVersion__c where Model__c =:model];
        
            for(VersionCallOffer__c aux : listaPDeletar){
                for(PVVersion__c versoes : todasVersoes){
                aux.Version__c = versoes.id;
                novasVersoes.add(aux);
            }
        }
        Database.update(novasVersoes);
       
    }
    
    public VFC155_UpdateDateCallOffer(PVCommercial_Action__c acao){
      listaChamadaOfertas = getCallOffer(acao);
      listaOfertas = getOferta(acao);
      atualizaGrupoOfertas = new List<Group_Offer__c>();
      atualizaOferta = new List<Offer__c>();
      atualizaChamada = new List<PVCall_Offer__c>();
      atualizacaoEmLote = new List<sObject>();
      idGrupoOferta = new List<String>();
      idGrupoAtualizado = new List<String>();
      idCallOffer = new List<String>();
    }
    //listas principais
    private List<Offer__c> listaOfertas;
    private List<String> stringOffer;
    private List<PVCall_Offer__c> listaChamadaOfertas;
    //listas para update
    List<sObject> atualizacaoEmLote;
    List<Group_Offer__c> atualizaGrupoOfertas;
    List<Offer__c> atualizaOferta;
    List<PVCall_Offer__c> atualizaChamada;
    //checagem
    List<String> idGrupoOferta;
    List<String> idGrupoAtualizado;
    List<String> idCallOffer;
    
    //get e set
    public List<PVCall_Offer__c> getCallOffer(PVCommercial_Action__c acao){
      return[ select Acount_Number__c,Action_Type__c,Calls_in_Commercial_Action__c,Call_Offer_End_Date__c,Call_Offer_Start_Date__c,
          Coefficient__c,Commercial_Action_End_Date__c,Commercial_Action_Start_Date__c,Commercial_Action__c,
          Description__c,Financing_Condition__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,Minimum_Entry__c,
          Model2__c,Model__c,Month_Rate__c,Name,Period_in_Months__c,Regional__c,State__c,Status__c,SystemModstamp,Template_Price__c
          from  PVCall_Offer__c where Commercial_Action__c =:acao.Id
      ];
    }
    
    public List<Offer__c> getOferta(PVCommercial_Action__c acao){
      return[ select Id,Offer_to_Internet__c,Name,Group_Offer__c,Status__c,Stage__c,Offer_Start_Date_Website__c, Call_Offer__c,Offer_End_Date_Website__c,PV_OF_CheckUpdate__c from Offer__c
              where Commercial_Action__c =: acao.Id
      ];
    }
    
    private List<Group_Offer__c> getGrupoOfertas(){
      return [select id,Date_End_Offer__c from Group_Offer__c where id in:(idGrupoOferta)];
    }
    //--------------------------------------------------------------------------------------------------------
    //busca os valores do id do grupo de ofertas em ofertas
    public void getIdGrupoOferta(){
    	for(Offer__C oferta : listaOfertas){
          idGrupoOferta.add(oferta.Group_Offer__c);
    	}
    	System.debug('######### id grupo oferta '+idGrupoOferta);
    	
    }
    //altera a data da chamada de oferta
    public void alteraCallOffer(PVCommercial_Action__c acaoAnterior, PVCommercial_Action__c acao){
        List<PVCall_Offer__c> listCall = new List<PVCall_Offer__c>();
        if(listaChamadaOfertas !=null && listaChamadaOfertas.size()>0)
        for(PVCall_Offer__c chamada: listaChamadaOfertas){
          if((chamada.Call_Offer_End_Date__c == acaoAnterior.End_Date__c || chamada.Call_Offer_End_Date__c> acao.End_Date__c ) &&
                  (chamada.Call_Offer_Start_Date__c < acao.End_Date__c)){
          	 chamada.Call_Offer_End_Date__c = acao.End_Date__c;
          	 atualizaChamada.add(chamada);
          	 idCallOffer.add(chamada.Id);
          }
          
        }
    }
    
    public void alteraGroupOffer(PVCommercial_Action__c acaoAnterior, PVCommercial_Action__c acao){
      
	      List<Group_Offer__c> listaGrupoOferta = [select id,Date_End_Offer__c,Date_Start_Offer__c from Group_Offer__c where id in:(idGrupoOferta)];
	      List<Group_Offer__c>  listGrupo = new List<Group_Offer__c> ();
	      
	      for(Group_Offer__c grupo:listaGrupoOferta){
	        if((grupo.Date_End_Offer__c == acaoAnterior.End_Date__c || grupo.Date_End_Offer__c > acao.End_Date__c)
	               && (grupo.Date_Start_Offer__c < acao.End_Date__c  )
	             ){
	             grupo.Date_End_Offer__c = acao.End_Date__c;
	             idGrupoAtualizado.add(grupo.Id);
	             atualizaGrupoOfertas.add(grupo);
	         
	        }
	      }
    }
    
    public void atualizaOferta(PVCommercial_Action__c acaoAnterior, PVCommercial_Action__c acao){
    	Date dat = System.today();
      List<Offer__c> lisOferta = [select Id,Offer_to_Internet__c,Name,Group_Offer__c,Status__c,Stage__c,Offer_Start_Date_Website__c, 
                                      Call_Offer__c,Offer_End_Date_Website__c,PV_OF_CheckUpdate__c from Offer__c
                                      where Group_Offer__c in:(idGrupoAtualizado) and Group_Offer__r.Date_End_Offer__c >: dat 
                                      and Offer_to_Internet__c = true and Call_Offer__c in:(idCallOffer)
                                  ];
      for(Offer__c ofert : lisOferta){
      
	      if(ofert.Offer_End_Date_Website__c >= acao.End_Date__c || ofert.Offer_Start_Date_Website__c  < acao.End_Date__c){
	      	//Offer_Start_Date_Website__c =
	      	ofert.Offer_End_Date_Website__c =  acao.End_Date__c;
	      	atualizaOferta.add(ofert);
	      }
      }    
    }
    
    public void atualizacaoEmMassa(){
      Database.update(atualizaChamada);
      Database.update(atualizaGrupoOfertas);
      Database.update(atualizaOferta);
      Database.update(atualizaChamada);
      
    }
    /*
    //
    //Novo jeito para atualizar a data da chamada de oferta
    //não alterar o codigo acima pois é usado em outras partes e funciona
    //Felipe Livino - Kolekto 28/11/2014
    */
    //atributos iniciais/ recebidos da trigger - pois iremos usar para realizar a comparação de datas 
    //atributos para serem carregados durante os métodos
    private List<PVCommercial_Action__c> listaUpdateComercialAction = new List<PVCommercial_Action__c>();
    private List<String> listaIdsComercialAction = new List<String>();
    private Map<Id,PVCommercial_Action__c> mapComercialActionAntigo = new Map<Id,PVCommercial_Action__c>();
    private Map<Id,PVCommercial_Action__c> mapComercialActionNovo = new Map<Id,PVCommercial_Action__c>();
    private List<String> listaIdGroupOffer = new List<String>();
    
    
    //construtor para verificar a alteração das datas - usar somente em after update
    public VFC155_UpdateDateCallOffer(List<PVCommercial_Action__c> listCommercialAction, Map<Id,PVCommercial_Action__c> mapACommercialAction){
    	for(PVCommercial_Action__c acaoComercial: listCommercialAction){
    		if(acaoComercial.End_Date__c >= mapACommercialAction.get(acaoComercial.id).End_Date__c){
    			listaUpdateComercialAction.add(acaoComercial);
    			listaIdsComercialAction.add(acaoComercial.Id);
    			mapComercialActionNovo.put(acaoComercial.id,acaoComercial);
    		}
    		else{
    			acaoComercial.addError('Atenção: A data de fim da ação comercial é inválida');
    			listaUpdateComercialAction.clear();
    			break;
    		}
    		
    	}
    	this.mapComercialActionAntigo = 	mapACommercialAction.clone();
    }
    
    public void updateDateCallOffer(){
    	if(!listaUpdateComercialAction.isEmpty() && listaUpdateComercialAction.size()>0){
	    	List<PVCall_Offer__c> listCallOffer = new List<PVCall_Offer__c>();
	    	for(PVCall_Offer__c chamadaOferta : [select id,Call_Offer_End_Date__c,Commercial_Action__c from PVCall_Offer__c where Commercial_Action__c in:listaIdsComercialAction]){
	    		if(chamadaOferta.Call_Offer_End_Date__c == mapComercialActionAntigo.get(chamadaOferta.Commercial_Action__c).End_Date__c){
	    			PVCall_Offer__c chamadaOffer = new PVCall_Offer__c(
	    				id = chamadaOferta.id,
	    				Call_Offer_End_Date__c = mapComercialActionNovo.get(chamadaOferta.Commercial_Action__c).End_Date__c
	    			);	
	    			listCallOffer.add(chamadaOffer);
	    		}
	    	}
	    	if(listCallOffer.size()>0 && !listaUpdateComercialAction.isEmpty())
	    		try{
	    			Database.update(listCallOffer);
	    		}catch(Exception e){System.debug('### Erro ao adicionar chamada de oferta');}
    	}
    } 
    
    public void updateDateOffer(){
    	if(listaUpdateComercialAction.size()>0 && !listaUpdateComercialAction.isEmpty()){
    		Set<Group_Offer__c> listGroupOffer = new Set<Group_Offer__c>();
	    	List<Offer__c> listOferta = new List<Offer__c>();
	    	for(Offer__c oferta : [select id,Group_Offer__c, Group_Offer__r.Date_End_Offer__c,Offer_End_Date_Website__c,Offer_to_Internet__c,Commercial_Action__c from Offer__c where 
	    									Commercial_Action__c in:listaIdsComercialAction]){
	    		if(oferta.Offer_to_Internet__c){
	    			if(oferta.Offer_End_Date_Website__c == mapComercialActionAntigo.get(oferta.Commercial_Action__c).End_Date__c){
	    				Offer__c ofert = new Offer__c();
	    				ofert.id = oferta.id;
	    				ofert.Offer_End_Date_Website__c = mapComercialActionNovo.get(oferta.Commercial_Action__c).End_Date__c;
	    				listOferta.add(ofert);
	    			}
	    		}
	    		if(oferta.Group_Offer__r.Date_End_Offer__c == mapComercialActionAntigo.get(oferta.Commercial_Action__c).End_Date__c){
	    			/////////////////-----------Grupo de Ofertas -------------------------------
	    			Group_Offer__c grupoOfertas = new Group_Offer__c();
	    			grupoOfertas.id =oferta.Group_Offer__c;
	    			grupoOfertas.Date_End_Offer__c = mapComercialActionNovo.get(oferta.Commercial_Action__c).End_Date__c;
	    			System.debug('##### grupoOfertas'+grupoOfertas);
	    			listGroupOffer.add(grupoOfertas);
	    			System.debug('##### grupoOfertas'+grupoOfertas);
	    		}
	    	}
	    	if(!listOferta.isEmpty() && listOferta.size()>0){
	    		System.debug('##### listOferta'+listOferta);
	    		update listOferta;
	    	}
	    	if(!listGroupOffer.isEmpty() && listGroupOffer.size()>0){
	    		System.debug('##### listGroupOffer'+listGroupOffer);
	    		List<Group_Offer__c> result = new List<Group_Offer__c>();
				for (Group_Offer__c s : listGroupOffer) {
					
						result.add(s);
					
				}
				System.debug('##### result'+result);
	    		update result;
	    	}
	    	
	    		
    	}								
    }
    
   
    
    
}