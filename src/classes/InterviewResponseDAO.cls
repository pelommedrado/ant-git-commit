public with sharing class InterviewResponseDAO {
	public static InterviewResponse__c getInterviewResById(Id InterviewResId){
		return [SELECT 	AccountId__c,AnswerDate__c,AnswerLink__c,ContactId__c,ContactChannel__c,
						CustomerExperience__c, CustomerReturnedRenaultRepairShop__c,
						CustumerGetInTouch__c, DealerGetInTouchAfterService__c,
						InterviewId__c, ReasonReturn__c, ReccomendDealerNote__c,
						TestDriveOffered__c, VehicleDeliveriedDateCombined__c, Origin__c
		        FROM InterviewResponse__c
		        WHERE Id = :interviewResId];
	}

	public static InterviewResponse__c getInterviewResById(String InterviewResId){
		return [SELECT 	AccountId__c,AnswerDate__c,AnswerLink__c,ContactId__c,ContactChannel__c,
						CustomerExperience__c, CustomerReturnedRenaultRepairShop__c,
						CustumerGetInTouch__c, DealerGetInTouchAfterService__c,
						InterviewId__c, ReasonReturn__c, ReccomendDealerNote__c,
						TestDriveOffered__c, VehicleDeliveriedDateCombined__c, Origin__c
		        FROM InterviewResponse__c
		        WHERE Id = :interviewResId];
	}

	public static List<InterviewResponse__c> getInterviewResById(Set<Id> InterviewResIdSet){
		return [SELECT 	AccountId__c,AnswerDate__c,AnswerLink__c,ContactId__c,ContactChannel__c,
						CustomerExperience__c, CustomerReturnedRenaultRepairShop__c,
						CustumerGetInTouch__c, DealerGetInTouchAfterService__c,
						InterviewId__c, ReasonReturn__c, ReccomendDealerNote__c,
						TestDriveOffered__c, VehicleDeliveriedDateCombined__c, Origin__c
		        FROM InterviewResponse__c
		        WHERE Id IN: InterviewResIdSet];
	}
}