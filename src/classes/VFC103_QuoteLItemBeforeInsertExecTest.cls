/**
    Date: 03/04/2013
    Purpose: Classe de teste relacionado a classe: VFC103_QuoteLItemBeforeInsertExecute. 
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC103_QuoteLItemBeforeInsertExecTest {

    static testMethod void myUnitTestBeforeInsertQuoteItem() {
        
        Opportunity opp = new Opportunity();
        Quote q = new Quote();
        PricebookEntry pBookEntry;
        
        PricebookEntry listPriceBookEntry = new PricebookEntry();    
        QuoteLineItem qLItem = new QuoteLineItem();
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;
                 
        // criar nova cotação
        q.Name = 'QuoteNameTest_1';
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb2.Id;
        insert q;

        // selecionar pricebookentry
        pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1]; 

        Test.startTest();
               
        try{
            // criar item de cotação
            QuoteLineItem qLineItem = new QuoteLineItem();
            qLineItem.QuoteId = q.Id;
            qLineItem.PricebookEntryId = pBookEntry.Id;
            qLineItem.Quantity = 1;
            qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
            insert qLineItem;
            system.debug('Insert QuoteLineItem is Ok! - Test is Ok!');
        }
        catch(Dmlexception e){
            system.debug('Insert failed - Test failed:  '+e.getMessage());
        }        
                 
        // selecionar pricebookentry
        listPriceBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode FROM PricebookEntry WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' AND
                                                                                              Pricebook2Id =: pb2.Id AND                                                                    
                                                                                              IsActive = true LIMIT 1];
        try{
             qLItem.QuoteId = q.Id;
             qLItem.PricebookEntryId = pBookEntry.Id;
             insert qLItem;
             system.debug('Insert QuoteLineItem is Ok! - Test failed');
        }
        catch(Dmlexception e){
			system.debug('Insert QuoteLineItem is not Ok! - failed - Test is Ok!:  '+e.getMessage()); 
        }
        
        Test.stopTest();
    } 
}