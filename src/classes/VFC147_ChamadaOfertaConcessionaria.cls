public class VFC147_ChamadaOfertaConcessionaria{ 
List<String> reg  = new List<String>(); 
List<String> css10  = new List<String>(); 
List<String> estadual  = new List<String>(); 
List<String> estado_adicionado  = new List<String>(); 
List<String> cidades_adicionadas  = new List<String>(); 
List<String> cidades_removidas  = new List<String>(); 
List<String> regiao_removida  = new List<String>(); 
List<String> cidades10  = new List<String>(); 
List<SelectOption> todas_reg = new List<SelectOption>(); 
List<SelectOption> regiao2 = new List<SelectOption>(); 
List<SelectOption> estados1 = new List<SelectOption>(); 
List<SelectOption> cidades1 = new List<SelectOption>(); 
List<SelectOption> todosEstados = new List<SelectOption>(); 
List<SelectOption> todasCidades = new List<SelectOption>(); 
List<SelectOption> conce1 = new List<SelectOption>(); 
List<SelectOption> todasCss = new List<SelectOption>(); 
Set<SelectOption> todosEstadosAux = new Set<SelectOption>(); 
List<Account> cssInit = new List<Account>(); 
private ApexPages.StandardSetController controller; 
private final PVCall_Offer__c callOffer; 

public VFC147_ChamadaOfertaConcessionaria(ApexPages.StandardController controller) { 
    String idChamada = ApexPages.currentPage().getParameters().get('id');
        callOffer = [SELECT Id, Name, Commercial_Action__c, PV_CH_checkStatus__c FROM PVCall_Offer__c 
            WHERE Id = :idChamada];
        init(callOffer.Id); 
} 
public void init(String chamadaOferta){ 
    cssInit.addAll([Select id, Name, NameZone__c, ShippingState, ShippingCity   From  Account where Active_PV__c = true and  Id in (select Concessionaria__c from Dealer_Call_Offer__c where Call_Offer__c =: chamadaOferta)]);
    Set<SelectOption> regioes = new Set<SelectOption>(); 
    Set<SelectOption> estados = new Set<SelectOption>(); 
    Set<SelectOption> cidades = new Set<SelectOption>(); 
    Set<SelectOption> concessionarias = new Set<SelectOption>();
    for (Account a : cssInit){ 
        if (a.NameZone__c != NULL && !regioes.contains(new SelectOption(a.NameZone__c,a.NameZone__c))){ 
            regioes.add(new SelectOption(a.NameZone__c,a.NameZone__c)); 
        }
        if (!estados.contains(new SelectOption(a.ShippingState,a.ShippingState))){ 
            estados.add(new SelectOption(a.ShippingState,a.ShippingState)); 
        } 
        if(!cidades.contains(new SelectOption(a.ShippingCity,a.ShippingCity))){ 
            cidades.add( new SelectOption(a.ShippingCity,a.ShippingCity)); 
        } 
        if(!concessionarias.contains(new SelectOption(a.Name,a.Name))){ 
            concessionarias.add(new SelectOption(a.Name,a.Name)); 
        } 
    } 
    
    todas_reg.addAll(regioes);
    todosEstados.addAll(estados); 
    todasCidades.addAll(cidades); 
    todasCss.addAll(concessionarias);
    RegionalNotAdded();
    StatesNotAdded();
    CityNotAdded();
    DeallerNotAdded();
    
    todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort();
} 

//listas para retornar os valores ap?s preenchido
List<String> regional3 = new List<String>();
List<String> estado3 = new List<String>();
List<String> cidade3 = new List<String>();
//get para as lista acima
public List<String> getRegional3(){return regional3;}
public List<String> getEstado3(){return estado3;}
public List<String> getCidade3(){return cidade3;}
//regionais n?o adicionados
public void RegionalNotAdded(){
  
  Set<String> conc = new Set<String>();
  List<Account> csst = todas_css();
  for(Account a : csst){
    if(a.NameZone__c != NULL)
      conc.add(a.NameZone__c);
  }
  for(String a:conc)
    for (SelectOption x : todas_reg)
      if(a.equals(x.getValue())){
        regional3.add(a);
        conc.remove(a);
      } 
  for(String b:conc)
    regiao2.add(new SelectOption(b,b));
}
//estados n?o adicionados
public void StatesNotAdded(){
  Set<String> conc = new Set<String>();
  List<Account> csst = todas_css();
  for(Account a : csst)
   for(String b : getRegional3())
     if(a.NameZone__c != NULL && a.NameZone__c.equals(b))
       conc.add(a.ShippingState);
  for(String a:conc)
  for (SelectOption x : todosEstados)
     if(a.equals(x.getValue())){
        estado3.add(a);
        conc.remove(a);
     }
  estadual.addAll(conc);
}
//cidades n?o adicionadas
  public void CityNotAdded(){
  Set<String> conc = new Set<String>();
  List<Account> csst = todas_css();
  for(Account a : csst)
   for(String b : getEstado3())
     if(a.ShippingState.equals(b))
       conc.add(a.ShippingCity);
  for(String a:conc)
  for (SelectOption x : todasCidades)
     if(a.equals(x.getValue())){
        cidade3.add(a);
        conc.remove(a);
     }
  cidades_removidas.addAll(conc);
}
//concession?rias n?o adicionadas
public void DeallerNotAdded(){
  Set<String> conc = new Set<String>();
  List<Account> csst = todas_css();
  for(Account a : csst)
   for(String b : getCidade3())
     if(a.ShippingCity.equals(b))
       conc.add(a.Name);
  for(String a:conc)
  for (SelectOption x : todasCss)
     if(a.equals(x.getValue())){
        conc.remove(a);
     }
   cidades10.addAll(conc);
}
public VFC147_ChamadaOfertaConcessionaria(){}

public void relacionarTodasCss(String idCallOffer){
   List<Account> css = [Select id From Account where Active_PV__c = true and  Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH'];
    if(css != NULL){
      List<Dealer_Call_Offer__c> relacao = new List<Dealer_Call_Offer__c>();   
      for( Account concessionaria : css){
        Dealer_Call_Offer__c dealer = new Dealer_Call_Offer__c();
        dealer.Call_Offer__c = idCallOffer;
        dealer.Concessionaria__c = concessionaria.Id;
        relacao.add(dealer);
      }
      insert relacao;
    }
       
}
public void relacionarTodasCssForTrigger(List<PVCall_Offer__c> listaChamada){
   List<Account> css = [Select id From Account where Active_PV__c = true and  Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH'];
    if(css != NULL){
      List<Dealer_Call_Offer__c> relacao = new List<Dealer_Call_Offer__c>();   
      for(PVCall_Offer__c idCallOffer : listaChamada)
        for( Account concessionaria : css){
            Dealer_Call_Offer__c dealer = new Dealer_Call_Offer__c();
            dealer.Call_Offer__c = idCallOffer.id;
            dealer.Concessionaria__c = concessionaria.Id;
            relacao.add(dealer);
      	}
      insert relacao;
    }
       
}

public void deletarCss(List<Dealer_Call_Offer__c> lista){
  List<Dealer_Call_Offer__c> dealer = [select Call_Offer__c, Concessionaria__c from Dealer_Call_Offer__c where Call_Offer__c =:callOffer.Id];
  List<Dealer_Call_Offer__c> listaDeletar = new List<Dealer_Call_Offer__c>();
  boolean aux;
  for(Dealer_Call_Offer__c b : dealer){
     aux = true;
     for(Dealer_Call_Offer__c a : lista){   
       if(a.Id == b.Id)
       aux=false;
     }
     if(aux)
       listaDeletar.add(b);
     
  }
  delete listaDeletar;
  ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Concessionárias atualizadas com sucesso!');
  ApexPages.addMessage(msg); 
  
}

public PVCall_Offer__c getCallOffer() { 
        return callOffer; 
} 
public VFC147_ChamadaOfertaConcessionaria(PVCall_Offer__c co){ 
    this.callOffer = co; 
} 
public PageReference salvar(){ 
  
  if(!callOffer.PV_CH_checkStatus__c){
    String idOferta = calloffer.id; 
    Set<String> ids = new Set<String>(); 
    list<Account>  css = todas_Css(); 
    List<Dealer_Call_Offer__c> dealer = new List<Dealer_Call_Offer__c>();
    
    for (Account c : css){
       for(SelectOption conce : todasCss){ 
            if((c.name != null) && (conce.getValue().equals(c.Name))){ 
               if (!ids.contains(c.id)){
                 Dealer_Call_Offer__c dealer2 = new Dealer_Call_Offer__c();
                 dealer2.Concessionaria__c = c.Id;
                 dealer2.Call_Offer__c = idOferta;
                 dealer.add(dealer2);
                 ids.add(c.id);
               } 
            }
        } 
    } 
    deletarCss(dealer);
    try{
      insert dealer;
      ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Concessionárias atualizadas com sucesso!');
      ApexPages.addMessage(msg); 
    }catch(Exception e){}  
    ids.clear(); 
  }
  return null; 
}

public PageReference cancelar(){ 
    cidades_removidas.clear(); 
    estadual.clear(); 
    cidades10.clear(); 
    todosEstados.clear(); 
    todasCidades.clear(); 
    todasCss.clear(); 
    reg.clear(); 
    todas_reg.clear(); 
    return null; 
} 

/*-----------------------------------------------------gets & sets------------------------------------------------------------------*/

public List<SelectOption> getRegiao2(){
  return regiao2;
}
public void setRegiao2(SelectOption regiao2){
  this.regiao2.add(regiao2);
}
public List<SelectOption> getTodas_reg(){
  return todas_reg;
}
public void setTodas_reg(SelectOption todas_reg){
  this.todas_reg.add(todas_reg);
}

public List<SelectOption> getCid_removidas(){ 
    List<SelectOption> aux = new List<SelectOption>(); 
    for (String x:cidades10){ 
        aux.add(new SelectOption(x,x)); 
    } 
    return aux; 
} 
public List<String> getCss10(){ 
    return css10; 
} 
public void setCss10(List<String> novo){ 
    this.css10=novo; 
} 
public List<SelectOption> getTodas2_Css(){ 
    return todasCss; 
} 
public List<SelectOption> getTodas_Cidades(){ 
    return todasCidades; 
} 
public List<SelectOption> getTodosEstados(){ 
    return todosEstados; 
} 
public void setTodosEstados(List<SelectOption> entrada){ 
    this.todosEstados = entrada; 
}
public List<String> getRegiao_removida(){ 
    return regiao_removida; 
} 
public void setRegiao_removida(List<String> novo){ 
    regiao_removida.clear(); 
    this.regiao_removida = novo; 
} 
public List<String> getEstado_adicionado(){ 
    return estado_adicionado; 
} 
public void setEstado_adicionado(List<String> novo){ 
    this.estado_adicionado = novo; 
} 
public List<String> getCidades_adicionadas(){ 
    return cidades_adicionadas; 
} 
public void setCidades_adicionadas(List<String> novo){ 
    this.cidades_adicionadas = novo; 
} 
public List<String> getCidades_removidas(){ 
    return cidades_removidas; 
} 
public void setCidades_removidas(List<String> novo){ 
    this.cidades_removidas.addAll(novo); 
} 
public List<String> getEstadual(){ 
    return estadual; 
} 
public void setEstadual(List<String> novo){ 
    this.estadual.addAll(novo); 
} 
public List<String> getCidades10(){ 
    return cidades10; 
} 
public void setCidades10(List<String> novo){ 
        cidades10.addAll(novo); 
} 
public String reg2 {get; set;}
 
public List<String> getReg(){ 
    return reg; 
} 
public void setReg(List<String> novo){ 
    this.reg = novo; 
}

/*-----------------------------------------------------gets & sets------------------------------------------------------------------*/


public list<Account> todas_Css(){ 
       list<Account> css = [Select id, Name, NameZone__c, ShippingState, ShippingCity From  Account where Active_PV__c = true and Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH' ]; 
        return css; 
}

//Carrega os estados do lado esquerdo para serem adicionados
public List<SelectOption> getAllest(){ 
    List<SelectOption> aux = new List<SelectOption>(); 
    for (String a : estadual){ 
        aux.add(new SelectOption(a,a)); 
    } 
    return aux; 
}
 
//Carrega as cidades do lado esquerdo para serem adicionados 
public List<SelectOption> getAllCidade(){ 
    List<SelectOption> aux = new List<SelectOption>(); 
    for (String a : cidades_removidas) { 
        aux.add(new SelectOption(a,a)); 
    } 
    return aux; 
}

//adiciona regional  
public PageReference addRegional() {
        cidades10.clear();      //limpa op??es de concessionarias selecionadas 
        cidades_removidas.clear();  //limpa op??es de cidades selecionadas
        todosEstados.clear();     //limpa op??es de estados selecionados
        todasCss.clear();       //limpa selectionOptions de concessionarias
        list<Account>  css = todas_Css(); 
        todasCidades.clear();
        
        //remove a regional
        for(String a : reg){ 
            for(integer i = 0; i < regiao2.size();i++){
                if (regiao2.get(i).getValue().equals(a)){
                  todas_reg.add(new SelectOption (a,a));
                  regiao2.remove(i);    
                } 
            } 
        }
        set<String> estado = new Set<String>();
        set<String> cidade = new Set<String>();
        set<String> conc = new Set<String>();
        for(SelectOption re:todas_reg){
            for(Account c:css){
              if ((c.NameZone__c != null) && (c.NameZone__c.equals(re.getValue()) && (c.ShippingState != null))){
                 estado.add(c.ShippingState);
                 if ((c.ShippingCity != null)){
                     cidade.add(c.ShippingCity);
                     conc.add(c.Name);
                  } 
              }
              
            }
        }
        for(String a:estado)
              todosEstados.add(new SelectOption(a,a));
        for(String b:cidade)      
              todasCidades.add(new SelectOption(b,b));
        for(String c:conc)
              todasCss.add(new SelectOption(c,c));      
    
        todas_reg.sort();
        regiao2.sort();
        estados1.sort();
        cidades1.sort();
        todosEstados.sort();
        todasCidades.sort();
        conce1.sort();
        todasCss.sort();
        return null;
}

//remove regional, estado, cidade e concessionaria 
public PageReference remRegional() {
  estadual.clear();
    cidades10.clear();      //limpa op??es de concessionarias selecionadas 
        cidades_removidas.clear();  //limpa op??es de cidades selecionadas
        todosEstados.clear();     //limpa op??es de estados selecionados
        todasCss.clear();       //limpa selectionOptions de concessionarias
        list<Account>  css = todas_Css(); 
        todasCidades.clear(); 
        
        //remove a regional
        for(String a : regiao_removida){ 
            for(integer i = 0; i < todas_reg.size();i++){ 
                if (todas_reg.get(i).getValue().equals(a)){
                  regiao2.add(todas_reg.get(i)); 
                    todas_reg.remove(i);
                } 
            } 
        }
        //carrega apenas o estado com rela??o ao estado que n?o foi removido
        for (SelectOption x : todas_reg){ 
            Set<String> estado = new Set<String>(); 
            for (Account c : css){ 
                if ((c.NameZone__c != null) && (c.NameZone__c.equals(x.getValue()))){ 
                    if (!estado.contains(c.ShippingState) && (c.ShippingState != null)){ 
                        estado.add(c.ShippingState); 
                    } 
                } 
            }
          for (String c : estado){ 
                  todosEstados.add(new SelectOption(c,c)); 
          }
         
        } 
        //carrega apenas a cidade com rela??o ao estado n?o removido
        for (SelectOption x : todosEstados){ 
            Set<String> cidade = new Set<String>(); 
            for (Account c : css){ 
                if ((c.ShippingState != null) && (c.ShippingState.equals(x.getValue()))){ 
                    if (!cidade.contains(c.ShippingCity) && (c.ShippingCity != null)){ 
                        cidade.add(c.ShippingCity); 
                    } 
                } 
            }
          for (String c : cidade){ 
                  todasCidades.add(new SelectOption(c,c)); 
          }
         
        }
        //carrega apenas a concessionaria com rela??o a cidade n?o removida
        for (SelectOption item: todasCidades){ 
          Set<String> conce = new Set<String>(); 
          for (Account c : css){ 
              if ((c.ShippingCity != null) && (c.ShippingCity.equals(item.getValue()))){ 
                  if (!conce.contains(c.name) && (c.ShippingCity != null)){ 
                      conce.add(c.name); 
                  } 
              } 
          } 
          for (String k : conce){ 
              todasCss.add(new SelectOption(k,k)); 
          } 
        }
        todas_reg.sort();
        regiao2.sort();
        estados1.sort();
        cidades1.sort();
        todosEstados.sort();
        todasCidades.sort();
        conce1.sort();
        todasCss.sort();
        return null; 
}

//remove estados, cidades e concessionarias 
public PageReference removeEstados() { 
        cidades10.clear(); 
        cidades_removidas.clear(); 
        todasCss.clear();
        list<Account>  css = todas_Css(); 
        todasCidades.clear(); 
        for(String a : estadual){ 
            for(integer i = 0; i < todosEstados.size();i++){ 
                if (todosEstados.get(i).getValue().equals(a)){ 
                    todosEstados.remove(i); 
                } 
            } 
        } 
        //carrega apenas a cidade com rela??o ao estado n?o removido
        for (SelectOption x : todosEstados){ 
            Set<String> cidade = new Set<String>(); 
            for (Account c : css){ 
                if ((c.ShippingState != null) && (c.ShippingState.equals(x.getValue()))){ 
                    if (!cidade.contains(c.ShippingCity) && (c.ShippingCity != null)){ 
                        cidade.add(c.ShippingCity); 
                    } 
                } 
            }
          for (String c : cidade){ 
                  todasCidades.add(new SelectOption(c,c)); 
          }
         
        }
        //carrega apenas a concessionaria com rela??o a cidade n?o removida
        for (SelectOption item: todasCidades){ 
          Set<String> conce = new Set<String>(); 
          for (Account c : css){ 
              if ((c.ShippingCity != null) && (c.ShippingCity.equals(item.getValue()))){ 
                  if (!conce.contains(c.name) && (c.ShippingCity != null)){ 
                      conce.add(c.name); 
                  } 
              } 
          } 
          for (String k : conce){ 
              todasCss.add(new SelectOption(k,k)); 
          } 
      }
    todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort();
        return null; 
}

//adiciona estados 
public PageReference removeEstados2() { 
        List<Account> conta = todas_Css();
        Set<String> conc = new Set<String>();
        Set<String> cidade = new Set<String>();
        for (String x: estado_adicionado){ 
            for (integer i = 0; i < estadual.size();i++){ 
                if (estadual.get(i).equals(x)){ 
                    estadual.remove(i); 
                    todosEstados.add(new SelectOption(x,x)); 
                } 
            }
            for(Account a : conta){
              if(a.ShippingState.equals(x)){
                 cidade.add(a.ShippingCity);
                 conc.add(a.Name);
              }
            }  
        } 
        for(String a:cidade)
          todasCidades.add(new SelectOption(a,a));
        for(String b:conc)
          todasCss.add(new SelectOption(b,b));
      todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort(); 
        return null; 
} 

//remove cidade e concessionaria
public PageReference remover_cidades(){ 
    cidades10.clear(); 
    list<Account>  css = todas_Css(); 
    todasCss.clear(); 
    for (String a : cidades_removidas){ 
         for (integer i = 0; i < todasCidades.size();i++){ 
              if (todasCidades.get(i).getValue().equals(a)){ 
                  todasCidades.remove(i); 
              } 
         } 
    
    }
    //carrega apenas a concessionaria com rela??o a cidade n?o removida
    for (SelectOption item: todasCidades){ 
        Set<String> conce = new Set<String>(); 
        for (Account c : css){ 
            if ((c.ShippingCity != null) && (c.ShippingCity.equals(item.getValue()))){ 
                if (!conce.contains(c.name) && (c.ShippingCity != null)){ 
                    conce.add(c.name); 
                } 
            } 
        } 
        for (String k : conce){ 
            todasCss.add(new SelectOption(k,k)); 
        } 
    } 
        todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort();
    return null; 
}

//adiciona cidade 
public PageReference cidade_adicionada(){ 
   List<Account> conta = todas_Css();
    for (String x: cidades_adicionadas){ 
            for (integer i = 0; i < cidades_removidas.size();i++){ 
                if (cidades_removidas.get(i).equals(x)){  
                    cidades_removidas.remove(i); 
                    todasCidades.add(new SelectOption(x,x)); 
                } 
            }
            for(Account a : conta){
              if(a.ShippingCity.equals(x)){
                  todasCss.add(new SelectOption(a.Name,a.Name));
              }
            } 
     } 
          todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort(); 
     return null; 
} 

//adiciona concessionaria 
public PageReference remover_css(){ 
 
    for (String a:cidades10){ 
        for (integer i=0; i < todasCss.size();i++){ 
            if (todasCss.get(i).getValue().equals(a)){ 
                todasCss.remove(i); 
                
            } 
        } 
    } 
        todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort();
    return null; 
}

//adiciona concessionaria 
public PageReference adicionar_css(){ 
    for (String a:css10){ 
        todasCss.add(new SelectOption(a,a)); 
        for (integer i = 0; i < cidades10.size();i++){ 
            if (cidades10.get(i).equals(a)){ 
                cidades10.remove(i);  
            } 
        }  
    } 
    
    todas_reg.sort();
    regiao2.sort();
    estados1.sort();
    cidades1.sort();
    todosEstados.sort();
    todasCidades.sort();
    conce1.sort();
    todasCss.sort();
    return null; 
} 

}