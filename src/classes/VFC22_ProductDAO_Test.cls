/**
	Class   -   VFC22_ProductDAO_Test
    Author  -   RameshPrabu
    Date    -   30/10/2012
    
    #01 <RameshPrabu> <30/10/2012>
        Created this class using test for VFC22_ProductDAO.
**/
@isTest 
private class VFC22_ProductDAO_Test {

    static testMethod void VFC22_ProductDAO_Test() {
        // TO DO: implement unit test
        List<Product2> lstProducts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
        Test.startTest();
        List<Product2> Product = VFC22_ProductDAO.getInstance().findProductRecordsbyName(lstProducts[0].Name);
        system.assert(Product.size()!=0);
        VFC22_ProductDAO.getInstance().fetchAllProducts();
        VFC22_ProductDAO.getInstance().fetchAll_Model_Products();
        VFC22_ProductDAO.getInstance().fetchAll_Model_Id_In_Map();
        VFC22_ProductDAO.getInstance().fetchAll_Version_Products();
        VFC22_ProductDAO.getInstance().fetch_All_Versions_Map();
        VFC22_ProductDAO.getInstance().fetch_All_Versions_Map_with_ProductCode();     
        Test.stopTest();
        System.assert(Product.size() > 0);
    }

    static testMethod void test1(){

        Product2 prd = new Product2(Name='SANDERO');
        Insert prd;

        Test.startTest();

        VFC22_ProductDAO.getInstance().fetch_All_Version_Product('FLM');
        VFC22_ProductDAO.getInstance().get_Accessory_Products();
        VFC22_ProductDAO.getInstance().fetchProductsByRecordType();
        VFC22_ProductDAO.getInstance().fetchProductsByModel('SANDERO');
        VFC22_ProductDAO.getInstance().fetchProductsByVersion('Authentic');
        VFC22_ProductDAO.getInstance().findById(prd.Id);
        VFC22_ProductDAO.getInstance().fetchActiveModelsByRecordTypeDevName();

        Test.stopTest();
    }
}