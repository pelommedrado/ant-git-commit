/**
* Classe que representa os dados da página de confirmação de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC77_TestDriveConfirmationVO 
{
	/*atributos utilizados somente para controle interno, não são exibidos na tela 
	 (usados para guardar variáveis para posterior utilização, retornar listas de outros objetos)*/
	public String id {get;set;}
	public String opportunityId {get;set;}
	public String dealerId {get;set;}
	public List<TDV_TestDrive__c> lstSObjTestDriveAgenda {get;set;}
	public List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicleAvailable{get;set;}
	
	/*atributos referentes a seção informações do test drive*/
	public String name {get;set;}
	public String sellerName {get;set;}
	public String opportunityName {get;set;}
	public String status {get;set;}
	public String vehicleInterest {get;set;}
	
	/*atributos referentes a seção informações do cliente*/
	public String customerName {get;set;}
	public String currentVehicle {get;set;}
	public String persLandline {get;set;}
	public Integer yearCurrentVehicle {get;set;}
	public String persMobPhone {get;set;}
	
	/*atributos referentes a seção agenda de veículos*/
	public String testDriveVehicleId {get;set;}
	public String plaque {get;set;}
	public String model {get;set;}
	public String version {get;set;}
	public String color {get;set;}
	public DateTime dateBooking {get;set;}
	public String dateBookingFmt {get;set;}
	public Date dateBookingNavigation {get;set;}
	public String dateBookingNavigationFmt {get;set;}
	public String selectedTime {get;set;}
	public String vehicleScheduled {get;set;}
		
	/*picklists que serão exibidos na tela*/
	public List<SelectOption> lstSelOptionInterestVehicle {get;set;}
	public List<SelectOption> lstSelOptionVehiclesAvailable {get;set;}
	public List<SelectOption> lstSelOptionReasonCancellation {get;set;}
	
	/*atributo referente ao motivo de cancelamento do test drive exibido no pop-up*/
	public String reasonCancellation {get;set;}
			
	/*matriz que contém a agenda do veículo (foi criado uma lista de listas para poder exibir a agenda na tela em formato tabular - matriz)*/
	public List<List<VFC64_ScheduleTestDriveVO>> lstAgendaVehicle {get;set;}
				
	public VFC77_TestDriveConfirmationVO()
	{
		
	}
	
}