/**
*	Class		-   VFC63_Item_DAO
*   Author 		-   Suresh Babu
*   Date   		-   12/03/2013
*   Description	-	DAO class for Item object to query records.
*/

public class VFC63_Item_DAO extends VFC01_SObjectDAO{
	private static final VFC63_Item_DAO instance = new VFC63_Item_DAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC63_Item_DAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC63_Item_DAO getInstance(){
        return instance;
    }
    
    /**
    	Method used to fetch all Item records.
    **/
    public List<Item__c> fetchAll_Item_Records(){
    	List<Item__c> lstAllItems = new List<Item__c>();
    	
    	lstAllItems = [SELECT 
    						Name, Key__c, Label__c, Id 
    					FROM 
    						Item__c 
    					WHERE 
    						Name != null
    					];
    	return lstAllItems;
    }
    
    /**
    	Method used to store values in Map of Items with Name.
    **/
    public Map<String, Item__c> fetch_Items_Map_Name(){
    	Map<String, Item__c> map_allItems = new Map<String, Item__c>();
    	List<Item__c> lstAllItems = new List<Item__c>();
    	
    	lstAllItems = [SELECT 
    						Name, Key__c, Label__c, Id 
    					FROM 
    						Item__c 
    					WHERE 
    						Name != null
    					];
    	for( Item__c item : lstAllItems ){
    		map_allItems.put( item.Name, item );
    	}
    	return map_allItems;
    }
    
    /**
    	Method to get Color Records from Item object..
    **/
    public Map<String, Item__c> fetch_Color_Items_Map_Name(){
    	Id color_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Color' );
    	Map<String, Item__c> map_allItems = new Map<String, Item__c>();
    	List<Item__c> lstAllItems = new List<Item__c>();
    	
    	lstAllItems = [SELECT 
    						Name, Key__c, Label__c, Id 
    					FROM 
    						Item__c 
    					WHERE 
    						RecordTypeId =: color_RecordTypeId
    					];
    	for( Item__c item : lstAllItems ){
    		map_allItems.put( item.Name, item );
    	}
    	return map_allItems;
    }
    
    /**
    	Method to get all Option records form Item object..
    **/
    public Map<String, Item__c> fetch_Option_Items_Map_Name(){
    	Id option_RecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Option' );
    	Map<String, Item__c> map_allItems = new Map<String, Item__c>();
    	List<Item__c> lstAllItems = new List<Item__c>();
    	
    	lstAllItems = [SELECT 
    						Name, Key__c, Label__c, Id 
    					FROM 
    						Item__c 
    					WHERE 
    						RecordTypeId =: option_RecordTypeId
    					];
    	for( Item__c item : lstAllItems ){
    		map_allItems.put( item.Name, item );
    	}
    	return map_allItems;
    }
}