/**
* Classe que representa exceções geradas na atualização de Oportunidade.
* @author Christian Ranha.
*/
public with sharing class VFC91_UpdateOpportunityException extends Exception {

	public VFC91_UpdateOpportunityException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}