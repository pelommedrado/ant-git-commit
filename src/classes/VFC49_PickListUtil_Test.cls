@isTest
private class VFC49_PickListUtil_Test {

    static testMethod void testVFC49_PickListUtil() {
    	Set<String> value = new Set<String>();
    	value.add('SRC');
        VFC49_PickListUtil.buildPickList(Account.AccSubSource__c);
        VFC49_PickListUtil.buildPickList(Account.AccSubSource__c, value);
        VFC49_PickListUtil.buildPickList(Account.AccSubSource__c,'SRC2');
        
    }
}