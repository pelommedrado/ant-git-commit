/*  Mock of synchronization 
*************************************************************************************************************
25 Aug 2014 : Creation. Author : Donatien Veron (AtoS)
*************************************************************************************************************/
@IsTest 
global class Myr_Synchro_ATC_MK implements HttpCalloutMock {
	
	global enum MODE {MODE_CUST_KO, MODE_CUST_OK_VIN_KO, MODE_CUST_OK_VIN_OK, PAGE_VIN_OK, GET_CSDB}
	MODE mMode;
	
	global Myr_Synchro_ATC_MK(MODE iMode) {
		mMode = iMode;
	}
	
	CS04_MYR_Settings__c setting = CS04_MYR_Settings__c.getInstance();
	

	global HTTPResponse respond(HTTPRequest req) {
    	HttpResponse res;
    	if( mMode == MODE.MODE_CUST_OK_VIN_OK) {
    		if(req.getEndPoint().equalsIgnoreCase('/vin/')){
	        	res = new HttpResponse();
	        	res.setHeader('Content-Type', 'application/json');
	        	res.setBody('<status><value>200</value><responseCode>200</responseCode><responseDiag>Well done</responseDiag></status>');
	        	res.setStatusCode(200);
    		}else{
    			res = new HttpResponse();
	        	res.setHeader('Content-Type', 'application/json');
	        	res.setBody('<status><value>200</value><responseCode>200</responseCode><responseDiag>Well done</responseDiag></status>');
	        	res.setStatusCode(200);
    		}
    	}
    	
    	if( mMode == MODE.MODE_CUST_OK_VIN_KO) {
    		if(req.getEndPoint().containsIgnoreCase('/vin/')){
	        	res = new HttpResponse();
	        	res.setHeader('Content-Type', 'application/json');
	        	res.setBody('<status><value>400</value><responseCode>400</responseCode><responseDiag>call of VIN KO</responseDiag></status>');
	        	res.setStatusCode(400);
    		}else{
    			res = new HttpResponse();
	        	res.setHeader('Content-Type', 'application/json');
	        	res.setBody('<status><value>200</value><responseCode>200</responseCode><responseDiag>Well done</responseDiag></status>');
	        	res.setStatusCode(200);
    		}
    	}
    	
    	if( mMode == MODE.PAGE_VIN_OK) {
        	res = new HttpResponse();
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('<status><value>0</value><responseCode>0</responseCode><responseDiag>VIN OK</responseDiag></status>');
        	res.setStatusCode(0);
    	}
    	
    	if( mMode == MODE.GET_CSDB) {
        	res = new HttpResponse();
        	res.setHeader('Content-Type', 'application/json');
        	res.setBody('<?xml version="1.0" encoding="UTF-8"?><customerVehicles><customer><address>303f79f528af997019f436053c905394d214f91f9f7b728518a7b0d7e1c70bf5</address><addressType>INDIVIDUAL</addressType><city>c468212ddf8d851136ece324c5bd02d6e03e59a55d375d1bdbcf2461a8ff874c</city><civility /><countryIsoCode>GB</countryIsoCode><email>b1d31eb895a29695f9aac6efe743aa17aca9d799f47f9c6cc79a4d816d1e3edd</email><firstName>85debe77d106863639fe02e7ac9b6e527705cb3b5b037de20e95501019bc05ad</firstName><lastName>5c19cedcddeda21f25ffcfc06d5710cde6baed4f7c30f683e7cc7ee3fca711a4</lastName><optInATCProfile>00000</optInATCProfile><portalCountryCode>GB</portalCountryCode><zipCode>e846a3867ee2598139c1957e15fafe06f2357059c177aaa944cb5769bf1a5ae1</zipCode></customer><vehicles><vin>111</vin><vin>111</vin><vin>111</vin></vehicles><status><responseCode>0</responseCode><responseDiag>CSDB-0000 : Success</responseDiag><value>OK</value></status></customerVehicles>');
        	res.setStatusCode(0);
    	}
    	
    	return res;
    }
}