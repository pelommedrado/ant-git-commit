/**
	Class   -   VFC25_UserDAO_Test
    Author  -   Suresh Babu
    Date    -   29/10/2012
    
    #01 <Suresh Babu> <29/10/2012>
        Created this class using test for VFC25_UserDAO.
**/
@isTest 
private class VFC25_UserDAO_Test
{
	
	static testMethod void VFC25_UserDAO_Test()
	{
		// Prepare test data
		User user = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertUserObjects();
		
		// Start test
		Test.startTest();
        
        // Test01
        List<User> userRecords = VFC25_UserDAO.getInstance().fetchAllUserRecords();
        
        // Test02
        User userRecord =  VFC25_UserDAO.getInstance().fetchUserByRecordId(userRecords[0].Id) ;

		// Test03
		List<Account> lstDealerRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        List<Contact> lstContact = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToContact(lstDealerRecords);
        List<Id> contactIds = New List<Id>();
        for(Contact con :  lstContact){
        	contactIds.add(con.Id);
        }
        List<User> userRecordsContact = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId(contactIds);

        // Test04
        List<Id> lstContactIds = new List<Id>();
        Boolean DVE_Seller = false;
        List<User> test04 = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId(lstContactIds, DVE_Seller);
        
        // Stop test
        Test.stopTest();

        // Validade data
        system.assert( userRecords.size() > 1 );
        
        
        
	}
}