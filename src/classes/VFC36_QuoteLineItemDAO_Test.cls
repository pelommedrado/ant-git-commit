/**
	Class   -   VFC36_QuoteLineItemDAO_Test
    Author  -   Suresh Babu
    Date    -   22/01/2013
    
    #01 <Suresh Babu> <22/01/2013>
        Created this class using test for VFC36_QuoteLineItemDAO.
**/
@isTest(SeeAllData=true)
private class VFC36_QuoteLineItemDAO_Test {
    
    static Quote quote;
    static Opportunity opportunity1;
    
    static{
        
        Id recordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
        
        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.RgStateTexto__c = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone = '1133334444';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.Sex__c = 'Man';
        sObjAccPersonal.MaritalStatus__c = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate = System.today();
        insert sObjAccPersonal;
        
        opportunity1 = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            AccountId = sObjAccPersonal.Id);
        insert opportunity1;
        
        VFC61_QuoteDetailsVO orc = 
            Sfa2Utils.criarOrcamento(
                opportunity1.Id, 
                null);
        
        quote = [
            SELECT ID
            FROM Quote
            WHERE ID =: orc.Id
        ];
    }

    static testMethod void QuoteLinItem_Test1() {
    	// insert Records for QuoteLineItem
        
        Test.startTest();
        
    	//List<Quote> lstQuoteRecords = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToQuote();
        
        List<Quote> lstQuoteRecords = new List<Quote>{quote};
    	
    	Set<Id> setQuoteIDs = new Set<Id>();
        Set<String> setQuoteStrings = new Set<String>();
        
        String quoteId = '';
        
    	for( Quote quote_Record : lstQuoteRecords ){
    		setQuoteIDs.add( quote_Record.Id );
            setQuoteStrings.add( quote_Record.Id );
            quoteId = quote_Record.Id;
    	}
        //List<QuoteLineItem> fetchQuoteLineItems_UsingQuoteIds( Set<Id> setQuoteIds)
        List<QuoteLineItem> lstQuoteLineItems = VFC36_QuoteLineItemDAO.getInstance().fetchQuoteLineItems_UsingQuoteIds(setQuoteIDs);
        system.Debug(lstQuoteLineItems);
        
        List<QuoteLineItem> qliLst = VFC36_QuoteLineItemDAO.getInstance().fetchQuoteLineItemByQuoteId('', setQuoteStrings);
        
        List<QuoteLineItem> qliLst2 = VFC36_QuoteLineItemDAO.getInstance().getQuoteLineItemByQuoteId(quoteId, setQuoteStrings);
        
        VFC36_QuoteLineItemDAO.getInstance().getQuoteLineItemById(quoteId);
        
        Test.stopTest();
    }
}