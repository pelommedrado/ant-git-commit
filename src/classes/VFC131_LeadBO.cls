public with sharing class VFC131_LeadBO {
    
    private static final VFC131_LeadBO instance = new VFC131_LeadBO();
    public Id accountRecTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
    public Id leadVendasPF = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
    public Id DVR = Utils.getRecordTypeId('Lead','DVR');
    public Id service = Utils.getRecordTypeId('Lead','Service');
    public Id voucher = Utils.getRecordTypeId('Lead','Voucher');
    public List<User> users = [select Id from User where Name = 'Renault do Brasil'];
    public Id renaultOwner = users.isEmpty() ? null : users.get(0).Id;
    
    private VFC131_LeadBO(){}
        
    public static VFC131_LeadBO getInstance(){
        return instance;
    }
    
    
    /**
    *
    */
    public void updateMolicar(List<Lead> lstNewLead) {
        for (Lead lead: lstNewLead) {
            if (lead.CRV_CurrentVehicle_WebToLead__c != null && lead.CRV_CurrentVehicle_WebToLead__c != '') {
                if (VFC11_MolicarDAO.getInstance().findMolicarById(lead.CRV_CurrentVehicle_WebToLead__c) != null)             
                    lead.CRV_CurrentVehicle__c =  lead.CRV_CurrentVehicle_WebToLead__c;             
            }           
        }
    }
    
    /**
    * Business rule to convert a lead into account
    * - alead : required
    * - aCampaign : optional
    */
    public Opportunity convertLeadAndCreateOpportunity(Lead lead, Campaign aCampaign){
        system.debug(LoggingLevel.INFO, '*** convertLeadAndCreateOpportunity()');
        system.debug(LoggingLevel.INFO, '*** lead='+lead);
        system.debug(LoggingLevel.INFO, '*** aCampaign='+aCampaign);
        
        Id oppId;
        Opportunity opp = new Opportunity();
        
        if (lead != null) {
            
            // Account associated to the lead       
            Account account = null;
            Lead leadOld = null;
            
            // Check if there's an account using the lead's CPF
            if (!String.isEmpty(lead.CPF_CNPJ__c)){  
                account = VFC12_AccountDAO.getInstance().fetchAccountUsingCPF(lead.CPF_CNPJ__c, accountRecTypeId);
                system.debug(LoggingLevel.INFO, '*** account='+account);
                if (account != null)
                    lead.Account__c = account.Id;
                
                // Checks for a lead that was created before the lead that is entering using the lead cpf.
                List<Lead> lsLead = [
                    SELECT Id FROM lead WHERE CPF_CNPJ__c =: lead.CPF_CNPJ__c 
                    AND lead.RecordTypeId !=: voucher ORDER BY CreatedDate asc
                ];
                
                if(lsLead.get(0).Id != null){
                	leadOld = VFC09_LeadDAO.getInstance().findLeadByLeadId(lsLead.get(0).Id);
                }
            }
            
            // Mapping columns from Lead to Account
            account = VFC26_LeadAndAccountMapping.getInstance().leadToAccountMapping(lead, leadOld , lead.Account__c);  
            
            System.debug('ACCOUNT: ' + account);
            
            // Begin transaction
            Savepoint sp = Database.setSavepoint();
            try {
                String accountId = null;
                Database.SaveResult accResult = null;
                
                if(lead.Account__c != null) {
                    accResult = VFC12_AccountDAO.getInstance().updateData(account);
                    accountId = lead.Account__c;
                    
                }  else {
                    accResult = VFC12_AccountDAO.getInstance().insertData(account);
                    accountId = accResult.getId();
                }
                
                if ( accountId != null) {
                    // Get inserted account information
                    Account accountInfo = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(accountId);
                    
                    //convert the new lead and create opportunity
                    if(lead != null && lead.IsConverted == false)
                        oppId = convertLead(accountId, lead);
                    
                    //convert lead that was created before the lead that is entering.
                    if(LeadOld != null && LeadOld.IsConverted == false && lead.Id != leadOld.Id)
                        convertLeadNotCreateOpportunity(accountId, leadOld);
                    
                    // verifica se campanha é Ação Promocional e gera voucher
                    if(aCampaign != null && aCampaign.type != null && aCampaign.type.equals('Promotional Action')){
                        geraLeadvoucher(lead);
                    }
                    
                    List<Model__c> modelList = new List<Model__c>();
                    List<PVVersion__c> versionList = new List<PVVersion__c>();
                    List<Optional__c> colorList = new List<Optional__c>();
                    
                    if( lead.TransactionCode__c != null && lead.SemiclairVersion__c != null && lead.SemiclairColor__c != null){
                        
                        modelList = [
                            SELECT Id, IsNewRelease__c, Name, Model_PK__c 
                            FROM Model__c 
                            WHERE Name =: lead.VehicleOfInterest__c 
                            AND Status_SFA__c = 'Active' 
                            LIMIT 1
                        ];
                        
                        System.debug('modelList: ' + modelList);
                        
                        if(!modelList.isEmpty()){
                            versionList = [
                                SELECT Id, Price__c
                                FROM PVVersion__c
                                WHERE Semiclair__c =: lead.SemiclairVersion__c
                                AND Model__c =: modelList[0].Id
                            ];
                        
                        }
                        
                        System.debug('versionList: ' + versionList);
                        
                        if(!versionList.isEmpty()){
                            colorList = [
                                SELECT Id
                                FROM Optional__c
                                WHERE Optional_Code__c =: lead.SemiclairColor__c
                                AND Version__c =: versionList[0].Id
                            ];
                        
                        } else{
                            lead.SemiclairVersion__c = 'XXXXX';
                            lead.SemiclairColor__c = 'XXXXX';
                        }
                        
                        if(colorList.isEmpty()){
                            lead.SemiclairColor__c = 'XXXXX';
                        }
                        
                        System.debug('colorList: ' + colorList);
                        
                    }
                    
                    // Update Opp with origins
                    system.debug('*** Opportunity created = ' + oppId);
                    opp = createOpportunity(accountInfo, aCampaign, lead, oppId);

                    /* NOVO PROCESSO DE LANÇAMENTOS DE NOVOS MODELOS RENAULT */
                    
                    System.debug('lead.TransactionCode__c: ' + lead.TransactionCode__c);
                    System.debug('lead.SemiclairVersion__c: ' + lead.SemiclairVersion__c);
                    System.debug('lead.SemiclairColor__c: ' + lead.SemiclairColor__c);

                    if( lead.TransactionCode__c != null && lead.SemiclairVersion__c != null && lead.SemiclairColor__c != null){
                        
                        /*List<Model__c> modelList = [
                            SELECT Id, IsNewRelease__c, Name, Model_PK__c 
                            FROM Model__c 
                            WHERE Name =: lead.VehicleOfInterest__c 
                            AND Status_SFA__c = 'Active' 
                            LIMIT 1
                        ];
                        
                        System.debug('modelList: ' + modelList);
                        
                        List<PVVersion__c> versionList = [
                            SELECT Id, Price__c
                            FROM PVVersion__c
                            WHERE Semiclair__c =: lead.SemiclairVersion__c
                            AND Model__c =: modelList[0].Id
                        ];
                        
                        System.debug('versionList: ' + versionList);
                        
                        List<Optional__c> colorList = new List<Optional__c>();
                        
                        if(!versionList.isEmpty()){
                            colorList = [
                                SELECT Id
                                FROM Optional__c
                                WHERE Optional_Code__c =: lead.SemiclairColor__c
                                AND Version__c =: versionList[0].Id
                            ];
                        }
                        
                        System.debug('colorList: ' + colorList);*/

                        if(!modelList.isEmpty() && !versionList.isEmpty() && !colorList.isEmpty()){

                            createTestDrive( opp.Id, opp.AccountId, lead.VehicleOfInterest__c );
                            Id quoteId = createQuote(opp);
                            createQuoteLineItem(quoteId, lead.SemiclairColor__c, lead.SemiclairUpholstered__c, lead.SemiclairHarmony__c, modelList[0].Name, modelList[0].Id, versionList[0].Id, lead.SemiclairOptional__c);
                            //createQuoteLineItem(quoteId, lead, modelList[0], versionList[0]);
                            DistrinetCallServico.call(quoteId, lead.SemiclairColor__c, lead.SemiclairUpholstered__c, lead.SemiclairHarmony__c, modelList[0].Name, modelList[0].Id, versionList[0].Id, lead.SemiclairOptional__c);

                        }
                    }
                }
                
            } catch (Exception ex) {
                System.debug('Ex: ' + ex.getCause());
                Database.rollback(sp);
                throw ex;
            }
            
        }
        return opp;
    }
    
    @future
    public static void createQuoteLineItem(Id quoteId, String color, String upho, String harmo, String modelName, String modelId, String versionId, String optionals){
        Lead l = new Lead(SemiclairColor__c=color,
                          SemiclairUpholstered__c=upho,
                          SemiclairHarmony__c=harmo,
                          SemiclairOptional__c=optionals);
        
        VFC131_LeadBO.getInstance().createQuoteLineItem(quoteId, l, new Model__c(Id=modelId, Name=modelName) , new PVVersion__c(Id=versionId));
        //DistrinetCallServico.call(quoteId);
        
    }

    public void createQuoteLineItem(Id quoteId, Lead lead, Model__c model, PVVersion__c version){
        
        String[] modelos = model.Name.split(' ');
        
        Product2 produto = [
            SELECT id, Name, Version__c, Model__r.Name, ModelSpecCode__c
            FROM Product2
            WHERE Name IN: modelos LIMIT 1
        ];
        
        List<String> optionalSemiclairs = new List<String>
            { lead.SemiclairColor__c, lead.SemiclairUpholstered__c, lead.SemiclairHarmony__c };

        if(lead.SemiclairOptional__c != null) {
            List<String> items = lead.SemiclairOptional__c.split(' ');
            for(String it: items) {
                optionalSemiclairs.add(it);
            }
        }
        
        List<Optional__c> opList = [
            SELECT Id, Name, Optional_Code__c, Amount__c, Type__c
            FROM Optional__c
            WHERE Optional_Code__c IN: optionalSemiclairs
            AND Version__c =: version.Id
        ];

        System.debug('***opList: '+opList);
        
        List<String> optionalCodes = new List<String>();
        List<String> optionalNames = new List<String>();
        String colorName, upholsteryName, harmonyName, colorCode, upholsteryCode, harmonyCode;
        
        for(Optional__c o : opList) {
            if(o.Type__c != 'Cor' && o.Type__c != 'Trim' && o.Type__c != 'Harmonia'){
                optionalNames.add(o.Name);
                optionalCodes.add(o.Optional_Code__c);
            }
            if(o.Type__c == 'Cor'){
                colorName = o.Name;
                colorCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Trim'){
                upholsteryName = o.Name;
                upholsteryCode = o.Optional_Code__c;
            }
            if(o.Type__c == 'Harmonia'){
                harmonyName = o.Name;
                harmonyCode = o.Optional_Code__c;
            }
        }
        
        PricebookEntry sObjPricebookEntry = getPricebookEntry(produto.Id);
        
        QuoteLineItem quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quoteId;
        quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 0;
        
        quoteLineItem.Model_AOC__c      = model.Id;
        quoteLineItem.Version_AOC__c    = version.Id;
        quoteLineItem.Color_AOC__c      = colorCode;
        quoteLineItem.ColorName__c      = colorName;
        quoteLineItem.Upholstery_AOC__c = upholsteryCode;
        quoteLineItem.UpholsteryName__c = upholsteryName;
        quoteLineItem.Harmony_AOC__c    = harmonyCode;
        quoteLineItem.HarmonyName__c    = harmonyName;
        quoteLineItem.Optionals_AOC__c  = String.join(optionalCodes, ' ');
        quoteLineItem.OptionalsNames__c = String.join(optionalNames, ' ');

        System.debug('***quoteLineItem: '+quoteLineItem);
        
        VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem);

    }

    public PricebookEntry getPricebookEntry(String idProduto){
        
        PricebookEntry sObjPriceBookEntry = null;
        String priceBookId =  getStandardPriceBook().Id;
        sObjPriceBookEntry = VFC75_AccessoryDAO.getInstance().getPricebookEntry(idProduto, priceBookId); 
        
        return sObjPriceBookEntry;
    } 

    public Pricebook2 getStandardPriceBook(){

        Pricebook2 sObjPricebook2 = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();
        return sObjPricebook2;
    } 

    public Id createQuote(Opportunity opp){

        Quote q = new Quote(
            Name = 'ORÇAMENTO (' + opp.Account.Name + ')',
            OpportunityId = opp.Id,
            ExpirationDate = System.today(),
            Status = 'Pre order sent to Distrinet',
            Pricebook2Id = getStandardPriceBook().Id,
            BillingAccount__c = opp.AccountId,
            DeliveryDate__c = System.today().addDays(10)
        );
        Insert q;

        return q.Id;
    }

    // Test Drive apenas para lançamento do Kwid
    @future
    public static void createTestDrive(String oppId, String accId, String model){

        TDV_TestDrive__c td = new TDV_TestDrive__c(
            Opportunity__c = oppId,
            Status__c = 'Not Performed',
            Customer__c = accId,
            ReasonCancellation__c = 'Test Drive ' + model
        );
        Insert td;

    }
    
    public Account mappingLeadToAccount(Account account, Lead lead, Lead leadOld){
        if(account != null)
            account = new Account(Id = account.Id);
        /* If Account is New Dont update Origins */
        else{
            account = new Account();
            account.Source__c = leadOld.LeadSource;
            account.AccSubSource__c = leadOld.SubSource__c;
            account.Detail__c = leadOld.Detail__c;
            account.Sub_Detail__c = leadOld.Sub_Detail__c;
        }
        account.RecordTypeId = accountRecTypeId;
        if(leadOld.RecordTypeId.equals(leadVendasPF)){
            account.Cliente_DVE__c = true;
            if(!String.isEmpty(leadOld.Type_DVE__c))
                account.Type_DVE__c = leadOld.Type_DVE__c;
            else
                account.Type_DVE__c = lead.Type_DVE__c;
        }
        //datas insert of lead that web-to-lead
        account.FirstName = lead.FirstName;
        account.LastName = lead.LastName;
        account.PersonHomePhone = lead.Home_Phone_Web__c;
        account.PersMobPhone__c = lead.Mobile_Phone__c;
        account.CustomerIdentificationNbr__c = lead.CPF_CNPJ__c;
        account.PersonEmail = lead.Email;
        account.ShippingCity = lead.City;
        account.ShippingState = lead.State;
        account.DealerInterest_BR__c = lead.DealerOfInterest__c;
        account.VehicleInterest_BR__c = lead.VehicleOfInterest__c;
        account.IDBIRPrefdDealer__c = lead.DealerOfInterest__c ;
        return account;
    }
    
    public Id convertLead(Id accountId, Lead lead){
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity( false );
        lc.setLeadId(lead.Id);
        if(lead.RecordTypeId.equals(leadVendasPF))
            lc.setConvertedStatus('Qualified');
        else if(lead.RecordTypeId.equals(service))
            lc.setConvertedStatus('Performed');
        else
            lc.setConvertedStatus('Hot Lead');
        
        // if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (lead.Owner.type == 'Queue')
            lc.setOwnerId(RenaultOwner);
            
        System.debug('antes do erro');
        Database.Leadconvertresult successLeadConversion = Database.convertLead(lc);
        Id oppId = successLeadConversion.getOpportunityId();
        
        return oppId;
    }
    
    public void convertLeadNotCreateOpportunity(Id accountId, Lead leadOld){
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity( true );
        lc.setLeadId(leadOld.Id);
        if(leadOld.RecordTypeId.equals(leadVendasPF))
            lc.setConvertedStatus('Qualified');
        else if(leadOld.RecordTypeId.equals(service))
            lc.setConvertedStatus('Performed');
        else
            lc.setConvertedStatus('Hot Lead');
        
        // if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (leadOld.Owner.type == 'Queue')
            lc.setOwnerId( userInfo.getUserId() );
        
        Database.Leadconvertresult successLeadConversion = Database.convertLead(lc);
    }
    
    public Opportunity createOpportunity(Account accountInfo,Campaign aCampaign,Lead lead, Id oppId){
        
        //oportunidade gerada
        Opportunity opportunityInsert = new Opportunity();
        
        // busca concessionaria pela conta pessoal gerada ou concessionaria de interesse do lead
        Account dealer;
        if(accountInfo.IDBIRPrefdDealer__c != null){
            dealer = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(accountInfo.IDBIRPrefdDealer__c);

        }else if(lead.DealerOfInterest__c != null){
            dealer = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lead.DealerOfInterest__c);
        }
        
        System.debug('Lead: ' + lead);
        System.debug('dealer: ' + dealer);
        
        // Atribuição de proprietario
        if(lead.RecordTypeId.equals(leadVendasPF)){
            opportunityInsert.RecordTypeId = Utils.getRecordTypeId('Opportunity','Vendas_Empresa_PF');
            opportunityInsert.OwnerId = RenaultOwner;     
            
        } else if (lead.RecordTypeId.equals(DVR) && dealer != null && 
           !lead.LeadSource.equalsIgnoreCase('Dealer') && !lead.SubSource__c.equalsIgnoreCase('PROSPECT')) {
            opportunityInsert.RecordTypeId = Utils.getRecordTypeId('Opportunity','DVR');
            opportunityInsert.OwnerId = atribuiProprietario(opportunityInsert,dealer);
            
        } else if (lead.RecordTypeId.equals(service) && dealer != null){
            opportunityInsert.RecordTypeId = Utils.getRecordTypeId('Opportunity','Service');
            opportunityInsert.OwnerId = setDealerManagerOwner(dealer);
            
        } else if(lead.RecordTypeId.equals(DVR) && dealer != null && 
            lead.LeadSource.equalsIgnoreCase('Dealer') && lead.SubSource__c.equalsIgnoreCase('PROSPECT')) {
            opportunityInsert.RecordTypeId = Utils.getRecordTypeId('Opportunity','DVR');
            opportunityInsert.OwnerId = UserInfo.getUserId();
        }
        
        if(lead.PromoCode__c != null && lead.PromoCode__c != '')
            opportunityInsert.PromoCode__c = lead.PromoCode__c;
        if (aCampaign != null)
            opportunityInsert.CampaignId = aCampaign.Id;

        opportunityInsert.Name = Label.OPPName +' ' + '(' + accountInfo.Name + ')';
        opportunityInsert.AccountId = accountInfo.Id;
        opportunityInsert.Dealer__c = accountInfo.IDBIRPrefdDealer__c != null ? accountInfo.IDBIRPrefdDealer__c : lead.DealerOfInterest__c;
        opportunityInsert.Description = lead.Description;
        opportunityInsert.OpportunitySource__c = lead.LeadSource;
        opportunityInsert.OpportunitySubSource__c = lead.SubSource__c;
        opportunityInsert.Detail__c = lead.Detail__c;
        opportunityInsert.Sub_Detail__c = lead.Sub_Detail__c;
        opportunityInsert.Internet_Support__c = lead.Internet_Support__c;
        opportunityInsert.Customer_External_Key__c = lead.Customer_External_Key__c;
        opportunityInsert.Form_ID__c = lead.Form_ID__c;
        opportunityInsert.StageName = 'Identified';
        opportunityInsert.CloseDate = Date.today() + 30;
        opportunityInsert.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opportunityInsert.Offer__c = lead.Offer__c;
        opportunityInsert.LeadEmail__c                 =   lead.Email;
        opportunityInsert.Mobile__c                    =   lead.Mobile_Phone__c;
        
        if(String.isNotEmpty(lead.OptinWhatsApp__c)){
            opportunityInsert.OptinWhatsApp__c = lead.OptinWhatsApp__c;
        }
        
        /* SOMENTE PARA VEICULOS LANCAMENTO COM CODIGO DE TRANSACAO BANCARIA NO LEAD */
        if(lead.TransactionCode__c != null) {
            opportunityInsert.TransactionCode__c           =   lead.TransactionCode__c;
            opportunityInsert.SemiclairModel__c            =   lead.SemiclairModel__c;
            opportunityInsert.SemiclairVersion__c          =   lead.SemiclairVersion__c;
            opportunityInsert.SemiclairHarmony__c          =   lead.SemiclairHarmony__c;
            opportunityInsert.SemiclairOptional__c         =   lead.SemiclairOptional__c;
            opportunityInsert.SemiclairUpholstered__c      =   lead.SemiclairUpholstered__c;
            opportunityInsert.SemiclairColor__c            =   lead.SemiclairColor__c;
            opportunityInsert.TransactionCode__c           =   lead.TransactionCode__c;
            
            if(String.isNotEmpty(lead.SemiclairVersion__c) && !lead.SemiclairVersion__c.equalsIgnoreCase('XXXXX') &&
                String.isNotEmpty(lead.SemiclairColor__c) && !lead.SemiclairColor__c.equalsIgnoreCase('XXXXX')){
                opportunityInsert.StageName                =   'Order';
                
            } else{
                opportunityInsert.StageName                =   'Identified';
                
            }
        
        }
        
        // Somente para request a service, pois associa um veiculo
        if(lead.VehicleRegistrNbr__c != null){
            opportunityInsert.Vehicle_Of_Interest__c = accountInfo.VehicleInterest_BR__c;
            opportunityInsert.VehicleRegistrNbr__c = lead.VehicleRegistrNbr__c;
            opportunityInsert.ServiceType__c = lead.ServiceType__c; 
            opportunityInsert.ScheduledPeriod__c = lead.ScheduledPeriod__c;
            opportunityInsert.ProposalScheduleDate__c = lead.ProposalScheduleDate__c;
            opportunityInsert.ProposalScheduleHour__c = lead.ProposalScheduleHour__c;
        }
        
        opportunityInsert.Id = oppId;
        system.debug(LoggingLevel.INFO, '*** opportunityInsert='+opportunityInsert);
        Update opportunityInsert;
        
        Return opportunityInsert;
    }
    
    public Id setDealerManagerOwner(account dealer){

        // recupera o conjunto de permissões Request a Service
        List<PermissionSet> permissionDealerUsers = [ SELECT Id, Name FROM PermissionSet WHERE Name = 'BR - Request a Service'];
        Set<Id> dealerUsers = new Set<Id>();

        system.debug('### permissionDealerUsers: ' + dealerUsers);

        // recupera os usuarios de concessionarias atribuidos
        if(!permissionDealerUsers.isEmpty()){
            List<PermissionSetAssignment> assigneeDealers = [SELECT AssigneeId FROM PermissionSetAssignment 
                WHERE PermissionSetId in: permissionDealerUsers];
            for(PermissionSetAssignment psa: assigneeDealers){
                dealerUsers.add(psa.AssigneeId);
            }
        }

        system.debug('### dealerUsers: ' + dealerUsers);

        // recupera os usuarios e suas respectivas contas de concessionaria
        List<User> lsUserDealer = [Select Id, Contact.AccountId from User where IsActive = TRUE AND Id in: dealerUsers];

        system.debug('### lsUserDealer: ' + lsUserDealer);

        for(User u: lsUserDealer){
            if(u.Contact.AccountId.equals(dealer.Id)){
                return u.Id;
            }
        }
        return dealer.OwnerId;
    }
    public void insertTask(Opportunity sObjOpportunity){
        System.debug('Oppp:' + sObjOpportunity);
        Task t = new Task();          
        t.WhatId = sObjOpportunity.Id;
        t.OwnerId = sObjOpportunity.OwnerId;
        t.Subject = 'Oportunidade ' + sObjOpportunity.OpportunitySource__c;
        t.Priority = 'High';
        t.IsReminderSet = true;
        t.ActivityDate = Date.today() + 1;
        t.ReminderDateTime = DateTime.now().addMinutes(-1);
        
        /*obtÃ©m o contato pelo id da conta*/
        Id ContactId = [select Id from Contact where AccountId =: sObjOpportunity.AccountId].Id;
        t.WhoId = ContactId;
        Database.insert(t);
    }
    
    public void geraLeadvoucher(Lead lead){
        Lead voucher = lead.clone();
        voucher.Status = 'To Rescue';
        voucher.RecordTypeId = Utils.getRecordTypeId('Lead', 'Voucher');
        Database.Insert(voucher);
    }
    
    public Id atribuiProprietario(Opportunity opportunityInsert, account accountDelaer){
        if(accountDelaer.Manager_Type__c != null && accountDelaer.Manager_Type__c.equals('Sales Cloud')){
            return opportunityInsert.OwnerId = accountDelaer.OwnerId;
            
        }else if(accountDelaer.Manager_Type__c != null && accountDelaer.Manager_Type__c.equals('Community Cloud')){
            
            Set<String> managerPermissions = new Set<String>{'BR_SFA_Dealer_Manager', 'BR_SFA_Dealer_Manager_Agenda'};
            Set<Id> managerPermissionsIds = new Set<Id>();
            
            // recupera conjunto de permissão do gerente da concessionaria
            Set<Id> conjGerentesIds = new Set<Id>();
            List<PermissionSetAssignment> managerUsers = new List<PermissionSetAssignment>();
            
            
            List<PermissionSet> conjPermManagerLst = [SELECT Id FROM PermissionSet WHERE Name IN: managerPermissions Limit 2];
            
            for(PermissionSet psetManager : conjPermManagerLst){
                managerPermissionsIds.add(psetManager.Id);
            }
            
            //Id conjPermManagerId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Dealer_Manager' Limit 1].Id;
            if(!managerPermissionsIds.isEmpty())
                managerUsers = [SELECT AssigneeId FROM PermissionSetAssignment
                                WHERE PermissionSetId IN: managerPermissionsIds LIMIT 9999];

            System.debug('***managerUsers: '+managerUsers);
            
            for(PermissionSetAssignment ps: managerUsers){
                conjGerentesIds.add(ps.AssigneeId);
            }
            
            Set<String> managerExecPermissions = new Set<String>{'BR_SFA_Dealer_Executive', 'BR_SFA_Dealer_Executive_Agenda'};
            Set<Id> managerExecPermissionsIds = new Set<Id>();
            
            // recupera conjunto de permissão do gerente executivo da concessionaria
            Set<Id> conjGerentesExecIds = new Set<Id>();
            List<PermissionSetAssignment> managerExecUsers = new List<PermissionSetAssignment>();
            
            List<PermissionSet> conjPermManagerExecLst = [SELECT Id FROM PermissionSet WHERE Name IN: managerExecPermissions Limit 2];
            
            for(PermissionSet psetManager : conjPermManagerExecLst){
                managerExecPermissionsIds.add(psetManager.Id);
            }
            
            //Id conjPermManagerExecId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Dealer_Executive' Limit 1].Id;
            if(!managerExecPermissionsIds.isEmpty())
                managerExecUsers = [SELECT AssigneeId FROM PermissionSetAssignment
                                WHERE PermissionSetId IN: managerExecPermissionsIds LIMIT 9999];

            System.debug('***managerExecUsers: '+managerExecUsers);
            
            for(PermissionSetAssignment ps: managerUsers){
                conjGerentesExecIds.add(ps.AssigneeId);
            }
            
            // recupera os gerentes da concessionaria
            List<User> userManager = [SELECT Id from User WHERE Id IN : conjGerentesIds
                                   AND IsActive = TRUE AND AccountId =: accountDelaer.Id limit 1];

            System.debug('***userManager: '+userManager);
            
            // recupera os gerentes de grupo da concessionaria
            List<User> userExecutive = [SELECT Id from User WHERE Id IN : conjGerentesExecIds  
                                        AND IsActive = TRUE AND AccountId =: accountDelaer.Id limit 1];

            System.debug('***userExecutive: '+userExecutive);
            
            // recupera os gerentes de grupo do grupo de concessionarias
            List<User> userGroupExecutive = [SELECT Id from User WHERE UserRole.PortalRole =: 'Executive' 
                                   AND IsActive = TRUE AND Contact.Account.ParentId =: accountDelaer.ParentId limit 1];

            System.debug('***userGroupExecutive: '+userGroupExecutive);

            if( !userManager.isEmpty() ){
                opportunityInsert.OwnerId = userManager.get(0).Id;

            }else if( !userExecutive.isEmpty() ){
                opportunityInsert.OwnerId = userExecutive.get(0).Id;

            }else if( !userGroupExecutive.isEmpty() ){
                opportunityInsert.OwnerId = userGroupExecutive.get(0).Id;

            }else{
                opportunityInsert.OwnerId = accountDelaer.OwnerId;
            }

            System.debug('***opportunityInsert.OwnerId: '+opportunityInsert.OwnerId);

            return opportunityInsert.OwnerId;
            
        }else{
            return opportunityInsert.OwnerId = accountDelaer.OwnerId;
        }
    }
    
    /**VFC33_TaskDAO
    * [before insert]
    * Using BIR_Dealer_of_Interest__c, update the lookup field DealerOfInterest__c
    */
    public void updateLookupDealerOfInterest(List<Lead> lstNewLead) {
        system.debug(LoggingLevel.INFO, '*** updateLookupDealerOfInterest');
        
        Set<String> setBIRnumber = new Set<String>();
        for (Lead lead : lstNewLead) {
            if (lead.BIR_Dealer_of_Interest__c != null && lead.BIR_Dealer_of_Interest__c != '') {
                setBIRnumber.add(lead.BIR_Dealer_of_Interest__c);
            }
            /* @Hugo Medrado {kolekto} | Plataforma Varejo 
            * Preenche Id da Concessionaria e da Oferta
            */
            if(lead.IdDealer__c != null && lead.IdDealer__c != ''){
                lead.DealerOfInterest__c = lead.IdDealer__c; 
            }
            if(lead.IdOffer__c != null && lead.IdOffer__c != ''){
                lead.Offer__c = lead.IdOffer__c;
            }
        }
        
        // Fetch the deler using the set
        Map<String, Id> mapDealer;
        if(!setBIRnumber.isEmpty())
            mapDealer = VFC12_AccountDAO.getInstance().fetchDealerIdByBIRnumberSet(setBIRnumber);         
        
        for (Lead lead : lstNewLead) {
            if (lead.BIR_Dealer_of_Interest__c != null && lead.BIR_Dealer_of_Interest__c != '')  {
                Id dealerId = mapDealer.get(lead.BIR_Dealer_of_Interest__c);
                if (dealerId != null) {
                    lead.DealerOfInterest__c = dealerId;
                    system.debug(LoggingLevel.INFO, '*** lead.BIR_Dealer_of_Interest__c:'+lead.BIR_Dealer_of_Interest__c+' ['+dealerId+']');
                }   
            }
        }
    }
    
    /**
    * [before insert]
    * Using Vehicle_of_Interest__c, update the lookup field Vehicle_of_Interest__c
    */
    public void updateLookupVehicleOfInterest(List<Lead> lstNewLead) {
        system.debug(LoggingLevel.INFO, '*** updateLookupVehicleOfInterest');
        
        Set<String> setVeiculosInteresse = new Set<String>();
        for (Lead lead : lstNewLead)
            setVeiculosInteresse.add(lead.VehicleOfInterest__c);

        List<Product2> products = [SELECT Id, Name, IsActive, Model__c, ModelYear__c, Optionals__c, ProductCode, Description, Family, RecordTypeId, Version__c  
                        FROM Product2
                        WHERE Name in: setVeiculosInteresse 
                        ORDER BY Name, Version__c ASC];
        
        Map<String,Id> mapVeiculoInteresseProduto = new Map<String,Id>();
        if(!products.isEmpty()){
            for(Product2 product: products)
                mapVeiculoInteresseProduto.put(product.Name, product.Id);
        }
            
        for (Lead lead : lstNewLead) {
            if (lead.VehicleOfInterest__c != null && lead.VehicleOfInterest__c != '')
                lead.VehicleOfInterestLookup__c = mapVeiculoInteresseProduto.get(lead.VehicleOfInterest__c);
        }   
    }  
    
    /*Backup método updateLookupVehicleOfInterest, pois foi modificado por apresentar 101 querys
     *
        public void updateLookupVehicleOfInterest(List<Lead> lstNewLead) {
        system.debug(LoggingLevel.INFO, '*** updateLookupVehicleOfInterest');
        
        for (Lead lead : lstNewLead) {
            if (lead.VehicleOfInterest__c != null && lead.VehicleOfInterest__c != '') {
                // Fetch the Vehicle using the set
                List<Product2> product = VFC22_ProductDAO.getInstance().findProductRecordsbyName(lead.VehicleOfInterest__c);
                If(product != null && product.size() > 0)  {       
                    lead.VehicleOfInterestLookup__c = product.get(0).Id;
                }
            }
        }
    } 
    
    /**
    * [before insert]
    * Using SecondVehicleOfInterest__c, update the lookup field SecondVehicleOfInterestLookup__c
    */
    public void updateLookupSecondVehicleOfInterest(List<Lead> lstNewLead) {
        system.debug(LoggingLevel.INFO, '*** updateLookupSecondVehicleOfInterest');
        
        for (Lead lead : lstNewLead) {
            if (lead.SecondVehicleOfInterest__c != null && lead.SecondVehicleOfInterest__c != '') {
                // Fetch the Vehicle using the set
                List<Product2> product = VFC22_ProductDAO.getInstance().findProductRecordsbyName(lead.SecondVehicleOfInterest__c);
                If(product != null && product.size() > 0)  {       
                    lead.SecondVehicleOfInterestLookup__c = product.get(0).Id;
                }
            }
        }
    }
    
    /**
    * @Hugo Medrado {kolekto}
    * Plataforma Varejo
    * Efetua o mapeamento da Oferta 
    */     
    
}