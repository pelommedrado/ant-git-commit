public with sharing class VehicleInvoiceChecker {

	public void startSetting(List<VEH_Veh__c> vehicleList) {
		System.debug('start()');

		final Set<String> chassiSet = new Set<String>();
		for(VEH_Veh__c veiculo : vehicleList) {
			chassiSet.add(veiculo.Name);
		}

		final Map<String, FaturaDealer2__c> chassiFaturaLastMap =
			createChassiFaturaLastMap(chassiSet);

		for(VEH_Veh__c vehicle : vehicleList) {
			final FaturaDealer2__c faturaLast = chassiFaturaLastMap.get(vehicle.Name);

			if(faturaLast != null) {
				vehicle.ValidInvoice__c = 'S';
                if(vehicle.DeliveryDate__c != null) {
                	faturaLast.Delivery_Date__c = dateToString(vehicle.DeliveryDate__c);
                }
			} else {
				vehicle.ValidInvoice__c = 'N';
			}
		}
        Database.update(vehicleList);
		Database.update(chassiFaturaLastMap.values());
	}

	private Map<String, FaturaDealer2__c> createChassiFaturaLastMap(Set<String> chassiSet) {
		final List<FaturaDealer2__c> faturaList = [
			SELECT Id, VIN__c FROM FaturaDealer2__c
			WHERE VIN__c IN: chassiSet AND Invoice_Type__c =: FaturaNotaService.NFTYPE_NEW_VEHICLE
			ORDER BY Invoice_Data__c DESC NULLS LAST
		];

		final Map<String, FaturaDealer2__c> chassiFaturaLastMap = new Map<String, FaturaDealer2__c >();
		for(FaturaDealer2__c fatura : faturaList) {
			final FaturaDealer2__c faturaTemp = chassiFaturaLastMap.get(fatura.VIN__c);
			if(faturaTemp == null) {
				chassiFaturaLastMap.put(fatura.VIN__c, fatura);
			}
		}
		return chassiFaturaLastMap;
	}

	/*public void startSetting(List<VEH_Veh__c> scope){

		List<VEH_Veh__c> lstVehicle = new List<VEH_Veh__c>();
		List<String> lstChassi = new List<String>();
		for(VEH_Veh__c veh : scope){
			lstVehicle.add(veh);
			lstChassi.add(veh.Name);
		}

		List<faturaDealer2__c> lstFaturas = [SELECT Id, VIN__c
											FROM FaturaDealer2__c
											WHERE VIN__c IN : lstChassi
											AND   Invoice_Type__c = 'New Vehicle'];

		Map<String, faturaDealer2__c> chassiFaturaMap = new Map<String, faturaDealer2__c>();

		for(FaturaDealer2__c fatura : lstFaturas){
			chassiFaturaMap.put(fatura.VIN__c, fatura);
		}

		List<VEH_Veh__c>		vehiclesToUpdate 	= new List<VEH_Veh__c>();
		List<FaturaDealer2__c> 	faturasToUpdate 	= new List<FaturaDealer2__c>();

		for(VEH_Veh__c vehicle : lstVehicle){
			faturaDealer2__c fatura = chassiFaturaMap.get(vehicle.Name);
			if(fatura != null){
				vehicle.ValidInvoice__c = 'S';
				fatura.Delivery_Date__c = dateToString(vehicle.DeliveryDate__c);
				faturasToUpdate.add(fatura);
			}
			else{
				vehicle.ValidInvoice__c = 'N';
			}
			vehiclesToUpdate.add(vehicle);
		}

		Database.update(vehiclesToUpdate);
		Database.update(faturasToUpdate);

	}*/

  private String dateToString(Date data){
  	List<String> arrayData 	= new List<String>();
  	arrayData 				= String.valueOf(data).split('-');
  	return arrayData[2]+'/'+arrayData[1]+'/'+arrayData[0];
  }
}