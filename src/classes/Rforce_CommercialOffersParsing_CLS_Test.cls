/*****************************************************************************************
    Name            : Rforce_CommercialOffersParsing_CLS_Test
    Description     : This class is used to test Rforce_CommercialOffersParsing_CLS,Rforce_CommercialOffersPush_CTR and Rforce_CommercialOffersPropose_CLS
    Approach        : Passing the String XML response to Parse engine to parse
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date:18/05/2015
    Imnplemented By :Vinoth Baskaran
 
******************************************************************************************/
@isTest()
public with sharing class Rforce_CommercialOffersParsing_CLS_Test {

       //Added for 8.0_SP2:Parsing the XML Response as String to Parse the XML
       static testMethod void testCommercialOffersParsing()

        {
        Rforce_CommercialOffersParsing_CLS commercialOffer= new Rforce_CommercialOffersParsing_CLS();
     
        string strXML = '<propositions><proposition id="1709291508" offer-id="1709291508" offerSpace-id="1709108384" weight="1" rank="2" code="TEST_RFORCE" label="Test WS Rforce_celine" startDate="2014-05-11 22:00:00.000Z" endDate="2015-07-30 22:00:00.000Z" validityDate="2015-07-30 22:00:00.000Z"><xmlRepresentation><desMkt>test rforce</desMkt><deskEli>test rforce</deskEli><deskLeg>test rforce</deskLeg><pictureGen>https://renault-s.neolane.net/res/renault/6d6a3ea37ad7cdb1a2615a419f6180bb.jpg</pictureGen><pictureDetail>https://renault-s.neolane.net/res/renault/75f28d037eb95a952dc935ddc5366d18.jpg</pictureDetail></xmlRepresentation><category type="EXCLU"/><context weightExpr="1"/><newOffer>false</newOffer></proposition></propositions>';
    
        commercialOffer.parseCommercialOffers(strXML);
        commercialOffer.parsePushEvents(strXML);
        commercialOffer.dateFormat('2008-10-05-11:20:20');
        
        Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
        Rforce_CommercialOffersPropose_CLS offerPropose = new Rforce_CommercialOffersPropose_CLS('test@gmail.com','india','test','test','test','test','test','test');
        Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc12', Phone = '0000', RecordTypeId = RTID, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
        insert Acc;
        
        Commercial_Offer__c comOffer = new Commercial_Offer__c();
        comOffer.account__c=acc.id;
        comOffer.Offer_Id__c ='24234234221';            
        insert comOffer;       
       // Rforce_CommercialOffersPush_CTR.pushOffer('test@gmail.com','https://google.com',comOffer.Offer_Id__c,'Test1 Acc12','1-0438250');

        }
     

}