@isTest
private class TestAP02Goodwill 
{
    static testMethod void TestAP02Goodwill_TEST ()
    {
        Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
        
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com',BypassVR__c=true,BypassWF__c=true);

        Account Acc = new Account(Name='Test1',Phone='1000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
        insert Acc;    
        Contact Con = 
        new Contact(FirstName = 'Mandatory', LastName='Test Contact',Salutation='Mr.',ProEmail__c ='lro@lro.com',AccountId=Acc.Id);
        insert Con;
        
        VEH_Veh__c v = new VEH_Veh__c (Name = '45678934567123456', VehicleBrand__c = 'Renault', DeliveryDate__c= date.parse('04/06/2010'));
        insert v;
                 
        Case c = new Case (Origin='Mobile',Type='Information Request',SubType__c='Booking',VIN__c= v.Id, Status='Open',CaseBrand__c= 'RNO', Priority='Urgent',Kilometer__c= 100000, Description='Description ',From__c='Customer', AccountId=Acc.Id, ContactId=Con.Id);
        insert c;
        Id RecordId= [select Id from RecordType where sObjectType='Goodwill__c' and Name='CAR RENTAL' limit 1].Id;
       
        Goodwill__c g = new Goodwill__c (case__c = c.Id,ResolutionCode__c='Voucher',RecordTypeId=RecordId,ORDate__c= date.parse('02/02/2010'),ExpenseCode__c='OTS Issue',BudgetCode__c='SRC',Organ__c= 'BATTERY', SRCPartRate__c=0.5,QuotWarrantyRate__c=1000, Country__c= 'Brazil');
        Goodwill_Grid_Line__c ggl = new Goodwill_Grid_Line__c (Country__c = 'Slovakia', VehicleBrand__c = 'Renault',Organ__c = 'BATTERY', Max_Age__c = 100, Max_Mileage__c= 800000);
               
         System.runAs(usr)
        {                 
        Test.startTest();
    
        insert ggl;
        insert g;
        g.Country__c = '';
        update g;
    
        Test.stopTest();       
    }
    }
}