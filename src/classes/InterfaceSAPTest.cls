/* See all data true, because in insert QuoteLineItem is need. */
@isTest //(SeeAllData = true) 
private class InterfaceSAPTest {

	public static void setup(){

		Product2 prod = new Product2(
			Name = 'Laptop X200', 
            Family = 'Hardware'
        );
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
            UnitPrice = 10000, 
            IsActive = true
        );
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(
        	Name='Custom Pricebook',
        	isActive=true
    	);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = prod.Id,
            UnitPrice = 12000, 
            IsActive = true
        );
        insert customPrice;

		Opportunity opp = MyOwnCreation.getInstance().criaOpportunity();
		Insert opp;

		Quote q = MyOwnCreation.getInstance().criaQuote();
		q.Name = '123quote123';
		q.OpportunityId = opp.Id;
		q.Pricebook2Id = customPB.Id;
		Database.Insert(q);

		VEH_Veh__c vehicle = MyOwnCreation.getInstance().criaVeiculo();
		vehicle.Name = 'GHFTSD2453726354Y';
		Insert vehicle;

		VehicleBooking__c vbk = new VehicleBooking__c(); 
		vbk.Status__c = 'Active';
		vbk.Vehicle__c = vehicle.Id;
		vbk.Quote__c = q.Id;
		Insert vbk;

		Model__c models = new Model__c(
            Model_Spec_Code__c = 'FLM',
            Name = 'Fluence',
            Model_PK__c = 'FLM'
        );
        Insert models;

        PVVersion__c versoes = new PVVersion__c(
            Semiclair__c = 'HH R3 2G',
            Name = 'Authenthic 1.6',
            Model__c = models.Id,
            PVC_Maximo__c = 2,
            PVR_Minimo__c = 3,
            Price__c = 2,
            Version_Id_Spec_Code__c = 'HH R3 2G'
        );
        Insert versoes;

        Optional__c optionals = new Optional__c(
            Name = 'Tapete',
            Optional_Code__c = 'PK556',
            Version__c = versoes.Id
        );
        Insert optionals;

		QuoteLineItem qli = new QuoteLineItem(
			QuoteId = q.Id,
			Model_AOC__c = models.Id,
			Version_AOC__c = versoes.Id,
			Color_AOC__c = 'OV369',
			Upholstery_AOC__c = 'DRAP01',
			Harmony_AOC__c = 'HARM01',
			Optionals_AOC__c = 'PK556',
			Product2Id = prod.Id,
			PricebookEntryId = customPrice.Id,
			Quantity = 1,
			UnitPrice = 1000
		);
		Insert qli;

	}

	@isTest 
	static void processCL1s() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_LogicCode__c = 'CL1',
			PC_CustomerNumberInternalSedre__c = '30573712808',
			PC_TypeOfCustomerSedre__c = '1',
			PC_FirstName__c = 'Nome',
			PC_PersonalOrCompanyClientName__c = 'Sobrenome',
			PC_WayNome__c = 'Rua',
			PC_City__c = 'Cidade',
			PC_State__c = 'SP',
			PC_MobilePhone__c = '44444444',
			PC_PersonalPhone__c = '33333333'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);

	}
	
	@isTest 
	static void processMCOs() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_ChassisNumberCurrent__c = '12345RFDSER2345RW',
			PC_SemiclairModel__c = 'FLM',
			PC_SemiclairVersion__c = 'FGD GD 23H',
			PC_SemiclairOptional__c = 'PK556; PK223',
			PC_ColorBoxCriterion__c = 'OV369',
			PC_SemiclairUpholstered__c = 'DRAP01',
			PC_SemiclairHarmony__c = 'HARM01',
			PC_LogicCode__c = 'MCO'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);
		
	}

	@isTest 
	static void processACOs() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_ChassisNumberCurrent__c = '12345RFDSER2345RW',
			PC_SemiclairModel__c = 'FLM',
			PC_SemiclairVersion__c = 'FGD GD 23H',
			PC_SemiclairOptional__c = 'PK556; PK223',
			PC_ColorBoxCriterion__c = 'OV369',
			PC_SemiclairUpholstered__c = 'DRAP01',
			PC_SemiclairHarmony__c = 'HARM01',
			PC_LogicCode__c = 'ACO'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);

	}

	@isTest 
	static void processDSFs() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_ChassisNumberCurrent__c = '12345RFDSER2345RW',
			PC_SemiclairModel__c = 'FLM',
			PC_SemiclairVersion__c = 'FGD GD 23H',
			PC_SemiclairOptional__c = 'PK556; PK223',
			PC_ColorBoxCriterion__c = 'OV369',
			PC_SemiclairUpholstered__c = 'DRAP01',
			PC_SemiclairHarmony__c = 'HARM01',
			PC_LogicCode__c = 'DSF'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);
		
	}

	@isTest 
	static void processAFCs() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_ChassisNumberCurrent__c = '12345RFDSER2345RW',
			PC_SemiclairModel__c = 'FLM',
			PC_SemiclairVersion__c = 'FGD GD 23H',
			PC_SemiclairOptional__c = 'PK556; PK223',
			PC_ColorBoxCriterion__c = 'OV369',
			PC_SemiclairUpholstered__c = 'DRAP01',
			PC_SemiclairHarmony__c = 'HARM01',
			PC_LogicCode__c = 'AFC'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);
		
	}

	@isTest
	static void testObterVeicRelEAtualizarCotacoes(){

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Map<String,VehicleBooking__c> vbkMap = new Map<String,VehicleBooking__c>();

		VehicleBooking__c vbk = [SELECT Id, Quote__c, Vehicle__c, Quote__r.Opportunity.Dealer__r.ParentId FROM VehicleBooking__c LIMIT 1];
		vbkMap.put(vbk.Vehicle__c, vbk);

		List<Quote> quotes = [SELECT Id FROM Quote];

		InterfaceSAP.obterVeicRelEAtualizarCotacoes(vbkMap,quotes);
	}

	/*@isTest
	static void testCriarVeiculos(){

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Map<String,Id> veiculoMap = new Map<String,Id>();

		Veh_Veh__c v = [SELECT Id, Name FROM Veh_Veh__c LIMIT 1];
		veiculoMap.put(v.Name, v.Id);

		List<Quote> quotes = new List<Quote>();
		quotes.add(q);

		InterfaceSAP.criarVeiculos(quotes,veiculoMap);
	}*/

	@isTest
	static void testRecuperarVBK(){

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Map<String,Id> veiculoMap = new Map<String,Id>();

		Veh_Veh__c v = [SELECT Id, Name FROM Veh_Veh__c LIMIT 1];
		veiculoMap.put(v.Name, v.Id);

		List<Quote> quotes = new List<Quote>();
		quotes.add(q);

		InterfaceSAP.recuperarVBK(quotes,veiculoMap);
	}


	@isTest
	static void processCCOs() {

		setup();

		Quote q = [SELECT Id FROM Quote LIMIT 1];

		Quote quote1 = new Quote(
			Id = q.Id,
			PC_ChassisNumberCurrent__c = '12345RFDSER2345RW',
			PC_SemiclairModel__c = 'FLM',
			PC_SemiclairVersion__c = 'FGD GD 23H',
			PC_SemiclairOptional__c = 'PK556; PK223',
			PC_ColorBoxCriterion__c = 'OV369',
			PC_SemiclairUpholstered__c = 'DRAP01',
			PC_SemiclairHarmony__c = 'HARM01',
			PC_LogicCode__c = 'CCO'
		);
		Database.Update(quote1, Quote.fields.External_Id__c);
		
	}
	
}