@isTest
private class FaturaClienteServiceTest {

	@isTest
	static void itShouldCreateAccountCompany() {

		final FaturaDealer2__c faturaInvalid = new FaturaDealer2__c();
		faturaInvalid.Customer_Identification_Number__c = '51820533000150';
		faturaInvalid.External_Key__c = '1004';
		faturaInvalid.RecordTypeId = FaturaDealer2Utils.RECORDTYPEID_REGISTER;
		faturaInvalid.Status__c = 'Invalid';
		Database.insert(faturaInvalid);

		final FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
  	fatura.Customer_Type__c = 'J';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '1005';
		fatura.RecordTypeId = FaturaDealer2Utils.RECORDTYPEID_REGISTER;
		fatura.Status__c = 'Unprocessed';
		Database.insert(fatura);

		final List<faturaDealer2__c> faturaList = new List<faturaDealer2__c> { fatura };
		final FaturaClienteService faturaService = new FaturaClienteService(faturaList);
		faturaService.startValidation();

		final List<FaturaDealer2__c> faturaUpdateList = [
			SELECT Id, Status__c FROM FaturaDealer2__c WHERE External_Key__c = '1004'
		];
		System.assertEquals(faturaUpdateList.isEmpty(), false);
		System.assertEquals(faturaUpdateList.get(0).Status__c, 'Unprocessed');

		final List<FaturaDealer2__c> faturaDeleteList = [
			SELECT Id, Status__c FROM FaturaDealer2__c WHERE External_Key__c = '1005'
		];
		System.assertEquals(faturaDeleteList.isEmpty(), false);
        System.assertEquals(faturaDeleteList.get(0).Status__c, 'Valid');
	}

	@isTest
	static void itShouldCreateAccountPersonal() {
		final FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c = '44542350746';
  		fatura.Customer_Type__c = 'F';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.External_Key__c = '1004';
		fatura.RecordTypeId = FaturaDealer2Utils.RECORDTYPEID_REGISTER;
		fatura.Status__c = 'Unprocessed';
		Database.insert(fatura);

		final List<faturaDealer2__c> faturaList = new List<faturaDealer2__c> { fatura };
		final FaturaClienteService faturaService = new FaturaClienteService(faturaList);
		faturaService.startValidation();

		final List<Account> accList = [
			SELECT Id, RecordTypeId FROM Account WHERE CustomerIdentificationNbr__c = '44542350746'
		];

		System.assertEquals(accList.isEmpty(), false);
		System.assert(accList.get(0).RecordTypeId == Utils.getRecordTypeId('Account', 'Personal_Acc'));
	}
}