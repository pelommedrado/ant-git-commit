@isTest
private class PassingPurchaseServicoTest {
    
    @isTest
    static void test() {
        
        User admin = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User manager = new User();
        
        System.runAs(admin){
            Account acc = new Account(
                Name = 'DealerAcc', 
                Country__c = 'Brazil',
                ShippingCity = 'Cidade', 
                ShippingState = 'Estado', 
                NameZone__c = 'R2', 
                TV_Signal__c = 'Pra?a',
                Active_PV__c = true,
                isDealerActive__c = true,
                PV_Cooperative__c = true,
                PV_NotCooperative__c = true, 
                IDBIR__c= '123ABC123',
                Status__c = 'Active',
                RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
                Active_to_PPO__c = true,
                Dealer_Status__c = 'Active'
            );
            Database.insert( acc );
            
            Contact cont = new Contact(
                RecordTypeId ='012D0000000KApJIAW',
                LastName = 'teste',
                // Name = 'teste',
                AccountId = acc.Id,
                CPF__c = '898.612.386-03',
                Email='teste2@teste.com.br',
                Phone = '1122334455'
            );
            Database.insert( cont );
            
            manager = new User(
                FirstName = 'Test',
                LastName = 'User',
                Email = 'test@org.com',
                Username = 'test@org1.com',
                Alias = 'tes',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles',
                CommunityNickname = 'testing',
                ProfileId = '00eD0000001PnJi',
                ContactId = cont.Id,
                BIR__c ='123ABC123'
            );
            Database.insert(manager);
        }
        
        
        PassingPurchaseServico pServ = new PassingPurchaseServico('Hoje');
        pServ.executar();
        
        pServ = new PassingPurchaseServico('Ontem');
        pServ.executar();
    }
}