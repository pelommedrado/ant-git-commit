/**
* Classe que será disparada pela trigger do objeto Quote após a atualização dos registros.
* @author Felipe Jesus Silva.
*/
public without sharing class VFC104_QuoteAfterUpdateExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{		
		List<Quote> lstSObjQuote = (List<Quote>) lstNewtData;
		Map<Id, Quote> mapSObjOldQuote = (Map<Id, Quote>) mapOldData;
		
		this.checkStatus(lstSObjQuote, mapSObjOldQuote);
        
        cancelarCotacaoAberta(lstNewtData);
	}
	
    /**
     * Deve cancelar todas as cotacoes da oportunidade a parti do
     * faturamento de uma das cotacoes
     */
    private void cancelarCotacaoAberta(List<Quote> quoteList) {
        List<String> quotes = new List<String>();
        
        for(Quote qu : quoteList) {
            if(qu.Status.equals('Billed')) {
                quotes.add(qu.OpportunityId);
            }
        }
        
        List<Quote> quotesCancelados = [
            SELECT Id, Name, Status 
            FROM Quote 
            WHERE OpportunityId IN :quotes AND Status = 'Open'             
        ];
        
        for(Quote qu : quotesCancelados) {
            qu.Status = 'Canceled';
        }
        
        update quotesCancelados;
    }
    
	private void checkStatus(List<Quote> lstSObjQuote, Map<Id, Quote> mapSObjOldQuote)
	{
		Quote sObjOldQuote = null;
		Opportunity sObjOpportunity = null;
		Map<String, VehicleBooking__c> mapSObjVehicleBooking = null;
		VehicleBooking__c sObjVehicleBooking = null;
		List<Opportunity> lstSObjOpportunity = new List<Opportunity>();
		Set<String> setQuoteId = new Set<String>();
        Set<String> setQuoteId2 = new Set<String>();
		List<Quote> lstSObjQuoteAux = new List<Quote>();
		
        // Hugo Medrado -> Analisar e reativar codigo
        /*for(Quote sObjQuote : lstSObjQuote){
            if(sObjQuote.Status.equals('Pre Order Sent') || sObjQuote.Status.equals('Pre Order Requested'))
            	setQuoteId2.add(sObjQuote.Id);
        }
        if(!setQuoteId2.isEmpty()){
            try{
            	Map<String,VehicleBooking__c> vbkUpdate = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesOrClosedByQuotes(setQuoteId2);
            	VFC86_VehicleBookingBO.getInstance().updateVehicleBookings(vbkUpdate.values());
            }catch(Exception e){
            	//lstSObjQuote.get(0).addError('A reserva desse veículo não é mais válida. Favor Reservar o veículo novamente e em seguida Gerar a Pre Ordem.');
            	lstSObjQuote.get(0).addError(e.getMessage()+' Id da Reserva:'+lstSObjQuote.get(0).Id);        
            }
    	}*/
        
        Map<String, Opportunity> mapOpp = new Map<String, Opportunity>();
		for(Quote sObjQuote : lstSObjQuote)
		{
			System.debug('*** sObjQuote.Status:' + sObjQuote.Status);
			
			sObjOldQuote = mapSObjOldQuote.get(sObjQuote.Id);
			
			if((String.isNotEmpty(sObjQuote.Status)) && (sObjQuote.Status != sObjOldQuote.Status))
			{
				sObjOpportunity = new Opportunity(Id = sObjQuote.OpportunityId);

				// Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
            	sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
				
				if(sObjQuote.Status.equals('Billed'))
				{
					sObjOpportunity.StageName = 'Billed';
					
					setQuoteId.add(sObjQuote.Id);
					
					lstSObjQuoteAux.add(sObjQuote);
				}
				else if(sObjQuote.Status.equals('Canceled'))
				{
                    System.debug('*** UserInfo.getProfileId():' + UserInfo.getProfileId());
                    if(UserInfo.getProfileId().equals('00eD0000001PhWSIA0') ) {
                        sObjOpportunity.StageName = 'Quote';
                    }
					
					setQuoteId.add(sObjQuote.Id);
					
					lstSObjQuoteAux.add(sObjQuote);
				}			
				else if(sObjQuote.Status.equals('Pre Order Sent') || sObjQuote.Status.equals('Pre Order Requested') || sObjQuote.Status.equalsIgnoreCase('Pre order sent to Distrinet'))
				{
					System.debug('OPP STAGE =>  ORDER');
					sObjOpportunity.StageName = 'Order';
                    
				}
				
                System.debug('sObjOpportunity ***: ' + sObjOpportunity);
                System.debug('sObjQuote.OpportunityId ***: ' + sObjQuote.OpportunityId);
                System.debug('mapOpp.containsKey(sObjQuote.OpportunityId) ***: ' + mapOpp.containsKey(sObjQuote.OpportunityId));
                
                if(!mapOpp.containsKey(sObjQuote.OpportunityId)) {
                    lstSObjOpportunity.add(sObjOpportunity);
                    mapOpp.put(sObjQuote.OpportunityId, sObjOpportunity);
                }
				
        	}
		}
		
		System.debug('Cotacoes a Atualizar ***' + setQuoteId);
		if(!setQuoteId.isEmpty())
		{
			try
			{
				mapSObjVehicleBooking = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesOrClosedByQuotes(setQuoteId);
				System.debug('VBKs encontradas ***'+mapSObjVehicleBooking);
				
				for(Quote sObjQuote : lstSObjQuoteAux)
				{
					if(mapSObjVehicleBooking.containsKey(sObjQuote.Id))
					{
						sObjVehicleBooking = mapSObjVehicleBooking.get(sObjQuote.Id);
						
						System.debug('***' + sObjQuote.Status);						
						sObjVehicleBooking.Status__c = sObjQuote.Status.equals('Billed') ? 'Closed' : 'Canceled';
						
						if(sObjQuote.Status == 'Canceled'){
							sObjVehicleBooking.Status__c = 'Canceled';
						}
					}
				}
			}
			catch(VFC89_NoDataFoundException ex)
			{				
			}				
		}
		
        System.debug('Oportunidade a atualizar *** ' + lstSObjOpportunity);
		if(!lstSObjOpportunity.isEmpty())
		{
			VFC47_OpportunityBO.getInstance().updateOpportunities(lstSObjOpportunity);
		}
		
		System.debug('VBKs A atualizar ***'+mapSObjVehicleBooking);
		if(mapSObjVehicleBooking != null)
		{
			//update sObjVehicleBooking;
			VFC86_VehicleBookingBO.getInstance().updateVehicleBookings(mapSObjVehicleBooking.values());
		}
	}
	
}