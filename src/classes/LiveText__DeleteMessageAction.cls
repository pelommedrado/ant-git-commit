/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class DeleteMessageAction {
    global DeleteMessageAction() {

    }
    @InvocableMethod(label='Delete Sms Schedule' description='Deletes the given entries From SMS_Schedule__c')
    global static List<LiveText.ActionResult> DeleteSmsScheduleMessages(List<LiveText.DeleteSmsScheduleItem> items) {
        return null;
    }
}
