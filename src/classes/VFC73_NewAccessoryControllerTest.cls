/**
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC73_NewAccessoryControllerTest {

    static testMethod void myUnitTestAccessoryController() {
        
        Opportunity opp = new Opportunity();
        Quote q = new Quote();        
        PricebookEntry pBookEntry;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;
                 
        // criar nova cotação
        q.Name = 'QuoteNameTest_1';
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb2.Id;
        insert q;

		//PageReference pageref = new PageReference('https://c.cs7.visual.force.com/apex/VFP17_AddAccessory');
        PageReference pageref = new PageReference('VFP17_AddAccessory');
        
        Test.setCurrentPageReference(pageref);
        ApexPages.currentPage().getParameters().put('id', q.Id);

		// selecionar pricebookentry (seeAllData)
        pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];

       // criar item de cotação
       QuoteLineItem qLineItem = new QuoteLineItem();
       qLineItem.QuoteId = q.Id;
       qLineItem.PricebookEntryId = pBookEntry.Id;
       qLineItem.Quantity = 1;
       qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
       insert qLineItem;
                     
       //==================================================================================================
       // INCLUIR ACESSORIO 1 - COM PARAMENTO QUOTELINEITEM NOVO
       //==================================================================================================              
       ApexPages.currentPage().getParameters().put('quoteLineItemId', qLineItem.Id);
       ApexPages.StandardController sc = new ApexPages.StandardController(q);
       VFC73_NewAccessoryController accessoryController = new VFC73_NewAccessoryController(sc);
       accessoryController.initialize();
       accessoryController.itemDescription = 'Test_Insert_Accessory_1';      
       accessoryController.saveAccessory(); // OPERAÇÃO PRINCIPAL PARA TEST
       
       //==================================================================================================
       // INCLUIR ACESSORIO 2 - COM PARAMENTO QUOTELINEITEM NULL
       //==================================================================================================
       
       //pageref = new PageReference('https://c.cs7.visual.force.com/apex/VFP17_AddAccessory');
       pageref = new PageReference('VFP17_AddAccessory');
       
       Test.setCurrentPageReference(pageref);
       ApexPages.currentPage().getParameters().put('id', q.Id);

       //Testar caso null Id
       ApexPages.currentPage().getParameters().put('quoteLineItemId', null);
       
       sc = new ApexPages.StandardController(q);
       
       accessoryController = new VFC73_NewAccessoryController(sc);
       
       accessoryController.initialize();
       accessoryController.saveAccessory(); // OPERAÇÃO PRINCIPAL PARA TEST
       
       
       //=======================================================================================================
       // CAPTURAR EXCEPTION NO METODO DA INCLUSÃO DO ACESSORIO
       //=======================================================================================================

       //pageref = new PageReference('https://c.cs7.visual.force.com/apex/VFP17_AddAccessory');
       pageref = new PageReference('VFP17_AddAccessory');
       
       Test.setCurrentPageReference(pageref);
       ApexPages.currentPage().getParameters().put('id', q.Id);

       try{
           ApexPages.currentPage().getParameters().put('quoteLineItemId', '1');
       
           //Testar caso null Id
           sc = new ApexPages.StandardController(q);
       
           accessoryController = new VFC73_NewAccessoryController(sc);
       
           accessoryController.initialize();
       
           accessoryController.cancelRequest(); // OPERAÇÃO PRINCIPAL PARA TEST
       }
       catch(Exception e){
           accessoryController.saveAccessory();
           accessoryController.cancelRequest();
       }
    }
}