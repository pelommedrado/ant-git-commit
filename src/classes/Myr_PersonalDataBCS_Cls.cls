/** APEX class Used to manage the call to the BCS WebService (PERSONAL DATA) **/
public with sharing class Myr_PersonalDataBCS_Cls {
  
  	public class Myr_PersonalDataBCS_Cls_Exception extends Exception {}
  
  	//Members: webservice and request made
  	public Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService bcsDataSource;
  	public Myr_PersonalDataBCS10_WS.getCustData getCustDataReq;
  
  	/** Format of the date expected for BCS is YYYY-MM-DD
  		@return Date parsed from the entry
  	**/
  	private static Date parseBCSDates(String iDate, String iInfoDate) {
  		try {
			String[] myDate = iDate.split('-');
			Date newDate = Date.newinstance(Integer.valueOf(myDate[0]), Integer.valueOf(myDate[1]), Integer.valueOf(myDate[2]));
			return newDate;	
		} catch (Exception e) {
			//continue without interrupting the treatment
			system.debug('### Myr_PersonalDataBCS_Cls - <mapPersonalDataFields> - '+iInfoDate+' conversion problem: ' + e.getMessage());
		}
		return null;
  	}
  
  	//default constructor
  	public Myr_PersonalDataBCS_Cls() {} 
  
  	/** CALL The BCS Personal Data WebService and @return a Myr_PersonalDataResponse_Cls response **/
  	public Myr_PersonalDataResponse_Cls getPersonaData_BCS(Myr_PersonalData_WS.RequestParameters iParams) {
    	Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element bcsResponse = getBCSData(iParams);
    	Myr_PersonalDataResponse_Cls response = mapPersonalDataFields(bcsResponse);
    	return response;
  	}   

	/** Call the webservice BCS with the given parameters **/
	private Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element getBCSData(Myr_PersonalData_WS.RequestParameters iParams) {
    	if( iParams.brand == null ) {
      		throw new Myr_PersonalDataBCS_Cls_Exception('Brand is mandatory');
    	}
    	if( iParams.vin == null && iParams.Registration == null && iParams.ident1 == null & iParams.idClient == null &&
      		iParams.email == null && ( (iParams.lastname == null && iParams.zip == null) || (iParams.lastname == null && iParams.city == null) ) ) {
        	throw new Myr_PersonalDataBCS_Cls_Exception('The minimum parameters for RBX should one of the following combinations: vin or registration or ident1 or idClient or email or lastname + city or lastname + zip');
    	}
    	//Prepare the header
	    bcsDataSource = new Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService();
	    bcsDataSource.endpoint_x = System.label.BcsWsUrl;
	    bcsDataSource.clientCertName_x = System.label.clientcertnamex;
	    bcsDataSource.timeout_x = Integer.valueOf(CS04_MYR_Settings__c.getInstance().Myr_PersonalData_Timeout__c);
	    bcsDataSource.inputHttpHeaders_x = new Map<String, String>();
	    bcsDataSource.inputHttpHeaders_x.put('Content-Type', 'application/soap+xml; charset=utf-8');
	    bcsDataSource.inputHttpHeaders_x.put('Keep-Alive', '1115');
    	//Prepare the request parameters: 
    	Myr_PersonalDataBCS10_WS.custDataRequest custDataRequest = new Myr_PersonalDataBCS10_WS.custDataRequest();
    	custDataRequest.mode = '';
        custDataRequest.country = (iParams.country == null) ? '' : iParams.country;
        custDataRequest.brand = (iParams.brand == null) ? '' : iParams.brand;
        custDataRequest.demander = iParams.demander; //by default
        custDataRequest.vin = (iParams.vin == null ) ? '' : iParams.vin;
        custDataRequest.lastName = (iParams.LastName == null ) ? '' : iParams.LastName;
        custDataRequest.firstName = (iParams.firstName == null ) ? '' : iParams.firstName;
        custDataRequest.city = (iParams.City == null ) ? '' : iParams.City;
        custDataRequest.zip = (iParams.Zip == null ) ? '' : iParams.Zip;
        custDataRequest.ident1 = (iParams.ident1 == null ) ? '' : iParams.ident1;
        custDataRequest.idClient = (iParams.idClient == null ) ? '' : iParams.idClient;
        custDataRequest.idMyr = '';
        custDataRequest.firstRegistrationDate = '';
        custDataRequest.registration = (iParams.Registration == null ) ? '' : iParams.Registration;
        custDataRequest.email = (iParams.email == null ) ? '' : iParams.email;
        custDataRequest.ownedVehicles = '';
        custDataRequest.nbReplies = (iParams.nbReplies == null) ? '' : String.valueOf(iParams.nbReplies);
    	Myr_PersonalDataBCS10_WS.servicePrefs servicesPrefs = new Myr_PersonalDataBCS10_WS.servicePrefs();
    	servicesPrefs.irn = '?';
        servicesPrefs.sia = '?';
        servicesPrefs.reqid = '?';
        servicesPrefs.userid = '?';
        servicesPrefs.language = '?';
        servicesPrefs.country = '?';
   	 	Myr_PersonalDataBCS10_WS.request request = new Myr_PersonalDataBCS10_WS.request();
	    //request.servicePrefs =  servicesPrefs;
	    request.custDataRequest = custDataRequest;
	    getCustDataReq = new Myr_PersonalDataBCS10_WS.getCustData();
	    getCustDataReq.request = request;
	    //Call BCS
	    bcsDataSource.inputHttpHeaders_x.put('Content-Length', String.ValueOf(getCustDataReq.toString().length()));
    
    	Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element responseBCS = null;
    	if( Test.isRunningTest() ) {
			system.debug('### Myr_PersonalDataSIC_Cls - <getPersonaData_BCS> - BCS is running test');
			//CHECK COUNTRY CODE
			String countryCode = custDataRequest.country;
			List<Country_Info__c> countryInfo = [SELECT Id, Name FROM Country_Info__c WHERE Country_Code_2L__c = :countryCode];
			system.debug('### Myr_PersonalDataSIC_Cls - Nb of Countries found: CountryCode=' + countryCode + ', countries=' + countryInfo);
			system.assertEquals( 1, countryInfo.size());
			//CHECK COUNTRY CODE
			Myr_PersonalDataBCS10_WS_MK mock = new Myr_PersonalDataBCS10_WS_MK();
			//Prepare the workaround due to the bug System.CalloutException: You have uncommitted work pending. Please commit or rollback before calling out in Winter16
	    	//concatenate the fieldsinto the test dedicated fields
	    	//Order is : lastname;firstname;email;custIdentNumber;purchaseNature;vehicleenddaterelation
	    	//impacted classes are SIC MDM and BCS
	    	CS04_MYR_Settings__c myrSettings = CS04_MYR_Settings__c.getInstance();
	    	if( !String.isBlank(myrSettings.MYR_Test_DataSource__c) ) {
		    	List<String> params = myrSettings.MYR_Test_DataSource__c.split(';');
				mock.bcsResponse.getCustDataResponse.response.clientList[0].LastName = (params.size() > 0) ? params[0] : '';
				mock.bcsResponse.getCustDataResponse.response.clientList[0].FirstName = (params.size() > 1) ? params[1] : '';
				mock.bcsResponse.getCustDataResponse.response.clientList[0].contact.email = (params.size() > 2) ? params[2] : '';
				if( params.size() > 3 && !String.isBlank(params[3])) {
					mock.bcsResponse.getCustDataResponse.response.clientList[0].ident1 = params[3];	
				}
				if( params.size() > 4 && !String.isBlank(params[4])) {
					mock.bcsResponse.getCustDataResponse.response.clientList[0].vehicleList[0].purchaseNature = params[4];	
				}
				if( params.size() > 5 && !String.isBlank(params[5])) {
					mock.bcsResponse.getCustDataResponse.response.clientList[0].vehicleList[0].possessionEnd = params[5];	
				}
	    	}
			responseBCS = mock.bcsResponse;
		} else {	
			responseBCS = bcsDataSource.getCustData(getCustDataReq);
		}

    	return responseBCS;
  	}
  
	/** Map the BCS response into a PersonalData response **/
  	private Myr_PersonalDataResponse_Cls mapPersonalDataFields(Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element iBCSResponse) {
		Myr_PersonalDataResponse_Cls pDataResponse = new Myr_PersonalDataResponse_Cls();
    	pDataResponse.response.dataSource = Myr_PersonalData_WS.DataSourceType.BCS;
    	if( iBCSResponse == null ) return pDataResponse;
    	if( iBCSResponse.getCustDataResponse == null ) return pDataResponse;
    	if( iBCSResponse.getCustDataResponse.response == null ) return pDataResponse;
    	if( iBCSResponse.getCustDataResponse.response.clientList != null ) {
	      	for( Myr_PersonalDataBCS10_WS.clientList bcsClient : iBCSResponse.getCustDataResponse.response.clientList ) {
	      		Myr_PersonalDataResponse_Cls.InfoClient infoClient = new Myr_PersonalDataResponse_Cls.InfoClient();
	        	//******** customer ids ******************************************************
	        	infoClient.IdClient = bcsClient.idClient;
	        	infoClient.CustIdentNumber = bcsClient.ident1;
	        	infoClient.CustIdentNumber2 = bcsClient.ident2;
	        	infoClient.MyrId = bcsClient.idMyr;
	        	//******** person ************************************************************
	            infoClient.FirstName = bcsClient.firstName;
	            infoClient.LastName = bcsClient.lastName;
	            infoClient.MiddleName = bcsClient.middleName;
	            if( !String.isBlank(bcsClient.title) ) {
	            	if( '1'.equalsIgnoreCase(bcsClient.title) ) {
	            		infoClient.Title = 'Mr.';
	            	} else if( '2'.equalsIgnoreCase(bcsClient.title) ) {
	            		infoClient.Title = 'Mrs.';
	            	} else if( '3'.equalsIgnoreCase(bcsClient.title) ) {
	            		infoClient.Title = 'Miss.';
	            	}
	            }
	            infoClient.DateOfBirth = parseBCSDates(bcsClient.birthDay, 'Birth Date');
	            infoClient.Lang = bcsClient.lang;
	            if( !String.isBlank(bcsClient.sex) ) {
	            	if( '1'.equalsIgnoreCase(bcsClient.sex) ) {
	            		infoClient.Sex = 'Man';
	            	} else if( '2'.equalsIgnoreCase(bcsClient.sex) ) {
	            		infoClient.Sex = 'Woman';
	            	}
	            } 
	            infoClient.typeperson = bcsClient.typeperson;
	            //******** communication agreement ********************************************
	            if( bcsClient.BCScommAgreement != null && bcsClient.BCScommAgreement.size() > 0 ) {
	            	infoClient.GlobalCommAgreement = bcsClient.BCScommAgreement[0].Global_Comm_Agreement;
	             	infoClient.PostCommAgreement = bcsClient.BCScommAgreement[0].Post_Comm_Agreement;
	              	infoClient.TelCommAgreemen = bcsClient.BCScommAgreement[0].Tel_Comm_Agreement;
	             	infoClient.SMSCommAgreement = bcsClient.BCScommAgreement[0].SMS_Comm_Agreement;
	              	infoClient.FaxCommAgreement = bcsClient.BCScommAgreement[0].Fax_Comm_Agreement;
	              	infoClient.EmailCommAgreement = bcsClient.BCScommAgreement[0].Email_Comm_Agreement;
	            }
	            //******** phone numbers ******************************************************
	            if( bcsClient.contact != null ) {
	            	//SDP: mapping SIC and BCS are perhaps inverted. The implementation below is asked by the business team
	        		//@see mail of Pierre Roussy (23.09.2015 18:07 / RE: WS SFDC SIC), check for a mail titled (Helios : MyRenault : Web Service Personal Data mapping CMDM téléphones)
	        		//sent the 24th of September in 2015 by Philippe Patry.
	            	infoClient.LandLine1 = ((bcsClient.contact.phoneCode1 == null) ? '' : bcsClient.contact.phoneCode1) 
	        			+ ((bcsClient.contact.phoneNum1 == null) ? '' : bcsClient.contact.phoneNum1); 
		        	infoClient/**.LandLine2**/.MobilePhone1 = ((bcsClient.contact.phoneCode2 == null) ? '' : bcsClient.contact.phoneCode2) 
	        			+ ((bcsClient.contact.phoneNum2 == null) ? '' : bcsClient.contact.phoneNum2);
	        		infoClient/**.MobilePhone1**/.LandLine2 = ((bcsClient.contact.phoneCode3 == null) ? '' : bcsClient.contact.phoneCode3) 
	        			+ ((bcsClient.contact.phoneNum3 == null) ? '' : bcsClient.contact.phoneNum3);
	            	infoClient.email = bcsClient.contact.email;
	            }
	            //******** address ******************************************************
	            if( bcsClient.address != null ) {      
					infoClient.StrName = bcsClient.address.strName;          //MDM: strAddressLine
	              	infoClient.StrNum = bcsClient.address.strNum;
	              	infoClient.StrType = bcsClient.address.strType;        
	              	infoClient.StrCompl1 = bcsClient.address.compl1;
	              	infoClient.StrCompl2 = bcsClient.address.compl2;
	              	infoClient.StrCompl3 = bcsClient.address.compl3;
	              	infoClient.CountryCode = bcsClient.address.countryCode;
	              	infoClient.AreaCode = bcsClient.address.areaCode;
	              	infoClient.Zip = bcsClient.address.zip;
	              	infoClient.City = bcsClient.address.city;
	            }
	            //******** dealers  *****************************************************
	        	infoClient.dealers = new List<Myr_PersonalDataResponse_Cls.Dealer>();
	        	if( bcsClient.dealerList != null ) { 
	        		for( Myr_PersonalDataBCS10_WS.dealerList dealer : bcsClient.dealerList) {
	        			if( dealer.birId != null ) { 
	        				try {
	        					infoClient.dealers.add( new Myr_PersonalDataResponse_Cls.Dealer(String.valueOf(dealer.birId[0]))); 
	        				} catch (Exception e) {
	        					//continue without interrupting the treatment
	        					system.debug('### Myr_PersonalDataBCS_Cls - <mapPersonalDataFields> - Dealer problem: ' + e.getMessage());
	        				}
	        			}
	        		}
	        	}
	        	//******** Vehicle informations *****************************************
	        	infoClient.vcle = new List<Myr_PersonalDataResponse_Cls.Vehicle>();
	        	if( bcsClient.vehicleList != null ) {
		          	for( Myr_PersonalDataBCS10_WS.vehicleList bcsVehicle : bcsClient.vehicleList ) {
		            	Myr_PersonalDataResponse_Cls.Vehicle vehicle = new Myr_PersonalDataResponse_Cls.Vehicle();
		            	vehicle.vin = bcsVehicle.vin;
		                vehicle.IdClient = bcsClient.idClient;
		                vehicle.brandCode = bcsVehicle.brandCode;
		                vehicle.modelCode = bcsVehicle.modelCode; 
		                vehicle.modelLabel = bcsVehicle.modelLabel;
		                vehicle.versionLabel = bcsVehicle.versionLabel;
		                vehicle.registration = bcsVehicle.registration;
		                vehicle.vehicleType = '';
		              	vehicle.startDate = '';          //MDM
		              	vehicle.endDate = '';          //MDM
		              	vehicle.firstName = '';          //MDM
		              	vehicle.lastName = '';           //MDM
		              	vehicle.VNVO_NewVehicle = null;
		              	if( !String.isBlank(bcsVehicle.new_x) ) {
		              		if( bcsVehicle.new_x.equalsIgnoreCase('VN')) {
		              			vehicle.VNVO_NewVehicle = true;
		              		} else if( bcsVehicle.new_x.equalsIgnoreCase('VO') ) {
		              			vehicle.VNVO_NewVehicle = false;
		              		} 
		              	}
		              	vehicle.TypeRelationship = bcsVehicle.purchaseNature;
		              	
		              	//Management of the dates
		              	vehicle.lastRegistrationDate = parseBCSDates(bcsVehicle.registrationDate, 'Last Registration Date');
		              	vehicle.firstRegistrationDate = parseBCSDates(bcsVehicle.firstRegistrationDate, 'First Registration Date');
		              	vehicle.possessionBegin = parseBCSDates(bcsVehicle.possessionBegin, 'Possession Begin');
		                vehicle.possessionEnd = parseBCSDates(bcsVehicle.possessionEnd, 'Possession End');
		                vehicle.TechnicalControlDate = parseBCSDates(bcsVehicle.technicalInspectionDate, 'Technical Inspection Date');
		              	infoClient.vcle.add(vehicle);
		          	}
	        	}
		        pDataResponse.response.infoClients.add(infoClient);
		        pDataResponse.response.rawResponse = iBCSResponse.toString();
				pDataResponse.response.rawQuery = (bcsDataSource == null) ? 'Mock mode = no query' : bcsDataSource.toString() + ' ------- ' + getCustDataReq.toString();
			}
    	}
    	return pDataResponse;
  	}
}