/** Call Renault Personal Data webservices (RBX/SIC, MDM and BCS) with different parameters depending on the country
    Check the results found and return it if it's valid.
    @author S. Ducamp
    @date 25.08.2015
**/
public with sharing class Myr_ManageAccount_PersDataCall_Cls {
    
    //To test different cases, we check the lastname in the feedback of the webservice in test mode
    //if the lastname is equal to one of the following value, we skip the response of the webservice
    public enum TestSkipMode {
        SKIP_FLcE,  //skip FLcE within FLcE_POL
        SKIP_1ST    //skip the first matching key
    } 
    
    public enum Keys {
        PID,       //Call with RENAULT PErsonal data id identifier if it's filled
        FLE,       //Call with FIRSTNAME, LASTNAME, EMAIL -no check in output- 
        FLcE,      //Call with FIRSTNAME, LASTNAME -check the mail in output-
        CFL,       //Call with CUSTIDENTNUMBER + LASTNAME + FIRSTNAME -no check in output-
        CcFL,      //Call with CUSTIDENTNUMBER - check Firstname and Lastname in output
        FLcE_POL,  //specific FLcE matcher for Poland due to the end of the lastname for the married women  
        FLV,       //Call with FIRSTNAME, LASTNAME, VIN -no check in output- 
        FLP        //Call with FIRSTNAME, LASTNAME, PHONENUMBER -no check in output- 
    }
    
    /** Inner Exception class **/
    public class MyrPersDataCallException extends Exception {}

    /** Members variables **/
    private String brand;
    private String country;
    private List<String> matchingKeys;
    private Country_Info__c countryInfo;
    private Caller caller;
    @TestVisible private Map<String, IPDataMatcher> mapMatchingKeys;
    @TestVisible private String SuccessKey;

    public String getSuccessKey() {
        return SuccessKey;
    }

    /** Inner Class used to search Personal Data informations **/
    public class PersDataParameters {
        public String renaultPersDataId;
        public String brand;
        public String country;
        public String lastName;
        public String firstname;
        public String email;
        public String customerIdentificationNbr;
        public String vin;
        public String city;
        public String phoneNumber;
    }

    /**
    * @constructor
    * @author sebastien.ducamp@atos.net.hqmyrdev
    * @description  
    */
    public Myr_ManageAccount_PersDataCall_Cls(String brand, String country) {
        this.brand = brand;
        this.country = country;
        if( !String.isBlank(country) ) {
            countryInfo = Country_Info__c.getInstance(country);
        }
        caller = new Caller(brand, country);
        matchingKeys = null;
        system.debug('##Ludo CallWSMYR');
        loadMatchingKeys();
    }

    /** Load the Personal Data Matching Keys 
      * @return void
    **/
    private void loadMatchingKeys() {
        mapMatchingKeys = new Map<String, IPDataMatcher>();
        mapMatchingKeys.put( Keys.PID.name(), new Matcher_PID(caller) );
        mapMatchingKeys.put( Keys.FLE.name(), new Matcher_FLE(caller) );
        mapMatchingKeys.put( Keys.CFL.name(), new Matcher_CFL(caller) );
        mapMatchingKeys.put( Keys.FLcE.name(), new Matcher_FLcE(caller) );
        mapMatchingKeys.put( Keys.CcFL.name(), new Matcher_CcFL(caller) );
        mapMatchingKeys.put( Keys.FLcE_POL.name(), new Matcher_FLcE_POL(caller) );
        mapMatchingKeys.put( Keys.FLV.name(), new Matcher_FLV(caller) );
        mapMatchingKeys.put( Keys.FLP.name(), new Matcher_FLP(caller) );
    }

    /** @return Caller, the caller used for Personal Data **/
    public Caller getCaller() {
        return caller;
    }

    /* Specify a list of matching keys on the fly that will be taken in priority over the country configuration **/
    public void specifyMatchingKeyOnTheFly( List<String> listKeys ) {
        matchingKeys = listKeys;
    }

    public TRVResponseWebservices findCustomer(PersDataParameters iSearchParameters) {
        system.debug('### Myr_ManageAccount_PersDataCall_Cls - <findCustomer> - BEGIN');
        system.debug('### Myr_ManageAccount_PersDataCall_Cls - <findCustomer> - CountryInfo='+countryInfo);
        //Check entries
        system.debug('##Ludo findCustomer 1');
        if( null == countryInfo ) {
            throw new MyrPersDataCallException('No country info found for ' + iSearchParameters.country);
        }
        system.debug('##Ludo findCustomer 2');
        if( !countryInfo.Myr_EnablePersDataCall__c ) {
            throw new MyrPersDataCallException('Personal Data is deactivated for ' + iSearchParameters.country);
        }
        system.debug('##Ludo findCustomer 3');
        if( String.isBlank(countryInfo.DataSource__c) ) {
            throw new MyrPersDataCallException('No Personal Data source configured for ' + iSearchParameters.country);
        }
        system.debug('##Ludo findCustomer 4');
        if( String.isBlank(countryInfo.Myr_MatchingKeysPersData__c) ) {
            throw new MyrPersDataCallException('No Personal Data matching keys configured for ' + iSearchParameters.country);
        }
        system.debug('##Ludo findCustomer 5'); 
        
        //Prepare the keys
        if( null == matchingKeys ) {
            //no prioritary matching keys specified, use the country configuration
            system.debug('##Ludo no key');
            matchingKeys = countryInfo.Myr_MatchingKeysPersData__c.split(';');
        }
        system.debug('### Myr_ManageAccount_PersDataCall_Cls - <findCustomer> - matchingKeys=' + matchingKeys);
        for( Integer i = 0; i < matchingKeys.size(); ++i ) {
            String curKey = matchingKeys[i];
            system.debug('### Myr_ManageAccount_PersDataCall_Cls - <findCustomer> - treat key=' + curKey);
            IPDataMatcher matcher = mapMatchingKeys.get(curKey);
            if( null == matcher ) {
                throw new MyrPersDataCallException('No Personal Data matching keys found for country=' + country + ' and key=' + curKey);
            }
            TRVResponseWebservices pDataResponse = matcher.getPersData(iSearchParameters);
            if( null != pDataResponse ) {
                if( Test.isRunningTest() && (i == 0) && TestSkipMode.SKIP_1ST.name().equalsIgnoreCase(pDataResponse.response.infoClients[0].lastname1) ) {
                    continue; //skip this iteration and go the following one
                }
                //found a customer !! we can return it and stop the research
                SuccessKey = curKey;
                system.debug('##Ludo findCustomer 6 : ' + pDataResponse);
                return pDataResponse;
            }
        }
		system.debug('##Ludo findCustomer 6 null'); 
        //no one found
        return null;
    }

/***************************************************************************************************************************************/
/** Personal Data Matchers Implementation ***************************/

    public interface IPDataMatcher {
        //Caller getCaller();
        TRVResponseWebservices getPersData(PersDataParameters iSearchParameters);
    }
    
    public abstract class APDataMatcher implements IPDataMatcher {
        //members
        public Caller caller;
        //default constructor
        public APDataMatcher(Caller iCaller) {
            this.caller = iCaller;
        }
        abstract TRVResponseWebservices getPersData(PersDataParameters iSearchParameters);
        /** @return Caller used **/ 
        /*
        public Caller getCaller() {
            return caller;
        } 
        */
    }

    /** Matcher PID : Key1=Renault Personal Data Id / No Check
        Countries: All
        @return the single personal data response if found
    **/ 
    public class Matcher_PID extends APDataMatcher {
        public Matcher_PID(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
			system.debug('### Myr_ManageAccount_PersDataCall_Cls - <Matcher_PID> - getPersData, renaultPersDataId=' + iSearchParameters.renaultPersDataId );
			if( String.isBlank( iSearchParameters.renaultPersDataId ) ) {
				return null;
			}
            return caller.searchPID( iSearchParameters.renaultPersDataId );
        }
    }
    
    /** Matcher FLE : Key1=Firstname + Lastname + Email / No Check
        Countries: Russia(BCS)
        @return the single personal data response if found
    **/ 
    public class Matcher_FLE extends APDataMatcher {
        public Matcher_FLE(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchFLE( iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.email );
        }
    }
    
    /** Matcher CFL : Firstname + Lastname + CustomerIdentificationNumber
        Countries: Romania(BCS)
        @return the single personal data response if found
    **/ 
    public class Matcher_CFL extends APDataMatcher {
        public Matcher_CFL(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchCFL(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.customerIdentificationNbr);
        }
    }
    
    /** Matcher FLcE : Key1 = Firstname + Lastname => check Email in the output 
        Countries: 
        @return the single personal data response if found
    **/ 
    public class Matcher_FLcE extends APDataMatcher {
        public Matcher_FLcE(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchFLcE(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.email);
        }
    }
    
    /** Matcher FLcE_POL : Key1 = Firstname + Lastname => check Email in the output
        if the first call fails (not found) try to replace the end of the lastname ki by ka or replace ka by ki
        ( married women could change the end of their lastname by ka. this concerns ski, dzki, cki )
        Countries: Poland(RBX)
        @return the single personal data response if found
    **/
    public class Matcher_FLcE_POL extends APDataMatcher {
        public Matcher_FLcE_POL(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            TRVResponseWebservices pData = caller.searchFLcE(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.email);
            if( Test.isRunningTest() && pData != null ) {
                //skip the result to be able to test FLE after that
                if( TestSkipMode.SKIP_FLcE.name().equalsIgnoreCase(pData.response.infoClients[0].lastname1) ) {
                    pData = null;
                }
            }
            if( pData == null ) {
                //try to replace the end of the name ki by ka or ka by ki
                if( iSearchParameters.lastname !=  null ) {
                    String lastname_pol = null;
                    if( iSearchParameters.lastname.endsWithIgnoreCase('ki') ) {
                        lastname_pol = iSearchParameters.lastname.left(iSearchParameters.lastname.length() - 2) + 'ka';
                    } else if( iSearchParameters.lastname.endsWithIgnoreCase('ka') ) {
                        lastname_pol = iSearchParameters.lastname.left(iSearchParameters.lastname.length() - 2) + 'ki';
                    }
                    if( !String.isBlank(lastname_pol) ) {
                        pData = caller.searchFLcE(iSearchParameters.firstname, lastname_pol, iSearchParameters.email);  
                    }
                }
            }
            return pData;
        }
    }
    
    /** Matcher CcFL_FLcE : CustomerIdentificationNumber => check the Firstname + Lastname
        Countries: Italy(RBX)
        @return the single personal data response if found
    **/
    public class Matcher_CcFL extends APDataMatcher {
        public Matcher_CcFL(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchCcFL(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.customerIdentificationNbr);
        }
    }

    /** Matcher FLV : Firstname + LAstname + Vin => no check in output
        Countries: All the countries using MDM
        @return the single personal data response if found
    **/
    public class Matcher_FLV extends APDataMatcher {
        public Matcher_FLV(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchFLV(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.vin);
        }
    }

    /** Matcher FLP : Firstname + LAstname + PhoneNumber => no check in output
        Countries: All the countries using MDM
        @return the single personal data response if found
    **/
    public class Matcher_FLP extends APDataMatcher {
        public Matcher_FLP(Caller iCaller) {
            super(iCaller);
        }
        public override TRVResponseWebservices getPersData(PersDataParameters iSearchParameters) {
            return caller.searchFLP(iSearchParameters.firstname, iSearchParameters.lastname, iSearchParameters.phoneNumber);
        }
    }

/***************************************************************************************************************************************/
/** Personal Data Caller ***************************/

    //Listing of the tries with the raw responses given
    public class CallTry {
        public String rawQuery;
        public String rawResponse;
    }
    
    /** class to encapsulate the webservice calls **/   
    public class Caller {
        public List<CallTry> callTries;
        /** basic parameters **/
        private String country;
        private String brand;
        private Myr_PersonalData_WS.CALL_MODE callMode = Myr_PersonalData_WS.CALL_MODE.MYRENAULT;
        private CS04_MYR_Settings__c myrSettings;
        /** default constructor **/
        public Caller(String iBrand, String iCountry) {
            callTries = new List<CallTry>();
            country = iCountry;
            brand = iBrand;
            myrSettings = CS04_MYR_Settings__c.getInstance();
        }
        // ------------------------------------- private methods
        /** Init the request to use **/
        private TRVRequestParametersWebServices.RequestParameters initRequest() {
            TRVRequestParametersWebServices.RequestParameters defaultRequest = new TRVRequestParametersWebServices.RequestParameters();
           // defaultRequest.callMode = decimal.valueof(string.valueof(callMode));
            defaultRequest.country = country;
            defaultRequest.brand = brand;
          //  defaultRequest.mode = Myr_PersonalData_WS.PDATA_MODE.MODE_0;
            defaultRequest.nbReplies = '1';
            if( myrSettings != null ) {
                if( Myr_MyRenaultTools.Brand.Dacia.name().equalsIgnoreCase(brand) ) {
                    defaultRequest.demander = myrSettings.Myd_PersDataDemander__c;
                } else if( Myr_MyRenaultTools.Brand.Renault.name().equalsIgnoreCase(brand) ) {
                    defaultRequest.demander = myrSettings.Myr_PersDataDemander__c;
                }
            } else {
                defaultRequest.demander = 'DefaultMyR';
            }
            return defaultRequest;
        } 
        /** Call the webservice **/
        private TRVResponseWebservices searchData(TRVRequestParametersWebServices.RequestParameters req) {
            system.debug(' ### Myr_ManageAccount_PersDataCall_Cls - <searchData> - requests=' + req.toString());
            if( req == null ) return null;
            CallTry call = new CallTry();
            call.rawQuery = req.toString();
            TRVResponseWebservices response = null;
            try { 
                response = Myr_PersonalData_WS.getPersonalData(req);
                if( response != null ) {
                call.rawResponse = response.toString();
                } else {
                    call.rawResponse = 'no response';
                }
            } catch (Exception e) {
                call.rawResponse = 'no response, exception='+e.getMessage();
            } 
            callTries.add(call);
            system.debug(' ### Myr_ManageAccount_PersDataCall_Cls - <searchData> - response=' + ((null==response)?'null' : response.toString()));
            return response;
        }

        // ------------------------------------- public researchers 
        /** Search in Personal data for the given Renault Personal Data Identified, no check in the response 
            @return the single personal data response if found **/
        public TRVResponseWebservices searchPID(String renaultPersDataId ) {
            TRVRequestParametersWebServices.RequestParameters request = initRequest();
            request.idClient = renaultPersDataId;
            TRVResponseWebservices persData = searchData(request);
            if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                return persData;
            }
            return null;
        } 


        /** Search in Personal data for the given Firstname, Lastname and Email, no check in the response 
            @return the single personal data response if found **/
        public TRVResponseWebservices searchFLE(String firstname, String lastname, String email) {
            TRVRequestParametersWebServices.RequestParameters request = initRequest();
            request.firstname = firstname;
            request.lastname = lastname;
            request.email = email;
            TRVResponseWebservices persData = searchData(request);
            if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                return persData;
            }
            return null;
        } 

        /** Search in Personal data for the given Firstname and Lastname, check the email in the response 
        @return the single personal data response if found **/
        public TRVResponseWebservices searchFLcE(String firstname, String lastname, String email) {
            TRVRequestParametersWebServices.RequestParameters request = initRequest();
            request.firstname = firstname;
            request.lastname = lastname;
            TRVResponseWebservices persData = searchData(request);
            if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                if( email != null ) {
                    if( email.equalsIgnoreCase(persData.response.infoClients[0].email) ) {
                        return persData;
                    }
                } else if( persData.response.infoClients[0].email == null ) {
                    return persData;
                } 
            }
            return null;
        }

        /** Search into Personal Data for the customer identification number + Firstname + Lastname
        @return the single personal data response if found **/
        public TRVResponseWebservices searchCFL(String firstname, String lastname, String custIdentNumber) {
            TRVRequestParametersWebServices.RequestParameters request = initRequest();
            request.firstname = firstname;
            request.lastname = lastname;
            request.ident1 = custIdentNumber;
            
            TRVResponseWebservices persData = searchData(request);
            if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                return persData; 
            }
            return null;
        }

        /** Search into Personal Data for the customer identification number and check the firstname and lastname in the response
        @return the single personal data response if found **/
        public TRVResponseWebservices searchCcFL(String firstname, String lastname, String custIdentNumber) {
            system.debug(' ### Myr_ManageAccount_PersDataCall_Cls - <searchCcFL> - firstname='+firstname+', lastname='+lastname+' custIdentNumber='+custIdentNumber);
            TRVRequestParametersWebServices.RequestParameters request = initRequest();
            request.ident1 = custIdentNumber;
            
            TRVResponseWebservices persData = searchData(request);
            
            if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                system.debug(' ### Myr_ManageAccount_PersDataCall_Cls - <searchCcFL> - firstname='+firstname+', lastname='+lastname
                            +', persData.firstname='+persData.response.infoClients[0].firstname1+', persData.lastname='+persData.response.infoClients[0].lastname1);  
                if( firstname != null && lastname != null ) {
                    if( firstname.equalsIgnoreCase(persData.response.infoClients[0].firstname1) && lastname.equalsIgnoreCase(persData.response.infoClients[0].lastname1) ) {
                        return persData;            
                    }
                }
            }
            return null;
        }

        /** Search in Personal data for the given Firstname, Lastname and Vin, no check in the response 
            @return the single personal data response if found **/
        public TRVResponseWebservices searchFLV(String firstname, String lastname, String vin) {
            system.debug('### Myr_ManageAccount_PersDataCall_Cls - <searchFLV> - firstname='+firstname+', lastname='+lastname+', vin='+vin);
            if( !String.isBLank(vin) ) {
                TRVRequestParametersWebServices.RequestParameters request = initRequest();
                request.firstname = firstname;
                request.lastname = lastname;
                request.vin = vin;
                TRVResponseWebservices persData = searchData(request);
                if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                    return persData;
                }
            } else {
                system.debug('### Myr_ManageAccount_PersDataCall_Cls - <searchFLP> - No Vin for MDM - Personal Data not called');
            }
            return null;
        }

        /** Search in Personal data for the given Firstname, Lastname and PhoneNumber, no check in the response 
            @return the single personal data response if found **/
        public TRVResponseWebservices searchFLP(String firstname, String lastname, String phoneNumber) {
            system.debug('### Myr_ManageAccount_PersDataCall_Cls - <searchFLP> - firstname='+firstname+', lastname='+lastname+', phoneNumber='+phoneNumber);
            if( !String.isBLank(phoneNumber) ) {
                TRVRequestParametersWebServices.RequestParameters request = initRequest();
                request.firstname = firstname;
                request.lastname = lastname;
                request.strFixeLandLine = phoneNumber;
                TRVResponseWebservices persData = searchData(request);
                if( persData != null && persData.response != null && persData.response.infoClients != null && persData.response.infoClients.size() == 1 ) {
                    return persData;
                }
            } else {
                system.debug('### Myr_ManageAccount_PersDataCall_Cls - <searchFLP> - No PhoneNumber for MDM - Personal Data not called');
            }
            return null;
        } 
    }
    
}