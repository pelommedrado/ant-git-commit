public with sharing class VFC126_CampaignConfig
{

    public static final Set<String> setMarketingProfiles = 
        new Set<String>{'Marketing Renault', 'MKT - Marketing Administrator'};
        
    public static final Set<String> setDealerProfiles = 
            new Set<String>{'SFA - Seller', 'Renault mais cliente - sfa 2.0', 'Assistente', 'SFA - Receptionist', 'SFA - Dealer VN Manager','SFA - Dealer Administrator','SFA - Dealer VN Manager Not SFA'};

	public static final Set<String> setInterfaceProfiles = 
		new Set<String>{'<Renault> - Interface'};

    public static final Set<String> setDataLoaderUsers = 
        //new Set<String>{'Data Loader','Rodrigo Silva'};
        new Set<String>{'Data Loader'};
       
    // Used as a separator for campaign members
    public static final String CAMPAIGN_CHAR_SEPARATOR = '@';

    public VFC126_CampaignConfig() {}
}