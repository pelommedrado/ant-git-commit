public with sharing class TR001Case {

    public static void calculateCaseAge(List<Case> lstCases)
    {       
        for(Case mycase : lstCases)
        {
            mycase.REP_DurationInHours__c = BusinessHours.diff(mycase.BusinessHoursId, mycase.CreatedDate, System.now())/3600000.0;
        }
            
    }

}