/**
*   WebService Class    -   WSC03_WebServiceTestDrive
*   Author              -   Suresh Babu
*   Date                -   07/11/2012
*   
*   #01 <Suresh Babu> <07/11/2012>
*       Web service class to schedule Test drive for Customer.
*   
*   #02 <Suresh Babu> <28/11/2012>
*       Add functionality to through error messages with error code mentioned by Jorge.
*       
**/
global class WSC03_WebServiceTestDrive
{

    /**
    *   This Method was used get all Dealers in selected City and State.
    *   @param  model       -   use vehicleModel, entered by customer to refine search.
    *   @param  town        -   Customer City.
    *   @param  stateCode   -   Customer State Code.
    *   @return lstWSTestDriveWrapper   -   List of Test Drive Wrapper result to select the Dealer for Test Drive.
    **/
    webservice static List<WSC03_WSTestDriveAccountVO> availabilityOfVehicles (String model, String town, String stateCode)
    {
        system.debug('*** availabilityOfVehicles()');

        List<WSC03_WSTestDriveAccountVO> lstWSTestDriveWrapper = new List<WSC03_WSTestDriveAccountVO>();
        Set<Id> setAccountId = new Set<Id>();
        
        Set<Account> setAccountRecords = new Set<Account>();
        List<TDV_TestDriveVehicle__c> lstTestDriveVehicleRecords = new List<TDV_TestDriveVehicle__c>();
        
        
        String errorMsg = null;

        //  Check vehicle model
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkVehicleModel(model);
        if (errorMsg != null)
            return buildErrorMessageForAvailabilityOfVehicles(errorMsg);

        //  Check town is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkTown(town);
        if (errorMsg != null)
            return buildErrorMessageForAvailabilityOfVehicles(errorMsg);

        // Check state code is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkStateCode(town, stateCode);
        if (errorMsg != null)
            return buildErrorMessageForAvailabilityOfVehicles(errorMsg);
        
        // Get list of TestDriveVehicle records using model, town and statecode
        try
        {
            lstTestDriveVehicleRecords = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicleRecordsUsingCriteria(model, town, stateCode);

            for (TDV_TestDriveVehicle__c testDrive : lstTestDriveVehicleRecords)
            {    
                if (!setAccountId.contains(testDrive.Account__c))
                {
                    WSC03_WSTestDriveAccountVO accountWrapperRecord = new WSC03_WSTestDriveAccountVO();    
                    accountWrapperRecord.accountId          =   testDrive.Account__r.Id;
                    accountWrapperRecord.accountName        =   testDrive.Account__r.Name;
                    accountWrapperRecord.accountStreet      =   testDrive.Account__r.ShippingStreet;
                    accountWrapperRecord.accountCity        =   testDrive.Account__r.ShippingCity;
                    accountWrapperRecord.accountState       =   testDrive.Account__r.ShippingState;
                    accountWrapperRecord.accountPostalCode  =   testDrive.Account__r.ShippingPostalCode;
                    accountWrapperRecord.accountCountry     =   testDrive.Account__r.ShippingCountry;
                    lstWSTestDriveWrapper.add(accountWrapperRecord);
                }
                setAccountId.add(testDrive.Account__c);
            }
            return lstWSTestDriveWrapper;
        }
        catch (Exception e) {
            return buildErrorMessageForAvailabilityOfVehicles('Unhandled error: ' + e);
        }
    }


    /**
    *   This Method was used get the Scheduled Hours of Selected Dealer.
    *   @param  accountId           -   Dealer id to get the Scheduled information.
    *   @param  vehicleModel        -   Selected Car model.
    *   @param  dateOfBooking       -   Date of Booking for Test Drive.
    *   @return hoursAvailable      -   Scheduled Hours for Dealer. 
    **/
    webservice static List<WSC03_WSTestDriveHoursAvailableVO> ConsultationOfAgenda (String accountId, String vehicleModel, String dateOfBooking)
    {
        List<WSC03_WSTestDriveHoursAvailableVO> lstVOAvailable = new List<WSC03_WSTestDriveHoursAvailableVO>();

        String errorMsg = null;

        // Validate accountId parameter
        errorMsg = WSC03_WebServiceTestDriveValidationBO.validateAccountId(accountId);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgenda(errorMsg);
        
        // Validate if the account Id exists in SFDC
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkAccountExistsInSalesforce(accountId);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgenda(errorMsg);

        //  Check vehicle model
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkVehicleModel(vehicleModel);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgenda(errorMsg);

        // Check dateOfBooking
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkDate(dateOfBooking);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgenda(errorMsg);

        // Convert dates from String to Date 
        Date startBookingDate = VFC69_Utility.convertStringToDate(dateOfBooking);

        // Fetch the hours available for this account
        List<String> rangeOfWorkingHours = VFC12_AccountDAO.getInstance().fetchTimeRangeForTestDrive(accountId);

        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        List<String> hoursAvailable = null;
        List<String> lstHoursAvailable = null;
        try
        {
            // Fetch the test drive vehicle availability and build a list of available dates            
            lstHoursAvailable = WSC03_WebServiceTestDriveBO.getInstance().fetchTestDriveVehicleAvailability(rangeOfWorkingHours, accountId,
                vehicleModel, startBookingDate, startBookingDate, false);
            system.debug('*** lstHoursAvailable=' + lstHoursAvailable);
            
            // Return the available dates
            if (lstHoursAvailable.size() > 0)
            {
                for (String available : lstHoursAvailable)
                {
                    //  Add available time to List of String
                    WSC03_WSTestDriveHoursAvailableVO hourAvailability = new WSC03_WSTestDriveHoursAvailableVO();
                    hourAvailability.HourAvailable =  available;
                    lstVOAvailable.add(hourAvailability);
                }
            }
            else {
                return buildErrorMessageForConsultationOfAgenda('No hours available for this model vehicle at this dealer.');
            }
        }
        catch (Exception e) {
            system.debug('*** ERROR:'+e.getStackTraceString());
            return buildErrorMessageForConsultationOfAgenda('Unhandled error: ' + e);
        }
        
        return lstVOAvailable;
    }


    /**
    *   This Method was used Schedule the Test Drive for Customer.
    *   @param  vehicleModel    -   use vehicleModel, entered by customer to refine search.
    *   @param  accountId    -   Selected accountId for Test Drive booking
    *   @param  dateOfBooking   -   Date of Booking : comes in the GMT of the user
    *   @param  firstName   -   Customer First Name
    *   @param  lastName    -   Customer Last Name
    *   @param  email       -   Customer email id
    *   @param  postalCode  -   Customer postal code
    *   @param  CPF         -   Customer CPF (NUMCL)
    *   @param  phone       -   Customer personal landline
    *   @return lstScheduledInformation     -   Success or Failure message from WSC03_WSTestDriveScheduleVO.
    **/
    webservice static List<WSC03_WSTestDriveScheduleVO> SchedulingTestDrive(String vehicleModel, String accountId, 
        String dateOfBooking, String firstName, String lastName, String email, String postalCode, String cpf, String phone)
    {
        system.debug('*** SchedulingTestDrive()');

        List<WSC03_WSTestDriveScheduleVO> lstScheduledInformation = new List<WSC03_WSTestDriveScheduleVO>();
        
        // Initialize SavePoint object
        Savepoint sp = Database.setSavepoint();

        String errorMsg = null;

        // Validate accountId parameter
        errorMsg = WSC03_WebServiceTestDriveValidationBO.validateAccountId(accountId);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);
        
        // Validate if the account Id exists in SFDC
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkAccountExistsInSalesforce(accountId);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);
              
        //  Check vehicle model
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkVehicleModel(vehicleModel);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check first Name is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkFirstName(firstName);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check Last Name is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkLastName(lastName);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check Email is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkEmail(email);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check PostalCode is null
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkPostalCode(postalCode);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);
        
        // Check date of Booking
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkDateTime(dateOfBooking);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check if cpf is null or blank
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkCPF(cpf);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check if phone is null or blank
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkPhone(phone);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Get list start and end hour for scheduling a test drive
        List<Integer> lstWorkingHours = new List<Integer>();
        List<String> RangeOfWorkingHours = VFC12_AccountDAO.getInstance().fetchTimeRangeForTestDrive(accountId);
        for (String s : RangeOfWorkingHours)
        {
            if (s.contains(':')) {
                s = s.replace(':', '');
                Integer n = Integer.valueOf(s);
                lstWorkingHours.add(n);
            }
        }
        
        // Convert date of booking to date
        List<String> lstDateAndTime = dateOfBooking.split(' ');
        List<String> lstDateFormated = lstDateAndTime[0].split('/');
        List<String> lstTimeFormated = lstDateAndTime[1].split(':');
        Datetime dateTimeForBooking = Datetime.newInstance(Integer.valueOf(lstDateFormated[0]), Integer.valueOf(lstDateFormated[1]), Integer.valueOf(lstDateFormated[2]), Integer.valueOf(lstTimeFormated[0]), Integer.valueOf(lstTimeFormated[1]), 0);
        
        // Check if the date of booking didn't passed
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkDateOfBookingDidntPassed(dateTimeForBooking);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        // Check if the spot is available
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkIfSpotIsAvailable(lstWorkingHours, vehicleModel, dateTimeForBooking, accountId, lstTimeFormated);
        if (errorMsg != null)
            return buildErrorMessageForSchedulingTestDrive(errorMsg);

        try
        {
            /* get record type id using RecordType developer name */
            Id recordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
            
            Account accountRecord = new Account();
            Account dealerAccount = new Account();

            // Now, search using CPF
            accountRecord = VFC12_AccountDAO.getInstance().fetchAccountUsingCPF(cpf, recordTypeId);

            /*  fetch dealer account  using dealer id */
            dealerAccount = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(accountId);
			System.debug('###!!! accountRecord '+accountRecord);
            /* if check  account record is not null */
            if (accountRecord != null)
            {
                if (dealerAccount.isDealerActive__c == false)
                {
                    // For a non-partner Dealer, send just an email with the test drive data
                    system.debug('*** 1.isDealerActive__c == false');
                    String name = firstName + ' ' + lastName;
                    
                    // Send email to customer
                    String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template05', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, accountRecord.Id);
                    system.debug('*** errorMessageCustomer:'+errorMessageCustomer);

                    // Send email to dealer
                    String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template07', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking,accountRecord.Id);
                    system.debug('*** errorMessage:'+errorMessage);

                    if (errorMessage == '') {
                        WSC03_WSTestDriveScheduleVO schedule = new WSC03_WSTestDriveScheduleVO();
                        schedule.result = 'OK';
                        lstScheduledInformation.add( schedule );
                        return lstScheduledInformation;
                    }
                    else {
                        return buildErrorMessageForSchedulingTestDrive('Unhandled error: ' + errorMessage);
                    }
                    
                }
                else
                {
                    system.Debug('*** 1.isDealerActive__c == true');

                    Double productAmount = 0.0;
                    /* if check  account VehicleInterest is not null */
                    if(accountRecord.VehicleInterest_BR__c != null){
                         /*  fetch product record  using VehicleInterest */
                        List<Product2> product = VFC22_ProductDAO.getInstance().findProductRecordsbyName(accountRecord.VehicleInterest_BR__c);
                        system.Debug(' Product -->'+product);
                        /* if check product is not empty */
                        if(!product.isEmpty()){
                            /*  fetch price book entry records  using Product Id */
                            PricebookEntry price = VFC24_PriceBookDAO.getInstance().findPricebyProductId(product[0].Id);
                            system.Debug(' price book -->'+price);
                            /* if price book entry not equal to null */
                            if(Price != null){
                                productAmount = price.UnitPrice;
                            }
                        }
                    }
                    /* Initialize opportuinty object */
                    Opportunity opportunityRecord = new Opportunity();
                    /* concadinate the Name, vehicleModel and account name and then assign opportunity objects */
                    opportunityRecord.Name = Label.OPPName + ' ' + '(' + accountRecord.Name + ')'; 
                    opportunityRecord.StageName = 'Identified' ;
                    opportunityRecord.CloseDate = Date.today() + 30;
                    opportunityRecord.Dealer__c = dealerAccount.Id;
                    opportunityRecord.AccountId = accountRecord.Id;
                    opportunityRecord.OpportunitySource__c = 'INTERNET';
                    opportunityRecord.OpportunitySubSource__c = 'RENAULT SITE';
                    opportunityRecord.Detail__c = 'TEST DRIVE';
                    opportunityRecord.OwnerId = dealerAccount.OwnerId;
                    opportunityRecord.Amount = productAmount;
                    opportunityRecord.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER;
                    
                    /* insert the opportunity  */
                    VFC23_OpportunityDAO.getInstance().insertData( opportunityRecord );
                    
                    system.Debug(' Dealer Id -->'+accountId);
                    system.Debug(' car Model -->'+vehicleModel);
                    
                    /* fetch testdrivevehicle records using dealer id and car model */
                    TDV_TestDriveVehicle__c testDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingDealerId( dealerAccount.Id, vehicleModel ); 
                    system.debug('*** testDriveVehicle.Id:'+testDriveVehicle.Id);
                    system.debug('*** accountRecord.Id:'+accountRecord.Id);
                    
                    /* initialize the TestDrive Object */
                    TDV_TestDrive__c testDrive = new TDV_TestDrive__c();
                    testDrive.Opportunity__c = opportunityRecord.Id;
                    testDrive.DateBooking__c = dateTimeForBooking;
                    testDrive.Customer__c = accountRecord.Id;
                    
                    // 31/01/2013 16:20
                    // Modificado por Mimica
                    // Solicitado por Thiago
                    // Não será usado o ID da Concessionária preferida mais
                    // O ID será o passado via WS
                    //testDrive.Dealer__c = accountRecord.IDBIRPrefdDealer__c;
                    testDrive.Dealer__c = dealerAccount.Id;
                    
                    testDrive.TestDriveVehicle__c = testDriveVehicle.Id;
                    /* insert the TestDrive Object record */
                    VFC28_TestDriveDAO.getInstance().insertData( testDrive );

                    String name = firstName + ' ' + lastName;

                    // Send email to customer
                    String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template04', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, accountRecord.Id);
                    system.debug('*** errorMessageCustomer:'+errorMessageCustomer);

                    // Send email to dealer
                    String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template06', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, accountRecord.Id);
                    system.debug('*** errorMessage:'+errorMessage);
                    
                    WSC03_WSTestDriveScheduleVO schedule = new WSC03_WSTestDriveScheduleVO();
                    schedule.result = 'OK';
                    lstScheduledInformation.add( schedule );
                    return lstScheduledInformation;
                }
            }
            else{
                accountRecord = new Account();
                
                accountRecord.FirstName = firstName;
                accountRecord.LastName = lastName;
                accountRecord.Source__c = 'INTERNET';
                accountRecord.AccSubSource__c = 'RENAULT SITE';
                accountRecord.Detail__c = 'TESTE DRIVE';
                accountRecord.PersEmailAddress__c = email;
                accountRecord.BillingPostalCode = postalCode;
                accountRecord.CustomerIdentificationNbr__c = cpf;
                accountRecord.PersLandline__c = phone;
                accountRecord.VehicleInterest_BR__c = vehicleModel;
                accountRecord.IDBIRPrefdDealer__c = dealerAccount.Id;
                accountRecord.RecordTypeId = recordTypeId;
                
                 /* insert the Account Object record */
                Database.Saveresult accountSaveResult = VFC12_AccountDAO.getInstance().insertData( accountRecord );
                system.Debug(' accountSaveResult -->'+accountSaveResult);
                system.Debug(' accountRecord -->'+accountRecord);

                if (dealerAccount.isDealerActive__c == false)
                {
                    system.Debug('*** 2.isDealerActive__c == false');
                    
                    String name = firstName + ' ' + lastName;
                    
                    // Send email to customer
                    String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template05', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, accountRecord.Id);
                    system.debug('*** errorMessageCustomer:'+errorMessageCustomer);

                    // Send email to dealer
                    String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template07', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, accountRecord.Id);
                    system.debug('*** errorMessage:'+errorMessage);
                    
                  
                    
                    if (errorMessage == '') {
                        WSC03_WSTestDriveScheduleVO schedule = new WSC03_WSTestDriveScheduleVO();
                        schedule.result = 'OK';
                        lstScheduledInformation.add( schedule );
                        return lstScheduledInformation;
                    }
                    else {
                        return buildErrorMessageForSchedulingTestDrive('Unhandled error: ' + errorMessage);
                    }
                }
                else
                {
                    system.Debug('*** 2.isDealerActive__c == true');

                    Double productAmount = 0.0;
                    /*  fetch product record  using VehicleInterest */
                    if(accountRecord.VehicleInterest_BR__c != null){
                        List<Product2> product = VFC22_ProductDAO.getInstance().findProductRecordsbyName(accountRecord.VehicleInterest_BR__c);
                         /* if check product is not empty */
                        if(!product.isEmpty()){
                            /*  fetch price book entry records  using Product Id */
                            PricebookEntry price = VFC24_PriceBookDAO.getInstance().findPricebyProductId(product[0].Id);
                             /* if price book entry not equal to null */
                            if(Price != null){
                                productAmount = price.UnitPrice;
                            }
                        }
                    }
                    /* Initialize opportuinty object */
                    Opportunity opportunityRecord = new Opportunity();
                    /* concadinate the Name, vehicleModel and account name and then assign opportunity objects */
                    opportunityRecord.Name = Label.OPPName + ' ' + '(' + accountRecord.FirstName + ' ' + accountRecord.LastName + ')'; 
                    opportunityRecord.StageName = 'Identified' ;
                    opportunityRecord.CloseDate = Date.today() + 30;
                    opportunityRecord.Dealer__c = dealerAccount.Id;
                    opportunityRecord.AccountId = accountRecord.Id;
                    opportunityRecord.OpportunitySource__c = 'INTERNET';
                    opportunityRecord.OpportunitySubSource__c = 'RENAULT SITE';
                    opportunityRecord.Detail__c = 'TEST DRIVE';
                    opportunityRecord.OwnerId = dealerAccount.OwnerId;
                    opportunityRecord.Amount = productAmount;
                    opportunityRecord.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER;
                    
                    system.debug(' opportunityRecord --> '+opportunityRecord);
                    /* insert the opportunity  */
                    VFC23_OpportunityDAO.getInstance().insertData( opportunityRecord );
                    
                    /* fetch testdrivevehicle records using dealer id and car model */
                    TDV_TestDriveVehicle__c testDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().fetchTestDriveVehicle_UsingDealerId( dealerAccount.Id, vehicleModel );
                    if (testDriveVehicle == null) {
                        return buildErrorMessageForSchedulingTestDrive('Unhandled error: Veículo não encontrado');
                    }
                    else {
                    
                        /* initialize the TestDrive Object */
                        TDV_TestDrive__c testDrive = new TDV_TestDrive__c();
                        testDrive.Opportunity__c = opportunityRecord.Id;
                        testDrive.DateBooking__c = dateTimeForBooking;
                        testDrive.Customer__c = accountRecord.Id;
                        testDrive.Dealer__c = accountRecord.IDBIRPrefdDealer__c;
                        testDrive.TestDriveVehicle__c = testDriveVehicle.Id;
                        
                        system.debug(' testDrive Record --> '+testDrive);
                        /* insert the TestDrive Object record */
                        VFC28_TestDriveDAO.getInstance().insertData( testDrive );
                        
                        String name = firstName + ' ' + lastName;

                        // Send email to customer
                        String errorMessageCustomer = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template04', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, null);
                        system.debug('*** errorMessageCustomer:'+errorMessageCustomer);
    
                        // Send email to dealer
                        String errorMessage = VFC119_TestDriveEmailBO.getInstance().sendTestDriveEmail('template06', dealerAccount, name, email, phone, postalCode, vehicleModel, dateTimeForBooking, null);
                        system.debug('*** errorMessage:'+errorMessage);

                        WSC03_WSTestDriveScheduleVO schedule = new WSC03_WSTestDriveScheduleVO();
                        schedule.result = 'OK';
                        lstScheduledInformation.add( schedule );
                        return lstScheduledInformation;
                    }
                }
            }
        }
        catch (Exception e) {
            Database.rollback(sp);
            system.debug('*** ERROR:' + e.getStackTraceString());
            return buildErrorMessageForSchedulingTestDrive('Unhandled error: ' + e.getMessage());
        }
    }
    

    /**
    *   This Method was used get the Scheduled Hours of Selected Dealer using a range of dates
    *   @param  accountId           -   Dealer id to get the Scheduled information.
    *   @param  vehicleModel        -   Selected Car model.
    *   @param  stardDateOfBooking  -   Start date of Booking for Test Drive.
    *   @param  endDateOfBooking    -   End date of Booking for Test Drive.
    *   @return hoursAvailable      -   Scheduled days/Hours for Dealer. 
    **/
    webservice static List<WSC03_WSTestDriveHoursAvailableVO> ConsultationOfAgendaByRange(String accountId,
        String vehicleModel, String startDateOfBooking, String endDateOfBooking)
    {
        system.debug('*** ConsultationOfAgendaByRange()');
        
        List<WSC03_WSTestDriveHoursAvailableVO> lstVOAvailable = new List<WSC03_WSTestDriveHoursAvailableVO>();
        
        String errorMsg = null;

        // Validate accountId parameter
        errorMsg = WSC03_WebServiceTestDriveValidationBO.validateAccountId(accountId);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgendaByRange(errorMsg);
        
        // Validate if account Id exists in SFDC
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkAccountExistsInSalesforce(accountId);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgendaByRange(errorMsg);

        //  Check vehicle model
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkVehicleModel(vehicleModel);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgendaByRange(errorMsg);

        // Check startDateOfBooking and endDateOfBooking
        errorMsg = WSC03_WebServiceTestDriveValidationBO.checkDatesOfBooking(startDateOfBooking, endDateOfBooking);
        if (errorMsg != null)
            return buildErrorMessageForConsultationOfAgendaByRange(errorMsg);

        // Convert dates from String to Date 
        Date startBookingDate = VFC69_Utility.convertStringToDate(startDateOfBooking);
        Date endBookingDate = VFC69_Utility.convertStringToDate(endDateOfBooking);

        // Fetch the hours available for this account
        List<String> rangeOfWorkingHours = VFC12_AccountDAO.getInstance().fetchTimeRangeForTestDrive(accountId);
        system.debug('*** rangeOfWorkingHours='+rangeOfWorkingHours);

        List<TDV_TestDrive__c> lstTestDriveRecords = null;
        List<String> hoursAvailable = null;

        try
        {
            // Fetch the test drive vehicle availability and build a list of available dates            
            List<String> lstHoursAvailable = WSC03_WebServiceTestDriveBO.getInstance().fetchTestDriveVehicleAvailability(rangeOfWorkingHours, accountId,
                vehicleModel, startBookingDate, endBookingDate, true);
            
            // Return the available dates
            if (lstHoursAvailable.size() > 0)
            {
                for (String available : lstHoursAvailable)
                {
                    //  Add available time to List of String
                    WSC03_WSTestDriveHoursAvailableVO hourAvailability = new WSC03_WSTestDriveHoursAvailableVO();
                    hourAvailability.HourAvailable =  available;
                    lstVOAvailable.add(hourAvailability);
                }
            }
            else {
                return buildErrorMessageForConsultationOfAgenda('No hours available for this model vehicle at this dealer.');
            }
        }
        catch (Exception e) {
            system.debug('*** ERROR:'+e.getStackTraceString());
            return buildErrorMessageForConsultationOfAgenda('Unhandled error: ' + e);
        }
        
        return lstVOAvailable;
    }


    //
    // Return an object with the error message
    private static List<WSC03_WSTestDriveAccountVO> buildErrorMessageForAvailabilityOfVehicles(String errorMsg)
    {
        List<WSC03_WSTestDriveAccountVO> lstWSTestDriveVO = new List<WSC03_WSTestDriveAccountVO>();
        WSC03_WSTestDriveAccountVO accountVO = new WSC03_WSTestDriveAccountVO();
        accountVO.errorMessage = errorMsg;
        lstWSTestDriveVO.add(accountVO);
        return lstWSTestDriveVO;
    }
    
    //
    // Return an object with the error message
    private static List<WSC03_WSTestDriveHoursAvailableVO> buildErrorMessageForConsultationOfAgenda(String errorMsg)
    {
        List<WSC03_WSTestDriveHoursAvailableVO> lstHoursAvailable = new List<WSC03_WSTestDriveHoursAvailableVO>();
        WSC03_WSTestDriveHoursAvailableVO hourAvailability = new WSC03_WSTestDriveHoursAvailableVO();
        hourAvailability.errorMessage = errorMsg;
        lstHoursAvailable.add(hourAvailability);
        return lstHoursAvailable;   
    }

    //
    // Return an object with the error message
    private static List<WSC03_WSTestDriveScheduleVO> buildErrorMessageForSchedulingTestDrive(String errorMsg)
    {
        List<WSC03_WSTestDriveScheduleVO> lstScheduledInformation = new List<WSC03_WSTestDriveScheduleVO>();
        WSC03_WSTestDriveScheduleVO schedule = new WSC03_WSTestDriveScheduleVO();
        schedule.errorMessage = errorMsg;
        lstScheduledInformation.add(schedule);
        return lstScheduledInformation;
    }
    
    // Return an object with the error message
    private static List<WSC03_WSTestDriveHoursAvailableVO> buildErrorMessageForConsultationOfAgendaByRange(String errorMsg)
    {
        List<WSC03_WSTestDriveHoursAvailableVO> lstHoursAvailable = new List<WSC03_WSTestDriveHoursAvailableVO>();
        WSC03_WSTestDriveHoursAvailableVO hourAvailability = new WSC03_WSTestDriveHoursAvailableVO();
        hourAvailability.errorMessage = errorMsg;
        lstHoursAvailable.add(hourAvailability);
        return lstHoursAvailable;   
    }
    
 

}