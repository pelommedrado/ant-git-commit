@isTest
public class ScheduleSynchronizeAOCTest {
    
    public static testMethod void testCallout() {
    	
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ChamadaServicoFakeAOCTest());
        
        // Call ScheduleSynchronizeAOC batch and use callout
        ScheduleSynchronizeAOC call = new ScheduleSynchronizeAOC();
        Database.executeBatch(call,1);

    }
    
}