@isTest
private class VFC137_ZipCodeService_Test
{
	private static testMethod void unitTest01()
	{
		// Test Data
		ZipCodeBase__c zip = VFC129_UTIL_TestData.createZipCodeBase('22000100');
		insert zip;

		// Start test
        Test.startTest();

        // #Test 01
        ZipCodeBase__c test01 = VFC137_ZipCodeService.getAddressGivenZipCode('22000100');
        ZipCodeBase__c test02 = VFC137_ZipCodeService.getAddressGivenZipCode('');

        // Stop test
        Test.stopTest();

        // Verify data
        system.assert(test01 != null);
        system.assert(test02 == null);
	}
}