/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class ActionResult {
    @InvocableVariable(label='Error Message' description='Error message if action failed' required=false)
    global String ErrorMessage;
    @InvocableVariable(label='Success' description='Status of action' required=false)
    global Boolean IsSuccess;
    @InvocableVariable(label='Tag' description='Object that generated this result' required=false)
    global Id Tag;
}
