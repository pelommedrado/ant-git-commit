@isTest
private class VFC141_RMCNew_Opportunity_ControllerTest {
    
    private static Account accDealer, person1, person2, person3, person4;
    private static Contact manager1Contact, manager2Contact, seller1Contact, seller2Contact;
    private static User manager1, manager2, seller1, seller2;
    private static Lead lead1;
    private static Opportunity oppt1, oppt2, opp3, oppt4;
    private static Quote qt1;
    
    
    static {
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        system.runAs( localUser ){
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            // CRIAÇÃO DE CONTA DE CONCESSIONÁRIA
            accDealer = MyOwnCreation.getInstance().criaAccountDealer();
            accDealer.ChaseTaskTime__c = 16;
            
            // CRIAÇÃO DE CONTAS DE CLIENTES
            person1 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste1',
                PersEmailAddress__c = 'test1@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );
            
            person2 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste2',
                PersEmailAddress__c = 'test2@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );
            
            person3 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste3',
                PersEmailAddress__c = 'test3@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );
            
            person4 = new Account(
                FirstName = 'Sir',
                LastName = 'Teste4',
                PersEmailAddress__c = 'test4@email.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = Utils.randomNumberAsString(8),
                PersLandline__c = Utils.randomNumberAsString(8)
            );
            
            Database.insert(new List<Account>{accDealer, person1, person2, person3, person4});
            
            
            
            // CRIAÇÃO DOS CONTATOS DOS USUÁRIOS MANAGER
            manager1Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'manager1@org1.com',
                FirstName = 'User',
                LastName = 'Manager1',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );
            
            manager2Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'manager2@org1.com',
                FirstName = 'User',
                LastName = 'Manager2',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );
            
            // CRIAÇÃO DOS CONTATOS DOS USUÁRIOS SELLER
            seller1Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'seller1@org1.com',
                FirstName = 'User',
                LastName = 'Seller1',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );
            
            seller2Contact = new Contact(
                CPF__c =  Utils.generateCpf(),
                Email = 'seller2@org1.com',
                FirstName = 'User',
                LastName = 'Seller2',
                MobilePhone = '11223344556',
                AccountId = accDealer.Id
            );
            
            Database.insert(new List<Contact>{manager1Contact, manager2Contact, seller1Contact, seller2Contact});
            
            
            // CRIAÇÃO DOS USUÁRIOS MANAGER
            manager1 = new User(
                ContactId = manager1Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName = 'User',
                LastName = 'Manager1',
                Username = 'manager1@org1.com.teste',
                Email = 'manager1@org1.com',
                Alias = 'ma1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );
            
            manager2 = new User(
                ContactId = manager2Contact.Id,
                ProfileId = Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName = 'User',
                LastName = 'Manager2',
                Username = 'manager2@org1.com.teste',
                Email = 'manager2@org1.com',
                Alias = 'ma2',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );
            
            // CRIAÇÃO DOS USUÁRIOS SELLER
            seller1 = new User(
                ContactId = seller1Contact.Id,
                ProfileId = Utils.getProfileId('SFA - Seller'),
                FirstName = 'User',
                LastName = 'Seller1',
                Username = 'seller1@org1.com.teste',
                Email = 'seller1@org1.com',
                Alias = 'se1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );
            
            seller2 = new User(
                ContactId = seller2Contact.Id,
                ProfileId = Utils.getProfileId('SFA - Seller'),
                FirstName = 'User',
                LastName = 'Seller2',
                Username = 'seller2@org1.com.teste',
                Email = 'seller2@org1.com',
                Alias = 'se2',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
            );
            
            Database.insert(new List<User>{manager1, manager2, seller1, seller2});
            
            
            PermissionSet sellerPermission = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'];
            
            insert new List<PermissionSetAssignment>{
                new PermissionSetAssignment(
                    AssigneeId = seller1.Id,
                    PermissionSetId = sellerPermission.Id
                ),
                new PermissionSetAssignment(
                    AssigneeId = seller2.Id,
                    PermissionSetId = sellerPermission.Id
                )
            };
            
            
            
            lead1 = new Lead(
                RecordTypeId = Utils.getRecordTypeId('Lead', 'LDD_Internet'),
                OwnerId = manager1.Id,
                FirstName = 'Test',
                LastName = 'Lead',
                CPF_CNPJ__c = Utils.generateCpf(),
                LeadSource = 'Internet',
                SubSource__c = 'Marketing',
                Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found'
            );
            
            Database.insert(lead1);
            
            
            oppt1 = new Opportunity(
                OwnerId = manager1.Id,
                CloseDate = date.today().addDays(5),
                StageName = 'Identified',
                Amount = 10000,
                Expiration_Time__c = datetime.newInstance(date.today().addDays(5), time.newInstance(17, 0, 0, 0)),
                Name = 'Oportunidade (Sir Teste1)',
                AccountId = person1.Id,
                Dealer__c = accDealer.Id,
                OpportunitySource__c = 'TEST DRIVE',
                OpportunitySubSource__c = 'INTERNET',
                SourceMedia__c = 'Site Concessionária'
            );
            
            oppt2 = new Opportunity(
                OwnerId = manager2.Id,
                CloseDate = date.today().addDays(7),
                StageName = 'Identified',
                Amount = 10000,
                Expiration_Time__c = datetime.newInstance(date.today().addDays(5), time.newInstance(17, 0, 0, 0)),
                Name = 'Oportunidade (Sir Teste2)',
                AccountId = person2.Id,
                Dealer__c = accDealer.Id,
                OpportunitySource__c = 'TEST DRIVE',
                OpportunitySubSource__c = 'INTERNET',
                SourceMedia__c = 'Site Concessionária'
            );
            
            Database.insert(new List<Opportunity>{oppt1, oppt2});
            
            
            qt1 = new Quote(
                Name = 'teste',
                Status = 'Open',
                OpportunityId = oppt1.Id
            );
            Database.insert(qt1);
        }
    }
    
    
    static testMethod void myUnitTest() {
        
        Account persAccount;
        Lead lead;
        
        //Executando como usuario da comunidade - Vendedor
        System.runAs(seller1){
            
            Campaign campaign = new Campaign (name='teste');
            database.insert(campaign);
            
            persAccount = new Account(
                FirstName = 'NewUser', 
                LastName = 'Last Name',
                Country__c = 'Brazil',
                CustomerIdentificationNbr__c = '73412052612',
                PersEmailAddress__c = 'teste@teste.com',
                RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc')
            );
            insert(persAccount);
            
            lead = new Lead( FirstName = 'Test', LastName = 'Lead', CPF_CNPJ__c = '73412052612' );
            Database.insert( lead );
            
            Lead leadOld = new Lead(
                FirstName = 'Test',
                LastName = 'Lead',
                CPF_CNPJ__c = '89254857882',
                RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'),
                LeadSource = 'Internet',
                SubSource__c = 'Marketing',
                Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found'
            );
            Database.insert( leadOld );
            
            test.startTest();
            
            // Test Controller e variaveis
            PageReference pageRef = Page.VFP21_RMCNew_Opportunity;
            Test.setCurrentPage(pageRef);
            VFC141_RMCNew_Opportunity_Controller controller = new VFC141_RMCNew_Opportunity_Controller();
            
            String fieldMapping = controller.fieldMapping;
            String campaignId = controller.campaignId;
            list<SelectOption> campaignSelectList = controller.campaignSelectList;
            String sellerId = controller.sellerId;
            list<SelectOption> sellerSelectList = controller.sellerSelectList;
            
            controller.acc.PersMobPhone__c = '123456789';
            controller.cnt.FirstName = 'Test';
            controller.cnt.LastName = 'Account';
            controller.opp.Vehicle_Of_Interest__c = 'CLIO';
            
            //testar metodo save sem conta e sem lead antigo
            controller.acc.CustomerIdentificationNbr__c = '20280602634';
            controller.save();
            
            //testar metodo save sem conta e com Lead antigo 
            controller.leadId = leadOld.Id;
            controller.acc.CustomerIdentificationNbr__c = '89254857882';
            controller.save();
            
            //testar metodo save com conta e com Lead antigo
            controller.leadId = lead.Id;
            controller.acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
            system.debug('$$$' + persAccount.CustomerIdentificationNbr__c);
            controller.acc.CustomerIdentificationNbr__c = '73412052612';
            controller.save();
            
            //testar metodo save - exception todos os contatos estao nulos
            controller.acc.PersEmailAddress__c = null;
            controller.acc.PersMobPhone__c = null;
            controller.acc.PersLandline__c = null;
            controller.save();
            
            //testar CPF inválido
            controller.acc.CustomerIdentificationNbr__c = '2325642552452152214';
            controller.save();
            
            //testar metodo de validação de CPF
            controller.isCpfValid('36645921667');
            
            controller.leadId = leadOld.Id;
            controller.retailPlataform(leadOld.Id);
            controller.updateLeadFromSFA(leadOld);
            //controller.selectAccount();
            controller.selectLead();
            VFC141_RMCNew_Opportunity_Controller.limparFormatacaoCpfCnpj('642.430.583-18');
            
            test.stopTest();
        }
    }
    
    static testMethod void coverSellerName() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        
        Test.startTest();
        String sellerName = ctrl.sellerName;
        Test.stopTest();
    }
    
    static testMethod void coverAccountPrefix() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        
        Test.startTest();
        String accountPrefix = ctrl.accountPrefix;
        Test.stopTest();
    }
    
    static testMethod void coverInsertOpportunity() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        
        Test.startTest();
        ctrl.insertOpportunity(accDealer.Id);
        Test.stopTest();
    }
    
    static testMethod void coverDealerSelectList() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        
        Test.startTest();
        List< SelectOption > dealerSelectList = ctrl.dealerSelectList;
        Test.stopTest();
    }
    
    static testMethod void coverConvertLead() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        
        Test.startTest();
        ctrl.convertLead(accDealer.Id, lead1);
        Test.stopTest();
    }
    
    static testMethod void coverSelectAccount() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        ctrl.acc = person1;
        
        Test.startTest();
        ctrl.selectAccount();
        Test.stopTest();
    }
    
    static testMethod void coverQueryWrapper() {
        Test.startTest();
        
        VFC141_RMCNew_Opportunity_Controller.QueryWrapper qw = new VFC141_RMCNew_Opportunity_Controller.QueryWrapper('teste', 'Account');
        
        Test.stopTest();
    }
    
    
    static testMethod void coverSave_1() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            //PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc')
            //VehicleInterest_BR__c = 'test',
            //PersMobPhone__c = Utils.randomNumberAsString(8),
            //PersLandline__c = Utils.randomNumberAsString(8)
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_2() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = '123456',
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_3() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO');
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_4() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE'));
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_5() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE'));
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        ctrl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa');
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_6() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR'));
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_7() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR'));
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_8() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE'));
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = Utils.generateCpf(),
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        
        ctrl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa');
        ctrl.companyName = '[ kolekto ] tecnologia';
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_9() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE'));
        
        String cpfTest = Utils.generateCpf();
        ctrl.acc = new Account(
            Name = 'TestSave',
            CustomerIdentificationNbr__c = cpfTest,
            PersEmailAddress__c = 'testsave@email.com',
            Phone = Utils.randomNumberAsString(8),
            RecordTypeId = Utils.getRecordTypeId('Account','Company_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
        );
        Database.insert(ctrl.acc);
        
        ctrl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'DVE');
        ctrl.companyName = '[ kolekto ] tecnologia';
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }
    
    static testMethod void coverSave_10() {
        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR'));
        
        String cpfTest = Utils.generateCpf();
        ctrl.acc = new Account(
            FirstName = 'Test',
            LastName = 'Save',
            CustomerIdentificationNbr__c = cpfTest,
            PersEmailAddress__c = 'testsave@email.com',
            RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
            PersMobPhone__c = Utils.randomNumberAsString(8),
            PersLandline__c = Utils.randomNumberAsString(8)
            //VehicleInterest_BR__c = 'test',
        );
        Database.insert(ctrl.acc);
        
        insert new Lead(
            RecordTypeId = Utils.getRecordTypeId('Lead', 'LDD_Internet'),
            OwnerId = manager2.Id,
            FirstName = 'Test',
            LastName = 'Save',
            CPF_CNPJ__c = cpfTest,
            LeadSource = 'Internet',
            SubSource__c = 'Marketing',
            Detail__c = 'I am Interested',
            Sub_Detail__c = 'Not Found'
        );
        
        Test.startTest();
        ctrl.save();
        Test.stopTest();
    }

    static testMethod void coverSave_11() {

        VFC141_RMCNew_Opportunity_Controller ctrl = new VFC141_RMCNew_Opportunity_Controller();
        ctrl.opp = new Opportunity(Vehicle_Of_Interest__c = 'SANDERO', RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR'));
        
        Test.startTest();

        Id dvrRtId = ctrl.dvrRtId;
        Id dveRtId = ctrl.dveRtId;
        Id smnRtId = ctrl.smnRtId;
        
        Test.stopTest();
    }
    
    
}