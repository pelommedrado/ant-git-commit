@isTest
public class WSC10_LMTCreateLeadTest {
    
    public static testMethod void wayHappyTest(){
        
        MyOwnCreation moc = new MyOwnCreation();
        Campaign c = moc.criaCampanha();
        Insert c;
        
        WSC10_LMTCreateLead.Brands brands;
        
        WSC10_LMTCreateLead.AfterSales afterSales = new WSC10_LMTCreateLead.AfterSales();
        afterSales.description = 'descricao';
        afterSales.endDateOfValidityOfTheOffer = System.today();
        afterSales.quoteInterventionDuration = '40';
        afterSales.quotePrice = 5;
        afterSales.quoteStartDate = System.today();
        afterSales.quoteURI = 'Syete';
        afterSales.quoteWishedOperations = 'teste';
        
        WSC10_LMTCreateLead.Client client = new WSC10_LMTCreateLead.Client();
        client.acceptEMailContact = 's';
        client.acceptPhoneContact = 's';
        client.acceptSMSContact = 's';
        client.additionalInfo = 'teste';
        client.brochure = 'teste';
        client.businessMobilePhone = '23232323';
        client.businessPhone = '33333333';
        client.city = 'teste';
        client.country = 'teste';
        client.eMail = 'teste@teste.com';
        client.firstName = 'teste';
        client.houseNumber = 'teste';
        client.intentToTakeBack = 's';
        client.lastName = 'teste';
        client.mobilePhone = '45454545';
        client.phone = '44444444';
        client.place = 'teste';
        client.postalCode = '34343333';
        client.salutation = 'teste';
        client.street = 'teste';
        
        WSC10_LMTCreateLead.ClientPro clientPro = new WSC10_LMTCreateLead.ClientPro();
        clientPro.company = 'teste';
        clientPro.proEmail = 'teste@testando.com';
        clientPro.proOwnedFleet = 'teste';
        clientPro.proRegistrationNbr = 'teste';
        
        WSC10_LMTCreateLead.Dealer dealer = new WSC10_LMTCreateLead.Dealer();
        dealer.appointmentDate = system.today();
        dealer.createdDateByCustomer = system.today();
        dealer.dealerOfInterest = 'teste';
        dealer.exportDateTime = system.now();
        dealer.firstActionDate = system.today();
        dealer.lateDate = system.today();
        dealer.preassignedSeller = 'teste';
        dealer.raiser = 'teste';
        dealer.warningDate = system.today();
        
        WSC10_LMTCreateLead.Finance finance = new WSC10_LMTCreateLead.Finance();
        finance.financeDeposit = 6;
        finance.financeLoanTime = 'teste';
        finance.financePreac = 4;
        finance.fundingRequest = true;
        finance.typeFinancement = 'teste';
        
        WSC10_LMTCreateLead.LeadInfo info = new WSC10_LMTCreateLead.LeadInfo();
        info.appointmentEndTime = System.today();
        info.appointmentStartTime = System.today();
        info.campaign = c.Id;
        info.comment = '45124';
        info.recordType = Utils.getRecordTypeId('Lead', 'DVR');
        info.context = 'VF352Fs3s';
        info.description = 'teste';
        info.device = 'BRL';
        info.leadF6Id = 'teste';
        info.leadSource = 'teste';
        info.mailDiscount = 8;
        info.orgin = 'teste';
        info.promotionType = 'teste';
        info.sbolPromotion = 'teste';
        info.subTypeOfInterest = 'teste';
        info.typeOfInterest = 'teste';
        info.leadProvider = 'teste';
        info.brandOfInterest = brands;
        info.VehicleOfInterest = 'teste';
        //info.numdbm = 4;
        //info.voucherProtocol = '23424543';
        
        WSC10_LMTCreateLead.OwnVehicle ownVehicle = new WSC10_LMTCreateLead.OwnVehicle();
        ownVehicle.annualMileage = 'teste';
        ownVehicle.brand = 'teste';
        ownVehicle.energy = 'teste';
        ownVehicle.engine = 'teste';
        ownVehicle.firstRegistration = 'teste';
        ownVehicle.mileage = 'teste';
        ownVehicle.model = 'teste';
        ownVehicle.registrationNbr = 'teste';
        ownVehicle.version = 'teste';
        ownVehicle.VIN = 'teste';
        
        WSC10_LMTCreateLead.ServicesContract serContr = new WSC10_LMTCreateLead.ServicesContract();
        serContr.serviceContractsAnnualPrice = 5;
        serContr.serviceContractsAnnualPriceVAT = 5;
        serContr.serviceContractsCarLoan = true;
        serContr.serviceContractsDuration = 6;
        serContr.serviceContractsKilometer = '6';
        serContr.serviceContractsProductType = 'teste';
        serContr.serviceContractsRequest = 'teste';
        
        WSC10_LMTCreateLead.Vehicle veiculo = new WSC10_LMTCreateLead.Vehicle();
        veiculo.brand = 'teste';
        veiculo.energy = 'teste';
        veiculo.tireConfiguratorBrand = 'teste';
        veiculo.tireConfiguratorDiameter = 'teste';
        veiculo.tireConfiguratorHeight = 'teste';
        veiculo.tireConfiguratorSpeed = 'teste';
        veiculo.tireConfiguratorType = 'teste';
        veiculo.tireConfiguratorWidth = 'teste';
        veiculo.vehicleBodyType = 'teste';
        veiculo.vehicleColor = 'teste';
        veiculo.vehicleConfiguratorURI = 'teste';
        veiculo.vehicleCotation = 'teste';
        veiculo.vehicleDiscountPrice = 5;
        veiculo.vehicleEngine = 'teste';
        veiculo.vehicleID = 'teste';
        veiculo.vehicleOptions = 'teste';
        veiculo.vehicleRegistrationNumber = 'teste';
        veiculo.vehicleRirstRegistration = 'teste';
        veiculo.vehicleVersion = 'teste';
        veiculo.vehicleVIN = 'teste';
        veiculo.voNumber = 'teste';
        
        WSC10_LMTCreateLead.WebServiceResultList webSresult = new WSC10_LMTCreateLead.WebServiceResultList();
        webSresult.code = 'teste';
        webSresult.libelle = 'teste';
        webSresult.status = 'teste';

        WSC10_LMTCreateLead.LeadInput leadInput = new WSC10_LMTCreateLead.LeadInput();
        leadInput.afterSales = afterSales;
        leadInput.client = client;
        leadInput.clientPro = clientPro;
        leadInput.dealer = dealer;
        leadInput.finance = finance;
        leadInput.ownVehicle = ownVehicle;
        leadInput.servicesContract = serContr;
        leadInput.vehicle = veiculo;
        leadInput.leadInfo = info;
        
        List<WSC10_LMTCreateLead.LeadInput> leadInputList = new List<WSC10_LMTCreateLead.LeadInput>();
        leadInputList.add(leadInput);
        
        WSC10_LMTCreateLead.WebServiceResultList result = new WSC10_LMTCreateLead.WebServiceResultList();
        result = WSC10_LMTCreateLead.createLead(leadInputList);
        System.debug('***result: '+result);
        System.assert(result.status.equals('Lead(s) and CampaignMember(s) creation OK'));
        
        WSC10_LMTCreateLead.enviarEmail(leadInputList);
        
    }
    
    public static testMethod void errorInCreateLeadAndCampaignMember(){
        
        MyOwnCreation moc = new MyOwnCreation();
        Campaign c = moc.criaCampanha();
        Insert c;
        
        WSC10_LMTCreateLead.Brands brands;
        
        WSC10_LMTCreateLead.AfterSales afterSales = new WSC10_LMTCreateLead.AfterSales();
        afterSales.description = 'descricao';
        afterSales.endDateOfValidityOfTheOffer = System.today();
        afterSales.quoteInterventionDuration = '40';
        afterSales.quotePrice = 5;
        afterSales.quoteStartDate = System.today();
        afterSales.quoteURI = 'Syete';
        afterSales.quoteWishedOperations = 'teste';
        
        WSC10_LMTCreateLead.Client client = new WSC10_LMTCreateLead.Client();
        client.acceptEMailContact = 's';
        client.acceptPhoneContact = 's';
        client.acceptSMSContact = 's';
        client.additionalInfo = 'teste';
        client.brochure = 'teste';
        client.businessMobilePhone = '23232323';
        client.businessPhone = '33333333';
        client.city = 'teste';
        client.country = 'teste';
        client.eMail = 'teste@teste.com';
        client.firstName = 'teste';
        client.houseNumber = 'teste';
        client.intentToTakeBack = 's';
        client.lastName = NULL;
        client.mobilePhone = '45454545';
        client.phone = '44444444';
        client.place = 'teste';
        client.postalCode = '34343333';
        client.salutation = 'teste';
        client.street = 'teste';
        
        WSC10_LMTCreateLead.ClientPro clientPro = new WSC10_LMTCreateLead.ClientPro();
        clientPro.company = 'teste';
        clientPro.proEmail = 'teste@testando.com';
        clientPro.proOwnedFleet = 'teste';
        clientPro.proRegistrationNbr = 'teste';
        
        WSC10_LMTCreateLead.Dealer dealer = new WSC10_LMTCreateLead.Dealer();
        dealer.appointmentDate = system.today();
        dealer.createdDateByCustomer = system.today();
        dealer.dealerOfInterest = 'teste';
        dealer.exportDateTime = system.now();
        dealer.firstActionDate = system.today();
        dealer.lateDate = system.today();
        dealer.preassignedSeller = 'teste';
        dealer.raiser = 'teste';
        dealer.warningDate = system.today();
        
        WSC10_LMTCreateLead.Finance finance = new WSC10_LMTCreateLead.Finance();
        finance.financeDeposit = 6;
        finance.financeLoanTime = 'teste';
        finance.financePreac = 4;
        finance.fundingRequest = true;
        //finance.monthly = 4;
        finance.typeFinancement = 'teste';
        
        WSC10_LMTCreateLead.LeadInfo info = new WSC10_LMTCreateLead.LeadInfo();
        info.appointmentEndTime = System.today();
        info.appointmentStartTime = System.today();
        info.campaign = c.Id;
        info.comment = '45124';
        info.recordType = Utils.getRecordTypeId('Lead', 'DVR');
        info.context = 'jasgd73ersdx';
        info.description = 'teste';
        info.device = 'BRL';
        info.leadF6Id = 'teste';
        info.leadSource = 'teste';
        info.mailDiscount = 8;
        info.orgin = 'teste';
        info.promotionType = 'teste';
        info.sbolPromotion = 'teste';
        info.subTypeOfInterest = 'teste';
        info.typeOfInterest = 'teste';
        info.leadProvider = 'teste';
        info.brandOfInterest = brands;
        info.VehicleOfInterest = 'teste';
        //info.numdbm = 4;
        //info.voucherProtocol = '23424543';
        
        WSC10_LMTCreateLead.OwnVehicle ownVehicle = new WSC10_LMTCreateLead.OwnVehicle();
        ownVehicle.annualMileage = 'teste';
        ownVehicle.brand = 'teste';
        ownVehicle.energy = 'teste';
        ownVehicle.engine = 'teste';
        ownVehicle.firstRegistration = 'teste';
        ownVehicle.mileage = 'teste';
        ownVehicle.model = 'teste';
        ownVehicle.registrationNbr = 'teste';
        ownVehicle.version = 'teste';
        ownVehicle.VIN = 'teste';
        
        WSC10_LMTCreateLead.ServicesContract serContr = new WSC10_LMTCreateLead.ServicesContract();
        serContr.serviceContractsAnnualPrice = 5;
        serContr.serviceContractsAnnualPriceVAT = 5;
        serContr.serviceContractsCarLoan = true;
        serContr.serviceContractsDuration = 6;
        serContr.serviceContractsKilometer = '6';
        serContr.serviceContractsProductType = 'teste';
        serContr.serviceContractsRequest = 'teste';
        
        WSC10_LMTCreateLead.Vehicle veiculo = new WSC10_LMTCreateLead.Vehicle();
        veiculo.brand = 'teste';
        veiculo.energy = 'teste';
        veiculo.tireConfiguratorBrand = 'teste';
        veiculo.tireConfiguratorDiameter = 'teste';
        veiculo.tireConfiguratorHeight = 'teste';
        veiculo.tireConfiguratorSpeed = 'teste';
        veiculo.tireConfiguratorType = 'teste';
        veiculo.tireConfiguratorWidth = 'teste';
        veiculo.vehicleBodyType = 'teste';
        veiculo.vehicleColor = 'teste';
        veiculo.vehicleConfiguratorURI = 'teste';
        veiculo.vehicleCotation = 'teste';
        veiculo.vehicleDiscountPrice = 5;
        veiculo.vehicleEngine = 'teste';
        veiculo.vehicleID = 'teste';
        veiculo.vehicleOptions = 'teste';
        veiculo.vehicleRegistrationNumber = 'teste';
        veiculo.vehicleRirstRegistration = 'teste';
        veiculo.vehicleVersion = 'teste';
        veiculo.vehicleVIN = 'teste';
        veiculo.voNumber = 'teste';
        
        WSC10_LMTCreateLead.WebServiceResultList webSresult = new WSC10_LMTCreateLead.WebServiceResultList();
        webSresult.code = 'teste';
        webSresult.libelle = 'teste';
        webSresult.status = 'teste';

        WSC10_LMTCreateLead.LeadInput leadInput = new WSC10_LMTCreateLead.LeadInput();
        leadInput.afterSales = afterSales;
        leadInput.client = client;
        leadInput.clientPro = clientPro;
        leadInput.dealer = dealer;
        leadInput.finance = finance;
        leadInput.ownVehicle = ownVehicle;
        leadInput.servicesContract = serContr;
        leadInput.vehicle = veiculo;
        leadInput.leadInfo = info;
        
        List<WSC10_LMTCreateLead.LeadInput> leadInputList = new List<WSC10_LMTCreateLead.LeadInput>();
        leadInputList.add(leadInput);

        WSC10_LMTCreateLead.WebServiceResultList result = new WSC10_LMTCreateLead.WebServiceResultList();
        result = WSC10_LMTCreateLead.createLead(leadInputList);
        System.debug('***result: '+result);
        System.assert(result.status.contains('1 Lead(s) records could not be processed'));
        
        WSC10_LMTCreateLead.enviarEmail(leadInputList);
 
    }

}