/**
* Classe de teste da classe VFC111_TestDriveSACBusinessDelegate.
* @author Felipe Jesus Silva.
*/
@isTest(SeeAllData = true)
private class VFC111_TestDriveSACBusinessDelegateTest 
{
	
	static Id getRecordTypeId(String developerName)
	{
		RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
		
		return sObjRecordType.Id;
	}
	
	static Account createDealerAccount(String bir)
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Network_Site_Acc'); 
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccDealer = new Account();
		sObjAccDealer.RecordTypeId = recordTypeId;
		sObjAccDealer.Name = 'Dealer Test';
		sObjAccDealer.Phone='1000';	 
        sObjAccDealer.IDBIR__c = bir;	 
		sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com'; 
		sObjAccDealer.ShippingCity = 'Paris';
		sObjAccDealer.ShippingCountry = 'France';
		sObjAccDealer.ShippingState = 'IDF';
		sObjAccDealer.ShippingPostalCode = '75013';
		sObjAccDealer.ShippingStreet = 'my street';
		
		return sObjAccDealer;
	}
	
	static Account createPersonalAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Personal_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccPersonal = new Account();
		sObjAccPersonal.RecordTypeId = recordTypeId; 
		sObjAccPersonal.FirstName = 'Personal';
		sObjAccPersonal.LastName = 'Account';
		sObjAccPersonal.Phone='1000';
		sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
		sObjAccPersonal.ShippingCity = 'Paris';
		sObjAccPersonal.ShippingCountry = 'France';
		sObjAccPersonal.ShippingState = 'IDF';
		sObjAccPersonal.ShippingPostalCode = '75013';
		sObjAccPersonal.ShippingStreet = 'my street';
		sObjAccPersonal.YrReturnVehicle_BR__c = 2013; 
		sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
		//sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
		//sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
		sObjAccPersonal.PersLandline__c = '1133334444';
		sObjAccPersonal.PersMobPhone__c = '1155556666';
		
		return sObjAccPersonal;
	}
	
	static VEH_Veh__c createVehicle()
	{
		VEH_Veh__c sObjVehicle = new VEH_Veh__c();
		sObjVehicle.Name = 'TESTCHASSISANDERO';
		sObjVehicle.Model__c = 'Sandero';
		sObjVehicle.Version__c = 'Stepway 1.6 8V';
		sObjVehicle.VersionCode__c = 'S1';
		sObjVehicle.Color__c = 'Black';
		sObjVehicle.VehicleRegistrNbr__c = 'ABC-1234';
		
		return sObjVehicle;
	}
	
	static TDV_TestDrive__c createTestDrive()
	{
		TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
		sObjTestDrive.Status__c = 'Scheduled';
		sObjTestDrive.Opportunity__c = null;
		
		return sObjTestDrive;
	}
	
	/**
	* Testa o método getTestDriveByAccount.
	* Esse método chama os métodos getVehiclesAvailable e getAgendaVehicle que automaticamente serão testados também.
	*/
	testMethod
	static void getTestDriveByAccountTest()
	{
		Account sObjAccDealer = null;
		Account sObjAccDealerAux = null;
		Account sObjAccPersonal = null;
		VEH_Veh__c sObjVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC110_TestDriveSACVO testDriveVO = null;
		
		/*cria e insere a conta que representa a concessionária*/
		sObjAccDealer = createDealerAccount('1111');		
		insert sObjAccDealer;
		
		/*obtém o id do proprietário dessa conta*/
		sObjAccDealerAux = [SELECT OwnerId FROM Account WHERE Id =: sObjAccDealer.Id];
		
		/*cria a conta pessoal e vincula a concessionária*/
		sObjAccPersonal = createPersonalAccount();
		sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
		insert sObjAccPersonal;
		
		/*cria e insere o veículo*/
		sObjVehicle = createVehicle();
		insert sObjVehicle;
		
		/*cria a insere o objeto TestVehicle*/
		sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
		insert sObjTestDriveVehicle;
		
		Test.startTest();
	
		/*chama o método que será efetivamente testado para obter o VO a partir da conta pessoal*/
		testDriveVO = VFC111_TestDriveSACBusinessDelegate.getInstance().getTestDriveByAccount(sObjAccPersonal.Id);
		
		/*certifica de que as opções setadas no VO estão corretas*/
		System.assertEquals(testDriveVO.accountId, sObjAccPersonal.Id);		
		System.assertEquals(testDriveVO.sObjAccount.DealerInterest_BR__c, sObjAccPersonal.DealerInterest_BR__c);
		System.assertEquals(testDriveVO.dealerOwnerId, sObjAccDealerAux.OwnerId);
		System.assertEquals(testDriveVO.customerName, sObjAccPersonal.FirstName + ' ' + sObjAccPersonal.LastName); 
		System.assertEquals(testDriveVO.persLandline, sObjAccPersonal.PersLandline__c);
		System.assertEquals(testDriveVO.persMobPhone, sObjAccPersonal.PersMobPhone__c);
		System.assertEquals(testDriveVO.dateBookingNavigation, Date.today());
		System.assertEquals(testDriveVO.yearCurrentVehicle, sObjAccPersonal.YrReturnVehicle_BR__c);
		System.assertEquals(testDriveVO.vehicleInterest, sObjAccPersonal.VehicleInterest_BR__c);
		System.assert(testDriveVO.lstSObjModel.size() > 0);
		System.assertEquals(testDriveVO.lstSObjTestDriveVehicleAvailable.size(), 1);
		System.assertEquals(testDriveVO.plaque, sObjVehicle.VehicleRegistrNbr__c);
		System.assertEquals(testDriveVO.model, sObjVehicle.Model__c);
		System.assertEquals(testDriveVO.version, sObjVehicle.Version__c);
		System.assertEquals(testDriveVO.color, sObjVehicle.Color__c);
		
		Test.stopTest();		
	}
	
	/**
	* Testa o método getVehicleDetails.
	*/
	testMethod
	static void getVehicleDetailsTest()
	{
		Account sObjAccDealer = null;
		VEH_Veh__c sObjVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC110_TestDriveSACVO testDriveVO = null;
		VFC110_TestDriveSACVO testDriveVOResponse = null;
		
		/*cria e insere a conta que representa a concessionária*/
		sObjAccDealer = createDealerAccount('1112');		
		insert sObjAccDealer;
		
		/*cria e insere o veículo*/
		sObjVehicle = createVehicle();
		insert sObjVehicle;
		
		/*cria a insere o objeto TestVehicle*/
		sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
		insert sObjTestDriveVehicle;
		
		/*instancia o VO e seta o atributos necessários para testar o método*/
		testDriveVO = new VFC110_TestDriveSACVO();
		testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
		testDriveVO.dateBookingNavigation = Date.today();
		
		Test.startTest();
		
		/*chama o método que será efetivamente testado*/
		testDriveVOResponse = VFC111_TestDriveSACBusinessDelegate.getInstance().getVehicleDetails(testDriveVO);
		
		/*certifica de que as informações setadas no VO de resposta estão corretas*/
		System.assertEquals(testDriveVOResponse.model, sObjVehicle.Model__c);
		System.assertEquals(testDriveVOResponse.version, sObjVehicle.Version__c);
		System.assertEquals(testDriveVOResponse.color, sObjVehicle.Color__c);		
		System.assertEquals(testDriveVOResponse.plaque, sObjVehicle.VehicleRegistrNbr__c);
		
		Test.stopTest();
	}
	
	/**
	* Testa o método scheduleTestDrive.
	*/
	testMethod
	static void scheduleTestDriveTest()
	{
		Account sObjAccDealer = null;
		Account sObjAccDealerAux = null;
		Account sObjAccPersonal = null;
		VEH_Veh__c sObjVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC110_TestDriveSACVO testDriveVO = null;
		Boolean isTestDriveScheduled = null;
		Date dtTomorrow = null;
		Opportunity sObjOppCreated = null;
		TDV_TestDrive__c sObjTestDriveCreated = null;
		
		/*cria e insere a conta que representa a concessionária*/
		sObjAccDealer = createDealerAccount('1113');		
		insert sObjAccDealer;
		
		/*obtém o id do proprietário dessa conta*/
		sObjAccDealerAux = [SELECT OwnerId FROM Account WHERE Id =: sObjAccDealer.Id];
		
		/*cria a conta pessoal e vincula a concessionária*/
		sObjAccPersonal = createPersonalAccount();
		sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
		insert sObjAccPersonal;
		
		/*cria e insere o veículo*/
		sObjVehicle = createVehicle();
		insert sObjVehicle;
		
		/*cria a insere o objeto TestVehicle*/
		sObjTestDriveVehicle = new TDV_TestDriveVehicle__c(Account__c = sObjAccDealer.Id, Vehicle__c = sObjVehicle.Id, Available__c = true);
		insert sObjTestDriveVehicle;
		
		/*instancia o VO e seta o atributos necessários para testar o método*/
		testDriveVO = new VFC110_TestDriveSACVO();		
		testDriveVO.accountId = sObjAccPersonal.Id;
		testDriveVO.dealerOwnerId = sObjAccDealerAux.OwnerId;
		testDriveVO.sObjAccount.DealerInterest_BR__c = sObjAccDealerAux.Id;
		testDriveVO.vehicleInterest = 'SANDERO';
		testDriveVO.customerName = 'CUSTOMER TEST';
		testDriveVO.testDriveVehicleId = sObjTestDriveVehicle.Id;
		
		/*define a data de agendamento de 1 hora atrás para provocar erro no test*/
		testDriveVO.dateBooking = DateTime.now().addHours(-1);
		
		Test.startTest();
		
		try
		{
			/*força um erro no agendamento do test drive pois a data de agendamento setada é menor que a data atual*/
			VFC111_TestDriveSACBusinessDelegate.getInstance().scheduleTestDrive(testDriveVO);
		}
		catch(VFC58_CreateTestDriveException ex)
		{
			isTestDriveScheduled = false;
		}
		
		/*certifica de que o test drive NÃO foi agendado*/
		System.assertEquals(isTestDriveScheduled, false);
		
		dtTomorrow = Date.today() + 1;
		
		/*muda a data de agendamento para que agora haja sucesso no agendamento do test drive*/
		testDriveVO.dateBooking = DateTime.newInstance(dtTomorrow.year(), dtTomorrow.month(), dtTomorrow.day(), 8, 0, 0);
			
		try
		{
			/*tenta a gendar novamente (dessa vez deve ter sucesso)*/
			VFC111_TestDriveSACBusinessDelegate.getInstance().scheduleTestDrive(testDriveVO);	
			
			isTestDriveScheduled = true;	
		}
		catch(Exception ex)
		{
			isTestDriveScheduled = false;
		}
		
		/*certifica de que o test drive foi agendado*/
		System.assertEquals(isTestDriveScheduled, true);
		
		/*obtém a oportunidade que foi criada*/
		sObjOppCreated = [SELECT Dealer__c, OpportunityTransition__c, OwnerId FROM Opportunity WHERE AccountId =: testDriveVO.accountId];
		
		/*certifica de que as informações principais da oportunidade estão corretas*/
		System.assertEquals(sObjOppCreated.Dealer__c, testDriveVO.sObjAccount.DealerInterest_BR__c);
		System.assertEquals(sObjOppCreated.OpportunityTransition__c, VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER);
		System.assertEquals(sObjOppCreated.OwnerId, testDriveVO.dealerOwnerId);
		
		/*obtém o test drive que foi criado*/
		sObjTestDriveCreated = [SELECT DateBooking__c, Status__c, TestDriveVehicle__c FROM TDV_TestDrive__c WHERE Opportunity__c =: sObjOppCreated.Id];
		
		/*certifica de que as informações principais do test drive estão corretas*/
		System.assertEquals(sObjTestDriveCreated.DateBooking__c, testDriveVO.dateBooking);
		System.assertEquals(sObjTestDriveCreated.Status__c, 'Scheduled');
		System.assertEquals(sObjTestDriveCreated.TestDriveVehicle__c, testDriveVO.testDriveVehicleId);
	
		Test.stopTest();		
	}
	
}