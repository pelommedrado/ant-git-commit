public with sharing class VFC106_DBM_Import_BO
{
	private static final VFC106_DBM_Import_BO instance = new VFC106_DBM_Import_BO();
	  
	public static VFC106_DBM_Import_BO getInstance() {
		return instance;
	}
	  
	private VFC106_DBM_Import_BO(){}


	// Private attributes
	private Id leadRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('LDD_Marketing');
	private Id personalAccountRecType = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');

	private Map<String, List<MLC_Molicar__c>> mapBrandWithMolicars = null;
  	private Map<Id, Account> mapIdWithAccount = null;
  	private Map<Decimal, Account> mapNUMDBMAccounts = null;
  	private Map<String, Account> mapCPFandCNPJAccounts = null;
  	private Map<Id, Lead> mapIdWithLead = null;
  	private Map<Decimal, Lead> mapNUMDBMLead = null;
  	private Map<String, List<Lead>> mapCPFandCNPJLeads = null;
  	private Map<String, Campaign> mapCampaignSingle = null;

	private Boolean isAccount = false;
	private Boolean isLead = false;
	private Boolean isMolicar = false;
	private Boolean isNewLead = false;
      
	private Account fetchedAccount = null;
	private Lead fetchedLead = null;
	private MLC_Molicar__c molicar = null;


	/**
	 * [after insert]
	 * Handle data inserted in DBM_Import__c object, classifying it in Lead or Account
	 * and aplying some validations
	 */
	public void afterInsertOnDBMImport(List<DBM_Import__c> lstNewDBMImport)
	{
		system.debug(LoggingLevel.INFO, '*** afterInsertOnDBMImport()');

		List<Id> lstDBMToDeleteIds = new List<Id>();
  	    
  	    Map<Id, Account> mapAccountsToUpdate = new Map<Id, Account>();
  	    Map<Id, Lead> mapLeadsToUpdate = new Map<Id, Lead>();
  	    List<CampaignMember> lstCampaignMemberToInsert = new List<CampaignMember>();

  	    Set<String> setImportIds = new Set<String>();
  	    Set<Decimal> setImportNUMDBMs = new Set<Decimal>();
  	    Set<String> setImportCPFandCNPJs = new Set<String>();
  	    Set<String> setImportBrands = new Set<String>();
  	    Set<String> setCampaignSingleCode = new Set<String>();

  	    for (DBM_Import__c importDBM : lstNewDBMImport)
  	    {
			if (importDBM.Id_Salesforce__c != null) setImportIds.add(importDBM.Id_Salesforce__c);
  	    	if (importDBM.NUMDBM__c != null) setImportNUMDBMs.add(importDBM.NUMDBM__c);
  	    	if (importDBM.CPF_CNPJ__c != null) setImportCPFandCNPJs.add(importDBM.CPF_CNPJ__c);
  	    	if (importDBM.Molicar_Brand__c != null) setImportBrands.add(importDBM.Molicar_Brand__c);
  	    	
  	    	if (importDBM.CampaignCode__c != null) setCampaignSingleCode.addAll(importDBM.CampaignCode__c.split(VFC126_CampaignConfig.CAMPAIGN_CHAR_SEPARATOR));
  	    	system.debug(LoggingLevel.INFO, '*** importDBM.CampaignCode__c='+importDBM.CampaignCode__c);
  	    }

		// Load helper data
  	    mapBrandWithMolicars = VFC11_MolicarDAO.getInstance().returnMolicarswithBrand(setImportBrands);
  	    mapIdWithAccount = VFC12_AccountDAO.getInstance().returnMapWithIdAndAccount(setImportIds);
  	    mapNUMDBMAccounts = VFC12_AccountDAO.getInstance().returnAccountsUsingNUMDBMField(setImportNUMDBMs);
  	    mapCPFandCNPJAccounts = VFC12_AccountDAO.getInstance().returnAccountsUsingCustomerIdentifyNumber(setImportCPFandCNPJs);
  	   
  	    mapIdWithLead = VFC09_LeadDAO.getInstance().returnMapWithIdAndLeadRecord(setImportIds);
  	    mapNUMDBMLead = VFC09_LeadDAO.getInstance().returnLeadRecordsUsingNUMDBMField(setImportNUMDBMs);
  	    mapCPFandCNPJLeads = VFC09_LeadDAO.getInstance().returnLeadsUsingCPF_CNPJ(setImportCPFandCNPJs);   	        	 
		
		mapCampaignSingle = VFC121_CampaignDAO.getInstance().fetchCampaignSingleByDBMCampaignCodeSet(setCampaignSingleCode);

		system.debug(LoggingLevel.INFO, '*** mapBrandWithMolicars=' + mapBrandWithMolicars);
		system.debug(LoggingLevel.INFO, '*** mapIdWithLead=' + mapIdWithLead);
		system.debug(LoggingLevel.INFO, '*** mapNUMDBMLead=' + mapNUMDBMLead);
		system.debug(LoggingLevel.INFO, '*** mapCPFandCNPJLeads=' + mapCPFandCNPJLeads);
		system.debug(LoggingLevel.INFO, '*** mapCampaignSingle=' + mapCampaignSingle);

  	    for (DBM_Import__c importDBM : lstNewDBMImport)
  	    {
  	   		// Reset variables
  	   	    isAccount = false;
  	   	    isLead = false;
  	   	    isMolicar = false;
  	   	    isNewLead = false;
  	   	    fetchedAccount = new Account();
  	   	    fetchedLead = new Lead();
  	   	    molicar = null;

			lstDBMToDeleteIds.add(importDBM.Id);

      	   	system.debug(LoggingLevel.INFO, '*** importDBM.Molicar_Brand__c=' + importDBM.Molicar_Brand__c);

			// Check if a Molicar object exists
			isMolicar = this.checkMolicarObjectExists(importDBM);

			// Check if is an Account or a Lead and fill in some global variables
			this.checkIfAccountOrLead(importDBM);

			system.debug(LoggingLevel.INFO, '*** isAccount='+isAccount);
			system.debug(LoggingLevel.INFO, '*** isMolicar='+isMolicar);
      	   	system.debug(LoggingLevel.INFO, '*** isLead='+isLead);
      	   	system.debug(LoggingLevel.INFO, '*** isNewLead='+isNewLead);

			// Update objects (Account and / or Leads)
			if (isAccount)
			{
  	   	       	if (fetchedAccount.RecordTypeId == personalAccountRecType)
  	   	       	{
	      	   	    if (isMolicar) {
						fetchedAccount.CurrentVehicle_BR__c = molicar.Id;
					}
					
					// Update Account fields
					system.debug(LoggingLevel.INFO, '*** updateAccount');
					updateAccountFieldsFromImportDBM(fetchedAccount, importDBM);
      	   	       	
      	   	       	//AccountsToUpdate.add(fetchedAccount);
      	   	       	mapAccountsToUpdate.put(fetchedAccount.Id, fetchedAccount);

					// Copy account data to a lead mirror record
					Lead existingLead = returnOpenLead(fetchedAccount.Id, importDBM, mapNUMDBMLead, mapCPFandCNPJLeads);
      	   	       	if (existingLead != null)
      	   	       	{
      	   	       	  	if (isMolicar) {
							existingLead.CRV_CurrentVehicle__c = molicar.Id;
      	   	       	  	}

	      	   	       	// update mirror lead
	      	   	       	system.debug(LoggingLevel.INFO, '*** cloneAccount To a Lead');
	      	   	       	cloneAccountIntoLead(existingLead, fetchedAccount);    

	      	   	       	mapLeadsToUpdate.put(existingLead.Id, existingLead);

	      	   	       	// Check if we should associate a lead to a campaign member
	      	   	       	VFC122_CampaignBO.getInstance().processCampaignMember(importDBM, existingLead, 
	      	   	       		mapCampaignSingle, lstCampaignMemberToInsert);
					}
      	   	       	else if (existingLead == null)
      	   	       	{
      	   	       		system.debug(LoggingLevel.INFO, '*** create a new Lead');
      	   	       		
      	   	       		// Create a new lead
  	   	       	  	    Lead createdLead = new Lead();
  	   	       	  	    createdLead.RecordTypeId = leadRecordTypeId;
  	   	       	  	    createdLead.Account__c  = fetchedAccount.Id;
      	   	       	    
      	   	       	    if (isMolicar) {
      	   	       	    	createdLead.CRV_CurrentVehicle__c = molicar.Id;
      	   	       	    }
      	   	       	    
      	   	       	    // Clone acccount fields to lead object
      	   	       	    cloneAccountIntoLead(createdLead, fetchedAccount);

						// Insert lead here to allow the CM to be added
						// This will require to use at the top 20 record per transaction in dataloader
						insert createdLead;
						
      	   	       	    //lstLeadsToInsert.add(createdLead);
      	   	       	    
      	   	       	    // Check if we should associate a lead to a campaign member
      	   	       	    VFC122_CampaignBO.getInstance().processCampaignMember(importDBM, createdLead, 
	      	   	       		mapCampaignSingle, lstCampaignMemberToInsert);
					}
				}
				else {
					system.debug(LoggingLevel.INFO, '*** The record with Id '+ fetchedAccount.Id + ' was skipped due a different recordType');
				}
			}
			else if (isLead && !isNewLead)
			{
				if (isMolicar)
      	   	    	fetchedLead.CRV_CurrentVehicle__c = molicar.Id;
      	   	    
      	   	    // Copy fields from DBM to lead
      	   	    system.debug(LoggingLevel.INFO, '*** update Lead');
      	   	    updateLeadFieldsFromImportDBM(fetchedLead, importDBM);

      	   	    // LeadsToUpdate.add(fetchedLead);
      	   	    mapLeadsToUpdate.put(fetchedLead.Id, fetchedLead);
      	   	    
      	   	    // Check if we should associate a lead to a campaign member
      	   	    VFC122_CampaignBO.getInstance().processCampaignMember(importDBM, fetchedLead, 
      	   	    	mapCampaignSingle, lstCampaignMemberToInsert);
			}
			else if (isLead && isNewLead)
			{
				system.debug(LoggingLevel.INFO, '*** create Lead 2');
				
			    Lead createdLead = new Lead();
  	   	       	createdLead.RecordTypeId = leadRecordTypeId;
  	   	       	
  	   	       	// Copy fields from DBM to lead
  	   	       	updateLeadFieldsFromImportDBM(createdLead, importDBM);    

  	   	       	//lstLeadsToInsert.add(createdLead);
  	   	       	// Insert lead here to allow the CM to be added
				// This will require to use at the top 20 record per transaction in dataloader
  	   	       	insert createdLead;
  	   	       	
  	   	       	// Check if we should associate a lead to a campaign member
      	   	    VFC122_CampaignBO.getInstance().processCampaignMember(importDBM, createdLead, 
      	   	    	mapCampaignSingle, lstCampaignMemberToInsert);
			}
		}
      	   
		system.debug(LoggingLevel.INFO, '*** mapAccountsToUpdate:'+mapAccountsToUpdate);
		system.debug(LoggingLevel.INFO, '*** mapLeadsToUpdate:'+mapLeadsToUpdate);
		system.debug(LoggingLevel.INFO, '*** lstCampaignMemberToInsert.size():'+lstCampaignMemberToInsert.size());
		
		// Deduplicate cm with same lead.Id and campaign.Id
		List<CampaignMember> deduplicatedList = VFC122_CampaignBO.getInstance().deduplicatePossibleCampaignMember(lstCampaignMemberToInsert);
		system.debug(LoggingLevel.INFO, '*** deduplicatedList.size():'+deduplicatedList.size());
		
		// Remove existing CM already in DB
		List<CampaignMember> nonExistingCMList = VFC127_CampaignMemberBO.getInstance().removeExistingCM(deduplicatedList);
		system.debug(LoggingLevel.INFO, '*** nonExistingCMList.size():'+nonExistingCMList.size());

		// Create a transaction
		Savepoint sp = Database.setSavepoint();
		try
		{
			// Account
			update mapAccountsToUpdate.values();
	
			// Leads
			update mapLeadsToUpdate.values();
			
			// Insert campaign members
			insert nonExistingCMList;
		}
        catch (Exception e)
        {
            Database.rollback(sp);
            system.debug(LoggingLevel.ERROR, e.getMessage());
            throw e;
        }

		if (!Test.isRunningTest())
		{
			system.debug(LoggingLevel.INFO, '*** calling delete ...');
			deleteDBMRecords(lstDBMToDeleteIds);
		}
	}

	/**
	 * Delete all data in object DBM_Import__c after all processing is done
	 */
	@Future
	public static void deleteDBMRecords(List<Id> lstDBMToDeleteIds)
	{
		system.debug(LoggingLevel.INFO, '*** deleteDBMRecords()');
		
	    List<DBM_Import__c> lstDBMToDelete = [SELECT Id FROM DBM_Import__c where Id IN :lstDBMToDeleteIds];

		system.debug(LoggingLevel.INFO, '*** lstDBMToDelete.size()='+lstDBMToDelete.size());
		if (lstDBMToDelete.size() > 0) {
			delete lstDBMToDelete;
			system.debug(LoggingLevel.INFO, '*** deleted');
		}
	}

	/**
	 * For Updating Account Fields With DBM_Import Fields
	 */
	private void updateAccountFieldsFromImportDBM(Account account, DBM_Import__c aDBMImport)
	{
		system.debug(LoggingLevel.INFO, '*** updateAccountFieldsFromImportDBM()');

		// Do not update some account fields, because they could be changed in SFDC. Only fields
		// related to marketdata are being updated.

	  	//account.RecordTypeId                    = personalAccountRecType ;
	  	//account.FirstName                       = aDBMImport.First_Name__c;
	  	//account.LastName                        = aDBMImport.Last_Name__c;
	  	//account.CustomerIdentificationNbr__c    = aDBMImport.CPF_CNPJ__c;
	    //account.PersLandline__c                 = aDBMImport.Home_Phone__c;
	  	//account.PersEmailAddress__c             = aDBMImport.Email__c;
	  	//account.ShippingStreet                  = aDBMImport.Street__c;
	  	//account.ShippingCity                    = aDBMImport.City__c;
	  	//account.ShippingState                   = aDBMImport.State__c;
	  	//account.ShippingPostalCode              = aDBMImport.Postal_Code__c;
	  	//account.ShippingCountry                 = aDBMImport.Country__c;
	  	//account.CustomerProfile__c              = aDBMImport.Customer_Profile__c;
	  	//account.PersonBirthdate                 = aDBMImport.Birthdate__c;
	  	//account.CurrentMilage__c                = aDBMImport.Current_Milage__c;
	  	
	  	account.LeadSubsource__c                = aDBMImport.Sub_Source__c;
	  	account.ValueOfCredit__c                = aDBMImport.Value_Of_Credit__c;
	  	account.EndDateOfFinanciating_BR__c     = aDBMImport.End_Date_Of_Financiating__c;
	  	account.DateContemplation_BR__c         = aDBMImport.Date_Of_Contemplation__c;
	  	account.ContemplationLetter_BR__c       = aDBMImport.Contemplation_Letter__c;
	  	account.RecentBirthdate_BR__c           = aDBMImport.Flag_Recent_Birthdate__c;
	  	account.InfoUpdatedLast5Yrs_BR__c       = aDBMImport.Flag_Info_Updated_Last_5_Years__c;
	  	account.PhoneNumberOK_BR__c             = aDBMImport.Flag_Phone_Number_OK__c;
	  	account.EmailOK_BR__c                   = aDBMImport.Flag_Email_OK__c;
	  	account.AddressOK_BR__c                 = aDBMImport.Flag_Address_OK__c;
	  	account.ParticipateOnOPA_BR__c          = aDBMImport.Flag_Participate_On_OPA__c;
	  	account.VehicleInterest_BR__c           = aDBMImport.Vehicle_of_Interest__c;
	  	account.CadastralCustmer_BR__c          = aDBMImport.Cadastral_Customer__c;
	  	account.BehaviorCustmr_BR__c            = aDBMImport.Behavior_Customer__c;
	  	account.PreApprovedCredit_BR__c         = aDBMImport.Flag_Pre_Approved_Credit__c;
	  	account.MktdatasCluster_BR__c           = aDBMImport.Marketdata_Cluster__c;
	  	account.PropensityModel_BR__c           = aDBMImport.Flag_Propensity_Model__c;
	  	account.AfterSales_BR__c                = aDBMImport.Flag_After_Sales__c;
	  	account.AfterSalesOffer_BR__c           = aDBMImport.After_Sales_Offer__c;
	  	account.RFV__c                          = aDBMImport.RFV__c;
	  	account.ParticipatedInCampaigns__c      = aDBMImport.Flag_Participated_In_Campaigns__c;
	  	account.VehicleOfCampaign__c            = aDBMImport.Vehicle_Of_Campaign__c;
	  	account.ProfMobPhone__c					= aDBMImport.Business_Mobile_Phone__c;
	  	account.ProfLandline__c					= aDBMImport.Business_Phone__c;
	  	account.PersMobPhone__c 				= aDBMImport.Mobile__c;
	}

	/**
	 * Clone account into a lead record
	 */
	private void cloneAccountIntoLead(Lead leadObj, Account account)
	{
		system.debug(LoggingLevel.INFO, '*** cloneAccountIntoLead()');

		leadObj.NUMDBM__c                  = account.NUMDBM_BR__c; // external Id
	  	leadObj.SubSource__c               = account.LeadSubsource__c;
	  	leadObj.FirstName                  = account.FirstName;
	  	leadObj.LastName                   = account.LastName;
	  	leadObj.CPF_CNPJ__c                = account.CustomerIdentificationNbr__c; // exclusive field but not external Id
	  	leadObj.HomePhone__c               = account.PersLandline__c;
	  	leadObj.Email                      = account.PersEmailAddress__c;
	  	leadObj.Street                     = account.ShippingStreet;
	  	leadObj.City                       = account.ShippingCity;
	  	leadObj.State                      = account.ShippingState;
	  	leadObj.PostalCode                 = account.ShippingPostalCode;
	  	leadObj.Country                    = account.ShippingCountry;
	  	leadObj.CustomerProfile__c         = account.CustomerProfile__c;
	  	leadObj.Birthdate__c               = account.PersonBirthdate;
	  	leadObj.CurrentMilage__c           = account.CurrentMilage__c;
	  	leadObj.ValueOfCredit__c           = account.ValueOfCredit__c;
	  	leadObj.EndDateOfFinanciating__c   = account.EndDateOfFinanciating_BR__c;
	  	leadObj.DateOfContemplation__c     = account.DateContemplation_BR__c;
	  	leadObj.ContemplationLetter__c     = account.ContemplationLetter_BR__c;
	  	leadObj.RecentBirthdate__c         = account.RecentBirthdate_BR__c;
	  	leadObj.InformationUpdatedInTheLast5Years__c 
	  	                                   = account.InfoUpdatedLast5Yrs_BR__c;
	  	leadObj.PhoneNumberOK__c           = account.PhoneNumberOK_BR__c;
	  	leadObj.EmailOK__c                 = account.EmailOK_BR__c;
	  	leadObj.AddressOK__c               = account.AddressOK_BR__c;
	  	leadObj.ParticipateOnOPA__c        = account.ParticipateOnOPA_BR__c;
	  	leadObj.VehicleOfInterest__c       = account.VehicleInterest_BR__c;
	  	leadObj.CadastralCustomer__c       = account.CadastralCustmer_BR__c;
	  	leadObj.BehaviorCustomer__c        = account.BehaviorCustmr_BR__c;
	  	leadObj.PreApprovedCredit__c       = account.PreApprovedCredit_BR__c;
	  	leadObj.MarketdatasCluster__c      = account.MktdatasCluster_BR__c;
	  	leadObj.PropensityModel__c         = account.PropensityModel_BR__c;
	  	leadObj.AfterSales__c              = account.AfterSales_BR__c;
	  	leadObj.AfterSalesOffer__c         = account.AfterSalesOffer_BR__c;
	  	leadObj.RFV__c                     = account.RFV__c;
	    leadObj.ParticipatedInCampaigns__c = account.ParticipatedInCampaigns__c;
		leadObj.VehicleOfCampaign__c       = account.VehicleOfCampaign__c;
		leadObj.BusinessMobilePhone__c	   = account.ProfMobPhone__c;
  		leadObj.BusinessPhone__c		   = account.ProfLandline__c;
  		leadObj.MobilePhone				   = account.PersMobPhone__c;
	}

	/**
	 * For Updating Lead Fields With DBM_Import Fields.
	 */
	private void updateLeadFieldsFromImportDBM(Lead leadObj, DBM_Import__c aDBMImport)
	{
		system.debug(LoggingLevel.INFO, '*** UpdateLeadFieldsFromImportDBM()');

	  	leadObj.NUMDBM__c                  = aDBMImport.NUMDBM__c;
	  	leadObj.SubSource__c               = aDBMImport.Sub_Source__c;
	  	leadObj.FirstName                  = aDBMImport.First_Name__c;
	  	leadObj.LastName                   = aDBMImport.Last_Name__c;
	  	leadObj.CPF_CNPJ__c                = aDBMImport.CPF_CNPJ__c;
	  	leadObj.HomePhone__c               = aDBMImport.Home_Phone__c;
	  	leadObj.Email                      = aDBMImport.Email__c;
	  	leadObj.Street                     = aDBMImport.Street__c;
	  	leadObj.City                       = aDBMImport.City__c;
	  	leadObj.State                      = aDBMImport.State__c;
	  	leadObj.PostalCode                 = aDBMImport.Postal_Code__c;
	  	leadObj.Country                    = aDBMImport.Country__c;
	  	leadObj.CustomerProfile__c         = aDBMImport.Customer_Profile__c;
	  	leadObj.Birthdate__c               = aDBMImport.Birthdate__c;
	  	leadObj.CurrentMilage__c           = aDBMImport.Current_Milage__c;
	  	leadObj.ValueOfCredit__c           = aDBMImport.Value_Of_Credit__c;
	  	leadObj.EndDateOfFinanciating__c   = aDBMImport.End_Date_Of_Financiating__c;
	  	leadObj.DateOfContemplation__c     = aDBMImport.Date_Of_Contemplation__c;
	  	leadObj.ContemplationLetter__c     = aDBMImport.Contemplation_Letter__c;
	  	leadObj.RecentBirthdate__c         = aDBMImport.Flag_Recent_Birthdate__c;
	  	leadObj.InformationUpdatedInTheLast5Years__c 
	  	                                   = aDBMImport.Flag_Info_Updated_Last_5_Years__c;
	  	leadObj.PhoneNumberOK__c           = aDBMImport.Flag_Phone_Number_OK__c;
	  	leadObj.EmailOK__c                 = aDBMImport.Flag_Email_OK__c;
	  	leadObj.AddressOK__c               = aDBMImport.Flag_Address_OK__c;
	  	leadObj.ParticipateOnOPA__c        = aDBMImport.Flag_Participate_On_OPA__c;
	  	leadObj.VehicleOfInterest__c       = aDBMImport.Vehicle_of_Interest__c;
	  	leadObj.CadastralCustomer__c       = aDBMImport.Cadastral_Customer__c;
	  	leadObj.BehaviorCustomer__c        = aDBMImport.Behavior_Customer__c;
	  	leadObj.PreApprovedCredit__c       = aDBMImport.Flag_Pre_Approved_Credit__c;
	  	leadObj.MarketdatasCluster__c      = aDBMImport.Marketdata_Cluster__c;
	  	leadObj.PropensityModel__c         = aDBMImport.Flag_Propensity_Model__c;
	  	leadObj.AfterSales__c              = aDBMImport.Flag_After_Sales__c;
	  	leadObj.AfterSalesOffer__c         = aDBMImport.After_Sales_Offer__c;
	  	leadObj.RFV__c                     = aDBMImport.RFV__c;
	    leadObj.ParticipatedInCampaigns__c = aDBMImport.Flag_Participated_In_Campaigns__c;
		leadObj.VehicleOfCampaign__c       = aDBMImport.Vehicle_Of_Campaign__c;
		leadObj.BusinessMobilePhone__c     = aDBMImport.Business_Mobile_Phone__c;
  		leadObj.BusinessPhone__c    	   = aDBMImport.Business_Phone__c;
  		leadObj.MobilePhone				   = aDBMImport.Mobile__c;
	}

	/**
	 * To Check any Open Leads are Available for Particular Account.
	 */
	private Lead returnOpenLead(Id AccountId, DBM_Import__c importDBM, Map<Decimal, Lead> mapNUMDBMLead, Map<String, List<Lead>> mapCPFandCNPJLeads)
	{	 
		system.debug(LoggingLevel.INFO, '*** returnOpenLead()');

		if (importDBM.NUMDBM__c != null && mapNUMDBMLead.containsKey(importDBM.NUMDBM__c)) {
			return mapNUMDBMLead.get(importDBM.NUMDBM__c); 
		}
		else if (importDBM.CPF_CNPJ__c != null && mapCPFandCNPJLeads.containsKey(importDBM.CPF_CNPJ__c)) {
			List<Lead> leads = mapCPFandCNPJLeads.get(importDBM.CPF_CNPJ__c);

			// Return the first lead with the same CPF
			if (!leads.isEmpty())
				return leads.get(0);
			else
				return null;
		}
		else {
			return null;
		}
	}
	
	/**
	 * Check if a Molicar object exists. If found, set the variable molicar and return true,
	 * else leave molicar as null and return false.
	 */
	private Boolean checkMolicarObjectExists(DBM_Import__c importDBM)
	{
		Boolean isMolicar = false;	
	
		if (mapBrandWithMolicars.containsKey(importDBM.Molicar_Brand__c))
  	   	{
			List<MLC_Molicar__c> lstMolicar = mapBrandWithMolicars.get(importDBM.Molicar_Brand__c);
			for (MLC_Molicar__c molicarObj : lstMolicar) 
			{
				if (molicarObj.Model__c == importDBM.Molicar_Model__c
  	   	      	 && molicarObj.Configuration__c == importDBM.Molicar_Configuration__c)
  	   	      	{
  	   	      	 	molicar = molicarObj;
  	   	      	 	isMolicar = true;
  	   	      	 	break;
				}
			}    	
		}
		return isMolicar;
	}
	
	/**
	 * Check if is an Account or a Lead and fill some global variables
	 */
	private String checkIfAccountOrLead(DBM_Import__c importDBM)
	{
		Boolean found = false;

		if (!found && importDBM.NUMDBM__c !=null && mapNUMDBMAccounts.containsKey(importDBM.NUMDBM__c)) {
			fetchedAccount = mapNUMDBMAccounts.get(importDBM.NUMDBM__c);
  	   	    isAccount = true;
  	   	    system.debug(LoggingLevel.INFO, '*** Account NUMDBM__c found');
  	   	    found = true;
		}
		
		if (!found && importDBM.Id_Salesforce__c !=null && mapIdWithAccount.containsKey(importDBM.Id_Salesforce__c)) {
			fetchedAccount = mapIdWithAccount.get(importDBM.Id_Salesforce__c);
			isAccount = true;
  	   	    system.debug(LoggingLevel.INFO, '*** Account Id_Salesforce__c found');
  	   	    found = true;
		}
  	   	
  	   	if (!found && importDBM.CPF_CNPJ__c != null && mapCPFandCNPJAccounts.containsKey(importDBM.CPF_CNPJ__c)) {
			fetchedAccount = mapCPFandCNPJAccounts.get(importDBM.CPF_CNPJ__c);
  	   	    isAccount = true;
  	   	    system.debug(LoggingLevel.INFO, '*** Account CPF_CNPJ__c found');
  	   	    found = true;
		} 
  	   	
  	   	if (!found && importDBM.Id_Salesforce__c != null && mapIdWithLead.containsKey(importDBM.Id_Salesforce__c) && isAccount == false) {
			fetchedLead = mapIdWithLead.get(importDBM.Id_Salesforce__c);
			isLead = true;
			system.debug(LoggingLevel.INFO, '*** Lead Id_Salesforce__c found');
			found = true; }
		
		if (!found && importDBM.NUMDBM__c != null && mapNUMDBMLead.containsKey(importDBM.NUMDBM__c) && isAccount == false) {
			fetchedLead = mapNUMDBMLead.get(importDBM.NUMDBM__c);
			isLead = true;
			system.debug(LoggingLevel.INFO, '*** Lead NUMDBM__c found');
			found = true;
		} 
		
		if (!found && importDBM.CPF_CNPJ__c != null && mapCPFandCNPJLeads.containsKey(importDBM.CPF_CNPJ__c) && isAccount == false) {
			List<Lead> leads = mapCPFandCNPJLeads.get(importDBM.CPF_CNPJ__c);
      	   	for (Lead Leadobj : leads)
      	   	{
				if (Leadobj.NUMDBM__c == importDBM.NUMDBM__c)
      	   	    {
      	   	    	// Give preference to the lead with the same NUMDBM
					fetchedLead = Leadobj;
					isLead = true;
					system.debug(LoggingLevel.INFO, '*** Lead by NUMDBM__c, CPF_CNPJ__c found');
					found = true;
      	   	    }
      	   	    else
      	   	    {
      	   	    	// Use the last lead in the list
      	   	    	fetchedLead = Leadobj;
					isLead = true;
					system.debug(LoggingLevel.INFO, '*** Lead by else, CPF_CNPJ__c found');
					found = true;
      	   	    }
			}
		}
		
		if (!found)
		{
			isLead = true;
			isNewLead = true;
			system.debug(LoggingLevel.INFO, '*** isLead and isNewLead true');
		}
		return '';
	}
}