/**
* Classe que contém métodos utilitários relacionados a construção de pickLists.
* @author Felipe Jesus Silva.
*/
public class VFC49_PickListUtil 
{	
    
    public static List<SelectOption> buildPickList()
    {
        List<SelectOption> lstSelOption = new List<SelectOption>();
        
        List<Product2> produtoList = [
            SELECT Id, Name, ActiveToSFA__c FROM Product2 WHERE ActiveToSFA__c = true
        ];
        
        for(Product2 produto : produtoList) 
        { 
            lstSelOption.add(new SelectOption(produto.Name, produto.Name)); 
        }
        
        return lstSelOption;
    }
    
    public static List<SelectOption> buildPickList(Schema.SObjectField field)
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		List<Schema.PicklistEntry> lstPickListEntry = field.getDescribe().getPicklistValues();
		
		for(Schema.PicklistEntry pickListEntry : lstPickListEntry) 
		{ 
        	lstSelOption.add(new SelectOption(pickListEntry.getValue(), pickListEntry.getLabel())); 
      	}
		
		return lstSelOption;
	}
	
	public static List<SelectOption> buildPickList(Schema.SObjectField field, String firstOption)
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		List<Schema.PicklistEntry> lstPickListEntry = field.getDescribe().getPicklistValues();
				
		lstSelOption.add(new SelectOption(firstOption, firstOption));
		
		for(Schema.PicklistEntry pickListEntry : lstPickListEntry) 
		{ 
        	lstSelOption.add(new SelectOption(pickListEntry.getValue(), pickListEntry.getLabel())); 
      	}
		
		return lstSelOption;
	}
	
	public static List<SelectOption> buildPickList(Schema.SObjectField field, Set<String> setValues)
	{
		List<SelectOption> lstSelOption = new List<SelectOption>();
		List<Schema.PicklistEntry> lstPickListEntry = field.getDescribe().getPicklistValues();
						
		for(Schema.PicklistEntry pickListEntry : lstPickListEntry) 
		{ 
        	if(setValues.contains(pickListEntry.getValue()))
        	{
        		lstSelOption.add(new SelectOption(pickListEntry.getValue(), pickListEntry.getLabel())); 
        	}       	
      	}
		
		return lstSelOption;
	}
	
	
}