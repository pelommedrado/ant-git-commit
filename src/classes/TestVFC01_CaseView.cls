@isTest
public with sharing class TestVFC01_CaseView  
{   
 static testMethod void CaseView_Test(){
       User usr = new User (LastName='ts1',FirstName='Rotondo1', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId='00eD0000001PhWZIA0',TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com',RecordDefaultCountry__c='Brazil');
       List<Case> cslist=new List<Case>();
       //Test start
       Test.startTest(); 
       System.runas(usr){
       Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId='012D0000000KAoFIAW', ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
       insert Acc;
       Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',PersoEmail__c='lro@lro.com',Email='lro@lro.com',AccountId=Acc.Id);
       insert Con;
       Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
       try{
       insert c;   
       cslist.add(c);    
       }catch(Exception e){
           System.debug('Error'+e);
        }
       ApexPages.StandardController controller1=new ApexPages.StandardController(c);   
       VFC01_CaseView tst1=new  VFC01_CaseView(controller1);     
       tst1.getSortDirection();
       tst1.setSortDirection('ss');       
       tst1.getCasePageList();
       tst1.first();
       tst1.last();
       tst1.previous();
       tst1.next();
       tst1.CaseViewList();
       tst1.redirectToPage();    
       }
    Test.StopTest();
    //Test end    
    }
    static testMethod void testVFC01_CaseView()
    {
       User usr = new User (LastName='ts1',FirstName='Rotondo1', alias='lro',Email='lrotondo@rotondo.com',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId='00eD0000001PhWZIA0',TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com', RecordDefaultCountry__c='Brazil');
       List<Case> cslist=new List<Case>();
       Test.startTest(); 
       System.runas(usr){
       
       
       Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId='012D0000000KAoFIAW', ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
       try {
       insert Acc;
      }catch(DMLException e) {System.debug('Acc'+e);}
      // Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
        Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',PersoEmail__c='lro@lro.com',Email='lro@lro.com',AccountId=Acc.Id);
         try {
        insert Con;
         }catch(DMLException e) {System.debug('Con...'+e);}
       // Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
          Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
        try{
        insert c;
        cslist.add(c);   
        SYstem.debug('cslist--->'+cslist); 
        }
        catch(DMLException e) {System.debug('edddddddddddd'+e);}
        catch(Exception e){System.debug('esssssssssss'+e);}
       
    ApexPages.StandardController controller1=new ApexPages.StandardController(c);
   VFC01_CaseView ts1=new  VFC01_CaseView(controller1);
   ts1.getSortDirection();   
   ts1.getCasePageList();
   ts1.first();
       ts1.last();
       ts1.previous();
       ts1.next();
   ts1.CaseViewList();
      
   ts1.redirectToPage();
    
    }
    Test.StopTest();
    }
    }