@isTest
private class FaturaNotaServiceTest {

	
	@isTest static void shouldCreateOwnership() {
        System.debug('!!!shouldCreateOwnership');
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '51820533000150';
		Database.insert(acc);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();
		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

		lstFaturas.add(fatura);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(1, checkVehRel.size());
	}

    @isTest static void shouldFailAtCustomerIdentification() {
        System.debug('!!!shouldFailAtCustomerIdentification');
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '51820533000150';
		Database.insert(acc);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'XXXXXXXXXXXXXX';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = fatura.Invoice_Number__c + fatura.BIR_NFE_Issuer__c + fatura.Invoice_Serie__c;
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

		lstFaturas.add(fatura);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(0, checkVehRel.size());
	}

	/* ESTRANGEIRO AINDA PARA ARRUMAR

    @isTest static void shouldPassAtCustomerIdentification() {
        System.debug('!!!shouldPassAtCustomerIdentification');

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'Estrangeiro';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
        fatura.Invoice_Number__c = '123';
        fatura.BIR_NFE_Issuer__c = '456';
        fatura.Invoice_Serie__c  = '789';
        fatura.External_Key__c = fatura.Invoice_Number__c + fatura.BIR_NFE_Issuer__c + fatura.Invoice_Serie__c;
        fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
        Database.insert(fatura);

		lstFaturas.add(fatura);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals('', [SELECT Id, Error_Messages__c FROM FaturaDealer2__c].Error_Messages__c);

        System.assertEquals(1, checkVehRel.size());

	}

	*/

    @isTest static void shouldFaillAtEveryValidation() {
        System.debug('!!!shouldFaillAtEveryValidation');
		Account acc = new Account();

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'XXXXXXXXXXX';
		fatura.VIN__c = '3425GSVFAGS23XXXX';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadfsdfdas.com';
		fatura.Phone_1__c = '1X2738112X';
		fatura.Phone_2__c = '1X9909887X';
		fatura.Phone_3__c = '1X2098776X';
		fatura.External_Key__c = fatura.Invoice_Number__c + fatura.BIR_NFE_Issuer__c + fatura.Invoice_Serie__c;
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

		lstFaturas.add(fatura);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(0, checkVehRel.size());
	}

    @isTest static void shouldCreateThreeOwnership() {
        System.debug('!!!shouldCreateThreeOwnership');
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '51820533000150';
		Database.insert(acc);

        Account acc2 = new Account();
		acc2.Name = 'testAccount2';
		acc2.Phone = '12345678';
		acc2.CustomerIdentificationNbr__c = '46843566634';
		Database.insert(acc2);

        Account acc3 = new Account();
		acc3.Name = 'testAccount3';
		acc3.Phone = '12345678';
		acc3.CustomerIdentificationNbr__c = '26288272710';
		Database.insert(acc3);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

        VEH_Veh__c veh2 = new VEH_Veh__c();
		veh2.Name = '1234ABCDEFG123456';
		Database.insert(veh2);

        VEH_Veh__c veh3 = new VEH_Veh__c();
		veh3.Name = '5678HIJKLMN654321';
		Database.insert(veh3);

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();
		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '46843566634';
		fatura2.VIN__c = '1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'New Vehicle';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '1127381122';
		fatura2.Phone_2__c = '1299098876';
		fatura2.Phone_3__c = '1320987761';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura2);

        faturaDealer2__c fatura3 = new faturaDealer2__c();
		fatura3.Customer_Identification_Number__c = '26288272710';
		fatura3.VIN__c = '5678HIJKLMN654321';
		fatura3.Invoice_Type__c = 'New Vehicle';
		fatura3.Manufacturing_Year__c = '1994';
        fatura3.Customer_Type__c = 'F';
		fatura3.Model_Year__c = '1994';
		fatura3.Customer_Email__c = 'dadf@sdfdas.com';
		fatura3.Phone_1__c = '1127381122';
		fatura3.Phone_2__c = '1299098876';
		fatura3.Phone_3__c = '1320987761';
		fatura3.External_Key__c = '789';
		fatura3.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura3);

		lstFaturas.add(fatura);
        lstFaturas.add(fatura2);
        lstFaturas.add(fatura3);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(3, checkVehRel.size());
	}

    @isTest static void shouldCreateOnlyTwoOwnership() {
        System.debug('!!!shouldCreateOnlyTwoOwnership');
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '51820533000150';
		Database.insert(acc);

        Account acc2 = new Account();
		acc2.Name = 'testAccount2';
		acc2.Phone = '12345678';
		acc2.CustomerIdentificationNbr__c = '46843566634';
		Database.insert(acc2);

        Account acc3 = new Account();
		acc3.Name = 'testAccount3';
		acc3.Phone = '12345678';
		acc3.CustomerIdentificationNbr__c = '26288272710';
		Database.insert(acc3);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

        VEH_Veh__c veh2 = new VEH_Veh__c();
		veh2.Name = '1234ABCDEFG123456';
		Database.insert(veh2);

        VEH_Veh__c veh3 = new VEH_Veh__c();
		veh3.Name = '5678HIJKLMN654321';
		Database.insert(veh3);

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();
		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'Estrangeiro';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '46843566634';
		fatura2.VIN__c = '1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'New Vehicle';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '1127381122';
		fatura2.Phone_2__c = '1299098876';
		fatura2.Phone_3__c = '1320987761';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura2);

        faturaDealer2__c fatura3 = new faturaDealer2__c();
		fatura3.Customer_Identification_Number__c = '26288272710';
		fatura3.VIN__c = '5678HIJKLMN654321';
		fatura3.Invoice_Type__c = 'New Vehicle';
		fatura3.Manufacturing_Year__c = '1994';
        fatura3.Customer_Type__c = 'F';
		fatura3.Model_Year__c = '1994';
		fatura3.Customer_Email__c = 'dadf@sdfdas.com';
		fatura3.Phone_1__c = '1127381122';
		fatura3.Phone_2__c = '1299098876';
		fatura3.Phone_3__c = '1320987761';
		fatura3.External_Key__c = '789';
		fatura3.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura3);

		lstFaturas.add(fatura);
        lstFaturas.add(fatura2);
        lstFaturas.add(fatura3);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(2, checkVehRel.size());
	}

    @isTest static void shouldCreateTwoOwnershipWithoutFoundingReferences() {
        System.debug('!!!shouldCreateTwoOwnershipWithoutFoundingReferences');

		List<faturaDealer2__c> lstFaturas = new List<faturaDealer2__c>();
		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '46843566634';
		fatura2.VIN__c = '1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'New Vehicle';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '11273811221';
		fatura2.Phone_2__c = '12990988761';
		fatura2.Phone_3__c = '13209877611';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
      	Database.insert(fatura2);

		lstFaturas.add(fatura);
        lstFaturas.add(fatura2);

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

        List<VRE_VehRel__c> checkVehRel = [SELECT Id FROM VRE_VehRel__c];

        System.assertEquals(2, checkVehRel.size());
	}

	@isTest static void shouldRun(){
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '51820533000150';
		Database.insert(acc);

        Account acc2 = new Account();
		acc2.Name = 'testAccount2';
		acc2.Phone = '12345678';
		acc2.CustomerIdentificationNbr__c = '46843566634';
		Database.insert(acc2);

        Account acc3 = new Account();
		acc3.Name = 'testAccount3';
		acc3.Phone = '12345678';
		acc3.CustomerIdentificationNbr__c = '26288272710';
		Database.insert(acc3);

		System.debug('!!!contas: '+acc+' '+acc2+' '+acc3);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '3425GSVFAGS234637';
		Database.insert(veh);

        VEH_Veh__c veh2 = new VEH_Veh__c();
		veh2.Name = '1234ABCDEFG123456';
		Database.insert(veh2);

        VEH_Veh__c veh3 = new VEH_Veh__c();
		veh3.Name = '5678HIJKLMN654321';
		Database.insert(veh3);

		System.debug('!!!veículos: '+veh+' '+veh2+' '+veh3);

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'J';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1227381122';
		fatura.Phone_3__c = '1320987761';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '46843566634';
		fatura2.VIN__c = '1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'New Vehicle';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '1127381122';
		fatura2.Phone_2__c = '1299098876';
		fatura2.Phone_3__c = '1320987761';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura2);

        faturaDealer2__c fatura3 = new faturaDealer2__c();
		fatura3.Customer_Identification_Number__c = '26288272710';
		fatura3.VIN__c = '5678HIJKLMN654321';
		fatura3.Invoice_Type__c = 'New Vehicle';
		fatura3.Manufacturing_Year__c = '1994';
        fatura3.Customer_Type__c = 'F';
		fatura3.Model_Year__c = '1994';
		fatura3.Customer_Email__c = 'dadf@sdfdas.com';
		fatura3.Phone_1__c = '1127381122';
		fatura3.Phone_2__c = '1299098876';
		fatura3.Phone_3__c = '1320987761';
		fatura3.External_Key__c = '789';
		fatura3.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura3);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);
		lstFaturas.add(fatura2);
		lstFaturas.add(fatura3);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		System.debug('!!!Test Result - Fatura:'   			+	[SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,

																		Status__c
																FROM faturaDealer2__c]);
		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);
		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);

		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];
		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals(3, checkVRE.size());
	}

	@isTest static void shouldRunWithSomeFails(){

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'XXX';//'51820533000150';
		fatura.VIN__c = 'XXXXXXXXXXXXXXXXX';//'3425GSVFAGS234637';
		fatura.Invoice_Type__c = 'Parts and Accessories';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'XXX';//'dadf@sdfdas.com';
		fatura.Phone_1__c = 'XX27381122XXX';
		fatura.Phone_2__c = 'XX27381122XXX';
		fatura.Phone_3__c = 'XX20987761XXX';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '';//'46843566634';
		fatura2.VIN__c = 'XXXXXXXXXXXXXXXXX';//'1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'Parts and Accessories';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '27381122';
		fatura2.Phone_2__c = '99098876';
		fatura2.Phone_3__c = '20987761';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura2);

        faturaDealer2__c fatura3 = new faturaDealer2__c();
		fatura3.Customer_Identification_Number__c = '26288272710';
		fatura3.VIN__c = '5678HIJKLMN654321';
		fatura3.Invoice_Type__c = 'New Vehicle';
		fatura3.Manufacturing_Year__c = '1994';
        fatura3.Customer_Type__c = 'F';
		fatura3.Model_Year__c = '1994';
		fatura3.Customer_Email__c = 'dadf@sdfdas.com';
		fatura3.Phone_1__c = '1127381122';
		fatura3.Phone_2__c = '1299098876';
		fatura3.Phone_3__c = '1320987761';
		fatura3.External_Key__c = '789';
		fatura3.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura3);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);
		lstFaturas.add(fatura2);
		lstFaturas.add(fatura3);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		System.debug('!!!Test Result - Fatura:'   			+	[SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,

																		Status__c
																FROM faturaDealer2__c]);
		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		Name,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);
		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);
		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];

		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals(1, checkVRE.size());
	}

	@isTest static void shouldRunOnceAgain(){
		Account acc3 = new Account();
		acc3.FirstName = 'test';
		acc3.LastName = 'Account3';
		acc3.Phone = '12345678';
		acc3.CustomerIdentificationNbr__c = '26288272710';
		acc3.PersMobPhone__c = '12345678';
		acc3.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
		Database.insert(acc3);

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = 'XXX';//'51820533000150';
		fatura.VIN__c = '3425GSVFAGS000000';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'XXX';//'dadf@sdfdas.com';
		fatura.Phone_1__c = '';
		fatura.Phone_2__c = '';
		fatura.Phone_3__c = '';
		fatura.External_Key__c = '123';
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

        faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '';//'46843566634';
		fatura2.VIN__c = 'XXXXXXXXXXXXXXXXX';//'1234ABCDEFG123456';
		fatura2.Invoice_Type__c = 'Parts and Accessories';
		fatura2.Manufacturing_Year__c = '1994';
        fatura2.Customer_Type__c = 'F';
		fatura2.Model_Year__c = '1994';
		fatura2.Customer_Email__c = 'dadf@sdfdas.com';
		fatura2.Phone_1__c = '1127381122';
		fatura2.Phone_2__c = '1299098876';
		fatura2.Phone_3__c = '1320987761';
		fatura2.External_Key__c = '456';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura2);

        faturaDealer2__c fatura3 = new faturaDealer2__c();
		fatura3.Customer_Identification_Number__c = '26288272710';
		fatura3.VIN__c = '5678HIJKLMN654321';
		fatura3.Invoice_Type__c = 'New Vehicle';
		fatura3.Manufacturing_Year__c = '1994';
        fatura3.Customer_Type__c = 'F';
		fatura3.Model_Year__c = '1994';
		fatura3.Customer_Email__c = 'dadf@sdfdas.com';
		fatura3.Phone_1__c = '1127381122';
		fatura3.Phone_2__c = '1299098876';
		fatura3.Phone_3__c = '1320987761';
		fatura3.External_Key__c = '789';
		fatura3.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura3);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);
		lstFaturas.add(fatura2);
		lstFaturas.add(fatura3);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		System.debug('!!!Test Result - Fatura:'   			+	[SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,

																		Status__c
																FROM faturaDealer2__c]);
		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		Name,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);
		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);
		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];

		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals(1, checkVRE.size());
	}

	@isTest static void shouldRuntTestingNewUpdatesDevolution(){
		Account acc = new Account();
		acc.FirstName = 'test';
		acc.LastName = 'Account3';
		acc.Phone = '12345678';
		acc.CustomerIdentificationNbr__c = '26288272710';
		acc.PersMobPhone__c = '12345678';
		acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
		Database.insert(acc);

		VEH_Veh__c veh = new VEH_Veh__c();
		veh.Name = '5678HIJKLMN654321';
		Database.insert(veh);


        faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '26288272710';
		fatura.VIN__c = '5678HIJKLMN654321';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.Sequential_File__c = '123';
		fatura.External_Key__c = '789';
        fatura.Invoice_Date_Mirror__c = System.today().addDays(-1);
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

		faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '26288272710';
		fatura2.VIN__c = '5678HIJKLMN654321';
		fatura2.Sequential_File__c = '123';
		fatura2.Invoice_Type__c = 'Devolution';
		fatura2.External_Key__c = '190';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
        fatura2.Invoice_Date_Mirror__c = System.today();
		Database.insert(fatura2);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);
		lstFaturas.add(fatura2);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		List<faturaDealer2__c> checkFaturas 				= [SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,
																		isReturned__c,
																		Status__c
																FROM faturaDealer2__c];

		System.debug('!!!Test Result - Fatura:'   			+	checkFaturas);

		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		Name,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);
		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);
		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];

		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals('Inactive', checkVRE[0].Status__c);

		System.assertEquals(true, checkFaturas[0].isReturned__c);

	}

	@isTest static void shouldRunNewUpdatesCancellation(){
		Account acc3 = new Account();
		acc3.FirstName = 'test';
		acc3.LastName = 'Account3';
		acc3.Phone = '12345678';
		acc3.CustomerIdentificationNbr__c = '26288272710';
		acc3.PersMobPhone__c = '12345678';
		acc3.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
		Database.insert(acc3);

        faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.Customer_Identification_Number__c = '26288272710';
		fatura.VIN__c = '5678HIJKLMN654321';
		fatura.Invoice_Type__c = 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
        fatura.Customer_Type__c = 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1299098876';
		fatura.Phone_3__c = '1320987761';
		fatura.Sequential_File__c = '123';
		fatura.External_Key__c = '789';
        fatura.Invoice_Date_Mirror__c = System.today().addDays(-1);
		fatura.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		Database.insert(fatura);

		faturaDealer2__c fatura2 = new faturaDealer2__c();
		fatura2.Customer_Identification_Number__c = '26288272710';
		fatura2.VIN__c = '5678HIJKLMN654321';
		fatura2.Invoice_Type__c = 'Cancellation';
		fatura2.Sequential_File__c = '123';
		fatura2.External_Key__c = '190';
		fatura2.RecordTypeId = Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
        fatura2.Invoice_Date_Mirror__c = System.today();
		Database.insert(fatura2);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);
		lstFaturas.add(fatura2);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		List<faturaDealer2__c> checkFaturas 				= [SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,
																		isReturned__c,
																		isCancelled__c,
																		Status__c
																FROM faturaDealer2__c];

		System.debug('!!!Test Result - Fatura:'   			+	checkFaturas);

		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		Name,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);

		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);

		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];

		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals('Inactive', checkVRE[0].Status__c);

		System.assertEquals(true, checkFaturas[0].isCancelled__c);

	}

	@isTest static void shouldSuccessfullyPass(){

		faturaDealer2__c fatura = new faturaDealer2__c();
		fatura.BIR_NFE_Issuer__c='7600548.000000000000000';
		fatura.BIR_Name_NFE_Issuer__c='SBV COMERCIAL DE VEIC. PEC E SERV LTDA';
		fatura.Invoice_Type__c='Devolution';
		fatura.VIN__c='93Y5SRD04GJ485889';
		fatura.Integration_Protocol__c='e830160c57ad278464ccecbb5b35b96e';
		fatura.Sequential_File__c='3276939.000000000000000';
		fatura.Invoice_Number__c='NFe35170412286960000149550010000364761868981290';
		fatura.Invoice_Serie__c='1.000000000000000';
		fatura.Invoice_Data__c='2017-04-24T12:30:00-03:00';
		fatura.Delivery_Date__c='28/10/2016 00:00:00.000000';
		fatura.Customer_Name__c='IVANETE VIEIRA DE MENEZES';
		fatura.Address__c='R BELA,09';
		fatura.Number__c='9.000000000000000';
		fatura.Neighborhood__c='PARQUE BOTUJURU';
		fatura.City__c='SAO BERNARDO DO CAMP';
		fatura.Postal_Code__c='9821230.000000000000000';
		fatura.State__c='SP';
		fatura.Customer_Type__c='F';
		fatura.Customer_Identification_Number__c='2215211865.000000000000000';
		fatura.Customer_Email__c='ivonetebojuru@hotmail.com';
		fatura.DDD1__c='11041014604.000000000000000';
		fatura.Phone_1__c='11041014604.000000000000000';
		fatura.Manufacturing_Year__c='2016.000000000000000';
		fatura.Model_Year__c='2016.000000000000000';
		fatura.Payment_Type__c='0.000000000000000';
		fatura.RecordTypeId=Utils.getRecordTypeId('FaturaDealer2__c','Invoice');
		fatura.External_Key__c='NFe3517041228696000014955001000036476186898129076005481';
		Database.insert(fatura);

		List<FaturaDealer2__c> lstFaturas = new List<FaturaDealer2__c>();
		lstFaturas.add(fatura);

		test.startTest();

		FaturaNotaService servicoNota = new FaturaNotaService(lstFaturas);
      	servicoNota.startValidation();

		test.stopTest();

		System.debug('!!!Test Result - Fatura:'   			+	[SELECT Id,
																		Error_Messages__c,
																		Invoice_Type__c,
																		Customer_Email__c,
																		Customer_Email_Mirror__c,

																		Status__c
																FROM faturaDealer2__c]);
		System.debug('!!!Test Result - Contas:'   			+	[SELECT Id,
																		CustomerIdentificationNbr__c,
																		Email__c,
																		Shipping_Street__c,
																		Shipping_Complement__c,
																		Shipping_Neighborhood__c,
																		Shipping_City__c,
																		Shipping_PostalCode__c,
																		Shipping_State__c,
																		Phone,
																		FirstName,
																		LastName,
																		PersonBirthdate,
																		PersLandline__c,
																		PersMobPhone__c
																FROM Account]);
		System.debug('!!!Test Result - Veículos:' 			+	[SELECT Id,
																		Name,
																		DateofManu__c,
																		ModelYear__c,
																		DeliveryDate__c
																FROM VEH_Veh__c]);

		List<VRE_VehRel__c> checkVRE 						=	[SELECT Id,
																		Account__c,
																		VIN__c,
																		Status__c
																FROM VRE_VehRel__c];
		System.debug('!!!Test Result - Relação de Posse:' 	+	checkVRE);

		System.assertEquals(0, checkVRE.size());
	}
}