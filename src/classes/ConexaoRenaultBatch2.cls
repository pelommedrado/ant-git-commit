global class ConexaoRenaultBatch2 implements Schedulable, Database.Batchable<sObject> {

	String query = null;
	Integer num = null;

	global ConexaoRenaultBatch2() {
		query = buildQueryFatura();
	}

	global void execute(SchedulableContext sc) {
  	System.debug('Iniciando a execucao do batch');
    final ConexaoRenaultBatch2 fatBatch = new ConexaoRenaultBatch2();
    Database.executeBatch(fatBatch);
  }

	private String buildQueryFatura() {
		return 'SELECT Id, Invoice_Date_Mirror__c, VN_Survey_Date__c'
			+ ' FROM FaturaDealer2__c'
			+ ' WHERE Status__c = \'Valid\' AND Customer_Type__c = \'F\''
			+ ' AND Invoice_Type__c = \'New Vehicle\''
			+ ' AND InvoiceDateFilled__c = TRUE AND VN_Survey_Date__c = NULL';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('execute()');
		ConexaoRenaultBo.getInstance().executeFatura2(scope);
	}

	global void finish(Database.BatchableContext BC) {
	}
}