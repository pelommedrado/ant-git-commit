global class UserInformationCleanBatch implements Schedulable, Database.Batchable<sObject> {
    
    global void execute(SchedulableContext sc) {
        System.debug('Iniciando a execucao do batch');
        
        UserInformationCleanBatch cleanBatch = new UserInformationCleanBatch();
        Database.executeBatch(cleanBatch);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id FROM UserInformation__c WHERE CreatedDate < LAST_N_DAYS:90');
    }
    
    global void execute(Database.BatchableContext BC, List<UserInformation__c> scope) {
        try {
            System.debug('Executando a limpeza...');
            System.debug('Scope: ' +  scope.size());
            
            DELETE scope;
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
}