@isTest
public class SFA_RenaultMaisCliente_HeaderTest {
    
    public static Account acc { get;set; }
    
    static testmethod void test() {
        
        W2CParams__c w2c = new W2CParams__c(
        	Org_Id__c = '005c0000001YdcR',
            RecordTypeId__c = '012Q0000000Gj9cAAC',
            Name = 'PRD',
            URLW2C__c = 'www.teste.teste.com'
        );
        Database.insert(w2c);
        
        User user = criarUser();
        System.runAs(user) {
            
            UserInformation__c info = new UserInformation__c();
            info.Application__c = 'CAC';
            info.User__c = user.Id;
            info.Dealer__c = acc.Id;
            
            insert info;
            
            SFA_RenaultMaisCliente_HeaderController ctl = new SFA_RenaultMaisCliente_HeaderController();
            ctl.init();
            List<String> optionAppList = ctl.optionAppList;
            SFA_RenaultMaisCliente_HeaderController.setApplication('CAC', UserInfo.getSessionId());
        }
        
        MyOwnCreation userMoc = new MyOwnCreation();
        User userComm = userMoc.CriaUsuarioComunidade();
        System.runAs(userComm){
            Boolean teste = SFA_RenaultMaisCliente_HeaderController.supportUser;
        }
    }
    
    static testMethod void testSetHomePage(){
        List<String> monthList = new List<String>{
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        };
        
        String year = String.valueOf(Date.today().year());
        String month = monthList[Date.today().month()-1];
        
        String responseAssert = '/cac/apex/ObjetivoSeller?ano=' + year +'&mes=' + month;
        
        Test.startTest();
        
        System.assertEquals('/cac/apex/VFP20_RMCDesktop', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Vendedor'));
        System.assertEquals('/cac/apex/VFP20_RMCDesktop', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Recepcionista'));
        System.assertEquals('/cac/apex/CAC_DealerChoiceDeptCase', SFA_RenaultMaisCliente_HeaderController.setHomePage('CAC'));
        System.assertEquals('/cac/apex/SFA_PassingPurchase', SFA_RenaultMaisCliente_HeaderController.setHomePage('Pedidos Diários'));
        System.assertEquals('/renaultmaiscliente/sfc/#search', SFA_RenaultMaisCliente_HeaderController.setHomePage('Biblioteca'));
        System.assertEquals('/cac/apex/VFP31_VehicleTracking', SFA_RenaultMaisCliente_HeaderController.setHomePage('Logística - Concessionário'));
        System.assertEquals('/cac/apex/VFP31_VehicleTracking', SFA_RenaultMaisCliente_HeaderController.setHomePage('Logística - Assistente Comercial'));
        System.assertEquals('/cac/apex/FaturaDealer', SFA_RenaultMaisCliente_HeaderController.setHomePage('Acompanhamento DCL.CRM'));
        System.assertEquals('/cac/a1G/o', SFA_RenaultMaisCliente_HeaderController.setHomePage('Plataforma de Varejo - Grupo'));
        System.assertEquals('/cac/a1G/o', SFA_RenaultMaisCliente_HeaderController.setHomePage('Plataforma de Varejo - Concessionária'));
        System.assertEquals('/cac/00O570000072IBa', SFA_RenaultMaisCliente_HeaderController.setHomePage('E3M'));
        System.assertEquals('/cac/AgendaServico', SFA_RenaultMaisCliente_HeaderController.setHomePage('Request a Service'));
        System.assertEquals('/cac/apex/MNERpage', SFA_RenaultMaisCliente_HeaderController.setHomePage('MNER'));
        System.assertEquals('/cac/apex/VFP24_ManagerLeadBox', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Gerente'));
        System.assertEquals('/cac/apex/leadsMontadora', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Gerente - Grupo'));
        System.assertEquals('/cac/_ui/core/chatter/ui/ChatterPage', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA – Gestão Comercial'));
        System.assertEquals('/cac/apex/LeadBoxCommunity', SFA_RenaultMaisCliente_HeaderController.setHomePage('Leads Montadora'));
        System.assertEquals('/cac/apex/getvoucher', SFA_RenaultMaisCliente_HeaderController.setHomePage('Ações Promocionais'));
        System.assertEquals('/cac/apex/ObjetivoGeral', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - SalesWay - Executive'));
        System.assertEquals(responseAssert, SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - SalesWay - Manager'));
        System.assertEquals('/cac/apex/DashboardTask', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Vendedor - Agenda'));
        System.assertEquals('/cac/apex/DashboardTask', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Gerente - Agenda'));
        System.assertEquals('/cac/apex/DashboardTask', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Gerente Grupo - Agenda'));
        System.assertEquals('/cac/apex/DashboardTask?profile=BDC', SFA_RenaultMaisCliente_HeaderController.setHomePage('SFA - Gerente BDC - Agenda'));
        System.assertEquals('', SFA_RenaultMaisCliente_HeaderController.setHomePage(''));
        
        Test.stopTest();
    }
    
    static User criarUser() {
        acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '79856523',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        Contact cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_SFA__c = 'Available'
        );
        
        Database.insert( cont );
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='79856523',
            isCac__c = true
            
        );
        
        return manager;
    }
}