/*  Create a myRenault account, associated vehicles and relations
    Call asynchronously "Personal Data" external web services to get customer informations
    Search existing account into SFDC using by matching keys
    Generate asynchronously a notification "DEALER_ACCOUNT_CREATOR"
  Error codes :     OK              : WS01MS000 
  OK WITH WARNING : WS01MS001 -> WS01MS500
  CRITICAL ERROR  : WS01MS501 -> WS01MS999
  *************************************************************************************************************
  25 Aug 2014 : Creation 
  01 Apr 2015 : Add VIN management
  05 May 2015 : R15.07 8.0 PinkBull  : German optins, BCS Personal Data, Merge Russian Dealer subscription
  08 Jul 2015 : Correct personal data call (customeridentificationnbr ? mapper dans ident1)
  24 Aug 2015 : R15.09 8.1 PinkBullBaby : Poland - country parameter is loaded into PL_country__c. Adding rules regarding the personal data webservices call
  22 Sep 2015 : R15.11 9.0 ChocolateCamel : New IN parameters : billingState, billingCountry, secondFirstname; Matching with accountSfdcId disabled; added MDM matching keys for France and Turquie
  16 Nov 2015 : R16.01 9.1 ChocolateCamelBaby, D. Veron (AtoS) : User's login and account's personal email become independent
  18 Mar 2016 : R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
  23 May 2016 : All the fields in entry of manageAccount should be trimmed (CGS)  
  *************************************************************************************************************/
global without sharing class Myr_ManageAccount_WS {  

	private static Long Begin_Time=DateTime.now().getTime();
	private static String logRunId = Myr_ManageAccount_WS.class.getName() + Begin_Time;

	/** Enum used to simulate some case impossible to reproduce today on certain orgs
	  The test is run only if the lastname passed in parameter is equal to the TEST_FAILURE AND Test.isRunningTest OK **/
	global enum TEST_FAILURE_SIMULATION { FAILURE_CIN_VALIDRULE, FAILURE_INITIALCHECK,
		FAxLUREVxNxSRT, FAxLUREVREUPD, FAxLUREVRExSRT, FAxLUREVxNGENER }

	/** Inner class exception **/
	private class ManageAccountException extends Exception { }

	WebService static Myr_ManageAccount_WS_Response manageAccount(String accountBrand, String country, String accountSource, String accountSubSource, String accountSubSubSource, String city, String addressStreet, String zipCode, String billingState, String billingCountry, String emailAddress, String landLine1, String mobilePhone1, String firstName, String secondFirstname, String lastName, String secondLastName, String salutationDigital, String title, String customerIdentificationNbr, String vin, String generalOptin, String channelEmail, String channelSMS, String channelPhone, String channelPost, String accountSfdcId, String goldenId, String localDatabaseID, String subscriberDealerID, String subcriberDealerIPN) {

		//Initialization of inner class object
		Myr_ManageAccount.Inputs_And_Results i = new Myr_ManageAccount.Inputs_And_Results();
		i.accountBrand = removeSpace(accountBrand);
		i.country = removeSpace(country);
		i.accountSource = removeSpace(accountSource);
		i.accountSubSource = removeSpace(accountSubSource);
		i.accountSubSubSource = removeSpace(accountSubSubSource);
		i.city = removeSpace(city);
		i.addressStreet = removeSpace(addressStreet);
		i.zipCode = removeSpace(zipCode);
		i.billingState = removeSpace(billingState);
		i.billingCountry = removeSpace(billingCountry);
		if (emailAddress!=null) {i.emailAddress=removeSpace(emailAddress).toLowerCase();}
		i.landLine1 = removeSpace(landLine1);
		i.mobilePhone1 = removeSpace(mobilePhone1);
		i.firstName = removeSpace(firstName);
		i.secondFirstname = removeSpace(secondFirstname);
		i.lastName = removeSpace(lastName);
		i.secondLastName = removeSpace(secondLastName);
		i.salutationDigital = removeSpace(salutationDigital);
		i.title = removeSpace(title);
		i.customerIdentificationNbr = removeSpace(customerIdentificationNbr);
		i.vin = removeSpace(vin);
		i.generalOptin = removeSpace(generalOptin);
		i.channelEmail = removeSpace(channelEmail);
		i.channelSMS = removeSpace(channelSMS);
		i.channelPhone = removeSpace(channelPhone);
		i.channelPost = removeSpace(channelPost);
		i.accountSfdcId = removeSpace(accountSfdcId);
		i.goldenId = removeSpace(goldenId);
		i.localDatabaseID = removeSpace(localDatabaseID);
		i.subscriberDealerID = removeSpace(subscriberDealerID);
		i.subscriberDealerIPN = removeSpace(subcriberDealerIPN);
		i.a = new Account();
		i.WebServiceCalled='Myr_ManageAccount_WS';

		Myr_ManageAccount manageAccount = new Myr_ManageAccount(i);
		Myr_ManageAccount_WS_Response response = upgradeResponse( manageAccount.createAccount() );
		return logAndExit(response, i);

	}

	// All the fields in entry of manageAccount should be trimmed
	private static string removeSpace(String str) {
		if (str != null && !String.isBlank(str)){
			str = str.trim();
		}
		return str; 
	}

	private static Myr_ManageAccount_WS_Response upgradeResponse(Myr_ManageAccount.Myr_ManageAccount_Response r) {
		Myr_ManageAccount_WS_Response_Msg info = new Myr_ManageAccount_WS_Response_Msg();
		Myr_ManageAccount_WS_Response rep = new Myr_ManageAccount_WS_Response();
		if (r != null && r.info != null) {
			rep.accountSfdcId = r.accountSfdcId;
			info.code = r.info.code;
			info.message = r.info.message;
			rep.info = info;
		}
		return rep;
	}

	global class Myr_ManageAccount_WS_Response {
		WebService String accountSfdcId;
		WebService Myr_ManageAccount_WS_Response_Msg info;
	}

	global class Myr_ManageAccount_WS_Response_Msg {
		WebService String code;
		WebService String message;
	}

	private static Myr_ManageAccount_WS_Response logAndExit(Myr_ManageAccount_WS_Response response, Myr_ManageAccount.Inputs_And_Results i) {
		INDUS_Logger_CLS.addGroupedLogFull(logRunId, i.idParentLog, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_ManageAccount_WS', 'Myr_ManageAccount_WS Output', response.info.code, response.info.message, i.ErrorLevel, i.a.Id, null, null, Begin_Time, DateTime.now().getTime(), 'Inputs : <' + i.inputs + '>'); 
		return response;   
	}
}