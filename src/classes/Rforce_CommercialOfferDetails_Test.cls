@isTest
public class Rforce_CommercialOfferDetails_Test {

/**
 * @author Venkatesh Kumar
 * @date 13/12/2015
 * @description This method is test the String buffer class.
 */

static TestMethod void testCommercialOfferDetails() {
    system.debug('StringBuffer_Test : testStringBuffer ======  ' );
    Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
    insert ctr;
    User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'France', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
    System.runAs(usr) {
    Test.startTest();
    Rforce_CommercialOfferDetails_CLS commOfferPojo = new Rforce_CommercialOfferDetails_CLS();
Rforce_CommercialOfferDetails_CLS commOfferPojova = new Rforce_CommercialOfferDetails_CLS('firsatname','Lastname','title','salutatu','id','accountid','strAcctCustomerIdentificationNumber','strAcctPhonenumber','strVenhVIN','strVenhVehicleModel','strVenhYearModel','strComofferMarketingDesc','strCommofferName','strLegalConditions','strEligibility','strOffercode',null,'name');
    Test.stopTest();
    }
    }
    }