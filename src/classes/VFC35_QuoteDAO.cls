/**
*   Class   -   VFC35_QuoteDAO
*   Author  -   SureshBabu
*   Date    -   13/01/2013
*    
*   #01 <Suresh Babu> <13/01/2013>
*      Created this Class to handle records from Quote related Queries.
*/
public with sharing class VFC35_QuoteDAO extends VFC01_SObjectDAO {
    private static final VFC35_QuoteDAO instance = new VFC35_QuoteDAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC35_QuoteDAO(){}

    /**
    * Method responsible for providing the instance of this class.. 
    */  
    public static VFC35_QuoteDAO getInstance(){
        return instance;
    }

    /** 
    * This Method was used to get Quote Records based on opportunityIds 
    * @param setOpportunityIDs  - This field using get Quote details by opportunity Id
    * @return lstQuoteRecords   - fetch and return the result in lstQuoteRecords.
    */
    public List<Quote> fetchQuoteRecords_UsingOpportunityIDs( Set<Id> setOpportunityIDs ){
        List<Quote> lstQuoteRecords = null;
        
        system.debug('*** setOpportunityIDs='+setOpportunityIDs);
        
        lstQuoteRecords = [SELECT 
                           Id, Name, OpportunityId, QuoteNumber, status, DMSNumber__c, CreatedDate, ExpirationDate,
                           TotalPrice, PaymentMethods__c, Entry__c, YearUsedVehicle__c, PriceUsedVehicle__c,
                           UsedVehicleLicensePlate__c, FinancingAgent__c, AmountFinanced__c, LastModifiedDate,
                           Accessory1__c, Accessory1Price__c, Accessory2__c, Accessory2Price__c, Accessory3__c, Accessory3Price__c
                           FROM 
                           Quote
                           WHERE
                           OpportunityId IN : setOpportunityIDs
                          ];
        
        system.debug('*** lstQuoteRecords.size()='+lstQuoteRecords.size());
        system.debug('*** lstQuoteRecords='+lstQuoteRecords);
        
        return lstQuoteRecords;
    }
    
    /** 
    * This Method was used to get Quote Records based on QuoteNumber and Dealer Id. 
    * @param setQuoteNumber     -   This field using get Quote details by QuoteNumber.
    * @param dealerId           -   This field using get Quote details using Dealer Id.
    * @return lstQuoteRecords   -   fetch and return the result in lstQuoteRecords.
    */
    public List<Quote> fetchQuoteRecords_UsingQuoteNumber_DealerId( Set<String> setQuoteNumber, Id dealerId ){
        List<Quote> lstQuoteRecords = null;
        lstQuoteRecords =   [SELECT Id, Opportunity.Id, Opportunity.Dealer__c, 
                             Opportunity.AccountId, Opportunity.OwnerId,
                             QuoteNumber, Status, InvoiceNumber__c,
                             DMSNumber__c, CreatedDate, ExpirationDate,
                             TotalPrice, PaymentMethods__c, Entry__c,
                             YearUsedVehicle__c, PriceUsedVehicle__c,
                             UsedVehicleLicensePlate__c, FinancingAgent__c,
                             FinancingType__c, AmountFinanced__c,
                             NumberOfInstallments__c, ValueOfInstallments__c,
                             UsedVehicle__c, LastModifiedDate, BillingAccount__c,
                             Accessory1__c, Accessory1Price__c, Accessory2__c, Accessory2Price__c, Accessory3__c, Accessory3Price__c
                             FROM 
                             Quote
                             WHERE 
                             QuoteNumber IN : setQuoteNumber 
                             AND Opportunity.Dealer__c =: dealerId]; 
        return lstQuoteRecords;
    }
    
    /** 
    * This Method was used to get Quote Records based on Dealer Id.
    * @param dealerId           -   This field using get Quote details using Dealer Id.
    * @return lstQuoteRecords   -   fetch and return the result in lstQuoteRecords.
    */
    public List<Quote> fetchQuoteRecords_UsingDealerId( Id dealerId ){
        List<Quote> lstQuoteRecords = null;
        lstQuoteRecords = [SELECT Id, Opportunity.Id, Opportunity.Dealer__c, 
                           Opportunity.AccountId, Opportunity.OwnerId,
                           QuoteNumber, Status, InvoiceNumber__c,
                           DMSNumber__c, CreatedDate, ExpirationDate,
                           TotalPrice, PaymentMethods__c, Entry__c,
                           YearUsedVehicle__c, PriceUsedVehicle__c,
                           UsedVehicleLicensePlate__c, FinancingAgent__c,
                           FinancingType__c, AmountFinanced__c,
                           NumberOfInstallments__c, ValueOfInstallments__c,
                           UsedVehicle__c, LastModifiedDate, PaymentDetails__c,BillingAccount__c,
                           Accessory1__c, Accessory1Price__c, Accessory2__c, Accessory2Price__c, Accessory3__c, Accessory3Price__c
                           FROM Quote
                           WHERE Opportunity.Dealer__c = : dealerId
                           AND Status = 'Pre Order Requested'];
        return lstQuoteRecords;
    }
    
    /** 
    * Método responsável por buscar o Quote e fazer um subselect na lista relaciona QuoteLineItems
    * @param: Id Oportunidade   
    * @return: lista Quote
    * @autor: Elvia Serpa
    */
    public List<Quote> findByOpportunityId(String opportunityId)
    {
        return [ Select 
                Id , 
                Name ,
                QuoteNumber , 
                TotalPrice , 
                Status ,  
                PC_OrderNumberSedre__c,
                IsSyncing,
                Accessory1__c, Accessory1Price__c, Accessory2__c, Accessory2Price__c, Accessory3__c, Accessory3Price__c,
                (Select Vehicle__r.Model__c, Vehicle__r.Name, PricebookEntry.Name From QuoteLineItems Where PricebookEntry.Product2.RecordType.DeveloperName IN ('PDT_ModelVersion','PDT_Model'))
                From 
                Quote 
                where
                OpportunityId = :opportunityId 
                order by QuoteNumber DESC];
    }
    
    /** 
    *
    */
    public Quote findById(String quoteId)
    {
        return [Select
                CreatedDate , 
                TotalPrice , 
                Name ,
                Opportunity.Vehicle_Of_Interest__c,
                Opportunity.Dealer__c ,
                Opportunity.StageName,
                Opportunity.Account.Name,
                Opportunity.Account.FirstName,
                Opportunity.Account.LastName,
                Opportunity.Account.Phone,
                Opportunity.Account.VehicleInterest_BR__c ,
                Opportunity.Account.Shipping_Street__c,
                Opportunity.Account.Shipping_Number__c,
                Opportunity.Account.Shipping_Complement__c,
                Opportunity.Account.Shipping_Neighborhood__c,
                Opportunity.Account.Shipping_City__c,
                Opportunity.Account.Shipping_PostalCode__c,
                Opportunity.Account.Shipping_State__c,
                Opportunity.Account.Shipping_Country__c,
                Opportunity.Account.PersEmailAddress__c,
                Opportunity.Account.MaritalStatus__c,
                Opportunity.Account.Sex__c,
                Opportunity.Account.PersonBirthdate,
                Opportunity.Account.PersLandline__c,
                Opportunity.Account.ProfLandline__c,
                Opportunity.Account.PersMobPhone__c,
                Opportunity.Account.CustomerIdentificationNbr__c,
                Opportunity.Account.Fax,
                Opportunity.Account.PersonEmail,
                Opportunity.Account.RGStateRegistration__c,
                Opportunity.Account.RGStateTexto__c,
                Opportunity.Account.IsPersonAccount,
                BillingAccount__r.Name,
                BillingAccount__r.RecordTypeId,
                BillingAccount__r.FirstName,
                BillingAccount__r.LastName,
                BillingAccount__r.Phone,
                BillingAccount__r.VehicleInterest_BR__c ,
                BillingAccount__r.Shipping_Street__c,
                BillingAccount__r.Shipping_Number__c,
                BillingAccount__r.Shipping_Complement__c,
                BillingAccount__r.Shipping_Neighborhood__c,
                BillingAccount__r.Shipping_City__c,
                BillingAccount__r.Shipping_PostalCode__c,
                BillingAccount__r.Shipping_State__c,
                BillingAccount__r.Shipping_Country__c,
                BillingAccount__r.PersEmailAddress__c,
                BillingAccount__r.MaritalStatus__c,
                BillingAccount__r.Sex__c,
                BillingAccount__r.PersonBirthdate,
                BillingAccount__r.PersLandline__c,
                BillingAccount__r.ProfLandline__c,
                BillingAccount__r.PersMobPhone__c,
                BillingAccount__r.CustomerIdentificationNbr__c,
                BillingAccount__r.Fax,
                BillingAccount__r.RgStateTexto__c,
                BillingAccount__r.CompanyID__c,
                BillingAccount__r.ProfEmailAddress__c,
                Opportunity.AccountId ,
                Opportunity.Owner.Name ,
                Opportunity.Owner.Email ,
                Opportunity.Owner.Phone ,
                ExpirationDate ,
                Entry__c ,
                AmountFinanced__c ,
                UsedVehicle__c ,
                UsedVehicle__r.Brand__c ,
                UsedVehicle__r.Model__c ,
                YearUsedVehicle__c ,
                PriceUsedVehicle__c ,
                PC_OrderNumberSedre__c,
                CDCFinancing__c ,
                LSGFinancing__c ,
                NumberOfInstallments__c ,
                ValueOfInstallments__c ,
                PaymentMethods__c ,
                PaymentDetails__c ,
                UsedVehicleLicensePlate__c ,
                IdentityCard__c ,
                CPF_BR__c ,
                ProofIncome__c ,
                ProofResidence__c ,
                LastStatementIncomeTax__c ,
                LastPaycheckStatement__c ,
                LastStatementBank__c ,
                DriversLicense__c ,
                SocialContract__c ,
                LastStatementIncomeTaxEnterprise__c ,
                IdentityCardAllMembers__c ,
                CPFAllMembers__c ,
                RegistrationFormSignedPartners__c ,
                LastBalanceSheet__c ,
                BalanceLastTwoYears__c ,
                RelationshipFleetDebt__c ,
                QuoteNumber ,
                OpportunityId ,
                Status ,
                DUT__c,
                Procuration__c,
                Inspection__c,
                FinesPaid__c,
                PublicProcuration__c,
                Licensing__c,
                LeaseTermination__c,
                ManualAndSpareKey__c,
		DeliveryDate__c,
                SignalValue__c,
                SignalValueCk__c,
                PC_DmsOrderNumber__c,
                Accessory1__c, Accessory1Price__c, Accessory2__c, Accessory2Price__c, Accessory3__c, Accessory3Price__c,
                (Select Vehicle__r.Model__c, Vehicle__r.Name, PricebookEntry.Name From QuoteLineItems Where PricebookEntry.Product2.RecordType.DeveloperName <> 'PDT_Accessory')
                From 
                Quote
                Where
                Id = :quoteId ];                     
    }
    
    /** 
    *
    */
    public Quote fetchById(String quoteId)
    {
        return [  Select
                Id,
                Status,
                PC_LogicCode__c,
                QuoteNumber,
                Opportunity.AccountId ,
                Opportunity.Owner.Name ,                        
                Opportunity.Dealer__c,
                Opportunity.Dealer__r.ParentId,
                Opportunity.Account.VehicleInterest_BR__c
                From 
                Quote
                Where
                Id = :quoteId ];                                
    }
    
    public List<Quote> findByQuoteNumberSet(Set<String> quoteNumberLst){
        
        return [
            SELECT Id,
            Name,
            OpportunityId,
            PC_LogicCode__c,
            PC_CustomerNumberInternalSedre__c, 
            PC_ChassisNumberCurrent__c, 
            PC_SemiclairModel__c, 
            PC_SemiclairVersion__c, 
            PC_SemiclairOptional__c, 
            PC_ColorBoxCriterion__c, 
            PC_SemiclairUpholstered__c, 
            PC_SemiclairHarmony__c, 
            PC_SemiclairColor__c, 
            PC_FirstName__c, 
            PC_PersonalOrCompanyClientName__c, 
            PC_CustomerCompany__c, 
            PC_WayNome__c, 
            PC_City__c, 
            External_Id__c,
            PC_State__c, 
            PC_PersonalPhone__c, 
            PC_MobilePhone__c, 
            PC_ErrorMessage__c, 
            PC_DmsOrderNumber__c 
            FROM Quote
            WHERE QuoteNumber IN :quoteNumberLst
        ];
        
    }
    
    public List<Quote> findBySedreNumberAndOwner(Set<String> ownerControl, Set<String> orderSedre){
        
        return [
            SELECT Id,
            PC_LogicCode__c,
            Name,
            OpportunityId,
            PC_CustomerNumberInternalSedre__c, 
            PC_ChassisNumberCurrent__c, 
            PC_SemiclairModel__c, 
            PC_SemiclairVersion__c, 
            PC_SemiclairOptional__c, 
            PC_ColorBoxCriterion__c, 
            PC_SemiclairUpholstered__c, 
            PC_SemiclairHarmony__c, 
            PC_SemiclairColor__c, 
            PC_FirstName__c, 
            PC_PersonalOrCompanyClientName__c, 
            PC_CustomerCompany__c, 
            PC_OrderNumberSedre__c,
            PC_OwnerAccountControl__c,
            PC_WayNome__c, 
            PC_City__c, 
            PC_State__c, 
            PC_PersonalPhone__c, 
            PC_MobilePhone__c, 
            PC_ErrorMessage__c, 
            PC_DmsOrderNumber__c 
            FROM Quote
            WHERE PC_OwnerAccountControl__c IN :ownerControl
            AND PC_OrderNumberSedre__c IN :orderSedre
        ];
        
    }
        
    
    /** 
    *
    */
    public Quote insertData(Quote sObjQuote)
    {
        Quote sObjQuoteAux = null;
        
        super.insertData(sObjQuote);
        
        sObjQuoteAux = this.findById(sObjQuote.Id);
        
        return sObjQuoteAux;
    }
}