/**
* Classe responsável por fazer o papel de business delegate da página de test drive.
* @author Felipe Jesus Silva.
*/
public class VFC56_TestDriveBusinessDelegate 
{
	private static final VFC56_TestDriveBusinessDelegate instance = new VFC56_TestDriveBusinessDelegate();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC56_TestDriveBusinessDelegate() 
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC56_TestDriveBusinessDelegate getInstance()
	{
		return instance;
	}
	
	public VFC54_TestDriveVO getTestDriveByOpportunity(String opportunityId)
	{

		System.debug('****VFC56_TestDriveBusinessDelegate.getTestDriveByOpportunity');

		TDV_TestDrive__c sObjTestDrive = null;
		List<Product2> lstSObjModel = null;
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicleAux = null;		
		VFC54_TestDriveVO testDriveVO = null; 
		VFC54_TestDriveVO testDriveVOAux = null;
		List<VFC108_DocumentationRVAVO> lstDocumentationRVA = null;
		
		/*cria ou obtém um test drive existente para essa oportunidade*/
		sObjTestDrive = VFC30_TestDriveBO.getInstance().getTestDriveConfirmedOrOpenFromOpportunity(opportunityId);
		
		/*obtém a lista de modelos*/
		lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
		
		/*mapeia os dados do objeto para o VO*/
		testDriveVO = this.mapToValueObject(sObjTestDrive, lstSObjModel);
		
		/*armazena a lista de modelos no VO*/
		testDriveVO.lstSObjModel = lstSObjModel;
					 		
		/*obtém a lista de documentos*/
		lstDocumentationRVA = this.getDocumentsRVA(testDriveVO);
		
		/*armazena a lista de documentos no VO*/
		testDriveVO.lstDocumentationRVA = lstDocumentationRVA;
		
		/*obtém a lista de veículos que estão disponíveis para test drive (parâmetros: id da concessionária e veículo de interesse)*/			                                                                    
		lstSObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getVehiclesAvailable(testDriveVO.dealerId, testDriveVO.vehicleInterest);
																							  
		if(!lstSObjTestDriveVehicle.isEmpty())
		{
			testDriveVO.lstSObjTestDriveVehicleAvailable = lstSObjTestDriveVehicle;
		
			testDriveVO.dateBookingNavigation = Date.today();
																							  																						  		
			/*verifica se o status do test drive é aberto*/
			if(testDriveVO.status.equals('Open'))
			{
				sObjTestDriveVehicleAux = lstSObjTestDriveVehicle.get(0);
			
				testDriveVO.testDriveVehicleId = sObjTestDriveVehicleAux.Id;
				testDriveVO.model = sObjTestDriveVehicleAux.Vehicle__r.Model__c;
				testDriveVO.version = sObjTestDriveVehicleAux.Vehicle__r.Version__c;
				testDriveVO.color = sObjTestDriveVehicleAux.Vehicle__r.Color__c;
				testDriveVO.plaque = sObjTestDriveVehicleAux.Vehicle__r.VehicleRegistrNbr__c;
			}
		
			/*obtém a agenda do veículo*/
			testDriveVOAux = this.getAgendaVehicle(testDriveVO);																				  																							  
			 
			testDriveVO.lstSObjTestDriveAgenda = testDriveVOAux.lstSObjTestDriveAgenda;
		}
							 	
		return testDriveVO;
	}
	
	public VFC54_TestDriveVO getVehiclesAvailable(VFC54_TestDriveVO testDriveVO)
	{
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicleAux = null;
		List<VFC108_DocumentationRVAVO> lstDocumentationRVA = null;
		VFC54_TestDriveVO testDriveVOAux = new VFC54_TestDriveVO();
		
		/*obtém a lista de veículos que estão disponíveis para test drive (parâmetros: veículo de interesse e id da concessionária)*/		
		lstSObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getVehiclesAvailable(testDriveVO.dealerId, testDriveVO.vehicleInterest);
		
		/*verifica se a busca retornou registros*/
		if(!lstSObjTestDriveVehicle.isEmpty())
		{
			/*obtém o primeiro veículo da lista*/
			sObjTestDriveVehicleAux = lstSObjTestDriveVehicle.get(0);
			
			testDriveVOAux.testDriveVehicleId = sObjTestDriveVehicleAux.Id;
			testDriveVOAux.dateBookingNavigation = Date.today();
			
			/*obtém a agenda desse veículo*/
			testDriveVOAux = this.getAgendaVehicle(testDriveVOAux);
			
			testDriveVOAux.model = sObjTestDriveVehicleAux.Vehicle__r.Model__c;
			testDriveVOAux.version = sObjTestDriveVehicleAux.Vehicle__r.Version__c;
			testDriveVOAux.color = sObjTestDriveVehicleAux.Vehicle__r.Color__c;		
			testDriveVOAux.plaque = sObjTestDriveVehicleAux.Vehicle__r.VehicleRegistrNbr__c;
			testDriveVOAux.dateBookingNavigation = Date.today();
			
			testDriveVOAux.lstSObjTestDriveVehicleAvailable = lstSObjTestDriveVehicle;
		}
		
		/*obtém a lista de documentos*/
		lstDocumentationRVA = this.getDocumentsRVA(testDriveVO);
		
		testDriveVOAux.lstDocumentationRVA = lstDocumentationRVA;
			                                                                    	
		return testDriveVOAux;
	}
	
	private List<VFC108_DocumentationRVAVO> getDocumentsRVA(VFC54_TestDriveVO testDriveVO)
	{

		System.debug('****VFC56_TestDriveBusinessDelegate.getDocumentsRVA');

		List<Attachment> lstSObjAttachment = null;
		VFC108_DocumentationRVAVO docRVAVO = null;
		List<VFC108_DocumentationRVAVO> lstDocumentationRVA = new List<VFC108_DocumentationRVAVO>();
		
		/*obtém a lista de anexos desse veículo de interesse*/
		lstSObjAttachment = VFC107_AttachmentDAO.getInstance().findByProductName(testDriveVO.vehicleInterest);
		
		for(Attachment sObjAttachment : lstSObjAttachment)
		{
			docRVAVO = new VFC108_DocumentationRVAVO();			
			docRVAVO.id = sObjAttachment.Id;
			docRVAVO.name = sObjAttachment.Name;
			
			lstDocumentationRVA.add(docRVAVO);
		}
		
		return lstDocumentationRVA;
	}
	
	public VFC54_TestDriveVO getVehicleDetails(VFC54_TestDriveVO testDriveVO)
	{
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC54_TestDriveVO testDriveVOResponse = null;
		
		/*obtém o veículo pelo id*/	
		sObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getById(testDriveVO.testDriveVehicleId); 
			
		/*obtém o VO contendo a lista de tests drive agendados/confirmados para esse veículo*/
		testDriveVOResponse = this.getAgendaVehicle(testDriveVO);
			
		/*seta o restante dos dados no objeto de resposta*/
		testDriveVOResponse.model = sObjTestDriveVehicle.Vehicle__r.Model__c;
		testDriveVOResponse.version = sObjTestDriveVehicle.Vehicle__r.Version__c;
		testDriveVOResponse.color = sObjTestDriveVehicle.Vehicle__r.Color__c;		
		testDriveVOResponse.plaque = sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c;

		return testDriveVOResponse;
	}
	
	public VFC54_TestDriveVO getAgendaVehicle(VFC54_TestDriveVO testDriveVO)
	{
		System.debug('*****VFC56_TestDriveBusinessDelegate.getAgendaVehicle');

		List<TDV_TestDrive__c> lstSObjTestDrive = null;
		VFC54_TestDriveVO testDriveVOResponse = new VFC54_TestDriveVO();
		
		lstSObjTestDrive = VFC30_TestDriveBO.getInstance().getTestsDriveScheduledOrConfirmed(testDriveVO.testDriveVehicleId, testDriveVO.dateBookingNavigation);
		
		testDriveVOResponse.lstSObjTestDriveAgenda = lstSObjTestDrive; 
		
		return testDriveVOResponse; 
	}
	
	public void updateTestDrive(VFC54_TestDriveVO testDriveVO)
	{
		TDV_TestDrive__c sObjTestDrive = this.mapToTestDriveObject(testDriveVO);
		
		VFC30_TestDriveBO.getInstance().updateTestDrive(sObjTestDrive);
	}
	
	public VFC54_TestDriveVO updateTestDriveAndCreateNew(VFC54_TestDriveVO testDriveVO)
	{
		VFC54_TestDriveVO testDriveVONew = null;
		
		/*atualiza o test drive recebido*/
		this.updateTestDrive(testDriveVO);
		
		testDriveVONew = this.createNewTestDrive(testDriveVO.opportunityId);
		
		return testDriveVONew;		
	}
	
	public VFC54_TestDriveVO createNewTestDrive(String opportunityId)
	{		
		VFC54_TestDriveVO testDriveVONew = null;		
		VFC54_TestDriveVO testDriveVOAux = null;
		List<Product2> lstSObjModel = null;
		List<VFC108_DocumentationRVAVO> lstDocumentationRVA = null;
		
		/*cria um novo test drive*/	
		TDV_TestDrive__c sObjTestDrive = VFC30_TestDriveBO.getInstance().createNewTestDrive(opportunityId);
		
		/*obtém a lista de modelos*/
		lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
		
		/*mapeia os dados do objeto para o VO*/
		testDriveVONew = this.mapToValueObject(sObjTestDrive, lstSObjModel);
		
		/*obtém o VO contendo os veículos disponíveis e a agenda do primeiro veículo disponível*/
		testDriveVOAux = this.getVehiclesAvailable(testDriveVONew);
		
		/*obtém a lista de documentos*/
		lstDocumentationRVA = this.getDocumentsRVA(testDriveVONew);
	
		/*seta no novo VO a lista de veículos disponíveis*/
		testDriveVONew.lstSObjTestDriveVehicleAvailable = testDriveVOAux.lstSObjTestDriveVehicleAvailable;
		
		/*seta os detalhes do primeiro veículo disponível*/
		testDriveVONew.model = testDriveVOAux.model;
		testDriveVONew.version = testDriveVOAux.version;
		testDriveVONew.color = testDriveVOAux.color;		
		testDriveVONew.plaque = testDriveVOAux.plaque;
		testDriveVONew.dateBookingNavigation = testDriveVOAux.dateBookingNavigation;
		
		/*seta a lista contendo a agenda*/
		testDriveVONew.lstSObjTestDriveAgenda = testDriveVOAux.lstSObjTestDriveAgenda;
		
		/*armazena a lista de modelos no VO*/
		testDriveVONew.lstSObjModel = lstSObjModel;
		
		/*armazena a lista de documentos no novo VO*/
		testDriveVONew.lstDocumentationRVA = lstDocumentationRVA;
				
		return testDriveVONew;
	}
	
	public void cancelOpportunity(VFC54_TestDriveVO testDriveVO)
	{
		List<Task> listTask = VFC33_TaskDAO.getInstance().findOpenRecurrencebyOpportunityId(testDriveVO.opportunityId); 
                	
        if(listTask != null && listTask.size() > 0){
        	throw new VFC34_ApplicationException('Você deve encerrar as tarefas recursivas desta oportunidade antes de cancelar.');
        } 
		
		Opportunity sObjOpportunity = new Opportunity(Id = testDriveVO.opportunityId);

	    // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();

		sObjOpportunity.StageName = 'Lost';
		sObjOpportunity.ReasonLossCancellation__c = testDriveVO.reasonLossCancellationOpportunity;
		
		VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity);	
	}
	
	public void updateOpportunityForQuoteStage(String opportunityId)
	{
		Opportunity sObjOpportunity = new Opportunity(Id = opportunityId);

	    // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();

		sObjOpportunity.StageName = 'Quote';
		
		VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity);
	}
	
	public void cancelTestDriveAndUpdateOpportunityForQuoteStage(VFC54_TestDriveVO testDriveVO)
	{
		Opportunity sObjOpportunity = null;
		TDV_TestDrive__c sObjTestDrive = null;
		
		sObjOpportunity = new Opportunity(Id = testDriveVO.opportunityId);

	    // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        sObjOpportunity.DateTimeSellerUpdatedStageOpp__c = System.now();
        
		sObjOpportunity.StageName = 'Quote';
		
		sObjTestDrive = new TDV_TestDrive__c(Id = testDriveVO.id);
		sObjTestDrive.Status__c = 'Not Performed';
		sObjTestDrive.ReasonCancellation__c = testDriveVO.reasonCancellation;
		
		VFC47_OpportunityBO.getInstance().updateOpportunity(sObjOpportunity, sObjTestDrive);	
	}
	
	private VFC54_TestDriveVO mapToValueObject(TDV_TestDrive__c sObjTestDrive, List<Product2> lstSObjModel)
	{

		System.debug('****VFC56_TestDriveBusinessDelegate.mapToValueObject');

		String vehicleInterest = null;
		VFC54_TestDriveVO testDriveVO = new VFC54_TestDriveVO();
		
		/*popula o VO*/
		testDriveVO.id = sObjTestDrive.Id;
		testDriveVO.opportunityId = sObjTestDrive.Opportunity__c;
		testDriveVO.dealerId = sObjTestDrive.Opportunity__r.Dealer__c;
		testDriveVO.testDriveVehicleId = sObjTestDrive.TestDriveVehicle__c;			
		testDriveVO.model = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c;
		testDriveVO.version = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Version__c;
		testDriveVO.color = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Color__c;
		testDriveVO.plaque = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.VehicleRegistrNbr__c;
		testDriveVO.dateBooking = sObjTestDrive.DateBooking__c;
		testDriveVO.name = sObjTestDrive.Name;
		testDriveVO.sellerName = UserInfo.getName();
		testDriveVO.opportunityName = sObjTestDrive.Opportunity__r.Name; 
		testDriveVO.status = sObjTestDrive.Status__c;	
		testDriveVO.customerName = sObjTestDrive.Opportunity__r.Account.Name; 
		testDriveVO.persLandline = sObjTestDrive.Opportunity__r.Account.PersLandline__c;
		testDriveVO.persMobPhone = sObjTestDrive.Opportunity__r.Account.PersMobPhone__c;
		testDriveVO.motor = sObjTestDrive.Car_Engine__c;
        testDriveVO.cambio = sObjTestDrive.Car_Exchange__c;
        
		if(sObjTestDrive.Opportunity__r.Account.CurrentVehicle_BR__c != null)
		{
			testDriveVO.currentVehicle = sObjTestDrive.Opportunity__r.Account.CurrentVehicle_BR__r.Model__c;
		}
				
		if(sObjTestDrive.Opportunity__r.Account.YrReturnVehicle_BR__c != null)
		{
			testDriveVO.yearCurrentVehicle = sObjTestDrive.Opportunity__r.Account.YrReturnVehicle_BR__c.intValue();
		}
		
		if(String.isNotEmpty(sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c))
		{
			vehicleInterest = sObjTestDrive.TestDriveVehicle__r.Vehicle__r.Model__c.toUpperCase();
		}
		else if(String.isEmpty(sObjTestDrive.Opportunity__r.Account.VehicleInterest_BR__c))
		{
			/*obtém o nome do primeiro modelo da lista*/
			vehicleInterest = lstSObjModel.get(0).Name;
		}
		else
		{
			/*a princípio obtém o nome do primeiro modelo da lista*/
			vehicleInterest = lstSObjModel.get(0).Name;
			
			/*é necessário verificar se o veículo de interesse da conta contém na lista de modelos*/			
			for(Product2 sObjModel: lstSObjModel)
			{
				/*se o veículo de interesse da conta for igual a algum modelo da lista, obtém o veículo da conta*/
				if(sObjTestDrive.Opportunity__r.Account.VehicleInterest_BR__c == sObjModel.Name)
				{
					vehicleInterest = sObjTestDrive.Opportunity__r.Account.VehicleInterest_BR__c.toUpperCase();
					
					break;
				}
			}
		}
	
		testDriveVO.vehicleInterest = vehicleInterest;
		
		return testDriveVO;
	}
	
	private TDV_TestDrive__c mapToTestDriveObject(VFC54_TestDriveVO testDriveVO)
	{
		TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c(Id = testDriveVO.Id);
		
		sObjTestDrive.TestDriveVehicle__c = testDriveVO.testDriveVehicleId;		
		sObjTestDrive.DepartureTime__c = testDriveVO.sObjTestDrive.DepartureTime__c;
		sObjTestDrive.ReturnTime__c = testDriveVO.sObjTestDrive.ReturnTime__c;
		sObjTestDrive.FuelOutputLevel__c = testDriveVO.fuelOutputLevel;
		sObjTestDrive.Status__c = testDriveVO.status;
		sObjTestDrive.OutputKm__c = String.isNotEmpty(testDriveVO.outputKM) ? Double.valueOf(testDriveVO.outputKM) : null;
		sObjTestDrive.ReturnKm__c = String.isNotEmpty(testDriveVO.returnKM) ? Double.valueOf(testDriveVO.returnKM) : null;
		sObjTestDrive.FuelReturnLevel__c = testDriveVO.fuelReturnLevel;
		sObjTestDrive.Design__c = testDriveVO.design;
		sObjTestDrive.InternalSpace__c = testDriveVO.internalSpace;
		sObjTestDrive.Handling__c = testDriveVO.handling;
		sObjTestDrive.Security__c = testDriveVO.security;
		sObjTestDrive.Performance__c = testDriveVO.performance;
		sObjTestDrive.PanelCommands__c = testDriveVO.panelCommands;
		sObjTestDrive.EquipmentLevel__c = testDriveVO.equipmentLevel;
		sObjTestDrive.BuyingInterestAfterTestDrive__c = testDriveVO.buyingInterestAfterTestDrive;
		sObjTestDrive.Comments__c = testDriveVO.comments;
		sObjTestDrive.ReasonCancellation__c = testDrivevO.reasonCancellation;
		sObjTestDrive.DateBooking__c = testDriveVO.dateBooking;
		sObjTestDrive.Car_Engine__c = testDriveVO.motor;
       	sObjTestDrive.Car_Exchange__c = testDriveVO.cambio;
		return sObjTestDrive;
	}
			
}