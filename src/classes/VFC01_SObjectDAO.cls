/**
 * Classe que define métodos de operações DML comuns a todos objetos SalesForce.
 * Essa classe não pode ser utilizada diretamente, apenas determina o comportamento de suas descendentes.
 * @author Felipe Silva
 */
public abstract class VFC01_SObjectDAO {
	/**
    * Insere um objeto na base de dados.
    * @param sObj Objeto a ser inserido. Este objeto é qualquer descendente de SObject, sendo possível passar um objeto padrão ou customizado.
    * @return Um objeto SaveResult indicando o resultado da inserção.
    */
    public Database.SaveResult insertData(SObject sObj) 
    {
        Database.SaveResult sr = null;
        
        try 
        {
            sr = Database.insert(sObj);
        } 
        catch(DMLException ex) 
        {
            throw ex;
        }
         
        return sr;
    }
  
    /**
     * Insere uma lista de objetos na base de dados.
     * @param sObjList A lista de objetos a ser inserida. Esta lista pode ser composta por qualquer
     * descendente de SObject, sendo possível passar uma lista de objetos padrão ou customizados.
     * @return Uma lista de objetos SaveResult indicando o resultado da inserção.
     */
    public List<Database.SaveResult> insertData(List<SObject> sObjList)
    {
        List<Database.SaveResult> srList = null;
        
        srList = Database.insert(sObjList);
    
    	return srList;
    }
    
    /**
    * Atualiza um objeto na base de dados.
    * @param sObj Objeto a ser atualizado. Este objeto é qualquer descendente de SObject, sendo possível passar um objeto padrão ou customizado.
    * @return Um objeto SaveResult indicando o resultado da atualização.
    */
    public Database.SaveResult updateData(SObject sObj)
    {
        Database.SaveResult sr = null;
        
        try
        {
            sr = Database.update(sObj);
        }
        catch(DMLException ex)
        {
            throw ex;
        }
        
        return sr;
    }
    
    /**
    * Atualiza uma lista de objetos na base de dados.
    * @param sObjList A lista de objetos a ser atualizada. Esta lista pode ser composta por qualquer descendente de SObject, 
    * sendo possível passar uma lista de objetos padrão ou customizados.
    * @return Uma lista de objetos SaveResult indicando o resultado da atualização.
    */
    public List<Database.SaveResult> updateData(List<SObject> sObjList)
    {
        List<Database.SaveResult> srList = null;
        
        srList = Database.update(sObjList);
        return srList;
    }
    
    /*
    *	Upser list of SObjects
    *	@param	sObjList	-	a list of sObject records.
	*	@return srList		-	Upsert Result.
    */
     public List<Database.Upsertresult> upsertData(List<SObject> sObjList)
    {
        List<Database.Upsertresult> srList = null;
        
        srList = Database.upsert(sObjList);
        return srList;
    }
    
    /**
    * Deleta um objeto da base de dados.
    * @param id Id do objeto a ser deletado. 
    * @return Um objeto DeleteResult indicando o resultado da deleção.
    */
    public Database.DeleteResult deleteData(String id)
    {
        Database.DeleteResult dr = null;
        
        dr = Database.delete(id);
        return dr;
    }
    
    /**
    * Deleta uma lista de objetos da base de dados.
    * @param sObjList A lista de Ids a ser deletada. 
    * @return Uma lista de objetos DeleteResult indicando o resultado da deleção.
    */
    public List<Database.DeleteResult> deleteData(List<String> idList)
    {
        List<Database.DeleteResult> drList = null;
        
        drList = Database.delete(idList);
        
        return drList;
    }
}