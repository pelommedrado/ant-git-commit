@isTest
public class PV_UtilsCallOfferTest {
	
    
    
    static testMethod void createDocument(){
        //criar veículo
        Model__c vehicle = createModel();
        Database.insert(vehicle,true);
        System.assert(vehicle.id != null);
        
        //criar ação comercial
        PVCommercial_Action__c commecialAction = createCommercialAction(vehicle.id);
        Database.insert(commecialAction,true);
        System.assert(commecialAction.id != null);
        
        //criar chamada de oferta
        PVCall_Offer__c callOffer = createCallOffer(commecialAction.id);
        Database.insert(callOffer,true);
 		callOffer.Status__c = 'Inactive';
        Database.update(callOffer);
        callOffer.Status__c = 'Pending Analyst';
        Database.update(callOffer);
        callOffer.Status__c = 'Pending';
        Database.update(callOffer);
        callOffer.Status__c = 'Active';
        Database.update(callOffer);
        System.assert(callOffer.id !=null);
        
        //criar condições financeiras
        PV_FinancingConditions__c condicao = createFinancingConditions(Double.valueOf(0.0235));
        Database.insert(condicao , true);
        System.assert(condicao.Id != null);
        
        //manutenção trigger
        callOffer.Coefficient__c = 0.0235;
        Database.update(callOffer);
        
        //
        PV_UtilsCallOffer pv1 = new PV_UtilsCallOffer();
   
        
        List<PVCall_Offer__c> lsCall = new List<PVCall_Offer__c>();
        lsCall.add(callOffer);
        Map<Id,PVCall_Offer__c> oldMapCallOffer = new Map<Id,PVCall_Offer__c>();
        oldMapCallOffer.put(callOffer.Id, callOffer);
        
        System.debug('#### lsCall '+lsCall);
        PV_UtilsCallOffer pv2 =  new PV_UtilsCallOffer(lsCall,oldMapCallOffer);
        
        
        boolean vazio =pv2.returnValueCaseValid('');
        boolean doubl = pv2.returnValueCaseValid(0.16);
        boolean str = pv2.returnValueCaseValid('0.16');
       // pv2.searchFinancingConditions();
    }
    
    private static PV_FinancingConditions__c createFinancingConditions(double coeficiente){
        PV_FinancingConditions__c condicao = new PV_FinancingConditions__c(
        	Coefficient__c = coeficiente,
            Flag__c = 'teste',
            Month__c = 30,
            Status__c = 'ACTIVE',
            Tax__c  = 0.99
        );
        return condicao;
    }
    
    private static PVCall_Offer__c createCallOffer(Id commercialAction){
        PVCall_Offer__c callOffer = new PVCall_Offer__c(
              Commercial_Action__c = commercialAction,
              Call_Offer_Start_Date__c = Date.newInstance(2015,09,10),
              Call_Offer_End_Date__c = Date.newInstance(2020, 09, 19),
              Minimum_Entry__c = 50.0,
              Period_in_Months__c = 30,
              Month_Rate__c = 0.99,
            	Status__c = 'Draft'
            );
        return callOffer;
        
    }
    
    private static PVCommercial_Action__c createCommercialAction(Id modelId){
        PVCommercial_Action__c comercialAction = new PVCommercial_Action__c(
        	End_Date__c = Date.newInstance(2020, 09, 20),
            Model__c = modelId,
            Start_Date__c = Date.newInstance(2015,09,09),
            Status__c = 'Active',
            Type_of_Action__c = 'teste'
        );
        return comercialAction;        
    } 
    
    private static Model__c createModel(){
        Model__c model = new Model__c(
             Market__c = 'abc',
             Status__c = 'Active',
             Model_PK__c = '1111111'
        );
        return model;
    }
    
    
    
    
}