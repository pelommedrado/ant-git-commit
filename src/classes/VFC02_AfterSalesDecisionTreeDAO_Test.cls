/**
	Class   -   VFC02_AfterSalesDecisionTreeDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC01_AfterSalesDecisionTreeDAO.
**/
@isTest
public with sharing class VFC02_AfterSalesDecisionTreeDAO_Test {
	static testMethod void VFC02_AfterSalesDecisionTreeDAO_Test() {
        // TO DO: implement unit test
        List<AST_AfterSalesDecisionTree__c> lstAfterSalesInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToAfterSalesDecisionTreeObjects();
        Double currentMilage = 2000;
        Boolean afterSalesOffer = false;
        String rfv = '';
        
        VFC02_AfterSalesDecisionTreeDAO.getInstance().findAfterSalesDecisionTree(currentMilage, afterSalesOffer, rfv);
        Test.startTest();
        List<AST_AfterSalesDecisionTree__c> lstAfterSales = VFC02_AfterSalesDecisionTreeDAO.getInstance().findAfterSalesDecisionTreeWithNullCurrentMilage(lstAfterSalesInsert[2].AfterSalesOffer__c, lstAfterSalesInsert[2].RFV__c);
        Test.stopTest();
        System.assert(lstAfterSales.size() > 0);
    }
}