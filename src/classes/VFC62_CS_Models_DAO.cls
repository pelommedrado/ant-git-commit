/**
*	Class   -   VFC62_CS_Models_DAO
*   Author  -   SureshBabu
*   Date    -   10/03/2013
*	Description	-	DAO class for Models custom settings to retrieve records.
*/
public class VFC62_CS_Models_DAO extends VFC01_SObjectDAO {
	private static final VFC62_CS_Models_DAO instance = new VFC62_CS_Models_DAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC62_CS_Models_DAO(){}

    /**
    * Method responsible for providing the instance of this class.. 
    */  
    public static VFC62_CS_Models_DAO getInstance(){
        return instance;
    }
    
    /**
    *	This method was used to retrieve all record ids from Custom setting models.
    **/
    public List<Id> fetch_All_Models(){
    	List<Id> lstModelIds = new List<Id>();
    	List<CS_Models__c> lstModels = new List<CS_Models__c>();
    	
    	lstModels = [SELECT Name, Id, Document_URL__c FROM CS_Models__c WHERE Name != null ];
    	
    	for( CS_Models__c model : lstModels ){
    		lstModelIds.add( model.Id );
    	}
    	if(lstModelIds.size() > 0 )
    		return lstModelIds;
    	else
    		return null;
    }
}