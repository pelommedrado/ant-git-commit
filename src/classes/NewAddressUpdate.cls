global class NewAddressUpdate implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	Public String query;
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {


		query = 'SELECT Id, ShippingPostalCode, BillingPostalCode FROM Account WHERE ShippingPostalCode != null OR BillingPostalCode != null';

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> accs) {

   			System.debug('accs: ' + accs);

   		    List<Account> accToUpdate = new List<Account>();

   		    List<Contact> cnts = new List<Contact>();
   		    List<Contact> cntToUpdate = new List<Contact>();

   		    List<Lead> lds = new List<Lead>();
   		    List<Lead> ldToUpdate = new List<Lead>();

   			//Process to get address
			for(Account a : accs){

				if(a.ShippingPostalCode != null){

					String fullAddress = Utils.searchCEP(a.ShippingPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO'){

						a.Shipping_PostalCode__c = a.ShippingPostalCode;
						a.Shipping_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            a.Shipping_Country__c = 'Brazil';

					}
	            }

	            if(a.BillingPostalCode != null){

					String fullAddress = Utils.searchCEP(a.BillingPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO'){

						a.Billing_PostalCode__c = a.BillingPostalCode;
						a.Billing_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            a.Billing_Country__c = 'Brazil';

					}

	            }

	            System.debug('conta: ' + a);

	            accToUpdate.add(a);

			}


			//Processo para contatos -------------------------------------------------------------------------------------------------------------


			query = 'SELECT Id, MailingPostalCode, OtherPostalCode FROM Contact WHERE MailingPostalCode != null OR OtherPostalCode != null';
			cnts = Database.query(query);

			System.debug('cnts: ' + cnts);

			for(Contact c : cnts){

				if(c.MailingPostalCode != null){

					String fullAddress = Utils.searchCEP(c.MailingPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO'){

						c.Mailing_PostalCode__c = c.MailingPostalCode;
						c.Mailing_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            c.Mailing_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            c.Mailing_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            c.Mailing_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            c.Mailing_Country__c = 'Brazil';

					}
	            }

	            if(c.OtherPostalCode != null){

					String fullAddress = Utils.searchCEP(c.OtherPostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO'){

						c.Other_PostalCode__c = c.OtherPostalCode;
						c.Other_Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            c.Other_Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            c.Other_City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            c.Other_State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            c.Other_Country__c = 'Brazil';

					}

	            }

	            System.debug('contato: ' + c);
	            cntToUpdate.add(c);

			}


			//Processo para lead -------------------------------------------------------------------------------------------------------------

			query = 'SELECT Id, PostalCode FROM Lead WHERE PostalCode != null';
			lds = Database.query(query);

			System.debug('leads: ' + lds);

			for(Lead l : lds){

				if(l.PostalCode != null){

					String fullAddress = Utils.searchCEP(l.PostalCode);

					if(fullAddress != 'CEP NÃO ENCONTRADO' && fullAddress != 'DIGITE UM CEP VÁLIDO'){

						l.PostalCode__c = l.PostalCode;
						l.Street__c = fullAddress.split(',')[0].split(':')[1].replace(' "', '').replace('"', '');
			            l.Neighborhood__c = fullAddress.split(',')[1].split(':')[1].replace(' "', '').replace('"', '');
			            l.City__c = fullAddress.split(',')[2].split(':')[1].replace(' "', '').replace('"', '');
			            l.State__c = fullAddress.split(',')[3].split(':')[1].replace(' "', '').replace('"', '');
			            l.Country__c = 'Brazil';

					}
	            }
	            System.debug('lead: ' + l);

	            ldToUpdate.add(l);

			}

			Database.SaveResult[] sr = Database.update(accToUpdate, false);
			Database.SaveResult[] sr1 = Database.update(cntToUpdate, false);
			Database.SaveResult[] sr2 = Database.update(ldToUpdate, false);
	
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}