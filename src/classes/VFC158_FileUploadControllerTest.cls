@isTest
private class VFC158_FileUploadControllerTest {

    static testMethod void deveCriarVFC158_FileUploadController() {
        
        Model__c model = new Model__c(
            //   ENS__c = 'abc',
            name = 'ab',
            Market__c = 'abc',
            Status__c = 'Active',
            Model_Spec_Code__c = 'ABC',
            Phase__c = '1',
            Model_PK__c = 'ABC-1'
        );
        
        Database.insert( model );
        
        ApexPages.StandardController sc = 
            new ApexPages.StandardController(model);
        ApexPages.currentPage().getParameters().put('pid', model.id);
        
        VFC158_FileUploadController vfc = new VFC158_FileUploadController(sc);
        vfc.fileUploaderCallback(null);
        vfc.registerController(null);
        Id id = vfc.parentId;
        
        System.assertEquals(model.Id, vfc.parentId);
    }
}