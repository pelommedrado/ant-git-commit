/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Rforce_Bcs_WS_Test implements HttpCalloutMock {
	
	 private HttpResponse resp;
	 
     public Rforce_Bcs_WS_Test(String testBody) {
       resp = new HttpResponse();
       resp.setBody(testBody);
       resp.setStatusCode(200);
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        String formattedtext='<response><clientList><idClient>13553167</idClient><lastName>DURAND</lastName><title>1</title><lang>FRA</lang><firstName>RAYNALD</firstName><BCScommAgreement><Global.Comm.Agreement/><Preferred.Communication.Method/><Post.Comm.Agreement/><Tel.Comm.Agreement/><SMS.Comm.Agreement/><Fax.Comm.Agreement/><Email.Comm.Agreement/></BCScommAgreement><sex>1</sex><contact/><address><strName>13 RUE FELIX TERRIER</strName><countryCode>FR</countryCode><zip>75020</zip><city>PARIS</city></address><typeperson>P</typeperson><vehicleList><vin>VF1C4050500764359</vin><brandCode>RENAULT</brandCode><modelCode>XX</modelCode><modelLabel>OTHER</modelLabel><registrationDate>1991-09-10</registrationDate><firstRegistrationDate>1987-05-01</firstRegistrationDate><registration>381JNK75</registration><new>VO</new><possessionBegin>1991-09-10</possessionBegin></vehicleList></clientList><nbReplies>1</nbReplies></response>';
        
         HttpCalloutMock mock = new Rforce_Bcs_WS_Test(formattedtext);
         Test.setMock(HttpCalloutMock.class, mock);
        
        Rforce_Bcs_WS testgetInfo = new Rforce_Bcs_WS();
        String testget = testgetInfo.getAccountInfo('VF1C4050500764359', '', 'FR', 'DURAND', 'RAYNALD', 'RENAULT', '', '', '', '');
       
       System.debug('Resultat en durrrrrr'+formattedtext);
       System.debug('Resultat attendu'+testget);
       
       // System.assertEquals(formattedtext, testget);
    }
}