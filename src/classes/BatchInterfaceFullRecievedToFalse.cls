global class BatchInterfaceFullRecievedToFalse implements Database.Batchable<sObject> {
    
    String query;
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        query = 'SELECT Id, Received_By_Stock_Full__c FROM VEH_Veh__c WHERE Received_By_Stock_Full__c = true';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<VEH_Veh__c> recievedToFalse){
        
        if(!recievedToFalse.isEmpty() || recievedToFalse != null){
            for(Integer i = 0; i<recievedToFalse.size(); i++){
                recievedToFalse[i].Received_By_Stock_Full__c = false;
            }
            
            try{
                update recievedToFalse;
            } catch(Exception e){
                System.debug('** Error updating "Received_By_Stock_Full__c": ' + e.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){}

}