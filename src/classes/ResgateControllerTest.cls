@isTest
public class ResgateControllerTest {
    
    public static testMethod void test(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        Insert dealer;
        
        Opportunity opp = moc.criaOpportunity();
        opp.StageName = 'Lost';
        opp.Dealer__c = dealer.Id;
        opp.Rescue__c = NULL;
        opp.ReasonLossCancellation__c = 'Abandonment';
        Insert opp;
        
        system.debug('$$$ opp: ' + opp);
        
        ResgateController controller = new ResgateController();
        ApexPages.currentPage().getParameters().put('oppId', opp.Id);
        
        List<Opportunity> oppList = controller.lsOpp;

        controller.ignorarOpp();
        
        PageReference pg = controller.criaNovaOpp();
        
        // testing reopen Opportunity
        PageReference pg2 = new PageReference('/' + opp.Id);
        system.assertEquals(pg2.getUrl(), controller.reabrirOpp().getURL());
        
        List<SelectOption> options = controller.dealerSelectList;
        
    }

}