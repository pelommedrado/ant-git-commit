global with sharing class RFORCE_sendSmsController {

	//used for marketing cloud
	private static String accessToken;
	private static String client_id;
	private static String client_secret;
	private static String messageId;
	private static String phonePage;
	private static String message;

	//used in Page
    public String showHeader{get;set;}
	private static String caseId;
	public Case caso {get;set;}
    public List<FieldsToPutOnSMS__mdt> fieldsList{
        get{

            List<String> fieldDoNotSelected = new List<String>();
            String field = '';
            field += (String.isEmpty(caso.VIN__c) || (String.isNotEmpty(caso.VIN__c) && String.isEmpty(caso.VIN__r.Name)) ) ? 'VIN__r.Name,': '';        
            field += (String.isEmpty(caso.AccountId) || (String.isNotEmpty(caso.AccountId) && String.isEmpty(caso.Account.CustomerIdentificationNbr__c)) ) ? 'Account.CustomerIdentificationNbr__c,': ''; 
            field += (String.isEmpty(caso.CaseNumber))  ? 'CaseNumber,': ''; 
            field += (String.isEmpty(caso.AccountId) || (String.isNotEmpty(caso.AccountId) && String.isEmpty(caso.Account.FirstName))) ? 'Account.FirstName,': ''; 
            field += (String.isEmpty(caso.AccountId) || (String.isNotEmpty(caso.AccountId) && String.isEmpty(caso.Account.LastName))) ? 'Account.LastName,': ''; 
            field += (String.isEmpty(caso.AccountId) || (String.isNotEmpty(caso.AccountId) && String.isEmpty(caso.Account.Name))) ? 'Account.Name,': ''; 
            field += (String.isEmpty(caso.VIN__c) || (String.isNotEmpty(caso.VIN__c) && String.isEmpty(caso.VIN__r.VehicleRegistrNbr__c))) ? 'VIN__r.VehicleRegistrNbr__c,': ''; 

            if(String.isNotEmpty(field) && field.contains(','))
                fieldDoNotSelected.addAll(field.split(','));

            return [select MasterLabel, Field__c FROM FieldsToPutOnSMS__mdt where Field__c not in: fieldDoNotSelected];
        }
        set;
    }

    public String fieldsListToString{get{
        return JSON.serialize([select MasterLabel, Field__c FROM FieldsToPutOnSMS__mdt]);
    }set;}

    public String queryFieldsCaseInfo{ 
        get{
            System.debug('### caseId '+caseId);
            if(String.isEmpty(caseId)) return '';

            Map<String,String> caseFieldsMap = new Map<String,String>();

            String query = 'SELECT ';
            for(integer i = 0; i< fieldsList.size(); i++){
                FieldsToPutOnSMS__mdt field = fieldsList.get(i);
                if(i < (fieldsList.size()-1))
                    query += ' ' + field.Field__c + ',';
                else
                    query += ' ' + field.Field__c + ' ';   
            }

            query += 'FROM Case where Id = \''+ caseId +'\'';   
            
            Case casoS = Database.query(query);
            return JSON.serialize(casoS);

        }
    }


	public RFORCE_sendSmsController() {
        showHeader = ApexPages.currentPage().getParameters().get('header');
		caseId = ApexPages.currentPage().getParameters().get('id');
		setCaseValue(caseId);
		
	}


	private void setCaseValue(String caseId){
		if(String.isNotEmpty(caseId)){ 
			caso = [SELECT Id, AccountId, Account.PersMobPhone__c, Account.ProfMobPhone__c, Account.RecordTypeId,
					Account.RecordType.DeveloperName, Account.FirstName, Account.LastName, Account.Name,
                    VIN__c, VIN__r.Name, VIN__r.VehicleRegistrNbr__c, Account.CPF__pc, Account.CustomerIdentificationNbr__c ,
                    CaseNumber
					FROM Case 
					where Id =:caseId 
				   ];
		}else{
			caso = new Case();
		}
	}


	

	//abaixo códigos utilizados para chamadas no marketing cloud
	@RemoteAction
    global static String sendSMS(String phonePageM, String messageM, String caseId){
        setMarketingCloudSettings();
    	message = messageM;
    	phonePage = phonePageM;
    	String loginLog = login();
    	System.debug('### loginLog '+loginLog);
    	String messageLog = sendMessage(caseId);
    	System.debug('### messageLog '+messageLog);
        return messageLog;
	}

	private static void setMarketingCloudSettings(){
		marketing_cloud_configuration__c marketingCloudSettings = marketing_cloud_configuration__c.getAll().get('default');
		messageId = marketingCloudSettings.messageId__c;
		client_id = marketingCloudSettings.clientId__c;
		client_secret = marketingCloudSettings.clientSecret__c;

	}

	public static String login(){
        try{
         	accessToken = '';
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            
            req.setEndpoint('https://auth.exacttargetapis.com/v1/requestToken');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
           
            req.setBody('{"clientId": "' + client_id + '","clientSecret": "' + client_secret + '"}');
            
            if(!Test.isRunningTest()){
            	res = http.send(req);
            }else{
                String body = '{"accessToken": "7cPZUYhAt0a46Po1S1vH28sG","expiresIn": 3479}';
                res.setBody(body);
            }
            System.debug('### req.getBody() '+req.getBody());
            System.debug('### res.getBody() '+res.getBody());

            JSONParser parser = JSON.createParser(res.getBody());
            
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    
                    String text = parser.getText();
                    
                    parser.nextToken();
                    if (text == 'accessToken') {
                        accessToken = parser.getText();
                        System.debug('accessToken&&' + accessToken);
                        return 'Success';
                        break;
                    } else {
                        System.debug(LoggingLevel.WARN, 'JsonClassifier consuming unrecognized property: '+text);
                       
                    }
                }
            }
            
        }catch(Exception e){
           return e.getMessage();
        }   
       return 'Fail';
    }


	private static String sendMessage(String caseId){
		String urlRequest = 'https://www.exacttargetapis.com/sms/v1/messageContact/'+messageId+'/send';

		String body = '';
		body = '{ '; 
    	body += ' "mobileNumbers": [';
    	body += ' "'+phonePage +'" ';
    	body += '],';
    	body += '"Subscribe": true,';
    	body += '"Resubscribe": true,';
    	body += '"keyword": "RENAULT",'; 
    	body += '"Override": true,';
   		body += ' "messageText": "'+message+'" ';
        body += ' }';

        HttpRequest req 	= new HttpRequest();
        HTTPResponse res 	= new HTTPResponse();
        req.setEndPoint(urlRequest);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        
        req.setMethod('POST');
        req.setCompressed(true);
        req.setBody(body);
        
        try{	
        	Http http = new Http();
            
            if(!Test.isRunningTest()){
        		res = http.send(req);
            }else{
                String body2 = 'tokenId';
                res.setBody(body2);
            }
            System.debug('### res.getBody() '+res.getBody());
            if(res.getBody().contains('tokenId')){
        	   
                saveRecordSendedSMS(caseId,'Sent', '', message,phonePage);
                saveAccount(caseId,phonePage);
                return 'Success';
            }else{
                String message = res.getBody();
                if(message.contains('{"errors":["An invalid phone number was provided: Invalid phone number"')){
                    message = 'Telefone inválido';
                }else if(message.contains('errors":[')){
                    message = message.replace('{"errors":["','');
                    message = message.replace('"]}','');

                }
                saveRecordSendedSMS(caseId,'Error', res.getBody(), message,phonePage);
                return message;
            }
    	}catch(Exception e){
            saveRecordSendedSMS(caseId,'Error', e.getMessage(), message,phonePage);
    		return e.getMessage();
    	}
        

        saveRecordSendedSMS(caseId,'Error', 'Process failure', message,phonePage);
        return 'Fail';
	}

    private static void saveRecordSendedSMS(String caseId,String status, String logError, String sendedText, String phonePage){
        Case caso = [select Id, AccountId From Case where Id =: caseId];
        SendedSMS__c sendedSMS = new SendedSMS__c();
        sendedSMS.Account__c = caso.AccountId;
        sendedSMS.Case__c = caso.Id;
        sendedSMS.LogError__c = logError;
        sendedSMS.SMSTextSendet__c = sendedText;
        sendedSMS.Status__c = status;
		sendedSMS.SendedPhoneNumber__c = phonePage;
        Database.insert(sendedSMS);
    }

    private static void saveAccount(String caseId, String phoneNumber){
        Case caso = [select Id, AccountId, Account.RecordType.DeveloperName From Case where Id =: caseId];
        phoneNumber = validatePhone(phoneNumber);
        Account account = new Account();
        account.Id = caso.AccountId;

        account.PersMobPhone__c = phoneNumber;

        account.ProfMobPhone__c = phoneNumber;
   
        update account;
    }

    private static String validatePhone(String value){
        if(value.contains('+55')) return value.replace('+55','');   
        if(value.contains('55')) return value.replace('55',''); 

        return value;
    }


}