/**
 * Class using the WS09_RCArch : gives information relative to the cases and their attachements    
**/
public with sharing class WS_RCArch_UtilsV2 {
    
 
    private static String endpoint = System.label.VFP05_RCArchivesURL;
    private static String certificate = System.label.clientCertNamex; 
    private static Integer timeout = 80000;

   
      /** @return the list of the cases linked to the given vehicle **/
    public WebserviceRcArchivageV2.caseId[] getCaseIdList(String VehiculeVin,String countryCodeValue){
        String vin = VehiculeVin; 
        WebserviceRcArchivageV2.ServiceImplementationPort RCARCH_WS = new WebserviceRcArchivageV2.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;      
        RCARCH_WS.timeout_x = timeout;
        WebserviceRcArchivageV2.caseId[] RCARCH_RWS = RCARCH_WS.getCaseIdList(countryCodeValue, vin); 
        System.debug('Case Id List for'+VehiculeVin+'contains'+ RCARCH_RWS ); 
        return RCARCH_RWS;
    } 
    

     /** @return the details for a given case id **/
    public WebserviceRcArchivageV2.caseDetails getCaseDetails(String caseId,String countryCodeValue) {    
        // ---- WEB SERVICE CALLOUT -----    
        WebserviceRcArchivageV2.ServiceImplementationPort RCARCH_WS = new WebserviceRcArchivageV2.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        WebserviceRcArchivageV2.caseDetails RCARCH_RWS = RCARCH_WS.getCaseDetails(countryCodeValue, caseId);   
        return RCARCH_RWS; 
    }
    
     /** @return the details for a given case id **/
    public String getXmlDetails(String countryvalue,String caseId) {
        // ---- WEB SERVICE CALLOUT -----    
        
        WebserviceRcArchivageV2.ServiceImplementationPort RCARCH_WS = new WebserviceRcArchivageV2.ServiceImplementationPort();
        String ConvertedText='';
       String RCARCH_RWS='';
        try
        {
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        RCARCH_RWS = RCARCH_WS.getXmlDetails(countryvalue,caseId);
        
        System.debug('RCARCH_RWS>>>>>>>>>>>>>>'+RCARCH_RWS);
            
         if(RCARCH_RWS!=null)
         {
         Blob b=EncodingUtil.base64Decode(RCARCH_RWS);
         ConvertedText=b.toString();
             
         System.debug('ConvertedText>>>>>>>>>>>>>>'+ConvertedText);
         }  
        }
         catch(CalloutException e) {
         system.debug ('RCARCH-GETATTCHAMENTCONTENT, CALLOUT EXCEPTION'+e.getMessage());
           
       } catch (Exception e) {
            system.debug ('RCARCH-GETATTCHAMENTCONTENT, UNKNOWN EXCEPTION'+e.getMessage());
       }
         return ConvertedText;
    }
    
     
    
    /** @return the list of attachements for a given case **/
    public WebserviceRcArchivageV2.attachmentDetailsList[] getAttachmentDetailsList(String countryCode, String caseId) {
        // ---- WEB SERVICE CALLOUT -----    
        WebserviceRcArchivageV2.ServiceImplementationPort RCARCH_WS = new WebserviceRcArchivageV2.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        WebserviceRcArchivageV2.attachmentDetailsList[] RCARCH_RWS = RCARCH_WS.getAttachmentDetailsList(countryCode, caseId);   
        
        return RCARCH_RWS;  
    }
    
    /** @return the content of the given attachment **/
    public String getAttachmentContent(String ParentId,String caseId, String attachmentName) {
     /*   // ---- WEB SERVICE CALLOUT -----    
        WS_RCArch_Case_REST WSARCH = new WS_RCArch_Case_REST();
        String doc = WSARCH.getAttachmentContent(ParentId, caseId, attachmentName);
        return doc; 
        
        */
        
        System.debug('Jes :@:@:@:@ Into getAttachmentContent caseId >>>>>>>>'+caseId);
        System.debug('Jes :@:@:@:@ Into getAttachmentContent attachmentName>>>>>>>>'+attachmentName);

        WebserviceRcArchivageV2.ServiceImplementationPort RCARCH_WS = new WebserviceRcArchivageV2.ServiceImplementationPort();
        RCARCH_WS.endpoint_x = endpoint;
        RCARCH_WS.clientCertName_x = certificate;
        RCARCH_WS.timeout_x=timeout;
        System.debug('Jes :@:@:@:@ b4 WebserviceRcArchivageV2 getAttachmentContent');
       
        RCARCH_WS.inputHttpHeaders_x = new Map<String, String>();
        RCARCH_WS.inputHttpHeaders_x.put('Content-Type', 'text/xml; charset=utf-8');
        RCARCH_WS.inputHttpHeaders_x.put('Accept-Charset', 'utf-8');
        RCARCH_WS.inputHttpHeaders_x.put('Accept', 'text/xml; charset=utf-8');
        //RCARCH_WS.outputHttpHeaders_x = new Map<string, string>();
        system.debug('JES headers:::::' + RCARCH_WS.outputHttpHeaders_x);
       
        String Response = RCARCH_WS.getAttachmentContent(ParentId, caseId, attachmentName);
        
        System.debug('Response for the getAttachmentContent in WSRCArch Utils 2 is >>>>>>>>'+Response);
        
        return Response;  
       
    }
  
    /** @return all the cases information for a given vehicle including case Ids, case Details,
     * case Xml details and list of attachments.
     * The attachments content is not included by default but could be added in the future
     **/
 
}