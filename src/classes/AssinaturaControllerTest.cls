@isTest
public class AssinaturaControllerTest {
	
    public static void createEngagement(){
        
        Engagement__c engSent = new Engagement__c(Status__c = 'Sent');
        insert engSent;
        
        Engagement__c eng = new Engagement__c(Status__c = 'Proposal', 
                                              Month__c = 'Junho');
        insert eng;
        
        Engagement_Item__c engItem = new Engagement_Item__c(Engagement__c = eng.Id, 
                                                            Vehicle_Model__c = 'Fluence', 
                                                            Vehicle_Version__c = 'GT Line', 
                                                            Vehicle_Optional__c = 'Techno Pack', 
                                                            Historical_Average_Sales__c = 10, 
                                                            In_Course__c = 5, 
                                                            Cores_Inativas__c = 'KNS', 
                                                            Proposal__c = 5, 
                                                            Signature__c = 2);
        insert engItem;
        
        for(Integer i=0 ; i<3 ; i++){
            Engagement_Color__c engColor = new Engagement_Color__c(Item__c = engItem.Id, 
                                                                   Colors__c = '694', 
                                                                   Month__c = 'Junho', 
                                                                   Signature__c = 3);
            insert engColor;
        }
        
    }
    
    static testMethod void testObterCoresMes(){
        
        AssinaturaController ass = new AssinaturaController();
        AssinaturaController.TableRowCor tabCor = new AssinaturaController.TableRowCor();
        AssinaturaController.TableRowMes tabMes = new AssinaturaController.TableRowMes();
        createEngagement();
        
        Engagement_Item__c engItem = [SELECT Id, Vehicle_Model__c, Vehicle_Version__c, Vehicle_Optional__c, 
                                      Historical_Average_Sales__c, In_Course__c 
                                      FROM Engagement_Item__c];
        
        AssinaturaController.TableRowModelo tab = new AssinaturaController.TableRowModelo();
        tab.header = false;
        tab.modelo = engItem.Vehicle_Model__c;
        tab.versao = engItem.Vehicle_Version__c;
        tab.opcao = engItem.Vehicle_Optional__c;
        tab.historico = engItem.Historical_Average_Sales__c;
        tab.emCurso = engItem.In_Course__c;
        tab.id = engItem.Id;
        
        
        test.startTest();
            ass.actionInit();
            ass.obterCoresMes();
        test.stopTest();
    }
    
    static testMethod void testActionResumo(){
        AssinaturaController ass = new AssinaturaController();
        
        createEngagement();
        User admin = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User user = new User();
        
        System.runAs(admin){
        Id p = utils.getProfileId('Partner Community User');
       
        Account ac = new Account(recordTypeId = utils.getRecordTypeId('Account', 'Network_Site_Acc'), 
                                 name ='Grazitti', 
                                 Phone = '1155550000', 
                                 IDBIR__c = 'personal BIR');
        insert ac; 
       
        Contact con = new Contact(LastName ='testCon', AccountId = ac.Id);
        insert con;  
                  
        user = new User(alias = 'test123', email = 'test123@noemail.com',
                emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                localesidkey = 'en_US', profileid = p, country = 'United States', IsActive = true,
                ContactId = con.Id, BIR__c = 'personal BIR', 
                timezonesidkey = 'America/Los_Angeles', username = 'testerTest@noemail.com');
       
        insert user;
        }
        
        test.startTest();
        System.runAs(user){
            ass.actionInit();
            ass.actionResumo();
        }
        test.stopTest();
        
        System.assertEquals('personal BIR', ass.birResumo);
    }
    
    static testMethod void testSalvaCores(){
        AssinaturaController ass = new AssinaturaController();
        
        createEngagement();
        
        List<Engagement_Color__c> colorsList = [SELECT Id FROM Engagement_Color__c];
        String paramsId = '';
        Integer n = 3;
        
        for(Engagement_Color__c e : colorsList){
            paramsId += e.Id + ':' + n + ',';
            n += 3;
        }
        paramsId = paramsId.substring(0, paramsId.length()-1);
        
        PageReference pg = Page.Assinatura2;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('param', paramsId);
        
        test.startTest();
        ass.salvaCores();
        test.stopTest();
        
        
    }
    
    static testMethod void testSalvaAssinaturas(){
        AssinaturaController ass = new AssinaturaController();
        
        createEngagement();
        
        List<Engagement_Item__c> itemsList = [SELECT Id FROM Engagement_Item__c];
        String paramsId = '';
        Integer n = 3;
        
        for(Engagement_Item__c e : itemsList){
            paramsId += e.Id + ':' + n + ',';
            n += 3;
        }
        paramsId = paramsId.substring(0, paramsId.length()-1);
        
        PageReference pg = Page.Assinatura2;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('regAss', paramsId);
        
        test.startTest();
        ass.salvaAssinaturas();
        test.stopTest();
    }
    
    static testMethod void testAvancarResumo(){
        AssinaturaController ass = new AssinaturaController();
        
        Engagement__c eng = new Engagement__c(Status__c = 'Validated');
        insert eng;
        
        Engagement_Item__c engItem = new Engagement_Item__c(Engagement__c = eng.Id, 
                                                            Vehicle_Model__c = 'Fluence', 
                                                            Vehicle_Version__c = 'GT Line', 
                                                            Vehicle_Optional__c = 'Techno Pack');
        insert engItem;
        
        List<Engagement_Item__c> itemsList = [SELECT Id FROM Engagement_Item__c];
        String paramsId = '';
        Integer n = 3;
        
        for(Engagement_Item__c e : itemsList){
            paramsId += e.Id + ':' + n + ',';
            n += 3;
        }
        paramsId = paramsId.substring(0, paramsId.length()-1);
        
        PageReference pg = Page.Assinatura2;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('saveRes', paramsId);
        
        test.startTest();
        ass.avancarResumo();
        test.stopTest();
    }
    
    static testMethod void testAvancar(){
        AssinaturaController ass = new AssinaturaController();
        
        createEngagement();
        
        List<Engagement_Color__c> colorsList = [SELECT Id FROM Engagement_Color__c];
        String paramsId = '';
        Integer n = 3;
        
        for(Engagement_Color__c e : colorsList){
            paramsId += e.Id + ':' + n + ',';
            n += 3;
        }
        paramsId = paramsId.substring(0, paramsId.length()-1);
        
        PageReference pg = Page.Assinatura2;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('param', paramsId);
        
        test.startTest();
        ass.actionInit();
        ass.avancar();
        test.stopTest();
    }
    
    static testMethod void testTelaAss(){
        AssinaturaController ass = new AssinaturaController();
        
        test.startTest();
        ass.telaAss();
        test.stopTest();
    }
    
    static testMethod void testAccording(){
        AssinaturaController ass = new AssinaturaController();
        
        test.startTest();
        ass.according();
        test.stopTest();
    }
    
    static testMethod void testGetUsuario(){
        AssinaturaController ass = new AssinaturaController();
        
        String userExpected;
        String userActual;
        User currentUser = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()];
        
        test.startTest();
        System.runAs(currentUser){
            userActual = ass.getUsuario();
        }
        test.stopTest();
        
        userExpected = currentUser.Name;
        
        System.assertEquals(userExpected, userActual);
    }
    
}