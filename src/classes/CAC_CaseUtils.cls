public with sharing class CAC_CaseUtils {
    
    public Datetime now(){
        Datetime now = System.now();
        return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
    }
    
    private User userInfos;
    private final List<Case> lsCase;
    private final Map<Id,Profile> mapProfile;
    private final Map<Id,Case> mapOldCase;
    private Map<Id,RecordType> mapAvailableRecordTypeToUser;
    
    public CAC_CaseUtils(){}
    public CAC_CaseUtils(List<Case> caseList, Map<Id,Case>oldCase){
        Id idUser = UserInfo.getUserId();
        userInfos = [select Id, isCac__c, roleInCac__c from User where id =: idUser];
        this.lsCase = caseList;
        this.mapOldCase = oldCase;
        this.mapAvailableRecordTypeToUser = getAvailableRecordType();
    }
    
    
    
    
    public void statusAnswersWaiting(){
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Analyst')){
            
            
            Set<Id> setIdCase = new Set<ID>();
            for(Case caseI: lsCase)
                if(caseI.Status.equals('Waiting for reply'))
                setIdCase.add(caseI.Id);

            
            //mapa com os ultimos comentário do caso
            Map<Id,CaseComment> mapCaseCommentId = new Map<Id,CaseComment>();
            for(CaseComment caseCom : [select Id, ParentId,CreatedById,IsPublished  from CaseComment where ParentId in:(setIdCase) and IsPublished = true order by id asc ])
                mapCaseCommentId.put(caseCom.ParentId, caseCom);
            
            
            for(Case caseI: lsCase){
                
                if(caseI.Status.equals('Waiting for reply') && mapCaseCommentId.containsKey(caseI.id)){
                    if(!mapCaseCommentId.get(caseI.id).CreatedById.equals(UserInfo.getUserId())){
                        caseI.addError('Insira um comentário');
                    }
                }else if(caseI.Status.equals('Waiting for reply')){
                    caseI.addError('Insira um comentário');
                }
            }
            
        }
    }
    
    public void statusAnswers(){
        if(userInfos.isCac__c && userInfos.roleInCac__c.equals('Analyst')){
            for(Case caseCac : lsCase){
                
                if(mapOldCase.get(caseCac.id).Answer__c <> caseCac.Answer__c && !caseCac.Status.equalsIgnoreCase('Answered'))
                    caseCac.addError('O campo status deve ser alterado para Respondido');
                
                if(String.isNotEmpty(mapOldCase.get(caseCac.id).Answer__c)  
                   && !mapOldCase.get(caseCac.id).Answer__c.equals(caseCac.Answer__c) )
                    caseCac.addError('A reposta não pode ser alterada');
            }
            
        }
    }
    
    
    public Map<Id,RecordType> getAvailableRecordType(){
        Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>();
        Set<String> lsDeveloperName = new Set<String>();
        
        for(cacweb_Mapping__c custonSetCac : cacweb_Mapping__c.getAll().values())
            lsDeveloperName.add(custonSetCac.Record_Type_Name__c);
        
        return new Map<Id,RecordType>([select Id, SObjectType, DeveloperName, Name from RecordType where DeveloperName in:(lsDeveloperName)]);
    }
    
    
    
    
    
    public void updateField(){

        Map<String,cacweb_settingTime__c> mapExpired = getExpiredDate();
        
        for(Case caseUp: lsCase){
            
            if(String.isNotBlank(caseUp.Status) && caseUp.Status.equals('New')){//caseUp.Status != mapOldCase.get(caseUp.Id).Status) {
                
               
                if(caseUp.OwnerId != UserInfo.getUserId() &&
                   userInfos.isCac__c && 
                   String.isNotEmpty(userInfos.roleInCac__c) && 
                   userInfos.roleInCac__c.equals('Dealer')){
                       if(!String.isEmpty(caseUp.Subject__c) && mapExpired.containsKey(caseUp.Subject__c)){
                           caseUp.Expiration_Date__c = corretDayReturn(
                               Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).hoursExpire__c)),
                               Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                               Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                           );

                           caseUp.beforeExpiredDate__c = corretDayReturn(
                               Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).alertExpire__c)),
                               Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                               Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                           );
                           
                           integer valueStartDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c);
                           integer valueFinishedDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c);
                           
                           caseUp.CAC_nearAutomaticClosure__c = corretDayReturn(25,
                                                                                valueStartDate,
                                                                                valueFinishedDate);
                           caseUp.CAC_automaticClosure__c = corretDayReturn(27,
                                                                            valueStartDate,
                                                                            valueFinishedDate);
                           caseUp.CAC_HourExpired8__c = corretDayReturn(8,
                                                                        valueStartDate,
                                                                        valueFinishedDate);
                           
                           caseUp.CAC_HourExpired16__c = corretDayReturn(16,
                                                                         valueStartDate,
                                                                         valueFinishedDate);
                           caseUp.CAC_HourExpired24__c = corretDayReturn(24,
                                                                         valueStartDate,
                                                                         valueFinishedDate);
                           
                       }
                   }
            }
        }
    }
    
    
    //insere a data de expiração
    public dateTime corretDayReturn(long hour , integer startHourSac , integer finishHourSac){
        List<String> lsDaysOfWeek= new List<String>{'Sun','Mon','Tue','Wed','Thu','Fri','Sat'};//dias da semana
            //informações do momento
            integer dayUp = now().day(), monthUp = now().month(), 
            yearUp = now().year(), hourUp = now().hour() ,minutesUp = now().minute();
        
        //inicio/fim do funcionamento do atendimento
        long inicioFuncionamento = startHourSac*60, finalFuncionamento = finishHourSac*60;
        //qual dia é hoje
        String dayOfWeek = now().format('EEE');
        
        //encontrar o dia do mes
        integer dayController=0;
        for(dayController = 0; dayController<lsDaysOfWeek.size();dayController++){
            if(lsDaysOfWeek.get(dayController).equals(dayOfWeek))
                break;//parar quando o dia for correspondido
        }
        
        //recuperar o horario do usuário
        integer agora = Integer.valueOf(System.now().format('HH'))*60 + Integer.valueOf(System.now().format('mm'));
        long hourToMinutes = (hour*60)+(agora);
        
        
        //verifica se o caso foi aberto no sabado ou domingo
        if(lsDaysOfWeek.get(dayController).equals('Sat')){
            hourToMinutes+=1080;
        }else if(lsDaysOfWeek.get(dayController).equals('Sun')){
            hourToMinutes+=540;
        }
        
        //se passar do horario de atendimento pula para o proximo dia
        if(Integer.valueOf(System.now().format('HH'))>=finishHourSac){
            dayUp++;
            minutesUp = 0;
            hourUp = 8;
        }
        integer i=0;
        while(hourToMinutes !=0 ){
            if(hourToMinutes < finalFuncionamento && hourToMinutes > inicioFuncionamento){//verifica se está no mesmo dia
                hourUp =  Integer.valueOf(hourToMinutes/60);
                minutesUp = Integer.valueOf(Math.mod(hourToMinutes, 60));
                hourToMinutes = 0;
                //verifica se caiu no sabado ou domingo
                //
                if(datetime.newInstance(yearUp, monthUp , dayUp , hourUp-3, minutesUp , 0).format('EEE').equals('Sat')){
                    dayUp+=2;
                    
                }
                if(datetime.newInstance(yearUp, monthUp , dayUp , hourUp-3, minutesUp , 0).format('EEE').equals('Sun')){
                    dayUp++;
                }
                //retorna a data correta
                return datetime.newInstance(yearUp, monthUp , dayUp , hourUp, minutesUp , 0);
                
            }else{//proximo dia
                i++;
                dayUp+=dayControl(dayController,lsDaysOfWeek);
                minutesUp = 0;
                hourUp = 8;
                hourToMinutes = inicioFuncionamento+(hourToMinutes-finalFuncionamento);
                
                
                if(dayController==6 )
                    dayController=0;
                dayController++;
            }
            
        }
        
        return datetime.newInstance(yearUp, monthUp , dayUp , hourUp, 0 , 0);
        
    }
    
    public integer dayControl(integer day, List<String>lsDays){
        if(lsDays.get(day).equals('Sun') )
            return 2;
        if(lsDays.get(day).equals('Sat'))
            return 3;
        return 1;
    }
    
    
    //get ExpiredDate by Subject
    public Map<String,cacweb_settingTime__c> getExpiredDate(){
        Map<String,cacweb_settingTime__c> mapSettingTime = new Map<String,cacweb_settingTime__c>();
        for(cacweb_settingTime__c settingTime: cacweb_settingTime__c.getAll().values()){
            mapSettingTime.put(settingTime.Subject__c, settingTime);
        }
        return mapSettingTime;
    }
    
    
    public static void alterOwner(List<Case> listaCaso, Map<Id,Case> mapOldCaso){
        
        User userInf = [select Id, isCac__c, roleInCac__c from User where id =: UserInfo.getUserId()];
        if(userInf.isCac__c && String.isNotEmpty(userInf.roleInCac__c) && userInf.roleInCac__c.equals('Analyst')){
            for(Case casea: listaCaso){
                if(casea.OwnerId != UserInfo.getUserId()){
                    if((casea.Status.equals('Waiting for reply')||
                        casea.Status.equals('Answered')||
                        casea.Status.equals('Under Analysis')
                       ) && 
                       mapOldCaso.get(casea.id).Status.equals('New')){
                           casea.OwnerId = UserInfo.getUserId();
                           if(casea.Status.equals('Waiting for reply')||casea.Status.equals('Answered') ){
                               casea.Status = 'Under Analysis';
                           }
                       }
                }
            }
            
            //Para status respondido
            List<Id> lsIdOldOwner = new List<Id>();
            for(Case casea: listaCaso){//Waiting for reply
                if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered'))
                   || 
                   (!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply'))
                  ){
                      lsIdOldOwner.add(mapOldCaso.get(casea.Id).OwnerId);
                  }
            }
            
            Map<String,User>mapUser = new Map<String,User>([select id , email from User where id in:(lsIdOldOwner)]);
            
            for(Case casea: listaCaso){
                
                casea.Owner_CAC__c = casea.Owner.Name;
                if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered') )
                   ||
                   (!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply') )
                  ){
                      if(mapUser.containsKey(casea.OwnerId)){
                          casea.CAC_EmailAnalyst__c = mapUser.get(casea.OwnerId).Email;
                          casea.OwnerId = casea.CreatedById;
                      }
                  }
            }
        }else if(userInf.isCac__c && String.isNotEmpty(userInf.roleInCac__c) && userInf.roleInCac__c.equals('Dealer')){
            
            Map<id,RecordType> mapRecordType =  new Map<id,RecordType>(Utils.getRecordType('Case'));
            
            List<String>lsNameQueue = new List<String>();
            Map<String,cacweb_Mapping__c> mapCacMap = new Map<String,cacweb_Mapping__c>();
            for(cacweb_Mapping__c cacMap: cacweb_Mapping__c.getAll().values()){
                lsNameQueue.add(cacMap.Name_Queue__c);
                mapCacMap.put(cacMap.Record_Type_Name__c, cacMap);
            }
            
            Map<String,Group> mapGroup = new Map<String,Group>();
            for( Group groupQueue : [select Id, DeveloperName from Group where Type = 'Queue' and DeveloperName in:(lsNameQueue)])
                mapGroup.put(groupQueue.DeveloperName, groupQueue);
            
            Map<String,CaseComment> mapCaseComem = new Map<String,CaseComment>();   
            for(CaseComment casec: [select id,ParentId, CreatedDate from CaseComment where ParentId in:(listaCaso) and CreatedById =: UserInfo.getUserId()])
                mapCaseComem.put(casec.ParentId, casec);
            
            
            for(case caso : listaCaso){
                if(!Trigger.isInsert)
                    if(String.isNotBlank(caso.Status) && 
                       caso.Status.equals('New') && 
                       mapRecordType.containsKey(caso.RecordTypeId) &&
                       mapCacMap.containsKey(mapRecordType.get(caso.RecordTypeId).DeveloperName) &&
                       mapGroup.containsKey(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c)
                      ){
                          if(mapCaseComem.containsKey(caso.Id) && mapCaseComem.get(caso.Id).CreatedDate> mapOldCaso.get(caso.Id).LastModifiedDate){
                              caso.OwnerId =mapGroup.get(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c).Id;
                              
                          }
                          if(!mapOldCaso.get(caso.Id).Status.equals('New'))
                              caso.Case_Return__c  = true;
                          
                      }
            }
        }
    }
    
    public void blockStatus(){
        
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c)){
            
            for(Case caso: lsCase){
                if( 
                    (mapOldCase.get(caso.id).Status.equals('Closed Dealer') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Automatically Answered') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Automatically Waiting Answer') 
                    )
                ){
                    caso.addError('O caso  não pode ser modificado porque está fechado.');  
                }
                
            }
        }
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Analyst')){
            for(Case caso: lsCase){

                if((mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                    mapOldCase.get(caso.Id).Status.equals('Answered'))&&
                   mapOldCase.get(caso.Id).Status != caso.Status
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }
                if(caso.Status.equals('New') && (mapOldCase.get(caso.Id).Status.equals('Under Analysis')||
                                                 mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                                                 mapOldCase.get(caso.Id).Status.equals('Answered')
                                                )
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }else{
                      if(!(caso.Status.equals('New')||
                           caso.Status.equals('Under Analysis')||
                           caso.Status.equals('Waiting for reply')||
                           caso.Status.equals('Answered')
                          )){
                              caso.addError('O caso não pode ser modificado');
                          }
                      
                  }
            }
        }
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer')){
            for(Case caso: lsCase){
                if(!(caso.Status.equals('New')||
                     caso.Status.equals('Closed Dealer')||
                     caso.Status.equals('Closed Automatically Answered')||
                     caso.Status.equals('Closed Automatically Waiting Answer')
                    )){
                        caso.addError('O caso não pode ser modificado');
                    }
            }
        }
    }
    
    /*
public Datetime now(){
Datetime now = System.now();
return now.addSeconds( UserInfo.getTimeZone().getOffset( now ) / 1000 );
}

private final List<Case> lsCase;
private final Map<Id,Profile> mapProfile;
private final Map<Id,Case> mapOldCase;
private Map<Id,RecordType> mapAvailableRecordTypeToUser;

public CAC_CaseUtils(){}
public CAC_CaseUtils(List<Case> caseList, Map<Id,Case>oldCase){
this.lsCase = caseList;
this.mapProfile  = new Map<Id,Profile> ([select id,Name from Profile 
where Name = 'CAC - Dealer'
or Name =: 'CAC - Full']);
this.mapOldCase = oldCase;
this.mapAvailableRecordTypeToUser = getAvailableRecordType();
}




public void statusAnswersWaiting(){
if(mapProfile.containsKey(UserInfo.getProfileId())){
if(mapProfile.get(UserInfo.getProfileId()).Name.equals('CAC - Full')){
System.debug('#### contem perfil');
Set<Id> setIdCase = new Set<ID>();
for(Case caseI: lsCase)
if(caseI.Status.equals('Waiting for reply'))
setIdCase.add(caseI.Id);
System.debug('#### setIdCase '+setIdCase);

//mapa com os ultimos comentário do caso
Map<Id,CaseComment> mapCaseCommentId = new Map<Id,CaseComment>();
for(CaseComment caseCom : [select Id, ParentId,CreatedById,IsPublished  from CaseComment where ParentId in:(setIdCase) and IsPublished = true order by id asc ])
mapCaseCommentId.put(caseCom.ParentId, caseCom);

System.debug('#### mapCaseCommentId '+mapCaseCommentId);
for(Case caseI: lsCase){

if(caseI.Status.equals('Waiting for reply') && mapCaseCommentId.containsKey(caseI.id)){
System.debug('#### entrou na condição 1');
if(!mapCaseCommentId.get(caseI.id).CreatedById.equals(UserInfo.getUserId())){
caseI.addError('Insira um comentário');
}
}else if(caseI.Status.equals('Waiting for reply')){
caseI.addError('Insira um comentário');
System.debug('#### entrou na condição 2');
}
}
}
}
}

public void statusAnswers(){
if(mapProfile.containsKey(UserInfo.getProfileId())){
if(mapProfile.get(UserInfo.getProfileId()).Name.equals('CAC - Full')){
for(Case caseCac : lsCase){

if(mapOldCase.get(caseCac.id).Answer__c <> caseCac.Answer__c && !caseCac.Status.equalsIgnoreCase('Answered'))
caseCac.addError('O campo status deve ser alterado para Respondido');

if(String.isNotEmpty(mapOldCase.get(caseCac.id).Answer__c)  
&& !mapOldCase.get(caseCac.id).Answer__c.equals(caseCac.Answer__c) )
caseCac.addError('A reposta não pode ser alterada');
}
}
}
}


public Map<Id,RecordType> getAvailableRecordType(){
Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>();
Set<String> lsDeveloperName = new Set<String>();

for(cacweb_Mapping__c custonSetCac : cacweb_Mapping__c.getAll().values())
lsDeveloperName.add(custonSetCac.Record_Type_Name__c);

return new Map<Id,RecordType>([select Id, SObjectType, DeveloperName, Name from RecordType where DeveloperName in:(lsDeveloperName)]);
}





public void updateField(){
Map<String,cacweb_settingTime__c> mapExpired = getExpiredDate();
Id idAnalyst = Utils.getProfileId('CAC - Full');
for(Case caseUp: lsCase){
if(String.isNotBlank(caseUp.Status) && caseUp.Status.equals('New')){//caseUp.Status != mapOldCase.get(caseUp.Id).Status) {
if(caseUp.OwnerId != UserInfo.getUserId() && UserInfo.getProfileId() != idAnalyst){
if(!String.isEmpty(caseUp.Subject__c) && mapExpired.containsKey(caseUp.Subject__c)){
System.debug('#### mapExpired '+mapExpired.get(caseUp.Subject__c));
System.debug('#### inicio de expiração');
caseUp.Expiration_Date__c = corretDayReturn(
Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).hoursExpire__c)),
Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
);
System.debug('#### fim de expiração');
caseUp.beforeExpiredDate__c = corretDayReturn(
Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).alertExpire__c)),
Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
);

integer valueStartDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c);
integer valueFinishedDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c);

caseUp.CAC_nearAutomaticClosure__c = corretDayReturn(25,
valueStartDate,
valueFinishedDate);
caseUp.CAC_automaticClosure__c = corretDayReturn(27,
valueStartDate,
valueFinishedDate);
caseUp.CAC_HourExpired8__c = corretDayReturn(8,
valueStartDate,
valueFinishedDate);

caseUp.CAC_HourExpired16__c = corretDayReturn(16,
valueStartDate,
valueFinishedDate);
caseUp.CAC_HourExpired24__c = corretDayReturn(24,
valueStartDate,
valueFinishedDate);

}
}
}
}
}


//insere a data de expiração
public dateTime corretDayReturn(long hour , integer startHourSac , integer finishHourSac){
List<String> lsDaysOfWeek= new List<String>{'Sun','Mon','Tue','Wed','Thu','Fri','Sat'};//dias da semana
//informações do momento
integer dayUp = now().day(), monthUp = now().month(), 
yearUp = now().year(), hourUp = now().hour() ,minutesUp = now().minute();

//inicio/fim do funcionamento do atendimento
long inicioFuncionamento = startHourSac*60, finalFuncionamento = finishHourSac*60;
//qual dia é hoje
String dayOfWeek = now().format('EEE');

//encontrar o dia do mes
integer dayController=0;
for(dayController = 0; dayController<lsDaysOfWeek.size();dayController++){
if(lsDaysOfWeek.get(dayController).equals(dayOfWeek))
break;//parar quando o dia for correspondido
}

//recuperar o horario do usuário
integer agora = Integer.valueOf(System.now().format('HH'))*60 + Integer.valueOf(System.now().format('mm'));
long hourToMinutes = (hour*60)+(agora);


//verifica se o caso foi aberto no sabado ou domingo
if(lsDaysOfWeek.get(dayController).equals('Sat')){
hourToMinutes+=1080;
}else if(lsDaysOfWeek.get(dayController).equals('Sun')){
hourToMinutes+=540;
}

//se passar do horario de atendimento pula para o proximo dia
if(Integer.valueOf(System.now().format('HH'))>=finishHourSac){
dayUp++;
minutesUp = 0;
hourUp = 8;
}
integer i=0;
while(hourToMinutes !=0 ){
if(hourToMinutes < finalFuncionamento && hourToMinutes > inicioFuncionamento){//verifica se está no mesmo dia
hourUp =  Integer.valueOf(hourToMinutes/60);
minutesUp = Integer.valueOf(Math.mod(hourToMinutes, 60));
hourToMinutes = 0;
//verifica se caiu no sabado ou domingo
//
if(datetime.newInstance(yearUp, monthUp , dayUp , hourUp-3, minutesUp , 0).format('EEE').equals('Sat')){
dayUp+=2;

}
if(datetime.newInstance(yearUp, monthUp , dayUp , hourUp-3, minutesUp , 0).format('EEE').equals('Sun')){
dayUp++;
}
//retorna a data correta
return datetime.newInstance(yearUp, monthUp , dayUp , hourUp, minutesUp , 0);

}else{//proximo dia
i++;
dayUp+=dayControl(dayController,lsDaysOfWeek);
minutesUp = 0;
hourUp = 8;
hourToMinutes = inicioFuncionamento+(hourToMinutes-finalFuncionamento);


if(dayController==6 )
dayController=0;
dayController++;
System.debug('### dayController '+dayController);
}

}

return datetime.newInstance(yearUp, monthUp , dayUp , hourUp, 0 , 0);

}

public integer dayControl(integer day, List<String>lsDays){
if(lsDays.get(day).equals('Sun') )
return 2;
if(lsDays.get(day).equals('Sat'))
return 3;
return 1;
}


//get ExpiredDate by Subject
public Map<String,cacweb_settingTime__c> getExpiredDate(){
Map<String,cacweb_settingTime__c> mapSettingTime = new Map<String,cacweb_settingTime__c>();
for(cacweb_settingTime__c settingTime: cacweb_settingTime__c.getAll().values()){
mapSettingTime.put(settingTime.Subject__c, settingTime);
}
return mapSettingTime;
}


public static void alterOwner(List<Case> listaCaso, Map<Id,Case> mapOldCaso){
if(Utils.getProfileId('CAC - Full').equals(UserInfo.getProfileId())){
for(Case casea: listaCaso){
if(casea.OwnerId != UserInfo.getUserId()){
if((casea.Status.equals('Waiting for reply')||
casea.Status.equals('Answered')||
casea.Status.equals('Under Analysis')
) && 
mapOldCaso.get(casea.id).Status.equals('New')){
casea.OwnerId = UserInfo.getUserId();
if(casea.Status.equals('Waiting for reply')||casea.Status.equals('Answered') ){
casea.Status = 'Under Analysis';
}
}
}
}

//Para status respondido
List<Id> lsIdOldOwner = new List<Id>();
for(Case casea: listaCaso){//Waiting for reply
if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered'))
|| 
(!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply'))
){
lsIdOldOwner.add(mapOldCaso.get(casea.Id).OwnerId);
}
}

Map<String,User>mapUser = new Map<String,User>([select id , email from User where id in:(lsIdOldOwner)]);

for(Case casea: listaCaso){

casea.Owner_CAC__c = casea.Owner.Name;
if((!mapOldCaso.get(casea.Id).Status.equals('Answered') && casea.Status.equals('Answered') )
||
(!mapOldCaso.get(casea.Id).Status.equals('Waiting for reply') && casea.Status.equals('Waiting for reply') )
){
if(mapUser.containsKey(casea.OwnerId)){
casea.CAC_EmailAnalyst__c = mapUser.get(casea.OwnerId).Email;
casea.OwnerId = casea.CreatedById;
}
}
}
}else if(Utils.getProfileId('CAC - Dealer').equals(UserInfo.getProfileId())){//usuário comunidade

Map<id,RecordType> mapRecordType =  new Map<id,RecordType>(Utils.getRecordType('Case'));

List<String>lsNameQueue = new List<String>();
Map<String,cacweb_Mapping__c> mapCacMap = new Map<String,cacweb_Mapping__c>();
for(cacweb_Mapping__c cacMap: cacweb_Mapping__c.getAll().values()){
lsNameQueue.add(cacMap.Name_Queue__c);
mapCacMap.put(cacMap.Record_Type_Name__c, cacMap);
}

Map<String,Group> mapGroup = new Map<String,Group>();
for( Group groupQueue : [select Id, DeveloperName from Group where Type = 'Queue' and DeveloperName in:(lsNameQueue)])
mapGroup.put(groupQueue.DeveloperName, groupQueue);

Map<String,CaseComment> mapCaseComem = new Map<String,CaseComment>();   
for(CaseComment casec: [select id,ParentId, CreatedDate from CaseComment where ParentId in:(listaCaso) and CreatedById =: UserInfo.getUserId()])
mapCaseComem.put(casec.ParentId, casec);


for(case caso : listaCaso){
if(!Trigger.isInsert)
if(String.isNotBlank(caso.Status) && 
caso.Status.equals('New') && 
mapRecordType.containsKey(caso.RecordTypeId) &&
mapCacMap.containsKey(mapRecordType.get(caso.RecordTypeId).DeveloperName) &&
mapGroup.containsKey(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c)
){
if(mapCaseComem.containsKey(caso.Id) && mapCaseComem.get(caso.Id).CreatedDate> mapOldCaso.get(caso.Id).LastModifiedDate){
caso.OwnerId =mapGroup.get(mapCacMap.get(mapRecordType.get(caso.RecordTypeId).DeveloperName).Name_Queue__c).Id;

}
if(!mapOldCaso.get(caso.Id).Status.equals('New'))
caso.Case_Return__c  = true;

}
}
}
}

public void blockStatus(){

if(UserInfo.getProfileId() == Utils.getProfileId('CAC - Dealer') || 
UserInfo.getProfileId() == Utils.getProfileId('CAC - Full')
){

for(Case caso: lsCase){
if( 
(mapOldCase.get(caso.id).Status.equals('Closed Dealer') ||
mapOldCase.get(caso.id).Status.equals('Closed Automatically Answered') ||
mapOldCase.get(caso.id).Status.equals('Closed Automatically Waiting Answer') 
)
){
caso.addError('O caso  não pode ser modificado porque está fechado.');  
}

}
}
if(UserInfo.getProfileId() == Utils.getProfileId('CAC - Full')){
for(Case caso: lsCase){
System.debug('###### mapOldCase.get(caso.Id).Status '+mapOldCase.get(caso.Id).Status);
System.debug('######  caso.Status '+ caso.Status);
if((mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
mapOldCase.get(caso.Id).Status.equals('Answered'))&&
mapOldCase.get(caso.Id).Status != caso.Status
){
caso.addError('O caso não pode ser modificado1');
}
if(caso.Status.equals('New') && (mapOldCase.get(caso.Id).Status.equals('Under Analysis')||
mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
mapOldCase.get(caso.Id).Status.equals('Answered')
)
){
caso.addError('O caso não pode ser modificado2');
}else{
if(!(caso.Status.equals('New')||
caso.Status.equals('Under Analysis')||
caso.Status.equals('Waiting for reply')||
caso.Status.equals('Answered')
)){
caso.addError('O caso não pode ser modificado3');
}

}
}
}
if(UserInfo.getProfileId() == Utils.getProfileId('CAC - Dealer')){
for(Case caso: lsCase){
if(!(caso.Status.equals('New')||
caso.Status.equals('Closed Dealer')||
caso.Status.equals('Closed Automatically Answered')||
caso.Status.equals('Closed Automatically Waiting Answer')
)){
caso.addError('O caso não pode ser modificado');
}
}
}
}

*/
    
}