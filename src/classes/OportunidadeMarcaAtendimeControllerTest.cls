@isTest(seeAllData=true)
private class OportunidadeMarcaAtendimeControllerTest {

    static testMethod void test() {
        
        Account sObjAccPersonal = createPersonalAccount();
		//sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
		insert sObjAccPersonal;
        
        Opportunity opp1 = new Opportunity();       
        boolean isCreatedOpportunity1 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb21 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp1.AccountId = sObjAccPersonal.Id;
        opp1.Name = 'OppNameTest_1';
        opp1.StageName = 'Identified';
        opp1.CloseDate = Date.today();
        opp1.Pricebook2Id = pb21.Id;
        opp1.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opp1.CurrencyIsoCode = 'BRL';
        insert opp1;
        
        ApexPages.Standardcontroller st = new ApexPages.Standardcontroller(opp1);
        OportunidadeMarcaAtendimentoController ct = new OportunidadeMarcaAtendimentoController(st);
        
        ct.marcarAtendimento();
        opp1.Description  = 'Oi';
        opp1.StageName = 'In Attedance';
        update opp1;
        
        st = new ApexPages.Standardcontroller(opp1);
        ct = new OportunidadeMarcaAtendimentoController(st);
        
        ct.marcarAtendimento();
        
        opp1.StageName = 'Identified';
        update opp1;
        
        st = new ApexPages.Standardcontroller(opp1);
        ct = new OportunidadeMarcaAtendimentoController(st);
        
        ct.marcarAtendimento();
    }
    
    static Id getRecordTypeId(String developerName)
    {
        RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
        
        return sObjRecordType.Id;
    }
    
    static Account createPersonalAccount()
    {
        /*obtém o record type de concessionária*/
        Id recordTypeId = getRecordTypeId('Personal_Acc');
        
        /*cria e insere a conta que representa a concessionária*/
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        
        return sObjAccPersonal;
    }
}