@isTest
private class VFC136_ZipCodeDAO_Test
{
	private static testMethod void unitTest01()
	{
		// Test Data
		ZipCodeBase__c zip = VFC129_UTIL_TestData.createZipCodeBase('22000100');
		insert zip;

		// Start test
        Test.startTest();

        // #Test 01
        ZipCodeBase__c test01 = VFC136_ZipCodeDAO.getInstance().searchZipCodeByNumber('22000100');
        ZipCodeBase__c test02 = null;
        try {
        	test02 = VFC136_ZipCodeDAO.getInstance().searchZipCodeByNumber('00000000');
        }
        catch (Exception e) {
        	// No zip found
        }

        // Stop test
        Test.stopTest();

        // Verify data
        system.assert(test01 != null);
        system.assert(test02 == null);
	}
}