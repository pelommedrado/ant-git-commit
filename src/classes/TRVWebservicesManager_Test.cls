@isTest
private class TRVWebservicesManager_Test {

    
    static testMethod void myUnitTestbcs() 
    {
        User u = [Select RecordDefaultCountry__c from User Where RecordDefaultCountry__c = 'France' Limit 1]; 
        system.runAs(u) //Test BCS
        {
            
            Test.startTest();   
                Test.setMock(WebServiceMock.class, new AsyncTRVwsdlbcs());
    
                TRVRequestParametersWebServices.RequestParameters req = new TRVRequestParametersWebServices.RequestParameters(); 
                req.firstname = 'Ludovic'; 
                req.lastname = 'testname'; 
                req.country = 'FRA'; 
                req.city = 'paris'; 
                req.demander = 're7_CCB'; 
                req.user_x = 'test@test.com'; 
                req.logger = false; 
                
                //call webservices 
                TRVResponseWebservices response = TRVWebservicesManager.requestSearchParty(req);
                //system.debug('##Ludo TEST' + response.response.dataSource); 
                //system.debug('##Ludo TEST' + response.response.infoClients.get(0).Firstname1); 
                system.assertEquals(response.response.dataSource, 'BCS'); 
                system.assertEquals(response.response.infoClients.get(0).Firstname1, 'RAYNALD');
            Test.stopTest();
        }
        
        
    }
    static testMethod void myUnitTestrbx()
    {
        User u = [Select RecordDefaultCountry__c from User Where RecordDefaultCountry__c = 'France1' Limit 1]; 
        system.runAs(u) //test RBX
        {
            Test.startTest();
                Test.setMock(WebServiceMock.class, new AsyncTRVwsdlrbx());
    
                TRVRequestParametersWebServices.RequestParameters req = new TRVRequestParametersWebServices.RequestParameters(); 
                req.firstname = 'Ludovic'; 
                req.lastname = 'testname'; 
                req.country = 'FRA'; 
                req.city = 'paris'; 
                req.demander = 're7_CCB'; 
                req.user_x = 'test@test.com'; 
                req.logger = false; 
                req.AdriaticCountrySF = 'France';
                req.MiDCECountrySF = 'France';
                req.NRCountrySF = 'France';
                req.PlCountrySF = 'France';
                req.UkIeCountrySF = 'France';
                
                //system.debug('##Ludo test : ' + listu.get(1).RecordDefaultCountry__c);
                //call webservices 
                TRVResponseWebservices response = TRVWebservicesManager.requestSearchParty(req);
                system.debug('##Ludo TEST' + response); 
                system.debug('##Ludo TEST' + response.response.infoClients.get(0).Firstname1); 
                //system.assertEquals(response.response.dataSource, 'RBX'); 
                //system.assertEquals(response.response.infoClients.get(0).Firstname1, 'RAYNALD');
                Test.stopTest();
                   
        }
        
    }
    static testMethod void myUnitTestmdmOrder()
    {
        //TRVwsdlmdm trvWsdl = new TRVwsdlmdm(); 
        //trvWsdl.renaultSoap11 renaultsoap = new trvWsdl.renaultSoap11();
        
        //TRVwsdlmdm.renaultSoap11 rs11 = new TRVwsdlmdm.renaultSoap11();
        
        TRVwsdlmdm.RequestorCredentials reqCredentials = new TRVwsdlmdm.RequestorCredentials();
        reqCredentials.Username = 'admin';
        reqCredentials.EncryptedPassword = 'admin';
        
        TRVwsdlmdm.CustomerOrderCriteria schCriterias = new TRVwsdlmdm.CustomerOrderCriteria();
        schCriterias.FirstName = 'Francois';
        schCriterias.LastName = 'Toto';
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncTRVwsdlmdm());
            TRVwsdlmdm.renaultSoap11 rs11 = new TRVwsdlmdm.renaultSoap11();
            rs11.GetCustomerOrder(reqCredentials,schCriterias); 
        Test.stopTest();
        
    }
    static testMethod void myUnitTestmdm()
    {
        User u = [Select RecordDefaultCountry__c from User Where RecordDefaultCountry__c = 'France2' Limit 1]; 
        system.runAs(u) //test MDM
        {
            Test.startTest();
                Test.setMock(WebServiceMock.class, new AsyncTRVwsdlmdm());
    
                TRVRequestParametersWebServices.RequestParameters req = new TRVRequestParametersWebServices.RequestParameters(); 
                req.firstname = 'Ludovic'; 
                req.lastname = 'testname'; 
                req.country = 'FRA'; 
                req.city = 'paris'; 
                req.demander = 're7_CCB'; 
                req.user_x = 'test@test.com'; 
                req.strPartyID = '354564';
                req.user = u; 
                req.username = 'test@test.com'; 
                req.strMobile = '0606060606'; 
                req.BusinessId1 = '21534';
                req.BusinessId2 = '21534';
                req.PersonalId1 = '21534';
                req.PersonalId2 = '21534';
                req.callmode = 0; 
                req.technicalCountry ='france2';
                req.strLocalID = '2316';
                req.logger = false; 
                
                //system.debug('##Ludo test : ' + listu.get(1).RecordDefaultCountry__c);
                //call webservices 
                TRVResponseWebservices response = TRVWebservicesManager.requestSearchParty(req);
                system.debug('##Ludo TEST' + response); 
                system.debug('##Ludo TEST' + response.response.infoClients.get(0).Firstname1);
                req.logger = true; 
                response = TRVWebservicesManager.requestGetParty(req);
                system.debug('##Ludo TEST' + response); 
                system.debug('##Ludo TEST' + response.response.infoClients.get(0).Firstname1); 
                
                //system.assertEquals(response.response.dataSource, 'RBX'); 
                //system.assertEquals(response.response.infoClients.get(0).Firstname1, 'RAYNALD');
                
                //cover template response : 
                response.response.info.codRetour = '?';
                response.response.info.msgRetour = '?';
                response.response.searchPartyResponse = 1; 
                
                for(TRVResponseWebservices.Infoclient c : response.response.infoClients)
                {
                    c.FirstName2 = '?';
                    c.LastName2 = '?';
                    c.name = '?';
                    
                    c.deceased = '?'; 
                    c.title = '?';
                    c.civility = '?';
                    c.sex = '?';
                    c.marital = '?';
                    c.Lang = '?';
                    c.renaultEmployee = '?';
                    c.nbrChildrenHome = '?';
                    c.preferredMedia = '?';
                    c.OccupationalCategoryCodeP = '?';
                    c.OccupationalCategoryCode = '?';
                    c.partySegment = '?';
                    c.commercialName = '?';
                    c.numberOfEmployees = 10;
                    c.financialStatus = '?';
                    c.codeNAF = '?';
                    c.legalNature = '?';
                    c.partySub = '?';
        
                    c.addressLine2 = '?';
                    c.compl1 = '?';
                    c.compl3 = '?';
                    c.CountryCode = '?';
                    c.PersonOtherStreet = '?';
                    c.PersonOtherCity = '?';
                    c.PersonOtherState = '?';
                    c.PersonOtherPostalCode = '?';
                    c.PersonOtherCountry = '?';
                    c.prefDealer = '?';
                    c.secondCompanyID = '?';
                    c.localCustomerID1 = '?';
                    c.localCustomerID2 = '?';
                    c.OccupationalCategoryDescription ='?';
                    c.TelCommAgreementDate = system.today();
                    c.EmailCommAgreementDate = system.today();
                    c.SMSCommAgreementDate = system.today();
                    c.PostCommAgreementDate = system.today();
                    c.stopComFlag = '?';
                    c.stopComFlagDate = system.today();
                    c.stopComSMDFlag = '?';
                    c.stopComSMDFlagDate = system.today();
                    
                    for (TRVResponseWebservices.Vehicle v : c.vcle)
                    {
                        v.model = '?';
                        v.deliveryDate = '?';
                        v.technicalControlDate = '?';
                        v.dataCompletionBVM = '?';
                        v.dataCompletionETICOM = '?';
                        v.dataCompletionSLK = '?';
                        v.dataBVMUpto = '?';
                        v.vehicleType = '?';
                        v.possessionEnd = '?';
                        v.vnvo = '?';
                        v.status = '?';
                        v.garageStatus = '?';
                        v.vinExternal = '?';
                    }
                }
                
                Test.stopTest();
                
                
                   
        }
        
    }
    static testMethod void myUnitTestGetCountryInfo()
    {
        //TODO Ludo Add test unit 
        User u = [Select RecordDefaultCountry__c from User Where RecordDefaultCountry__c = 'France2' Limit 1]; 
        system.runAs(u) //test MDM
        {
            TRVRequestParametersWebServices.RequestParameters req = new TRVRequestParametersWebServices.RequestParameters(); 
            req.firstname = 'Ludovic'; 
            req.lastname = 'testname'; 
            req.country = 'FRA'; 
            req.city = 'paris'; 
            req.demander = 're7_CCB'; 
            req.user_x = 'test@test.com'; 
            req.logger = true;
            TRVWebservicesManager.getcountryInfo(req); 
    
            req.country = null; 
            req.logger = false;
            TRVWebservicesManager.getcountryInfo(req);
            
        }
        u = [Select RecordDefaultCountry__c from User Where RecordDefaultCountry__c = 'region' Limit 1];
        system.runAs(u) //test region
        {
            TRVRequestParametersWebServices.RequestParameters req = new TRVRequestParametersWebServices.RequestParameters(); 
            req.firstname = 'Ludovic'; 
            req.lastname = 'testname'; 
            req.country = 'FRA'; 
            req.city = 'paris'; 
            req.demander = 're7_CCB'; 
            req.user_x = 'test@test.com'; 
            req.logger = true;
            req.technicalCountry = 'france2';
            TRVWebservicesManager.getcountryInfo(req);
            
            req.country = null; 
            req.logger = false;
            TRVWebservicesManager.getcountryInfo(req);
        }
    }
    @testSetup  
    static void methodName() 
    {
        //insert User
        //Profile profileCus = [SELECT Id FROM Profile WHERE Profile.Name =  'System Administrator'];
        Set<String> objectFields = Schema.SObjectType.User.fields.getMap().keySet();

        list<User> listu = new list<User>();
        User u = new User();   //user 0 : BCS
        u.ProfileId = Label.PROFILE_SYSTEM_ADMIN; 
        u.FirstName = 'testFirstName';
        u.LastName = 'testLastName';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
		if(objectFields.contains('iscac__c')) {
			u.put('iscac__c', true);
        }
        listu.add(u); 

        u = new User(); //user RBX
        u.ProfileId = Label.PROFILE_SYSTEM_ADMIN; 
        u.FirstName = 'testFirstName1';
        u.LastName = 'testLastName1';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France1' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
		if(objectFields.contains('iscac__c')) {
			u.put('iscac__c', true);
        }
        listu.add(u); 

        u = new User(); //user MDM
        u.ProfileId = Label.PROFILE_SYSTEM_ADMIN; 
        u.FirstName = 'testFirstName2';
        u.LastName = 'testLastName2';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'France2' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
		if(objectFields.contains('iscac__c')) {
			u.put('iscac__c', true);
        }
        listu.add(u); 

        u = new User(); //user Region
        u.ProfileId = Label.PROFILE_SYSTEM_ADMIN; 
        u.FirstName = 'testFirstName3';
        u.LastName = 'testLastName3';
        u.Email = u.FirstName + u.LastName + '@example.com';
        u.Username = u.FirstName + u.LastName + '@example.com.renault.countrelation';
        u.Alias = u.FirstName.left(1) + u.LastName.left(7);
        u.CommunityNickName = u.FirstName + u.LastName;
        u.RecordDefaultCountry__c = 'region' ;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'fr_FR_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'fr';
		if(objectFields.contains('iscac__c')) {
			u.put('iscac__c', true);
        }
        listu.add(u); 
        insert listu; 
        
        //insert custom setting country info
        list<Country_Info__c> listCI= new list<Country_Info__c>();
        Country_Info__c countryInfo = new Country_Info__c(); 
        countryInfo.name = 'France'; 
        countryInfo.Country_Code_3L__c = 'FRA'; 
        countryInfo.Country_Code_2L__c = 'FR';
        countryInfo.DataSource__c = 'BCS'; 
        countryInfo.Language__c = 'fr';
        listCI.add(countryInfo); 
        countryInfo = new Country_Info__c();
        countryInfo.name = 'France1'; 
        countryInfo.Country_Code_3L__c = 'FRA'; 
        countryInfo.Country_Code_2L__c = 'FR';
        countryInfo.DataSource__c = 'SIC'; 
        countryInfo.call_mode__c=12;
        countryInfo.Language__c = 'fr';
        listCI.add(countryInfo); 
        countryInfo = new Country_Info__c();
        countryInfo.name = 'France2'; 
        countryInfo.Country_Code_3L__c = 'FRA'; 
        countryInfo.Country_Code_2L__c = 'FR';
        countryInfo.DataSource__c = 'MDM'; 
        countryInfo.Language__c = 'fr';
        listCI.add(countryInfo);
        countryInfo = new Country_Info__c();
        countryInfo.name = 'region'; 
        countryInfo.Country_Code_3L__c = 'FRA'; 
        countryInfo.Country_Code_2L__c = 'FR';
        countryInfo.DataSource__c = ''; 
        countryInfo.Language__c = 'fr';
        listCI.add(countryInfo);
        insert listCI;

        list<WebServicesInfo__c> listws= new list<WebServicesInfo__c>();
        WebServicesInfo__c wsinfo = new WebServicesInfo__c();  
        wsinfo.name = 'MDM'; 
        wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CMDM_Renault_uat_1_6';  
        listws.add(wsinfo); 
        wsinfo = new WebServicesInfo__c(); 
        wsinfo.name = 'BCS'; 
        wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/BCS_CrmGetCustDataService';
        listws.add(wsinfo);
        wsinfo = new WebServicesInfo__c(); 
        wsinfo.name = 'RBX'; 
        wsinfo.endpoint__c = 'https://webservices2.renault.fr/ccb/0/CrmGetCustData_4_1';
        listws.add(wsinfo);
        insert listws; 
        
        //insert table transco
        List<sObject> allObjects = new List<sObject>();
        allObjects.add(new MDMVehicleRelationType__c(Name='1',Value__c='Own', DataSource__c = 'MDM', VehicleRelationType__c='Owner')); 
        allObjects.add(new MDMVehicleRelationType__c(Name='2',Value__c='Own', DataSource__c = 'BCS', VehicleRelationType__c='Owner'));
        allObjects.add(new MDMVehicleRelationType__c(Name='3',Value__c='Own', DataSource__c = 'RBX', VehicleRelationType__c='Owner'));
        
        allObjects.add(new MDMCountry__c(Name='1',Value__c='FRANCE', DataSource__c = 'MDM', Country__c='France')); 
        allObjects.add(new MDMCountry__c(Name='2',Value__c='FRA', DataSource__c = 'BCS', Country__c='France'));
        allObjects.add(new MDMCountry__c(Name='3',Value__c='FRA', DataSource__c = 'RBX', Country__c='France'));
        
        allObjects.add(new Language__c(Name='1',Value__c='FRE', DataSource__c = 'MDM', MDMLanguage__c='English')); 
        allObjects.add(new Language__c(Name='2',Value__c='ENG', DataSource__c = 'BCS', MDMLanguage__c='English'));
        allObjects.add(new Language__c(Name='3',Value__c='ENG', DataSource__c = 'RBX', MDMLanguage__c='English'));
        
        allObjects.add(new MDMCategory__c(Name='1',Value__c='EMPLOYE', DataSource__c = 'MDM', JobClass__c='Worker')); 
        allObjects.add(new MDMCategory__c(Name='2',Value__c='A1', DataSource__c = 'BCS', JobClass__c='Worker'));
        allObjects.add(new MDMCategory__c(Name='3',Value__c='A1', DataSource__c = 'RBX', JobClass__c='Worker'));
        
        allObjects.add(new MDMPartySegment__c(Name='1',Value__c='Others', DataSource__c = 'MDM', PartySegment__c='Others')); 
        allObjects.add(new MDMPartySegment__c(Name='2',Value__c='Others', DataSource__c = 'BCS', PartySegment__c='Others'));
        allObjects.add(new MDMPartySegment__c(Name='3',Value__c='Others', DataSource__c = 'RBX', PartySegment__c='Others'));
        
        allObjects.add(new MDMFinancialStatus__c(Name='1',Value__c='ACT', DataSource__c = 'MDM', FinancialStatus__c='ACT')); 
        allObjects.add(new MDMFinancialStatus__c(Name='2',Value__c='ACT', DataSource__c = 'BCS', FinancialStatus__c='ACT'));
        allObjects.add(new MDMFinancialStatus__c(Name='3',Value__c='ACT', DataSource__c = 'RBX', FinancialStatus__c='ACT'));
        
        allObjects.add(new Gender__c(Name='1',Value__c='M', DataSource__c = 'MDM', Gender__c='Woman')); 
        allObjects.add(new Gender__c(Name='2',Value__c='F', DataSource__c = 'BCS', Gender__c='Woman'));
        allObjects.add(new Gender__c(Name='3',Value__c='F', DataSource__c = 'RBX', Gender__c='Woman'));
        
        allObjects.add(new MaritalStatus__c(Name='1',Value__c='Single', DataSource__c = 'MDM', MaritalStatus__c='Single')); 
        allObjects.add(new MaritalStatus__c(Name='2',Value__c='Single', DataSource__c = 'BCS', MaritalStatus__c='Single'));
        allObjects.add(new MaritalStatus__c(Name='3',Value__c='Single', DataSource__c = 'RBX', MaritalStatus__c='Single'));
        
        allObjects.add(new MDMCivillity__c(Name='1',Value__c='M', DataSource__c = 'MDM', Civility__c='Mr.')); 
        allObjects.add(new MDMCivillity__c(Name='2',Value__c='M', DataSource__c = 'BCS', Civility__c='Mr.'));
        allObjects.add(new MDMCivillity__c(Name='3',Value__c='M', DataSource__c = 'RBX', Civility__c='Mr.'));
        
        allObjects.add(new MDMTitle__c(Name='1',Value__c='M', DataSource__c = 'MDM', Title__c='Owner')); 
        allObjects.add(new MDMTitle__c(Name='2',Value__c='M', DataSource__c = 'BCS', Title__c='Owner'));
        allObjects.add(new MDMTitle__c(Name='3',Value__c='M', DataSource__c = 'RBX', Title__c='Owner'));
        insert allObjects;
        
        List<sObject> allObjectsbis = new List<sObject>();
        allObjectsbis.add(new MDMPartySubType__c(Name='1',Value__c='AC', DataSource__c = 'MDM', Sub__c='Craftsman/Retailer')); 
        allObjectsbis.add(new MDMPartySubType__c(Name='2',Value__c='AC', DataSource__c = 'BCS', Sub__c='Craftsman/Retailer'));
        allObjectsbis.add(new MDMPartySubType__c(Name='3',Value__c='AC', DataSource__c = 'RBX', Sub__c='Craftsman/Retailer'));
        
        insert allObjectsbis;
    
    }
    
    
}