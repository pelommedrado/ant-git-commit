public class AgendamentoServicoController {

    public static Id RECORDTYPE_SERVICE    = Utils.getRecordTypeId('Opportunity', 'Service');
    
    public String tipoServico { get;set; }
    public List<SelectOption> tipoServicoSelect { get; set; }
    
    public Task data { get;set; }
    
    public String tecnico { get;set; }
    public List<SelectOption> tecnicoSelect { get; set; }
    
    public String status { get;set; }
    public List<SelectOption> statusSelect { get; set; }
    
    public List<ItemEvent> itemEventList { get;set; }
    
    public String accId { get;set; }
    public List<Contact> contactList { get;set;}

    public List<Opportunity> notInList { get;set;}
     
    public AgendamentoServicoController() {
        this.tipoServicoSelect = Utils.getPicklistValues('Opportunity', 'ServiceType__c');
        
        this.statusSelect = new List<SelectOption>();
        this.statusSelect.add(new SelectOption('Pre Scheduled', 'Pré Agendado'));
        this.statusSelect.add(new SelectOption('Scheduled', 'Agendado'));
        this.statusSelect.add(new SelectOption('Performed', 'Realizado'));
        this.statusSelect.add(new SelectOption('Unperformed', 'Não Realizado'));
        
        this.data = new Task();
        
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        
        this.accId  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        this.contactList = [
            SELECT Id, Name
            FROM Contact
            WHERE AccountId =: accID AND Available_to_Service__c = 'Available'
            ORDER BY Name
        ];
        
        this.tecnicoSelect = new List<SelectOption>();
        for(Contact ct : contactList) {
            this.tecnicoSelect.add(new SelectOption(ct.Id, ct.Name));
        }
        
        filtrar();
    }
    
    public PageReference filtrar() {
        System.debug('Filtrando resultados...');
        System.debug('Tipo servico:' + tipoServico);
        System.debug('Tecnico:' + tecnico);
        System.debug('Status:' + status);
        System.debug('Data:' + data);
        
        System.debug('Account:' + accId);
        System.debug('Record Type:' + RECORDTYPE_SERVICE);
        
        this.itemEventList = new List<ItemEvent>();
        
        List<Opportunity> oppList = Database.query(montarQueryOpp());
        
        if(oppList.isEmpty()) {
            return null;
        }
        
        Map<String, ItemEvent> oppItemMap = new Map<String, ItemEvent>();
        for(Opportunity op : oppList) {
            ItemEvent item = new ItemEvent();
            item.opp = op;
            
            itemEventList.add(item);
            oppItemMap.put(op.Id, item);
        }
        
        String query = montarQueryEvent(oppList);
        System.debug('Query:' + query);
        List<Event> eventosList = Database.query(query);
        
        /*Map<String, Opportunity> oppEventMap = new Map<String, Opportunity>();
        for(Opportunity op : oppList) {
            oppEventMap.put(op.Id, op);
        }*/
        
        /*Map<String, Contact> contMap = new Map<String, Contact>();
        for(Contact ct : this.contactList) {
            contMap.put(ct.Id, ct);
        }*/
        
        for(Event ev : eventosList) {
            ItemEvent item = oppItemMap.get(ev.WhatId);
            
            if(item != null) {
                item.evento = ev;
                //item.cont = contMap.get(ev.WhoId);    
            }
        }
        
        return null;
    }
 
    private String montarQueryOpp() {
        
        Datetime df = Datetime.now().addDays(-15);
        notInList = [
            SELECT ID 
            FROM Opportunity 
            WHERE (StageName = 'Performed' OR StageName = 'Unperformed') AND LastModifiedDate <: df
        ];
        
        
        String dateTimeFormat = formatData(0, 0, 0, 'yyyy-MM-dd');
        
        return 'SELECT Id, Name, ' 	+ 
            'Service_Protocol__c, ' + 
            'Mobile__c, ' 			+ 
            'VehicleRegistrNbr__c, '+ 
            'ServiceType__c, ' 		+
            'ProposalScheduleDate__c, ' 	+
            'ScheduledDate__c, ' + 
            'ScheduledHour__c, ' + 
            'ResponsibleTechnical__r.Name, ' +
            'StageName ' 			+
            'FROM Opportunity ' 	+
            'WHERE Dealer__c =: accID AND RecordTypeId =: RECORDTYPE_SERVICE' + 
            ( String.isNotBlank(tecnico)        ? ' AND ResponsibleTechnical__c =: tecnico' : '' ) + 
            ( String.isNotBlank(tipoServico)        ? ' AND ServiceType__c =: tipoServico' : '' ) + 
            ( String.isNotBlank(status)        ? ' AND StageName =: status' : '' ) + 
            
            ( String.isNotBlank(dateTimeFormat)        ? ' AND ScheduledDate__c = ' + dateTimeFormat : '' ) +
            
            (String.isBlank(tecnico) && String.isBlank(tipoServico) && String.isBlank(status) ? ' AND ID NOT IN:notInList '  : '') +
            
            ' LIMIT 500 ';
    }
    
    private String montarQueryEvent(List<Opportunity> oppList) {
        String dateTimeFormat = formatData(0, 0, 0, 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''); 
        String dateTimeFormat2 = formatData(23, 59, 59, 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');  
        
        return    
            'SELECT Id, WhatId , WhoId, StartDateTime ' + 
            'FROM Event ' +
            'WHERE Id != null AND WhatId in: oppList ' + 
            ( String.isNotBlank(dateTimeFormat)        ? 
             'AND StartDateTime >= ' + dateTimeFormat + ' AND StartDateTime <= ' + dateTimeFormat2 : '' ) +
            ' LIMIT 500';
    }
    
    private String formatData(Integer h, Integer m, Integer s, String form) {
        if(data.ActivityDate == null) {
            return null;
        }
        
        Datetime dt = datetime.newInstance(
            data.ActivityDate.year(), 
            data.ActivityDate.month(), 
            data.ActivityDate.day(), h, m, s);
        
        return dt.format(form);  
    }
    
    public PageReference visualizar() {
        String oppId = 
            ApexPages.currentPage().getParameters().get('oppId');
        
        return new PageReference('/apex/AgendamentoVerServico?Id=' + oppId); 
    }
    
    public PageReference novo() {
        return Page.AgendamentoEditarServico;
    }
    
    public PageReference limparFiltro() {
        this.tipoServico = null;
        this.tecnico = null;
        this.status = null;
        this.data = new Task();
        
        filtrar();
        
        return null;
    }
    
    public PageReference editar() {
        String oppId = 
            ApexPages.currentPage().getParameters().get('oppId');
        
        return new PageReference('/apex/AgendamentoEditarServico?Id=' + oppId); 
    }
    
    public class ItemEvent {
        public Event evento { get;set; }
        public Opportunity opp { get;set; }
       
        public ItemEvent() {
        }
    }
}