public with sharing class CaseCommentTriggerHandler {

    public static void onBeforeInsert(list < Casecomment > listcomment) {
 
    }  
    //After Insert method start
/**
 * @author Sumanth(RNTBCI)
 * @date 16/07/2015
 * @description All the functionalities on after insert event are called inside this method.Inserting the case comment details to IKR activity object,Activity Management on Case
 * @param list of case comments and old map of case comments 
 */    
    public static void onAfterInsert(list <CaseComment> lCaseComment, boolean bIsInsert, Map <Id, CaseComment> mOldMap) {
        User u=[Select Id,Profile.Name,Email from User where ID= :userinfo.getuserid()];
        String sIKRUserId = System.Label.IKRUserId;        
        //Create a task to SRC user when a attachment is inserted by dealer or IKR User
        Set<Id> scaseid =new Set<Id>();
        Set<Id> groupid =new Set<Id>();
        List<Case> lcaselist=new List<Case>();
        List<Task> lSRCtasklist=new List<Task>();
        List<Case> lgroupcaselist=new List<Case>();
        String IKRUserId = System.Label.IKRUserId;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Will excute only for community dealers and IKR users 
        if(u.Profile.Name==system.label.USR_Id_CoreServiceProfile || u.Id==IKRUserId){
            //Getting list of attachments on case
            for(CaseComment csc : lCaseComment){
                if(csc.ParentId.getSobjectType() == Case.SobjectType){
                    scaseid.add(csc.ParentId);    
                }
            }
            //Creating task only if the case is owned by user
            if(scaseid!=null){
                try{
                    lcaselist=[Select Id,OwnerId,ContactId,Casenumber,CountryCase__c from Case where Id IN: scaseid];
                    for(Case c : lcaselist){
                        String ownerid=c.OwnerId;
                        if(ownerid.SubString(0,3)!='00G'){
                            System.debug('Inside 1--->');
                            Task t=new Task(WhoId=c.ContactId,IsReminderSet = true,WhatId=c.id,Type=system.label.TSK_Type_Dealer_To_SRC,Subject=system.label.TSK_Type_Dealer_To_SRC,ActivityDate=system.today(),Priority=system.label.TSK_Priority_Normal,Status=system.label.TSK_Status_ToDo,ReminderDateTime=system.now().addHours(2),OwnerId=c.OwnerId);                                              
                            lSRCtasklist.add(t);
                        } 
                        else if(ownerid.SubString(0,3)=='00G'){
                            groupid.add(c.OwnerId);
                            lgroupcaselist.add(c); 
                        }
                    }    
                        if(lgroupcaselist!=null){
                            Map<Id,Group> mcasegroup = new Map<Id,Group>([Select Id,Email from Group where Id IN : groupid]);
                            system.debug('mcasegroup -->'+mcasegroup );
                            for(Case cs : lgroupcaselist){
                                String queueemail=mcasegroup.get(cs.OwnerId).Email;
                                system.debug('queueemail-->'+queueemail);
                                if(queueemail!=null){
                                    List<String> EmailList = new List<String>();
                                    EmailList.add(queueemail);
                                    mail.setToAddresses(EmailList);
                                    mail.setSubject(system.label.CSC_NewCaseCommentonCase + cs.CaseNumber);
                                    mail.setUseSignature(false);
                                    string msg = system.label.CSC_NewCaseCommentonCase + cs.CaseNumber +' '+system.label.ATT_NewAttachmentBy+' '+ userInfo.getName();
                                    mail.setHtmlBody(msg);
                                    // Send the email
                                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                                  
                                }                         
                            }
                        }                             
                    insert lSRCtasklist;                         
                }catch(DMLException e){
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                        System.debug('For IKR --> Task Insert Failed due to'+e.getDmlMessage(i));
                    }                    
                }
            }    
        }  
        //Send an email to Dealer and create a task when an CaseComment is inserted by SRC agent
        //Getting the list of attachment on cases
        if(u.Profile.Name!=system.label.USR_Id_CoreServiceProfile){
            for(CaseComment csc : lCaseComment){
                if(csc.ParentId.getSobjectType() == Case.SobjectType){
                    scaseid.add(csc.ParentId);    
                }
            }       
            if(scaseid!=null){
                try{
                    lcaselist=[Select Id,OwnerId,ContactId,Dealer_User__c,EmailContactDealer__c,CreatedById,DealerContactLN__c,CaseNumber from Case where Id IN: scaseid];
                    for(Case c : lcaselist){
                        String ownerid=c.OwnerId;
                        //Sending email to dealer contact on case
                        if(c.EmailContactDealer__c!=null){
                            List<String> EmailList = new List<String>();
                            EmailList.add(c.EmailContactDealer__c);
                            mail.setToAddresses(EmailList);
                            mail.setSubject(system.label.CSC_NewCaseCommentonCase  + c.CaseNumber);
                            mail.setUseSignature(false);
                            string msg = system.label.CSC_NewCaseCommentonCase + c.CaseNumber + ' ' +system.label.ATT_NewAttachmentBy + ' ' +userInfo.getName();
                            mail.setHtmlBody(msg);
                            // Send the email
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });      
                            System.debug('Inside 2--->');                              
                            //Creating Task
                            Task t=new Task(WhoId=c.ContactId,IsReminderSet = true,WhatId=c.id,Type=system.label.TSK_Type_Comment_SRC_to_Dealer,Subject=system.label.TSK_Type_Comment_SRC_to_Dealer,ActivityDate=system.today(),Priority=system.label.TSK_Priority_Normal,Status=system.label.TSK_Status_ToDo,ReminderDateTime=system.now().addHours(2),OwnerId=c.Dealer_User__c);                                              
                            lSRCtasklist.add(t);                       
                        }    
                    }
                    insert lSRCtasklist;                        
                }catch(DMLException e){
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                        System.debug('For SRC --> Task Insert Failed due to'+e.getDmlMessage(i));
                    }
                }
            }    
        }                 
    }
    //After Insert method end 
}