public class SfaRedirectPageController {
	
	public String appHomePage {get;set;}
	
	public List<String> optionAppList {
		get {
			SFA_RenaultMaisCliente_HeaderController ctrl = new SFA_RenaultMaisCliente_HeaderController();
			return ctrl.optionAppList;
		}
	}
	
	public SfaRedirectPageController() {
		String currentApp = optionAppList != null && !optionAppList.isEmpty() ? optionAppList[0] : '';
		System.debug('@@@ currentApp: ' + currentApp);
		
		appHomePage = SFA_RenaultMaisCliente_HeaderController.setHomePage(currentApp);
	}
	
}