public with sharing class VFC123_CampaignValidation
{
	// Public attributes
	public Boolean hasError {get;set;}
	
	// Private attributes
	
	/**
	 * Contructor
	 */	
	public VFC123_CampaignValidation()
	{
		hasError = false;
	}

	/**
	 * Validade data when it comes from data loader
	 */
	public List<Campaign> validadeCampaignForDataLoader(List<Campaign> lstNewCampaign)
	{
		system.debug(LoggingLevel.INFO, '*** VFC123_CampaignValidation.validadeCampaignForDataLoader()');
		
		List<Campaign> lstNewValidCampaign = new List<Campaign>();
		
		Set<String> setBIR = new Set<String>();
		Set<String> setParentCampaign = new Set<String>();
		
		// Get all recordTypes Ids necessary, where the key is the developer name
		Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
			getRecordTypeIdByDevNameSet(new Set<String>{'ParentCampaign', 'SingleCampaign'});

		// Build all sets to retrieve data
		for (Campaign campaign : lstNewCampaign)
		{
			// All campaigns via dataloader will be single campaign
			campaign.RecordTypeId = mapRecordTypes.get('SingleCampaign');
			
			setBIR.add(campaign.ID_BIR__c);
			setParentCampaign.add(campaign.DBMCampaignCode__c);
		}
		
		// Fetch a map of AccountId, where the key is the BIR number
		Map<String, Id> mapDealerByBIRNumberSet = VFC12_AccountDAO.getInstance().
			fetchDealerIdByBIRnumberSet(setBIR);
		
		// Fetch a map of CampaignId, where the key is DBMCampaignCode__c
		Map<String, Campaign> mapParentCampaignByDBMCampaignCodeSet = VFC121_CampaignDAO.getInstance().
			fetchCampaignParentByDBMCampaignCodeSet(setParentCampaign);

		// Validation
		for (Campaign campaign : lstNewCampaign)
		{
			system.debug(LoggingLevel.INFO, '*** Validating campaign...');

			this.hasError = false;
			Boolean isSingleCampaign = (campaign.RecordTypeId == mapRecordTypes.get('SingleCampaign'));

			if (isSingleCampaign)
			{
				// ID_BIR__c
				this.validateID_BIR(campaign, mapDealerByBIRNumberSet);

				// DBMCampaignCode__c
				this.validateDBMCampaignCode(campaign, mapRecordTypes, mapParentCampaignByDBMCampaignCodeSet);
				
				// Vehicle__c is optional, no validation required
				
				// DealerInclusionAllowed__c
				this.validateDealerInclusionAllowed(campaign);
				
				// ManualInclusionAllowed__c
				this.validateManualInclusionAllowed(campaign);
			
				// DBMCampaignCode__c generation
				campaign.DBMCampaignCode__c = campaign.DBMCampaignCode__c + '-' + campaign.ID_BIR__c;
			}
			
			if (!this.hasError)
				lstNewValidCampaign.add(campaign);
		}
		return lstNewValidCampaign;
	}
	
	/**
	 * Validade data when it comes from the UI (manually)
	 */
	public List<Campaign> validadeCampaignForDataEnteredManually(List<Campaign> lstNewCampaign)
	{
		system.debug(LoggingLevel.INFO, '*** VFC123_CampaignValidation.validadeCampaignForDataEnteredManually()');
		
		List<Campaign> lstNewValidCampaign = new List<Campaign>();
		/*
		Set<String> setDealer = new Set<String>();
		Set<Id> setParent = new Set<Id>();
		
		// Get all recordTypes Ids necessary, where the key is the developer name
		Map<String, Id> mapRecordTypes = VFC08_RecordTypeDAO.getInstance().
			getRecordTypeIdByDevNameSet(new Set<String>{'ParentCampaign', 'SingleCampaign'});

		// Build all sets to retrieve data
		for (Campaign campaign : lstNewCampaign)
		{
			setDealer.add(campaign.Dealer__c);
			setParent.add(campaign.ParentId);
		}
		
		// Fetch a map of AccountId, where the key is the BIR number
		Map<Id, Account> mapDealerByIdSet = VFC12_AccountDAO.getInstance().
			returnMapWithIdAndAccount(setDealer);
		
		// Fetch a map of CampaignId, where the key is DBMCampaignCode__c
		Map<Id, Campaign> mapParentCampaignByIdSet = VFC121_CampaignDAO.getInstance().
			fetchCampaignByIdSet(setParent);

		// Validation		
		//VFC123_CampaignValidation campaignValidation = new VFC123_CampaignValidation();
		for (Campaign campaign : lstNewCampaign)
		{
			this.hasError = false;
			Boolean isSingleCampaign = (campaign.RecordTypeId == mapRecordTypes.get('SingleCampaign'));
			
			if (isSingleCampaign)
			{
				// Dealer__c
				this.validateDealer(campaign, mapDealerByIdSet);
				
				// ParentId
				this.validateParentId(campaign, mapRecordTypes, mapParentCampaignByIdSet);

				// Vehicle__c is optional, no validation required
				
				// DealerInclusionAllowed__c
				this.validateDealerInclusionAllowed(campaign);
				
				// ManualInclusionAllowed__c
				this.validateManualInclusionAllowed(campaign);
			
				// DBMCampaignCode__c generation
				campaign.DBMCampaignCode__c = campaign.DBMCampaignCode__c + '-'+ campaign.ID_BIR__c;
			}
			
			if (!this.hasError)
				lstNewValidCampaign.add(campaign);
		}
		*/
		return lstNewValidCampaign;
	}
	
	/**
	 * Validate field ID_BIR__c
	 */
	private void validateID_BIR(Campaign campaign, Map<String, Id> mapDealerByBIRNumberSet)
	{
		if (!this.hasError && (campaign.ID_BIR__c == null || campaign.ID_BIR__c == ''))
		{
			system.debug(LoggingLevel.INFO, '*** ID_BIR__c is null or blank)');
			this.hasError = true;
			campaign.addError('Campaign[SFERROR3]: Não foi possível localizar a concessionária informada.');
		}
		else
		{
			Id dealerId = mapDealerByBIRNumberSet.get(campaign.ID_BIR__c);
			if (!this.hasError && dealerId == null)
			{
				system.debug(LoggingLevel.INFO, '*** dealerId not found');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR3]: Não foi possível localizar a concessionária informada.');
			}
			else
			{
				system.debug(LoggingLevel.INFO, '*** found dealerId='+dealerId);
				// Associate the Id of the dealer
				campaign.Dealer__c = dealerId;
			}
		}
	}
	
	/**
	 * Validate field DBMCampaignCode__c
	 */
	private void validateDBMCampaignCode(Campaign campaign, Map<String, Id> mapRecordTypes, 
		Map<String, Campaign> mapParentCampaignByDBMCampaignCodeSet)
	{
		if (!this.hasError && (campaign.DBMCampaignCode__c == null || campaign.DBMCampaignCode__c == ''))
		{
			system.debug(LoggingLevel.INFO, '*** DBMCampaignCode__c is null or blank)');
			this.hasError = true;
			campaign.addError('Campaign[SFERROR1]: Não foi possível localizar a Campanha Pai informada.');
		}
		else
		{
			Campaign parentCampaign = mapParentCampaignByDBMCampaignCodeSet.get(campaign.DBMCampaignCode__c);
			if (!this.hasError && parentCampaign == null)
			{
				system.debug(LoggingLevel.INFO, '*** parentCampaignId not found');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR1]: Não foi possível localizar a Campanha Pai informada.');
			}
			else if (!this.hasError && parentCampaign.RecordTypeId != mapRecordTypes.get('ParentCampaign'))
			{
				system.debug(LoggingLevel.INFO, '*** parentCampaign.recordTypeId != ParentCampaign');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR2]: Tipo de campanha inválida.');
			}
			else if (!this.hasError)
			{
				system.debug(LoggingLevel.INFO, '*** ParentId= '+parentCampaign.Id);
				// Associate the Id of the dealer
				campaign.ParentId = parentCampaign.Id;
			}
		}
	}
	
	/**
	 * Validate field DealerInclusionAllowed__c
	 */
	private void validateDealerInclusionAllowed(Campaign campaign)
	{
		// If not specified, use false
		if (campaign.DealerInclusionAllowed__c == null)
		{
			campaign.DealerInclusionAllowed__c = false;
		}
	}
	
	/**
	 * Validate field ManualInclusionAllowed__c
	 */
	private void validateManualInclusionAllowed(Campaign campaign)
	{
		// If not specified, use false
		if (campaign.ManualInclusionAllowed__c == null)
		{
			campaign.ManualInclusionAllowed__c = false;
		}
	}
    
    /* Retirado por Edvaldo - {kolekto} por não ser utilizado em nenhuma classe 

	/**
	 * Validate field Dealer__c
	 *
	public void validateDealer(Campaign campaign, Map<Id, Account> mapDealerByIdSet)
	{
		if (!this.hasError && campaign.Dealer__c == null)
		{
			system.debug(LoggingLevel.INFO, '*** Dealer__c is null or blank)');
			this.hasError = true;
			campaign.addError('Campaign[SFERROR3]: Não foi possível localizar a concessionária informada.');
		}
		else
		{
			Account dealer = mapDealerByIdSet.get(campaign.Dealer__c);
			if (!this.hasError && dealer == null)
			{
				system.debug(LoggingLevel.INFO, '*** Dealer__c not found');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR3]: Não foi possível localizar a concessionária informada.');
			}
			else
			{
				system.debug(LoggingLevel.INFO, '*** dealer.Id='+dealer.Id);
				// Associate the BIR number
				campaign.ID_BIR__c = dealer.IDBIR__c;
			}
		}
	}
	
	/**
	 * Validate field ParentId
	 *
	public void validateParentId(Campaign campaign, Map<String, Id> mapRecordTypes, 
		Map<Id, Campaign> mapParentCampaignByIdSet)
	{
		if (!this.hasError && campaign.ParentId == null)
		{
			system.debug(LoggingLevel.INFO, '*** ParentId is null or blank)');
			this.hasError = true;
			campaign.addError('Campaign[SFERROR1]: Não foi possível localizar a Campanha Pai informada.');
		}
		else
		{
			Campaign parentCampaign = mapParentCampaignByIdSet.get(campaign.ParentId);
			if (!this.hasError && parentCampaign == null)
			{
				system.debug(LoggingLevel.INFO, '*** ParentId not found');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR1]: Não foi possível localizar a Campanha Pai informada.');
			}
			else if (!this.hasError && parentCampaign.recordTypeId != mapRecordTypes.get('ParentCampaign'))
			{
				system.debug(LoggingLevel.INFO, '*** parentCampaign.recordTypeId != ParentCampaign');
				this.hasError = true;
				campaign.addError('Campaign[SFERROR2]: Tipo de campanha inválida.');
			}
			else
			{
				system.debug(LoggingLevel.INFO, '*** parentCampaign.DBMCampaignCode__c='+parentCampaign.DBMCampaignCode__c);
				// Add the first part of the code
				campaign.DBMCampaignCode__c = parentCampaign.DBMCampaignCode__c;
			}
		}
	}
    
    */
}