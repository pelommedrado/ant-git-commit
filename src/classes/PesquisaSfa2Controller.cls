public without sharing class PesquisaSfa2Controller {

    public Boolean showForm { get;set; }

    public Account acc { get;set; }
    public Contact cnt { get;set; }
    public Opportunity opp { get;set; }
    public Decimal diasRetorno { get; set; }
    public String companyName { get;set; }
    public String oppRecTypeVendaDireta {get;set;}

    public Boolean isReceptionist { get;set; }
    public Boolean isSeller { get;set; }

    public Id leadId { get;set; }

    public Id dvrRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','DVR');
        }
    }

    public Id dveRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','DVE');
        }
    }

    public Id smnRtId{
        get{
            return Utils.getRecordTypeId('Opportunity','Seminovos');
        }
    }

    /** Veiculo selecionado a partir do estoque **/
    public VRE_VehRel__c veiculoRel { get; set; }
    /** Texto de pesquisa padrao **/
    public String textSearch { get;set; }

    public String contaOpp { get;set; }

    public String nomeUser {
        get { return Userinfo.getName(); }
    }
    public String fieldMapping {
        get { return Json.Serialize(PesquisaSfa2Controller.returnFieldMap);}
    }
    public Id campaignId {
        get { return this.opp.CampaignId; }
        set { this.opp.CampaignId = value; }
    }

    public List<SelectOption> campaignSelectList {
        get {
            Id accDealerId;
            try {
                accDealerId = [
                    SELECT Contact.AccountId
                    FROM User WHERE Id =: Userinfo.getUserId()
                ].Contact.AccountId;

            } catch(Exception ex) {
                System.debug('Ex:' + ex);
                accDealerId = null;
            }
            return VFC120_CampaignService.fetchCampaignList('Dealer', null, null, accDealerId);
        }
    }
    public Id sellerId {
        get { return this.opp.OwnerId; }
        set { this.opp.OwnerId = value; }
    }
    public String sellerName {
        get {
            return sellerId != null ? [
                SELECT Name FROM User WHERE Id =: sellerId LIMIT 1
            ].Name : '';
        }
    }
    public String accountPrefix {
        get { return Account.SObjectType.getDescribe().getKeyPrefix(); }
    }

    /*
     * by @Lucas Andrade - [ kolekto ] - 27/09/2016
     * Nova funcionalidade de visão GRUPO na criação de novo passante por recepcionista
     */
    public String dealerBIR {get;set;}

    public Boolean isExecutive {
        get {
            List< PermissionSetAssignment > psets = [
                SELECT AssigneeId FROM PermissionSetAssignment
                WHERE PermissionSetId IN (
                    SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Dealer_Executive'
                )];

            Set < Id > dealers = new Set < Id >();
            for(PermissionSetAssignment p : psets) dealers.add(p.AssigneeId);
            if(dealers.contains(UserInfo.getUserId())){
                return true;
            } else {
                return false;
            }
        }
    }

    public List< SelectOption > dealerSelectList {
        get {
            User u = [SELECT Id, Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId
                      FROM User WHERE Id =: UserInfo.getUserId()];

            List< Account > accs = [
                SELECT Id, Name, IDBIR__c FROM Account
                WHERE RecordTypeId =: Utils.getRecordTypeId('Account', 'Network_Site_Acc')
                AND isDealerActive__c = true
                AND ParentId =: u.Contact.Account.ParentId ORDER BY Name ASC
            ];
            List< SelectOption > dealerSelectList = new List< SelectOption >();
            dealerSelectList.add( new SelectOption( '', '--' + Label.OPPNone + '--' ) );
            for(Account a : accs){
                dealerSelectList.add( new SelectOption(a.IDBIR__c, a.Name) );
            }

            return dealerSelectList;
        }
    }

    /*
     * Fim de alterações por @Lucas Andrade
     */
    public List<SelectOption> sellerSelectList {
        get {
            List< PermissionSetAssignment > psets = [
                SELECT AssigneeId FROM PermissionSetAssignment
                WHERE PermissionSetId IN (
                    SELECT Id FROM PermissionSet
                    WHERE (Name = 'BR_SFA_Seller' OR Name = 'BR_SFA_Hostess' OR Name = 'BR_SFA_Seller_Agenda')
                )];
            Set < Id > sellers = new Set < Id >();
            for ( PermissionSetAssignment p : psets ) sellers.add(p.AssigneeId);

            List< Contact > contactList = VFC29_ContactDAO.getInstance().fetchContactRecordsUsingCriteria();
            List< Id > contactIdList = new List< Id >();
            for( Contact c : contactList ) contactIdList.add( c.Id );

            List< User > userList = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId( contactIdList );

            String BIR;

            if(isExecutive && this.dealerBIR != null){
                BIR = this.dealerBIR;
            } else {
                BIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).Contact.Account.IDBIR__c;
            }

            List<SelectOption> sellerSelectList = new List<SelectOption>();
            sellerSelectList.add(new SelectOption('', '--' + Label.OPPNone + '--' ));
            for(User u : userList) {
                if( sellers.contains(u.Id) && u.Contact.Account.IDBIR__c == BIR ){
                    sellerSelectList.add(new SelectOption(u.Id, u.Name));
                }
            }
            return sellerSelectList;
        }
    }

    /**
     * Construtor da classe.
     */
    public PesquisaSfa2Controller() {
        //recuperar parametros da pagina
        String veiculoId 	= ApexPages.currentPage().getParameters().get('veh');
        String entrada 		= ApexPages.currentPage().getParameters().get('entrada');
        String conta 		= ApexPages.currentPage().getParameters().get('conta');


        System.debug('### Veiculo:' + veiculoId);
        System.debug('### Entrada:' + entrada);

        this.textSearch = entrada;
        this.isReceptionist = false;
        this.isSeller = false;
        this.showForm = false;

        configPerfil();
        clearObjects();
        setCustomerInformation();

        this.veiculoRel = Sfa2Utils.obterVeiculo(veiculoId);
        if(this.veiculoRel != null) {
            //modelo do veiculo
            String model = this.veiculoRel.VIN__r.Model__c;
            //configurar o veiculo de interesse da oportunidade
            this.opp.Vehicle_Of_Interest__c = model.split(' ')[0];

            List<VehicleBooking__c> listVehicleBooking = [
                SELECT Id, ClientName__c, ClientSurname__c,
                CustomerIdentificationNbr__c, HomePhone__c, ClientEmail__c,
                MobilePhone__c
                FROM VehicleBooking__c
                WHERE Vehicle__c =: this.veiculoRel.VIN__c
                AND Status__c = 'Active' AND Vehicle__r.Status__c = 'Booking'
            ];

            if(!listVehicleBooking.isEmpty()) {
                VehicleBooking__c vbk = listVehicleBooking.get(0);
                this.cnt.FirstName = vbk.ClientName__c;
                this.cnt.LastName = vbk.ClientSurname__c;

                this.acc.PersLandline__c = vbk.HomePhone__c;
                this.acc.PersEmailAddress__c = vbk.ClientEmail__c;
                this.acc.PersMobPhone__c = vbk.MobilePhone__c;
                this.acc.CustomerIdentificationNbr__c = vbk.CustomerIdentificationNbr__c;

                this.showForm = true;
            }
        }

        if(conta != null) {
            contaOpp = conta;
        }

        Account accComun = Sfa2Utils.obterContaUserComunidade();
        this.diasRetorno = accComun.ReturnPeriod__c;
        this.dealerBIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).Contact.Account.IDBIR__c;
    }

    private void configPerfil() {
        String profileName = [
            SELECT Name
            FROM Profile
            WHERE Id =: Userinfo.getProfileId()
            LIMIT 1
        ].Name;

        Set<String> permissionByUserList =
            new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());

        if(profileName == 'SFA - Receptionist'  ||
           (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess'))) {
               isReceptionist = true;

           }

        if(profileName == 'SFA - Seller' ||
           (!permissionByUserList.isEmpty() &&
            (permissionByUserList.contains('BR_SFA_Seller') || (permissionByUserList.contains('BR_SFA_Seller_Agenda'))))) {
               isSeller = true;
           }

        System.assert( isReceptionist || isSeller, Label.VFP20_Unauthorized_access);
    }

    /**
     * Solicitar acesso a oportunidade
     */
    public PageReference solicitarAcessoOportunidade() {
        System.debug('Solicitando acesso a oportunidade');
        String oppId = Apexpages.currentPage().getParameters().get('oppId');
        System.debug('OportunidadeId:' + oppId);

        Opportunity opp = [
            SELECT Id, Name, Ownership_requested_by_ID__c, OwnerId
            FROM Opportunity
            WHERE Id =: oppId];

        if(opp.Ownership_requested_by_ID__c != null) {
            User user = [
                SELECT Id, LastName, FirstName
                FROM User
                WHERE Id =: opp.Ownership_requested_by_ID__c
            ];
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.WARNING,
                'Existe uma solicitação de acesso em andamento para ' + opp.Name +
                ' efetuada por ' + user.LastName + ' ' + user.FirstName));
            return null;
        }

        opp.Ownership_requested_by_ID__c = Userinfo.getUserId();

        try {
            update opp;

            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.CONFIRM,
                'Sua solicitação de acesso a ' + opp.Name +  ' foi enviado com sucesso.'));

        } catch(Exception ex) {
            System.debug('Ex: ' + ex);

            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR,
                ex.getMessage()));
        }

        return null;
    }

    public void clearObjects() {
        acc = new Account();
        cnt = new Contact();
        opp = new Opportunity();

        companyName = null;
    	oppRecTypeVendaDireta = null;

        sellerId = Userinfo.getUserId();
    }

    private Boolean isValido() {
        if(String.isBlank(acc.PersEmailAddress__c) 	&&
           String.isBlank(acc.PersMobPhone__c ) 	&&
           String.isBlank(acc.PersLandline__c ) ) {
               Apexpages.addMessage(new Apexpages.Message(
                   ApexPages.Severity.ERROR,
                   Label.ERRPageErrorMessage 	+ ' ' +
                   Label.ACCEmail 				+ ' ' +
                   Label.UTIL_OR 				+ ' ' +
                   Label.ACCHomePhone 			+ ' ' +
                   Label.UTIL_OR 				+ ' ' +
                   Label.LDDCellular ));

               return null;
           }

        if(String.isNotBlank(acc.CustomerIdentificationNbr__c) && acc.CustomerIdentificationNbr__c.length() == 11) {
            if(!Sfa2Utils.isCpfValido(acc.CustomerIdentificationNbr__c) ) {
                Apexpages.addMessage(new Apexpages.Message(
                    ApexPages.Severity.ERROR, 'CPF Inválido!'));
                return null;
            }

        } /*else {
            if(!String.isEmpty(opp.SourceMedia__c) &&
               !opp.SourceMedia__c.equalsIgnoreCase('Telephone')) {
                   Apexpages.addMessage(new Apexpages.Message(
                       ApexPages.Severity.ERROR, 'O campo CPF é obrigatório!'));
                   return null;
               }
        }*/

        if(String.isBlank(opp.RecordTypeId) ) {
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR, 'Preencha o tipo de negociação!'));

            return null;
        }

        if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE') &&
           String.isBlank(oppRecTypeVendaDireta) && String.isBlank(acc.Type_DVE__c)){
               Apexpages.addMessage(new Apexpages.Message(
                   ApexPages.Severity.ERROR,
                   'Preencha os dados do cliente na seção de Vendas Especiais.'));
               return null;
           }

        if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE') &&
           oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa') &&
           String.isBlank(companyName)){
               Apexpages.addMessage(new Apexpages.Message(
                   ApexPages.Severity.ERROR,
                   'Preencha o nome da empresa!'));
               return null;
           }

        System.debug('### Oportunidade opp:' + opp);
        if(String.isEmpty(opp.Vehicle_Of_Interest__c) || String.isEmpty(opp.SourceMedia__c) ) {
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR,
                'Por favor informe todos os campos obrigarórios'));
            return null;
        }
        return true;
    }

    public Pagereference save() {
        System.debug('Tipo de registro da OPP: ' + opp.RecordTypeId);
        System.debug('Tipo de registro da OPP ESPERADO: ' + Utils.getRecordTypeId('Opportunity', 'DVE'));
        System.debug('Tipo de registro da OPP em VENDA DIRETA: ' + oppRecTypeVendaDireta);
        System.debug('Tipo de registro da OPP em VENDA DIRETA ESPERADO: ' + Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa'));
        System.debug('Company Name: ' + companyName);
        System.debug('Tipo de Cliente VENDA DIRETA: ' + acc.Type_DVE__c);
        System.debug('EMAIL: ' + acc.PersEmailAddress__c);
        System.debug('PHONE: ' + acc.PersMobPhone__c);
        sYSTEM.debug('HOME PHONE: ' + acc.PersLandline__c);

        if(isValido() == null) {
            return null;
        }

        // Begin transaction
        Savepoint sp = Database.setSavepoint();

        try {
            String BIR = [
                SELECT BIR__c FROM User WHERE Id =: sellerId
            ].BIR__c;

            Id dealerId = [
                SELECT Id FROM Account WHERE IDBIR__c =: BIR LIMIT 1
            ].Id;

            /*--Start Code-- Modificado por @Edvaldo - kolekto --*/

            List<Lead> lsLead;
            List<Account> lsAccount;
            List<Contact> lsContact;
            Lead leadOld;
            Lead lead;

            acc.CustomerIdentificationNbr__c =
                Sfa2Utils.limparFormatacaoCpfCnpj(acc.CustomerIdentificationNbr__c);
            //check if there's an account with CPF equals the CPF field of form.
            if(acc.CustomerIdentificationNbr__c != null) {
                lsAccount = [
                    SELECT Id
                    FROM Account
                    WHERE CustomerIdentificationNbr__c =: acc.CustomerIdentificationNbr__c
                    ORDER BY CreatedDate ASC
                ];
            }

            //check if there's leads that have CPF equals the CPF field of form.
            if(acc.CustomerIdentificationNbr__c != null) {
                lsLead = [
                    SELECT Id, IsConverted, CPF_CNPJ__c, FirstName, LastName,
                    LeadSource, SubSource__c, Detail__c, Sub_Detail__c, CreatedDate, RecordTypeId
                    FROM lead
                    WHERE CPF_CNPJ__c =: acc.CustomerIdentificationNbr__c AND RecordTypeId !=: utils.getRecordTypeId('Lead','Voucher')
                    ORDER BY CreatedDate ASC];
            }

            //get first lead inserted in DataBase.
            if(lsLead != null && !lsLead.isEmpty()) {
                leadOld = lsLead.get(0);
                leadOld = VFC09_LeadDAO.getInstance().findLeadByLeadId(leadOld.Id);
            }

            //update the Lead with contact information of SFA
            if(leadOld != null && leadOld.IsConverted == false){
                updateLeadFromSFA(leadOld);
            }

            // insert  Account informations.
            //acc.FirstName 				= cnt.FirstName;
            //acc.LastName 				= cnt.LastName;
            acc.VehicleInterest_BR__c 	= opp.Vehicle_Of_Interest__c;

            if(!String.isBlank(acc.RecordTypeId)) {

                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'DVE')){
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                }
            }

            //insert or update account
            if(lsAccount != null && !lsAccount.isEmpty()) {
                acc.Id = lsAccount.get(0).Id;

                if(lsAccount.get(0).RecordTypeId == Utils.getRecordTypeId('Account', 'Company_Acc')){
                    acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    acc.Phone = acc.PersLandline__c;
                    acc.Cliente_DVE__c = true;

                    cnt.PersEmail__c = acc.PersEmailAddress__c;
                    cnt.HomePhone = acc.PersLandline__c;
                    cnt.PersMobPhone__c = acc.PersMobPhone__c;

                    if(companyName != null){
                        acc.Name = companyName;
                    }

                    //check if there's an contact in company account with this name
                    lsContact = [SELECT Id, FirstName, LastName
                                FROM Contact
                                WHERE FirstName = :cnt.FirstName
                                AND LastName = :cnt.LastName
                                ORDER BY CreatedDate ASC];

                    //update or insert the contact within existing account
                    if(!lsContact.isEmpty() && lsContact != null){
                        cnt.Id = lsContact[0].Id;
                    }

                }

                if(leadOld != null && leadOld.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))){
                    acc.Cliente_DVE__c = true;

                    if(leadOld.Type_DVE__c != null && !leadOld.Type_DVE__c.equals('')) {
                        acc.Type_DVE__c = leadOld.Type_DVE__c;
                    }
                }



                Database.update(acc);
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.upsert(cnt);
                }
            }

            if( (lsAccount == null || lsAccount.isEmpty()) && leadOld != null) {

                if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')){

                    acc.Cliente_DVE__c = true;

                    if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){

                        cnt.RecordTypeId = Utils.getRecordTypeId('Contact', 'Network_Ctc');
                        cnt.PersEmail__c = acc.PersEmailAddress__c;
                        cnt.HomePhone = acc.PersLandline__c;
                        cnt.PersMobPhone__c = acc.PersMobPhone__c;

                        acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    	acc.Phone = acc.PersLandline__c;
                    	acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                        acc.Name = companyName;

                    } else{
                        acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                        acc.FirstName = cnt.FirstName;
            			acc.LastName = cnt.LastName;
                    }

                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                    acc.FirstName = cnt.FirstName;
            		acc.LastName = cnt.LastName;
                }

                acc.Source__c 		= leadOld.LeadSource;
                acc.AccSubSource__c = leadOld.SubSource__c;
                acc.Detail__c 		= leadOld.Detail__c;
                acc.Sub_Detail__c 	= leadOld.Sub_Detail__c;

                Database.Upsert( acc );
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.insert(cnt);
                }
            }

            if((lsAccount == null || lsAccount.isEmpty()) && leadOld == null){

                if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')){

                    acc.Cliente_DVE__c = true;

                    if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){

                        cnt.RecordTypeId = Utils.getRecordTypeId('Contact', 'Network_Ctc');
                        cnt.PersEmail__c = acc.PersEmailAddress__c;
                    	cnt.HomePhone = acc.PersLandline__c;
                    	cnt.PersMobPhone__c = acc.PersMobPhone__c;

                    	acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
                        acc.Name = companyName;
                        acc.ProfEmailAddress__c = acc.PersEmailAddress__c;
                    	acc.Phone = acc.PersLandline__c;



                    } else{
                        acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                        acc.FirstName = cnt.FirstName;
            			acc.LastName = cnt.LastName;
                    }

                } else{
                    acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
                    acc.FirstName = cnt.FirstName;
            		acc.LastName = cnt.LastName;
                }

                acc.Source__c 		= 'DEALER';
                acc.AccSubSource__c = 'PASSING';
                acc.Detail__c 		= 'NEW VEHICLE';

                Database.Upsert( acc );
                if(oppRecTypeVendaDireta == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')){
                    cnt.AccountId = acc.Id;
                	Database.insert(cnt);
                }
            }

            //get lead that is entering
            if(leadId != null) {
                lead = VFC09_LeadDAO.getInstance().findLeadByLeadId(leadId);
            }

            //convert lead that was created before the lead that is entering.
            if(LeadOld != null && LeadOld.IsConverted == false){
                convertLead(acc.Id, leadOld);
            }

            //convert the new lead
            if(lead != null && leadOld != NULL && lead.IsConverted == false && lead.Id != leadOld.Id) {
                convertLead(acc.Id, lead);
            }

            //insert opportunity
            if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'DVE')){
                opp.RecordTypeId = oppRecTypeVendaDireta;
            }

            System.debug('RECORD TYPE DA OPP ANTES DE INSERIR: ' + opp.RecordType.Name);
            insertOpportunity(dealerId);

            /*--Finish Code-- Modificado por @Edvaldo - kolekto --*/

            //coloca o Lead em quarentena - by @Felipe Livino - kolekto
            if(leadId != null) {
                retailPlataform(leadId);
            }

        } catch(Exception e) {
            Database.rollback(sp);

            system.debug('***ERRO: ' + e);

            String message = e.getMessage();

            if(message.contains('Cannot specify any additional fields when marrying or separating a Person-Account')){
                Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR, 'Não é possível alterar o tipo desta conta. Por favor, crie uma nova.'));

            } else {

            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR,
                message.contains( '_EXCEPTION, ' ) ? message.substringAfter( '_EXCEPTION, ' ) :
                message.contains( 'DUPLICATE_VALUE' ) && message.contains( 'CustomerIdentificationNbr__c' ) ?
                Label.VFP23_NewOpportunity_DuplicateId : message));
            }

            return null;
        }

        if( isSeller ) {
            Pagereference newPage;

            if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Seminovos')
                || opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')
                || opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa_PF')){

                newPage = new PageReference('/' + opp.Id);

            }else{

                newPage = Page.ClienteOportunidadeSfa2;
                newPage.getParameters().put( 'Id', opp.Id );

                if(veiculoRel != null) {
                    newPage.getParameters().put('veh', veiculoRel.Id);
                }
        }

            return newPage;
        }

        Apexpages.addMessage(new Apexpages.Message(
            ApexPages.Severity.Confirm,
            'Negociação encaminhada para ' + sellerName ));

        clearObjects();

        return null;
    }

    @RemoteAction
    public static Opportunity[] verificarRetornoOpp(String cpf) {
        System.debug('### verificando cpf: ' + cpf);

        if(String.isEmpty(cpf)) {
            return new Opportunity[0];
        }

        Account accComun = Sfa2Utils.obterContaUserComunidade();

        /*if(!accComun.ReturnActiveToSFA__c) {
           return new Opportunity[0];
        }*/

        /*Account acc = [
            SELECT Id
            FROM Account
            WHERE CustomerIdentificationNbr__c =: cpf
            LIMIT 1
        ];

        if(acc == null) {
           return new Opportunity[0];
        }*/

        Account[] acc = [
            SELECT Id
            FROM Account
            WHERE CustomerIdentificationNbr__c =: cpf
        ];

        if(acc.size() == 0) {
           return new Opportunity[0];
        }

        List<Opportunity> oppList = [
            SELECT Id
            FROM Opportunity
            WHERE AccountId =: acc[0].Id
            AND Dealer__c =: accComun.Id
            AND StageName IN ('Identified', 'Test Drive', 'Quote', 'Lost')
            ORDER BY CreatedDate DESC
            LIMIT 20
        ];

        OppHistoricoRetorno oppHist = new OppHistoricoRetorno();
        List<Id> oppHisList = oppHist.tratar(oppList);

        List<Opportunity> nOppLi = [
            SELECT Id, Name, toLabel( StageName ), CreatedDate, CloseDate, OpportunitySource__c, OpportunitySubSource__c, Campaign_Name__c, Owner.Name
            FROM Opportunity
            WHERE Id=: oppHisList
        ];

        return nOppLi;
    }

    /*--Start Code-- Métodos criados por @Edvaldo - kolekto --*/

    //--Start Code--  modified by Felipe Livino {kolekto} || Retail Plataform
    public void retailPlataform(Id leadId){
        List<Quarantine__c> listaQuarentena = [
            SELECT id
            FROM Quarantine__c
            WHERE Lead__c =: leadId
        ];

        System.debug('### listaQuarentena ' + listaQuarentena);

        List<Quarantine__c> listaUpdateQuarentena = new List<Quarantine__c>();
        for(Quarantine__c quarentena : listaQuarentena){
            quarentena.Account__c = acc.Id;
            quarentena.Opportunity__c = opp.id;

            listaUpdateQuarentena.add(quarentena);
        }

        Database.update (listaUpdateQuarentena);

        System.debug('###### listaUpdateQuarentena '+listaUpdateQuarentena);
    }

    //--Finish Code--  modified by Felipe Livino {kolekto} || Retail Plataform

    public void insertOpportunity(Id dealerId){
        if ( opp.Vehicle_Of_Interest__c != null ){
            String vehicle = opp.Vehicle_Of_Interest__c;

            List< Product2 > product = VFC22_ProductDAO.getInstance().findProductRecordsbyName( vehicle );

            if (!product.isEmpty()){
                PricebookEntry pbEntry = VFC24_PriceBookDAO.getInstance().findPricebyProductId( product[0].Id );
                if ( pbEntry != null )
                    opp.Amount = pbEntry.UnitPrice;
            }
        }
        opp.Name = Label.OPPName + ' ' + '(' + cnt.FirstName + ' ' + cnt.LastName + ')';
        opp.AccountId = acc.Id;
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today() + 30;
        opp.OpportunitySource__c = 'DEALER';
        opp.OpportunitySubSource__c = 'PASSING';
        opp.Detail__c = 'NEW VEHICLE';
        opp.Dealer__c = dealerId;

        if(opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa')
            || opp.RecordTypeId == Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa_PF')){

                opp.StageName = 'Lost';
                opp.ReasonLossCancellation__c = 'Forward to DVE';

        }

        // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        opp.DateTimeSellerUpdatedStageOpp__c = System.now();

        Database.insert( opp );
    }

    public void updateLeadFromSFA(Lead l) {
        l.HomePhone__c 		= acc.PersLandline__c;
        l.Email 			= acc.PersEmailAddress__c;
        l.MobilePhone 		= acc.PersMobPhone__c;
        l.OwnerId 			= userinfo.getUserId();
        l.SFAConverted__c 	= true;

        Database.update(l);

        system.debug('***** Lead= '+l);
    }
    public void convertLead(Id accountId, Lead l){
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity(true);
        lc.setLeadId(l.Id);

        if(l.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))) {
            lc.setConvertedStatus('Qualified');

        } else {
            lc.setConvertedStatus('Hot Lead');

        }


        //if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (l.Owner.type == 'Queue') {
            lc.setOwnerId( Userinfo.getUserId() );
        }

        Database.Leadconvertresult successLeadConversion = Database.convertLead(lc);

        Id oppId = successLeadConversion.getOpportunityId();
    }

    /*--Finish Code-- Métodos criados por @Edvaldo - kolekto --*/

    public void selectAccount() {
        List<String> lt = PesquisaSfa2Controller.returnFieldMap.get('Account');
        List<String> lt1= PesquisaSfa2Controller.searchFieldMap.get('Account');

        Set<String> accFieldSet = new Set<String>(lt);
        accFieldSet.addAll(lt1);

        acc = Database.query('select ' +
                             String.join( new List<String>(accFieldSet), ', ' ) +
                             ', IsPersonAccount, PersEmailAddress__c, Type_DVE__c from Account where Id = \''
                             + acc.Id + '\' limit 1' );

        System.debug('Conta: ' + acc);

        cnt.FirstName = acc.FirstName;
        cnt.LastName = acc.LastName;
        sellerId = Userinfo.getUserId();

        system.debug('sellerId: ' + sellerId);
    }

    public void selectLead() {
        Lead baseLead = [
            SELECT Id, FirstName, LastName, LeadSource, SubSource__c, Detail__c, Sub_Detail__c
            FROM Lead
            WHERE Id =: leadId
        ];

        cnt.FirstName = baseLead.FirstName;
        cnt.LastName = baseLead.LastName;
        sellerId = Userinfo.getUserId();
    }

    private class QueryWrapper {
        public String SObjectName { get;set; }
        public String queryWhere { get;set; }
        private List<String> returnFieldList;
        private String searchStr;
        public String queryStr {
            get {
                return 'find \'' + this.searchStr + '\' in all fields returning ' +
                    this.SObjectName + '(Id, ' + String.join( returnFieldList, ', ' )  +
                    (queryWhere != null ? queryWhere : '') +
                    ' ORDER BY CreatedDate Desc limit 20)';
            }
        }

        public QueryWrapper(String searchStr, String SObjectName) {
            this.searchStr = searchStr;
            this.SObjectName = SObjectName;
            List<String> lt = PesquisaSfa2Controller.returnFieldMap.get(this.SObjectName);
            this.returnFieldList = lt;

            System.assert( returnFieldList != null );
        }
    }

    @RemoteAction
    public static SObject[] queryObject(String searchStr, String SObjectName){
        QueryWrapper queryObj = new QueryWrapper( searchStr, SObjectName );

        //a pesquisa e de Oportunidade?
        if(SObjectName.equals('Opportunity')) {
            //codigo da concessionaria
            String bir = VFC25_UserDAO.getInstance().fetchUserByRecordId(
                Userinfo.getUserId()).BIR__c;
            //obter os usuarios dos vendedores da concessionaria
            List<User> vends = Sfa2Utils.obterVendedoresConcessionaria(bir);
            System.debug('### Vendedores Concessionaria: ' + vends);

            queryObj.queryWhere = montarQueryInUser(vends);
        }

        System.debug('###########' + queryObj.queryStr);

        return Search.query(queryObj.queryStr)[0];
    }

    /**
     * Montar query In com a lista de users
     */
    private static String montarQueryInUser(List<User> userList) {
        String inStg = ' WHERE OwnerId IN (';
        for(User us : userList) {
            inStg += '\'' + us.id +
                //e o ultimo elemento da lista?
                (userList.get(userList.size()-1).Id == us.Id ? '\')' : '\',');
        }

        return inStg;
    }
    public String cmId{get;set;}
    public void setCustomerInformation(){
        cmId = ApexPages.currentPage().getParameters().get('cmId');

        if(cmId != null){
            CampaignMember cm = [SELECT Id, Name, Lead.CPF_CNPJ__c
                             	FROM CampaignMember
                             	WHERE Id =: cmId];


            cnt.FirstName = cm.Name.split(' ')[0];
            cnt.LastName = cm.Name.split(' ', 2)[1];
            acc.CustomerIdentificationNbr__c = cm.Lead.CPF_CNPJ__c;
        }
    }

   public static final Map<String, List<String>> searchFieldMap = new Map<String, List<String>> {
        'Lead' => new List<String>
        {'Email','FirstName','LastName','Phone','BusinessPhone__c',
            'CPF_CNPJ__c','HomePhone__c', 'Mobile_Phone__c'},
        'Account' => new List<String>
        {'CustomerIdentificationNbr__c','FirstName','LastName', 'HOMEPHONE__c',
            'Phone','PersMobPhone__c','PersLandline__c','ProfLandline__c', 'Email__c','PersonEmail'},
        'Opportunity' => new List<String> { 'Name' },
        'Case' => new List< String >{ 'CaseNumber' }
   };

   public static final Map<String, List<String>> returnFieldMap = new Map<String, List<String>> {
   		'Lead' => new List<String>
        { 'FirstName', 'LastName', 'CreatedDate', 'toLabel( LeadSource )', 'SubSource__c',
            'VehicleOfInterestLookup__r.Name WHERE isConverted = false'},
        'Account' => new List<String>
        { 'FirstName', 'LastName', 'HOMEPHONE__c', 'Phone', 'PersMobPhone__c',
            'ShippingCity', 'ShippingPostalCode'},
        'Opportunity' => new List<String>
        { 'Name', 'toLabel( StageName )', 'CreatedDate', 'CloseDate', 'Vehicle_Of_Interest__c', 'OpportunitySource__c', 
            'OpportunitySubSource__c', 'Campaign_Name__c', 'Owner.Name'},
        'Case' => new List<String>
        { 'CaseNumber', 'CreatedDate', 'toLabel( Type )', 'Subtype__c', 'Subject', 'toLabel( Status )'}
   };

       public class OppHistoricoRetorno extends OportunidadeHistoricoAbstract implements OportunidadeHistoricoAbstract.OportunidadeHistoricoListener {
           public List<Id> oppHisList;
           public List<Id> tratar(List<Opportunity> oppList) {
               oppHisList = new List<Id>();
               for(Opportunity op : oppList) {
                   init(op.Id);

                   verificarOportunidade(this);
               }
               return oppHisList;
           }

           public void oportunidadeExpirou() {
           }
           public void oportunidadeAceita() {
               oppHisList.add(this.opp.Id);
           }
           public void oportunidadeReabrir() {
               oppHisList.add(this.opp.Id);
           }
           public void oportunidadeNaoReabre() {
           }
           public void oportunidadeNaoReabreFaturada() {
           }
       }
}