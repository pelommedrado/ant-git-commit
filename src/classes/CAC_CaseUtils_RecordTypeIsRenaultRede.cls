public class CAC_CaseUtils_RecordTypeIsRenaultRede extends CAC_CaseUtilsMaster{

    public CAC_CaseUtils_RecordTypeIsRenaultRede(){
        super();
    }
    public CAC_CaseUtils_RecordTypeIsRenaultRede(List<Case> caseList, Map<Id,Case>oldCase){
        super(caseList,oldCase);
    }
    
    public void statusAnswersWaiting(){
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer')){
            
            
            Set<Id> setIdCase = new Set<ID>();
            for(Case caseI: lsCase)
                if(caseI.Status.equals('Waiting for reply'))
                setIdCase.add(caseI.Id);

            
            //mapa com os ultimos comentário do caso
            Map<Id,CaseComment> mapCaseCommentId = new Map<Id,CaseComment>();
            for(CaseComment caseCom : [select Id, ParentId,CreatedById,IsPublished  from CaseComment where ParentId in:(setIdCase) and IsPublished = true order by id asc ])
                mapCaseCommentId.put(caseCom.ParentId, caseCom);
            
            
            for(Case caseI: lsCase){
                
                if(caseI.Status.equals('Waiting for reply') && mapCaseCommentId.containsKey(caseI.id)){
                    if(!mapCaseCommentId.get(caseI.id).CreatedById.equals(UserInfo.getUserId())){
                        caseI.addError('Insira um comentário');
                    }
                }else if(caseI.Status.equals('Waiting for reply')){
                    caseI.addError('Insira um comentário');
                }
            }
            
        }
    }
    
    public void statusAnswers(){
        if(userInfos.isCac__c && !String.isEmpty(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer')){
            for(Case caseCac : lsCase){
                
                if(String.isNotBlank(caseCac.Status) && caseCac.Status.equalsIgnoreCase('Answered') && String.isBlank(caseCac.Answer__c))
                    caseCac.addError('Preencher o campo "Responder"');
                
                if(mapOldCase.get(caseCac.id).Answer__c <> caseCac.Answer__c  && !caseCac.Status.equalsIgnoreCase('Answered'))
                    caseCac.addError('O campo status deve ser alterado para Respondido');
                
                if(String.isNotEmpty(mapOldCase.get(caseCac.id).Answer__c)  
                   && !mapOldCase.get(caseCac.id).Answer__c.equals(caseCac.Answer__c) )
                    caseCac.addError('A reposta não pode ser alterada');
            }
            
        }
    }
    
    
    public void updateField(){
	
        Map<String,cacweb_settingTime__c> mapExpired = getExpiredDate();
        
        for(Case caseUp: lsCase){
            

            
            if(String.isNotBlank(caseUp.Status) && caseUp.Status.equals('New')){
                

                
                if(caseUp.OwnerId == UserInfo.getUserId() &&
                   userInfos.isCac__c && 
                   String.isNotEmpty(userInfos.roleInCac__c) && 
                   userInfos.roleInCac__c.equals('Analyst')){
                       if(!String.isEmpty(caseUp.Subject__c) && mapExpired.containsKey(caseUp.Subject__c)){
                           System.debug('#### entrou nos ifs');
                           if(String.isNotBlank(caseUp.Status) && caseUp.Status.equals('New') && !caseUp.Expired__c){
                               caseUp.Expiration_Date__c = corretDayReturn(
                                   Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).hoursExpire__c)),
                                   Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                                   Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                               );
                                System.debug('##### caseUp.Expiration_Date__c '+caseUp.Expiration_Date__c);
                               caseUp.beforeExpiredDate__c = corretDayReturn(
                                   Long.valueOf(String.valueOf(mapExpired.get(caseUp.Subject__c).alertExpire__c)),
                                   Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c),
                                   Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c)
                               );
                               System.debug('##### caseUp.beforeExpiredDate__c '+caseUp.beforeExpiredDate__c);
                           }
                           integer valueStartDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).StartHourAttendance__c);
                           integer valueFinishedDate = Integer.valueOf(mapExpired.get(caseUp.Subject__c).FinishHourAttendance__c);
                           
                           
                           caseUp.CAC_nearAutomaticClosure__c = corretDayReturn(25,
                                                                                valueStartDate,
                                                                                valueFinishedDate);
                           System.debug('##### caseUp.CAC_nearAutomaticClosure__c '+caseUp.CAC_nearAutomaticClosure__c);
                           caseUp.CAC_automaticClosure__c = corretDayReturn(27,
                                                                            valueStartDate,
                                                                            valueFinishedDate);
                           System.debug('##### caseUp.CAC_automaticClosure__c '+caseUp.CAC_automaticClosure__c);
                           caseUp.CAC_HourExpired8__c = corretDayReturn(8,
                                                                        valueStartDate,
                                                                        valueFinishedDate);
                           System.debug('##### caseUp.CAC_HourExpired8__c '+caseUp.CAC_HourExpired8__c);
                           caseUp.CAC_HourExpired16__c = corretDayReturn(16,
                                                                         valueStartDate,
                                                                         valueFinishedDate);
                           System.debug('##### caseUp.CAC_HourExpired16__c  '+caseUp.CAC_HourExpired16__c );
                           caseUp.CAC_HourExpired24__c = corretDayReturn(24,
                                                                         valueStartDate,
                                                                         valueFinishedDate);
                           
                       }
                   }
            }
        }
    }
    
    public static void alterOwner(List<Case> caseList){
        
        List<Id> accountIdList = new List<Id>();
        for(Case caso : caseList)
            if(String.isNotBlank(caso.Dealer__c))
        		accountIdList.add(caso.Dealer__c);
        
        Map<Id,Account> accountMap = new Map<Id,Account>([select  id,QueueId__c from Account where id in:(accountIdList) and QueueId__c <> NULL]);
        
        for(Case caso : caseList){
            if(String.isNotBlank(caso.Dealer__c) && accountMap.containsKey(caso.Dealer__c)){
        		     caso.OwnerId =   accountMap.get(caso.Dealer__c).QueueId__c;
            }
        }
    }
    
    public void blockStatus(){
        
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c)){
            
            for(Case caso: lsCase){
                if( 
                    (mapOldCase.get(caso.id).Status.equals('Closed Dealer') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Automatically Answered') ||
                     mapOldCase.get(caso.id).Status.equals('Closed Dealer')  ||
                     mapOldCase.get(caso.id).Status.equals('Closed Analyst')
                    )
                ){
                    caso.addError('O caso  não pode ser modificado porque está fechado.');  
                }
                
            }
        }
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Dealer')){
            for(Case caso: lsCase){

                if((mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                    mapOldCase.get(caso.Id).Status.equals('Answered'))&& 
                   mapOldCase.get(caso.Id).Status != caso.Status
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }
                if(caso.Status.equals('New') && (mapOldCase.get(caso.Id).Status.equals('Under Analysis')||
                                                 mapOldCase.get(caso.Id).Status.equals('Waiting for reply')||
                                                 mapOldCase.get(caso.Id).Status.equals('Answered')
                                                )
                  ){
                      caso.addError('O caso não pode ser modificado');
                  }else{
                      if(!(caso.Status.equals('New')||
                           caso.Status.equals('Under Analysis')||
                           caso.Status.equals('Waiting for reply')||
                           caso.Status.equals('Answered')
                          )){
                              caso.addError('O caso não pode ser modificado');
                          }
                      
                  }
            }
        }
        if(userInfos.isCac__c && String.isNotBlank(userInfos.roleInCac__c) && userInfos.roleInCac__c.equals('Analyst')){
            for(Case caso: lsCase){
                if(!(caso.Status.equals('New')||
                     caso.Status.equals('Closed Analyst')||
                     caso.Status.equals('Closed Automatically Answered')||
                     caso.Status.equals('Closed Automatically Waiting Answer')
                    )){
                        caso.addError('O caso não pode ser modificado');
                    }
            }
        }
    }
    
}