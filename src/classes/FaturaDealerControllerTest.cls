@istest class FaturaDealerControllerTest {
    
    static testmethod void test() {
        User manager = criarUser();
        
        insert manager;
        
        System.runAs(manager) {
            
        //Executando como usuario da comunidade - Vendedor    
            FaturaDealer__c fat = new FaturaDealer__c();
            fat.NFTIPREG__c = '10';
            fat.NFBIRENV__c = '07600999';
            fat.NFBIREMI__c = '07600999';
            fat.NFSEQUEN__c = '2780';
            fat.NFNROREG__c = '00029';
            
            fat.NFBANDEI__c = 'REN';
            fat.NFCODOPR__c = '25';
            fat.NFCODFIS__c = '5403  VN';
            fat.NFTIPONF__c = 'VN';
            fat.NFNRONFI__c = '00073295';
            fat.NFCHASSI__c = '11111111111111911';
            
            fat.NFSERNFI__c = 'U';
            fat.NFDTANFI__c = '20150827';
            fat.NFNOMCLI__c = 'ITAMAR JOSE VIEIRA FERNANDES';
            fat.NFTIPVIA__c = 'RUA';
            fat.NFNOMVIA__c = 'Manoel Mancellos Moura';
            
            fat.NFNROVIA__c = '681';
            fat.NFCPLEND__c = 'AP 103';
            fat.NFBAIRRO__c = 'Canasvieiras';
            fat.NFCIDADE__c = 'FLORIANÓPOLIS';
            fat.NFNROCEP__c = '88054030';
            
            fat.NFESTADO__c = 'SC';
            fat.NFPAISRE__c = '00';
            fat.NFESTCIV__c = '';
            fat.NFSEXOMF__c = 'M';
            fat.NFDTANAS__c = '195908';
            fat.NFTIPCLI__c = 'F';
            fat.NFCPFCGC__c = '37970380972';
            
            fat.NFEMAILS__c = 'pelommedrado@gmail.com';
            fat.NFDDDRES__c = '11';
            fat.NFTELRES__c = '77778888';
            fat.NFDDDCEL__c = '11';
            fat.NFTELCEL__c = '99996666';
            fat.NFANOFAB__c = '2000';
            fat.NFANOMOD__c = '2000';
            
            insert fat;
            
            List<FaturaDealer__c> lsFat = new List<FaturaDealer__c>();
            lsFat.add(fat);
            
            FaturaDealerController fatController = new FaturaDealerController();
            fatController.exportExcelInvalidos();
            fatController.exportExcelValidos();
            //fatController.format(d);
            //fatController.format(d);
            //fatController.format(d, scale);
            //fatController.format(s);
            fatController.generateInvalidos(lsFat);
            fatController.generateValidos(lsFat);
            fatController.getLabelsInvalidos();
            fatController.getLabesValidos();
            fatController.limparFiltro();
            //fatController.line(cells);
            //fatController.montarQuery();
            //fatController.montarQueryFull();
            //fatController.montarQueryInAccount(accList);
            //fatController.montarQueryInvalidados();
            //fatController.montarQueryValidados();
            fatController.pesquisarFat();
            fatController.pesquisarFatAll();
            
        }       
    }
    
    static User criarUser() {
        
        Account parentAcc = new Account (Name ='ParentAcc');
        parentAcc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        parentAcc.ShippingCity = 'Barueri';
        parentAcc.ShippingState = 'SP';
        parentAcc.ShippingCountry = 'Brasil';
        parentAcc.ShippingPostalCode = '';
        parentAcc.Phone = '11999999999';
        parentAcc.ProfEmailAddress__c ='teste@teste.com'; 
        parentAcc.RecordTypeId = [select id from RecordType where developerName='Network_Acc' limit 1 ].id;
        database.insert(parentAcc);
        
        Account acc = new Account(
            parentId = parentAcc.Id,
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '123ABC123',
            ReturnActiveToSFA__c = true,
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        Contact cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455'
        );
        Database.insert( cont );
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='123ABC123',
            isCac__c = true
            
        );
        
        return manager;
    }
    
}