@isTest
public with sharing class TestTRI01_EM_ME{

static testMethod void testTriggerOnEmail(){

User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',BypassVR__c=true,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
System.runAs(usr)
{

Test.startTest();
Case c = new Case();
c.Description='This is the description';
insert c;
System.debug('c.Id>>>>>>>>>>>>>>>>>>>>>><<<'+c.Id);

EmailMessage e=new EmailMessage();
e.FromAddress='lrotondo@rotondo.com';
e.parentId=c.Id;
e.ToAddress='pidikiti@live.com';
e.Subject='This is subject';
insert e;

System.debug('e.Id>>>>>>>>>>>>>>>>>>>>>><<<'+e.Id);
System.debug('c.MailSentDate__c>>>>>>>>>>>>>>>>>>>>>><<<'+c.MailSentDate__c);
Test.stopTest();
}
}

}