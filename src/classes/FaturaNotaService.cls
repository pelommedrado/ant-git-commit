public with sharing class FaturaNotaService extends FaturaService {
	public static final String NFTYPE_CANCELLATION      = 'Cancellation';
	public static final String NFTYPE_DEVOLUTION        = 'Devolution';
	public static final String NFTYPE_NEW_VEHICLE       = 'New Vehicle';
 	public static final String NFTYPE_USER_CARS         = 'Used Cars';
	public static final String NFTYPE_PART_ACCESSORIES  = 'Parts and Accessories';

	private final List<String> typeList = new List<String> { NFTYPE_USER_CARS, NFTYPE_NEW_VEHICLE };

	private List<String> seqFileList = new List<String>();
	private Set<String> chassiSet = new Set<String>();

	public FaturaNotaService(List<FaturaDealer2__c> faturaList) {
		super(faturaList);
	}

	protected override Boolean isValidation(FaturaDealer2__c fatura) {
		return FaturaDealer2Utils.isRecordTypeInvoice(fatura);
	}

	protected override void faturaValid(FaturaDealer2__c fatura) {
		super.faturaValid(fatura);
		seqFileList.add(fatura.Sequential_File__c);
		chassiSet.add(fatura.VIN__c);
	}

	protected override void validation(FaturaDealer2__c fatura) {
		System.debug('validation()');
		if(isNfType(fatura, NFTYPE_NEW_VEHICLE)) {
			FaturaDealer2Utils.validationFatura(fatura);
		}
		else if(isNfType(fatura, NFTYPE_USER_CARS)) {
			FaturaDealer2Utils.validationFatura(fatura);
		}
		else if(isNfType(fatura, NFTYPE_PART_ACCESSORIES)) {
			FaturaDealer2Utils.validationFatura(fatura);
		}
		else if(isNfType(fatura, NFTYPE_CANCELLATION)) {
		}
		else if(isNfType(fatura, NFTYPE_DEVOLUTION)) {
		}

		if(!isNfType(fatura, NFTYPE_PART_ACCESSORIES)) {
			FaturaDealer2Utils.validationChassi(fatura);
		}
		System.debug('fatura:' + fatura);
		FaturaDealer2Utils.faturaIntegrationDate(fatura);
	}

	private Boolean isNfType(FaturaDealer2__c fatura, String nfType) {
	    if(String.isEmpty(fatura.Invoice_Type__c)) {
	      return false;
	    }
	    return fatura.Invoice_Type__c.equalsIgnoreCase(nfType);
	  }

	protected override Boolean isUpdateAccount(FaturaDealer2__c fatura) {
		if(isNfType(fatura, NFTYPE_NEW_VEHICLE)
			|| isNfType(fatura, NFTYPE_USER_CARS)
			|| isNfType(fatura, NFTYPE_PART_ACCESSORIES)) {
			return true;
		}
		return false;
	}

	protected override void processValid() {
		System.debug('processValid()');

		final Map<String, Account> customerIdAccountMap = processAccount();
        try {
            Database.upsert(customerIdAccountMap.values());
        } catch(Exception ex) {
            for(Account acc: customerIdAccountMap.values()) {
                System.debug('Acc:' + acc);
            }
            throw ex;
        }
    	

    	//ITEM 3.
    	assignAccount(customerIdAccountMap);

		final Map<String, VEH_Veh__c> chassiVehicleMap 	= processVehicle();
		Database.upsert(chassiVehicleMap.values());

    	final Map<String, VRE_VehRel__c> chassiVehicleRelMap = processVehicleRelation(customerIdAccountMap, chassiVehicleMap);
		Database.upsert(chassiVehicleRelMap.values());

		processHistory();
	}

	private Map<String, VEH_Veh__c> processVehicle() {
		System.debug('processVehicle()');

		final List<VEH_Veh__c> vehicleAllList = [SELECT Id, 
														Name 
												FROM VEH_Veh__c 
												WHERE Name IN : chassiSet];

    final Map<String, VEH_Veh__c> chassiVehicleMap = new Map<String, VEH_Veh__c>();

    for(VEH_Veh__c veh : vehicleAllList) {
      chassiVehicleMap.put(veh.Name, veh);
    }

	for(FaturaDealer2__c fatura : faturaValidList) {
      	if(isNfType(fatura, NFTYPE_USER_CARS) || isNfType(fatura, NFTYPE_NEW_VEHICLE)) {
        	VEH_Veh__c vehicle  = chassiVehicleMap.get(fatura.VIN__c);
        	vehicle = updateVehicle(vehicle, fatura);
        	if(vehicle.Id == null) {
          		chassiVehicleMap.put(vehicle.Name, vehicle);
        	}
      	}
    }

		return chassiVehicleMap;
	}

	private VEH_Veh__c updateVehicle(VEH_Veh__c vehicle, FaturaDealer2__c fatura) {
	    if(vehicle == null) {
	      	vehicle = new VEH_Veh__c();
	      	vehicle.Name = fatura.VIN__c;
	    }

	    if(!String.isEmpty(fatura.Manufacturing_Year__c)) {
	      	vehicle.DateofManu__c = Date.valueOf(fatura.Manufacturing_Year__c + '-01-01');
	    }
	    if(!String.isEmpty(fatura.Model_Year__c)) {
	      	vehicle.ModelYear__c = Integer.valueOf(fatura.Model_Year__c);
	    }
	    if(!String.isEmpty(fatura.Delivery_Date__c)) {
	      	vehicle.DeliveryDate__c = FaturaDealer2Utils.validDate(fatura.Delivery_Date__c);
	    }
	    return vehicle;
  	}

	private Map<String, VRE_VehRel__c> processVehicleRelation(
		Map<String, Account> customerIdAccountMap, Map<String, VEH_Veh__c> chassiVehicleMap) {
    	System.debug('processVehicleRelation()');

    	final Map<String, VRE_VehRel__c> chassiVehicleRelMap  =
			createVehicleRelationMap(customerIdAccountMap, chassiVehicleMap);

		final Map<String, VEH_Veh__c> customerIdVehicleMap  = new Map<String, VEH_Veh__c>();
    	for(FaturaDealer2__c fatura : faturaValidList) {
			final String cpfCnpj = fatura.Customer_Identification_Number__c;
			final Account account = customerIdAccountMap.get(cpfCnpj);

			if(account == null) {
				faturaInvalid(fatura);
				FaturaDealer2Utils.addMsg(fatura, 'Conta não encontrada para a nota  | ');
			} 
			else {
				final VRE_VehRel__c relation = chassiVehicleRelMap.get(fatura.VIN__c);
				if(relation == null) {
					final VEH_Veh__c vehicle = chassiVehicleMap.get(fatura.VIN__c);
					customerIdVehicleMap.put(cpfCnpj, vehicle);
				}
			}
		}

		for(Account acc : customerIdAccountMap.values()) {
			final String cpfCnpj = acc.CustomerIdentificationNbr__c;
			final VEH_Veh__c veiculo = customerIdVehicleMap.get(cpfCnpj);

			if(veiculo != null) {
				final VRE_VehRel__c relation  = new VRE_VehRel__c();
				relation.Account__c	= acc.id;
				relation.Status__c	= 'Active';
				relation.VIN__c	= veiculo.Id;
				chassiVehicleRelMap.put(veiculo.Name, relation);
			}
		}

		for(FaturaDealer2__c fatura : faturaValidList) {
			if(isNfType(fatura, NFTYPE_DEVOLUTION) || isNfType(fatura, NFTYPE_CANCELLATION)) {
				final VRE_VehRel__c relation = chassiVehicleRelMap.get(fatura.VIN__c);
				if(relation != null) {
					relation.Status__c = 'Inactive';
				}
			} else if(isNfType(fatura, NFTYPE_USER_CARS) || isNfType(fatura, NFTYPE_NEW_VEHICLE)) {
				final VRE_VehRel__c relation = chassiVehicleRelMap.get(fatura.VIN__c);
				if(relation != null) {
					relation.Status__c = 'Active';
				}
			}
		}

		return chassiVehicleRelMap;
  	}

	private Map<String, VRE_VehRel__c> createVehicleRelationMap(
		Map<String, Account> customerIdAccountMap, Map<String, VEH_Veh__c> chassiVehicleMap) {
		System.debug('createVehicleRelationMap()');

		final Map<String, VRE_VehRel__c> chassiVehicleRelMap  = new Map<String, VRE_VehRel__c>();
    	final List<VRE_VehRel__c> vehicleRelationList = [SELECT Id, 
    															Account__c, 
    															Status__c, 
    															VIN__r.Name 
    													FROM VRE_VehRel__c
    													WHERE Account__c IN: customerIdAccountMap.values()
    													AND VIN__c IN: chassiVehicleMap.values()];

    	for(VRE_VehRel__c veiRel : vehicleRelationList) {
      		chassiVehicleRelMap.put(veiRel.VIN__r.Name, veiRel);
    	}

		return chassiVehicleRelMap;
	}

	private void processHistory() {
		System.debug('processHistory()');

    	final Map<String, FaturaDealer2__C> chassiFaturaMap = processHistoryDevolution();
    	final Map<String, FaturaDealer2__C> seqFileFaturaMap = processHistoryCancellation();

    	for(FaturaDealer2__c fatura : faturaValidList) {
	      	if(isNfType(fatura, NFTYPE_DEVOLUTION)) {
	        	final FaturaDealer2__c fat = chassiFaturaMap.get(fatura.VIN__c);
	        	//Melhorias Fatura Dealer 2
				//ITEM 2 -  Faturas de devolução e cancelamento interagindo com faturas posteriores.
				//ITEM 2.
			 	if(fat != null && isNfType(fat, NFTYPE_NEW_VEHICLE)) {
                    if(fat.Invoice_Date_Mirror__c < fatura.Invoice_Date_Mirror__c) {
                        fat.isReturned__c = true;
                    }
                }
	      	} 
	      	else if(isNfType(fatura, NFTYPE_CANCELLATION)) {
	        	final FaturaDealer2__c fat = seqFileFaturaMap.get(fatura.Sequential_File__c);
	        	//ITEM 2.
                if(fat != null && isNfType(fat, NFTYPE_NEW_VEHICLE)) {
                    if(fat != null && fat.Invoice_Date_Mirror__c < fatura.Invoice_Date_Mirror__c) {
                        fat.isCancelled__c = true;
                    }    
                }	
	      	}
    	}

	    Database.update(chassiFaturaMap.values());
	    Database.update(seqFileFaturaMap.values());
	}

	private Map<String, FaturaDealer2__c> processHistoryCancellation() {
		final List<FaturaDealer2__c> lstCancellation =  [
            SELECT Id, Invoice_Type__c, Invoice_Date_Mirror__c, VIN__c, Sequential_File__c, Integration_Date__c
			FROM FaturaDealer2__c WHERE Invoice_Type__c IN: typeList 
            	AND isReturned__c = false AND isCancelled__c = false 
            	AND Sequential_File__c IN: seqFileList
				ORDER BY Integration_Date__c ASC
        ];
    	
    	final Map<String, FaturaDealer2__c> seqFileFaturaMap = new Map<String, FaturaDealer2__c>();
    	
    	for(FaturaDealer2__C fat : lstCancellation) {
      		seqFileFaturaMap.put(fat.Sequential_File__c, fat);
    	}
		
		return seqFileFaturaMap;
	}

	private Map<String, FaturaDealer2__c> processHistoryDevolution() {
    	final List<FaturaDealer2__C> lstDevolution = [
            SELECT Id, Invoice_Type__c, Invoice_Date_Mirror__c, VIN__c, Sequential_File__c, Integration_Date__c
    		FROM FaturaDealer2__c WHERE Invoice_Type__c IN: typeList 
            	AND isReturned__c = false AND isCancelled__c = false 
            	AND VIN__c IN: chassiSet
    			ORDER BY Integration_Date__c ASC
        ];

	    final Map<String, FaturaDealer2__c> chassiFaturaMap = new Map<String, FaturaDealer2__c>();

	    for(FaturaDealer2__C fat : lstDevolution) {
	    	chassiFaturaMap.put(fat.VIN__c, fat);
	    }
		
		return chassiFaturaMap;
	}
}