/**
* Classe responsável por fazer o papel de business delegate da página de test drive SAC.
* @author Felipe Jesus Silva.
*/
public virtual class VFC111_TestDriveSACBusinessDelegate 
{
	private static final VFC111_TestDriveSACBusinessDelegate instance = new VFC111_TestDriveSACBusinessDelegate();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	public VFC111_TestDriveSACBusinessDelegate()
	{		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC111_TestDriveSACBusinessDelegate getInstance()
	{
		return instance; 
	}
	
	public VFC110_TestDriveSACVO getTestDriveByAccount(String accountId)
	{
		Account sObjAccount = null;
		List<Product2> lstSObjModel = null;
		VFC110_TestDriveSACVO testDriveVO = null;
		VFC110_TestDriveSACVO testDriveVOAux = null;
		
		/*obtém a conta pelo id*/
		sObjAccount = VFC113_AccountBO.getInstance().getById(accountId);
		
		/*obtém a lista de modelos*/
		lstSObjModel = VFC22_ProductDAO.getInstance().findByRecordType('PDT_Model');
		
		/*mapeia os dados para o VO*/
		testDriveVO = this.mapToValueObject(sObjAccount, lstSObjModel);
	
		/*armazena a lista de modelos no VO*/
		testDriveVO.lstSObjModel = lstSObjModel;
		
		/*verifica se a concessionária de interesse está preenchida*/
		if(testDriveVO.sObjAccount.DealerInterest_BR__c != null)
		{
			/*obtém o VO contendo os veículos disponíveis e a agenda do primeiro veículo disponível*/
			testDriveVOAux = this.getVehiclesAvailable(testDriveVO);
			
			/*armazena a lista de veículos disponíveis*/
			testDriveVO.lstSObjTestDriveVehicleAvailable = testDriveVOAux.lstSObjTestDriveVehicleAvailable;
			
			/*seta os detalhes do primeiro veículo disponível*/
			testDriveVO.plaque = testDriveVOAux.plaque;
			testDriveVO.model = testDriveVOAux.model;
			testDriveVO.version = testDriveVOAux.version;
			testDriveVO.color = testDriveVOAux.color;
			
			/*armazena a agenda do primeiro veículo disponível*/
			testDriveVO.lstSObjTestDriveAgenda = testDriveVOAux.lstSObjTestDriveAgenda;
		}
		
		return testDriveVO;
	}
	
	public VFC110_TestDriveSACVO getVehiclesAvailable(VFC110_TestDriveSACVO testDriveVO)
	{
		List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicleAux = null;
		VFC110_TestDriveSACVO testDriveVOAux = new VFC110_TestDriveSACVO();
		
		/*obtém a lista de veículos que estão disponíveis para test drive (parâmetros: veículo de interesse e id da concessionária)*/		
		lstSObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getVehiclesAvailable(testDriveVO.sObjAccount.DealerInterest_BR__c, testDriveVO.vehicleInterest);
		
		/*verifica se a busca retornou registros*/
		if(!lstSObjTestDriveVehicle.isEmpty())
		{
			/*obtém o primeiro veículo da lista*/
			sObjTestDriveVehicleAux = lstSObjTestDriveVehicle.get(0);
			
			testDriveVOAux.testDriveVehicleId = sObjTestDriveVehicleAux.Id;
			testDriveVOAux.dateBookingNavigation = Date.today();
			
			/*obtém a agenda desse veículo*/
			testDriveVOAux = this.getAgendaVehicle(testDriveVOAux);
			
			testDriveVOAux.plaque = sObjTestDriveVehicleAux.Vehicle__r.VehicleRegistrNbr__c;
			testDriveVOAux.model = sObjTestDriveVehicleAux.Vehicle__r.Model__c;
			testDriveVOAux.version = sObjTestDriveVehicleAux.Vehicle__r.Version__c;
			testDriveVOAux.color = sObjTestDriveVehicleAux.Vehicle__r.Color__c;			
			testDriveVOAux.dateBookingNavigation = Date.today();
			testDriveVOAux.dealerOwnerId = sObjTestDriveVehicleAux.Account__r.OwnerId;
			
			testDriveVOAux.lstSObjTestDriveVehicleAvailable = lstSObjTestDriveVehicle;
		}
			                                                                    	
		return testDriveVOAux;
	}
	
	public VFC110_TestDriveSACVO getAgendaVehicle(VFC110_TestDriveSACVO testDriveVO)
	{
		List<TDV_TestDrive__c> lstSObjTestDrive = null;
		VFC110_TestDriveSACVO testDriveVOResponse = new VFC110_TestDriveSACVO();
		
		lstSObjTestDrive = VFC30_TestDriveBO.getInstance().getTestsDriveScheduledOrConfirmed(testDriveVO.testDriveVehicleId, testDriveVO.dateBookingNavigation);
		
		testDriveVOResponse.lstSObjTestDriveAgenda = lstSObjTestDrive; 
		
		return testDriveVOResponse; 
	}
	
	public VFC110_TestDriveSACVO getVehicleDetails(VFC110_TestDriveSACVO testDriveVO)
	{
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		VFC110_TestDriveSACVO testDriveVOResponse = null;
		
		/*obtém o veículo pelo id*/	
		sObjTestDriveVehicle = VFC66_TestDriveVehicleBO.getInstance().getById(testDriveVO.testDriveVehicleId); 
			
		/*obtém o VO contendo a lista de tests drive agendados/confirmados para esse veículo*/
		testDriveVOResponse = this.getAgendaVehicle(testDriveVO);
			
		/*seta o restante dos dados no objeto de resposta*/
		testDriveVOResponse.model = sObjTestDriveVehicle.Vehicle__r.Model__c;
		testDriveVOResponse.version = sObjTestDriveVehicle.Vehicle__r.Version__c;
		testDriveVOResponse.color = sObjTestDriveVehicle.Vehicle__r.Color__c;		
		testDriveVOResponse.plaque = sObjTestDriveVehicle.Vehicle__r.VehicleRegistrNbr__c;

		return testDriveVOResponse;
	}
	
	public void scheduleTestDrive(VFC110_TestDriveSACVO testDriveVO)
	{
		SavePoint objSavePoint = null;
		TDV_TestDrive__c sObjTestDrive = null;	
		Opportunity sObjOpportunity = new Opportunity();
		
        sObjOpportunity.AccountId = testDriveVO.accountId;
        sObjOpportunity.OwnerId = testDriveVO.dealerOwnerId;
        sObjOpportunity.Dealer__c = testDriveVO.sObjAccount.DealerInterest_BR__c;
        sObjOpportunity.CloseDate = Date.today() + 30;
        sObjOpportunity.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER;
        sObjOpportunity.Name = 'Oportunidade Test Drive ' +  '(' + testDriveVO.customerName  +')';
        sObjOpportunity.StageName = 'Identified';
        sObjOpportunity.OpportunitySource__c = 'PHONE';
        sObjOpportunity.OpportunitySubSource__c = 'SAC';
                  
        try
        {
        	/*obtém o savepoint para controlar a transação*/
        	objSavePoint = Database.setSavepoint();
        	
        	/*cria a oportunidade*/
        	VFC47_OpportunityBO.getInstance().createOpportunity(sObjOpportunity);
        	 	     	
        	/*instancia o test drive e vincula à oportunidade criada*/
        	sObjTestDrive = new TDV_TestDrive__c();
        	sObjTestDrive.Opportunity__c = sObjOpportunity.Id;		
			sObjTestDrive.TestDriveVehicle__c = testDriveVO.testDriveVehicleId;
			sObjTestDrive.DateBooking__c = testDriveVO.dateBooking;
			sObjTestDrive.Status__c = 'Scheduled';
			
			/*cria o test drive*/
			VFC30_TestDriveBO.getInstance().createNewTestDrive(sObjTestDrive);
        }
        catch(Exception ex)
        {
        	Database.rollback(objSavePoint);
        	
        	throw ex;
        }         
	}
	
	private VFC110_TestDriveSACVO mapToValueObject(Account sObjAccount, List<Product2> lstSObjModel)
	{
		String vehicleInterest = null;
		VFC110_TestDriveSACVO testDriveVO = new VFC110_TestDriveSACVO();
		
		/*popula o VO*/
		testDriveVO.accountId = sObjAccount.Id;
		testDriveVO.sObjAccount.DealerInterest_BR__c = sObjAccount.DealerInterest_BR__c;
		testDriveVO.dealerOwnerId = sObjAccount.DealerInterest_BR__r.OwnerId;
		testDriveVO.customerName = sObjAccount.Name; 
		testDriveVO.persLandline = sObjAccount.PersLandline__c;
		testDriveVO.persMobPhone = sObjAccount.PersMobPhone__c;
		testDriveVO.persEmail = sObjAccount.PersEmailAddress__c;
		testDriveVO.dateBookingNavigation = Date.today();
		testDriveVO.currentVehicle = sObjAccount.CurrentVehicle_BR__r.Model__c;
		
		if(sObjAccount.YrReturnVehicle_BR__c != null)
		{
			testDriveVO.yearCurrentVehicle = sObjAccount.YrReturnVehicle_BR__c.intValue();
		}
		
		if(String.isEmpty(sObjAccount.VehicleInterest_BR__c))
		{
			/*obtém o nome do primeiro modelo da lista*/
			vehicleInterest = lstSObjModel.get(0).Name;
		}
		else
		{
			/*a princípio obtém o nome do primeiro modelo da lista*/
			vehicleInterest = lstSObjModel.get(0).Name;
			
			/*é necessário verificar se o veículo de interesse da conta contém na lista de modelos*/			
			for(Product2 sObjModel: lstSObjModel)
			{
				/*se o veículo de interesse da conta for igual a algum modelo da lista, obtém o veículo da conta*/
				if(sObjAccount.VehicleInterest_BR__c == sObjModel.Name)
				{
					vehicleInterest = sObjAccount.VehicleInterest_BR__c.toUpperCase();
					
					break;
				}
			}
		}
	
		testDriveVO.vehicleInterest = vehicleInterest;
		
		return testDriveVO;
	}
}