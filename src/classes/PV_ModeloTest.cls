@isTest
private class PV_ModeloTest {
    
  @isTest static void myTestMethod() {
    Model__c model = new Model__c(
      //ENS__c = 'abc',
      Market__c = 'abc',
      Status__c = 'Active',
      Model_Spec_Code__c = 'ABC',
      Phase__c = '1',
      Model_PK__c = 'ABC-1'
    );
    Database.insert( model );

    PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
      Model__c = model.Id,
      Start_Date__c = System.today().addMonths( -1 ),
      End_Date__c = System.today().addMonths( 1 ),
      Type_of_Action__c = 'abc',
      Status__c = 'Active'
    );
    Database.insert( commercialAction );

    PVCall_Offer__c callOffer = new PVCall_Offer__c(
      Commercial_Action__c = commercialAction.Id,
      Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
      Call_Offer_End_Date__c = System.today().addMonths( 1 ),
      Minimum_Entry__c = 100.0,
      Period_in_Months__c = 5,
      Month_Rate__c = 0.99
    );
    Database.insert( callOffer );

    System.assertEquals( model.Id, [select Model__c from PVCall_Offer__c where Id = :callOffer.Id limit 1].Model__c );
  }
}