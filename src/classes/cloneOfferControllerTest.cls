@isTest
public class cloneOfferControllerTest {
    
    static testMethod void testa1(){
        
        test.startTest();
        
        Offer__c offer = new Offer__c( 
            From_To_Offer__c = false,
            hasUpgrade__c = false,
            Offer_to_Internet__c = true,
            Total_Inventory_Vehicle__c = 3,
            Condition__c = 'Financiado',
            ValueTo__c = 40060,
            Minimum_Input__c = 75,
            Number_of_Installments__c = 36,
            Monthly_Tax__c = 0,
            Coefficient__c = 0.02861,
            Entry_Value__c = 30045,
            Installment_Value__c = 305.01,
            Stamp__c = 'Marketing of factory;Big marketing in factory',
            Offer_Start_Date_Website__c = System.today().addDays( -1 ),
            Offer_End_Date_Website__c = System.today().addDays( 1 ),
            Pricing_Template__c = 'Sight',
            Featured_In_Offer__c = 'Complete'
        );
        
        //Database.insert(offer);
        
        Test.setCurrentPageReference(new PageReference('Page.cloneOffer')); 
		System.currentPageReference().getParameters().put('id', 'testeId');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(offer);
        
        cloneOfferController clone = new cloneOfferController(sc);
            
        test.stopTest();
        
    }

}