@isTest
private class FDealer2SimpleControllerTest {
	static User userCommunity;
	static Account accCommunity;

	static {
		User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
    User user;
    Account account;

		MyOwnCreation moc = new MyOwnCreation();
    System.runAs(localUser) {
        //necessario usuario possuir papel
        UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
        localUser.UserRoleId = r.Id;
        update localUser;

        accCommunity = moc.criaAccountDealer();
        accCommunity.ReturnActiveToSFA__c = true;
				accCommunity.IDBIR__c = '12341';
        insert(accCommunity);

        Contact contact = moc.criaContato();
        contact.AccountId = accCommunity.Id;
        insert(contact);

        userCommunity = moc.criaUser();
        userCommunity.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
        userCommunity.ContactId = contact.Id;
        insert(userCommunity);
    }
	}

	@isTest static void shouldCheckLabels() {
		FDealer2SimpleController controller = new FDealer2SimpleController();
		System.assertEquals(false, controller.Address == null);
		System.assertEquals(false, controller.Address_Complement == null);
		System.assertEquals(false, controller.BIR_Group_Issuer == null);
		System.assertEquals(false, controller.BIR_Name_NFE_Issuer == null);
		System.assertEquals(false, controller.BIR_NFE_Issuer == null);
		System.assertEquals(false, controller.Birth_Date == null);
		System.assertEquals(false, controller.isCancelled == null);
		System.assertEquals(false, controller.City == null);
		System.assertEquals(false, controller.Customer_Email == null);
		System.assertEquals(false, controller.Customer_Email_Mirror == null);
		System.assertEquals(false, controller.Customer_Identification_Number == null);
		System.assertEquals(false, controller.Customer_Name == null);
		System.assertEquals(false, controller.Customer_Type == null);
		System.assertEquals(false, controller.DDD1 == null);
		System.assertEquals(false, controller.DDD2 == null);
		System.assertEquals(false, controller.DDD3 == null);
		System.assertEquals(false, controller.Delivery_Date == null);
		System.assertEquals(false, controller.Error_Messages == null);
		System.assertEquals(false, controller.External_Key == null);
		System.assertEquals(false, controller.Group_Name_Issuer == null);
		System.assertEquals(false, controller.Integration_Date == null);
		System.assertEquals(false, controller.Integration_Protocol == null);
		System.assertEquals(false, controller.Invoice_Data == null);
		System.assertEquals(false, controller.Invoice_Number == null);
		System.assertEquals(false, controller.Invoice_Serie == null);
		System.assertEquals(false, controller.Invoice_Type == null);
		System.assertEquals(false, controller.Manufacturing_Year == null);
		System.assertEquals(false, controller.Model_Year == null);
		System.assertEquals(false, controller.Neighborhood == null);
		System.assertEquals(false, controller.TrackNumber == null);
		System.assertEquals(false, controller.Payment_Type == null);
		System.assertEquals(false, controller.Phone_1 == null);
		System.assertEquals(false, controller.Phone_2 == null);
		System.assertEquals(false, controller.Phone_3 == null);
		System.assertEquals(false, controller.Postal_Code == null);
		System.assertEquals(false, controller.Region == null);
		System.assertEquals(false, controller.isReturned == null);
		System.assertEquals(false, controller.Sector == null);
		System.assertEquals(false, controller.Sequential_File == null);
		System.assertEquals(false, controller.Shipping_BIR == null);
		System.assertEquals(false, controller.State == null);
		System.assertEquals(false, controller.StatusInvoice == null);
		System.assertEquals(false, controller.VIN == null);
	}

	@isTest static void shouldGetCsvContent() {
		System.runAs(userCommunity) {
			FaturaDealer2__c fatura = new FaturaDealer2__c();
			fatura.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;
			fatura.Customer_Identification_Number__c 	= '51820533000150';
			fatura.VIN__c 								= '3425GSVFAGS234637';
			fatura.Invoice_Type__c 						= 'New Vehicle';
			fatura.Manufacturing_Year__c 				= '1994';
	    fatura.Customer_Type__c 					= 'J';
			fatura.Model_Year__c 						= '1994';
			fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura.DDD1__c 								= '11';
			fatura.Phone_1__c 							= '27381122';
			fatura.DDD2__c 								= '12';
			fatura.Phone_2__c 							= '99098876';
			fatura.DDD3__c 								= '13';
			fatura.Phone_3__c 							= '20987761';
			fatura.External_Key__c 						= '123';
			fatura.Delivery_Date_Mirror__c 				=  System.today();
			fatura.Status__c 							= 'Invalid';
			Database.insert(fatura);

			FDealer2SimpleController controller = new FDealer2SimpleController();
			PageReference check = controller.searchAllInvoice();
			System.assertEquals('System.PageReference[/servlet/servlet.FileDownload?file', String.valueOf(check).substringBefore('='));
		}
	}

	@isTest static void shouldGetCsvContentWithBothDates() {
		System.runAs(userCommunity) {
			FaturaDealer2__c fatura 					= new FaturaDealer2__c();
			fatura.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;
			fatura.Customer_Identification_Number__c 	= '51820533000150';
			fatura.VIN__c 								= '3425GSVFAGS234637';
			fatura.Invoice_Type__c 						= 'New Vehicle';
			fatura.Manufacturing_Year__c 				= '1994';
	    fatura.Customer_Type__c 					= 'J';
			fatura.Model_Year__c 						= '1994';
			fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura.DDD1__c 								= '11';
			fatura.Phone_1__c 							= '27381122';
			fatura.DDD2__c 								= '12';
			fatura.Phone_2__c 							= '99098876';
			fatura.DDD3__c 								= '13';
			fatura.Phone_3__c 							= '20987761';
			fatura.External_Key__c 						= '123';
			fatura.Delivery_Date_Mirror__c 				=  System.today();
			fatura.Integration_Date_Mirror__c 			= System.today();
			fatura.Status__c 							= 'Invalid';
			Database.insert(fatura);

			FDealer2SimpleController controller = new FDealer2SimpleController();
			List<String> ontem 		= String.valueOf(System.today().addDays(-1)).substringBefore(' ').split('-');
			List<String> hoje 		= String.valueOf(System.today()).substringBefore(' ').split('-');
			controller.sendFrom 	= ontem[2]+'/'+ontem[1]+'/'+ontem[0];
			controller.sendTo 		= hoje[2]+'/'+hoje[1]+'/'+hoje[0];
			controller.invoiceFrom 	= ontem[2]+'/'+ontem[1]+'/'+ontem[0];
			controller.invoiceTo 	= hoje[2]+'/'+hoje[1]+'/'+hoje[0];
			PageReference check 	= controller.searchAllInvoice();
			System.assertEquals('System.PageReference[/servlet/servlet.FileDownload?file', String.valueOf(check).substringBefore('='));
		}
	}

	@isTest static void shouldFillInvoiceList() {
		System.runAs(userCommunity) {
			FaturaDealer2__c fatura = new FaturaDealer2__c();
			fatura.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;
			fatura.Customer_Identification_Number__c 	= '51820533000150';
			fatura.VIN__c 								= '3425GSVFAGS234637';
			fatura.Invoice_Type__c 						= 'New Vehicle';
			fatura.Manufacturing_Year__c 				= '1994';
	        fatura.Customer_Type__c 					= 'J';
			fatura.Model_Year__c 						= '1994';
			fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura.DDD1__c 								= '11';
			fatura.Phone_1__c 							= '27381122';
			fatura.DDD2__c 								= '12';
			fatura.Phone_2__c 							= '99098876';
			fatura.DDD3__c 								= '13';
			fatura.Phone_3__c 							= '20987761';
			fatura.External_Key__c 						= '123';
			fatura.Delivery_Date_Mirror__c 				=  System.today();
			fatura.Status__c 							= 'Invalid';
			Database.insert(fatura);

			FDealer2SimpleController controller = new FDealer2SimpleController();
			controller.searchInvoice();
			System.assertEquals(false, controller.invoiceList.isEmpty());
			System.assertEquals(1, controller.invoiceList.size());
			System.assertEquals('3425GSVFAGS234637', controller.invoiceList[0].VIN__c);
		}
	}

	@isTest static void shouldFillInvoiceListWithOneInvoice() {
		System.runAs(userCommunity) {
			FaturaDealer2__c fatura = new FaturaDealer2__c();
			fatura.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;
			fatura.Customer_Identification_Number__c 	= '51820533000150';
			fatura.VIN__c 								= '3425GSVFAGS234637';
			fatura.Invoice_Type__c 						= 'New Vehicle';
			fatura.Manufacturing_Year__c 				= '1994';
      fatura.Customer_Type__c 					= 'J';
			fatura.Model_Year__c 						= '1994';
			fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura.DDD1__c 								= '11';
			fatura.Phone_1__c 							= '27381122';
			fatura.DDD2__c 								= '12';
			fatura.Phone_2__c 							= '99098876';
			fatura.DDD3__c 								= '13';
			fatura.Phone_3__c 							= '20987761';
			fatura.External_Key__c 						= '123';
			fatura.Delivery_Date_Mirror__c 				=  System.today();
			fatura.Integration_Date_Mirror__c 			= System.today();
			fatura.Invoice_Date_Mirror__c 				=  Date.ValueOf('2017-01-01');
			fatura.Status__c 							= 'Invalid';
			Database.insert(fatura);

			FaturaDealer2__c fatura2 = new FaturaDealer2__c();
			fatura2.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;
			fatura2.Customer_Identification_Number__c 	= '46843566634';
			fatura2.VIN__c 								= '1234ABCDEFG123456';
			fatura2.Invoice_Type__c 					= 'New Vehicle';
			fatura2.Manufacturing_Year__c 				= '1994';
	    fatura2.Customer_Type__c 					= 'F';
			fatura2.Model_Year__c 						= '1994';
			fatura2.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura2.DDD1__c 							= '11';
			fatura2.Phone_1__c 							= '27381122';
			fatura2.DDD2__c 							= '12';
			fatura2.Phone_2__c 							= '99098876';
			fatura2.DDD3__c 							= '13';
			fatura2.Phone_3__c 							= '20987761';
			fatura2.External_Key__c 					= '456';
			fatura2.Delivery_Date_Mirror__c 				=  Date.ValueOf('1900-01-01');
			fatura2.Status__c 							= 'Invalid';
			Database.insert(fatura2);

			FDealer2SimpleController controller = new FDealer2SimpleController();
			List<String> ontem 		= String.valueOf(System.today().addDays(-1)).substringBefore(' ').split('-');
			List<String> hoje 		= String.valueOf(System.today()).substringBefore(' ').split('-');
			//controller.sendFrom 	= ontem[2]+'/'+ontem[1]+'/'+ontem[0];
			//controller.sendTo 		= hoje[2]+'/'+hoje[1]+'/'+hoje[0];
			controller.invoiceFrom 	= '31/12/2016';
			controller.invoiceTo 	= '01/01/2017';
			controller.searchInvoice();
			System.assertEquals(false, controller.invoiceList.isEmpty());
			System.assertEquals(1, controller.invoiceList.size());
			System.assertEquals('3425GSVFAGS234637', controller.invoiceList[0].VIN__c);
		}
	}

	/*@isTest static void shouldNotFillInvoiceListBecauseOfInvoiceDateFrom() {
		FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c 	= '51820533000150';
		fatura.VIN__c 								= '3425GSVFAGS234637';
		fatura.Invoice_Type__c 						= 'New Vehicle';
		fatura.Manufacturing_Year__c 				= '1994';
        fatura.Customer_Type__c 					= 'J';
		fatura.Model_Year__c 						= '1994';
		fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
		fatura.DDD1__c 								= '11';
		fatura.Phone_1__c 							= '27381122';
		fatura.DDD2__c 								= '12';
		fatura.Phone_2__c 							= '99098876';
		fatura.DDD3__c 								= '13';
		fatura.Phone_3__c 							= '20987761';
		fatura.External_Key__c 						= '123';
		fatura.Delivery_Date_Mirror__c 				=  System.today();
		fatura.Status__c 							= 'Invalid';
		Database.insert(fatura);

		FDealer2SimpleController controller = new FDealer2SimpleController();
		controller.invoiceFrom = '01/01/1999';
		controller.searchInvoice();
		System.assertEquals('(ApexPages.Message["A data mínima para início da análise ', String.valueOf(ApexPages.getMessages()).substringBeforeLast('é'));
		System.assertEquals(true, controller.invoiceList.isEmpty());
		System.assertEquals(0, controller.invoiceList.size());
	}*/

	@isTest static void shouldStillFillInvoiceListBecauseOfInvoiceDateTo() {
		System.runAs(userCommunity) {
			FaturaDealer2__c fatura = new FaturaDealer2__c();
			fatura.BIR_NFE_Issuer__c = accCommunity.IDBIR__c;

			fatura.Customer_Identification_Number__c 	= '51820533000150';
			fatura.VIN__c 								= '3425GSVFAGS234637';
			fatura.Invoice_Type__c 						= 'New Vehicle';
			fatura.Manufacturing_Year__c 				= '1994';
		      fatura.Customer_Type__c 					= 'J';
			fatura.Model_Year__c 						= '1994';
			fatura.Customer_Email__c 					= 'dadf@sdfdas.com';
			fatura.DDD1__c 								= '11';
			fatura.Phone_1__c 							= '27381122';
			fatura.DDD2__c 								= '12';
			fatura.Phone_2__c 							= '99098876';
			fatura.DDD3__c 								= '13';
			fatura.Phone_3__c 							= '20987761';
			fatura.External_Key__c 						= '123';
			fatura.Delivery_Date_Mirror__c 				=  System.today();
			fatura.Status__c 							= 'Invalid';
			Database.insert(fatura);

			FDealer2SimpleController controller = new FDealer2SimpleController();
			List<String> ontem 		= String.valueOf(System.today().addDays(-1)).substringBefore(' ').split('-');
			List<String> amanha 	= String.valueOf(System.today().addDays(1)).substringBefore(' ').split('-');
			controller.invoiceTo 	= amanha[2]+'/'+amanha[1]+'/'+amanha[0];
			controller.searchInvoice();
			System.assertEquals('(ApexPages.Message["A data máxima para término da análise ', String.valueOf(ApexPages.getMessages()).substringBeforeLast('é'));
			System.assertEquals(false, controller.invoiceList.isEmpty());
			System.assertEquals(1, controller.invoiceList.size());
		}
	}
}