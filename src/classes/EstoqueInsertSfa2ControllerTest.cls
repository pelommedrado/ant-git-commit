@isTest
private class EstoqueInsertSfa2ControllerTest {

    public static Opportunity opp;
    public static Quote q;
    public static User manager;
    public static Model__c modelC;
    public static PVVersion__c version;
    public static VRE_VehRel__c vre;
    public static VEH_Veh__c v;

    public static void setup(){

        //Classe para criar os objetos
        MyOwnCreation moc = new MyOwnCreation();

        List<Account> insertAccount = new List<Account>();
        
        Account dealer = moc.criaAccountDealer();
        dealer.Name = 'Teste';
        
        Account dealer2 = moc.criaAccountDealer();
        dealer.Name = 'Teste 2';
        dealer2.IDBIR__c = '1234567';
        dealer2.ParentId = dealer.Id;

        Account a = moc.criaPersAccount();

        insertAccount.add(dealer);
        insertAccount.add(dealer2);
        insertAccount.add(a);
        Insert insertAccount;
        
        v = moc.criaVeiculo();
        v.DateofManu__c = system.today();
        Insert v;
        
        vre = moc.criaVeiculoRelacionado();
        vre.VIN__c = v.Id;
        vre.Account__c = dealer2.Id;
        insert vre;
     
        opp = moc.criaOpportunity();
        opp.AccountId = a.Id;
        opp.Dealer__c = dealer2.Id;
        Insert opp;

        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;

        q = moc.criaQuote();
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb.Id;
        Insert q;
        
        Product2 prd2 = moc.criaProduct2();
        prd2.Name = 'CLIO';
        prd2.ModelSpecCode__c = 'Name';
        Insert prd2;
        
        modelC = new Model__c();
        modelC.Name = 'CLIO';
        modelC.Status_SFA__c = 'Active';
        modelC.Model_PK__c = 'VVV-3';
        insert modelC;
        
        version = new PVVersion__c();
        version.Price__c = 1;
        version.Model__c = modelC.Id;
        version.PVC_Maximo__c = 1;
        version.PVR_Minimo__c = 1;
        version.Version_Id_Spec_Code__c = 'VEC199_BRES';
        insert version;
        
        manager = criarUser();
        insert manager;

    }
    
    public static testMethod void testClassAbstract() {

        setup();

        Test.startTest();
        
        System.runAs(manager) {
            Apexpages.currentPage().getParameters().put('oppId', opp.Id);
            Apexpages.currentPage().getParameters().put('Id', q.Id);
            EstoqueInsertSfa2Controller ct2 = new EstoqueInsertSfa2Controller();
            ct2.selectVehicle();
            ct2.obterContaVendedor();
            String temp = ct2.vehiclePrefix;
        }   

        Test.stopTest();
        
    }
    
    public static testMethod void testCancelVehicle() {

        setup();
        
        Test.startTest();
        
        Apexpages.currentPage().getParameters().put('oppId', opp.Id);
        Apexpages.currentPage().getParameters().put('Id', q.Id);

        EstoqueInsertSfa2Controller ct = new EstoqueInsertSfa2Controller();
        ct.cancelVehico();
        
        Test.stopTest();

    }

    public static testMethod void testLoadVersions() {

        setup();
        
        Test.startTest();
        
        EstoqueInsertSfa2Controller.loadVersions(modelC.ID, '2005');
        
        Test.stopTest();
    }

    public static testMethod void testLoadOptionals() {

        setup();
        
        Test.startTest();

        EstoqueInsertSfa2Controller.loadOptionals(version.Id);
        
        Test.stopTest();
    }

    public static testMethod void testLoadMilesimes() {

        setup();
        
        Test.startTest();
        
        EstoqueInsertSfa2Controller.loadMilesimes(modelC.Id);
        
        Test.stopTest();
    }

    public static testMethod void testIncludeItemVehicle() {

        setup();
        
        Test.startTest();
        
        Apexpages.currentPage().getParameters().put('oppId', opp.Id);
        Apexpages.currentPage().getParameters().put('Id', q.Id);
        EstoqueInsertSfa2Controller ct = new EstoqueInsertSfa2Controller();
        
        ct.includeItemVehicle();
        Apexpages.currentPage().getParameters().put('veh', vre.Id);
        ct.includeItemVehicle();
        
        Test.stopTest();
    }
        
    public static testMethod void testIncludeItemGeneric() {

        setup();
        
        Test.startTest();
        
        Apexpages.currentPage().getParameters().put('oppId', opp.Id);
        Apexpages.currentPage().getParameters().put('Id', q.Id);

        EstoqueInsertSfa2Controller ct = new EstoqueInsertSfa2Controller();
        
        ct.includeItemGeneric();
        Apexpages.currentPage().getParameters().put('modelo', modelC.Id);
        Apexpages.currentPage().getParameters().put('milesime', '2005');
        Apexpages.currentPage().getParameters().put('versao', version.Id);
        Apexpages.currentPage().getParameters().put('cor', 'amarelo');
        Apexpages.currentPage().getParameters().put('estofado', 'estofado');
        Apexpages.currentPage().getParameters().put('harmonia', 'armonia');
        Apexpages.currentPage().getParameters().put('opcional', 'opt1,opt2,opt3');
        ct.includeItemGeneric();
        
        Test.stopTest();
    }

    public static testMethod void testIsCotacao(){

        setup();

        Test.startTest();

        EstoqueInsertSfa2Controller.isCotacao(vre.Id);

        Test.stopTest();
    }

    static User criarUser() {

        List<Account> updateAcc = new List<Account>();
        
        Account parentAcc = new Account (Name ='ParentAcc');
        parentAcc.ShippingStreet = 'Estrada da Aldeinha, 220 - Alphaville Emp.';
        parentAcc.ShippingCity = 'Barueri';
        parentAcc.ShippingState = 'SP';
        parentAcc.ShippingCountry = 'Brasil';
        parentAcc.ShippingPostalCode = '';
        parentAcc.Phone = '11999999999';
        parentAcc.ProfEmailAddress__c ='teste@teste.com'; 
        parentAcc.RecordTypeId = [select id from RecordType where developerName='Network_Acc' limit 1 ].id;

        updateAcc.add(parentAcc);
        
        Account acc = new Account(
            parentId = parentAcc.Id,
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '123ABC123',
            ReturnActiveToSFA__c = true,
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        
        updateAcc.add(acc);

        Insert updateAcc;
        
        Contact cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455'
        );
        Database.insert( cont );
        
        User userManager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='123ABC123',
            isCac__c = true
            
        );
        
        return userManager;
    }
}