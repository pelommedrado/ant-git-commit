/*  Mock of synchronization AdobeCampain
*************************************************************************************************************
10 May 2016 : Creation. Author : Donatien Veron (AtoS)
*************************************************************************************************************/
@IsTest 
global class Myr_AdobeCampain_MK implements HttpCalloutMock {
  
  global enum MODE {AC01001PRE, AC01001ACT, AC01001NWC, AC01101PRE, AC01102PRE, AC01103PRE, AC01201PRE, AC01202PRE, AC01101ACT, AC01102ACT, AC01103ACT, AC01104ACT, AC01201ACT, AC01202ACT, AC01101NWC, AC01102NWC, AC01103NWC, AC01104NWC, AC01201NWC, AC01202NWC}
  MODE mMode;
  global String template='';
  
  global Myr_AdobeCampain_MK(MODE iMode) { 
    mMode = iMode;
    template+='{';
	template+='"$schema": "http://json-schema.org/draft-04/schema#",';
	template+='"description": "Description of Real time WS",';
	template+='"response": {';
	template+='"Description": "<MESSAGE_LABEL>"';
	template+='"Code": "<CODE>"';
	template+='}';
	template+='}';
  }

  global HTTPResponse respond(HTTPRequest req) {
      String My_Code;
      String My_Message; 
      String My_Body;
      
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');

      if(mMode==MODE.AC01001PRE){
			My_Code='AC01001PRE';
      		My_Message='Success';
      }
      if( mMode == MODE.AC01001ACT) {
      		My_Code='AC01001ACT';
      		My_Message='Success';
      }
      if( mMode == MODE.AC01001NWC) {
      		My_Code='AC01001NWC';
      		My_Message='Success';
      }
      if( mMode == MODE.AC01101PRE) {
      		My_Code='AC01101PRE';
      		My_Message='Missing parameter(s) for account creation ';
      }
      if( mMode == MODE.AC01102PRE) {
      		My_Code='AC01102PRE';
      		My_Message='Invalid parameter for account creation ';
      }
      if( mMode == MODE.AC01103PRE) {
      		My_Code='AC01103PRE';
      		My_Message='Invalid mail format (format : xxx@yyy.com)';
      }
      if( mMode == MODE.AC01201PRE) {
      		My_Code='AC01201PRE';
      		My_Message='Update account fail (technical error description)';
      }
      if( mMode == MODE.AC01202PRE) {
      		My_Code='AC01202PRE';
      		My_Message='Delivery signal failure  (technical  error description)';
      }
      if( mMode == MODE.AC01101ACT) {
      		My_Code='AC01101ACT';
      		My_Message='Missing parameter(s) for account creation ';
      }
      if( mMode == MODE.AC01102ACT) {
      		My_Code='AC01102ACT';
      		My_Message='Invalid parameter for account creation ';
      }
      if( mMode == MODE.AC01103ACT) {
      		My_Code='AC01103ACT';
      		My_Message='Missing parameter(s) for vehicle creation ';
      }
      if( mMode == MODE.AC01104ACT) {
      		My_Code='AC01104ACT';
      		My_Message='Invalid parameter for vehicle creation ';
      }
      if( mMode == MODE.AC01201ACT) {
      		My_Code='AC01201ACT';
      		My_Message='Update account fail (technical  error description)';
      }
      if( mMode == MODE.AC01202ACT) {
      		My_Code='AC01202ACT';
      		My_Message='Update vehicle fail (technical  error description)';
      }
      if( mMode == MODE.AC01101NWC) {
      		My_Code='AC01101NWC';
      		My_Message='Missing parameter(s) for account creation ';
      }
      if( mMode == MODE.AC01102NWC) {
      		My_Code='AC01102NWC';
      		My_Message='Invalid parameter for account creation ';
      }
      if( mMode == MODE.AC01103NWC) {
      		My_Code='AC01103NWC';
      		My_Message='Missing parameter(s) for vehicle creation ';
      }
      if( mMode == MODE.AC01104NWC) {
      		My_Code='AC01104NWC';
      		My_Message='Invalid parameter for vehicle creation ';
      }
      if( mMode == MODE.AC01201NWC) {
      		My_Code='AC01201NWC';
      		My_Message='Update account fail (technical error description)';
      }
      if( mMode == MODE.AC01202NWC) {
      		My_Code='AC01202NWC';
      		My_Message='Update vehicle fail (technical error description)';
      }      
      
      My_Body=template.replace('<MESSAGE_LABEL>',My_Message);
      My_Body=template.replace('<CODE>',String.valueOf(My_Code));
      res.setBody(My_Body);
      res.setStatusCode(200);
      
      return res;
    }
}