public abstract class EstoqueAbstractController {
    
    public String vehiclePrefix {
        get {
            return VRE_VehRel__c.SObjectType.getDescribe().getKeyPrefix();
        }
    }
    
    public Opportunity oppInterest{
        get{
            String oppId = ApexPages.currentPage().getParameters().get('oppId');
            
            if(oppId != null){
                List<Opportunity> oppLst = [SELECT Id, CreatedDate, TransactionCode__c FROM Opportunity WHERE Id =: oppId LIMIT 1];
                return !oppLst.isEmpty() ? oppLst[0] : null;
            } else{
                return null;
            }
        }
    }
    
    public List<SelectOption> modelos { 
        get {
            
            Datetime dateWithKwid = Datetime.newInstance(2017, 08, 03);
            
            modelos = new List<SelectOption>();
            modelos.add(new SelectOption('', Label.VFP23_Choose_Vehicle.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_1, Label.VFP23_Vehicle_1.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_2, Label.VFP23_Vehicle_2.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_3, Label.VFP23_Vehicle_3.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_4, Label.VFP23_Vehicle_4.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_5, Label.VFP23_Vehicle_5.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_6, Label.VFP23_Vehicle_6.toUpperCase()));
            modelos.add(new SelectOption(Label.VFP23_Vehicle_7, Label.VFP23_Vehicle_7.toUpperCase()));
            modelos.add(new SelectOption('CAPTUR','CAPTUR'));
            modelos.add(new SelectOption('KWID','KWID'));
            /*if(oppInterest != null && oppInterest.TransactionCode__c != null){
            	modelos.add(new SelectOption('KWID','KWID'));
            } else if(oppInterest != null && oppInterest.CreatedDate <= dateWithKwid){
                modelos.add(new SelectOption('KWID','KWID'));
            }*/
            return modelos;
        }
        set; 
    }
    public String modelo { get;set; }
    
    public List<SelectOption> cores{ get;set; }
    public String cor { get;set; }
    
    public List<SelectOption> opcionais{ get;set; }
    public String opcional { get;set; }
    
    public List<SelectOption> versoes{ get;set; }
    public String versao { get;set; }
    
    public String versaoTabela { get;set; }
    public List<SelectOption> versaoTabelaList { get;set; }
    
    public List<ObjVeiculo> vehicleList { get;set; }
    
    public Account obterContaVendedor() {
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        Id AccID  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        return [
            SELECT Id, Sell_from_stock__c
            FROM Account
            WHERE Id =: AccID
        ];
    }
    
    /**
     * Selecionar veiculos
     */
    public void selectVehicle() {
        User currUser = Sfa2Utils.obterUsuarioLogado();
        System.debug('User: ' + currUser);
        Account parentAcc = Sfa2Utils.obterContaParente(currUser);
        System.debug('Parent Acc: ' + parentAcc);
        List<Account> accList = Sfa2Utils.obterListaConta(parentAcc);
        
        System.debug( '@@@\n' + Json.serializePretty( accList ) );
        
        System.debug('** Select Vehicle **');
        System.debug('CurrUser' + currUser);
        System.debug('ParentAcc' + parentAcc);
        System.debug('AccList' + accList);
        System.debug('Modelo' + modelo);
        
        vehicleList = obterVeiculo(accList);
        configCores(vehicleList);
        configVersao(vehicleList);
        configOpcoes(vehicleList);
        getVersions();
        System.debug('VehicleList' + vehicleList);
    }
    
    /**
     * Obter veiculos a parti da lista de contas.
     */    
    protected List<ObjVeiculo> obterVeiculo(List<Account> accList) {        
        String queryUrl = montarQuery(accList);
        System.debug('##### queryUrl ' + queryUrl);
        
        //obter lista de veiculos
        List<VRE_VehRel__c> vreL = Database.query(queryUrl);
        
        //lista com os ids dos veiculos obtidos
        Set<String> setVehicleId = new Set<String>();
        for(VRE_VehRel__c vre : vreL){
            setVehicleId.add(vre.VIN__r.Id);
        }
        
        //obter lista de VehicleBooking__c dos veiculos
        List<VehicleBooking__c> listVehicleBooking = 
            VFC85_VehicleBookingDAO.getInstance().findByVehicleAndStatus(setVehicleId, 'Active');
        
        //mapear veiculos com seus devidos VehicleBooking__c
        Map<String, VehicleBooking__c> veiculoBkMap = new Map<String, VehicleBooking__c>(); 
        for(VehicleBooking__c vb : listVehicleBooking) {
            veiculoBkMap.put(vb.Vehicle__c, vb);
        }
        
        return criarList(vreL, veiculoBkMap);
    }
    
    protected List<ObjVeiculo> criarList(List<VRE_VehRel__c> vreL, Map<String, VehicleBooking__c> veiculoBkMap) {
        List<ObjVeiculo> objL = new List<ObjVeiculo>();
        for(VRE_VehRel__c vre : vreL) {
            ObjVeiculo ob = new ObjVeiculo();
            ob.v = vre;
            ob.negociar = false;
            
            if(vre.VIN__r.Status__c == 'Available' || vre.VIN__r.Status__c == 'In Transit' || vre.VIN__r.Status__c == 'Stock') {
                ob.negociar = true;
            }
            
            VehicleBooking__c vb = veiculoBkMap.get(vre.VIN__r.Id);
            if(vb != null) {
                ob.negociar = false;
            }
            
            objL.add(ob);
        }
        
        return objL;
    }
    
    /**
     * Montar query de pesquisa de veiculos do estoque
     */
    protected String montarQuery(List<Account> accList) {
        return 
            'SELECT Id, Account__r.IDBIR__c,'   + 
            'Days_in_Stock__c,'             + 
            'VIN__r.Id,'                +
            'VIN__r.Color__c,'              + 
            'VIN__r.Name,'                  +
            'VIN__r.Booking_User__r.Name,'  + 
            'VIN__r.Model__c,'              +
            'VIN__r.Optional__c,'           +
            'VIN__r.Price__c,'              +
            'VIN__r.Status__c,'             +
            'VIN__r.State__c,'              +
            'VIN__r.Version__c,'            +
            'VIN__r.Year_Model_Version__c ' +
            'FROM VRE_VehRel__c '           +
            
            'WHERE VIN__r.Model__c LIKE \'%' + modelo + '%\' ' +
            ( String.isNotBlank(cor)        ? 'AND VIN__r.Color__c    LIKE \'%' + cor       						+ '%\'' : '' ) + 
            ( String.isNotBlank(versao)     ? 'AND VIN__r.Version__c  LIKE \'%' + versao.replace('\'', '\\\'')  	+ '%\'' : '' ) + 
            ( String.isNotBlank(opcional)   ? 'AND VIN__r.Optional__c LIKE \'%' + opcional.replace('\'', '\\\'')   	+ '%\'' : '' ) +
            montarQueryInAccount(accList)                       +
            ' AND VIN__r.Status__c != \'Billed\''               +
            ' AND VIN__r.Status__c != \'Immobilized\''          +
            ' AND VIN__r.Is_Available__c=true ' 				+
            ' AND VRE_VehRel__c.Status__c = \'Active\''         +
            ' ORDER BY VRE_VehRel__c.Days_in_Stock__c DESC';
    }
    
    /**
     * Montar query In com a lista de contas
     */
    private String montarQueryInAccount(List<Account> accList) {
        String inStg = ' AND Account__c in(';
        
        for(Account acc : accList) {
            //e o ultimo elemento da lista?
            if(accList.get(accList.size()-1).Id == acc.Id) {
                inStg += '\'' + acc.id + '\')';
                
            } else {
                inStg += '\'' + acc.id + '\',';
                
            }
        }
        
        return inStg;
    }
    
    private void getVersions() {
        System.debug('### Modelo: ' + modelo);
        
        Map<String, Product2> mapVersion = new Map<String, Product2>();  
        List<Product2> lstVersion = 
            VFC22_ProductDAO.getInstance().fetchProductsByModel(modelo);  
        
        for(Product2 auxLstVersion : lstVersion) {
            if(auxLstVersion.Version__c != null) {
                mapVersion.put(auxLstVersion.Version__c, auxLstVersion); 
            }                  
        }
        
        versaoTabelaList = new List<SelectOption>();
        versaoTabelaList.add(new SelectOption('', 'Selecionar')); 
        
        for(Product2 versionAux : mapVersion.values()) {
            versaoTabelaList.add(new SelectOption(versionAux.Id, versionAux.Version__c));
        }   
    }
    
    /**
     * Configurar combo de opcoes
     */     
    public void configOpcoes(List<ObjVeiculo> veiculos) {
        Set<String> optiStg = new Set<String>();
        
        for(ObjVeiculo ve : veiculos) {
            optiStg.add(ve.v.VIN__r.Optional__c);
        }
        
        System.debug('Optional:' + optiStg);
        
        opcionais = new List<SelectOption>();
        opcionais.add(new SelectOption('', Label.VFP23_Choose_Optional.toUpperCase()));
        for(String c : optiStg) {
            if(c != null)
            opcionais.add(new SelectOption(c, c));
        }
    }
    
    /**
     * Configurar combo de versoes
     */     
    public void configVersao(List<ObjVeiculo> veiculos) {
        Set<String> versaoStg = new Set<String>();
        
        for(ObjVeiculo ve : veiculos) {
            versaoStg.add(ve.v.VIN__r.Version__c);
        }
        
        System.debug('Versoes:' + versaoStg);
        
        versoes = new List<SelectOption>();
        versoes.add(new SelectOption('', Label.VFP23_Choose_Version.toUpperCase()));
        for(String c : versaoStg) {
            versoes.add(new SelectOption(c, c));
        }
    }
    
    /**
     * Configurar combo de cores
     */     
    public void configCores(List<ObjVeiculo> veiculos) {
        Set<String> coresStg = new Set<String>();
        
        for(ObjVeiculo ve : veiculos) {
            coresStg.add(ve.v.VIN__r.Color__c);
        }
        
        System.debug('Cores:' + coresStg);
        
        cores = new List<SelectOption>();
        cores.add(new SelectOption('', Label.VFP23_Choose_Color.toUpperCase()));
        for(String c : coresStg) {
            cores.add(new SelectOption(c, c));
        }
    }
    
    public class ObjVeiculo {
        public VRE_VehRel__c v { get;set; }
        public Boolean negociar { get;set; }
    }
}