@isTest(seeAllData = true)
private class  Rforce_RcArch_Test {

    static testMethod void myRCArchTests() {
        myRCArchWebServiceTests();

    }

    static private void myRCArchWebServiceTests() {

        Test.setMock(WebServiceMock.class, new  Rforce_RcArch_WebServiceMock_Test());
        //Rforce_WS_RCArch_UtilsV2 RCArchUtils = new Rforce_WS_RCArch_UtilsV2();
        Rforce_Utils08_RCArchivage RCArchUtils2 = new Rforce_Utils08_RCArchivage();
        /* Create a vehicle standard controller */
        /* Prepare the vehicule */
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'VF1BGRG06GGG85766';
        v.CountryOfDelivery__c = 'Brazil';
        insert v;
        Id RTID_COMPANY = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
        Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '+0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes',Country__c='France');
        insert Acc;
        Rforce_VehicleAttributes vehController = new Rforce_VehicleAttributes(new ApexPages.StandardController(v));
        Rforce_AccountAttributes accController = new Rforce_AccountAttributes(new ApexPages.StandardController(acc));

        /* Prepare the case */
        Case c = new Case();

        /* Prepare the attachment */
        Attachment a = new Attachment();
        a.Name = 'test';

        /* DO NOT ISERT IT in database : salesforce forgot callout calls with an opened transaction. */

        try {
            Rforce_RcArchivage.caseId[] caseListRespController = RCArchUtils2.getCaseIdList(v.Name, v.CountryOfDelivery__c);

        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }

        try {
            Rforce_RcArchivage.caseDetails caseDetailsRespController = RCArchUtils2.getCaseDetails(c.Id, v.CountryOfDelivery__c);
        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }

     
        try {
        String bcid='';
        String pcid='';
            Rforce_RcArchivage.caseId[] caseListResponse = RCArchUtils2.getCaseIdListAccount(acc.FirstName, acc.LastName, acc.ShippingPostalCode, acc.ShippingCity, acc.Phone, acc.Name, bcid, pcid);
        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }
        
        try {
        String bcid='';
        String pcid='';
        String countrycode='FRA';
        String localId='';
        String email='';
            Rforce_RcArchivageEnhanced.caseDetailsList[] caseListResponse = RCArchUtils2.getCaseIdListAccountConcat(acc.FirstName, acc.LastName, acc.ShippingPostalCode, acc.ShippingCity, acc.Phone, acc.Name, bcid, pcid,acc.Country__c,localId,email);
        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }

       
        try {
            Rforce_RcArchivage.attachmentDetailsList[] attachmentListResp = RCArchUtils2.getAttachmentDetailsList(v.CountryOfDelivery__c, c.Id);
        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }

        try {

            String attachContentResp1 = RCArchUtils2.getAttachmentContent('test', 'test', 'test');
        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }

        try {

            String attachContentResp1 = RCArchUtils2.getXmlDetails('test', 'test');

        } catch (CallOutException e) {
            system.Debug(e.getMessage());
        }
             try{        
             Rforce_RcArchivage.getCaseDetailsResponse casRes = new Rforce_RcArchivage.getCaseDetailsResponse();        
             Rforce_RcArchivage.getCaseIdListResponse1 casRes6 = new Rforce_RcArchivage.getCaseIdListResponse1();        
             Rforce_RcArchivage.getCaseIdListResponse casRes9 = new Rforce_RcArchivage.getCaseIdListResponse();        
             Rforce_RcArchivage.getAttachmentContent casResAtt = new Rforce_RcArchivage.getAttachmentContent();        
             Rforce_RcArchivage.getAttachmentContentResponse casResAtt2 = new Rforce_RcArchivage.getAttachmentContentResponse();        
             Rforce_RcArchivage.getXmlDetailsResponse casResAtt3 = new Rforce_RcArchivage.getXmlDetailsResponse();        
             Rforce_RcArchivage.attachmentDetailsList casResAtt4 = new Rforce_RcArchivage.attachmentDetailsList();        
             Rforce_RcArchivage.GetRCDataError casResAtt5 = new Rforce_RcArchivage.GetRCDataError();        
             Rforce_RcArchivage.caseDetails casResAtt1 = new Rforce_RcArchivage.caseDetails();        
             Rforce_RcArchivage.caseId casResId = new Rforce_RcArchivage.caseId();        
             Rforce_RcArchivage.getAttachmentDetailsListResponse casRespo = new Rforce_RcArchivage.getAttachmentDetailsListResponse();                
             }        
             catch(CallOutException e){        
             system.Debug(e.getMessage());        
             }
             try{        
             Rforce_RcArchivageEnhanced.getCaseDetailsResponse casRes = new Rforce_RcArchivageEnhanced.getCaseDetailsResponse();        
             Rforce_RcArchivageEnhanced.getCaseIdListResponse1 casRes6 = new Rforce_RcArchivageEnhanced.getCaseIdListResponse1();        
             Rforce_RcArchivageEnhanced.getCaseIdListResponse casRes9 = new Rforce_RcArchivageEnhanced.getCaseIdListResponse();        
             Rforce_RcArchivageEnhanced.getAttachmentContent casResAtt = new Rforce_RcArchivageEnhanced.getAttachmentContent();        
             Rforce_RcArchivageEnhanced.getAttachmentContentResponse casResAtt2 = new Rforce_RcArchivageEnhanced.getAttachmentContentResponse();        
             Rforce_RcArchivageEnhanced.getXmlDetailsResponse casResAtt3 = new Rforce_RcArchivageEnhanced.getXmlDetailsResponse();        
             Rforce_RcArchivageEnhanced.attachmentDetailsList casResAtt4 = new Rforce_RcArchivageEnhanced.attachmentDetailsList();        
             Rforce_RcArchivageEnhanced.GetRCDataError casResAtt5 = new Rforce_RcArchivageEnhanced.GetRCDataError();        
             Rforce_RcArchivageEnhanced.caseDetails casResAtt1 = new Rforce_RcArchivageEnhanced.caseDetails(); 
             Rforce_RcArchivageEnhanced.getCaseDetails casResAtts1 = new Rforce_RcArchivageEnhanced.getCaseDetails(); 
             Rforce_RcArchivageEnhanced.attachmentContentDetails casResAttss1 = new Rforce_RcArchivageEnhanced.attachmentContentDetails();      
             Rforce_RcArchivageEnhanced.getCaseIdListAccount gCaseAcc = new Rforce_RcArchivageEnhanced.getCaseIdListAccount();
             Rforce_RcArchivageEnhanced.getXmlDetails gXmlAcc = new Rforce_RcArchivageEnhanced.getXmlDetails();
             Rforce_RcArchivageEnhanced.caseDetailsList casResId = new Rforce_RcArchivageEnhanced.caseDetailsList ();  
             Rforce_RcArchivageEnhanced.getCaseIdList gList = new Rforce_RcArchivageEnhanced.getCaseIdList();   
             Rforce_RcArchivageEnhanced.getCaseIdListResponse2 gListRes = new Rforce_RcArchivageEnhanced.getCaseIdListResponse2();
             Rforce_RcArchivageEnhanced.getAttachmentDetailsList gAttachList = new Rforce_RcArchivageEnhanced.getAttachmentDetailsList();
             Rforce_RcArchivageEnhanced.getAttachmentDetailsListResponse casRespo = new Rforce_RcArchivageEnhanced.getAttachmentDetailsListResponse();                
             }        
             catch(CallOutException e){        
             system.Debug(e.getMessage());        
             }            

    }
}