@RestResource(urlMapping='/services/*')
global class InterviewRespServicesWS {

    @HttpGet
    global static InterviewResponse__c getServicesInterview() {
        RestRequest request = RestContext.request;
        String interviewResId = request.requestURI.substring(
        request.requestURI.lastIndexOf('/')+1);
        InterviewResponse__c result = InterviewResponseDAO.getInterviewResById(interviewResId);
        return result;
    }

    global class ResponseInterviewService{
        public String message;
        public String error;
        public Id interviewResponseId;
    }

    @HttpPost
    global static ResponseInterviewService createServicesInterview(Boolean has_dealership_get_in_touch, Boolean has_user_had_to_return_by_the_same_problebm,
                                         	String reason_of_the_return, Integer dealership_rating, 
                                         	String user_experience, Boolean has_doubt_suggestion_complaint,
                                            String client_user_id) {

        System.debug(client_user_id);

        List<Interview__c> allInterviewTypes =  [SELECT Id, Name, Description__c 
                                                FROM Interview__c 
                                                WHERE Name  = 'Pesquisa PV'
                                                LIMIT 1];

        List<Account> accountToAssing = [SELECT Id FROM Account WHERE Id =: client_user_id];

        String dealerGetInTouch     = has_dealership_get_in_touch ? 'Sim.' : 'Não.';
        String userSameProblem 	    = has_user_had_to_return_by_the_same_problebm ? 'Sim.' : 'Não.'; //CONFERIR CONFERIR CONFERIR;
        String returnReason		    = reason_of_the_return;
        //Common
        Integer dealerRate          = dealership_rating;
        String userExperience       = user_experience;
        Boolean getInTouch          = has_doubt_suggestion_complaint;

        InterviewResponse__c newInterviewRes = new InterviewResponse__c();
        
        newInterviewRes.DealerGetInTouchAfterService__c         = dealerGetInTouch;
        newInterviewRes.CustomerReturnedRenaultRepairShop__c    = userSameProblem;
        newInterviewRes.ReasonReturn__c                         = returnReason;

        newInterviewRes.ReccomendDealerNote__c                  = dealerRate;
        newInterviewRes.CustomerExperience__c                   = userExperience;
        newInterviewRes.CustumerGetInTouch__c                   = getInTouch;


        //FIXED
        if(!accountToAssing.isEmpty()){
            newInterviewRes.AccountId__c                        = accountToAssing[0].id;
        }
        newInterviewRes.InterviewId__c                          = allInterviewTypes[0].Id;
        newInterviewRes.Origin__c                               = 'Facebook';
        newInterviewRes.AnswerDate__c                           = System.now();
        //UNUSED
        //newInterviewRes.AccountId__c                            = accountId;
        //newInterviewRes.AnswerDate__c                           = answerDate;
        //newInterviewRes.AnswerLink__c                           = answerLink;
        //newInterviewRes.ContactId__c                            = contactId;
        //newInterviewRes.ContactChannel__c                       = contactChannel;
        /*newInterviewRes.TestDriveOffered__c                     = testDriveOfferedText;
        newInterviewRes.VehicleDeliveriedDateCombined__c          = vehicleDelivery;*/

        ResponseInterviewService serviceReturn = new ResponseInterviewService();

        try{
            Database.insert(newInterviewRes);
        }
        catch(Exception ex){
            serviceReturn.message = 'Error';
            serviceReturn.error = ex.getMessage();
            return serviceReturn;
        }
        serviceReturn.message = 'Sucess';
        serviceReturn.error = '';
        serviceReturn.interviewResponseId = newInterviewRes.Id;
        return serviceReturn;
    }

}