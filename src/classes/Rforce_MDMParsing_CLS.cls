/** @author: Vetrivel Sundararajan
  * @date: 06/08/2014
  * @description: This apex class has below methods 
  * @getVehiclesDetails() 
  * @Pharse the MDM Response xml to fetch the Vehicle Details and generate List of Rforce_MDM_Vehicle as response.
  * @getMDMVehiclesDetails()
  * @Pharse the MDM Response xml to fetch the Vehicle Details and generate List of Rforce_MDMPartyOverview_CLS.Vehicle as response.
 **/  

public class Rforce_MDMParsing_CLS {

  /** 
    * @author Vetrivel Sundararajan 
    * @date 29/01/2015 
    * @description Generate the List for Vehicles in the Rforce_MDMVehicle_CLS from the webservice and return the response to the calling function. 
    */
  public List<Rforce_MDMVehicle_CLS> getVehiclesDetails(String strMdmXml, String strPartyID) {
   
   system.debug('CLASS:Rforce_MDMParsing_CLS-->METHOD:getVehiclesDetails()');          
   List<Rforce_MDMVehicle_CLS>  vehicleDataValueList=new List<Rforce_MDMVehicle_CLS>();
   Dom.Document doc = new Dom.Document();
   doc.load(strMdmXml);
   Dom.XMLNode rootElement = doc.getRootElement();
   Rforce_MDMVehicle_CLS vehicleDataValue=new Rforce_MDMVehicle_CLS();   
    
    for (Dom.XmlNode assets : rootElement.getChildElements()) {
         for (Dom.XmlNode child : assets.getChildElements()) {
                              
            if (child.getname() == 'Vehicle') {
                vehicleDataValue=new Rforce_MDMVehicle_CLS();

               for (dom.XmlNode vehiclechild: child.getchildren() ) {
                    
                    vehicleDataValue.idClient = strPartyID;
                    if (vehiclechild.getName()=='VehicleIdentificationNumber'){
                        vehicleDataValue.vin=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='Brand'){
                        vehicleDataValue.brandCode=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='Model'){
                        vehicleDataValue.modelCode=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='ModelLabel'){
                        vehicleDataValue.modelLabel=vehiclechild.getText();
                    } 
                 
                    String strVehicleDeliveryDate='';
                    String deliveryDateFormat='';

                    if (vehiclechild.getName()=='RegistrationDate'){
                        strVehicleDeliveryDate=vehiclechild.getText();
                    }
                    if (strVehicleDeliveryDate!=null && strVehicleDeliveryDate.length() >0) {
                        Date dt = Date.valueOf(strVehicleDeliveryDate);
                        deliveryDateFormat= String.valueOf(dt);
                        DateTime d = Date.valueOf(deliveryDateFormat);
                        deliveryDateFormat= d.format('dd/MM/yyyy'); 
                        vehicleDataValue.firstRegistrationDate=deliveryDateFormat;
                    }else {
                        vehicleDataValue.firstRegistrationDate=strVehicleDeliveryDate;
                    }
                    
                    if (vehiclechild.getName()=='PartyVehicleType'){
                        vehicleDataValue.vehicleType=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='RegistrationNumber'){
                        vehicleDataValue.registration=vehiclechild.getText();
                    }
                    vehicleDataValue.versionLabel ='';
                    vehicleDataValue.possessionBegin='';
                    
                    String strStartDate='';
                    String startDateFormat='';
  
                    if (vehiclechild.getName()=='StartDate'){
                        strStartDate=vehiclechild.getText();
                    }
                    if (strStartDate!=null && strStartDate.length() >0) {
                        Date dt = Date.valueOf(strStartDate);
                        startDateFormat= String.valueOf(dt);
                        DateTime d = Date.valueOf(startDateFormat);
                        startDateFormat= d.format('dd/MM/yyyy'); 
                        vehicleDataValue.startDate=startDateFormat;
                    }else {
                        vehicleDataValue.startDate=strStartDate;
                    }
                      
                    if (vehiclechild.getName()=='Endate'){
                        vehicleDataValue.EndDate=vehiclechild.getText();
                    }
                }
                       vehicleDataValueList.add(vehicleDataValue);
             }

         }
    }  
    
    system.debug('CLASS:Rforce_MDMParsing_CLS-->METHOD:getVehiclesDetails()--> vehicleDataValueList size :'+ vehicleDataValueList.size());
    system.debug('CLASS:Rforce_MDMParsing_CLS-->METHOD:getVehiclesDetails()--> vehicleDataValueList Value :'+ vehicleDataValueList);      
 
   
   return vehicleDataValueList;
  }
 
   /** 
    * @author Vetrivel Sundararajan 
    * @date 29/01/2015 
    * @description Generate the List for Vehicles in the Rforce_MDMPartyOverview_CLS.Vehicle from the webservice and return the response to the calling function. 
    */
  

  public List<Rforce_MDMPartyOverview_CLS.Vehicle> getMDMVehiclesDetails(String strMdmXml, String strPartyID) {
             
   List<Rforce_MDMPartyOverview_CLS.Vehicle>  vehicleDataValueList=new List<Rforce_MDMPartyOverview_CLS.Vehicle>();
   Dom.Document doc = new Dom.Document();
   doc.load(strMdmXml);
   Dom.XMLNode rootElement = doc.getRootElement();
   Rforce_MDMPartyOverview_CLS.Vehicle vehicleDataValue=new Rforce_MDMPartyOverview_CLS.Vehicle();   
    
    for (Dom.XmlNode assets : rootElement.getChildElements()) {
         for (Dom.XmlNode child : assets.getChildElements()) {
                              
            if (child.getname() == 'Vehicle') {
                vehicleDataValue=new Rforce_MDMPartyOverview_CLS.Vehicle();

               for (dom.XmlNode vehiclechild: child.getchildren() ) {
                    
                    vehicleDataValue.idClient = strPartyID;
                    if (vehiclechild.getName()=='VehicleIdentificationNumber'){
                        vehicleDataValue.vin=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='Brand'){
                        vehicleDataValue.brandCode=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='BrandLabel'){
                        vehicleDataValue.brandLabel=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='Model'){
                        vehicleDataValue.modelCode=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='ModelLabel'){
                        vehicleDataValue.modelLabel=vehiclechild.getText();
                    } 
                    if (vehiclechild.getName()=='DeliveryDate') {
                        vehicleDataValue.deliveryDate=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='RegistrationDate'){
                        vehicleDataValue.firstRegistrationDate=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='CurrentMileage'){
                         vehicleDataValue.currentMileage=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='CurrentMileageDate'){
                         vehicleDataValue.currentMileageDate=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='NewVehicle'){
                         vehicleDataValue.newVehicle=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='PartyVehicleType'){
                         vehicleDataValue.vehicleType=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='RegistrationNumber'){
                         vehicleDataValue.registration=vehiclechild.getText();
                    }
                       vehicleDataValue.versionLabel ='';
                       vehicleDataValue.possessionBegin='';
                    
                    if (vehiclechild.getName()=='StartDate'){
                         vehicleDataValue.startDate=vehiclechild.getText();
                    }
                    if (vehiclechild.getName()=='Endate'){
                        vehicleDataValue.EndDate=vehiclechild.getText();
                    }
                }
                       vehicleDataValueList.add(vehicleDataValue);
             }

         }
    }  
    
    system.debug('CLASS:Rforce_MDMParsing_CLS-->METHOD:getMDMVehiclesDetails()--> vehicleDataValueList size :'+ vehicleDataValueList.size());
    system.debug('CLASS:Rforce_MDMParsing_CLS-->METHOD:getMDMVehiclesDetails()--> vehicleDataValueList Value :'+ vehicleDataValueList);      
 
   
   return vehicleDataValueList;
  } 
}