/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto Contact.
* @author Felipe Jesus Silva.
*/
public class VFC42_ContactBO
{
	private static final VFC42_ContactBO instance = new VFC42_ContactBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC42_ContactBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC42_ContactBO getInstance()
	{
		return instance;
	}
	
	/**
	* Obtém o mapeamento dos contatos pelo id da conta.
	* Como contas clientes possuem somente 1 contato, cada entrada no map estará associada a um id de conta.
	* @param setAccountId Um set contendo os ids das contas.
	* @return Um map com a seguinte estrutura:
	* Chave: O id da conta.
	* Valor: O contato relacionado a essa conta.
	*/
	public Map<String, Contact> getMappingByAccount(Set<String> setAccountId)
	{
		List<Contact> lstSObjContact = null;
		Map<String, Contact> mapSObjContact = new Map<String, Contact>();
		
		lstSObjContact = VFC41_ContactDAO.getInstance().findByAccountId(setAccountId);
		
		for(Contact sObjContact : lstSObjContact)
		{
			mapSObjContact.put(sObjContact.AccountId, sObjContact);
		}
		
		return mapSObjContact;
	}

}