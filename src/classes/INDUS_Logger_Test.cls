/**
	Test class to check INDUS_Logger_Cls
	@date 06.04.2016, R16.04, v1.0, quick init to cover funcitons not covered by MyRenault. To be completed later
**/
@isTest
private class INDUS_Logger_Test {

	static testMethod void test_InsertGroupedLog() {
		String runId = 'RUNID_TEST001';
		String parentLogId = null;
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String ApexClass = 'INDUS_Logger_Cls_UnitTest';
		String Function = 'test_InsertGroupedLog';
		String Record = 'record';
		String ExceptType = 'OK';
		String Message = 'insert parent log';
		String error = 'no error';
		Id logParent = INDUS_Logger_CLS.addGroupedLog (runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, error);
		parentLogId = logParent;
		Id logChild = INDUS_Logger_CLS.addGroupedLog (runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, error);

		system.assertEquals( 2, [SELECT Id FROM Logger__c].size());
		system.assertEquals( 2, [SELECT Id FROM Logger__c WHERE RunId__c = :runId].size());
		system.assertEquals( 1, [SELECT Id FROM Logger__c WHERE Id = :logParent AND ParentLog__c = null].size());
		system.assertEquals( 1, [SELECT Id FROM Logger__c WHERE ParentLog__c = :logParent].size());
	}

	//Crash when trying to insert a log child. Check that a specifi exception log is generated for this case
	static testMethod void test_addLogChild_Crash() {
		Id parentLogId = '0017E0000063TB3QAM'; //fake logger account: this is not an error. This is to test the exception ...
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String ApexClass = 'INDUS_Logger_Cls_UnitTest';
		String Function = 'test_addLogChild_Crash';
		String Record = 'record';
		String ExceptType = 'OK';
		String Message = 'insert parent log';
		INDUS_Logger_CLS.ErrorLevel errorLevel = INDUS_Logger_CLS.ErrorLevel.Info;
		INDUS_Logger_CLS.addLogChild (parentLogId, project, ApexClass, Function, Record, ExceptType, Message, errorLevel);
		system.assertEquals( 1, [SELECT Id FROM Logger__c].size());
		Logger__c logIndus = [SELECT Id, Project_Name__c, Apex_Class__c, Error_Level__c FROM Logger__c];
		system.assertEquals( INDUS_Logger_CLS.ProjectName.INDUS.name(), logIndus.Project_Name__c);
		system.assertEquals( INDUS_Logger_CLS.class.getName(), logIndus.Apex_Class__c);
		system.assertEquals( INDUS_Logger_CLS.ErrorLevel.Warn.name(), logIndus.Error_Level__c);
	}

	//Crash when trying to insert a log child. Check that a specifi exception log is generated for this case
	static testMethod void test_addGroupedLogFull() {
		//Prepare some data
		Account acc = new Account( 
			Name='Fake account', PersEmailAddress__c='test.email@test.com', ProfEmailAddress__c ='test.email@test.com', 
			HomePhone__c='02352685487', 
			ShippingCity  = 'Paris', ShippingCountry='France', ShippingStreet = 'street',
			BillingCity = 'Paris', BillingCountry='France', BillingStreet = 'street');
		insert acc;
		VEH_Veh__c veh = new VEH_Veh__c( Name='VFTGY6756YHUJ7654');
		insert veh;
		VRE_VehRel__c vehRel = new VRE_VehRel__c( Account__c=acc.Id, VIN__c=veh.Id);
		insert vehRel;

		//Prepare the log
		String runId = 'RUNID-0003';
		Id parentLogId = null;
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String ApexClass = 'INDUS_Logger_Cls_UnitTest';
		String Function = 'test_addGroupedLogFull';
		String Record = 'record';
		String ExceptType = 'OK';
		String Message = 'insert grouped log full';
		String errorLevel = INDUS_Logger_CLS.ErrorLevel.Info.name();
		String accountId = acc.Id;
		String relationId = vehRel.Id;
		String vehicleId = veh.Id;
		Long Begin_Time = system.now().getTime();
		Long End_Time = system.now().getTime();
		
		INDUS_Logger_CLS.addGroupedLogFull (runId, parentLogId, project, ApexClass, Function, ExceptType, Message, errorLevel, accountId, relationId, vehicleId, Begin_Time, End_Time, Record);
		
		system.assertEquals( 1, [SELECT Id FROM Logger__c].size());
		Logger__c logIndus = [SELECT Id, Account__c, Vehicle__c, Vehicle_Relationship__c, Duration__c FROM Logger__c];
		system.assertEquals( acc.Id, logIndus.Account__c);
		system.assertEquals( veh.Id, logIndus.Vehicle__c);
		system.assertEquals( vehRel.Id, logIndus.Vehicle_Relationship__c);
		system.assertEquals( End_Time - Begin_Time, logIndus.Duration__c);
	}

	//test supervision log parent
	static testMethod void test_addGroupedLogSupParent() {
		String runId = 'RUNID_TEST001';
		String parentLogId = null;
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String ApexClass = 'INDUS_Logger_Cls_UnitTest';
		String Function = 'test_addGroupedLogSupParent';
		String Record = 'record';
		String ExceptType = 'exception type';
		String Message = 'insert parent log';
		String error = 'no error';
		String Job_Id = 'JOBID-0008';
		Datetime Dt_Start_Time = system.now();
		Datetime Completed_Date = system.now();
		String Status = 'OK';
		Integer Total_Job_Items = 100;
		Integer Job_Items_Processed = 100;
		Integer Number_Of_Errors = 0;
		String Extended_Status = null;
		Integer Total_Scope = 100;
		Integer Nb_Records_Created = 10;
		Integer Nb_Records_Updated = 80;
		Integer Nb_Records_Deleted = 10;
		Integer Nb_Error_Insert = 0;
		Integer Nb_Error_Update = 0;
		Integer Nb_Error_Delete = 0;
		Integer Nb_Email_Sent = 15;
		Integer Nb_Error_Email = 5;

		INDUS_Logger_CLS.addGroupedLogSupParent (runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, error, Job_Id, Completed_Date, Status, Total_Job_Items, Job_Items_Processed, Number_Of_Errors, Extended_Status, Dt_Start_Time, Total_Scope, Nb_Records_Created, Nb_Records_Updated, Nb_Records_Deleted, Nb_Error_Insert, Nb_Error_Update, Nb_Error_Delete, Nb_Email_Sent, Nb_Error_Email);

		system.assertEquals( 1, [SELECT Id FROM Logger__c].size());
		Logger__c logIndus = [SELECT Id, RecordTypeId, Nb_Records_Created__c, Nb_Records_Updated__c, Nb_Records_Deleted__c
							, Nb_Error_Insert__c, Nb_Error_Update__c, Nb_Error_Delete__c
							, Nb_Email_Sent__c, Nb_Error_Email__c
							 FROM Logger__c];
		system.assertEquals( [select Id from recordtype where developername = 'Application' and  SobjectType = 'Logger__c' ].Id, logIndus.RecordTypeId);
		system.assertEquals( Nb_Records_Created, logIndus.Nb_Records_Created__c);
		system.assertEquals( Nb_Records_Updated, logIndus.Nb_Records_Updated__c);
		system.assertEquals( Nb_Records_Deleted, logIndus.Nb_Records_Deleted__c);
		system.assertEquals( Nb_Error_Insert, logIndus.Nb_Error_Insert__c);
		system.assertEquals( Nb_Error_Update, logIndus.Nb_Error_Update__c);
		system.assertEquals( Nb_Error_Delete, logIndus.Nb_Error_Delete__c);
		system.assertEquals( Nb_Email_Sent, logIndus.Nb_Email_Sent__c);
		system.assertEquals( Nb_Error_Email, logIndus.Nb_Error_Email__c);

	}
	
	//test supervision log child
	static testMethod void test_addGroupedLogSupFils() {
		String runId = 'RUNID_TEST001';
		String parentLogId = null;
		INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
		String ApexClass = 'INDUS_Logger_Cls_UnitTest';
		String Function = 'test_addGroupedLogSupFils';
		String Record = 'record';
		String ExceptType = 'exception type';
		String Message = 'insert log';
		String error = INDUS_Logger_CLS.ErrorLevel.Warn.name();

		INDUS_Logger_CLS.addGroupedLogSupFils(runId, parentLogId, project, ApexClass, Function, Record, ExceptType, Message, error);

		system.assertEquals( 1, [SELECT Id FROM Logger__c].size());
		Logger__c logIndus = [SELECT Id, RecordTypeId FROM Logger__c];
		system.assertEquals( [select Id from recordtype where developername = 'Application' and  SobjectType = 'Logger__c' ].Id, logIndus.RecordTypeId);
	}


	
}