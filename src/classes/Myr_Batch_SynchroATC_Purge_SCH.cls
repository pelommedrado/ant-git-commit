/*  Purge of Synchro_ATC
*************************************************************************************************************
25 August 2014  : Creation
*************************************************************************************************************/
global class Myr_Batch_SynchroATC_Purge_SCH implements Schedulable {
    global void execute(SchedulableContext sc) {
        Myr_Batch_SynchroATC_Purge_BAT batch = new Myr_Batch_SynchroATC_Purge_BAT();
        Database.executebatch( batch );
    }
}