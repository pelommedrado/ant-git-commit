global class VFC138_VehicleBookingBatch  implements Database.Batchable<sObject>, Database.Stateful
{
	/**
	 * Start method: fetch records to be processed
	 */
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		system.debug(LoggingLevel.INFO, '*** VFC138_VehicleBookingBatch.start()');
		String query = VFC85_VehicleBookingDAO.getInstance().buildVehicleBookingQueryForBatchProcess(); 
		return Database.getQueryLocator(query);
	}
	
	/**
	 * Execute method: do the work
	 */
	global void execute(Database.BatchableContext BC, List<sObject> lstObjects)
	{
		system.debug(LoggingLevel.INFO, '*** VFC124_CampaignBatch.execute()');
		
		Datetime dtNow = Datetime.now();
        List<VehicleBooking__c> lstToExpire = new List<VehicleBooking__c>();

		for (sObject vehicleBookingRecord : lstObjects)
		{
            VehicleBooking__c vehicleBooking = (VehicleBooking__c)vehicleBookingRecord;
            
            
            if(vehicleBooking.ExpireDate__c < Datetime.now()){
            	vehicleBooking.Status__c = 'Expired';
            	lstToExpire.add(vehicleBooking);
            }
		}
		
		if (lstToExpire.size() > 0)
		{
			system.debug(LoggingLevel.INFO, '*** lstToExpire = '+lstToExpire);
			update lstToExpire;
		}	
	}
	
	/**
	 * Finish method: execute post processing
	 */
	global void finish(Database.BatchableContext BC)
	{
		system.debug(LoggingLevel.INFO, '*** VFC138_VehicleBookingBatch.finish()');
        system.debug(LoggingLevel.INFO, '*** Processo concluído');
	}
}