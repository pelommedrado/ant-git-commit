@isTest
public class getvoucherControllerTest {
    
    public static testMethod void test(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Opportunity opp = moc.criaOpportunity();
        Insert opp;
       
        PageReference pageRef = new PageReference('/apex/getvoucher');
        Test.setCurrentPage(pageRef);
        
        getvoucherController controller = new getvoucherController();
        pageRef.getParameters().put('oppId', opp.Id);
        
        List<Opportunity> oppList = controller.lsOpp;
        
        // testa retorno da pagina
        String page = controller.consultarVouchers().getUrl();
        system.assertEquals('/apex/getvoucherconsult?CPF='+opp.Account_CPF__c+'&Id='+opp.Id+'&stage='+opp.StageName, page);
        
    }

}