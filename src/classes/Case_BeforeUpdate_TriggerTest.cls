@isTest
public class Case_BeforeUpdate_TriggerTest {
    
      public static TestMethod void myTest(){
        
        cacweb_RecordTypeAll__c customSet = new cacweb_RecordTypeAll__c();
        customSet.Name = 'Test';
        customSet.developerName__c = 'Cacweb_AfterSalesMethodsQuality';
        customSet.idRecordType__c = Utils.getRecordTypeId('Case', 'Cacweb_AfterSalesMethodsQuality');
        Insert customSet;
        
        Case c = new Case(
            RecordTypeId = Utils.getRecordTypeId('Case', 'Cacweb_AfterSalesMethodsQuality')
        );
        Insert c;
        
        CacWeb_RecordType_RenaultRede__c cacRede = new CacWeb_RecordType_RenaultRede__c();
        cacRede.Name = 'teste2';
        cacRede.RecordType_ID__c = Utils.getRecordTypeId('Case', 'Cacweb_AfterSalesMethodsQuality');
        cacRede.recordType_developerName__c = 'Cacweb_AfterSalesMethodsQuality';
        Insert cacRede;
        
        Case c2 = new Case(
            RecordTypeId = Utils.getRecordTypeId('Case', 'Cacweb_AfterSalesMethodsQuality')
        );
        Insert c2;
        
    }

}