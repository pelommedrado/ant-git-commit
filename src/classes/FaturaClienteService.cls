public with sharing class FaturaClienteService extends FaturaService {

	public FaturaClienteService(List<FaturaDealer2__c> faturaList) {
		super(faturaList);
	}

	protected override Boolean isValidation(FaturaDealer2__c fatura) {
		return FaturaDealer2Utils.isRecordTypeRegister(fatura);
	}

	protected override void validation(FaturaDealer2__c fatura) {
		FaturaDealer2Utils.validationFatura(fatura);
	}

	protected override void processValid() {
		System.debug('processValid()');

	    final Map<String, Account> customerIdAccountMap = processAccount();
	    Database.upsert(customerIdAccountMap.values());

	    //ITEM 3.
	    assignAccount(customerIdAccountMap);

	    final List<FaturaDealer2__c> faturaHistoryList = processHistory();
	    //Reprocessamento de histórico
        //Database.upsert(faturaHistoryList);
        try {
            FaturaNotaService servicoNota = new FaturaNotaService(faturaHistoryList);
            servicoNota.startValidation();
        }
        catch(Exception ex) {
            System.debug('History execution - Ex:' + ex);
        }
	}

	private List<FaturaDealer2__c> processHistory() {
			System.debug('processHistory()');

			final List<FaturaDealer2__c> faturaHistoryList = 	[
				SELECT Id, Customer_Identification_Number__c, Customer_Email__c,
				Phone_1__c, Phone_2__c, Phone_3__c
				FROM FaturaDealer2__c WHERE Status__c = 'Invalid'
	      	AND Customer_Identification_Number__c IN: customerIdSet
			];

    	final Map<String, List<FaturaDealer2__c>> faturaHistoryMap = new Map<String, List<FaturaDealer2__c>>();

	    for(FaturaDealer2__c fatura : faturaHistoryList) {
					final String key = fatura.Customer_Identification_Number__c;
					final List<FaturaDealer2__c> faturaList = faturaHistoryMap.get(key);

	      	if(faturaList == null) {
		        faturaHistoryMap.put(key, new List<FaturaDealer2__c> { fatura } );
	      	}
	      	else {
		        faturaList.add(fatura);
	      	}
	    }

	    for(FaturaDealer2__c fatura : faturaValidList) {
					final String key = fatura.Customer_Identification_Number__c;
	      	final List<FaturaDealer2__c> faturaList = faturaHistoryMap.get(key);
					copyUnprocessed(fatura, faturaList);
	    }

		return faturaHistoryList;
	}

	private void copyUnprocessed(FaturaDealer2__c fatura, List<FaturaDealer2__c> faturaList) {
		System.debug('copy()');

		if(faturaList == null) {
			return;
		}
		for(FaturaDealer2__c fat : faturaList) {
			fat.Customer_Identification_Number__c = fatura.Customer_Identification_Number__c;
			fat.Customer_Email__c = fatura.Customer_Email__c;
			fat.Phone_1__c = fatura.Phone_1__c;
			fat.Phone_2__c = fatura.Phone_2__c;
			fat.Phone_3__c = fatura.Phone_3__c;
			fat.Status__c = 'Unprocessed';
		}
	}

	//Melhorias Fatura Dealer 2
	//2. Modificar a classe de Register para não excluir mais os Dados Cadastris Válidos

	//ITEM 2
	//protected override void processExit() {
	//	super.processExit();
		//Database.delete(faturaValidList);
	//}
}