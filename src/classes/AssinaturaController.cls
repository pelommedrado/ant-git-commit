public class AssinaturaController {
    
    public List<TableRowModelo> listModeloResumo { get;set; }
    /** Lista de modelos de veiculos **/
    public List<TableRowModelo> listModelo 	{ get;set; }
    /** Lista de cores de veiculos **/
    public List<colors_E3M_mapping__c> listCores 			{ get;set; }
    /** Lista com os meses propostos **/
    public Set<String> mesSet  				{ get;set; }
    /** Para Soma de Linhas de Cores **/
    public List<String> versaoOpcional {get;set;}
    /** Para renderizar a tabela se existir engajamento **/
    public Boolean ExistEngagemet {get;set;}
    /** Para passar para tela de Assinatura Produção **/
    public String bir;
    public List<TotalMeses> listMeses {get;set;}
    public Decimal totalHistorico {get;set;}
    public Decimal totalEmCurso {get;set;}
    
    public String corMesSelect { get;set; }
    public Boolean showCores { get;set; }
    
    public AssinaturaController() {
        
        this.mesSet  = new Set<String>();
        this.listCores = new List<colors_E3M_mapping__c>();
        this.showCores = false;
        this.versaoOpcional = new List<String>();
        configListaCores();
    }
    
    /**
    * Para configurar a lista de cores de veiculos, inserir no CustomSet colors_E3M_mapping__c
    */
    
    private void configListaCores() {
        Map<String,colors_E3M_mapping__c> mapCustonSet = colors_E3M_mapping__c.getAll();
        List<colors_E3M_mapping__c> lsCores = new List<colors_E3M_mapping__c>();
        for(colors_E3M_mapping__c cores: mapCustonSet.values()){
            listCores.add(cores);
            listCores.sort();
        }
    }
    
    private List<Engagement__c> obterListEngagement() {
        
        //recupera a concessionaria do user conectado
        User user = [
            SELECT AccountId
            FROM User
            WHERE Id =: UserInfo.getUserId()
        ];
        
        //recupera engajamentos da concessionaria
        List<Engagement__c> lsEng = [
            SELECT Id, Month__c, Number_Month__c
            FROM Engagement__c
            WHERE Account__c =: user.AccountId 
            AND Status__c != 'Validated' AND Status__c != 'Sent'
            Order by Number_Month__c
        ];
        
        return lsEng;
    }
    
    private List<Engagement_Item__c> obterEngagement_Item() {
        List<Engagement__c> listEng = obterListEngagement();
        
        //recupera todos os meses do engajamento
        for(Engagement__c eg : listEng) {
            System.debug('Eng:' + eg);
            mesSet.add(eg.Month__c);
        }
        
        //recupera todos os modelos existentes
        List<Engagement_Item__c> dList = [
            SELECT Id, Vehicle_Model__c, Vehicle_Version__c, Vehicle_Optional__c, 
            Historical_Average_Sales__c,
            Engagement__r.Month__c, Cores_Inativas__c,
            In_Course__c, Proposal__c, Signature__c, BIR__c
            FROM Engagement_Item__c
            WHERE Engagement__c in: listEng
            ORDER BY Sort_order__c, Vehicle_Version__c, Vehicle_Optional__c, engagement__r.Month__c
        ];
        
        return dList;
    }
    
    public void actionInit() {
        
        this.listModelo = new List<TableRowModelo>();
        
        //recupera todos os modelos existentes
        List<Engagement_Item__c> dList = obterEngagement_Item();
        
        system.debug('dList = '+dList);
        
        if(dList.isEmpty()){
            ExistEngagemet = false;
        }else{
            ExistEngagemet = true;
        }
        
        //Set<String> mSet    = new Set<String>();
        Map<String, TableRowModelo> mapTabRowMod = new Map<String, TableRowModelo>();
        Map<String, TableRowModelo> mapaTableRowModelo = new Map<String, TableRowModelo>();
        
        totalHistorico = 0;
        totalEmCurso = 0;
        Map<String,Decimal> mes = new Map<String,Decimal>();
        String mesesReal;
        for(Engagement_Item__c d : dList) {
            
            if(mesesReal == null || mesesReal.equals(d.Engagement__r.Month__c)){
                totalHistorico += d.Historical_Average_Sales__c;
                totalEmCurso += d.In_Course__c;
                mes.put(mesesReal,totalHistorico);
            }
            
            mesesReal = d.Engagement__r.Month__c;
            
            //cria a linha total de cada modelo
            if(!mapTabRowMod.containsKey(d.Vehicle_Model__c)) {
                TableRowModelo tr1 = new TableRowModelo();
                tr1.header      = true;
                tr1.modelo      = d.Vehicle_Model__c;
                tr1.versao      = 'Total';
                tr1.opcao       = '';
                
                //para cada mes do set cria um TableRowMes
                for(String m : mesSet) {
                    TableRowMes trr = new TableRowMes();
                    trr.mes = m;
                    tr1.meses.add(trr); //TableRowModelo add a lista de meses existentes
                }
                
                TableRowMes trm1 = new TableRowMes();
                trm1.mes = mesesReal;
                trm1.assinatura = 0;
                trm1.proposta = 0;
                tr1.mapM.put(mesesReal, trm1);
                
                mapTabRowMod.put(d.Vehicle_Model__c, tr1);
                
                //mSet.add(d.Vehicle_Model__c);
                this.listModelo.add(tr1);
            }
            
            TableRowModelo tr = criarTableRowModelo(d, mapaTableRowModelo, mapTabRowMod);
            
            TableRowMes trm = new TableRowMes();
            trm.mes 		= mesesReal;
            trm.assinatura 	= d.Signature__c;
            trm.proposta	= d.Proposal__c;
            trm.item		= d;
   
            tr.mapM.put(mesesReal, trm);
        }
        
        for(TableRowModelo tm :this.listModelo) {
            for(TableRowMes tr: tm.meses) {
                
                TableRowMes temp = tm.mapM.get(tr.mes);
                if(temp != null && !tm.header) {
                    tr.assinatura = temp.assinatura;
                    tr.proposta = temp.proposta;
                    tr.item = temp.item;
                    tr.showInput = (temp.proposta != null && temp.proposta >= 0) ? true : false;
                    
                    TableRowModelo h = mapTabRowMod.get(tm.modelo);
                    for(TableRowMes t : h.meses) {
                        if(t.mes.equalsIgnoreCase(tr.mes)) {
                            t.proposta += tr.proposta;
                            if(tr.assinatura != null) {
                                t.assinatura += tr.assinatura;     
                            } 
                        } 
                    }
                }
            }
        }
    }
    
    private TableRowModelo criarTableRowModelo(Engagement_Item__c d, Map<String, TableRowModelo> mapa, Map<String, TableRowModelo> mapHeader) {
        
        String iden = d.Vehicle_Model__c + d.Vehicle_Version__c + d.Vehicle_Optional__c;
        
        if(!mapa.containsKey(iden)) {
            
            TableRowModelo tr = new TableRowModelo();
            tr.header       = false;
            tr.modelo       = d.Vehicle_Model__c;
            tr.versao       = d.Vehicle_Version__c;
            tr.versionopt	= d.Vehicle_Version__c + (d.Vehicle_Optional__c == null ? '' : d.Vehicle_Optional__c);
            tr.versionopt	= tr.versionopt.replaceAll('[^a-zA-Z0-9]+','');
            tr.opcao        = (String.isEmpty(d.Vehicle_Optional__c) ? '' : d.Vehicle_Optional__c);
            tr.historico    = d.Historical_Average_Sales__c;
            tr.emCurso      = d.In_Course__c;
            //gerar identificador
            tr.id = iden;
            
            TableRowModelo h = mapHeader.get(d.Vehicle_Model__c);
            h.historico += d.Historical_Average_Sales__c;
            h.emCurso += d.In_Course__c;
            
            //preenche lista de versao Opcional
            String versao, opcional;
            if(tr != null){
                versao = tr.versao;
                opcional = tr.opcao;
                versaoOpcional.add(versao+opcional);
            }
            
            //para cada mes do set cria um TableRowMes
            for(String mes : mesSet) {
                TableRowMes tm = new TableRowMes();
                tm.mes = mes;
                tr.meses.add(tm); //TableRowModelo add a lista de meses existentes
            }
            
            mapa.put(tr.id, tr);
            this.listModelo.add(tr);
            
            return tr;
        }
        
        return mapa.get(iden);
    }
    
    public PageReference obterCoresMes() {
        
        List<Id> listItemIds = new List<Id>();
        for(TableRowModelo tm :this.listModelo) {
            for(TableRowMes tr: tm.meses) {
                
                if(tr.proposta != null && tr.mes.equalsIgnoreCase(corMesSelect) && (tr.assinatura == null || tr.assinatura < 0) && !tm.header){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error , 'Favor preencher todas as assinaturas ' +
                                                                    'de Modelo/Versão/Opção antes de avançar para as informações de cores ' +
                                                                   'para o mês de: '+tr.mes);
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                
                if(tr.item != null) {
                    listItemIds.add(tr.item.Id);
                }
            }
        }
        
        //busca todas as cores no banco de acordo com o mes selecionado e linhas da tabela
        List<Engagement_Color__c> res = [
            SELECT Id, Colors__c, Month__c, Signature__c, Item__r.Id
            FROM Engagement_Color__c
            WHERE Item__r.Month__c in: mesSet AND Item__c in: listItemIds
            ORDER BY Item__r.Month__c, 	Item__c, Colors__c
        ];
        
        Map<String, List<Engagement_Color__c>> mEngItem = 
            new Map<String, List<Engagement_Color__c>>();
        
        //coloca todas as cores num mapa de ModeloId -> Lista de Cores
        for(Engagement_Color__c i : res) {
            System.debug('Cor base:' + i);
            
            String iden = i.Item__r.Id;
            
            List<Engagement_Color__c> listEngCor = mEngItem.get(iden);
            if(listEngCor == null) {
                listEngCor = new List<Engagement_Color__c>();
                mEngItem.put(iden, listEngCor);
            }
            
            listEngCor.add(i);
        }
        
        //Para atualizar as cores
        List<Engagement_Color__c> lsCores = new List<Engagement_Color__c>();
        
        for(TableRowModelo tm :this.listModelo) {
            for(TableRowMes tr: tm.meses) {
                tr.cores.clear();
                List<Engagement_Color__c> listEngCor = null;
                
                if(tr.item != null) {
                    listEngCor = mEngItem.get(tr.item.Id);
                }
                
                if(listEngCor == null || listEngCor.isEmpty()) {
                    listEngCor = new List<Engagement_Color__c>();
                    
                    for(colors_E3M_mapping__c cores : listCores) {
                        
                        Engagement_Color__c temp = new Engagement_Color__c();
                        temp.Colors__c 		= cores.Color_Code__c;
                        temp.Month__c 		= tr.mes;
                        temp.Signature__c 	= 0;
                        temp.Item__c = (tr.item != null ) ? tr.item.id : null;
                        
                        listEngCor.add(temp);
                    }
                }
                
                Decimal soma = 0;
                for(Engagement_Color__c it : listEngCor) {
                    
                    TableRowCor trc = new TableRowCor();
                    trc.codCor = it.Colors__c;
                    trc.assinatura = String.valueOf(it.Signature__c);
                    soma += it.Signature__c;
                    tr.somaAssCores = soma;
                    trc.res = it;
                    
                    if(tr.item != null && tr.item.Cores_Inativas__c != null) {
                        String[] corDesativa = tr.item.Cores_Inativas__c.split(';');
                        Set<String> setCorDesativa = new Set<String>();
                        for(String s : corDesativa) {
                            setCorDesativa.add(s);
                        }
                        if(setCorDesativa.contains(it.Colors__c)) {
                            trc.ativa = false;
                        }
                    }
                    
                    tr.cores.add(trc);
                    
                    if(it.Item__c != null /*&& corMesSelect.equalsIgnoreCase(it.Month__c)*/) {
                        lsCores.add(it);
                    }
                }
            }
        }
        
        upsert lsCores;
        showCores = true;
        return null;
    }
    
    public void actionResumo() {
        
        actionInit();
        
        Date data = system.today();
        this.diaResumo = data.day();
        
        Integer mesNum = data.month();
        String mes;
        if(mesNum == 1)
            this.mesResumo = 'Janeiro';
        if(mesNum == 2)
            this.mesResumo = 'Fevereiro';
        if(mesNum == 3)
            this.mesResumo = 'Março';
        if(mesNum == 4)
            this.mesResumo = 'Abril';
        if(mesNum == 5)
            this.mesResumo = 'Maio';
        if(mesNum == 6)
            this.mesResumo = 'Junho';
        if(mesNum == 7)
            this.mesResumo = 'Julho';
        if(mesNum == 8)
            this.mesResumo = 'Agosto';
        if(mesNum == 9)
            this.mesResumo = 'Setembro';
        if(mesNum == 10)
            this.mesResumo = 'Outubro';
        if(mesNum == 11)
            this.mesResumo = 'Novembro';
        if(mesNum == 12)
            this.mesResumo = 'Dezembro';
        
        this.anoResumo =  data.year();
        this.birResumo = [Select IDBIR__c from Account where id in (Select AccountId from User where Id =: UserInfo.getUserId())].IDBIR__c;
        
        this.listModeloResumo = new List<TableRowModelo>();
        for(TableRowModelo t: listModelo) {
            listModeloResumo.add(t);
        }
    }
    
    public class TableRowModelo {
        public Boolean header 		{ get;set;}
        public String modelo    	{ get;set;}
        public String versao    	{ get;set;}
        public String versionopt	{ get;set;}
        public String opcao     	{ get;set;}
        public Decimal historico 	{ get;set;}
        public Decimal emCurso   	{ get;set;}
        public Decimal sumHistorico { get;set;}
        public Decimal sumEmCurso   { get;set;}
        public String id			{ get;set;}
        
        public List<TableRowMes>  meses 		{ get;set; }
        /** Valores dos meses que possuem informacoes **/
        public Map<String, TableRowMes> mapM 	{ get;set; }
        
        public TableRowModelo() {
            this.meses = new List<TableRowMes>();
            this.mapM = new Map<String, TableRowMes>();
            this.historico = 0;
            this.emCurso = 0;
        }
    }
    
    public class TableRowMes {
        public Engagement_Item__c item  { get;set; }
        public String mes   			{ get;set; }
        public Decimal proposta			{ get;set; }
        public Decimal assinatura		{ get;set; }
        public Decimal somaAssCores			{ get;set; }
        public Boolean showInput 		{ get;set; }
        public List<TableRowCor> cores 	{ get;set; }
        
        public TableRowMes() {
            this.cores = new List<TableRowCor>();
            this.showInput = false;
            this.proposta = 0;
            this.assinatura = 0;
        }
    }
    
    public class TableRowCor {
        public Engagement_Color__c res { get;set; }
        public String codCor   		{ get;set; }
        public String assinatura 	{ get;set; }
        public Boolean ativa 		{ get;set; }
        
        public TableRowCor() {
            this.ativa = true;
        }
    }
    
    public class TotalMeses {
        public Engagement_Item__c item  { get;set; }
        public String mes   			{ get;set; }
        public Decimal proposta			{ get;set; }
        public Decimal assinatura		{ get;set; }
        public Boolean showInput 		{ get;set; }
        public TotalMeses() {
            this.proposta = 0;
            this.assinatura = 0;
        }
    }     
    
    //salva os registros da pagina
    public PageReference salvaCores() {
        
        Map<String, Engagement_Color__c> mapaCor = recebeCores();
        
        System.debug('lsCores =' + mapaCor.values());
        
        try {
            Database.Update(mapaCor.values());
            addMensagemInfo('Cores salvas com sucesso');
            
        } catch(Exception ex) {
            addMensagemErro('Erro ao salvar registro: ' + ex);
            
        }
        
        return null;
    }
    
    private void addMensagemInfo(String msg) {
        ApexPages.Message myMsg = 
            new ApexPages.Message(ApexPages.Severity.INFO , msg);
        ApexPages.addMessage(myMsg);
    }
    
    private void addMensagemErro(String msg) {
        ApexPages.Message myMsg = 
            new ApexPages.Message(ApexPages.Severity.Error , msg);
        ApexPages.addMessage(myMsg);
    }
    
     //salva os registros de Assinaturas
    public PageReference salvaAssinaturas(){

        String m = ApexPages.currentPage().getParameters().get('mesAss');
        //System.debug('MESES:' + m);
        this.corMesSelect = m;
        
        //recupera resultados via parametro da pagina
        String passedParams = ApexPages.currentPage().getParameters().get('regAss'); 
        List<String> listPassedParams = passedParams.split(',');
        
        system.debug('listPassedParams = '+listPassedParams);
        
        List<Engagement_Item__c> lsItems = new List<Engagement_Item__c>();
        
        for(String s: listPassedParams){
            List<String> str = s.split(':');
            if(!str[0].Equals('undefined')){
                Engagement_Item__c r = new Engagement_Item__c();
                r.Id = str[0];
                if(!str[1].equals('@'))
                	r.Signature__c = Decimal.valueOf( str[1] );
                else
                    r.Signature__c = null;
                lsItems.add(r);
            }
        }
        
        system.debug('lsItems ='+lsItems);
        
        try{
            Database.Update(lsItems);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO , 'Assinaturas salvas com sucesso'); 
            ApexPages.addMessage(myMsg);
            actionInit();
        }catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error , 'Erro ao salvar registros: ' + ex);
            ApexPages.addMessage(myMsg);
        }
        
        return null;
    }
    
    public Map<String, Engagement_Color__c> recebeCores(){
        
        //recupera resultados via parametro da pagina
        String passedParams = ApexPages.currentPage().getParameters().get('param');
        List<String> listPassedParams = passedParams.split(',');
        
        System.debug('listPassedParams = '+listPassedParams);
        
        Map<String, Engagement_Color__c> mapaCor = new Map<String, Engagement_Color__c>();
        
        for(String s: listPassedParams) {
            List<String> str = s.split(':');
            String iden  = str[0];
            String assin = str[1];
            Decimal assinatura = 0;
            
            if(!iden.equalsIgnoreCase('undefined')) {
                
                try { 
                    if(!String.isEmpty(assin)) {
                        assinatura = Decimal.valueOf(assin);     
                    }
                    
                    Engagement_Color__c r = new Engagement_Color__c();
                    r.Id = iden;
                    r.Signature__c = assinatura;
                    
                    mapaCor.put(iden, r);
                    
                } catch(Exception ex) {
                    System.debug('Ex:' + ex);
                    
                    addMensagemErro('O valor do campo Cor deve ser um número');
                    return null;
                }
            }
        }
        return mapaCor;
    }
    
    //avanca da tela de assinatura e proposta para a tela de resumo
    public PageReference avancarResumo(){
        
        String res = ApexPages.currentPage().getParameters().get('saveRes');
        System.debug('res:' + res);

        List<String> listPassedParams = res.split(',');
        
        system.debug('listPassedParams = '+listPassedParams);
        
        List<Engagement_Item__c> assDaTela = new List<Engagement_Item__c>();
        
        for(String s: listPassedParams){
            List<String> str = s.split(':');
            if(!str[0].Equals('undefined')){
                Engagement_Item__c r = new Engagement_Item__c();
                r.Id = str[0];
                if(!str[1].equals('@'))
                    r.Signature__c = Decimal.valueOf( str[1] );
                else
                    r.Signature__c = null;
                assDaTela.add(r);

            }
        }
        
        Update assDaTela;
        
        system.debug('assDaTela = '+assDaTela);
        
        //recupera apenas o modelo e mes do banco (assinatura continua a da tela)
        Map<Id,Engagement_Item__c> mapEngIt = new Map<Id,Engagement_Item__c>();
        List<Engagement_Item__c> lsModelosBanco = [Select Proposal__c, Vehicle_Model__c, Month__c from Engagement_Item__c where id in: assDaTela];
        for(Engagement_Item__c i: lsModelosBanco){
            mapEngIt.put(i.Id,i);
        }
        
        //recupera todas as cores para a soma
        List<Engagement_Color__c> lsCores = [Select Id, Signature__c, Item__c from Engagement_Color__c where Item__c in(
            Select Id from Engagement_Item__c where Id in: assDaTela)];
        
        system.debug('lsCores = '+lsCores);

        //verifica se pra cada modelo mudou a soma de assinatura
        for(Engagement_Item__c it: assDaTela){
            Decimal soma = 0;
            for(Engagement_Color__c i: lsCores){
                if(it.Id.Equals(i.Item__c)){
                	soma += i.Signature__c;
                }
            }
            
            if(mapEngIt.get(it.Id).Proposal__c != null && it.Signature__c != soma ){
                addMensagemErro('Revise as assinaturas de Cores do veículo: ' + mapEngIt.get(it.Id).Vehicle_Model__c  + 
                                ' para o mês de ' + mapEngIt.get(it.Id).Month__c ) ;
                return null;
            }
        }
        
        PageReference pageRef = new PageReference('/apex/AssinaturaProducao');
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
    //avanca da tela de cores para a tela de resumo
    public PageReference avancar(){
        
        Map<String, Engagement_Color__c> mapaCor = recebeCores();
        
        System.debug('lsCores =' + mapaCor.values());
        
        Update mapaCor.values();
        
        system.debug('list Modelo: '+ listModelo);
        
        //para localizar algum erro na tabela;
        Integer erro = 0;
        for(TableRowModelo tm :this.listModelo) {           
            for(TableRowMes tr: tm.meses) {
                
                if(tr.proposta != null && tr.proposta >= 0) { 
                    
                    Decimal total = 0;
                    for(TableRowCor cor : tr.cores) {
                        System.debug('Cor:' + cor + ' total:' + total);
                        if(cor.res != null && cor.res.Id != null) {
                            Engagement_Color__c corEng = mapaCor.get(cor.res.Id);
                            total += corEng.Signature__c;
                        }
                    }
                    for(TableRowCor cor : tr.cores){
                        system.debug('cor = '+cor);
                        system.debug('cor.res.Id = '+cor.res.Id);
                        system.debug('total = '+total);
                        system.debug('tr.assinatura = '+tr.assinatura);	
                        if(cor.res.Id != null && total != tr.assinatura) {
                            addMensagemErro('Revise as Assinaturas para: ' + tm.modelo + ' do Mês de '+ tr.mes);
                            erro += 1;
                            return null;
                        }
                    }
                }
            }      
        }
        
        system.debug('erro = '+erro);
        
        //Se não localizar nenhum erro redireciona para a página
        if(erro == 0){
            PageReference pageRef = new PageReference('/apex/AssinaturaProducao');
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return null;
    }
    
    //avanca para a tela de assinatura
    public PageReference telaAss(){
        PageReference pageRef = new PageReference('/apex/Assinatura2');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void according(){
        List<Engagement__c> lsEng = obterListEngagement();
        for(Engagement__c eng: lsEng){
            eng.Status__c = 'Sent';
        }
        try{
            Update lsEng;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO , 'Engajamento enviado com sucesso'); 
            ApexPages.addMessage(myMsg);
        }catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error , 'Erro ao atualizar engajamentos: ' + ex);
            ApexPages.addMessage(myMsg);
        }
    }
    
    public Integer diaResumo { get;set;}
    public String mesResumo { get;set; }
    public Integer anoResumo { get;set; }
    public String birResumo { get;set;}
   
    public String getUsuario(){
        return UserInfo.getName();
    }
}