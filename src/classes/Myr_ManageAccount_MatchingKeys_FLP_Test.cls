@isTest
/********************************************************************************************************************
Testclass about matching key "Firstname, Lastname, Phone" (FLP)
@author Donatien Veron (AtoS)
2016 Oct, D. Veron (AtoS), R16.11 - F2067-US3892 - Creation
********************************************************************************************************************/
public class Myr_ManageAccount_MatchingKeys_FLP_Test {

	@testsetup static void setCustomSettings() {
		
		//Declaration
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser;
        
		//Custom settings
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		
		//Technical users
		listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy' , '', true));
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Brazil', '', true));
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Russia', '', true));
		Myr_Datasets_Test.insertTechnicalUsers(listReqTechUser);
    }
	
	//FLP - SOSL
    static void test_FLP_SOSL(String Mode) {
		//Declarations
		Account acc1;
		String Country_Test;
		String Firstname_Test;
		String Lastname_Test;
		String Phone_Test1;
		String Phone_Test2;
		String CustomerIdNumber;
		Country_Info__c M_C;
		Myr_ManageAccount_MatchingKeys_Cls.Response res;
        Myr_ManageAccount_MatchingKeys_Cls matcher;
		User M_U;

		
		//Initializations
		CustomerIdNumber=Myr_Datasets_Test.get_Customer_Identification_Number();
		Firstname_Test='TestFLP_Firstname';
		Lastname_Test='TestFLP_Lastname';

		if(Mode.equals('BOTH_PHONE_EMPTY')){
			Phone_Test1='';
			Phone_Test2='';
		}else{
			Phone_Test1='+3311111111';
			Phone_Test2='+3362222222';
		}

		Country_Test=Myr_Datasets_Test.getRoleName();
		System.debug('Country_Test'+Country_Test);
		
		M_C = [select Myr_MatchingKey__c from Country_Info__c where Name=:Country_Test];
		M_C.Myr_MatchingKey__c='FLP';
		M_C.Myr_MatchingKeys_Algorithm__c='SOSL';
		update M_C;
		M_C = [select Myr_MatchingKey__c, Myr_MatchingKeys_Algorithm__c from Country_Info__c where Name=:Country_Test];
		system.assertEquals('FLP',M_C.Myr_MatchingKey__c);
		system.assertEquals('SOSL',M_C.Myr_MatchingKeys_Algorithm__c);

		M_U=Myr_Datasets_Test.getTechnicalUserWithRole(Country_Test);
		system.runAs(M_U) {
			acc1 = Myr_Datasets_Test.insertPersonalAccountAndUser(
				  Firstname_Test
				, Lastname_Test
				, '89900'
				, 'jean.albert_atos@yopmail.com'
				, 'jean.albert_atos@yopmail.com'
				, ''
				, 'USER_ACTIF'
				, ''
				, 'France'
				, ''
				, ''
				, ''
				, CustomerIdNumber
			);

			acc1.PersMobPhone__c = Phone_Test1;
			acc1.HomePhone__c = Phone_Test2;
			update acc1;

			acc1 = [select Firstname, Lastname, PersMobPhone__c, HomePhone__c, Tech_Name_LandlinePhone__c, Tech_Name_MobilePhone__c from Account];
			system.assertEquals(Firstname_Test, acc1.Firstname);
			system.assertEquals(Lastname_Test, acc1.Lastname);
			system.assertEquals(Firstname_Test+Lastname_Test+Phone_Test1, acc1.Tech_Name_MobilePhone__c);
			system.assertEquals(Firstname_Test+Lastname_Test+Phone_Test2, acc1.Tech_Name_LandlinePhone__c);
			       
			matcher = new Myr_ManageAccount_MatchingKeys_Cls(
						  'Renault'			// brand
						, Country_Test		// country
						, Firstname_Test	// firstname
						, Lastname_Test		// lastname
						, ''				// emailAddress
						, ''				// zipCode
						, ''				// customerIdentificationNumber
						, ''				// vin
						, ''				// accountSfdcId
						, Phone_Test1		// mobilePhone
						, ''				// landlinePhone
						);

			res = matcher.match();
        } 
        // SOSL request doesnt works on testclass, that is why return code is not WS01MS501
    }
	
	static testMethod void test_FLP_SOSL() {
		test_FLP_SOSL('');
	}

	static testMethod void test_FLP_SOSL_Both_Phone_Empty() {
		test_FLP_SOSL('BOTH_PHONE_EMPTY');
	}

	//FLP - SOQL
    static void test_FLP_SOQL(String mode) {
		//Declarations
		Account acc1;
		String Country_Test;
		String Firstname_Test='TestFLP_Firstname';
		String Lastname_Test='TestFLP_Lastname';
		String Phone_Test1;
		String Phone_Test2;
		String CustomerIdNumber;
		Country_Info__c M_C;
		Myr_ManageAccount_MatchingKeys_Cls.Response res;
        Myr_ManageAccount_MatchingKeys_Cls matcher;
		User M_U;
		
		//Initializations
		CustomerIdNumber=Myr_Datasets_Test.get_Customer_Identification_Number();
		if(mode.equals('MOBILE')){
			Phone_Test1='+3311111111';
		}else if(mode.equals('LANDLINE')){
			Phone_Test2='+3362222222';
		}else if(mode.equals('BOTH_MATCH')){
			Phone_Test1='+3311111111';
			Phone_Test2='+3362222222';
		}
		Country_Test=Myr_Datasets_Test.getRoleName();
		System.debug('Country_Test'+Country_Test);
		M_C = [select Myr_MatchingKey__c from Country_Info__c where Name=:Country_Test];
		M_C.Myr_MatchingKey__c='FLP';
		M_C.Myr_MatchingKeys_Algorithm__c='SOQL';
		update M_C;

		M_C = [select Myr_MatchingKey__c, Myr_MatchingKeys_Algorithm__c from Country_Info__c where Name=:Country_Test];
		system.assertEquals('FLP',M_C.Myr_MatchingKey__c);
		system.assertEquals('SOQL',M_C.Myr_MatchingKeys_Algorithm__c);

		M_U=Myr_Datasets_Test.getTechnicalUserWithRole(Country_Test);

		system.runAs(M_U) {
			acc1 = Myr_Datasets_Test.insertPersonalAccountAndUser(
				  Firstname_Test
				, Lastname_Test
				, '89900'
				, 'jean.albert_atos@yopmail.com'
				, 'jean.albert_atos@yopmail.com'
				, ''
				, 'USER_ACTIF'
				, ''
				, Country_Test
				, ''
				, ''
				, ''
				, CustomerIdNumber
			);

			if(mode.equals('BOTH_NOMATCH')){
				Phone_Test1='+1';
				Phone_Test2='+2';
			}else{
				acc1.PersMobPhone__c = Phone_Test1;
				acc1.HomePhone__c = Phone_Test2;
			}
			update acc1;

			acc1 = [select Firstname, Lastname, PersMobPhone__c, HomePhone__c, Tech_Name_LandlinePhone__c, Tech_Name_MobilePhone__c from Account];
			system.assertEquals(Firstname_Test, acc1.Firstname);
			system.assertEquals(Lastname_Test, acc1.Lastname);

			if(!mode.equals('BOTH_NOMATCH')){
				system.assertEquals(Phone_Test1, acc1.PersMobPhone__c);
				system.assertEquals(Phone_Test2, acc1.HomePhone__c);
			}
			
			       
			matcher = new Myr_ManageAccount_MatchingKeys_Cls(
						  'Renault'			// brand
						, Country_Test		// country
						, Firstname_Test	// firstname
						, Lastname_Test		// lastname
						, ''				// emailAddress
						, ''				// zipCode
						, ''				// customerIdentificationNumber
						, ''				// vin
						, ''				// accountSfdcId
						, Phone_Test1		// mobilePhone
						, Phone_Test2		// landlinePhone
						);

			res = matcher.match();
			if(mode.equals('BOTH_NOMATCH')){
				system.assertEquals(null,res.acc);
			}else{
				system.assertNotEquals(null,res.acc);
			}
        } 
        
    }

	static testMethod void test_FLP_SOQL_Mobile() {
		test_FLP_SOQL('MOBILE');
	}
	
	static testMethod void test_FLP_SOQL_Landline() {
		test_FLP_SOQL('LANDLINE');
	}

	static testMethod void test_FLP_SOQL_Both_Phones() {
		test_FLP_SOQL('BOTH_MATCH');
	}

	static testMethod void test_FLP_SOQL_Both_Phones_No_Match() {
		test_FLP_SOQL('BOTH_NOMATCH');
	}

}