/**
    Date: 03/04/2013
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC105_TaskAfterInsertExecTest {

    static testMethod void myUnitTestTaskAfterInsert() {
   
   //================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER
//================================================================      
        Opportunity opp = new Opportunity();       
        boolean isCreatedOpportunity = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;
        
        Test.startTest();

        try{
             opp = [SELECT OpportunityTransition__c FROM Opportunity WHERE Id = :opp.Id];
             isCreatedOpportunity = true;
        }
        catch(QueryException e){
            isCreatedOpportunity = false;
        }

        Test.stopTest();

        system.assertEquals(isCreatedOpportunity,true);
        //system.assertEquals(opp.OpportunityTransition__c, VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER);
    }
}