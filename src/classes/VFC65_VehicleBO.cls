/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto Vehicle (VEH_Veh__c).
* @author Felipe Jesus Silva.
*/
public class VFC65_VehicleBO 
{
	private static final VFC65_VehicleBO instance = new VFC65_VehicleBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC65_VehicleBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC65_VehicleBO getInstance()
	{
		return instance;
	}
	
	public void updateVehicle(VEH_Veh__c sObjVehicle)
	{
		try
		{
			VFC21_VehicleDAO.getInstance().updateData(sObjVehicle);
		}
		catch(DMLException ex)
		{
			throw new VFC93_UpdateVehicleException(ex, ex.getDMLMessage(0));
		}
	}
	
	public void updateVehicles(List<VEH_Veh__c> lstSObjVehicle)
	{
		VFC21_VehicleDAO.getInstance().updateData(lstSObjVehicle);
	}
}