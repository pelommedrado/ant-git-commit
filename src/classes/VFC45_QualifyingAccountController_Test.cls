@isTest(seeAllData=true)
private class VFC45_QualifyingAccountController_Test {
	
	static Id getRecordTypeId(String developerName)
	{
		RecordType sObjRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: developerName];
		
		return sObjRecordType.Id;
	}
	
	static Account createDealerAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Network_Site_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccDealer = new Account();
		sObjAccDealer.Name = 'Dealer Test';
        sObjAccDealer.IDBIR__c = '10450';
		sObjAccDealer.Phone='1000';
		sObjAccDealer.RecordTypeId = recordTypeId; 
		sObjAccDealer.ProfEmailAddress__c = 'dealer@mail.com'; 
		sObjAccDealer.ShippingCity = 'Paris';
		sObjAccDealer.ShippingCountry = 'France';
		sObjAccDealer.ShippingState = 'IDF';
		sObjAccDealer.ShippingPostalCode = '75013';
		sObjAccDealer.ShippingStreet = 'my street';
		
		return sObjAccDealer;
	}
	
	static Account createPersonalAccount()
	{
		/*obtém o record type de concessionária*/
		Id recordTypeId = getRecordTypeId('Personal_Acc');
		
		/*cria e insere a conta que representa a concessionária*/
		Account sObjAccPersonal = new Account();
		sObjAccPersonal.FirstName = 'Personal';
		sObjAccPersonal.LastName = 'Account';
		sObjAccPersonal.Phone='1000';
		sObjAccPersonal.RecordTypeId = recordTypeId; 
		sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
		sObjAccPersonal.ShippingCity = 'Paris';
		sObjAccPersonal.ShippingCountry = 'France';
		sObjAccPersonal.ShippingState = 'IDF';
		sObjAccPersonal.ShippingPostalCode = '75013';
		sObjAccPersonal.ShippingStreet = 'my street';
		sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
		sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
		//sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
		//sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
		sObjAccPersonal.PersLandline__c = '1133334444';
		sObjAccPersonal.PersMobPhone__c = '1155556666';
		
		return sObjAccPersonal;
	}
	
	static VEH_Veh__c createVehicle()
	{
		VEH_Veh__c sObjVehicle = new VEH_Veh__c();
		sObjVehicle.Name = 'TESTCHASSISANDERO';
		sObjVehicle.Model__c = 'Sandero';
		sObjVehicle.Version__c = 'Stepway 1.6 8V';
		sObjVehicle.VersionCode__c = 'S1';
		sObjVehicle.Color__c = 'Black';
		sObjVehicle.VehicleRegistrNbr__c = 'ABC-1234';
		
		return sObjVehicle;
	}
	
	static TDV_TestDrive__c createTestDrive()
	{
		TDV_TestDrive__c sObjTestDrive = new TDV_TestDrive__c();
		sObjTestDrive.Status__c = 'Scheduled';
		sObjTestDrive.Opportunity__c = null;
		
		return sObjTestDrive;
	}
	
    static testMethod void testVFC45_QualifyingAccountController() {
        
        Account sObjAccDealer = null;
		Account sObjAccPersonal = null;
		VEH_Veh__c sObjVehicle = null;
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		
		/*cria e insere a conta que representa a concessionária*/
		sObjAccDealer = createDealerAccount();		
		insert sObjAccDealer;
		
		/*cria a conta pessoal e vincula a concessionária*/
		sObjAccPersonal = createPersonalAccount();
		sObjAccPersonal.DealerInterest_BR__c = sObjAccDealer.Id;
		insert sObjAccPersonal;
		
		/*cria e insere o veículo*/
		sObjVehicle = createVehicle();
		insert sObjVehicle;
		
        Opportunity opp1 = new Opportunity();       
        boolean isCreatedOpportunity1 = false;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb21 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp1.AccountId = sObjAccPersonal.Id;
        opp1.Name = 'OppNameTest_1';
        opp1.StageName = 'Identified';
        opp1.CloseDate = Date.today();
        opp1.Pricebook2Id = pb21.Id;
        opp1.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opp1.CurrencyIsoCode = 'BRL';
        insert opp1;
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);
        
        PageReference pageRef = Page.VFP06_QualifyingAccount;
        pageRef.getParameters().put('id',opp1.id);
        Test.setCurrentPage(pageRef);
        
        VFC45_QualifyingAccountController cont = new VFC45_QualifyingAccountController(controller);
        
        cont.getStates();
        cont.getBrands();
        cont.getModels();
        cont.selectedBrand ='RENAULT';
        cont.getBrandDetails();
        cont.getModelDetails();
        cont.cancelOpportunity();
        cont.openPopUpCancellation();
        cont.performTestDrive();
        cont.saveOpportunity();
        cont.verifyCancellationOption();
        cont.getAddressGivenZipCode();
    }
}