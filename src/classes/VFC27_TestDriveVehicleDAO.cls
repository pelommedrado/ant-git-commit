/**
*	Class	-	VFC27_TestDriveVehicleDAO
*	Author	-	Surseh Babu
*	Date	-	29/10/2012
*
*	#01 <Suresh Babu> <29/10/2012>
*		DAO class for Test Drive Vehicle object to Query records.
**/
public with sharing class VFC27_TestDriveVehicleDAO extends VFC01_SObjectDAO{
	private static final VFC27_TestDriveVehicleDAO instance = new VFC27_TestDriveVehicleDAO();
	    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC27_TestDriveVehicleDAO(){}

    /**
    * Method responsible for providing the instance of this class..
    */  
    public static VFC27_TestDriveVehicleDAO getInstance(){
        return instance;
    }
    
    /**
    * 	This Method was used to get Test Drive Vehicle Records.
    * 	@param	carModel - use carModel, entered by customer to refine search.
    *	@param	town	-	compare this town value with Dealer's town to fetch records.
    *	@param	stateCode	-	compare this stateCode with Dealer's State code to refine search criteria.
    * 	@return	lstTestDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    */
    public List<TDV_TestDriveVehicle__c> fetchTestDriveVehicleRecordsUsingCriteria( String carModel, String town, String stateCode ){
    	List<TDV_TestDriveVehicle__c> lstTestDriveVehicleRecords = null;
    	
    	Id networkSiteRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Network_Site_Acc' );
    	
    	lstTestDriveVehicleRecords = [SELECT 
    										Account__c, Account__r.Name, Account__r.Id, 
    										Account__r.ShippingStreet, Account__r.ShippingCity, 
    										Account__r.ShippingState, Account__r.ShippingPostalCode, 
    										Account__r.ShippingCountry, Account__r.RecordTypeId,
    										AgendaClosingDate__c, AgendaOpeningDate__c, Available__c, 
    										Id, Name, Vehicle__c, Vehicle__r.Model2__c, Vehicle__r.Model__c
    									FROM 
    										TDV_TestDriveVehicle__c
    									WHERE
    										Vehicle__r.Model__c =: carModel AND
    										Account__r.ShippingCity =: town AND
    										Account__r.ShippingState =: stateCode AND
    										Account__r.RecordTypeId =: networkSiteRecordTypeId
    									];
    	return lstTestDriveVehicleRecords;
    }
    
    /**
    *	This Method was used to get Test Drive Vehicle Records.
    *	@param	dealerId	-	currently selected DealerId for query parameter.
    * 	@param	carModel - use carModel, entered by customer to refine search.
    * 	@return	lstTestDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    **/
    public TDV_TestDriveVehicle__c fetchTestDriveVehicle_UsingDealerId( Id dealerId, String carModel ){
    	TDV_TestDriveVehicle__c testDriveVehicleRecords = null;
    	
    	try {
			testDriveVehicleRecords = [SELECT 
	    									Account__c, AgendaClosingDate__c, AgendaOpeningDate__c, Available__c, 
	    									Id, Name, Vehicle__c, Vehicle__r.Model__c, Vehicle__r.Model2__c 
	    								FROM 
	    									TDV_TestDriveVehicle__c
	    								WHERE
	    									Vehicle__r.Model__c =: carModel
	    								AND Account__c =: dealerId
	    								limit 1];
    	}
    	catch (Exception e) {
    		testDriveVehicleRecords = null;
    	}
    	return testDriveVehicleRecords;
    }
    /**
    *	This Method was used to get Test Drive Vehicle Records.
    *	@param	TestDriveVehicle	-	Trigger called and passing  Test Drive Vehicle new entry values like Id, vehicle Id, Account Id, Opening date and closing date  .
    * 	@return	testDriveVehicleRecords - fetch and return the List Test Drive Vehicle Records from Test Drive Vehicle object.
    **/
    public List<TDV_TestDriveVehicle__c> fetchTestDriveVehicle_UsingTestDriveVehicle( TDV_TestDriveVehicle__c TestDriveVehicle){
    	List<TDV_TestDriveVehicle__c> testDriveVehicleRecords = null;
    	
    	testDriveVehicleRecords = [SELECT 
    									Account__c, AgendaClosingDate__c, AgendaOpeningDate__c, Available__c, 
    									Id, Name, Vehicle__c, Vehicle__r.Model__c, Vehicle__r.Model2__c 
    								FROM 
    									TDV_TestDriveVehicle__c
    								WHERE
    									Id != : TestDriveVehicle.Id And
    									Vehicle__c =: TestDriveVehicle.Vehicle__c AND
    									Account__c =: TestDriveVehicle.Account__c And
    									Available__c =: true And
    									((AgendaOpeningDate__c <= :TestDriveVehicle.AgendaOpeningDate__c and AgendaClosingDate__c >= :TestDriveVehicle.AgendaOpeningDate__c) 
										or (AgendaOpeningDate__c  <= :TestDriveVehicle.AgendaClosingDate__c and AgendaClosingDate__c  >= :TestDriveVehicle.AgendaClosingDate__c) 
										or(AgendaOpeningDate__c >= :TestDriveVehicle.AgendaOpeningDate__c and AgendaOpeningDate__c <= :TestDriveVehicle.AgendaClosingDate__c)
										 or (AgendaClosingDate__c  >= :TestDriveVehicle.AgendaOpeningDate__c and AgendaClosingDate__c  <= :TestDriveVehicle.AgendaClosingDate__c))
    								
    								];
    	return testDriveVehicleRecords;
    }
    /**
    *	This Method was used to get Test Drive Vehicle Records.
    *	@param	TestDriveVehId	-	Test Drive Vehicle Id using to get and  Test Drive Vehicle account values .
    * 	@return	testDriveVehicleRecords - fetch and return the  Test Drive Vehicle Records from Test Drive Vehicle object.
    **/
    public Id fetchTestDriveVehicleAccount_UsingId( Id TestDriveVehId ){
    	TDV_TestDriveVehicle__c testDriveVehicleRecords = null;
    	try{
    		testDriveVehicleRecords = [SELECT 
    									Account__c, AgendaClosingDate__c, AgendaOpeningDate__c, Available__c, 
    									Id, Name, Vehicle__c, Vehicle__r.Model__c, Vehicle__r.Model2__c  
    								FROM 
    									TDV_TestDriveVehicle__c
    								WHERE
    									Id =: TestDriveVehId
    								limit 1 
    								];
    		return testDriveVehicleRecords.Account__c;
    	}catch(QueryException ex){
        	return null;
        }
    	
    }
    
    public TDV_TestDriveVehicle__c findById(String id)
    {
    	TDV_TestDriveVehicle__c sObjTestDriveVehicle = [SELECT Vehicle__r.Color__c,
    														   Vehicle__r.Model2__c,
                                                        	   Vehicle__r.Model__c,
    														   Vehicle__r.VehicleRegistrNbr__c,
    														   Vehicle__r.VersionCode__c,
    														   Vehicle__r.Version__c  														   
    												    FROM TDV_TestDriveVehicle__c
    												    WHERE Id =: id];
    	
    	return sObjTestDriveVehicle;
    }
    
    public List<TDV_TestDriveVehicle__c> findByCriteria(String account, Boolean available, String vehicleModel)
    {
    	List<TDV_TestDriveVehicle__c> lstSObjTestDriveVehicle = [SELECT Account__r.OwnerId,
    	                                                                Id,
    																	Vehicle__r.Color__c,
    																	Vehicle__r.Model2__c,
                                                                 		Vehicle__r.Model__c,
    	                                                                Vehicle__r.VehicleRegistrNbr__c,
    	                                                                Vehicle__r.VersionCode__c,
    	                                                                Vehicle__r.Version__c
    	                                                         FROM TDV_TestDriveVehicle__c
    	                                                         WHERE Account__c =: account
    	                                                         AND Available__c =: available
    	                                                         AND Vehicle__r.Model__c LIKE : '%'+vehicleModel+'%' 
    	                                                         ORDER BY Vehicle__r.VehicleRegistrNbr__c];
    	
    	return lstSObjTestDriveVehicle;
    }
    
    
    public List<TDV_TestDriveVehicle__c> fetchTestDriveVehicle_UsingDataRange(Id AccountId, String model, Date dtbegin, Date dtEnd)
    {
    	system.debug('*** AccountId='+AccountId);
    	system.debug('*** model='+model);
    	system.debug('*** dtbegin='+dtbegin);
    	system.debug('*** dtEnd='+dtEnd);

    	List<TDV_TestDriveVehicle__c> testDriveVehicleRecords = 
    		[SELECT Id, Name, Account__c, AgendaClosingDate__c, AgendaOpeningDate__c, Available__c, 
					Vehicle__c, Vehicle__r.Model__c, Vehicle__r.Model2__c 
			   FROM TDV_TestDriveVehicle__c
		       WHERE Account__c =: AccountId
				 AND Vehicle__r.Model__c = :model
				 AND Available__c = true
				 AND ((AgendaOpeningDate__c <= :dtEnd and AgendaClosingDate__c >= :dtEnd)
				   or (AgendaOpeningDate__c >= :dtbegin and AgendaClosingDate__c >= :dtEnd)
				   or (AgendaOpeningDate__c >= :dtbegin and AgendaClosingDate__c >= :dtbegin)
				   or (AgendaOpeningDate__c >= :dtbegin and AgendaClosingDate__c <= :dtEnd)
					    )];
    	return testDriveVehicleRecords;
    	
    }
    
    
    
}