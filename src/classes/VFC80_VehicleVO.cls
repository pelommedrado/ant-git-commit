/**
* Classe que representa os dados de veículo.
* @author Elvia Serpa.
*/
public with sharing class VFC80_VehicleVO 
{
    public String itemModel {get;set;}
    public String itemVersion {get;set;}
    public Decimal itemYear {get;set;}
    public String itemColor {get;set;}  
    public Quote objsQuote {get;set;}
    

    public List<SelectOption> lstProd { get; set; } 
    public List<SelectOption> lstVers { get; set; }
    public List<SelectOption> lstYear { get; set; }
    public List<SelectOption> lstOptionColor { get; set; }
            
    public List<VFC94_VehicleItemVO> lstVehico {get; set;}  

    public VFC80_VehicleVO() 
    {
        lstVehico = new List<VFC94_VehicleItemVO>();    
    } 
}