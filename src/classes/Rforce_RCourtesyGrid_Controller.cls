/**
  * @Class Name : Rforce_RCourtesyGrid_Controller
  * @author: Vetrivel Sundararajan
  * @date: 09/06/2015
  * @description: This apex class will be used to update the Standard and Maxi DailyRate
  * amount and this amount will be used in Replacement Goodwill Amount in Goodwill Object.  
 */

public with sharing class Rforce_RCourtesyGrid_Controller {

 User user {get; set;} 

 public RCourtesyGrid__c rCourtesyGrid{get;set;}
 public List<RCourtesyGrid__c> rCourtesyGridList{get;set;}
 public String  strCarSegmentName{get;set;}
 public Integer intStandardDailyRate{get;set;}
 public Integer intMaxiDailyRate{get;set;}
 public boolean displaySection;

/**
 * Constructor Name : Rforce_RCourtesyGrid_Controller
 * @description: This Constructor will be used to fetch the particular user record values
 * and based on the user Record Default Country which will be used to fetch values from RCourtesyGrid__c Object. 
*/ 

 public Rforce_RCourtesyGrid_Controller() {
  
    user = [Select Id, RecordDefaultCountry__c, UserType__c from user where Id = :UserInfo.getUserId()];
    rCourtesyGridList=[Select CarSegmentName__c,Country__c,MaxiDailyRate__c,StandardDailyRate__c from RCourtesyGrid__c where Country__c = :user.RecordDefaultCountry__c];
    rCourtesyGrid= new RCourtesyGrid__c();
    displaySection=false;
 }

 public boolean getDisplaySection() {        
    return displaySection;       
 }     
  
  public void save() {
   update rCourtesyGridList;
  }
  
  public void create() {
    displaySection=true;
  }
  
  public void cancelRecord(){
    displaySection=false;
  }

/**
 * @Method Name : createNewGridRecord
 * @description: This method will be used to update the record in the RCourtesyGrid Object.
*/ 

  public Pagereference createNewGridRecord() {
  
    system.debug('## Inside createNewGridRecord ##');
    PageReference pageRef= new PageReference('/apex/Rforce_RCourtesyGrid_Page'); 
    pageRef.setRedirect(true); 
           
    displaySection=false;
   
    rCourtesyGrid.CarSegmentName__c=strCarSegmentName;
    rCourtesyGrid.StandardDailyRate__c=intStandardDailyRate;
    rCourtesyGrid.MaxiDailyRate__c=intMaxiDailyRate;
    rCourtesyGrid.Country__c=user.RecordDefaultCountry__c;

    try {                
       upsert rCourtesyGrid;      
    } catch (System.DmlException e) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getDmlMessage(0)));              
        } 
     
     return pageRef;
  }
}