/* This class is to redirect users to Case  View - Reena 14/04/2013 */
public with sharing class VFC02_CaseRedirect {

      
   //This method is used to redirect users to case Tab/Case View Page
    public PageReference RedirectPage(){
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        If(usrProfileName =='Service Profile'|| usrProfileName =='CORE - Service Profile' ){
            PageReference EduListPage = new PageReference('/apex/VFP01_CaseView');
            EduListPage.setRedirect(true);
            return EduListPage;
        }
        else{
            return new PageReference('/500');
        } 
    }

    

}