/**
	Class   -   VFC08_RecordTypeDAO
    Author  -   Ramesh Prabu
    Date    -   05/10/2012
    
    #01 <Suresh Babu> <05/10/2012>
        Created this Class to handle record from RecordType - related Queries.
**/
public with sharing class VFC08_RecordTypeDAO
{
	private static final VFC08_RecordTypeDAO instance = new VFC08_RecordTypeDAO();
    
    private VFC08_RecordTypeDAO(){}

    public static VFC08_RecordTypeDAO getInstance(){
        return instance;
    }
    
    /**
     * This Method was used to get RecordType Record based on DeveloperName
     */
    public RecordType findRecordTypeByDeveloperName(String DevName)
    {
        RecordType recType = 
        	[SELECT r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, r.Description 
		   	   FROM RecordType r 
			  WHERE r.DeveloperName =: DevName];
        return recType;
    }
    
    /**
     *
     */
    public Id fetchRecordTypeIdUsingDevName(String DevName)
    {
        RecordType recType = 
        	[SELECT r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, r.Description 
			   FROM RecordType r 
			  WHERE r.DeveloperName =: DevName];
        return recType.Id;
    }
    
    /**
	 * Return a Map of RecordType Ids given a set of developer names
	 * The key is the developer name
	 */
	public Map<String, Id> getRecordTypeIdByDevNameSet(Set<String> setDevName)
	{
		try
		{
			Map<String, Id> mapRec = new Map<String, Id>(); 
		    List<RecordType> listRecordType = [SELECT Id, Name, DeveloperName
		                          			     FROM RecordType
		                              			WHERE DeveloperName IN :setDevName];
		    for (RecordType rec : listRecordType)
		    {
		    	mapRec.put(rec.DeveloperName, rec.Id);
		    }
		    return mapRec;
		}
		catch (Exception e){
			return null;
		}
	}
}