@IsTest
private class DashLembreteServicoTest {

    private static User user;
    private static Account accDealer;

    static {
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr) {
            //Test.startTest();

            accDealer = MyOwnCreation.getInstance().criaAccountDealer();
            INSERT accDealer;

            Contact ctt = new Contact(
                CPF__c 		=  '44476157114',
                Email 		= 'manager1@org1.com',
                FirstName 	= 'User',
                LastName 	= 'Manager1',
                MobilePhone = '11223344556',
                AccountId 	= accDealer.Id
            );

            INSERT ctt;

            user = new User(
                ContactId 			= ctt.Id,
                ProfileId 			= Utils.getProfileId('BR - Renault + Cliente Manager'),
                FirstName 			= 'User',
                LastName 			= 'Manager1',
                Username 			= 'manager1@org1.com.teste',
                Email 				= 'manager1@org1.com',
                Alias 				= 'ma1',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c 				= '123456',
                EmailEncodingKey	='UTF-8',
                LanguageLocaleKey	='en_US',
                LocaleSidKey		='en_US',
                TimeZoneSidKey		='America/Los_Angeles'
            );

            INSERT user;
            //Test.stopTest();
        }
    }

    static testMethod void deveObterLembreteUser() {
      System.RunAs(user) {
        final List<Task> taskList =
          TaskBuild.getInstance().createListTask(user.Id, null, 10);
        INSERT taskList;
        final List<Event> eventList =
          EventBuild.getInstance().createListEvent(user.Id, null, 10);
        INSERT eventList;

       final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        List<DashLembreteServico.Lembrete> lembretes = dashLembreteServico.obterLembrete(user.Id);
        System.assertEquals(20, lembretes.size());
      }
    }

    static testMethod void deveOrdenaPorDataAsc() {
        List<Task> taskList = TaskBuild.getInstance().createListTask(user.Id, null, 10);
        INSERT taskList;
        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        List<DashLembreteServico.Lembrete> lembretes = dashLembreteServico.obterLembrete(user.Id);
        lembretes = dashLembreteServico.sortLembrete(lembretes, 1);
        System.assertEquals('Task0', lembretes.get(0).Subject);
    }

    static testMethod void deveOrdenaPorDataDesc() {
        List<Task> taskList =
          TaskBuild.getInstance().createListTask(user.Id, null, 10);
        INSERT taskList;
        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        List<DashLembreteServico.Lembrete> lembretes = dashLembreteServico.obterLembrete(user.Id);
        lembretes = dashLembreteServico.sortLembrete(lembretes, 0);
        System.assertEquals('Task9', lembretes.get(0).Subject);
    }

    static testMethod void deveObterLembreteOportunity() {
      System.RunAs(user) {
        Opportunity opp = OpportunityBuild.getInstance()
          .createOpportunity(user.Id, accDealer.Id, accDealer.Id );
          INSERT opp;

        List<Task> taskList = TaskBuild.getInstance().createListTask(user.Id, opp.Id, 10);
        INSERT taskList;

        List<Event> eventList = EventBuild.getInstance().createListEvent(user.Id, opp.Id, 10);
        INSERT eventList;

        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        List<DashLembreteServico.Lembrete> lembretes = dashLembreteServico.obterLembrete(opp);
        System.assertEquals(20, lembretes.size());
      }
    }

    static testMethod void deveCriarLembretePersecution() {
      System.RunAs(user) {
        final Opportunity opp = OpportunityBuild.getInstance()
          .createOpportunity(user.Id, accDealer.Id, accDealer.Id );
        opp.ChaseOpportunity__c = true;
        INSERT opp;

        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        final DashLembreteServico.Lembrete lembrete = dashLembreteServico.createLembrete(opp);
      }
    }

    static testMethod void deveCriarLembreteRescue() {
      System.RunAs(user) {
        final Opportunity opp = OpportunityBuild.getInstance()
          .createOpportunity(user.Id, accDealer.Id, accDealer.Id );
        opp.RescueOpportunity__c = true;
        INSERT opp;

        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        final DashLembreteServico.Lembrete lembrete = dashLembreteServico.createLembrete(opp);
      }
    }

    static testMethod void deveCriarLembreteDeliveryConfirmation() {
      System.RunAs(user) {
        final Opportunity opp = OpportunityBuild.getInstance()
          .createOpportunity(user.Id, accDealer.Id, accDealer.Id );
        opp.ConfirmDeliveryOpportunity__c = true;
        INSERT opp;

        final DashLembreteServico dashLembreteServico = new DashLembreteServico();
        final DashLembreteServico.Lembrete lembrete = dashLembreteServico.createLembrete(opp);
      }
    }
}