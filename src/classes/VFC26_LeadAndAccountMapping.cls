public with sharing class VFC26_LeadAndAccountMapping
{
    private static final VFC26_LeadAndAccountMapping instance = new VFC26_LeadAndAccountMapping();
    
    private VFC26_LeadAndAccountMapping(){}

    public static VFC26_LeadAndAccountMapping getInstance(){
        return instance;
    }
    
    /**
     * Account object fields mapping to Lead object fields 
     * @param accountRecord - Account record map to lead record
     * @param transactionRecordId - Transaction record map to lead record
     * @return leadRecord - Map and return the lead fields.
     */
    public Lead accountToLeadMapping(Id LeadRecordId, Account accountRecord, Id transactionRecordId)
    {
        Lead leadRecord;
        
        // if check account id is not equal to null
        if (LeadRecordId != null) {
            leadRecord = new Lead(Id = LeadRecordId);
        }
        else {
            leadRecord = new Lead();
            
            /* Start Code - Used to origins in Lead, only for new leads   --- by @Edvaldo - kolekto   -----  */
            Case c = VFC19_AccountCustomerActivity.CaseRecord;
            if(c != null){
                if(c.Type == 'Other' && c.Origin == 'RENAULT SITE' && c.CaseSubSource__c == 'Webform'){
                    leadRecord.LeadSource                           =   'CSC';
                    leadRecord.SubSource__c                         =   'RENAULT SITE';
                    leadRecord.Detail__c                            =   'SUGGESTION';
                }
                else if(c.Type == 'Service Request' && c.Origin == 'RENAULT SITE' && c.CaseSubSource__c == 'Webform' && c.SubType__c == 'Communication & Events'){
                    leadRecord.LeadSource                           =   'CSC';
                    leadRecord.SubSource__c                         =   'RENAULT SITE';
                    leadRecord.Detail__c                            =   'NEWS';
                }
                else if(c.Type == 'Service Request' && c.Origin == 'RENAULT SITE' && c.CaseSubSource__c == 'Webform' && c.SubType__c == 'Brochure Request'){
                    leadRecord.LeadSource                           =   'CSC';
                    leadRecord.SubSource__c                         =   'RENAULT SITE';
                    leadRecord.Detail__c                            =   'BROCHURES';
                }
                else if(c.Origin == 'RENAULT SITE' && c.CaseSubSource__c == 'CHAT'){
                    leadRecord.LeadSource                           =   'CSC';
                    leadRecord.SubSource__c                         =   'RENAULT SITE';
                    leadRecord.Detail__c                            =   'CHAT – LIVE AGENT';
                }
                else{
                    leadRecord.LeadSource                           =   'CSC';
                    leadRecord.SubSource__c                         =   'RECEPTIVE PHONE';
                }
            }else{
                leadRecord.LeadSource                           =   'CSC';
                leadRecord.SubSource__c                         =   'RECEPTIVE PHONE';
            }
            /* Finish Code - Used to origins of Lead  --- by @Edvaldo - kolekto   -----  */
        }
        
        leadRecord.NonEffectiveContact__c               =   0; // Start with zero
        leadRecord.Street                               =   accountRecord.ShippingStreet;
        leadRecord.City                                 =   accountRecord.ShippingCity;
        leadRecord.State                                =   accountRecord.ShippingState;
        leadRecord.PostalCode                           =   accountRecord.ShippingPostalCode;
        leadRecord.Country                              =   accountRecord.ShippingCountry;
        leadRecord.isResAddressRef__c                   =   accountRecord.isResAddressRef__c;
        leadRecord.RefAddressStreet__c                  =   accountRecord.RefAddressStreet__c;
        leadRecord.RefAddressCity__c                    =   accountRecord.RefAddressCity__c;
        leadRecord.RefAddressPostalCode__c              =   accountRecord.RefAddressPostalCode__c;
        leadRecord.RefAddressState__c                   =   accountRecord.RefAddressState__c;
        leadRecord.RefAddressCountry__c                 =   accountRecord.RefAddressCountry__c;
        leadRecord.FirstName                            =   accountRecord.FirstName;
        leadRecord.LastName                             =   accountRecord.LastName;
        leadRecord.MobilePhone                          =   accountRecord.PersMobPhone__c;
        leadRecord.AddressDateUpdated__c                =   accountRecord.AddressDate__pc;
        leadRecord.AddressOK__c                         =   accountRecord.AddressOK_BR__c;
        leadRecord.AddressUpdated__c                    =   accountRecord.Address__pc;
        leadRecord.AfterSales__c                        =   accountRecord.AfterSales_BR__c;
        leadRecord.AfterSalesOffer__c                   =   accountRecord.AfterSalesOffer_BR__c;
        leadRecord.BehaviorCustomer__c                  =   accountRecord.BehaviorCustmr_BR__c;
        leadRecord.Birthdate__c                         =   accountRecord.PersonBirthdate;
        leadRecord.BusinessMobilePhone__c               =   accountRecord.ProfMobPhone__c;
        leadRecord.BusinessMobilePhoneDateUpdated__c    =   accountRecord.ProfMobiPhoneDate__pc;
        leadRecord.BusinessMobilePhoneUpdated__c        =   accountRecord.ProfMobiPhone__pc;
        leadRecord.BusinessPhone__c                     =   accountRecord.ProfLandline__c;
        leadRecord.BusinessPhoneDateUpdated__c          =   accountRecord.ProPhoneDate__pc;
        leadRecord.BusinessPhoneUpdated__c              =   accountRecord.ProfPhone__pc;
        leadRecord.CadastralCustomer__c                 =   accountRecord.CadastralCustmer_BR__c;
        leadRecord.ContemplationLetter__c               =   accountRecord.ContemplationLetter_BR__c;
        leadRecord.CPF_CNPJ__c                          =   accountRecord.CustomerIdentificationNbr__c;
        leadRecord.DateOfContemplation__c               =   accountRecord.DateContemplation_BR__c;
        leadRecord.DealerOfInterest__c                  =   accountRecord.IDBIRPrefdDealer__c;
        leadRecord.EmailOK__c                           =   accountRecord.EmailOK_BR__c;
        leadRecord.Email                                =   accountRecord.PersEmailAddress__c;
        leadRecord.EndDateOfFinanciating__c             =   accountRecord.EndDateOfFinanciating_BR__c;
        leadRecord.HomePhone__c                         =   accountRecord.PersLandline__c;
        leadRecord.HomePhoneDateUpdated__c              =   accountRecord.PersPhoneDate__pc;
        leadRecord.HomePhoneUpdated__c                  =   accountRecord.PersPhone__pc;
        leadRecord.InformationUpdatedInTheLast5Years__c =   accountRecord.InfoUpdatedLast5Yrs_BR__c;
        leadRecord.MarketdatasCluster__c                =   accountRecord.MktdatasCluster_BR__c;
        leadRecord.NUMDBM__c                            =   accountRecord.NUMDBM_BR__c;
        leadRecord.ParticipateOnOPA__c                  =   accountRecord.ParticipateOnOPA_BR__c;
        leadRecord.PersonalEmailDateUpdated__c          =   accountRecord.PersEmailDate__pc;
        leadRecord.PersonalEmailUpdated__c              =   accountRecord.PersEmail__pc;
        leadRecord.PersonalMobilePhoneDateUpdated__c    =   accountRecord.PersMobiPhoneDate__pc;
        leadRecord.PersonalMobilePhoneUpdated__c        =   accountRecord.PersMobiPhone__pc;
        leadRecord.PhoneNumberOK__c                     =   accountRecord.PhoneNumberOK_BR__c;
        leadRecord.PreApprovedCredit__c                 =   accountRecord.PreApprovedCredit_BR__c;
        leadRecord.PropensityModel__c                   =   accountRecord.PropensityModel_BR__c;
        leadRecord.RecentBirthdate__c                   =   accountRecord.RecentBirthdate_BR__c;
        leadRecord.SMSDateUpdated__c                    =   accountRecord.SMSDate__pc;
        leadRecord.SMSUpdated__c                        =   accountRecord.SMS__pc;
        leadRecord.ValueOfCredit__c                     =   accountRecord.ValueOfCredit__c;
        leadRecord.VehicleOfInterest__c                 =   accountRecord.VehicleInterest_BR__c;
        leadRecord.CustomerProfile__c                   =   accountRecord.CustomerProfile__c;

        if(transactionRecordId != null){
            /* fetch  transaction record using transaction Id */
            TST_Transaction__c transactionRecord =  VFC06_TransactionDAO.getInstance().findTransactionById(transactionRecordId);
            leadRecord.CRV_CurrentVehicle__c                =   transactionRecord.CurrentVehicle__c;
            leadRecord.CurrentMilage__c                     =   transactionRecord.CurrentMilage__c;
            leadRecord.IntentToPurchaseNewVehicle__c        =   transactionRecord.IntentToPurchaseNewVehicle__c;
            leadRecord.IsRenaultVehicleOwner__c             =   transactionRecord.IsRenaultVehicleOwner__c;
            leadRecord.IsVehicleOwner__c                    =   transactionRecord.IsVehicleOwner__c;
        }
        
        leadRecord.OptInDealer__c                       =   accountRecord.OptInDealer__c;
        leadRecord.Event__c                             =   accountRecord.Event__c;
        leadRecord.OtherInformation__c                  =   accountRecord.OtherInformation__c;
        leadRecord.ParticipatedInCampaigns__c           =   accountRecord.ParticipatedInCampaigns__c;
        leadRecord.OptinEmail__c                        =   accountRecord.OptinEmail__c;
        leadRecord.OptinPhone__c                        =   accountRecord.OptinPhone__c;
        leadRecord.OptinSMS__c                          =   accountRecord.OptinSMS__c;
        leadRecord.RFV__c                               =   accountRecord.RFV__c;
        leadRecord.SecondVehicleOfInterest__c           =   accountRecord.SecondVehicleOfInterest__c;
        leadRecord.TestDriveDate__c                     =   accountRecord.TestDriveDate__c;
        leadRecord.TestDriveDone__c                     =   accountRecord.TestDriveDone__c;
        leadRecord.TestDriveModel__c                    =   accountRecord.TestDriveModel__c;
        leadRecord.TypeOfInterest__c                    =   accountRecord.TypeOfInterest__c;
        leadRecord.VehicleOfCampaign__c                 =   accountRecord.VehicleOfCampaign__c;
        leadRecord.Status                               =   'Identified';
        leadRecord.Account__c                           =   accountRecord.Id;

        return leadRecord;
    }
     /* Lead object fields mapping to Account object fields 
     *  @param leadRecord  - passing the lead fields
        @param recordTypeId  - passing the recordTypeId to account object
        @param accountId  - passing the accountId and get already exist account record fields
     *  @return updatedAccount - Map and return the updatedAccount fields.
     */
    
    public Account leadToAccountMapping( Lead leadRecord, Lead leadOld , Id accountId){
        
        String recordTypeId = Utils.getRecordTypeId('Account','Personal_Acc');
        String leadVendasPF = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
        Account updatedAccount;
        
        /* check if exist an account */
        if( accountId != null ){
            updatedAccount = new Account( Id = accountId );
            
        /* else create an Account*/
        }else{
            updatedAccount = new Account();
            
            // Update origins only to news accounts - origins by @Edvaldo - kolekto
            if(leadOld != null){
                updatedAccount.Source__c = leadOld.LeadSource;
                updatedAccount.AccSubSource__c = leadOld.SubSource__c;
                updatedAccount.Detail__c = leadOld.Detail__c;
                updatedAccount.Sub_Detail__c = leadOld.Sub_Detail__c;
                if(leadOld.RecordTypeId.equals(leadVendasPF)){
                    updatedAccount.Cliente_DVE__c = true;
                    updatedAccount.Type_DVE__c = leadOld.Type_DVE__c;
                }
            }
        }
        
        //Only to DVE account in update and insert - by @Edvaldo - kolekto
        if(leadRecord != null && leadRecord.RecordTypeId.equals(leadVendasPF)){
            updatedAccount.Cliente_DVE__c = true;
            if(String.isNotEmpty(leadRecord.Type_DVE__c))
                updatedAccount.Type_DVE__c = leadRecord.Type_DVE__c;
        }
        
        updatedAccount.Shipping_Street__c           =   leadRecord.Street__c;
        updatedAccount.ShippingStreet               =   leadRecord.Street__c;
        updatedAccount.Shipping_Number__c           =   leadRecord.Number__c;
        updatedAccount.Shipping_Complement__c       =   leadRecord.Complement__c;
        updatedAccount.Shipping_Neighborhood__c     =   leadRecord.Neighborhood__c;
        updatedAccount.Shipping_City__c             =   leadRecord.City__c ;
        updatedAccount.Shipping_State__c            =   leadRecord.State__c ;
        updatedAccount.Shipping_PostalCode__c       =   leadRecord.PostalCode__c;
        updatedAccount.ShippingPostalCode           =   leadRecord.PostalCode__c;
        updatedAccount.ShippingCity                 =   leadRecord.City;
        updatedAccount.Shipping_Country__c          =   leadRecord.Country__c  ;
        updatedAccount.isResAddressRef__c           =   leadRecord.isResAddressRef__c;
        updatedAccount.RefAddressStreet__c          =   leadRecord.RefAddressStreet__c;
        updatedAccount.RefAddressNumber__c          =   leadRecord.RefAddressNumber__c;
        updatedAccount.RefAddressComplement__c      =   leadRecord.RefAddressComplement__c;
        updatedAccount.RefAddressNeighborhood__c    =   leadRecord.RefAddressNeighborhood__c;
        updatedAccount.RefAddressCity__c            =   leadRecord.RefAddressCity__c;
        updatedAccount.RefAddressPostalCode__c      =   leadRecord.RefAddressPostalCode__c;
        updatedAccount.RefAddressState__c           =   leadRecord.RefAddressState__c;
        updatedAccount.RefAddressCountry__c         =   leadRecord.RefAddressCountry__c;
        updatedAccount.FirstName                    =   leadRecord.FirstName  ;
        updatedAccount.LastName                     =   leadRecord.LastName ;
        updatedAccount.PersMobPhone__c              =   leadRecord.MobilePhone ;
        updatedAccount.AddressDate__pc              =   leadRecord.AddressDateUpdated__c;
        updatedAccount.AddressOK_BR__c              =   leadRecord.AddressOK__c ;
        updatedAccount.Address__pc                  =   leadRecord.AddressUpdated__c ;
        updatedAccount.AfterSales_BR__c             =   leadRecord.AfterSales__c;
        updatedAccount.AfterSalesOffer_BR__c        =   leadRecord.AfterSalesOffer__c ;
        updatedAccount.BehaviorCustmr_BR__c         =   leadRecord.BehaviorCustomer__c ;
        updatedAccount.PersonBirthdate              =   leadRecord.Birthdate__c ;
        updatedAccount.ProfMobPhone__c              =   leadRecord.BusinessMobilePhone__c ;
        updatedAccount.ProfMobiPhoneDate__pc        =   leadRecord.BusinessMobilePhoneDateUpdated__c ;
        updatedAccount.ProfMobiPhone__pc            =   leadRecord.BusinessMobilePhoneUpdated__c ;
        updatedAccount.ProfLandline__c              =   leadRecord.BusinessPhone__c ;
        updatedAccount.ProPhoneDate__pc             =   leadRecord.BusinessPhoneDateUpdated__c ;
        updatedAccount.ProfPhone__pc                =   leadRecord.BusinessPhoneUpdated__c ;
        updatedAccount.CadastralCustmer_BR__c       =   leadRecord.CadastralCustomer__c  ;
        updatedAccount.ContemplationLetter_BR__c    =   leadRecord.ContemplationLetter__c ;
        updatedAccount.CustomerIdentificationNbr__c =   leadRecord.CPF_CNPJ__c ;
        //updatedAccount.CurrentVehicle_BR__c           =   leadRecord.CRV_CurrentVehicle__c ;
        updatedAccount.DateContemplation_BR__c      =   leadRecord.DateOfContemplation__c ;
        updatedAccount.IDBIRPrefdDealer__c          =   leadRecord.DealerOfInterest__c ;
        updatedAccount.Description					=	leadRecord.Description; 
        updatedAccount.PersEmailAddress__c          =   leadRecord.Email;
        updatedAccount.EmailOK_BR__c                =   leadRecord.EmailOK__c  ;
        updatedAccount.EndDateOfFinanciating_BR__c  =   leadRecord.EndDateOfFinanciating__c ;
        updatedAccount.PersLandline__c              =   leadRecord.HomePhone__c;
        updatedAccount.HomePhone__c                 =   leadRecord.HomePhone__c;
        updatedAccount.PersPhoneDate__pc            =   leadRecord.HomePhoneDateUpdated__c;
        updatedAccount.PersPhone__pc                =   leadRecord.HomePhoneUpdated__c ;
        updatedAccount.InfoUpdatedLast5Yrs_BR__c    =   leadRecord.InformationUpdatedInTheLast5Years__c ;
        updatedAccount.MktdatasCluster_BR__c        =   leadRecord.MarketdatasCluster__c ;
        updatedAccount.NUMDBM_BR__c                 =   leadRecord.NUMDBM__c ;
        updatedAccount.ParticipateOnOPA_BR__c       =   leadRecord.ParticipateOnOPA__c ;
        updatedAccount.PersEmailDate__pc            =   leadRecord.PersonalEmailDateUpdated__c ;
        updatedAccount.PersEmail__pc                =   leadRecord.PersonalEmailUpdated__c ;
        updatedAccount.PersMobiPhoneDate__pc        =   leadRecord.PersonalMobilePhoneDateUpdated__c ;
        updatedAccount.PersMobiPhone__pc            =   leadRecord.PersonalMobilePhoneUpdated__c ;
        updatedAccount.PhoneNumberOK_BR__c          =   leadRecord.PhoneNumberOK__c ;
        updatedAccount.PreApprovedCredit_BR__c      =   leadRecord.PreApprovedCredit__c ;
        updatedAccount.PropensityModel_BR__c        =   leadRecord.PropensityModel__c ;
        updatedAccount.RecentBirthdate_BR__c        =   leadRecord.RecentBirthdate__c  ;
        updatedAccount.SMSDate__pc                  =   leadRecord.SMSDateUpdated__c ;
        updatedAccount.SMS__pc                      =   leadRecord.SMSUpdated__c ;
        updatedAccount.ValueOfCredit__c             =   leadRecord.ValueOfCredit__c ;
        //updatedAccount.VehicleInterest_BR__c        =   leadRecord.VehicleOfInterest__c ;
        updatedAccount.VehicleInterest_BR__c        =   leadRecord.Vehicle_of_Interest_Formula__c;
        updatedAccount.CustomerProfile__c           =   leadRecord.CustomerProfile__c;
        updatedAccount.LeadSource__c                =   leadRecord.LeadSource ;
        updatedAccount.LeadSubsource__c             =   leadRecord.SubSource__c ; 
        updatedAccount.OptInDealer__c               =   leadRecord.OptInDealer__c;
        updatedAccount.Event__c                     =   leadRecord.Event__c;
        updatedAccount.OtherInformation__c          =   leadRecord.OtherInformation__c;
        updatedAccount.ParticipatedInCampaigns__c   =   leadRecord.ParticipatedInCampaigns__c;
        updatedAccount.OptinEmail__c                =   leadRecord.OptinEmail__c;
        updatedAccount.OptinPhone__c                =   leadRecord.OptinPhone__c;
        updatedAccount.OptinSMS__c                  =   leadRecord.OptinSMS__c;
        updatedAccount.RFV__c                       =   leadRecord.RFV__c;
        updatedAccount.SecondVehicleOfInterest__c   =   leadRecord.SecondVehicleOfInterest__c;
        updatedAccount.TestDriveDate__c             =   leadRecord.TestDriveDate__c;
        updatedAccount.TestDriveDone__c             =   leadRecord.TestDriveDone__c;
        updatedAccount.TestDriveModel__c            =   leadRecord.TestDriveModel__c;
        updatedAccount.TypeOfInterest__c            =   leadRecord.TypeOfInterest__c;
        updatedAccount.VehicleOfCampaign__c         =   leadRecord.VehicleOfCampaign__c;       
        updatedAccount.RecordTypeId                 =   recordTypeId;
        updatedAccount.SemiclairModel__c            =   leadRecord.SemiclairModel__c;
        updatedAccount.SemiclairVersion__c          =   leadRecord.SemiclairVersion__c;
        updatedAccount.SemiclairHarmony__c          =   leadRecord.SemiclairHarmony__c;
        updatedAccount.SemiclairOptional__c         =   leadRecord.SemiclairOptional__c;
        updatedAccount.SemiclairUpholstered__c      =   leadRecord.SemiclairUpholstered__c;
        updatedAccount.SemiclairColor__c            =   leadRecord.SemiclairColor__c;
        updatedAccount.TransactionCode__c           =   leadRecord.TransactionCode__c;
        updatedAccount.RgStateTexto__c              =   leadRecord.RgStateTexto__c;
        updatedAccount.Sex__c                       =   leadRecord.Sex__c;
        updatedAccount.MaritalStatus__c             =   leadRecord.MaritalStatus__c;
        updatedAccount.PersonEmail                  =   leadRecord.Email;
        updatedAccount.ShippingCity                 =   leadRecord.City;
        updatedAccount.ShippingState                =   leadRecord.State;
        updatedAccount.OptinWhatsApp__c             =   leadRecord.OptinWhatsApp__c;
    
        
        return updatedAccount;
    }
}