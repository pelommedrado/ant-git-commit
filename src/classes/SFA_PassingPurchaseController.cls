public class SFA_PassingPurchaseController {
    
    public SFA_PassingPurchaseController(){}
    
    String UserId = UserInfo.getUserId();
    List<User> lsUser = [select accountId from User where id = :UserId];
    String contaId = lsUser.get(0).accountId;
    List<Account> lsAccount = [select Name from Account where id = :contaId];
    String nameAccount;
        
    public List<PassingAndPurchaseOrder__c> lsPurchase{
        get{
            List<PassingAndPurchaseOrder__c> m = new List<PassingAndPurchaseOrder__c>();
            m = Database.query('select Id, Name, Date__c, PAS_Sum__c, PO_Sum__c, PO_and_DS_Sum__c, TDV_Sum__c, Direct_Sales__c, VFS__c, CreatedDate' +
                               ' from PassingAndPurchaseOrder__c where OwnerId = '+ '\'' + UserId + '\' LIMIT 999');
            return m;
        }
    }
    
    public PageReference novo(){
        	PageReference pg = new PageReference('/a1T/e');
            return pg;
        }
    }