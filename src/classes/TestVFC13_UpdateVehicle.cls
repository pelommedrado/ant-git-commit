/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class TestVFC13_UpdateVehicle {
    
    static testMethod void testUpdateVehicle() 
    {     
        
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',BypassVR__c=true,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
        System.runAs(usr)
        {
            Test.startTest();
            
            
            Test.setMock(WebServiceMock.class, new Test_WS01_ApvGetDetVehXml_WebServiceMock());
            
            String x= 'apex_schema_type_info=(http://xml.bvm.icm.apv.bservice.renault, true, false), field_order_type_info=(getApvGetDetVehXmlReturn), getApvGetDetVehXmlReturn=ApvGetDetVehXmlResponse:[apex_schema_type_info=(http://xml.bvm.icm.apv.bservice.renault, true, false),, NFab=J051283, NFab_type_info=(NFab, http://www.w3.org/2001/XMLSchema, string, 1, 1, true), NMot=Q014695, NMot_type_info=(NMot, http://www.w3.org/2001/XMLSchema, string, 1, 1, true)';
            
            WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse responseMock= VFC13_UpdateVehicle.searchBVM('93YLSR1RH8J023030');
            
            VEH_Veh__c veh= new VEH_Veh__c();
            veh.Name='93YLSR1RH8J023030';
            veh.VehicleBrand__c='Renault';
            insert veh; 
            
            Pagereference pageRendered=VFC13_UpdateVehicle.updateVehicleData(responseMock,'93YLSR1RH8J023030',veh.Id);
            
            
            VFC13_UpdateVehicle.getVehicleDetails(veh.Name, veh.Id);
            
            Test.stopTest();
            System.debug('TESTS ENDED');
            
        }
    }
}