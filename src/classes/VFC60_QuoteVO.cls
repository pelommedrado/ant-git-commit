/**
* Classe que representa os dados da página de orçamento.
* @author Thiago Alves dos Santos.
*/ 
public class VFC60_QuoteVO 
{
	/*atributos usados para controle interno, não são utilizados na tela*/
	public String id {get;set;}
	public String mergeId {get;set;}
	public String opportunityId {get;set;}
	public String dealerId {get;set;}
	public Opportunity  fieldsOpportunity {get; set;}
	public Quote sObjQuote {get;set;}
	
	/*atributos referentes a seção informações da Oportunidade*/
	public String opportunityName {get;set;}
	public String customerName {get;set;}
	public String customerId {get;set;}
	public String sellerName {get;set;}
	public String status {get;set;}
	public String reasonCancellation {get;set;}
	public String vehicleInterest {get;set;}
	public String SyncedQuoteId {get;set;}
	
	public List<VFC67_QuoteItemVO> lstQuotes {get; set;}
	
	/*atributos referentes as listas que serão exibidas em tela*/
	public List<SelectOption> lstSelOptionReasonLossCancellationOpp {get;set;}
	public List<SelectOption> lstSelOptionInterestVehicle {get;set;}
	
	/*atributo referente ao motivo de cancelamento da opportunidade*/
	public String reasonLossCancellationOpportunity {get;set;}
	
	/*flags que indicam se os controles estão ou não habilitados/visíveis*/
	public Boolean statusDisabled {get;set;}
	public Boolean reasonCancellationDisabled {get;set;}
	public Boolean disabledFields {get;set;}
	public Boolean commentsDisabled {get;set;}
	public Boolean btnCancelOpportunityDisabled {get;set;}
	public Boolean reasonCancellationPopUpVisible {get;set;}
	
	public VFC60_QuoteVO()
	{
		lstQuotes = new List<VFC67_QuoteItemVO>();
		fieldsOpportunity = new Opportunity();
		this.initialize();
	}
	
	/*atributos referentes a seção informações da Quote*/
	public String quoteName {get;set;}
	public String numberQuote {get;set;}	
	public String vehicle {get;set;} 
    public String vehicleVin {get;set;}
	public String valueTotal {get;set;} 
	public String idcustomer {get;set;}
	public String customerEmail {get;set;}
	public String homePhone {get;set;}
	public String mobilePhone {get;set;}
	public String customerEmailQuote {get;set;}
	public String homePhoneQuote {get;set;}
	public String mobilePhoneQuote {get;set;}	
	public Boolean useCustomerData {get;set;}

	/*Account fields*/
	public String accountFirstName {get;set;}
	public String accountLastName {get;set;}
	public String accountCnpjCpf {get;set;}
	public String accountEmail {get;set;}
	public String accountCelPhone {get;set;}
	public String accountResPhone {get;set;}
	public String accountComPhone {get;set;}
	public String accountFax {get;set;}
	public String accountStreet {get;set;}
	public String accountNumber {get;set;}
	public String accountComplement {get;set;}
	public String accountNeighborhood {get;set;}
	public String accountCountry {get;set;}
	public String accountZipCode {get;set;}
	public String accountState {get;set;}
	public String accountCity {get;set;}
	public String accountMarStatus {get;set;}
	public Date accountBirthday {get;set;}
	public String accountGender {get;set;}
	public String accountRG {get;set;}
	
	/*Billing Account fields*/
	public String tipoCliente {get;set;}
	
	public Quote FKQuote {get;set;}
	public String billFirstName {get;set;}
	public String billLastName {get;set;}
	public String billCpf {get;set;}
	public String billEmail {get;set;}
	public String billCelPhone {get;set;}
	public String billResPhone {get;set;}
	public String billComPhone {get;set;}
	public String billFax {get;set;}
	public String billStreet {get;set;}
	public String billNumber {get;set;}
	public String billComplement {get;set;}
	public String billNeighborhood {get;set;}
	public String billCountry {get;set;}
	public String billZipCode {get;set;}
	public String billState {get;set;}
	public String billCity {get;set;}
	public String billMarStatus {get;set;}
	public Date billBirthday {get;set;}
	public String billGender {get;set;}
	public String billRG {get;set;}
	
	/* PJ */
	public String billPJName {get;set;}
	public String billPJCnpj {get;set;}
	public String billPJPhone {get;set;}
	public String billPJZipCode {get;set;}
	public String billPJState{get;set;}
	public String billPJEmail{get;set;}
	public String billPJStreet{get;set;}
	public String billPJNumber {get;set;}
	public String billPJComplement{get;set;}
	public String billPJNeighborhood{get;set;}
	public String billPJCountry{get;set;}
	public String billPJCity{get;set;}
	public String billPJFax{get;set;}

	/*atributos referentes a seção sintese*/
	public String entry {get;set;}
	public String amountFinanced {get;set;}
	public String usedVehicle {get;set;}
	public String yearUsedVehicle  {get;set;}
	public String priceUsedVehicle {get;set;}
	public Boolean cdcCFinancing {get;set;}
	public Boolean lsgFinancing {get;set;}
	public String numberOfParcels {get;set;}
	public String valueOfParcel {get;set;}
	public String totalPrice {get;set;}
	public String paymentMethods {get;set;}
	public String paymentDetails {get;set;}
	public String usedVehicleLicensePlate {get;set;}
	public String usedVehicleModel {get;set;}
	public String usedVehicleBrand {get;set;}
	

	/*campos padrões do salesforce*/
	public Opportunity sObjOpp {get;set;}
	
	
	private void initialize()
	{	
		this.FKQuote = new Quote();
		this.opportunityName = '';
		this.customerName = '';
		this.sellerName = '';
		this.status = '';
		this.reasonCancellation = '';
		this.vehicleInterest = '';
		this.statusDisabled = false;
		this.reasonCancellationDisabled = false;
		this.disabledFields = false;
		this.commentsDisabled = false;
		this.btnCancelOpportunityDisabled = false;
		this.reasonCancellationPopUpVisible = false;
		
		this.quoteName = '';
		this.vehicle = '';
		this.valueTotal = null;
		this.customerName = '';
		this.customerEmail = '';
		this.homePhone = '';
		this.mobilePhone = '';
		this.customerEmailQuote = '';
		this.homePhoneQuote = '';
		this.mobilePhoneQuote = '';
		this.useCustomerData = false;	
		this.customerEmailQuote = '';
		this.homePhoneQuote = '';
		this.mobilePhoneQuote = '';	
		this.tipoCliente = 'cpf';				
	}	
}