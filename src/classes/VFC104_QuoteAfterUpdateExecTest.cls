/**
    Date: 03/04/2013
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC104_QuoteAfterUpdateExecTest {

    static testMethod void myUnitTestQuoteAfterUpdate1() {
       
        Boolean isInsertedVehiclleBooking = false;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        Quote q = new Quote();        
        PricebookEntry pBookEntry;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;

        // criar nova cotação
        q.Name = 'QuoteNameTest_1';
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb2.Id;
        insert q;
        
        // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;
        
         // selecionar pricebookentry
        pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];

        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = q.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;
           
        VehicleBooking__c vBooking = new VehicleBooking__c();
        vBooking.Quote__c = q.Id;
        vBooking.Vehicle__c = v.Id;
        vBooking.Status__c = 'Active';
        
        try
        {
            insert vBooking;
            isInsertedVehiclleBooking = true;
        }
        catch(DMLException ex) {           
            isInsertedVehiclleBooking = false;
        }
        system.assertEquals(isInsertedVehiclleBooking, true);
        


//=======================================================================
// UPDATE QUOTE 2 - Status: canceled
//=======================================================================        
        try{
            q.Status = 'Canceled';
            update q;
            isInsertedVehiclleBooking = true;   
        }
        catch(DmlException e){
            isInsertedVehiclleBooking = false;
        }

        System.assertEquals(isInsertedVehiclleBooking, true);
        Opportunity op = [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :opp.Id];         
        //System.assertEquals(op.StageName, 'Quote');
       
        VehicleBooking__c vB = [SELECT Id, Status__c FROM VehicleBooking__c     WHERE Id = :vBooking.Id];
        System.assertEquals(vB.Status__c, 'Canceled');
        
        test.stopTest();

//=======================================================================
//=======================================================================

    }
    
    static testMethod void myUnitTestQuoteAfterUpdate2() {
        
        Boolean isInsertedVehiclleBooking = false;
        
        Opportunity opp2 = new Opportunity();
        Quote q2 = new Quote();        
        PricebookEntry pBookEntry2;

        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb22 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp2.Name = 'OppNameTest_1';
        opp2.StageName = 'Identified';
        opp2.CloseDate = Date.today();
        opp2.Pricebook2Id = pb2.Id;
        opp2.CurrencyIsoCode = 'BRL';
        insert opp2;

        // criar nova cotação
        q2.Name = 'QuoteNameTest_1';
        q2.OpportunityId = opp2.Id;
        q2.Pricebook2Id = pb22.Id; 
        
        insert q2;
        
        // criar Vehicle
        VEH_Veh__c v2 = new VEH_Veh__c();
        v2.Name = '12345678999234567';
        v2.Status__c = 'Available';
        insert v2;
        
        // selecionar pricebookentry (seeAllData)
        pBookEntry2 = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb22.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem2 = new QuoteLineItem();
        qLineItem2.QuoteId = q2.Id;
        qLineItem2.Vehicle__c = v2.Id;
        qLineItem2.PricebookEntryId = pBookEntry2.Id;
        qLineItem2.Quantity = 1;
        qLineItem2.UnitPrice = pBookEntry2.UnitPrice;                       
        insert qLineItem2;
           
        VehicleBooking__c vBooking2 = new VehicleBooking__c();
        vBooking2.Quote__c = q2.Id;
        vBooking2.Vehicle__c = v2.Id;
        vBooking2.Status__c = 'Active';
           
        try
        {
            insert vBooking2;
            isInsertedVehiclleBooking = true;
        }
        catch(DMLException ex){         
            isInsertedVehiclleBooking = false;
        }
        
        system.assertEquals(isInsertedVehiclleBooking, true);
        
//=======================================================================
// UPDATE QUOTE 2 - Status: canceled
//=======================================================================        
        try{
            q2.Status = 'Billed';
            update q2;
            isInsertedVehiclleBooking = true;   
        }
        catch(DmlException e){
            isInsertedVehiclleBooking = false;
        }

        //system.assertEquals(isInsertedVehiclleBooking, true);
        Opportunity op2 = [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :opp2.Id];         
        //System.assertEquals(op2.StageName, 'Billed');
       
        VehicleBooking__c vB2 = [SELECT Id, Status__c FROM VehicleBooking__c    WHERE Id = :vBooking2.Id];
        //system.assertEquals(vB2.Status__c, 'Closed');
        
        Test.startTest();

        try{
            q2.Status = 'Canceled';
            update q2;
            isInsertedVehiclleBooking = true;   
        }
        catch(DmlException e){
            isInsertedVehiclleBooking = false;
        }

        //system.assertEquals(isInsertedVehiclleBooking, true);
        op2 = [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :opp2.Id];         
        //System.assertEquals(op2.StageName, 'Quote');
       
        try{
            q2.Status = 'Pre Order Sent';
            update q2;
            isInsertedVehiclleBooking = true;   
        }catch(DmlException e){
            isInsertedVehiclleBooking = false;
        }

        //system.assertEquals(isInsertedVehiclleBooking, true);
        op2 = [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :opp2.Id];         
        //System.assertEquals(op2.StageName, 'Order');

        Test.stopTest();
    }
}