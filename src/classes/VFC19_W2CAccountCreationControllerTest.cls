@isTest
public class VFC19_W2CAccountCreationControllerTest {
    
    static testMethod void testAccountRedirectPage(){
        
        List<Case>caseList = new List<Case>();
        Case case1 = new Case(
            Origin = 'Renault Site',
            CaseSubSource__c = 'WebForm',
            Type = 'Other'
        );
        caseList.add(case1);
        
        Case case2 = new Case(
            Origin = 'Renault Site',
            CaseSubSource__c = 'WebForm',
            Type = 'Service Request',
            SubType__c = 'Communication & Events'
        );
        caseList.add(case2);
        
        Case case3 = new Case(
            Origin = 'Renault Site',
            CaseSubSource__c = 'WebForm',
            Type = 'Service Request',
            SubType__c = 'Brochure Request'
        );
        caseList.add(case3);

        
        Case case4 = new Case(
            Origin = 'Renault Site',
            CaseSubSource__c = 'Chat'
        );
        caseList.add(case4);
  
        
        Case case5 = new Case(
            Origin = 'Renault Site'
        );
        caseList.add(case5);
 
        Database.insert(caseList);
        
        PageReference pg;
        
        pg = startPages(case1.Id);
        pg = startPages(case2.Id);
        pg = startPages(case3.Id);
        pg = startPages(case4.Id);
        pg = startPages(case5.Id);
        
    }
    
    public static PageReference startPages(Id CaseId){
        
        ApexPages.currentPage().getParameters().put('caseId', caseId);
        
        VFC19_W2CAccountcreationPageController controller = new VFC19_W2CAccountcreationPageController();
        PageReference pg = controller.accountRedirectPage();
        system.assertNotEquals(pg, null);
        return pg;
    }
    
}