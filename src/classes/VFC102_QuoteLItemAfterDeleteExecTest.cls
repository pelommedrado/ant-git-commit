/**
    Date: 03/04/2013
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC102_QuoteLItemAfterDeleteExecTest {

    static testMethod void myUnitTestQuoteLItemAfterDelete() {
        
        Opportunity opp = new Opportunity();
        Quote q = new Quote();        
        PricebookEntry pBookEntry;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp.Name = 'OppNameTest_1';
        opp.StageName = 'Identified';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = pb2.Id;
        opp.CurrencyIsoCode = 'BRL';
        insert opp;
                    
        // criar nova cotação
        q.Name = 'QuoteNameTest_1';
        q.OpportunityId = opp.Id;
        q.Pricebook2Id = pb2.Id; 
        insert q;
        
        // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        insert v;

        // selecionar pricebookentry (seeAllData)
        pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                      FROM PricebookEntry 
                      WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                      AND Pricebook2Id =: pb2.Id
                      AND IsActive = true
                      and CurrencyIsoCode = 'BRL'
                      limit 1];
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = q.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;
           
        VehicleBooking__c vBooking = new VehicleBooking__c();
        vBooking.Quote__c = q.Id;
        vBooking.Vehicle__c = v.Id;
        vBooking.Status__c = 'Active';
        insert vBooking;
    
        Test.startTest();
        
        delete qLineItem;
        
        vBooking = [SELECT Status__c FROM VehicleBooking__c WHERE Id = :vBooking.Id];

        Test.stopTest();
        
        system.assertEquals(vBooking.Status__c, 'Canceled');
    }
}