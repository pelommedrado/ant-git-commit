public class VFC45_QualifyingAccountController 
{
	// Private attributes
	private List<String> brandsDetailList;
    private List<String> modelDetailList;
    private List<Campaign> lstCampaign;

    // Public attributes
	public VFC46_QualifyingAccountVO qualifyingAccountVO {get;set;}
    public Boolean cancelButtonDisabled {get;set;}
    public String selectedBrand {get;set;}
    public String selectedModel {get;set;}


	/**
	 * Controller constructor
	 */
    public VFC45_QualifyingAccountController(ApexPages.Standardcontroller controller)
    {
		this.initialize(controller.getId());
    }
    
    /**
     * Initialization process
     */
    private void initialize(String opportunityId)
    {
    	system.debug(LoggingLevel.INFO, '*** initialize() start');

        this.qualifyingAccountVO = VFC133_QualifyingAccountBOWOS.getInstance().getAccountByOpportunityId(opportunityId);

        if (this.qualifyingAccountVO.sObjCampaign == null)
        {
        	system.debug(LoggingLevel.INFO, '*** sObjCampaign = null');
        	this.qualifyingAccountVO.sObjCampaign = new Campaign();	
        	this.qualifyingAccountVO.promoCode = 'Teste';
        }
        
        // set Brazil as default country
        qualifyingAccountVO.shippingCountry = 'Brasil';
        
        if (this.qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c != null)
        {
            selectedBrand = this.qualifyingAccountVO.vehicleInterestBRBrand;
            selectedModel = this.qualifyingAccountVO.vehicleInterestBRModel;
        }
        this.getBrandDetails();
        this.getModelDetails();
        
        system.debug(LoggingLevel.INFO, '*** initialize() end');
	}
        
	/**
	 *
	 */
	public PageReference performTestDrive()
    {
		List<MLC_Molicar__c> lstMolicarDetId = null;
        PageReference pageRef = null;
        ApexPages.Message message = null;
                
        try
        {
    	 	lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
            
            if (!lstMolicarDetID.isEmpty())
            {
            	qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c = lstMolicarDetID[0].Id;          	
            }
   	
            VFC48_QualifyingAccountBusinessDelegate.getInstance().updateDataForTestDriveStage(this.qualifyingAccountVO);
            pageRef = new PageReference('/apex/VFP08_TestDrive?Id=' + this.qualifyingAccountVO.opportunityId);
        }
        catch (Exception e)
        {
            message = new ApexPages.Message(ApexPages.Severity.Confirm, 'Erro ao salvar os dados: ' + e.getMessage());               
            ApexPages.addMessage(message);
        }                   
        return pageRef;
    }

	/**
	 *
	 */
	public PageReference saveOpportunity()
	{		
		List<MLC_Molicar__c> lstMolicarDetId = null;
        PageReference pageRef = null;
        ApexPages.Message message = null;
        
        try
        {
            lstMolicarDetId = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel);
            if (!lstMolicarDetID.isEmpty())
            {
            	qualifyingAccountVO.sObjAccount.CurrentVehicle_BR__c = lstMolicarDetID[0].Id;          	
            }
            VFC48_QualifyingAccountBusinessDelegate.getInstance().updateData(this.qualifyingAccountVO); 
            pageRef = new PageReference('/' + this.qualifyingAccountVO.opportunityId);
        }
        catch (Exception e)
        {		
            message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Erro ao salvar os dados: ' + e.getMessage());
            ApexPages.addMessage(message);
        }
        return pageRef;
    }

	/**
	 *
	 */
    public void openPopUpCancellation()
    {
        this.qualifyingAccountVO.reasonLossCancellation = '';
        this.cancelButtonDisabled = true;       
        
        // verifica se o picklist ainda não foi construído
        if (this.qualifyingAccountVO.lstSelOptionReasonLossCancellation == null)
        {
	        // cria o picklist que exibe as opções de cancelamento da oportunidade
	        this.qualifyingAccountVO.lstSelOptionReasonLossCancellation = VFC49_PickListUtil.buildPickList(Opportunity.ReasonLossCancellation__c, '');
        }
    }
    
    /**
     *
     */
    public void verifyCancellationOption()
    {
		this.cancelButtonDisabled = String.isEmpty(this.qualifyingAccountVO.reasonLossCancellation);
    }
        
	/**
	 *
	 */        
	public PageReference cancelOpportunity()
    {
        PageReference pageRef = null;
        ApexPages.Message message = null;
                        
        try
        {
            VFC48_QualifyingAccountBusinessDelegate.getInstance().updateDataForCancellationStage(qualifyingAccountVO);
            pageRef = new PageReference('/' + this.qualifyingAccountVO.opportunityId);
            return pageRef;
        }
        catch (Exception e)
        {
            message = new ApexPages.Message(ApexPages.Severity.Confirm, 'Erro ao salvar os dados: ' + e.getMessage());
            ApexPages.addMessage(message);
        }
                        
        return pageRef;
    }
        
    /*  
     * Add values to Lead State field in visualforce page.
     * @return option - return the State result and to store values form record
     */
    public List<SelectOption> getStates()
    {
        List<SelectOption> state_Option = new List<SelectOption>();
        state_Option.add(new SelectOption('', ''));
        
        // Hard coded state values.
        state_Option.add(new SelectOption('AC', 'AC'));
        state_Option.add(new SelectOption('AL', 'AL'));
        state_Option.add(new SelectOption('AP', 'AP'));
        state_Option.add(new SelectOption('AM', 'AM'));
        state_Option.add(new SelectOption('BA', 'BA'));
        state_Option.add(new SelectOption('CE', 'CE'));
        state_Option.add(new SelectOption('DF', 'DF'));
        state_Option.add(new SelectOption('ES', 'ES'));
        state_Option.add(new SelectOption('GO', 'GO'));
        state_Option.add(new SelectOption('MA', 'MA'));
        state_Option.add(new SelectOption('MT', 'MT'));
        state_Option.add(new SelectOption('MS', 'MS'));
        state_Option.add(new SelectOption('MG', 'MG'));
        state_Option.add(new SelectOption('PR', 'PR'));
        state_Option.add(new SelectOption('PB', 'PB'));
        state_Option.add(new SelectOption('PA', 'PA'));
        state_Option.add(new SelectOption('PE', 'PE'));
        state_Option.add(new SelectOption('PI', 'PI'));
        state_Option.add(new SelectOption('RJ', 'RJ'));
        state_Option.add(new SelectOption('RN', 'RN'));
        state_Option.add(new SelectOption('RS', 'RS'));
        state_Option.add(new SelectOption('RO', 'RO'));
        state_Option.add(new SelectOption('RR', 'RR'));
        state_Option.add(new SelectOption('SC', 'SC'));
        state_Option.add(new SelectOption('SE', 'SE'));
        state_Option.add(new SelectOption('SP', 'SP'));
        state_Option.add(new SelectOption('TO', 'TO'));

        return state_Option;
    }
    
     /*      
     * get Brand values from Molicar object and add those values to visualforce page picklist.
     * @return option - return the result and to store values form record                     
     */
    public List<Selectoption> getBrands()
    {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', ''));
        
        if (!brandsDetailList.isEmpty())
        {
            // Hard coded top 10 list
            option.add(new SelectOption('RENAULT', 'RENAULT'));
            option.add(new SelectOption('CHEVROLET', 'CHEVROLET'));
            option.add(new SelectOption('CITROEN', 'CITROEN'));
            option.add(new SelectOption('FIAT', 'FIAT'));
            option.add(new SelectOption('FORD', 'FORD'));
            option.add(new SelectOption('GMC', 'GMC'));
            option.add(new SelectOption('HONDA', 'HONDA'));
            option.add(new SelectOption('HYUNDAI', 'HYUNDAI'));
            option.add(new SelectOption('PEUGEOT', 'PEUGEOT'));
            option.add(new SelectOption('TOYOTA', 'TOYOTA'));
            option.add(new SelectOption('VOLKSWAGEN', 'VOLKSWAGEN'));

            for (String molicar : brandsDetailList)
            {
				option.add(new SelectOption(molicar, molicar));
            }
        }
        return option;
    }
    
    /*      
     * get Model values from Molicar object 
     * @return option - return the molicar  model result and to store values form record
     */
    public List<Selectoption> getModels()
    {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('', ''));
        
        // check Brand name must not be null and the model names list also not equalto null
        if ((selectedBrand != null || selectedBrand != '') && modelDetailList != null)
        {
            if (!modelDetailList.isEmpty())
            {
                for (String model : this.modelDetailList)
                {
                	option.add(new SelectOption(model, model));
                }
            }
        }
        return option;
    }
    
    /**
     * get Brand  values from Molicar object using selected brand 
     * @return - dont have any value return 
     */
    public void getBrandDetails()
    {
	    brandsDetailList = new List<String>();
	    AggregateResult[] agrMolicarBrandDetails = VFC11_MolicarDAO.getInstance().findMolicarBrand();

	    if (!agrMolicarBrandDetails.isEmpty())
	    {
            for (AggregateResult Brand : agrMolicarBrandDetails)
            {
				// To avoid duplication add Brand values to set list of Brands.
				brandsDetailList.add(String.valueOf(Brand.get('Brand__c')));
			}
		}
	    brandsDetailList.sort();
	}

	/**
     * get Model values from Molicar object using selected Id 
     * @return - dont have any value return 
     */
    public void getModelDetails()
    {       
		this.modelDetailList = new List<String>();
        List<MLC_Molicar__c> lstMolicarModels = new List<MLC_Molicar__c>();
        
        // get Brand values from Molicar object using selected Brand Id
        lstMolicarModels = VFC11_MolicarDAO.getInstance().findMolicarByBrand(this.selectedBrand);
        
        if (!lstMolicarModels.isEmpty())
        {
            for (MLC_Molicar__c molicar : lstMolicarModels)
            {
				this.modelDetailList.add(molicar.Model__c);
            }
        }
    }

	/**
     * Get the address information based on the zip code.
     */
    public PageReference getAddressGivenZipCode()
    {
    	system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCode()');
    	
    	String zipCodeNumber = qualifyingAccountVO.shippingPostalCode;
    	
    	if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
	    	ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
			system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

			if (zipCode != null) {
				qualifyingAccountVO.shippingStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
				qualifyingAccountVO.shippingCity = zipCode.City__c; 
				qualifyingAccountVO.shippingState = zipCode.State__c;
			}
    	}
		return null;
    }
}