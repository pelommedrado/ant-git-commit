@isTest
private class FaturaDealerBatchTest {
    
    @isTest
    static void test() {
        
        Account acc = new Account(
            name='Test',
            Country__c = 'Brazil',
            Email__c = 'test@test.com',
            Phone = '1122334455',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
            IDBIR__c = '7600999'
        );
        insert(acc);
        
        FaturaDealer__c fat = new FaturaDealer__c();
        fat.NFTIPREG__c = '10';
        fat.NFBIRENV__c = '07600999';
        fat.NFBIREMI__c = '07600997';
        fat.NFSEQUEN__c = '2780';
        fat.NFNROREG__c = '00029';
        fat.NFBANDEI__c = 'REN';
        fat.NFCPFCGC__c = '37970';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFTIPREG__c = '10';
        fat.NFBIRENV__c = '07600999';
        fat.NFBIREMI__c = '07600999';
        fat.NFSEQUEN__c = '2780';
        fat.NFNROREG__c = '00029';
        
        fat.NFBANDEI__c = 'REN';
        fat.NFCODOPR__c = '25';
        fat.NFCODFIS__c = '5403  VN';
        fat.NFTIPONF__c = 'VN';
        fat.NFNRONFI__c = '00073295';
        fat.NFCHASSI__c = '11111111111111911';
        
        fat.NFSERNFI__c = 'U';
        fat.NFDTANFI__c = '20150827';
        fat.NFNOMCLI__c = 'ITAMAR JOSE VIEIRA FERNANDES';
        fat.NFTIPVIA__c = 'RUA';
        fat.NFNOMVIA__c = 'Manoel Mancellos Moura';
        
        fat.NFNROVIA__c = '681';
        fat.NFCPLEND__c = 'AP 103';
        fat.NFBAIRRO__c = 'Canasvieiras';
        fat.NFCIDADE__c = 'FLORIANÓPOLIS';
        fat.NFNROCEP__c = '88054030';
        
        fat.NFESTADO__c = 'SC';
        fat.NFPAISRE__c = '00';
        fat.NFESTCIV__c = '';
        fat.NFSEXOMF__c = 'M';
        fat.NFDTANAS__c = '195908';
        fat.NFTIPCLI__c = 'F';
        fat.NFCPFCGC__c = '37970380972';
        
        fat.NFEMAILS__c = 'pelommedrado@gmail.com';
        fat.NFDDDRES__c = '11';
        fat.NFTELRES__c = '77778888';
        fat.NFDDDCEL__c = '11';
        fat.NFTELCEL__c = '99996666';
        fat.NFANOFAB__c = '2000';
        fat.NFANOMOD__c = '2000';
        
        insert fat;
        
        Test.startTest();
        FaturaDealerBatch c = new FaturaDealerBatch();
        c = new FaturaDealerBatch(null);
        Database.executeBatch(c);
        Test.stopTest();
        
        List<FaturaDealer__c> fatList = [
            SELECT Id, Error_Messages__c, RecordType.Name
            FROM FaturaDealer__c
        ];
        
        for(FaturaDealer__c f : fatList) {
            System.debug('REG:' + f.RecordType.Name + ' Id:' + f.Id + ' Erro: ' + f.Error_Messages__c);
        }
        //System.assertEquals(i, 0);
    }
}