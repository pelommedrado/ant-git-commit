public class VFC156_PV_VersionCallOffer_Util {

    public VFC156_PV_VersionCallOffer_Util (){}
    
    /*public void insertVersionInCallOffer(VersionCallOffer__c versao){
        //System.debug('######## iniciando');
        PVCall_Offer__c chamada = [select id from PVCall_Offer__c where id =:versao.Call_Offer__c];
       //System.debug('######## chamada '+chamada.Id);
        List<PVVersion__c> listVersao = [select name from PVVersion__c where id in (select Version__c from VersionCallOffer__c where Call_Offer__c =: chamada.id)];
        //System.debug('########lista versao'+listVersao);
        String aux ='';
        for(PVVersion__c vers: listVersao)
        	aux += vers.name+';  ';
        
        chamada.Version__c = aux;
        //System.debug('######## chamada modicada'+chamada);
        update chamada;
        //System.debug('######## processo finalizado');
    }*/
    
    //insere as versões na chamada de oferta assim que a ação comercial for clonada
    public void insertVersionCallOffer(List<PVCall_Offer__c> listaChamada){
        List<Id> listIdChamada = new list<Id>();
        for(PVCall_Offer__c cl:listaChamada)
            listIdChamada.add(cl.Id);
        
        List<VersionCallOffer__c> listaVersoes = [select id from VersionCallOffer__c where Call_Offer__c in:(listIdChamada)];
        delete listaVersoes;
        PVCall_Offer__c chamada = new PVCall_Offer__c();
        for(PVCall_Offer__c chaAux : listaChamada){
            chamada = chaAux;
            break;
        }
        List<PVVersion__c> listVersao = [select id, Name from PVVersion__c where Model__c =:chamada.Model__c];
        List<VersionCallOffer__c> listaVersoesInsert = new List<VersionCallOffer__c>();
        for(PVCall_Offer__c chaAux : listaChamada){
            for(PVVersion__c versao : listVersao){
                VersionCallOffer__c versaoChamada = new VersionCallOffer__c();
                    versaoChamada.Call_Offer__c = chaAux.Id;
                    versaoChamada.Version__c = versao.Id;
                    versaoChamada.Status__c = 'Active';
                
                listaVersoesInsert.add(versaoChamada);
            }
        }
        insert listaVersoesInsert;
       /* List<Id> idCallOffer = new List<Id>();
        for(int i=0;i<listaChamada.size();i++){
            idCallOffer.add(listaChamada.get(i));
        }
        List<PVVersion__c> listVersao = [select id, name from PVVersion__c where id in (select id from VersionCallOffer__c where Call_Offer__c in:(idCallOffer) ) ];
        
        List<PVVersion__c> listVersao ;
        List<PVCall_Offer__c> listChamadaAux = new List<PVCall_Offer__c>();
        for(PVCall_Offer__c chamada : listaChamada){
            listVersao = [select name from PVVersion__c where id in (select Version__c from VersionCallOffer__c where Call_Offer__c =: chamada.id)];
       		//chamada.Version__c = String.valueOf(listVersao);
            listChamadaAux.add(chamada);
        }
         update listChamadaAux;
        /* //System.debug('######## iniciando');
       // PVCall_Offer__c chamada = [select id from PVCall_Offer__c where id =:versao.Call_Offer__c];
       //System.debug('######## chamada '+chamada.Id);
        if(chamada.id!=null){
            List<PVVersion__c> listVersao = [select name from PVVersion__c where id in (select Version__c from VersionCallOffer__c where Call_Offer__c =: chamada.id)];
            //System.debug('########lista versao'+listVersao);
            if(listVersao!=null){
                String aux ='';
                for(PVVersion__c vers: listVersao)
                    aux += vers.name+';  ';
                
                chamada.Version__c = aux;
                //System.debug('######## chamada modicada'+chamada);
                update chamada;
            }    
        }
        //System.debug('######## processo finalizado');*/
    }
}