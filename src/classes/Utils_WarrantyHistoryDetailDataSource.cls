public with sharing class Utils_WarrantyHistoryDetailDataSource {

    // Boolean variable used to specify if the dummy implementation must be used or the real one
    public Boolean Test;
    public String modulo;
    public String numInt;
	public String histoBim;
	public String rc;
	  
         public WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response getWarrantyHistoryDetailData(VFC05_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
          
        // --- PRE TREATMENT ----
        WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Request request = new WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Request();       
        //WS05_ApvGetDonIran1.ApvGetListOtsRequestParms requestParameters = new WS05_ApvGetDonIran1.ApvGetListOtsRequestParms();
        WS07_iran2BimIcmApvBserviceRenault.ServicePreferences servicePref = new WS07_iran2BimIcmApvBserviceRenault.ServicePreferences();
        servicePref.vin = VehController.getVin();
        //request.requestParms = requestParameters;    
        servicePref.codPays = System.Label.codPays;  
        servicePref.codlanguage = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        servicePref.numInt = numInt;
        servicePref.modulo = modulo;
        servicePref.histoBim = histoBim;
        servicePref.rc = rc;
        
        System.debug('######### numInt '+numInt+ ' modulo '+modulo+' histoBim '+histoBim+' rc '+rc);    
        request.ServicePreferences = servicePref; 
    
        // ---- WEB SERVICE CALLOUT -----    
        WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2 WHWS = new WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2();
        WHWS.endpoint_x = System.label.VFP05_WarrantyHistoryDetailURL;   
        WHWS.clientCertName_x = System.label.RenaultCertificate;         
        WHWS.timeout_x=40000;
        
        WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response WH_WS = new WS07_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response();    
    
        if (Test==true) {
            WH_WS = Utils_Stubs.WarrantyHistoryDetailStub();    
        } else {
            WH_WS = WHWS.getApvGetDonIran2(request);
        }
         return WH_WS;  
     
    }
}