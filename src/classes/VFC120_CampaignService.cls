public with sharing class VFC120_CampaignService
{

	/**
	 * Build a picklist of available campaigns for this lead
	 * Used in heating lead and account pages
	 * 1º regra: Habilitar apenas o combo de campanhas depois que for selecionado a concessionária 
	 *           de interesse;
	 * 2º regra: Trazer as campanhas que estejam abertas a rede(Sem concessionária relacionada), 
	 *           Permite Compartilhar PA-Ativa = true, Permite Inclusão Manual = true, ativa = true.
	 * 3º regra: Trazer as campanhas ativas que estejam relacionadas com a concessionária de interesse 
	 *           se esta possui as flags ManualInclusionAllowed__c e PAActiveShareAllowed__c ativas.
	 * 4º regra: Trazer as campanhas ativas que estejam relacionadas com a concessionária de interesse e
	 *           que o lead/conta já é membro de campanha e PAActiveShareAllowed__c ativo.
	 */
	public static List<SelectOption> fetchCampaignList(String origen, Id leadId, Id contactId, Id accountDealerId)
	{
		system.debug(LoggingLevel.INFO, '*** fetchCampaignList() begin');

        List<SelectOption> lstOption = new List<SelectOption>();
		lstOption.add(new SelectOption('', '--'+Label.OPPNone+'--'));

		// For 2nd and 3th rules
		Set<Id> setCampaignId = new Set<Id>();
		List<Campaign> lstCampaign = new List<Campaign>(); 
		
		if (origen == 'Dealer')
			lstCampaign = VFC121_CampaignDAO.getInstance().fetchCampaignForDealer(accountDealerId);
		else if (origen == 'PA')
			lstCampaign = VFC121_CampaignDAO.getInstance().fetchCampaignForPA(accountDealerId);
        
        system.debug('$$$'+lstCampaign);
		
		for (Campaign campaign : lstCampaign)
		{
        	if (origen == 'PA' && campaign.ManualInclusionAllowed__c)
        	{
        		system.debug(LoggingLevel.INFO, '*** added #02');
        		// 3th rule
        		lstOption.add(new SelectOption(campaign.Id, campaign.Name));
        	}
        	else if (origen == 'Dealer' && campaign.DealerInclusionAllowed__c)
        	{
        		system.debug(LoggingLevel.INFO, '*** added #03');
        		// 3th rule
        		lstOption.add(new SelectOption(campaign.Id, campaign.Name));
        	}
        	else
        	{
        		system.debug(LoggingLevel.INFO, '*** added #04');
        		// Store for 4th rule to check if is a campaign member
        		setCampaignId.add(campaign.Id);
        	}
		}

		// Check for 4th rule
		List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
		if (leadId != null) {
			lstCampaignMember = VFC128_CampaignMemberDAO.getInstance().
				fetchLeadCampaignMemberFor4thRule(setCampaignId, leadId);
		}
		else if (contactId != null) {
			lstCampaignMember = VFC128_CampaignMemberDAO.getInstance().
				fetchContactCampaignMemberFor4thRule(setCampaignId, contactId);
		}
		
		for (CampaignMember cm : lstCampaignMember) {
			lstOption.add(new SelectOption(cm.campaignId, cm.campaign.Name));
		}
		
		// Sort alphabeticaly
		lstOption.sort();

        return lstOption;
	}
	
	/**
	 * se ela ja foi membro da campanha atualza apenas o status do membro da campanha. 
     * se ele  nao for membro e a campanha permitir inclusao vc adiciona como membro daquela campanha
     * permitir inclusão significa a flag "PA Active Share Allowed" e "Manual Inclusion Allowed" marcadas
	 */
	public static void addOrUpdateLeadToCampaignMember(String origen, Id campaignId, Id leadId, Id contactId)
	{
		system.debug(LoggingLevel.INFO, '*** VFC120_CampaignService.addOrUpdateLeadToCampaignMember()');
		
		system.debug(LoggingLevel.INFO, '*** origen='+origen);
		system.debug(LoggingLevel.INFO, '*** campaignId='+campaignId);
		system.debug(LoggingLevel.INFO, '*** leadId='+leadId);
		system.debug(LoggingLevel.INFO, '*** contactId='+contactId);
		
		if (campaignId != null)
		{
			CampaignMember campaignMember = null;
			if (leadId != null) {
				campaignMember = VFC128_CampaignMemberDAO.getInstance().fetchLeadCampaignMemberOfThisCampaign(campaignId, leadId);
			}
			else if (contactId != null) {
				campaignMember = VFC128_CampaignMemberDAO.getInstance().fetchContactCampaignMemberOfThisCampaign(campaignId, contactId);
			}
			else {
				throw new VFC34_ApplicationException('Campaign: a lead or contact must be informed to add to the campaignMember.');
			}
			
			system.debug(LoggingLevel.INFO, '*** campaignMember='+campaignMember);

			if (campaignMember == null)
			{
				 // The campaign will be shown in heating page only if allow inclusin, but we will
				 // check this again during the save process
				 Campaign campaign = VFC121_CampaignDAO.getInstance().fetchCampaignById(campaignId);

				 if ( (origen == 'PA' && campaign.PAActiveShareAllowed__c) // && campaign.ManualInclusionAllowed__c
				   || (origen == 'Dealer' && campaign.DealerShareAllowed__c) // && campaign.DealerInclusionAllowed__c
				    )
				 {
					CampaignMember cm = new CampaignMember(CampaignId = campaign.Id,
                                                           Status = 'Responded',
                                                           Transferred__c = false,
                                                           LeadId = leadId,
                                                           ContactId = contactId);
					if (origen == 'PA')
						cm.Source__c = 'Marketing';
					else
						cm.Source__c = 'Dealer';

					// Check if the lead/contact belongs to another campaign in the same hierarchy
					// if yes, change Transferred__c to true only for Dealer
					Boolean isTransferred = VFC128_CampaignMemberDAO.getInstance().
						checkIftCampaignMemberShouldBeTransferred(campaign.ParentId, campaign.Id, leadId, contactId);
					system.debug(LoggingLevel.INFO, '*** isTransferred='+isTransferred);
					if (isTransferred)
						cm.Transferred__c = true;

                    insert cm;
                    system.debug(LoggingLevel.INFO, '*** cm='+cm);
				 }	 	
			}
			else
			{
				campaignMember.Status = 'Responded';
				update campaignMember;
			}
		}
	}
}