/** 
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC100_PDFQuoteController_Test {
	
	static testMethod void unitTest01()
    {
    	// Prepare Test Data
    	Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
		}
        Account account1 = new Account(Name = 'Account Test 01', IDBIR__c = '1000100',
                				       RecordTypeId = recTypeIds.get('Network_Site_Acc'));
		insert account1;

		// select Standard price Book (seeAllData)
		Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];

    	Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
							                       StageName = 'Identified',
							                       CloseDate = Date.today() + 30,
							                       OpportunitySource__c = 'NETWORK',
							                       OpportunitySubSource__c = 'THROUGH',
							                       AccountId = account1.Id,
							                       Pricebook2Id = pb2.Id);
		insert opportunity1;

        Quote aQuote = new Quote (Name = 'QuoteNameTest_1',
        					 OpportunityId = opportunity1.Id,
        					 Pricebook2Id = pb2.Id);
        insert aQuote;
		
		 // criar Vehicle
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'Vehicle_100000000';
        v.Status__c = 'Available';
        v.Optional__c = 'option1 + option2 + option3';
        insert v;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
				                     FROM PricebookEntry 
				                     WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
				                     AND Pricebook2Id =: pb2.Id
				                     AND IsActive = true
				                     AND CurrencyIsoCode = 'BRL'
				                     limit 1];
				                     
				                   
        
        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = aQuote.Id;
        qLineItem.Vehicle__c = v.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;                       
        insert qLineItem;

		 /* criar item
        QuoteLineItem qLineItem2 = new QuoteLineItem();
        qLineItem2.QuoteId = aQuote.Id;
        qLineItem2.PricebookEntryId = pBookEntry.Id;
        qLineItem2.Quantity = 1;
        qLineItem2.UnitPrice = 100.00;
        qLineItem2.Description = 'acessorio 1'; 
        qLineItem2.itemDescription = 'Acessório';                
        insert qLineItem2;
		*/
		
		system.debug('GISELE>>'+aQuote.Id);

    	Apexpages.currentPage().getParameters().put('id', aQuote.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(aQuote);

    	// Start Test
    	Test.startTest();

        VFC100_PDFQuoteController controller = new VFC100_PDFQuoteController(stdCont);
        controller.initialize();
        controller.getDealer();        
        controller.getCustomerAdress();
        controller.getAccessories();
        controller.getTotalAccessories();
        controller.getVehicle();
        
        
		// Stop Test
		Test.stopTest();
    }
}