@isTest
private class ConexaoRenaultBatch2Test {

	static {
		FaturaDealer2__c fatura = new FaturaDealer2__c();
		fatura.Customer_Identification_Number__c = '51820533000150';
		fatura.VIN__c = '3425GSVFAGS234637';
		fatura.Invoice_Type__c 	= 'New Vehicle';
		fatura.Manufacturing_Year__c = '1994';
    fatura.Customer_Type__c 		= 'F';
		fatura.Model_Year__c = '1994';
		fatura.Customer_Email__c = 'dadf@sdfdas.com';
		fatura.Phone_1__c = '1127381122';
		fatura.Phone_2__c = '1199098876';
		fatura.Phone_3__c = '1120987761';
		fatura.External_Key__c = '123';
		fatura.Delivery_Date_Mirror__c = System.today();
		fatura.Status__c = 'Valid';
		fatura.Invoice_Date_Mirror__c = System.today();
		Database.insert(fatura);
	}

	@isTest
	static void itShouldConexaoRenaultBatch2() {
			Database.executeBatch(new ConexaoRenaultBatch2());

			final List<FaturaDealer2__c> faturaList = [
				SELECT Id, Invoice_Date_Mirror__c, VN_Survey_Date__c
				FROM FaturaDealer2__c
				WHERE Status__c = 'Valid' AND Customer_Type__c = 'F'
					AND Invoice_Type__c = 'New Vehicle'
					AND Invoice_Date_Mirror__c != NULL AND VN_Survey_Date__c = NULL
			];

			System.assertEquals(false, faturaList.isEmpty());
	}

	static testMethod void itShouldConexaoRenaultSchedulable() {
    String chron = '0 0 23 * * ?';
    System.schedule('Test Sched', chron, new ConexaoRenaultBatch2());
  }
}