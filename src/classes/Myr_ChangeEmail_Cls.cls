/** 
  * Apex class that encapsulates all the work around Change Email
  * @see https://rc-pmo-online.com/ F1855
  * @see HELIOS - MYR & MYD change email management - MM - V3.1 - 20160907.docx
  *
  * @author S. Ducamp
  * @version 16.11: SDP: initial revision
  **/

public class Myr_ChangeEmail_Cls {

	public static final String LIST_FIELDS_TO_MERGE = 'myr_account_delete_reason__c,myr_creationdate__c,'
	+ 'myr_dealer_1_bir__c,myr_dealer_2_bir__c,myr_dealer_3_bir__c,myr_dealer_4_bir__c,myr_dealer_5_bir__c,'
	+ 'myr_last_connection_date__c,myr_prefered_dealer_update_date__c,myr_status__c,myr_status_updatedate__c,'
	+ 'myr_subscriber_dealer_bir__c,myr_subscriber_dealer_ipn__c,myd_account_delete_reason__c,myd_creationdate__c,'
	+ 'myd_dealer_1_bir__c,myd_dealer_2_bir__c,myd_dealer_3_bir__c,myd_dealer_4_bir__c,myd_dealer_5_bir__c,'
	+ 'myd_last_connection_date__c,myd_prefered_dealer_update_date__c,myd_status__c,myd_status_updatedate__c,'
	+ 'myd_subscriber_dealer_bir__c,myd_subscriber_dealer_ipn__c,'
	+ 'my_purchase_intention_brand__c,my_purchase_intention_date__c,my_purchase_intention_model__c,my_purchase_intention_update_date__c';

	/** members **/
	private Long mBeginTime;
	private Account mAccount;
	private User mUser;
	private String mAccountId;
	private String mOutAccountId;
	private String mOutAccountStatus;
	private Date mOutAccountStatusUpdate;
	private String mNewEmail;
	private String mOtherOldLoginId; //The login id previously on gigya of the other brand
	private String mBrand; //Determination of the brand used to call the webservice by checking GIGYA
	private Boolean mNotification; //Boolean to determine if the notification should be generated in the cases 1 & 2

	//HQREQ-06381: identify notifications to copy
	private Map<Id, String> mMapOutAccountNotifIdStatus;

	private Logger mLogger;

	/** current response of change Email **/
	private Myr_ChangeEmail_Response mResponse;

	/** Gigya: keep Renault Gigya in memory to avoid 2 calls **/
	private Map<String, GigyaStructure> mMapGigya;

	/** Custom Exception: only used to simulate test cases **/
	public class Myr_ChangeEmail_Exception extends Exception{}
	/** List of possible exception: to use in emailAddress parameter **/
	public enum TEST_EXCEPTION {EXCP_UPDATE_USER, EXCP_GENERAL}

	//Manage the priority of each Garage Status to make easier the comparision required by the business
	private static final Map<String, Integer> MAP_GARAGE_STATUS_PRIORITY = new Map<String, Integer> {
		system.Label.Myr_GarageStatus_Confirmed => 0
		, system.Label.Myr_GarageStatus_UnConfirmed => 1
		, system.Label.Myr_GarageStatus_Rejected => 2
		, system.Label.Myr_GarageStatus_Taken => 3
	};
	public static Integer getGarageStatusPriority(String status) {
		if (MAP_GARAGE_STATUS_PRIORITY.containsKey(status)) {
			return MAP_GARAGE_STATUS_PRIORITY.get(status);
		}
		//return the lowest priority
		return MAP_GARAGE_STATUS_PRIORITY.keySet().size();
	}

	/** @constructor **/
	public Myr_ChangeEmail_Cls(String accountSfdcId, String emailAddress) {
		mBeginTime = DateTime.now().getTime();
		mAccountId = accountSfdcId;
		mNewEmail = emailAddress;
		mOtherOldLoginId = '';
		mMapGigya = new Map<String, GigyaStructure> ();
		mLogger = new Logger('changeEmail', accountSfdcId, 'Inputs: <accountSfdcId=' + accountSfdcId + ', emailAddress=' + emailAddress + '>');
	}

	/** Main function that processes the email changes **/
	public Myr_ChangeEmail_Response process() {
		try {
			if( Test.isRunningTest() && mNewEmail.startsWithIgnoreCase( TEST_EXCEPTION.EXCP_GENERAL.name() ) ) {
				throw new Myr_ChangeEmail_Exception('Test Exception');
			}

			mResponse = null;

			//Check the inputs and prepare the account and user records
			checkInputs();
			if (mResponse != null) return logAndExit();

			//Trying to determine the brand used to call this webservice ... Very ugly, but used to 'avoid' evolution on Dlbi side :-)
			identifyBrand();
			if (mResponse != null) return logAndExit();

			//Determine if Gigya should be updated and do this update before updating the salesforce user
			updateGigya();
			if (mResponse != null) return logAndExit();

			//Deactivate the other account found (will be merged within this one in asynchronous update)
			deactivateOtherUser();
			if (mResponse != null) return logAndExit();

			//Update the user
			updateUser();
			if (mResponse != null) return logAndExit();

			//Finally if all were OK, update the account (including notifications and merge)
			//Asynchronous update as we cannot change in the same transaction the username (setup) and account information (data)
			//owtherwise, we will get MIXED_DML_EXCEPTION
			asynchronousUpdate(mBrand, mAccountId, mOutAccountId, mNewEmail, mOutAccountStatus, mOutAccountStatusUpdate, mNotification, mMapOutAccountNotifIdStatus);
		} catch(Exception e) {
			system.debug('### Myr_ChangeEmail_Cls - <process> - General Exception: ' + e.getMessage());
			system.debug('### Myr_ChangeEmail_Cls - <process> - Stack Trace: ' + e.getStackTraceString());
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS504,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS504_Msg, new List<String> { e.getMessage() })
			);
			return logAndExit();
		}

		mResponse = new Myr_ChangeEmail_Response(system.Label.Myr_ChangeEmail_WS04MS000, system.Label.Myr_ChangeEmail_WS04MS000_Msg);
		return logAndExit();
	}

	/** Log the current situation in Logger__c and return the response **/
	private Myr_ChangeEmail_Response logAndExit() {
		mLogger.log(mResponse);
		return mResponse;
	}

	/**********************************************************************************************/
	/////// Private functions

	/** Check inputs and prepare the objects required to process change email **/
	private void checkInputs() {
		//check mandatory parameters
		missingMandatory();
		if (mResponse != null) return;
		//check too long parameters
		tooLongParameters();
		if (mResponse != null) return;
		//Try to find the account
		List<Account> listAccounts = [SELECT Id, Country__c, Myr_Status__c, MyRenaultID__c, Myd_Status__c, MyDaciaID__c FROM Account WHERE Id = :mAccountId];
		if (listAccounts.size() != 1) {
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS502,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS502_Msg, new List<String> { mAccountId })
			);
			return;
		}
		mAccount = listAccounts[0];
		mAccountId = listAccounts[0].Id; // get the id on 18 characters for Gigya ......
		//Check Emails
		if (String.isBlank(mAccount.MyRenaultID__c) && String.isBlank(mAccount.MyDaciaID__c)) {
			mResponse = new Myr_ChangeEmail_Response(system.Label.Myr_ChangeEmail_WS04MS505, system.Label.Myr_ChangeEmail_WS04MS505_Msg);
			return;
		}
		//Check the presence of a user linked to the account
		List<User> listUsers = [SELECT Id, Username, Email FROM User WHERE AccountId = :mAccountId];
		if (listUsers.size() != 1) {
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS503,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS503_Msg, new List<String> { mAccountId })
			);
			return;
		}
		mUser = listUsers[0];

		//preparere new email for following treatments
		mNewEmail = mNewEmail.toLowerCase();

		return;
	}

	/** Trying to identify brand by using MyRenaultID and MyDaciaID. If not possible call Gigya
	  to determine if the current MyRenaultID is present in Gigya MyRenault **/
	private void identifyBrand() {
		system.debug('### Myr_ChangeEmail_Cls - <identifyBrand> - BEGIN');
		//Assuming that we have already checked that at least one of the id is filled !
		if (String.isBlank(mAccount.MyRenaultID__c)) {
			mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
			mAccount.MyRenaultID__c = mNewEmail;
		}
		if (String.isBlank(mAccount.MyDaciaID__c)) {
			mBrand = Myr_MyRenaultTools.Brand.Renault.name();
			mAccount.MyDaciaID__c = mNewEmail;
		}
		if (String.isBlank(mBrand)) {
			//Oups, that means that both MyRenaultID and MyDaciaID were filled ...
			//Trying to determine the brand by calling Gigya Renault and compare the MyRenaultID
			//If Gigya Renault contains MyRenaultId, then we can consider than the brand called is MyRenault otherwise, it is MyDacia
			callGigya(mAccountId, Myr_MyRenaultTools.Brand.Renault.name());
			if (mResponse != null) {
				return;
			}
			Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults = mMapGigya.get(Myr_MyRenaultTools.Brand.Renault.name()).gigyaResults;
			//assuming that the new email has already been forced within Gigya ....
			if (gigyaResults.results[0].loginIDs.emails.contains(mNewEmail)) {
				mBrand = Myr_MyRenaultTools.Brand.Renault.name();
			} else {
				mBrand = Myr_MyRenaultTools.Brand.Dacia.name();
			}
		}
		system.debug('### Myr_ChangeEmail_Cls - <identifyBrand> - END - mBrand=' + mBrand);
	}

	/** Check if the mandatory parameters are filled **/
	private void missingMandatory() {
		String missing = '';
		if (String.isBlank(mAccountId)) {
			missing += 'accountSfdcId';
		}
		if (String.isBlank(mNewEmail)) {
			missing += ((String.isBlank(missing)) ? '' : ',') + 'emailAddress';
		}
		if (!String.isBlank(missing)) {
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS501,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS501_Msg, new List<String> { missing })
			);
		}
	}

	/** Check if the parameters are not too long **/
	private void tooLongParameters() {
		String tooLongs = '';
		if (mAccountId.length() > 18) {
			tooLongs += 'accountSfdcId';
		}
		if (mNewEmail.length() > 80) {
			tooLongs += ((String.isBlank(tooLongs)) ? '' : ',') + 'emailAddress';
		}
		if (!String.isBlank(tooLongs)) {
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS507,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS507_Msg, new List<String> { tooLongs })
			);
		}
	}

	/** Determine if Gigya should be updated and which referential should be updated **/
	private void updateGigya() {
		system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - BEGIN');
		String accountIdToUpd = null;
		if (!Country_Info__c.getInstance(mAccount.Country__c).Myr_MultiBrandCountry__c) {
			system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - Country not multi-brand');
			return; //nothing to do
		}
		String otherBrand = Myr_MyRenaultTools.getOtherBrand(mBrand);
		if (mAccount.MyRenaultID__c == mAccount.MyDaciaID__c) {
			system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - MyRenaultID == MyDaciaId case');
			//Determine if the other referential Gigya should be updated also, not only the current Gigya referential
			String otherStatus = (String) mAccount.get(Myr_MyRenaultTools.getStatusField(otherBrand));
			String gigyaStatus = system.Label.Myr_Status_Created + system.Label.Myr_Status_Activated;
			if( !String.isBlank(otherStatus) && gigyaStatus.containsIgnoreCase(otherStatus) ) {
				mNotification = true;
				accountIdToUpd = mAccountId;
			} else {
				return; //nothing more to do
			}
		} else {
			system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - MyRenaultID <> MyDaciaId case');
			List<Account> listAccounts = new List<Account> ();
			if (Myr_MyRenaultTools.checkRenaultBrand(mBrand)) {
				listAccounts = [SELECT Id, MyRenaultID__c, MyDaciaID__c, Myr_Status__c, Myd_Status__c, MyD_Status_UpdateDate__c, MYR_Status_UpdateDate__c FROM Account WHERE MyDaciaID__c = :mNewEmail];
			} else {
				listAccounts = [SELECT Id, MyRenaultID__c, MyDaciaID__c, Myr_Status__c, Myd_Status__c, MyD_Status_UpdateDate__c, MYR_Status_UpdateDate__c FROM Account WHERE MyRenaultID__c = :mNewEmail];
			}
			if (listAccounts.size() == 0) {
				system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - No other account found');
				return; //nothing to be done, the process can continue
			} else if (listAccounts.size() > 1) {
				system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - More than 1 other account found');
				//oups, more than 1 account ????
				mResponse = new Myr_ChangeEmail_Response(
				                                         system.Label.Myr_ChangeEmail_WS04MS506,
				                                         String.format(system.Label.Myr_ChangeEmail_WS04MS506_Msg, new List<String> { otherBrand })
				);
				return;
			} else {
				system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - 1 other account found');
				//just one other account ! Keep the id for the future merge
				mOutAccountId = listAccounts[0].Id;
				if (Myr_MyRenaultTools.checkRenaultBrand(mBrand)) {
					mOutAccountStatus = listAccounts[0].Myd_Status__c;
					mOutAccountStatusUpdate = listAccounts[0].MyD_Status_UpdateDate__c;
				} else {
					mOutAccountStatus = listAccounts[0].Myr_Status__c;
					mOutAccountStatusUpdate = listAccounts[0].MYR_Status_UpdateDate__c;
				}
				accountIdToUpd = mOutAccountId;
			}
			system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - mOutAccountId=' + mOutAccountId);
		}

		system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - Update Gigya referential');
		//One remaining things to do: update the other Gigya referential with the new email ....
		callGigya(accountIdToUpd, otherBrand);
		if (mResponse != null) {
			return;
		}
		Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults = mMapGigya.get(otherBrand).gigyaResults;		try {
			List<String> listLogins = new List<String>(gigyaResults.results[0].loginIDs.emails);
			mOtherOldLoginId = listLogins[0];
		} catch (Exception e) {
			//silent exception, try to continue
		}
		Myr_GigyaFunctions_Cls gigya = mMapGigya.get(otherBrand).gigya;
		Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response response = gigya.forceNewLoginId(gigyaResults.results[0].uid, mAccountId, mNewEmail, mOtherOldLoginId);
		if (!response.isOk()) {
			String error = response.code + ' - ' + response.msg;
			system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - Update Gigya referential PROBLEM, code='+response.code+', message='+response.msg);
			mResponse = new Myr_ChangeEmail_Response(
				system.Label.Myr_ChangeEmail_WS04MS509,
				String.format(system.Label.Myr_ChangeEmail_WS04MS509_Msg, 
					new List<String> { otherBrand, gigyaResults.results[0].uid, mNewEmail, error }));
			return;
		}
		system.debug('### Myr_ChangeEmail_Cls - <updateGigya> - Update Gigya referential OK');
	}

	/** Deactivate the other account before triggering the changeEmail. Otherwise it will be impossible to change the email
	  Just call the existing Deactivate User webservice **/
	private void deactivateOtherUser() {
		system.debug('### Myr_ChangeEmail_Cls - <deactivateOtherUser> - BEGIN');
		if (String.isBlank(mOutAccountId)) {
			system.debug('### Myr_ChangeEmail_Cls - <deactivateOtherUser> - Nothing to do');
			//nothing to do
			return;
		}

		//HQREQ-06381: identify notifications to copy
		identifyNotificationsToRecopy();

		Myr_UserDeActivation_WS.Myr_UserDeActivation_WS_Response response =Myr_UserDeActivation_WS.deActivateUser(mOutAccountId, Myr_MyRenaultTools.getOtherBrand(mBrand));
		system.debug('### Myr_ChangeEmail_Cls - <deactivateOtherUser> - Deactivate response='+response);
		//check if the response is ok
		if (Pattern.matches('WS[0-9]{2}MS[0-4]{1}[0-9]{2}', response.info.code)) {
			//OK ou OK with warning
			return;
		} else {
			//Oups !!!! Try to recover the account id on Gigya
			String gigyaRecover = 'Nothing to be done';
			if( !String.isBlank(mOutAccountId) ) {
				//It is possible to make a callout after a DML (update) if this DML has failed (DMLException)
				String otherBrand = Myr_MyRenaultTools.getOtherBrand(mBrand);
				Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults = mMapGigya.get(otherBrand).gigyaResults;
				Myr_GigyaFunctions_Cls gigya = mMapGigya.get(otherBrand).gigya;
				Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response responseGig = gigya.forceNewLoginId(gigyaResults.results[0].uid, mOutAccountId, mOtherOldLoginId, mNewEmail);
				gigyaRecover = responseGig.code + ' - ' + responseGig.msg;
			}
			system.debug('### Myr_ChangeEmail_Cls - <deactivateOtherUser> - Gigya Recovering=' + gigyaRecover);
			mResponse = new Myr_ChangeEmail_Response(
				system.Label.Myr_ChangeEmail_WS04MS510,
			    String.format(system.Label.Myr_ChangeEmail_WS04MS510_Msg, new List<String> { mOutAccountId, response.info.code + ' - ' + response.info.message, gigyaRecover }));
			return;
		}
	}

	/** Call Gigya for the given brand with the current AccountId
	  @parameter Myr_MyRenaultTools.Brand the referential to call
	 **/
	private void callGigya(String accountId, String brand) {
		Myr_GigyaFunctions_Cls.GigyaLogType logType = Myr_GigyaFunctions_Cls.GigyaLogType.ASYNCHRONOUS;
		Myr_GigyaFunctions_Cls.GigyaMode env = Myr_GigyaFunctions_Cls.GigyaMode.NOVERIFICATIONEMAIL; //required to force a new login id
		Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls(logType, env);
		Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response response = gigya.viewData(accountId, brand);
		if (response.isOK()) {
			Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults = gigya.getGigyaData();
			if (gigyaResults.results.size() != 1) {
				String error = (gigyaResults.results.size() == 0) ?
				'No Gigya record found for the account'
				: 'Too many Gigya records found for this account (' + gigyaResults.results + ')';
				mResponse = new Myr_ChangeEmail_Response(system.Label.Myr_ChangeEmail_WS04MS508,
				                                         String.format(system.Label.Myr_ChangeEmail_WS04MS508_Msg, new List<String> { error }));
				return;
			}
			mMapGigya.put(brand, new GigyaStructure(gigya, gigyaResults));
		} else {
			String error = response.code + ' - ' + response.msg;
			mResponse = new Myr_ChangeEmail_Response(system.Label.Myr_ChangeEmail_WS04MS508,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS508_Msg, new List<String> { error }));
			return;
		}
	}

	/** Update User information **/
	private void updateUser() {
		system.debug('### Myr_ChangeEmail_Cls - <updateUser> - BEGIN');
		mUser.Email = mNewEmail;
		mUser.Username = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c + mNewEmail;
		try {
			if( TesT.isRunningTest() && mNewEmail.startsWithIgnoreCase( TEST_EXCEPTION.EXCP_UPDATE_USER.name() ) ) {
				throw new Myr_ChangeEmail_Exception('Test Exception');
			}
			update mUser;
		} catch(Exception e) {
			//oups !
			system.debug('### Myr_ChangeEmail_Cls - <updateUser> - exception: ' + e.getMessage());
			mResponse = new Myr_ChangeEmail_Response(
			                                         system.Label.Myr_ChangeEmail_WS04MS504,
			                                         String.format(system.Label.Myr_ChangeEmail_WS04MS504_Msg, new List<String> { e.getMessage() })
			);
		}
		system.debug('### Myr_ChangeEmail_Cls - <updateUser> - END');
	}

	/** Identify the notifications to copy from the future deleted account to the new one **/
	private void identifyNotificationsToRecopy() {
		system.debug('### Myr_ChangeEmail_Cls - <identifyNotificationsToRecopy> - BEGIN');
		if (String.isBlank(mOutAccountId)) {
			system.debug('### Myr_ChangeEmail_Cls - <identifyNotificationsToRecopy> - Nothing to do');
			//nothing to do
			return;
		}

		List<Customer_Message__c> listMessages = [SELECT Id, Status__c FROM Customer_Message__c 
													WHERE Account__c=:mOutAccountId 
													AND Status__c != :system.Label.Myr_Customer_Message_Status_Deleted
													AND Template__r.MessageType__r.ChangeEmail_Recopy__c = true
													ORDER BY Status__c DESC, CreatedDate
													LIMIT 50];
		mMapOutAccountNotifIdStatus = new Map<Id, String>();
		for( Customer_Message__c msg : 	listMessages ) {
			mMapOutAccountNotifIdStatus.put( msg.Id, msg.Status__c );
		}		
	}

	/**********************************************************************************************/
	/** INNER CLASSES **/
	public class Myr_ChangeEmail_Response {
		public Myr_ChangeEmail_Response_Info info;
		//@inner constructor
		public Myr_ChangeEmail_Response(String code, String message) {
			info = new Myr_ChangeEmail_Response_Info(code, message);
		}
	}

	public class Myr_ChangeEmail_Response_Info {
		public String code;
		public String message;
		//@inner constructor
		public Myr_ChangeEmail_Response_Info(String code, String message) {
			this.code = code;
			this.message = message;
		}
	}

	/** Structure to keep the gigya results **/
	public class GigyaStructure {
		public Myr_GigyaFunctions_Cls gigya;
		public Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults;
		//@inner constructor
		public GigyaStructure(Myr_GigyaFunctions_Cls gigya, Myr_GigyaParser_Cls.GigyaAccountParser gigyaResults) {
			this.gigya = gigya;
			this.gigyaResults = gigyaResults;
		}
	}

	/** Logger object used **/
	public class Logger {
		private String mFunction;
		private Long mBeginTime;
		private String mAccountId;
		private String mInputs;
		//@inner constructor
		public Logger(String function, String accountId, String inputs) {
			this.mFunction = function;
			this.mBeginTime = system.Now().getTime();
			this.mAccountId = accountId;
			this.mInputs = inputs;
		}
		public void log(String code, String message) {
			String runLogId = null;
			String parentLogId = null;
			String className = Myr_ChangeEmail_WS.class.getName();
			String error = '';
			String accountId = mAccountId;
			String relationId = null;
			String vehId = null;
			Long endTime = system.now().getTime();
			String record = mInputs;
			if( !system.isFuture() ) {
				futureLog(runLogId, parentLogId, className, mFunction, code, message, error, accountId, relationId, vehId, mBeginTime, endTime, record);
			} else {
				INDUS_Logger_CLS.addGroupedLogFull(runLogId, parentLogId, INDUS_Logger_CLS.ProjectName.MYR, className, mFunction, code, message, error, accountId, relationId, vehId, mBeginTime, endTime, record);
			}
		}
		// Log the current situation
		public void log(Myr_ChangeEmail_Response response) {
			log((response != null && response.info != null) ? response.info.code : '',
			(response != null && response.info != null) ? response.info.message : '');
		}
	}

	@future
	private static void futureLog(String runLogId, String parentLogId, String className, String function, String code, String message, String error, String accountId, String relationId, String vehId, Long beginTime, Long endTime, String record) {
		INDUS_Logger_CLS.addGroupedLogFull(runLogId, parentLogId, INDUS_Logger_CLS.ProjectName.MYR, className, function, code, message, error, accountId, relationId, vehId, beginTime, endTime, record);
	}

	/**********************************************************************************************/
	/** Asynchronous functions **/
	@future
	private static void asynchronousUpdate(String brand, String accIdIn, String accIdOut, String newEmail, String accOutStatus, Date accOutDate, Boolean notification, Map<Id, String> mapAccOutMsgIdStatus) {
		system.debug('### Myr_ChangeEmail_Cls - <asynchronousUpdate> - BEGIN brand='+brand+', accIdIn='+accIdIn+', accIdOut='+accIdOut+', newEmail='+newEmail+', accOutStatus='+accOutStatus+', notification='+notification);
		AccountUpdateClass updaterAccount = new AccountUpdateClass(brand, accIdIn, accIdOut, newEmail, accOutStatus, accOutDate, notification, mapAccOutMsgIdStatus);
		updaterAccount.process();
		system.debug('### Myr_ChangeEmail_Cls - <asynchronousUpdate> - END');
	}

	/** Inner class used to manage the account updates including merge. Should be used ASYNCHRONOUSLY **/
	public class AccountUpdateClass {
		//members Account
		private Account mMainAccount;
		private String mAccIdOut;
		private String mBrand;
		private String mNewEmail;
		private String mAccOutStatus; //Keep the original status ... otherwise it could be deleted as we are deactivating the other account ...
		private Date mAccOutStatusDate;
		private Boolean mNotification = false;
		private List<String> mLogSilentExceptions; //not treated yet
		//private members
		private String query;
		private Logger logger;
		//HQREQ-06381: identify notifications to copy
		private Map<Id, String> mMapOutAccNotifId;

		//@inner constructor
		public AccountUpdateClass(String brand, String accIdIn, String accIdOut, String newEmail, String accOutStatus, Date accOutStatusDate, Boolean notification, Map<Id, String> mapAccOutNotifIdStatus) {
			mBrand = brand;
			mNewEmail = newEmail;
			mAccIdOut = accIdOut;
			mAccOutStatus = accOutStatus;
			mAccOutStatusDate = accOutStatusDate;
			mNotification = notification;
			query = 'SELECT Id, AccountBrand__c, Country__c, PersEmailAddress__c, ' + LIST_FIELDS_TO_MERGE + ' FROM Account';
			mMainAccount = (Account) Database.query(query + ' WHERE Id = :accIdIn');
			String inputs = '<Inputs: ' + brand + ', ' + accIdIn + ', ' + accIdOut + ', ' + newEmail + ', ' + notification + '>';
			logger = new Logger('updateAccount', accIdIn, inputs);
			mLogSilentExceptions = new List<String>();
			mMapOutAccNotifId = mapAccOutNotifIdStatus;
		}

		//Process the updates
		public void process() {
			Boolean hasProblem = false;

			try {
				updateAccount(); //update the information on account due to changeEmail
			} catch(Exception e) {
				//oups problem don't go further !
				logger.log('CRITICAL', 'Unable to update the account: ' + e.getMessage());
				return;
			}

			try {
				mergeAccounts(); //merge the accounts in case we found 2 different accounts with the same id on different brand
			} catch(Exception e) {
				//oups problem. Account updated but not merged. Go further
				logger.log('WARN', 'Unable to merge the account: ' + e.getMessage());
				hasProblem = true;
			}

			hasProblem = insertNotification();

			if (!hasProblem) {
				logger.log('INFO', 'Account updated with success');
			}
		}

		//Update the account informations
		private void updateAccount() {
			system.debug('### Myr_ChangeEmail_Cls - <updateAccount> - BEGIN ');
			String otherBrand = Myr_MyRenaultTools.getOtherBrand(mBrand);
			//update the brand on which the changeEmail was required
			String idField = Myr_MyRenaultTools.getIdField(mBrand);
			system.debug('### Myr_ChangeEmail_Cls - <updateAccount> - idField='+idField);
			mMainAccount.put(idField, mNewEmail);
			//update the other brand in case the value is empty
			String otherIdField = Myr_MyRenaultTools.getIdField(otherBrand);
			system.debug('### Myr_ChangeEmail_Cls - <updateAccount> - otherIdField='+otherIdField);
			if (String.isBlank((String) mMainAccount.get(otherIdField))) {
				mMainAccount.put(otherIdField, mNewEmail);
			}
			//update persemailaddress in case it was empty
			if (String.isBlank(mMainAccount.PersEmailAddress__c)) {
				mMainAccount.PersEmailAddress__c = mNewEmail;
			}
			//Don't forget to update ....
			update mMainAccount;
			system.debug('### Myr_ChangeEmail_Cls - <updateAccount> - END ');
		}

		//Merge the accounts if another one has been identified
		private void mergeAccounts() {
			if (String.isBLank(mAccIdOut)) {
				//nothing to be done
				return;
			}
			//--- GET THE ACCOUNT TO MERGE
			Account toMergeAccount = (Account) Database.query(query + ' WHERE Id = :mAccIdOut');

			//BEFORE starting anything, forbid the generation of the DEALER_AUTO_ADDED notification
			Myr_CreateMessages_TriggerHandler_Cls.forbid_DEALER_AUTO_ADDED_Generation();

			//--- MERGE ACCOUNT informations: if brand is Renault, then we merge dacia information into renault account
			//if brand is dacia, then we merge renault information into dacia account
			//In the case of merge the information are crushed without considering the existing data into the account
			String prefixToMerge = (Myr_MyRenaultTools.checkRenaultBrand(mBrand) ? 'myd' : 'myr');
			List<String> listFieldsToMerge = LIST_FIELDS_TO_MERGE.split(',');
			for (String field : listFieldsToMerge) {
				if (field.startsWith(prefixToMerge)) {
					mMainAccount.put(field, toMergeAccount.get(field));
				}
			}
			if (String.isBlank((String) mMainAccount.get('my_purchase_intention_brand__c')) && !String.isBlank((String) toMergeAccount.get('my_purchase_intention_brand__c'))) {
				mMainAccount.put('my_purchase_intention_brand__c', toMergeAccount.get('my_purchase_intention_brand__c'));
				mMainAccount.put('my_purchase_intention_date__c', toMergeAccount.get('my_purchase_intention_date__c'));
				mMainAccount.put('my_purchase_intention_model__c', toMergeAccount.get('my_purchase_intention_model__c'));
				mMainAccount.put('my_purchase_intention_update_date__c', toMergeAccount.get('my_purchase_intention_update_date__c'));
			}
			mMainAccount.AccountBrand__c = 'Renault & Dacia';
			//Set the original status of the other account
			mMainAccount.put( Myr_MyRenaultTools.getStatusField( Myr_MyRenaultTools.getOtherBrand(mBrand) ), mAccOutStatus );
			update mMainAccount;
			//force to do a second update to force thr proper update dates ...
			mMainAccount.put( prefixToMerge + '_status_updatedate__c', mAccOutStatusDate );
			mMainAccount.put( prefixToMerge + '_prefered_dealer_update_date__c', toMergeAccount.get( prefixToMerge + '_prefered_dealer_update_date__c' ) );
			update mMainAccount;

			//--- MERGE CONFIGURATIONS: just modify the parent of the relation ...
			List<Vehicle_Configuration__c> listConfigurations = [SELECT Id, Account__c FROM Vehicle_Configuration__c WHERE Account__c = :mAccIdOut];
			for (Vehicle_Configuration__c config : listConfigurations) {
				config.Account__c = mMainAccount.Id;
			}
			update listConfigurations;

			//--- MERGE VEHICLE RELATIONS
			mergeVehicleRelations();

			//--- MERGE NOTIFICATIONS
			mergeNotifications();

			//--- DEACTIVATE THE OTHER ACCOUNT
			//Done in synchronous ChangeEmail, otherwise it's impossible to applyt he changeEmail with an email already used
		}

		//Merge Vehicle Relations
		private void mergeVehicleRelations() {
			system.debug('### Myr_ChangeEmail_Cls - <mergeVehicleRelations> - BEGIN ');
			//Get relations of the main account
			List<VRE_VehRel__c> listRelationsMainAccount = [SELECT Id, Account__c, VIN__c, My_Garage_Status__c FROM VRE_VehRel__c WHERE Account__c = :mMainAccount.Id];
			Map<String, VRE_VehRel__c> mapMainRelations = new Map<String, VRE_VehRel__c> ();
			for (VRE_VehRel__c vehRel : listRelationsMainAccount) {
				mapMainRelations.put(vehRel.VIN__c, vehRel);
			}
			//Get relations of the accounts to merge
			List<VRE_VehRel__c> listRelationsMergeAccount = [SELECT Id, Account__c, VIN__c, My_Garage_Status__c FROM VRE_VehRel__c WHERE Account__c = :mAccIdOut];
			List<VRE_VehRel__c> listRelationsToUp = new List<VRE_VehRel__c> ();
			List<VRE_VehRel__c> listRelationsToDel = new List<VRE_VehRel__c> ();
			for (VRE_VehRel__c mergeRel : listRelationsMergeAccount) {
				//transfert the relations to the main account and if the relations already exists, apply the following priority to determine
				//which one has to be kept
				if (mapMainRelations.containsKey(mergeRel.VIN__c)) {
					VRE_VehRel__c mainRel = mapMainRelations.get(mergeRel.VIN__c);
					if (getGarageStatusPriority(mergeRel.My_Garage_Status__c) < getGarageStatusPriority(mainRel.My_Garage_Status__c)) {
						//transfer the relation from merge account and delete the relation from the main account
						system.debug('### Myr_ChangeEmail_Cls - <mergeVehicleRelations> - Transfer ' + mergeRel.VIN__c + ' on main account');
						mergeRel.Account__c = mMainAccount.Id;
						listRelationsToUp.add(mergeRel);
						listRelationsToDel.add(mainRel);
					} else {
						system.debug('### Myr_ChangeEmail_Cls - <mergeVehicleRelations> - Keep the main acco�unt relation for ' + mergeRel.VIN__c);
						//just delete the relation on the merged account.
						listRelationsToDel.add(mergeRel);
					}
				} else {
					//transfert the relations without modifying nothing
					mergeRel.Account__c = mMainAccount.Id;
					listRelationsToUp.add(mergeRel);
				}
			}
			//delete relations
			delete listRelationsToDel; //delete first, otherwise it will be impossible to transfer the relations :-)
			//update relations
			update listRelationsToUp;
			
			system.debug('### Myr_ChangeEmail_Cls - <mergeVehicleRelations> - END ');
		}

		//Merge the notifications from the future deactivated account and the current one. The merge
		//is based on a field created on Customer Message Settings
		//HQREQ-06381 / HotFix Week50-2017: the notifications to recopy are now passed as parameters to the AccountUpdate sub process
		//                                  instead of a query in this method. This allow to retrieve the right status before deleting the other user.
		private void mergeNotifications() {
			system.debug('### Myr_ChangeEmail_Cls - <mergeNotifications> - BEGIN ');
			try {
				if( null == mMapOutAccNotifId ) return; //nothing to do
				if( 0 == mMapOutAccNotifId.keySet().size() ) return; //nothing to do
				//Select the notifications to recopy from the old account to the new one based on the setting Customer_Message_Settings__c.ChangeEmail_Recopy__c
				//CAUTION ! The clone function should be based on a select all query ...
				Myr_MyRenaultTools.SelectAll selectAll = Myr_MyRenaultTools.buildSelectAllQuery('Customer_Message__c', '');
				List<Id> listOutNotifIds = new List<Id>();
				listOutNotifIds.addAll(mMapOutAccNotifId.keySet());
				List<Customer_Message__c> listMsgsToMerge = Database.query( selectAll.Query + ' WHERE Id IN :listOutNotifIds' );
				system.debug('### Myr_ChangeEmail_Cls - <mergeNotifications> - Notifications found: ' + listMsgsToMerge.size());
				List<Customer_Message__c> listClonedMsgs = new List<Customer_Message__c>();
				for( Customer_Message__c msg : listMsgsToMerge ) {
					//clone the record to the kept account
					Customer_Message__c cloneMsg = msg.clone();
					cloneMsg.Account__c = mMainAccount.Id;
					cloneMsg.Status__c = mMapOutAccNotifId.get(msg.Id); //set the status before deleting the account was deactivated
					listClonedMsgs.add( cloneMsg );
				}
				insert listClonedMsgs; 
			} catch (Exception e) {
				String excep = '### Myr_ChangeEmail_Cls - <mergeNotifications> - Exception: ' + e.getMessage() + ' - ' + e.getStackTraceString();
				mLogSilentExceptions.add( excep );
				//simply ignore the recopy and let the process continue to the end
				system.debug(excep);
			}
			system.debug('### Myr_ChangeEmail_Cls - <mergeNotifications> - END ');
		}

		//Insert notification on change email if required
		//@return true if the insertion was OK, false otherwise
		private Boolean insertNotification() {
			try { 
				if (!mNotification) {
					return true; //nothing to be done
				}
				Myr_CustMessage_Cls notification = new Myr_CustMessage_Cls();
				notification.accountSfdcId = mMainAccount.Id;
				notification.channels = new List<Myr_CustMessage_Cls.Channel> { Myr_CustMessage_Cls.Channel.popin, Myr_CustMessage_Cls.Channel.message };
				//generates the notification on the other brand.
				notification.brand = (Myr_MyRenaultTools.checkRenaultBrand(mBrand) ? Myr_CustMessage_Cls.Brand.Renault : Myr_CustMessage_Cls.Brand.Dacia);
				notification.typeId = 'EMAIL_UPDATED';
				notification.criticity = true; //priority 1 message
				Myr_CreateCustMessage_Cls.CreateMsgResponse response = Myr_CreateCustMessage_Cls.createMessages(new List<Myr_CustMessage_Cls> { notification });
				if (Myr_CreateCustMessage_Cls.Status.OK == response.status) {
					return true;
				}
				logger.log('WARN', 'Unable to insert the notification: ' + response.listStatus[0].code + ' ' + response.listStatus[0].message);
				return false;
			} catch (Exception e) {
				logger.log('WARN', 'Unable to insert the notification: ' + e.getMessage() );
				return false;
			}
		}
	}
}