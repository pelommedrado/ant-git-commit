@isTest
public class ProspectionManagerControllerTest {
    
    static testMethod void test1(){
        
        Test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        Insert dealer;
        
        User seller = moc.criaUser();
        seller.ProfileId = Utils.getSystemAdminProfileId(); //Utils.getProfileId('BR - Renault + Cliente');
        Insert seller;
        
        Lead l = moc.CriaLead();
        l.DealerOfInterest__c = dealer.Id;
        Insert l;
        
        Campaign c = moc.criaCampanha();
        c.IsActive = true;
        c.Status = 'In Progress';
        c.WebToOpportunity__c = false;
        Insert c;
        
        CampaignMember cm = moc.criaMembroCampanha();
        cm.LeadId = l.Id;
        cm.CampaignId = c.Id;
        Insert cm;       
        
        ProspectionManagerController ctrl = new ProspectionManagerController();
        
        ctrl.leadList.add(cm);
        
        Map<Id, List<PermissionSet>> psets = new Map<Id, List<PermissionSet>>();
        List<CampaignMember> leads = new List<CampaignMember>();
        List<Lead> toUpdate = new List<Lead>();
        
        ctrl.attrLeads();
        
        ctrl.decideAttrAction();
        
        ctrl.statusSelected = 'status';
        ctrl.dealerSelected = dealer.Id;
        ctrl.sellerSelected = seller.Id;
        ctrl.decideAttrAction();
        
        ctrl.qtde = 0;
        ctrl.decideAttrAction();
        
        ctrl.qtde = 1;
        ctrl.decideAttrAction();
        
        ctrl.deleteList();
        ctrl.filterLeads();
        ctrl.getAttrLeads(psets, leads);
        ctrl.getCampanhas();
        ctrl.getDealers();
        ctrl.getLeadOwners();
        ctrl.getLeads();
        ctrl.getNotAttrLeads(psets, leads);
        ctrl.getSellers();
        ctrl.getStatus();
        ctrl.newLead();
        //ctrl.removeAttrLeads();
        ctrl.updateLeads(toUpdate);
        
        ctrl.statusSelected = 'attr';
        ctrl.deleteList();
        
        Test.stopTest();
    }
    
    static testMethod void test2(){
        
        Test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        Insert dealer;
        
        User seller = moc.criaUser();
        seller.ProfileId = Utils.getSystemAdminProfileId(); //Utils.getProfileId('BR - Renault + Cliente');
        Insert seller;
        
        Lead l = moc.CriaLead();
        l.DealerOfInterest__c = dealer.Id;
        Insert l;
        
        Campaign c = moc.criaCampanha();
        c.IsActive = true;
        c.Status = 'In Progress';
        c.WebToOpportunity__c = false;
        Insert c;
        
        CampaignMember cm = moc.criaMembroCampanha();
        cm.LeadId = l.Id;
        cm.CampaignId = c.Id;
        Insert cm;       
        
        ProspectionManagerController ctrl = new ProspectionManagerController();
        
        ctrl.leadList.add(cm);
        
        Map<Id, List<PermissionSet>> psets = new Map<Id, List<PermissionSet>>();
        List<CampaignMember> leads = new List<CampaignMember>();
        List<Lead> toUpdate = new List<Lead>();
        
        ctrl.attrLeads();
        
        ctrl.decideAttrAction();
        
        ctrl.qtde = 0;
        ctrl.decideAttrAction();
        
        ctrl.qtde = 1;
        ctrl.decideAttrAction();
        
        ctrl.statusSelected = 'notAttr';
        ctrl.dealerSelected = dealer.Id;
        ctrl.sellerSelected = seller.Id;
        ctrl.decideAttrAction();
        
        ctrl.deleteList();
        ctrl.filterLeads();
        ctrl.getAttrLeads(psets, leads);
        ctrl.getCampanhas();
        ctrl.getDealers();
        ctrl.getLeadOwners();
        ctrl.getLeads();
        ctrl.getNotAttrLeads(psets, leads);
        ctrl.getSellers();
        ctrl.getStatus();
        ctrl.newLead();
        //ctrl.removeAttrLeads();
        ctrl.updateLeads(toUpdate);
        
        ctrl.statusSelected = 'attr';
        ctrl.deleteList();
        
        Test.stopTest();
    }

}