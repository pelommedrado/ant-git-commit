/** Apex class used to parse the results from Gigya

	@author S. Ducamp
	@date 29.09.2015
	
	@version 1.0 initial version
**/
public without sharing class Myr_GigyaParser_Cls {

    /** ------------------------------------------------------------------------------------------------
        Token Parser
        @see http://developers.gigya.com/037_API_reference/010_Socialize/z_OAuth2_End_Points/socialize.getToken
    **/
    public class TokenParser {
        public String access_token;
        public String expires_in;
    }
    
    /** ------------------------------------------------------------------------------------------------
        Gigya Account Parser: search account into Gigya using email and country and retrieve a list of 
        possible accounts
    **/
    
    /** GigyaAccountParser: extract only the required information to identify the acount properly
    	@see http://developers.gigya.com/display/GD/accounts.search%20REST 
    **/
    public class GigyaAccountParser {
        public String statusCode;
        public String errorCode;
        public String errorMessage;
        public String errorDetails;
        public String statusReason;
        public String callId;
        public List<GigyaAccounts> results {get; private set;}
    }
    
    //Used by the Json Parser
    //Search Account Parsing: definition of the root results 
    public class GigyaAccounts {
        public String UID {get; private set;}
        public Boolean isActive {get; private set;}
		public Boolean isRegistered {get; private set;}
        public Boolean isVerified {get; private set;}
        public Profile profile {get; private set;}
        public LoginIDs loginIDs {get; private set;}
        public Emails emails {get; private set;}
        public Datetime registered {get; private set;}
        public Datetime created {get; private set;}
        public Datetime lastLogin {get; private set;}
		public Data data {get; private set;}
    }
    
    //Used by the Json Parser
    //Search Account Parsing: definition of the Profil into results
    public class Profile {
        public String email;
        public String firstName;
        public String lastName;
        public String age;
        public String gender;
        public String country;
    }
    
    //Used by the Json Parser
    //Search Account Parsing: definition of the LoginIDs into results
    public class LoginIDs {
    	public String username;
    	public Set<String> emails {get; private set;}
    	public Set<String> unverifiedEmails {get; private set;}
    }
    
    //Used by the Json Parser
    //Search Account Parsing: definition of the Emails into results
    public class Emails {
    	public Set<String> verified {get; private set;}
    	public Set<String> unverified {get; private set;}
    }

	//Used by the Json Parser
    //Parse the data class
	public class Data {
		public String accountSfdcActivated {get; private set;}
		public String newEmail {get; private set;}
	}

}