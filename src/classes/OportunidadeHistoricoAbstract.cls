public abstract class OportunidadeHistoricoAbstract {
    
    /** Oportunidade selecionada **/
    protected Opportunity opp;
    
    /** Conta do usuario **/
    protected Account acc;
    
    protected void init(String oppId) {
        this.opp = obterOportunidade(oppId);
        this.acc = Sfa2Utils.obterContaUserComunidade();
    }
    
    protected Boolean verificarOportunidade(OportunidadeHistoricoListener listener) {
        if(!acc.ReturnActiveToSFA__c) {
            return true;
        }
        
        Integer intDays =  countDaysLastModified();
        System.debug('### Dias passados: ' + intDays);
        
        // expirou?
        if(intDays > acc.ReturnPeriod__c) {
            listener.oportunidadeExpirou();
            return false;
            
        } else if(isFasePerdida()) {
            if(isPerdidaValido()) {
                listener.oportunidadeReabrir();
                
                return true;
                
            } else {
                listener.oportunidadeNaoReabre();
            }
            
            return false;
            
        } else if(isFaseFaturada()) {
            listener.oportunidadeNaoReabreFaturada();
            return false;
            
        } else if(isFaseNormal()) {
            listener.oportunidadeAceita();
            
            return true;
            
        } 
        
        return true;
    }
    
    protected Integer countDaysLastModified() {
        Datetime dtLast = opp.LastModifiedDate;
        Date dtHoje = Datetime.now().Date();
        
        return dtLast.Date().daysBetween(dtHoje);
    }
    
    private Boolean isFaseNormal() {
        return (opp.StageName == 'Identified' ||
                opp.StageName == 'In Attendance' ||
                opp.StageName == 'Test Drive' || 
                opp.StageName == 'Quote'); 
    }
    
    private Boolean isFasePerdida() {
        return opp.StageName == 'Lost';
    }
    
    private Boolean isFaseFaturada() {
        return opp.StageName == 'Billed';
    }
    
    private Boolean isPerdidaValido() {
        return 
            opp.ReasonLossCancellation__c == 'Price' ||
            opp.ReasonLossCancellation__c == 'Product' ||
            opp.ReasonLossCancellation__c == 'Financing Not Approved' ||
            opp.ReasonLossCancellation__c == 'Searching / Quote' ||
            opp.ReasonLossCancellation__c == 'Opportunity Expired' ||
            opp.ReasonLossCancellation__c == 'Abandonment' ||
            opp.ReasonLossCancellation__c == 'Attendance';
    }
    
     /**
     * Obter a oportunidade.
     */
    private Opportunity obterOportunidade(String oppId) {
        if(String.isEmpty(oppId)) {
            return null;
        }
        
        return [
            SELECT Id, StageName, LastModifiedDate, ReasonLossCancellation__c, AccountId
            FROM Opportunity 
            WHERE Id =: oppId 
        ];
    }
    
    public interface OportunidadeHistoricoListener {
        void oportunidadeExpirou();
        void oportunidadeAceita();
        void oportunidadeReabrir();
        void oportunidadeNaoReabre();
        void oportunidadeNaoReabreFaturada();
    }
}