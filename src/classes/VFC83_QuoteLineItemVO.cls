/**
* Classe que representa os dados do item da página de detalhe do orçamento.
* @author Felipe Jesus Silva / Christian Ranha
*/
public class VFC83_QuoteLineItemVO 
{
	public String id {get;set;}
	
	public Double unitPrice {get;set;}
	public Double totalPrice {get;set;}

	public String unitPriceMask {get;set;}
	public String totalPriceMask {get;set;}
	
	public String vehicleId {get;set;}
	public String vehicleName {get;set;}
	public Boolean isReserved {get;set;}
	public String item {get;set;}
	public String description {get;set;}
	
	public VFC83_QuoteLineItemVO()
	{
		this.id = '';
		this.unitPrice = 0;
		this.totalPrice = 0;
		this.vehicleId = '';
		this.vehicleName = '';
		this.isReserved = false;
		this.item = '';		
		this.description = '';
	}
}