@isTest
private class PV_OptionalFillMultiselectAsTextTest {
  
  @isTest static void MyUnitMethod() {
    Model__c model = new Model__c(
      //ENS__c = 'abc',
      Market__c = 'abc',
      Status__c = 'Active',
      Model_PK__c = 'ABC-1',
      Phase__c = '1',
      Model_Spec_Code__c = 'ABC'
    );
    Database.insert( model );

    PV_ModelMilesime__c milesime = new PV_ModelMilesime__c(
      Model__c = model.Id,
      ENS__c = 'ENS',
      Milesime_PK__c = 'ABC-1ENS'
    );

    Database.insert( milesime );

    PVVersion__c version = new PVVersion__c(
      Model__c = model.Id,
      Milesime__c = milesime.Id,
      Version_Id_Spec_Code__c = 'abc',
      Price__c = 100.0,
      PVC_Maximo__c = 90.0,
      PVR_Minimo__c = 80.0,
      Version_PK__c = 'ABC-1ENSabc'
    );
    Database.insert( version );

    Optional__c optional = new Optional__c(
      Featured_In_Product__c = 'Valor 1; Valor 3',
      Version__c = version.Id,
      Optional_Code__c = 'X',
      Optional_PK__c = 'ABC-1ENSabcX'
    );
    Database.insert( optional );

    System.assert( String.isNotBlank(
      [select Featured_In_Product_as_Text__c from Optional__c where Id = :optional.Id limit 1].Featured_In_Product_as_Text__c
    ) );
  }
}