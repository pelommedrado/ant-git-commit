global class WSC04_PreOrderDMSVO
{
    webservice String accessoriesList               {get;set;}  // required: Lista acessórios (descrição)   [Text:1000]
    webservice Date   birthDate                     {get;set;}  // rquired: Data de nascimento ###customerBirthDate
    webservice String cellphone                     {get;set;}  // optional: Telefone celular   [Text:11] DDD+Numero ###customerMobilehone
    webservice String city                          {get;set;}  // required: Cidade [Text:35] ###customerCity
    webservice String commercialLandline            {get;set;}  // optional: Telefone comercial [Text:11] DDD+Numero ###customerProfessionalPhone
    webservice String country                       {get;set;}  // required: País   [Text:35] ###customerCountry
    webservice String CPF_CNPJ                      {get;set;}  // required: CPF/CNPJ [14]
    webservice String CPFSalesperson                {get;set;}  // required: CPF Vendedor [Text:11] ###salespersonCPF
    webservice Date creationDate                    {get;set;}  // required: Data de criação ###createdDate
    webservice String customerEmail                 {get;set;}  // required: Email Cliente [Text:50]
    //webservice String customerLastName                {get;set;}  // required: Last Name of cliente  [Text:80]
    webservice String customerName                  {get;set;}  // required: Nome cliente   [Text:100]
    webservice String emailSalesperson              {get;set;}  // required: Email vendedor [Text:50] ###salespersonEmail
    webservice Double entryValue                    {get;set;}  // optional: Entrada [10.2]
    webservice String errorMessage                  {get;set;}  // optional
    webservice Datetime expirationDate              {get;set;}  // required: Data de validade
    webservice String fax                           {get;set;}  // optional: Fax [Text:11] DDD+Numero : removed as requested by Hugo
    webservice String fundingAgent                  {get;set;}  // optional: Agente Financiador [Text:100] ###financingAgent
    webservice String fundingType                   {get;set;}  // optional: Tipo de financiamento [Text:20] ###financingType
    webservice Double fundingValue                  {get;set;}  // optional: Valor financiamento [10.2] ###amountFinanced
    webservice String identificationHobby           {get;set;}  // optional: Identificação hobby [Text:50]
    webservice Integer installmentsNumber           {get;set;}  // optional: Número de parcelas [3] ###numberOfInstallments
    webservice Double installmentsValue             {get;set;}  // optional: Valor das parcelas [10.2] ###valueOfInstallmentsValue
    webservice String invoiceNumber                 {get;set;}  // optional: Número Nota Fiscal [Text:100]
    webservice String job                           {get;set;}  // optional: Ocupação   [Text:50] : removed as requested by Hugo
    webservice String literacy                      {get;set;}  // optional: Grau de instrução [Text:30]: removed as requested by Hugo
    webservice String maritalStatus                 {get;set;}  // optional: Estado civil   EN:[Single / Married / Divorced / Widowed / Other] ###customerMaritalStatus
    webservice String nameSalesperson               {get;set;}  // required: Nome vendedor [Text:100] ###salespersonName


    // Re-included as requested by Hugo 05/04/2013
    webservice String neighbourhood                 {get;set;}  // required: Bairro [Text:35] : removed as requested by Hugo


    webservice Integer numberOfChildren             {get;set;}  // optional: Numero de filhos : removed as requested by Hugo
    webservice String orderNumber                   {get;set;}  // required: Número da Cotação do Pedido [Text:10] ###quoteNumber
    webservice String orderNumberDMS                {get;set;}  // optional: Número Pedido DMS [Text:100] ###DMSNumber
    webservice String paymentType                   {get;set;}  // required: Forma de Pagamento [Text:100] BR: [Financiamento / Leasing / Consorcio / Usado / Entrada a Vista / Parcela a Prazo] ###paymentMethods
    webservice String phoneSalesperson              {get;set;}  // required: Telefone vendedor [Text:11] DDD+Numero ###salespersonPhone
    webservice String residencialLandline           {get;set;}  // required: Telefone residencial   [Text:11] DDD+Numero ###customerHomePhone
    webservice String RG_StateRegistration          {get;set;}  // required: RG/Inscrição Estadual [Text:15] ###customerRGStateRegistration
    webservice String salaryRange                   {get;set;}  // optional: Faixa salarial [Text:50] : removed as requested by Hugo ###salaryRange
    webservice String sex                           {get;set;}  // optional: Sexo    EN: [M=Masculine, F=Feminine, O=Not Informed] ###customerSex
    webservice String state                         {get;set;}  // required: Estado [Text:35] ###customerState
    webservice String status                        {get;set;}  // required: Status  EN: [Open / Requested Pre Order / Pre Order Sent / Invoiced / Cancelled]
    webservice String streetAndComplement           {get;set;}  // required: Logradouro(+Complemento)   [Text:50] ###customerStreetAndComplement
    webservice String usedCar                       {get;set;}  // optional: Veículo usado [Text:100]
    webservice String usedCarLicencePlate           {get;set;}  // optional: Placa Usado Negociado [Text:7] ###usedVehicleLicencePlate
    webservice Double usedCarValue                  {get;set;}  // optional: Valor veiculo usado [10.2] ###usedVehiclePrice
    webservice Integer usedCarYear                  {get;set;}  // optional: Ano veiculo usado [4] ###usedVehicleYear
    webservice Double value                         {get;set;}  // required: Valor [10.2] Carro + Acessórios + Outros(Despachante, taxa de serviços, etc) ###totalPrice
    webservice String VIN                           {get;set;}  // required: Chassi [Text:17]
    webservice String zipCode                       {get;set;}  // required: CEP [Text:8] ###customerPostalCode
    webservice Datetime lastUpdateDate              {get;set;}  // required: Data Última Atualização

    //fields for new address
    //webservice String street                        {get;set;}
    //webservice String addressNumber                 {get;set;}
    //webservice String complement                    {get;set;}
    //webservice String neighborhood                  {get;set;}

    //webservice String usedVehicleBrand                {get;set;}
    //webservice String usedVehicleModel                {get;set;}
    //webservice String usedVehicleConfiguration        {get;set;}
    //webservice Boolean error                          {get;set;}  // optional: False: no error ; True: error
    //webservice String neighbourhood                   {get;set;}  // required: Bairro [Text:35] : removed as requested by Hugo

    webservice String paymentDetails                   {get;set;}  // optional: Observações 
    
    global WSC04_PreOrderDMSVO(){} 
}