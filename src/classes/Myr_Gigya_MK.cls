/**
  * Mock class that simulates the response of Gigya
  * To use the mock, we have to put in place several responses for the different cases. Indeed 3 calls could be made to Gigya to be able to
  * call the proper function : get the token, search the account and reset the password
  * This simulation is achieved by setting several responses in an internal structure
  * The propoer response within the structure is set on the fly when calling respond depending on the query made
  *
  * @author S. Ducamp
  * @date 01.10.2015
  *
  * @version 1.0 inital revision : getToken + resendActivationMail + resetPassword 
 */
@isTest
global class Myr_Gigya_MK implements HttpCalloutMock {
	
	private CS_Gigya_Settings__c gigSettings;
	private TupleResponse responses;
	private String gigDataCenterId;
	
	/** Defined the shape of an httpresponse **/
	global class IntResponse {
		public Integer statusCode;
		public String status;
		public String body; 
	}
	
	/** the 3 steps possible: token + acount search + the gigya function **/
	global class TupleResponse {
		public IntResponse token;
		private List<IntResponse> accSearch;
		public List<IntResponse> gigFuncResponse;
		
		//manage several possible responses in the mock when requesting an account search (usefull for Myr_GigyaLayoutDisplay_WS_Test) 
		private Integer nbCallsAccSch;
		//manage several possible responses in the mock when requesting a function
		private Integer nbCallsFunction;
		
		//Add a new possible response for the acount search
		public void addAccountSearch(Integer i_code, String i_status, String i_body) {
			IntResponse newSearch = new IntResponse();
			newSearch.statusCode = i_code;
			newSearch.status = i_status;
			newSearch.body = i_body;
			accSearch.add( newSearch );
		}

		//Add a new possible response for the Gigya function
		public void addFunctionResp(Integer i_code, String i_status, String i_body) {
			IntResponse newFct = new IntResponse();
			newFct.statusCode = i_code;
			newFct.status = i_status;
			newFct.body = i_body;
			gigFuncResponse.add( newFct );
		}

		//return the available account search, if the nbCalls is greater than the size of possile response, then return the last item of the list
		public IntResponse getAccountSearch() {
			IntResponse response = accSearch[ ((nbCallsAccSch>=accSearch.size())?accSearch.size()-1:nbCallsAccSch) ];
			nbCallsAccSch++;
			return response;
		}
		//return the available gigya function response, if the nbCalls is greater than the size of possile response, then return the last item of the list
		public IntResponse getFunctionResult() {
			IntResponse response = gigFuncResponse[ ((nbCallsFunction>=gigFuncResponse.size())?gigFuncResponse.size()-1:nbCallsFunction) ];
			nbCallsFunction++;
			return response;
		}
		//@constructor
		public TupleResponse() {
			token = new IntResponse();
			accSearch = new List<IntResponse>();
			gigFuncResponse = new List<IntResponse>();
			nbCallsAccSch = 0;
			nbCallsFunction = 0;
		}
		
	}
	
	/**@construtor**/
	global Myr_Gigya_MK(String dataCenterId) {
		gigDataCenterId = dataCenterId;
		gigSettings = CS_Gigya_Settings__c.getInstance();
		//prepare the structure of the response
		responses = new TupleResponse();
	}

	/** the main function that retuns the httpresponse 
		@return HttpResponse
	**/
    global HttpResponse respond(HttpRequest request) {
    	HttpResponse httpResponse = new HttpResponse();
    	//analyze the query to be able to respond the proper answer
    	String endPoint = request.getEndPoint();
    	String accSchUrl = gigSettings.Url_Account_Search__c.replace('<Data_Center_ID>',gigDataCenterId);
    	system.debug('### - Myr_Gigya_MK - <respond> - endPoint='+endPoint+', accSchUrl='+accSchUrl);
    	if( endPoint.startsWith(gigSettings.Url_Token__c) ) {
    		system.debug('### - Myr_Gigya_MK - <respond> - url token encountered');
    		//token case
    		httpResponse.setStatusCode(responses.token.statusCode);
    		httpResponse.setStatus(responses.token.status);
    		if( responses.token.body != null ) {
    			httpResponse.setBody(responses.token.body);
    		}
    	} else if( endPoint.contains(accSchUrl) ) {
    		system.debug('### - Myr_Gigya_MK - <respond> - search account encountered');
    		//account search case
    		IntResponse accSearch = responses.getAccountSearch();
    		httpResponse.setStatusCode(accSearch.statusCode);
    		httpResponse.setStatus(accSearch.status);
    		if( accSearch.body != null ) {
    			httpResponse.setBody(accSearch.body);
    		}
    	} else {
    		system.debug('### - Myr_Gigya_MK - <respond> - others cases encountered');
    		//other funtion case
			IntResponse fctResp = responses.getFunctionResult();
    		httpResponse.setStatusCode(fctResp.statusCode);
    		httpResponse.setStatus(fctResp.status);
    		if( fctResp.body != null ) {
    			httpResponse.setBody(fctResp.body);
    		}
    	}
    	return httpResponse;
    }
       
    /** TOKEN POSSIBLE RESPONSE ------------------------------------------------------------------------------------------------------------ **/
    //@return Http 400 when trying to get the token
    global void setToken_BadRequest() {
    	responses.token.statusCode = 400;
    	responses.token.status = 'Bad Request';
    }
    //@return Http 200 but no response when trying to get the token
    global void setToken_EmptyResp() {
    	responses.token.statusCode = 200;
    	responses.token.status = 'OK';
    	//no body
    }
    //@return a proper token
    global void setToken_OK() {
    	responses.token.statusCode = 200;
    	responses.token.status = 'OK';
    	responses.token.body = '{\"access_token\":\"SlAV32hkKG\",\"expires_in\":3600}';
    }
    //@return a proper token
    global void setToken_EmptyToken() {
    	responses.token.statusCode = 200;
    	responses.token.status = 'OK';
    	responses.token.body = '{\"access_token\":\"\",\"expires_in\":3600}';
    }
    
    /** ACCOUNT SEARCH POSSIBLE RESPONSE ----------------------------------------------------------------------------------------------------- **/
    //@return Http 400 when trying to search the account
    global void setAccSch_BadRequest() {
    	responses.addAccountSearch(400, 'Bad Request', null);
    }
    //@return Http 200 but no response when trying to search for thr account
    global void setAccSch_EmptyResp() {
    	responses.addAccountSearch(200, 'OK', null);
    	//no body
    }
    //@return Http 200 and more than one account (2 accounts)
    global void setAccSch_MultipleAcc() {
    	String body = '{\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"31ba039fb8d340ceb2f43d52c89bf187\",\"time\":\"2015-03-22T11:42:25.943Z\",\"results\":[{\"UID\":\"17490\",\"isRegistered\":true,\"registeredTimestamp\":1344525120445,\"registered\":\"2012-08-09T15:12:00.445Z\",\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"h17490@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"h17490@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"rastropovich17490@gmail.com\",\"firstName\":\"Joe\",\"lastName\":\"Smith\",\"age\":\"31\",\"gender\":\"m\",\"country\":\"US\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"17490\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"h17490@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302}],\"data\":{},\"created\":\"2012-08-09T15:12:00.297Z\",\"createdTimestamp\":1344525120297,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302},{\"UID\":\"10067\",\"isRegistered\":true,\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"vich@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"vich@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"vich10067@gmail.com\",\"firstName\":\"David\",\"lastName\":\"Cohen\",\"age\":\"50\",\"gender\":\"m\",\"country\":\"Canada\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"10067\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"vich@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:02:56.969Z\",\"lastUpdatedTimestamp\":1344524576969,\"oldestDataUpdated\":\"2012-08-09T15:02:56.969Z\",\"oldestDataUpdatedTimestamp\":1344524576969}],\"data\":{},\"password\":{\"hash\":\"YG8PL6PwxlH0+KbUb4vG3w==\",\"hashSettings\":{\"algorithm\":\"pbkdf2\",\"rounds\":5000,\"salt\":\"iIj9T09VwfcvLv/0D7rFkA==\"}},\"tfaStatus\":\"forceOff\",\"created\":\"2012-08-09T15:02:56.961Z\",\"createdTimestamp\":1344524576961,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:02:56.969Z\",\"lastUpdatedTimestamp\":1344524576969,\"oldestDataUpdated\":\"2012-08-09T15:02:56.969Z\",\"oldestDataUpdatedTimestamp\":1344524576969}]}';
    	responses.addAccountSearch(200, 'OK', body);
    }
	//@return Http 200 and more than one account (4 accounts)
	global void setAccSch_MultipleAcc_4() {
		String body = '{\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"31ba039fb8d340ceb2f43d52c89bf187\",\"time\":\"2015-03-22T11:42:25.943Z\",\"results\":[{\"UID\":\"17490\",\"isRegistered\":true,\"registeredTimestamp\":1344525120445,\"registered\":\"2012-08-09T15:12:00.445Z\",\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"h17490@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"h17490@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"rastropovich17490@gmail.com\",\"firstName\":\"Joe\",\"lastName\":\"Smith\",\"age\":\"31\",\"gender\":\"m\",\"country\":\"US\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"17490\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"h17490@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302}],\"data\":{},\"created\":\"2012-08-09T15:12:00.297Z\",\"createdTimestamp\":1344525120297,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302},{\"UID\":\"10067\",\"isRegistered\":true,\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"vich@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"vich@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"vich10067@gmail.com\",\"firstName\":\"David\",\"lastName\":\"Cohen\",\"age\":\"50\",\"gender\":\"m\",\"country\":\"Canada\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"10067\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"vich@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:02:56.969Z\",\"lastUpdatedTimestamp\":1344524576969,\"oldestDataUpdated\":\"2012-08-09T15:02:56.969Z\",\"oldestDataUpdatedTimestamp\":1344524576969}],\"data\":{},\"password\":{\"hash\":\"YG8PL6PwxlH0+KbUb4vG3w==\",\"hashSettings\":{\"algorithm\":\"pbkdf2\",\"rounds\":5000,\"salt\":\"iIj9T09VwfcvLv/0D7rFkA==\"}},\"tfaStatus\":\"forceOff\",\"created\":\"2012-08-09T15:02:56.961Z\",\"createdTimestamp\":1344524576961,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:02:56.969Z\",\"lastUpdatedTimestamp\":1344524576969,\"oldestDataUpdated\":\"2012-08-09T15:02:56.969Z\",\"oldestDataUpdatedTimestamp\":1344524576969},{\"UID\":\"15234\",\"isRegistered\":true,\"registeredTimestamp\":1344525120445,\"registered\":\"2012-08-09T15:12:00.445Z\",\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"h17490@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"h17490@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"rastropovich17490@gmail.com\",\"firstName\":\"Joe\",\"lastName\":\"Smith\",\"age\":\"31\",\"gender\":\"m\",\"country\":\"US\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"17490\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"h17490@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302}],\"data\":{},\"created\":\"2012-08-09T15:12:00.297Z\",\"createdTimestamp\":1344525120297,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302},{\"UID\":\"567890\",\"isRegistered\":true,\"registeredTimestamp\":1344525120445,\"registered\":\"2012-08-09T15:12:00.445Z\",\"isActive\":true,\"isVerified\":false,\"iRank\":0,\"loginIDs\":{\"username\":\"h17490@gmail.com\",\"emails\":[],\"unverifiedEmails\":[]},\"emails\":{\"verified\":[],\"unverified\":[\"h17490@gmail.com\"]},\"socialProviders\":\"site\",\"profile\":{\"email\":\"rastropovich17490@gmail.com\",\"firstName\":\"Joe\",\"lastName\":\"Smith\",\"age\":\"31\",\"gender\":\"m\",\"country\":\"US\"},\"identities\":[{\"provider\":\"site\",\"providerUID\":\"17490\",\"isLoginIdentity\":false,\"gender\":\"\",\"email\":\"h17490@gmail.com\",\"allowsLogin\":false,\"isExpiredSession\":false,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302}],\"data\":{},\"created\":\"2012-08-09T15:12:00.297Z\",\"createdTimestamp\":1344525120297,\"lastLogin\":\"2012-08-09T15:02:56.961Z\",\"lastLoginTimestamp\":0,\"lastUpdated\":\"2012-08-09T15:12:00.302Z\",\"lastUpdatedTimestamp\":1344525120302,\"oldestDataUpdated\":\"2012-08-09T15:12:00.302Z\",\"oldestDataUpdatedTimestamp\":1344525120302}]}';
		responses.addAccountSearch(200, 'OK', body);
	}
    //@return Http 200 and not found
    global void setAccSch_NotFound() {
    	String body = '{\"objectsCount\":1,\"totalCount\":1,\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"cf059f1dfdbd44c28180955bea6880cd\",\"time\":\"2015-10-01T10:36:20.169Z\"}'; 
    	responses.addAccountSearch(200, 'OK', body);
    }
    //@return Http 200 and errorCode != 0
    global void setAccSch_ErrorCode() {
    	String body = '{"statusCode": 200,"errorCode":10,"statusReason":"","errorMessage":"internal error","callId": "03168515884044f19c6f825509ace465","time": "2015-10-01T10:38:22.700Z"}';
    	responses.addAccountSearch(200, 'OK', body);
    }
    //@return Http 200 and more than one account
    global void setAccSch_OK() {
    	String body = '{\"results\":[{\"UID\":\"31f50cda43114fdf8e8b50f92083a74f\",\"created\":\"2015-08-26T10:04:59.061Z\",\"registered\":\"2015-08-26T10:04:59.061Z\",\"isActive\":true,\"isVerified\":false,\"isRegistered\":true,\"loginIDs\":{\"emails\":[],\"unverifiedEmails\":[\"testing_register3@mailinator.com\"]},\"emails\":{\"verified\":[],\"unverified\":[\"testing_register3@mailinator.com\"]},\"profile\":{\"email\":\"testing_register3@mailinator.com\"},\"data\":{\"accountSfdcActivated\":true,\"newEmail\":\"\"}}],\"objectsCount\":1,\"totalCount\":1,\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"cf059f1dfdbd44c28180955bea6880cd\",\"time\":\"2015-10-01T10:36:20.169Z\"}'; 
    	responses.addAccountSearch(200, 'OK', body);
    }
    //@return Http 200 and one account with the given mail adress in the field corresponding to the parameter
    global void setAccSch_OK(String loginVerified, String loginUnverified, String emailVerified, String emailNotVerified, String newEmail) {
    	String body = '{\"results\":[{\"UID\":\"31f50cda43114fdf8e8b50f92083a74f\",\"created\":\"2015-08-26T10:04:59.061Z\",\"registered\":\"2015-08-26T10:04:59.061Z\",\"isActive\":true,\"isVerified\":false,\"isRegistered\":true,\"loginIDs\":{\"emails\":[\"'+loginVerified+'\"],\"unverifiedEmails\":[\"'+loginUnverified+'\"]},\"emails\":{\"verified\":[\"'+emailVerified+'\"],\"unverified\":[\"'+emailNotVerified+'\"]},\"profile\":{\"email\":\"testing_register3@mailinator.com\"},\"data\":{\"accountSfdcActivated\":true,\"newEmail\":\"'+newEmail+'\"}}],\"objectsCount\":1,\"totalCount\":1,\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"cf059f1dfdbd44c28180955bea6880cd\",\"time\":\"2015-10-01T10:36:20.169Z\"}'; 
    	responses.addAccountSearch(200, 'OK', body);
    }
    
    /** GIGYA MAIN FUNCTION POSSIBLE RESPONSES ------------------------------------------------------------------------------------------------ **/
    /** could work for resendActivationEmail and resetPassword **/
    //@return Http400 for the response
    global void setFunction_Http400() {
		String body = '{\"objectsCount\":1,\"totalCount\":1,\"statusCode\":200,\"errorCode\":0,\"statusReason\":\"OK\",\"callId\":\"cf059f1dfdbd44c28180955bea6880cd\",\"time\":\"2015-10-01T10:36:20.169Z\"}';
		responses.addFunctionResp( 400, 'Bad Request', body );
    }
    //@return Http200 and empty response
    global void setFunction_EmptyResp() {
		//no body
		responses.addFunctionResp( 200, 'OK', null );
    }
    //@return Http200 and error code in the response
    global void setFunction_ErrorCode() {
		String body = '{"statusCode": 200,"errorCode":15,"statusReason":"","errorMessage":"email not valid","callId": "03168515884044f19c6f825509ace465","time": "2015-10-01T10:38:22.700Z"}';
		responses.addFunctionResp( 200, 'OK', body );
    }
    //@return Http200 and response ok
    global void setFunction_OK() {
		String body = '{"statusCode": 200,"errorCode":0,"statusReason":"OK","callId": "03168515884044f19c6f825509ace465","time": "2015-10-01T10:38:22.700Z"}';
		responses.addFunctionResp( 200, 'OK', body) ;
    }
}