/* This class is to capture and redirect Record Type for Goodwill - Reena Jeyapal-09/04/2013 */

public with sharing class VFC01_GoodwillRecType {
    
    Id ent= System.currentPageReference().getParameters().get('ent');
    String retURL= System.currentPageReference().getParameters().get('retURL');
    string caseNo=System.currentPageReference().getParameters().get('CF00ND0000004qc7b');
    String caseId=System.currentPageReference().getParameters().get('CF00ND0000004qc7b_lkid');
    String recordId;
    String RecTypeNames;
    String RecTypeIds;

    public VFC01_GoodwillRecType(ApexPages.StandardController controller) {
        recordId = controller.getId();
     }
     
    // This method is used to redirect the agent based on RecordType Selection   
    public PageReference redirectToPage(){
    try{
    
        /* For China profile to find available RecTypes(one)-Reena 06-Aug-2013 */
        if (ApexPages.currentPage().getParameters().get('RecordType') == null) {
        Schema.DescribeSObjectResult describeAccount = Schema.SObjectType.Goodwill__c;
        List<Schema.RecordTypeInfo> rtInfos = describeAccount.getRecordTypeInfos();
        for(Schema.RecordTypeInfo rtInfo : rtInfos) {
        
        
        //Exclude Master RecordType
        if(!String.valueOf(rtInfo.getRecordTypeId()).endsWith('AAA') && rtInfo.isAvailable()) {
        ApexPages.currentPage().getParameters().put('RecordType', rtInfo.getRecordTypeId());
             
        }
       }
        if (ApexPages.currentPage().getParameters().get('RecordType') == null) {
        System.debug('null');
        }
        }
        
    
    }
    catch(Exception e){System.debug('Error'+e);
     }
    
    String GooTypeId=ApexPages.currentPage().getParameters().get('RecordType');
    

String DevName='';
        try{
         DevName =[Select Id,Name,DeveloperName from Recordtype WHERE SObjectType = 'Goodwill__c' and Id=:GooTypeId].DeveloperName;
        }
       catch(QueryException e)
        {
         System.debug('Error'+e);
        } 

    if(DevName=='TECHNICAL'){
        return new PageReference('/apex/VFP02_FTSGoodwill?RecordType='+GooTypeId+'&ent='+ent+'&retURL='+retURL+'&CF00ND0000004qc7b='+caseNo+'&CF00ND0000004qc7b_lkid='+caseId);
    }  
    else{
    
        return new PageReference('/a02/e?RecordType='+GooTypeId+'&nooverride=1&ent='+ent+'&retURL='+retURL+'&CF00ND0000004qc7b='+caseNo+'&CF00ND0000004qc7b_lkid='+caseId);
    
    } 
    
    
    }  
    
}