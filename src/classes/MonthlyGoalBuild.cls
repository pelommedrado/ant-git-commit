@isTest
public class MonthlyGoalBuild {
	
	public static MonthlyGoalBuild instance = null;
    
    public static MonthlyGoalBuild getInstance() {
        if(instance == null) {
            instance = new MonthlyGoalBuild();
        }
        return instance;
    }
    
    private MonthlyGoalBuild() {
    }
    
    public Monthly_Goal_Group__c createGoalGroup(String month, Decimal year, String matrixBir) {

        Monthly_Goal_Group__c goalGroup = new Monthly_Goal_Group__c();
        goalGroup.General_Objetive__c 			      = 100;
		goalGroup.Clio_Proposal__c 			          = 10; 
		goalGroup.Duster_Proposal__c 		          = 10;
		goalGroup.Duster_Oroch_Proposal__c 	          = 10;
		goalGroup.Fluence_Proposal__c 		          = 10;
		goalGroup.Kangoo_Proposal__c 		          = 10;
		goalGroup.Logan_Proposal__c 			      = 10;
		goalGroup.Master_Proposal__c 		          = 10;
		goalGroup.Sandero_Proposal__c 		          = 10;
		goalGroup.Sandero_RS_Proposal__c 	          = 10;
		goalGroup.Sandero_Stepway_Proposal__c         = 10;
		goalGroup.Month__c 							  = month;
		goalGroup.Year__c 							  = year;
		goalGroup.Matrix_Bir__c  					  = matrixBir;

		return goalGroup;
    }

    public Monthly_Goal_Dealer__c createGoalDealer(String goalGroupId, String dealerId){

    	Monthly_Goal_Dealer__c goalDealer = new Monthly_Goal_Dealer__c();
    	goalDealer.General_Objetive__c 			       = 100;
		goalDealer.Clio_Proposal__c 			       = 10; 
		goalDealer.Duster_Proposal__c 		           = 10;
		goalDealer.Duster_Oroch_Proposal__c 	       = 10;
		goalDealer.Fluence_Proposal__c 		           = 10;
		goalDealer.Kangoo_Proposal__c 		           = 10;
		goalDealer.Logan_Proposal__c 			       = 10;
		goalDealer.Master_Proposal__c 		           = 10;
		goalDealer.Sandero_Proposal__c 		           = 10;
		goalDealer.Sandero_RS_Proposal__c 	           = 10;
		goalDealer.Sandero_Stepway_Proposal__c         = 10;
		goalDealer.Monthly_Goal_Group__c 			   = goalGroupId;
		goalDealer.Dealer__c 						   = dealerId;
		return goalDealer;

    }

    public Monthly_Goal_Seller__c createGoalSeller(String goalDealerId, String sellerId){

    	Monthly_Goal_Seller__c goalSeller = new Monthly_Goal_Seller__c();
    	goalSeller.General_Objetive__c 			       = 100;
		goalSeller.Clio_Proposal__c 			       = 10; 
		goalSeller.Duster_Proposal__c 		           = 10;
		goalSeller.Duster_Oroch_Proposal__c 	       = 10;
		goalSeller.Fluence_Proposal__c 		           = 10;
		goalSeller.Kangoo_Proposal__c 		           = 10;
		goalSeller.Logan_Proposal__c 			       = 10;
		goalSeller.Master_Proposal__c 		           = 10;
		goalSeller.Sandero_Proposal__c 		           = 10;
		goalSeller.Sandero_RS_Proposal__c 	           = 10;
		goalSeller.Sandero_Stepway_Proposal__c         = 10;
		goalSeller.Monthly_Goal_Dealer__c 			   = goalDealerId;
		goalSeller.Seller__c 						   = sellerId;
		return goalSeller;

    }
	
}