@isTest
private class TestTask_BeforeInsert_Trigger {
    
    static testmethod void testTaskBeforeInsert() {
        
        User usr = new User (
            LastName = 'Mehdi', 
            RecordDefaultCountry__c = 'Brazil', 
            alias = 'lro', 
            Email = 'test@taskhandler.com', 
            BypassVR__c = false, 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = Label.PROFILE_SYSTEM_ADMIN, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'test@taskhandler.com');
        
        System.runAs(usr) {
            
            Test.startTest();
            Case cse = new Case();
            cse.Status = 'New';
            insert cse;
            
            Task tsk1 = new Task();
            tsk1.type = 'type';
            tsk1.status = 'NotStarted';
            tsk1.WhatId = cse.Id;
            tsk1.Priority = 'Normal';
            tsk1.subject = 'subject';           
            insert tsk1;
            
            Task taskCreated = [Select id from Task where type = 'type'];
            
            System.assertNotEquals(taskCreated , null, 'Test 1 object was null and not inserted correctly');
            System.assertEquals(taskCreated.id, taskCreated.id, 'Field1 and Field2 not equals in test 0'); 
            
            Test.stopTest();
        }                       
    }
}