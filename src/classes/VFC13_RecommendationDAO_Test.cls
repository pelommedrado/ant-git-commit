/**
	Class   -   VFC13_RecommendationDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC13_RecommendationDAO.
**/
@isTest
public with sharing class VFC13_RecommendationDAO_Test {
	
	static testMethod void VFC13_RecommendationDAO_Test() {
        // TO DO: implement unit test
        List<RCM_Recommendation__c> lstRecommendationInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRecommendationObject();
        Test.startTest();
        List<RCM_Recommendation__c> lstRecommendation =  VFC13_RecommendationDAO.getInstance().findRecommendationsWithName();
        Test.stopTest();
        system.assert(lstRecommendation.size() > 0);
    }
}