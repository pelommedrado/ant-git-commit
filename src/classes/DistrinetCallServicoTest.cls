@isTest(SeeAllData=true)
private class DistrinetCallServicoTest {

    static Quote quote;
    static QuoteLineItem quoteLineItem;
    static VEH_Veh__c v;
    static Model__c modelo;
    static PVVersion__c versao;

    static void setup(){

        MyOwnCreation moc = new MyOwnCreation();

        Product2 prod = new Product2(
            Name = 'CLIO'
        );
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
            UnitPrice = 10000, 
            IsActive = true
        );
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            isActive=true
        );
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = prod.Id,
            UnitPrice = 12000, 
            IsActive = true
        );
        insert customPrice;

        Account account1 = new Account(
            Name = 'Account Test 01', 
            RecordTypeId = Utils.getRecordTypeId('Account','Network_Site_Acc'),
            IDBIR__c = '761001'
        );
        
        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName           = 'Personal';
        sObjAccPersonal.LastName            = 'Account';
        sObjAccPersonal.Phone               = '1133334444';
        sObjAccPersonal.RecordTypeId        = Utils.getRecordTypeId('Account','Personal_Acc');
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity        = 'Paris';
        sObjAccPersonal.ShippingCountry     = 'France';
        sObjAccPersonal.ShippingState       = 'IDF';
        sObjAccPersonal.ShippingPostalCode  = '75013';
        sObjAccPersonal.ShippingStreet      = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        sObjAccPersonal.PersLandline__c     = '1133334444';
        sObjAccPersonal.PersMobPhone__c     = '1155556666';
        sObjAccPersonal.RgStateTexto__c     = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone     = '1133334444';
        sObjAccPersonal.PersLandline__c     = '1133334444';
        sObjAccPersonal.PersMobPhone__c     = '1155556666';
        sObjAccPersonal.Sex__c              = 'Man';
        sObjAccPersonal.MaritalStatus__c    = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate     = System.today();

        List<Account> accList = new List<Account>();
        accList.add(sObjAccPersonal);
        accList.add(account1);
        Insert accList;
        
        Opportunity opportunity1 = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            Dealer__c = account1.Id,
            AccountId = sObjAccPersonal.Id);
        Insert opportunity1;
        
        v = moc.criaVeiculo();
        v.Model__c = 'CLIO 4';
        v.Price__c = 1;
        v.DateofManu__c = system.today();
        Insert v;
        
        VRE_VehRel__c vr = moc.criaVeiculoRelacionado();
        vr.VIN__c = v.Id;
        vr.Account__c = account1.Id;
        Insert vr;

        quote = moc.criaQuote();
        quote.Name = '123quote123';
        quote.OpportunityId = opportunity1.Id;
        quote.BillingAccount__c = sObjAccPersonal.Id;
        quote.Pricebook2Id = customPB.Id;
        Insert quote;

        modelo = new Model__c(
            Name = 'modelo',
            Model_PK__c = '2342'
        );
        Insert modelo;
        
        versao = new PVVersion__c(
            Name = 'Versao',
            Model__c = modelo.Id,
            PVC_Maximo__c = 2,
            PVR_Minimo__c = 1,
            Price__c = 1,
            Version_Id_Spec_Code__c = '234werferg'
        );
        Insert versao;
        
        Optional__c opt = new Optional__c(
            Name = 'Azul',
            Type__c = 'Cor',
            Optional_Code__c = '3N4',
            Version__c = versao.Id
        );
        
        Optional__c opt2 = new Optional__c(
            Name = 'harmonia',
            Type__c = 'Harmonia',
            Optional_Code__c = '3U4',
            Version__c = versao.Id
        );
        
        Optional__c opt3 = new Optional__c(
            Name = 'Trim',
            Type__c = 'Trim',
            Optional_Code__c = '3NT',
            Version__c = versao.Id
        );

        List<Optional__c> optList = new List<Optional__c>();
        optList.add(opt);
        optList.add(opt2);
        optList.add(opt3);
        Insert optList;

        quoteLineItem = new QuoteLineItem();
        quoteLineItem.QuoteId = quote.Id;
        quoteLineItem.PricebookEntryId = customPrice.Id;
        quoteLineItem.Quantity = 1;
        quoteLineItem.UnitPrice = 1;
        quoteLineItem.Optionals_AOC__c = 'optional1 optional2';
        quoteLineItem.Harmony_AOC__c = opt2.Id;
        quoteLineItem.Upholstery_AOC__c = opt3.Id;
        quoteLineItem.Color_AOC__c = opt.Id;
        Insert quoteLineItem;

    }
    
    static testMethod void testCall() {

        setup();
        
        Test.startTest();
        
        //String orderId, String color, String upho, String harmo, String modelName, String modelId, String versionId
        
        DistrinetCallServico.call(quote.Id, quoteLineItem.Color_AOC__c, quoteLineItem.Upholstery_AOC__c, '3U4', 'CLIO', modelo.Id, versao.Id, 'optional1 optional2');

        Test.stopTest();
    }

    static testMethod void testCall2() {

        setup();

        quoteLineItem.Vehicle__c = v.Id;
        Update quoteLineItem;
        
        Test.startTest();
        
        DistrinetCallServico.call(quote.Id, quoteLineItem.Color_AOC__c, quoteLineItem.Upholstery_AOC__c, '3U4', 'CLIO', modelo.Id, versao.Id, 'optional1 optional2');

        Test.stopTest();
    }

    static testMethod void testCall3() {

        setup();

        modelo.IsNewRelease__c = true;
        Update modelo;

        quoteLineItem.Model_AOC__c = modelo.Id;
        Update quoteLineItem;
        
        Test.startTest();
        
        DistrinetCallServico.call(quote.Id, quoteLineItem.Color_AOC__c, quoteLineItem.Upholstery_AOC__c, '3U4', 'CLIO', modelo.Id, versao.Id, 'optional1 optional2');

        Test.stopTest();
    }

}