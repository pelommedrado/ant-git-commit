@isTest 
public with sharing class VFC106_DBM_Import_BO_Test {

	static testMethod void unitTest1()
	{
		// Prepare test data
		Id LeadRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('LDD_Marketing');
		Id AccountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');

		List<MLC_Molicar__c> lstMolicar = new List<MLC_Molicar__c>
			{new MLC_Molicar__c(Brand__c = 'FORD',
                                Model__c = 'ECOSPORT',
                                Configuration__c = 'SUV Compacto'),
             new MLC_Molicar__c(Brand__c = 'FIAT',
                                Model__c = 'PALIO',
                                Configuration__c = 'UNO')
		    };
		insert lstMolicar;			

		List<Account> lstAccount = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        Account acc1 = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lstAccount[0].Id);
        acc1.RecordTypeId = AccountRecordTypeId;
        acc1.LastName = 'Doe';
        update acc1;

        Account acc2 = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lstAccount[1].Id);
        acc2.NUMDBM_BR__c = 1000004;
        update acc2;
        
        List<Lead> lstLeads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
		Lead lead1 = VFC09_LeadDAO.getInstance().findLeadByLeadId(lstLeads[0].Id);
		lead1.lastName = 'Doe1';
		lead1.NUMDBM__c = 1000002;	
		update lead1;
		
		Lead lead2 = VFC09_LeadDAO.getInstance().findLeadByLeadId(lstLeads[1].Id);
		lead2.lastName = 'Doe1';
		lead2.Account__c = acc2.Id;	
		update lead2;

		List<DBM_Import__c> lstNewImports = new List<DBM_Import__c>();
		lstNewImports.add (new DBM_Import__c(NUMDBM__c = 2000001,
		                                     //Id_Salesforce__c = acc1.Id ,
											 First_Name__c = 'John 101',
											 Last_Name__c = 'Doe',
											 Email__c = 'john.doe101@email.com',
		                                     Molicar_Brand__c = 'FORD',
		                                     Molicar_Model__c = 'ECOSPORT',
		                                     Molicar_Configuration__c = 'SUV Compacto'));

		lstNewImports.add (new DBM_Import__c(NUMDBM__c = 1000004,		                                     
											 First_Name__c = 'John',
											 Last_Name__c = 'Doe',
											 Email__c = 'john.doe102@email.com',
		                                     Molicar_Brand__c = 'FORD',
		                                     Molicar_Model__c = 'ECOSPORT',
		                                     Molicar_Configuration__c = 'SUV Compacto'));                                   
		lstNewImports.add (new DBM_Import__c(NUMDBM__c = 2000002,
		                                     Id_Salesforce__c = lead1.Id ,
		                                     First_Name__c = 'John 2',
											 Last_Name__c = 'Doe',
											 Email__c = 'john.doe103@email.com',
		                                     Molicar_Brand__c = 'FORD',
		                                     Molicar_Model__c = 'ECOSPORT',
		                                     Molicar_Configuration__c = 'SUV Compacto'));
		lstNewImports.add (new DBM_Import__c(NUMDBM__c = 2000003,
										     First_Name__c = 'John 3',
										     Email__c = 'john.doe104@email.com',
											 Last_Name__c = 'Doe'));

		// Start test
		Test.startTest();

		// Call trigger
		upsert lstNewImports;

		// Stop test
		Test.stopTest();
		
	}
}