global class Rforce_SearchVehicleController{

 public String urlForLink {get;set;}
 public String vehiclePrefix {get;set;}
 public static String countryCode {get;set;}
 
  
public Rforce_SearchVehicleController() {
    countryCode ='';
    urlForLink = URL.getSalesforceBaseUrl().toExternalForm();        
    Schema.DescribeSObjectResult descrVehicle = Schema.SObjectType.VEH_Veh__c;
    vehiclePrefix = descrVehicle.getKeyPrefix();
}
/*
webservice static Boolean getVehicleDetails(String VINNO,String VID){
    
    PageReference pageRef;
    Rforce_BVM_WS.ApvGetDetVehXmlResponse rep;
    Boolean status=false;

    if (VINNO != null){
        rep = searchBVM(VINNO);
     }
       if (rep != null && rep.detVeh != null) {
        pageRef = updateVehicleData(rep,VINNO,VID);   
        status=true;    
     }
      else {
     // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Not found');
     // ApexPages.addMessage(myMsg);
        status=false;
    
      }   
   return status;
}
*/
webservice static Boolean getVehicleDetailsBVM(String VINNO,String VID){
    
    PageReference pageRef;
    Rforce_BVMComplet_WS.ApvGetDetVehResponse rep;
    Boolean status=false;

    if (VINNO != null){
        rep = searchBVMComplet(VINNO);
     }
       if (rep != null && rep.detVeh != null) {
        pageRef = updateVehicleDataBVM(rep,VINNO,VID);   
        status=true;    
     }
      else {
        status=false;
    
      }   
   return status;
}
/*
 public static Rforce_BVM_WS.ApvGetDetVehXmlResponse searchBVM(String VINNO)
    {
        Rforce_BVM_WS.ApvGetDetVehXmlRequest request = new Rforce_BVM_WS.ApvGetDetVehXmlRequest();               
        Rforce_BVM_WS.ServicePreferences servicePref = new Rforce_BVM_WS.ServicePreferences();
        Rforce_BVM_WS.ApvGetDetVehXml VehWS = new Rforce_BVM_WS.ApvGetDetVehXml();
        Rforce_BVM_WS.ApvGetDetVehXmlResponse VEH_WS = new Rforce_BVM_WS.ApvGetDetVehXmlResponse();
       
        try
        {  
            servicePref.bvmsi25SocEmettrice  = 'CCB';
            servicePref.bvmsi25Vin = VINNO;
            servicePref.bvmsi25CodePaysLang = countryCode; //FRA 
            servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
            request.ServicePreferences = servicePref;
              
            VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
            VehWS.clientCertName_x = System.label.renaultcertificate;           
            VehWS.timeout_x=40000;
            System.debug('req***********'+request); 
            System.debug('servicePref ***********'+servicePref); 

            return (Rforce_BVM_WS.ApvGetDetVehXmlResponse)VehWS.getApvGetDetVehXml(request);
        }            
        catch (Exception e)
        {                    
            system.debug(VEH_WS);
            system.debug('erreur*********** : ' + String.valueOf(e));
            return null;
        }
    }
 */   
       public static Rforce_BVMComplet_WS.ApvGetDetVehResponse searchBVMComplet(String VINNO)
    {
        
        Rforce_BVMComplet_WS.ApvGetDetVehRequest request = new Rforce_BVMComplet_WS.ApvGetDetVehRequest();               
        Rforce_BVMComplet_WS.ServicePreferences servicePref = new Rforce_BVMComplet_WS.ServicePreferences();
        Rforce_BVMComplet_WS.ApvGetDetVeh VehWS = new Rforce_BVMComplet_WS.ApvGetDetVeh();
        Rforce_BVMComplet_WS.ApvGetDetVehResponse VEH_WS = new Rforce_BVMComplet_WS.ApvGetDetVehResponse();
       
        try
        {  
            servicePref.bvmsi24CodeChampIn  = 'AA';
            servicePref.bvmsi24CodeChampOut  = 'OF';
            servicePref.bvmsi24ValeurChamp = VINNO;
            servicePref.bvmsi24CodePaysLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FRA 
            servicePref.bvmsi24CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
            request.ServicePreferences = servicePref;
              
            VehWS.endpoint_x = System.label.Url_BVM_Complet; 
            VehWS.clientCertName_x = System.label.RenaultCertificate;           
            VehWS.timeout_x=40000;
            
            return (Rforce_BVMComplet_WS.ApvGetDetVehResponse)VehWS.getApvGetDetVeh(request);
        }            
        catch (Exception e)
        {                    
            system.debug(VEH_WS);
            system.debug('erreur : ' + String.valueOf(e));
            return null;
        }
    }
 /*   
    public static PageReference updateVehicleData(Rforce_BVM_WS.ApvGetDetVehXmlResponse vehBVM,String VIN,String VID)
    {
        VEH_Veh__c veh = new VEH_Veh__c();
        VEH_Veh__c inf= new VEH_Veh__c();
        inf= [Select FirstRegistrDate__c, LastRegistrDate__c,VehicleRegistrNbr__c  from VEH_Veh__c where Id = :VID];
        veh.Name = VIN;
        veh.Id=VID;   
        veh.FirstRegistrDate__c=inf.FirstRegistrDate__c; 
        veh.LastRegistrDate__c=inf.LastRegistrDate__c;
        veh.VehicleRegistrNbr__c=inf.VehicleRegistrNbr__c;    
                
        //VersionCode__c p460:version
        if (vehBVM.detVeh.version != null){
            veh.VersionCode__c = vehBVM.detVeh.version;
        }
        
        //AfterSalesType__c p460:tvv
        if (vehBVM.detVeh.tvv != null) {
            veh.AfterSalesType__c = vehBVM.detVeh.tvv;
        }
         
         //EngineType__c p460:typeMot 
        if (vehBVM.detVeh.typeMot != null){
            veh.EngineType__c = vehBVM.detVeh.typeMot;         
        }
        
        //EngineIndex__c p460:indMot
        if (vehBVM.detVeh.indMot != null){
            veh.EngineIndex__c = vehBVM.detVeh.indMot; 
        }
        
         //EngineManuNbr__c p460:NMot
        if (vehBVM.detVeh.NMot != null){
            veh.EngineManuNbr__c = vehBVM.detVeh.NMot;
        }
           
        //GearBoxType__c p460:typeBoi
        if (vehBVM.detVeh.typeBoi != null){
            veh.GearBoxType__c = vehBVM.detVeh.typeBoi;
        } 
 
       //GearBoxIndex__c p460:indBoi 
        if (vehBVM.detVeh.indBoi != null){
            veh.GearBoxIndex__c = vehBVM.detVeh.indBoi; 
        }  
        
      //GearBoxManuNbr__c p460:NBoi
        if (vehBVM.detVeh.NBoi != null){
            veh.GearBoxManuNbr__c = vehBVM.detVeh.NBoi;   
        } 
         
        //DateofManu__c p460:dateTcmFab 
        if (vehBVM.detVeh.dateTcmFab != null && vehBVM.detVeh.dateTcmFab.length() > 7) {
            veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(0, 2)));
        }  
      
         // updating the EnergyType from BVM service when the value is null
    
        if (veh.EnergyType__c == null) {
            if (vehBVM.detVeh.criteres != null) {
            for (Rforce_BVM_WS.DetVehCritere crit : vehBVM.detVeh.criteres)
            {
                //EnergyType__c Objet 019
                if (crit.cdObjOf == '019')
                    veh.EnergyType__c = crit.critereOf;
            }
          }
        }
      
          
        if ( veh.DeliveryDate__c == null) {
            if (vehBVM.detVeh.dateLiv != null && vehBVM.detVeh.dateLiv.length() > 7)
                veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(0, 2)));
        }
              
        if (veh.Model__c == null) {
             if (vehBVM.detVeh.libModel != null)
             veh.Model__c = vehBVM.detVeh.libModel;
        }
        
        
        if (veh.BodyType__c == null) {
            if (vehBVM.detVeh.libCarrosserie != null){
                veh.BodyType__c = vehBVM.detVeh.libCarrosserie;
           }
       }     

       
    // Update the new Criteria Field   
       
       
     Rforce_BVM_WS.DetVehCritere[] criteres=vehBVM.detVeh.criteres;

      String ModelRangeValue;
      String ModelCode;
      for(integer i=0;i<criteres.size();i++)
      {

      System.debug('Criteria Label Value>>>>>>>>>>>'+Criteres[i].libCritOf);
      
      System.debug('Criteria Value>>>>>>>>>>>'+Criteres[i].critereOf);
      System.debug('Criteria Value cdCritOf>>>>>>>>>>>'+Criteres[i].cdCritOf);
      System.debug('Criteria Value cdObjOf>>>>>>>>>>>'+Criteres[i].cdObjOf);
     
        if(criteres[i].cdObjOf =='008')
          {
         ModelCode=criteres[i].critereOf;
         
         System.debug('ModelCode>>>>>>>>>>>'+ModelCode);
     }
     
     
       if(criteres[i].libCritOf=='MODELE FICHE GAMME')
          {
         ModelRangeValue=criteres[i].critereOf;
         
         System.debug('ModelRangeValue>>>>>>>>>>>'+ModelRangeValue);
     }
     }     
           if (veh.Model_Range__c == null) {
           if (ModelRangeValue != null){
             veh.Model_Range__c = ModelRangeValue;
                          
           }
       }     
       
       
       
       if (veh.ModelCode__c == null) {
           if (ModelCode != null){
             veh.ModelCode__c = ModelCode;
                          
           }
       }     
       
       veh.BVM_data_Upto_date__c =true;
    
        System.debug('res*********'+vehBVM);
            
                 
        try
        {
            update veh;
            System.debug('Salesorce Identification used in Plugin >>>>>'+Veh);
        }
        catch(DMLException dmle)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSaveVehicle);
            ApexPages.addMessage(myMsg);
            return null;
        }
                return new PageReference('/' + veh.Id);
    }
*/
public static PageReference updateVehicleDataBVM(Rforce_BVMComplet_WS.ApvGetDetVehResponse vehBVM,String VIN,String VID)
    {
        VEH_Veh__c veh = new VEH_Veh__c();
        VEH_Veh__c inf= new VEH_Veh__c();
        inf= [Select FirstRegistrDate__c, LastRegistrDate__c,CommunityReceptionCode__c,GearboxPN__c,EnginePN__c,EngineAssemblyFactoryName__c,GearboxAssemblyFactoryName__c,SpecificPlateLine__c,AssemblyFactoryName__c,Line4OvalPlate__c, Line3OvalPlate__c,Line2OvalPlate__c,MaxGrossWeightofFrontAxle__c,VehicleRegistrNbr__c,GrossTrainWeight__c,MaxWeightRearAxle__c,RegistrationCountrycode__c,MaxGrossVehicleWeight__c, DeliveryDate__c, DateofManu__c, VersionCode__c, ModelCode__c, BodyType__c, Model__c, EnergyType__c  from VEH_Veh__c where Id = :VID];
        veh.Name = VIN;
        veh.Id=VID;   
        veh.FirstRegistrDate__c=inf.FirstRegistrDate__c; 
        veh.LastRegistrDate__c=inf.LastRegistrDate__c;
        veh.VehicleRegistrNbr__c=inf.VehicleRegistrNbr__c; 
        veh.DeliveryDate__c = inf.DeliveryDate__c;
        veh.DateofManu__c=inf.DateofManu__c;
        veh.VersionCode__c=inf.VersionCode__c; 
        veh.RegistrationCountrycode__c = inf.RegistrationCountrycode__c;
        veh.MaxGrossVehicleWeight__c = inf.MaxGrossVehicleWeight__c;
        veh.MaxWeightRearAxle__c = inf.MaxWeightRearAxle__c;
        veh.GrossTrainWeight__c = inf.GrossTrainWeight__c;
        veh.MaxGrossWeightofFrontAxle__c = inf.MaxGrossWeightofFrontAxle__c;
        veh.Line2OvalPlate__c = inf.Line2OvalPlate__c;
        veh.Line3OvalPlate__c = inf.Line3OvalPlate__c;
        veh.Line4OvalPlate__c = inf.Line4OvalPlate__c;
        veh.SpecificPlateLine__c = inf.SpecificPlateLine__c;
        veh.AssemblyFactoryName__c = inf.AssemblyFactoryName__c;
        veh.GearboxAssemblyFactoryName__c = inf.GearboxAssemblyFactoryName__c;
        veh.EngineAssemblyFactoryName__c = inf.EngineAssemblyFactoryName__c;
        veh.EnginePN__c = inf.EnginePN__c;
        veh.GearboxPN__c = inf.GearboxPN__c;
        veh.CommunityReceptionCode__c=inf.CommunityReceptionCode__c;
        
        if (veh.VehicleRegistrNbr__c == null){
          if (vehBVM.detVeh.bvmso24NImmat != null)
            veh.VehicleRegistrNbr__c = vehBVM.detVeh.bvmso24NImmat; 
        }
        
        if (veh.CommunityReceptionCode__c == null){
          if (vehBVM.detVeh.bvmso24NRecCom != null)
            veh.CommunityReceptionCode__c = vehBVM.detVeh.bvmso24NRecCom; 
        }
        
        
        if(veh.RegistrationCountrycode__c == null){
          if(vehBVM.detVeh.bvmso24PaysImmat !=null)
        veh.RegistrationCountrycode__c = vehBVM.detVeh.bvmso24PaysImmat;
        }
        if(veh.GearboxPN__c == null){
        if(vehBVM.detVeh.bvmso24RefBoi != null)
        veh.GearboxPN__c = vehBVM.detVeh.bvmso24RefBoi;
        }
        
        if(veh.EnginePN__c == null){
          if(vehBVM.detVeh.bvmso24RefMot !=null)
        veh.EnginePN__c = vehBVM.detVeh.bvmso24RefMot;
        }
        
        if(veh.EngineAssemblyFactoryName__c == null){
        if(vehBVM.detVeh.bvmso24LibUsimot !=null)
        veh.EngineAssemblyFactoryName__c = vehBVM.detVeh.bvmso24LibUsimot;
        }
        
        if(veh.GearboxAssemblyFactoryName__c == null){
          if(vehBVM.detVeh.bvmso24LibUsiboi != null)
            veh.GearboxAssemblyFactoryName__c =  vehBVM.detVeh.bvmso24LibUsiboi;    
        }
        
        if(veh.AssemblyFactoryName__c == null){
           if(vehBVM.detVeh.bvmso24LibUsiveh != null)
           veh.AssemblyFactoryName__c = vehBVM.detVeh.bvmso24LibUsiveh;
        }
        
        if(veh.Line2OvalPlate__c == null){
        if(vehBVM.detVeh.bvmso24Ligne2P12 != null)
        veh.Line2OvalPlate__c = vehBVM.detVeh.bvmso24Ligne2P12;
        }
        
        if(veh.SpecificPlateLine__c == null){
        if(vehBVM.detVeh.bvmso24PlacSpec != null)
        veh.SpecificPlateLine__c = vehBVM.detVeh.bvmso24PlacSpec;
        }
        
        if(veh.Line3OvalPlate__c == null){
        if(vehBVM.detVeh.bvmso24Ligne3P12 != null)
        veh.Line3OvalPlate__c = vehBVM.detVeh.bvmso24Ligne3P12;
        }
        
        if(veh.Line4OvalPlate__c == null){
        if(vehBVM.detVeh.bvmso24Ligne4P12 != null)
        veh.Line4OvalPlate__c = vehBVM.detVeh.bvmso24Ligne4P12;
        }
        
        if(veh.MaxGrossWeightofFrontAxle__c == null){
           if(vehBVM.detVeh.bvmso24Ptmaav != null)
           veh.MaxGrossWeightofFrontAxle__c = vehBVM.detVeh.bvmso24Ptmaav;
        }
        
        if(veh.GrossTrainWeight__c == null){
        if(vehBVM.detVeh.bvmso24Ptr != null)
        veh.GrossTrainWeight__c = vehBVM.detVeh.bvmso24Ptr;
        }
        
        if(veh.MaxGrossVehicleWeight__c == null){
            if(vehBVM.detVeh.bvmso24Ptma !=null)
            veh.MaxGrossVehicleWeight__c = vehBVM.detVeh.bvmso24Ptma;
        }
        if(veh.MaxWeightRearAxle__c == null){
            if(vehBVM.detVeh.bvmso24Ptmaar != null)
            veh.MaxWeightRearAxle__c = vehBVM.detVeh.bvmso24Ptmaar;
        }
            
        if (veh.FirstRegistrDate__c == null){
             if (vehBVM.detVeh.bvmso24DateImmat1 != null && vehBVM.detVeh.bvmso24DateImmat1.length() > 7)
            veh.FirstRegistrDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat1.substring(0, 2)));
          }        
            
        if (veh.LastRegistrDate__c == null){
             if (vehBVM.detVeh.bvmso24DateImmat != null && vehBVM.detVeh.bvmso24DateImmat.length() > 7)
            veh.LastRegistrDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateImmat.substring(0, 2))); 
        }
                 
        //VersionCode__c p460:version
        if (veh.VersionCode__c==null){
        if (vehBVM.detVeh.bvmso24Version != null)
            veh.VersionCode__c = vehBVM.detVeh.bvmso24Version;
        }
        
        if (veh.CountryOfDelivery__c==null){
        if (vehBVM.detVeh.bvmso24PaysLiv != null)
            veh.CountryOfDelivery__c = vehBVM.detVeh.bvmso24PaysLiv;
        }
        
        if (veh.DeliveryDate__c==null){
        if (vehBVM.detVeh.bvmso24DateLiv != null && vehBVM.detVeh.bvmso24DateLiv.length() > 7)
            veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateLiv.substring(0, 2)));
        }           


        if (vehBVM.detVeh.bvmso24LibModel != null)
            veh.Model__c = vehBVM.detVeh.bvmso24LibModel;



        if (vehBVM.detVeh.bvmso24LibCarrosserie != null)
            veh.BodyType__c = vehBVM.detVeh.bvmso24LibCarrosserie;
         
        //AfterSalesType__c p460:tvv
        if (vehBVM.detVeh.bvmso24Tvv != null)
            veh.AfterSalesType__c = vehBVM.detVeh.bvmso24Tvv;
         
         //EngineType__c p460:typeMot 
       if (vehBVM.detVeh.bvmso24TypeMot != null)
            veh.EngineType__c = vehBVM.detVeh.bvmso24TypeMot;
        
        //EngineIndex__c p460:indMot
        if (vehBVM.detVeh.bvmso24IndMot != null)
            veh.EngineIndex__c = vehBVM.detVeh.bvmso24IndMot;
        
         //EngineManuNbr__c p460:NMot
        if (vehBVM.detVeh.bvmso24NMot != null)
            veh.EngineManuNbr__c = vehBVM.detVeh.bvmso24NMot;
            
            if (vehBVM.detVeh.bvmso24NFab != null)
            veh.VehicleManuNbr__c = vehBVM.detVeh.bvmso24NFab; 
           
        //GearBoxType__c p460:typeBoi
       if (vehBVM.detVeh.bvmso24TypeBoi != null)
            veh.GearBoxType__c = vehBVM.detVeh.bvmso24TypeBoi;
 
       //GearBoxIndex__c p460:indBoi 
       if (vehBVM.detVeh.bvmso24IndBoi != null)
            veh.GearBoxIndex__c = vehBVM.detVeh.bvmso24IndBoi;
        
      //GearBoxManuNbr__c p460:NBoi
        if (vehBVM.detVeh.bvmso24NBoi != null)
            veh.GearBoxManuNbr__c = vehBVM.detVeh.bvmso24NBoi;
         
        //DateofManu__c p460:dateTcmFab 
        if(veh.DateofManu__c==null){
         if (vehBVM.detVeh.bvmso24DateTcmFab != null && vehBVM.detVeh.bvmso24DateTcmFab.length() > 7)
            veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.bvmso24DateTcmFab.substring(0, 2)));
         
        }
         // updating the EnergyType from BVM service when the value is null
    

            if (vehBVM.detVeh.bvmso24DataCrit != null)
        {
             String nrj;
             String cdnrj;
             
              
                for (integer i = 0; i < vehBVM.detVeh.bvmso24DataCrit.length(); i++)
        {
         
                if (vehBVM.detVeh.bvmso24DataCrit.substring(i, i + 3) == '019')
                {
                            nrj = vehBVM.detVeh.bvmso24DataCrit.substring(i, i + 6);
                              
                }
                i = i + 97;
         }
             
             cdnrj=mapNrj(nrj);
             
              System.debug('verifffff nrjjjjj: '+cdnrj);
            
             veh.EnergyType__c =cdnrj;
             
        }
    // Added the below condition to update the IntialParis Customer


    if (vehBVM.detVeh.bvmso24DataCrit != null)
    {
        String objCode;
        String criteriaCode;
        for (Integer j = 0; j < vehBVM.detVeh.bvmso24DataCrit.length(); j++)
        {
            if (vehBVM.detVeh.bvmso24DataCrit.substring(j, j + 3) == 'G04')
            {
                objCode = vehBVM.detVeh.bvmso24DataCrit.substring(j, j + 6);
                system.debug('## ObjectCode is..::'+ objCode);

            }
            j=j + 97;
        }
        if (objCode!=null && objCode=='G04002')
        {
            veh.Initiale_Paris__c=true;
        }
    }         
       
    // Update the new Criteria Field   
              

            if (vehBVM.detVeh.bvmso24CodeModel2 != null)
            veh.ModelCode__c = vehBVM.detVeh.bvmso24CodeModel2;
  
       
       veh.BVM_data_Upto_date__c =true;
    
        System.debug('res*********'+vehBVM);
            
                 
        try
        {
            update veh;
            System.debug('Salesorce Identification used in Plugin >>>>>'+Veh);
        }
        catch(DMLException dmle)
        {
         //   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSaveVehicle);
         //   ApexPages.addMessage(myMsg);
            return null;
        }
                return new PageReference('/' + veh.Id);
    }
    
    public static string mapNrj(String cd) {
    
    String EssenceTest;
    
    if (cd == '019001')
    EssenceTest='ELEC';
    else if (cd == '019002')
    EssenceTest='FLEXFL';
    else if (cd == '019003')
    EssenceTest='CNGS';
    else if (cd == '019004')
    EssenceTest='BIODIE';
    else if (cd == '019006')
    EssenceTest='BIOESS';
    else if (cd == '019008')
    EssenceTest='GNV';
    else if (cd == '019010')
    EssenceTest='ESS';
    else if (cd == '019011')
    EssenceTest='ESSSPB';
    else if (cd == '019020')
    EssenceTest='DIESEL';
    else if (cd == '019030')
    EssenceTest='GPL';
    else if (cd == '019035')
    EssenceTest='BICARB';
    else if (cd == '019999')
    EssenceTest='SAN019';
    // Added for Intial Paris customer
    else if (cd == 'G04002')
    EssenceTest='INIPAR';  
     
    return EssenceTest;
    
}

    
  }