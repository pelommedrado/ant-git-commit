//Use to simulate a response from the WebService
@isTest
global class  Rforce_RcArch_WebServiceMock_Test implements WebServiceMock  {
    
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        

        if (responseName.equals('getAttachmentDetailsListResponse')){
          Rforce_RcArchivage.attachmentDetailsList myAttach = new Rforce_RcArchivage.attachmentDetailsList();
          myAttach.attachmentName='test';
          myAttach.createdDate=datetime.now();
          myAttach.extension='pdf';
          
          // Send the response
          Rforce_RcArchivage.getAttachmentDetailsListResponse response_AttDetails = new Rforce_RcArchivage.getAttachmentDetailsListResponse();
          response_AttDetails.attachmentDetails = new Rforce_RcArchivage.attachmentDetailsList[0];
          response_AttDetails.attachmentDetails.add(myAttach);
          response.put('response_x', response_AttDetails); 
        }
        
        if (responseName.equals('getCaseIdListResponse')){
          Rforce_RcArchivage.getCaseIdList req = ((Rforce_RcArchivage.getCaseIdList)request);
          Rforce_RcArchivage.caseId myCase = new Rforce_RcArchivage.caseId();
          // Create a case
          myCase.caseId = '';
          myCase.countryCode = '';
          // Send the response
          Rforce_RcArchivage.getCaseIdListResponse response_caseId = new Rforce_RcArchivage.getCaseIdListResponse();
          response_caseId.caseIdList = new Rforce_RcArchivage.caseId[0];
          response_caseId.caseIdList.add(myCase);
          response.put('response_x', response_caseId);                
        }
        
        if (responseName.equals('getCaseIdListResponse1')){
          Rforce_RcArchivage.getCaseIdListAccount req = ((Rforce_RcArchivage.getCaseIdListAccount)request);
          Rforce_RcArchivage.caseId myCase = new Rforce_RcArchivage.caseId();
          // Create a case
          myCase.caseId = '';
          myCase.countryCode = '';
          // Send the response
          Rforce_RcArchivage.getCaseIdListResponse1 response_caseId = new Rforce_RcArchivage.getCaseIdListResponse1();
          response_caseId.caseIdList = new Rforce_RcArchivage.caseId[0];
          response_caseId.caseIdList.add(myCase);
          response.put('response_x', response_caseId);                
        }
        
         if (responseName.equals('getCaseIdListResponse2')){
          Rforce_RcArchivageEnhanced.getCaseIdListAccountConcat req = ((Rforce_RcArchivageEnhanced.getCaseIdListAccountConcat)request);
          Rforce_RcArchivageEnhanced.caseDetailsList myCase = new Rforce_RcArchivageEnhanced.caseDetailsList();
          // Create a case
          myCase.caseId = '';
          myCase.countryCode = '';
          // Send the response
          Rforce_RcArchivageEnhanced.getCaseIdListResponse2 response_caseId = new Rforce_RcArchivageEnhanced.getCaseIdListResponse2();
          response_caseId.caseIdList = new Rforce_RcArchivageEnhanced.caseDetailsList[0];
          response_caseId.caseIdList.add(myCase);
          response.put('response_x', response_caseId);                
        }
        
        
        
        
          if (responseName.equals('getAttachmentContentResponse')){
         /* Rforce_RcArchivageAttachment.getAttachmentContent request_x = new Rforce_RcArchivageAttachment.getAttachmentContent();
          request_x.countryCode = 'FRA';
          request_x.caseId = '1-244507361';
          request_x.attachmentName = 'S_ACTIVITY_ATT_1-41KY13_1-IBAL8.SAF';*/
          Rforce_RcArchivageAttachment.attachmentContentDetails request_x = new Rforce_RcArchivageAttachment.attachmentContentDetails();
          request_x.attachmentContent='Test';
          request_x.extension='Test';
          Rforce_RcArchivageAttachment.getAttachmentContentResponse res = new Rforce_RcArchivageAttachment.getAttachmentContentResponse();
          res.attachmentContentDetails=request_x;
         //  Rforce_RcArchivage.getAttachmentContentResponse response_x ='This is attachment Content';
          //res.attachmentContent='This is attachment Content';
          
         // res.add
        //response_x.attachmentContent=request_x;

          response.put('response_x',res);                
        }
        
         if (responseName.equals('getXmlDetailsResponse')){
          Rforce_RcArchivage.getXmlDetailsResponse res = new Rforce_RcArchivage.getXmlDetailsResponse();
          res.xmlData='Test';
          response.put('response_x',res);                
        }

        if (responseName.equals('getCaseDetailsResponse')){
          // Cases Details request **/
          Rforce_RcArchivage.getCaseDetails caseDetailsReq = ((Rforce_RcArchivage.getCaseDetails)request);
          
          // Create case details       
          Rforce_RcArchivage.caseDetails caseDetails = new Rforce_RcArchivage.caseDetails();
          caseDetails.activityCloseDate = datetime.now();
          caseDetails.accountId = 'test';
          caseDetails.assetId = 'test';
          caseDetails.brandInfoExchangeFlag = 'test';
          caseDetails.buId = 'test';
          caseDetails.build = 'test';
          caseDetails.city = 'test';
          caseDetails.clientAgreement = 'test';
          caseDetails.conflictId = 'test';
          caseDetails.contactCellPhNum = 'test';
          caseDetails.contactHomePhNum = 'test';
          caseDetails.contactId = 'test';
          caseDetails.contactWorkPhNum = 'test';
          caseDetails.country = 'test';
          caseDetails.created = datetime.now();
          caseDetails.createdBy = 'test';
          caseDetails.createdDate = datetime.now();
          caseDetails.descText = 'test';
          caseDetails.detail = 'test';
          caseDetails.firstName = 'test';
          caseDetails.from_x = 'test';
          caseDetails.lastName = 'test';
          caseDetails.lastUpdated = datetime.now();
          caseDetails.lastUpdatedBy = 'test';
          caseDetails.login = 'test';
          caseDetails.modificationNumber = 'test';
          caseDetails.name = 'test';
          caseDetails.negoResult = 'test';
          caseDetails.orgId = 'test';
          caseDetails.purchaseDate = datetime.now();
          caseDetails.purchaseIntention = 'test';
          caseDetails.quoteType = 'test';
          caseDetails.rcName = 'test';
          caseDetails.rcRowId = 'test';
          caseDetails.resAuthor = 'test';
          caseDetails.resDate = datetime.now();
          caseDetails.rowId = 'test';
          caseDetails.serialNum = 'test';
          caseDetails.srArea = 'test';
          caseDetails.srAreaCode = 'test';
          caseDetails.srBrand = 'test';
          caseDetails.srCustNum = 'test';
          caseDetails.srNum = 'test';
          caseDetails.srSubtypeCode = 'test';
          caseDetails.state = 1;
          caseDetails.subArea = 'test';
          caseDetails.traitment = 'test';
          caseDetails.zipCode = 'test';
          

          // Send the response
          Rforce_RcArchivage.getCaseDetailsResponse response_casedetails = new Rforce_RcArchivage.getCaseDetailsResponse();
          response_casedetails.caseDetails = caseDetails;
          response.put('response_x', response_casedetails); 
        }
  
     
   }
}