public with sharing class VFC02_FTSGoodwill {
   
    //variables
    public FTS__c FTS{get;set;}
    public Goodwill__c gudwill{get;set;}
    public FTS__c ftsRec{get;set;}
    List<FTS__c> arryFTS=new List<FTS__c>();
    List<FTS__c> selFTS=new List<FTS__c>();
    List<Case> caseRec=new List<Case>();
    List<Goodwill_Grid_Line__c> gwGridLines=new List<Goodwill_Grid_Line__c>(); 
    ID FTSId;
    boolean display;
    boolean show;
    Decimal TotGenAmt=0.0;
    Date orDate;
    String caseBrand; 
    date OrdateGw;
    Integer VehicleAg;
    Integer km;
    Integer SRCAmt;
    Integer numberDaysDue;

    String RetURL=ApexPages.currentPage().getParameters().get('retURL');
    Id caseIds=System.currentPageReference().getParameters().get('CF00ND0000004qc7b_lkid');
    Id caseId=ApexPages.currentPage().getParameters().get('id');
    Id ftsRecId=ApexPages.currentPage().getParameters().get('ftsIds');
    public String errorMessage {get;set;}
    public String caseCountry;
    public String caseCurrencyIsoCode;
    public VFC02_FTSGoodwill(ApexPages.StandardSetController controller) {
       
        if(caseId!=null){
        
        
       
        //Commented to replace this with onclick Javascript Custom Button  
        // arryFTS = (List<FTS__c>) controller.getSelected();
        selFTS=[select Id,Total_General_Amount__c,Name,ORDate__c from FTS__c where Id =:ftsRecId];
        for(integer i=0;i<selFTS.size();i++){
        try{
            FTS=[select Id,Total_General_Amount__c,Name,ORDate__c from FTS__c where Id=:selFTS[i].Id];
            TotGenAmt=TotGenAmt+FTS.Total_General_Amount__c;
           }
        catch(Exception e){
            System.debug('Error'+e);
        }
        FTSId=FTS.Id;
        orDate=FTS.ORDate__c;
        }
        }   
        if(caseIds!=null){
        caseCountry=[select CountryCase__c from Case where id=:caseIds].CountryCase__c;   
         caseCurrencyIsoCode=[select CurrencyIsoCode from Case where id=:caseIds].CurrencyIsoCode;  
         }
                 
    }


    
    public Decimal getEduGoCount()
    {
      return TotGenAmt;
    }
    
    public boolean getDisplay()
    {
      return display;
    }
    
    public boolean getShow()
    {
      return show;
    }
    
    public PageReference recalculate() {
        return null;
    }
    
    // This method is for initialization on Page Load
    public PageReference initialize() {
        gudwill= new Goodwill__c(); 
        if(caseId!=null){
            display=true;
            gudwill.QuotWarrantyRate__c=TotGenAmt; 
            gudwill.ORDate__c=orDate;
            gudwill.Case__c=caseId;
            gudwill.FTS__c=FTSId;
           
            
        try{
        if(gudwill.ORDate__c!=null){
            CalculateSRC();
        }
        }
        catch(Exception e){
            System.debug('Error***'+e);} 
        }
        else{
            display=false;
            show=true;
            gudwill.Case__c=caseIds;
        }
        if(caseCountry!=null){
            gudwill.Country__c=caseCountry;
            }
            if(caseCurrencyIsoCode!=null){
            gudwill.CurrencyCode__c=caseCurrencyIsoCode;
            }
            return null;
    }
    
    // This method is used to save the record into Technical Goodwill
    public PageReference saveGoodwill()
    {
        PageReference gw=null;
        try{
            if(caseId==null){
            gudwill.Case__c=caseIds;
        }
        else{
            gudwill.Case__c=caseId;
        }
        gudwill.AgreementNbr__c=gudwill.AgreementNbr__c;
        gudwill.GoodwillStatus__c=gudwill.GoodwillStatus__c;
        gudwill.ResolutionCode__c=gudwill.ResolutionCode__c;
        gudwill.Country__c=gudwill.Country__c;
        gudwill.CurrencyCode__c=gudwill.CurrencyCode__c;
        gudwill.Description__c=gudwill.Description__c;
        gudwill.ORDate__c=gudwill.ORDate__c;
        gudwill.Organ__c=gudwill.Organ__c;
        gudwill.QuotWarrantyRate__c=gudwill.QuotWarrantyRate__c;
        gudwill.ICMPartRate__c=gudwill.ICMPartRate__c;
        gudwill.QuotClientRate__c=gudwill.QuotClientRate__c;
        gudwill.DealerPartRate__c=gudwill.DealerPartRate__c;
        gudwill.SRCPartRate__c=gudwill.SRCPartRate__c;
        gudwill.SRCDecidedRate__c=gudwill.SRCDecidedRate__c;
        gudwill.DeviationReason__c=gudwill.DeviationReason__c;
        gudwill.DeviationReasDesc__c=gudwill.DeviationReasDesc__c;
        gudwill.DealerName__c=gudwill.DealerName__c;
        gudwill.BankName__c=gudwill.BankName__c;
        gudwill.CreditCode__c=gudwill.CreditCode__c;
        
        if(gudwill.ResolutionCode__c ==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Resolution Code'));
            return null;
        }
        if(gudwill.ExpenseCode__c ==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Expense Code'));
            return null;
        }
        
        if(gudwill.BudgetCode__c ==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the Budget Code'));
            return null;
        } 
            gudwill.BudgetCode__c=gudwill.BudgetCode__c;
            gudwill.ExpenseCode__c=gudwill.ExpenseCode__c;
            upsert gudwill;
        }
        catch(Exception e){
            System.debug('Error'+e);
        }
        try{
            if(caseId!=null){
            FTS.Id=FTSId;
            FTS.Goodwill__c=gudwill.Id;
            if(gudwill.GoodwillStatus__c=='Approved'){
            if(gudwill.SRCDecidedRate__c!=null){
                FTS.SRC_Participation_Amount__c=(gudwill.SRCDecidedRate__c * gudwill.QuotWarrantyRate__c/100);
            }
            else{
                FTS.SRC_Participation_Amount__c=(gudwill.SRCPartRate__c * gudwill.QuotWarrantyRate__c);
            }
            }
            else{
                FTS.SRC_Participation_Amount__c=0.0;
            }
             upsert FTS;
            }
        }
        catch(Exception e){
            System.debug('Error'+e);
        } 
        return gw= new PageReference('/'+gudwill.Id);
    }
    
    // This method is used for Goodwill Calculation(SRC)
    public void CalculateSRC(){
    if(caseIds!=null){
        caseRec=[select CaseBrand__c,DeliveryDate__c,Kilometer__c from Case where Id=:caseIds];
    }
    else if(caseId!=null){
        caseRec=[select CaseBrand__c,DeliveryDate__c,Kilometer__c from Case where Id=:caseId];
    }
    if(caseBrand==null)
    {
    caseBrand='Renault';
    }
    else{
        caseBrand=caseRec[0].CaseBrand__c;
    }
        OrdateGw=caseRec[0].DeliveryDate__c;
        if((gudwill.ORDate__c!=null) & (caseRec[0].DeliveryDate__c!=null)){
        Date myDate = date.valueOf(gudwill.ORDate__c);
        Date myDate1 = date.valueOf(caseRec[0].DeliveryDate__c);
        numberDaysDue =myDate1.daysBetween(myDate);
    }
    else{
        numberDaysDue=0; 
    }
    VehicleAg=Integer.valueOf(numberDaysDue/30.5);
    km=Integer.valueOf(caseRec[0].Kilometer__c);
    
    System.debug('test'+gudwill.Country__c);
     System.debug('VehicleAg'+VehicleAg+caseBrand+km+'gw'+gudwill);
     
    if(gudwill.Country__c==null ||caseBrand==null || VehicleAg==null || km==null) {
    SRCAmt=0;
    }
    else{
            gwGridLines = [Select Age_interval__c, Max_Age__c, Mileage_Interval__c, Max_Mileage__c, Row_Number__c, f1__c, f2__c, f3__c, f4__c, f5__c, f6__c, f7__c, f8__c, f9__c, f10__c, f11__c, f12__c, f13__c, f14__c, f15__c, f16__c, f17__c, f18__c, f19__c, f20__c, f21__c, f22__c, f23__c, f24__c, f25__c, f26__c, f27__c, f28__c, f29__c, f30__c, f31__c, f32__c, f33__c, f34__c, f35__c, f36__c, f37__c, f38__c, f39__c, f40__c, f41__c, f42__c, f43__c, f44__c, f45__c, f46__c, f47__c, f48__c, f49__c, f50__c, f51__c, f52__c, f53__c, f54__c, f55__c, f56__c, f57__c, f58__c, f59__c, f60__c, f61__c from Goodwill_Grid_Line__c where Country__c = :gudwill.Country__c AND VehicleBrand__c = :caseBrand AND Organ__c = :gudwill.Organ__c order by Row_Number__c];
            if (gwGridLines.size() == 0 || VehicleAg > gwGridLines.get(0).Max_Age__c || km > gwGridLines.get(0).Max_Mileage__c)
            { 
                SRCAmt= 0;
            }
            else
            {
                Integer mileageLine = Integer.valueOf(km / gwGridLines.get(0).Mileage_Interval__c)+1;
                Integer ageColumn = Integer.valueOf(VehicleAg / gwGridLines.get(0).Age_interval__c)+1;
                SRCAmt= 0;
            for (Goodwill_Grid_Line__c gw : gwGridLines)
            { 
                if (gw.Row_Number__c == mileageLine) 
                SRCAmt= Integer.valueOf(gw.get('f'+ageColumn+'__c')); 
            }
            }
       }
        gudwill.SRCPartRate__c=SRCAmt;
        
     }
     public void getGoodwillDetials(){
         String recTpeId;
         String AccCountry;
         Boolean IsBrazilCountry;
         Integer gwdAmount;
     }
     public void getCaseDetials(){
         String casrecTpeId;
         String casCountry;
         Boolean IsBrazilCountry;
     }     
    }