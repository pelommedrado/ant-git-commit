public class AgendaServicoController {
    
    public Datetime data { get;set; }
    public Account acc { get;set; }
    public List<Contact> contactList { get; set; }
    public List<ItemTab> rows { get;set; }
    public String horario { get;set; }
    public String semana { get;set;}
    
    public PageReference init() {
       Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        Id AccID  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        this.acc =  [
            SELECT Id, Name
            FROM Account
            WHERE Id =: AccID
        ];
        
        this.contactList = [
            SELECT Id, Name
            FROM Contact
            WHERE AccountId =: acc.Id AND Available_to_Service__c = 'Available'
            ORDER BY Name
        ];
        
        System.debug('contactList: ' + contactList);
        
        this.data = Datetime.now();
        
        Integer minMax = 105;
        Integer intervalo = 15;
        Integer minMin = -30;
        
        DateTime ini = data.addMinutes(minMin-intervalo);
        DateTime fim = data.addMinutes(minMax);
        
        List<Event> eventList = [
            SELECT Id, StartDateTime, Who.Name, Subject, What.Name
            FROM Event
            WHERE WhoId in: contactList AND StartDateTime >=:ini AND StartDateTime <=:fim
            ORDER BY StartDateTime
        ];
        
        List<String> rageHoraList = new List<String>();
        for(Integer i = minMin;i < minMax; i +=15 ) {
            DateTime d1 = data.addMinutes(i);
            String s1 = d1.format('HH:') + tratarMin(d1);
            rageHoraList.add(s1);
        }
        
        this.rows = new List<ItemTab>();
        Map<String, ItemTab> mapEvent = new Map<String, ItemTab>();
        Integer at = 0; 
        for(String h: rageHoraList) {
            at++;
            ItemTab temp = new ItemTab(); 
            temp.hora = h;
            
            if(at == 3) {
                temp.atual = true;
            }            
            
            for(Contact c: contactList) {
                ItemEvent iv = new ItemEvent();
                iv.tecnico = c.Name;
                
                temp.ev.add(iv);                
            }
            
            mapEvent.put(h, temp);
            this.rows.add(temp);
        }
        
        for(Event e : eventList) {
            //String key = e.StartDateTime.format('HH:mm');
            String key = e.StartDateTime.format('HH:') + tratarMin(e.StartDateTime);
            ItemTab iT = mapEvent.get(key);
            
            if(iT != null) {
                for(ItemEvent iv : iT.ev) {
                    if(iv.tecnico.equals(e.Who.Name)) {
                        iv.assunto = e.Subject;
                        iv.cliente = e.What.Name;
                    }
                }    
            }
        }
        
        System.debug('Lista contato:' + contactList);
        System.debug('Data local:' + data);
        System.debug('Data inicio:' + ini);
        System.debug('Data fim:' + fim);
        System.debug('Eventos:' + eventList);
        
        Map<String, String> week = new Map<String, String>();
        week.put('Sun', 'Domingo');
        week.put('Mon', 'Segunda');
        week.put('Tue', 'Terça');
        week.put('Wed', 'Quarta');
        week.put('Thu', 'Quinta');
        week.put('Fri', 'Sexta');
        week.put('Sat', 'Sábado');
        
        TimeZone tz = UserInfo.getTimeZone();
        this.semana 	= week.get(data.format('E'));
        this.horario = data.format('HH:mm', tz.getID());
        
        return null;
    }
    
    private String tratarMin(Datetime hora) {
        String s1 = hora.format('mm');
        Integer i1 = (Integer) Integer.valueOf(s1)/15;
        
        return (i1 == 0) ? '00' : String.valueOf(i1 * 15);
    }
    
    public class ItemTab {
        public Boolean atual { get;set; }
        public String hora { get; set; }
        public List<ItemEvent> ev { get; set; }
        public ItemTab() {
            ev = new List<ItemEvent>();
            atual = false;
        }
    }
    
    public class ItemEvent {
        public String tecnico { get; set; }
        public String cliente { get; set; }
        public String assunto { get; set; }
    }
}