Public class Rforce_Utils_Stubs {

    // Order Stub
    public static Rforce_BVM_WS.ApvGetDetVehXmlResponse VehicleStub() {
     
        Rforce_BVM_WS.ApvGetDetVehXmlResponse DummyResponse = new Rforce_BVM_WS.ApvGetDetVehXmlResponse();
        Rforce_BVM_WS.DetVehInfoMsg DummyVehInfoMsg = new Rforce_BVM_WS.DetVehInfoMsg();
        DummyVehInfoMsg.codeRetour = '0000';
        DummyVehInfoMsg.msgRetour = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        
        Rforce_BVM_WS.DetVeh DummyDetVeh = new Rforce_BVM_WS.DetVeh();
        DummyDetVeh.dateLiv = '22.07.2005';
        DummyDetVeh.dateTcmFab = '25.01.2005';
        DummyDetVeh.libConst='RENAULT';
        DummyDetVeh.libModel= 'LAGUNA II';
        DummyDetVeh.version='IP2 19DCI+';
        DummyDetVeh.libCarrosserie='BERLINE 5PRTES';
        DummyDetVeh.typeMot='F9Q';
        DummyDetVeh.NMot='C058210';
        DummyDetVeh.typeBoi='PK6';
        DummyDetVeh.NBoi='C012125';
        DummyDetVeh.indBoi='068';
        DummyDetVeh.NFab='S714313';
        DummyDetVeh.tvv='BGRG06';
        
                      
        DummyResponse.detVehInfoMsg = DummyVehInfoMsg;
        DummyResponse.detVeh = DummyDetVeh;         
        return DummyResponse;
    }    
    
    // Warranty Stub
    public static Rforce_servicesBcsDfr.ListDetWarrantyCheck WarrantyStub() {
    
        Rforce_servicesBcsDfr.ListDetWarrantyCheck DummyResponse = new Rforce_servicesBcsDfr.ListDetWarrantyCheck();
        Rforce_servicesBcsDfr.ArrayOfErrMsg DummyInfoMsg = new Rforce_servicesBcsDfr.ArrayOfErrMsg();
        Rforce_servicesBcsDfr.ErrMsg DummyMsg = new Rforce_servicesBcsDfr.ErrMsg();
        
   //     DummyMsg.typeMsg = '0000';
   //     DummyMsg.zoneMsg = 'RECHERCHE VALIDE, VEHICULE TROUVE';
   //     DummyInfoMsg.ErrMsg.add(DummyMsg); 
        
        Rforce_servicesBcsDfr.ListDetWarrantyCheck DummyDet = new Rforce_servicesBcsDfr.ListDetWarrantyCheck();
        
        // Assistance warranty;
        Rforce_servicesBcsDfr.ArrayOfAssistanceWarranty assArray =  new Rforce_servicesBcsDfr.ArrayOfAssistanceWarranty();
        Rforce_servicesBcsDfr.AssistanceWarranty[] ass =  new Rforce_servicesBcsDfr.AssistanceWarranty[]{};
        Rforce_servicesBcsDfr.AssistanceWarranty oneass =  new Rforce_servicesBcsDfr.AssistanceWarranty();
        
        oneass.datFinGarASS = '12.01.6001';
        oneass.kmMaxGarASS = '98749';
        ass.add(oneass);
        assArray.AssistanceWarranty = ass;
        DummyDet.listAssWarranty = assArray;
        
        // BatteryWarranty ;
        Rforce_servicesBcsDfr.ArrayOfBatteryWarranty bttArray =  new Rforce_servicesBcsDfr.ArrayOfBatteryWarranty();
        Rforce_servicesBcsDfr.BatteryWarranty[] btt =  new Rforce_servicesBcsDfr.BatteryWarranty[]{};
        Rforce_servicesBcsDfr.BatteryWarranty onebtt =  new Rforce_servicesBcsDfr.BatteryWarranty();
        
        onebtt.datFinGarBTT = '12.01.3501';
        onebtt.kmMaxGarBTT = '97659';
        btt.add(onebtt);
        bttArray.BatteryWarranty = btt;
        DummyDet.listBttWarranty = bttArray;
        
        // CorrosionWarranty ;
        Rforce_servicesBcsDfr.ArrayOfCorrosionWarranty corArray =  new Rforce_servicesBcsDfr.ArrayOfCorrosionWarranty();
        Rforce_servicesBcsDfr.CorrosionWarranty[] cor =  new Rforce_servicesBcsDfr.CorrosionWarranty[]{};
        Rforce_servicesBcsDfr.CorrosionWarranty onecor =  new Rforce_servicesBcsDfr.CorrosionWarranty();
        
        onecor.datFinGarCOR = '12.01.3441';
        onecor.kmMaxGarCOR = '23999';
        cor.add(onecor);
        corArray.CorrosionWarranty = cor;
        DummyDet.listCorWarranty = corArray;
        
// ManufacturerWarranty ;
        Rforce_servicesBcsDfr.ArrayOfManufacturerWarranty manArray =  new Rforce_servicesBcsDfr.ArrayOfManufacturerWarranty();
        Rforce_servicesBcsDfr.ManufacturerWarranty[] man =  new Rforce_servicesBcsDfr.ManufacturerWarranty[]{};
        Rforce_servicesBcsDfr.ManufacturerWarranty oneman =  new Rforce_servicesBcsDfr.ManufacturerWarranty();
        
        oneman.datFinGarGAR = '12.01.3301';
        oneman.kmMaxGarGAR = '65999';
        man.add(oneman);
        manArray.ManufacturerWarranty = man;
        DummyDet.listGarWarranty = manArray;
                
// GME ;
        Rforce_servicesBcsDfr.ArrayOfGMPeWarranty gmeArray =  new Rforce_servicesBcsDfr.ArrayOfGMPeWarranty();
        Rforce_servicesBcsDfr.GMPeWarranty[] gme =  new Rforce_servicesBcsDfr.GMPeWarranty[]{};
        Rforce_servicesBcsDfr.GMPeWarranty onegme =  new Rforce_servicesBcsDfr.GMPeWarranty();
        
        onegme.datFinGarGME = '12.01.3201';
        onegme.kmMaxGarGME = '45999';
        gme.add(onegme);
        gmeArray.GMPeWarranty = gme;
        DummyDet.listGmeWarranty = gmeArray;
        
// PaintingWarranty ;
        Rforce_servicesBcsDfr.ArrayOfPaintingWarranty turArray =  new Rforce_servicesBcsDfr.ArrayOfPaintingWarranty();
        Rforce_servicesBcsDfr.PaintingWarranty[] tur =  new Rforce_servicesBcsDfr.PaintingWarranty[]{};
        Rforce_servicesBcsDfr.PaintingWarranty onetur =  new Rforce_servicesBcsDfr.PaintingWarranty();
        
        onetur.datFinGarTUR = '12.01.2101';
        onetur.kmMaxGarTUR = '99999';
        tur.add(onetur);
        turArray.PaintingWarranty = tur;
        DummyDet.listTurWarranty = turArray;

// CorrosionWarranty ;
        onecor =  new Rforce_servicesBcsDfr.CorrosionWarranty();
        
        onecor.datFinGarCOR = '12.01.3441';
        onecor.kmMaxGarCOR = '23999';
        cor.add(onecor);
        corArray.CorrosionWarranty = cor;
        DummyDet.listCorWarranty = corArray;
                              
//        DummyResponse.GetInfoWarrantyCheckReturn.listErrMsg = DummyInfoMsg;
        DummyResponse  = DummyDet;    
        return DummyResponse;
    }    
    
    // OTS Stub
    public static Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse OTSStub() {
    
        Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse DummyResponse = new Rforce_otsIcmApvBserviceRenault.ApvGetListOtsResponse();
        Rforce_otsIcmApvBserviceRenault.OtsInfoMsg[] DummyArrayOtsInfoMsg = new Rforce_otsIcmApvBserviceRenault.OtsInfoMsg[]{};
        Rforce_otsIcmApvBserviceRenault.OtsInfoMsg DummyOtsInfoMsg = new Rforce_otsIcmApvBserviceRenault.OtsInfoMsg();
        DummyOtsInfoMsg.code = '0000';
        DummyOtsInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummyArrayOtsInfoMsg.Add(DummyOtsInfoMsg);

        Rforce_otsIcmApvBserviceRenault.Ots[] DummyArrayOtsDet = new Rforce_otsIcmApvBserviceRenault.Ots[]{};
        Rforce_otsIcmApvBserviceRenault.Ots DummyOtsDet = new Rforce_otsIcmApvBserviceRenault.Ots();
        DummyOtsDet.libOts = 'Pose d\'une attache sur le dispositif de verrouillage du siege conducteur.';
        DummyOtsDet.numOts = '0B5X';
        DummyOtsDet.descOts='RENAULT a identifie un risque de non-verrouillage permanent ou par intermittence du siege conducteur, ce qui est susceptible de mettre en cause votre securite. L\'intervention consiste a poser une attache sur le dispositif de verrouillage du siege concerne.';
        DummyOtsDet.numNoteTech = '4818D';
        DummyArrayOtsDet.Add(DummyOtsDet);
        
        DummyOtsDet = new Rforce_otsIcmApvBserviceRenault.Ots();
        DummyOtsDet.libOts = 'A second OTS because this can be a list';
        DummyOtsDet.numOts = '0B5X1';
        DummyOtsDet.descOts='RENAULT a identifie un risque de non-verrouillage permanent ou par intermittence du siege conducteur, ce qui est susceptible de mettre en cause votre securite. L\'intervention consiste a poser une attache sur le dispositif de verrouillage du siege concerne.';
        DummyOtsDet.numNoteTech = '1234D';
        DummyArrayOtsDet.Add(DummyOtsDet);
                      
        DummyResponse.apvGetListOtsMsg = DummyArrayOtsInfoMsg;
        DummyResponse.apvGetListOts = DummyArrayOtsDet;         
        return DummyResponse;
    }    
    
     // Maintenance Stub
    public static Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse MaintStub() {
    
        Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse DummyResponse = new Rforce_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse();
        Rforce_fullRepApvBserviceRenault.InfoMsg DummyInfoMsg = new Rforce_fullRepApvBserviceRenault.InfoMsg();
        DummyInfoMsg.codeMsg = '0000';
        DummyInfoMsg.libelleMsg = 'RECHERCHE VALIDE, VEHICULE TROUVE';
         
        Rforce_fullRepApvBserviceRenault.PgmEnt DummyDet = new Rforce_fullRepApvBserviceRenault.PgmEnt();
        DummyDet.libFix = new Rforce_fullRepApvBserviceRenault.PgmEntLibFix[]{};
        
        Rforce_fullRepApvBserviceRenault.PgmEntLibFix lbl = new Rforce_fullRepApvBserviceRenault.PgmEntLibFix();
        Rforce_fullRepApvBserviceRenault.PgmEntLibFix lbl2 = new Rforce_fullRepApvBserviceRenault.PgmEntLibFix();
        Rforce_fullRepApvBserviceRenault.PgmEntLibFix lbl3 = new Rforce_fullRepApvBserviceRenault.PgmEntLibFix();
        Rforce_fullRepApvBserviceRenault.PgmEntLibFix lbl4 = new Rforce_fullRepApvBserviceRenault.PgmEntLibFix();
        
        lbl.numLib = '26';
        lbl.libelleFix = 'Le respect du programme d\'entretien, des périodicités et normes des produits et ingrédients est obligatoire pendant la période de garantie et s\'applique au pays de vente de votre véhicule. En dehors du pays d\'origine, veuillez consultez un représentant de la marque.';
        DummyDet.libFix.add(lbl);
        lbl2.numLib = '27';
        lbl2.libelleFix = 'OPERATIONS A REALISER AU PREMIER DES 2 TERMES ATTEINTS :';
        DummyDet.libFix.add(lbl2);
        lbl3.numLib = '28';
        lbl3.libelleFix = 'Les opérations suivantes doivent être effectuées pendant le service:';
        DummyDet.libFix.add(lbl3);
        lbl4.numLib = '43';
        lbl4.libelleFix = 'Important: Le fabricant recommande d\'adapter la fréquence de remplacement de certaines parties ou de liquides qui sont touchés par les conditions particulières d\'utilisation. Veuillez vous référer à la feuille jointe pour plus de détails';
        DummyDet.libFix.add(lbl4);
        
      
        Rforce_fullRepApvBserviceRenault.PgmEntSecondary[] seclabels = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary[]{};
        
        Rforce_fullRepApvBserviceRenault.PgmEntSecondary se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Anti-corrosion check';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the signalling and the exterior/interior lighting';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check and lubricate the bonnet lock';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition of the windscreen and door mirrors';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the exhaust pipe';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the wipers blades and screen washer fluid levels';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the levels, condition and sealing of the cooling circuit';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the presence of the airbag and engine compartment labels';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the levels, condition and sealing of the brake/clutch circuit';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the 12 V battery with the test tool';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition and sealing of the gaiters/rubber mountings/ball joints/shock absorbers';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the computers with the diagnostic tool';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the condition and pressure of the tyres and road wheel security';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the operation of the instrument panel warning lights';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the presence of the wheel valve caps';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'reinitialise the maintenance/oil service display';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'Check the wear of the brake discs and paps';
        seclabels.add(se);
        
        se = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        se.labelSecondary = 'documentation and positioning of the maintenance label';
        seclabels.add(se);     
        
    
        DummyDet.secondary = seclabels;
            
        //for (Integer s=0;s<10;s++){
        //    Rforce_fullRepApvBserviceRenault.PgmEntSecondary seclabel = new Rforce_fullRepApvBserviceRenault.PgmEntSecondary();
        //    seclabel.labelSecondary = 'Operation to be performed ' + s;
        //    seclabels.add(seclabel);
        //}
             
    
        Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary[] primaryOperations = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary[]{};
        
        Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Renault Service';        
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Drain the engine oil';
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the cabin filter';
        op.period_Distan_Toutes = '1';
        op.period_Km_Toutes = '20000';
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the oil filter';
        op.period_Km_Toutes = '20000';
        op.period_Distan_Toutes = '1';
        
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the brake fluid';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '4';
        
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the coolant';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '4';
        
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the accessories belt and rollers';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '6';
        
        primaryOperations.add(op);
        
        op = new Rforce_fullRepApvBserviceRenault.PgmEntMereAndPrimary();
        op.libelle = 'Replace the timing belt and rollers';
        op.period_Km_Toutes = '120000';
        op.period_Distan_Toutes = '6';
        
        primaryOperations.add(op);
        
        
        
        DummyDet.merePrimary = primaryOperations;
        
        DummyResponse.InfoMsg = DummyInfoMsg;
        DummyResponse.PgmEnt = DummyDet;         
        return DummyResponse;
        
        
    }    
    
    // WarrantyHistory Stub
    public static Rforce_ApvGetDonIran1.ApvGetDonIran1Response WarrantyHistoryStub() {
    
        Rforce_ApvGetDonIran1.ApvGetDonIran1Response DummyResponse = new Rforce_ApvGetDonIran1.ApvGetDonIran1Response();
        Rforce_ApvGetDonIran1.DonIran1 DummydonIran1 = new Rforce_ApvGetDonIran1.DonIran1();
        
        Rforce_ApvGetDonIran1.DonIran1InfoMsg[] DummylistDonIran1InfoMsg = new Rforce_ApvGetDonIran1.DonIran1InfoMsg[]{};
        Rforce_ApvGetDonIran1.DonIran1InfoMsg DummyInfoMsg = new Rforce_ApvGetDonIran1.DonIran1InfoMsg ();
        
        DummyInfoMsg.code = '0000';
        DummyInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummylistDonIran1InfoMsg.Add(DummyInfoMsg);

        Rforce_ApvGetDonIran1.DonIran1Sortie[] DummyArraySortie = new Rforce_ApvGetDonIran1.DonIran1Sortie[]{};
        Rforce_ApvGetDonIran1.DonIran1Sortie DummyDet = new Rforce_ApvGetDonIran1.DonIran1Sortie();
        
        
        DummyDet.catInt= 'R';
        DummyDet.datOuvOr= '07.09.2010';
        DummyDet.km= '55839';
        DummyDet.libInt= 'CLIMATISATION,YC. TABLEAU DE COMMANDE';
        DummyDet.libRc= 'BLOQUE,COMM.SANS EFFET,N\'ENCLENCHE,NE CHARGE PAS';
        DummyDet.numInt= '008131727301';
        DummyDet.numOts= 'H';
        DummyDet.rc= '7P';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '0';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '100';
        DummyDet.outModulo= '201005';
          
        DummyArraySortie.Add(DummyDet);
        DummyDet = new Rforce_ApvGetDonIran1.DonIran1Sortie();
        
        DummyDet.catInt= 'A';
        DummyDet.datOuvOr= '06.09.2010';
        DummyDet.km= '56502';
        DummyDet.libInt= 'BOITE DE VITESSE TOUT TYPE';
        DummyDet.libRc= 'BLOQUE,COMM.SANS EFFET,N\'ENCLENCHE,NE CHARGE PAS';
        DummyDet.numInt= '008169731901';
        DummyDet.numOts= 'Y';
        DummyDet.rc= '7P';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '50';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '50';
        DummyDet.outModulo= '201006';
        DummyArraySortie.Add(DummyDet);
        
        DummyArraySortie.Add(DummyDet);
        DummyDet = new Rforce_ApvGetDonIran1.DonIran1Sortie();
        
        DummyDet.catInt= 'A';
        DummyDet.datOuvOr= '13.11.2009';
        DummyDet.km= '39968';
        DummyDet.libInt= 'DIRECTION, COLONNE, BOITIER, ASSISTANCE';
        DummyDet.libRc= '';
        DummyDet.numInt= '007721893901';
        DummyDet.numOts= '0B1E';
        DummyDet.rc= '';
        DummyDet.txPec1Entretien= '0';
        DummyDet.txPec1Incident= '100';
        DummyDet.txPec2= '0';
        DummyDet.txPec3= '0';
        DummyDet.txPec4= '0';
        DummyDet.outModulo= '201007';
        DummyArraySortie.Add(DummyDet);
        
        
        DummydonIran1.sortie = DummyArraySortie;
                      
        DummyResponse.listDonIran1InfoMsg= DummylistDonIran1InfoMsg;
        DummyResponse.donIran1 = DummydonIran1;         
        return DummyResponse;
    }    
    
      // Contract Stub
    public static Rforce_CustdataCrmBserviceRenault.GetCustDataResponse ContractStub() {
    
        Rforce_CustdataCrmBserviceRenault.GetCustDataResponse DummyResponse = new Rforce_CustdataCrmBserviceRenault.GetCustDataResponse();
        // Rforce_CustdataCrmBserviceRenault.OtsInfoMsg[] DummyArrayInfoMsg = new Rforce_CustdataCrmBserviceRenault.InfoMsg[]{};
        // Rforce_CustdataCrmBserviceRenault.OtsInfoMsg DummyInfoMsg = new Rforce_CustdataCrmBserviceRenault.InfoMsg();
        
        DummyResponse.responseCode = '0000';
        
        
      //  Contract[]->Vehincle[]->PersonalInformation[]->Response
        Rforce_CustdataCrmBserviceRenault.Contract DummyDet = new Rforce_CustdataCrmBserviceRenault.Contract();
        
        DummyDet.idContrat = '09911249308';
//        DummyDet.type_x = '2321321321';
        DummyDet.productLabel = '43/12 - GARANTIE OR 12 MOIS';
//        DummyDet.initKm = '24,060';
        DummyDet.maxSubsKm = '524,000';
    //    DummyDet.subsDate = '2321321321';
 //       DummyDet.initContractDate = '06.03.2012';
  //      DummyDet.endContractDate = '05.03.2013';
        DummyDet.status = 'ACTIF';
        //DummyDet.updDate = '2321321321';
    //    DummyDet.idMandator = '2321321321';
        DummyDet.idCard = '8250992901649641';
        Rforce_CustdataCrmBserviceRenault.Contract[] contractList = new Rforce_CustdataCrmBserviceRenault.Contract[]{};
        contractList.Add(DummyDet);
        
        // Simulate 2 contracts being returned
        DummyDet = new Rforce_CustdataCrmBserviceRenault.Contract();
        DummyDet.idContrat = '065656565';
//        DummyDet.type_x = '2321321321';
        DummyDet.productLabel = 'Some product ';
//        DummyDet.initKm = '24,060';
        DummyDet.maxSubsKm = '10,524,000';
    //    DummyDet.subsDate = '2321321321';
 //       DummyDet.initContractDate = '06.03.2012';
  //      DummyDet.endContractDate = '05.03.2013';
        DummyDet.status = 'PAS ACTIF';
        //DummyDet.updDate = '2321321321';
    //    DummyDet.idMandator = '2321321321';
        DummyDet.idCard = '54654654654';
        
        contractList.Add(DummyDet);
        
        Rforce_CustdataCrmBserviceRenault.Vehicle vehicle = new Rforce_CustdataCrmBserviceRenault.Vehicle();
        vehicle.contractList = contractList;
        Rforce_CustdataCrmBserviceRenault.Vehicle[] vehicleList = new Rforce_CustdataCrmBserviceRenault.Vehicle[]{};
        vehicleList.add(vehicle);
        Rforce_CustdataCrmBserviceRenault.PersonnalInformation pi = new Rforce_CustdataCrmBserviceRenault.PersonnalInformation();
        pi.firstName = 'LUC';
        pi.lastName = 'BUCHETON';
        Rforce_CustdataCrmBserviceRenault.Address address = new Rforce_CustdataCrmBserviceRenault.Address();
        address.strNum = '58';
        address.strName = 'Avenue GAILARDIN';
        address.city = 'MONTARGIS';
        address.zip = '45200';
        address.countryCode = 'FRA';
        pi.address = address;
        pi.vehicleList = vehicleList;
        Rforce_CustdataCrmBserviceRenault.PersonnalInformation[] clientList = new Rforce_CustdataCrmBserviceRenault.PersonnalInformation[]{};
        clientList.add(pi);
        DummyResponse.clientList = clientList;
        
                      
//        DummyResponse.apvGetListOtsMsg = DummyArrayOtsInfoMsg;
   //     DummyResponse.contractList = DummyContractList;         
        return DummyResponse;
    }    
    
    // WarrantyHistoryDetail Stub
    public static Rforce_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response WarrantyHistoryDetailStub() {
    
        Rforce_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response DummyResponse = new Rforce_iran2BimIcmApvBserviceRenault.ApvGetDonIran2Response();
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2 DummydonIran2 = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2();
        
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg[] DummylistDonIran2InfoMsg = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg[]{};
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg DummyInfoMsg = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2InfoMsg ();
        
        DummyInfoMsg.code = '0000';
        DummyInfoMsg.text = 'RECHERCHE VALIDE, VEHICULE TROUVE';
        DummylistDonIran2InfoMsg.Add(DummyInfoMsg);

        //Rforce_ApvGetDonIran1.DonIran2[] DummyArraySortie = new Rforce_ApvGetDonIran1.DonIran1Sortie[]{};
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2 DummyDet = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2();
        
        
        DummyDet.constClient = 'R';
        DummyDet.diag = '07.09.2010';
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2Pieces[] pieces = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2Pieces[]{};
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2Pieces piece = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2Pieces();
        piece.qtePi = '1';
        piece.libePi = 'CONTROLE TECHNI';
        piece.refePidms = 'CT';
        pieces.add(piece);
        
        piece = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2Pieces();
        piece.qtePi = '3';
        piece.libePi = 'CONTROLE TECHNI STUFF';
        piece.refePidms = 'CT43234';
        pieces.add(piece);
        
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre[] mainOeuvres = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre[]{};
        Rforce_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre mainOeuvre = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre();
        mainOeuvre.libOpecode = 'CLIMATISATION,YC. TABLEAU DE COMMANDE';
        mainOeuvre.opecode = '5242';
        mainOeuvre.tpsMo = '0.3';
          
        mainOeuvres.add(mainOeuvre);
        mainOeuvre = new Rforce_iran2BimIcmApvBserviceRenault.DonIran2MainOeuvre();
        mainOeuvre.libOpecode = 'CLIMATISATION,YC. blah TABLEAU DE blah COMMANDE ';
        mainOeuvre.opecode = '9877';
        mainOeuvre.tpsMo = '0.9';  
        mainOeuvres.add(mainOeuvre);
        
        DummyDet.pieces = pieces;
        DummyDet.mainOeuvre = mainOeuvres;
        
        DummyResponse.listDonIran2InfoMsg= DummylistDonIran2InfoMsg;
        DummyResponse.donIran2 = DummyDet;         
        return DummyResponse;
    }    
    
}