@isTest
public class VFC120_CampaignServiceTest {
    
    public static testMethod void myTest(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Lead lead = moc.CriaLead();
        insert lead;
        
        Contact contato = moc.criaContato();
        insert contato;
        
        Account dealer = moc.criaAccountDealer();
        insert dealer;
        
        Campaign campanha = moc.criaCampanha();
        campanha.Dealer__c = dealer.Id;
        insert campanha;
        
        system.debug('$$$'+campanha);
        
        VFC120_CampaignService.fetchCampaignList('Dealer', lead.Id, contato.Id, dealer.Id);
        VFC120_CampaignService.fetchCampaignList('PA', lead.Id, contato.Id, dealer.Id);
        VFC120_CampaignService.fetchCampaignList('PA', null, contato.Id, dealer.Id);
        VFC120_CampaignService.addOrUpdateLeadToCampaignMember('PA', campanha.Id, lead.Id, Contato.Id);
        VFC120_CampaignService.addOrUpdateLeadToCampaignMember('Dealer', campanha.Id, null, Contato.Id);
        
    }

}