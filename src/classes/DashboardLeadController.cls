public class DashboardLeadController extends AtividadeAbstract {
	public Lead l 			{ get;set; }
	public ActivityMap act  { get;set; }
    
    public String activityDate 	{ get;set; }
    public String activityHour 	{ get;set; }
    
    public Boolean disableMotivo   { get;set; }
    public Boolean disablePrevisao { get;set; }
    public Boolean disableHora	   { get;set; }
    
    public String motivo { get;set; }
    public List<SelectOption> motivosList { get;set; }
    
    public String statusLead { get;set; }
    public List<SelectOption> statusLeadList { 
        get {
            List<SelectOption> selectList = new List<SelectOption>();
            selectList.add(new SelectOption('qualificado', 'Qualificado'));
            selectList.add(new SelectOption('nqualificado', 'Não Qualificado'));
            selectList.add(new SelectOption('ncontato', 'Não Conseguiu Contato'));
            return selectList;
        }
    }
    
    public List<SelectOption> naoQualiList {
        get {
            List<SelectOption> selectList = new List<SelectOption>();
            selectList.add(new SelectOption('Interest in product competition', 'Interesse em Produto da Concorrente'));
            selectList.add(new SelectOption('Already it has vehicle', 'Já Possui Veículo'));
            selectList.add(new SelectOption('Not Interested in the brand', 'Sem Interesse na Marca'));
            selectList.add(new SelectOption('Not in purchase time', 'Não Está em Momento de Compra'));
            selectList.add(new SelectOption('Customer dissatisfied with Renault', 'Cliente Insatisfeito com a Renault'));
            selectList.add(new SelectOption('Customer dissatisfied with Dealer', 'Cliente Insatisfeito com a Concessionária'));
            selectList.add(new SelectOption('Customer recently contacted', 'Cliente Contatado Recentemente'));
            return selectList; 
        }
    }
    
    public List<SelectOption> naoContatoList {
        get {
            List<SelectOption> selectList = new List<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = TST_Transaction__c.WhyNotContact__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple) {
                selectList.add(new SelectOption(f.getValue(), f.getLabel()));
            }
            return selectList; 
        }
    }
    
    public void changeStatus() {
        this.disableMotivo 		= true;
        this.disablePrevisao 	= true;
        this.disableHora 		= true;
        this.act.activityDate 	= activityDate;
        this.act.activityHour 	= activityHour;
        
        if(String.isEmpty(statusLead)) return;
        
        if(statusLead.equalsIgnoreCase('nqualificado')) {
            this.motivosList = naoQualiList;
            this.motivo = '';
            this.disableMotivo = false;
            this.disablePrevisao = false;
            
        } else if(statusLead.equalsIgnoreCase('ncontato')) {
            this.motivosList = naoContatoList;
            this.motivo = '';
            this.disableMotivo = false;
            this.disableHora = false;
            this.act.activityDate = null;
            this.act.activityHour = null;
            
        } else if(statusLead.equalsIgnoreCase('qualificado')) {
            
        }
    }
    
    public DashboardLeadController() {
        this.disableMotivo 	 = true;
        this.disablePrevisao = true;
        this.disableHora 	 = true;
        this.motivosList 	 = naoQualiList;
        
        Id actId 		  = ApexPages.currentPage().getParameters().get('actId');
        this.act 		  = getAct(actId);
        this.l 			  = getLead(this.act.whoId);
        this.activityDate = act.activityDate;
        this.activityHour = act.activityHour;
        
        System.debug('#### actId -> ' + actId);
        System.debug('#### this.act -> ' + this.act);
        System.debug('#### this.l -> ' + this.l);
        
        this.isVehicleOwner = (this.l.IsVehicleOwner__c != null && this.l.IsVehicleOwner__c.equals('Y'));
        this.selectedBrand  = (this.l.CRV_CurrentVehicle__c != null ? this.l.CRV_CurrentVehicle__r.Brand__c : null);
        this.selectedModel  = (this.l.CRV_CurrentVehicle__c != null ? this.l.CRV_CurrentVehicle__r.Model__c : null);
        getModelDetails();
    }

    public Lead getLead(Id leadId) {
        if(String.isEmpty(leadId)) return null;
        
        List<Lead> leadL = [
            SELECT Id, FirstName, LastName, CPF_CNPJ__c, LeadSource, SubSource__c,
            HomePhone__c, MobilePhone, Email, VehicleOfInterest__c, DealerOfInterest__c,
            IsVehicleOwner__c, CRV_CurrentVehicle__c, CRV_CurrentVehicle__r.Brand__c,
            CRV_CurrentVehicle__r.Model__c, Mobile_Phone__c, Home_Phone_Web__c,
            IntentToPurchaseNewVehicle__c
            FROM Lead WHERE id=: leadId
        ];
        
        if(leadL.isEmpty()) return null;
        
        return leadL.get(0);
    }
    
    public PageReference save() {
        System.debug('save()');
        
        if(!isValid()) return null;
        
        try {
            
            this.l.IsVehicleOwner__c = this.isVehicleOwner ? 'Y' : 'N';
            if(!String.isEmpty(selectedBrand) && !String.isEmpty(selectedModel)) {
                this.l.CRV_CurrentVehicle__c = VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(selectedBrand, selectedModel)[0].Id;
            }
            
            System.debug('Lead: ' + l);
            Database.update(this.l);
            
            if(statusLead.equalsIgnoreCase('qualificado')) {
                Opportunity opp = VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(l, null);
                
                System.debug('Opportunity: ' + opp);
                
            } else if(statusLead.equalsIgnoreCase('nqualificado')) {
                l.Status = 'Not Interested';
                l.IntentToPurchaseNewVehicle__c = previsao;
                
                Database.update(this.l);
                
                createTransaction(l, motivo);
                
            } else if(statusLead.equalsIgnoreCase('ncontato')) {
                System.debug('Não contatou');
                
                l.Status = 'To be Confirmed';
                
                if(motivo.equalsIgnoreCase('Wrong phone number') || motivo.equalsIgnoreCase('Died') ) {
                    l.Status = 'Others';
                }
                Database.update(this.l);
                
                final Task task = mapActivityToTask(act);
                System.debug('task ' + task);
                Database.update(task);
                
                createTransaction(l, motivo);
            }
            
            final PageReference pg = new PageReference('/apex/DashboardTask');
            pg.setRedirect(true);
            return pg;
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
            Apexpages.addMessage( new Apexpages.Message(ApexPages.Severity.ERROR, ex.getMessage()) );
        }
        
        return null;
    }
    
    private void createTransaction(Lead lead, String motv) {
        List<TST_Transaction__c> leadContact = [
          SELECT Id FROM TST_Transaction__c 
          WHERE isLeadContact__c = true AND Lead__c =: lead.Id
        ];
        TST_Transaction__c trans = new TST_Transaction__c();
        trans.Lead__c 				= lead.Id;
        trans.TypeOfContact__c 		= 'Phone';
        trans.ContactStatus__c 		= 'Not Effective';
        trans.TransactionStatus__c 	= lead.Status;
        trans.WhyNotContact__c  	= motv;
        trans.Contact_date__c 		= Datetime.now();
        trans.isLeadContact__c		= leadContact.isEmpty();
        INSERT trans; 
    }
    
    private Boolean isValid() {
        if(String.isEmpty(l.LastName) || String.isEmpty(l.FirstName)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'O nome e o sobrenome são obrigatórios') );
            return false;
        }
        
        if(String.isBlank(l.HomePhone__c) && String.isBlank(l.MobilePhone) && String.isBlank(l.Email) ) {
            Apexpages.addMessage(new Apexpages.Message(
                ApexPages.Severity.ERROR, Label.ERRPageErrorMessage + ' ' + Label.ACCEmail + ' ' + Label.UTIL_OR + ' ' + Label.ACCHomePhone + ' ' + Label.UTIL_OR + ' ' + Label.LDDCellular));
            return false;
        }
        
        if(String.isEmpty(l.VehicleOfInterest__c)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione o veículo de interesse.') );
            return false;
        }
        
        if(this.isVehicleOwner) {
            if(String.isEmpty(selectedBrand)) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione a marca.') );
                return false;
                
            } else if(selectedBrand.equalsIgnoreCase('RENAULT') && String.isEmpty(selectedModel)) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione o modelo.') );
                return false;
            }
        }
        
        if(String.isEmpty(statusLead)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione o status do lead.') );
            return false;
            
        } else if(statusLead.equalsIgnoreCase('nqualificado')) {//Nao qualificado
            
            if(String.isEmpty(motivo)) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'O campo motivo é necessário') );
                return false;
                
            } else if(String.isEmpty(previsao)) {
                //Nao qualificado
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'O campo previsão é necessário') );
                return false;
            }
            
        } else if(statusLead.equalsIgnoreCase('ncontato')) {
            if(String.isEmpty(motivo)) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'O campo motivo é necessário') );
                return false;
            }
            
            if(!motivo.equalsIgnoreCase('Wrong phone number') && !motivo.equalsIgnoreCase('Died') ) {
                l.Status = 'Others';
                if(String.isBlank(act.activityDate) || String.isBlank(act.activityHour)) {
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Selecione Data, Hora da Atividade'));
                    return false;
                }
            }
        }
        
        return true;
    }
    public PageReference cancelar() {
        PageReference pg = new PageReference('/apex/DashboardTask');
        pg.setRedirect(true);
        return pg;
    }
}