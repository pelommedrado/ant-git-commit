@IsTest
public class OpportunityBuild {

    public static OpportunityBuild instance = null;

    public static OpportunityBuild getInstance() {
        if(instance == null) {
            instance = new OpportunityBuild();
        }
        return instance;
    }

    private OpportunityBuild() {
    }

    public Opportunity createOpportunity(Id userId, Id accPessoalId, Id accDealerId) {
        final Date data = Date.today().addDays(5);
        final Opportunity opp = new Opportunity();
        opp.OwnerId 				= userId;
        opp.AccountId 				= accPessoalId;
        opp.Dealer__c 				= accDealerId;
        opp.CloseDate 				= data;
        opp.StageName 				= 'Identified';
        opp.Expiration_Time__c 		= Datetime.newInstance(data, Time.newInstance(17, 0, 0, 0));
        opp.Name 					= 'Opportunity';
        opp.OpportunitySource__c 	= 'TEST DRIVE';
        opp.OpportunitySubSource__c = 'INTERNET';
        opp.SourceMedia__c 			= 'Site Concessionária';
        return opp;
    }
}