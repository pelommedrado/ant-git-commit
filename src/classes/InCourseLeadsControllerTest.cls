@isTest
public class InCourseLeadsControllerTest {
    
    public static testMethod void teste1(){
        
        Test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Lead lead = moc.CriaLead();
        lead.CPF_CNPJ__c = '60609523562';
        lead.RecordTypeId = Utils.getRecordTypeId('Lead', 'Voucher');
        lead.Status = 'To Rescue';
        Insert lead;
        
        Campaign campanha = moc.criaCampanha();
        campanha.Billing_Required__c = FALSE;
        campanha.Refundable__c = FALSE;
        campanha.IsActive = TRUE;
        campanha.Status = 'In Progress';
        Insert campanha;
        
        CampaignMember cm = moc.criaMembroCampanha();
        cm.CampaignId = campanha.Id;
        cm.LeadId = lead.Id;
        Insert cm;
        
        PageReference pageRef = new PageReference('/apex/InCourseLeads');
        Test.setCurrentPage(pageRef);
        
        InCourseLeadsController controller = new InCourseLeadsController();
        pageRef.getParameters().put('cmId',cm.Id);
        
        List<CampaignMember> lsCampMember = controller.lsCampMember;
        
        // testa pesquisa com CPF e verifica se retornou um registro
        ApexPages.currentPage().getParameters().put('valor','60609523562');
        controller.localizaVoucher();
        system.assertEquals(1,controller.campMemberList.size());
        
        //testa retorno da pagina e status do lead atualizado dentro do metodo
        system.assertEquals('/apex/PromotionalActions', controller.resgatar().getUrl());
        system.assertEquals('Rescued', [SELECT Status FROM Lead Where Id =: lead.Id].Status);
        
        // testa retorno da pagina
        system.assertEquals('/apex/PromotionalActions', controller.loadPagePromotionalActions().getUrl());
        
        // testa retorno da pagina
        system.assertEquals('/apex/VoucherRescuedPassing?cmId='+cm.Id, controller.loadVoucherRescuedPassPage().getUrl());
        
        //teste retorno da pagina
        system.assertEquals('/apex/getvoucher?CPF='+lead.CPF_CNPJ__c, controller.loadGetVoucherConsult().getUrl());
        
        //testa retorno da pagina
        system.assertEquals('/apex/PesquisaSfa2?cmId='+cm.Id, controller.loadPagePesquisSfa().getUrl());
        
        
        Test.stopTest();
        
    }

}