public with sharing class VFC98_Restriction {

    static final String SINGLE_QUOTES       =   '\'' ;  
    static final String EQUALS              =   ' = ' ;
    static final String ANDZ                =   ' and ' ;
    static final String ORZ                 =   ' or ' ;
    static final String LIKEZ               =   ' like ' ;
    static final String NOT_NULL            =   ' != null ' ;
    static final String IS_NULL             =   ' = null ' ;
    static final String IS_IN               =   ' in ' ;
    static final String NOT_EQUALS          =   ' != ' ;
    static final String GREATER_THAN        =   ' > ' ;
    static final String GREATER_THAN_EQUALS =   ' >= ' ;
    static final String LESS_THAN           =   ' <  ' ;
    static final String LESS_THAN_EQUALS    =   ' <= ' ;
    static final String LIMIT_RESTRICTION   =   ' limit ' ; 
    static final String ORDER_BY            =   ' order by ' ;
    static final String NOTZ                =   ' NOT ' ;
    static final String PARENTHESES_START   =   ' ( ';
    static final String PARENTHESES_END     =   ' ) ';
    
    
    public static String logicalOperator (String operator, String constrant) {
        if ( nullToEmpty(constrant) == '') return '';
        return  operator + constrant; 
    }
    
    public static String constrantWithOutSingleQuotes (String fieldName
                                                      , String value
                                                      , String operator) {
        if (nullToEmpty(value) == '') return '';
        return fieldName + operator + value; 
    }
    
    public static String constrantWithParentheses (String fieldName
                                                      , String value
                                                      , String operator) {
        if (nullToEmpty(value) == '') return '';
        return  PARENTHESES_START + fieldName + operator + value + PARENTHESES_END; 
    }    

    public static String constrant (String fieldName, String operator) {
        return fieldName + operator; 
    }

    public static String constrant (String fieldName, String value, String operator) {
        
        if (nullToEmpty(value) == '') return '';
        
        return fieldName + operator + SINGLE_QUOTES + String.escapeSingleQuotes( value ) + SINGLE_QUOTES; 
    }
    
    public static String constrant (String fieldName, Boolean value, String operator) {
        return fieldName + operator + value ; 
    }   

    public static String eq (String fieldName, String value) {
        return constrant(fieldName, value , EQUALS);
    }
    
    public static String eq (String fieldName, Boolean value) {
        return constrant(fieldName, value , EQUALS);
    }
     
    
    public static String eq(String fieldName, String value,  Boolean allowNull) {   
        if(allowNull){
            return fieldName + EQUALS + SINGLE_QUOTES + String.escapeSingleQuotes(nullToEmpty(value)) + SINGLE_QUOTES;
        }  else {
            return eq(fieldName, value);
        }
    }

    /*
    public static String eq (QueryMapping mapping) {
        return constrant(mapping.fieldName, mapping.getValue() , EQUALS);
    }*/

    public static String notEq (String fieldName, String value) {
        return constrant(fieldName, value , NOT_EQUALS); 
    }

    /*
    public static String notEq (QueryMapping mapping) {
        return constrant(mapping.fieldName, mapping.getValue() , NOT_EQUALS); 
    }
    */


    public static String lt (String fieldName, String value) {
        return constrant(fieldName, value , LESS_THAN);  
    }
    /*
    public static String lt (QueryMapping mapping) {
        return constrant(mapping.fieldName, mapping.getValue() , LESS_THAN);  
    }
    */


    public static String le (String fieldName, String value) {
        return  constrant(fieldName, value , LESS_THAN_EQUALS); 
    }


    public static String gt (String fieldName, String value) {
        return  constrant(fieldName, value , GREATER_THAN); 
    }

    public static String ge (String fieldName, String value) {
        return  constrant(fieldName, value , GREATER_THAN_EQUALS); 
    }

    public static String between ( String fieldName ,  DateTime lo, DateTime hi  ) {
        String lowFormatedDate =  ( lo != null ) ? lo.format('yyyy-MM-dd')+'T00:00:00Z' : '';
        String hightFormatedDate = ( hi != null ) ? hi.format('yyyy-MM-dd')+'T00:00:00Z' : '';
        return  betweenDates(fieldName, lowFormatedDate, hightFormatedDate);
    }

    public static String betweenDates (String fieldName, String lo , String hi  ) {
        
        if ( (lo == null || lo == '') && (hi == null || hi == '') ) return ''; 
        
        if (lo == null || lo == '') 
            return constrantWithOutSingleQuotes ( fieldName, hi ,   EQUALS);
        
        if (hi == null || hi == '') 
            return constrantWithOutSingleQuotes ( fieldName, lo ,   EQUALS);
        

        return  constrantWithParentheses('', 
                                          constrantWithOutSingleQuotes (fieldName, lo , GREATER_THAN_EQUALS) 
                                          + andd ( constrantWithOutSingleQuotes (fieldName, hi , LESS_THAN_EQUALS) ), 
                                         '');
    }
    
    
    public static String betweenNotEqual ( String fieldName ,  DateTime lo, DateTime hi  ) {
        String lowFormatedDate =  ( lo != null ) ? lo.addDays(1).format('yyyy-MM-dd') : 'T00:00:00';
        String hightFormatedDate = ( hi != null ) ? hi.addDays(1).format('yyyy-MM-dd') : 'T00:00:00';
        
        return  betweenDatesNotEqual(fieldName, lowFormatedDate, hightFormatedDate);
    }

    public static String betweenDatesNotEqual (String fieldName, String lo , String hi  ) {
        
        if ( (lo == null || lo == '') && (hi == null || hi == '') ) return ''; 
        
        if (lo == null || lo == '') 
            return constrantWithOutSingleQuotes ( fieldName, hi ,   EQUALS);
        
        if (hi == null || hi == '') 
            return constrantWithOutSingleQuotes ( fieldName, lo ,   EQUALS);
        

        return  constrantWithParentheses('', 
                                          constrantWithOutSingleQuotes (fieldName, lo , GREATER_THAN) 
                                          + orr ( constrantWithOutSingleQuotes (fieldName, hi , LESS_THAN) ), 
                                         '');
    }
    
    /*
    public static String betweenDates (QueryMapping lo  , QueryMapping hi  ) {
        return betweenDates (lo.fieldName , lo.getValue() , hi.getValue());     
    }
    */

    public static String between (String fieldName, String lo , String hi ) {
        if ( (lo == null || lo == '') && (hi == null || hi == '') ) return ''; 
        if (lo == null || lo == '') return eq(fieldName, hi);
        if (hi == null || hi == '') return eq(fieldName, lo);
        
        return    constrant(fieldName, lo , GREATER_THAN_EQUALS) 
                + andd ( constrant(fieldName, hi , LESS_THAN_EQUALS) ) ; 

    }
    
    public static String isLike (String fieldName, String value) {
        if (value == null) return '';
        return    constrant(fieldName, value , LIKEZ);
    }

    public static String eqOrLike (String fieldName, String value) {
        if (value == null) return '';
                
        if (value.indexOf('*') > -1) {          
            return isLike(fieldName,value.replace ('*','%'));
        }               
        
        return eq (fieldName, value);               
    }

    //@deprecated
    public static String isNotNull (String fieldName, String value) {
        if (value == null || value == '') return '';
        return    constrant(fieldName, NOT_NULL);
    }

    public static String isNotNull (String fieldName) {
        return    constrant(fieldName, NOT_NULL);
    }


    public static String isNull (String fieldName, String value) {
        if (value == null || value == '') return '';
        return    constrant(fieldName, IS_NULL);
    }
    
    public static String notExpression (String expression) {
            if (expression == null || expression == '') return '';
        return  NOTZ + constrantWithParentheses ('', expression, '');
    }

   /* 
    public static String isIn (QueryMapping mapping) {
        return    constrantWithOutSingleQuotes(mapping.fieldName
                                            , mapping.getValue() 
                                            , IS_IN );
    }
    */

    public static String isIn (String fieldName, String[] values) {
        return    constrantWithOutSingleQuotes (fieldName 
                           , convertArrayStringIntoInClause(values)
                           , IS_IN);
    }
    
    public static String isIn (String fieldName, String values) {
        return    constrantWithOutSingleQuotes(fieldName, values , IS_IN );
    }
    
    public static String isNotIn (String fieldName, String[] values) {
        if(values.size() < 1)
            return '';
        
        return  constrantWithOutSingleQuotes (fieldName 
                           , convertArrayStringIntoInClause(values)
                           , NOTZ + IS_IN);
    }
        
    public static String convertArrayStringIntoInClause (String[] values) {
        String value = '( ';
        Integer i = 0;
        for (String val : values) {
            if (i > 0) value += ','; 
            value += addStringInQuery(val);             
            i++;
        }
        value += ' )';
        return value;       
    }
    
    public static String andd ( String constrant ) {
        return logicalOperator(ANDZ, constrant);        
    }

    public static String orr ( String constrant ) {
        return logicalOperator(ORZ, constrant);     
    }

    public static String addLimit (Integer queryLimit) {
        return LIMIT_RESTRICTION + queryLimit; 
    }
    
    public static String orderBy ( String expression ) { 
        return ORDER_BY + expression;
    }
    
    private static String addStringInQuery (String value) {
        return SINGLE_QUOTES + String.escapeSingleQuotes(value) + SINGLE_QUOTES;
    }
    
    
    private static String nullToEmpty (String value) {
        return (value != null) ? value : '';
    }
}