@IsTest
public class UserBuild {
    
    public static UserBuild instance = null;
    
    public static UserBuild getInstance() {
        if(instance == null) {
            instance = new UserBuild();
        }
        return instance;
    }
    
    private UserBuild() {
    }

    public User createUser(Id contactId, Id prodileId) {
        User user = new User();
        user.ContactId 			= contactId;
        user.ProfileId 			= prodileId;
        user.FirstName 			= 'User';
        user.LastName 			= 'Test';
        user.Username 			= 'manager1@org1.com.teste';
        user.Email 				= 'manager1@org1.com.teste';
        user.Alias 				= 'user';
        user.BIR__c 			= '1234';
        user.EmailEncodingKey	='UTF-8';
        user.LanguageLocaleKey	='en_US';
        user.LocaleSidKey		='en_US';
        user.TimeZoneSidKey		='America/Los_Angeles'; 
        user.RecordDefaultCountry__c = 'Brazil';
        return user;
    }
}