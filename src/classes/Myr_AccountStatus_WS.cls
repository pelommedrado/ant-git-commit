/*  MyRenault BackEnd WebService. Used to give the status of an account linked to the given email addresse.
	Used to indicate if the given email address is available for account creation.
    Error codes :   OK              : WS09MS000 
                    OK WITH WARNING : WS09MS001 -> WS09MS500
                    CRITICAL ERROR  : WS09MS501 -> WS09MS999
*************************************************************************************************************
@version 16.10: SDP / F1991-US3659 / Initial creation
*************************************************************************************************************/
global without sharing class Myr_AccountStatus_WS {

	/** Exception management for test purposes **/
	public static final String GENERAL_EXCEPTION_SIMULATION = 'test.simulateException@unittest.atos.net';
	public class Myr_AccountStatus_WS_Exception extends Exception {}

	/** WebService main function **/
	WebService static Myr_AccountStatus_WS_Response checkEmail( Myr_AccountStatus_WS_Input inputs ) {
		AccountStatusChecker accountStatus = new AccountStatusChecker();
		Myr_AccountStatus_WS_Response response = new Myr_AccountStatus_WS_Response();
		try {
			response = accountStatus.getAccountStatus( inputs );
		} catch( Exception e ) {
			response = accountStatus.returnException(e.getMessage());
		}
		return response;
	}

	/**** MAIN CLASS ***************************************************************************************/

	global class AccountStatusChecker {
		//members
		private long beginTime;
		private long endTime;
		private Myr_AccountStatus_WS_Response response;
		private Myr_AccountStatus_WS_Input inputs;

		//@constructor
		public AccountStatusChecker() {
			this.beginTime = DateTime.now().getTime();
			this.response = new Myr_AccountStatus_WS_Response();
		}

		/** This is the main function used to check the availability of the given emailAddress
			@param inputsWS: the inputs given to the webservice
			@return response: the response for the given inputs
		**/
		public Myr_AccountStatus_WS_Response getAccountStatus(Myr_AccountStatus_WS_Input inputsWS) {
			inputs = inputsWS;

			//simulate exception for test purposes
			if( Test.isRunningTest() && GENERAL_EXCEPTION_SIMULATION.equalsIgnoreCase( inputs.emailAddress ) ) {
				throw new Myr_AccountStatus_WS_Exception('Test exception');
			}
			
			//missing mandatory parameters
			missingMandatory();
			if( !String.isBlank(response.info.code) ) return returnResponse(); //oups there was a problem !
			
			//too long parameters
			tooLongParameters();
			if( !String.isBlank(response.info.code) ) return returnResponse(); //oups there was a problem !

			//email format
			if( !Myr_MyRenaultTools.isEmailValid( inputs.emailAddress ) ) {
				response.info.code = system.Label.Myr_AccountStatus_WS09MS503;
				response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS503_Msg, new List<String>{inputs.emailAddress} );
				return returnResponse();
			}

			//check number of records found
			String username = inputs.emailAddress;
			if( !inputs.emailAddress.startsWithIgnoreCase( CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c ) ) {
				username = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Unique__c + inputs.emailAddress;
			}
			List<Account> listAccounts = [SELECT Id, isCustomerPortal, Myr_Status__c, Myd_Status__c FROM Account WHERE MyRenaultId__c = :inputs.emailAddress OR MyDaciaId__c = :inputs.emailAddress];
			if( listAccounts.size() > 1 ) {
				//found more than 1 account
				response.info.code = system.Label.Myr_AccountStatus_WS09MS504;
				response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS504_Msg, new List<String>{inputs.emailAddress} );
			} else if( listAccounts.size() == 0 ) {
				//check username availability
				Boolean usernameAvailable = Myr_Users_Utilities_Class.isUsernameAvailableAlThroughOrgs( username );
				if( usernameAvailable ) {
					response.info.code = system.Label.Myr_AccountStatus_WS09MS000;
					response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS000_Msg, new List<String>{inputs.emailAddress} );
					response.output.userStatus = system.Label.Myr_AccountStatus_UserStatus_NotExist;
				} else {
					response.info.code = system.Label.Myr_AccountStatus_WS09MS505;
					response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS505_Msg, new List<String>{inputs.emailAddress} );
				}
			} else if ( listAccounts.size() == 1 ) {
				Account foundAccount = listAccounts[0];
				response.output.myr_status = foundAccount.MYR_Status__c;
				response.output.myd_status = foundAccount.MyD_Status__c;
				response.output.accountSfdcId = foundAccount.Id;
				//only one account found
				//check user existence
				if( foundAccount.isCustomerPortal ) {
					response.info.code = system.Label.Myr_AccountStatus_WS09MS002;
					response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS002_Msg, new List<String>{inputs.emailAddress} );
					response.output.userStatus = system.Label.Myr_AccountStatus_UserStatus_Exist;
				} else {
					Boolean usernameAvailable = Myr_Users_Utilities_Class.isUsernameAvailableAlThroughOrgs( username );
					if( usernameAvailable ) {
						response.info.code = system.Label.Myr_AccountStatus_WS09MS001;
						response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS001_Msg, new List<String>{inputs.emailAddress} );
						response.output.userStatus = system.Label.Myr_AccountStatus_UserStatus_NotExist;
					} else {
						response.output.empty();
						response.info.code = system.Label.Myr_AccountStatus_WS09MS505;
						response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS505_Msg, new List<String>{inputs.emailAddress} );
					}
				}
			}

			return returnResponse();
		}

		/** returns the response and logs the result **/
		private Myr_AccountStatus_WS_Response returnResponse() {
			endTime = DateTime.now().getTime();
			logResult(null);
			return response;
		}

		/** returns the response in a case of exception and logs the result **/
		public Myr_AccountStatus_WS_Response returnException(String codeException) {
			response.info.code = system.Label.Myr_AccountStatus_WS09MS999;
			response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS999_Msg, new List<String>{codeException} );
			response.output.empty();
			endTime = DateTime.now().getTime();
			logResult(codeException);
			return response;
		}

		/** Logs the current status **/
		private void logResult(String codeException) {
			String runId = null;
			String parentLogId = null;
			INDUS_Logger_CLS.ProjectName project = INDUS_Logger_CLS.ProjectName.MYR;
			String apexClass = Myr_AccountStatus_WS.class.getName();
			String function = 'checkEmail';
			String record = String.valueOf( inputs );
			String excepType = response.info.code;
			String message = String.valueOf( response );
			INDUS_Logger_CLS.ErrorLevel errorLevel = INDUS_Logger_CLS.ErrorLevel.Info;
			if( !String.isBlank( codeException ) ) {
				excepType = 'System Exception';
				message = codeException;
				errorLevel = INDUS_Logger_CLS.ErrorLevel.Critical;
			}
			//insert the log
			INDUS_Logger_CLS.addGroupedLogFull (runId, parentLogId, project, apexClass, function, excepType, message, String.valueOf(errorLevel), null, null, null, beginTime, endTime, record);
		}

		/** Check if all the parameters are present **/
		private void missingMandatory() {
			if( String.isBlank(inputs.emailAddress) ) {
				response.info.code = system.Label.Myr_AccountStatus_WS09MS501;
				response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS501_Msg, new List<String>{'emailAddress'} );
			}
		}

		/** check if there were not too long parameters **/
		private void tooLongParameters() {
			if( !String.isBlank( inputs.emailAddress ) && inputs.emailAddress.length() > 80 ) {
				response.info.code = system.Label.Myr_AccountStatus_WS09MS502;
				response.info.message = String.format( system.Label.Myr_AccountStatus_WS09MS502_Msg, new List<String>{inputs.emailAddress} );
			}
		}

	}

	/**** INNER TOOL CLASSES *******************************************************************************/

	//Request
	global class Myr_AccountStatus_WS_Input {
		WebService String emailAddress;
	}

	//Response
	global class Myr_AccountStatus_WS_Response {
		WebService Myr_AccountStatus_WS_Response_Info info;
		WebService Myr_AccountStatus_WS_Response_Output output;
		public Myr_AccountStatus_WS_Response() {
			info = new Myr_AccountStatus_WS_Response_Info();
			output = new Myr_AccountStatus_WS_Response_Output();
		}
	}

	global class Myr_AccountStatus_WS_Response_Info {
		WebService String code;
		WebService String message;
	}

	global class Myr_AccountStatus_WS_Response_Output {
		WebService String accountSfdcId;
		WebService String myr_status;
		WebService String myd_status;
		WebService String userStatus;
		public void empty() {
			accountSfdcId = null;
			myr_status = null;
			myd_status = null;
			userStatus = null;
		}
	}

}