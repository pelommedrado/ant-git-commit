@isTest
public class VFC86_VehicleBookingBOTest {
    
    static VEH_Veh__c veh;
    static VehicleBooking__c vbk;
    static VehicleBooking__c vbkTwo;
    static Quote quote;
    static Opportunity opp;
    
    static Set<String> vehicles = new Set<String>();
    static Set<String> quotes = new Set<String>();
    
    static{
        
        MyOwnCreation moc = new MyOwnCreation();
        
        opp = moc.criaOpportunity();
        insert opp;
        
        quote = moc.criaQuote();
        quote.OpportunityId = opp.id;
        quote.Status = 'Active';
        insert quote;
        
        veh = moc.criaVeiculo();
        insert veh;
        
        VEH_Veh__c veh2 = moc.criaVeiculo();
        veh2.Name = '12345678912458966';
        insert veh2;
        
        vbk = new VehicleBooking__c();
        vbk.Vehicle__c = veh.Id;
        vbk.Status__c = 'Active';
        vbk.Quote__c = quote.id;
        insert vbk;
        
        vbkTwo = new VehicleBooking__c();
        vbkTwo.Vehicle__c = veh2.Id;
        vbkTwo.Status__c = 'Active';
        
        vehicles.add(veh.id);
        quotes.add(quote.id);
        
        
    }
    
    static testMethod void Test_getMappingVehicleBookingActivesByVehicles(){
        
        test.startTest();
        
        Map<String, VehicleBooking__c> toAssert = new Map<String, VehicleBooking__c>();
        toAssert.put(vbk.Vehicle__c, vbk);
        
        Map<String, VehicleBooking__c> recieved = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesByVehicles(vehicles);
        System.assertEquals(toAssert, recieved);
        
        test.stopTest();
        
    }
    
    static testMethod void Test_updateVehicleBooking(){
        
        test.startTest();
        
        VFC86_VehicleBookingBO.getInstance().updateVehicleBooking(vbk);
        VFC86_VehicleBookingBO.getInstance().insertVehicleBooking(vbkTwo);
       
        test.stopTest();
        
    }
    
    static testMethod void Test_updateVehicleBookingsFromQuotesForCanceledStatus(){
        
        test.startTest();
        
        //cobre getVehicleBookingActivesByQuotes();
        
        VFC86_VehicleBookingBO.getInstance().updateVehicleBookingsFromQuotesForCanceledStatus(quotes);
        
        test.stopTest();
        
    }
    
    static testMethod void Test_getMappingVehicleBookingActivesByQuotes(){
        
        test.startTest();
        
        Map<String, VehicleBooking__c> toAssert = new Map<String, VehicleBooking__c>();
        toAssert.put(vbk.Quote__c, vbk);
        
        Map<String, VehicleBooking__c> recieved = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesByQuotes(quotes);
        System.assertEquals(toAssert, recieved);
        
        Map<String, VehicleBooking__c> recieved2 = VFC86_VehicleBookingBO.getInstance().getMappingVehicleBookingActivesOrClosedByQuotes(quotes);
        System.assertEquals(toAssert, recieved);
        
        VehicleBooking__c recieved3 = VFC86_VehicleBookingBO.getInstance().getVehicleBookingActiveByQuote(quote.Id);
        //System.assertEquals(vbk, recieved3);
        
        test.stopTest();
        
    }

}