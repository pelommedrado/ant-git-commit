@isTest(SeeAllData=true)
public class VFC63_QuoteBusinessDelegateTest {
    
    static Opportunity opp;
    static Quote quote;
    static VFC60_QuoteVO quoteVO = new VFC60_QuoteVO();
    static Account customer;
    
    static{
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        Account dealer = moc.criaAccountDealer();
        dealer.IDBIR__c = '1001';
        insert dealer;
        
        customer = moc.criaPersAccount();
        customer.CustomerIdentificationNbr__c = '23584102502';
        customer.Sex__c = 'Woman';
        insert customer;
        
        opp = moc.criaOpportunity();
        opp.AccountId = customer.Id;
        opp.Dealer__c = dealer.Id;
        insert opp;
        
        Product2 p2 = moc.criaProduct2();
        p2.IsActive = true;
        p2.RecordTypeId = Utils.getRecordTypeId('Product2', 'PDT_ModelVersion');
        insert p2;
        
        PricebookEntry pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = standardPB.Id;
        pe.Product2Id = p2.id;
        pe.UnitPrice = 1000;
        pe.UseStandardPrice = false;
        pe.IsActive = true;
        insert pe;
        
        quote = moc.criaQuote();
        quote.OpportunityId = opp.id;
        quote.BillingAccount__c = customer.Id;
        quote.Pricebook2Id = standardPB.Id;
        insert quote;
        
        QuoteLineItem qli = moc.criaQuoteLineItem();
        qli.QuoteId = quote.id;
        qli.PricebookEntryId = pe.Id;
        qli.Quantity = 1;
        qli.UnitPrice = 1000;
        insert qli;
        
        quoteVO.opportunityId = opp.id;
        quoteVO.reasonLossCancellationOpportunity = 'reason';
        quoteVO.sObjQuote = quote;
        quoteVO.sObjQuote.Opportunity = opp;
        quoteVO.sObjQuote.Opportunity.Account = customer;
        quoteVO.accountFirstName = 'FirstName';
        quoteVO.accountLastName = 'LastName';
        quoteVO.accountEmail = 'teste@teste.com.br';
        quoteVO.accountCelPhone = '23232323';
        quoteVO.accountResPhone = '23232323';
        quoteVO.FKQuote = quote;
        
    }
    
    static testMethod void testGetOpportunity(){
        
        test.startTest();
        
        VFC63_QuoteBusinessDelegate.getInstance().getOpportunity(opp.id);
        
        test.stopTest();
        
    }
    
    static testMethod void testCancelOpportunity(){
        
        test.startTest();
        
        VFC63_QuoteBusinessDelegate.getInstance().cancelOpportunity(quoteVO);
        
        test.stopTest();
        
    }
    
    static testMethod void testGetQuote(){
        
        test.startTest();
        
        VFC63_QuoteBusinessDelegate.getInstance().getQuote(opp.id);
        
        test.stopTest();
        
    }
    
    static testMethod void testSynchronizationOpportunity(){
        
        test.startTest();
        
        VFC63_QuoteBusinessDelegate.getInstance().synchronizationOpportunity(opp.id, quote.id);
        
        test.stopTest();
        
    }
    
    static testMethod void testGetQuoteById(){
        
        test.startTest();
        
        VFC63_QuoteBusinessDelegate.getInstance().getQuoteById(quote.id);
        
        test.stopTest();
        
    }

    static testMethod void testUpdateQuoteVOByAccount(){

        Test.startTest();

        VFC63_QuoteBusinessDelegate.getInstance().updateQuoteVOByAccount(customer,quoteVO);

        Test.stopTest();
    }

    static testMethod void tesSaveQuote(){

        Test.startTest();

        VFC63_QuoteBusinessDelegate.getInstance().saveQuote(quoteVO);

        Test.stopTest();
    }

    static testMethod void testGetCustomerById(){

        Test.startTest();

        VFC63_QuoteBusinessDelegate.getInstance().getCustomerById(customer.Id);

        Test.stopTest();
    }

}