@isTest
private class AgendamentoServicoControllerTest {
    
    public static Account acc { get;set; }
    
    static testMethod void unitTest01() {
        
        User user = criarUser();
        
        System.runAs(user) {
            
            Opportunity opp1 = new Opportunity();       
            opp1.AccountId = acc.Id;
            opp1.Name = 'OppNameTest_1';
            opp1.StageName = 'Identified';
            opp1.CloseDate = Date.today();
            opp1.CurrencyIsoCode = 'BRL';
            opp1.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'Service');
            opp1.Dealer__c = acc.Id;
            
            insert opp1;
            
            Datetime dt = Datetime.now();
            
            Event evento = new Event();
            evento.Subject = 'opp.ServiceType__c';
            evento.WhatId = opp1.Id;
            evento.StartDateTime = dt;
            evento.EndDateTime = dt.addMinutes(15);
            
            insert evento;
            
            AgendamentoServicoController contr = new AgendamentoServicoController();
            contr.visualizar();
            contr.novo();
            contr.editar();
            contr.limparFiltro();
            contr.data.ActivityDate = Date.today();
            contr.filtrar();
        }
    }
    
    static User criarUser() {
        acc = new Account(
            Name = 'DealerAcc', 
            ShippingCity = 'Cidade', 
            ShippingState = 'Estado', 
            NameZone__c = 'R2', 
            TV_Signal__c = 'Pra?a',
            Active_PV__c = true,
            isDealerActive__c = true,
            PV_Cooperative__c = true,
            PV_NotCooperative__c = true, 
            IDBIR__c= '79856523',
            RecordTypeId = VFC145_OfferWizardController.dealerRecTypeId
        );
        Database.insert( acc );
        
        Contact cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = acc.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_Service__c = 'Available'
        );
        
        Database.insert( cont );
        
        User manager = new User(
            FirstName = 'Test',
            LastName = 'User',
            Email = 'test@org.com',
            Username = 'test@org1.com',
            Alias = 'tes',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = 'testing',
            ProfileId = '00eD0000001PnJi',
            ContactId = cont.Id,
            BIR__c ='79856523',
            isCac__c = true
            
        );
        
        return manager;
    }
}