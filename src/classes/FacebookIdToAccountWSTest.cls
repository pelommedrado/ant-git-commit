@isTest
private class FacebookIdToAccountWSTest {
	@TestSetup static void setup(){
		Account acc = new Account();
		acc.Name = 'testAccount';
		acc.Phone = '12345678';

		Database.insert(acc);

		Account cncs = new Account();
		cncs.Name = 'cncsTest';
		cncs.Phone = '12345678';
		cncs.IDBIR__c = '12345';

		Database.insert(cncs);

		SurveySent__c ss = new SurveySent__c();
		ss.Account__c = acc.id;
		ss.BIR__c     = cncs.IDBIR__c;

		Database.insert(ss);
	}

	@isTest static void shoudAssignFacebookIdVN(){
		Account acc = [SELECT Id FROM Account WHERE Name = 'testAccount' LIMIT 1];

		Test.setMock(HttpCalloutMock.class, new FacebookIdToAccountMock());

		Id checkId;
		FacebookIdToAccountWS.WSResponse innerClass;

		Test.startTest();
		innerClass = FacebookIdToAccountWS.assignFacebookId('12345', '123', 'API Key', acc.Id, 'VN');
		Test.stopTest();

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.accountId == null);

		checkId = innerClass.accountId;

		Account check = [SELECT Id , FacebookUserId__c, FacebookPageId__c FROM Account WHERE Id =: checkId];
		System.assertEquals('12345' ,check.FacebookUserId__c);
		System.assertEquals('123', check.FacebookPageId__c);

	}

	@isTest static void shoudAssignFacebookIdPV(){
		Account acc = [SELECT Id FROM Account WHERE Name = 'testAccount' LIMIT 1];

		Test.setMock(HttpCalloutMock.class, new FacebookIdToAccountMock());

		Id checkId;
		FacebookIdToAccountWS.WSResponse innerClass;

		Test.startTest();
		innerClass = FacebookIdToAccountWS.assignFacebookId('12345', '123', 'API Key', acc.Id, 'PV');
		Test.stopTest();

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.accountId == null);

		checkId = innerClass.accountId;

		Account check = [SELECT Id , FacebookUserId__c, FacebookPageId__c FROM Account WHERE Id =: checkId];
		System.assertEquals('12345' ,check.FacebookUserId__c);
		System.assertEquals('123', check.FacebookPageId__c);

	}

	@isTest static void shoudAssignFacebookIdWithoutCategory(){
		Account acc = [SELECT Id FROM Account WHERE Name = 'testAccount' LIMIT 1];

		Test.setMock(HttpCalloutMock.class, new FacebookIdToAccountMock());

		Id checkId;
		FacebookIdToAccountWS.WSResponse innerClass;

		Test.startTest();
		innerClass = FacebookIdToAccountWS.assignFacebookId('12345', '123', 'API Key', acc.Id, '');
		Test.stopTest();

		System.assertEquals('Sucess', innerClass.message);
		System.assertEquals('', innerClass.error);
		System.assertEquals(false, innerClass.accountId == null);

		checkId = innerClass.accountId;

		Account check = [SELECT Id , FacebookUserId__c, FacebookPageId__c FROM Account WHERE Id =: checkId];
		System.assertEquals('12345' ,check.FacebookUserId__c);
		System.assertEquals('123', check.FacebookPageId__c);

	}

	@isTest static void shoudntGetAccount(){
		Contact contato = new Contact();
		contato.LastName = 'testContato';
		Database.insert(contato);

		Test.setMock(HttpCalloutMock.class, new FacebookIdToAccountMock());

		Id checkId;
		FacebookIdToAccountWS.WSResponse innerClass;

		innerClass = FacebookIdToAccountWS.assignFacebookId('12345', '123', 'API Key', contato.Id, 'PV');

		System.assertEquals('Error', innerClass.message);
		System.assertEquals('Account not Found', innerClass.error);
		System.assertEquals(true, innerClass.accountId == null);
	}

	@isTest static void shoudnGetAccountException(){
		Account acc = [SELECT Id FROM Account WHERE Name = 'testAccount' LIMIT 1];

		String exceed = '';

		for(Integer i = 0; i < 4; i++){
			exceed += '00110011001';
		}

		Test.setMock(HttpCalloutMock.class, new FacebookIdToAccountMock());

		Id checkId;
		FacebookIdToAccountWS.WSResponse innerClass;

		innerClass = FacebookIdToAccountWS.assignFacebookId(exceed, '123', 'API Key', acc.Id, 'VN');

		System.assertEquals('Error', innerClass.message);
		System.assertEquals(true, innerClass.error.contains('Facebook User Id: valor de dados grande demais: '+ exceed));
		System.assertEquals(true, innerClass.accountId == null);
	}
}