/**
* Class - VFC18_VehicleRelationDAO
*   #01 <Suresh Babu> <19/10/2012>
*		This DAO class fetch records from Vehicle Relation object using criteria mentioned.
*/
public with sharing class VFC18_VehicleRelationDAO extends VFC01_SObjectDAO{
	private static final VFC18_VehicleRelationDAO instance = new VFC18_VehicleRelationDAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC18_VehicleRelationDAO(){}

    /**
    * Method responsible for providing the instance of this class.. 
    */  
    public static VFC18_VehicleRelationDAO getInstance(){
        return instance;
    }
    
    /**
    * This Method was used to get VehicleRelationRecords Record based on some  fields
    * @param licensePlate - use licensePlate to fetch records from VehicleRelation object.
    * @return lstVehicleRelationRecords - fetch and return the result in lstVehicleRelationRecords list.
    */
    public List<VRE_VehRel__c> fetchVehicleRelationRecords(String licensePlate, String frame){
    	Id personalAccount_recordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'Personal_Acc' );
    	List<VRE_VehRel__c> lstVehicleRelationRecords = null;
    	
    	String active = 'Active';
    	String query_VehicleRelations = 'SELECT '+
    									' Account__c, CreatedById, CreatedDate, IsDeleted, EndDateRelation__c, '+
    									' LastModifiedById, LastModifiedDate, MaintenanceDealer__c, Model__c, '+
    									' Name, SellerDealer__c, StartDateRelation__c, Status__c, Id, '+
    									' TypeRelation__c, VIN__r.Name, VNVO__c, VehicleBrand__c, VehicleRegistrNbr__c, '+
    									' VersionCode__c '+
    									' FROM VRE_VehRel__c '+
    									' WHERE Status__c =: active'+
    									' AND Account__r.RecordTypeId =: personalAccount_recordTypeId'+
    									' AND EndDateRelation__c = null ';
		Boolean isQuerAdded = false;
		if( licensePlate != null && licensePlate != '' ){
			licensePlate += '%';
			query_VehicleRelations += 'AND (VehicleRegistrNbr__c LIKE : licensePlate ';
			isQuerAdded = true;
		}
		if( frame != null && frame != '' ){
			frame = '%' + frame + '%';
			if( isQuerAdded == true ){
				query_VehicleRelations += ' OR VIN__r.Name LIKE : frame )';
			}
			else{
				query_VehicleRelations += ' AND VIN__r.Name LIKE : frame';
			}
		} else {
			if (isQuerAdded)
				query_VehicleRelations += ')';
		}
    	
    	system.debug('*** query_VehicleRelations=' + query_VehicleRelations);

    	lstVehicleRelationRecords = Database.query( query_VehicleRelations );
    	
    	return lstVehicleRelationRecords;
    }
    
    /**
    *	This method was used to get Vehicle Relation records based on Account's IDBIR value.
    *	@param	numberBIR	-	Account's IDBIR value.
    *	@return lstVehicleRelationRecords	-	fetch and return the result in lstVehicleRelationRecords list.
    **/    
    public List<VRE_VehRel__c> fetchVREUsingAccountIDBIR( String numberBIR ){

    	List<VRE_VehRel__c> lstVehicleRelationRecords = null;
    	
    	lstVehicleRelationRecords = [SELECT 
    									Id, Name, DateOfEntryInStock__c, Account__c,
    									VIN__c,VIN__r.Id, VIN__r.Name, VIN__r.Model__c, VIN__r.LastModifiedDate,
    									VIN__r.VersionCode__c, VIN__r.Version__c, VIN__r.DateofManu__c, VIN__r.ModelYear__c, VIN__r.Color__c,
    									VIN__r.Optional__c , VIN__r.Price__c , VIN__r.Status__c
    								FROM
    									VRE_VehRel__c
    								WHERE
    									Account__c IN ( SELECT Id FROM Account WHERE IDBIR__c =: numberBIR )
    									AND VIN__r.Status__c <> 'Billed'
    								];
    	return lstVehicleRelationRecords;
    }
    
    
    /**
    *	This method was used to get the VehiclesRelation records using its VIN number.
    *	@param	
    *	@return	mapVehiclesRelations	-	return the list on Vehicle records
    **/ 
    public Map<String, VRE_VehRel__c> fetchVehicleRelationUsingVIN( Set<String> setVIN, Id dealerId ){
    	Map<String, VRE_VehRel__c> mapVehiclesRelations = new Map<String, VRE_VehRel__c>(); 
    	List<VRE_VehRel__c> lstVehicleRelationRecords = null;

    	lstVehicleRelationRecords = [SELECT 
    									Id, Name, DateOfEntryInStock__c, Account__c,
    									VIN__c,VIN__r.Id, VIN__r.Name, VIN__r.Model__c, VIN__r.LastModifiedDate,
    									VIN__r.VersionCode__c, VIN__r.Version__c, VIN__r.DateofManu__c, VIN__r.ModelYear__c, VIN__r.Color__c,
    									VIN__r.Optional__c , VIN__r.Price__c , VIN__r.Status__c
    								FROM
    									VRE_VehRel__c
    								WHERE
    									VIN__r.Name IN : setVIN
    									AND Account__c =: dealerId
    								];
    	system.Debug(' lstVehicleRelationRecords -->'+lstVehicleRelationRecords);
    	for (VRE_VehRel__c vehRel: lstVehicleRelationRecords) {
    		mapVehiclesRelations.put(vehRel.VIN__r.Name, vehRel);
    	}
    	return mapVehiclesRelations;
    }
    
    
    /**
    *	This method was used to get the VehiclesRelation records using Vehicle Ids
    *	@param	
    *	@return	mapVehiclesRelations	-	return the list on Vehicle records
    **/ 
    public Map<String, VRE_VehRel__c> fetchVehicleRelationUsingVehicleIds(Id dealerId, List<Id> lstVehIds){
    	Map<String, VRE_VehRel__c> mapVehiclesRelations = new Map<String, VRE_VehRel__c>(); 
    	List<VRE_VehRel__c> lstVehicleRelationRecords = null;

    	lstVehicleRelationRecords = [SELECT 
    									Id, Name, DateOfEntryInStock__c, Account__c, Account__r.Name,
    									VIN__c,VIN__r.Id, VIN__r.Name, VIN__r.Model__c, VIN__r.LastModifiedDate,
    									VIN__r.VersionCode__c, VIN__r.Version__c, VIN__r.DateofManu__c, VIN__r.ModelYear__c, VIN__r.Color__c,
    									VIN__r.Optional__c , VIN__r.Price__c , VIN__r.Status__c
    								FROM
    									VRE_VehRel__c
    								WHERE
    									    Account__c =: dealerId
    									AND VIN__r.Id IN : lstVehIds
    								];
    	system.Debug('*** lstVehicleRelationRecords -->'+lstVehicleRelationRecords);
    	for (VRE_VehRel__c vehRel: lstVehicleRelationRecords) {
    		mapVehiclesRelations.put(vehRel.VIN__r.Name, vehRel);
    	}
    	return mapVehiclesRelations;
    }
    
    public List<VRE_VehRel__c> fetchVehicleRelationActiveUsingVehicleIds(List<Id> lstVehIds){
    	List<VRE_VehRel__c> lstVehicleRelationRecords = null;

    	lstVehicleRelationRecords = [SELECT 
    									Id, Name, DateOfEntryInStock__c, Account__c, Account__r.RecordTypeId,
    									VIN__c,VIN__r.Id, VIN__r.Name, VIN__r.Model__c, VIN__r.LastModifiedDate,
    									VIN__r.VersionCode__c, VIN__r.Version__c, VIN__r.DateofManu__c, VIN__r.ModelYear__c, VIN__r.Color__c,
    									VIN__r.Optional__c , VIN__r.Price__c , VIN__r.Status__c
    								FROM
    									VRE_VehRel__c
    								WHERE
    									Status__c = 'Active'
    									AND VIN__r.Id IN : lstVehIds
    								ORDER BY 
    									CreatedDate DESC
    								];
    	
    	return lstVehicleRelationRecords;
    }
    

}