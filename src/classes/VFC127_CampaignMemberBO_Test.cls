/**
 * We must have access to a user with specific profiles
 */
@IsTest(SeeAllData=true)
private class VFC127_CampaignMemberBO_Test
{
    private static testMethod void unitTest01_MKT()
    {
        // Test Data
        User userMKT = [SELECT id, profileId, profile.name, userRoleId 
                          FROM User
                         WHERE profile.name IN :VFC126_CampaignConfig.setMarketingProfiles and isActive = true
                         limit 1];

        Lead lead = VFC129_UTIL_TestData.createLead();
        insert lead;

        system.runAs(userMKT)
        {
            Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_XPTO_RENAULT_1');
            campaignParent.ManualInclusionAllowed__c = false;
            insert campaignParent;
            
            Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_XPTO_RENAULT_S1', null, campaignParent.Id);
            insert campaignSingle;
        
            CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);

            // Start test
            Test.startTest();
    
            // Execute tests
            try {
                VFC127_CampaignMemberBO.getInstance().checkIfProfileCanInsert(new List<CampaignMember>{campaignMember});
            }
            catch (Exception e) {
                system.assert(e.getMessage().contains('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha'));
            }
        }

        // Stop test
        Test.stopTest();
    }
    
    private static testMethod void unitTest02_Dealer()
    {
        // Test Data
        User userDealer = [SELECT id, profileId, profile.name, userRoleId 
                             FROM User
                            WHERE profile.name IN :VFC126_CampaignConfig.setDealerProfiles and isActive = true
                            limit 1];

        Lead lead = VFC129_UTIL_TestData.createLead();
        insert lead;

        system.runAs(userDealer)
        {
            Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_XPTO_RENAULT_1');
            campaignParent.ManualInclusionAllowed__c = false;
            insert campaignParent;

            Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_XPTO_RENAULT_S1', null, campaignParent.Id);
            insert campaignSingle;
        
            CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);

            // Start test
            Test.startTest();
    
            // Execute tests
            try {
                VFC127_CampaignMemberBO.getInstance().checkIfProfileCanInsert(new List<CampaignMember>{campaignMember});
            }
            catch (Exception e) {
                system.assert(e.getMessage().contains('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha'));
            }
        }

        // Stop test
        Test.stopTest();
    }
    
    private static testMethod void unitTest02_MKT(){
        
        // Test Data
        User userMKT = [SELECT id, profileId, profile.name, userRoleId 
                        FROM User
                        WHERE profile.name IN :VFC126_CampaignConfig.setMarketingProfiles and isActive = true
                        limit 1];
        
        Lead lead = VFC129_UTIL_TestData.createLead();
        insert lead;
        
        system.runAs(userMKT)
        {
            Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_XPTO_RENAULT_1');
            campaignParent.ManualInclusionAllowed__c = false;
            insert campaignParent;
            
            Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_XPTO_RENAULT_S1', null, campaignParent.Id);
            insert campaignSingle;
            
            CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);
            
            // Start test
            Test.startTest();
            
            // Execute tests
            try {
                VFC127_CampaignMemberBO.getInstance().checkIfProfileCanDelete(new List<CampaignMember>{campaignMember});
            }
            catch (Exception e) {
                system.assert(e.getMessage().contains('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha'));
            }
        }
        
        // Stop test
        Test.stopTest();
    }
    
    private static testMethod void unitTest03_MKT(){
        
        // Test Data
        User userMKT = [SELECT id, profileId, profile.name, userRoleId 
                        FROM User
                        WHERE profile.name IN :VFC126_CampaignConfig.setMarketingProfiles and isActive = true
                        limit 1];
        
        Lead lead = VFC129_UTIL_TestData.createLead();
        insert lead;
        
        system.runAs(userMKT)
        {
            Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_XPTO_RENAULT_1');
            campaignParent.ManualInclusionAllowed__c = false;
            insert campaignParent;
            
            Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_XPTO_RENAULT_S1', null, campaignParent.Id);
            insert campaignSingle;
            
            CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);
            
            // Start test
            Test.startTest();
            
            // Execute tests
            try {
                VFC127_CampaignMemberBO.getInstance().removeExistingCM(new List<CampaignMember>{campaignMember});
            }
            catch (Exception e) {
                system.assert(e.getMessage().contains('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha'));
            }
        }
        
        // Stop test
        Test.stopTest();
    }
    
    private static testMethod void unitTest04_MKT(){
        
        // Test Data
        User userMKT = [SELECT id, profileId, profile.name, userRoleId 
                        FROM User
                        WHERE profile.name IN :VFC126_CampaignConfig.setMarketingProfiles and isActive = true
                        limit 1];
        
        Account dealer = new Account(
            Name='Dealer teste',
            RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
            IDBIR__c = '1234567'
        );
        Insert dealer;            
        
        Lead lead = VFC129_UTIL_TestData.createLead();
        lead.DealerOfInterest__c = dealer.Id;
        lead.Email = 'teste@test.com.br';
        lead.Phone = '9343434343';
        insert lead;
        
        system.runAs(userMKT)
        {
            Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_XPTO_RENAULT_1');
            campaignParent.ManualInclusionAllowed__c = false;
            insert campaignParent;
            
            Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_XPTO_RENAULT_S1', null, campaignParent.Id);
            campaignSingle.WebToOpportunity__c = true;
            insert campaignSingle;
            
            CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);
            
            // Start test
            Test.startTest();
            
            // Execute tests
            try {
                VFC127_CampaignMemberBO.getInstance().checkIfShouldConvertLeadIntoOpportunity(new List<CampaignMember>{campaignMember});
            }catch (Exception e) {
                system.debug('$$$ exception: ' + e);
                system.assert(e.getMessage().contains('Campaign: a campanha não permite que o perfil deste usuário crie membros de campanha'));
            }
        }
        
        // Stop test
        Test.stopTest();
    }
}