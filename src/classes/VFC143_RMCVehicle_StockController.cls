public with sharing class VFC143_RMCVehicle_StockController {
  
  public String modelName{get;set;}
  public List< VRE_VehRel__c > vehicleList{get;set;}
  public String vehiclePrefix{
    get{return VRE_VehRel__c.SObjectType.getDescribe().getKeyPrefix();}
  }
  
  public VFC143_RMCVehicle_StockController () {
    vehicleList = new List< VRE_VehRel__c >();
  }
  
  public void selectVehicle(){
    User currUser = [select BIR__c from User where Id = :Userinfo.getUserId()];
    Account parentAcc = [select parent.Id from Account where IDBIR__c = :currUser.BIR__c and parent.Id != null limit 1].parent;
    List< Account > accList = [select Id from Account where ParentId = :parentAcc.Id];
    System.debug( '@@@\n' + Json.serializePretty( accList ) );
    vehicleList = [
      select Account__r.IDBIR__c, Days_in_Stock__c, VIN__r.Color__c, VIN__r.Name,
        VIN__r.Booking_User__r.Name, VIN__r.Model__c,
        VIN__r.Optional__c, VIN__r.Price__c, VIN__r.Status__c, VIN__r.Version__c, VIN__r.Year_Model_Version__c
      from VRE_VehRel__c
      where VIN__r.Model__c like :('%'+modelName+'%')
        and Account__c in :accList
        and VIN__r.Status__c != 'Billed'
        and VRE_VehRel__c.Status__c = 'Active' 
      order by VRE_VehRel__c.Days_in_Stock__c desc
    ];
  }
}