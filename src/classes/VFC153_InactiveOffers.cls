/*
  Proj: Retail plataform
*/
public with sharing class VFC153_InactiveOffers {
    
    private List<PVCall_Offer__c> callOfferList;
    private List<Offer__c> OfferList;
    private List<PVCommercial_Action__c> commercialActionList;
    
    
    public VFC153_InactiveOffers(){}
    
    
    public VFC153_InactiveOffers(List<PVCommercial_Action__c> commercialActionList){
        this.commercialActionList = commercialActionList;
        this.callOfferList = getCallOffer(commercialActionList);
        this.OfferList = getOffer(callOfferList);
    }
    
    
    public VFC153_InactiveOffers(List<PVCall_Offer__c> callOfferList){
        this.callOfferList = callOfferList;
        this.OfferList = getOffer(callOfferList);
    }
    
    
    
    public List<PVCall_Offer__c> getCallOffer(List<PVCommercial_Action__c> commercialActionList){
        return [select id,Commercial_Action__c from PVCall_Offer__c where Commercial_Action__c in:(commercialActionList)];
    }
    
    public List<Offer__c> getOffer(List<PVCall_Offer__c> callOfferList){
		return [select id from Offer__c where Call_Offer__c in:(callOfferList) and Status__c <> 'INACTIVE'];        
    }
    
    
    
    public void inativaFluxoDeTrabalho(List<sObject> objectList){
      Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
      List<ProcessInstanceWorkitem> workitem = new List<ProcessInstanceWorkitem>([select Id from ProcessInstanceWorkitem where ProcessInstanceId in(select Id from ProcessInstance where Status = 'Pending' and TargetObjectId in:objectList)]);
      for(integer b =0; b<workitem.size();b++){
        if ((workitem != null) && (workitem.size() > 0)){
          pwr.setWorkItemId(workitem[b].id);
          pwr.setAction('Removed'); 
          Approval.ProcessResult pr = Approval.process(pwr);
        }
      }
    }
    
    
    
    public void inativaOffer(){
        for(Offer__c offer : OfferList)
            offer.Status__c = 'INACTIVE';
        Database.update(OfferList,true);
    }
    
    public void inativaCallOffer(){
        for(PVCall_Offer__c callOffer: callOfferList)
            callOffer.Status__c = 'Inactive';
        Database.update(callOfferList,true);
    }
    
    
    
    
    public void inativarChamadaDeOfertaFactory(){
         inativaFluxoDeTrabalho(callOfferList);
         inativaCallOffer(); 

    }
    
    public void inativarOfertaFactory(){
        inativaFluxoDeTrabalho(OfferList);
        inativaOffer();

    }
    
}