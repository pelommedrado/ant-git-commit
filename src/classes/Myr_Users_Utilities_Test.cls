/** Apex Class used to test Myr_Users_Utilities
	@author S. Ducamp
	@date 29.07.2015
	@version 1.0
 */
@isTest
private class Myr_Users_Utilities_Test {
	
	@testsetup static void setCustomSettings() {
   		Myr_Datasets_Test.prepareRequiredCustomSettings();
  	}
	
	//Customer Identification Number = France (no one)
	static testMethod void test_CIN_France() {
		Country_Info__c countryInf_FRA = Country_Info__c.getInstance('France');
    	system.assertEquals(null, countryInf_FRA.Myr_CustomerIdentificationNbr_Pattern__c);
    	
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('France', 'truc bidule')); //no CIN
	}
	
	//Customer Identification Number = BRAZIL
	//Generator: http://www.geradorcpf.com/
    static testMethod void test_CIN_Brazil() {
    	Country_Info__c countryInf_BRA = Country_Info__c.getInstance('Brazil');
    	system.assertNotEquals(null, countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c);
    	
    	system.assertEquals('not the proper format ['+countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', 'truc bidule')); //KO: 11 digits expected
        system.assertEquals('not the proper format ['+countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '12345678') ); //KO not 11 digits
        system.assertEquals('not the proper format ['+countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '1234567834234234') ); //KO not 11 digits
        system.assertEquals('not the proper format ['+countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '123C5678A09') ); //KO not 11 digits
        system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '12345678901')); //OK
        system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '76401361999')); //OK
        system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '04357788897')); //OK
        system.assertEquals('not the proper format ['+countryInf_BRA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Brazil', '043.577.888-97') ); //KO not 11 digits
    }
    
    //Customer Identification Number = ITALY
    //Generator: http://zip-codes.nonsolocap.it/codice-fiscale/
    static testMethod void test_CIN_ITALY() {
    	Country_Info__c countryInf_ITA = Country_Info__c.getInstance('Italy');
    	system.assertNotEquals(null, countryInf_ITA.Myr_CustomerIdentificationNbr_Pattern__c);
    	
    	system.assertEquals('not the proper format ['+countryInf_ITA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMNGL67C56B80')); //KO: not enough characters
    	system.assertEquals('not the proper format ['+countryInf_ITA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMNGL67C56B808GTG6')); //KO: too many chracters
    	system.assertEquals('not the proper format ['+countryInf_ITA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMN3L67C56B808G')); //KO the first 6 characters are not exclusively alpha
    	system.assertEquals('not the proper format ['+countryInf_ITA.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMN3L67C56B80_G')); //KO: special characters
    	
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTARRT71M23G795Y')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTAVNT87H51H501S')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTCNTN67A19B619R')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMNGL67C56B808G')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BTMNGL67C56B808G')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Italy', 'BBSTFK60M19Z352W')); //OK
    	
    }
	
	//Customer Identification Number = ROMANIA
	//Generator: http://redoxwap.freehostia.com/cnpgenerator.php
    static testMethod void test_CIN_Romania() {
    	Country_Info__c countryInf_ROM = Country_Info__c.getInstance('Romania');
    	system.assertNotEquals(null, countryInf_ROM.Myr_CustomerIdentificationNbr_Pattern__c);
    	
    	system.assertEquals('not the proper format ['+countryInf_ROM.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', 'truc bidule')); //KO: 13 digits expected
    	system.assertEquals('not the proper format ['+countryInf_ROM.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1234567')); //KO: not 13 digits expected
    	system.assertEquals('not the proper format ['+countryInf_ROM.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1234567890123456')); //KO: not 13 digits expected
    	system.assertEquals('not the proper format ['+countryInf_ROM.Myr_CustomerIdentificationNbr_Pattern__c+'] is expected', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1234C678A0123')); //KO: not 13 digits expected
    	
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1950526338284')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '2910720231113')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1950117377591')); //OK
    	system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '2920221212113')); //OK
		system.assertEquals(null, Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1800401237751')); //OK
    	
    	system.assertEquals('not the proper format [control digit is not correct]', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '1950526338287')); //KO
    	system.assertEquals('not the proper format [control digit is not correct]', Myr_Users_Utilities_Class.checkCustomerIdentNbr('Romania', '2920221212118')); //KO
    }

	//Test that the get profiles methods work: just for unit tests and sufficent unitary code coverage
	static testMethod void test_GetProfiles() {
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosReadOnlyProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosTechniqueProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosCommunityProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosCustomerMessageProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosDealerProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosViewerBusinessProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosUpdateBusinessProfile() );
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosLocalAdministratorProfile() );
		Boolean notFoundException = false;
		try {
			Myr_Users_Utilities_Class.getProfile('FakeStrangeTestProfile');
		} catch (Exception e) {
			notFoundException = true;
		}
		system.assertEquals( true, notFoundException);
	}

	//Test get HeliosTechnique Permission set: just for unit tests and sufficent unitary code coverage
	static testMethod void test_GetTechniquePermSet() {
		//existing perm set
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getHeliosTechniquePermSet() );
		//fake perm set
		CS04_MYR_Settings__c setting = [SELECT Id, Myr_PermissionSet_HeliosTechnique__c FROM CS04_MYR_Settings__c];
		setting.Myr_PermissionSet_HeliosTechnique__c = 'fake perm set';
		update setting;
		Boolean notFoundException = false;
		try {
			Myr_Users_Utilities_Class.getHeliosTechniquePermSet();
		} catch (Exception e) {
			notFoundException = true;
		}
		system.assertEquals( true, notFoundException);
	}

	//Test the record types
	static testMethod void test_GetRecordTypes() {
		//Get dealer record type ok
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getDealerRecordTypeId() );
		//Get dealer record type KO
		CS04_MYR_Settings__c setting = [SELECT Id, Myr_Personal_Account_Record_Type__c, Myr_Dealer_Record_Type__c FROM CS04_MYR_Settings__c];
		setting.Myr_Dealer_Record_Type__c = 'fake_record_type';
		update setting;
		system.assertEquals( null, Myr_Users_Utilities_Class.getDealerRecordTypeId() );
		//Get dealer record type ok with default
		setting.Myr_Dealer_Record_Type__c = null;
		update setting;
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getDealerRecordTypeId() );
		// -------------------------------------------------------------------------------------------
		//Get Personal Account record type ok
		system.assertNotEquals( null, Myr_Users_Utilities_Class.getPersAccRecordTypeId() );
		//Get Personal Account record type KO
		setting.Myr_Personal_Account_Record_Type__c = 'fake_record_type';
		update setting;
		system.assertEquals( null, Myr_Users_Utilities_Class.getPersAccRecordTypeId() );
	}

	//Test the user creation
	static testMethod void test_UserCreation() {
		//Get Technical user
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('France', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );

		Account acc = Myr_Datasets_Test.insertPersonalAccounts(1, Myr_Datasets_Test.UserOptions.NO_USERS)[0];
		acc = [SELECT Id, Firstname, Lastname, Name, MyRenaultID__c, PersonContactId, Salutation, country__c FROM Account WHERE Id = :acc.Id];
		system.runAs( Myr_Datasets_Test.getTechnicalUser('France') ) {
			User usr = Myr_Users_Utilities_Class.createUser( acc, Myr_MyRenaultTools.Brand.Renault.name(), 'GMT', 'fr_FR_EURO', 'fr', 'EUR');
			system.assertNotEquals( null, usr.Id);
		}
	}

	//Username availability: ok
	static testMethod void test_UserNameAvailability_OK() {
		String username = 'testFakeEmailAtos@rhelios.1234.renault.com';
		Boolean available = Myr_Users_Utilities_Class.isUsernameAvailableAlThroughOrgs( 'testFakeEmailAtos@rhelios.1234.renault.com' );
		system.assertEquals( true, available );
		system.assertEquals( 0, [SELECT Id FROM User WHERE Username=:username].size() );
	}

	//Username availability: ok
	static testMethod void test_UserNameAvailability_Failed() {
		//Insert a user
		User dealerUser = Myr_Datasets_Test.getDealerUser('testFakeAtos', '1234AnyEmail', 'France');
		insert dealerUser;
		system.assertEquals( 1, [SELECT Id FROM User WHERE Username=:dealerUser.username].size() );

		Boolean available = Myr_Users_Utilities_Class.isUsernameAvailableAlThroughOrgs( dealerUser.username );
		system.assertEquals( false, available );
		system.assertEquals( 1, [SELECT Id FROM User WHERE Username=:dealerUser.username].size() );
	}

	//Test the username generated when trying to deactivate a user
	static testMethod void testCancelledName() {
		String prefix = CS04_MYR_Settings__c.getInstance().Myr_Prefix_Trash__c;

		String cancelledName = Myr_Users_Utilities_Class.generateCancelUsername('12345678');
		system.assertEquals( true, cancelledName.startsWithIgnoreCase( prefix + '.' + '12345678@' ) );
		String patt = prefix + '.' + '12345678@[\\d]{10,}.com';
		system.assertEquals( true, Pattern.matches(patt, cancelledName) );

		cancelledName = Myr_Users_Utilities_Class.generateCancelUsername('');
		system.assertEquals( true, cancelledName.startsWithIgnoreCase( prefix + '.' + '@' ) );
		patt = prefix + '.' + '@[\\d]{10,}.com';
		system.assertEquals( true, Pattern.matches(patt, cancelledName) );

		cancelledName = Myr_Users_Utilities_Class.generateCancelUsername(null);
		system.assertEquals( true, cancelledName.startsWithIgnoreCase( prefix + '.' + 'null@' ) );
		patt = prefix + '.' + 'null@[\\d]{10,}.com';
		system.assertEquals( true, Pattern.matches(patt, cancelledName) );
	}
}