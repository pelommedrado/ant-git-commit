/* Purge of accounts
  *************************************************************************************************************
  18 Mar 2016 : Creation - purge of german accounts
  *************************************************************************************************************/
global class Myr_Batch_Germany26_SCH implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executebatch(new Myr_Batch_Germany2_Account_Delete_BAT());
	}
}