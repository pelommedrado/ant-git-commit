public class DashboardUtil {

    public static Map<String, String> mapTradutor =
        Utils.getMapLabelPicklist('Event', 'Subject');

    public static String traduzirSubject(String subject) {
        final String tr = mapTradutor.get(subject);
        return tr == null ? subject : tr;
    }

    public static List<String> paramLike(List<String> paramList) {
      List<String> paramLikeList = new List<String>();
      for(String param : paramList) {
        paramLikeList.add('%' + param + '%');
      }
      return paramLikeList;
    }

    public static List<String> paramFilter(String param) {
        System.debug('paramFilter: ' + param);
        List<String> paramList = new List<String>();
        if(!String.isEmpty(param)) {
            paramList = param.split(',');
        }
        return paramList;
    }

    public static Set<String> subjectAllSet {
        get {
            Set<String> temp = new Set<String>();
            temp.add('Confirm Test Drive');
            temp.add('Perform Test Drive');
            temp.add('Confirm Visit');
            temp.add('Perform Visit');
            temp.add('Customer Service');
            temp.add('Contact');
            temp.add('Negotiation');
            temp.add('Pre-Order');
            return temp;
        }
    }

    public static List<SelectOption> atividadeSelectOption(Boolean isBdcSelected) {
        Set<String> notList = new Set<String> { 'Prospection Attendance', 'Persecution', 'Delivery Confirmation', 'Rescue' };
        Set<String> bdcList = new Set<String> { 'Persecution', 'Delivery Confirmation', 'Rescue' };

        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Event.Subject.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        if(isBdcSelected){
   			for(Schema.PicklistEntry f : ple) {
                if(bdcList.contains(f.getValue()))
                    options.add(new SelectOption(f.getValue() + ',', f.getLabel()));
            }

        } else {
            for(Schema.PicklistEntry f : ple) {
                if(!notList.contains(f.getValue()) && subjectAllSet.contains(f.getValue()))
                    options.add(new SelectOption(f.getValue() + ',', f.getLabel()));
            }

        }

        return options;
    }

    public static List<SelectOption> modeloSelectOption() {
        List<SelectOption> li = new List<SelectOption>();
        li.add(new SelectOption('CAPTUR,', 'CAPTUR'));
        li.add(new SelectOption('DUSTER,', 'DUSTER'));
        li.add(new SelectOption('DUSTER OROCH,', 'OROCH'));
        li.add(new SelectOption('FLUENCE,', 'FLUENCE'));
        li.add(new SelectOption('KANGOO,', 'KANGOO'));
        li.add(new SelectOption('KANGOO EXPRESS,', 'KANGOO EXPRESS'));
        li.add(new SelectOption('KWID,', 'KWID'));
        li.add(new SelectOption('LOGAN,', 'LOGAN'));
        li.add(new SelectOption('MASTER,', 'MASTER'));
        li.add(new SelectOption('MASTER - MSU,', 'MASTER - MSU'));
        li.add(new SelectOption('MASTER MINIBUS,', 'MASTER MINIBUS'));
        li.add(new SelectOption('SANDERO,', 'SANDERO'));
        li.add(new SelectOption('SANDERO STEPWAY,', 'STEPWAY'));

        /*List<Product2> listProduct = [
            SELECT Id, Model__c, Model__r.Name
            FROM Product2
            WHERE RecordType.DeveloperName = 'PDT_ModelVersion'  AND IsActive = true
            ORDER BY Model__r.Name

            //SELECT Id, Model__c, Model__r.Name FROM Product2
            //WHERE RecordTypeId =: Utils.getRecordTypeId('Product2','PDT_ModelVersion')
        ];

        Set<String> modelosSet = new Set<String>();
        for(Product2 pd : listProduct) {
            if(!String.isEmpty(pd.Model__c)) {
                modelosSet.add(pd.Model__r.Name);
            }
        }*/
        /*final List<String> modelList = ('CAPTUR, DUSTER, OROCH, FLUENCE, KANGOO EXPRESS,'
          + 'KWID, LOGAN, MASTER, MASTER - MSU, MASTER MINIBUS, SANDERO, STEPWAY').split(',');
        modelList.sort();
        for(String st : modelList) {
            li.add(new SelectOption(st.trim() + ',', st));
        }*/
        return li;
    }

    public static List<User> obterSeller(String accDealer) {
        final List<User> userSeller = new List<User>();

        final Set<Id> userPermission =
            ProfileUtils.userPermission(ProfileUtils.permissionSeller);

        final List<User> userL = [
            SELECT Id, Name FROM User
            WHERE AccountId =: accDealer AND IsActive = TRUE
        ];

        for(User u : userL) {
            if(userPermission.contains(u.Id)) {
                userSeller.add(u);
            }
        }
        return userSeller;
    }
}