public class ObjetivoSellerController extends ObjetivoAbstractController {
    
    public Integer tClio  { get;set; }
    public Integer tDuster  { get;set; }
    public Integer tDusterOr  { get;set; }
    public Integer tFluence  { get;set; }
    public Integer tKangoo  { get;set; }
    public Integer tLogan  { get;set; }
    public Integer tMaster  { get;set; }
    public Integer tSandero  { get;set; }
    public Integer tSanderoRs  { get;set; }
    public Integer tSanderoSt { get;set; }
    
    public List<ObjetivoSeller> objSellerList { get;set; }
    
    private Map<String, ObjetivoSeller> mapSeller = new Map<String, ObjetivoSeller>();
    
    public ObjetivoSellerController() {
        init();
        updateData();
    }
    
    public override void updateMgd() {
        this.objSellerList = new List<ObjetivoSeller>();
        
        this.mapSeller = new Map<String, ObjetivoSeller>();
       
        for(Monthly_Goal_Dealer__c d : mgd) {
            ObjetivoSeller oS = new ObjetivoSeller();
            oS.dealer = d;
            mapSeller.put(d.Id, oS);
            this.objSellerList.add(oS);
        }
        
        List<Monthly_Goal_Seller__c> sellerList = [
            SELECT Id, Seller__r.Name, Monthly_Goal_Dealer__c, General_Objetive__c,
            Clio_Proposal__c, Duster_Proposal__c, Duster_Oroch_Proposal__c,
            Fluence_Proposal__c, Kangoo_Proposal__c, Logan_Proposal__c,
            Master_Proposal__c, Sandero_Proposal__c, Sandero_RS_Proposal__c, 
            Sandero_Stepway_Proposal__c
            FROM Monthly_Goal_Seller__c
            WHERE Monthly_Goal_Dealer__c =: mapSeller.keySet()
        ];
        
        for(Monthly_Goal_Seller__c s : sellerList) {
            ObjetivoSeller oS =  mapSeller.get(s.Monthly_Goal_Dealer__c);
            oS.sellers.add(s);
        }
        
        somar();
    }
    
    private void somar() {
        this.total = 0;
        this.tClio  =0;
        this.tDuster  =0;
        this.tDusterOr  =0;
        this.tFluence  =0;
        this.tKangoo  =0;
        this.tLogan  =0;
        this.tMaster  =0;
        this.tSandero  =0;
        this.tSanderoRs  =0;
        this.tSanderoSt =0;
        
        for(ObjetivoSeller os :objSellerList) {
            
            for(Monthly_Goal_Seller__c s : os.sellers) {
                this.tClio += (Integer) s.Clio_Proposal__c;
                this.tDuster += (Integer) s.Duster_Proposal__c;
                this.tDusterOr += (Integer) s.Duster_Oroch_Proposal__c;
                this.tFluence += (Integer) s.Fluence_Proposal__c;
                this.tKangoo += (Integer) s.Kangoo_Proposal__c;
                this.tLogan += (Integer) s.Logan_Proposal__c;
                this.tMaster += (Integer) s.Master_Proposal__c;
                this.tSandero += (Integer) s.Sandero_Proposal__c;
                this.tSanderoRs += (Integer) s.Sandero_RS_Proposal__c;
                this.tSanderoSt += (Integer) s.Sandero_Stepway_Proposal__c;
                
                this.total += (Integer)s.General_Objetive__c;
            }
        }
        
        if(this.total < ((Integer) this.mgg.General_Objetive__c) ) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'O valor distribuido ' + total + ' é menor que o valor proposto ' +  this.mgg.General_Objetive__c) );
        }
    }
    
    public void distribuirValores() {
        if(this.mgd.isEmpty()) {
            return;
        }
        
        this.total = 0;
        for(ObjetivoSeller os :objSellerList) {
            
            Integer va = 0;
            if(os.sellers.size() > 0) {
                va = (Integer) os.dealer.General_Objetive__c / os.sellers.size();
            }
            Integer to = 0;
                     
            for(Monthly_Goal_Seller__c s : os.sellers) {
                s.General_Objetive__c = va;
                to += va;
                
                Decimal newTotal = updateLine(s);
                s.General_Objetive__c = newTotal;
                
                this.total += (Integer) newTotal;
                this.tClio += (Integer) s.Clio_Proposal__c;
                this.tDuster += (Integer) s.Duster_Proposal__c;
                this.tDusterOr += (Integer) s.Duster_Oroch_Proposal__c;
                this.tFluence += (Integer) s.Fluence_Proposal__c;
                this.tKangoo += (Integer) s.Kangoo_Proposal__c;
                this.tLogan += (Integer) s.Logan_Proposal__c;
                this.tMaster += (Integer) s.Master_Proposal__c;
                this.tSandero += (Integer) s.Sandero_Proposal__c;
                this.tSanderoRs += (Integer) s.Sandero_RS_Proposal__c;
                this.tSanderoSt += (Integer) s.Sandero_Stepway_Proposal__c;
            }
            
            Integer resto = (Integer) os.dealer.General_Objetive__c - to;
            if(resto > 0 && os.sellers.size() > 0) {
                Monthly_Goal_Seller__c s =
                    os.sellers.get(os.sellers.size()-1);
                s.General_Objetive__c += resto;
            }
        }
        
        somar();
    }

    private Decimal updateLine(Monthly_Goal_Seller__c s) {
        /*s.Clio_Proposal__c 	 		  = s.General_Objetive__c * (this.mgg.Clio_Proposal_percent__c/100);
        s.Duster_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Duster_Proposal_percent__c/100);
        s.Duster_Oroch_Proposal__c 	  = s.General_Objetive__c * (this.mgg.Duster_Oroch_Proposal_percent__c/100);
        s.Fluence_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Fluence_Proposal_percent__c/100);
        s.Kangoo_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Kangoo_Proposal_percent__c/100);
        s.Logan_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Logan_Proposal_percent__c/100);
        s.Master_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Master_Proposal_percent__c/100);
        s.Sandero_Proposal__c 		  = s.General_Objetive__c * (this.mgg.Sandero_Proposal_percent__c/100);
        s.Sandero_RS_Proposal__c 	  = s.General_Objetive__c * (this.mgg.Sandero_RS_Proposal_percent__c/100);
        s.Sandero_Stepway_Proposal__c = s.General_Objetive__c * (this.mgg.Sandero_Stepway_Proposal_percent__c/100);
        */
        s.Clio_Proposal__c 	 		  = calc(s.General_Objetive__c, this.mgg.Clio_Proposal_percent__c);
        s.Duster_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Duster_Proposal_percent__c);
        s.Duster_Oroch_Proposal__c 	  = calc(s.General_Objetive__c, this.mgg.Duster_Oroch_Proposal_percent__c);
        s.Fluence_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Fluence_Proposal_percent__c);
        s.Kangoo_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Kangoo_Proposal_percent__c);
        s.Logan_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Logan_Proposal_percent__c);
        s.Master_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Master_Proposal_percent__c);
        s.Sandero_Proposal__c 		  = calc(s.General_Objetive__c, this.mgg.Sandero_Proposal_percent__c);
        s.Sandero_RS_Proposal__c 	  = calc(s.General_Objetive__c, this.mgg.Sandero_RS_Proposal_percent__c);
        s.Sandero_Stepway_Proposal__c = calc(s.General_Objetive__c, this.mgg.Sandero_Stepway_Proposal_percent__c);
        
        
        Decimal newTotal = 0;  
        newTotal += s.Clio_Proposal__c;
        newTotal += s.Duster_Proposal__c;
        newTotal += s.Duster_Oroch_Proposal__c;
        newTotal += s.Fluence_Proposal__c;
        newTotal += s.Kangoo_Proposal__c;
        newTotal += s.Logan_Proposal__c;
        newTotal += s.Master_Proposal__c;
        newTotal += s.Sandero_Proposal__c;
        newTotal += s.Sandero_RS_Proposal__c;
        newTotal += s.Sandero_Stepway_Proposal__c;
       
        return newTotal;
    }
    
    public void updateValoresSeller() {
        String param = Apexpages.currentPage().getParameters().get('acc');
        
        for(ObjetivoSeller os :objSellerList) {
            for(Monthly_Goal_Seller__c s : os.sellers) {
                if(!String.isEmpty(param) && s.Id != param) {
                    continue;
                }
                Decimal newTotal = updateLine(s);
                s.General_Objetive__c = newTotal;
            }
        }
        somar();
    }   
    
    public void salvar() {
        if(this.mgd.isEmpty()) {
            return;
        }
        somar();
        if(this.total < ((Integer) this.mgg.General_Objetive__c) ) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'O valor distribuido ' + total + ' é menor que o valor proposto ' +  this.mgg.General_Objetive__c) );
            return;
        }
        
        Savepoint sp = Database.setSavepoint();
        try {         
            List<Monthly_Goal_Seller__c> sellerList = new List<Monthly_Goal_Seller__c>();
            for(ObjetivoSeller os :objSellerList) {
                sellerList.addAll(os.sellers);
            }
            
            UPDATE sellerList;
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.Confirm, 
                'Alterações salvas com sucesso. '));
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex.getMessage());
            System.debug('LINE: ' + ex.getLineNumber());
            
            Database.rollback( sp );
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'Ex: ' + ex.getMessage() ) );
            
        }
    }
    
    public PageReference atribuirDealer() {
        PageReference page = new PageReference(
            '/ObjetivoGeral?mes=' + this.mgg.Month__c + '&ano=' + this.mgg.Year__c);
        return page;
    }
    
    public class ObjetivoSeller {
        public Monthly_Goal_Dealer__c 	dealer		{ get;set; }
        public List<Monthly_Goal_Seller__c> sellers { get;set; }
        
        public ObjetivoSeller() {
            this.dealer = new  Monthly_Goal_Dealer__c();
            this.sellers = new List<Monthly_Goal_Seller__c>();
        }
    }
}