public class VFC100_PDFQuoteController {
	 
	private String quoteId;
	private Opportunity opportunity;
	public  VFC61_QuoteDetailsVO quoteDetailsVO {get;set;}	
	public  VEH_Veh__c vehicle;		
	private List<VFC83_QuoteLineItemVO> accessories;
	private String totalAccessories;
	public  List<String> optional{get;set;}	
	public  Account dealer;
	public String veiculoId{get;set;}
	public String maskPriceVehicle{get;set;}
	public List<String> maskPriceAccessories{get;set;}
	public List<String> customerAdress;
	
    public String veiculoNome { get; set; }
    public String veiculoCor { get; set; }
    public String veiculoOpcional { get; set; }
    public String unitPrice { get; set;  }
    
	public VFC100_PDFQuoteController (ApexPages.StandardController controller)
    {
        this.quoteId = controller.getId(); 
    }
	public PageReference initialize()
    {
    	Double entry;
    	Double priceUsed;
    	Double amountFinanced;
    	Double valueOfParcel;
    	this.quoteDetailsVO = VFC65_QuoteDetailsBusinessDelegate.getInstance().getQuoteDetails(this.quoteId, ''); 
        this.opportunity = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(quoteDetailsVO.opportunityId); 
        if(quoteDetailsVO == null || opportunity == null){
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Orçamento ou Oportunidade não localizada(o)'));    
        	return null;    	
        }
                
        if(String.isNotEmpty(quoteDetailsVO.entryMask)){
        	entry = Double.valueOf(quoteDetailsVO.entryMask);
        }else{
        	entry = 0.00;
        }
        if(String.isNotEmpty(quoteDetailsVO.priceUsedVehicleMask)){
        	priceUsed = Double.valueOf(quoteDetailsVO.priceUsedVehicleMask);
        }else{
        	priceUsed = 0.00;
        }
        if(String.isNotEmpty(quoteDetailsVO.amountFinancedMask)){
        	amountFinanced = Double.valueOf(quoteDetailsVO.amountFinancedMask);
        }else{
        	amountFinanced = 0.00;
        }
        if(String.isNotEmpty(quoteDetailsVO.valueOfParcelMask)){
        	valueOfParcel = Double.valueOf(quoteDetailsVO.valueOfParcelMask);
        }else{
        	valueOfParcel = 0.00;
        }
        quoteDetailsVO.entryMask = VFC69_Utility.priceFormats(entry);
       	quoteDetailsVO.priceUsedVehicleMask = VFC69_Utility.priceFormats(priceUsed);
       	quoteDetailsVO.amountFinancedMask = VFC69_Utility.priceFormats(amountFinanced);
       	quoteDetailsVO.valueOfParcelMask = VFC69_Utility.priceFormats(valueOfParcel);
        
       
        //System.debug('quoteDetailsVO :' + quoteDetailsVO);
        
        List<QuoteLineItem> quoteLineItemList = [SELECT 
					Id, 
					QuoteId, 
					Description,
					UnitPrice, 
					PricebookEntry.Id,
					Vehicle__r.Id, 
					Vehicle__r.Name,
					Vehicle__r.Status__c,
                    PricebookEntry.Pricebook2Id, 
                    PricebookEntry.Product2Id, 
                    PricebookEntry.UnitPrice,
                    PricebookEntry.Product2.Name,
                    PricebookEntry.Product2.Model__r.Name,
                    PricebookEntry.Product2.ModelYear__c, 
                    PricebookEntry.Product2.Optionals__c, 
                    PricebookEntry.Product2.Version__c,
                    PricebookEntry.Product2.Description,
                    PricebookEntry.Product2.RecordTypeId
	    		FROM 
					QuoteLineItem
				WHERE
					QuoteId =: this.quoteId
					order by Description, Vehicle__r.Name];
        
        QuoteLineItem qlI = null;
        
        if(!quoteLineItemList.isEmpty()) {
            qlI = quoteLineItemList.get(0);
            
            unitPrice = VFC69_Utility.priceFormats(qlI.UnitPrice);
        }
        
        
        
        getVehicle();
        
        if(veiculoNome == null && qlI != null) {
            //QuoteLineItem qlI = quoteLineItemList.get(0);
            
            System.debug('PricebookEntry.Product2:' + qlI.PricebookEntry.Product2);
            
            veiculoNome = 
                (qlI.PricebookEntry.Product2.Model__r.Name != null ? qlI.PricebookEntry.Product2.Model__r.Name : '') + ' ' + 
                (qlI.PricebookEntry.Product2.Version__c != null ? qlI.PricebookEntry.Product2.Version__c : '') + ' ' + 
                (qlI.PricebookEntry.Product2.ModelYear__c != null ? qlI.PricebookEntry.Product2.ModelYear__c : '');
            veiculoCor = '';
            veiculoOpcional = qlI.PricebookEntry.Product2.Optionals__c;
        }
        
     	return null;
    }
    
    public Account getDealer(){
    	 
    	Account dealer = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(opportunity.Dealer__c);
    	if(dealer != null){
    		return dealer;
    	}else{
    		return new Account();
    	}    	
    }
	
	public List<String> getCustomerAdress(){
		
		Account auxCustomerAdress = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(opportunity.AccountId);
     	List<String> adress = new List<String>();
     	
     	if(String.isNotEmpty(auxCustomerAdress.ShippingStreet)){
	     	adress.add(auxCustomerAdress.ShippingStreet);
	     	adress.add(auxCustomerAdress.ShippingCity+' '+auxCustomerAdress.ShippingState+' '+auxCustomerAdress.ShippingPostalCode+' '+auxCustomerAdress.ShippingCountry);
     	}else{
     		adress.add('');
     		adress.add('');
     	}
		return adress;
	}
     
    public List<VFC83_QuoteLineItemVO> getAccessories(){
     	
     	List<VFC83_QuoteLineItemVO>  lstQuoteLineItem = new List<VFC83_QuoteLineItemVO>();
     	maskPriceAccessories = new List<String>();
     	integer i = 0;     	
     	
     	for(VFC83_QuoteLineItemVO quoteLineItem : this.quoteDetailsVO.lstQuoteLineItemVO){
     		
     		if(quoteLineItem.item == 'Acessório' && i < 5){
     			lstQuoteLineItem.add(quoteLineItem);
     			if(quoteLineItem.unitPrice != null){
     				maskPriceAccessories.add(VFC69_Utility.priceFormats(quoteLineItem.unitPrice));
     			}else{
     				maskPriceAccessories.add(VFC69_Utility.priceFormats(0));
     			}  
     		}
     	}
     	
     	//garante que haverá 6 linhas em acessórios
     	if(lstQuoteLineItem.size() < 6){
     		
     		for(integer j=(lstQuoteLineItem.size()-1);j<6;j++){
     			lstQuoteLineItem.add(new VFC83_QuoteLineItemVO());
     			maskPriceAccessories.add(VFC69_Utility.priceFormats(0));
     		}     		
     	}
     	
     	return lstQuoteLineItem;
     }
     
     public String getTotalAccessories(){
     	
     	Double Totalprice = 0;
     	
     	for(VFC83_QuoteLineItemVO quoteLineItem : this.quoteDetailsVO.lstQuoteLineItemVO){    		
     		if(quoteLineItem.item == 'Acessório'){
     			Totalprice += quoteLineItem.unitPrice;  
     		}
     	}
     	return VFC69_Utility.priceFormats(Totalprice);
     } 
     
      public VEH_Veh__c getVehicle(){
     	
     	List<VEH_Veh__c> Vehicles = new List<VEH_Veh__c>();
     	VEH_Veh__c Vehicle = new VEH_Veh__c();
     	
        for(VFC83_QuoteLineItemVO quoteLineItem : quoteDetailsVO.lstQuoteLineItemVO)
        {
            if(String.isNotEmpty(quoteLineItem.vehicleId))
             {	
            	 
            	 Vehicles = VFC21_VehicleDAO.getInstance().fetchVehicleRecordsUsingVehicleRelationIDs(new List<Id>{quoteLineItem.vehicleId});
            	 if(Vehicles.size() > 0){
            	 	Vehicle = Vehicles.get(0);
            	 	maskPriceVehicle = VFC69_Utility.priceFormats(Vehicle.Price__c);
            	 	if(String.isNotEmpty(Vehicles.get(0).Optional__c) && Vehicles.get(0).Optional__c.length() > 98){
            	 		Vehicles.get(0).Optional__c = Vehicles.get(0).Optional__c.substring(0, 98);
            	 	}
                     
                     veiculoNome = Vehicle.Model__c + ' ' + Vehicle.Version__c + ' ' + Vehicle.Manufacturing_Year__c;
                     veiculoCor = Vehicle.Color__c;
                     veiculoOpcional = Vehicle.Optional__c;
                     
            	 	return Vehicles.get(0);
            	 }            	 
       		}
        }
        return Vehicle;
     }
}