/**
* Classe responsável por prover funcionalidades de negócio relacionadas ao objeto TDV_TestDriveVehicle__c.
* @author Felipe Jesus Silva.
*/
public class VFC76_AccessoryBO {
	
	private static final VFC76_AccessoryBO instance = new VFC76_AccessoryBO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC76_AccessoryBO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC76_AccessoryBO getInstance()
	{
		return instance;
	}
	
	public TDV_TestDriveVehicle__c getById(String id)
	{
		TDV_TestDriveVehicle__c sObjTestDriveVehicle = null;
		
		sObjTestDriveVehicle = VFC27_TestDriveVehicleDAO.getInstance().findById(id);
		
		return sObjTestDriveVehicle;
	}

}