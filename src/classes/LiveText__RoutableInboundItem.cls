/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class RoutableInboundItem {
    @InvocableVariable(label='Object Id' description='Identifier for ITR Message object to be routed.' required=true)
    global Id ObjectId;
    @InvocableVariable(label='Text Session Status' description='Optional status for conversation. If not specified, conversation will be New' required=false)
    global String Status;
    global RoutableInboundItem() {

    }
}
