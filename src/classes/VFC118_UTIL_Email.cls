public class VFC118_UTIL_Email
{
	private static final VFC118_UTIL_Email instance = new VFC118_UTIL_Email();

    private VFC118_UTIL_Email(){}

    public static VFC118_UTIL_Email getInstance(){
        return instance;
    }


    public String sendEmailHtml(String emailTo, String subject, String bodyHtml)
    {
    	String errorMessage = '';

    	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(new String[] { emailTo });
        email.setSubject(subject);
        email.setHtmlBody(bodyHtml);
        
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.Email[] { email });
    
        if (!results.get(0).isSuccess()) {
            system.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
            errorMessage = results.get(0).getErrors()[0].getMessage();
        }

        return errorMessage;
    }
}