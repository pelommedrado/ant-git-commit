@isTest
public class VehicleBooking_TriggerTest {
    
    public static testMethod void testVehBook(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        insert acc;
        
        VEH_Veh__c v = moc.criaVeiculo();
        v.Status__c = 'Booking';
        v.Name = '12345678912458966';
        v.Received_By_Stock_Full__c = true;
        insert v;
        
        VEH_Veh__c v1 = moc.criaVeiculo();
        v1.Status__c = 'Available';
        v1.Received_By_Stock_Full__c = false;
        insert v1;
        
        Opportunity opp = moc.criaOpportunity();
        insert opp;
        
        Quote q = moc.criaQuote();
        q.OpportunityId = opp.Id;
        insert q;
        
        test.startTest();
        
        VehicleBooking__c vb = new VehicleBooking__c();
        vb.Vehicle__c = v1.Id;
        vb.Quote__c = q.id;
        insert vb;
        
        vb.Status__c = 'Canceled';
        update vb;
        
        test.stopTest();
        
        
    }

}