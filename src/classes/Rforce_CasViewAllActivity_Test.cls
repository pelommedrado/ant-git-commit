/*<!--===========================================================================+
 |   Updated History                                                                  
                                                                 
 | DATE       DEVELOPER    ISCRUM ID    WORK REQUEST           DESCRIPTION                               
 | ====       =========    ==========   ===========            =========== 
 | 24/06/2014 Siva Valipi       181     Activity Management   Test class for Print Activity History. 
 |
 |  
 |
 +===========================================================================*/

@isTest(SeeAllData = true)
public  class Rforce_CasViewAllActivity_Test   
{
static testMethod void testViewAllActivity()
{
    Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
    Account Acc = new Account(Name='Acc1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street');
    insert Acc;
    Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
    insert Con;
    VEH_Veh__c veh = new VEH_Veh__c(name='VF1BGRGGGG3285766',DeliveryDate__c=date.parse('02/02/2007'));
    insert veh;    
    Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id,VIN__c=veh.Id);
    insert c;
    
            task tsk=new task();
            tsk.Priority='high';
            tsk.whatid=c.id;
            tsk.whoid=c.ContactId;
            tsk.status='completed';
            tsk.type='alert';
           // tsk.Contains_link_or_attachement__c=
          
              insert tsk;
    
      Test.StartTest();
     ApexPages.currentPage().getParameters().put('id',c.id);
     ApexPages.StandardController sc = new ApexPages.standardController(c);
     Rforce_CasViewAllActivity_Extn  obj = new Rforce_CasViewAllActivity_Extn(sc);
     obj.getWhoid();
     Test.stopTest();
     }
}