/**
*	Class   -   VFC12_AccountDAO_Test
*   Author  -   Rameshprabu
*   Date    -   29/10/2012
*    
*   #01 <RameshPrabu> <29/10/2012>
*      Created this Class using test for VFC12_AccountDAO.
*/
@isTest 
public with sharing class VFC12_AccountDAO_Test
{	
	static testMethod void VFC12_AccountDAO_Test1()
	{
        // TO DO: implement unit test
        Map<String,ID> recTypeIds = new Map<String,ID>();
    	for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where 
        					(SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc','Personal_Acc'))])
        {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        //List<Account> lstAccounts = [Select Id, FirstName, LastName,Email__c,PostalCode__c,RecordTypeId from Account where recordTypeId= :recTypeIds.get('Personal_Acc')];
		List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
		Test.startTest();
        Account acc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lstAccounts[0].Id);
        
        system.debug('acc Details...' +acc);
        System.assert(acc != null);
      
		//List<Account> lstAccounts1 =  VFC12_AccountDAO.getInstance().fetchAccountRecordsUsingName(lstAccounts[0].FirstName, lstAccounts[0].LastName);
		//system.debug('account Details...' +lstAccounts1);
        List<Account> lstAccounts3 =  VFC12_AccountDAO.getInstance().fetchAccountRecordsUsingRecordTypeId(recTypeIds.get('Personal_Acc'));
        //Account lstAccount4 = VFC12_AccountDAO.getInstance().fetchAccountUsingNameAndEmail(lstAccounts3[0].FirstName, lstAccounts3[0].LastName, lstAccounts3[0].Email__c, lstAccounts3[0].ShippingPostalCode, lstAccounts3[0].RecordTypeId);
        
        Account anAccount = VFC12_AccountDAO.getInstance().fetchAccountUsingCPF('11111111111', lstAccounts[0].RecordTypeId);
        
        List<Id> accountIds = new List<Id>();
        accountIds.add(acc.Id); 
        List<Account> lstAccountRecord = VFC12_AccountDAO.getInstance().fetchAccountRecordsUsingListIDs(accountIds);
 
        List<Account> lstNWSiteAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        
        Account accounts = VFC12_AccountDAO.getInstance().fetchNetworkSiteAccountUsingName(lstNWSiteAccounts[0].Name, lstNWSiteAccounts[0].RecordTypeId);
        String query = 'Select Id,Name from Account where Name != null';
        List<Account> accountlist = VFC12_AccountDAO.getInstance().fetchAccountsByQueryString(query);
        
        //fetchAccountForPreOrderWS
        
        Set<Id> setDealerIDs = new Set<Id>();
        setDealerIDs.add( lstNWSiteAccounts[0].Id );

        Set<String> setBirs = new Set<String>();
        setBirs.add('1000');
        
        List<Account> preOrderAccounts = VFC12_AccountDAO.getInstance().fetchAccountForPreOrderWS(setDealerIDs);
        
        //fetchDealersUsing_IDBIR
        String stringBIR = '1000';
        
        Map<Id, Account> mapDealerWithId = VFC12_AccountDAO.getInstance().fetchDealersUsing_IDBIR(stringBIR);
        
        //checkAccountsWithIDBIR
        List<Account> lstDealerWithIDBIR = VFC12_AccountDAO.getInstance().checkAccountsWithIDBIR(stringBIR);
        
        //fetchCustomersUsing_DealerId
        Map<Id, Account> mapCustomerWithId = VFC12_AccountDAO.getInstance().fetchCustomersUsing_DealerId(setDealerIDs);

        Map<String, Id> fetchDealerIdByBIRnumberSet = VFC12_AccountDAO.getInstance().fetchDealerIdByBIRnumberSet(setBirs);
        
        //fetchCustomersUsing_SetIds
        
        Set<Id> setCustomerIds = new Set<Id>();
        setCustomerIds.add( accountIds[0] );
        
        List<Account> lstCustomerWithInIDs = VFC12_AccountDAO.getInstance().fetchCustomersUsing_SetIds(setCustomerIds);
        
        //fetchAccountsUsingCityAndState
        List<Account> lstDealersWithAddress = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertDealerAccount_ForCalculateLatAndLong();
        List<Account> lstDealersInCityAndState = VFC12_AccountDAO.getInstance().fetchAccountsUsingCityAndState('Curitiba', 'PR');
        
        //fetchTimeRangeForTestDrive
        List<String> lstTimeRange = VFC12_AccountDAO.getInstance().fetchTimeRangeForTestDrive(lstDealersWithAddress[0].Id);



        Test.stopTest();
	}


	static testMethod void VFC12_AccountDAO_Test2()
	{
		// Prepare test data
        Map<String,ID> recTypeIds = new Map<String,ID>();
    	for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where 
        					(SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc','Personal_Acc'))])
        {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        
		List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        Account acc = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lstAccounts[0].Id);
        Account acc2 = VFC12_AccountDAO.getInstance().fetchAccountUsingAccountId(lstAccounts[1].Id);
        acc.CustomerIdentificationNbr__c = '88982624007';
        acc.NUMDBM_BR__c = 10101010;
        update acc;
        acc2.NUMDBM_BR__c = 10101010;
        update acc2;

		// Start test
		Test.startTest();
	
        Account fetchAccountToMerge = VFC12_AccountDAO.getInstance().fetchAccountToMerge('88982624007');
		Map<Id, Account> test01 = VFC12_AccountDAO.getInstance().returnMapWithIdAndAccount(new Set<String>{acc.Id});
		Map<Decimal, Account> test02 = VFC12_AccountDAO.getInstance().returnAccountsUsingNUMDBMField(new Set<Decimal>{10101010});
		Map<String, Account> test03 = VFC12_AccountDAO.getInstance().returnAccountsUsingCustomerIdentifyNumber(new Set<String>{'10101010'});

		// Stop test
		Test.stopTest();
	}
}