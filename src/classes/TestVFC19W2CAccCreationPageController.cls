// Test Class for Create Account - Reena J
@isTest
public with sharing class TestVFC19W2CAccCreationPageController {
    
    static testMethod void myUnitTest() {
        
        User usr = new User (LastName='ts1',
                             FirstName='Rotondo1', 
                             alias='lro',
                             Email='lrotondo@rotondo.com',
                             EmailEncodingKey='UTF-8',
                             LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US',
                             ProfileId='00eD0000001PhWZIA0',
                             TimeZoneSidKey='America/Los_Angeles', 
                             UserName='lrotondo@lrotondo.com');
        
        Test.startTest(); 
        System.runas(usr){
            Case c = new Case(Origin='Renault Site',
                              Type='Information Request',
                              SubType__c = 'Booking',
                              Status='New',
                              Priority='Normal',
                              Description='Description',
                              From__c='Customer',
                              CaseSubSource__c = 'Webform',
                              Address_Web__c = 'Address_Web__c',
                              City_Web__c = 'City_Web__c',
                              State_Web__c = 'State_Web__c'
                              );
            try {
                insert c;
            } catch(Exception e) {
            }
            
            ApexPages.currentPage().getParameters().put('caseId',c.Id); 
            //ApexPages.StandardController controller1=new ApexPages.StandardController(c);
            VFC19_W2CAccountcreationPageController tst1 = new VFC19_W2CAccountcreationPageController();
            tst1.country = 'country';
            tst1.accFirstName = 'accFirstName';
            tst1.accLastName = 'accLastName';
            
            tst1.accountRedirectPage();
        }
        
        Test.StopTest();
    }
}