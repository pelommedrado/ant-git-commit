@isTest
private class VFC80_VehicleVOTest {

    static testMethod void deveCriarVFC80_VehicleVO() {
        VFC80_VehicleVO vo = new VFC80_VehicleVO();
        vo.itemColor = 'itemColor';
        vo.itemModel = 'itemModel';
        vo.itemVersion = 'itemVersion';
        vo.itemYear = 10;
        
        System.assertEquals(10, vo.itemYear);
    }
}