@isTest 
private class WSC04_WebServiceDMSv2Test {

    static testMethod void updateStock() {
        
        test.startTest();
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        acc.IDBIR__c = '1100';
        insert acc;
        
        WSC04_WebServiceDMSv2.VeiculoEstoque vei = new WSC04_WebServiceDMSv2.VeiculoEstoque();
        
        Integer numBir = 1100;
        List<WSC04_WebServiceDMSv2.VeiculoEstoque> veiculoList = new List<WSC04_WebServiceDMSv2.VeiculoEstoque>();
        veiculoList.add(vei);
        
        WSC04_WebServiceDMSv2.updateStock(numBir, veiculoList);
        
        vei.VIN                 = '98376209815639876';
        vei.model               = 'Fluence';
        vei.version             = '1000';
        vei.manufacturingYear   = 2010;
        vei.modelYear           = 2012;
        vei.color               = 'Yellow';
        vei.optionals           = 'yes';
        vei.entryInventoryDate  = System.today() - 20;
        vei.price               = 20000;
        vei.lastUpdateDate      = System.now();
        vei.status              = 'Available';
        vei.stage               = 'In Transit';
        
        WSC04_WebServiceDMSServico wsService = new WSC04_WebServiceDMSServico();
        wsService.atualizacaoContinua(numBir, veiculoList);
        
        WSC04_WebServiceDMSv2.updateStock(numBir, veiculoList);
        vei.manufacturingYear = 4000;
        vei.modelYear = 1000;
        WSC04_WebServiceDMSv2.updateStock(numBir, veiculoList);
        vei.CostumerDeliveryDate = System.today();
        WSC04_WebServiceDMSv2.updateStock(numBir, veiculoList);
        vei.stage               = 'Nao existe';
        vei.status              = 'Nao existe';
        WSC04_WebServiceDMSv2.updateStock(numBir, veiculoList);        
        
        test.stopTest();
    }
}