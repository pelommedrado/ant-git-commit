global class AccountCaseCorrectionScheduled implements Schedulable {
	global void execute(SchedulableContext sc) {
		ID batchprocessid = Database.executeBatch(new AccountCaseCorrectionBatch(), 50);
		System.debug(batchprocessid);
	}
}