/**
* @author Vinoth Baskaran(RNTBCI)
* @date 17/07/2014
* @description This Class is an Custom Soap service and it will create case and attachement accordding to the user input.
*/
global class Rforce_CreateCaseAttach_Helios_CLS{
/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description It accepts the Casedetails from the external service.
* @param case Details
* @return boolean value according to request
*/   
    public static boolean createCase(Rforce_W2CSchema_CLS.Casedetails heliosCaseDetails,Case c){
        boolean bCasInsert;
        if(heliosCaseDetails.caseFrom!='' && heliosCaseDetails.description!=''){          
        bCasInsert=Rforce_CreateCaseMapping_CLS.insertCase(c);     
        }
    return bCasInsert;    
    }
/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description It accepts the attachment list,Casedetails and case instance and send us the cresponse as string.
* @param case Details,case insstance,attachment details
* @return String value according to request
*/     
    public static String getResponse(List<Attachment> attachLst,Case c,Integer attachCount){ 
          Case cs=[Select CaseNumber,Description,(Select Id,Name from attachments) from Case where Id=:c.Id];                                                 
          if(attachLst.size()!=attachCount){                                              
                cs.Description=cs.Description+System.label.Rforce_Attachment_Missing;
                update cs;                                                                                  
          }                          
        return System.label.Rforce_CAS_Case+' '+cs.CaseNumber+' '+System.label.Rforce_CAS_WithAttach+' '+ '' +attachCount +' ' +'Out of'+ ' '+'' +cs.attachments.size()+' '+System.label.Rforce_CAS_Attachments;                                                                                                                                         
   }
   public static String getResponseWithoutAttach(Case c){   
         Case cs1=[Select CaseNumber,Description from Case where Id=:c.Id]; 
         cs1.Description=cs1.Description+system.label.Rforce_Attachment_Missing;
         update cs1;                                                   
         return System.label.Rforce_CAS_Case+' '+cs1.CaseNumber+' '+System.label.Rforce_CAS_WithoutAttach; 
   } 
}