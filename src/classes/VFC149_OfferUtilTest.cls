@isTest
public with sharing class VFC149_OfferUtilTest {

  static testMethod void myUnitTest(){
      User manager = new User(
        
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          IsActive = true,
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = '00eD0000001PfLO',
          BIR__c ='123ABC123'
        );
     Database.insert( manager );
      
   
    
      Model__c model = new Model__c(
     //ENS__c = 'abc',
      Market__c = 'abc',
      Status__c = 'Active',
      Model_Spec_Code__c = 'ABC',
      Phase__c = '1',
      Model_PK__c = 'ABC-1'
    );
    Database.insert( model );

    PVCommercial_Action__c commercialAction = new PVCommercial_Action__c(
      Model__c = model.Id,
      Start_Date__c = System.today().addMonths( -1 ),
      End_Date__c = System.today().addMonths( 1 ),
      Type_of_Action__c = 'abc',
      Status__c = 'Active'
    );
    Database.insert( commercialAction );

    PVCall_Offer__c callOffer = new PVCall_Offer__c(
      Commercial_Action__c = commercialAction.Id,
      Call_Offer_Start_Date__c = System.today().addMonths( -1 ),
      Call_Offer_End_Date__c = System.today().addMonths( 1 ),
      Minimum_Entry__c = 100.0,
      Period_in_Months__c = 5,
      Month_Rate__c = 0.99
    );
    Database.insert( callOffer );
    
    
    
      
    Account dealerAcc = new Account(
          RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Network_Site_Acc'].Id,
          Name = 'Concession�ria teste',
          IDBIR__c = '123ABC123',
          NameZone__c = 'R2',
          OwnerId = manager.Id
        );
   Database.insert( dealerAcc );
    
    Offer_Item__c pricingTemplate = new Offer_Item__c(
        Name = 'Pricing Template', 
        RecordTypeId = Utils.getRecordTypeId('Offer_Item__c', 'Pricing_Template'),
        Code__c = 'PT'
    );
    Database.insert(pricingTemplate);
    
   Group_Offer__c group_offer = new Group_Offer__c(
       Status__c = 'Active',
       Type_of_Action__c ='Internet',
       Date_Start_Offer__c = Date.today()-9,
       Date_End_Offer__c = Date.today(),
       Type_of_Offer__c = 'Cooperada',
       Number_Offer__c = '2'
   );
   Database.insert(group_offer);
   
   Offer__c offer = new Offer__c(
        Status__c = 'Active',
        Group_Offer__c = group_offer.ID,
        Stage__c = 'Approved',
        Featured_In_Offer__c = 'Complete',
        Total_Inventory_Vehicle__c = 3,
        Minimum_Input__c = 50,
        Number_Of_Installments__c = 60,
        Monthly_Tax__c = 0.99,
        Coefficient__c = 0.0225,
        Value_From__c = 30000,
        ValueTo__c = 30000,
        hasUpgrade__c = true,
        Type_Of_Upgrade__c = 'Modelo/Vers�o',
        Entry_Value__c = 15000,
        Installment_Value__c = 545.00,
        isUpgrade__c='',
        Pricing_Template_Lookup__c = pricingTemplate.Id
   );
   Database.insert(offer);
   offer.Monthly_Tax__c = 0;   
   Database.update(offer);   
   
   /*Offer__c offer2 = new Offer__c(
        Status__c = 'Active',
        Group_Offer__c = group_offer.ID,
        Stage__c = 'Approved',
        Featured_In_Offer__c = 'Complete',
        Total_Inventory_Vehicle__c = 3,
        Minimum_Input__c = 50,
        Number_Of_Installments__c = 60,
        Monthly_Tax__c = 0.99,
        Offer_Upgrade__c = offer.id,
        Coefficient__c = 0.0225,
        Value_From__c = 30000,
        ValueTo__c = 30000,
        Entry_Value__c = 15000,
        Installment_Value__c = 545.00
        //isUpgrade__c=offer.Id
   );
   Database.insert(offer2);
   */
   VFC149_OfferUtil.CorrectionFieldWhenOfferEqualsInternet(offer);
   VFC149_OfferUtil.calculoIof(offer);
   VFC149_OfferUtil vf = new VFC149_OfferUtil();
   vf.custoEfetivoAoMes(offer); 
   Double cetm =15.2;  
   VFC149_OfferUtil.calculaCETAM(offer);
   VFC149_OfferUtil.calculaCETAA(offer,cetm);
      
  }
  
}