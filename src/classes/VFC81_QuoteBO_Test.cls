@isTest(seeAllData=true)
private class VFC81_QuoteBO_Test {

    public static Opportunity opp;
    public static Quote quot;

    public static void setup(){
        opp = MyOwnCreation.getInstance().criaOpportunity();
        Insert opp;

        quot = MyOwnCreation.getInstance().criaQuote();
        quot.OpportunityId = opp.Id;
        Insert quot;
    }

    static testMethod void testGetQuoteCreatedOrOpenFromOpportunity(){
        Test.startTest();

        setup();

        Quote q = VFC81_QuoteBO.getInstance().getQuoteCreatedOrOpenFromOpportunity(quot.Id,opp.Id);
        System.assertEquals(quot.Id,q.Id);

        Quote q2 = VFC81_QuoteBO.getInstance().getQuoteCreatedOrOpenFromOpportunity(null,opp.Id);
        System.assertEquals(opp.Id,q2.OpportunityId);

        Test.stopTest();
    }

    static testMethod void testCreateNewQuote(){
        Test.startTest();

        setup();

        Quote q = VFC81_QuoteBO.getInstance().createNewQuote(opp.Id);
        System.assertEquals(opp.Id,q.OpportunityId);

        Test.stopTest();
    }

    static testMethod void testUpdateQuote(){
        Test.startTest();

        setup();

        VFC81_QuoteBO.getInstance().updateQuote(quot);

        Test.stopTest();
    }

    static testMethod void testUpdateLastStatusField(){
        Test.startTest();

        setup();

        Map<Id,Quote> mapQuote = new Map<Id,Quote>();
        mapQuote.put(quot.Id,quot);

        VFC81_QuoteBO.getInstance().updateLastStatusField(mapQuote,mapQuote);

        Test.stopTest();
    }

}