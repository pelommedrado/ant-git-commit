@isTest(seealldata=true)
public with sharing class TestVFC01_GoodwillRecType  
{
static testMethod void testGoodRscType()

{
    Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
    Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'Paris', ShippingCountry = 'France', ShippingState = 'IDF', ShippingPostalCode = '75013', ShippingStreet = 'my street');
    insert Acc;
    
    Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
    insert Con;
    
    Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
    insert c;
    Case c1 = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
    insert c1;
    Test.StartTest();
    
    ApexPages.currentPage().getParameters().put('ent','01ID0000000ObDqMAK');
    ApexPages.currentPage().getParameters().put('retURL',c.id);
    
    ApexPages.currentPage().getParameters().put('CF00ND0000004qc7b_lkid',c.id);
    ApexPages.currentPage().getParameters().put('RecordType','012D0000000KBFG');
    ApexPages.currentPage().getParameters().put('RecordType','012D0000000KBFB');
    ApexPages.currentPage().getParameters().put('RecordType','012D0000000KBFL');
    ApexPages.currentPage().getParameters().put('RecordType',null);
    
    
    ApexPages.StandardController controller=new ApexPages.StandardController(c);
    VFC01_GoodwillRecType tst=new VFC01_GoodwillRecType(controller);
    tst.redirectToPage();
    
    
    ApexPages.currentPage().getParameters().put('ent','01ID0000000ObDqMAK');
    ApexPages.currentPage().getParameters().put('retURL',c1.id);
    
    ApexPages.currentPage().getParameters().put('CF00ND0000004qc7b_lkid',c1.id);
    
    ApexPages.currentPage().getParameters().put('RecordType','012D0000000KBFB');
    
    ApexPages.StandardController controller1=new ApexPages.StandardController(c1);
    VFC01_GoodwillRecType tst1=new VFC01_GoodwillRecType(controller1);
    tst1.redirectToPage();
    
    Test.stopTest();

    }
      }