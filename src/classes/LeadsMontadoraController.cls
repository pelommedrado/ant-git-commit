public with sharing class LeadsMontadoraController {

  public String dealerFilter{get;set;}
  private Map< Id, Id > oppOwnerMap{get;set;}
  private List< Id > selectedOppIdList{get;set;}
  private Id accDealerId{get;set;}
  public String campaignFilter{get;set;}
  public VFP24_LeadBoxMessages__c messages{get;set;}
  public List<oppWrapper> lsOppWrapper{get;set;}

  public String oppIdListAsString{
    get{
      return JSON.Serialize( selectedOppIdList );
    }
    set{
      //selectedOppIdList = (List< Id >)Json.Deserialize( value, List< Id >.class );
      oppOwnerMap = (Map< Id, Id >)Json.Deserialize( value, Map< Id, Id >.class );
    }
  }

  private String region{
    get{
      try{
        return [select NameZone__c from Account where Id = :accDealerId].NameZone__c;
      }catch( Exception e ){
        return 'R1';
      }
    }
  }
  
  public String welcomeMessage{
    get{
      try{
        return (String)messages.get( 'Welcome_message_' + region + '_' + Userinfo.getLanguage() + '__c' );
      }catch( Exception e ){
        return messages.Welcome_message_R1_pt_BR__c;
      }
    }
  }

  public List<oppWrapper> oppList{
    get{
      return lsOppWrapper;
    }
  }
  
  public List< SelectOption > campaignSelectList{
    get{
      List< SelectOption > campaignSelectList = new List< SelectOption >();
      Set< String > campaignNameSet = new Set< String >();
      
      String userBIR = [select BIR__c from User where Id = :Userinfo.getUserId()].BIR__c;
      accDealerId = [
      select Id 
      from Account
      where RecordType.DeveloperName = 'Network_Site_Acc'
        and IDBIR__c = :userBIR
      ].Id;
      
      campaignSelectList.add( new SelectOption( '', Label.VFP24_All_Campaign ) );
      for( Opportunity opp : [select Campaign_Name__c from Opportunity where Campaign_Name__c != null and Dealer__c =:accDealerId] )
        campaignNameSet.add( opp.Campaign_Name__c );

        system.debug('$$$ campaignNameSet: '+campaignNameSet);
      
      List< String > campaignNameList = new List< String >( campaignNameSet );
      campaignNameList.sort();
      
      for( String campaignName : campaignNameList )
        campaignSelectList.add( new SelectOption( campaignName, campaignName ) );
        
      return campaignSelectList;
    }
  }

  public List< SelectOption > dealerSelectList{
    get{
      List< SelectOption > dealerSelectList = new List< SelectOption >();

      List<Opportunity> lsOpp = [SELECT Dealer__c, Dealer__r.Name FROM Opportunity 
      WHERE OpportunitySource__c <> 'NETWORK' AND StageName = 'Identified' order by CreatedDate asc];
      
      dealerSelectList.add(new SelectOption('','Todas as Concessionárias'));

      Set<Id> dealerId = new Set<Id>();
      for(Opportunity opp : lsOpp ){
        if(!dealerId.contains(opp.Dealer__c)){
          dealerSelectList.add( new SelectOption( opp.Dealer__c, opp.Dealer__r.Name ) );
          dealerId.add(opp.Dealer__c);
        }
      }
        
      return dealerSelectList;
    }
  }
  
  public List< SelectOption > sellerSelectList{
    get{

      List<Contact> contactList = [SELECT AccountId, Id,  Name, ContactType__c FROM Contact 
        WHERE AccountId = '0013E000002PTot' AND ContactType__c IN  ('Seller' , 'Receptionist')];

      List< Id > contactIdList = new List< Id >();
      for( Contact c : contactList ) contactIdList.add( c.Id );
      
      List< User > userList = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId( contactIdList );
      String BIR = VFC25_UserDAO.getInstance().fetchUserByRecordId( Userinfo.getUserId() ).BIR__c;
      List< SelectOption > sellerSelectList = new List< SelectOption >();
      
      sellerSelectList.add( new SelectOption( 
        Userinfo.getUserId(), '--' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName() + '--' ) );
      for( User u : userList )
        if( !u.DVESeller_BR__c && u.BIR__c == BIR )
          sellerSelectList.add( new SelectOption( u.Id, u.Name ) );
      
      return sellerSelectList;
    }
  }
  
  public LeadsMontadoraController(){
    messages = VFP24_LeadBoxMessages__c.getInstance( Userinfo.getProfileId() );
    String userBIR = [select BIR__c from User where Id = :Userinfo.getUserId()].BIR__c;
    system.debug('$$$ userBIR: '+userBIR);
    accDealerId = [
      select Id 
      from Account
      where RecordType.DeveloperName = 'Network_Site_Acc'
        and IDBIR__c = :userBIR
      ].Id;
    system.debug('$$$ accDealerId: '+accDealerId);
    loadOppMap();
    
    selectedOppIdList = new List< Id >();
    campaignFilter = '';
  }
  
  public void loadOppMap(){

    // recupera as oportunidades de acordo com o filtro escolhido na pagina
    List<Opportunity> lsOpp = Database.query( 
      'SELECT Id, Name, CreatedDate, Vehicle_of_Interest__c, VehicleInterest__c, OpportunitySubSource__c,' +
      'Campaign_Name__c, Dealer__c, Dealer__r.Name FROM Opportunity WHERE OpportunitySource__c <> \'NETWORK\' ' +
      'AND OpportunitySource__c <> \'DEALER\' ' +
      'AND OpportunitySubSource__c <> \'PASSING\' ' +
      'AND StageName = \'Identified\' ' +
      'AND Submitted__c = false ' +
      (!String.isBlank( campaignFilter ) ? 'AND Campaign_Name__c = \'' + campaignFilter + '\' ' : '') +
      (!String.isBlank( dealerFilter ) ? 'AND Dealer__c = \'' + dealerFilter + '\' ' : '') +
      'order by CreatedDate asc'
    );

    system.debug('$$$ lsOpp: ' + lsOpp);

    //armazena as concessinarias
    Set<Id> dealers = new Set<Id>();
    for(Opportunity opp: lsOpp){
      dealers.add(opp.Dealer__c);
    }

    // recupera os contatos das concessionarias
    List<Contact> lsContactId = [SELECT Id from Contact WHERE AccountId IN: dealers];

    system.debug('$$$ lsContactId: ' + lsContactId);

    // recupera os usuarios referente a cada contato
    List<User> userList = [SELECT Id, Name, AccountId FROM User WHERE IsActive = true AND 
        ContactId IN: lsContactId ORDER BY Name];

    system.debug('$$$ userList: ' + userList);

    // monta mapa de concessionaria e lista de opções de usuarios
    Map<Id,List<SelectOption>> mapDealerUser = new Map<Id,List<SelectOption>>();
    List<SelectOption> option;
    Set<Id> userId = new Set<Id>();
    
    for(Opportunity opp: lsOpp){
      option = new List<SelectOption>();
      option.add(new SelectOption('','Selecione um proprietário'));
      for(User u: userList){
        if(u.AccountId.equals(opp.Dealer__c) && !userId.contains(u.Id)){
          option.add(new SelectOption(u.Id,u.Name));
          mapDealerUser.put(u.AccountId,option);
          userId.add(u.Id);
        }
      }
    }

    system.debug('$$$ mapDealerUser: ' + mapDealerUser);

   lsOppWrapper = new List<OppWrapper>();
    for(Opportunity opp: lsOpp){
      OppWrapper oppObj = new OppWrapper();
      oppObj.oppId = opp.Id;
      oppObj.oppName = opp.Name;
      oppObj.oppCreateDate = getUserDatetime(opp.CreatedDate);
      oppObj.oppVehicleInterest = opp.VehicleInterest__c;
      oppObj.oppOpportunitySubSource = opp.OpportunitySubSource__c;
      oppObj.oppCampaignName = opp.Campaign_Name__c;
      oppObj.oppDealer = opp.Dealer__r.Name; 
      oppObj.dealerContacts = mapDealerUser.get(opp.Dealer__c);
      lsOppWrapper.add(oppObj);
    }

    system.debug('$$$ lsOppWrapper: ' + lsOppWrapper);

  }
  
  public void forward(){
    try{
      List< Opportunity > modifiedOppList = new List< Opportunity >();

      for(Id oppId : new List< Id >( oppOwnerMap.keySet() ) ){
        modifiedOppList.add(
          new Opportunity( 
            Id = oppId,
            Submitted__c = true,
            OwnerId = (Id)oppOwnerMap.get( oppId )
          )
        );
      }

      System.debug( '@@@ BEFORE UPDATE: ' + Json.SerializePretty( modifiedOppList ) );

      Database.update( modifiedOppList );

      System.debug( '@@@ AFTER UPDATE: ' + Json.SerializePretty( modifiedOppList ) );

    }catch( Exception e ){
      Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
    }

    loadOppMap();
  }

  public class OppWrapper{
    public Id oppId{get;set;}
    public String oppName{get;set;}
    public Datetime oppCreateDate{get;set;}
    public String oppVehicleInterest{get;set;}
    public String oppOpportunitySubSource{get;set;}
    public String oppCampaignName{get;set;}
    public String oppDealer{get;set;}
    public List<SelectOption> dealerContacts{get;set;}
  }
    
    public static Datetime getUserDatetime(Datetime dt){
   		return dt.addSeconds( UserInfo.getTimeZone().getOffset( dt ) / 1000 );
	}

}