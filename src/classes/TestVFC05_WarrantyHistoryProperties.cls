@isTest
public with sharing class TestVFC05_WarrantyHistoryProperties {
    
    static testMethod void VFC05_WarrantyHistory(){
        Test.startTest();
        
        VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics whb = 
            new VFC05_WarrantyHistoryProperties.WarrantyHistoryBasics();
        
        String catInt 			= whb.catInt;
	    String datOuvOr 		= whb.datOuvOr;
	    String km 				= whb.km;
	    String libInt 			= whb.libInt;
	    String libRc 			= whb.libRc;
	    String numInt 			= whb.numInt;
	    String numOts 			= whb.numOts ;
	    String rc  				= whb.rc;
	    String txPec1Entretien 	= whb.txPec1Entretien;
	    String txPec1Incident  	= whb.txPec1Incident;
	    String txPec2 			= whb.txPec2;
	    String txPec3 			= whb.txPec3;
	    String txPec4 			= whb.txPec4;
	    String modulo 			= whb.modulo;
		String histoBim 		= whb.histoBim;
		String constClient 		= whb.constClient;
		String diag 			= whb.diag;
        
        VFC05_WarrantyHistoryProperties.PiecesandOeuvres[] o = whb.PandO;
        
        VFC05_WarrantyHistoryProperties.PiecesandOeuvres po = 
            new VFC05_WarrantyHistoryProperties.PiecesandOeuvres();
        
        String Code 	= po.Code;
        String Temps 	= po.Temps;
        String Quantite = po.Quantite;
        String Libelles = po.Libelles;
        
        Test.stopTest();
    }
}