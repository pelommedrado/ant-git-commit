public class ClienteOportunidadeDetalheSfa2Controller extends AbstractOportunidadeController {
    private String opportunityId;
    
    public LIst<SelectOption> motivoCancelList { get;set; }
    public String motivoCancelamento { get;set; }
    
    public List<Quote> orcamentos { get;set; }
    public VFC60_QuoteVO quoteVO { get;set; }
    public VFC60_QuoteVO quoteItemVO {get;set;}
    public VFC61_QuoteDetailsVO quoteDetailsVO {get;set;}
    private String idQuote;
    
    public ClienteOportunidadeDetalheSfa2Controller(ApexPages.StandardController controller) {
        this.opportunityId = controller.getId();
        this.quoteVO = new VFC60_QuoteVO();
    }
    
    public PageReference initialize() {
        this.orcamentos = null;
        System.debug('### this.opportunityId: ' + this.opportunityId);
        
        this.quoteVO = VFC63_QuoteBusinessDelegate.getInstance().getOpportunity(this.opportunityId);
        
        initAction(this.opportunityId);
        
        System.debug('>>> this.quoteVO >>> ' + this.quoteVO);
        this.buildPicklists();
        
        /* Monta a Grid Orçamento */
        quoteItemVO = VFC63_QuoteBusinessDelegate.getInstance().getQuote(this.opportunityId);
        
        this.motivoCancelList =
            VFC49_PickListUtil.buildPickList(Opportunity.ReasonLossCancellation__c, '');
        
        return null;
    }
    
    public boolean getIsReadOnly() {
        return (quoteVO.status == 'Lost') || 
            (quoteVO.status == 'Billed') || 
            (quoteVO.status == 'Order');
    }
    
    private void buildPicklists() {
        /*cria os picklists que deverão ser exibidos assim que a tela for carregada*/
        this.quoteVO.lstSelOptionInterestVehicle = VFC49_PickListUtil.buildPickList();
    }
    
    public PageReference cancelOpportunity() {
        PageReference pageRef = null;
        ApexPages.Message message = null;
        
        motivoCancelamento = 
            ApexPages.currentPage().getParameters().get('motivo');
        System.debug('### ' + motivoCancelamento);
        
        this.quoteVO.reasonLossCancellationOpportunity = motivoCancelamento;
        try {
            VFC63_QuoteBusinessDelegate.getInstance().cancelOpportunity(this.quoteVO);
            
            pageRef = new PageReference('/ClienteOportunidadeDetalheSfa2?Id=' + this.opportunityId); 
            
            return pageRef;
            
        } catch(Exception e) {
            message = new ApexPages.Message(
                ApexPages.Severity.Confirm, 
                'Erro ao salvar os dados: ' + e.getMessage());
            
            ApexPages.addMessage(message);
        }
        
        return pageRef;
    }
    
    public PageReference cancelarPreOrdem() {
        for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes) {
            if(auxQuote.isCheckOpp == true) {
                Quote quote = new Quote(Id=auxQuote.id, Status='Canceled');
                update quote;
                
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.CONFIRM, 'Orçamento cancelado com sucesso.') );
                
                break;
            }
        }
        initialize();
        return null;
    }
    
    public PageReference sendToRequest() {
        Boolean isCheck = false;
        String strMessage = null;
        ApexPages.Message message = null;
        
       for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes) {
            if(auxQuote.status == 'Canceled' && auxQuote.isCheckOpp == true) {
                VFC61_QuoteDetailsVO quote = Sfa2Utils.criarOrcamento(opportunityId, null);
                PageReference pageRef =
                    new PageReference('/apex/ClientePreOrdemSfa2?Id=' + quote.Id + '&oppId=' + opportunityId);
                pageRef.setRedirect(true);
                return pageRef; 
            }
        }
        
        for(VFC67_QuoteItemVO auxQuote : quoteItemVO.lstQuotes) {
            if(auxQuote.isCheckOpp == true) {
                idQuote = auxQuote.id;
                isCheck = true;
                
                /*if(auxQuote.VehicleRule == '' || auxQuote.VehicleRule == null) {
                    strMessage = 'Existem veículos sem chassi definido no orçamento. Favor cadastrar estes chassis antes de gerar o pedido.'; 
                    message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                    ApexPages.addMessage(message);  
                    return null;                            
                }*/
            }
        }
        
        if(isCheck == false) {
            strMessage = 'Selecione um orçamento para gerar a pré ordem.';       
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);  
            return null;    
        }
        
        PageReference pageRef =
            new PageReference('/apex/ClientePreOrdemSfa2?Id=' + idQuote + '&oppId=' + opportunityId);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public void updateVehicleInterest() {
        Opportunity opp = [
            SELECT Id, Account.Id
            FROM Opportunity
            WHERE Id =: opportunityId
        ];
        
        Account acc = [
            SELECT Id, VehicleInterest_BR__c
            FROM Account
            WHERE Id =: opp.Account.Id
        ];
        
        acc.VehicleInterest_BR__c = quoteVO.vehicleInterest;
        
        update acc;
    }
}