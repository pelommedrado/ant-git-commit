/**
*	Class	-	WSC07_MockHttpResponseGenerator
*	Author	-	Rameshprabu
*	Date	-	05/04/2013
*
*	#01 <Rameshprabu> <05/04/2013>
*		This class was used to test Http callout.
**/
@isTest
global class WSC07_MockHttpAOC_Batch_Step2 implements HttpCalloutMock {
	/* This webservice method  called */
	global HTTPResponse respond(HTTPRequest req) {
		/* System assert check get method */
        System.assertEquals('GET', req.getMethod());
        /* initialize http response object */
        HttpResponse res = new HttpResponse();
        /* set header in http response */
        res.setHeader('Content-Type', 'application/json');
        /* set body in http response */
        res.setBody('{"marketingModelPresentation":{"asMap":{"mapRepresentation":{"map":{"NFG$RANGE":{"presentationItem":{"key":"NFG$RANGE","order":"20","label":{"pt":"Bolso tipo canguru atrás dos bancos dianteiros"}}},"PROSCS":{"presentationItem":{"key":"PROSCS","order":"1","label":{"pt":"Protetor inferior do cárter"}}},"OV727":{"presentationItem":{"key":"OV727","order":"14","label":{"pt":"Vermelho Vivo"}}},"PT923":{"presentationItem":{"key":"PT923","order":"0","label":{"pt":"Volume do porta-malas (L)"}}}},"metadata":{"key":"key","value":"presentationItem"}}},"colors":[{"presentationItem":{"key":"TEKNS","order":"02","label":{"pt":"Cinza Quartz"}}}],"colorThemes":[{"presentationItem":{"key":"HARM01","order":"0","label":{"pt":"Acabamento interno na cor carbono"}}}],"key":"LCM","order":"000001","label":{"pt":"SYMBOL"},"versionsPresentation":[{"presentationGroup":{"items":[{"presentationGroup":{"items":[{"versionItem":{"versionIdSpecCode":"VEC162_BRES","key":"VEC162_BRES","order":"0","label":{"pt":"Privilège 1.6 16V"},"versionId":{"countrySpecCode":"BRES","versionSeq":"162"},"showableSpecCodes":["QPA$MV","NFG$AIRBA1"]}}],"key":"ENS_183","order":"0_0","label":{"pt":"Symbol Privilège 2012/2013"}}}],"key":"ENS_182","order":"0","label":{"pt":"Symbol"}}}],"equipementsPresentation":[{"presentationGroup":{"items":[{"presentationItem":{"key":"DRAP03","order":"0","label":{"pt":"Tecido dos bancos tipo Setis Veludo"}}},{"presentationItem":{"key":"BANAR","order":"1","label":{"pt":"Banco traseiro rebatível 1/1"}}}],"key":"PREZIN","order":"0","label":{"pt":"Itens Internos"}}}],"technicalSpecificationsPresentation":[{"presentationGroup":{"items":[{"presentationGroup":{"items":[{"presentationItem":{"key":"PT923_506_litros","order":"0","label":{"pt":"506 litros"}}}],"key":"PT923","order":"0","label":{"pt":"Volume do porta-malas (L)"}}}],"key":"DEC44","order":"8","label":{"pt":"Volume"}}}]}}');
        /* set statuscode in http response */
        res.setStatusCode(200);
        return res;
	}
}