global class BotConnectionBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    String facebookUserId;
    String facebookPageId;
    String calloutType;
    String clientUserId;
    String customProperty;
    
    global BotConnectionBatch (String facebookId, String pageId, String userId, String category, String customP) {
        facebookUserId  = facebookId;
        calloutType     = category; 
        facebookPageId  = pageId;
        clientUserId    = userId;
        customProperty  = customP;
    }
    
    global Iterable<SObject> start(Database.BatchableContext BC) {
        FacebookIdToAccountWS.facebookConnection(facebookUserId, facebookPageId, clientUserId, calloutType, customProperty);
        return new List<Account>();
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}