@isTest(seeAllData=true)
private class VFC50_OppOrchestrationController_Test {
	
	
	
    static testMethod void myUnitTest() {
        
        Opportunity opp1 = new Opportunity();       
        boolean isCreatedOpportunity1 = false;
        
        // selecionar Standard price Book (seeAllData)
		Pricebook2 pb21 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp1.Name = 'OppNameTest_1';
        opp1.StageName = 'Identified';
        opp1.CloseDate = Date.today();
        opp1.Pricebook2Id = pb21.Id;
        opp1.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opp1.CurrencyIsoCode = 'BRL';
        insert opp1;
        
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opp1);
        
        PageReference pageRef = Page.VFP07_OpportunityOrchestration;    		
		Test.setCurrentPage(Page.VFP07_OpportunityOrchestration);    		    		
		pageRef.getParameters().put('id',opp1.id);    	
        
        VFC50_OpportunityOrchestrationController cont = new VFC50_OpportunityOrchestrationController(controller);
        cont.redirect();
         
        opp1.StageName = 'Test Drive';
        update opp1;
        
        controller = new ApexPages.Standardcontroller(opp1);
        cont = new VFC50_OpportunityOrchestrationController(controller);
        cont.redirect();
        
        opp1.StageName = 'Billed';
        update opp1;
        
        controller = new ApexPages.Standardcontroller(opp1);       
        cont = new VFC50_OpportunityOrchestrationController(controller);
        cont.redirect();
        
    }
}