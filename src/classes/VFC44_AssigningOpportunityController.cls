/**
* Classe controladora da página de atribuição de oportunidade.
* @author Felipe Jesus Silva.
*/
public class VFC44_AssigningOpportunityController 
{

    private Task sObjTask;
    private Opportunity sObjOpportunity;
    public List<SelectOption> lstSellerSelOption {get;set;}
    public String selectedSeller {get;set;}

    public VFC44_AssigningOpportunityController() {
        this.initialize(ApexPages.currentPage().getParameters().get('taskId'));
    }

    private void initialize(String taskId)
    {
        List<Contact> lstSObjContact = null;
        List<String> lstContactId = null;
        List<User> lstSObjUser = null;
        String oppId = '';
                
        if(taskId != null)
        {
            /*localiza a tarefa pelo id*/
            this.sObjTask = VFC33_TaskDAO.getInstance().findById(taskId);
            oppId = sObjTask.WhatId;

        }else
        {
            
            oppId = ApexPages.currentPage().getParameters().get('oppId');
            
        }

        /*localiza a oportunidade pelo id*/
        this.sObjOpportunity = VFC23_OpportunityDAO.getInstance().fetchOpportunityUsingOpportunityId(oppId);
    
        /*localiza os contatos(vendedores) da concessionária correspondente a essa oportunidade*/
        lstSObjContact = VFC41_ContactDAO.getInstance().findByAccountIdAndContactType(sObjOpportunity.Dealer__c, 'Seller');
        
        lstContactId = new List<String>(new Map<String, Contact>(lstSObjContact).keySet());
        
        lstSObjUser = VFC25_UserDAO.getInstance().fetchUserRecordsUsingContactId(lstContactId);
        
        this.buildSellerPicklist(lstSObjUser);
    }
    
    private void buildSellerPicklist(List<User> lstSObjUser)
    {
        this.lstSellerSelOption = new List<SelectOption>();
        
        for(User sObjUser : lstSObjUser)
        {
            this.lstSellerSelOption.add(new SelectOption(sObjUser.Id, sObjUser.Name));
        }
    }
    
    public void sendToSeller()
    {
        ApexPages.Message message = null;
        
        /*definine o vendedor selecionado como proprietário da oportunidade*/
        this.sObjOpportunity.OwnerId = this.selectedSeller;
        
        if(sObjTask != null)
            this.sObjTask.Status = 'Completed';
        
        if((this.sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER) || 
           (this.sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER))
        {
            this.sObjOpportunity.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER; 
        }
        else if((this.sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER))           
        {
           this.sObjOpportunity.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_MANAGER_TO_SELLER;            
        }
        
        try
        {
            VFC23_OpportunityDAO.getInstance().updateData(this.sObjOpportunity); 
            
            if(sObjTask != null)
                VFC33_TaskDAO.getInstance().updateData(this.sObjTask);
            
            //message = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Oportunidade encaminhada com sucesso!');
        }
        catch(DMLException ex)
        {
            //message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Erro ao encaminhar oportunidade para o vendedor. Motivo: \n' + ex.getMessage());
            
            //ApexPages.addMessage(message);
        }
            
        //return null;
    }
}