/**
*   WebService Class    -   WSC08_AOC_Scheduler_Step1
*   Author              -   Suresh Babu
*   Date                -   20/03/2013
*   
*   #01 <Suresh Babu> <20/03/2013>
*       Scheduler class to initialize the Batch process for AOC.
**/

global class WSC08_AOC_Scheduler_Step1 implements Schedulable{
	global void execute(SchedulableContext ctx) {
		WSC07_AOC_Batch_Step1 b1 = new WSC07_AOC_Batch_Step1();
        Database.executeBatch(b1, 1);
	}
}