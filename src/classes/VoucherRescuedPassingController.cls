public without sharing class VoucherRescuedPassingController {
    
    public CampaignMember cm{get;set;}
    public String status{get;set;}
    
    public VoucherRescuedPassingController(){
        String cmId = ApexPages.currentPage().getParameters().get('cmId');
        
        system.debug('cmId: '+cmId);
        
        cm = [SELECT Id, CampaignId, Campaign.Name, Campaign.Refundable__c, Lead.Name, Lead.CPF_CNPJ__c, 
              Lead.Voucher_Protocol__c, Campaign.Description, Campaign.EndDate, Lead.FirstName, Lead.LastName,
              Lead.LeadSource, Lead.Email, Lead.VehicleOfInterest__c, Lead.Mobile_Phone__c, Lead.VIN__c, Lead.HomePhone__c,
              Lead.Vehicle_Invoice__c, Campaign.Billing_Required__c, Campaign.Available_to_Third_Parties__c, LeadId, 
              Campaign.WebToOpportunity__c
              FROM CampaignMember WHERE Id =: cmId limit 1];
        
        system.debug('cm: '+cm);
    }
    
    public PageReference registraPassagem(){
        
        // oportunidade a ser relacionada ao Lead Voucher
        Opportunity opp = new Opportunity();
        
        // Cria um lead do tipo DVR
        Lead leadDVR = new Lead(
            FirstName = cm.Lead.FirstName,
            LastName = cm.Lead.LastName,
            CPF_CNPJ__c = cm.Lead.CPF_CNPJ__c,
            LeadSource = 'Dealer',
            SubSource__c = 'PASSING',
            Detail__c = 'VOUCHERS',
            Email = cm.Lead.Email,
            VehicleOfInterest__c = cm.Lead.VehicleOfInterest__c,
            Mobile_Phone__c = cm.Lead.Mobile_Phone__c,
            VIN__c = cm.Lead.VIN__c,
            HomePhone__c = cm.Lead.HomePhone__c,
            Vehicle_Invoice__c = cm.Lead.Vehicle_Invoice__c,
            RecordTypeId = Utils.getRecordTypeId('Lead', 'DVR'),
            DealerOfInterest__c = [SELECT Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId()].Contact.AccountId
        );
        Insert leadDVR;
        
        // Converter o Lead e criar oportunidade
        if ( VFC127_CampaignMemberBO.getInstance().ChecaSeDeveConverterLead(leadDVR) )    
            opp = VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(leadDVR, null);
        
        //Relaciona a campanha à Oportunidade gerada
        opp.CampaignId = cm.CampaignId;
        system.debug('status: '+status);
        opp.StageName = cm.Campaign.Billing_Required__c ? 'Billed' : status ;
        Update opp;

        //atualiza o lead voucher relacionando com a oportunidade gerada
        Lead leadVoucher = new Lead(
            Id = cm.Lead.Id,
            Status = 'Rescued',
            Related_Opportunity__c = opp.Id
        );
        Update leadVoucher;
        
        PageReference pg = new PageReference('/apex/PromotionalActions');
        return pg;
    }
    
    public PageReference loadPagePromotionalActions(){
        PageReference pg = new PageReference('/apex/PromotionalActions');
        return pg;
    }
}