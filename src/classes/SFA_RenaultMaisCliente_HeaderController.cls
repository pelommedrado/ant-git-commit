global class SFA_RenaultMaisCliente_HeaderController {
    
    private static String userId = UserInfo.getUserId();
    public String userSessionId {
        get {
            return  UserInfo.getSessionId();
        }
    }
    
    public static Boolean supportUser{
        get {
            return CAC_SupportCaseController.supportUser;
        }
    }
    
    public String appSelected {get;set;}
    
    public List<String> optionAppList {
        get {
            
            UserInformation__c uInfo = obterUltimoUserInformation();
            String applicationSelected = '';
           	
            if(uInfo != null) {
                applicationSelected = uInfo.Application__c;
            }
            
            List<String> optionList = new List<String>();
            
            for(String permission: getPermissionSetAvailable()) {
                if(String.isNotBlank(permission)) {
                    optionList.add(permission);
                }
            }
            
            List<String> optAppList = new List<String>();
            
            if(!String.isEmpty(applicationSelected)) {
                optAppList.add(applicationSelected);
            }
            
            for(String app : optionList) {
                
                if(!String.isEmpty(applicationSelected)) {
                    if(app != applicationSelected) {
                        optAppList.add(app);    
                    }
                    
                } else {
                    optAppList.add(app);    
                    
                }
                
            }
            
            return optAppList;
        }
    }
    
    @RemoteAction
    public static String setHomePage(String appSelect){
        if(appSelect == 'SFA - Vendedor') {
            return '/cac/apex/VFP20_RMCDesktop';
            
        } else if(appSelect == 'SFA - Recepcionista') {
            return '/cac/apex/VFP20_RMCDesktop';
            
        } else if(appSelect == 'CAC') {
            return '/cac/apex/CAC_DealerChoiceDeptCase';
            
        } else if(appSelect == 'Pedidos Diários') {
            return '/cac/apex/SFA_PassingPurchase';
            
        } else if(appSelect == 'Biblioteca') {
            return '/renaultmaiscliente/sfc/#search';
            
        } else if(appSelect == 'Logística - Concessionário') {
            return '/cac/apex/VFP31_VehicleTracking';
            
        } else if(appSelect == 'Logística - Assistente Comercial') {
            return '/cac/apex/VFP31_VehicleTracking';
            
        } else if(appSelect == 'Acompanhamento DCL.CRM') {
            return '/cac/apex/FaturaDealer';
            
        } else if(appSelect == 'Plataforma de Varejo - Grupo') {
            return '/cac/a1G/o';
            
        } else if(appSelect == 'Plataforma de Varejo - Concessionária') {
            return '/cac/a1G/o';
            
        } else if(appSelect == 'E3M') {
            return '/cac/00O570000072IBa';
            
        } else if(appSelect == 'Request a Service') {
            return '/cac/AgendaServico';
            
        } else if(appSelect == 'MNER') {
            return '/cac/apex/MNERpage';
            
        } else if(appSelect == 'SFA - Gerente') {
            return '/cac/apex/VFP24_ManagerLeadBox';
            
        } else if(appSelect == 'SFA - Gerente - Grupo') {
            return '/cac/apex/leadsMontadora';
            
        } else if(appSelect == 'SFA – Gestão Comercial') {
            return '/cac/_ui/core/chatter/ui/ChatterPage';
            
        } else if(appSelect == 'Leads Montadora') {
            return '/cac/apex/LeadBoxCommunity';
            
        } else if(appSelect == 'Ações Promocionais') {
            return '/cac/apex/getvoucher';
            
        } else if(appSelect == 'SFA - SalesWay - Executive') {
            return '/cac/apex/ObjetivoGeral';
            
        } else if(appSelect == 'SFA - SalesWay - Manager') {
            List<String> monthList = new List<String>{
                'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            };
            
            String year = String.valueOf(Date.today().year());
            String month = monthList[Date.today().month()-1];
            
            return '/cac/apex/ObjetivoSeller?ano=' + year +'&mes=' + month;
            
        } else if(appSelect == 'SFA - Vendedor - Agenda') {
            return '/cac/apex/DashboardTask';
            
        } else if(appSelect == 'SFA - Gerente - Agenda') {
            return '/cac/apex/DashboardTask';
            
        } else if(appSelect == 'SFA - Gerente Grupo - Agenda') {
            return '/cac/apex/DashboardTask';
            
        } else if(appSelect == 'SFA - Gerente BDC - Agenda') {
            return '/cac/apex/DashboardTask?profile=BDC';
        }
        
        return '';
    }
    
    public PageReference init() {
        System.debug('Iniciando header...');
        
        try {
            
            UserInformation__c uInfo = obterUltimoUserInformation();
            
            System.debug('uInfo: ' + uInfo);
            System.debug('userSessionId: ' + userSessionId);
            
            if(uInfo != null && uInfo.UserSessionId__c != userSessionId) {
                String appSelect = uInfo.Application__c;
                
                System.debug('Iniciando aplicação: ' + appSelect);
                addUserInformation(appSelect, userSessionId); 
            }
           
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
        }
        
        return null;
    }
    
    webservice static void setApplication(String app, String sessionId) {
        try {
            System.debug('Iniciando aplicação: ' + app);
            addUserInformation(app, sessionId);
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            
        }
    }
    
    public UserInformation__c obterUltimoUserInformation() {
        List<UserInformation__c> userInfoList = [
            SELECT ID, Application__c , CreatedDate, UserSessionId__c
            FROM UserInformation__c 
            WHERE User__c =: userId
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];
        
        if(userInfoList != null && !userInfoList.isEmpty()) {
            return userInfoList.get(0);
        }
        
        return null;
    }
    
    public static void addUserInformation(String app, String sessionId) {
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        
        Id accId  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        UserInformation__c userInfoApp 	= new UserInformation__c();
        userInfoApp.User__c 			= UserInfo.getUserId();
        userInfoApp.Application__c 		= app;
        userInfoApp.Dealer__c 			= accId;
        userInfoApp.UserSessionId__c 	= sessionId;

        System.debug('UserInformation__c: ' + userInfoApp);
        
        INSERT userInfoApp;
    }
    
    public List<String> getPermissionSetAvailable(){
        
        List<String> permisionNameList = new List<String>();
        
        List<SFA_PermissionSetAvailable__c> permissoesDisponiveisList = SFA_PermissionSetAvailable__c.getAll().values();
        System.debug('PermissoesDisponiveisList: ' + JSON.serialize(permissoesDisponiveisList));
        for(SFA_PermissionSetAvailable__c permission : permissoesDisponiveisList) {
            permisionNameList.add(permission.permissionSet_Name__c);
        }
        
        List<PermissionSetAssignment> setAssList = [
            SELECT PermissionSetId 
            FROM PermissionSetAssignment WHERE AssigneeId =: userId
        ];
        
        Set<Id> permissionSetAssingnmentIdSet = new Set<Id>();
        for(PermissionSetAssignment permissionSetAssi : setAssList) {
            permissionSetAssingnmentIdSet.add(permissionSetAssi.PermissionSetId); 
        }
        System.debug('PermissionSetAssingnmentIdSet: ' + JSON.serialize(permissionSetAssingnmentIdSet));
        
        List<PermissionSet> permissionSetList =  [
            SELECT Id, Name 
            FROM PermissionSet 
            WHERE Id in: (permissionSetAssingnmentIdSet) AND Name in:(permisionNameList)
        ];
        
        Set<String> permisionSetAvailableName = new Set<String>();
        for(PermissionSet permissionSet : permissionSetList) {
            permisionSetAvailableName.add(permissionSet.Name);
        }
        
        System.debug('PermissionSetList: ' + JSON.serialize(permissionSetList));
        
        Set<String> permissionSetName = new Set<String>();
        
        for(SFA_PermissionSetAvailable__c per : permissoesDisponiveisList) {
            if(permisionSetAvailableName.contains(per.permissionSet_Name__c)) {
                permissionSetName.add(per.Application__c);    
            }
        }
        
        return new List<String>(permissionSetName);
    }
   
}