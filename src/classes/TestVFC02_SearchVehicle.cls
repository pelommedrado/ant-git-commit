public class TestVFC02_SearchVehicle
{
    public static testMethod void testVFC02()
    {
        PageReference pageRef = Page.success;
        VFC02_SearchVehicle controller = new VFC02_SearchVehicle();
        controller = new VFC02_SearchVehicle(); 
        controller.VIN = 'VIN';
        controller.VRN = 'VRN';
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse ws = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();  
        WS01_ApvGetDetVehXml.DetVeh d = new WS01_ApvGetDetVehXml.DetVeh();
        ws.detVeh = d;
        ws.detVeh.dateLiv= 'r';
        ws.detVeh.dateTcmFab= 'r';
        ws.detVeh.indBoi= 'r';
        ws.detVeh.indMot= 'r';
        ws.detVeh.libCarrosserie= 'r';
        ws.detVeh.libConst= 'r';
        ws.detVeh.libModel= 'r';
        ws.detVeh.ligne2P12= 'r';
        ws.detVeh.ligne3P12= 'r';
        ws.detVeh.ligne4P12= 'r';
        ws.detVeh.marqCom= 'r';
        ws.detVeh.marqCon= 'r';
        ws.detVeh.marqDis= 'r';
        ws.detVeh.marqFab= 'r';
        ws.detVeh.mil= 'r';
        ws.detVeh.nbCrt= 1;
        ws.detVeh.NBoi= 'r';
        ws.detVeh.NFab= 'r';
        ws.detVeh.NMot= 'r';
        ws.detVeh.placSpec= 'r';
        ws.detVeh.ptma= 'r';
        ws.detVeh.ptmaar= 'r';
        ws.detVeh.ptmaav= 'r';
        ws.detVeh.ptr= 'r';
        ws.detVeh.refBoi= 'r';
        ws.detVeh.refMot= 'r';
        ws.detVeh.tapv= 'r';
        ws.detVeh.tvv= 'r';
        ws.detVeh.typeBoi= 'r';
        ws.detVeh.typeMot= 'r';
        ws.detVeh.version= 'r';
        ws.detVeh.vin= 'r';   
     	ws.detVeh.dateTcmFab='11.02.2008';
        ws.detVeh.dateLiv='11.02.2008';
        
        Test.StartTest();
        pageRef = controller.searchVehicle();     
        pageRef = controller.createVehicle(ws);
        ws = controller.searchBVM();
        Test.StopTest();
    }
}