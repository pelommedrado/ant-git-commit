@isTest
private class VFC54_TestDriveVO_Test {
    
    static testMethod void unitTest01() {
        
        VFC54_TestDriveVO vo = new VFC54_TestDriveVO();
        
        List<TDV_TestDrive__c> tdvList = new List<TDV_TestDrive__c>();
        List<TDV_TestDriveVehicle__c> tdvVeiList = new List<TDV_TestDriveVehicle__c>();
        List<Product2> prdList = new List<Product2>();
        TDV_TestDrive__c tdv = new TDV_TestDrive__c();
        
        vo.id = 'teste';
        vo.opportunityId = 'teste';
        vo.dealerId = 'teste';
        vo.lstSObjTestDriveAgenda = tdvList;
        vo.lstSObjTestDriveVehicleAvailable = tdvVeiList;
        vo.lstSObjModel = prdList;
        
        /*atributos referentes a seção informações do test drive*/
        vo.name = 'teste';
        vo.sellerName = 'teste';
        vo.opportunityName = 'teste';
        vo.sObjTestDrive = tdv;
        vo.fuelOutputLevel = 'teste';
        vo.status = 'teste';
        vo.reasonCancellation = 'teste';
        vo.vehicleInterest = 'teste';
        vo.outputKM = 'teste';
        vo.returnKM = 'teste';
        vo.fuelReturnLevel = 'teste';
        vo.motor = 'teste';
        vo.cambio = 'teste';
        
        /*atributos referentes a seção informações do cliente*/
        vo.customerName = 'teste';
        vo.currentVehicle = 'teste';
        vo.persLandline = 'teste';
        vo.yearCurrentVehicle = 2;
        vo.persMobPhone = 'teste';
        
        /*atributos referentes a seção agenda de veículos*/
        vo.testDriveVehicleId = 'teste';
        vo.plaque = 'teste';
        vo.model = 'teste';
        vo.version = 'teste';
        vo.color = 'teste';
        vo.dateBooking = System.now();
        vo.dateBookingFmt = 'teste';
        vo.dateBookingNavigation = System.today();
        vo.dateBookingNavigationFmt = 'teste';
        vo.selectedTime = 'teste';
        vo.vehicleScheduled = 'teste';
        
        /*atributos referentes a seção avaliação do test drive*/
        vo.design = 'teste';
        vo.internalSpace = 'teste';
        vo.handling = 'teste';
        vo.security = 'teste';
        vo.performance = 'teste';
        vo.panelCommands = 'teste';
        vo.equipmentLevel = 'teste';
        
        /*atributo referente a seção interesse após test drive*/
        vo.buyingInterestAfterTestDrive = 'teste';
        
        /*atributo referente a seção comentários e sugestões*/
        vo.comments = 'teste';
        
        /*atributo referente ao motivo de cancelamento da opportunidade*/
        vo.reasonLossCancellationOpportunity = 'teste';
        
        List<SelectOption> options = new List<SelectOption>();
        
        /*picklists que serão exibidos na tela*/
        vo.lstSelOptionStatus = options;
        vo.lstSelOptionReasonCancellation = options;
        vo.lstSelOptionVehicleInterest = options;
        vo.lstSelOptionFuelOutputLevel = options;
        vo.lstSelOptionFuelReturnLevel = options;
        vo.lstSelOptionVehiclesAvailable = options;
        vo.lstSelOptionReasonLossCancellationOpp = options;
        
        /*flags que indicam se os controles estão ou não habilitados/visíveis*/
        vo.statusDisabled = true;
        vo.reasonCancellationDisabled = true;
        vo.disabledFields = true;
        vo.navigationAgendaVisible = true;
        vo.commentsDisabled = true;
        vo.btnCompleteTestDriveDisabled = true;
        vo.btnCancelOpportunityDisabled = true;
        vo.confirmationGenerationQuoteVisible = true;
        vo.reasonCancellationPopUpVisible = true;
        vo.btnGenerateQuoteDisabled = true;
        
        /*variável que armazena o título do pop up de geração de orçamento*/
        vo.titlePopUpGenerationQuote = 'teste';
        
        /*flag que indica se o test drive já foi atualizado (salvo)*/
        vo.updatedTestDrive = true;
        
        List<List<VFC64_ScheduleTestDriveVO>> sch = new List<List<VFC64_ScheduleTestDriveVO>>();
        
        /*matriz que contém a agenda do veículo (foi criado uma lista de listas para poder exibir a agenda na tela em formato tabular - matriz)*/
        vo.lstAgendaVehicle = sch;
        
        List<VFC108_DocumentationRVAVO> doc = new List<VFC108_DocumentationRVAVO>();
        
        /*lista que contém os documentos RVA*/
        vo.lstDocumentationRVA = doc;
        
        
    }
}