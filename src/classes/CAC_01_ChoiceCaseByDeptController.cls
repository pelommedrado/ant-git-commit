public with Sharing class CAC_01_ChoiceCaseByDeptController{
    
    private final String userId = UserInfo.getUserId();
    private final String profileId = UserInfo.getProfileId();
    public List<String> recordTypeListName = new List<String>();
    public String choiseRecordType{get;set;}
    public static Case setCase{get;set;}
    public String  getChoiseRecordType {get;set;}
    public String userType{get;set;}
    
    public CAC_01_ChoiceCaseByDeptController(ApexPages.StandardController controller){ userType = UserInfo.getUserType();}
    public CAC_01_ChoiceCaseByDeptController(){userType = UserInfo.getUserType();}
    
    public String typeCaseSelect='';
    public String getTypeCaseSelect(){return typeCaseSelect;}
    public void setTypeCaseSelect(String value){
        typeCaseSelect = value;
    }
    
    public PageReference  typerecordtype(){
        return null;
    }
    
    public void selectRec(){
        String para = ApexPages.CurrentPage().getParameters().get('typeCaseSelect');
        setCase = new Case(recordTypeId = para);
    }
    
    public List<SelectOption> lsDepartmentSelect{
        get{ return populateDepartment();
           }
    }
    public List<SelectOption> populateDepartment(){
        
        if(String.isNotEmpty(typeCaseSelect)){
            List<SelectOption> lsSelectOptionDepartment = new List<SelectOption>();
            
            List<String> lsRecodTypeDeveloperName = new List<String>();             
            for(cacweb_Mapping__c chooseField: mapCustonSet.values()){
                if(chooseField.DirectionInEnglish__c.equals(typeCaseSelect)){
                    lsRecodTypeDeveloperName.add(chooseField.Record_Type_Name__c);
                }
                
            }
            
            
            set<String> lsIdRecordType = new set<String>();
            for(RecordType tipoReg : [select id from RecordType where DeveloperName in:(lsRecodTypeDeveloperName) and SobjectType ='Case']){
                lsIdRecordType.add(tipoReg.Id);
            }
            
            Set<String> lsFieldContains = new Set<String>();//set Para não repetir os valores repetidos do Custon Label
            lsTipoRegistroDisponivel.sort();
            for(RecordType tipoReg : lsTipoRegistroDisponivel){
                if(lsIdRecordType.contains(tipoReg.id) && !lsFieldContains.contains(tipoReg.Name)){
                    lsSelectOptionDepartment.add( new SelectOption (tipoReg.id,tipoReg.Name));
                    lsFieldContains.add(tipoReg.Name);
                }
            } 
            lsSelectOptionDepartment.sort();
            return lsSelectOptionDepartment;
        }
        return null;
        
    }
    
    
    
    
    
    
    
    
    public String valueDirection{get;set;}
    public List<SelectOption> populateDirection{
        get{
            List<String> lsRecodTypeDeveloperName = new List<String>();             
            for(cacweb_Mapping__c chooseField: mapCustonSet.values()){
                lsRecodTypeDeveloperName.add(chooseField.Record_Type_Name__c);
                
            }
            
            set<Id> lsIdRecordType = new set<Id>();
            for(RecordType tipoReg : [select id,name,DeveloperName from RecordType where DeveloperName in:(lsRecodTypeDeveloperName) and SobjectType ='Case' order by name DESC]){
                lsIdRecordType.add(tipoReg.id);
            }
            
            
            List<SelectOption> lsChoseField = new List<SelectOption>();//lista com os select Option
            Set<String> lsFieldContains = new Set<String>();//set Para não repetir os valores repetidos do Custon Label
            for(cacweb_Mapping__c chooseField: mapCustonSet.values()){
                lsTipoRegistroDisponivel.sort();
                for(RecordType tipoReg : lsTipoRegistroDisponivel){
                    if(!lsFieldContains.contains(chooseField.Department__c)){
                        if(lsIdRecordType.contains(tipoReg.id)){
                            lsChoseField.add( new SelectOption (chooseField.DirectionInEnglish__c,chooseField.Department__c));
                            lsFieldContains.add(chooseField.Department__c);
                        }
                    }
                    
                }
                
            }
            lsChoseField.sort();
            return lsChoseField;
        }
    }
    
    
    
    
    Map<String,cacweb_Mapping__c> mapCustonSet = cacweb_Mapping__c.getAll();
    List<RecordType> lsTipoRegistroDisponivel = getAvailableRecordType();
    
    public List<RecordType> getAvailableRecordType() {
        Schema.SObjectType objType = Case.SObjectType;
        List<RecordType> names = new List<RecordType>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                if (i.isAvailable()  && (!i.getName().equalsIgnoreCase('Mestre')|| !i.getName().equalsIgnoreCase('Master')))
                    names.add(new RecordType(id = i.getRecordTypeId(), name = i.getName()));
            }
        } 
        else 
            names.add(new RecordType(id = infos[0].getRecordTypeId(), name = infos[0].getName()));
        return names;
    }
    
    
    
}