public class FaturaDealer2Utils {

  private static final String objectFatura    = 'FaturaDealer2__c';
  private static final String registerFatura  = 'Register';
  private static final String invoiceFatura   = 'Invoice';

  public static final Id RECORDTYPEID_REGISTER  = Utils.getRecordTypeId(objectFatura, registerFatura);
  public static final Id RECORDTYPEID_INVOICE   = Utils.getRecordTypeId(objectFatura, invoiceFatura);

  public static Boolean validationFatura(FaturaDealer2__c fatura) {
    final Boolean isValidCpf = validationCpfCnpj(fatura);
    final Boolean isValidEmail = validationEmail(fatura);
    final Boolean isValidTelefone = validationTelefone(fatura);

    if(!String.isEmpty(fatura.Customer_Name__c)) {
			final List<String> parseList = fatura.Customer_Name__c.split(' ');
      if(parseList.size() < 2) {
        addMsg(fatura, 'Campo Nome, Sobrenome inválido | ');
      }
		}
    faturaDeliveryDate(fatura);
    faturaInvoiceDate(fatura);
    return isValidCpf && isValidEmail && isValidTelefone;
  }

  public static Boolean validationCpfCnpj(FaturaDealer2__c fatura) {
    System.debug('validationCpfCnpj() Invoice_Type__c: ' + fatura.Invoice_Type__c);
    if(!isFieldRequiredCpfCnpj(fatura)) {
      return false;
    }
    if(isCustomerTypeEstrangeiro(fatura)) {
      return true;
    }

    if(!ValidationUtils.isCpfCnpj(fatura.Customer_Identification_Number__c) ) {
      if(isCustomerTypeFisico(fatura)) {
        addMsg(fatura, 'Campo CPF inválido | ');
      } else if(isCustomerTypeJuridico(fatura)) {
        addMsg(fatura, 'Campo CNPJ inválido | ');
      }
      return false;
    }
    return true;
  }

  private static Boolean isFieldRequiredCpfCnpj(FaturaDealer2__c fatura) {
    System.debug('isFieldRequiredCpfCnpj() ');
    System.debug('Customer_Identification_Number__c: ' + fatura.Customer_Identification_Number__c);
    System.debug('Customer_Type__c: ' + fatura.Customer_Type__c);

    if(String.isEmpty(fatura.Customer_Identification_Number__c)) {
      msgErroCampoObrigado(fatura, 'CPF/CNPJ');
    }
    if(String.isEmpty(fatura.Customer_Type__c)) {
      msgErroCampoObrigado(fatura, 'Tipo de Cliente');
    }

    if(String.isEmpty(fatura.Customer_Identification_Number__c)
      || String.isEmpty(fatura.Customer_Type__c)) {
      return false;
    }
    return true;
  }

  public static Boolean isCustomerTypeEstrangeiro(FaturaDealer2__c fatura) {
    return fatura.Customer_Type__c.equalsIgnoreCase('Estrangeiro');
  }

  public static Boolean isCustomerTypeFisico(FaturaDealer2__c fatura) {
    return fatura.Customer_Type__c.equalsIgnoreCase('F');
  }

  public static Boolean isCustomerTypeJuridico(FaturaDealer2__c fatura) {
    return fatura.Customer_Type__c.equalsIgnoreCase('J');
  }

  public static Boolean validationEmail(FaturaDealer2__c fatura) {
    System.debug('validationEmail() Customer_Email__c: ' + fatura.Customer_Email__c);

    if(String.isEmpty(fatura.Customer_Email__c)){
      msgErroCampoObrigado(fatura, 'Email');
      return false;
    }
    if(!ValidationUtils.isEmail(fatura.Customer_Email__c)) {
      addMsg(fatura, 'Campo e-mail inválido | ');
      return false;
    }
    fatura.Customer_Email_Mirror__c = fatura.Customer_Email__c;
    return true;
  }

  public static Boolean validationTelefone(FaturaDealer2__c fatura) {
    System.debug('validationTelefone() RecordType: ' + fatura.RecordTypeId);
    final PhoneValidation phoneValidation = new PhoneValidation(fatura);
    if(isRecordTypeRegister(fatura)) {
      if(phoneValidation.getSumValid() < 2) {
        addMsg(fatura, 'São necessários dois telefones válidos | ');
        addMensagePhone(fatura, phoneValidation);
        return false;
      }
      return true;
    } else if(isRecordTypeInvoice(fatura)) {
      if(phoneValidation.getSumValid() < 1) {
        addMsg(fatura, 'É necessário um telefone válido | ');
        addMensagePhone(fatura, phoneValidation);
        return true;
      }
    }

    return false;
  }

  public static Boolean isRecordTypeInvoice(FaturaDealer2__c fatura) {
    return fatura.RecordTypeId == RECORDTYPEID_INVOICE;
  }

  public static Boolean isRecordTypeRegister(FaturaDealer2__c fatura) {
    return fatura.RecordTypeId == RECORDTYPEID_REGISTER;
  }

  private static void addMensagePhone(FaturaDealer2__c fatura, PhoneValidation phone) {
    if(phone.numTel1 == 0) {
      addMsg(fatura, 'Campo Telefone 1 inválido | ');
    }
    if(phone.numTel2 == 0) {
      addMsg(fatura, 'Campo Telefone 2 inválido | ');
    }
    if(phone.numTel3 == 0) {
      addMsg(fatura, 'Campo Telefone 3 inválido | ');
    }
  }

  public static Boolean validationChassi(FaturaDealer2__c fatura) {
    if(String.isEmpty(fatura.VIN__c)) {
      msgErroCampoObrigado(fatura, 'Chassi');
      return false;
    }

    if(!ValidationUtils.isChassiValido(fatura.VIN__c)) {
      addMsg(fatura, 'Campo Chassi inválido | ');
      return false;
    }
    return true;
    /*else {
      try {
        if(Integer.valueOf(fatura.VIN__c.substring(fatura.VIN__c.length()-6, fatura.VIN__c.length())) == 0){
          addMsg(fatura, 'Campo Chassi inválido | ');
          return false;
        }
        return true;
      }
      catch(Exception ex) {
        addMsg(fatura, 'Campo Chassi inválido | ');
        return false;
      }
    }*/
  }

  public static void faturaIntegrationDate(FaturaDealer2__c fatura) {
    System.debug('faturaIntegrationDate()');
    System.debug('Fatura::' + fatura);

    if(String.isEmpty(fatura.Integration_Date__c)) {
      return;
    }

    final List<String> token = fatura.Integration_Date__c.split(' ');
    if(token.size() != 2) {
       return;
    }

    try {
      fatura.Integration_Date_Mirror__c = DataUtils.parseDDmmYYYY(token[0]);
    } catch(Exception ex) {
      System.debug(ex);
    }
  }

  public static void faturaDeliveryDate(FaturaDealer2__c fatura) {
    if(String.isEmpty(fatura.Delivery_Date__c)) {
      fatura.Delivery_Date_Mirror__c = null;
      return;
    }

    if(fatura.Delivery_Date__c.indexOf('T') == -1) {
      fatura.Delivery_Date_Mirror__c = null;
      return;
    }
    try {
      fatura.Delivery_Date_Mirror__c = DataUtils.toDate(fatura.Delivery_Date__c);
    } catch(Exception ex) {
       System.debug(ex);
    }
  }

  public static void faturaInvoiceDate(faturaDealer2__c fatura){
      System.debug('faturaInvoiceDate()');
      if(String.isEmpty(fatura.Invoice_Data__c)) {
          return;
      }

      final List<String> token = fatura.Invoice_Data__c.split('T');
      if(token.size() != 2){
          return;
      }
      List<String> tempo = token[1].split('-')[0].split(':');

      try{
          Date data = Date.ValueOf(token[0]);
          fatura.Invoice_Date_Mirror__c = DateTime.newInstance(data.year(), data.month(), data.day(), Integer.valueOf(tempo[0]),Integer.valueOf(tempo[1]),Integer.valueOf(tempo[2]));
      }catch(Exception ex){
          System.debug(ex);
      }
  }

  public static Date validDate(String data) {
    List<String> arrayData = new List<String>();
    arrayData = data.split('/');
    return Date.valueOf(arrayData[2]+'-'+arrayData[1]+'-'+arrayData[0]);
  }

  private static void msgErroCampoObrigado(FaturaDealer2__c fatura, String campo) {
      String temp = 'Campo obrigatório ' + campo + ' não está preenchido' + ' | ';
      addMsg(fatura, temp);
  }

  public static void addMsg(FaturaDealer2__c fatura, String msg) {
    if(fatura.Error_Messages__c == null) {
      fatura.Error_Messages__c = '';
    }
    fatura.Error_Messages__c = fatura.Error_Messages__c + msg;
  }

  private class PhoneValidation {
    public Integer numTel1 { get;set; }
    public Integer numTel2 { get;set; }
    public Integer numTel3 { get;set; }
    public FaturaDealer2__c fatura { get;set;}
    public PhoneValidation(FaturaDealer2__c fatura) {
      this.numTel1 = 0;
      this.numTel2 = 0;
      this.numTel3 = 0;
      this.fatura = fatura;
    }
    public Integer getSumValid() {
      if(isNumberPhoneValid(this.fatura.Phone_1__c)) {
        numTel1++;
      }
      if(isNumberPhoneValid(this.fatura.Phone_2__c)) {
        numTel2++;
      }
      if(isNumberPhoneValid(this.fatura.Phone_3__c)) {
        numTel3++;
      }
      return numTel1 + numTel2 + numTel3;
    }
    private Boolean isNumberPhoneValid(String numberPhone) {
      if(String.isEmpty(numberPhone)) {
        return false;
      } else if(numberPhone.length() < 10 || numberPhone.length() > 11) {
        return false;
      }
      return ValidationUtils.isNumeroTelefone(numberPhone);
    }
  }
}