/** MyR  BackEnd: apex class that describes what is an insertable notification into the system 

    V1.0: S.Ducamp / Atos / 08.07.2012 - initial revision
	V2.0: D. Veron / Atos / 13.07.2016 - lifetime go from setting to template
	@version 16.10: SDP / F1415-US3657 / Create refresh after x days merge funcitonnality
	Patch : "email" is not anymore a channel option : D. Veron (AtoS)
**/
global without sharing class Myr_CustMessage_Cls {  

	//Inner class to store the information to identify the messages given in entry of the webservice
	global class Input {
		webservice String accountSfdcId;
		webservice String goldenid;
		webservice String typeid;
		webservice String brand;
		webservice String country;
		webservice String language;
	}
    
    //Inner class to set the current error/success code for the current message
    global class Status {
        webservice String code;
        webservice String message;
		webservice Input input; //Fill the inputs to easily retrieve the inputs used to build the status (even if the output table is in the same order than the input table)
        public Boolean checked = false;
		//@inner constructor
		global Status() {
			input = new Input();
		}
        global Boolean isOK() { 
            return ( (checked != null && checked && String.isBlank(code) && String.isBlank(message))
                     || (!String.isBlank(code) && !String.isBlank(message) && 
						( system.Label.Customer_Message_WS06MS000.equalsIgnoreCase(code) || system.Label.Customer_Message_WS06MS001.equalsIgnoreCase(code))
						) );  
        }
    }
    
    //Enum used only for test purposes and code coverage (simulate some cases very hard to reproduce)
    global enum GlobalFailureTest {SEARCH_ACCOUNT, SEARCH_VEHREL, SEARCH_USER, SEARCH_TEMPL, SEARCH_DUP, DB_UPSERT, CR_GLOBAL}
    
    //Enums to use for notifications
    global enum Channel {popin, message, email}
    //global enum Country {UK, Russia, France, Turkey, Germany, Italy, Brazil} => depends on what is set in the template table
    //global enum Language {en_US, fr, de, it, tr, ru, pt_PT, pt_BR} => depends on what is set in the template table
    global enum Brand {Renault, Dacia}
    
    // @return the Brand in function of the given String
    public static Brand BrandValueOf(String str_brand) {
    	if(str_brand == null){
    		return null;
    	}else if( str_brand.equalsIgnoreCase(Brand.Renault.name())) {
    		return Brand.Renault;
    	} else if ( str_brand.equalsIgnoreCase(Brand.Dacia.name())) {
    		return Brand.Dacia;
    	}
    	return null;
    }
    
    //members of the message 
    webservice String accountSfdcId; //SalesForce Id
    webservice String goldenId; //Renault Personal Data identifier (BCS, SIC or MDM) 
    webservice List<Channel> channels; 
    webservice String typeId; //not a enum to allow settings from salesforce side without delivering a new version of the WS 
    webservice String country; //=> depends on what is set in the template table
    webservice String language; //=> depends on what is set in the template table
    webservice String vin;
    webservice Brand brand;
    webservice String input1; //input to be used to replace the tag <input1> in the templates
    webservice String input2; //input to be used to replace the tag <input2> in the templates
    webservice String input3; //input to be used to replace the tag <input3> in the templates
    webservice String input4; //input to be used to replace the tag <input4> in the templates
    webservice String input5; //input to be used to replace the tag <input5> in the templates
    webservice Integer lifetime; //lifetime of the messages expressed in number of days
    webservice String title; //title of the notification: to replace the default title given by the template
    webservice String summary; //summary og the notification: to replace the default summary given by the template 
    webservice Blob body; //body of the notification: to replace the default body given by the template without applying the input replacement !
    webservice String cta_href; //click-to-action url: to replace the default
    webservice String cta_hrefmobile; //click-to-action url ofr mobile devices: to replace the default
    webservice String cta_label;
    webservice Boolean criticity; //Boolean to indicate the criticity of the message, by default false for not important
    webservice Date visibility; //Date from which the message will be visible in the front-end. By default, this value is set to today.
    
    //NOT WEBSERVICE MEMBERS: DO NOT EXPOSE THEM IN THE WEBSERV AS THERE ARE SET WHEN INSERTING THE MESSAGE !
    public Id account; //account to use for creating msg into salesforce:
    private VEH_Veh__c vehicle; 
    private Customer_Message_Templates__c template; //template to use for creation msg into salesforce
    private Customer_Message__c sfdcMessage;  //message to upsert
    private List<String> templMatchingKeys;
    private String dupAction;
	private Boolean dupKeepSfdcMsgAsItIs;
    @TestVisible private GlobalFailureTest failureSimulation; //do not expose this variable as it is just for test purposes (and use only in Test.isRunnngTest mode)
    
    //Set the template and prepare the matching keys
    global void setTemplate( Customer_Message_Templates__c iTempl ) {
        template = iTempl;
        if( !String.isBlank(template.MessageType__r.MergeKeys__c) ) {
            templMatchingKeys = template.MessageType__r.MergeKeys__c.split(';', 0);
        }
    }
    
    //Set the vehicle
    global void setVehicle(VEH_Veh__c veh) {
        vehicle = veh;
    }
    
    //@return Customer_Message_Templates__c, the template found for the current message
    global Customer_Message_Templates__c getTemplate() {
        return template;
    }
    
    //@return Customer_Message__c, the formatted Sfdc message
    global Customer_Message__c getSfdcMessage() {
        return sfdcMessage;
    }
    
    //@return String, the dup action applied
    global String getDupAction() {
        return dupAction;
    }

	//@return Boolean, indicating if salesforce should be updated or not after a dup
	global Boolean keepMsgInSfdcAsItIs() {
		return dupKeepSfdcMsgAsItIs;
	}
    
    //@return GlobalFailureTest, the global failure to test : test purposes only
    global GlobalFailureTest getTestFailure() {
        return failureSimulation; 
    }
    
    //@return the number of inputs for template, only the first inputs are used. For instance, if we have input1, input 2 and input5 filled, then the function
    //will return 2
    global Integer getNbInputsForTemplate() {
        Integer count = 0;
        if( !String.isEmpty(input1) ) {
            count++;
            if( !String.isEmpty(input2) ) {
                count++;
                if( !String.isEmpty(input3) ) {
                    count++;
                    if( !String.isEmpty(input4) ) {
                        count++;
                        if( !String.isEmpty(input5) ) {
                            count++;
                        }
                    }
                }
            } 
        }
        return count;
    }
    
    //check if the current notification is insertable into SalesForce. this valdiation does not take care about the existence or not of the accounts/settings/template
    //    This check should be done globally to optimize the queries.
    //    @return empty status if it's the case
    //    @return filled status if it's not the case  
    global Status isValid() {
        system.debug('### Myr_CustMessage_Cls - <isValid> - BEGIN: ' + this);
        Status status = new Status();
		//If the brand is not specified then use Renault by default
        if( brand == null ) {
            brand = Myr_CustMessage_Cls.Brand.Renault;
        }
		//Fill the inputs to easily retrieve the inputs used to build the status (even if the output table is in the same order than the input table)
		status.input.accountSfdcId = accountSfdcId;
		status.input.goldenid = goldenid;
		status.input.typeid = typeid;
		status.input.country = country;
		status.input.language = language;
		status.input.brand = brand.name();
		//by default, status is OK
        status.checked = true;
        if( String.isBlank(accountSfdcId) && String.isBlank(goldenId) ) {
            status.code = system.Label.Customer_Message_WS06MS501;
            status.message = system.Label.Customer_Message_WS06MS501_Msg + ' accountSfdcId or goldenId have to be specified to retrieve the account';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - accountId - ' + status.message);
            return status;
        } else if (!String.isBlank(accountSfdcId) && accountSfdcId.length() != 18 && accountSfdcId.length() != 15) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' accountSfdcId should have a length of 18 or 15 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - accountId - ' + status.message);
            return status;
        } else if (!String.isBlank(goldenId) && goldenId.length() > 50) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' goldenId has more than 50 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - goldenId - ' + status.message);
            return status;
        }
        if( String.isBlank(typeid) ) {
            status.code = system.Label.Customer_Message_WS06MS501;
            status.message = system.Label.Customer_Message_WS06MS501_Msg + ' typeId is mandatory';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - typeId - ' + status.message);
            return status;
        } else if( typeId.length() > 80 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' typeId has more than 80 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - typeId - ' + status.message);
            return status;
        }
        if (!String.isBlank(vin) && !Pattern.matches(system.Label.Customer_Message_Pattern_Vin, vin) ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' vin has not the proper format (17 alphanumeric characters)';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - vin - ' + status.message);
            return status;
        }
        if( !String.isEmpty(input1) && input1.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' input1 has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - input1 - ' + status.message);
            return status;
        }
        if( !String.isEmpty(input2) && input2.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' input2 has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - input2 - ' + status.message);
            return status;
        }
        if( !String.isEmpty(input3) && input3.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' input3 has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - input3 - ' + status.message);
            return status;
        }
        if( !String.isEmpty(input4) && input4.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' input4 has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - input4 - ' + status.message);
            return status;
        }
        if( !String.isEmpty(input5) && input5.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' input5 has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - input5 - ' + status.message);
            return status;
        }
        if( lifetime != null && !Pattern.matches(system.Label.Customer_Message_Pattern_Lifetime, String.valueOf(lifetime)) ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' lifetime has more than 3 digits';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - lifetime - ' + status.message);
            return status;
        }
        if( !String.isBlank(title) && title.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' title has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - title - ' + status.message);
            return status;
        }
        if( !String.isBlank(summary) && summary.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' summary has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - summary - ' + status.message);
            return status;
        }
        if( !String.isBlank(cta_href) && cta_href.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' cta_href has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - cta_href - ' + status.message);
            return status;
        }
        if( !String.isBlank(cta_hrefmobile) && cta_hrefmobile.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' cta_hrefmobile has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - cta_hrefmobile - ' + status.message);
            return status;
        }
        if( !String.isBlank(cta_label) && cta_label.length() > 255 ) {
            status.code = system.Label.Customer_Message_WS06MS502;
            status.message = system.Label.Customer_Message_WS06MS502_Msg + ' cta_label has more than 255 characters';
            system.debug('### Myr_CustMessage_Cls - <isValid> - Failed - cta_label - ' + status.message);
            return status;
        }
        return status;
    }
    
    //replace the tags <inputx> in the given string by the inputx parameters of the current message
    //@param the string in which we have to replace tags
    //@return the string with tag replaced
    @TestVisible
    private String replaceTags(String iMsg) {
        system.debug('### Myr_CustMessage_Cls - <replaceTags> - BEGIN: ' + iMsg);
        if( String.isBlank(iMsg) ) return iMsg;
        iMsg = (!String.isEmpty(input1)) ? iMsg.replace(system.Label.Customer_Message_Input1, input1) : iMsg;
        iMsg = (!String.isEmpty(input2)) ? iMsg.replace(system.Label.Customer_Message_Input2, input2) : iMsg;
        iMsg = (!String.isEmpty(input3)) ? iMsg.replace(system.Label.Customer_Message_Input3, input3) : iMsg;
        iMsg = (!String.isEmpty(input4)) ? iMsg.replace(system.Label.Customer_Message_Input4, input4) : iMsg;
        iMsg = (!String.isEmpty(input5)) ? iMsg.replace(system.Label.Customer_Message_Input5, input5) : iMsg;
        system.debug('### Myr_CustMessage_Cls - <replaceTags> - END: ' + iMsg);
        return iMsg;
    }
    
    //Replace the current sfdc message by the given one
    @TestVisible
    private void dupReplace(Customer_Message__c existingMsg) {
        if( existingMsg == null || sfdcMessage == null ) return;
        sfdcMessage.Id = existingMsg.Id; //replace the id to force a global upsert of the existing message
        dupAction = system.Label.Customer_Message_Tpl_Replace;
    }
    
    //Refresh the current sfdc message using the given one
    @TestVisible
    private void dupRefresh(Customer_Message__c msg) {
        if( msg == null || sfdcMessage == null ) return;
        //keep the given message: just update the expiration date and the channel
        Date expirationDate = sfdcMessage.ExpirationDate__c;
        String channel = sfdcMessage.Channel__c;    
        sfdcMessage = msg;
        //set the channel
        if( String.isBlank(sfdcMessage.Channel__c) ) {
            sfdcMessage.Channel__c = channel; //it does not change anything if it's already set :-p
        } else {
            List<String> lChannels = channel.split(';', 0);
            for( String ch : lChannels ) {
                if( !sfdcMessage.Channel__c.containsIgnoreCase(ch) ) {
                    sfdcMessage.Channel__c += (';' + ch);
                }
            }
        }
        //set the expiration date
        sfdcMessage.ExpirationDate__c = expirationDate;
        //refresh
        dupAction = system.Label.Customer_Message_Tpl_Refresh;
    }

	@TestVisible
	//if the status is read or deleted and the last status change was more than MergeRefresXDaysValue__c then
	//refresh and update the status to unread
	//otherwise nothing to be done !
	private void dupRefreshXDays(Customer_Message__c msg) {
		if( msg == null || sfdcMessage == null ) return;
		Datetime limitDate = system.today().addDays( -1 * Integer.valueOf(template.MessageType__r.MergeRefresXDaysValue__c) );
		system.debug('### Myr_CustMessage_Cls - <dupRefreshXDays> - msg.StatusUpdateDate__c='+msg.StatusUpdateDate__c+', limitDate=' + limitDate );
		if( msg.StatusUpdateDate__c < limitDate && (msg.Status__c == system.Label.Customer_Message_Read || msg.Status__c == system.Label.Customer_Message_Deleted) ) {
			system.debug('### Myr_CustMessage_Cls - <dupRefreshXDays> - apply refresh after x days');
			dupRefresh( msg );
			sfdcMessage.Status__c = system.Label.Customer_Message_Unread;
			dupAction = system.Label.Customer_Message_Tpl_RefreshXDays;
			system.debug('### Myr_CustMessage_Cls - <dupRefreshXDays> - dupKeepSfdcMsgAsItIs='+dupKeepSfdcMsgAsItIs);
		} else {
			dupKeepSfdcMsgAsItIs = true;
		}
	}
    
    //Keep the current sfdc message as it is and ignore the new message
    @TestVisible
    private void dupKeep(Customer_Message__c msg) {
        //nothing special to be done: simply ignore the new message
		dupKeepSfdcMsgAsItIs = true;
        dupAction = system.Label.Customer_Message_Tpl_Keep;
    }
    
    //Transverse the matching keys to detect a possible dup
    //@param the input Customer_Message__c
    //@return true if the member sfdc message is a dup of the input message considering the matching keys
    global Boolean isDup(Customer_Message__c msg) {
        system.debug('### Myr_CustMessage_Cls - <isDup> - BEGIN input=' + msg);
        system.debug('### Myr_CustMessage_Cls - <isDup> - BEGIN: sfdcMsg=' + this);
        //TODO check existence of matching key
        if( sfdcMessage.Account__c != msg.Account__c ) return false;
        if( sfdcMessage.Template__c != msg.Template__c ) return false;
        if( templMatchingKeys == null ) return false;
        Boolean isDup = true;
        for( String field : templMatchingKeys) {
            if( sfdcMessage.get(field) != null ) {
                String fieldValue = String.valueOf(sfdcMessage.get(field));
                isDup &= fieldValue.equalsIgnoreCase(String.valueOf(msg.get(field)));
            } else {
                isDup = false;
                break;
            }
        }
        system.debug('### Myr_CustMessage_Cls - <isDup> - END isDup=' + isDup);
        return isDup;
    }
    
    //Apply the appropriate dup action for all the matching duplicated messages in the list
    //@param the list of messages to control 
    global void applyDupAction(List<Customer_Message__c> listMsgs) {
        system.debug('### Myr_CustMessage_Cls - <applyDupAction> - BEGIN - listMsgs.size()=' + listMsgs.size());
        //initial controls to avoid further and useless analysis
        if( templMatchingKeys == null || sfdcMessage == null || listMsgs == null ) return; //nothing more to be done
        if( String.isBlank( template.MessageType__r.MergeAction__c) ) return; //no merge action so useless
		dupKeepSfdcMsgAsItIs = false;
        //in case of other merge action, continue
        for( Customer_Message__c msg : listMsgs ) {
            if( isDup(msg) ) {
                //if is Dup => apply the appropriate dup action
                if( template.MessageType__r.MergeAction__c.equalsIgnoreCase(system.Label.Customer_Message_Tpl_Replace) ) {
                    system.debug('### Myr_CustMessage_Cls - <applyDupAction> - DupAction=replace');
                    dupReplace(msg);
                } else if( template.MessageType__r.MergeAction__c.equalsIgnoreCase(system.Label.Customer_Message_Tpl_Refresh) ) {
                    system.debug('### Myr_CustMessage_Cls - <applyDupAction> - DupAction=refresh');
                    dupRefresh(msg);
                } else if( template.MessageType__r.MergeAction__c.equalsIgnoreCase(system.Label.Customer_Message_Tpl_Keep) ) {
                    system.debug('### Myr_CustMessage_Cls - <applyDupAction> - DupAction=keep');
                    dupKeep(msg);
                } else if( template.MessageType__r.MergeAction__c.equalsIgnoreCase(system.Label.Customer_Message_Tpl_RefreshXDays) ) {
                    system.debug('### Myr_CustMessage_Cls - <applyDupAction> - DupAction=refresh_after_x_days');
                    dupRefreshXDays(msg);
				}
            }
        }
        system.debug('### Myr_CustMessage_Cls - <applyDupAction> - END');
    }
    
    //Prepare the message for an insertion into SalesForce
    global void prepareSfdcMessage() {
        system.debug('### Myr_CustMessage_Cls - <prepareSfdcMessage> - BEGIN: ' + this);
        if( template == null ) {
            system.debug('### Myr_CustMessage_Cls - <prepareSfdcMessage> - Failed - no template');
            sfdcMessage = null;
            return;
        }
        sfdcMessage = new Customer_Message__c();
        sfdcMessage.Account__c = account;
        if( vehicle != null && vehicle.Name == vin ) {
            sfdcMessage.VIN__c = vehicle.Id;
        }
        sfdcMessage.Template__c = template.Id;
        sfdcMessage.Status__c = system.Label.Customer_Message_UnRead;
        sfdcMessage.Channel__c = '';
        //CHANNEL: prepare the list of channels
        
        //Channels
        if( channels == null || channels.size()==0) {
            sfdcMessage.Channel__c = template.Channel__c; 
        }else{
        	for( Channel ch : channels ) {
	            sfdcMessage.Channel__c += (!String.isBlank(sfdcMessage.Channel__c) && sfdcMessage.Channel__c.length() > 0) ? ';' : '';
	            sfdcMessage.Channel__c += ch;
	        }
        }
        //NOTE: Not enough inputs for templates: rejected
        if( (String.isBlank(title) || String.isBlank(summary) || String.isBLank(body.toString())) && getNbInputsForTemplate() < template.MessageType__r.NbInputs__c) {
            system.debug('### Myr_CustMessage_Cls - <prepareSfdcMessage> - Failed - not enough inputs');
            sfdcMessage = null;
            return;
        }
		//INPUTS: save the inputs (16.10)
		sfdcMessage.Input_1__c = input1;
		sfdcMessage.Input_2__c = input2;
		sfdcMessage.Input_3__c = input3;
		sfdcMessage.Input_4__c = input4;
		sfdcMessage.Input_5__c = input5;
        //TITLE: use the value given in input, or the default value in the associated template WITH TAGS <INPUTx> REPLACED
        sfdcMessage.Title__c = (!String.isBlank(title)) ? title : replaceTags(template.Title__c);
        //SUMMARY: use the value given in input, or the default value in the associated template WITH TAGS <INPUTx> REPLACED
        sfdcMessage.Summary__c = (!String.isBlank(summary)) ? summary : replaceTags(template.Summary__c);
        //BODY: use the value given in input, or the default value in the associated template WITH TAGS <INPUTx> REPLACED
        system.debug(' ### Myr_CustMessage_Cls - <prepareSfdcMessage> - body=' + body); 
        system.debug(' ### Myr_CustMessage_Cls - <prepareSfdcMessage> - body.toString()=' + ((body!=null)?body.toString():'<>'));
        sfdcMessage.Body__c = (body != null && body.size() > 0) ? body.toString() : replaceTags(template.Body__c);
        system.debug(' ### Myr_CustMessage_Cls - <prepareSfdcMessage> - sfdcMessage.Body__c=' + sfdcMessage.Body__c);
        //CTA-HREF: use the value given in input, or the default value in the associated template
        sfdcMessage.CtaHref__c = (!String.isBlank(cta_href)) ? cta_href : template.CtaHref__c;
        //CTA-HREFMOBILE: use the value given in input, or the default value in the associated template
        sfdcMessage.CtaHrefMobile__c = (!String.isBlank(cta_hrefmobile)) ? cta_hrefmobile : template.CtaHrefMobile__c;
        //CTA-LABEL: use the value given in input, or the default value in the associated template
        sfdcMessage.CtaLabel__c = (!String.isBlank(cta_label)) ? cta_label : template.CtaLabel__c;
        //EXPIRATION DATE: if a delay has been given in input, then use this value, otherwise set the expiration with the value given in the template. By default: no expiration date
        if( lifetime != null ) {
            sfdcMessage.ExpirationDate__c = system.today().addDays(lifetime);
        } else if( template.Lifetime__c != null ) {
            sfdcMessage.ExpirationDate__c = system.today().addDays(Integer.valueOf(template.Lifetime__c));
        } else {
            sfdcMessage.ExpirationDate__c = null;
        }
        //CRITICITY
        sfdcMessage.Criticity__c = (criticity != null) ? criticity : template.Criticity__c;
        //VISIBILITY DATE
        if( visibility != null ) {
            sfdcMessage.VisibilityDate__c = visibility; 
        }
		dupKeepSfdcMsgAsItIs = false;
    }
    
}