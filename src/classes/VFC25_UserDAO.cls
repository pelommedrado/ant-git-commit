/**
*	Class   -   VFC25_UserDAO
*   Author  -   SureshBabu
*   Date    -   23/10/2012
*    
*   #01 <Suresh Babu> <23/10/2012>
*      Created this Class to handle records from User related Queries.
*/
public with sharing class VFC25_UserDAO extends VFC01_SObjectDAO{
	private static final VFC25_UserDAO instance = new VFC25_UserDAO();
    
    /*private constructor to prevent the creation of instances of this class*/
    private VFC25_UserDAO(){}

    /**
    * Method responsible for providing the instance of this class.. 
    */  
    public static VFC25_UserDAO getInstance(){
        return instance;
    }
    
    /** 
	* This Method was used to get all User Records. 
    * @return userRecords - fetch and return the result in userRecords.
    */
    public List<User> fetchAllUserRecords(){
    	List<User> lstUserRecords = null;
    	
    	lstUserRecords = [SELECT
    						AboutMe, AccountId, IsActive, MobilePhone, City, CommunityNickname,
    						CompanyName, ContactId, Country, CreatedById, CreatedDate, Username, PostalCode,
    						DVESeller_BR__c, Email, FirstName, Name, LastName, Phone, ProfileId, UserRoleId,
    						State, Street, Title, Id, UserType, BIR__c
    					FROM 
    						User
    					WHERE
    						IsActive = true limit 49000
    					];
    	return lstUserRecords;
    }
    
    /** 
	* This Method was used to get all User Records. 
    * @return userRecords - fetch and return the result in userRecords.
    */
    public List<User> fetchUserRecordsUsingContactId(List<Id> contactIds){
    	List<User> lstUserRecords = null;
    	
    	lstUserRecords = [SELECT
    						AboutMe, AccountId, IsActive, MobilePhone, City, CommunityNickname,
    						CompanyName, ContactId, Country, CreatedById, CreatedDate, Username, PostalCode,
    						DVESeller_BR__c, Email, FirstName, Name, LastName, Phone, ProfileId, UserRoleId,
    						State, Street, Title, Id, UserType, BIR__c, Contact.Account.IDBIR__c 
    					FROM 
    						User
    					WHERE
    						IsActive = true And
    						ContactId IN : contactIds
    				    ORDER BY Name
    					];
    	return lstUserRecords;
    }
    
    /** 
	* This Method was used to get all User Records. 
    * @return userRecords - fetch and return the result in userRecords.
    */
    public List<User> fetchUserRecordsUsingContactId(List<Id> contactIds, Boolean DVE_Seller ){
    	List<User> lstUserRecords = null;
    	
    	lstUserRecords = [SELECT
    						AboutMe, AccountId, IsActive, MobilePhone, City, CommunityNickname,
    						CompanyName, ContactId, Country, CreatedById, CreatedDate, Username, PostalCode,
    						DVESeller_BR__c, Email, FirstName, Name, LastName, Phone, ProfileId, UserRoleId,
    						State, Street, Title, Id, UserType, BIR__c
    					FROM 
    						User
    					WHERE
    						IsActive = true And
    						ContactId IN : contactIds AND
    						DVESeller_BR__c = : DVE_Seller
    				    ORDER BY Name
    					];
    	return lstUserRecords;
    }
    
    
    /**
    *	This method was used to fetch the User record using User id, and return User record.
    *	@param	userId		-	User Id need to fetch the user record.
    *	@return userRecord	-	return the retched User record..
    */
    public User fetchUserByRecordId( Id userId ){
    	User userRecord  = new User();
    	
    	userRecord = [SELECT
    						AboutMe, AccountId, IsActive, MobilePhone, City, CommunityNickname,
    						CompanyName, ContactId, Country, CreatedById, CreatedDate, Username, PostalCode,
    						Email, FirstName, Name, LastName, Phone, ProfileId, UserRoleId,
    						State, Street, Title, Id, UserType, BIR__c, Contact.Account.IDBIR__c
    					FROM
    						User
    					WHERE
    						Id =: userId
    				];
    	return userRecord;
    }
    
    /** 
	* This Method was used to get all User Records using sales person Id.
	*	@param 	setSalesPersonIds	-	set of salesperson Ids.
    *	@return userRecords 		-	fetch and return the result in userRecords.
    */
    public List<User> fetchUserRecords_UsingSalesPersonIds(Set<Id> setSalesPersonIds){
    	List<User> lstUserRecords = null;
    	
    	lstUserRecords = [SELECT Id, Contact.Id, Contact.Name, Contact.Phone, 
	                                Contact.Email, Contact.CPF__c, Phone
	                         FROM 
	                         	User
	                         WHERE 
	                         	Id IN : setSalesPersonIds];
    	return lstUserRecords;
    }
    
}