/**
* Classe que representa os dados dos documentos RVA.
* @author Felipe Jesus Silva.
*/
public class VFC108_DocumentationRVAVO 
{
	public String id {get;set;}
	public String name {get;set;}
	
	public VFC108_DocumentationRVAVO()
	{
		
	}
}