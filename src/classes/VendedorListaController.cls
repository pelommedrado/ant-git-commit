public class VendedorListaController {
    
    public List<Contact> vendedorList 	{ get;set; }
    public Id accId 					{ get;set; }
    public Contact ctt 					{ get;set; }
    public String availableSelect 		{ get;set; }
    
    public List<SelectOption> availableSelectOption {
        get {
            return Utils.getPicklistValues('Contact', 'Available_to_SFA__c');
        }
    }
    
    public VendedorListaController() {
        this.ctt = new Contact();
        this.vendedorList = new List<Contact>();
        
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        
        this.accId  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        filtrar();
    }
    
    public PageReference filtrar() {
        System.debug('Filtrando resultados...');
        //System.debug('Tipo servico:' + tipoServico);
        
        String query = ' SELECT Id, Name, ' + 
            'Account.Name,' + 
            'Phone, ' +
            'Email, ' +
            'Available_to_SFA__c ' +
            
            'FROM Contact ' +
            'WHERE AccountId =: accID ' +
            (ctt.Available_to_SFA__c != null ? ' AND Available_to_SFA__c = \'' + ctt.Available_to_SFA__c + '\' ' : '') + 
            'ORDER BY Name';
        
        this.vendedorList = Database.query(query);
        
        if(this.vendedorList == null) {
            this.vendedorList = new List<Contact>();
        }
        return null;
    }
    
    public PageReference novoContato() {
        PageReference pRef = new PageReference(
            '/cac/003/e?retURL=/cac/VendedorLista&RecordType=012D0000000KApJ&ent=Contact');
        return pRef;
    }
    
    public PageReference editarContato() {
        String id = 
            ApexPages.currentPage().getParameters().get('cttId');
        
        PageReference pRef = new PageReference(
            '/cac/' + id + '/e?retURL=/cac/VendedorLista&RecordType=012D0000000KApJ&ent=Contact');
        return pRef;
    }
    
}