@IsTest
private class MonthlySellerQuoteExecutionTest {
    
    @Future
    static void insertUser() {
        Profile partnerProfile = Myr_Users_Utilities_Class.getProfile('SFA - Seller');
        String language = '';
        User usr = new User();  
        usr.FirstName = 'firstname';
        usr.LastName = 'lastname';
        usr.Username =  Myr_MyRenaultTools.removeAllSpaces(usr.FirstName + '.' + usr.LastName + '@atos.net');
        usr.Email = usr.Username;
        usr.CommunityNickname = Myr_Users_Utilities_Class.createCommunityNickName(usr.firstname, usr.lastname);
        usr.Alias = usr.FirstName.left(1) + usr.LastName.left(3); 
        usr.ProfileId = partnerProfile.Id;
        usr.TimeZoneSidKey = 'GMT';
        usr.LocaleSidKey = 'fr_FR_EURO';
        usr.EmailEncodingKey = 'ISO-8859-1';
        usr.LanguageLocaleKey = (String.isBlank(language)?'fr':language);
        usr.ContactId = cont.Id;
        usr.BIR__c = '123465';
        //usr.RecordDefaultCountry__c = country;
        
        Database.insert( usr );
        
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'];
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = usr.ID;
        psa.PermissionSetId = ps.Id;
        INSERT psa;
    }
    
    static testMethod void test1() {
        insertUser();
        
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_Seller'];
        List<PermissionSetAssignment> l = [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId=:ps.id];
        opportunity1.OwnerId = l.get(0).AssigneeId;
        UPDATE opportunity1;
        
        quote.Status = 'Billed';
        List<Quote> lQ = new List<Quote>();
        lQ.add(quote);
        new MonthlySellerQuoteExecution().createMonthlyGoalSellerRelation(lQ);
    }
    
    static testMethod void test2() {
        new MonthlySellerQuoteExecution().createSellerRelation(null, null);
    }
    
    static Opportunity opportunity1;
    static Quote quote;
    static Account account1;
    static Contact cont;
    
    static {
        
        account1 = new Account(
            Name = 'Account Test 01',
            RecordTypeId = [select id from RecordType where developerName='Network_Site_Acc' limit 1 ].id,
            IDBIR__c = '1001');
        insert account1;
        
        cont = new Contact(
            RecordTypeId ='012D0000000KApJIAW',
            LastName = 'teste',
            // Name = 'teste',
            AccountId = account1.Id,
            CPF__c = '898.612.386-03',
            Email='teste2@teste.com.br',
            Phone = '1122334455',
            Available_to_Service__c = 'Available'
        );
        
        Database.insert( cont );
        
        Id recordTypeId = getRecordTypeId('Personal_Acc');
        
        //cria e insere a conta que representa a concessionária
        Account sObjAccPersonal = new Account();
        sObjAccPersonal.CustomerIdentificationNbr__c = '08233350540';
        sObjAccPersonal.FirstName = 'Personal';
        sObjAccPersonal.LastName = 'Account';
        sObjAccPersonal.Phone='1000';
        sObjAccPersonal.RecordTypeId = recordTypeId; 
        sObjAccPersonal.ProfEmailAddress__c = 'personal@mail.com'; 
        sObjAccPersonal.ShippingCity = 'Paris';
        sObjAccPersonal.ShippingCountry = 'France';
        sObjAccPersonal.ShippingState = 'IDF';
        sObjAccPersonal.ShippingPostalCode = '75013';
        sObjAccPersonal.ShippingStreet = 'my street';
        sObjAccPersonal.YrReturnVehicle_BR__c = 2013;
        sObjAccPersonal.VehicleInterest_BR__c = 'SANDERO';
        //sObjAccPersonal.PersLandline__c = '(11) 3333-4444';
        //sObjAccPersonal.PersMobPhone__c = '(11) 5555-6666';
        sObjAccPersonal.RgStateTexto__c = '44787456';
        sObjAccPersonal.PersEmailAddress__c = 'email@gmail.com';
        sObjAccPersonal.PersonHomePhone = '1133334444';
        sObjAccPersonal.PersLandline__c = '1133334444';
        sObjAccPersonal.PersMobPhone__c = '1155556666';
        sObjAccPersonal.Sex__c = 'Man';
        sObjAccPersonal.MaritalStatus__c = '1-MARRIED';
        sObjAccPersonal.PersonBirthdate = System.today();
        insert sObjAccPersonal;
        
        opportunity1 = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            //OwnerId = us.Id,
            AccountId = sObjAccPersonal.Id);
        insert opportunity1;
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Product2 prd = moc.criaProduct2();
        Insert prd;
        
        Pricebook2 pb = moc.criaStdPricebook2();
        Update pb;
        
        Product2 prd2 = moc.criaProduct2();
        prd2.ModelSpecCode__c = 'Name';
        prd2.Model__c = prd.Id;
        Insert prd2;
        
        PricebookEntry pe = moc.criaPricebookEntry();
        pe.Pricebook2Id = pb.Id;
        pe.Product2Id = prd.Id;
        Insert pe;
       
        VFC61_QuoteDetailsVO orc = 
            Sfa2Utils.criarOrcamento(
                opportunity1.Id, 
                null);
        quote = [
            SELECT ID, Status, OpportunityId
            FROM Quote
            WHERE ID =: orc.Id
        ];
    }
    
    static Id getRecordTypeId(String developerName) {
        RecordType sObjRecordType = [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName =: developerName];
        
        return sObjRecordType.Id;
    }
}