public class QuoteTransactionExecution {
    public void execute(List<Quote> quoteList) {
        
        List<TST_Transaction__c> transList = new List<TST_Transaction__c>();
        
        for(Quote quote: quoteList) {
            TST_Transaction__c trans = new TST_Transaction__c();
            trans.RecordTypeId 				= Utils.getRecordTypeId('TST_Transaction__c', 'SFA');
            trans.Type__c 					= 'Quote';
            trans.TransactionStatus__c 		= quote.Status;
            trans.Account__c 				= quote.Opportunity.AccountId;
            trans.Opportunity__c			= quote.OpportunityId;
            trans.OwnerId 					= quote.LastModifiedById;
            trans.Quote__c					= quote.Id;
            transList.add(trans);
        }
        
        insert transList;
    } 
}