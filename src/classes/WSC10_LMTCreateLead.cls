global class WSC10_LMTCreateLead {
    
    global enum Brands {
        Renault
            }  
    
    global class AfterSales {
        webservice String description;
        webservice Date endDateOfValidityOfTheOffer;
        webservice String quoteInterventionDuration;  // alterado de Decimal para String
        webservice Decimal quotePrice;
        webservice Date quoteStartDate;
        webservice String quoteURI;
        webservice String quoteWishedOperations; 
    }
    
    global class Client {
        webservice String acceptEMailContact;
        webservice String acceptPhoneContact;
        webservice String acceptSMSContact;
        webservice String additionalInfo;
        webservice String brochure;
        webservice String businessMobilePhone;
        webservice String businessPhone;
        webservice String city;
        webservice String country;
        webservice String eMail;
        webservice String firstName;
        webservice String houseNumber;
        webservice String intentToTakeBack;
        webservice String lastName;
        webservice String mobilePhone;
        webservice String phone;
        webservice String place;
        webservice String postalCode;
        webservice String salutation;
        webservice String street;
        webservice String registrationNbr;
		webservice String state;
    }
    
    global class ClientPro {
        webservice String company;
        webservice String proEmail;
        webservice String proOwnedFleet;
        webservice String proRegistrationNbr;
        webservice String purchaseVolume;
        webservice String type;
    }
    
    global class Dealer {
        webservice Date appointmentDate;
        webservice Date createdDateByCustomer;
        webservice String dealerOfInterest;
        webservice DateTime exportDateTime;
        webservice Date firstActionDate;
        webservice Date lateDate;
        webservice String preassignedSeller;
        webservice String raiser;
        webservice Date warningDate;
    }
    
    global class Finance { 
        webservice Integer financeDeposit;
        webservice String financeLoanTime;
        webservice Integer financePreac;
        webservice Boolean fundingRequest;
        webservice Decimal monthlyPayment;
        webservice String typeFinancement;
    }
    
    //global class Lead {
    global class LeadInfo {
        webservice Date appointmentEndTime;
        webservice Date appointmentStartTime;
        webservice String campaign;
        webservice String comment;
        webservice String context;
        webservice String description;
        webservice String device;
        webservice String leadF6Id;
        webservice String leadSource;
        //webservice String mailDiscount;
        webservice Decimal mailDiscount;
        webservice String orgin;
        webservice String promotionType;
        webservice String sbolPromotion;
        webservice String subTypeOfInterest;
        webservice String typeOfInterest;
        
        //added
        webservice String leadProvider;
        webservice Brands brandOfInterest;
        webservice String VehicleOfInterest;
        webservice String recordType;
        webservice String internetSupport;
        webservice String customerExternalKey;
        webservice String secondVehicleOfInterest;
    }
    
    global class OwnVehicle {
        webservice String annualMileage;
        webservice String brand;
        webservice String energy;
        webservice String engine;
        webservice String firstRegistration;
        webservice String mileage;
        webservice String model;
        webservice String registrationNbr;
        webservice String version;
        webservice String VIN;
    }
    
    global class ServicesContract {
        webservice Decimal serviceContractsAnnualPrice;
        webservice Decimal serviceContractsAnnualPriceVAT;
        webservice Boolean serviceContractsCarLoan;
        webservice Integer serviceContractsDuration;
        webservice String serviceContractsKilometer;
        webservice String serviceContractsProductType;
        webservice String serviceContractsRequest;
    }
    
    global class Vehicle {
        webservice String brand;
        webservice String energy;
        webservice String tireConfiguratorBrand;
        webservice String tireConfiguratorDiameter;
        webservice String tireConfiguratorHeight;
        webservice String tireConfiguratorSpeed;
        webservice String tireConfiguratorType;
        webservice String tireConfiguratorWidth;
        webservice String vehicleBodyType;
        webservice String vehicleColor;
        webservice String vehicleConfiguratorURI;
        webservice String vehicleCotation;
        webservice Decimal vehicleDiscountPrice;
        webservice String vehicleEngine;
        webservice String vehicleID;
        webservice String vehicleOptions;
        webservice String vehicleRegistrationNumber;
        webservice String vehicleRirstRegistration;
        webservice String vehicleVersion;
        webservice String vehicleVIN;
        webservice String voNumber;
    }
    
    global class LeadInput {
        webservice AfterSales afterSales;
        webservice Client client;
        webservice ClientPro clientPro;
        webservice Dealer dealer;
        webservice Finance finance;
        //webservice Lead lead;
        webservice LeadInfo leadInfo;
        webservice OwnVehicle ownVehicle;
        webservice ServicesContract servicesContract;
        webservice Vehicle vehicle;
    }
    
    global class WebServiceResultList {
       webService String code;
       webService String libelle;
       webService String status;
    }
    
    //WebService static String createLead(LeadInput leadInput) {
    WebService static WebServiceResultList createLead(List<LeadInput> leadInputs) {
        
        List<Lead> leadList = new List<Lead>();
        List<Lead> leadToCampMemberList = new List<Lead>();
        WebServiceResultList result = new WebServiceResultList();
        
        for(LeadInput leadInput :  leadInputs ) {
            Lead lead = leadCreate(leadInput);
            leadList.add(lead);
        }
        
        /* method add by @Edvaldo - [ kolekto ] to insert incomplet list in case of errors*/
        
        String errorInLeadCreation = '';
        List<Database.SaveResult> dbresult = Database.Insert(leadList,false);
        
        Integer qtdErros = 0;
        for(Integer i=0; i < dbresult.size(); i++){
            if (dbresult.get(i).isSuccess()){
                leadToCampMemberList.add(leadList.get(i));
            }else{
                //failed record from the list
                qtdErros += qtdErros + 1;
                errorInLeadCreation += 'Lead Name: ' + leadList.get(i).FirstName + ' ' + leadList.get(i).LastName +
                    ' CPF/CNPJ: ' + (leadList.get(i).CPF_CNPJ__c != NULL ? leadList.get(i).CPF_CNPJ__c + ' ' : ' ') +
                    dbresult.get(i).getErrors().get(0) + '\n';
                
                system.debug('errorInLeadCreation: '+errorInLeadCreation);
            }
        }
        
        // Create Campaign Member to convert in CampaignMember's trigger
        WebServiceResultList errorInCampMemberCreation = createCampMember(leadToCampMemberList);
        
        if(!errorInLeadCreation.equals('') || !errorInCampMemberCreation.libelle.equals('')){
            String erro = errorInLeadCreation + errorInCampMemberCreation.libelle;
            String status = String.valueOf(qtdErros) + ' Lead(s) records could not be processed. ' 
                + errorInCampMemberCreation.status;
            result = createErrorList('2', erro, status);
            return result;
        }
        
        result = createErrorList('0','','Lead(s) and CampaignMember(s) creation OK');
        
        //envia email para envolvidos
        sendMail(result);
        
        return result;
    }
    
    public static void sendMail(WebServiceResultList result){
        
        List<String> emailsToSend = Label.WSC10EmailsToSend.split(',');
        
        if(emailsToSend != NULL){
            
            List<Messaging.SingleEmailMessage> listMails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses( emailsToSend );
            mail.setSubject( 'WSC10_LMTCreateLead' );
            
            String conteudo;
            
            conteudo = 'Error Code: ' + result.code + '\n' +
                'Error: ' + result.libelle + '\n' +
                'Status: ' + result.status;
            
            mail.setPlainTextBody( conteudo );
            listMails.add(mail);
            
            //Messaging.sendEmail( new List< Messaging.Email >{ mail } );
            Messaging.sendEmail( listMails );
            
        }
    }
    
    public static void enviarEmail(List<LeadInput> leadInputs) {
        //added / SDP 
        List<Messaging.SingleEmailMessage> listMails = new List<Messaging.SingleEmailMessage>();
        for(LeadInput leadInput :  leadInputs ) {
            //added / SDP       
            System.debug('CreateLead:' + leadInput);
            
            String conteudo = '';
            
            AfterSales afterSales = leadInput.afterSales;
            //added / SDP
            if( afterSales != null ) {
                //added / SDP
                conteudo += '-- AfterSales -- \n';
                conteudo += 'description: '                 + afterSales.description + ' \n';
                conteudo += 'endDateOfValidityOfTheOffer: ' + afterSales.endDateOfValidityOfTheOffer + ' \n';
                conteudo += 'quoteInterventionDuration: '   + afterSales.quoteInterventionDuration + ' \n';
                conteudo += 'quotePrice: '                  + afterSales.quotePrice + ' \n';
                conteudo += 'quoteStartDate: '              + afterSales.quoteStartDate + ' \n';
                conteudo += 'quoteURI: '                    + afterSales.quoteURI + ' \n';
                conteudo += 'quoteWishedOperations: '       + afterSales.quoteWishedOperations + ' \n';
            }
            
            Client client = leadInput.client;
            //added / SDP
            if( client != null ) {
                //added / SDP
                conteudo += '-- Client -- \n';
                conteudo += 'acceptEMailContact: '  + client.acceptEMailContact + ' \n';
                conteudo += 'acceptPhoneContact: '  + client.acceptPhoneContact + ' \n';
                conteudo += 'acceptSMSContact: '    + client.acceptSMSContact + ' \n';
                conteudo += 'additionalInfo: '      + client.additionalInfo + ' \n';
                conteudo += 'brochure: '            + client.brochure + ' \n';
                conteudo += 'businessMobilePhone: ' + client.businessMobilePhone + ' \n';
                conteudo += 'businessPhone: '       + client.businessPhone + ' \n';
                conteudo += 'city: '                + client.city + ' \n';
                conteudo += 'country: '             + client.country + ' \n';
                conteudo += 'eMail: '               + client.eMail + ' \n';
                conteudo += 'firstName: '           + client.firstName + ' \n';
                conteudo += 'houseNumber: '         + client.houseNumber + ' \n';
                conteudo += 'intentToTakeBack: '    + client.intentToTakeBack + ' \n';
                conteudo += 'lastName: '            + client.lastName + ' \n';
                conteudo += 'mobilePhone: '         + client.mobilePhone + ' \n';
                conteudo += 'phone: '               + client.phone + ' \n';
                conteudo += 'place: '               + client.place + ' \n';
                conteudo += 'postalCode: '          + client.postalCode + ' \n';
                conteudo += 'salutation: '          + client.salutation + ' \n';
                conteudo += 'street: '              + client.street + ' \n';
            }
            
            ClientPro clientPro = leadInput.clientPro;
            //added / SDP
            if( clientPro != null ) {
                //added / SDP
                conteudo += '-- ClientPro -- \n';
                conteudo += 'company: '                 + clientPro.company + ' \n';
                conteudo += 'proEmail: '                + clientPro.proEmail + ' \n';
                conteudo += 'proOwnedFleet: '           + clientPro.proOwnedFleet + ' \n';
                conteudo += 'proRegistrationNbr: '      + clientPro.proRegistrationNbr + ' \n';
            }
            
            Dealer dealer = leadInput.dealer;
            //added / SDP
            if( dealer != null ) {
                //added / SDP
                conteudo += '-- Dealer -- \n';
                conteudo += 'appointmentDate: '         + dealer.appointmentDate + ' \n';
                conteudo += 'createdDateByCustomer: '   + dealer.createdDateByCustomer + ' \n';
                conteudo += 'dealerOfInterest: '        + dealer.dealerOfInterest + ' \n';
                conteudo += 'exportDateTime: '          + dealer.exportDateTime + ' \n';
                conteudo += 'firstActionDate: '         + dealer.firstActionDate + ' \n';
                conteudo += 'lateDate: '                + dealer.lateDate + ' \n';
                conteudo += 'preassignedSeller: '       + dealer.preassignedSeller + ' \n';
                conteudo += 'raiser: '                  + dealer.raiser + ' \n';
                conteudo += 'warningDate: '             + dealer.warningDate + ' \n';
            }
            
            Finance finance = leadInput.finance;
            //added / SDP
            if( finance != null ) {
                //added / SDP
                conteudo += '-- Finance -- \n';
                conteudo += 'financeDeposit: '      + finance.financeDeposit + ' \n';
                conteudo += 'financeLoanTime: '     + finance.financeLoanTime + ' \n';
                conteudo += 'financePreac: '        + finance.financePreac + ' \n';
                conteudo += 'fundingRequest: '      + finance.fundingRequest + ' \n';
                conteudo += 'fundingRequest: '      + finance.fundingRequest + ' \n';
                conteudo += 'monthlyPayment: '      + finance.monthlyPayment + ' \n';
                conteudo += 'typeFinancement: '     + finance.typeFinancement + ' \n';
            }
            
            //Lead lead = leadInput.lead;
            LeadInfo lead = leadInput.leadInfo;
            //added / SDP
            if( lead != null ) {
                //added / SDP
                conteudo += '-- Lead -- \n';
                conteudo += 'appointmentEndTime: '      + lead.appointmentEndTime + ' \n';        
                conteudo += 'appointmentStartTime: '    + lead.appointmentStartTime + ' \n';
                conteudo += 'campaign: '                + lead.campaign + ' \n';
                conteudo += 'comment: '                 + lead.comment + ' \n';
                conteudo += 'context: '                 + lead.context + ' \n';
                conteudo += 'description: '             + lead.description + ' \n';
                conteudo += 'device: '                  + lead.device + ' \n';
                conteudo += 'leadF6Id: '                + lead.leadF6Id + ' \n';
                conteudo += 'leadSource: '              + lead.leadSource + ' \n';
                conteudo += 'mailDiscount: '            + lead.mailDiscount + ' \n';
                conteudo += 'orgin: '                   + lead.orgin + ' \n';
                conteudo += 'promotionType: '           + lead.promotionType + ' \n';
                conteudo += 'sbolPromotion: '           + lead.sbolPromotion + ' \n';
                conteudo += 'subTypeOfInterest: '       + lead.subTypeOfInterest + ' \n';
                conteudo += 'typeOfInterest: '          + lead.typeOfInterest + ' \n';
            }
            
            OwnVehicle ownVehicle = leadInput.ownVehicle;
            //added / SDP
            if( ownVehicle != null ) {
                //added / SDP
                conteudo += '-- OwnVehicle -- \n';
                conteudo += 'annualMileage: '       + ownVehicle.annualMileage + ' \n'; 
                conteudo += 'brand: '               + ownVehicle.brand + ' \n'; 
                conteudo += 'energy: '              + ownVehicle.energy + ' \n'; 
                conteudo += 'engine: '              + ownVehicle.engine + ' \n'; 
                conteudo += 'firstRegistration: '   + ownVehicle.firstRegistration + ' \n'; 
                conteudo += 'mileage: '             + ownVehicle.mileage + ' \n'; 
                conteudo += 'model: '               + ownVehicle.model + ' \n'; 
                conteudo += 'registrationNbr: '     + ownVehicle.registrationNbr + ' \n'; 
                conteudo += 'version: '             + ownVehicle.version + ' \n'; 
                conteudo += 'VIN: '                 + ownVehicle.VIN + ' \n'; 
            }
            
            ServicesContract servicesContract = leadInput.servicesContract;
            //added / SDP
            if( servicesContract != null ) {
                //added / SDP
                conteudo += '-- ServicesContract -- \n';
                conteudo += 'serviceContractsAnnualPrice: '     + servicesContract.serviceContractsAnnualPrice + ' \n'; 
                conteudo += 'serviceContractsAnnualPriceVAT: '  + servicesContract.serviceContractsAnnualPriceVAT + ' \n'; 
                conteudo += 'serviceContractsCarLoan: '         + servicesContract.serviceContractsCarLoan + ' \n'; 
                conteudo += 'serviceContractsDuration: '        + servicesContract.serviceContractsDuration + ' \n'; 
                conteudo += 'serviceContractsKilometer: '       + servicesContract.serviceContractsKilometer + ' \n'; 
                conteudo += 'serviceContractsProductType: '     + servicesContract.serviceContractsProductType + ' \n'; 
                conteudo += 'serviceContractsRequest: '         + servicesContract.serviceContractsRequest + ' \n'; 
            }
            
            Vehicle vehicle = leadInput.vehicle;
            //added / SDP
            if( vehicle != null ) {
                //added / SDP
                conteudo += '-- Vehicle -- \n';
                conteudo += 'brand: '   + vehicle.brand + ' \n'; 
                conteudo += 'energy: '                      + vehicle.energy + ' \n'; 
                conteudo += 'tireConfiguratorBrand: '       + vehicle.tireConfiguratorBrand + ' \n'; 
                conteudo += 'tireConfiguratorDiameter: '    + vehicle.tireConfiguratorDiameter + ' \n'; 
                
                conteudo += 'tireConfiguratorHeight: '  + vehicle.tireConfiguratorHeight + ' \n'; 
                conteudo += 'tireConfiguratorSpeed: '   + vehicle.tireConfiguratorSpeed + ' \n'; 
                conteudo += 'tireConfiguratorType: '    + vehicle.tireConfiguratorType + ' \n'; 
                conteudo += 'tireConfiguratorWidth: '   + vehicle.tireConfiguratorWidth + ' \n';
                
                conteudo += 'vehicleBodyType: '         + vehicle.vehicleBodyType + ' \n'; 
                conteudo += 'vehicleColor: '            + vehicle.vehicleColor + ' \n'; 
                conteudo += 'vehicleConfiguratorURI: '  + vehicle.vehicleConfiguratorURI + ' \n'; 
                conteudo += 'vehicleCotation: '         + vehicle.vehicleCotation + ' \n'; 
                
                conteudo += 'vehicleDiscountPrice: '    + vehicle.vehicleDiscountPrice + ' \n'; 
                conteudo += 'vehicleEngine: '           + vehicle.vehicleEngine + ' \n'; 
                conteudo += 'vehicleID: '               + vehicle.vehicleID + ' \n'; 
                conteudo += 'vehicleOptions: '          + vehicle.vehicleOptions + ' \n'; 
                
                conteudo += 'vehicleRegistrationNumber: '   + vehicle.vehicleRegistrationNumber + ' \n'; 
                conteudo += 'vehicleRirstRegistration: '    + vehicle.vehicleRirstRegistration + ' \n'; 
                conteudo += 'vehicleVersion: '              + vehicle.vehicleVersion + ' \n'; 
                conteudo += 'vehicleVIN: '                  + vehicle.vehicleVIN + ' \n'; 
                conteudo += 'voNumber: '                    + vehicle.voNumber + ' \n'; 
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses( new List<String> {  'hugo@kolekto.com.br', 'helios@makolab.pl' } );
            mail.setSubject( 'WS LMT - createLead' );
            mail.setPlainTextBody( conteudo );
            //added / SPD
            listMails.add(mail);
            //added / SPD
        }
        
        //Messaging.sendEmail( new List< Messaging.Email >{ mail } );
        Messaging.sendEmail( listMails );
    }
    
    public static Lead leadCreate(LeadInput leadInput){
        
        Lead lead = new Lead();
        lead.BIR_Dealer_of_Interest__c = String.isNotEmpty(leadInput.Dealer.dealerOfInterest) ?
            leadInput.Dealer.dealerOfInterest : '';
        lead.City__c = String.isNotEmpty(leadInput.Client.city) ?
            leadInput.Client.city : '';
        lead.company = String.isNotEmpty(leadInput.ClientPro.company) ?
            leadInput.ClientPro.company : '';
        lead.CPF_CNPJ__c = String.isNotEmpty(leadInput.Client.registrationNbr) ? 
            leadInput.Client.registrationNbr : String.isNotEmpty(leadInput.ClientPro.proRegistrationNbr) ? 
            leadInput.ClientPro.proRegistrationNbr : '';
        lead.CurrencyIsoCode = String.isNotEmpty(leadInput.LeadInfo.device)? 
            leadInput.LeadInfo.device : '';
        lead.Customer_External_Key__c = String.isNotEmpty(leadInput.leadInfo.customerExternalKey) ? 
            leadInput.LeadInfo.customerExternalKey : '';
        lead.Description = String.isNotEmpty(leadInput.leadInfo.description) ? 
            leadInput.leadInfo.description : '';
        lead.Detail__c = String.isNotEmpty(leadInput.leadInfo.typeOfInterest) ?
            leadInput.LeadInfo.typeOfInterest : '';
        lead.Email = String.isNotEmpty(leadInput.Client.eMail) ?
            leadInput.Client.eMail : String.isNotEmpty(leadInput.ClientPro.proEmail) ?
            leadInput.ClientPro.proEmail : '';
        lead.FirstName = String.isNotEmpty(leadInput.Client.firstName) ? 
            leadInput.Client.firstName : '';
        lead.Form_ID__c = String.isNotEmpty(leadInput.leadInfo.leadF6Id) ?
            leadInput.LeadInfo.leadF6Id : '';
        lead.Home_Phone_Web__c = String.isNotEmpty(leadInput.Client.phone) ? 
            leadInput.Client.phone : '';
        lead.Internet_Support__c = String.isNotEmpty(leadInput.leadInfo.internetSupport) ?
            leadInput.LeadInfo.internetSupport : '';
        lead.LastName = String.isNotEmpty(leadInput.Client.lastName) ?
            leadInput.Client.lastName : '';
        lead.LeadSource = String.isNotEmpty(leadInput.leadInfo.leadSource) ?
            leadInput.LeadInfo.leadSource : '';
        lead.Mobile_Phone__c = String.isNotEmpty(leadInput.Client.mobilePhone) ?
            leadInput.Client.mobilePhone : '';
        lead.NUMDBM__c = String.isNotEmpty(leadInput.LeadInfo.comment) ? 
            Decimal.valueOf(leadInput.LeadInfo.comment) : 0;
        lead.OptinEmail__c = String.isNotEmpty(leadInput.Client.acceptEMailContact) ?
            leadInput.Client.acceptEMailContact : '';
        lead.OptinSMS__c = String.isNotEmpty(leadInput.Client.acceptSMSContact) ?
            leadInput.Client.acceptSMSContact : '';
        lead.phone = String.isNotEmpty(leadInput.Client.phone) ?
            leadInput.Client.phone : '';
        lead.ProposalScheduleDate__c = leadInput.Dealer.appointmentDate;
        lead.ProposalScheduleHour__c = String.isNotEmpty(leadInput.Client.additionalInfo) ?
            leadInput.Client.additionalInfo : '';
        lead.Purchase_volume__c = String.isNotEmpty(leadInput.clientPro.purchaseVolume) ? 
            Decimal.valueOf(leadInput.clientPro.purchaseVolume) : 0;
        lead.RecordTypeId = String.isNotEmpty(leadInput.leadInfo.recordType) ?
            leadInput.LeadInfo.recordType : '';
        lead.Related_campaign__c = String.isNotEmpty(leadInput.leadInfo.campaign) ?
            leadInput.LeadInfo.campaign : '';
        lead.ScheduledPeriod__c = String.isNotEmpty(leadInput.AfterSales.quoteInterventionDuration) ?
            leadInput.AfterSales.quoteInterventionDuration : '';
        lead.SecondVehicleOfInterest__c = String.isNotEmpty(leadInput.leadInfo.secondVehicleOfInterest) ?
            leadInput.leadInfo.secondVehicleOfInterest : '';
        lead.ServiceType__c = String.isNotEmpty(leadInput.AfterSales.quoteWishedOperations) ?
            leadInput.AfterSales.quoteWishedOperations : '';
        lead.State = String.isNotEmpty(leadInput.Client.state) ?
            leadInput.Client.state : '';
        lead.Sub_Detail__c = String.isNotEmpty(leadInput.leadInfo.subTypeOfInterest) ?
            leadInput.LeadInfo.subTypeOfInterest : '';
        lead.SubSource__c = String.isNotEmpty(leadInput.leadInfo.orgin) ?
            leadInput.LeadInfo.orgin : '';
        lead.Type_DVE__c = String.isNotEmpty(leadInput.ClientPro.type) ?
            leadInput.ClientPro.type : '';
        lead.VehicleOfInterest__c = String.isNotEmpty(leadInput.leadInfo.VehicleOfInterest) ?
            leadInput.LeadInfo.VehicleOfInterest : '';
        lead.VehicleRegistrNbr__c = String.isNotEmpty(leadInput.ownVehicle.registrationNbr) ?
            leadInput.ownVehicle.registrationNbr : '';
        lead.Voucher_Protocol__c = String.isNotEmpty(leadInput.leadInfo.context) ?
            leadInput.leadInfo.context : '';
                                                         
        System.debug('***Lead to create: '+lead);
        
        return lead;
        
    }
    
    public static WebServiceResultList createErrorList(String codeError, String erro, String status){      
        WebServiceResultList result = new WebServiceResultList();
        System.debug('ERROR: ' + erro);
        
        result.code = codeError;
        result.libelle = erro;
        result.status = status;
        
        System.debug('Result: ' + result);
        return result;   
    }
    
    public static WebServiceResultList createCampMember(List<Lead> leadList){
        
        List<CampaignMember> campMemberList = new List<CampaignMember>();
        WSC10_LMTCreateLead.WebServiceResultList resultClass = new WSC10_LMTCreateLead.WebServiceResultList();
        
        for(Lead l: leadList){
            if( l.Related_campaign__c != NULL ){
                CampaignMember cm = new CampaignMember();
                cm.LeadId = l.Id;
                cm.CampaignId = l.Related_campaign__c;
                
                campMemberList.add(cm);
            }
        }
        
        WSC10_LMTCreateLead.WebServiceResultList result = new WSC10_LMTCreateLead.WebServiceResultList();
        
        String erro = '';
        List<Database.SaveResult> dbResult = Database.Insert(campMemberList,false);
        
        Integer qtdErros = 0;
        for(Integer i=0; i < dbResult.size(); i++){
            if (!dbResult.get(i).isSuccess()){
                //failed record from the list
                qtdErros += qtdErros + 1;
                erro += 'Lead Id: ' + campMemberList.get(i).LeadId + ' ' + dbResult.get(i).getErrors().get(0) + '\n';
                
                System.debug('ERROR: ' + erro);
                
                result.code = '2';
                result.libelle = erro;
                result.status = String.valueOf(qtdErros) + ' Campaign Member(s) records could not be processed';
                
                return result;
            }
        }
        
        System.debug('ERROR: ' + erro);
        
        result.code = '0';
        result.libelle = erro;
        result.status = 'CampaignMember(s) creation OK';
        
        return result;
    }
    
}