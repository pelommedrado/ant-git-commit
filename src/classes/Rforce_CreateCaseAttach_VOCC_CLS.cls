/**
* @author Vinoth Baskaran(RNTBCI)
* @date 28/07/2015
* @description This Class is an Custom Soap service and it will create case and attachement accordding to the user input.
*/
global class Rforce_CreateCaseAttach_VOCC_CLS {

/**
* @author Vinoth Baskaran
* @date 28/07/2015
* @description It accepts the Casedetails from the external service.
* @param case Details
* @return String value according to request
*/
    public static boolean createCase(Rforce_W2CSchema_CLS.Casedetails vocdCaseDetails,Case c){
    boolean bCasInsert;
    List<Case>parentCaseId;
        if(vocdCaseDetails.origin ==System.label.Rforce_CAS_VOCD_Origin  && vocdCaseDetails.subType==System.label.RForce_VOC_Call_Center && vocdCaseDetails.subSource==System.label.RForce_VOC_Call_Center){
           
            if(vocdCaseDetails.subject == ''){
            c.Subject=System.label.RForce_Hot_Alert_VOC_Call_Center; 
            }           
            c.Priority=system.label.Rforce_CAS_Priority_Urgent; 
            
            //Added by Sumanth for Customer Experience split                                                                                   
            String svocSatisfaction;
            String svocKeepInTouch;
            String svocExperience;
            String sheaderCustomerExperience;
            String sheaderBrandRecomendation;
            String sheaderBuyAgainIntention;
            String sheaderSatisfaction;
            String sheaderLetsKeepInTouch;
            String VOCCSubject;
            
            if(!String.isBlank(vocdCaseDetails.customerExperience)){
                List<String> lcustomerExperience=new List<String>();
                lcustomerExperience= splitCusotmerExperience(vocdCaseDetails.customerExperience);
                if(lcustomerExperience.size()>0)
                   svocSatisfaction=lcustomerExperience[0];
                if(lcustomerExperience.size()>1) 
                   svocKeepInTouch=lcustomerExperience[1];
                if(lcustomerExperience.size()>2) 
                   svocExperience=lcustomerExperience[2];   
                      
            }
            try{
                Country_Info__c oCtr=[Select VOCLetsKeepInTouch__c,VOCSatisfaction__c,VOCCSubject__c,VOCBrandRecommendation__c,VOCBuyAgainIntention__c,VOCCustomerExperience__c from Country_Info__c where Name =: vocdCaseDetails.country];
                sheaderCustomerExperience=getCustomerExperience(oCtr);
                sheaderBrandRecomendation=getBrandRecomendation(oCtr);
                sheaderBuyAgainIntention=getBuyAgainIntention(oCtr);
                sheaderLetsKeepInTouch=getVOCCLetsKeepInTouch(oCtr);
                sheaderSatisfaction=getVOCCSatisfaction(oCtr);
                if(vocdCaseDetails.subject == ''){
                    c.Subject=getVOCCSubject(oCtr); 
                }                
            }catch(Exception e){
                sheaderCustomerExperience=System.label.Rforce_VOC_Customer_Experience;
                sheaderBrandRecomendation=System.label.Rforce_VOC_Brand_Recommendation;
                sheaderBuyAgainIntention=System.label.Rforce_VOC_Buy_again_intention;
                sheaderLetsKeepInTouch=System.label.Rforce_CAS_VOC_KeepInTouch;
                sheaderSatisfaction=System.label.Rforce_CAS_VOC_Satisfaction;
                if(vocdCaseDetails.subject == ''){
                    c.Subject=System.label.RForce_Hot_Alert_VOC_Call_Center; 
                }                
            }        
            
            c.Description=sheaderSatisfaction+'\n' + svocSatisfaction +'\n'+'\n'+sheaderLetsKeepInTouch+'\n' + svocKeepInTouch +'\n'+'\n'+sheaderCustomerExperience+'\n' + svocExperience +'\n'+'\n'+sheaderBrandRecomendation+'\n' + vocdCaseDetails.brandRecommendation +'\n'+'\n'+sheaderBuyAgainIntention+'\n' + vocdCaseDetails.buyAgainIntention;                                
          
            c.First_registration_Web__c=vocdCaseDetails.firstRegistration;
            
            if(String.IsNotBlank(vocdCaseDetails.caseNumber)){
             parentCaseId=[Select id,accountId,contactId,From__c,type,Language_Web__c,PT_Dealer_Web__c,VIN_Web__c,CaseBrand__c from case where  CaseNumber=:vocdCaseDetails.caseNumber];
            }
            if(parentCaseId != null){
                for(Case cs:parentCaseId){
                    c.Parentid=cs.id;
                    if(cs.From__c !=''){
                    c.From__c=cs.From__c;
                    }
                    if(cs.Language_Web__c != ''){
                    c.Language_Web__c=cs.Language_Web__c;
                    }
                    if(cs.PT_Dealer_Web__c != ''){
                    c.PT_Dealer_Web__c=cs.PT_Dealer_Web__c;
                    }
                    if(cs.VIN_Web__c != ''){
                    c.VIN_Web__c =cs.VIN_Web__c;
                    }
                    if(cs.type != ''){
                    c.type =cs.type;
                    }
                    if(cs.CaseBrand__c != ''){
                    c.CaseBrand__c=cs.CaseBrand__c;
                    }                                       
                    c.accountId=cs.accountId;                                  
                    c.contactId=cs.contactId;                                     
                } 
            }
            
            bCasInsert=Rforce_CreateCaseMapping_CLS.insertCase(c);                                                                                                       
        }        
    return bCasInsert;
    }
    //Splits the Customer Experince from VOC  
    public Static List<String> splitCusotmerExperience(String sCustExp){
        if(sCustExp==Null)
            return null;       
        List<String> lsCustExp=new List<String>();
            lsCustExp=sCustExp.split('-');
            return lsCustExp;
                   
    }
    //Returns Customer Experience translation from custom settings
    public Static String getCustomerExperience(Country_Info__c octrInfo){
        if(!String.IsBlank(octrInfo.VOCCustomerExperience__c))
            return octrInfo.VOCCustomerExperience__c;
        else
            return System.label.Rforce_VOC_Customer_Experience;   
    }
    //Returns BrandRecomendation translation from custom settings
    public Static String getBrandRecomendation(Country_Info__c octrInfo){
         if(!String.IsBlank(octrInfo.VOCBrandRecommendation__c))
            return octrInfo.VOCBrandRecommendation__c;
        else
            return System.label.Rforce_VOC_Brand_Recommendation;    
    }
    //Returns BuyAgainIntention translation from custom settings
    public Static String getBuyAgainIntention(Country_Info__c octrInfo){
         if(!String.IsBlank(octrInfo.VOCBuyAgainIntention__c))
            return octrInfo.VOCBuyAgainIntention__c;
        else
            return System.label.Rforce_VOC_Buy_again_intention;    
    }
    //Returns Subject translation from custom settings  
    public Static String getVOCCSubject(Country_Info__c octrInfo){
         if(!String.IsBlank(octrInfo.VOCCSubject__c))
            return octrInfo.VOCCSubject__c;
        else
            return System.label.RForce_Hot_Alert_VOC_Call_Center;    
    }
    //Returns LetsKeepInTouch translation from custom settings 
    public Static String getVOCCLetsKeepInTouch(Country_Info__c octrInfo){
         if(!String.IsBlank(octrInfo.VOCLetsKeepInTouch__c))
            return octrInfo.VOCLetsKeepInTouch__c;
        else
            return System.label.Rforce_CAS_VOC_KeepInTouch;    
    } 
    //Returns Satisfaction translation from custom settings 
    public Static String getVOCCSatisfaction(Country_Info__c octrInfo){
         if(!String.IsBlank(octrInfo.VOCSatisfaction__c))
            return octrInfo.VOCSatisfaction__c;
        else
            return System.label.Rforce_CAS_VOC_Satisfaction;    
    }             
}