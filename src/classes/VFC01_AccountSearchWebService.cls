public class VFC01_AccountSearchWebService{

public String firstName {get;set;}
public String lastName {get;set;}
public String zipCode {get;set;}
public String ErrorMessage ;
//default constructor
public VFC01_AccountSearchWebService() {

}

public void Call(String lastName, String firstName, String zipCode){
       Boolean Success ;
       WS04_CustdataCrmBserviceRenault.CrmGetCustData callout = new WS04_CustdataCrmBserviceRenault.CrmGetCustData() ;
       WS04_CustdataCrmBserviceRenault.CustDataRequest info = new WS04_CustdataCrmBserviceRenault.CustDataRequest();
       WS04_CustdataCrmBserviceRenault.GetCustDataRequest request = new WS04_CustdataCrmBserviceRenault.GetCustDataRequest();
       WS04_CustdataCrmBserviceRenault.GetCustDataResponse result = new WS04_CustdataCrmBserviceRenault.GetCustDataResponse();
       
       callout.endpoint_x = System.Label.endpointx;  //callout.endpoint_x = 'https://webservices2.renault.fr/ccb/0/CrmGetCustData_3_3'

       callout.clientCertName_x = System.Label.clientCertNamex;   //callout.clientCertName_x='CCB_dev_002'
       
        try
        {    
            request.custDataRequest = info;
            info.lastName = lastName ;
            info.firstName = firstName ;
            info.zip =zipCode ;
            result = callout.getCustData(request);
            String lastName_test ;
                
            for (integer i=0; i<result.clientList.size(); i++)
                lastName_test = result.clientList[i].lastName ;
            
            Success = true;
        }
        catch(exception e)
        {
            Success = false;
            ErrorMessage = String.valueOf(e);
            system.debug(result);
            system.debug('erreur : ' + e.getMessage());
        } 
}

public void submitWS() {

System.debug('#### firstName : ' + firstName);
System.debug('#### lastName : ' + lastName);
System.debug('#### zipCode : ' + zipCode);

Call( lastName, firstName,  zipCode);

}
}