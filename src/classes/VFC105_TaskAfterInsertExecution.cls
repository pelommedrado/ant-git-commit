/**
* Classe que será disparada pela trigger do objeto Task após a inserção dos registros.
* @author Felipe Jesus Silva.
*/
public class VFC105_TaskAfterInsertExecution implements VFC37_TriggerExecution
{
	public void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData)
	{
		List<Task> lstSObjTask = (List<Task>) lstNewtData;
		
		this.updateOpportunity(lstSObjTask);
	}
	
	private void updateOpportunity(List<Task> lstSObjTask)
	{
		String whatId = null;
		List<Opportunity> lstSObjOpportunity = null;
		Set<String> setOpportunityId = new Set<String>();
		List<Opportunity> lstSObjOpportunityUpdate = new List<Opportunity>();
		
		for(Task sObjTask : lstSObjTask)
		{
			whatId = sObjTask.WhatId;
			
			/*verifica se essa tarefa é de oportunidade*/
			if(String.isNotEmpty(whatId) && (whatId.startsWith('006')))
			{
				setOpportunityId.add(whatId);
			}	
				
		}
		
		if(!setOpportunityId.isEmpty())
		{
			try
			{
				/*obtém as oportunidades através do set de ids*/
				lstSObjOpportunity = VFC47_OpportunityBO.getInstance().findById(setOpportunityId);
				
				for(Opportunity sObjOpportunity : lstSObjOpportunity)
				{
					if((sObjOpportunity.OpportunityTransition__c != VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER) &&
					   (sObjOpportunity.OpportunityTransition__c != VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER))
					{
						sObjOpportunity.OpportunityTransition__c = VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER;
						
						/*adiciona a oportunidade na lista auxiliar para ser atualizada*/
						lstSObjOpportunityUpdate.add(sObjOpportunity);
					}
				}
				
				if(!lstSObjOpportunityUpdate.isEmpty())
				{
					VFC47_OpportunityBO.getInstance().updateOpportunities(lstSObjOpportunityUpdate);
				}				
			}
			catch(VFC89_NoDataFoundException ex)
			{
				/*se cair nessa exception significa que nenhuma oportunidade foi encontrada*/
			}
		}
	}

}