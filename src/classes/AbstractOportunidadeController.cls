public abstract class AbstractOportunidadeController {
    
    public String cpfCliente 		{ get;set; }
    public String oId 		 		{ get;set; }
   
    protected void initAction(String oppId) {
        if(!String.isEmpty(oppId)) {
            
            List<Opportunity> accClientList = [
                SELECT Id, Voucher_Alert__c, Account.CustomerIdentificationNbr__c
                FROM Opportunity
                WHERE Id =: oppId
            ];
            
            if(!accClientList.isEmpty()) {
                Opportunity oppVoucher = accClientList.get(0);
                
                if(!String.isEmpty(oppVoucher.Account.CustomerIdentificationNbr__c)) {
                    cpfCliente = oppVoucher.Account.CustomerIdentificationNbr__c;
                    oId = oppId;
                }
                
            }
        }
    }
    
    @RemoteAction
    public static CampaignMember[] verificarCampanha(String cpf, String oppId) {
        System.debug('### verificando cpf: ' + cpf);
        
        if(String.isEmpty(cpf)) {
            return new CampaignMember[0];
        }
        
        List<Opportunity> opLi = [
            SELECT Id, Voucher_Alert__c
            FROM Opportunity
            WHERE Id =: oppId AND Voucher_Alert__c = true
        ];
        
        if(!opLi.isEmpty()) {
            return new CampaignMember[0];
        }
        
        List<CampaignMember> mCampList = [
            SELECT Id, Campaign.Name, Lead.Voucher_Protocol__c, Campaign.Billing_Required__c
            FROM CampaignMember
            WHERE Lead.CPF_CNPJ__c =: cpf AND Lead.RecordTypeId =: Utils.getRecordTypeId('Lead', 'Voucher')
            AND Campaign.Status = 'In Progress' AND Campaign.IsActive = true AND Lead.Status = 'To rescue'
        ];
        
        System.debug('mCampList: ' + mCampList);
        
        return mCampList;
    }
    
    @RemoteAction
    public static void resgatarVoucher(String cpf, String leadId, String oppId) {
        System.debug('### resgatarVoucher cpf: ' + cpf + ' oppId:' + oppId + ' leadId:' + leadId);
        
        List<Lead> leadList = [
            SELECT Id, Status, Related_Opportunity__c
            FROM Lead
            WHERE ID =: leadId AND RecordTypeId =: Utils.getRecordTypeId('Lead', 'Voucher')
        ];
        
        System.debug('Lead : ' + leadList);
        
        if(!leadList.isEmpty()) {
            
            Lead lead = leadList.get(0);
            lead.Status = 'Rescued';
            lead.Related_Opportunity__c = oppId;
            
            UPDATE leadList;
        } 
    }
    
    @RemoteAction
    public static void cancelarAlertVoucher(String oppId) {
        System.debug('### cancelarAlertVoucher oppId:' + oppId);
        
        List<Opportunity> oppList = [
            SELECT Id, Voucher_Alert__c
            FROM Opportunity
            WHERE Id =: oppId
        ];
        
        System.debug('Opportunity : ' + oppList);
        
        if(!oppList.isEmpty()) {
            for(Opportunity le : oppList) {
                le.Voucher_Alert__c = true;
            }
            
            UPDATE oppList;
        } 
    }
}