@isTest
public class AccountSearch_Controller_Test{
    
         static testMethod void testmet()
    {
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];    
    Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'French');
    insert ctr;
 
    User usr = new User (LastName = 'Rotondo', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com',RecordDefaultCountry__c='France');
    Test.startTest(); 
    List<SelectOption> options = new List<SelectOption>();
    System.runas(usr){
   
    Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
    Account Acc = new Account(Name='Test1',Phone='0000',RecordTypeId=RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com');
    insert Acc;
    
    Contact Con = new Contact(LastName='Test Contact',FirstName='Test', Salutation='Mr.',ProEmail__c='lro@lro.com',AccountId=Acc.Id);
    insert Con;
    
    Case c = new Case(Origin='Email',Type='Information Request',SubType__c = 'Booking',Status='Open',Priority='Medium',Description='Description',From__c='Customer',AccountId=Acc.Id,ContactId=Con.Id);
    insert c;

    ApexPages.currentPage().getParameters().put('show','false');
    ApexPages.currentPage().getParameters().put('Id',Acc.Id);
    ApexPages.currentPage().getParameters().put('getAcName','Rotondo1');
    ApexPages.currentPage().getParameters().put('AccountId',Acc.Id);
    
    ApexPages.currentPage().getParameters().put('getAccEmail','lrotondo@rotondo.com'); 
    ApexPages.currentPage().getParameters().put('show','true');
     ApexPages.currentPage().getParameters().put('getDataSource','MDM');  
     
     PageReference pageRef = Page.Rforce_AccountSearch_Page;
     Test.setCurrentPage(pageRef);
     
    
    
    ApexPages.StandardController controller1=new ApexPages.StandardController(Acc);    
   // Rforce_AccountSearch_Controller pageCtrl = new Rforce_AccountSearch_Controller(controller1);
    Rforce_AccountSearch_Controller pageCtrl1 = new Rforce_AccountSearch_Controller();

    pageCtrl1.AccountId=acc.Id;
    
     //Rforce_AccountSearch_Controller accController = new Rforce_AccountSearch_Controller(new ApexPages.StandardController(acc));    
      Rforce_AccountSearch_Controller pageCtrl = new Rforce_AccountSearch_Controller(new ApexPages.StandardController(acc));    
      
 //   pageCtrl.getCountryInfo();
    pageCtrl.getDataSource='MDM';
    pageCtrl.partyID='20028';
    pageCtrl.getfName='test';
    pageCtrl.getAcName='test';
    pageCtrl.getAccEmail='test';
    pageCtrl.getPhNo='test';
    pageCtrl.getStreet='test';
    pageCtrl.getCity='test';
    pageCtrl.getZip='test';
    pageCtrl.showDetail=1;
    
    pageCtrl.firstName='test';
    pageCtrl.country='test';
    pageCtrl.brand='test';
    pageCtrl.demander='test';
    pageCtrl.lastName='test';
    pageCtrl.city='test';
    pageCtrl.zip='test';
    pageCtrl.ident1='test';
    pageCtrl.idClient='test';
    pageCtrl.idMyr='test';
    pageCtrl.firstRegistrationDate='01/01/2000';
    pageCtrl.registration='test';
    pageCtrl.email='test';
    pageCtrl.ownedVehicles='test';
    pageCtrl.nbReplies='test';
    pageCtrl.bcsId='test';
    
    //pageCtrl.getAccount();
    pageCtrl.getItems();
    pageCtrl.getCountryItems();
    pageCtrl.getAccountList();
    //pageCtrl.getSelected();   
   // pageCtrl.createNewAccount();   
    pageCtrl.selectedId = 0;
    pageCtrl.createAccount();   
    //pageCtrl.getAccount();
    pageCtrl.MergeAccount();

    
    
        
        
    }
         Test.stopTest(); 
    }
   

}