public class Rforce_Rbx_WS {

public String getCustData(String vin,String Registration,String country,String LastName,String firstName,String brand,String IdClt, String Ident1, String City, String ZipCode){
      String strBody;
      HttpRequest req = new HttpRequest();
      req.setMethod('POST');
      req.setEndpoint(System.label.RbxWsUrl);
      req.setClientCertificateName(System.label.clientcertnamex);
      req.setTimeout(120000); 
      req.setHeader('Content-Type', 'application/soap+xml; charset=utf-8');
      req.setHeader('SOAPAction', '"getCustData"');
      req.setHeader('Keep-Alive', '1115');
      req.setHeader('Accept', 'text/xml');
      req.setHeader('User-Agent', 'SFDC-Callout/29.0');
      
    String requestStr = ''
    +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://custdata.crm.bservice.renault">'
    +'<soapenv:Header/>'
    +'<soapenv:Body>'
    +'<cus:getCustData>'
    +'<request>'
    +'<servicePrefs>'
    +'<irn>?</irn>'
    +'<sia>?</sia>'
    +'<reqid>?</reqid>'
    +'<userid>?</userid>'
    +'<language>?</language>'
    +'<country>?</country>'
    +'</servicePrefs>'
    +'<custDataRequest>'
    +'<mode>7</mode>'
    +'<country>'+country+'</country>'
    +'<brand>'+brand+'</brand>'
    +'<demander>PRODCCB</demander>'
    +'<vin>'+vin+'</vin>'
    +'<lastName>'+LastName+'</lastName>'
    +'<firstName>'+firstName+'</firstName>'
    +'<city>'+City+'</city>'
    +'<zip>'+ZipCode+'</zip>'
    +'<ident1>'+ident1+'</ident1>'
    +'<idClient>'+IdClt+'</idClient>'
    +'<firstRegistrationDate></firstRegistrationDate>'
    +'<registration>'+Registration+'</registration>'
    +'<ownedVehicles>1</ownedVehicles>'
    +'<nbReplies>10</nbReplies>'
    +'</custDataRequest>'
    +'</request>'
    +'</cus:getCustData>'
    +'</soapenv:Body>'
    +'</soapenv:Envelope>';
        req.setHeader('Content-Length', String.valueOf(requestStr.length()));
        req.setBody(requestStr);
        Http http = new Http();
        try {
            //Execute web service call here
            System.debug('REQUEST='+req.toString());
            System.debug('REQUEST='+req.getBody());      
            HTTPResponse res = http.send(req);  
            //Helpful debug messages
            System.debug('STATUS='+res.getStatus());
            System.debug('STATUS_CODE='+res.getStatusCode());
           
            strBody = res.getBody();
            System.debug('Response String>>>>>>>>>'+strBody );
            System.debug('STATUS CODE'+res.getStatusCode());

       } catch(CalloutException e) {
            system.debug ('RBX WS, CALLOUT EXCEPTION'+e.getMessage());

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Problem with the Webservice'));
           
       } catch (Exception e) {
            system.debug ('RBX WS, UNKNOWN EXCEPTION'+e.getMessage());

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Problem with the Webservice'));
            
       }
            String finalStr='';
            Integer pos1,pos2;
           If(!String.isBlank(strBody))
           {
  
            pos1=strBody.indexof('<response>');
            pos2=strBody.indexof('</response>');
           }
    else{
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Response is Empty'));
          
    }
           
   
        if(pos1>0&&pos2>0)
       {
          String formattedStr=strBody.substring(pos1,pos2);
           finalStr=formattedStr+'</response>';
       }  
    else
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Response is Empty'));
     
    }
       return finalStr;
}
}