@isTest
public class CAC_CaseUtilsTest {
    
    static testMethod void WaitingReply(){
        CAC_CaseUtilsTest.createCustonSet();
        CAC_CaseUtils caseUtil = new CAC_CaseUtils();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste1@teste2.com',
                'Analyst'
            )
        ){
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,lsOldCase);
            caseUtils.statusAnswersWaiting();
        }
        Database.insert(returnComentarioCaso(lsCaseId));
        
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Analyst'
            )
        ){
            CAC_CaseUtils caseUtils3 = new CAC_CaseUtils(lsCase,lsOldCase);
            caseUtils3.statusAnswersWaiting();
        }
        Test.stopTest();
    }
    
    
    
    
    static testMethod void statusAnswers(){
        CAC_CaseUtilsTest.createCustonSet();
        CAC_CaseUtils caseUtil = new CAC_CaseUtils();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        System.runAs(
            createUser(
                'teste2@teste3.com',
                'Analyst'
            )
        ){
            for(Case casea :lsCase){
                casea.Description = 'abc';
                casea.Status = 'Answered';
                casea.Answer__c ='slajlksjlkaj';
                lsCaseId.add(casea.ID);
            }
            CAC_CaseUtils caseUtils3 = new CAC_CaseUtils(lsCase,lsOldCase);
            caseUtils3.statusAnswers();
            
        }
        
        
    }
    
    
    static testMethod void corretDayReturn(){
         List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        
        Long hours = 200;
        dateTime data;
         CAC_CaseUtils caseUtils = new  CAC_CaseUtils(lsCase,new Map<id,Case>(lsCase));
        data = caseUtils.corretDayReturn(hours,08,17);
    }
    
    
    static testMethod void AvailableRecordType(){
        CAC_CaseUtilsTest.createCustonSet();
        CAC_CaseUtils caseUtil = new CAC_CaseUtils();
        caseUtil.now();
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Map<Id,Case> lsOldCase = new Map<Id,Case>(lsCase);
        List<Id> lsCaseId = new List<Id>();
        for(Case casea :lsCase){
            casea.Description = 'abc';
            casea.Status = 'Waiting for reply';
            lsCaseId.add(casea.ID);
        }
        
        Test.startTest();
        System.runAs(
            createUser(
                'teste3@teste1.com',
                'Analyst'
            )
        ){
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,lsOldCase);
            caseUtils.getAvailableRecordType();
        }
        
    }
    
    
    
    
    static testMethod void updateField(){
        
        User usr = new User(
            Id = UserInfo.getUserId(),
        	roleInCac__c = 'Dealer',
            isCac__c = true
        );
        update usr;
        
        CAC_CaseUtilsTest.createCustonSet();
        
        User user1 = CAC_CaseUtilsTest.createUser('a1dcqadasdsas@test.com','Dealer');
        List<Case> lsCase;
       // System.runAs(user1){
            lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
       // }
        for(Case caso: lsCase){
            caso.Status = 'New';
            caso.OwnerID = user1.Id;
        }  
        Database.update(lsCase,true);
        
       // User user2 = CAC_CaseUtilsTest.createUser('atestesas@test.com','Dealer');
        Map<Id,Case> lsOldCase  = new Map<Id,Case>(lsCase);
        //System.runAs(user2){    
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,lsOldCase);
            caseUtils.updateField();
        //}
    }
    
     
    
    static testMethod void alterOwnerFull(){
        
        User user = CAC_CaseUtilsTest.createUser('a1asdapiopias@test.com','Analyst');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        for(Case caase : lsCase){
            caase.Status = 'Under Analysis';
            lsNewCase.add(caase);
        }
        
        
        
        System.runAs(user){
            CAC_CaseUtils.alterOwner(lsNewCase, oldCaseMap);
        }
    }
    
    static testMethod void blockStatusError1(){
        User user = CAC_CaseUtilsTest.createUser('a1asdapiopias@test.com',Utils.getProfileId('CAC - Full'));
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        System.runAs(user){
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
        
        
    }
    
    static testMethod void blockStatusError2(){
        User user = CAC_CaseUtilsTest.createUser('a1asdapiopias@test.com','Analyst');
        
        List<Case> lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Under Analysis';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'New';
        
        System.runAs(user){
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
        
        
    }
    
    static testMethod void blockStatusError3(){
        User user = CAC_CaseUtilsTest.createUser('a1asdapiopias@test.com','Analyst');
        List<Case> lsCase;
        
        
        lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        
        List<Case> lsNewCase = new List<Case>();
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        Map<id,Case>oldCaseMap = new Map<Id,Case>(lsCase);
        
        
        for(Case caase : lsCase)
            caase.Status = 'Closed Automatically Answered';
        
        System.runAs(user){
            CAC_CaseUtils caseUtils = new CAC_CaseUtils(lsCase,oldCaseMap);
            caseUtils.blockStatus();
        }
    }
    
    
    
    static testMethod void alterOwnerDealer(){
        List<Case> lsCase;
        User user = CAC_CaseUtilsTest.createUser('a1asdapiopias@test.com','Dealer');
        System.runAs(user){
            lsCase = returnNewCase(Utils.getRecordTypeId('Case','Cacweb_PartsWarehouse'));
        }
        
    }
    
    
    private static List<CaseComment> returnComentarioCaso(List<Id> lsCaseId){
        List<CaseComment>lsCaseComment = new List<CaseComment>();
        for(Id caseId : lsCaseId){
            CaseComment comenta = new CaseComment(
                CommentBody = 'asdasdadadsadadsadasdasdasd',
                IsPublished = true,
                ParentId = caseId
            );
            lsCaseComment.add(comenta);
        }
        
        return lsCaseComment;
    }
    
    private static List<Case> returnNewCase(Id recordType){
        List<Case>lsCase = new List<Case>();
        for(integer i=0; i<30; i++){
            Case cas = new Case(
                Status = 'new',
                Subject__c = 'Diferido (Previsão de Peças)',
                Description = 'teste',
                Dealer_Contact_Name__c = 'teste',
                Contact_Phone__c  = '1122334455',
                Reference__c = 'teste',
                Order_Number__c = '1234567',
                RecordTypeId = recordType
            );
            lsCase.add(cas);
        }
        Database.insert(lsCase,true);
        return lsCase;
        
    }
    
    
    
    private static User createUser(String userName,String typeUserCac){
        Id profileId = Utils.getSystemAdminProfileId();
        String aliasName =  userName;
        if(aliasName.length()>=8)
            aliasName = aliasName.substring(0,7);
        User manager = new User(
            FirstName = aliasName,
            LastName = 'User',
            BypassVR__c = false,
            Email = 'test@org.com',
            Username = userName,
            Alias = userName.substring(0,7),
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            CommunityNickname = aliasName,
            ProfileId = profileId,
            BIR__c ='123ABC123',
            IsActive = true,
            isCac__c = true,
            roleInCac__c = typeUserCac
        );
        Database.insert( manager,true );
        return manager;
    }
    
    
    
    
    private static void createCustonSet(){
        cacweb_Mapping__c custonSet = new cacweb_Mapping__c(
            name = 'Cacweb_PartsWarehouse',
            Departamento__c='Parts',
            Department__c = 'Peças',
            DirectionInEnglish__c = 'Parts',
            Name_Queue__c= 'Cacweb_PartsWarehouse',
            Record_Type_Name__c = 'Cacweb_PartsWarehouse'
        );
        Database.insert(custonSet);
        
        cacweb_settingTime__c custon2 = new cacweb_settingTime__c(
            name = 'Diferido',
            alertExpire__c = 20,
            subject__c = 'Diferido (Previsão de Peças)',
            FinishHourAttendance__c = 17,
            StartHourAttendance__c = 08,
            hoursExpire__c = 500
        );
        Database.insert(custon2);
    }
    
    
    
    
    
    
    
    
    
    
}