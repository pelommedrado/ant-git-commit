/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class SetMessageStatusAction {
    global SetMessageStatusAction() {

    }
    @InvocableMethod(label='Set Message Status' description='Sets the status of the given messages and returns the status of each item.')
    global static List<LiveText.ActionResult> setMessagesStatus(List<LiveText.SetMessageStatusRequest> items) {
        return null;
    }
}
