@isTest
public class TriggerHandler_Case_Test{
    
    
    
   public static testMethod void VehupdateTriggerTest(){        
        try{
            VEH_Veh__c veh=new VEH_Veh__c();
            veh.Name='12345678998765432';
            veh.KmCheck__c=123;
            insert veh;
            
            VEH_Veh__c veh1=new VEH_Veh__c();
            veh1.Name='12345678998755432';
            insert veh1;                        
            
            Id vehid=veh.id;
            Case cs=new Case();
            cs.Origin='RENAULT SITE';
            cs.Status='New';        
            cs.Description='Trigger test class';
            cs.Kilometer__c=1200;                            
            cs.VIN__c=vehid;
            insert cs;
            
            Id veh1id=veh1.id;
            Case cs1=new Case();
            cs1.Origin='RENAULT SITE';
            cs1.Status='New';        
            cs1.Description='Trigger test class';                          
            cs1.VIN__c=vehid;
            insert cs1;
        }
        catch(Exception e){
            system.debug(e);
        }
    }  

}