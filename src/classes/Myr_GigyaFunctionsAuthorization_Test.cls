/**
 * Apex class dedicated to test the Gigya interface
 * This class will test the following classes: Myr_GigyaCaller_Cls, Myr_GigyaFunctions_Cls and Myr_GigyaParser_Cls
 * This class is especially dedicated to test the authorization level for Gigya functions
 * @author S. Ducamp
 * @date 02.03.2016
 * 
 * 
 */
@isTest 
private class Myr_GigyaFunctionsAuthorization_Test {

	@testsetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		Myr_Datasets_Test.prepareGigyaCustomSettings([select IsSandbox from Organization].IsSandbox);

		//Insert the required technical users for this test class
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser>();
		listReqTechUser.add( new Myr_Datasets_Test.RequestedTechUser('Italy', null, false) );
		Myr_Datasets_Test.insertTechnicalUsers( listReqTechUser );
	}

	
	//Return the Gigya_Datacenter_NoProd__c or Gigya_Datacenter__c
	private static String getGigyaDatacenter(Country_Info__c country) {
		if ([select IsSandbox from Organization].IsSandbox){
			return country.Gigya_Datacenter_NoProd__c;
		} else {
			return country.Gigya_Datacenter__c;
		}		
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	// --------- AUTHORIZATION
	
	//RESET PASSWORD OK: level 1 OK
	static testMethod void test_ResPwd_Authorized() {
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = true;
		authGigya.Gigya_Level2__c = false;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Check the authorization
    	Account acc = new Account();
    	system.runAs(Myr_Datasets_Test.getTechnicalUser('Italy')) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', '', 'jean.albert@atos.net', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c FROM Account WHERE Id = :acc.Id];
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.resetPassword(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_Failed_ResPwd_SendKO> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ResetPassword, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_000, resp.code);
    	system.assertEquals('GIGYA_000_Msg', resp.label);
    	system.assertEquals(resp.msg, system.Label.GIGYA_000_Msg);
    	system.assertEquals(true, resp.isOK());
	}

	//RESET PASSWORD KO: level 1 required
	static testMethod void test_ResPwd_NOTAuthorized() {
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = false;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Check the authorization
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c FROM Account WHERE Id = :acc.Id];
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('jean.albert@atos.net','','','','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.resetPassword(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_ResPwd_Authorized> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ResetPassword, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_590, resp.code);
    	system.assertEquals('GIGYA_590_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_590_Msg, resp.msg);
	}
	
	//RESEND ACTIVATION OK: level 1 OK
	static testMethod void test_ResAct_Authorized() {
		//Set the authorization level
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = true; //authorize the level 2 will by default auhtorize the level 1
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			acc.MyDaciaId__c = '';
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('','','','jean.albert@atos.net','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.resendActivationEmail(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ResAct_SendOK> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ResendActivationMail, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_000, resp.code);
    	system.assertEquals('GIGYA_000_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_000_Msg, resp.msg);
    	system.assertEquals(true, resp.isOK());
	}
	
	//RESEND ACTIVATION KO: level 1 required
	static testMethod void test_ResAct_NOTAuthorized() {
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = false;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			acc.MyDaciaId__c = '';
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK('','','','jean.albert@atos.net','');
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.resendActivationEmail(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ResAct_SendOK> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ResendActivationMail, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_590, resp.code);
    	system.assertEquals('GIGYA_590_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_590_Msg, resp.msg);
		system.assertEquals(false, resp.isOK());
	}

	//CHANGE EMAIL OK: level 2 Ok
	static testMethod void test_ChangeEmail_Authorized() {
		//Set the authorization level
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = true;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			acc.Myr_Status__c = system.Label.Myr_Status_Activated;
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK();
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
		String email = 'newEmail@test.atos.com';
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.changeEmail(acc.Id, 'Renault', email);
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ChangeEmail_SendOK_Renault> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ChangeEmail, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_000, resp.code);
    	system.assertEquals('GIGYA_000_Msg', resp.label);
    	system.assertEquals(true, resp.msg.contains(system.Label.GIGYA_000_Msg));
    	system.assertEquals(true, resp.isOK());
	}
	
	//CHANGE EMAIL KO: level 2 required
	static testMethod void test_ChangeEmail_NOTAuthorized() {
		//Set the authorization level
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = false;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_OK();
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
		String email = 'newEmail@test.atos.com';
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_Response resp = gigya.changeEmail(acc.Id, 'Renault', email);
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ChangeEmail_SendOK_Renault> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_ChangeEmail, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_590, resp.code);
    	system.assertEquals('GIGYA_590_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_590_Msg, resp.msg);
		system.assertEquals(false, resp.isOK());
	}
	
	//DELETE OK: level 3 OK
	static testMethod void test_Delete_Authorized() {
		//Set the authorization level
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = false;
		authGigya.Gigya_Level2__c = false;
		authGigya.Gigya_Level3__c = true;	
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			acc.MyDaciaId__c = '';
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_MultipResponse resp = gigya.deleteAccounts(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ResAct_SendOK> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_DeleteAccounts, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_000, resp.code);
    	system.assertEquals('GIGYA_000_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_000_Msg, resp.msg);
    	system.assertEquals(true, resp.isOK());
	}
	
	//DELETE KO: level 3 required
	static testMethod void test_Delete_NOTAuthorized() {
		//Set the authorization level
		CS04_MYRAuthorization_Settings__c authGigya = new CS04_MYRAuthorization_Settings__c();
		authGigya.Gigya_Level1__c = true;
		authGigya.Gigya_Level2__c = true;
		authGigya.Gigya_Level3__c = false;
		authGigya.SetupOwnerId = UserInfo.getUserId(); //current user
		insert authGigya;
		//Test
		User techUser = Myr_Datasets_Test.getTechnicalUser('Italy');
    	Account acc = new Account();
    	system.runAs(techUser) {
    		acc = Myr_Datasets_Test.insertPersonalAccountAndUser('Jean', 'Albert', '89900', 'jean.albert@atos.net', '', '', 'NO_USER', '', 'Italy', '', '', '', '');
    		acc = [SELECT Id, Country__c, PersEmailAddress__c, MyRenaultID__c, MyDaciaId__c FROM Account WHERE Id = :acc.Id];
			acc.MyRenaultID__c = 'jean.albert@atos.net';
			acc.MyDaciaId__c = '';
			update acc;
    		system.AssertEquals('Italy',acc.Country__c);
    	}
    	//Set the mock
    	Country_Info__c setItaly = Country_Info__c.getInstance('Italy');
    	Myr_Gigya_MK mock = new Myr_Gigya_MK(getGigyaDatacenter(setItaly));
    	mock.setToken_OK();
    	mock.setAccSch_MultipleAcc();
    	mock.setFunction_OK();
    	Test.setMock(HttpCalloutMock.class, mock );
    	//test 1: Http 400 when trying to launch the activation email
    	Myr_GigyaFunctions_Cls gigya = new Myr_GigyaFunctions_Cls();
    	Test.startTest();
    	Myr_GigyaFunctions_Cls.Myr_GigyaFunctions_MultipResponse resp = gigya.deleteAccounts(acc.Id, 'Renault');
    	Test.stopTest();
    	//check the result
    	system.debug('### Myr_GigyaFunctions_Test - <test_OK_ResAct_SendOK> - resp=' + String.ValueOf(resp));
    	system.assertEquals(Myr_GigyaFunctions_Cls.GigyaFunction.Myr_Gigya_DeleteAccounts, resp.gigFunction);
    	system.assertEquals(system.Label.GIGYA_590, resp.code);
    	system.assertEquals('GIGYA_590_Msg', resp.label);
    	system.assertEquals(system.Label.GIGYA_590_Msg, resp.msg);
		system.assertEquals(false, resp.isOK());
	}

}