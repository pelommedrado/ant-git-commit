public with sharing class VFC135_TaskAfterInsertExecution {
	
	private static final VFC135_TaskAfterInsertExecution instance = new VFC135_TaskAfterInsertExecution();
	
	private VFC135_TaskAfterInsertExecution(){}

    public static VFC135_TaskAfterInsertExecution getInstance(){
        return instance;
    }
    
    public void execute(List<Task> listTask){
    	
    	String whatId = null;
		List<Opportunity> lstSObjOpportunity = null;
		Set<String> setOpportunityId = new Set<String>();
		List<Opportunity> lstSObjOpportunityUpdate = new List<Opportunity>();
    	
    	for(Task sObjTask : listTask)
		{
			whatId = sObjTask.WhatId;
			
			/*verifica se essa tarefa é de oportunidade*/
			if(String.isNotEmpty(whatId) && (whatId.startsWith('006')) && sObjTask.Status == 'Completed')
			{
				setOpportunityId.add(whatId);
			}	
				
		}
		
		if(!setOpportunityId.isEmpty())
		{
			try
			{
				/*obtém as oportunidades através do set de ids*/
				lstSObjOpportunity = VFC47_OpportunityBO.getInstance().findById(setOpportunityId);
				
				for(Opportunity sObjOpportunity : lstSObjOpportunity)
				{
					if((sObjOpportunity.OpportunityTransition__c == VFC43_OpportunityTransitionType.IN_ATTENDANCE_SELLER))
					{
						sObjOpportunity.StartOfAttendance__c = Datetime.now();
						
						/*adiciona a oportunidade na lista auxiliar para ser atualizada*/
						lstSObjOpportunityUpdate.add(sObjOpportunity);
					}
				}
				
				if(!lstSObjOpportunityUpdate.isEmpty())
				{
					VFC47_OpportunityBO.getInstance().updateOpportunities(lstSObjOpportunityUpdate);
				}				
			}
			catch(VFC89_NoDataFoundException ex)
			{
				/*se cair nessa exception significa que nenhuma oportunidade foi encontrada*/
			}
		}
    }
	
}