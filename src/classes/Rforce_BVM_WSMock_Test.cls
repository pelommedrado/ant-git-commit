//Use to simulate a response from the WebService
@isTest
global class Rforce_BVM_WSMock_Test implements WebServiceMock  {

    
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
             
             
             
         Rforce_BVM_WS.DetVehInfoMsg detVehInfoMsg1=new  Rforce_BVM_WS.DetVehInfoMsg();
       detVehInfoMsg1.codeRetour='AA';
       detVehInfoMsg1.msgRetour='success';
       
       Rforce_BVM_WS.DetVeh detvehmock=new Rforce_BVM_WS.DetVeh();
       
     detvehmock.version='Version';
     detvehmock.tvv='tvv';
     detvehmock.typeMot='typeMot';
       detvehmock.indMot='indMot';
        detvehmock.NMot='NMot';
         detvehmock.typeBoi='typeBoi';
       detvehmock.indBoi='indBoi';
          detvehmock.NBoi='NBoi';
         detvehmock.dateTcmFab='11.02.2008';
         detvehmock.dateLiv='11.02.2008';
         detvehmock.libModel='ligne2P12';
         detvehmock.libCarrosserie='BERLINE 4PRTES';
       LIST<Rforce_BVM_WS.DetVehCritere> criteriaList=new LIST<Rforce_BVM_WS.DetVehCritere>();
       Rforce_BVM_WS.DetVehCritere detVehCriteriaMock=new Rforce_BVM_WS.DetVehCritere();
       detVehCriteriaMock.cdCritOf='criteria';
       detVehCriteriaMock.cdObjOf='019';
       criteriaList.add(detVehCriteriaMock);
         detvehmock.criteres=criteriaList;
         Rforce_BVM_WS.ApvGetDetVehXmlResponse responseMock=new  Rforce_BVM_WS.ApvGetDetVehXmlResponse();
         responseMock.detVehInfoMsg=detVehInfoMsg1;
         responseMock.detVeh=detvehmock;
        
        Rforce_BVM_WS.getApvGetDetVehXmlResponse_element element_mock= new Rforce_BVM_WS.getApvGetDetVehXmlResponse_element();
        
        
        element_mock.getApvGetDetVehXmlReturn=responseMock;
        

         Rforce_BVM_WS.getApvGetDetVehXmlResponse_element response_x=new Rforce_BVM_WS.getApvGetDetVehXmlResponse_element();

        response.put('response_x', element_mock); 

   }
}