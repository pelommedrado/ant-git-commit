public class ALR_SharingTracking {

    public List<ALR_Tracking__c> lsInTracking;//lista com os trackings
    public List<String> lsIdBir;
    public List<Account> lsAccount;//lista com as contas relacionadas já vem pré carregada   
    public List<User> lstUsuarios;
    public Map<String,String> mapIdCssAndTraking = new Map<String,String>();  
        
    public ALR_SharingTracking(){
        lsAccount = new List<Account>();
        lsInTracking = new List<ALR_Tracking__c>();
        lsIdBir = new List<String>();
        lstUsuarios = new List<User>();
        
      }
    //responsavel por inserir trackings
    public void insertTracking(List<VEH_Veh__c> listVehicle){
        Map<String,Account> mapIdDealer = new Map<String,Account>();//mapa contem a partir do id da bir o id da conta  
        for(Account conta: [select id,IDBIR__c from Account where Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH']){
            mapIdDealer.put(conta.IDBIR__c, conta);
        }
        
        
        for(VEH_Veh__c sObjVehicle : listVehicle){
           if(sObjVehicle.VehicleSource__c !=null && sObjVehicle.Id_Bir_BuyDealer__c!= null){
             if(sObjVehicle.VehicleSource__c.equalsIgnoreCase('SAP')) {
                 ALR_Tracking__c tracking = new ALR_Tracking__c();
                    tracking.Name = sObjVehicle.Name;
                  /* if(sObjVehicle.IDBIRSellingDealer__c == null){
                     sObjVehicle.IDBIRSellingDealer__c = sObjVehicle.Id_Bir_BuyDealer__c;
                   }*/
                   
                    
                   //concessionaria de venda
                if(sObjVehicle.IDBIRSellingDealer__c != null){
                    String IDBIRSellingDealer = sObjVehicle.IDBIRSellingDealer__c;
                    if(sObjVehicle.IDBIRSellingDealer__c.length() >8){ 
                                IDBIRSellingDealer = sObjVehicle.IDBIRSellingDealer__c.substring(2, sObjVehicle.IDBIRSellingDealer__c.length());
                    }
                    if(mapIdDealer.containsKey(IDBIRSellingDealer)) {
                        tracking.ALR_Dealer__c = mapIdDealer.get(IDBIRSellingDealer).Id;
                        System.debug('############# tracking.ALR_Dealer__c '+tracking.ALR_Dealer__c);
                            lsAccount.add(mapIdDealer.get(IDBIRSellingDealer)); 
                    }else{
                            System.debug('####### Entrou no else ');
                            if(sObjVehicle.Id_Bir_BuyDealer__c !=null){
                            String Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c;
                            if(sObjVehicle.Id_Bir_BuyDealer__c.length() >8){
                                        Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c.substring(2, sObjVehicle.Id_Bir_BuyDealer__c.length());
                            }
                           if(mapIdDealer.containsKey(Id_Bir_BuyDealer)){
                               tracking.ALR_Dealer__c =mapIdDealer.get(Id_Bir_BuyDealer).Id;
                            System.debug('###### tracking.ALR_Dealer__c '+tracking.ALR_Dealer__c);
                           }
                        }
                        }
                    }
                    //concessionária de compra
                if(sObjVehicle.Id_Bir_BuyDealer__c !=null){
                    String Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c;
                    if(sObjVehicle.Id_Bir_BuyDealer__c.length() >8){
                                Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c.substring(2, sObjVehicle.Id_Bir_BuyDealer__c.length());
                    }
                    if(mapIdDealer.containsKey(Id_Bir_BuyDealer)){
                        tracking.ALR_BuyDealer__c = mapIdDealer.get(Id_Bir_BuyDealer).Id;
                        lsAccount.add( mapIdDealer.get(Id_Bir_BuyDealer));
                        if(sObjVehicle.IDBIRSellingDealer__c == null){
                           tracking.ALR_Dealer__c =mapIdDealer.get(Id_Bir_BuyDealer).Id;
                        }
                    }
                }
                 if(sObjVehicle.Status__c!=null)
                    if(sObjVehicle.Status__c.equalsIgnoreCase('Stock')){
                        tracking.ALR_Date_MADP__c = System.today();
                        tracking.ALR_Dealer_Stock__c = System.today();
                        tracking.ALR_Vehicle_Position__c = 'On dealer stock';
                    }
                    tracking.ALR_Chassi__c = sObjVehicle.Id;
                    tracking.ALR_Forecast_Delivery__c = sObjVehicle.DeliveryDate__c;
                    tracking.ALR_Status_System_Lock__c = 'Unbloked';
                    tracking.ALR_Status_System_Delivery__c = 'On time';
                    tracking.ALR_CTe__c = '34567890';
                    tracking.ALR_Pilot_Delivery__c =sObjVehicle.DeliveryDate__c;
                    tracking.ALR_Vehicle_Position__c = 'On compound';
                    tracking.ALR_in_Compound__c = sObjVehicle.DeliveryDate__c;
                    tracking.ALR_Status_Tracking__c = 'Active';
                    tracking.OwnerId = '005D0000002RIXh';
                    lsInTracking.add(tracking);
              }
            }
          }     
        try{
          Database.upsert(lsInTracking);
        }catch(Exception e){
          system.debug('**** ERRO ao inserir TRACKING: '+e);
        }
               
    }
    
    public static void insertTrackingCaseUpdate(List<VEH_Veh__c> lsNewVehicle, Map<Id,VEH_Veh__c> mapOldVehicle){
      List<VEH_Veh__c> lsInsertVehicles = new List<VEH_Veh__c>();
        
      for(VEH_Veh__c vehicle:lsNewVehicle){
      if(vehicle.VehicleSource__c != null)
            if(vehicle.VehicleSource__c.equalsIgnoreCase('SAP')){
              if(mapOldVehicle.containsKey(vehicle.ID)){
                 // if(mapOldVehicle.get(vehicle.Id).Status__c.equalsIgnoreCase('In Transit')){
                    lsInsertVehicles.add(vehicle);
                  //}
    
              }
            }
      }
      if(!lsInsertVehicles.isEmpty()){
        ALR_SharingTracking  shaTrac = new ALR_SharingTracking();
        //shaTrac.insertTracking(lsInsertVehicles);
        shaTrac.insertTrackingCaseShare(lsInsertVehicles);
          shaTrac.compartilhar();
      }
      
    }
    public static void deleteTracking(List<VEH_Veh__c> listVehicle){
      List<VEH_Veh__c> lsVehicleDelete = new List<VEH_Veh__c>();
      for(VEH_Veh__c veiculo : listVehicle){
        if(veiculo.Status__c != null &&  veiculo.VehicleSource__c != null)
        if(veiculo.Status__c.equalsIgnoreCase('CANCELLED') && veiculo.VehicleSource__c.equalsIgnoreCase('SAP')){
          lsVehicleDelete.add(veiculo);
        }
      }
      if(!lsVehicleDelete.isEmpty()){
        List<ALR_Tracking__c> lsDelTracking = [select id from ALR_Tracking__c where ALR_Chassi__c in:(lsVehicleDelete) ];
        try{
            Database.delete(lsDelTracking);
        }catch(Exception e){System.debug('#### ERROR: Erro ao deletar tracking '+e);}
      }
      
    }
    
    
    //compartilha o traking com os usuarios das contas que estao na lista
    public void compartilhar(){
        if(!lsInTracking.isEmpty() && !lsAccount.isEmpty()){
           List<UserRole> listaPerfisUser = [select Id,ParentRoleId,PortalAccountId from UserRole where PortalAccountId =:lsAccount];

           List<Group> listGroupUsers = [select id,RelatedId from Group where RelatedId in:listaPerfisUser];
            List<ALR_Tracking__Share> listTrackingShare = new List<ALR_Tracking__Share>();

            Map<String,Group> mapGroup = new Map<String,Group>();
            for(Group grupo:listGroupUsers){
                mapGroup.put(grupo.RelatedId,grupo);
            }
            
            for(ALR_Tracking__c tracking : lsInTracking){
                System.debug('######### tracking.ALR_Dealer__c '+tracking.ALR_Dealer__c);
                System.debug('######### tracking.ALR_BuyDealer__c '+tracking.ALR_BuyDealer__c);
                System.debug('######### tracking.Id '+tracking.Id);
                if(tracking.ALR_Dealer__c!=null && tracking.ALR_BuyDealer__c!=null && tracking.Id!=null){
                    for(UserRole user : listaPerfisUser){
                        if(tracking.ALR_Dealer__c.equals(user.PortalAccountId)|| tracking.ALR_BuyDealer__c.equals(user.PortalAccountId)){
                            if(mapGroup.containsKey(user.id)){
                                ALR_Tracking__Share trackingShare = new ALR_Tracking__Share(
                                    ParentId = tracking.Id, 
                                    UserOrGroupId = mapGroup.get(user.id).Id,
                                    AccessLevel = 'Edit',//nivel de acesso - esta como somente leitura
                                    RowCause = 'Manual'
                                );
                                listTrackingShare.add(trackingShare);
                            }
                            
                        }
                    }
                
                }
            }
            if(!listTrackingShare.isEmpty())
            try{
                
                insert listTrackingShare;
            }catch(Exception e){System.debug('##### Erro ao compartilhar o tracking');}
           
        }
    }

    
    //método para compartilhamento  apartir da troca da concessionária pelo veiculo - esta na parte de update
    public void compartilharCasoConcessionariaTrocada(List<VEH_Veh__c> listNewVehicles, Map<Id,VEH_Veh__c> listOldVehicles,Map<Id,VEH_Veh__c> mapNewVehicles){
        List<VEH_Veh__c> ListvehicleAntigo = new List<VEH_Veh__c>();//nessa lista com os ids quando a css muda
        List<String> listIdCssAntiga = new List<String>();// contem o id da bir da css antiga
        List<String> listIdBir = new List<String>();//contem o id da bir da nova lista de veiculos
        /*
        Map<String,Account> mapAccount = new Map<String,Account>();
        for(Account conta: [select id,IDBIR__C from Account where Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH']){
            mapAccount.put(conta.IDBIR__C, conta);
        }*/
        //verifica quais idBir mudaram para fazer a alteração
        for(VEH_Veh__c newVehicles : listNewVehicles){
            VEH_Veh__c vehiculo = listOldVehicles.get(newVehicles.Id);
            if(vehiculo.IDBIRSellingDealer__c !=null ){
                if(!vehiculo.IDBIRSellingDealer__c.equals(newVehicles.IDBIRSellingDealer__c))  {
                    //if(mapAccount.containsKey(vehiculo.IDBIRSellingDealer__c)){
                        ListvehicleAntigo.add(newVehicles);
                        listIdCssAntiga.add(vehiculo.IDBIRSellingDealer__c);
                   // }
                }
            }
            if(vehiculo.Id_Bir_BuyDealer__c!=null){
                if(!vehiculo.Id_Bir_BuyDealer__c.equals(newVehicles.Id_Bir_BuyDealer__c))  {
                    
                    ListvehicleAntigo.add(newVehicles);
                    listIdCssAntiga.add(vehiculo.Id_Bir_BuyDealer__c);
                }
                    listIdBir.add(newVehicles.Id);
            }
            
        }
     
         
        //agora começa a brincadeira
        exlcuirCssDeCompartilhamento(ListvehicleAntigo);//método para excluir os trackings Share com as concessionárias antigas
        atalizaTracking(listNewVehicles,mapNewVehicles);//atualiza o id da concessionária no traking
     
        
    }
    public void validarExisteTracking(List<VEH_Veh__c> listNewVehicles){
        
        List<String> chassi = new List<String>();
        List<String> chassiNoTR = new List<String>();
		Set<String> numChassi = new Set<String>();
        
        Map<String, VEH_Veh__c> lsVehNoTR = new Map<String, VEH_Veh__c>();
        List<VEH_Veh__c> vehNoTR = new List<VEH_Veh__c>();

        for(VEH_Veh__c aux : listNewVehicles){
            	chassi.add(aux.Name);
            	lsVehNoTR.put(aux.Name, aux);
        }
        numChassi.addAll(chassi);
        List<ALR_Tracking__c> listTracking = [Select Name from ALR_Tracking__c where Name in: chassi];
       
        if(listTracking.isEmpty()){
              chassiNoTR.addAll(chassi);
        }else{
            for(ALR_Tracking__c tr : listTracking){
                if(!numChassi.contains(tr.Name)){
                    chassiNoTR.add(tr.Name);
                }
            }
        }
        
        for(String vin : chassiNoTR){
            if(lsVehNoTR.containsKey(vin))
                vehNoTR.add(lsVehNoTR.get(vin));
        }
        
        try{
            insertTracking(vehNoTR);
        }catch(Exception e){
            system.debug('** ERRO ao criar TRACKING: '+e);
        }
        
    }
    
    public void atalizaTracking(List<VEH_Veh__c> listNewVehicles,Map<Id,VEH_Veh__c> mapNewVehicles){
        //List com os Trakings que contem os ids do veiculo
        List<VEH_Veh__c>     listVeiculo = new List<VEH_Veh__c>();
        List<String> idBirBuy = new List<String>();
        List<String> idBirSeller = new List<String>();
        for(String aux : mapNewVehicles.keySet()){
            listVeiculo.add(mapNewVehicles.get(aux));
            if(mapNewVehicles.get(aux).Id_Bir_BuyDealer__c !=null)
                idBirBuy.add(mapNewVehicles.get(aux).Id_Bir_BuyDealer__c);
            if(mapNewVehicles.get(aux).IDBIRSellingDealer__c!=null)
                idBirSeller.add(mapNewVehicles.get(aux).IDBIRSellingDealer__c);
        }
       List<ALR_Tracking__c> listTracking = [select id,ALR_Chassi__c,
                                             ALR_Date_MADP__c, ALR_Forecast_Delivery__c, 
                                             ALR_Pilot_Delivery__c, ALR_Vehicle_Position__c,
                                             ALR_Dealer_Stock__c, ALR_On_TCC_compound__c, ALR_Way_Dealer__c, 
                                             ALR_Way_Next_Compound__c, ALR_Destroyed__c, ALR_in_Compound__c,
                                             ALR_Status_System_Lock__c from ALR_Tracking__c where ALR_Chassi__c in:(listVeiculo)];
       //List de conta quando 
       List<Account> listConta = [select id,IDBIR__c from Account where (IDBIR__c in:(idBirBuy) or 
                                    IDBIR__c in:(idBirSeller)) and  Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH']; 
       //lista auxiliar para armazenar os trackings para o update
       List<ALR_Tracking__c> listTrackingToUpdate = new List<ALR_Tracking__c>();
       
        //mapa de conta a chave vai ser o id da Bir o outro valor vai ser o id
        Map<String,Account> mapAccount = new Map<String,Account>();
        
        for(Account conta : listConta){
            mapAccount.put(conta.IDBIR__c,conta);
        }                                 
       
       for(ALR_Tracking__c tracking : listTracking){
            String birBuy = mapNewVehicles.get(tracking.ALR_Chassi__c).Id_Bir_BuyDealer__c;
            String birSelling = '';
            if(mapNewVehicles.get(tracking.ALR_Chassi__c).IDBIRSellingDealer__c!=null){
              birSelling = mapNewVehicles.get(tracking.ALR_Chassi__c).IDBIRSellingDealer__c;
            }
            else{
              birSelling = mapNewVehicles.get(tracking.ALR_Chassi__c).Id_Bir_BuyDealer__c;
            }
           system.debug('**** Tracking da lista (tracking): '+tracking);
            if(tracking!=null){
                    ALR_Tracking__c newTracking = tracking;
                    if(mapAccount.containsKey(birBuy) && mapAccount.containsKey(birSelling)){
                        newTracking.ALR_BuyDealer__c = mapAccount.get(birBuy).Id;
                        newTracking.ALR_Dealer__c =     mapAccount.get(birSelling).Id;
                        newTracking.ALR_Date_MADP__c = tracking.ALR_Date_MADP__c;
                        newTracking.ALR_Forecast_Delivery__c = tracking.ALR_Forecast_Delivery__c;
                        newTracking.ALR_Pilot_Delivery__c = tracking.ALR_Pilot_Delivery__c;
                        newTracking.ALR_Vehicle_Position__c = tracking.ALR_Vehicle_Position__c;
                        newTracking.ALR_Dealer_Stock__c = tracking.ALR_Dealer_Stock__c;
                        newTracking.ALR_On_TCC_compound__c = tracking.ALR_On_TCC_compound__c;
                        newTracking.ALR_Way_Dealer__c = tracking.ALR_Way_Dealer__c;
                        newTracking.ALR_Way_Next_Compound__c = tracking.ALR_Way_Next_Compound__c;
                        newTracking.ALR_Destroyed__c = tracking.ALR_Destroyed__c;
                        newTracking.ALR_in_Compound__c = tracking.ALR_in_Compound__c;
                        newTracking.ALR_Status_System_Lock__c = tracking.ALR_Status_System_Lock__c;
                        listTrackingToUpdate.add(tracking);
                        system.debug('**** Tracking que será atualizado (newTracking): '+tracking);
                    }
            }
       }
       try{
            update  listTrackingToUpdate;
       }catch(Exception e){System.debug('Erro ao atualizarTracking '+e);}
       
       
       //preenche as listas para compartilhar o veiculo
       lsInTracking= listTrackingToUpdate;
       lsAccount  = listConta;
       compartilhar();//compa
      
    }
    
   //exclui os strakings com as concessionárias antigas
    public void exlcuirCssDeCompartilhamento(List<VEH_Veh__c> ListvehicleAntigo){
        //cada veiculo pode ter no maximo um traking
        List<ALR_TRacking__c> listTracking = [select id from ALR_TRacking__c where ALR_Chassi__c in:(ListvehicleAntigo)];
        
        
        List<ALR_TRacking__Share>  trackingShare = [select id, ParentId,UserOrGroupId  from ALR_Tracking__Share 
                                     where ParentId in:(listTracking)] ;
        try{
          delete trackingShare;
        }catch(Exception e){System.debug('Erro no método exlcuirCssDeCompartilhamento na classe ALR_SharingTracking');}
    }
    
    
    public void insertTrackingCaseShare(List<VEH_Veh__c> listVehicle){
        Map<String,Account> mapIdDealer = new Map<String,Account>();//mapa contem a partir do id da bir o id da conta  
        for(Account conta: [select id,IDBIR__c from Account where Country__c = 'Brazil' and RecordTypeId = '012D0000000KAoH']){
            mapIdDealer.put(conta.IDBIR__c, conta);
        }
        
        //para achar o id do tracking para compartilhar
        List<String> lsIdVehiculo2 = new List<String>();
        for(VEH_Veh__c vehiculo2 : listVehicle){
            lsIdVehiculo2.add(vehiculo2.Id);
        }
        
        Map<String,ALR_Tracking__c> mapTracking = new Map<String,ALR_Tracking__c>();
        for(ALR_Tracking__c tracki : [select id,ALR_Chassi__c from ALR_Tracking__c where ALR_Chassi__c in:(lsIdVehiculo2)]){
            mapTracking.put(tracki.ALR_Chassi__c, tracki);
        }
        
        for(VEH_Veh__c sObjVehicle : listVehicle){
           if(sObjVehicle.VehicleSource__c !=null && sObjVehicle.Id_Bir_BuyDealer__c!= null){
             if(sObjVehicle.VehicleSource__c.equalsIgnoreCase('SAP')) {
                 
                 ALR_Tracking__c tracking = new ALR_Tracking__c();
                 if(mapTracking.containsKey(sObjVehicle.Id))   
                   tracking.Id = mapTracking.get(sObjVehicle.Id).Id;
                   //tracking.Name = sObjVehicle.Name;
                  /* if(sObjVehicle.IDBIRSellingDealer__c == null){
                     sObjVehicle.IDBIRSellingDealer__c = sObjVehicle.Id_Bir_BuyDealer__c;
                   }*/
                   
                    
                   //concessionaria de venda
                if(sObjVehicle.IDBIRSellingDealer__c != null){
                    String IDBIRSellingDealer = sObjVehicle.IDBIRSellingDealer__c;
                    if(sObjVehicle.IDBIRSellingDealer__c.length() >8){ 
                                IDBIRSellingDealer = sObjVehicle.IDBIRSellingDealer__c.substring(2, sObjVehicle.IDBIRSellingDealer__c.length());
                    }
                    if(mapIdDealer.containsKey(IDBIRSellingDealer)) {
                        tracking.ALR_Dealer__c = mapIdDealer.get(IDBIRSellingDealer).Id;
                        System.debug('############# tracking.ALR_Dealer__c '+tracking.ALR_Dealer__c);
                            lsAccount.add(mapIdDealer.get(IDBIRSellingDealer)); 
                    }else{
                            System.debug('####### Entrou no else ');
                            if(sObjVehicle.Id_Bir_BuyDealer__c !=null){
                            String Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c;
                            if(sObjVehicle.Id_Bir_BuyDealer__c.length() >8){
                                        Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c.substring(2, sObjVehicle.Id_Bir_BuyDealer__c.length());
                            }
                           if(mapIdDealer.containsKey(Id_Bir_BuyDealer)){
                               tracking.ALR_Dealer__c =mapIdDealer.get(Id_Bir_BuyDealer).Id;
                            System.debug('###### tracking.ALR_Dealer__c '+tracking.ALR_Dealer__c);
                           }
                        }
                        }
                    }
                    //concessionária de compra
                if(sObjVehicle.Id_Bir_BuyDealer__c !=null){
                    String Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c;
                    if(sObjVehicle.Id_Bir_BuyDealer__c.length() >8){
                                Id_Bir_BuyDealer = sObjVehicle.Id_Bir_BuyDealer__c.substring(2, sObjVehicle.Id_Bir_BuyDealer__c.length());
                    }
                    if(mapIdDealer.containsKey(Id_Bir_BuyDealer)){
                        tracking.ALR_BuyDealer__c = mapIdDealer.get(Id_Bir_BuyDealer).Id;
                        lsAccount.add( mapIdDealer.get(Id_Bir_BuyDealer));
                        if(sObjVehicle.IDBIRSellingDealer__c == null){
                           tracking.ALR_Dealer__c =mapIdDealer.get(Id_Bir_BuyDealer).Id;
                        }
                    }
                }
                   tracking.name = sObjVehicle.name;
                    tracking.ALR_Chassi__c = sObjVehicle.Id;
                    tracking.ALR_Forecast_Delivery__c = sObjVehicle.DeliveryDate__c;
                    tracking.ALR_Status_System_Lock__c = 'Unbloked';
                    tracking.ALR_Status_System_Delivery__c = 'On time';
                    tracking.ALR_CTe__c = '34567890';
                    tracking.ALR_Pilot_Delivery__c =sObjVehicle.DeliveryDate__c;
                    //tracking.ALR_Vehicle_Position__c = 'On compound';
                    tracking.ALR_in_Compound__c = sObjVehicle.DeliveryDate__c;
                    tracking.ALR_Status_Tracking__c = 'Active';
                    tracking.OwnerId = '005D0000002RIXh';
                    lsInTracking.add(tracking);
              }
            }
          }     
        try{
          Database.upsert(lsInTracking);
        }catch(Exception e){
          system.debug('**** ERRO ao inserir TRACKING: '+e);
        }
               
    }

    //case idBir contains 769, replace 760
    public static void replaceBirDealer(List<VEH_Veh__c> lsVehicle){
        for(VEH_Veh__c vehicle : lsVehicle){

            //Para parar 100 queries - 16/08/2017
            vehicle.Tech_VINExternalID__c = vehicle.Name;

            if(vehicle.Status__c != null && (vehicle.Status__c.equalsIgnoreCase('Stock') || vehicle.Status__c.equalsIgnoreCase('In Transit'))){
                vehicle.Is_Available__c = true;
            } else {
                vehicle.Is_Available__c = false;
            }

            //Fim da tratativa para 101 queries


            if(vehicle.IDBIRSellingDealer__c!=null){
                if(vehicle.IDBIRSellingDealer__c.contains('769')){
                    vehicle.IDBIRSellingDealer__c = '760' +  vehicle.IDBIRSellingDealer__c.substring(vehicle.IDBIRSellingDealer__c.length()-4,vehicle.IDBIRSellingDealer__c.length()) ;  
                }
                if(vehicle.IDBIRSellingDealer__c.length()>7){//verifica se a trigger vem com o id da bir com 000 na frente
                    vehicle.IDBIRSellingDealer__c = vehicle.IDBIRSellingDealer__c.substring(vehicle.IDBIRSellingDealer__c.length()-7,vehicle.IDBIRSellingDealer__c.length());
                }
            }
            if(vehicle.Id_Bir_BuyDealer__c!=null){
                if(vehicle.Id_Bir_BuyDealer__c.contains('769')){
                    vehicle.Id_Bir_BuyDealer__c = '760' +  vehicle.Id_Bir_BuyDealer__c.substring(vehicle.Id_Bir_BuyDealer__c.length()-4,vehicle.Id_Bir_BuyDealer__c.length()) ;
                }
                if(vehicle.Id_Bir_BuyDealer__c.length()>7){//verifica se a trigger vem com o id da bir com 000 na frente
                    vehicle.Id_Bir_BuyDealer__c = vehicle.Id_Bir_BuyDealer__c.substring(vehicle.Id_Bir_BuyDealer__c.length()-7,vehicle.Id_Bir_BuyDealer__c.length());
                }
            }
        }
    }
    
    
    public static void CaseStatusEqualsStock(List<VEH_Veh__c> lsNewVehicles, Map<Id,VEH_Veh__c> mapOldVehicles){
        System.debug('### CaseStatusEqualsStock');
        List<String> lsChassiVehicle = new List<String>();
        for(VEH_Veh__c vehicle: lsNewVehicles){
            if(vehicle.Status__c!=null){
                if(vehicle.Status__c.equalsIgnoreCase('Stock')){
                    if(mapOldVehicles.containsKey(vehicle.Id)){
                        if(mapOldVehicles.get(vehicle.Id).Status__c!=null){
                            if(mapOldVehicles.get(vehicle.Id).Status__c.equalsIgnoreCase('In Transit')){
                                lsChassiVehicle.add(vehicle.Id);
                            }
                        }else{
                            lsChassiVehicle.add(vehicle.Id);
                        }
                    }
                }
            }
        }
        List<ALR_Tracking__c> lsTracking = new List<ALR_Tracking__c>();
        for(ALR_Tracking__c trackingAux : [select id,name from ALR_Tracking__c where ALR_Chassi__c in:(lsChassiVehicle)]){
            System.debug('### Entrou no for do tracking');
            ALR_Tracking__c tracking = new ALR_Tracking__c();
            tracking.id = trackingAux.id;
            tracking.ALR_Date_MADP__c = System.today();
            tracking.ALR_Dealer_Stock__c = System.today();
            tracking.ALR_Vehicle_Position__c = 'On dealer stock';
            lsTracking.add(tracking);
        }
        
        try{
            System.debug('#### lsTrackingUp '+lsTracking);
            Database.update(lsTracking);
        }catch(Exception e){
            System.debug('##### erro ao atualizar de in transit para stock');
        }
    }
    
    
    
    
}