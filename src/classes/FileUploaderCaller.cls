public interface FileUploaderCaller {
  void registerController (FileUploaderController target);
  void fileUploaderCallback (FileUploaderController target);
}