global class BatchToUpdatevehicle implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

List<VEH_Veh__c> list_vehicleType=new List<VEH_Veh__c>();
VEH_Veh__c vehicleobject;

    global Database.Querylocator start (Database.Batchablecontext BC){
            return Database.getQueryLocator(System.label.Bvm_Batch_Query);     
     }
    global void execute (Database.Batchablecontext BC, list<VEH_Veh__c> scope){
     
         for(VEH_Veh__c s : scope){
           if (s.name != null){

           vehicleobject=  VFC13_UpdateVehicleCallout.getVehicleDetails(s.name,s.id);
           list_vehicleType.add(vehicleobject);
           }               
         }
         Database.update(list_vehicleType,false);
    
    }  
    global void finish(Database.Batchablecontext BC){
        


      
    }    
          
}