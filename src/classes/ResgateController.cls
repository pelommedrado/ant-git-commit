public with sharing class ResgateController {
    
    public List<Opportunity> oppList{get;set;}
    public String dealerFilter{get;set;}
    
    public ResgateController(){
        oppLoad();
    }
    
    public List<Opportunity> lsOpp{
        get{
            return oppList;
        }
    }

    public void oppLoad(){
        
        // recupera as oportunidades de acordo com o filtro escolhido na pagina
        List<Opportunity> lsOpp = Database.query( 
            'SELECT Id, Name, LastModifiedDate, VehicleInterest__c, OpportunitySource__c, StageName,' +
            'OpportunitySubSource__c, ReasonLossCancellation__c, Dealer__c ' +
            'FROM Opportunity WHERE (ReasonLossCancellation__c IN ' +
            '(\'Abandonment\',\'Attendance\',\'Financing Not Approved\',\'Price\',\'Product\',\'Searching / Quote\',\'Used evaluation\') ' +
            'AND StageName = \'Lost\' AND Rescue__c = NULL AND CreatedDate = LAST_N_DAYS:30 ' +
            (!String.isBlank( dealerFilter ) ? 'AND Dealer__c = \'' + dealerFilter + '\') ' : ')') +
            'OR (StageName = \'Identified\' AND ElegibleToRescue__c = TRUE AND Rescue__c = NULL) ' +
            'ORDER BY CreatedDate ASC limit 999'
        );
        
        oppList = new List<Opportunity>();
        oppList.addAll(lsOpp);
    }
    
    public List< SelectOption > dealerSelectList{
        get{
            List< SelectOption > dealerSelectList = new List< SelectOption >();
            
            List<Opportunity> lsOpp = [SELECT Dealer__c, Dealer__r.Name FROM Opportunity 
                                 WHERE (ReasonLossCancellation__c IN
                                 ('Abandonment','Attendance','Financing Not Approved','Price','Product','Searching / Quote','Used evaluation')
                                 AND StageName = 'Lost' AND Rescue__c = NULL AND CreatedDate = LAST_N_DAYS:30)
                                 OR (StageName = 'Identified' AND ElegibleToRescue__c = TRUE)
                                 ORDER BY CreatedDate ASC limit 999];
            
            dealerSelectList.add(new SelectOption('','Todas as Concessionárias'));
            
            Set<Id> dealerId = new Set<Id>();
            for(Opportunity opp : lsOpp ){
                if(!dealerId.contains(opp.Dealer__c)){
                    dealerSelectList.add( new SelectOption( opp.Dealer__c, opp.Dealer__r.Name ) );
                    dealerId.add(opp.Dealer__c);
                }
            }
            return dealerSelectList;
        }
    }
    
    public void ignorarOpp(){
        
        String res = ApexPages.currentPage().getParameters().get('oppId');
        System.debug('res:' + res);

        List<String> listPassedParams = res.split(',');
        
        system.debug('listPassedParams = '+listPassedParams);
        
        List<Opportunity> lsOpps = new List<Opportunity>();
        
        for(String oppId: listPassedParams){
            Opportunity opp = new Opportunity(
                Id = oppId,
                Rescue__c = 'Ignored rescue'
            );
            lsOpps.add(opp);
        }
        
        String erro = '';
        List<Database.SaveResult> result = Database.Update(lsOpps,false);
        
        String message = result.size() + 
            ' registros atualizados, porém um ou mais registros não foram atualizados devido ao(s) seguinte(s) erro(s):' + '\n';
        for(Integer i=0; i < result.size(); i++){
            if (result.get(i).isSuccess()){
                result.get(i).getId();
            }
            else if (!result.get(i).isSuccess()){
                //failed record from the list
                lsOpps.get(i);
                erro += 'ID: ' + lsOpps.get(i).Id + ' ' + result.get(i).getErrors().get(0) + '\n';  
            }
        }
        if(erro.equals('')){
            Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.CONFIRM, 'Oportunidade(s) atualizada(s) com sucesso' ) );
        }else{
            Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.WARNING, erro ) );
        }
        
        oppLoad();
    }
    
    public PageReference reabrirOpp(){
        Id oppId = ApexPages.currentPage().getParameters().get('oppId');
        
        Opportunity opp = new Opportunity(
            Id = oppId,

            // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
            DateTimeSellerUpdatedStageOpp__c = System.now(),

            StageName = 'In Attendance',
            Rescue__c = 'Rescued'
        );

        try{
            Update opp;  
        }catch( Exception e ){
            Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
        
        PageReference pg = new PageReference('/'+oppId);
        return pg;
    }
    
    public PageReference criaNovaOpp(){
        Id oppId = ApexPages.currentPage().getParameters().get('oppId');
        
        // atualiza oportunidade tratada para resgatada
        Opportunity opp = new Opportunity(
            Id = oppId,
            OpportunitySource__c = 'DEALER',
            OpportunitySubSource__c = 'THROUGH',
            Detail__c = 'NEW VEHICLE',
            Sub_Detail__c = 'RESCUE',
            Rescue__c = 'Rescued'  
        );

        try{
            Update opp;  
        }catch( Exception e ){
            Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
        
        // mapa da oportunidade escolhida com oportunidade nova
        Opportunity newOpp = mapOpportunity(oppId);
        newOpp.StageName = 'In Attendance';
        newOpp.CloseDate = system.today().addDays(30);
        newOpp.OwnerId = UserInfo.getUserId();
        newOpp.CurrencyIsoCode = 'BRL';
        newOpp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR');
        newOpp.Rescue__c = 'Rescued';
        newOpp.Rescued_Opportunity__c = oppId;
        newOpp.Id = null;

        try{
            Insert newOpp;  
        }catch( Exception e ){
            Apexpages.addMessage( new Apexpages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }

        // direciona para a oportunidade recem criada no SFA
        PageReference pg = new PageReference('/cac/apex/clienteoportunidadesfa2?Id='+newOpp.Id);
        return pg;
    }
    
    public Opportunity mapOpportunity(Id oppId){
        
        Opportunity opp = [SELECT AccountId, Amount, Dealer__c, Description, LeadSource__c, LeadSource, 
    				  LeadSubsource__c, Name, SyncedQuoteId, OpportunitySource__c, OpportunitySubSource__c,
                      Detail__c, Sub_Detail__c, OwnerId, Owner.Email, Owner.Name, Owner.Phone, 
                      PeriodNextPurchase__c,Pricebook2Id, IsPrivate, Probability, RecordTypeId,
                      SourceMedia__c, OpportunityTransition__c, CampaignId, Account.OptinSMS__c,
    				  Account.FirstName, Account.LastName, Account.Name, Account.PersonBirthdate,
    				  Account.CustomerIdentificationNbr__c, Account.PersEmailAddress__c, Account.PersonEmail,
					  Account.PersLandline__c, Account.ProfLandline__c, Account.ProfMobPhone__c, 
					  Account.PersMobPhone__c, Account.OptinPhone__c, Account.OptinEmail__c, 
    				  Account.TypeOfInterest__c, Account.Campaign_BR__c, Account.VehicleInterest_BR__c,
					  Account.SecondVehicleOfInterest__c, Account.CurrentVehicle_BR__c, 
					  Account.CurrentVehicle_BR__r.Brand__c, Account.CurrentVehicle_BR__r.Model__c,
    				  Account.YrReturnVehicle_BR__c, Account.ShippingStreet,
				      Account.ShippingCity, Account.ShippingState, Account.ShippingPostalCode, 
				      Account.ShippingCountry,Account.Id, Account.CompanyID__c, PromoCode__c 
			     FROM Opportunity    								
                WHERE Id =: oppId];
        
        return opp;
    }
    
}