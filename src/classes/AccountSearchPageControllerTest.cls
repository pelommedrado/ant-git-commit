@isTest
public class AccountSearchPageControllerTest{
    
    static testMethod void testmet() {
        
        Test.startTest(); 
        
        User usr = new User (LastName='ts1',  
                             FirstName='Rotondo1',
                             BypassVR__c=true, 
                             alias='lro',
                             Email='lrotondo@rotondo.com',
                             EmailEncodingKey='UTF-8',
                             LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US',
                             ProfileId='00eD0000001o3xDIAQ',
                             TimeZoneSidKey='America/Los_Angeles', 
                             UserName='lrotondo@lrotondo.com',
                             RecordDefaultCountry__c = 'Brazil');
        
        System.runas(usr) {
            
            Id RTID_COMPANY = [select Id from RecordType where sObjectType='Account' and DeveloperName='Company_Acc' limit 1].Id;
            Account Acc = new Account(Name='Test1',
                                      Phone='0000',
                                      RecordTypeId=RTID_COMPANY, 
                                      ProfEmailAddress__c = 'addr1@mail.com', 
                                      ShippingCity = 'Paris', 
                                      ShippingCountry = 'France', 
                                      ShippingState = 'IDF', 
                                      ShippingPostalCode = '75013', 
                                      ShippingStreet = 'my street');
            insert Acc;
            
            Contact Con = new Contact(LastName='Test Contact',
                                      FirstName='Test', 
                                      Salutation='Mr.',
                                      ProEmail__c='lro@lro.com',
                                      AccountId=Acc.Id);
            insert Con;
            
            Case c = new Case(Origin='Email',
                              Type='Information Request',
                              SubType__c = 'Booking',
                              Status='Open',
                              Priority='Medium',
                              Description='Description',
                              From__c='Customer',
                              AccountId=Acc.Id,
                              ContactId=Con.Id);
            insert c;
            
            ApexPages.currentPage().getParameters().put('show','false');
            ApexPages.currentPage().getParameters().put('Id',Acc.Id);
            ApexPages.currentPage().getParameters().put('getAcName','Rotondo1'); 
            ApexPages.currentPage().getParameters().put('getAccEmail','lrotondo@rotondo.com');
            
            ApexPages.StandardController controller1 = new ApexPages.StandardController(Acc);
            
            AccountSearchPageController pageCtrl = new AccountSearchPageController(controller1);
            pageCtrl.getfName 		= 'getfName';
            pageCtrl.getAcName 		= 'getAccEmail';
            pageCtrl.getAccEmail 	= 'getAccEmail';
            pageCtrl.getPhNo 		= 'getPhNo';
            pageCtrl.getStreet 		= 'getStreet';
            pageCtrl.getCity		= 'getCity';
            pageCtrl.getZip 		= 'getZip';
            
            AccountSearchPageController pageCtrl1 = new AccountSearchPageController();
            
            Integer i = pageCtrl.showDetail;
            pageCtrl.showDetail = 0;
            pageCtrl.VIN 		= 'VIN';
            pageCtrl.country 	= 'country';
            pageCtrl.brand 		= 'brand';
            pageCtrl.demander 	= 'demander';
            pageCtrl.lastName 	= 'lastName';
            pageCtrl.firstName 	= 'firstName';
            pageCtrl.city		= 'city';
            pageCtrl.zip		= 'zip';
            pageCtrl.ident1		= 'ident1';
            pageCtrl.idClient	= 'idClient';
            pageCtrl.idMyr		= 'idMyr';
            pageCtrl.firstRegistrationDate = 'firstRegistrationDate';
            pageCtrl.registration = 'registration';
            pageCtrl.email		= 'email';
            pageCtrl.ownedVehicles = 'ownedVehicles';
            pageCtrl.nbReplies  = 'nbReplies';
            pageCtrl.bcsId		= 'bcsId';
            
            pageCtrl.createNewAccount();     
            pageCtrl.createAccount();   
            pageCtrl.initialize();
            pageCtrl.getAccount();
            
            pageCtrl.MergeAccount();
            pageCtrl.processSelectedcmd();  
            pageCtrl.getAccountList();
            
            pageCtrl.TestButton();
            pageCtrl.getSelected();
            //pageCtrl.getCountryInfo();
            
           
        }
        
        Test.stopTest(); 
    }
}