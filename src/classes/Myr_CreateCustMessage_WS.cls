/** MyR BackEnd: Insert notifications linked to personal account
	The webservice is able to manage several notifications in inputs 
	(until 100 in a same chunk)
	
	Response codes of the WebService:
		WS06MS000	OK
		WS06MS001 – WS06MS500	OK WITH WARNING
		WS06MS501 – WS06MS999	CRITICAL ERROR 
	A return code is given for eahc message in entry
	
	V1.0: S. Ducamp / Atos / 08.07.2012 
**/			 
global without sharing class Myr_CreateCustMessage_WS {    
	
	WebService static Myr_CreateCustMessage_WS_Response createMessages(List<Myr_CustMessage_Cls> custMessages) {
		//insert the messages 
		Myr_CreateCustMessage_Cls.CreateMsgResponse crMsgResp = Myr_CreateCustMessage_Cls.createMessages(custMessages);  
		
		//prepare the response
		Myr_CreateCustMessage_WS_Response response = new Myr_CreateCustMessage_WS_Response();
		response.info = new Myr_CreateCustMessage_WS_Response_Msg();
		// --- global failure
		if( Myr_CreateCustMessage_Cls.Status.KO == crMsgResp.status ) {
			response.info.code = system.Label.Customer_Message_WS06MS501;
			response.info.message = system.Label.Customer_Message_WS06MS501_Msg_WS + ' ' + crMsgResp.message;
			return response; 
		}
		response.info.code = system.Label.Customer_Message_WS06MS000;
		response.info.message = system.Label.Customer_Message_WS06MS000_Msg_WS;
		response.messageStatus = crMsgResp.listStatus;
		Integer errorsCount = 0;
		for( Myr_CustMessage_Cls.Status status : crMsgResp.listStatus ) {
			errorsCount += (status.isOk()) ? 0 : 1; 
		}
		//set the global response
		if( errorsCount == crMsgResp.listStatus.size() ) {
			response.info.code = system.Label.Customer_Message_WS06MS502;
			response.info.message = system.Label.Customer_Message_WS06MS502_Msg_WS;
		} else if( errorsCount > 0) {
			response.info.code = system.Label.Customer_Message_WS06MS100;
			response.info.message = system.Label.Customer_Message_WS06MS100_Msg_WS;
		}

    	return response;
	}
	
	/** Inner classes that describe the global return of the webservice **/
	global class Myr_CreateCustMessage_WS_Response_Msg {
    	WebService String code;
    	WebService String message;
	}
	
	global class Myr_CreateCustMessage_WS_Response {
		WebService List<Myr_CustMessage_Cls.Status> messageStatus;
    	WebService Myr_CreateCustMessage_WS_Response_Msg info;
	}	

}