global class VWF156_ALRFlowActivateInsurance implements Process.Plugin{
    
    private static String DDDHomePhone;
    private static String DDDPhone;
    private static String DDDMobile;
    private static String HomePhone;
    private static String Phone;
    private static String Mobile;
    private static String Name;
    private static String Email;
    
    global Process.PluginResult invoke(Process.PluginRequest request){
        
        String contactID = (String) request.inputParameters.get('ALR_varContactID');
        system.debug('*** ID Contato: '+contactID);
        
        if(!String.isBlank(contactID))
        	VWF156_ALRFlowActivateInsurance.getContact(contactID);
        
        Map<String,String> result = new Map<String,String>();
        result.put('ALR_varNameContato', Name);
        result.put('ALR_varEmailContato', Email);
        result.put('ALR_varDDDHomePhone', DDDHomePhone);
        result.put('ALR_varDDDPhone', DDDPhone);
        result.put('ALR_varDDDMobile', DDDMobile);
        result.put('ALR_varHomePhone', HomePhone);
        result.put('ALR_varPhone', Phone);
        result.put('ALR_varMobile', Mobile); 
        
        return new Process.PluginResult(result);
    }
    
    //Descreve a entrada e saída de parametro no FLOW
    global Process.PluginDescribeResult describe(){
        
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.description = 'Dados Contato quando inspetor é o contato';
        result.tag = 'GET CONTATO TO INSPETOR';
        
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter>{
          
            new Process.PluginDescribeResult.InputParameter('ALR_varContactID',
                                                            Process.PluginDescribeResult.ParameterType.STRING, true)
            
        };
  		
        result.outputParameters = new List<Process.PluginDescribeResult.OutputParameter>{
        	
            new Process.PluginDescribeResult.OutputParameter('ALR_varNameContato',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varEmailContato',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varDDDPhone',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varPhone',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varDDDMobile',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varMobile',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varDDDHomePhone',
                                                             Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('ALR_varHomePhone',
                                                             Process.PluginDescribeResult.ParameterType.STRING)
        };
            
        return result;
    }
    
    public static void getContact(String contactID){
        
        try{
        
            Contact contato = [Select Name, Email, Phone, PersMobPhone__c, ProPhone__c from contact where Id =: contactID];
            
            //seta nome do contato
            if(!contato.Name.equalsIgnoreCase(' ')){
                System.debug('**** NOME');
                Name = contato.Name;
            }
            //seta E-mail do contato 
            if(!contato.Email.equalsIgnoreCase(' ')){
                System.debug('**** EMAIL');
                Email = contato.Email;
            }
            //Trata Phone
            if (contato.ProPhone__c <> null){
                System.debug('**** PHONE');
                Integer tamanho = contato.ProPhone__c.length();
                if(tamanho > 8){
                    if(tamanho > 10){
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.ProPhone__c.substring(0, 3));
                        DDDPhone = contato.ProPhone__c.substring(0, 3);
                        Phone = contato.ProPhone__c.substring(3, tamanho);
                        
                    }else{
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.ProPhone__c.substring(0, 2));
                        DDDPhone = contato.ProPhone__c.substring(0, 2);
                        Phone = contato.ProPhone__c.substring(2, tamanho);
                        
                    }                    
                }
            }
            
            //Trata Mobile
            if (contato.PersMobPhone__c <> null){
                System.debug('**** MOBILE'+contato.PersMobPhone__c);
                Integer tamanho = contato.PersMobPhone__c.length();
                if(tamanho > 9){
                    if(tamanho > 11){
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.PersMobPhone__c.substring(1, 3));
                        DDDMobile = contato.PersMobPhone__c.substring(1, 3);
                        Mobile = contato.PersMobPhone__c.substring(3, tamanho);
                        
                    }else{
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.PersMobPhone__c.substring(0, 2)); 
                        DDDMobile = contato.PersMobPhone__c.substring(0, 2);
                        Mobile = contato.PersMobPhone__c.substring(2, tamanho);
                        
                    }
                }
            }
            
            //Trata HomePhone
            if (contato.Phone <> null){
                System.debug('**** HOMEPHONE');
                Integer tamanho = contato.Phone.length();
                if(tamanho > 8){
                    if(tamanho > 10){
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.Phone.substring(0, 3));
                        DDDHomePhone = contato.Phone.substring(0, 3);
                        HomePhone = contato.Phone.substring(3, tamanho);
                        
                    }else{
                        //String temp = contato.Phone;
                        system.debug('**** '+contato.Phone.substring(0, 2)); 
                        DDDHomePhone = contato.Phone.substring(0, 2);
                        HomePhone = contato.Phone.substring(2, tamanho);
                        
                    }                    
                }
            }
        }catch(Exception e){
            system.debug('**** Mensagem: '+e);
        }
    }

}