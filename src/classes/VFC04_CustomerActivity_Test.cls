/**
Class   -   VFC04_CustomerActivity_Test
Author  -   RameshPrabu
Date    -   05/09/2012

#01 <RameshPrabu> <05/09/2012>
Created this class using test for VFC04_CustomerActivity_v2.
**/
@isTest 
public with sharing class VFC04_CustomerActivity_Test {
    
    static void insertSobjects(){
        List<RCM_Recommendation__c> recomm = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRecommendationObject();
        List<RDT_RelationshipDecisionTree__c> RelationshipTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRelationshipDecisionTreeObject();
        List<NCT_NonCustomerDecisionTree__c> NonCustomerTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToNonCustomerRelationshipObjects();
        List<CDT_CustomerDecisionTree__c> CustomerTree = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToCustomerRelationshipObjects();
        List<AST_AfterSalesDecisionTree__c> AfterSales = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToAfterSalesDecisionTreeObjects();
        List<TST_Transaction__c> lstTransactionsInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToTransactionObject();
        String developerName = 'RCM_CommercialRecommendation';
        RecordType recordTypeId =  VFC08_RecordTypeDAO.getInstance().findRecordTypeByDeveloperName(developerName);
        List<Account> lstAccounts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        List<Lead> leads1 = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        VFC09_LeadDAO.getInstance().findLeadByLeadId(leads1[0].Id);
        List<String> deleteData = new List<String>();
        deleteData.add(leads1[0].Id);
        List<Lead> lstLeadInsert = new List<Lead>();
        Lead leadInsert = new Lead();
        leadInsert.LastName = 'Ramesh';
        lstLeadInsert.add(leadInsert);
        
        Lead leadInsert1 = new Lead();
        leadInsert1.LastName = 'Ramesh';
        
        //Test.startTest();
        // desc
        //List<Database.SaveResult> insertList = VFC09_LeadDAO.getInstance().insertData(lstLeadInsert);
        //Test.stopTest();
        // System.assert(insertList.size() > 0);
        
        VFC09_LeadDAO.getInstance().insertData(leadInsert1);
        VFC09_LeadDAO.getInstance().updateData(leads1[0]);
        VFC09_LeadDAO.getInstance().updateData(leads1);
        VFC09_LeadDAO.getInstance().deleteData(deleteData);
        VFC09_LeadDAO.getInstance().deleteData(leads1[1].Id);
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test7(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        insert dealer;
        
        insertSobjects();
        
        
        /* update Lead and Insert Traansaction */
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = null;
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.leadStatus = 'Hot Lead';
        //system.debug('lstAccounts.....' +lstAccount[3].Name);
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.leadRecord.IsRenaultVehicleOwner__c = 'Y';
        customer.objCustomerActivityEntityForm.leadRecord.CurrentMilage__c = 3000;
        customer.objCustomerActivityEntityForm.leadRecord.IntentToPurchaseNewVehicle__c = 'No intention';
        customer.objCustomerActivityEntityForm.leadRecord.DealerOfInterest__c = dealer.Id;
        system.debug('Account Id.....' +customer.objCustomerActivityEntityForm.leadRecord.DealerOfInterest__c);
        system.debug('Lead Status.....' +customer.objCustomerActivityEntityForm.leadStatus);
        system.debug('Lead Record.....' +customer.objCustomerActivityEntityForm.leadRecord);
        system.debug('transactionRecord.....' +customer.objCustomerActivityEntityForm.transactionRecord);
        customer.updateLeadAndInsertTransaction();
        
        customer.leadConvertTOAccountAndOpportunity(leads[0].Id);
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test8(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test9(){
        
        test.startTest();
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Not Effective';
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotContact__c = null;
        customer.updateLeadAndInsertTransaction();
        
        //customer.leadStatus = 'Identified';
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotContact__c = 'busy';
        customer.updateLeadAndInsertTransaction();
        
        test.stopTest();
    }
        
        static testMethod void VFC04_CustomerActivity_Test17(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.leadStatus = '';
        customer.updateLeadAndInsertTransaction();
            
        customer.objCustomerActivityEntityForm.leadStatus = 'Lead';
        customer.updateLeadAndInsertTransaction();
            
        }
        
        static testMethod void VFC04_CustomerActivity_Test18(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.leadStatus = 'Portfolio';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.leadRecord.IsRenaultVehicleOwner__c = 'Y';
        customer.objCustomerActivityEntityForm.leadRecord.CurrentMilage__c = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test10(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.leadRecord.CurrentMilage__c = 3000;
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.leadRecord.IsVehicleOwner__c = 'Y';
        customer.objCustomerActivityEntityForm.thisBrand = null;
        customer.objCustomerActivityEntityForm.thisModel = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test11(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.thisBrand = 'FIAT';
        customer.objCustomerActivityEntityForm.thisModel = 'DUCATO-10';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.leadRecord.IntentToPurchaseNewVehicle__c = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test12(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.leadRecord.IntentToPurchaseNewVehicle__c = 'No intention';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotOpportunity__c = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    static testMethod void VFC04_CustomerActivity_Test13(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.leadStatus = 'Lead';
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotOpportunity__c = 'Interest in product competition';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.transactionRecord.BrandAndModelCompetitive__c = null;
        customer.updateLeadAndInsertTransaction();
        
    }
    static testMethod void VFC04_CustomerActivity_Test14(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.transactionRecord.BrandAndModelCompetitive__c = 'Test';
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.ReceiveNewsbyPhone = true;
        customer.objCustomerActivityEntityForm.ReceiveNewsviaEmail = false;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test15(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        customer.objCustomerActivityEntityForm.ReceiveNewsviaEmail = true;
        customer.objCustomerActivityEntityForm.ReceiveNewsbyPhone = false;
        customer.objCustomerActivityEntityForm.ReceiveUpdateviaSMS = false;
        customer.updateLeadAndInsertTransaction();
        
        customer.objCustomerActivityEntityForm.ReceiveUpdateviaSMS = true;
        customer.updateLeadAndInsertTransaction();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test16(){
        
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
        Lead leadinfo1 = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leadinfo1);
        
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        
        insertSobjects();
        
        List<RCM_Recommendation__c> lstRecomm = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToRecommendationObject();
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.recommendation1 = true;
        customer.objCustomerActivityEntityForm.recommendation2 = true;
        customer.objCustomerActivityEntityForm.recommendation3 = true;
        customer.objCustomerActivityEntityForm.recommendationValueId1 = lstRecomm[0].Id;
        customer.objCustomerActivityEntityForm.recommendationValueId2 = lstRecomm[1].Id;
        customer.objCustomerActivityEntityForm.recommendationValueId3 = lstRecomm[2].Id;
        //customer.updateLeadAndInsertTransaction();
        
        //Apexpages.currentPage().getParameters().put( 'id', leads[0].Id );
        system.debug('Lead Id.....' +customer.objCustomerActivityEntityForm.leadRecord.Id);
        system.debug('Lead Record.....' +customer.objCustomerActivityEntityForm.leadRecord);
        
        //List<Account> lstAccount = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObject();
        customer.objCustomerActivityEntityForm.leadStatus = 'Hot Lead';
        //system.debug('lstAccounts.....' +lstAccount[3].Name);
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.transactionRecord.TypeOfContact__c = 'Phone';
        customer.objCustomerActivityEntityForm.leadRecord.IsRenaultVehicleOwner__c = 'Y';
        customer.objCustomerActivityEntityForm.leadRecord.CurrentMilage__c = 3000;
        customer.objCustomerActivityEntityForm.leadRecord.IntentToPurchaseNewVehicle__c = 'No intention';
        //customer.objCustomerActivityEntityForm.leadRecord.DealerOfInterest__c = lstNWSiteAccounts[1].Id;
        system.debug('Account Id.....' +customer.objCustomerActivityEntityForm.leadRecord.DealerOfInterest__c);
        system.debug('Lead Status.....' +customer.objCustomerActivityEntityForm.leadStatus);
        system.debug('Lead Record.....' +customer.objCustomerActivityEntityForm.leadRecord);
        system.debug('transactionRecord.....' +customer.objCustomerActivityEntityForm.transactionRecord);
        customer.updateLeadAndInsertTransaction();
        
        
        customer.objCustomerActivityEntityForm.leadStatus = 'Hot Lead';
        customer.objCustomerActivityEntityForm.leadRecord.DealerOfInterest__c = null;
        customer.updateLeadAndInsertTransaction();
        
        customer.redirectToLeadRecord();
        
    }
    
    /*
Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
Lead leadinfo1 = new Lead();
ApexPages.StandardController stdCont1 = new ApexPages.StandardController(leadinfo1);
VFC04_CustomerActivity_v2 customer1 = new VFC04_CustomerActivity_v2(stdCont1);

VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[1].Id );

Apexpages.currentPage().getParameters().put( 'id', leads[1].Id );
Lead leadinfoNew1 = new Lead();
ApexPages.StandardController stdContnew1 = new ApexPages.StandardController(leadinfoNew1);*/
    
    
    static testMethod void VFC04_CustomerActivity_Test() {
        Lead leadinfonull = new Lead();
        ApexPages.StandardController stdContnull = new ApexPages.StandardController(leadinfonull);
        VFC04_CustomerActivity_v2 customerNull = new VFC04_CustomerActivity_v2(stdContnull);
        customerNull.enableNotContactReason();
        Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where 
                                  (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc','Personal_Acc'))])
        {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        //List<Account> lstNWSiteAccounts = [Select a.Id, a.Name,  a.OwnerId
        //	from Account a Where a.RecordTypeId= :recTypeIds.get('Network_Site_Acc')];
        List<Account> lstNWSiteAccounts =  VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToAccountObjectWithNetworkSite();
        system.debug('Account Info....' +lstNWSiteAccounts[0]);
        
        insertSobjects();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        Test.startTest();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        //Test.stopTest();
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[0].Id );
        Lead leadinfo = new Lead();
        ApexPages.StandardController stdCont = new ApexPages.StandardController(leads[0]);
        VFC04_CustomerActivity_v2 customer = new VFC04_CustomerActivity_v2(stdCont);
        system.debug('Lead Id.....' +customer.objCustomerActivityEntityForm.leadRecord.Id);
        customer.getTransactions();
        customer.getBrands();
        customer.getModels();
        customer.objCustomerActivityEntityForm.thisBrand = String.valueOf(molicar[0].Brand__c);
        customer.getModelDetails();
        customer.getModels();
        String Brand = customer.getSelectedBrand();        
        customer.setSelectedBrand(Brand);
        String Model = customer.getSelectedModel();
        customer.setSelectedModel(Model);
        
        customer.objCustomerActivityEntityForm.leadInfo.Status = 'Identified';
        customer.getStatus();
        customer.objCustomerActivityEntityForm.leadInfo.Status = 'Portfolio';
        customer.getStatus();
        String Status = customer.getSelectedStatus();
        customer.setSelectedStatus(Status);
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = null;
        customer.enableNotContactReason();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Not Effective';
        customer.enableNotContactReason();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.enableNotContactReason();
        
        customer.objCustomerActivityEntityForm.transactionRecord.ContactStatus__c = 'Effective';
        customer.objCustomerActivityEntityForm.leadInfo.Status = 'Hot Lead';
        customer.enableNotContactReason();
        
        customer.enableStrategicQuestions();
        customer.objCustomerActivityEntityForm.leadRecord.IsRenaultVehicleOwner__c = 'Y';
        customer.enableStrategicQuestions();
        customer.objCustomerActivityEntityForm.leadRecord.IsRenaultVehicleOwner__c = 'N';
        customer.enableStrategicQuestions();
        
        customer.enableBrandModel();
        customer.objCustomerActivityEntityForm.leadRecord.IsVehicleOwner__c = 'Y';
        customer.enableBrandModel();
        
        customer.objCustomerActivityEntityForm.leadStatus = null;
        customer.enableWhyNotOppoutunity();
        customer.objCustomerActivityEntityForm.leadStatus = 'Hot Lead';
        customer.enableWhyNotOppoutunity();
        customer.objCustomerActivityEntityForm.leadStatus = 'Identified';
        customer.enableWhyNotOppoutunity();
        
        customer.enableBrandCompetitive();
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotOpportunity__c = 'Interest in product competition';
        customer.enableBrandCompetitive();
        customer.objCustomerActivityEntityForm.transactionRecord.WhyNotOpportunity__c = 'competition';
        customer.enableBrandCompetitive();
        
        
        /*		VFC04_CustomerActivity_v2 customerNew1 = new VFC04_CustomerActivity_v2(stdContnew1);




Apexpages.currentPage().getParameters().put( 'id', leads[2].Id );
Lead leadinfo2 = new Lead();
ApexPages.StandardController stdCont2 = new ApexPages.StandardController(leadinfo2);
VFC04_CustomerActivity_v2 customer2 = new VFC04_CustomerActivity_v2(stdCont2);
*/     
        //VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[2].Id );
        /*         
Apexpages.currentPage().getParameters().put( 'id', leads[2].Id );
Lead leadinfoNew2 = new Lead();
ApexPages.StandardController stdContNew2 = new ApexPages.StandardController(leadinfoNew2);
VFC04_CustomerActivity_v2 customerNew2 = new VFC04_CustomerActivity_v2(stdContNew2);


Apexpages.currentPage().getParameters().put( 'id', leads[3].Id );
Lead leadinfo3 = new Lead();
ApexPages.StandardController stdCont3 = new ApexPages.StandardController(leadinfo3);
VFC04_CustomerActivity_v2 customer3 = new VFC04_CustomerActivity_v2(stdCont3);

VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[3].Id );

Apexpages.currentPage().getParameters().put( 'id', leads[3].Id );
Lead leadinfoNew3 = new Lead();
ApexPages.StandardController stdContNew3 = new ApexPages.StandardController(leadinfoNew3);
VFC04_CustomerActivity_v2 customerNew3 = new VFC04_CustomerActivity_v2(stdContNew3);
*/
        Test.stopTest();
    }
    
    
    static testMethod void VFC04_CustomerActivity_Test1() {
        insertSobjects();
        Test.startTest();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[4].Id );
        Lead leadinfo4 = new Lead();
        ApexPages.StandardController stdCont4 = new ApexPages.StandardController(leadinfo4);
        VFC04_CustomerActivity_v2 customer4 = new VFC04_CustomerActivity_v2(stdCont4);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[4].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[4].Id );
        Lead leadinfoNew4 = new Lead();
        ApexPages.StandardController stdContNew4 = new ApexPages.StandardController(leadinfoNew4);
        VFC04_CustomerActivity_v2 customerNew4 = new VFC04_CustomerActivity_v2(stdContNew4);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[5].Id );
        Lead leadinfo5 = new Lead();
        ApexPages.StandardController stdCont5 = new ApexPages.StandardController(leadinfo5);
        VFC04_CustomerActivity_v2 customer5 = new VFC04_CustomerActivity_v2(stdCont5);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[5].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[5].Id );
        Lead leadinfoNew5 = new Lead();
        ApexPages.StandardController stdContNew5 = new ApexPages.StandardController(leadinfoNew5);
        VFC04_CustomerActivity_v2 customerNew5 = new VFC04_CustomerActivity_v2(stdContNew5);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[6].Id );
        Lead leadinfo6 = new Lead();
        ApexPages.StandardController stdCont6 = new ApexPages.StandardController(leadinfo6);
        VFC04_CustomerActivity_v2 customer6 = new VFC04_CustomerActivity_v2(stdCont6);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[6].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[6].Id );
        Lead leadinfoNew6 = new Lead();
        ApexPages.StandardController stdContNew6 = new ApexPages.StandardController(leadinfoNew6);
        VFC04_CustomerActivity_v2 customerNew6 = new VFC04_CustomerActivity_v2(stdContNew6);
        
        Test.stopTest();
        
    }
    
    static testMethod void VFC04_CustomerActivity_Test2() {
        insertSobjects();
        Test.startTest();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        //Test.stopTest();
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        //Test.startTest();
        Apexpages.currentPage().getParameters().put( 'id', leads[7].Id );
        Lead leadinfo7 = new Lead();
        ApexPages.StandardController stdCont7 = new ApexPages.StandardController(leadinfo7);
        VFC04_CustomerActivity_v2 customer7 = new VFC04_CustomerActivity_v2(stdCont7);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[7].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[7].Id );
        Lead leadinfoNew7 = new Lead();
        ApexPages.StandardController stdContNew7 = new ApexPages.StandardController(leadinfoNew7);
        VFC04_CustomerActivity_v2 customerNew7 = new VFC04_CustomerActivity_v2(stdContNew7);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[8].Id );
        Lead leadinfo8 = new Lead();
        ApexPages.StandardController stdCont8 = new ApexPages.StandardController(leadinfo8);
        VFC04_CustomerActivity_v2 customer8 = new VFC04_CustomerActivity_v2(stdCont8);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[8].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[8].Id );
        Lead leadinfoNew8 = new Lead();
        ApexPages.StandardController stdContNew8 = new ApexPages.StandardController(leadinfoNew8);
        VFC04_CustomerActivity_v2 customerNew8 = new VFC04_CustomerActivity_v2(stdContNew8);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[9].Id );
        Lead leadinfo9 = new Lead();
        ApexPages.StandardController stdCont9 = new ApexPages.StandardController(leadinfo9);
        VFC04_CustomerActivity_v2 customer9 = new VFC04_CustomerActivity_v2(stdCont9);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[9].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[9].Id );
        Lead leadinfoNew9 = new Lead();
        ApexPages.StandardController stdContNew9 = new ApexPages.StandardController(leadinfoNew9);
        VFC04_CustomerActivity_v2 customerNew9 = new VFC04_CustomerActivity_v2(stdContNew9);
        
        Test.stopTest();
    }
    static testMethod void VFC04_CustomerActivity_Test3() {
        insertSobjects();
        Test.startTest();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        //Test.stopTest();
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[10].Id );
        Lead leadinfo10 = new Lead();
        ApexPages.StandardController stdCont10 = new ApexPages.StandardController(leadinfo10);
        VFC04_CustomerActivity_v2 customer10 = new VFC04_CustomerActivity_v2(stdCont10);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[10].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[10].Id );
        Lead leadinfoNew10 = new Lead();
        ApexPages.StandardController stdContNew10 = new ApexPages.StandardController(leadinfoNew10);
        VFC04_CustomerActivity_v2 customerNew10 = new VFC04_CustomerActivity_v2(stdContNew10);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[11].Id );
        Lead leadinfo11 = new Lead();
        ApexPages.StandardController stdCont11 = new ApexPages.StandardController(leadinfo11);
        VFC04_CustomerActivity_v2 customer11 = new VFC04_CustomerActivity_v2(stdCont11);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[11].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[11].Id );
        Lead leadinfoNew11 = new Lead();
        ApexPages.StandardController stdContNew11 = new ApexPages.StandardController(leadinfoNew11);
        VFC04_CustomerActivity_v2 customerNew11 = new VFC04_CustomerActivity_v2(stdContNew11);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[12].Id );
        Lead leadinfo12 = new Lead();
        ApexPages.StandardController stdCont12 = new ApexPages.StandardController(leadinfo12);
        VFC04_CustomerActivity_v2 customer12 = new VFC04_CustomerActivity_v2(stdCont12);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[12].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[12].Id );
        Lead leadinfoNew12 = new Lead();
        ApexPages.StandardController stdContNew12 = new ApexPages.StandardController(leadinfoNew12);
        VFC04_CustomerActivity_v2 customerNew12 = new VFC04_CustomerActivity_v2(stdContNew12);
        
        Test.stopTest();
    }
    static testMethod void VFC04_CustomerActivity_Test4() {
        insertSobjects();
        Test.startTest();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        //Test.stopTest();
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[13].Id );
        Lead leadinfo13 = new Lead();
        ApexPages.StandardController stdCont13 = new ApexPages.StandardController(leadinfo13);
        VFC04_CustomerActivity_v2 customer13 = new VFC04_CustomerActivity_v2(stdCont13);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[14].Id );
        Lead leadinfo14 = new Lead();
        ApexPages.StandardController stdCont14 = new ApexPages.StandardController(leadinfo14);
        VFC04_CustomerActivity_v2 customer14 = new VFC04_CustomerActivity_v2(stdCont14);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[14].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[14].Id );
        Lead leadinfoNew14 = new Lead();
        ApexPages.StandardController stdContNew14 = new ApexPages.StandardController(leadinfoNew14);
        VFC04_CustomerActivity_v2 customerNew14 = new VFC04_CustomerActivity_v2(stdContNew14);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[15].Id );
        Lead leadinfo15 = new Lead();
        ApexPages.StandardController stdCont15 = new ApexPages.StandardController(leadinfo15);
        VFC04_CustomerActivity_v2 customer15 = new VFC04_CustomerActivity_v2(stdCont15);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[15].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[15].Id );
        Lead leadinfoNew15 = new Lead();
        ApexPages.StandardController stdContNew15 = new ApexPages.StandardController(leadinfoNew15);
        VFC04_CustomerActivity_v2 customerNew15 = new VFC04_CustomerActivity_v2(stdContNew15);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[16].Id );
        Lead leadinfo16 = new Lead();
        ApexPages.StandardController stdCont16 = new ApexPages.StandardController(leadinfo16);
        VFC04_CustomerActivity_v2 customer16 = new VFC04_CustomerActivity_v2(stdCont16);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[16].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[16].Id );
        Lead leadinfoNew16 = new Lead();
        ApexPages.StandardController stdContNew16 = new ApexPages.StandardController(leadinfoNew16);
        VFC04_CustomerActivity_v2 customerNew16 = new VFC04_CustomerActivity_v2(stdContNew16);
        
        Test.stopTest();
    }
    
    
    static testMethod void VFC04_CustomerActivity_Test5() {
        insertSobjects();
        Test.startTest();
        List<MLC_Molicar__c> molicar = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        //Test.stopTest();
        system.debug('Lead Info....' +leads);
        System.assert(leads.size() != 0);
        
        Apexpages.currentPage().getParameters().put( 'id', leads[17].Id );
        Lead leadinfo17 = new Lead();
        ApexPages.StandardController stdCont17 = new ApexPages.StandardController(leadinfo17);
        VFC04_CustomerActivity_v2 customer17 = new VFC04_CustomerActivity_v2(stdCont17);
        system.debug('Recommendation value...' +customer17.objCustomerActivityEntityForm.recommendationValue1);
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[17].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[18].Id );
        Lead leadinfoNew17 = new Lead();
        ApexPages.StandardController stdContNew17 = new ApexPages.StandardController(leadinfoNew17);
        VFC04_CustomerActivity_v2 customerNew17 = new VFC04_CustomerActivity_v2(stdContNew17);
        
        Test.stopTest();
    }
    
    
    static testMethod void VFC04_CustomerActivity_Test6()
    {
        insertSobjects();
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Test.startTest();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[3].Id );
        Lead leadinfo3 = new Lead();
        ApexPages.StandardController stdCont3 = new ApexPages.StandardController(leadinfo3);
        VFC04_CustomerActivity_v2 customer3 = new VFC04_CustomerActivity_v2(stdCont3);
        
        VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertTransactionObjecFortLead( leads[3].Id );
        
        Apexpages.currentPage().getParameters().put( 'id', leads[3].Id );
        Lead leadinfoNew3 = new Lead();
        ApexPages.StandardController stdContNew3 = new ApexPages.StandardController(leadinfoNew3);
        VFC04_CustomerActivity_v2 customerNew3 = new VFC04_CustomerActivity_v2(stdContNew3);
        
        List<SelectOption> test01 = customerNew3.getLeadState();
        
        customerNew3.getRecommendationByVehicleofInterest();
        customerNew3.getRecommendationByCurrentMilage();
        //customerNew3.getRecommendationByCurrentVehicle();
        
        Test.stopTest();
    }
    
    
    static testMethod void VFC04_CustomerActivity_TestNEW()
    {
        List<Lead> leads = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToLeadObjects();
        
        Test.startTest();
        
        Apexpages.currentPage().getParameters().put( 'id', leads[3].Id );
        Lead leadinfo3 = new Lead();
        ApexPages.StandardController stdCont3 = new ApexPages.StandardController(leadinfo3);
        VFC04_CustomerActivity_v2 customer3 = new VFC04_CustomerActivity_v2(stdCont3);
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account dealer = moc.criaAccountDealer();
        dealer.IDBIR__c = '1000';
        insert dealer;
        
        Account customer = moc.criaPersAccount();
        customer.CustomerIdentificationNbr__c = '23584102502';
        customer.Sex__c = 'Woman';
        insert customer;
        
        Opportunity opp = moc.criaOpportunity();
        opp.AccountId = customer.Id;
        opp.Dealer__c = dealer.Id;
        insert opp;
        
        customer3.mapOpportunity(opp, customer, leads[3], 1000);
            
        Test.stopTest();
    }

}