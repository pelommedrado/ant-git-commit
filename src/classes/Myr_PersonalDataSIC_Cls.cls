/** APEX class Used to manage the call to the SIC WebService (RUBIX PERSONAL DATA) **/
public with sharing class Myr_PersonalDataSIC_Cls {
	
	public class Myr_PersonalDataSIC_Cls_Exception extends Exception {}
	
	public Myr_PersonalDataSIC41_WS.CrmGetCustData sicDataSource;
	public Myr_PersonalDataSIC41_WS.GetCustDataRequest request;
	
	/** Format of the date expected for BCS is YYYY-MM-DD
  		@return Date parsed from the entry
  	**/
  	private static Date parseSICDates(String iDate, String iInfoDate) {
  		try {
			String[] myDate = iDate.split('-');
			Date newDate = Date.newinstance(Integer.valueOf(myDate[0]), Integer.valueOf(myDate[1]), Integer.valueOf(myDate[2]));
			return newDate;	
		} catch (Exception e) {
			//continue without interrupting the treatment
			system.debug('### Myr_PersonalDataSIC_Cls - <mapPersonalDataFields> - '+iInfoDate+' conversion problem: ' + e.getMessage());
		}
		return null;
  	}
	
	//default constructor
	public Myr_PersonalDataSIC_Cls(){}
 
	/** CALL The SIC Personal Data WebService and @return a Myr_PersonalDataResponse_Cls response **/
	public Myr_PersonalDataResponse_Cls getPersonaData_SIC(Myr_PersonalData_WS.RequestParameters iParams) {
		Myr_PersonalDataSIC41_WS.GetCustDataResponse sicResponse = getSICData(iParams);
		Myr_PersonalDataResponse_Cls response = mapPersonalDataFields(sicResponse);
		return response;
	} 

	/** CALL The SIC Personal Data WebService and @return a Myr_PersonalData_SIC_WS.GetCustDataResponse response **/
	private Myr_PersonalDataSIC41_WS.GetCustDataResponse getSICData(Myr_PersonalData_WS.RequestParameters iParams) {
		//check values
		if( String.isBlank(iParams.vin) && String.isBlank(iParams.Registration) && String.isBlank(iParams.ident1) 
			&& String.isBlank(iParams.idClient) && String.isBlank(iParams.email) 
			&& ( (String.isBlank(iParams.lastname) && String.isBlank(iParams.zip)) || (String.isBlank(iParams.lastname) && String.isBlank(iParams.city) ) ) ) {
				throw new Myr_PersonalDataSIC_Cls_Exception('The minimum parameters for RBX should one of the following combinations: vin or registration or ident1 or idClient or email or lastname + city or lastname + zip');
		}
		//Prepare the header
		sicDataSource = new Myr_PersonalDataSIC41_WS.CrmGetCustData();
		sicDataSource.endpoint_x = System.label.RbxWsUrl;
		sicDataSource.clientCertName_x = System.label.clientcertnamex;
		sicDataSource.timeout_x = Integer.valueOf(CS04_MYR_Settings__c.getInstance().Myr_PersonalData_Timeout__c);
		sicDataSource.inputHttpHeaders_x = new Map<String, String>();
		sicDataSource.inputHttpHeaders_x.put('Content-Type', 'application/soap+xml; charset=utf-8');
		sicDataSource.inputHttpHeaders_x.put('Keep-Alive', '1115');
		//Prepare the request parameters: 
		Myr_PersonalDataSIC41_WS.CustDataRequest custDataRequest = new Myr_PersonalDataSIC41_WS.CustDataRequest();
		if( iParams.mode != null ) {
			custDataRequest.mode = iParams.mode.name().right(1);
		} else {
			custDataRequest.mode = '7'; //by default: Rforce mode
		}
        custDataRequest.country = iParams.country;
        custDataRequest.brand = iParams.brand;
        custDataRequest.demander = iParams.demander; //Default for Salesforce
        custDataRequest.vin = iParams.vin;
        custDataRequest.lastName = iParams.LastName;
        custDataRequest.firstName = iParams.firstName;
        custDataRequest.city = iParams.City;
        custDataRequest.zip = iParams.Zip;
        custDataRequest.ident1 = (iParams.ident1 == null ) ? '' : iParams.ident1;		//Customer Id (e.g. Italy: fiscal code)
        custDataRequest.idClient = (iParams.idClient == null ) ? '' : iParams.idClient; //Id Rubix (SIC)
        custDataRequest.typeperson = '';
        custDataRequest.idMyr = '';
        custDataRequest.firstRegistrationDate = '';
        custDataRequest.registration = iParams.Registration;
        custDataRequest.email = iParams.email;
        custDataRequest.sinceDate = '';
        custDataRequest.ownedVehicles = '';
        custDataRequest.nbReplies = (iParams.nbReplies == null) ? '' : String.valueOf(iParams.nbReplies);
		Myr_PersonalDataSIC41_WS.ServicePreferences servicesPrefs = new Myr_PersonalDataSIC41_WS.ServicePreferences();
		servicesPrefs.irn = '?';
        servicesPrefs.sia = '?';
        servicesPrefs.reqid = '?';
        servicesPrefs.userid = '?';
        servicesPrefs.language = '?';
        servicesPrefs.country = '?';
        request = new Myr_PersonalDataSIC41_WS.GetCustDataRequest();
		request.servicePrefs =  servicesPrefs;
		request.custDataRequest = custDataRequest;
		//Call SIC(RBX)
		
		sicDataSource.inputHttpHeaders_x.put('Content-Length', String.ValueOf(request.toString().length()));
		
		Myr_PersonalDataSIC41_WS.GetCustDataResponse responseSIC = null;
		if( Test.isRunningTest() ) {
			system.debug('### Myr_PersonalDataSIC_Cls - <getPersonaData_SIC> - RBX is running test');
			//CHECK COUNTRY CODE
			String countryCode = custDataRequest.country;
			List<Country_Info__c> countryInfo = [SELECT Id, Name FROM Country_Info__c WHERE Country_Code_3L__c = :countryCode];
			system.debug('### Myr_PersonalDataSIC_Cls - Nb of Countries found: CountryCode=' + countryCode + ', countries=' + countryInfo);
			system.assertEquals( 1, countryInfo.size());
			//CHECK COUNTRY CODE
			Myr_PersonalDataSIC41_WS_MK mock = new Myr_PersonalDataSIC41_WS_MK();
			//Prepare the workaround due to the bug System.CalloutException: You have uncommitted work pending. Please commit or rollback before calling out in Winter16
	    	//concatenate the fieldsinto the test dedicated fields
	    	//impacted classes are SIC MDM and BCS
	    	//Order is : lastname;firstname;email;custIdentNumber;purchaseNature;vehicleenddaterelation
	    	CS04_MYR_Settings__c myrSettings = CS04_MYR_Settings__c.getInstance();
	    	if( !String.isBlank(myrSettings.MYR_Test_DataSource__c) ) {
		    	List<String> params = myrSettings.MYR_Test_DataSource__c.split(';');
				mock.sicResponse.response.clientList[0].LastName = (params.size() > 0) ? params[0] : '';
				mock.sicResponse.response.clientList[0].FirstName = (params.size() > 1) ? params[1] : '';
				mock.sicResponse.response.clientList[0].contact.email = (params.size() > 2) ? params[2] : '';
				if( params.size() > 3 && !String.isBlank(params[3])) {
					mock.sicResponse.response.clientList[0].ident1 = params[3];	
				}
				if( params.size() > 4 && !String.isBlank(params[4])) {
					mock.sicResponse.response.clientList[0].vehicleList[0].purchaseNature = params[4];	
				}
				if( params.size() > 5 && !String.isBlank(params[5])) {
					mock.sicResponse.response.clientList[0].vehicleList[0].possessionEnd = params[5];	
				}
	    	}
			responseSIC = mock.sicResponse.response;
		} else {
			responseSIC = sicDataSource.getCustData(request);
		}
		
		return responseSIC;
	} 
	
	/** Map the SIC response into a PersonalData response **/
	private Myr_PersonalDataResponse_Cls mapPersonalDataFields(Myr_PersonalDataSIC41_WS.GetCustDataResponse iSICResponse) {
		Myr_PersonalDataResponse_Cls pDataResponse = new Myr_PersonalDataResponse_Cls();
		pDataResponse.response.dataSource = Myr_PersonalData_WS.DataSourceType.SIC;
		if( iSICResponse == null ) return null;
		if( iSICResponse.clientList != null ) {
			for( Myr_PersonalDataSIC41_WS.PersonnalInformation sicClient : iSICResponse.clientList ) {
				//******** customer ids ******************************************************
				Myr_PersonalDataResponse_Cls.InfoClient infoClient = new Myr_PersonalDataResponse_Cls.InfoClient();
				infoClient.IdClient = (sicClient.idClient!=null)?sicClient.idClient.trim():'';
				infoClient.CustIdentNumber = sicClient.ident1; //customer identification number like fiscal code for Italy
				infoClient.CustIdentNumber2 = sicClient.ident2; //customer identification number like fiscal code for Italy
	        	infoClient.strPartyID = '';					//MDM
	        	infoClient.strDOB = '';						//MDM
	        	infoClient.strAccoutType = '';				//MDM
	        	//******** person ************************************************************
	        	infoClient.LastName = sicClient.lastName;
	        	if( !String.isBlank(sicClient.title) ) {
	            	if( '1'.equalsIgnoreCase(sicClient.title) ) {
	            		infoClient.Title = 'Mr.';
	            	} else if( '2'.equalsIgnoreCase(sicClient.title) ) {
	            		infoClient.Title = 'Mrs.';
	            	} else if( '3'.equalsIgnoreCase(sicClient.title) ) {
	            		infoClient.Title = 'Mrs, Mr';
	            	}
	            }
	        	infoClient.Lang = sicClient.lang;
	        	infoClient.MiddleName = sicClient.middleName; 
	        	infoClient.FirstName = sicClient.firstName;
	        	infoClient.typeperson = sicClient.typeperson;
	        	if( !String.isBlank(sicClient.sex) ) {
	            	if( '1'.equalsIgnoreCase(sicClient.sex) ) {
	            		infoClient.Sex = 'Man';
	            	} else if( '2'.equalsIgnoreCase(sicClient.sex) ) {
	            		infoClient.Sex = 'Woman';
	            	}
	            } 
	        	infoClient.DateOfBirth = parseSICDates(sicClient.birthDay, 'Birth Day');
	        	//******** communication agreement ********************************************
	        	infoClient.GlobalCommAgreement = '';
	        	infoClient.PostCommAgreement = '';
	        	infoClient.TelCommAgreemen = '';
	        	infoClient.SMSCommAgreement = '';
	        	infoClient.FaxCommAgreement = '';
	        	infoClient.EmailCommAgreement = '';
	        	//******** phone numbers ******************************************************
	        	if( sicClient.contact != null ) {
	        		infoClient.LandLine1 = ((sicClient.contact.phoneCode1 == null) ? '' : sicClient.contact.phoneCode1) 
	        			+ ((sicClient.contact.phoneNum1 == null) ? '' : sicClient.contact.phoneNum1); 
		        	infoClient.LandLine2 = ((sicClient.contact.phoneCode2 == null) ? '' : sicClient.contact.phoneCode2) 
	        			+ ((sicClient.contact.phoneNum2 == null) ? '' : sicClient.contact.phoneNum2);
	        		infoClient.MobilePhone1 = ((sicClient.contact.phoneCode3 == null) ? '' : sicClient.contact.phoneCode3) 
	        			+ ((sicClient.contact.phoneNum3 == null) ? '' : sicClient.contact.phoneNum3);		        	
		        	infoClient.email = sicClient.contact.email;
	        	}
	        	//******** address ******************************************************
	        	if( sicClient.address != null ) {			
		        	infoClient.StrName = sicClient.address.strName;					//MDM: strAddressLine
		        	infoClient.StrNum = sicClient.address.strNum;
		        	infoClient.StrType = sicClient.address.strType;				
		        	infoClient.StrCompl1 = sicClient.address.compl1;
		        	infoClient.StrCompl2 = sicClient.address.compl2;
		        	infoClient.StrCompl3 = sicClient.address.compl3;
		        	infoClient.CountryCode = sicClient.address.countryCode;
		        	infoClient.Zip = sicClient.address.zip;
		        	infoClient.City = sicClient.address.city;
		        	infoClient.AreaCode = sicClient.address.areaLabel;
	        	}
	        	//******** dealers  *****************************************************
	        	infoClient.dealers = new List<Myr_PersonalDataResponse_Cls.Dealer>();
	        	if( sicClient.dealerList != null ) { 
	        		for( Myr_PersonalDataSIC41_WS.Dealer dealer : sicClient.dealerList) {
	        			if( dealer.birId != null ) { 
	        				try {
	        					infoClient.dealers.add( new Myr_PersonalDataResponse_Cls.Dealer(dealer.birId[0]));
	        				} catch (Exception e) {
	        					//continue without interrupting the treatment
	        					system.debug('### Myr_PersonalDataSIC_Cls - <mapPersonalDataFields> - Dealer problem: ' + e.getMessage());
	        				}
	        			}
	        		}
	        	}
				//******** vehicle informations ******************************************
				infoClient.vcle = new List<Myr_PersonalDataResponse_Cls.Vehicle>();
				for( Myr_PersonalDataSIC41_WS.Vehicle sicVehicle : sicClient.vehicleList ) {
					Myr_PersonalDataResponse_Cls.Vehicle vehicle = new Myr_PersonalDataResponse_Cls.Vehicle();
					vehicle.vin = sicVehicle.vin;
		          	vehicle.IdClient = sicClient.idClient;
		          	vehicle.brandCode = sicVehicle.brandCode;
			        vehicle.modelCode = sicVehicle.modelCode;
			        vehicle.modelLabel = sicVehicle.modelLabel;
			        vehicle.versionLabel = sicVehicle.versionLabel;
			        vehicle.registration = sicVehicle.registration;
			        vehicle.vehicleType = '';
			        vehicle.VNVO_NewVehicle = null;
			        if( !String.isBlank(sicVehicle.new_x) ) {
		            	if( sicVehicle.new_x.equalsIgnoreCase('Y')) {
		              		vehicle.VNVO_NewVehicle = true;
		              	} else if( sicVehicle.new_x.equalsIgnoreCase('N') ) {
		              		vehicle.VNVO_NewVehicle = false;
		              	} 
		            }
			        vehicle.TypeRelationship = sicVehicle.purchaseNature;

			        // ---- Management of the dates
			        vehicle.firstRegistrationDate = parseSICDates(sicVehicle.firstRegistrationDate, 'First Registration Date');
			        vehicle.lastRegistrationDate = parseSICDates(sicVehicle.registrationDate, 'Last Registration Date');
			        vehicle.TechnicalControlDate = parseSICDates(sicVehicle.technicalInspectionDate, 'Technical Control Date');
			        vehicle.possessionBegin = parseSICDates(sicVehicle.possessionBegin, 'Possession Begin');
			        vehicle.possessionEnd = parseSICDates(sicVehicle.possessionEnd, 'Possession End');
			        
				    vehicle.startDate = '';					//MDM
				    vehicle.endDate = '';					//MDM
				    vehicle.firstName = '';					//MDM
				    vehicle.lastName = '';	 				//MDM
				    infoClient.vcle.add(vehicle);
				}
				pDataResponse.response.infoClients.add(infoClient);
				pDataResponse.response.rawResponse = iSICResponse.toString();
				pDataResponse.response.rawQuery = (sicDataSource == null) ? 'Mock mode = no query' : sicDataSource.toString() + ' ------- ' + request.toString();
			}
		}
		return pDataResponse;
	}
}