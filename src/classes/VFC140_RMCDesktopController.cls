public with sharing class VFC140_RMCDesktopController {
    
    transient public List< Opportunity > oppList { get;set; }
    transient public VFP20_DesktopMessages__c messages { get;set; }
    transient public Map<String, Integer> oppSumMap { get;set; }
    
    public Integer oppSumMapSize {
        get {
            return oppSumMap != null ? oppSumMap.size() : 0;
        }
    }
    
    transient public String region { get;set; }
    transient public Boolean isReceptionist { get;set; }
    transient public Boolean isSeller { get;set; }
    transient public Boolean isManager { get;set; }
    //Painel de tarefas
    
    public String taskPrefix {
        get {
            return Task.SObjectType.getDescribe().getKeyPrefix();
        }
    }
    
    public String welcomeMessage{
        get {
            try {
                return (String)messages.get( 'Welcome_message_' + region + '_' + Userinfo.getLanguage() + '__c' );
            } catch( Exception e ) {
                return messages.Welcome_message_R1_pt_BR__c;
            }
        }
    }
    
    transient public list<Task> lateTask { get;set; }
    transient public list<Task> todayTask { get;set; }
    transient public list<Task> next7Days { get;set; }
    transient public list<Task> allOpenTask { get;set; }
    
    transient private final static String TASK_QUERY = 
        'select id, ActivityDate, Subject, What.name, Owner.name from Task where Status <> \'Completed\' and ';
    
    public VFC140_RMCDesktopController () {
        Set<String> permissionByUserList = new Set<String>(RenaultMaisCliente_Utils.permissionSetAvailable());
        isReceptionist = isSeller = isManager = false;
        
        String profileName = [
            select Name from Profile where Id = :Userinfo.getProfileId() limit 1].Name;
        
        if( profileName == 'SFA - Receptionist' || (!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Hostess'))){
            isReceptionist = true;
            
        } else if( profileName == 'SFA - Seller' ||(!permissionByUserList.isEmpty() && permissionByUserList.contains('BR_SFA_Seller'))){
            isSeller = true;
            
        } else if( profileName == 'SFA - Dealer VN Manager' )
            isManager = true;
        
        System.assert( isReceptionist || isSeller || isManager, Label.VFP20_Unauthorized_access);
        
        oppList = isSeller ? [
            select Id, Name, OpportunitySource__c, OpportunitySubSource__c, Campaign.Name, Campaign_Name__c , CreatedDate
            from Opportunity
            where StageName = 'Identified'
            and OwnerId = :Userinfo.getUserId()
            and (OpportunitySource__c <> 'NETWORK' AND OpportunitySource__c <> 'DEALER')
            order by CreatedDate asc
        ] : [
            select Id, Name, OpportunitySource__c, OpportunitySubSource__c, Account.Name, CreatedDate
            from Opportunity
            where OwnerId = :Userinfo.getUserId() and StageName <> 'Lost'
            order by CreatedDate asc
        ];
            
            oppSumMap = new Map< String, Integer >();
        
        if( isSeller || isManager ){
            List< AggregateResult > aggResList = [
                select count(Id) oppCount, StageName
                from Opportunity
                where OwnerId = : Userinfo.getUserId()
                and (
                    StageName in ('Identified', 'Test Drive', 'Quote', 'In Attendance') or
                    (StageName in ('Order', 'Billed', 'Lost') and LastModifiedDate = THIS_MONTH)
                )
                group by StageName
            ];
            
            Map< String, Integer > stageNameMap = new Map< String, Integer >();
            for( AggregateResult aggRes : aggResList )
                stageNameMap.put( (String)aggRes.get( 'StageName' ), (Integer)aggRes.get( 'oppCount' ) );
            
            oppSumMap.put( 'Identified', zeroOrValue( stageNameMap.get( 'Identified' ) ) );
            oppSumMap.put( 'Test Drive / Quote', 
                          zeroOrValue( stageNameMap.get( 'Test Drive' ) ) + 
                          zeroOrValue( stageNameMap.get( 'Quote' ) ) + 
                          zeroOrValue( stageNameMap.get( 'In Attendance' ) ));
            
            oppSumMap.put( 'Order', zeroOrValue( stageNameMap.get( 'Order' ) ) );
            oppSumMap.put( 'Billed', zeroOrValue( stageNameMap.get( 'Billed' ) ) );
            oppSumMap.put( 'Lost', zeroOrValue( stageNameMap.get( 'Lost' ) ) );
            
        } else {
            oppSumMap.put( 'Today', [select count() from Opportunity where CreatedDate = TODAY] );
            oppSumMap.put( 'Yesterday', [select count() from Opportunity where CreatedDate = YESTERDAY] );
            oppSumMap.put( 'Week', [select count() from Opportunity where CreatedDate = THIS_WEEK] );
            oppSumMap.put( 'Month', [select count() from Opportunity where CreatedDate = THIS_MONTH] );
            oppSumMap.put( 'Mine', oppList.size() );
        }
        
        try {
            User currUser =
                [select Contact.Account.NameZone__c from User where Id = :Userinfo.getUserId()];
            
            region = currUser.Contact.Account.NameZone__c;
            
        } catch( Exception e ) {
            region = 'R1';
        }
        
        String profileNameToQuery = (isSeller ? 'SFA - Seller' : isReceptionist ? 'SFA - Receptionist' : '');
        
        if(String.isNotBlank(profileNameToQuery) && 
           RenaultMaisCliente_Utils.getProfileMap(profileNameToQuery).containsKey(profileNameToQuery)){
               messages = VFP20_DesktopMessages__c.getInstance(  RenaultMaisCliente_Utils.getProfileMap(profileNameToQuery).get(profileNameToQuery).Id );
           } else   
               messages = VFP20_DesktopMessages__c.getInstance( Userinfo.getProfileId() );
        
        //Painel de tarefas
        lateTask = new list<Task>();
        todayTask = new list<Task>();
        next7Days = new list<Task>();
        allOpenTask = new list<Task>();
        
        loadTasks( 'ActivityDate < Today', lateTask);
        loadTasks('ActivityDate = Today',todayTask);
        loadTasks('ActivityDate > Today AND ActivityDate <= NEXT_WEEK',next7Days);
        loadTasks('isClosed=false',allOpenTask);
    }
    
    private void loadTasks (String condition, list<Task> taskList) {
        taskList.addAll( 
            (list<Task>) database.query(TASK_QUERY + 'Owner.id=\'' + Userinfo.getUserId() +'\' and ' + condition + ' order by ActivityDate asc') );
    }
    
    private Integer zeroOrValue (Integer value) {
        return value != null ? value : 0;
    }
}