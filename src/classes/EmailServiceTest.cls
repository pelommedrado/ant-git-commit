@isTest
private class EmailServiceTest {
	
    private static String body = 
        'Nome: Teste\n' 							+
        'Sobrenome: Classe\n' 						+ 
        'E-mail: teste@gmail.com\n' 				+ 
        'Telefone: (11)5656-7988\n' 				+ 
        'Celular: (11)7999-6666\n'  				+
        'Cidade: São Paulo\n'						+
		'Estado: SP\n'								+
		'Concessionária de Interesse: 123456\n' 	+
		'Modelo de Interesse: Sandero\n'			+
		'CPF: 84777848680\n'						+
        'Desejo receber informações por SMS:Y\n'	+
        'Desejo receber informações por E-mail:Y\n' +
        'Detalhe: Detalhe\n'						+
		'Sub Detalhe: Sub detalhe\n'				+
        'Id:\n'										+
        'Origem: PHONE\n'							+
        'Sub Origem: INTERNET\n'					+
    	'Observação: "kolekto\n\n\n\n kolekto "\n ';

   
    static {
        criarConta();
    }
    
    static testMethod void deveExtrairInfo() { 
        EmailService email = new EmailService();
        Map<String, String> mapInfo = email.extrairInfo(body);
        System.assertEquals(mapInfo.size(), 16);
    }
    
    static testMethod void deveIsValidCampos() {
        EmailService email = new EmailService();
        Map<String, String> mapInfo = email.extrairInfo(body);
        System.assertEquals(email.isValidCampos(mapInfo), true);
    }
    
    static testMethod void deveIsValidCamposSemNome() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        mapInfo.remove(EmailService.RX_NOME);
        
        System.assertEquals(email.isValidCampos(mapInfo), false);
    }
    
    static testMethod void deveIsValidCamposSemSobrenome() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        mapInfo.remove(EmailService.RX_SOBRENOME);
        
        System.assertEquals(email.isValidCampos(mapInfo), false);
    }
    
    static testMethod void deveIsValidCamposSemContato() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        mapInfo.remove(EmailService.RX_EMAIL);
        mapInfo.remove(EmailService.RX_CELULAR);
        mapInfo.remove(EmailService.RX_TELEFONE);
        
        System.assertEquals(email.isValidCampos(mapInfo), false);
    }
    
    static testMethod void deveCriarLead() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        
        Lead lead = email.criarLead(mapInfo);
        System.assertEquals(lead != null, true);
    }
    
    static testMethod void deveSalvarLead() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        
        email.salvarLead(mapInfo, body);
    }
    
    static testMethod void deveSalvarLeadUserFila() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        mapInfo.remove(EmailService.RX_CONCESSIO);
        
        email.salvarLead(mapInfo, body);
    }
    
    static testMethod void deveCriarLeadIncompleto() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        
        Incomplete_Lead__c lead = email.criarLeadIncompleto(mapInfo);
        
        System.assertEquals(lead != null, true);
    }
    
    static testMethod void deveSalvarLeadIncompleto() {
        EmailService email = new EmailService();
        
        Map<String, String> mapInfo = email.extrairInfo(body);
        
        email.salvarIncomLead(mapInfo, body);
    }
    
    static void criarConta() {
        
        Account Acc = new Account(Name='Test1',
                                  IDBIR__c = '123456',
                                  Phone='0000',
                                  RecordTypeId= Utils.getRecordTypeId('Account','Network_Site_Acc'), 
                                  ProfEmailAddress__c = 'addr1@mail.com', 
                                  ShippingCity = 'Paris', 
                                  ShippingCountry = 'France', 
                                  ShippingState = 'IDF', 
                                  ShippingPostalCode = '75013', 
                                  ShippingStreet = 'my street');
        insert Acc;
    }
}