@isTest
public class BatchInterfaceFullStockDMSTest {
    
    public static testMethod void testaBatch(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        Account acc = moc.criaAccountDealer();
        insert acc;
        
        VEH_Veh__c v = moc.criaVeiculo();
        v.Status__c = 'Booking';
        v.Name = '12345678912458966';
        v.Received_By_Stock_Full__c = true;
        insert v;
        
        VEH_Veh__c v1 = moc.criaVeiculo();
        v1.Status__c = 'Available';
        v1.Received_By_Stock_Full__c = false;
        insert v1;
        
        VRE_VehRel__c vr1 = moc.criaVeiculoRelacionado();
        vr1.Account__c = acc.Id;
        vr1.VIN__c = v1.Id;
        insert vr1;
        
        test.startTest();
        /*BatchInterfaceFullStockDMS b = new BatchInterfaceFullStockDMS();
        Database.executeBatch(b);*/
        String jobId = System.schedule('ScheduleApexClassTest', '0 0 1 ? * *', new ScheduleInterfaceFullStockDMS());
        BatchInterfaceFullRecievedToFalse b = new BatchInterfaceFullRecievedToFalse();
        Database.executeBatch(b);
        test.stopTest();
        
    }

}