/**
	Class   -   VFC11_MolicarDAO_Test
    Author  -   RameshPrabu
    Date    -   05/09/2012
    
    #01 <RameshPrabu> <05/09/2012>
        Created this class using test for VFC11_MolicarDAO.
**/
@isTest
public with sharing class VFC11_MolicarDAO_Test {
	
	static testMethod void VFC11_MolicarDAO_Test() {
        // TO DO: implement unit test
        
        String brand = '';
        String model = '';
        String molicarId = '';
        List<MLC_Molicar__c> lstMolicarBrandInsert = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordToMolicarObject();
        VFC11_MolicarDAO.getInstance().findMolicarById(molicarId );
        VFC11_MolicarDAO.getInstance().findMolicarByBrand(brand );
        VFC11_MolicarDAO.getInstance().findMolicarByBrandAndModel(brand, model);
        Test.startTest();
        List<AggregateResult> lstMolicarBrand = VFC11_MolicarDAO.getInstance().findMolicarBrand();
        Test.stopTest();
        System.assert(lstMolicarBrand.size() > 0);
    }
}