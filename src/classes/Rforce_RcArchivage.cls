public class Rforce_RcArchivage {
    public class getCaseIdListResponse {
        public Rforce_RcArchivage.caseId[] caseIdList;
        private String[] caseIdList_type_info = new String[]{'caseIdList','http://service.rc.ccb.renault.com/','caseId','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'caseIdList'};
    }      
    public class getCaseIdListResponse1 {
        public Rforce_RcArchivage.caseId[] caseIdList;
        private String[] caseIdList_type_info = new String[]{'caseIdList','http://service.rc.ccb.renault.com/','caseId','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'caseIdList'};
    }
    public class getCaseDetails {
        public String countryCode;
        public String caseId;
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] caseId_type_info = new String[]{'caseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'countryCode','caseId'};
    }
    public class getAttachmentContentResponse {
        public String attachmentContent;
        private String[] attachmentContent_type_info = new String[]{'attachmentContent','http://www.w3.org/2001/XMLSchema','base64Binary','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'attachmentContent'};
    }
    public class getXmlDetailsResponse {
        public String xmlData;
        private String[] xmlData_type_info = new String[]{'xmlData','http://www.w3.org/2001/XMLSchema','base64Binary','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'xmlData'};
    }
    public class getAttachmentDetailsListResponse {
        public Rforce_RcArchivage.attachmentDetailsList[] attachmentDetails;
        private String[] attachmentDetails_type_info = new String[]{'attachmentDetails','http://service.rc.ccb.renault.com/','attachmentDetailsList','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'attachmentDetails'};
    }
    public class attachmentDetailsList {
        public String attachmentName;
        public DateTime createdDate;
        public String extension;
        private String[] attachmentName_type_info = new String[]{'attachmentName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] createdDate_type_info = new String[]{'createdDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] extension_type_info = new String[]{'extension','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'attachmentName','createdDate','extension'};
    }
    public class GetRCDataError {
        public String fault;
        private String[] fault_type_info = new String[]{'fault','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'fault'};
    }
    public class caseDetails {
        public DateTime activityCloseDate;
        public String accountId;
        public String assetId;
        public String brandInfoExchangeFlag;
        public String buId;
        public String build;
        public String city;
        public String clientAgreement;
        public String conflictId;
        public String contactCellPhNum;
        public String contactHomePhNum;
        public String contactId;
        public String contactWorkPhNum;
        public String country;
        public DateTime created;
        public String createdBy;
        public DateTime createdDate;
        public String descText;
        public String detail;
        public String firstName;
        public String from_x;
        public String lastName;
        public DateTime lastUpdated;
        public String lastUpdatedBy;
        public String login;
        public String modificationNumber;
        public String name;
        public String negoResult;
        public String orgId;
        public DateTime purchaseDate;
        public String purchaseIntention;
        public String quoteType;
        public String rcName;
        public String rcRowId;
        public String resAuthor;
        public DateTime resDate;
        public String rowId;
        public String serialNum;
        public String srArea;
        public String srAreaCode;
        public String srBrand;
        public String srCustNum;
        public String srNum;
        public String srSubtypeCode;
        public Integer state;
        public String subArea;
        public String traitment;
        public String zipCode;
        private String[] activityCloseDate_type_info = new String[]{'activityCloseDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] accountId_type_info = new String[]{'accountId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] assetId_type_info = new String[]{'assetId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] brandInfoExchangeFlag_type_info = new String[]{'brandInfoExchangeFlag','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] buId_type_info = new String[]{'buId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] build_type_info = new String[]{'build','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] clientAgreement_type_info = new String[]{'clientAgreement','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] conflictId_type_info = new String[]{'conflictId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] contactCellPhNum_type_info = new String[]{'contactCellPhNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] contactHomePhNum_type_info = new String[]{'contactHomePhNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] contactId_type_info = new String[]{'contactId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] contactWorkPhNum_type_info = new String[]{'contactWorkPhNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] created_type_info = new String[]{'created','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] createdBy_type_info = new String[]{'createdBy','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] createdDate_type_info = new String[]{'createdDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] descText_type_info = new String[]{'descText','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] detail_type_info = new String[]{'detail','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] from_x_type_info = new String[]{'from','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] lastUpdated_type_info = new String[]{'lastUpdated','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] lastUpdatedBy_type_info = new String[]{'lastUpdatedBy','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] login_type_info = new String[]{'login','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] modificationNumber_type_info = new String[]{'modificationNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] negoResult_type_info = new String[]{'negoResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] orgId_type_info = new String[]{'orgId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] purchaseDate_type_info = new String[]{'purchaseDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] purchaseIntention_type_info = new String[]{'purchaseIntention','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] quoteType_type_info = new String[]{'quoteType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] rcName_type_info = new String[]{'rcName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] rcRowId_type_info = new String[]{'rcRowId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] resAuthor_type_info = new String[]{'resAuthor','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] resDate_type_info = new String[]{'resDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] rowId_type_info = new String[]{'rowId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] serialNum_type_info = new String[]{'serialNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srArea_type_info = new String[]{'srArea','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srAreaCode_type_info = new String[]{'srAreaCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srBrand_type_info = new String[]{'srBrand','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srCustNum_type_info = new String[]{'srCustNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srNum_type_info = new String[]{'srNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] srSubtypeCode_type_info = new String[]{'srSubtypeCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] state_type_info = new String[]{'state','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] subArea_type_info = new String[]{'subArea','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] traitment_type_info = new String[]{'traitment','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] zipCode_type_info = new String[]{'zipCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'activityCloseDate','accountId','assetId','brandInfoExchangeFlag','buId','build','city','clientAgreement','conflictId','contactCellPhNum','contactHomePhNum','contactId','contactWorkPhNum','country','created','createdBy','createdDate','descText','detail','firstName','from_x','lastName','lastUpdated','lastUpdatedBy','login','modificationNumber','name','negoResult','orgId','purchaseDate','purchaseIntention','quoteType','rcName','rcRowId','resAuthor','resDate','rowId','serialNum','srArea','srAreaCode','srBrand','srCustNum','srNum','srSubtypeCode','state','subArea','traitment','zipCode'};
    }
    public class getXmlDetails {
        public String countryCode;
        public String caseId;
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] caseId_type_info = new String[]{'caseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'countryCode','caseId'};
    }     
    public class caseId {
        public String caseId;
        public String countryCode;
        public String lastName;
        public String firstName;
        public String registrationNumber;
        public String applicationId;
        public String buId;
        public String build;
        public String serialNum;
        public DateTime createdDate;
        private String[] caseId_type_info = new String[]{'caseId','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] registrationNumber_type_info = new String[]{'registrationNumber','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] applicationId_type_info = new String[]{'applicationId','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] buId_type_info = new String[]{'buId','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] build_type_info = new String[]{'build','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] serialNum_type_info = new String[]{'serialNum','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] createdDate_type_info = new String[]{'createdDate','http://service.rc.ccb.renault.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'caseId','countryCode','lastName','firstName','registrationNumber','applicationId','buId','build','serialNum','createdDate'};
    }   
    public class caseIdMock {
        public String caseId;
        public String countryCode;
        public String lastName;
        public String firstName;
        public String buId;
        public String build;
        public String serialNum;
        private String[] caseId_type_info = new String[]{'caseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'caseId','countryCode'};
    }
    public class ServiceImplementationPort {
        public String endpoint_x = 'http://localhost:8080/RCSOAP1.1/ServiceInterface';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://service.rc.ccb.renault.com/', 'Rforce_RcArchivage'};
        public Rforce_RcArchivage.attachmentDetailsList[] getAttachmentDetailsList(String countryCode,String caseId) {
            Rforce_RcArchivage.getAttachmentDetailsList request_x = new Rforce_RcArchivage.getAttachmentDetailsList();
            Rforce_RcArchivage.getAttachmentDetailsListResponse response_x;
            request_x.countryCode = countryCode;
            request_x.caseId = caseId;
            Map<String, Rforce_RcArchivage.getAttachmentDetailsListResponse> response_map_x = new Map<String, Rforce_RcArchivage.getAttachmentDetailsListResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getAttachmentDetailsList',
              'http://service.rc.ccb.renault.com/',
              'getAttachmentDetailsListResponse',
              'Rforce_RcArchivage.getAttachmentDetailsListResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.attachmentDetails;
        }
        public Rforce_RcArchivage.caseId[] getCaseIdList(String countryCode,String vinNo) {
            Rforce_RcArchivage.getCaseIdList request_x = new Rforce_RcArchivage.getCaseIdList();
            Rforce_RcArchivage.getCaseIdListResponse response_x;
            request_x.countryCode = countryCode;
            request_x.vinNo = vinNo;
            Map<String, Rforce_RcArchivage.getCaseIdListResponse> response_map_x = new Map<String, Rforce_RcArchivage.getCaseIdListResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getCaseIdList',
              'http://service.rc.ccb.renault.com/',
              'getCaseIdListResponse',
              'Rforce_RcArchivage.getCaseIdListResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.caseIdList;
        }
        public Rforce_RcArchivage.caseId[] getCaseIdListAccount(String LastName,String FirstName,String ZipCode,String City,String PhoneNumber,String BusinessName,String BusinessCustomId,String PersonCustomId) {
            Rforce_RcArchivage.getCaseIdListAccount request_x = new Rforce_RcArchivage.getCaseIdListAccount();
            Rforce_RcArchivage.getCaseIdListResponse1 response_x;
            request_x.lastName = LastName;
            request_x.firstName = FirstName;
            request_x.zipCode = ZipCode;
            request_x.city = City;
            request_x.phoneNumber = PhoneNumber;
            request_x.businessName = BusinessName;
            request_x.businessCustomId = BusinessCustomId;
            request_x.personCustomId = PersonCustomId;
            Map<String, Rforce_RcArchivage.getCaseIdListResponse1> response_map_x = new Map<String, Rforce_RcArchivage.getCaseIdListResponse1>();
            response_map_x.put('response_x', response_x);
            System.debug('Sur Rforce_RcArchivage Avant Invoke L:'+LastName+'-F:'+FirstName+'-Z:'+ZipCode+'-C:'+City+'-P:'+PhoneNumber+'-B:'+BusinessName+'-BusinessCustomId:'+BusinessCustomId+'-PersonCustomId:'+PersonCustomId);
         
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getCaseIdListAccount',
              'http://service.rc.ccb.renault.com/',
              'getCaseIdListResponse1',
              'Rforce_RcArchivage.getCaseIdListResponse1'}
            );
            System.debug('Sur Rforce_RcArchivage Apres L:'+LastName+'-F:'+FirstName+'-Z:'+ZipCode+'-C:'+City+'-P:'+PhoneNumber+'-B:'+BusinessName+'-BusinessCustomId'+BusinessCustomId+'PersonCustomId'+PersonCustomId);
         
            response_x = response_map_x.get('response_x');
            return response_x.caseIdList;
        }
        
        public String getAttachmentContent(String VehicleId,String caseId,String attachmentName) {
            system.debug('## Inside WSDL class :getAttachmentContent ##');
            Rforce_RcArchivage.getAttachmentContent request_x = new Rforce_RcArchivage.getAttachmentContent();
            Rforce_RcArchivage.getAttachmentContentResponse response_x;
            request_x.countryCode = VehicleId;
            request_x.caseId = caseId;
            request_x.attachmentName = attachmentName;
            Map<String, Rforce_RcArchivage.getAttachmentContentResponse> response_map_x = new Map<String, Rforce_RcArchivage.getAttachmentContentResponse>();
            system.debug('## Assigingn Repsone ##');
            system.debug('## response_x is..::' + response_x);
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getAttachmentContent',
              'http://service.rc.ccb.renault.com/',
              'getAttachmentContentResponse',
              'Rforce_RcArchivage.getAttachmentContentResponse'}
            );
            response_x = response_map_x.get('response_x');
            system.debug('## response_x is..::'+ response_x);
            return response_x.attachmentContent;
        }
        public Rforce_RcArchivage.caseDetails getCaseDetails(String countryCode,String caseId) {
            Rforce_RcArchivage.getCaseDetails request_x = new Rforce_RcArchivage.getCaseDetails();
            Rforce_RcArchivage.getCaseDetailsResponse response_x;
            request_x.countryCode = 'RUS';
            request_x.caseId = caseId;
            Map<String, Rforce_RcArchivage.getCaseDetailsResponse> response_map_x = new Map<String, Rforce_RcArchivage.getCaseDetailsResponse>();
            response_map_x.put('response_x', response_x);
            
            System.debug('request_x on caseDetails Webservice >>>>>>>>'+request_x );
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getCaseDetails',
              'http://service.rc.ccb.renault.com/',
              'getCaseDetailsResponse',
              'Rforce_RcArchivage.getCaseDetailsResponse'}
            );
            response_x = response_map_x.get('response_x');
            
            System.debug('response_x on caseDetails Webservice >>>>>>>>'+response_x.caseDetails);
            return response_x.caseDetails;
        }
        public String getXmlDetails(String countryCode,String caseId) {
            Rforce_RcArchivage.getXmlDetails request_x = new Rforce_RcArchivage.getXmlDetails();
            Rforce_RcArchivage.getXmlDetailsResponse response_x;
            request_x.countryCode = countryCode;
            request_x.caseId = caseId;
            Map<String, Rforce_RcArchivage.getXmlDetailsResponse> response_map_x = new Map<String, Rforce_RcArchivage.getXmlDetailsResponse>();
            response_map_x.put('response_x', response_x);
            
             System.debug('request_x on caseDetails XML Details>>>>>>>>'+request_x );
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.rc.ccb.renault.com/',
              'getXmlDetails',
              'http://service.rc.ccb.renault.com/',
              'getXmlDetailsResponse',
              'Rforce_RcArchivage.getXmlDetailsResponse'}
            );
            response_x = response_map_x.get('response_x');
            System.debug('response_x on xmlData Webservice >>>>>>>>'+response_x.xmlData);
            return response_x.xmlData;
        }
    }
    public class getAttachmentContent {
        public String countryCode;
        public String caseId;
        public String attachmentName;
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] caseId_type_info = new String[]{'caseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] attachmentName_type_info = new String[]{'attachmentName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'countryCode','caseId','attachmentName'};
    }
    public class getCaseIdList {
        public String countryCode;
        public String vinNo;
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] vinNo_type_info = new String[]{'vinNo','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'countryCode','vinNo'};
    }
    public class getCaseIdListAccount {
        public String lastName;
        public String firstName;
        public String zipCode;
        public String city;
        public String phoneNumber;
        public String businessName;
        public String businessCustomId;
        public String personCustomId;
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] zipCode_type_info = new String[]{'zipCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] phoneNumber_type_info = new String[]{'phoneNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] businessName_type_info = new String[]{'businessName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] businessCustomId_type_info = new String[]{'businessCustomId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] personCustomId_type_info = new String[]{'personCustomId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'lastName','firstName','zipCode','city','phoneNumber','businessName'};
    }
    public class getCaseDetailsResponse {
        public Rforce_RcArchivage.caseDetails caseDetails;
        private String[] caseDetails_type_info = new String[]{'caseDetails','http://service.rc.ccb.renault.com/','caseDetails','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'caseDetails'};
    }
    public class getAttachmentDetailsList {
        public String countryCode;
        public String caseId;
        private String[] countryCode_type_info = new String[]{'countryCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] caseId_type_info = new String[]{'caseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.rc.ccb.renault.com/','false','false'};
        private String[] field_order_type_info = new String[]{'countryCode','caseId'};
    }
}