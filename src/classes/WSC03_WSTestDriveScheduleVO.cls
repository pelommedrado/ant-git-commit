/**
*   WebService Class    -   WSC03_WSTestDriveScheduleVO
*   Author              -   Suresh Babu
*   Date                -   28/11/2012
*   
*   #01 <Suresh Babu> <28/11/2012>
*       Schedule the Test Drive, after scheduling the status message was displayed via this wrapper class.  
**/
global class WSC03_WSTestDriveScheduleVO {
	webservice String result {get;set;}
	webservice String errorMessage {get;set;}
	
	global WSC03_WSTestDriveScheduleVO(){}
}