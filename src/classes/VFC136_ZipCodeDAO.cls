public with sharing class VFC136_ZipCodeDAO
{
	private static final VFC136_ZipCodeDAO instance = new VFC136_ZipCodeDAO();
    
	private VFC136_ZipCodeDAO(){}

    public static VFC136_ZipCodeDAO getInstance() {
        return instance;
    }

    
	/**
     * Returns a zipCodeBase object given a zip code number.
     */
    public ZipCodeBase__c searchZipCodeByNumber(String zipCodeNumber) {
    	ZipCodeBase__c zipCode = new ZipCodeBase__c(); 
    	try {
    		zipCode = [SELECT Id, Name, Street__c, Numerical_Range__c, Neighborhood__c,
    						  City__c, State__c 
    		            FROM ZipCodeBase__c
    		           WHERE Name = :zipCodeNumber
    		           limit 1];
    	}
    	catch (Exception e) {
    		 system.debug(LoggingLevel.INFO, '*** Zip code not found');
    		 zipCode = null;
    	}
    	return zipCode;
    }
}