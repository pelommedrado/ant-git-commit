public class AgendamentoEditarServicoController {
    
    public Opportunity opp  { get; set; }
    public Account acc      { get;set;  }
    public VEH_Veh__c veiculo       { get;set; }
    public VRE_VehRel__c relVeiculo { get;set; }
    public Event evento             { get;set; }
    
    public List<SelectOption> statusSelect  { get;  set; }
    public List<SelectOption> tecnicoSelect { get; set; }
    public List<SelectOption> tipoRel       { get; set; }
    
    public List<Opportunity> listHist { get;set; }
    public Id accId { get;set; }
    
    public String textSearch        { get;set; }
    public String tipoRelText       { get;set; }
    public String nome              { get;set; }
    public String sobrenome         { get;set; }
    public TabItem tabItem          { get;set; }
    
    public Boolean open             { get;set; }
    
    public AgendamentoEditarServicoController() {
        init();
        
        String oppId = 
            ApexPages.currentPage().getParameters().get('Id');
        
        initDados(oppId);
        
        createTab();
        updateHorario();
        
        System.debug('Opp:' + opp);
        System.debug('Acc:' + acc);
        System.debug('Eve:' + evento);
        System.debug('Vei:' + veiculo);
        System.debug('Rel:' + relVeiculo);
    }
    
    private void init() {
        this.statusSelect = new List<SelectOption>();
        //this.statusSelect.add(new SelectOption('Pre Scheduled', 'Pre Scheduled'));
        this.statusSelect.add(new SelectOption('Scheduled', 'Agendado'));
        this.statusSelect.add(new SelectOption('Performed', 'Realizado'));
        this.statusSelect.add(new SelectOption('Unperformed', 'Não Realizado'));
        
        this.tipoRel = new List<SelectOption>();
        this.tipoRel.add(new SelectOption('Owner', 'Proprietário'));
        this.tipoRel.add(new SelectOption('User', 'Usuário'));
        
        initContato();
        
        this.open = false;
    }
    
    private void initContato() {
        Id contactId = [
            SELECT contactid 
            FROM User 
            WHERE id =: Userinfo.getUserId()].contactId;
        
        accId  = [
            SELECT AccountID 
            FROM Contact 
            WHERE id =: contactId].AccountId;
        
        List<Contact> contactList = [
            SELECT Id, Name
            FROM Contact
            WHERE AccountId =: accID AND Available_to_Service__c = 'Available'
            ORDER BY Name
        ];
        this.tecnicoSelect = new List<SelectOption>();
        for(Contact ct : contactList) {
            this.tecnicoSelect.add(new SelectOption(ct.Id, ct.Name));
        }
    }
    
    private void initDados(String oppId) {
        this.opp = new Opportunity();
        this.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'Service');
        this.acc = new Account();
        this.acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
        
        this.evento = new Event();
        this.listHist = new List<Opportunity>();
        
        if(oppId != null) {
            this.opp = [
                SELECT Id, 
                Service_Protocol__c, 
                ProposalScheduleDate__c,
                ScheduledPeriod__c,
                ProposalScheduleHour__c,
                ServiceType__c,
                OpportunitySource__c,
                OpportunitySubSource__c,
                StageName,
                ResponsibleTechnical__c,
                ScheduledDate__c,
                ScheduledHour__c,
                Description,
                VehicleRegistrNbr__c,
                AccountId,
                ReasonLossCancellation__c
                
                FROM Opportunity
                WHERE Id =: oppId AND RecordTypeId =: Utils.getRecordTypeId('Opportunity', 'Service')
            ];
            
            this.acc = [
                SELECT Id, isPersonAccount, LastName, HomePhone__c,
                FirstName, PersonHomePhone, PersMobPhone__c,
                CustomerIdentificationNbr__c,PersonEmail 
                FROM Account
                WHERE Id =: opp.AccountId
            ];
            
            nome = this.acc.FirstName;
            sobrenome = this.acc.LastName;
            
            List<Event> eventList = [
                SELECT Id, WhatId , WhoId, StartDateTime   
                FROM Event
                WHERE WhatId =: opp.Id
                LIMIT 1
            ];
            
            if(!eventList.isEmpty()) {
                this.evento = eventList.get(0); 
            }
            
            if(opp.StageName == 'Pre Scheduled') {
                opp.StageName = 'Scheduled';
            }
            
        } else {
           this.opp.OpportunitySource__c = 'DEALER';
            
        }
        
        if(!String.isEmpty(opp.VehicleRegistrNbr__c)) {
            
            List<VEH_Veh__c> relList = [
                SELECT Id, VehicleRegistrNbr__c, Name, Model__c, Version__c
                FROM VEH_Veh__c 
                WHERE VehicleRegistrNbr__c =: opp.VehicleRegistrNbr__c
                LIMIT 1
            ];
            
            if(!relList.isEmpty()) {
                this.veiculo = relList.get(0);
                
                List<VRE_VehRel__c> rRelVec = [
                    SELECT Id, TypeRelation__c, VIN__r.Id 
                    FROM VRE_VehRel__c
                    WHERE VIN__r.Id =: this.veiculo.Id AND Account__c =: opp.AccountId
                ];
                
                if(!rRelVec.isEmpty()) {
                    this.relVeiculo = rRelVec.get(0);
                    this.tipoRelText = this.relVeiculo.TypeRelation__c;
                }
            }
            
            listHist = [
                SELECT Id, 
                Service_Protocol__c, 
                ScheduledDate__c,
                ServiceType__c,
                ResponsibleTechnical__r.Name,
                StageName
                FROM Opportunity
                WHERE /*AccountId =: accId AND*/ RecordTypeId =: Utils.getRecordTypeId('Opportunity', 'Service') 
                AND VehicleRegistrNbr__c =: opp.VehicleRegistrNbr__c
            ];
        }
    }
    
    public PageReference cancelar() {
        return Page.AgendamentoServico;  
    }
    
    private Boolean validar() {
        if(String.isBlank(nome) || String.isBlank(sobrenome)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar o Nome e o Sobrenome'));
            return false;  
        }
        
        if(String.isBlank(acc.CustomerIdentificationNbr__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar o CPF'));
             return false; 
        }
           
        if(String.isNotBlank(acc.CustomerIdentificationNbr__c)) {
            if(!Sfa2Utils.isCpfValido(acc.CustomerIdentificationNbr__c) ) {
                Apexpages.addMessage(new Apexpages.Message( 
                    ApexPages.Severity.ERROR, 
                    'É necessário informar CPF valido '));
                
                return false; 
            }
        }
        
        if(String.isBlank(acc.PersonEmail)  && 
           String.isBlank(acc.PersMobPhone__c )     && 
           String.isBlank(acc.PersonHomePhone ) ) {
               ApexPages.addMessage(
                   new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário informar Telefone ou Celular ou Email'));
               
                return false; 
           }
        
        if(String.isEmpty(opp.ResponsibleTechnical__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar um \'Técnico\''));
            return false;  
        }
        
        if(opp.ScheduledDate__c == null) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar um \'Data\''));
             return false; 
        }
        
        if(String.isEmpty(opp.ScheduledHour__c)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'É necessário selecionar um \'Horário\''));
            return false; 
        }
        
        if(veiculo == null) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 'O veículo informado não existe em nossa base. Insira-o para seguir com o atendimento.'));
            return false; 
        }
        
        if(String.isEmpty(tipoRelText)) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 
                                      'É necessário selecionar \'Tipo Relacionamento\'.'));
            return false;
        }
        
        return true;
    }
    
    public PageReference isCancelar() {
        if(opp.StageName == 'Unperformed') {
            open = true;
        } else {
            open = false;
        }
        
        return null;
    }
    
    public PageReference salvarNovo() {
        if(!validar()) {
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            
            Account ac = [
                SELECT Id, IDBIR__c
                FROM Account
                WHERE id =: accId
            ];
            
            Lead lead = new Lead();
            lead.OwnerId = UserInfo.getUserId();
            lead.Status             = 'Identified';
            lead.FirstName          = nome;
            lead.LastName           = sobrenome;
            lead.CPF_CNPJ__c        = acc.CustomerIdentificationNbr__c;
            lead.Email              = acc.PersonEmail;
            lead.HomePhone__c       = acc.PersonHomePhone;
            lead.Home_Phone_Web__c  = acc.PersonHomePhone;
            lead.Mobile_Phone__c    = acc.PersMobPhone__c;
            lead.MobilePhone        = acc.PersMobPhone__c;
            
            lead.BIR_Dealer_of_Interest__c = ac.IDBIR__c;
            lead.CurrencyIsoCode    = 'BRL';
            lead.LeadSource         = 'DEALER';
            lead.SubSource__c       = opp.OpportunitySubSource__c;
            lead.RecordTypeId       = Utils.getRecordTypeId('Lead', 'Service');
            lead.VehicleRegistrNbr__c = opp.VehicleRegistrNbr__c;
            
            lead.ServiceType__c             = opp.ServiceType__c;
            lead.ProposalScheduleDate__c    = opp.ScheduledDate__c;
            lead.ProposalScheduleHour__c    = opp.ScheduledHour__c;
            lead.ScheduledPeriod__c         = opp.ScheduledPeriod__c;
            lead.Description                = opp.Description;
            
            System.debug('Lead: ' + lead);
            
            Database.insert(lead);
            
            VFC131_LeadBO.getInstance().convertLeadAndCreateOpportunity(lead, null);
            
            Opportunity op = [
                SELECT Id, AccountId
                FROM Opportunity 
                WHERE LeadId__c =: lead.Id
            ];
            
            System.debug('Op:' + op);
            
            opp.Dealer__c = ac.Id;
            opp.Id = op.Id;
            opp.AccountId = op.AccountId;
            
            update opp;
            
            System.debug('Opp: ' + opp);
            
            Account ac2 = [
                SELECT Id, isPersonAccount, LastName, HomePhone__c,
                FirstName, PersonHomePhone, PersMobPhone__c,
                CustomerIdentificationNbr__c,PersonEmail 
                FROM Account
                WHERE Id =: opp.AccountId
            ];
            
            ac2.HomePhone__c = acc.PersonHomePhone;
            ac2.PersonHomePhone = acc.PersonHomePhone;
            ac2.PersonEmail = acc.PersonEmail;
            ac2.PersMobPhone__c = acc.PersMobPhone__c;
            
            update ac2;
            
            initDados(opp.Id);
            
            System.debug('Opp: ' + opp);
            System.debug('Acc: ' + acc);
            
            return salvar();
            
        } catch(Exception ex) {
            Database.rollback(sp);
            
            System.debug('Ex: ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
            return null;
        }
        
        return Page.AgendamentoServico; 
    }
    
    
    public PageReference salvar() {
        
        if(!validar()) {
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            this.acc.FirstName = nome;
            this.acc.LastName = sobrenome;
            upsert acc;
            
            if(relVeiculo == null || relVeiculo.VIN__r.Id != veiculo.Id) {
                relVeiculo = new VRE_VehRel__c();
                relVeiculo.VIN__c = veiculo.Id;
                relVeiculo.Account__c = acc.Id;
                
                opp.VehicleRegistrNbr__c = veiculo.VehicleRegistrNbr__c;
            }
            
            relVeiculo.TypeRelation__c = tipoRelText;
            
            //opp.StageName = 'Scheduled';
            upsert opp;
            upsert relVeiculo;
            
            String[] tm = opp.ScheduledHour__c.split(':');
            Integer ho = Integer.valueOf(tm[0]);
            Integer min = Integer.valueOf(tm[1]);
            
            Datetime dt = Datetime.newInstance(
                opp.ScheduledDate__c.year(), 
                opp.ScheduledDate__c.month(), 
                opp.ScheduledDate__c.day(), ho, min, 0);
            
            this.evento.Subject = opp.ServiceType__c;
            this.evento.WhatId = opp.Id;
            this.evento.WhoId = opp.ResponsibleTechnical__c;
            this.evento.StartDateTime = dt;
            this.evento.EndDateTime = dt.addMinutes(15);
            
            upsert evento;
            
        } catch(Exception ex) {
            Database.rollback(sp);
            
            System.debug('Ex: ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
            return null;
        }
        
        return Page.AgendamentoServico; 
    }
    
    public PageReference salvarCancelar() {
        try {
            System.debug('Opp: ' + opp);
            String motivo = 
                ApexPages.currentPage().getParameters().get('motivo');
            
            opp.ReasonLossCancellation__c = motivo;
            
            upsert opp;
            
        } catch(Exception ex) {
            System.debug('Ex: ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
            return null;
        }
        
        return Page.AgendamentoServico; 
    }
    
    private Boolean buscarVeiculo(String texto) {
        
        Boolean ret = false;
        if(!String.isEmpty(texto)) {
            List<VEH_Veh__c> relList = [
                SELECT Id, VehicleRegistrNbr__c, Name, Model__c, Version__c
                FROM VEH_Veh__c 
                WHERE VehicleRegistrNbr__c =: texto OR Name =: texto
                LIMIT 1
            ];
            
            this.relVeiculo = new VRE_VehRel__c();
            
            if(!relList.isEmpty()) {
                this.veiculo = relList.get(0);    
                this.opp.VehicleRegistrNbr__c = veiculo.VehicleRegistrNbr__c;
                ret = true;
                
                List<VRE_VehRel__c> relVecList = [
                    SELECT Id, TypeRelation__c, VIN__r.Id 
                    FROM VRE_VehRel__c
                    WHERE VIN__r.Id =: veiculo.Id AND Account__c =: acc.Id
                ];
                
                if(!relVecList.isEmpty()) {
                    this.relVeiculo = relVecList.get(0);
                    this.tipoRelText = this.relVeiculo.TypeRelation__c;
                }
            }
            
        }
        
        return ret;
    }
    
    public PageReference buscarVeiculo() {
        
        if(!buscarVeiculo(textSearch)) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                'Veículo não localizado. Verifique os dados informados.'));
        }
        
        return null;
    }
    
    public PageReference selectHora() {
        ApexPages.getMessages().clear();
        Map<String, TabItemRow> mapTab = new Map<String, TabItemRow>();
        for(List<TabItemRow> li : tabItem.tabList) {
            for(TabItemRow t : li) {
                mapTab.put(t.hora, t);
            }
        }
        
        String hora = 
            ApexPages.currentPage().getParameters().get('hora');
        
        System.debug('Select hora: ' + hora);
        TabItemRow tb = mapTab.get(hora);
        
        if(tb != null) {
            
            if(tb.cor.equals(tb.ver) || tb.cor.equals(tb.ama)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Hora indisponivel'));
                return null;
            }
            
            opp.ScheduledHour__c = hora;
            updateHorario();
        }
        
        return null;
    }
    
    public PageReference updateHorario() {
        System.debug('Tecnico:' + opp.ResponsibleTechnical__c);
        System.debug('Data:' + opp.ScheduledDate__c);
        
        if(opp.ResponsibleTechnical__c == null || opp.ScheduledDate__c ==null) {
            return null;
        }
        
        Datetime dt = Datetime.newInstance(
            opp.ScheduledDate__c.year(), 
            opp.ScheduledDate__c.month(), 
            opp.ScheduledDate__c.day(), 0, 0, 0);
        
        Datetime dt2 = Datetime.newInstance(
            opp.ScheduledDate__c.year(), 
            opp.ScheduledDate__c.month(), 
            opp.ScheduledDate__c.day(), 23, 59, 59);
        
        List<Event> eventList = [
            SELECT Id, WhatId , WhoId, StartDateTime 
            FROM Event
            WHERE  WhoId =: opp.ResponsibleTechnical__c 
            AND StartDateTime >=: dt AND StartDateTime <=: dt2
        ];
        
        Map<String, TabItemRow> mapTab = new Map<String, TabItemRow>();
        for(List<TabItemRow> li : tabItem.tabList) {
            for(TabItemRow t : li) {
                mapTab.put(t.hora, t);
                t.cor = t.bra;
            }
        }
        
        for(Event e: eventList) {
            String key = e.StartDateTime.format('HH:') + tratarMin(e.StartDateTime);
            System.debug('Key:' + key);
            TabItemRow t = mapTab.get(key);
            t.cor = t.ver;
        }
        
        if(opp.ScheduledHour__c != null) {
            TabItemRow t = mapTab.get(opp.ScheduledHour__c);
            System.debug('HOra:' + t);
            System.debug('HOra:' + opp.ScheduledHour__c);
            
            if(t != null) {
                t.cor = t.ama;    
            }
            
        }
        
        return null;  
    }
    
    private String tratarMin(Datetime hora) {
        String s1 = hora.format('mm');
        Integer i1 = (Integer) Integer.valueOf(s1)/15;
        
        return (i1 == 0) ? '00' : String.valueOf(i1 * 15);
    }
    
    private void createTab() {
        this.tabItem = new TabItem();
        
        tabItem.tabList = new List<List<TabItemRow>>();
        
        List<TabItemRow> itemList = new List<TabItemRow>();
        
        itemList.add(new TabItemRow('08:00'));
        itemList.add(new TabItemRow('08:15'));
        itemList.add(new TabItemRow('08:30'));
        itemList.add(new TabItemRow('08:45'));
        itemList.add(new TabItemRow('09:00'));
        itemList.add(new TabItemRow('09:15'));
        itemList.add(new TabItemRow('09:30'));
        itemList.add(new TabItemRow('09:45'));
        tabItem.tabList.add(itemList);
        
        itemList = new List<TabItemRow>();
        itemList.add(new TabItemRow('10:00'));
        itemList.add(new TabItemRow('10:15'));
        itemList.add(new TabItemRow('10:30'));
        itemList.add(new TabItemRow('10:45'));
        itemList.add(new TabItemRow('11:00'));
        itemList.add(new TabItemRow('11:15'));
        itemList.add(new TabItemRow('11:30'));
        itemList.add(new TabItemRow('11:45'));
        tabItem.tabList.add(itemList);
        
        itemList = new List<TabItemRow>();
        itemList.add(new TabItemRow('12:00'));
        itemList.add(new TabItemRow('12:15'));
        itemList.add(new TabItemRow('12:30'));
        itemList.add(new TabItemRow('12:45'));
        itemList.add(new TabItemRow('13:00'));
        itemList.add(new TabItemRow('13:15'));
        itemList.add(new TabItemRow('13:30'));
        itemList.add(new TabItemRow('13:45'));
        tabItem.tabList.add(itemList);
        
        itemList = new List<TabItemRow>();
        itemList.add(new TabItemRow('14:00'));
        itemList.add(new TabItemRow('14:15'));
        itemList.add(new TabItemRow('14:30'));
        itemList.add(new TabItemRow('14:45'));
        itemList.add(new TabItemRow('15:00'));
        itemList.add(new TabItemRow('15:15'));
        itemList.add(new TabItemRow('15:30'));
        itemList.add(new TabItemRow('15:45'));
        tabItem.tabList.add(itemList);
        
        itemList = new List<TabItemRow>();
        itemList.add(new TabItemRow('16:00'));
        itemList.add(new TabItemRow('16:15'));
        itemList.add(new TabItemRow('16:30'));
        itemList.add(new TabItemRow('16:45'));
        itemList.add(new TabItemRow('17:00'));
        itemList.add(new TabItemRow('17:15'));
        itemList.add(new TabItemRow('17:30'));
        itemList.add(new TabItemRow('17:45'));
        tabItem.tabList.add(itemList);
        
        itemList = new List<TabItemRow>();
        itemList.add(new TabItemRow('18:00'));
        itemList.add(new TabItemRow('18:15'));
        itemList.add(new TabItemRow('18:30'));
        itemList.add(new TabItemRow('18:45'));
        itemList.add(new TabItemRow('19:00'));
        itemList.add(new TabItemRow('19:15'));
        itemList.add(new TabItemRow('19:30'));
        itemList.add(new TabItemRow('19:45'));
        tabItem.tabList.add(itemList);
    }
    
    public class TabItem {
        public List<List<TabItemRow>> tabList { get;set; }
        public TabItem() {
        }
    }
    
    public class TabItemRow {
        public string hora { get;set;}
        public String ver = 'VER';
        public String ama = 'AMA';
        public String bra = 'BRA';
        public String cin = 'CIN';
        public String cor { get; set;}
        public TabItemRow(String hora) {
            this.hora = hora;
            this.cor = cin;
        }
    }
}