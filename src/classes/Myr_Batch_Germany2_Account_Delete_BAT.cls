/*****************************************************************************************************
  Purge of german myRenault accounts
******************************************************************************************************
31 Mar 2016 : Creation 
******************************************************************************************************/
global class Myr_Batch_Germany2_Account_Delete_BAT extends INDUS_Batch_AbstractBatch_CLS { 
	String Purge_Mode;
	String Account_Ids='';

    global Database.QueryLocator start(Database.BatchableContext info){
		String RetentionDay = Country_Info__c.getInstance('Germany2').Myr_Deleting_Retention__c;
		String query = 'Select Id from Account where country__c=\'Germany2\' and ((';
		query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MyD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay;
		query += ') OR (';
		query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myd_Status__c=null';
		query += ') OR (';
		query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
		query += 'Myr_Status__c=null';
		query += '))';
		query += Myr_Batch_Germany2_ToolBox.Add_Governor_Limit();
        return Database.getQueryLocator(query);
    }
    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> scope){
		System.debug('Myr_Batch_Germany2_Account_Delete_BAT.doExecute');
        return new DatabaseAction(scope, Action.ACTION_DELETE, false);
    }
    global override void doFinish(Database.BatchableContext info){ 
        System.debug('Myr_Batch_Germany2_Account_Delete_BAT.doFinish');
    }
    global override String getClassName(){
        return Myr_Batch_Germany2_Account_Delete_BAT.class.GetName();
    }
}