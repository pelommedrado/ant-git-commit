@IsTest
global class INDUS_Batch_AbstractBatchMock_TEST extends INDUS_Batch_AbstractBatch_CLS {

	global Boolean sendEmail = false;
    
    //global Boolean doExecuteException {get; set;} 
    global Iterable<SObject> iter {get; set;}
    //global Id jobId {get; set;}
    global Action requiredAction {get; set;} 
    
    public INDUS_Batch_AbstractBatchMock_TEST(){
    } 
    
    global Iterable<SObject> start(Database.BatchableContext bc){
        //jobId = bc.getJobId();
		system.debug( '#### batch mock start = ');
        return iter;   
    }
    
    /**
     *Returns class name. 
     */
    global override String getClassName(){
        return INDUS_Batch_AbstractBatchMock_TEST.class.GetName();
    }
    /**
     *Processes a chunk of data. 
     */     
    global override DatabaseAction doExecute(Database.BatchableContext info, List<sObject> scope){
        if (requiredAction != null) {
            DatabaseAction db = new DatabaseAction(scope, requiredAction, false);
            return db;
        }
		//SDP, 12/04/2016: added the following lines just to cover the EmailSending part
		system.debug( '#### sendEmail = ' + sendEmail);
		if( sendEmail ) {
			List<INDUS_Batch_AbstractBatch_CLS.EmailSending> listEmails = new List<INDUS_Batch_AbstractBatch_CLS.EmailSending>();
			for( SObject s : scope ) {
				//SDP, 12/04/2016: Ok this is stupid BUT I really need this to cover the EmailSending on HQ AND Brazil, Russia
				//So, if you try to modify this part think about az template that will exist on Russia and Brazil ... please :-)
				Id targetObject = s.Id;
				Id template = s.Id;
				Id what = s.Id;
				Id orgWideEmailAddressId = s.Id;
				Boolean saveAs = false;
				listEmails.add( new INDUS_Batch_AbstractBatch_CLS.EmailSending(targetObject, template, what, orgWideEmailAddressId, saveAs) );
				sendEmail( listEmails );
			}
		}
        return null;
    }
    /**
     *Finish.
     */     
    global override void doFinish(Database.BatchableContext info){
    } 
}