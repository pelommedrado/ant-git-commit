global class deleteLeadsBatch implements Database.Batchable<sObject> {
    
    global final String Query;
    
    global deleteLeadsBatch(String q){
        Query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CampaignMember> scope) {
        
        List<Id> leadIds = new List<Id>();
        for(CampaignMember cm: scope){
            leadIds.add(cm.LeadId);
        }
        
        Database.Delete(leadIds,false);
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = [SELECT Id, Status FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        
        String message = 'Seu processo de exclusão de Membros de Campanha foi concluído. \n'
            + 'Status: ' + a.Status;

        enviaEmail('Processo de exclusão de Membros de Campanha', 'Status', message);
    }
    
    public void enviaEmail(String senderDisplayName, String subject, String message ){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});
        //mail.setReplyTo('batch@acme.com');
        mail.setSenderDisplayName(senderDisplayName);
        mail.setSubject(subject);
        mail.setPlainTextBody(message);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}