@isTest
global class FacebookIdToAccountMock implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('teste');
        response.setStatusCode(200);
        return response; 
    }

}