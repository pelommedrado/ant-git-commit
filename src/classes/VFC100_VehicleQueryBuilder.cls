/**
*   Classe VFC100_VehicoQueryBuilder
*   Classe de geração de query Vehico
*   # 01 - Elvia Serpa - 27/03/2013 -  Criação da classe
**/  
public virtual class VFC100_VehicleQueryBuilder extends VFC99_QueryBuilder 
{
    private String dealer;
    private String model;
    private String version;
    private Decimal year;
    private String color;   
    private String reserved = '\'Reserved\'';
    private String available = '\'Available\'';
    
    public static final VFC100_VehicleQueryBuilder instance = new VFC100_VehicleQueryBuilder();
        
    public static VFC100_VehicleQueryBuilder getInstance()  
    {
        return instance;
    }
    
    //Construtor
    public VFC100_VehicleQueryBuilder()  
    {
        
    }
    
    //Método responsavel pela geração da query. Permite uma possível sobreecrita
    public virtual override void buildQuery() 
    { 
         this.criteria.query = 'Select Id , VehicleRegistrNbr__c , Account__c ,  VIN__c, Status__c, VIN__r.Optional__c, VIN__r.Status__c , '+
                                +' VIN__r.Name , VIN__r.Model__c , VIN__r.ModelYear__c , VIN__r.DateofManu__c,' +
                                +' VIN__r.Color__c , DateOfEntryInStock__c , VIN__r.VersionCode__c ,  ' +
                                +' VIN__r.Version__c, VIN__r.Price__c, Account__r.IDBIR__c  From VRE_VehRel__c ';

         System.debug('>>> this.criteria.query'+this.criteria.query);
         
         if( year!= null)
         {
             this.criteria
                    // .add( VFC98_Restriction.eq('Account__c', dealer) )
                    .add( VFC98_Restriction.eq('Status__c', 'Active') ) 
                 	.add( VFC98_Restriction.eq('Account__r.ParentId', dealer) )
                    //.add( ' ( VIN__r.Status__c = ' + Available + ' OR VIN__r.Status__c = ' + Reserved +' ) ')
                    .add( VFC98_Restriction.eq('VIN__r.Is_Available__c', true))
                    //.add( VFC98_Restriction.eq('VIN__r.Model__c', model) )
                    .add( VFC98_Restriction.isLike('VIN__r.Model__c','%'+ model+'%') )
                    .add( VFC98_Restriction.eq('VIN__r.VersionCode__c', version) )
                    .add( VFC98_Restriction.eq('VIN__r.Color__c', color) )
                    //.add( VFC98_Restriction.eq('VIN__r.ModelYear__c', year) )
                    .add(' VIN__r.ModelYear__c = ' + Year )
                    .orderingBy('VIN__r.Model__c,VIN__r.Version__c,DateOfEntryInStock__c');
         } 
         else 
         {
             this.criteria
                    // .add( VFC98_Restriction.eq('Account__c', dealer) )
                    .add( VFC98_Restriction.eq('Status__c', 'Active') )
                 	.add( VFC98_Restriction.eq('Account__r.ParentId', dealer) )
                    //.add( ' ( VIN__r.Status__c = ' + Available + ' OR VIN__r.Status__c = ' + Reserved +' ) ')
                    .add( VFC98_Restriction.eq('VIN__r.Is_Available__c', true))
                    //.add( VFC98_Restriction.eq('VIN__r.Model__c', model) )
                    .add( VFC98_Restriction.isLike('VIN__r.Model__c','%'+ model+'%') )
                    .add( VFC98_Restriction.eq('VIN__r.VersionCode__c', version) )
                    .add( VFC98_Restriction.eq('VIN__r.Color__c', color) )
                    .orderingBy('VIN__r.Model__c,VIN__r.Version__c, DateOfEntryInStock__c');     
         }
        System.debug('>>> this.criteria.query'+this.criteria.query);
        System.debug('>>> this.criteria.soql '+this.criteria.soql );
        
    }   
    
    //Faz chamada a metodo de montagem de query e retorna resultado da busca
    public List<VRE_VehRel__c> search(String dealer, String model, String version, Decimal year, String Color) 
    { 
        this.dealer = dealer;
        this.model = model;
        this.Version = version;
        this.Year = year;
        this.color = color;
        this.buildQuery(); 
        return criteria.search();
    }
}