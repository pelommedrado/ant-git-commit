/**
* @author sebastien.ducamp@atos.net.renault.hqtrvdev
* @description Apex Class that contains tools shared troughout the MyRenault Administration GUI
* @version 1.0 / 16.07 / 10.05.2016
*/
public class Myr_AdministrationGUI_Tools { 
	
	private CS04_MYR_Settings__c MyrSetting;
	@testvisible private Map<String,String> Groups;

	/* @constructor **/
	public Myr_AdministrationGUI_Tools() {
		MyrSetting = CS04_MYR_Settings__c.getInstance();
		prepareGroups();
	}

	/* @return List<String> the list of system fields **/
	public List<String> getSystemFields() {
		return system.Label.Myr_GUI_SystemFieldsList.split(',');
	}

	/* Prepare the groups to use within the GUI **/
	private void prepareGroups() {
		system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - BEGIN');
		Groups = new Map<String, String>();
		//system fields
		system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - Prepare System Fields');
		try {
			List<String> systemFields = getSystemFields();
			for( String sf : systemFields ) {
				Groups.put( sf, system.Label.Myr_GUI_SystemFields);
			}
		} catch (Exception e) {
			system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - exception prepare system fields: ' + e.getMessage() );
		}
		//customized groups
		try {
		List<String> settingGroups = MyrSetting.Myr_GUI_GroupingFields__c.split(';');
		for( String grp : settingGroups ) {
			try {
				List<String> couple = grp.split(':');
				Groups.put( couple[0], couple[1]);
			} catch( Exception e ) {
				system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - error for the group ' + grp + ': ' + e.getMessage() );
			}
		}
		} catch (Exception e) {
			system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - exception : ' + e.getMessage() );
		}
		system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - Found Groups: ' + Groups);
		system.debug('### Myr_AdministrationGUI_Tools - <prepareGroups> - END');
	}

	/* @return String the label of the group associated with the current field **/
	public String getGroup( String fieldName ) {
		for( String key : Groups.keySet() ) {
			if( fieldName.equalsIgnoreCase( key ) ) {
				return Groups.get( key );
			} else if( fieldName.containsIgnoreCase( key ) ) {
				String grp = Groups.get( key );
				if( !grp.equalsIgnoreCase( system.Label.Myr_GUI_SystemFields ) ) {
					return grp;
				}
			}
		}
		return null;
	}
	
	/* @return Map the field grouped by labels **/
	public Map<String, List<String>> buildGroups( List<String> fields ) {
		system.debug('### Myr_AdministrationGUI_Tools - <buildGroups> - BEGIN');
		Map<String, List<String>> GroupFields = new Map<String, List<String>>();
		List<String> withoutGroups = new List<String>();
		for( String f : fields ) {
			String grp = getGroup( f );
			system.debug('### Myr_AdministrationGUI_Tools - <buildGroups> Found Group: f='+f+', groupe='+grp);
			if( null == grp ) {
				withoutGroups.add( f );
			} else if( GroupFields.containsKey( grp ) ) {
				GroupFields.get( grp ).add( f );
			} else {
				GroupFields.put( grp, new List<String>{f} );
			}
		}
		GroupFields.put(system.Label.Myr_GUI_OtherFields, withoutGroups);
		system.debug('### Myr_AdministrationGUI_Tools - <buildGroups> - Groups='+GroupFields.keySet());
		system.debug('### Myr_AdministrationGUI_Tools - <buildGroups> - END');
		return GroupFields;
	}
}