/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description Default mapping for Rforce required fields to create case.
*/
global class Rforce_CreateCaseMapping_CLS{
/**
* @author Vinoth Baskaran
* @date 17/07/2014
* @description It accepts the Casedetails from the external service.
* @param case Details
* @return String value according to request
*/
    static Case cas=new case();
    static String sRectypeId;
    public static case createCaseMapping(Rforce_W2CSchema_CLS.CaseDetails caseDetails)
    {
           cas.Type=caseDetails.type;
           cas.SubType__c=caseDetails.subType;
           cas.CaseBrand__c=caseDetails.caseBrand;
           cas.Origin=caseDetails.origin;
           cas.CaseSubSource__c=caseDetails.subSource;
           cas.SuppliedEmail=caseDetails.suppliedEmail;
           cas.CountryCase__c=caseDetails.country;        
           cas.Title_Web__c=caseDetails.title;
           cas.Company_Web__c=caseDetails.company;
           cas.FirstName_Web__c=caseDetails.firstName;
           cas.LastName_Web__c=caseDetails.lastName;
           cas.Address_Web__c=caseDetails.address;
           cas.Postal_Code_Web__c=caseDetails.postalCode;
           cas.City_Web__c=caseDetails.city;
           cas.SuppliedPhone=caseDetails.suppliedPhone;
           cas.Cell_Phone_Web__c=caseDetails.cellPhone;
           cas.Subject=caseDetails.subject;           
           cas.Priority=caseDetails.priority;          
           cas.Status=system.label.CAS_Status_New;                   
           cas.CaseSubSource__c=caseDetails.subSource;           
           cas.Type=caseDetails.type;
           cas.Description=caseDetails.description;
           cas.SubType__c=caseDetails.subType;
           cas.Detail__c=caseDetails.detail;         
           cas.From__c=caseDetails.caseFrom;       
           cas.Language_Web__c=caseDetails.language;
           cas.PT_Dealer_Web__c=caseDetails.dealer;  
           cas.First_registration_Web__c=caseDetails.firstRegistration;  
           cas.Date_of_Purchase_Web__c =caseDetails.dateOfPurchase;
           cas.Renault_Contact_Title_Web__c=caseDetails.renaultContactTitle;
           cas.Renault_Contact_FirstName_Web__c=caseDetails.renaultContactFirstName;
           cas.Renault_Contact_LastName_Web__c=caseDetails.renaultContactLastName;
           cas.Renault_Contact_Email_Web__c=caseDetails.renaultContactEmail;  
           cas.Kilometer__c=caseDetails.kilometer;
           cas.Dealer_AfterSales_PostalCode_Web__c=caseDetails.dealerAfterSalesPostalCode;     
           if(caseDetails.comAgreement==System.label.Rforce_CAS_Swiss_Web_True){
                cas.Swiss_Web__c=True;
            } 
            else if(caseDetails.comAgreement==System.label.Rforce_CAS_Swiss_Web_False){
                cas.Swiss_Web__c=False;
            }               
           cas.Email_Opt_In_Web__c=caseDetails.emailOptIn;          
           cas.License_Number_Web__c=caseDetails.licenseNumber;
           cas.State_Web__c=caseDetails.state;
           cas.VIN_Web__c=caseDetails.vin;
           if(cas.CountryCase__c!=''){
                try{
                    String sRectypeName=Country_Info__c.getInstance(cas.CountryCase__c).Case_RecordType__c;
                    sRectypeId=[Select Id from RecordType where DeveloperName=:sRectypeName].Id;
                    cas.RecordTypeId=sRectypeId;    
                }catch(DmlException e) {
                     // DmlException handling code here.
                      System.debug('An exception occurred:' + e.getMessage());
                  }catch(Exception e) {
                     // Generic exception handling code here.
                      System.debug('An Generic exception occurred:' + e.getMessage());
                  } finally {
                     // Perform some clean up.                    
                  }                    
                
            }
    return cas;
    }   
    public static boolean insertCase(Case c)
    {
        try{
        //Perform some database operations that  might cause an exception.
        insert c;                       
        }catch(DmlException e) {
        // DmlException handling code here.
        System.debug('An exception occurred:' + e.getMessage());
        }catch(Exception e) {
        // Generic exception handling code here.
        System.debug('An Generic exception occurred:' + e.getMessage());
        } finally {
        // Perform some clean up.                    
        }                                                                       
        if(c.id != null){
        return true;          
        }          
        else{
        return false;
        }                   
    }
}