/*****************************************************************************************
    Name            : Rforce_CommercialOffers_CTR_TEST
    Description     : This Class will help us to test the Rforce_CommercialOffer_CTR.
    Project Name    : Rforce
    Release Number  : 8.0_SP2
    Implemented Date: 06-05-2015
    Imnplemented By : Ashok Muntha
 
******************************************************************************************/
@isTest(seeAlldata=true)
public with sharing class Rforce_CommercialOffers_CTR_TEST {

static testMethod void CommercialOffer(){
 
        Country_Info__c ctr = new Country_Info__c (Name = 'TestCountry', Country_Code_2L__c = 'BR', Language__c = 'Français', Case_RecordType__c = 'BR_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', alias = 'lro',RecordDefaultCountry__c='Brazil', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();

            Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Id caseRTId = [select Id from RecordType where sObjectType = 'Case' and DeveloperName = 'ZA_Case_RecType' limit 1].Id;
            Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc12', Phone = '0000', RecordTypeId = RTID, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert Acc;
            string accname=acc.name;
            VEH_Veh__c veh1 = new VEH_Veh__c(Name='Vhgg7898uh',VehicleBrand__c='Renault',Initiale_Paris__c=True);
            insert veh1;
            Case cs = new case (Type='Complaint', Origin='RENAULT SITE',RecordTypeId = caseRTId,VIN__c=veh1.id, AccountId=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Argentina');
            insert cs;
            
            Commercial_Offer__c cmOff = new Commercial_Offer__c();
            cmOff.account__c=acc.id;
            cmOff.Offer_Id__c ='24234234221';
            //cmOff.Offer_Id__c ='23423412';
            cmOff.InterestedStatus__c = 'Yes';
            
            insert cmOff;
            List<Commercial_Offer__c> lstOffersInterested = new List<Commercial_Offer__c>();
            lstOffersInterested = [SELECT Offer_Id__c FROM Commercial_Offer__c WHERE InterestedStatus__c='Yes' and Id =: cmOff.Id];
            Commercial_Offer__c strOfferId = [select Id from Commercial_Offer__c where  Id =: cmOff.Id];
            Case cCaseNo = [select Id,CaseNumber,VIN__c from Case where Id =: cs.Id ];

            //passing the sample response
            Test.setMock(Comparable.class, new Rforce_CommercialOffersResponse_CLS('jhfsd','9385sfd','ewrer4','345345','tsasfd45','5234rwe','43512334214','23423412','35432523r','3','2','RENCUSNATMYRASMN','dfgedger','20/05/2015','28/05/2015','29/05/2015','cdq3we32','324234de',1));
 
            Rforce_CommercialOffers_CTR controller= new Rforce_CommercialOffers_CTR();  
            


            PageReference pageRef = Page.Rforce_AccountCommercialOffer;
            Test.setCurrentPage(pageRef);
            pageref=controller.checkOffersBasedOnEmailOrVIN();
            controller.checkOffersBasedOnEmailOrVIN(); 
 
            controller.strObjectType='Case';
            ApexPages.currentPage().getParameters().put('Id',cs.Id);
            
            string responseXML = '(Rforce_CommercialOffersResponse_CLS:[sortBy=ByOfferID, strBrandListID=, strCategoryType=null, strContextWeight=null, strDesMkt=test rforce, strDeskEli=test rforce, strDeskLeg=test rforce, strNewOffer=false, strPictDetail=test, strPictGen=test, strPropCode=RENCUSNATMYRASMN, strPropEndDate=30/05/2015, strPropID=1709108266, strPropLabel=test celine ITA, strPropOfferID=1709108266, strPropOfferSpaceID=1709108384, strPropRank=3, strPropStartDate=31/03/2015, strPropWeight=1, strUsedOffer=, strValidityDate=30/05/2015], Rforce_CommercialOffersResponse_CLS:[sortBy=ByOfferID, strBrandListID=, strCategoryType=null, strContextWeight=null, strDesMkt=test rforce, strDeskEli=test rforce, strDeskLeg=test rforce, strNewOffer=false, strPictDetail=https://renault-s.neolane.net/res/renault/75f28d037eb95a952dc935ddc5366d18.jpg, strPictGen=https://renault-s.neolane.net/res/renault/6d6a3ea37ad7cdb1a2615a419f6180bb.jpg, strPropCode=RENCUSNATMYRASMN, strPropEndDate=30/07/2015, strPropID=1709291508, strPropLabel=Test WS Rforce_celine, strPropOfferID=1709291508, strPropOfferSpaceID=1709108384, strPropRank=4, strPropStartDate=11/05/2014, strPropWeight=1, strUsedOffer=, strValidityDate=30/07/2015])';
            
            List<Rforce_CommercialOffersResponse_CLS> rsXMl= new List<Rforce_CommercialOffersResponse_CLS>(); 
            Rforce_CommercialOffers_WS rforceCommercialOfferWS = new Rforce_CommercialOffers_WS();
            Rforce_CommercialOffersPropose_CLS rforceCommercialOffersPropose =new Rforce_CommercialOffersPropose_CLS();
            Rforce_CommercialOffersParsing_CLS rforceCommercialParsingCLS= new Rforce_CommercialOffersParsing_CLS();
            Case caseobject = new Case();
            String strInterestedOrNotSelected = 'true';
            
            Rforce_CommercialOffersResponse_CLS rxXMLObj = new Rforce_CommercialOffersResponse_CLS('jhfsd','9385sfd','ewrer4','345345','tsasfd45','5234rwe','43512334214','23423412','35432523r','3','2','RENCUSNATMYRASMN','dfgedger','','','','cdq3we32','324234de',1);

            
            rsXML.add(rxXMLObj);
            
            controller.rforceCommercialVINOffers=rsXML;
            controller.strOffersSelected='23423412';            
            controller.checkOffersBasedOnEmailOrVIN();
            
            controller.strObjectType='Case';
            ApexPages.currentPage().getParameters().put('Id',cs.Id);
            Rforce_CommercialOffersParsing_CLS rforceParsingCl= new Rforce_CommercialOffersParsing_CLS();
            
            controller.strCommercialOfferResponseXMLforEmail = '<proposition id="1709273056" offer-id="1709273056" offerSpace-id="1709108384" weight="4" rank="1" code="RENCUSNATMYRASMN" label="test wenshu" startDate="" endDate="" validityDate="">';
            
            rforceParsingCl.parseCommercialOffers(controller.strCommercialOfferResponseXMLforEmail);

            controller.rforceCommercialOffers=rsXML;            
            controller.strOffersSelected='23423412';
            controller.createOffer();
            ApexPages.currentPage().getParameters().put('strOfferId','23423412');
            pageref=controller.redirectOfferDetailpage();
            controller.refreshPage();
            controller.strSearchList='1234';
            controller.searchList();
            
            controller.strObjectType='Account';
            ApexPages.currentPage().getParameters().put('Id',Acc.Id);  
            Rforce_CommercialOffersParsing_CLS rforceParsing= new Rforce_CommercialOffersParsing_CLS();
            Rforce_CommercialOffersPropose_CLS offerPropose = new Rforce_CommercialOffersPropose_CLS('afef@gmail.com','india','3','kjhk3241324','0990kh','jhgjb','jhgjftu','13wefwe');

            controller.checkOffersBasedOnEmailOrVIN();
            controller.createOffer();
            controller.findTaxonomyByKey('REN','RENCUSNATMYRASMN');
            controller.findTaxonomyByKey('REN','RENCUSNATMYRNS');
            Integer strOfferCodeLength = 16;
            pageref=controller.redirectOfferDetailpage();
                    
            controller.getcheckOffersBasedOnEmailOrVINforSearch();
            
            Rforce_CustomXMLDom parser=new Rforce_CustomXMLDom();
            string strXML = '<proposition id="1706672456" offer-id="173243056" offerSpace-id="172134408384" weight="4" rank="1" code="RENCUSNATMYRASMN" label="test wenshu" startDate="23/05/2015" endDate="28/05/2015" validityDate="27/05/2015">';
            parser.parseFromString(strXML);
            
            Rforce_CommercialOffersParsing_CLS commOfferParsing = new Rforce_CommercialOffersParsing_CLS();
            
            commOfferParsing.dateFormat('2008-10-05-11:20:20');
            
            controller.rforceCommercialOffers=rsXML;
            
            system.debug('@@@controller.rforceCommercialOffers55'+controller.rforceCommercialOffers);
            controller.strOffersSelected='23423412';
            controller.createOffer();
            
            Boolean bResult = false;
            
            OfferHistory__c offerHistory = new OfferHistory__c(OfferId__c = '173243056', Offer_Customer_Email__c = 'test@mail.com',Offer__c=cmOff.id,OfferInterestedStatus__c = 'Yes');             
            insert offerHistory;
            Rforce_CommercialOffersPush_CTR.pushOffer('sdfwef@df.com','https://google.com',strOfferId.id,acc.id,cCaseNo.CaseNumber,offerHistory.id);
            Rforce_CommercialOffersPush_CTR.deleteOfferHistory(offerHistory.id);            
                        
            Test.stopTest();
}
}
Static testMethod void CommercialOfferCode(){
 
        Country_Info__c ctr = new Country_Info__c (Name = 'TestCountry', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c = 'FR_Case_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', alias = 'lro',RecordDefaultCountry__c='Italy', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Rforce_CommercialOffers_CTR offerController= new Rforce_CommercialOffers_CTR(); 
            offerController.findTaxonomyByKey('REN','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('REN','RENCUSNATMYRNS');
            offerController.findTaxonomyByKey('CUS','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('NAT','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('MYR','RENCUSNATMYRASMN');            
            offerController.findTaxonomyByKey('MYR','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('RAS','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('MN','RENCUSNATMYRASMN');
            offerController.findTaxonomyByKey('CUS','RENCUSNATMYRNS');
            offerController.findTaxonomyByKey('NAT','RENCUSNATMYRNS');
            offerController.findTaxonomyByKey('MYR','RENCUSNATMYRNS');
            offerController.getId();
            
            
            Test.stopTest();
            
        }
      
    } 
    }