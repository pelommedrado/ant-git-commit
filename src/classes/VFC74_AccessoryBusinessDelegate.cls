/**
* Classe responsável por fazer o papel de business delegate da página de novo acessório.
* @author Thiago Alves dos Santos.
*/
public class VFC74_AccessoryBusinessDelegate {

	private static final VFC74_AccessoryBusinessDelegate instance = new VFC74_AccessoryBusinessDelegate();

	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC74_AccessoryBusinessDelegate()
	{
		
	}

	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC74_AccessoryBusinessDelegate getInstance()
	{
		return instance;
	}
	
	
	public void createNewAccessory(String quoteId, String itemDescription)
	{	
		Quote sObjQuote = null;
		QuoteLineItem quoteLineItem = new QuoteLineItem();
		Pricebook2 sObjPricebook2 = getStandardPriceBook();
		PricebookEntry sObjPricebookEntry = getPricebookEntry();
		
		quoteLineItem.QuoteId = quoteId;
		quoteLineItem.Description = itemDescription;
		quoteLineItem.PricebookEntryId = sObjPricebookEntry.Id;
		quoteLineItem.Quantity = 1;
		quoteLineItem.UnitPrice = 0;
		VFC75_AccessoryDAO.getInstance().insertData(quoteLineItem);
		
	}
	
	public void updateAccessory(String quoteLineItemId, String itemDescription)
	{	
		QuoteLineItem quoteLineItem = VFC74_AccessoryBusinessDelegate.getInstance().getQuoteLineItemById(quoteLineItemId);
		quoteLineItem.Description = itemDescription;
		VFC75_AccessoryDAO.getInstance().updateData(quoteLineItem);
	}
	
		
	private Quote getQuoteById(String quoteId)
	{
		Quote sObjQuote = null;
		
		/*obtém o orçamento pelo id*/
		sObjQuote =  VFC35_QuoteDAO.getInstance().findById(quoteId);			    				    
						
		return sObjQuote; 
	}
	
	public QuoteLineItem getQuoteLineItemById(String quoteLineItemId)
	{
		
		/*obtém alinha de orçamento pelo id*/
		QuoteLineItem sObjQuoteLineItem =  VFC36_QuoteLineItemDAO.getInstance().getQuoteLineItemById(quoteLineItemId);		    				    
						
		return sObjQuoteLineItem; 
	}
	
	private Product2 getProduct2Accessory()
	{
		Product2 sObjProduct2 = new Product2();
		
		return sObjProduct2;
		
	} 
	
	private Pricebook2 getStandardPriceBook()
	{
		Pricebook2 sObjPricebook2 = VFC65_Pricebook2_DAO.getInstance().fetch_StandardPriceBook();
		return sObjPricebook2;
	} 
	
	
	private PricebookEntry getPricebookEntry()
	{
		PricebookEntry sObjPriceBookEntry = null;
		String productId = VFC22_ProductDAO.getInstance().get_Accessory_Products().Id;
		String priceBookId =  getStandardPriceBook().Id;
		sObjPriceBookEntry = VFC75_AccessoryDAO.getInstance().getPricebookEntry(productId, priceBookId); 
	
		return sObjPriceBookEntry;
	}
}