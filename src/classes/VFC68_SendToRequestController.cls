public with sharing class VFC68_SendToRequestController 
{
    private String quoteId;
    public VFC60_QuoteVO quoteVO {get;set;}
    public String mobilePhoneQuote {get;set;}
    public String customerEmailQuote {get;set;}
    public String homePhoneQuote {get;set;}
    
    public Boolean useExistBillingAccount {get;set;}
        
    public VFC68_SendToRequestController (ApexPages.StandardController controller)
    {
        this.quoteId = controller.getId();
        this.quoteVO = new VFC60_QuoteVO();
    }

    public PageReference initializeQuote() { 
        this.quoteVO = VFC63_QuoteBusinessDelegate.getInstance().getQuoteById(this.quoteId);
        
        if(quoteVO.sObjQuote.BillingAccount__r == null){
        	quoteVO.sObjQuote.BillingAccount__r = new Account();
        }
		        
        return null;
    }

    public PageReference saveRequest()
    {
        String strMessage = null;
        ApexPages.Message message = null;
            
            //validação de campo customizado devido ao request ajax
            System.debug('############################################################################### saveRequest ' + quoteVO);
            System.debug('###########################' + quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c);
            System.debug('###########################' + quoteVO.sObjQuote.BillingAccount__r.Sex__c);
            
            //valida dados obrigatórios da conta principal
            if((quoteVO.accountFirstName == '' || quoteVO.accountFirstName == null)
                 || (quoteVO.accountLastName == '' || quoteVO.accountLastName == null)
                 || (quoteVO.accountCnpjCpf == '' || quoteVO.accountCnpjCpf == null)
                 || (quoteVO.accountEmail == '' || quoteVO.accountEmail == null)
                 || (quoteVO.accountResPhone == '' || quoteVO.accountResPhone == null)
                 || (quoteVO.accountStreet == '' || quoteVO.accountStreet == null )
                 || (quoteVO.accountZipCode == '' || quoteVO.accountZipCode == null)
                 || (quoteVO.accountState == '' || quoteVO.accountState == null)
                 || (quoteVO.accountCity == '' || quoteVO.accountCity == null)
                 || (quoteVO.accountRG == '' || quoteVO.accountRG == null)
                 || (quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate == null)
                 || (quoteVO.sObjQuote.Opportunity.Account.Sex__c == '' || quoteVO.sObjQuote.Opportunity.Account.Sex__c == null)
                 || (quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c == '' || quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c == null)){
            	
            	
                message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha os campos obrigatórios.');
                ApexPages.addMessage(message);
                return null;
                
            }
			
			//caso a flag não for selecionada
			if(!useExistBillingAccount){
				if(quoteVO.tipoCliente == 'cpf'){
					if(((quoteVO.billFirstName == ''|| quoteVO.billFirstName == null)  )
	                 || ((quoteVO.billLastName == '' ||  quoteVO.billLastName == null) )
	                 || ((quoteVO.billCpf == '' || quoteVO.billCpf == null) )
	                 || ((quoteVO.billEmail == '' || quoteVO.billEmail == null) )
	                 || ((quoteVO.billResPhone == '' || quoteVO.billResPhone == null) )
	                 || ((quoteVO.billStreet == '' || quoteVO.billStreet == null) )
	                 || ((quoteVO.billZipCode == '' || quoteVO.billZipCode == null) )
	                 || ((quoteVO.billState == '' || quoteVO.billState == null) )
	                 || ((quoteVO.billCity == '' || quoteVO.billCity == null) )
	                 || ((quoteVO.billRG == '' || quoteVO.billRG == null) )
	                 || ((quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c == '' || quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c == null) )
	                 || ((quoteVO.sObjQuote.BillingAccount__r.Sex__c == '' || quoteVO.sObjQuote.BillingAccount__r.Sex__c == null) )
	                 || ((quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate == null ))){
	                 	
		                message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha os campos obrigatórios.');
		                ApexPages.addMessage(message);
		                return null;
	             	}
	             }else if(quoteVO.tipoCliente == 'cnpj'){
	             	if(((quoteVO.billPJName == ''|| quoteVO.billPJName == null)  )
	                 || ((quoteVO.billPJCnpj == '' || quoteVO.billPJCnpj == null) )
	                 || ((quoteVO.billPJEmail == '' || quoteVO.billPJEmail == null) )
	                 || ((quoteVO.billPJStreet == '' || quoteVO.billPJStreet == null) )
	                 || ((quoteVO.billPJZipCode == '' || quoteVO.billPJZipCode == null) )
	                 || ((quoteVO.billPJState == '' || quoteVO.billPJState == null) )
	                 || ((quoteVO.billPJCity == '' || quoteVO.billPJCity == null) )){
		             	message = new ApexPages.Message(ApexPages.Severity.ERROR, 'Preencha os campos obrigatórios.');
		               	ApexPages.addMessage(message);
		               	return null;
	             	}	
				}
				}
        
            try
            {
               Database.update( quoteVO.sObjQuote.Opportunity );
               VFC63_QuoteBusinessDelegate.getInstance().saveQuote(quoteVo);
               PageReference pageRef = new PageReference('/apex/VFP11_Quote?Id='+quoteVO.opportunityId);
               pageRef.setRedirect(true);
               return pageRef;                                      
            }
            catch(DMLException ex)
            {
                strMessage = ex.getDMLMessage(0);
                message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
                ApexPages.addMessage(message);
            }           
            return null;
    }

    public void refresh()
    {
    }
    
    public void useAccountInfo()
    {   
        quoteVO.tipoCliente = 'cpf';
        if(useExistBillingAccount){
            quoteVO.FKQuote.BillingAccount__c = this.quoteVO.sObjQuote.Opportunity.Account.Id;
            changeBillingAccount();         
        }
    }

	public PageReference changeTipoCliente()
    {
    	quoteVO.FKQuote.BillingAccount__c = null;
    	
    	return null;
    }

    /**
    * 
    */
    public PageReference changeBillingAccount()
    {   
        Account billingAccount = null;

   		System.debug('*** changeBillingAccount ' + quoteVO);

		if(useExistBillingAccount){
			
        	quoteVO.billFirstName = quoteVO.accountFirstName;
            quoteVo.billLastName = quoteVO.accountLastName;
            quoteVo.billCpf = quoteVO.accountCnpjCpf;
            quoteVo.billEmail = quoteVO.accountEmail;
            quoteVo.billCelPhone = quoteVo.accountCelPhone;
            quoteVo.billFax = quoteVo.accountFax;
            quoteVO.billResPhone = quoteVO.accountResPhone; 
            quoteVo.billComPhone = quoteVo.accountComPhone;
            quoteVO.billStreet = quoteVO.accountStreet;
            quoteVO.billZipCode = quoteVO.accountZipCode;
            quoteVO.billState = quoteVO.accountState;
            quoteVO.billCity = quoteVO.accountCity;
            quoteVO.billRG = quoteVO.accountRG;
            
            if(quoteVO.sObjQuote.BillingAccount__r == null)
            	quoteVO.sObjQuote.BillingAccount__r = new Account();
            
            quoteVO.sObjQuote.BillingAccount__r.PersonBirthdate = quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate;
        	quoteVO.sObjQuote.BillingAccount__r.MaritalStatus__c = quoteVo.sObjQuote.Opportunity.Account.MaritalStatus__c;
        	quoteVO.sObjQuote.BillingAccount__r.Sex__c = quoteVO.sObjQuote.Opportunity.Account.Sex__c;
        	quoteVO.tipoCliente = 'cpf';
		
		}else if(quoteVO.FKQuote.BillingAccount__c != null){
			
        	billingAccount = VFC63_QuoteBusinessDelegate.getInstance().getCustomerById(quoteVO.FKQuote.BillingAccount__c);
			
			Id personalAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Personal_Acc');
			Id companyAccount = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Company_Acc');
				
        	
        		if(billingAccount != null){
        			System.debug('####################### billingAccount'  + billingAccount);     
        			
        			if(billingAccount.RecordTypeId != null && billingAccount.RecordTypeId == companyAccount){
        				quoteVO.tipoCliente = 'cnpj';
        			}else if(billingAccount.RecordTypeId != null && billingAccount.RecordTypeId == personalAccount){
        				quoteVO.tipoCliente = 'cpf';
        			}
        			 
        			VFC63_QuoteBusinessDelegate.getInstance().updateQuoteVOByAccount(billingAccount,quoteVO);
        			
        			System.debug('####################### quoteVO.tipoCliente: '  + quoteVO.tipoCliente);
        			
        		}
        }
        return null; 
    }   
    
    /*  
    * Add values to Lead State field in visualforce page.
    * @return option - return the State result and to store values form record
    */
    public List<SelectOption> getStates(){
        /* Initialize the selectOption to store options */
        List<SelectOption> state_Option = new List<SelectOption>();
        
        /* Add none value to the picklist option */
        state_Option.add(new SelectOption('', ''));
        
        // Hard coded state values.
        state_Option.add(new SelectOption('AC', 'AC'));
        state_Option.add(new SelectOption('AL', 'AL'));
        state_Option.add(new SelectOption('AP', 'AP'));
        state_Option.add(new SelectOption('AM', 'AM'));
        state_Option.add(new SelectOption('BA', 'BA'));
        state_Option.add(new SelectOption('CE', 'CE'));
        state_Option.add(new SelectOption('DF', 'DF'));
        state_Option.add(new SelectOption('ES', 'ES'));
        state_Option.add(new SelectOption('GO', 'GO'));
        state_Option.add(new SelectOption('MA', 'MA'));
        state_Option.add(new SelectOption('MT', 'MT'));
        state_Option.add(new SelectOption('MS', 'MS'));
        state_Option.add(new SelectOption('MG', 'MG'));
        state_Option.add(new SelectOption('PR', 'PR'));
        state_Option.add(new SelectOption('PB', 'PB'));
        state_Option.add(new SelectOption('PA', 'PA'));
        state_Option.add(new SelectOption('PE', 'PE'));
        state_Option.add(new SelectOption('PI', 'PI'));
        state_Option.add(new SelectOption('RJ', 'RJ'));
        state_Option.add(new SelectOption('RN', 'RN'));
        state_Option.add(new SelectOption('RS', 'RS'));
        state_Option.add(new SelectOption('RO', 'RO'));
        state_Option.add(new SelectOption('RR', 'RR'));
        state_Option.add(new SelectOption('SC', 'SC'));
        state_Option.add(new SelectOption('SE', 'SE'));
        state_Option.add(new SelectOption('SP', 'SP'));
        state_Option.add(new SelectOption('TO', 'TO'));
        /* Return Option */
        return state_Option;
    }
    
    /**
     * 
     */
    public PageReference cancelRequest()
    {
       PageReference pageRef = new PageReference('/apex/VFP11_Quote?Id='+quoteVO.opportunityId);
       pageRef.setRedirect(true);
       return pageRef;      
    }
    
    /**
     * Get the address information based on the zip code.
     */
    public PageReference getAddressGivenZipCode()
    {
    	system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCode()');
    	
    	String zipCodeNumber = quoteVO.accountZipCode;
    	
    	if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
	    	ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
			system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

			if (zipCode != null) {
				quoteVO.accountStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
				quoteVO.accountCity = zipCode.City__c; 
				quoteVO.accountState = zipCode.State__c;
			}
    	}
		return null;
    }
    
    /**
     * Get the address information based on the zip code for the CPF section.
     */
    public PageReference getAddressGivenZipCodeCPF()
    {
    	system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCodeCPF()');
    	
    	String zipCodeNumber = quoteVO.billZipCode;
    	
    	if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
	    	ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
			system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

			if (zipCode != null) {
				quoteVO.billStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
				quoteVO.billCity = zipCode.City__c; 
				quoteVO.billState = zipCode.State__c;
			}
    	}
		return null;
    }
    
    /**
     * Get the address information based on the zip code for the CNPJ section.
     */
    public PageReference getAddressGivenZipCodeCNPJ()
    {
    	system.debug(LoggingLevel.INFO, '*** getAddressGivenZipCodeCNPJ()');
    	
    	String zipCodeNumber = quoteVO.billPJZipCode;
    	
    	if (zipCodeNumber != null && zipCodeNumber.length() == 8) {
	    	ZipCodeBase__c zipCode = VFC137_ZipCodeService.getAddressGivenZipCode(zipCodeNumber);
			system.debug(LoggingLevel.INFO, '*** zipCode='+zipCode);

			if (zipCode != null) {
				quoteVO.billPJStreet = zipCode.Street__c + ', ' + zipCode.Neighborhood__c;
				quoteVO.billPJCity = zipCode.City__c; 
				quoteVO.billPJState = zipCode.State__c;
			}
    	}
		return null;
    }
    
    public PageReference getAccount(){
    	
    	system.debug(LoggingLevel.INFO, '*** getAccount()');
    	
    	String accountCnpjCpf = quoteVO.accountCnpjCpf;
    	
    	if (accountCnpjCpf != null && (accountCnpjCpf.length() == 11 || accountCnpjCpf.length() == 14)) {
	    	
	    	Account mergeAccount = VFC12_AccountDAO.getInstance().fetchAccountToMerge(accountCnpjCpf); 
			
			system.debug(LoggingLevel.INFO, '*** mergeAccount ='+mergeAccount);

			if (mergeAccount != null) {
						
				quoteVO.sObjQuote.Opportunity.Account = mergeAccount;
                quoteVO.sObjQuote.Opportunity.AccountId = mergeAccount.Id;
                quoteVo.accountFirstName = mergeAccount.FirstName;
                quoteVo.accountLastName = mergeAccount.LastName;
                quoteVo.accountCnpjCpf = mergeAccount.CustomerIdentificationNbr__c;
                quoteVo.accountEmail = mergeAccount.PersEmailAddress__c;
                quoteVo.accountCelPhone = mergeAccount.PersMobPhone__c;
                quoteVo.accountResPhone = mergeAccount.PersLandline__c;
                quoteVo.accountComPhone = mergeAccount.ProfLandline__c;
                quoteVo.accountFax = mergeAccount.Fax;
                quoteVo.accountStreet = mergeAccount.ShippingStreet;
                quoteVo.accountZipCode = mergeAccount.ShippingPostalCode;
                quoteVo.accountState = mergeAccount.ShippingState;
                quoteVo.accountCity = mergeAccount.ShippingCity;
                quoteVO.sObjQuote.Opportunity.Account.MaritalStatus__c = mergeAccount.MaritalStatus__c;
                quoteVO.sObjQuote.Opportunity.Account.PersonBirthdate = mergeAccount.PersonBirthdate;
                quoteVO.sObjQuote.Opportunity.Account.Sex__c = mergeAccount.Sex__c;
                quoteVo.accountRG = (mergeAccount.RGStateRegistration__c > 0) ? String.valueOF(mergeAccount.RGStateRegistration__c) : '';
            }
    	}
    	
    	return null;
    }
    
    public Boolean mergeRecords(Account masterAccount , Account accountToMerge) {
   		Boolean success = false;
   		// Array to hold the results
   	
		      
			merge   accountToMerge masterAccount;
		      
		     
   return success;
}
}