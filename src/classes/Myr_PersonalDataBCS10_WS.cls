//Generated by wsdl2apex

public class Myr_PersonalDataBCS10_WS {
    public class getCustDataResponse {
        public Myr_PersonalDataBCS10_WS.response response;
        private String[] response_type_info = new String[]{'response','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class getCustDataTopElmt {
        public Myr_PersonalDataBCS10_WS.getCustData getCustData;
        private String[] getCustData_type_info = new String[]{'getCustData','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'getCustData'};
    }
    public class BCS_spcCrmGetCustDataService_getCustData_Output_element {
        public String faultactor;
        public String faultcode;
        public String faultstring;
        public Myr_PersonalDataBCS10_WS.getCustDataResponse getCustDataResponse;
        private String[] faultactor_type_info = new String[]{'faultactor','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] faultcode_type_info = new String[]{'faultcode','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] faultstring_type_info = new String[]{'faultstring','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] getCustDataResponse_type_info = new String[]{'getCustDataResponse','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'faultactor','faultcode','faultstring','getCustDataResponse'};
    }
    public class servicePrefs {
        public String irn;
        public String sia;
        public String reqid;
        public String userid;
        public String language;
        public String country;
        private String[] irn_type_info = new String[]{'irn','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] sia_type_info = new String[]{'sia','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] reqid_type_info = new String[]{'reqid','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] userid_type_info = new String[]{'userid','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] language_type_info = new String[]{'language','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] country_type_info = new String[]{'country','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'irn','sia','reqid','userid','language','country'};
    }
    public class BCScommAgreement {
        public String Global_Comm_Agreement;
        public String Preferred_Communication_Method;
        public String Post_Comm_Agreement;
        public String Tel_Comm_Agreement;
        public String SMS_Comm_Agreement;
        public String Fax_Comm_Agreement;
        public String Email_Comm_Agreement;
        private String[] Global_Comm_Agreement_type_info = new String[]{'Global.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] Preferred_Communication_Method_type_info = new String[]{'Preferred.Communication.Method','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] Post_Comm_Agreement_type_info = new String[]{'Post.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] Tel_Comm_Agreement_type_info = new String[]{'Tel.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] SMS_Comm_Agreement_type_info = new String[]{'SMS.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] Fax_Comm_Agreement_type_info = new String[]{'Fax.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] Email_Comm_Agreement_type_info = new String[]{'Email.Comm.Agreement','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'Global_Comm_Agreement','Preferred_Communication_Method','Post_Comm_Agreement','Tel_Comm_Agreement','SMS_Comm_Agreement','Fax_Comm_Agreement','Email_Comm_Agreement'};
    }
    public class contact {
        public String phoneCode1;
        public String phoneNum1;
        public String phoneCode2;
        public String phoneNum2;
        public String phoneCode3;
        public String phoneNum3;
        public String email;
        public String preferredCom;
        public String optin;
        private String[] phoneCode1_type_info = new String[]{'phoneCode1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] phoneNum1_type_info = new String[]{'phoneNum1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] phoneCode2_type_info = new String[]{'phoneCode2','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] phoneNum2_type_info = new String[]{'phoneNum2','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] phoneCode3_type_info = new String[]{'phoneCode3','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] phoneNum3_type_info = new String[]{'phoneNum3','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] preferredCom_type_info = new String[]{'preferredCom','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] optin_type_info = new String[]{'optin','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'phoneCode1','phoneNum1','phoneCode2','phoneNum2','phoneCode3','phoneNum3','email','preferredCom','optin'};
    }
    public class vehicleList {
        public String vin;
        public String brandCode;
        public String brandLabel;
        public String modelCode;
        public String modelLabel;
        public String versionLabel;
        public String registrationDate;
        public String firstRegistrationDate;
        public String registration;
        public String new_x;
        public String energy;
        public String fiscPow;
        public String dynPow;
        public String doorNum;
        public String transmissionType;
        public String capacity;
        public String colorCode;
        public String possessionBegin;
        public String paymentMethod;
        public String technicalInspectionDate;
        public String possessionEnd;
        public String purchaseNature;
        public Myr_PersonalDataBCS10_WS.workshopList[] workshopList;
        private String[] vin_type_info = new String[]{'vin','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] brandCode_type_info = new String[]{'brandCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] brandLabel_type_info = new String[]{'brandLabel','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] modelCode_type_info = new String[]{'modelCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] modelLabel_type_info = new String[]{'modelLabel','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] versionLabel_type_info = new String[]{'versionLabel','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] registrationDate_type_info = new String[]{'registrationDate','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] firstRegistrationDate_type_info = new String[]{'firstRegistrationDate','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] registration_type_info = new String[]{'registration','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] new_x_type_info = new String[]{'new','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] energy_type_info = new String[]{'energy','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] fiscPow_type_info = new String[]{'fiscPow','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] dynPow_type_info = new String[]{'dynPow','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] doorNum_type_info = new String[]{'doorNum','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] transmissionType_type_info = new String[]{'transmissionType','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] capacity_type_info = new String[]{'capacity','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] colorCode_type_info = new String[]{'colorCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] possessionBegin_type_info = new String[]{'possessionBegin','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] paymentMethod_type_info = new String[]{'paymentMethod','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] technicalInspectionDate_type_info = new String[]{'technicalInspectionDate','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] possessionEnd_type_info = new String[]{'possessionEnd','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] purchaseNature_type_info = new String[]{'purchaseNature','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] workshopList_type_info = new String[]{'workshopList','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'vin','brandCode','brandLabel','modelCode','modelLabel','versionLabel','registrationDate','firstRegistrationDate','registration','new_x','energy','fiscPow','dynPow','doorNum','transmissionType','capacity','colorCode','possessionBegin','paymentMethod','technicalInspectionDate','possessionEnd','purchaseNature','workshopList'};
    }
    public class custDataRequest {
        public String mode;
        public String country;
        public String brand;
        public String demander;
        public String vin;
        public String lastName;
        public String firstName;
        public String city;
        public String zip;
        public String ident1;
        public String idClient;
        public String idMyr;
        public String firstRegistrationDate;
        public String registration;
        public String email;
        public String ownedVehicles;
        public String nbReplies;
        private String[] mode_type_info = new String[]{'mode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] country_type_info = new String[]{'country','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] brand_type_info = new String[]{'brand','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] demander_type_info = new String[]{'demander','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] vin_type_info = new String[]{'vin','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] city_type_info = new String[]{'city','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] zip_type_info = new String[]{'zip','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] ident1_type_info = new String[]{'ident1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] idClient_type_info = new String[]{'idClient','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] idMyr_type_info = new String[]{'idMyr','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] firstRegistrationDate_type_info = new String[]{'firstRegistrationDate','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] registration_type_info = new String[]{'registration','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] ownedVehicles_type_info = new String[]{'ownedVehicles','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] nbReplies_type_info = new String[]{'nbReplies','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'mode','country','brand','demander','vin','lastName','firstName','city','zip','ident1','idClient','idMyr','firstRegistrationDate','registration','email','ownedVehicles','nbReplies'};
    }
    public class getCustDataResponseTopElmt {
        public Myr_PersonalDataBCS10_WS.getCustDataResponse getCustDataResponse;
        private String[] getCustDataResponse_type_info = new String[]{'getCustDataResponse','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'getCustDataResponse'};
    }
    public class response {
        public Myr_PersonalDataBCS10_WS.clientList[] clientList;
        public Myr_PersonalDataBCS10_WS.wsInfos wsInfos;
        public String responseCode;
        public String nbReplies;
        private String[] clientList_type_info = new String[]{'clientList','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] wsInfos_type_info = new String[]{'wsInfos','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] responseCode_type_info = new String[]{'responseCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] nbReplies_type_info = new String[]{'nbReplies','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'clientList','wsInfos','responseCode','nbReplies'};
    }
    public class BCS_spcCrmGetCustDataService_getCustData_Input_element {
        public Myr_PersonalDataBCS10_WS.getCustData getCustData;
        private String[] getCustData_type_info = new String[]{'getCustData','http://custdata.crm.bservice.renault',null,'1','1','false'};
        //SDP 06.11.2014 adaptation due to multi-schemes !!
        //private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','true'};
        //private String[] apex_schema_type_info = new String[]{'http://siebel.com/CustomUI','true','false'};
        private String[] field_order_type_info = new String[]{'getCustData'};
    }
    public class getCustData {
        public Myr_PersonalDataBCS10_WS.request request;
        private String[] request_type_info = new String[]{'request','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','true'};
        private String[] field_order_type_info = new String[]{'request'};
    }
    public class address {
        public String strName;
        public String strNum;
        public String compl1;
        public String compl2;
        public String compl3;
        public String strType;
        public String strTypeLabel;
        public String countryCode;
        public String zip;
        public String city;
        public String qtrCode;
        public String dptCode;
        public String sortCode;
        public String numberPB;
        public String zipPB;
        public String cityPB;
        public String areaCode;
        public String areaLabel;
        public String addressUpdateDate;
        private String[] strName_type_info = new String[]{'strName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] strNum_type_info = new String[]{'strNum','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] compl1_type_info = new String[]{'compl1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] compl2_type_info = new String[]{'compl2','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] compl3_type_info = new String[]{'compl3','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] strType_type_info = new String[]{'strType','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] strTypeLabel_type_info = new String[]{'strTypeLabel','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] zip_type_info = new String[]{'zip','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] city_type_info = new String[]{'city','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] qtrCode_type_info = new String[]{'qtrCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] dptCode_type_info = new String[]{'dptCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] sortCode_type_info = new String[]{'sortCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] numberPB_type_info = new String[]{'numberPB','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] zipPB_type_info = new String[]{'zipPB','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] cityPB_type_info = new String[]{'cityPB','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] areaCode_type_info = new String[]{'areaCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] areaLabel_type_info = new String[]{'areaLabel','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] addressUpdateDate_type_info = new String[]{'addressUpdateDate','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'strName','strNum','compl1','compl2','compl3','strType','strTypeLabel','countryCode','zip','city','qtrCode','dptCode','sortCode','numberPB','zipPB','cityPB','areaCode','areaLabel','addressUpdateDate'};
    }
    public class description {
        public String type_x;
        public String typeCode;
        public String subType;
        public String subTypeCode;
        private String[] type_x_type_info = new String[]{'type','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] typeCode_type_info = new String[]{'typeCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] subType_type_info = new String[]{'subType','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] subTypeCode_type_info = new String[]{'subTypeCode','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'type_x','typeCode','subType','subTypeCode'};
    }
    public class request {
        public Myr_PersonalDataBCS10_WS.servicePrefs servicePrefs;
        public Myr_PersonalDataBCS10_WS.custDataRequest custDataRequest;
        private String[] servicePrefs_type_info = new String[]{'servicePrefs','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] custDataRequest_type_info = new String[]{'custDataRequest','http://custdata.crm.bservice.renault',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'servicePrefs','custDataRequest'};
    }
    public class wsInfos {
        public String wsVersion;
        public String wsEnv;
        private String[] wsVersion_type_info = new String[]{'wsVersion','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] wsEnv_type_info = new String[]{'wsEnv','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'wsVersion','wsEnv'};
    }
    public class survey {
        public String surveyOK;
        public String value;
        public String loyMessage;
        private String[] surveyOK_type_info = new String[]{'surveyOK','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] value_type_info = new String[]{'value','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] loyMessage_type_info = new String[]{'loyMessage','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'surveyOK','value','loyMessage'};
    }
    public class dealerList {
        public Myr_PersonalDataBCS10_WS.birId[] birId;
        //public String[] birId;
        private String[] birId_type_info = new String[]{'birId','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'birId'};
    }
    public class clientList {
        public String idClient;
        public String lastName;
        public String title;
        public String lang;
        public String middleName;
        public String firstName;
        public String distinction1;
        public Myr_PersonalDataBCS10_WS.BCScommAgreement[] BCScommAgreement;
        public String distinction2;
        public String initiales;
        public String typeIdent1;
        public String ident1;
        public String ident2;
        public String birthDay;
        public String sex;
        public String soc;
        public String legalCategory;
        public Myr_PersonalDataBCS10_WS.contact contact;
        public Myr_PersonalDataBCS10_WS.address address;
        public String idMyr;
        public String typeperson;
        public String hasRenaultCard;
        public String flagMyR;
        public Myr_PersonalDataBCS10_WS.vehicleList[] vehicleList;
        public Myr_PersonalDataBCS10_WS.dealerList[] dealerList;
        public Myr_PersonalDataBCS10_WS.survey survey;
        private String[] idClient_type_info = new String[]{'idClient','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] title_type_info = new String[]{'title','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] lang_type_info = new String[]{'lang','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] middleName_type_info = new String[]{'middleName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] distinction1_type_info = new String[]{'distinction1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] BCScommAgreement_type_info = new String[]{'BCScommAgreement','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] distinction2_type_info = new String[]{'distinction2','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] initiales_type_info = new String[]{'initiales','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] typeIdent1_type_info = new String[]{'typeIdent1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] ident1_type_info = new String[]{'ident1','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] ident2_type_info = new String[]{'ident2','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] birthDay_type_info = new String[]{'birthDay','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] sex_type_info = new String[]{'sex','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] soc_type_info = new String[]{'soc','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] legalCategory_type_info = new String[]{'legalCategory','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] contact_type_info = new String[]{'contact','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] address_type_info = new String[]{'address','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] idMyr_type_info = new String[]{'idMyr','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] typeperson_type_info = new String[]{'typeperson','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] hasRenaultCard_type_info = new String[]{'hasRenaultCard','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] flagMyR_type_info = new String[]{'flagMyR','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] vehicleList_type_info = new String[]{'vehicleList','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] dealerList_type_info = new String[]{'dealerList','http://custdata.crm.bservice.renault',null,'0','-1','false'};
        private String[] survey_type_info = new String[]{'survey','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'idClient','lastName','title','lang','middleName','firstName','distinction1','BCScommAgreement','distinction2','initiales','typeIdent1','ident1','ident2','birthDay','sex','soc','legalCategory','contact','address','idMyr','typeperson','hasRenaultCard','flagMyR','vehicleList','dealerList','survey'};
    }
    public class workshopList {
        public String date_x;
        public String km;
        public String birId;
        public Myr_PersonalDataBCS10_WS.description description;
        private String[] date_x_type_info = new String[]{'date','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] km_type_info = new String[]{'km','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] birId_type_info = new String[]{'birId','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] description_type_info = new String[]{'description','http://custdata.crm.bservice.renault',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{'date_x','km','birId','description'};
    }
    public class birId {
        private String[] apex_schema_type_info = new String[]{'http://custdata.crm.bservice.renault','true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class BCS_spcCrmGetCustDataService {
        public String endpoint_x = 'http://bcsopeh01.mc2.renault.fr:18080/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&UserName=sadmin&Password=padmin';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://custdata.crm.bservice.renault', 'Myr_PersonalDataBCS10_WS'};
        public Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element getCustData(Myr_PersonalDataBCS10_WS.getCustData getCustData) {
            Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Input_element request_x = new Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Input_element();
            request_x.getCustData = getCustData;
            Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element response_x;
            Map<String, Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element> response_map_x = new Map<String, Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,																			
              'document/http://siebel.com/CustomUI:getCustData',
              //'http://custdata.crm.bservice.renault',                                 //SDP 06.11.2014 adaptation due to multi-schemes !!
              'http://siebel.com/CustomUI',                                             //SDP 06.11.2014 adaptation due to multi-schemes !!
              'BCS_spcCrmGetCustDataService_getCustData_Input',
              //'http://custdata.crm.bservice.renault',
              'http://siebel.com/CustomUI',
              'BCS_spcCrmGetCustDataService_getCustData_Output',
              'Myr_PersonalDataBCS10_WS.BCS_spcCrmGetCustDataService_getCustData_Output_element'}
            );
            
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}