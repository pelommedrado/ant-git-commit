public with sharing class InterviewResponseBuild {
	public Id accountId 	{get;set;}
	public Id contactId 	{get;set;}
	public Id interviewId 	{get;set;}

	public String origin		 			{get{return origin != null ? origin : 'Facebook';}set;}
	public String returnReason 				{get{return returnReason != null ? returnReason : 'test';}set;} 
	public String dealerInTouch 			{get{return dealerInTouch != null ? dealerInTouch : 'test';}set;}
	public String answerLink 				{get{return answerLink != null ? answerLink : 'www.test.com.br';}set;}
	public String contactChannel 			{get{return contactChannel != null ? contactChannel : 'Facebook';}set;}
	public String returnedToRepair 			{get{return returnedToRepair != null ? returnedToRepair : 'test';}set;}
	public String testDriveOffered 			{get{return testDriveOffered != null ? testDriveOffered : 'test';}set;}
	public String customerExperience 		{get{return customerExperience != null ? customerExperience : 'test';}set;}
	public String vehicleDeliveredInTime	{get{return vehicleDeliveredInTime != null ? vehicleDeliveredInTime : 'test';}set;}

	public Boolean customerInTouch {get{return customerInTouch != null ? customerInTouch : False;}set;}

	public Integer dealerScore		{get{return dealerScore != null ? dealerScore : 0;}set;}

	public DateTime answerDate	  	{get{return answerDate != null ? answerDate : System.Today();}set;}

	public static InterviewResponseBuild instance = null;

	public static InterviewResponseBuild getInstance() {
        if(instance == null) {
            instance = new InterviewResponseBuild();
        }
        return instance;
    }

	public InterviewResponse__c newInterviewResp(Id accId, Id cntId, Id intrvwId){
		this.accountId 		= accId;
		this.contactId 		= cntId;
		this.interviewId 	= intrvwId;
		return generateInterviewResp();
	}

	public InterviewResponse__c generateInterviewResp(){
		InterviewResponse__c newInterviewRes = new InterviewResponse__c();
		newInterviewRes.AccountId__c                            = this.accountId;
		newInterviewRes.AnswerDate__c                       	= this.answerDate;
		newInterviewRes.AnswerLink__c                           = this.answerLink;
		newInterviewRes.ContactId__c                          	= this.contactId;
		newInterviewRes.ContactChannel__c                       = this.contactChannel;
		newInterviewRes.CustomerExperience__c                   = this.customerExperience;
		newInterviewRes.CustomerReturnedRenaultRepairShop__c    = this.returnedToRepair;
		newInterviewRes.CustumerGetInTouch__c                   = this.customerInTouch;
		newInterviewRes.DealerGetInTouchAfterService__c         = this.dealerInTouch;
		newInterviewRes.ReasonReturn__c                         = this.returnReason;
		newInterviewRes.ReccomendDealerNote__c                  = this.dealerScore;
		newInterviewRes.TestDriveOffered__c                     = this.testDriveOffered;
		newInterviewRes.VehicleDeliveriedDateCombined__c        = this.vehicleDeliveredInTime;
		newInterviewRes.InterviewId__c                          = this.interviewId;
		newInterviewRes.Origin__c 								= this.origin;

		return newInterviewRes;
	}
}