@istest class FaturaDealerServicoTest {
    
    static testmethod void test() {
        FaturaDealer__c fat = new FaturaDealer__c();
        fat.NFTIPREG__c = '10';
        fat.NFBIRENV__c = '07600999';
        fat.NFBIREMI__c = '07600999';
        fat.NFSEQUEN__c = '2780';
        fat.NFNROREG__c = '00029';
        
        fat.NFBANDEI__c = 'REN';
        fat.NFCODOPR__c = '25';
        fat.NFCODFIS__c = '5403  VN';
        fat.NFTIPONF__c = 'VN';
        fat.NFNRONFI__c = '00073295';
        fat.NFCHASSI__c = '11111111111111911';
        
        fat.NFSERNFI__c = 'U';
        fat.NFDTANFI__c = '20150827';
        fat.NFNOMCLI__c = 'ITAMAR JOSE VIEIRA FERNANDES';
        fat.NFTIPVIA__c = 'RUA';
        fat.NFNOMVIA__c = 'Manoel Mancellos Moura';
        
        fat.NFNROVIA__c = '681';
        fat.NFCPLEND__c = 'AP 103';
        fat.NFBAIRRO__c = 'Canasvieiras';
        fat.NFCIDADE__c = 'FLORIANÓPOLIS';
        fat.NFNROCEP__c = '88054030';
        
        fat.NFESTADO__c = 'SC';
        fat.NFPAISRE__c = '00';
        fat.NFESTCIV__c = '';
        fat.NFSEXOMF__c = 'M';
        fat.NFDTANAS__c = '195908';
        fat.NFTIPCLI__c = 'F';
        fat.NFCPFCGC__c = '37970380972';
        
        fat.NFEMAILS__c = 'pelommedrado@gmail.com';
        fat.NFDDDRES__c = '11';
        fat.NFTELRES__c = '77778888';
        fat.NFDDDCEL__c = '11';
        fat.NFTELCEL__c = '99996666';
        fat.NFANOFAB__c = '2000';
        fat.NFANOMOD__c = '2000';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151204';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '00207';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = null;
        fat.NFSEQUEN__c = '2041';
        fat.NFNROREG__c = '00206';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151201';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '00207';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151203';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '00207';
         insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = null;
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '01206';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151203';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '00207';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151203';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '01206';
        insert fat;
        
        fat = new FaturaDealer__c();
        fat.NFDTANFI__c = '20151204';
        fat.NFSEQUEN__c = '2040';
        fat.NFNROREG__c = '00207';
        insert fat;
        
        List<FaturaDealer__c> li = [
            SELECT Id, CreatedDate, Count__c, Reprocessed__c, RecordTypeId, 
            NFSEQUEN__c,
            NFNROREG__c,
            NFBANDEI__c, 
            NFCODOPR__c, 
            NFTIPONF__c, 
            NFTIPREG__c, 
            NFBIRENV__c, 
            NFBIREMI__c,
            NFDTANFI__c, 
            NFDTANAS__c, 
            NFTIPCLI__c, 
            NFCPFCGC__c, 
            NFNRONFI__c,
            NFEMAILS__c, 
            NFDDDRES__c, 
            NFTELRES__c, 
            NFDDDCEL__c, 
            NFTELCEL__c,
            NFCHASSI__c, 
            NFANOFAB__c, 
            NFANOMOD__c, 
            NFDTASIS__c
            FROM FaturaDealer__c
            ORDER BY Count__c
        ];
        
        List<FaturaDealerServico.FaturaDealerWrapper> lW = new List<FaturaDealerServico.FaturaDealerWrapper>();
        for(FaturaDealer__c f : li) {
            lW.add(new FaturaDealerServico.FaturaDealerWrapper(f));
        }
        lW.sort();
        
        for(FaturaDealerServico.FaturaDealerWrapper c: lW) {
            System.debug('T:' + c.fat.CreatedDate.getTime() + ' ' + c.fat);
        }
        
        FaturaDealerServico servico = new FaturaDealerServico();
        servico.iniciarValidacao(li);
        
    }
}