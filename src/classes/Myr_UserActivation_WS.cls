/*  Activation of user (Webservice) 
    Error codes :   OK              : WS02MS000 
                    OK WITH WARNING : WS02MS001 -> WS02MS500
                    CRITICAL ERROR  : WS02MS501 -> WS02MS999
*************************************************************************************************************
25 Aug 2014 : Creation
04 May 2015 : Assesment SalesForce, removed hard coded strings. Modify loggers to get more information for debug purposes
17 Sep 2015 : ATC synchronization
23 Sep 2015 : Correct message when the modification of the status on the account is OK (Critical before, Info now)
16 Nov 2015 : R16.01  9.1 ChocolateCamelBaby, D. Veron (AtoS) : User's login and account's personal email become independent
01 Fev 2016 : Patch
18 Mar 2016 : R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
04 May 2016 : R16.07 11.0 BlackEagle, D. Veron (AtoS) : AdobeCampain synchronizations
*************************************************************************************************************/
global class Myr_UserActivation_WS {
	
    //WebService used to activate a user on a given account
    //@param accountSfdcId, the SFDC id of the personal account on which we want activate the user
    //@param accountBrand, brand of the account
    //@return a msg and the activated user
    
    WebService static Myr_UserActivation_WS_Response activateUser(String accountSfdcId, String federationIdentifier, String timeZoneSidKey, String localeSidKey, String languageLocaleKey, String currencyCode, String accountBrand) {
        System.debug('Myr_UserActivation_WS: begin');
		String logRunId = Myr_UserActivation_WS.class.getName() + DateTime.now().getTime();
		Long Begin_Time = DateTime.now().getTime();
		String Inputs;
		List<Account> listAccount;
		User u;
		Profile heliosProfile;
		List<User> listUsers;
		Account M_A;

        //Check brand: renault by default if accountBrand is not filled in entry
        if( String.isBlank(accountBrand) ) {
        	accountBrand = Myr_MyRenaultTools.Brand.RENAULT.name();
        }

		//Prepare the inputs to log
        Inputs = 'accountSfdcId='+accountSfdcId
    			+', federationIdentifier='+federationIdentifier
    			+', timeZoneSidKey='+timeZoneSidKey
    			+', localeSidKey='+localeSidKey
                +', languageLocaleKey='+languageLocaleKey
                +', currencyCode='+currencyCode
				+', accountBrand='+accountBrand;
         
        //Check mandatory fields value
        String fieldsLabel=missingMandatory(accountSfdcId, timeZoneSidKey, localeSidKey, languageLocaleKey, currencyCode);
        if (fieldsLabel != ''){
            return prepareResponse(system.Label.Myr_UserActivation_WS02MS501, system.Label.Myr_UserActivation_WS02MS501_Msg + fieldslabel, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
        }
        //Too long parameter values
        fieldsLabel=tooLongParamValue(accountSfdcId, federationIdentifier, timeZoneSidKey, localeSidKey, languageLocaleKey, currencyCode, accountBrand);
        if (fieldsLabel != ''){
            return prepareResponse(system.Label.Myr_UserActivation_WS01MS507, system.Label.Myr_UserActivation_WS01MS507_Msg + fieldslabel, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
        }

        //Account exists ?
        try {
            listAccount = [SELECT Id
								, IsPersonAccount
								, PersonContactId
								, Salutation
								, PersEmailAddress__c
								, MyRenaultID__c
								, MyDaciaID__c
								, FirstName
								, LastName
								, country__c
								, MYR_Status__c
								, MYD_Status__c 
							FROM Account WHERE Id = :accountSfdcId AND isPersonAccount = true];
        } catch (Exception e) {
            return prepareResponse(system.Label.Myr_UserActivation_WS02MS502, system.Label.Myr_UserActivation_WS02MS502_Msg + ' exception=' + e.getMessage(), logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
        }
        if( listAccount.size() == 0) {
            return prepareResponse(system.Label.Myr_UserActivation_WS02MS502, system.Label.Myr_UserActivation_WS02MS502_Msg, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
        }
        M_A = listAccount[0];

		if( !String.isBlank(accountBrand) && accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name())) {
            ATC_Synchronization(accountSfdcId);//Synchro ATC
        }

        listUsers = new List<User>();
        //User exists?
        try { 
            listUsers = [SELECT Id, ProfileId, isActive, Profile.Name FROM User WHERE Contact.AccountId = :accountSfdcId];
        } catch (Exception e) {
            //addLog(logRunId, 'activateUser', 'Exception when requesting user, accountSfdcId=' + accountSfdcId, system.Label.Myr_UserActivation_WS02MS503, e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Critical.name());
        }
        if( listUsers.size() == 0) { 
            //Create User
            u = null;
            try {  
                u = Myr_Users_Utilities_Class.createUser(M_A, accountBrand, timeZoneSidKey, localeSidKey, languageLocaleKey, currencyCode); 
                fireFutureEvents(accountSfdcId, accountBrand, logRunId, Inputs);
                return prepareResponse(system.Label.Myr_UserActivation_WS02MS000, system.Label.Myr_UserActivation_WS02MS000_Msg, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Info, Begin_Time, Inputs);
            } catch (Exception e) {
                return prepareResponse(system.Label.Myr_UserActivation_WS02MS503, system.Label.Myr_UserActivation_WS02MS503_Msg + ' exception=' + e.getMessage(), logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
            }
        } else {
            u = listUsers[0];
            heliosProfile = Myr_Users_Utilities_Class.getHeliosCommunityProfile();
            //The user have a community profile
            if( u.ProfileId == heliosProfile.Id ) {
                //Activated user ?
                if(!u.isActive) {
                    try {
                        u.isActive = true; 
                        u = Myr_Users_Utilities_Class.prepareUsername( u, listAccount[0], accountBrand );
                        update u;
						fireFutureEvents(accountSfdcId, accountBrand, logRunId, Inputs);
                    } catch (Dmlexception dmlE) {
                        return prepareResponse(system.Label.Myr_UserActivation_WS02MS504, system.Label.Myr_UserActivation_WS02MS504_Msg + dmlE.getMessage() + ', u.Username=' + u.Username, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
                    }
					return prepareResponse(system.Label.Myr_UserActivation_WS02MS002, system.Label.Myr_UserActivation_WS02MS002_Msg, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Info, Begin_Time, Inputs);
                } else {
					fireFutureEvents(accountSfdcId, accountBrand, logRunId, Inputs);
                	return prepareResponse(system.Label.Myr_UserActivation_WS02MS001, system.Label.Myr_UserActivation_WS02MS001_Msg, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Info, Begin_Time, Inputs);  
                }
            //The user NOT have a community profile 
            } else {
                try {
                    u.ProfileId = heliosProfile.Id;
                    u.isActive = true;
                    update u;
                    fireFutureEvents(accountSfdcId, accountBrand, logRunId, Inputs);
                    return prepareResponse(system.Label.Myr_UserActivation_WS02MS003, system.Label.Myr_UserActivation_WS02MS003_Msg, logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Info, Begin_Time, Inputs);
                } catch (Dmlexception e) {
                    return prepareResponse(system.Label.Myr_UserActivation_WS02MS505, system.Label.Myr_UserActivation_WS02MS505_Msg + u.Profile.Name + ' exception: ' + e.getMessage(), logRunId, accountSfdcId, INDUS_Logger_CLS.ErrorLevel.Critical, Begin_Time, Inputs);
                }
            }
        }
        return null;
    }
    
    //@return the response in case of a bad situation
    private static Myr_UserActivation_WS_Response prepareResponse(String code, String message, String logRunId, String logRecord, INDUS_Logger_CLS.ErrorLevel logErrLvl, Long Begin_Time, String inputs) {
		addLog( logRunId, 'activateUser', logRecord, code, message, logErrLvl.name(), Begin_Time, inputs);
        Myr_UserActivation_WS_Response response = new Myr_UserActivation_WS_Response();
        response.info = new Myr_UserActivation_WS_Response_Msg();
        response.info.code = code;
        response.info.message = message;
        return response;
    }
    
    //Global format of the response
    global class Myr_UserActivation_WS_Response {
        WebService Myr_UserActivation_WS_Response_Msg info;
    }
    
    global class Myr_UserActivation_WS_Response_Msg {
        WebService String code;
        WebService String message;
    }
    
	static String missingMandatory(String accountSfdcId, String timeZoneSidKey, String localeSidKey, String languageLocaleKey, String currencyCode) { 
	    String str='';
	    if (accountSfdcId!=null && accountSfdcId==''){str+='accountSfdcId ';}
	    if (timeZoneSidKey!=null && timeZoneSidKey==''){str+='timeZoneSidKey ';}
	    if (localeSidKey!=null && localeSidKey==''){str+='localeSidKey ';}
	    if (languageLocaleKey!=null && languageLocaleKey==''){str+='languageLocaleKey ';}
	    if (currencyCode!=null && currencyCode==''){str+='currencyCode ';}
	    return str;
	}
  
	//@return too long value parameters
	static String tooLongParamValue(String accountSfdcId, String federationIdentifier, String timeZoneSidKey, String localeSidKey, String languageLocaleKey, String currencyCode, String accountBrand) { 
	    String str='';
	    if (accountSfdcId!=null && accountSfdcId.length()>18){str+='accountSfdcId ';}
	    if (federationIdentifier!=null && federationIdentifier.length()>512){str+='federationIdentifier ';}
	    if (timeZoneSidKey!=null && timeZoneSidKey.length()>140){str+='timeZoneSidKey ';}    
	    if (localeSidKey!=null && localeSidKey.length()>150){str+='localeSidKey ';}    
	    if (languageLocaleKey!=null && languageLocaleKey.length()>150){str+='languageLocaleKey ';}
	    if (currencyCode!=null && currencyCode.length()>150){str+='currencyCode ';}
	    if (accountBrand!=null && accountBrand.length()>16){str+='accountBrand ';}
	    return str;
	}

	//SDP Hard case: trigger the adobe campaign synchronization AND update the account status
	//Account status modification is already in future method because it's not possible to update User and Account in the same transaction
	//as Adobe Campaign is a callout, we should also trigger this in a future method.
	//Whatever the chosen solution we fire the synchronization without be sure that the Account has been really updated to Activated status ....
	//And this is also impossible to call the method from a trigger: we are already in a future method and we will be forced to make
	//a synchronized callout (i.e. without future) from a trigger that is not possible.
	@future( callout=true )
	static void fireFutureEvents( String accountSfdcId, String accountBrand, String logRunId, String Inputs ) {
		//Get the account
		Account myAcc = [SELECT Id, PersEmailAddress__c, MyRenaultID__c, MyDaciaID__c, Country__c FROM Account WHERE Id = :accountSfdcId];
		//Adobe Campaign synchronization
		AC_Synchronization( accountSfdcId, myAcc.Country__c, accountBrand);
		//update the account status to activated
		updateStatusAccount(myAcc, accountBrand, logRunId, Inputs);
	}

	//Set the account status
  	static void updateStatusAccount(Account myAcc, String accountBrand, String logRunId, String Inputs) {
		system.debug('### Myr_UserActivation_WS - <updateStatusAccount> - BEGIN - accountSfdcId='+myAcc.Id);
		Long Begin_Time = DateTime.now().getTime();
		try {
		    String log = 'none';
		    if( accountBrand==null ){
		        accountBrand='';
		    }
		    System.debug('### Myr_UserActivation_WS.updateStatusAccount : accountBrand=<' + accountBrand +'>');
		    if( accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.RENAULT.name()) || accountBrand == '' ) {
				if( String.isBlank(myAcc.PersEmailAddress__c) ) {
					myAcc.PersEmailAddress__c = myAcc.MyRenaultID__c;
				}
		        myAcc.MYR_Status__c = system.Label.MYR_Status_Activated;
		        log = 'Label.MYR_Status_Active';
		    } else if ( accountBrand.equalsIgnoreCase(Myr_MyRenaultTools.Brand.DACIA.name()) ) {
				if( String.isBlank(myAcc.PersEmailAddress__c) ) {
					myAcc.PersEmailAddress__c = myAcc.MyDaciaID__c;
				}
		        myAcc.MYD_Status__c = system.Label.MYD_Status_Activated;
		        log = 'Label.MYD_Status_Active';
		    }
		    update myAcc;

		    addSyncLog(logRunId, 'setStatusAccount', myAcc.Id, 'Update OK', 'Account updated brand='+log, INDUS_Logger_CLS.ErrorLevel.Info.name(), Inputs, Begin_Time);
		} catch (Exception e) {
		    addSyncLog(logRunId, 'setStatusAccount', myAcc.Id, 'Update Exception', 'Problem upadting asynchronously the account: ' + e.getMessage(), INDUS_Logger_CLS.ErrorLevel.Critical.name(), Inputs, Begin_Time);
		}
	}

	//Synchronization AdobeCampain (Synchro AC)
    private static void AC_Synchronization(Id Account_Id, String Country, String My_Brand) { 
		System.debug('### Myr_UserActivation_WS.AC_Synchronization : Account_Id=<' + Account_Id + '>, Country=<'+ Country + '>, My_Brand=<' + My_Brand + '>');
		try{	        
			if( Myr_AdobeCampain.Is_Org_AC_Synchronizable() && Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand, system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation) ) {
				Myr_AdobeCampain.sync(system.Label.Myr_Synchro_Adobe_Campain_Flow_Activation, My_Brand, Account_Id);
			}else{
				System.debug('### Myr_UserActivation_WS.AC_Synchronization : Myr_AdobeCampain.Is_Org_AC_Synchronizable()=<' + Myr_AdobeCampain.Is_Org_AC_Synchronizable() + '>, Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand,\'Activation\')=<'+ Myr_AdobeCampain.Is_AC_Synchronizable(Country, My_Brand,'Activation'));
			}
		}catch (Exception e){     
			//
		}
    }

	 //Add the logs for this webservice in future mode: avoid the mixed_dml_exception as we could have updated the username of the user
    @future 
    private static void addLog(String runLogId, String myMethod, String myId, String exceptType, String myMessage, String myLevel, Long Begin_Time, String inputs) {
		Id id = INDUS_Logger_CLS.addGroupedLogFull(runLogId, null, INDUS_Logger_CLS.ProjectName.MYR, Myr_UserActivation_WS.class.getName(), myMethod, exceptType, myMessage, INDUS_Logger_CLS.ErrorLevel.Info.name(), myId, null, null, Begin_Time, DateTime.now().getTime(), 'Inputs: <' + inputs + '>');
    }
  
	private static void addSyncLog(String runLogId, String myMethod, String myId, String exceptType, String myMessage, String myLevel, String Inputs, Long Begin_Time) {
        Id id = INDUS_Logger_CLS.addGroupedLogFull(runLogId, null, INDUS_Logger_CLS.ProjectName.MYR, Myr_UserActivation_WS.class.getName(), myMethod, exceptType, myMessage + ' id='+myId, INDUS_Logger_CLS.ErrorLevel.Info.name(), myId, null, null, Begin_Time, DateTime.now().getTime(), 'Inputs: <' + Inputs + '>');
    }
    
    //ATC synchronization
    @future (callout=true)
    private static void ATC_Synchronization(Id Account_Id) { 
        Myr_Synchro_ATC.Change_Myr_Status(Account_Id, 'Activation_On_Account');
    }
}