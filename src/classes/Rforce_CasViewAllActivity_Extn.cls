/*<!--===========================================================================+
 |   Updated History                                                                  
                                                                 
 | DATE         DEVELOPER    ISCRUM ID     WORK REQUEST          DESCRIPTION                               
 | ====         =========    ==========    ===========           =========== 
 | 24/06/2014   Siva Valipi     181       Activity Management    Displaying The Activity History Records in Table format with Sorting and Searching.. . 
 |
 |  
 |
 +===========================================================================*/
public class Rforce_CasViewAllActivity_Extn {

public Id caseId=ApexPages.currentPage().getParameters().get('id');
public List<case> caseDetail{get;set;}
public String whoid_name {get;set;}
public List<task> acHist{get;set;}

//Extension class constructor 
public Rforce_CasViewAllActivity_Extn(ApexPages.StandardController controller) {
    acHist=new List<task>();
    caseDetail=[Select Id,caseNumber,contactId,(Select id,OwnerId,LastModifiedDate,type,createddate,ReminderDateTime,Contains_link_or_attachement__c,ActivityDate,CreatedBy.Name ,Subject,Status,Priority,Description,From_To__c from tasks) from Case where Id=:caseId]; 
  
    for (Case a : caseDetail) {
     // idurl=caseDetail.id;
        for (task  ah : a.getSObjects('tasks')) { 
     
         if((string)a.contactId!= null && ((string)a.contactId).startsWith('003')){
               //whoid_name=[select name from contact where id=:a.contactId].name;  
                    
             }
             acHist.add(ah);     
          
        }
    }
 }
 //This methode Return the whoid_name
 public String getWhoid()
 {

     return whoid_name ;
      
 } 

}