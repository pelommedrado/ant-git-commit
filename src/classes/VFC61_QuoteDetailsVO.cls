/**
* Classe que representa os dados da página de detalhe do orçamento.
* @author Thiago Alves dos Santos.
*/
public class VFC61_QuoteDetailsVO 
{
	/*atributos usados para controle interno, não são utilizados na tela*/
	public String id {get;set;}
	public String opportunityId {get;set;}
	public String oppStatus {get;set;}
	public Boolean vehicleWasReserved {get;set;}
	public String idReservedVehicle {get;set;}
	public String vehicleBookingId {get;set;}
			
	/*flags que indicam se os controles estão ou não habilitados/visíveis*/
	public Boolean addVehicleDisabled {get;set;}

	/*picklists que serão exibidos na tela*/
	public List<SelectOption> lstSelOptionPaymentMethods {get;set;}
	
	/*campos padrões do salesforce*/
	public Quote sObjQuote {get;set;}
	
	/*atributos referentes a seção informações do Orçamento*/
	public String quoteNumber {get;set;}
	public String quoteName {get;set;}
	public Datetime createdDate {get;set;}
	public DateTime expirationDate {get;set;}
	public String status {get;set;}
	
	/*atributos referentes a seção cliente*/
	public String customerName {get;set;}
	public String customerPersonEmail {get;set;}
	public String customerPersLandline {get;set;}
	public String customerPersMobPhone {get;set;}
	
	/*atributos referentes a seção vendedor*/
	public String sallerName {get;set;}
	public String sallerEmail {get;set;}
	public String sallerPhone {get;set;}
	
	/*atributos referentes a seção itens do orçamento*/
	public List<VFC83_QuoteLineItemVO> lstQuoteLineItemVO {get;set;}
	
	/*atributos referentes a seção sintese*/
	public Double entry {get;set;}
	public String entryMask {get;set;}
	
    public Double signalValue { get;set; }
	public String signalValueMask { get;set; }
    
	public Double amountFinanced {get;set;}
	public String amountFinancedMask {get;set;}

	public String usedVehicle {get;set;}
	public String yearUsedVehicle  {get;set;}
	public Double priceUsedVehicle {get;set;}
	public String priceUsedVehicleMask {get;set;}
	
    public Boolean signalValueCheck { get;set; }
	public Boolean cdcCFinancing {get;set;}
	public Boolean lsgFinancing {get;set;}
	public String numberOfParcels {get;set;}
	public Double valueOfParcel {get;set;}
	public String valueOfParcelMask {get;set;}
	
	public Double totalPrice {get;set;}
	public String totalPriceMask {get;set;}
	
	public String paymentMethods {get;set;}
	public String paymentDetails {get;set;}
	public String usedVehicleLicensePlate {get;set;}
	public String usedVehicleModel {get;set;}
	public String usedVehicleBrand {get;set;}
	
	/*atributos referentes a seção documentos básicos necessários - pessoa física*/
	public Boolean identityCard {get;set;}
	public Boolean cpf {get;set;}
	public Boolean proofIncome {get;set;}
	public Boolean proofResidence {get;set;}
	public Boolean lastStatementIncomeTax {get;set;}
	public Boolean lastPaycheckStatement {get;set;}
	public Boolean lastStatementBank {get;set;}
	public Boolean driversLicense {get;set;}

	/*atributos referentes a seção documentos básicos necessários - empresa*/
	public Boolean socialContract {get;set;}
	public Boolean lastStatementIncomeTaxEnterprise {get;set;}
	public Boolean identityCardAllMembers {get;set;}
	public Boolean cpfAllMembers {get;set;}
	public Boolean registrationFormSignedPartners {get;set;}
	public Boolean lastBalanceSheet {get;set;}
	public Boolean balanceLastTwoYears {get;set;}
	public Boolean relationshipFleetDebt {get;set;}
	
	/*atributos referentes a seção documentos Básicos Necessários - Veículo Usado*/
	public Boolean dut {get;set;}
	public Boolean procuration {get;set;}
	public Boolean inspection {get;set;}
	public Boolean finesPaid {get;set;}
	public Boolean publicProcuration {get;set;}
	public Boolean licensing {get;set;}
	public Boolean leaseTermination {get;set;}
	public Boolean ManualSpareKey {get;set;}
    
    //atributos referente a seção de acessórios
    public String accessory1 {get;set;}
    public String accePrice1 {get;set;}
    public String accessory2 {get;set;}
    public String accePrice2 {get;set;}
    public String accessory3 {get;set;}
    public String accePrice3 {get;set;}
	
	
	/**
	* Construtor
	*/
	public VFC61_QuoteDetailsVO()
	{
		this.vehicleWasReserved = false;
		this.sObjQuote = new Quote();
		this.lstQuoteLineItemVO = new List<VFC83_QuoteLineItemVO>();	
	}
}