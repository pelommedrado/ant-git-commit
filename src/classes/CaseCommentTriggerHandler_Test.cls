@isTest
private class CaseCommentTriggerHandler_Test {
    @isTest
    static void itShouldpass() {

        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', CurrencyCode__c = 'EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id, developername from RecordType where sObjectType = 'Account' and developername = 'Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, ProfEmailAddress__c = 'addr1@mail.com');
            insert Acc;
            System.debug('###Acc Id =' + Acc.Id);

        Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Status='New', Description='Trigger test clas', CountryCase__c='Colombia', dealer__c=acc.id);
            insert cs;

            System.debug('###itShouldpass method - Case Id =' + cs.Id);
            CaseComment caseCom = new CaseComment();
            caseCom.ParentId = cs.Id;


            caseCom.CommentBody = 'commentB';
            List<Casecomment> listcomment = new List<Casecomment>();
            try {
                insert caseCom;
                System.debug('###itShouldpass method - Insert Success' + caseCom.Id);
                listcomment.add(caseCom);
            } catch (Exception e) {

            }

            if (listcomment.size() > 0) {
                System.debug('###cod - listsize ###'+listcomment.size());
                CaseCommentTriggerHandler.onBeforeInsert(listcomment);
            }

            Test.stopTest();
        }
    }

    @isTest
    static void itShouldFail() {
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', CurrencyCode__c = 'EUR');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', RecordDefaultCountry__c = 'Colombia', alias = 'lro', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();
            Id RTID_COMPANY = [select Id, developername from RecordType where sObjectType = 'Account' and developername = 'Network_Site_Acc'].Id;
            Account Acc = new Account(Name = 'Test1', Phone = '0000', RecordTypeId = RTID_COMPANY, IDBIR__c = '00001314');
            insert Acc;
        Case cs = new case (Type='Complaint', Origin='RENAULT SITE', Status='New', Description='Trigger test clas', CountryCase__c='Colombia', dealer__c=acc.id);
            insert cs;
            CaseComment caseCom = new CaseComment();
            caseCom.ParentId = cs.Id;
            System.debug('###Case Id =' + cs.Id);
            caseCom.CommentBody = 'commentB';
            List<Casecomment> listcomment = new List<Casecomment>();
            try {
                insert caseCom;
                System.debug('###Insert Success' + caseCom.Id);
                listcomment.add(caseCom);
            } catch (Exception e) {

            }
            CaseCommentTriggerHandler.onBeforeInsert(listcomment);

            Test.stopTest();
        }
    }
    private static testMethod void testCaseCommentInsert() {
        
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c = 'FR_Case_RecType');
        insert ctr;
        User usr = new User (LastName = 'Rotondo', alias = 'lro',RecordDefaultCountry__c='France', Email = 'lrotondo@rotondo.com', BypassVR__c = true, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = Label.PROFILE_SYSTEM_ADMIN, TimeZoneSidKey = 'America/Los_Angeles', UserName = 'lrotondo1@lrotondo.com');
        System.runAs(usr) {
            Test.startTest();

            Id RTID = [select Id from RecordType where sObjectType = 'Account' and DeveloperName = 'CORE_ACC_Personal_Account_RecType' limit 1].Id;
            Account Acc = new Account(FirstName = 'Test1', LastName = 'Acc', Phone = '0000', RecordTypeId = RTID, ProfEmailAddress__c = 'addr1@mail.com', ShippingCity = 'city', ShippingCountry = 'cntry', ShippingState = 'state', ShippingPostalCode = '75013', ShippingStreet = 'my street', ComAgreemt__c = 'Yes');
            insert Acc;
            Case cs = new case (Type='Complaint', Origin='RENAULT SITE', AccountId=Acc.Id, Status='New', Description='Trigger test clas', CountryCase__c='Argentina');
            insert cs;
            CaseComment ccomment = new CaseComment(IsPublished = true, ParentId = cs.Id, CommentBody = 'This is the comment');
            insert ccomment;

            // Adding the dummy Case comment in the List
            list <CaseComment> listCaseComment = new list<CaseComment>();
            listCaseComment.add(ccomment);
            CasecommentTriggerHandler.onAfterInsert(listCaseComment, true, null);
            Test.stopTest();
        }
//Added for test coverage to send an email to dealer contact when a case commemnts are created by SRC               
        Id caid;
        Id RectypeId=[SELECT Id FROM RecordType WHERE DeveloperName = 'Network_Site_Acc'].Id;
        Account ac=new Account(Name='Test Clas Account',IDBIR__c='1233g',RecordTypeId=RectypeId,Country__c='India',DealershipStatus__c='Active' );
        insert ac;
        system.debug('ac-->'+ac.Country__c);
        Contact cn=new Contact(LastName='NwtCon',AccountId=ac.Id,Email = 'testContact@test.com');
        insert cn;
        system.debug('cn-->'+cn);        
        Id profid=[SELECT Id FROM Profile WHERE Name = 'CORE - Service Profile'].Id;

        Id crurid=UserInfo.getUserId();
        System.runAs(usr) {
      
                Case Case1 = new Case  (Origin = 'Dealer', Type = 'Information Request', SubType__c = 'Booking', Status = 'Open', Priority = 'Urgent',
                                        Description = 'Description', From__c = 'Customer', Kilometer__c = 1000000, AccountId = ac.Id,ContactId=cn.Id,Dealer__c=ac.id,DealerContactLN__c=cn.Id);
                insert Case1;                
                caid=Case1.Id;
                Case1.OwnerId=crurid;
                update Case1;
                 
                CaseComment ccomment = new CaseComment(IsPublished = true, ParentId = caid, CommentBody = 'Community comment');
                insert ccomment;                             
        }
          CaseComment ccomment = new CaseComment(IsPublished = true, ParentId = caid, CommentBody = 'Community comment');
          insert ccomment;                  
    }    
}