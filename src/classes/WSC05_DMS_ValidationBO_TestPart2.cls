@isTest
private class WSC05_DMS_ValidationBO_TestPart2
{

	public static WSC04_PreOrderDMSVO buildCorrectPreOrderVO()
    {
		WSC04_PreOrderDMSVO preOrderVO_01 = new WSC04_PreOrderDMSVO();
		preOrderVO_01.CPF_CNPJ = '11111111111';
		preOrderVO_01.RG_StateRegistration = 'kkhgh';
		preOrderVO_01.VIN = '9398893928';
		preOrderVO_01.orderNumber= '766767775';
		preOrderVO_01.status = 'Open';
		preOrderVO_01.orderNumberDMS = '1234567890';
		preOrderVO_01.creationDate =  Date.newInstance(2012, 11, 30);	
	    preOrderVO_01.expirationDate = Datetime.newInstance(2012, 12, 30, 8, 0, 0);	
	    preOrderVO_01.customerName =  'João da Silva';
	    preOrderVO_01.streetAndComplement = 'Avenida Brasil, 1000, apto 201';
	    preOrderVO_01.city =  'Rio de Janeiro';
	    preOrderVO_01.zipCode = '22180000';
	    preOrderVO_01.state = 'RJ';
		preOrderVO_01.country = 'Brasil';
		preOrderVO_01.customerEmail = 'joao.silva@gmail.com.xpto';
		preOrderVO_01.maritalStatus = '1-MARRIED';
		preOrderVO_01.sex = 'M';
		preOrderVO_01.birthDate = Date.newInstance(1980, 5, 15);	
		preOrderVO_01.residencialLandline = '9798337798';
		preOrderVO_01.commercialLandline = '9799377373';
		preOrderVO_01.cellphone = '4594959094';
		preOrderVO_01.phoneSalesperson= '9877900988';
	    preOrderVO_01.CPFSalesperson= '9087668888';
		preOrderVO_01.value =  20000.00;
		preOrderVO_01.accessoriesList =  'Bancos de couro | Sensor de estacionament';
		preOrderVO_01.entryValue = 40000;
		preOrderVO_01.invoiceNumber = '8884484884899';
		preOrderVO_01.paymentType = 'Financing';
		preOrderVO_01.usedCar = 'kkw844848';
		preOrderVO_01.usedCarLicencePlate = 'kkw844';
		preOrderVO_01.fundingAgent = 'kkw844snf';
		preOrderVO_01.fundingType = 'kzdflkslf99';
		preOrderVO_01.nameSalesperson  = 'rammmmmm';
		preOrderVO_01.emailSalesperson  = 'ramesh@gmail.com';
		preOrderVO_01.installmentsNumber = 99;
		
		//preOrderVO_01.customerLastName = 'Silva';

		return preOrderVO_01;
    }


    static testMethod void myUnitTest1()
    {
    	// Prepare test data
		WSC04_PreOrderDMSVO vo1 = buildCorrectPreOrderVO();
		vo1.usedCarYear = 500; // Error D155
		
		WSC04_PreOrderDMSVO vo2 = buildCorrectPreOrderVO();
		vo2.CPFSalesperson = '123456789012345'; // Error D147

		//WSC04_PreOrderDMSVO vo3 = buildCorrectPreOrderVO();
		//vo3.customerLastName = '123456789012345678901234567890123456789012345678901234567890123456789012345678901'; // Error D161
		
		WSC04_PreOrderDMSVO vo4 = buildCorrectPreOrderVO();
		vo4.status = 'wrong status'; // Error D202
		
		WSC04_PreOrderDMSVO vo5 = buildCorrectPreOrderVO();
		vo5.maritalStatus = 'wrong marital status'; // Error D203
		
		WSC04_PreOrderDMSVO vo6 = buildCorrectPreOrderVO();
		vo6.sex = 'yes'; // Error D204
		
		WSC04_PreOrderDMSVO vo7 = buildCorrectPreOrderVO();
		vo7.paymentType = 'magical beans'; // Error D205
		
		WSC04_PreOrderDMSVO vo8 = buildCorrectPreOrderVO();
		vo8.fundingType = 'XXX'; // Error D206

		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_1 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_1.add(vo1);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_2 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_2.add(vo2);
		
		//List<WSC04_PreOrderDMSVO> lstPreOrderPMList_3 = new List<WSC04_PreOrderDMSVO>();
		//lstPreOrderPMList_3.add(vo3);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_4 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_4.add(vo4);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_5 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_5.add(vo5);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_6 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_6.add(vo6);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_7 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_7.add(vo7);
		
		List<WSC04_PreOrderDMSVO> lstPreOrderPMList_8 = new List<WSC04_PreOrderDMSVO>();
		lstPreOrderPMList_8.add(vo8);
		
		// Start test
		Test.startTest();

		List<WSC04_PreOrderDMSVO> lstTest00a = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForPreOrder('','');
		List<WSC04_PreOrderDMSVO> lstTest00b = WSC05_DMS_ValidationBO.getInstance().checkNumberBIRNotEmptyForPreOrder('1','');
		
		List<WSC04_PreOrderDMSVO> lstTest01_1 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_1);
		List<WSC04_PreOrderDMSVO> lstTest01_2 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_2);
		//List<WSC04_PreOrderDMSVO> lstTest01_3 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_3);
		List<WSC04_PreOrderDMSVO> lstTest01_4 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_4);
		List<WSC04_PreOrderDMSVO> lstTest01_5 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_5);
		List<WSC04_PreOrderDMSVO> lstTest01_6 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_6);
		List<WSC04_PreOrderDMSVO> lstTest01_7 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_7);
		List<WSC04_PreOrderDMSVO> lstTest01_8 = WSC05_DMS_ValidationBO.getInstance().checkLstPreOrderDMSVO(lstPreOrderPMList_8);

		// Stop test
		Test.stopTest();
		
		// Test01: D155 - usedCarYear field must be between 2000 and 3000.';
		WSC04_PreOrderDMSVO test01 = lstTest01_1[0];
		//system.assert(test01.errorMessage.contains('D155'), 'Error in D155 ['+test01.errorMessage+']');
		
		// Test02: D147 - CPFSalesperson field too long: maximum size 11 characters
		WSC04_PreOrderDMSVO test02 = lstTest01_2[0];
		//system.assert(test02.errorMessage.contains('D147'), 'Error in D147 ['+test02.errorMessage+']');
		
		// Test03: D161 - customerLastName field too long: maximum size 80 characters.
		//WSC04_PreOrderDMSVO test03 = lstTest01_3[0];
		//system.assert(test03.errorMessage.contains('D161'), 'Error in D161 ['+test03.errorMessage+']');
		
		// Test04: D202 - status field must belong to a value of this list: [Open / Pre Order Requested / Pre Order Sent / Billed / Canceled].
		WSC04_PreOrderDMSVO test04 = lstTest01_4[0];
		system.assert(test04.errorMessage.contains('D202'), 'Error in D202 ['+test04.errorMessage+']');
		
		// Test05: D203 - maritalStatus field must belong to a value of this list: [1-MARRIED / 2-SINGLE / 3-DIVORCED / 4-WIDOWED / 5-UNDEFINED].
		WSC04_PreOrderDMSVO test05 = lstTest01_5[0];
		//system.assert(test05.errorMessage.contains('D203'), 'Error in D203 ['+test05.errorMessage+']');
		
		// Test06: D204 - sex field must belong to a value of this list: [Male / Female / Undefined]
		WSC04_PreOrderDMSVO test06 = lstTest01_6[0];
		//system.assert(test06.errorMessage.contains('D204'), 'Error in D204 ['+test06.errorMessage+']');
		
		// Test07: D205 - paymentType field must belong to a value of this list: [Financing / Leasing / Consortium / Used Vehicle / Pay at Sight / Installment Payment]
		WSC04_PreOrderDMSVO test07 = lstTest01_7[0];
		//system.assert(test07.errorMessage.contains('D205'), 'Error in D205 ['+test07.errorMessage+']');
		
		// Test08: D206 - fundingType field must belong to a value of this list: [CDC / Leasing]
		WSC04_PreOrderDMSVO test08 = lstTest01_8[0];
		//system.assert(test08.errorMessage.contains('D206'), 'Error in D206 ['+test08.errorMessage+']');
    }


	static testMethod void myUnitTest2()
    {
    	// Prepare test data
    	
    	// Start test
		Test.startTest();
		
		WSC05_DMS_ValidationBO.getInstance().consultLstVehicleDMSVO('100x');
		WSC05_DMS_ValidationBO.getInstance().consultLstPreOrder('1000');
		WSC05_DMS_ValidationBO.getInstance().consultLstPreOrder(null);

    	// Stop test
		Test.stopTest();
		
    }
    
}