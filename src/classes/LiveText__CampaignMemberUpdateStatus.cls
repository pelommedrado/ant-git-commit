/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class CampaignMemberUpdateStatus {
    @InvocableVariable(label='Campaign Member Id' description='Campaign Member	to Update' required=true)
    global Id campaignMemberId;
    @InvocableVariable(label='Response' description='Response Type' required=true)
    global String responseType;
    global CampaignMemberUpdateStatus() {

    }
}
