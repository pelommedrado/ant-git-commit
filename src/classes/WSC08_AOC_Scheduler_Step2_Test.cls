/**
*   Test Class    		-   WSC08_AOC_Scheduler_Step2_Test
*   Author              -   Suresh Babu
*   Date                -   20/03/2013
*   
*   #01 <Suresh Babu> <20/03/2013>
*       Test class for WSC08_AOC_Scheduler_Step2 class.
**/

@isTest
private class WSC08_AOC_Scheduler_Step2_Test {

    static testMethod void myUnitTest() {
		String IsTest = '0 0 0 31 12 ? 2020';
	
		Test.startTest();
			String jobId = System.schedule('testScheduled', IsTest, new WSC08_AOC_Scheduler_Step2());
		Test.stopTest();
    }
}