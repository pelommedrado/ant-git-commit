@isTest
private class VFC128_CampaignMemberDAO_Test
{
	private static testMethod void unitTest01()
	{
		// Test Data
		Lead lead = VFC129_UTIL_TestData.createLead();
		insert lead;

		Campaign campaignParent = VFC129_UTIL_TestData.createCampaignParent('CAMPANHA_RENAULT');
		insert campaignParent;
		
		Campaign campaignSingle = VFC129_UTIL_TestData.createCampaignSingle('CAMPANHA_RENAULT_1', null, campaignParent.Id);
		insert campaignSingle;
		
		CampaignMember campaignMember = VFC129_UTIL_TestData.createCampaignMember(campaignSingle.id, lead.Id, null);
		insert campaignMember;
        
        MyOwnCreation moc = new MyOwnCreation();
        Contact contato = moc.criaContato();
        insert contato;

		// Start test
        Test.startTest();

        // Execute tests
        
        // #Test 03
        CampaignMember test01 = VFC128_CampaignMemberDAO.getInstance().
        	fetchLeadCampaignMemberOfThisCampaign(campaignSingle.Id, lead.Id);
        
        // #Test 02
        //List<CampaignMember> test02 =  VFC128_CampaignMemberDAO.getInstance().
        //	fetchCampaignMemberForHeatingPage(new Set<Id>{campaignSingle.Id}, lead.Id);
        
        // #Test 03
        CampaignMember test03 = null;
        try {
        	test03 = VFC128_CampaignMemberDAO.getInstance().
        		fetchLeadCampaignMemberOfThisCampaign(lead.Id, lead.Id);
        }
        catch (Exception e) {}
        
        VFC128_CampaignMemberDAO.getInstance().fetchContactCampaignMemberOfThisCampaign(campaignSingle.Id, contato.Id);
        Set<Id> setCampaignId = new Set<Id>();
        setCampaignId.add(campaignSingle.Id);
        VFC128_CampaignMemberDAO.getInstance().fetchLeadCampaignMemberFor4thRule(setCampaignId, lead.Id);
        VFC128_CampaignMemberDAO.getInstance().fetchContactCampaignMemberFor4thRule(setCampaignId, contato.Id);
        VFC128_CampaignMemberDAO.getInstance().checkIftCampaignMemberShouldBeTransferred(campaignParent.Id, campaignSingle.Id, lead.Id, contato.Id);
        VFC128_CampaignMemberDAO.getInstance().checkIftCampaignMemberShouldBeTransferred(campaignParent.Id, campaignSingle.Id, null, contato.Id);
        Set<Id> setLeadId = new Set<Id>();
        setLeadId.add(lead.Id);
        VFC128_CampaignMemberDAO.getInstance().fetchCMByLeadIdAndCampaignId(setCampaignId, setLeadId);

        // Stop test
        Test.stopTest();

        // Verify data
        system.assert(test01 != null);
        //system.assert(test02 != null);
        system.assert(test03 == null);
	}
}