/**
* Classe que representa exceções geradas na criação do objeto VehicleBooking__c.
* @author Felipe Jesus Silva.
*/
public class VFC88_CreateVehicleBookingException extends Exception 
{
	public VFC88_CreateVehicleBookingException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	} 

}