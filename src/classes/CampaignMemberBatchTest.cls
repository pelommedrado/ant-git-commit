@isTest
private class CampaignMemberBatchTest {
    
    static testmethod void test() {
        
        Id unprocessedRecordType = Utils.getRecordTypeId('InterfaceCampaignMembers__c','Unprocessed');
        
        // The query used by the batch job.
        String query = 'SELECT Id, AddressOK__c, AfterSales__c, AfterSalesOffer__c, BehaviorCustomer__c, Birthdate__c, BusinessPhone__c, ' +
            'CadastralCustomer__c, CampaignCode__c, City__c, ContemplationLetter__c, Country__c, CPF_CNPJ__c, CurrentMilage__c, ' +
            'CustomerProfile__c, DateOfContemplation__c, Detail__c, Email__c, EmailOK__c, EndDateOfFinanciating__c, ErrorMessage__c, ' +
            'FirstName__c, Home_Phone_Web__c, HomePhone__c, InformationUpdatedInTheLast5Years__c, IsRenaultVehicleOwner__c, LastName__c, ' +
            'LeadSource__c, MarketdatasCluster__c, MobilePhone__c, NUMDBM__c, ParticipatedInCampaigns__c, ParticipateOnOPA__c, ' + 
            'PhoneNumberOK__c, PostalCode__c, PreApprovedCredit__c, PropensityModel__c, RecentBirthdate__c, Related_campaign__c, ' + 
            'RFV__c, State__c, Street__c, Sub_Detail__c, SubSource__c, ValueOfCredit__c, VehicleOfCampaign__c, VehicleOfInterest__c, ' +
            'CPF_and_CodCampaing__c FROM InterfaceCampaignMembers__c WHERE RecordTypeId = \'' + unprocessedRecordType + '\'';
        
        // Create some test InterfaceCampaignMembers__c items to be insert by the batch job.
        List<InterfaceCampaignMembers__c> lsIcm = new List<InterfaceCampaignMembers__c>();
        for (Integer i=0;i<10;i++) {
            InterfaceCampaignMembers__c icm = new InterfaceCampaignMembers__c(
                CampaignCode__c = 'Cod' + i,
                CPF_CNPJ__c = '72413718362' + i,
                LastName__c = 'Sobrenome' + i
            );
            lsIcm.add(icm);
        }
        insert lsIcm;
        
        Test.startTest();
        CampaignMemberBatch c = new CampaignMemberBatch(query);
        Database.executeBatch(c);
        Test.stopTest();
        
        //Testa mensagem de retorno para validos
        String message = 'Seu processo foi finalizado com êxito. \n' + 'Registros Válidos: 24000 ou mais registros; \n' +
                'Registros Inválidos: 0; \n' + 'Total aproximado de registros processados: 24000;';
        
        System.assertEquals(message, c.returnMessage(24000,0));
        message = null;
        
        //Testa mensagem de retorno para invalidos
        message = 'Seu processo foi finalizado com êxito. \n' + 'Registros Válidos: 0; \n' +
            'Registros Inválidos: 24000 ou mais registros; \n' + 'Total aproximado de registros processados: 24000;';
        
        System.assertEquals(message, c.returnMessage(0,24000));
        
        // Verify InterfaceCampaignMembers__c items got inserted 
        Integer i = [SELECT COUNT() FROM InterfaceCampaignMembers__c];
        System.assertEquals(i, 10);
    }
}