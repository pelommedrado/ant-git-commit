public class ProfileUtils {
    
    public static String permissionSeller 	= 'BR_SFA_Seller_Agenda';
    public static String permissionDealer 	= 'BR_SFA_Dealer_Manager_Agenda';
    public static String permissionBdc 		= 'BR_SFA_Dealer_BDC_Agenda';
    public static String permissionExec		= 'BR_SFA_Dealer_Executive_Agenda';
    
    public static List<String> permissionAgendaList = new List<String> {
        permissionSeller, permissionDealer, permissionBdc, permissionExec
    };
    
    public static Boolean isSeller {
        get {
            return hasPermission(UserInfo.getUserId(), permissionSeller);
        }
    }
    
    public static Boolean isManager {
        get {
            return hasPermission(UserInfo.getUserId(), permissionDealer);
        }
    }
    
    public static Boolean isManagerBdc {
        get {
            return isManager && hasPermission(UserInfo.getUserId(), permissionBdc);
        }
    }
    
    public static Boolean isManagerExec {
        get {
            return hasPermission(UserInfo.getUserId(), permissionExec);
        }
    }
    
    public static Boolean hasPermission(Id userId, String permissionName) {
        final List<String> permissionNameList = new List<String>{ permissionName };
        return hasPermission(userId, permissionNameList);
    }
    
    public static Boolean hasPermission(Id userId, List<String> permissionNameList) {
        List<PermissionSetAssignment> psets = [
            SELECT AssigneeId FROM PermissionSetAssignment
            WHERE PermissionSetId IN (
                SELECT Id FROM PermissionSet WHERE Name IN: permissionNameList
            ) AND AssigneeId =:userId
        ];
        return psets.size() != 0;
    }
    
    public static Set<Id> userPermission(String permissionName) {
        Set<Id> userIds = new Set<Id>();
        List<PermissionSetAssignment> psets = [
            SELECT AssigneeId FROM PermissionSetAssignment
            WHERE PermissionSetId IN (
                SELECT Id FROM PermissionSet WHERE Name =: permissionName
            ) LIMIT 999
        ];
        for(PermissionSetAssignment psa : psets){
            userIds.add(psa.AssigneeId);
        }
        return userIds;
    }
    
    public static Boolean isUserAgenda(Id userId) {
        return hasPermission(userId, permissionAgendaList);
    }
}