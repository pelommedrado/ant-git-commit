public class VFC149_OfferUtil{
    public VFC149_OfferUtil (){}
    
    
    
    public  double custoEfetivoAoMes(Offer__c offer){
        
        double cet = 0, tec =0, despesaRegistroContrato = Double.valueof(Label.PV_Offer_despesaRegistroContrato), tarifaCadastro = Double.valueof(Label.PV_Offer_tarifaCadastro);
        //valor de entrada
        double valorEntrada = offer.Value_From__c * offer.Minimum_Input__c; 
        //System.out.println("Valor Entrada "+valorEntrada);
        //calcula o valor financiado
        double valorFinanciado = offer.Value_From__c - valorEntrada +(tarifaCadastro+despesaRegistroContrato);
        //System.out.println("Valor financiado "+valorFinanciado);
        // valor auxiliar
        double aux = offer.Coefficient__c * valorFinanciado;
        //System.out.println("aux "+aux);
        //valor da parcela do cliente
        double parcelaCliente = aux+tec;
        //System.out.println("parcela do cliente "+parcelaCliente);
        cet = rate(offer.Number_Of_Installments__c,parcelaCliente,offer.Value_From__c - valorEntrada);
        //System.out.println("cet "+cet);
        
        return cet;
    }
    
    private double rate(decimal nper, double pmt, decimal pv){       
        double error = 0.0000001; 
        double high =  1.00; 
        double low = 0.00;
        double rate =0;
        rate = (2.0 * (nper * pmt - pv)) / (pv * nper);
        
        while(true) {
            // check for error margin
            double calc = Math.pow(1 + rate,(Double) nper);
            calc = (rate * calc) / (calc - 1.0);
            calc -= pmt / pv;
            
            if (calc > error) {
                // guess too high, lower the guess
                high = rate;
                rate = (high + low) / 2;
            } else if (calc < -error) {
                // guess too low, higher the guess
                low = rate;
                rate = (high + low) / 2;
            } else {
                // acceptable guess
                break;
            }
        }
        
        //System.out.println("Rate : "+rate);       
        return rate; 
    }
    
    
    ///método correto prd calculo iof 
    public static double calculoIof(Offer__c offer){
        double cont = 0, pciof = 0, vfst = 0, vfciof = 0, vfsiof = 0, aux=0, iof = 0;
        double despesaRegistroContrato = Double.valueof(Label.PV_Offer_despesaRegistroContrato), tarifaCadastro = Double.valueof(Label.PV_Offer_tarifaCadastro);
        
        
        if(offer.Monthly_Tax__c !=0 && offer.Monthly_Tax__c != null){
            for(Integer i = 0; i < (Double)offer.Number_Of_Installments__c; i++)           
                cont +=  Math.pow((Double)(1 + offer.Monthly_Tax__c),(Double)i);
            
            System.debug('## Cont:'+cont);
            
            //funcPGTO
            if(cont != null && cont != 0){
                //vfst 
                vfst = (offer.ValueTo__c- (((offer.Minimum_Input__c/100) *  offer.ValueTo__c) + (despesaRegistroContrato+tarifaCadastro)));
                System.debug('########## vfst :'+vfst );
                
                // Calculo do pciof ->PMT(offer.Monthly_Tax__c,offer.Number_Of_Installments__c,ValueTo)
                double pvif = Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)),(Double) offer.Number_Of_Installments__c);
                System.debug('Calculo IOF  Monthly_Tax__c :'+  offer.Monthly_Tax__c);
                System.debug('Calculo IOF pvif :'+pvif );
                System.debug('Calculo IOF offer.ValueTo__c :'+offer.ValueTo__c );
                pciof = -((offer.Monthly_Tax__c/100) / (pvif - 1) * -(offer.ValueTo__c* pvif + 0));
                System.debug('############ pciof :'+pciof );
                
                //Função usada para calculo de iof 
                // -FV(offer.Monthly_Tax__c,offer.Number_Of_Installments__c,pciof)
                vfciof = (pciof * (Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)(offer.Number_Of_Installments__c)) - 1) / (offer.Monthly_Tax__c/100) + 0 * Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)offer.Number_Of_Installments__c));
                System.debug('############ vfciof :'+vfciof );
                
                //psiof
                double psiof = offer.ValueTo__c *  offer.Coefficient__c;
                System.debug('############ PSiof :'+psiof );
                
                // Calculo do Valor Futuro -> FV(offer.Monthly_Tax__c, offer.Number_Of_Installments__c, -psiof)
                vfsiof = (psiof * (Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)(offer.Number_Of_Installments__c)) - 1) / (offer.Monthly_Tax__c/100) + 0 * Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double) offer.Number_Of_Installments__c));
                //vfsiof = 52656.15;
                System.debug('############ vfsiof :'+vfsiof );
                //Calculo de iof
                iof = vfsiof - vfciof ;
                
                //armazena o valor da função no campo iof
                //OFF_IOF = String.valueOf(iof);
                iof= iof/2.54;
                
                if(iof<0)
                  iof=  iof*-1;
            }
            
            
        }else{
            double rate = (((offer.Installment_Value__c*offer.Number_Of_Installments__c+offer.Entry_Value__c)-offer.ValueTo__c)-(despesaRegistroContrato+tarifaCadastro));
            iof=rate;
        }
        System.debug('##########Resultado IOF: '+iof);
        
        return iof;
    }
          
    
    
    //usado para calcular o iof -- código caso tenhamos algum erro na implantação 
    /*public static double calculoIof(Offer__c offer){
//if(offer.Monthly_Tax__c == 0.00)
offer.Monthly_Tax__c = 0.001;
double cont = 0, pciof = 0, vfst = 0, vfciof = 0, vfsiof = 0, aux=0, iof = 0;
double despesaRegistroContrato = 97.93, tarifaCadastro = 498;
for(Integer i = 0; i < (Double)offer.Number_Of_Installments__c; i++)           
cont +=  Math.pow((Double)(1 + offer.Monthly_Tax__c),(Double)i);

System.debug('## Cont:'+cont);

//funcPGTO
if(cont != null && cont != 0){
//vfst 
vfst = (offer.ValueTo__c- (((offer.Minimum_Input__c/100) *  offer.ValueTo__c) + (despesaRegistroContrato+tarifaCadastro)));
System.debug('########## vfst :'+vfst );

// Calculo do pciof ->PMT(offer.Monthly_Tax__c,offer.Number_Of_Installments__c,ValueTo)
double pvif = Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)),(Double) offer.Number_Of_Installments__c);
System.debug('Calculo IOF  Monthly_Tax__c :'+  offer.Monthly_Tax__c);
System.debug('Calculo IOF pvif :'+pvif );
System.debug('Calculo IOF offer.ValueTo__c :'+offer.ValueTo__c );
pciof = -((offer.Monthly_Tax__c/100) / (pvif - 1) * -(offer.ValueTo__c* pvif + 0));
System.debug('############ pciof :'+pciof );

//Função usada para calculo de iof 
// -FV(offer.Monthly_Tax__c,offer.Number_Of_Installments__c,pciof)
vfciof = (pciof * (Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)(offer.Number_Of_Installments__c)) - 1) / (offer.Monthly_Tax__c/100) + 0 * Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)offer.Number_Of_Installments__c));
System.debug('############ vfciof :'+vfciof );

//psiof
double psiof = offer.ValueTo__c *  offer.Coefficient__c;
System.debug('############ PSiof :'+psiof );

// Calculo do Valor Futuro -> FV(offer.Monthly_Tax__c, offer.Number_Of_Installments__c, -psiof)
vfsiof = (psiof * (Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double)(offer.Number_Of_Installments__c)) - 1) / (offer.Monthly_Tax__c/100) + 0 * Math.pow((Double)(1 + (offer.Monthly_Tax__c/100)), (Double) offer.Number_Of_Installments__c));
//vfsiof = 52656.15;
System.debug('############ vfsiof :'+vfsiof );
//Calculo de iof
iof = vfsiof - vfciof ;

//armazena o valor da função no campo iof
//OFF_IOF = String.valueOf(iof);
System.debug('##########Resultado IOF: '+iof);
}

return iof;
}*/
    
    
    public  static void CorrectionFieldWhenOfferEqualsInternet(Offer__c oferta){
        System.debug('##'+oferta.Group_Offer__c);
        try{
            List<Group_Offer__c> grupo =[select id,Date_Start_Offer__c,Date_End_Offer__c,Type_of_Action__c from Group_Offer__c where id =: oferta.Group_Offer__c] ;
            for(Group_Offer__c grupoOfertas: grupo)
                if(grupoOfertas!=null && grupoOfertas.Type_of_Action__c.equals('Internet')){
                    Offer__c ofer = oferta;
                    ofer.Offer_to_Internet__c = true;
                    System.debug('####### internet '+ofer.Offer_to_Internet__c);
                    ofer.Offer_Start_Date_Website__c = grupoOfertas.Date_Start_Offer__c;
                    System.debug('####### data inicio '+ ofer.Offer_Start_Date_Website__c);
                    ofer.Offer_End_Date_Website__c = grupoOfertas.Date_End_Offer__c;
                    System.debug('####### data fim '+ofer.Offer_End_Date_Website__c);
                    //update ofer;
                }}catch(Exception e){System.debug('************** '+e);}
        
    }
    //caso mude o status da oferta e ela for upgrade de alguma outra o time stamp ? atualizado
    public void touchOfertaUpgrade(Offer__c oferta){
        List<Offer__c> listaOferta = [select id,PV_Of_LastUpdate__c,Offer_Upgrade__c from Offer__c where id =:oferta.isUpgrade__c];
        List<Offer__c> novaLista = new List<Offer__c>();
        if(listaOferta != null && listaOferta.size()>0)
            for(Offer__c ofertaSelecionada: listaOferta){
                if(ofertaSelecionada.PV_Of_LastUpdate__c)
                    ofertaSelecionada.PV_Of_LastUpdate__c=false;
                else
                    ofertaSelecionada.PV_Of_LastUpdate__c = true;
                novaLista.add(ofertaSelecionada);  
            }
        if(novaLista != null && novaLista.size()>0) 
            update novaLista; 
    }
    //somente para oferta o método abaixo - 
    public void touchOfertaUpgradeOferta(List<Offer__c> oferta){
        
        List<Id> idOfferUpgrade = new List<Id>();
        for(Offer__c offer : oferta)
            if(String.isNotEmpty(offer.isUpgrade__c))
            idOfferUpgrade.add(offer.isUpgrade__c);
        
        
        if(!idOfferUpgrade.isEmpty()){
            List<Offer__c> listaOferta = [select id,PV_Of_LastUpdate__c,Offer_Upgrade__c from Offer__c where id in:(idOfferUpgrade)];
            if(!listaOferta.isEmpty())
                Database.update(listaOferta);
        }
    }
    
    
    
    public static Double calculaCETAM(Offer__c offer){
        double nper = offer.Number_Of_Installments__c; 
        double pmt= offer.Installment_Value__c; 
        double pv = offer.ValueTo__c - offer.Entry_Value__c;
        double error = 0.0000001; 
        double high =  1.00; 
        double low = 0.00;
        
        double rate = (2.0 * (nper * pmt - pv)) / (pv * nper);
        System.debug('###### rate: '+rate);
        while(true) {
            // check for error margin
            double calc = Math.pow(1 + rate,nper);
            System.debug('###### Cacl antes da div '+calc);
            try{
                calc = (rate * calc) / (calc-1);//((calc==1.0?1.1:calc) - 1.0);
            }catch(Exception e){ System.debug('--------------------------- ' + e); break;}
            System.debug('###### Cacl depois da div '+calc);
            calc -= pmt / pv;
            
            if (calc > error) {
                // guess too high, lower the guess
                high = rate;
                rate = (high + low) / 2;
            } else if (calc < -error) {
                // guess too low, higher the guess
                low = rate;
                rate = (high + low) / 2;
            } else {
                // acceptable guess
                break;
            }
        }
        
        //System.out.println("Rate : "+rate);       
        return rate*100; 
    }
    
    public static Double calculaCETAA(Offer__c offer, Double cetAM){
        Double r = cetAM;
        integer nper = 12;
        Double c = 0.0;
        Double pv = -1.0;
        
        Double futureValue = -(c * (Math.pow(1 + (r/100), nper) - 1) / (r/100) + pv
                               * Math.pow(1 + (r/100), nper));
        
        Double result  = futureValue-1.0;
        system.debug('Result = '+result*100);
        return result*100;
    }
}