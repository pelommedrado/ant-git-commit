@isTest
public with sharing class ALR_SharingTrackingTest {

    static testMethod void myUnitTest(){
       
        /*User manager = new User(
          FirstName = 'Test',
          LastName = 'User',
          Email = 'test@org.com',
          Username = 'test@org1.com',
          Alias = 'tes',
          EmailEncodingKey='UTF-8',
          LanguageLocaleKey='en_US',
          LocaleSidKey='en_US',
          TimeZoneSidKey='America/Los_Angeles',
          CommunityNickname = 'testing',
          ProfileId = '00eD0000001o3xD',
          BIR__c ='123ABC123'
        );
        Database.insert( manager );
        System.debug('######## usuário inserido '+manager);*/
        
        List<Account> accList = new List<Account>();
        
        Account dealerAcc = new Account(
              Name = 'Concessionaria teste',
              IDBIR__c = '123ABC123',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        accList.add(dealerAcc);
        
        //Database.insert( dealerAcc );
        System.debug('######## conta inserido '+dealerAcc);
        
        Account dealerAcc2 = new Account(
              Name = 'Concessionaria teste2',
              IDBIR__c = '123ABC124',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        accList.add(dealerAcc2);
        //Database.insert( dealerAcc2 );
        System.debug('######## conta inserido '+dealerAcc2);
        
        Account dealerAcc3 = new Account(
              Name = 'Concessionaria teste3',
              IDBIR__c = '123ABC125',
              RecordTypeId = '012D0000000KAoH',
              Country__c = 'Brazil',
              NameZone__c = 'R2',
              ShippingState = 'SP',
              ShippingCity = 'Sao Paulo',
             // OwnerId = manager.Id,
              Active_PV__c = true,
              isDealerActive__c = true
              
        );
        accList.add(dealerAcc3);
        //Database.insert( dealerAcc3 );
        System.debug('######## conta inserido '+dealerAcc3);
        Database.insert( accList );
        
       List<Account> lsCss = new List<Account>(); 
        
        Account css1 = dealerAcc;
        Account css2 = dealerAcc2;
        Account css3 = dealerAcc3;
      
    /*     Account css1 = new Account();
         Account css2 = new Account();
         Account css3 = new Account();
           css1 = lsCss.get(0);
           css2 = lsCss.get(1);/*
         
        /*Group groupUser = new Group(
          DeveloperName = 'ValecJundiaiPartnerUser',
          DoesIncludeBosses = true,
          DoesSendEmailToMembers = false,
          //RelatedId = userRole.id,
          Type ='Role'
        );
        Database.insert(groupUser);*/
        
        VEH_Veh__c vehicle = new VEH_Veh__c(
          ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            IDBIRSellingDealer__c = css1.IDBIR__c,
            Id_Bir_BuyDealer__c = css2.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'fffffffffffffffff',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'fffffffffffffffff',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        
        
        VEH_Veh__c vehicle2 = new VEH_Veh__c(
          ALR_InvoiceNumber__c = '6775434',
            ALR_SerialNumberOnInvoice__c = '543',
            BVM_data_Upto_date__c =false,
            Color__c = 'PRATA KNH',
            CountryOfDelivery__c ='Brasil',
            DateofManu__c = date.today(),
            IDBIRSellingDealer__c = css1.IDBIR__c,
            Id_Bir_BuyDealer__c = css1.IDBIR__c,
            Is_Available__c = true,
            ModelCode__c = 'B4M',
            ModelYear__c = 2015,
            Model__c = 'Novo Sandero',
            Name = 'ffffffffffffffffa',
            Optional__c = 'ALAR01',
            Price__c =36080.0 ,
            Status__c = 'In Transit',
            Tech_VINExternalID__c = 'ffffffffffffffffa',
            VehicleBrand__c = 'Renault',
            Version__c = 'AUTP10D L2',
            VehicleSource__c = 'SAP'
        );
        
       
       List<VEH_Veh__c>  lstVeicOld = new List<VEH_Veh__c>();
       lstVeicOld.add(vehicle);
       lstVeicOld.add(vehicle2);
       Database.insert(lstVeicOld);
       
       List<VEH_Veh__c>  lstVeicNew = new List<VEH_Veh__c>();
       vehicle.IDBIRSellingDealer__c = css3.IDBIR__c;
       vehicle2.Id_Bir_BuyDealer__c = css3.IDBIR__c;
       vehicle.Status__c = 'Stock';
       lstVeicNew.add(vehicle);
       lstVeicNew.add(vehicle2);
       Database.update(lstVeicNew);

       Map<Id, VEH_Veh__c> vehMap = new Map<Id, VEH_Veh__c>();

       for(VEH_Veh__c veh : lstVeicOld){
          vehMap.put(veh.Id, veh);
       }
       
       ALR_SharingTracking alrSharingTrackng = new    ALR_SharingTracking();
        alrSharingTrackng.validarExisteTracking(lstVeicOld);
      alrSharingTrackng.insertTracking(lstVeicOld);
     alrSharingTrackng.lsAccount = lsCss;
     alrSharingTrackng.lsInTracking = [select id,ALR_Dealer__c,ALR_BuyDealer__c from ALR_Tracking__c where ALR_Chassi__c in:(lstVeicNew)];
      alrSharingTrackng.compartilhar();
      alrSharingTrackng.insertTrackingCaseShare(lstVeicNew);
      ALR_SharingTracking.insertTrackingCaseUpdate(lstVeicNew, vehMap);
      
      List<VEH_Veh__c> lsUpVehicle1 = new List<VEH_Veh__c>();
      List<VEH_Veh__c> lsUpVehicle2 = new List<VEH_Veh__c>();
      for(VEH_Veh__c vehi : lstVeicNew){
      	vehi.Status__c = 'CANCELLED';
      	lsUpVehicle1.add(vehi);
      }
       update(lsUpVehicle1);
       
       for(VEH_Veh__c vehi : lsUpVehicle1){
      	vehi.Status__c = 'In Transit';
      	lsUpVehicle2.add(vehi);
      }
       update(lsUpVehicle2);
        
       /*
       //list Novos velhos
       List<VEH_Veh__c>  lstVeicOld = new List<VEH_Veh__c>();
       lstVeicOld.add(vehicle);
       lstVeicOld.add(vehicle2);
     
     //veiculos novos
     List<VEH_Veh__c>  lstVeicOld = new List<VEH_Veh__c>();
     vehicle.IDBIRSellingDealer__c = lsCss.get(2);
     vehicle2.Id_Bir_BuyDealer__c = lsCss.get(2);
     
     ALR_SharingTracking alrSharingTrackng = new    ALR_SharingTracking();
     alrSharingTrackng.insertTracking(lstVeic);
     compartilharCasoConcessionariaTrocada();
     
        */
        
    }
}