/**
* @author sebastien.ducamp@atos
* @description This class is used to manage the custom setting Country_Info__c without asking repeatedly for ARPE.
* This class automatically display all the MYRrenault fields available for this custom field and the associated description.
* So, the description shoul be clear and comprehensible for everybody
* version 1.0: 16.07 / S�bastien DUCAMP / initial revision
*/
public class Myr_AdministrationGUI_Countries_CTR { 
 
	public Map<String, Country_Info__c> InnerSettings {get; set;}
	public Map<String,Boolean> EditableFields {get; private set;}
	public Map<String,List<String>> CountryFields {get; private set;}
	public Map<String, Myr_AdministrationGUI_Proxify_CLS.ProxySetting> CountriesSettings {get; set;}
	private final CS04_MYR_Settings__c MyrSettings = CS04_MYR_Settings__c.getInstance();
	public Boolean EditMode {get; private set;}
	private Myr_AdministrationGUI_Tools GUITools;

	public String SelectedCountry {get; set;} 

	@testvisible public Boolean ExceptionReadField = false;
	public class Myr_AdministrationGUI_Countries_CTR_Exception extends Exception {}

	/* @constructor **/
	public Myr_AdministrationGUI_Countries_CTR() {
		getSettings();
		EditMode = false; 
	}

	/* Prepare the fixed fields required in the general configuration **/
	private List<String> getFixedFields() {
		List<String> listFixed = new List<String>();
		//Prepare the global settings
		Set<String> objectFields = Schema.SObjectType.Country_Info__c.fields.getMap().keySet();
		try {
			if( Test.isRunningtest() && ExceptionReadField ) {
				throw new Myr_AdministrationGUI_Countries_CTR_Exception('Fake Exception');
			}
			if( !String.isblank(MyrSettings.Myr_GUI_CountryInfoFixedFields__c) ) {
				List<String> otherFields = MyrSettings.Myr_GUI_CountryInfoFixedFields__c.split(',');
				for( String f : otherFields ) {
					//Check that the fields exist, otherwise do not take it into account !
					if( objectFields.contains( f.toLowerCase() ) ) {
						listFixed.add( f );
					} else {
						String message = String.format( system.Label.Myr_GUI_Country_UnexistingFixedField, new List<String>{f});
						ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, message) );
					}
				}
			}
		} catch (Exception e) {
			//continue without working with those fields. Just display a warning message
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, system.Label.Myr_GUI_Country_InvalidFixedField) );
		}
		return listFixed;
	}

	@testvisible private void getSettings() {
		system.debug('#### Myr_AdministrationGUI_Countries_CTR - <getSettings> - BEGIN');
		GUITools = new Myr_AdministrationGUI_Tools();

		//prepare the queryAll functions based only on MYR fields
		Myr_MyRenaultTools.SelectAll queryAll = Myr_MyRenaultTools.buildSelectAllQuery( Country_Info__c.class.getName(), 'MYR' );
		EditableFields = new Map<String,Boolean>();
		for( String field : queryAll.Fields ) {
			EditableFields.put( field, true ); 
		}
		String queryFields = queryAll.Query;
		queryFields = queryFields.removeEndIgnoreCase( 'FROM ' + Country_Info__c.class.getName() ); //remove the end to add other fields
		system.debug('#### Myr_AdministrationGUI_Countries_CTR - <getSettings> - queryFields='+queryFields);
		
		//Fixed fields
		List<String> FixedFields = getFixedFields();
		//System Fields
		FixedFields.addAll( GUITools.getSystemFields() );
		//Update the query
		for( String f : FixedFields ) {
			queryFields += ',' + f;
			EditableFields.put( f.trim(), false );
		}

		//Prepare the groups
		List<String> fields = new List<String>();
		fields.addAll( queryAll.Fields );
		fields.addAll( FixedFields );
		CountryFields = GUITools.buildGroups( fields );
		system.debug('#### Myr_AdministrationGUI_Countries_CTR - <getSettings> - queryFields='+queryFields);
		
		//Query the fields
		String query =  queryFields + ' FROM Country_Info__c ORDER BY Name LIMIT 1000';
		List<Country_Info__c> listCountries = (List<Country_Info__c>) Database.query( query ) ;
		CountriesSettings = new Map<String, Myr_AdministrationGUI_Proxify_CLS.ProxySetting>();
		InnerSettings = new Map<String, Country_Info__c>();
		for( Country_Info__c cInf : listCountries ) {
			CountriesSettings.put( cInf.Name, Myr_AdministrationGUI_Proxify_CLS.proxify(cInf, EditableFields.keySet(), Country_Info__c.class.getName()) );
			InnerSettings.put( cInf.Name, cInf);
		}
	}

	/* @return the List<SelectOption> of the countries available for edition **/
	public List<SelectOption> getCountries() {
		List<SelectOption> options = new List<SelectOption>();
		if( CountriesSettings == null ) return options;
		options.add( new SelectOption( '', '' ) );
		for( String key : CountriesSettings.keySet() ) {
			options.add( new SelectOption(key, key) );
		}
		return options;
	}

	public class LogModification {
		public String name;
		public List<Myr_AdministrationGUI_Proxify_CLS.Modify> Modifications;
		public LogModification( String n, List<Myr_AdministrationGUI_Proxify_CLS.Modify> l ) {
			name = n;
			Modifications = l;
		}
	}

	/* Save the country settings **/
	public void save() {
		//Save the countries
		Country_Info__c cInf = null;
		List<LogModification> listModif = new List<LogModification>();
		try {
			SObject oldObj = CountriesSettings.get(SelectedCountry).original.clone();
			cInf = (Country_Info__c) CountriesSettings.get(SelectedCountry).copySettings();
			if( CountriesSettings.get(SelectedCountry).hasBeenModified() ) {
				listModif.add( new LogModification(cInf.Name,  CountriesSettings.get(SelectedCountry).getModifications() ) );
				update cInf;
			} else {
				String message = String.format( system.Label.Myr_GUI_Country_SaveNothing, new List<String>{cInf.Name} );
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, message) );
				return;
			}
		} catch (Exception e) {
			String message = String.format( system.Label.Myr_GUI_Country_SaveFailed, new List<String>{cInf.Name, e.getMessage()});
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, message ) );
			system.debug('#### Myr_AdministrationGUI_Countries_CTR - <exception> - stack: ' + e.getStackTraceString() );
			return;
		}

		//Log the information
		try {
			INDUS_Logger_CLS.addLog( INDUS_Logger_CLS.ProjectName.MYR
									, Myr_AdministrationGUI_Countries_CTR.class.getName()
									, 'Save Countries'
									, ''
									, 'OK'
									, 'List of Modifications: ' + listModif
									, INDUS_Logger_CLS.ErrorLevel.Info );
		} catch (Exception e) {
			//Do nothing: do not interrompt the treatment because of a logging problem
		}
		String message = String.format( system.Label.Myr_GUI_Country_SaveSuccess, new List<String>{cInf.Name} );
		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, message) );
		getSettings();
		EditMode = false;
	}

	/* Switch to Edit mode **/
	public void edit() {
		EditMode = true;
	}

	/* Exit the edit mode **/
	public void cancel() {
		EditMode = false;
		getSettings();
	}

}