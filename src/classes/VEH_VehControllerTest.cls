@isTest
public class VEH_VehControllerTest {
    static VEH_Veh__c veh = null;
    static Opportunity opp = null;
    static {
        MyOwnCreation moc = new MyOwnCreation();
        
        User u = moc.CriaUsuarioComunidade();
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        
        System.runAs(localUser) {
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = '0PS57000000XEsW';
            psa.AssigneeId = u.id;
            insert(psa);
            
        }
        Account a = moc.criaPersAccount();
        insert a;
        
        opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Lost';
        opp.OwnerId = u.id;
        opp.AccountId = a.Id;
        opp.CloseDate = System.today();
        insert opp;
        
        veh = new VEH_Veh__c();
        veh.Name = '87342098463872416';
        veh.Status__c = 'Billed';
        insert veh;
    }
    public static testMethod void teste1(){
        
        test.startTest();
        MyOwnCreation moc = new MyOwnCreation();
        Pricebook2 pc2 = moc.criaStdPricebook2();
        upsert pc2;
        
        Product2 pd2 = moc.criaProduct2();
        insert pd2;
        
        PricebookEntry pb = moc.criaPricebookEntry();
        pb.Pricebook2Id = pc2.id;
        pb.Product2Id = pd2.id;
        insert pb;
        
        Quote q = moc.criaQuote();
        q.Pricebook2Id = pc2.Id;
        q.OpportunityId = opp.Id;
        q.Status = 'Billed';
        insert q;
        
        QuoteLineItem ql = moc.criaQuoteLineItem();
        ql.PricebookEntryId = pb.Id;
        ql.QuoteId = q.id;
        ql.Vehicle__c = veh.Id;
        ql.Quantity = 1;
        ql.UnitPrice = 35000;
        insert ql;
        
        veh.Status__c = 'In Transit';
        update veh;
        
        test.stopTest();
        
        
    }
    
}