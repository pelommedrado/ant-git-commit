/*  Create a myRenault account, associated vehicles and relations
    Call asynchronously "Personal Data" external web services to get customer informations
    Search existing account into SFDC using by matching keys
    Generate asynchronously a notification "DEALER_ACCOUNT_CREATOR"
    Error codes :     OK              : WS01MS000 
                      OK WITH WARNING : WS01MS001 -> WS01MS500
                      CRITICAL ERROR  : WS01MS501 -> WS01MS999  
*************************************************************************************************************
  25 Aug 2014 : Creation 
  01 Apr 2015 : Add VIN management
  05 May 2015 : R15.07 8.0 PinkBull  : German optins, BCS Personal Data, Merge Russian Dealer subscription
  08 Jul 2015 : Correct personal data call (customeridentificationnbr ? mapper dans ident1)
  24 Aug 2015 : R15.09 8.1 PinkBullBaby : Poland - country parameter is loaded into PL_country__c. Adding rules regarding the personal data webservices call
  22 Sep 2015 : R15.11 9.0 ChocolateCamel : New IN parameters : billingState, billingCountry, secondFirstname; Matching with accountSfdcId disabled; added MDM matching keys for France and Turquie
  16 Nov 2015 : R16.01 9.1 ChocolateCamelBaby, D. Veron (AtoS) : User's login and account's personal email become independent
  18 Mar 2016 : R16.04 10.1 PurpleDragonBaby, D. Veron (AtoS) : Refactoring of logs
  04 May 2016 : R16.07 11.0 BlackEagle, D. Veron (AtoS) : new IN parameter : preferedDealerID
  23 May 2016 : All the fields in entry of manageAccount should be trimmed (CGS)   
*************************************************************************************************************/
global without sharing class Myr_CreateAccount_WS { 
   
private static Long Begin_Time=DateTime.now().getTime();
private static String logRunId = Myr_CreateAccount_WS.class.getName() + Begin_Time;

/** Enum used to simulate some case impossible to reproduce today on certain orgs
    The test is run only if the lastname passed in parameter is equal to the TEST_FAILURE AND Test.isRunningTest OK **/
global enum TEST_FAILURE_SIMULATION { FAILURE_CIN_VALIDRULE, FAILURE_INITIALCHECK,
                                      FAxLUREVxNxSRT, FAxLUREVREUPD, FAxLUREVRExSRT, FAxLUREVxNGENER}

/** Inner class exception **/
private class ManageAccountException extends Exception {}

WebService static Myr_CreateAccount_WS_Response createAccount (Myr_CreateAccount_WS_Question q) { 
        
    //Initialization of inner class object
    Myr_ManageAccount.Inputs_And_Results i = new Myr_ManageAccount.Inputs_And_Results();
	i.accountBrand=removeSpace(q.accountBrand);
    i.country=removeSpace(q.country);
    i.accountSource=removeSpace(q.accountSource);
    i.accountSubSource=removeSpace(q.accountSubSource);
    i.accountSubSubSource=removeSpace(q.accountSubSubSource);
    i.city=removeSpace(q.city);
    i.addressStreet=removeSpace(q.addressStreet);
    i.zipCode=removeSpace(q.zipCode);
    i.billingState=removeSpace(q.billingState);
    i.billingCountry=removeSpace(q.billingCountry);
    if (q.emailAddress!=null) {i.emailAddress=removeSpace(q.emailAddress).toLowerCase();}
    i.landLine1=removeSpace(q.landLine1);
    i.mobilePhone1=removeSpace(q.mobilePhone1);
    i.firstName=removeSpace(q.firstName);
    i.secondFirstname=removeSpace(q.secondFirstname);
    i.lastName=removeSpace(q.lastName);
    i.secondLastName=removeSpace(q.secondLastName);
    i.salutationDigital=removeSpace(q.salutationDigital);
    i.title=removeSpace(q.title);
    i.customerIdentificationNbr=removeSpace(q.customerIdentificationNbr);
    i.vin=removeSpace(q.vin);
    i.generalOptin=removeSpace(q.generalOptin);
    i.channelEmail=removeSpace(q.channelEmail);
    i.channelSMS=removeSpace(q.channelSMS);
    i.channelPhone=removeSpace(q.channelPhone);
    i.channelPost=removeSpace(q.channelPost);
    i.accountSfdcId=removeSpace(q.accountSfdcId); 
    i.goldenId=removeSpace(q.goldenId);
    i.localDatabaseID=removeSpace(q.localDatabaseID);
    i.subscriberDealerID=removeSpace(q.subscriberDealerID);
    i.subscriberDealerIPN=removeSpace(q.subscriberDealerIPN);
    i.registrNbr=removeSpace(q.registrNbr);
    i.registrNbrFirstDate=removeSpace(q.registrNbrFirstDate);
	i.preferedDealerID=removeSpace(q.preferedDealerID);
    i.a = new Account();
	i.WebServiceCalled='Myr_CreateAccount_WS';

	Myr_ManageAccount manageAccount = new Myr_ManageAccount(i);
	Myr_CreateAccount_WS_Response response = upgradeResponse( manageAccount.createAccount() );
	return logAndExit(response, i);

}

// All the fields in entry of createAccount should be trimmed
private static string removeSpace(String str) {
	if (str != null && !String.isBlank(str)){
		str = str.trim();
	}
	return str; 
}


private static Myr_CreateAccount_WS_Response upgradeResponse (Myr_ManageAccount.Myr_ManageAccount_Response r) {
    Myr_CreateAccount_WS_Response_Msg info = new Myr_CreateAccount_WS_Response_Msg();
    Myr_CreateAccount_WS_Response rep = new Myr_CreateAccount_WS_Response();
    if( r!=null && r.info!=null ) {
        rep.accountSfdcId=r.accountSfdcId;
        info.code=r.info.code;
        info.message=r.info.message;
        rep.info=info; 
    }
    return rep; 
}

global class Myr_CreateAccount_WS_Question {
    WebService String accountBrand;
    WebService String country;
    WebService String accountSource;
    WebService String accountSubSource;
    WebService String accountSubSubSource;
    WebService String city;
    WebService String addressStreet;
    WebService String zipCode;
    WebService String billingState;
    WebService String billingCountry;
    WebService String emailAddress;
    WebService String landLine1;
    WebService String mobilePhone1;
    WebService String firstName;
    WebService String secondFirstname;
    WebService String lastName;
    WebService String secondLastName;
    WebService String salutationDigital;
    WebService String title;
    WebService String customerIdentificationNbr;
    WebService String vin;
    WebService String generalOptin;
    WebService String channelEmail;
    WebService String channelSMS;
    WebService String channelPhone;
    WebService String channelPost;
    WebService String accountSfdcId;
    WebService String goldenId;
    WebService String localDatabaseID;
    WebService String subscriberDealerID;
    WebService String subscriberDealerIPN;
    WebService String registrNbr;
    WebService String registrNbrFirstDate;
	WebService String preferedDealerID; 
}

global class Myr_CreateAccount_WS_Response {
    WebService String accountSfdcId;
    WebService Myr_CreateAccount_WS_Response_Msg info; 
}

global class Myr_CreateAccount_WS_Response_Msg { 
    WebService String code;
    WebService String message;
}

private static Myr_CreateAccount_WS_Response logAndExit(Myr_CreateAccount_WS_Response response, Myr_ManageAccount.Inputs_And_Results i) {
	INDUS_Logger_CLS.addGroupedLogFull(logRunId, i.idParentLog, INDUS_Logger_CLS.ProjectName.MYR, 'Myr_CreateAccount_WS', 'Myr_CreateAccount_WS Output', response.info.code, response.info.message, i.ErrorLevel, i.a.Id, null, null, Begin_Time, DateTime.now().getTime(), + 'Inputs : <' + i.inputs + '>'); 
	return response;
}

}