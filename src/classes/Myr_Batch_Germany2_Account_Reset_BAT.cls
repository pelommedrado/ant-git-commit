/*****************************************************************************************************
  Purge of german myRenault accounts
******************************************************************************************************
31 Mar 2016 : Creation 
******************************************************************************************************/
global class Myr_Batch_Germany2_Account_Reset_BAT extends INDUS_Batch_AbstractBatch_CLS { 

	Global String Purge_Mode;
	Global String Account_Ids='';
	Global String The_Brand;

	global Myr_Batch_Germany2_Account_Reset_BAT(String Brand) {
		The_Brand=Brand;
	}


    global Database.QueryLocator start(Database.BatchableContext info){
		String RetentionDay = Country_Info__c.getInstance('Germany2').Myr_Deleting_Retention__c;
		String query = 'Select Id from Account where country__c=\'Germany2\' and ((';
		if (The_Brand.equalsIgnoreCase('DACIA')) { 
			query += 'Myd_Status__c=\'' + system.Label.Myd_Status_Deleted + '\' and MYD_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myr_Status__c!=\'' + system.Label.Myr_Status_Deleted + '\' and Myr_Status__c!=null';
		} else if (The_Brand.equalsIgnoreCase('RENAULT')) {
			query += 'Myr_Status__c=\'' + system.Label.Myr_Status_Deleted + '\' and MYR_Status_UpdateDate__c<LAST_N_DAYS:' + RetentionDay + ' and ';
			query += 'Myd_Status__c!=\'' + system.Label.Myd_Status_Deleted + '\' and Myd_Status__c!=null';
		}
		query += '))';
		query += Myr_Batch_Germany2_ToolBox.Add_Governor_Limit();
		System.debug('Myr_Batch_Germany2_Account_Reset_BAT.start query = ' + query);
        return Database.getQueryLocator(query);
    }

    global override DatabaseAction doExecute(Database.BatchableContext info, List<SObject> scope){
		System.debug('Myr_Batch_Germany2_Account_Reset_BAT.doExecute');

		List<Account> dbUserToUpdateList = new List<Account>();
        for( sObject record : scope )
        {
            Account dbUserToUpdate = (Account) record;
			Myr_Batch_Germany2_ToolBox.Set_Empty(dbUserToUpdate, The_Brand);
            dbUserToUpdateList.add(dbUserToUpdate);
            
        }
        return new DatabaseAction(dbUserToUpdateList, Action.ACTION_UPDATE, false);
    }

    global override void doFinish(Database.BatchableContext info){ 
        System.debug('Myr_Batch_Germany2_Account_Reset_BAT.doFinish');
    }
    global override String getClassName(){
        return Myr_Batch_Germany2_Account_Reset_BAT.class.GetName();
    }
}