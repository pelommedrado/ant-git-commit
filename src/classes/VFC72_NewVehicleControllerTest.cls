/**
    Date: 03/04/2013
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC72_NewVehicleControllerTest 
{
    static User usuario;
    
    static {
        Profile perfil = [SELECT Id FROM Profile WHERE Name =: 'Administrador do sistema' or Name = 'System Administrator' limit 1];
        
        //insere um usuário
        usuario = new User(FirstName = 'Nome',
                           LastName = 'Sobrenome',
                           Alias = 'NSobre',
                           Email = 'nomeEl@sobrenome.com',
                           Username = 'nomeEl@sobrenome.com',
                           CommunityNickname = '123jjjuuu',
                           EmailEncodingKey = 'ISO-8859-1',
                           TimeZoneSidKey = 'GMT',
                           LocaleSidKey = 'en_Us',
                           LanguageLocaleKey= 'en_Us',
                           ProfileId = perfil.Id,
                           isCac__c = true,
                           IsActive = true);
        insert usuario;
    }
    
    static testMethod void myUnitTest() 
    {
        //insere conta
        String acc_devName = 'Personal_Acc';
        /* get Account record type id using developer name */
        Id accountRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName(acc_devName);
        Account accountToInert = new Account();
        accountToInert.FirstName = 'Test';
        accountToInert.LastName = 'Accout';
        accountToInert.PersEmailAddress__c = 'testemail1@email.com';
        accountToInert.PersLandline__c = '';
        accountToInert.VehicleInterest_BR__c = 'Duster';
        accountToInert.PersMobPhone__c = '9987364634';
        accountToInert.RecordTypeId = accountRecordTypeId;
        accountToInert.VehicleInterest_BR__c = 'Duster';
        accountToInert.OwnerId = usuario.Id;
        insert accountToInert;
        
        //Insere conta 2
        /* get Account record type id using developer name */
        Id accountRecordTypeId2 = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName('Network_Site_Acc');
        Account accountToInert2 = new Account();
        accountToInert2.Name = 'topi';
        accountToInert2.PersEmailAddress__c = 'testemail1@email.com';
        accountToInert2.PersLandline__c = '';
        accountToInert2.VehicleInterest_BR__c = 'Duster';
        accountToInert2.PersMobPhone__c = '9987364634';
        accountToInert2.RecordTypeId = accountRecordTypeId2;
        accountToInert2.VehicleInterest_BR__c = 'Duster';
        accountToInert2.OwnerId = usuario.Id;
        accountToInert2.IDBIR__c = '254875';
        insert accountToInert2;     
        
        // selecionar Standard price Book (seeAllData)
    	Pricebook2 pb2 = [select id from Pricebook2 where IsStandard = true limit 1];
            
        //Insere oportunidade
        //String opp_devName = 'DVR';
        /* get Oppourtunity record type id using developer name */
        //Id oppRecordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( opp_devName );
        
        Id oppRecordTypeId = [
            SELECT r.SobjectType, r.Name, r.IsActive, r.Id, r.DeveloperName, r.Description 
            FROM RecordType r 
            WHERE r.DeveloperName ='DVR' AND SobjectType = 'Opportunity'].Id;
        
        Opportunity oppRecord = new Opportunity();
        oppRecord.Name = 'Test Opportunity';
        oppRecord.SourceMedia__c = 'Radio';
        oppRecord.CloseDate = system.today() + 30;
        oppRecord.StageName = 'Identified';
        oppRecord.OpportunitySource__c = 'NETWORK';
        oppRecord.OpportunitySubSource__c = 'THROUGH';
        oppRecord.Amount = 10000;
        oppRecord.RecordTypeId = oppRecordTypeId;
        oppRecord.AccountId = accountToInert.Id;
        oppRecord.Dealer__c = accountToInert2.Id;
        oppRecord.Pricebook2Id = pb2.Id;
        insert oppRecord;   
        
        //insere produtos
        Id recordTypeId1 = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'PDT_Model' );
        Product2 produto = new Product2();
        produto.Name = 'Fluence';
        produto.RECORDTYPEID = recordTypeId1;
        produto.ModelSpecCode__c='Test1';
        produto.Description = 'Fluence model.';
        insert produto;
         
        //insere produto 2
        Id recordTypeId = VFC08_RecordTypeDAO.getInstance().fetchRecordTypeIdUsingDevName( 'PDT_ModelVersion' );
        Product2 produto2 = new Product2();
        produto2.Name = 'Fluence Dynamique 2012/2013';
        produto2.RECORDTYPEID = recordTypeId;
        produto2.ModelSpecCode__c='Test2';
        produto2.Description = 'Fluence model.';
        insert produto2;        

        //insere cotação
        Quote quoteInsert = new Quote();
        quoteInsert.Name = 'Test';
        quoteInsert.OpportunityId = oppRecord.Id;
        quoteInsert.Pricebook2Id = pb2.Id;
        insert quoteInsert;

        // selecionar pricebookentry (seeAllData)
        PricebookEntry pBookEntry = [SELECT Id, UnitPrice, CurrencyIsoCode 
                              FROM PricebookEntry 
                              WHERE Product2.RecordType.DeveloperName = 'PDT_ModelVersion' 
                              AND Pricebook2Id =: pb2.Id
                              AND IsActive = true
                              and CurrencyIsoCode = 'BRL'
                              limit 1];

        // criar item de cotação
        QuoteLineItem qLineItem = new QuoteLineItem();
        qLineItem.QuoteId = quoteInsert.Id;
        qLineItem.PricebookEntryId = pBookEntry.Id;
        qLineItem.Quantity = 1;
        qLineItem.UnitPrice = pBookEntry.UnitPrice;     
                
        system.debug('>>> quoteInsert'+quoteInsert);
        system.debug('>>> produto'+produto);
        
        //Construtor
        ApexPages.StandardController stdContnull = new ApexPages.StandardController(quoteInsert);       
        VFC72_NewVehicleController newVehicle = new VFC72_NewVehicleController(stdContnull);
        VFC94_VehicleItemVO vehicleItemVO = new VFC94_VehicleItemVO();
            
        newVehicle.initializeVehicle();
        
        newVehicle.VehicleVO.itemModel = ''; 
        newVehicle.vehicleVO.lstVehico = new List<VFC94_VehicleItemVO>();
        newVehicle.processModelChange();

        //newVehicle.VehicleVO.itemModel = 'Duster';
        newVehicle.VehicleVO.itemModel = produto.Id; 
        newVehicle.processModelChange();
        newVehicle.getVehicles();

        /*newVehicle.modelSelectList();
        newVehicle.versionSelectList();
        newVehicle.YearSelectList();
        newVehicle.colorSelectList();
        newVehicle.vehiclesAvailable();*/
        vehicleItemVO.isCheckVehico = true;
        newVehicle.includeItemVehicle();
        newVehicle.includeItemGeneric();
        newVehicle.cancelVehico();          
    }
}