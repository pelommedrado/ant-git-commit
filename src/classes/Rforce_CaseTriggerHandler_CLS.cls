/*****************************************************************************************
    Name    : Rforce_CaseTriggerHandler_CLS
    Desc    : Case Trigger Handler
    Approach:
    Author  : Praneel PIDIKITI (Atos Integration)
    Project : Rforce
******************************************************************************************/

public with sharing class Rforce_CaseTriggerHandler_CLS {

    public static void onafterinsert(list < Case > listCase) {

        // Functionality : Sending mail to Delaerwhen the Case lookup is filled 
        //wont work in case of bulk loading  
        if (listCAse.size() == 1 && listCAse[0].Dealer__c != null) {
            // Verifying there is only one case in the context
        Account acc2 = [Select Id, country__c, ProfEmailAddress__c from Account where Id = :listCAse[0].Dealer__c];
        // Getting the dealer id 
        if (listCAse[0].CountryCase__c == 'Colombia' && acc2.ProfEmailAddress__c != null) {
            System.debug('###In SenEmailHelper2 Class### with CaseId value =' + listCAse[0].Id);
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Strings to hold the email addresses to which you are sending the email.
            // Adding professional email address to the list
            String[] toAddresses = new String[] {acc2.ProfEmailAddress__c};
            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(toAddresses);
            // Specify the name used as the display name.
            mail.setSenderDisplayName('Salesforce Support');
            // Specify the subject line for your email address.
            mail.setSubject('New Case Created : ' + listCAse[0].casenumber );
            // Set to True if you want to BCC yourself on the email.·
            mail.setBccSender(false);
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            mail.setUseSignature(false);
            // Specify the text content of the email.
            mail.setPlainTextBody('Your Case:  has been created.');
            mail.setHtmlBody('Your case:<b>' + listCAse[0].casenumber + '  </b>has been created.<p>' +
                             'To view your case <a href=System.label.Rforce_loginURL' + listCAse[0].Id + '>click here.</a>');
            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

    }



     public static void onafterupdate(list < Case > listCase) {

        // Functionality : Sending mail to Delaerwhen the Case lookup is filled 
        //wont work in case of bulk loading  
        if (listCAse.size() == 1 && listCAse[0].Dealer__c != null) {
            // Verifying there is only one case in the context
        Account acc2 = [Select Id, country__c, ProfEmailAddress__c from Account where Id = :listCAse[0].Dealer__c];
        // Getting the dealer id 
        if (listCAse[0].CountryCase__c == 'Colombia' && acc2.ProfEmailAddress__c != null) {
            System.debug('###In SenEmailHelper2 Class### with CaseId value =' + listCAse[0].Id);
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Strings to hold the email addresses to which you are sending the email.
            // Adding professional email address to the list
            String[] toAddresses = new String[] {acc2.ProfEmailAddress__c};
            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(toAddresses);
            // Specify the name used as the display name.
            mail.setSenderDisplayName('Salesforce Support');
            // Specify the subject line for your email address.
            mail.setSubject('New Case Created : ' + listCAse[0].casenumber );
            // Set to True if you want to BCC yourself on the email.·
            mail.setBccSender(false);
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            mail.setUseSignature(false);
            // Specify the text content of the email.
            mail.setPlainTextBody('Your Case:  has been created.');
            mail.setHtmlBody('Your case:<b>' + listCAse[0].casenumber + '  </b>has been created.<p>' +
                             'To view your case <a href=System.label.Rforce_loginURL' + listCAse[0].Id + '>click here.</a>');
            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

    }
}