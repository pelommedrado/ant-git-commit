/**
* Interface que especifica um método para execução de ações disparadas por triggers.
* Cada objeto deverá ter somente uma Trigger, e para cada operação distinta (before insert, after update etc), deverá ser criada
* uma classe que implemente essa interface. 
* @author Felipe Jesus Silva.
*/
public interface VFC37_TriggerExecution 
{
	/**
	* Executa uma ação disparada por uma trigger em uma deteminada operação.
	* As classes que implementarem essa interface deverão fazer o cast para o tipo correto do objeto que se quer trabalhar.
	* Ex: Se a trigger for no objeto account, para obter a lista de dados atuais é necessário colocar a instrução abaixo:
	* List<Account> lstAccount = (List<Account>) lstNewtData. O mesmo vale para os mapas:
	* Map<Id, Account> mapAccount = (Map<Id, Account>) mapNewData.
	* @param lstNewtData A lista de SObjects contendo os dados atuais (trigger.new).
	* @param lstOldData A lista de SObjects contendo os dados anteriores (trigger.old).
	* @param mapNewData O mapa contendo os dados atuais, tendo o id do objeto como chave e o próprio objeto como valor (trigge.newMap).
	* @param mapOldData O mapa contendo os dados anteriores, tendo o id do objeto como chave e o próprio objeto como valor (trigge.newMap).
	*/
	void execute(List<SObject> lstNewtData, List<SObject> lstOldData, Map<Id, SObject> mapNewData, Map<Id, SObject> mapOldData);
}