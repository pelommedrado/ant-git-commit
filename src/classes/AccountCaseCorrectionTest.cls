@isTest
private class AccountCaseCorrectionTest {
	
	@TestSetup static void setup(){
		Id recType = Utils.getRecordTypeId('Account', 'Personal_Acc');

		Account acc = new Account();
		acc.FirstName = 'test';
		acc.LastName = 'Account';
		acc.Phone = '12345678';
		acc.RecordTypeId = recType;
		acc.HomePhone__c = '12345678';
		Database.insert(acc);

		Case caso = new Case();
		caso.AccountId = acc.id;
		caso.Spam__c = true;
		caso.Type = 'Complaint';
		caso.Status = 'Closed';
		caso.Closure_reason__c = 'Duplicate';
		caso.CatalogFuncCode__c = 'abc';
		caso.From__c = 'Customer';
		caso.SubType__c = 'Card';
		caso.ProductFault__c = 'Belts';
		caso.SubProductFault__c = '0120: Body: noise (not wind)';
		caso.Answer__c = 'abc';
		Database.insert(caso);

		Case caso2 = new Case();
		caso2.AccountId = acc.id;
		caso2.Spam__c = true;
		caso2.Type = 'Complaint';
		caso2.Status = 'Closed';
		caso2.Closure_reason__c = 'Duplicate';
		caso2.CatalogFuncCode__c = 'abc';
		caso2.From__c = 'Customer';
		caso2.SubType__c = 'Card';
		caso2.ProductFault__c = 'Belts';
		caso2.SubProductFault__c = '0120: Body: noise (not wind)';
		caso2.Answer__c = 'abc';
		Database.insert(caso2);
	}

	@isTest static void shoudRunBatch() {
		Test.startTest();
		ID batchprocessid = Database.executeBatch(new AccountCaseCorrectionBatch(), 50);
		Test.stopTest();

		List<Account> accCheck = [SELECT Id, FirstName, StopComRCFrom__c, StopComRCTo__c FROM Account];
		List<Case> caseCheck = [SELECT Id, CreatedDate FROM Case ORDER BY CreatedDate ASC];

		System.assertEquals(caseCheck[0].CreatedDate, accCheck[0].StopComRCFrom__c);
		System.assertEquals(caseCheck[1].CreatedDate, accCheck[0].StopComRCTo__c);
	}

	@isTest static void shoudScheduleBatch() {
		AccountCaseCorrectionScheduled instance = new AccountCaseCorrectionScheduled();

		Test.startTest();
		String sch='0 5 2 * * ?';
		System.schedule('Batch Schedule', sch , instance);
		Test.stopTest();
	}
	
	
}