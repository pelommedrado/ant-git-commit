public with sharing class Utils_MaintDataSource {

    public Boolean Test;
    
      public WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse getMaintenanceData(VFC05_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
         
         return getMaintenanceData(VehController.getVin());
      }
         
      public WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse getMaintenanceData(String vin) {     
        // --- PRE TREATMENT ----
        WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullRequest request = new WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullRequest();       
        WS03_fullRepApvBserviceRenault.ServicePreferences servicePref = new WS03_fullRepApvBserviceRenault.ServicePreferences();

        //servicePref.vin = VehController.getVin();
        servicePref.vin = vin;    
        //servicePref.codLanguage = label.CodLanguage; //servicePref.codLanguage = 'FRA'; 
        servicePref.codLanguage = UserInfo.getLanguage().substring(0, 2).toUpperCase();
        request.ServicePreferences = servicePref;
    
        // ---- WEB SERVICE CALLOUT -----    
        WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFull MaintWS = new WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFull();
        MaintWS.endpoint_x = System.label.VFP05_MaintenanceURL;
        MaintWS.clientCertName_x = System.label.RenaultCertificate;     
        MaintWS.timeout_x=40000;
        
        WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse Maint_WS = new WS03_fullRepApvBserviceRenault.ApvGetDonPgmEntVinFullResponse();    
    
        if (Test==true) {
            Maint_WS = Utils_Stubs.MaintStub();     
        } else {
            Maint_WS = MaintWS.getApvGetDonPgmEntVinFull(request);
        }
        
         return Maint_WS;   
     
    }
}