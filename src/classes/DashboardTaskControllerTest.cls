@IsTest
private class DashboardTaskControllerTest {

    private static User userManager 	= null;
    private static Account accDealer 	= null;
    private static Account accPerson 	= null;

    private static Task task 		= null;
    private static Event event 		= null;
    private static Opportunity opp 	= null;

    static {
        Test.startTest();

        final User userRun = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.RunAs(userRun) {

            //criar conta Dealer
            accDealer = AccountBuild.getInstance()
                .createAccountDealer();
            INSERT accDealer;

            //criar contato para a conta Dealer
            final Contact ctt = ContactBuild.getInstance()
                .createContact(accDealer.Id);
            INSERT ctt;

            //criar um usuario
            final Id profileId = Utils.getProfileId('BR - Renault + Cliente Manager');
            userManager = UserBuild.getInstance()
                .createUser(ctt.Id, profileId);
            INSERT userManager;

            accPerson = AccountBuild.getInstance()
                .createAccountPersoal();
            INSERT accPerson;

            opp = OpportunityBuild.getInstance()
                .createOpportunity(userManager.Id, accPerson.Id, accDealer.Id);
            INSERT opp;

            task = TaskBuild.getInstance()
                .createTask(userManager.Id, opp.id);
            INSERT task;

            event = EventBuild.getInstance()
                .createEvent(userManager.Id, opp.id);
            INSERT event;
        }
        Test.stopTest();
    }

    static testMethod void testNewControllerSeller() {
        insertPermission(userManager, ProfileUtils.permissionSeller);
        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
        }
    }

    static testMethod void testNewControllerManager() {
        insertPermission(userManager, ProfileUtils.permissionDealer);
        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
        }
    }

    static testMethod void testNewControllerManagerBdc() {
        insertPermission(userManager, ProfileUtils.permissionDealer);
        insertPermission(userManager, ProfileUtils.permissionBdc);
        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
        }
    }

    static testMethod void testNewControllerManagerExec() {
        insertPermission(userManager, ProfileUtils.permissionExec);
        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
        }
    }

    static testMethod void testNewControllerNegociacao() {
        insertPermission(userManager, ProfileUtils.permissionExec);
        System.runAs(userManager) {
            System.currentPageReference().getParameters().put('aba', 'quote');
            DashboardTaskController ctl = new DashboardTaskController();
        }
    }

    static testMethod void testBusca() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            ctl.buscarTask();
        }
    }

    static testMethod void testBuscaComData() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            ctl.dataIniString = '2017/02/01';
            ctl.dataFimString = '2017/02/01';
            ctl.buscarTask();
        }
    }

    static testMethod void testBuscarTaskBDC() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            // final Account accPerson = AccountBuild.getInstance()
            //     .createAccountPersoal();
            // INSERT accPerson;
            final Opportunity oppNew = OpportunityBuild.getInstance()
                .createOpportunity(userManager.Id, accPerson.Id, accDealer.Id);
            oppNew.RescueOpportunity__c = true;
            INSERT oppNew;

            DashboardTaskController ctl = new DashboardTaskController();
            ctl.buscarTaskBDC();
        }
    }

    static testMethod void testChangeDealer() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            ctl.changeDealer();
        }
    }

    static testMethod void testChangeSeller() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            ctl.changeSeller();
        }
    }

    static testMethod void testSelectOption() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            List<SelectOption> sOL  = ctl.temperaturaList;
            List<SelectOption> sOL1 = ctl.itemModelo;
            List<SelectOption> sOL2 = ctl.itemAtividade;
        }
    }

    static testMethod void testSort() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('table', 'quote');
            ctl.selectTableMain();
            ctl.sortSelect();
            System.currentPageReference().getParameters().put('table', 'task');
            ctl.selectTableMain();
            ctl.sortSelect();
        }
    }

    static testMethod void testAbrirModalTask() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('taskId', task.Id);
            ctl.abrirModalTask();
            System.currentPageReference().getParameters().put('taskId', event.Id);
            ctl.abrirModalTask();
        }
    }

    static testMethod void testAbrirModalOpp() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            ctl.abrirModalOpp();
        }
    }

    static testMethod void testAbrirModalBDC() {
        insertPermission(userManager, ProfileUtils.permissionBdc);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            System.currentPageReference().getParameters().put('actSubj', 'Persecution');
            ctl.abrirModalBDC();
        }
    }

    static testMethod void testAbrirModalLead() {
        insertPermission(userManager, ProfileUtils.permissionBdc);

        System.runAs(userManager) {
            final Lead lead = LeadBuild.getInstance().createLead(accDealer.Id);
            INSERT lead;
            final Task taskLead = TaskBuild.getInstance().createTask(userManager.Id);
            taskLead.WhoId = lead.Id;
            INSERT taskLead;
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('taskId', taskLead.Id);
            ctl.abrirModalLead();
        }
    }

    static testMethod void testAbrirLembrete() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            final Lead lead = LeadBuild.getInstance().createLead(accDealer.Id);
            INSERT lead;
            final Task taskLead = TaskBuild.getInstance().createTask(userManager.Id);
            taskLead.WhoId = lead.Id;
            INSERT taskLead;
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('LemId', taskLead.Id);
            System.currentPageReference().getParameters().put('lead', lead.Id);
            ctl.abrirLembrete();
        }
    }

    static testMethod void testModalEditar() {
        insertPermission(userManager, ProfileUtils.permissionBdc);

        System.runAs(userManager) {
            final Lead lead = LeadBuild.getInstance().createLead(accDealer.Id);
            INSERT lead;
            final Task taskLead = TaskBuild.getInstance().createTask(userManager.Id);
            taskLead.WhoId = lead.Id;
            INSERT taskLead;
            DashboardTaskController ctl = new DashboardTaskController();

            System.currentPageReference().getParameters().put('taskId', taskLead.Id);
            ctl.abrirModalLead();
            ctl.modalEditar();

            System.currentPageReference().getParameters().put('table', 'lead');
            ctl.selectTableMain();
            ctl.abrirModalLead();
            ctl.modalEditar();
        }
    }

    static testMethod void testModalNova() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            ctl.abrirModalOpp();
            ctl.modalNova();
        }
    }

    static testMethod void testAtenderOpp() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            ctl.abrirModalOpp();
            ctl.atenderOpp();
        }
    }

    static testMethod void testRetornarOpp() {
        insertPermission(userManager, ProfileUtils.permissionSeller);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            ctl.abrirModalOpp();
            ctl.retornarOpp();
        }
    }

    static testMethod void testAceitarBDC() {
        insertPermission(userManager, ProfileUtils.permissionBdc);

        System.runAs(userManager) {
            DashboardTaskController ctl = new DashboardTaskController();
            System.currentPageReference().getParameters().put('oppId', opp.Id);
            System.currentPageReference().getParameters().put('actSubj', 'Persecution');
            ctl.abrirModalBDC();
            ctl.aceitarBDC();
        }
    }

    static void insertPermission(User u, String namePermission) {
        PermissionSet ps = [ SELECT ID From PermissionSet WHERE Name =: namePermission ];
        PermissionSetAssignment psA =
            new PermissionSetAssignment(AssigneeId = u.id, PermissionSetId = ps.Id );
        INSERT psA;
    }
}