/**
    Date: 03/04/2013
    seeAllData: is enable to have access to the standard Pricebook2, that every org has one.
    Why: Inserting a pricebook entry for a product isn’t feasible from a test since the standard
         pricebook isn’t accessible and can’t be created in a running test. Also, inserting a 
         pricebook entry for a custom pricebook isn’t supported since this requires defining a 
         standard pricebook. For such situations, annotate your test method with 
         IsTest(SeeAllData=true).
         http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_data_access.htm
 */
@isTest (seeAllData=true)
private class VFC38_OpportunityAfterInsertExecTest {

    static testMethod void myUnitTestOpportunityAfterInsert() {
        
//=====================================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER
//=====================================================================================     
        Opportunity opp1 = new Opportunity();       
        boolean isCreatedOpportunity1 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb21 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp1.Name = 'OppNameTest_1';
        opp1.StageName = 'Identified';
        opp1.CloseDate = Date.today();
        opp1.Pricebook2Id = pb21.Id;
        opp1.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_TO_MANAGER;
        opp1.CurrencyIsoCode = 'BRL';
        insert opp1;
        
        Test.startTest();
        
        try{
             Task t1 = [SELECT WhatId FROM Task WHERE WhatId = :opp1.Id];
             isCreatedOpportunity1 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity1 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity1,true);
        
        

//================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER
//================================================================      
        Opportunity opp2 = new Opportunity();       
        boolean isCreatedOpportunity2 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb22 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp2.Name = 'OppNameTest_1';
        opp2.StageName = 'Identified';
        opp2.CloseDate = Date.today();
        opp2.Pricebook2Id = pb22.Id;
        opp2.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_TO_MANAGER;
        opp2.CurrencyIsoCode = 'BRL';
        insert opp2;
        
        try{
             Task t2 = [SELECT WhatId FROM Task WHERE WhatId = :opp2.Id limit 1];
             isCreatedOpportunity2 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity2 = false;
        }
        
        system.assertEquals(isCreatedOpportunity2,true);
        
        


//================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER
//================================================================      
        Opportunity opp3 = new Opportunity();       
        boolean isCreatedOpportunity3 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb23 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp3.Name = 'OppNameTest_1';
        opp3.StageName = 'Identified';
        opp3.CloseDate = Date.today();
        opp3.Pricebook2Id = pb23.Id;
        opp3.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_MANAGER_TO_SELLER;
        opp3.CurrencyIsoCode = 'BRL';
        insert opp3;
        
        try{
             Task t3 = [SELECT WhatId FROM Task WHERE WhatId = :opp3.Id];
             isCreatedOpportunity3 = true;
        }catch(QueryException e){
            isCreatedOpportunity3 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity3,true);
        
        

//================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.TEST_DRIVE_MANAGER_TO_SELLER
//================================================================      
        Opportunity opp4 = new Opportunity();       
        boolean isCreatedOpportunity4 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb24 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp4.Name = 'OppNameTest_1';
        opp4.StageName = 'Identified';
        opp4.CloseDate = Date.today();
        opp4.Pricebook2Id = pb24.Id;
        opp4.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_MANAGER_TO_SELLER;
        opp4.CurrencyIsoCode = 'BRL';
        insert opp4;
        
        try{
             Task t4 = [SELECT WhatId FROM Task WHERE WhatId = :opp4.Id];
             isCreatedOpportunity4 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity4 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity4,true);
        
        
//================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.HOT_LEAD_RECEPTIONIST_TO_SELLER
//================================================================      
        Opportunity opp5 = new Opportunity();       
        boolean isCreatedOpportunity5 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb25 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp5.Name = 'OppNameTest_1';
        opp5.StageName = 'Identified';
        opp5.CloseDate = Date.today();
        opp5.Pricebook2Id = pb25.Id;
        opp5.OpportunityTransition__c = VFC43_OpportunityTransitionType.HOT_LEAD_RECEPTIONIST_TO_SELLER;
        opp5.CurrencyIsoCode = 'BRL';
        insert opp5;
        
        try{
             Task t5 = [SELECT WhatId FROM Task WHERE WhatId = :opp5.Id];
             isCreatedOpportunity5 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity5 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity5,true);
        
        

//================================================================
// insert Opportunity Transition: VFC43_OpportunityTransitionType.TEST_DRIVE_RECEPTIONIST_TO_SELLER
//================================================================      
        Opportunity opp6 = new Opportunity();       
        boolean isCreatedOpportunity6 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb26 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp6.Name = 'OppNameTest_1';
        opp6.StageName = 'Identified';
        opp6.CloseDate = Date.today();
        opp6.Pricebook2Id = pb26.Id;
        opp6.OpportunityTransition__c = VFC43_OpportunityTransitionType.TEST_DRIVE_RECEPTIONIST_TO_SELLER;
        opp6.CurrencyIsoCode = 'BRL';
        insert opp6;
        
        try{
             Task t6 = [SELECT WhatId FROM Task WHERE WhatId = :opp6.Id];
             isCreatedOpportunity6 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity6 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity6,true);
        

//================================================================
// insert Opportunity Transition: 
//================================================================      
        Opportunity opp7 = new Opportunity();       
        boolean isCreatedOpportunity7 = false;
        
        // selecionar Standard price Book (seeAllData)
        Pricebook2 pb27 = [select id from Pricebook2 where IsStandard = true limit 1];
        
        // criar nova oportunidade
        opp7.Name = 'OppNameTest_1';
        opp7.StageName = 'Identified';
        opp7.CloseDate = Date.today();
        opp7.Pricebook2Id = pb27.Id;
        opp7.CurrencyIsoCode = 'BRL';
        insert opp7;
        
        try{
             Task t7 = [SELECT WhatId FROM Task WHERE WhatId = :opp7.Id];
             isCreatedOpportunity7 = true;
        }
        catch(QueryException e){
            isCreatedOpportunity7 = false;
        }
        
        //system.assertEquals(isCreatedOpportunity7,true);


        Test.stopTest();        
    }
}