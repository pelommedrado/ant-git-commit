/**
* @author: Ashok Muntha
* @date: 06-05-2015
* @description: This class is used to Create the Offers.
* @checkOffersBasedOnEmailOrVIN()
* @refreshPage()
* @searchList()
* @getInterestedOffer()
* @removeInterestedOffer()
*/

public class Rforce_CommercialOffers_CTR {
    public Rforce_CommercialOffers_CTR() {}
    Rforce_CommercialOffers_WS         rforceCommercialOfferWS;
    Rforce_CommercialOffersPropose_CLS rforceCommercialOffersPropose;
    Rforce_CommercialOffersParsing_CLS rforceCommercialParsingCLS;       
    
    //public string listcomm{get;set;}
    public String strObjectType{get; set;}
    public String strEmail{get; set;}
    public String strCountryCode {get; set;}
    public String strAccountID {get; set;}
    public String strSearchList {set;get;}          
    public String strVIN{get; set;}
    public List<Rforce_CommercialOffersResponse_CLS> rforceCommercialOffers{get;set;}
    public List<Rforce_CommercialOffersResponse_CLS> rforceCommercialVINOffers{get;set;}
    public String strCommercialOfferResponseXMLforEmail='';
    public String strCommercialOfferResponseXMLforVIN='';
    public string strInterestedOrNotSelected{get;set;}
    public string strOffersSelected{get;set;}         
    public Account personAccount{get;set;}        
    public Case caseobject;    
    public id commOfferId{get;set;}
    public Boolean bCountry {get; set;} 
    public Static String strYes = System.Label.Rforce_CommercialOffer_Yes;
    public Static String strNo = System.Label.Rforce_CommercialOffer_No;
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to Check the offers based on the Email and VIN.
    */    

    /**
    * @Project RForce
    * @ModifiedBy Chandra Kanth  ( HQREQ - 4643)
    * @ModifiedDate 01/06/2016
    * @Description -- If case is not associated with Account not executing 'getCommercialOffersBasedOnEmailOrVin'.
    */
    public pageReference checkOffersBasedOnEmailOrVIN() {     
         PageReference chkOffersPageRef;
         rforceCommercialOffersPropose = new Rforce_CommercialOffersPropose_CLS();             
         rforceCommercialOfferWS       = new Rforce_CommercialOffers_WS();
         rforceCommercialParsingCLS    = new Rforce_CommercialOffersParsing_CLS();
         User u = [Select Id, RecordDefaultCountry__c from User Where Id = : UserInfo.getUserId()]; 
         if(u.RecordDefaultCountry__c == System.Label.User_Country_Brazil) {
            bCountry=false;
            rforceCommercialOffers        = new List<Rforce_CommercialOffersResponse_CLS>();
            if (strObjectType == 'Account') {
                if(ApexPages.currentPage().getParameters().get('Id') != null) {
                    try{
                        personAccount = [SELECT Name,PersEmailAddress__c,Country__c FROM Account WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];          
                        strEmail = personAccount.PersEmailAddress__c;
                        strCountryCode = Country_Info__c.getInstance(personAccount.Country__c).Country_Code_3L__c;
                        strAccountID = ApexPages.currentPage().getParameters().get('Id');
                        System.debug('strAccountID ====>'+strAccountID);
                        System.debug('Country code====>'+strCountryCode);
                        rforceCommercialOffersPropose.strEmailAndCountry= strEmail+'|'+strCountryCode.trim();
                        system.debug('**strCountryCode.trim()'+strCountryCode.trim());
                        strCommercialOfferResponseXMLforEmail = rforceCommercialOfferWS.getCommercialOffersBasedOnEmailOrVin('getEmailOffers',rforceCommercialOffersPropose);
                        system.debug('### Rforce_CommercialOffers_CTR : ACCOUNT XML strCommercialOfferResponseXMLforEmail is ->'+ strCommercialOfferResponseXMLforEmail);
                        if (strCommercialOfferResponseXMLforEmail!=null && strCommercialOfferResponseXMLforEmail.length() > 0) {
                             system.debug('### Rforce_CommercialOffers_CTR :  Inside if condition ##');
                             rforceCommercialOffers = rforceCommercialParsingCLS.parseCommercialOffers(strCommercialOfferResponseXMLforEmail);
                        } 
                    } catch(Exception e) {
                        system.debug(e);
                    }
                    system.debug('### Rforce_CommercialOffers_CTR :  rforceCommercialOffers -> ' + rforceCommercialOffers );
               }  
         } else if(strObjectType == 'Case') {
            if(ApexPages.currentPage().getParameters().get('Id') != null) {
                try {
                    caseobject = [SELECT Account.Id, Account.Country__c,VIN__r.Name, Account.PersEmailAddress__c,CountryCase__c FROM Case where id = : ApexPages.currentPage().getParameters().get('Id')];
                    strVIN     = caseobject.VIN__r.Name;
                    strEmail = caseobject.Account.PersEmailAddress__c;
                    
                    if(caseobject.Account != null) {
                        strCountryCode = Country_Info__c.getInstance(caseobject.Account.Country__c).Country_Code_3L__c;
                    } else {
                        strCountryCode = Country_Info__c.getInstance(caseobject.CountryCase__c).Country_Code_3L__c;                
                    }
                
                    strAccountID = caseobject.Account.Id;

                    rforceCommercialOffersPropose.strEmailAndCountry= strEmail+'|'+ strCountryCode.trim();
                    rforceCommercialOffersPropose.strVinAndCountry= strVIN+'|'+ strCountryCode.trim();            
                    
                    if(caseobject.Account != null) {   
                        strCommercialOfferResponseXMLforEmail = rforceCommercialOfferWS.getCommercialOffersBasedOnEmailOrVin('getEmailOffers',rforceCommercialOffersPropose);
                        system.debug('### Rforce_CommercialOffers_CTR :  CASE XML strCommercialOfferResponseXMLforEmail is. -> ' + strCommercialOfferResponseXMLforEmail);
                        if (strCommercialOfferResponseXMLforEmail!=null && strCommercialOfferResponseXMLforEmail.length() > 0) {
                            system.debug('## Case Inside if condition ##');
                            rforceCommercialOffers = rforceCommercialParsingCLS.parseCommercialOffers(strCommercialOfferResponseXMLforEmail);
                        } 
                    }           
                    strCommercialOfferResponseXMLforVIN = rforceCommercialOfferWS.getCommercialOffersBasedOnEmailOrVin('getVinOffers',rforceCommercialOffersPropose);
                    system.debug('### Rforce_CommercialOffers_CTR :  XML strCommercialOfferResponseXMLforVIN  is -> '+ strCommercialOfferResponseXMLforVIN);    
                 
                    if (strCommercialOfferResponseXMLforVIN!=null && strCommercialOfferResponseXMLforVIN.length() > 0) {
                        system.debug('### Rforce_CommercialOffers_CTR :  Inside if condition ##');
                        rforceCommercialVINOffers = rforceCommercialParsingCLS.parseCommercialOffers(strCommercialOfferResponseXMLforVIN);
                    }    
                    rforceCommercialOffers.addAll(rforceCommercialVINOffers);      
                    system.debug('### Rforce_CommercialOffers_CTR :  VIN Offers :: rforceCommercialOffers size is.::'+ rforceCommercialOffers.size());
                } catch(Exception e) {
                    system.debug(e.getMessage());
                }
            }
        }
         System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffersPropose.strEmailAndCountry -> ' + rforceCommercialOffersPropose.strEmailAndCountry);
         System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffersPropose.strVinAndCountry -> ' + rforceCommercialOffersPropose.strVinAndCountry);
         system.debug('### Rforce_CommercialOffers_CTR :  rforceCommercialOffers -> ' + rforceCommercialOffers );         
         rforceCommercialOffers = removeInterestedOffer(rforceCommercialOffers, strAccountID);
         if(rforceCommercialOffers != null) {
            rforceCommercialOffers.sort();
            getcheckOffersBasedOnEmailOrVINforSearch();
        }
     } else {
        bCountry=true;
     }         
         return chkOffersPageRef;        
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to Click on the Interested Offer, that page will be refreshed.
    */   
    public pageReference refreshPage() {     
         PageReference chkOffersPageRef;         
          if (strObjectType == 'Account') {
            strAccountID = ApexPages.currentPage().getParameters().get('Id');
            }else if(strObjectType == 'Case') {
            caseobject = [SELECT Account.Id, Account.Country__c,VIN__r.Name, Account.PersEmailAddress__c FROM Case where id = : ApexPages.currentPage().getParameters().get('Id')];
            strAccountID = caseobject.Account.Id;
        }
        rforceCommercialOffers = removeInterestedOffer(rforceCommercialOffers, strAccountID);
        System.debug('inside refreshPage rforceCommercialOffers==>'+rforceCommercialOffers);            
        rforceCommercialOffers.sort();
        return chkOffersPageRef;        
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to Search the offers in the OfferList based on the keywords
    */       
    public pageReference searchList() {    
        System.debug('### Rforce_CommercialOffers_CTR : Search value in list -> ' + strSearchList);
        System.debug('### Rforce_CommercialOffers_CTR : SearchList rforceCommercialOffers -> ' + rforceCommercialOffers);
        PageReference chkOffersPageRef;    
        if (strObjectType == 'Account') {
            strAccountID = ApexPages.currentPage().getParameters().get('Id');
        } else if(strObjectType == 'Case') {
            caseobject = [SELECT Account.Id, Account.Country__c,VIN__r.Name, Account.PersEmailAddress__c FROM Case where id = : ApexPages.currentPage().getParameters().get('Id')];
            strAccountID = caseobject.Account.Id;
       }
       if(rforceCommercialOffers != null) {     
            rforceCommercialOffers = removeInterestedOffer(rforceCommercialOffers, strAccountID);       
            // List<Rforce_CommercialOffersResponse_CLS> rforceCommercialOffersbyFilter;  
            List<Rforce_CommercialOffersResponse_CLS> rforceCommercialOffersbyFilter = new List<Rforce_CommercialOffersResponse_CLS>();          
            if(rforceCommercialOffers != null) {
                for(Integer i = 0; i<rforceCommercialOffers.size();i++){
                    // rforceCommercialOffersbyFilter = new List<Rforce_CommercialOffersResponse_CLS>();
                    System.debug('### Rforce_CommercialOffers_CTR : strSearchList -> ' + strSearchList);
                    System.debug('### Rforce_CommercialOffers_CTR : strSearchList.contains(rforceCommercialOffers.get(i).strValidityDate) -> ' + strSearchList.contains(rforceCommercialOffers.get(i).strValidityDate));
                    System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffers.get(i).strPropofferId -> ' + strSearchList.contains(rforceCommercialOffers.get(i).strPropOfferID));
                    System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffers.get(i).strPropStartDate -> ' + strSearchList.contains(rforceCommercialOffers.get(i).strPropStartDate));
                    System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffers.get(i).strPropEndDate -> ' + strSearchList.contains(rforceCommercialOffers.get(i).strPropEndDate));
                    System.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffers.get(i).strDesMkt -> ' + strSearchList.contains(rforceCommercialOffers.get(i).strDesMkt));
                    /* if(strSearchList.contains(rforceCommercialOffers.get(i).strValidityDate) || strSearchList.contains(rforceCommercialOffers.get(i).strPropOfferID) || strSearchList.contains(rforceCommercialOffers.get(i).strPropStartDate) || strSearchList.contains(rforceCommercialOffers.get(i).strPropEndDate)
                    || strSearchList.contains(rforceCommercialOffers.get(i).strDesMkt) || strSearchList.contains(rforceCommercialOffers.get(i).strDeskEli)
                    || strSearchList.contains(rforceCommercialOffers.get(i).strDeskLeg) || findTaxonomyByKey(strSearchList,rforceCommercialOffers.get(i).strPropCode) ) {  */

                    if(strSearchList.contains(rforceCommercialOffers.get(i).strValidityDate) || strSearchList.contains(rforceCommercialOffers.get(i).strPropOfferID) || strSearchList.contains(rforceCommercialOffers.get(i).strPropStartDate) || strSearchList.contains(rforceCommercialOffers.get(i).strPropEndDate)
                    || (rforceCommercialOffers.get(i).strDesMkt).contains(strSearchList) || (rforceCommercialOffers.get(i).strDeskLeg).contains(strSearchList)
                    || findTaxonomyByKey(strSearchList,rforceCommercialOffers.get(i).strPropCode) || (rforceCommercialOffers.get(i).strPropLabel).contains(strSearchList) 
                    ||  (rforceCommercialOffers.get(i).strPropCode).contains(strSearchList) ) {     
                        System.debug('### Rforce_CommercialOffers_CTR : Inside');     
                        rforceCommercialOffersbyFilter.add(rforceCommercialOffers.get(i));
                    // break;
                    }         
                }   
                rforceCommercialOffers =  rforceCommercialOffersbyFilter;       
                if(rforceCommercialOffers !=null) {   
                    rforceCommercialOffers.sort();
                }
            }
       }
       return chkOffersPageRef;    
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to receive the values from the Offer Object for the Account who has already interested in the offer
    */         
    public List<Commercial_Offer__c> getInterestedOffer(String strAccountID) {
        List<Commercial_Offer__c> lstOffersInterested = new List<Commercial_Offer__c>();
        system.debug('### Rforce_CommercialOffers_CTR : strAccountID 111 -> ' + strAccountID); 
        lstOffersInterested = [SELECT Offer_Id__c FROM Commercial_Offer__c WHERE InterestedStatus__c=:strYes and Account__c = :strAccountID];
        system.debug('### Rforce_CommercialOffers_CTR : lstOffersInterested -> ' + lstOffersInterested);
        if(lstOffersInterested  != null && lstOffersInterested.size()>0){
            system.debug('### Rforce_CommercialOffers_CTR : lstOffersInterested -> ' + lstOffersInterested);
        } 
        return lstOffersInterested ;
    }
 
    /**
    * @Project RForce
    * @ModifiedBy Chandra Kanth  (Amberfox Sprint1)
    * @ModifiedDate 15/08/2016
    * @Description -- This method is used to receive the 'Not Interested' offers from the Offer Object for the given Account.   
    */ 
    public List<Commercial_Offer__c> getNotInterestedOffer(String strAccountID){
        List<Commercial_Offer__c> lstOffersNotInterested = new List<Commercial_Offer__c>();
        lstOffersNotInterested = [SELECT Offer_Id__c FROM Commercial_Offer__c WHERE InterestedStatus__c=:strNo and Account__c = :strAccountID];
        return lstOffersNotInterested;
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to remove the records from the list received from the Webservice against the data available in the Offer Object
    */ 
    public List<Rforce_CommercialOffersResponse_CLS> removeInterestedOffer(List<Rforce_CommercialOffersResponse_CLS> rforceCommercialOffers, String strAccountID){
        List<Commercial_Offer__c> lstOffersInterested = getInterestedOffer(strAccountID);
        List<Rforce_CommercialOffersResponse_CLS> lstOffers = new  List<Rforce_CommercialOffersResponse_CLS>();
        system.debug('lstOffersInterested.size()-->'+lstOffersInterested.size());
        if(lstOffersInterested  != null && lstOffersInterested.size()>0){
        
        for(integer i = 0; i < lstOffersInterested.size(); i++) {
                for(integer j = 0; j < rforceCommercialOffers.size(); j++) {
                    if(lstOffersInterested.get(i).Offer_Id__c.equals(rforceCommercialOffers.get(j).strPropOfferID)) {
                        system.debug('### Rforce_CommercialOffers_CTR : lstOffersInterested.get(i).Offer_Id__c() --'+lstOffersInterested.get(i).Offer_Id__c);
                        system.debug('### Rforce_CommercialOffers_CTR : rforceCommercialOffers.get(j).strPropOfferID()--->'+rforceCommercialOffers.get(j).strPropOfferID);
                        system.debug('### Rforce_CommercialOffers_CTR : lstOffersInterested -> ' + lstOffersInterested.get(i));
                        rforceCommercialOffers.remove(j);
                        break;
                    }
                }
            }               
        } 
        
        /**
        * @Project RForce
        * @ModifiedBy Chandra Kanth  (Amberfox Sprint1)
        * @ModifiedDate 15/08/2016
        * @Description -- It will remove not interested offers from offers list so that it will not be displayed again to user in right pane   
        */   
        List<Commercial_Offer__c> lstOffersNotInterested = getNotInterestedOffer(strAccountID);
        if(lstOffersNotInterested  != null && lstOffersNotInterested.size() > 0) {
        
            for(integer i = 0; i < lstOffersNotInterested.size(); i++) {
                for(integer j = 0; j < rforceCommercialOffers.size(); j++) {
                    if(lstOffersNotInterested.get(i).Offer_Id__c.equals(rforceCommercialOffers.get(j).strPropOfferID)) {
                        rforceCommercialOffers.remove(j);
                        break;
                    }
                }
            }               
        }
       return rforceCommercialOffers;           
    } 
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to Insert the record or update the record in offer object.
    */     
    public void createOffer() {
        List<Commercial_Offer__c> comOffrLst = new List<Commercial_Offer__c>();
        Commercial_Offer__c createNComOffr = new Commercial_Offer__c();
        OfferHistory__c createNComOffrHistory = new OfferHistory__c();   
        Commercial_Offer__c commercialOfferInstance;                
        if (strObjectType == 'Account') {
            strAccountID = ApexPages.currentPage().getParameters().get('Id');
        } else if(strObjectType=='Case') {
            caseobject = [SELECT Account.Id, Account.Country__c,VIN__r.Name, Account.PersEmailAddress__c FROM Case where id = : ApexPages.currentPage().getParameters().get('Id')];
            strAccountID = caseobject.Account.Id;
        }

        List<Commercial_Offer__c> cList = [select id from Commercial_Offer__c where Offer_Id__c =:strOffersSelected and  Account__c =:strAccountID ];      
        Commercial_Offer__c commOff = (cList != null && cList.size()>0) ? cList[0] : null;
        System.debug('### Rforce_CommercialOffers_CTR :  commOff -> ' + commOff);
        
        if(commOff == null ) {
        
        for(Rforce_CommercialOffersResponse_CLS rfCmr:rforceCommercialOffers) {
            if(rfCmr.strPropOfferID.equals(strOffersSelected)) {
                if (strObjectType=='Account') {
                    createNComOffr.Account__c = personAccount.id;
                } else if(strObjectType=='Case') {
                 //  createNComOffr.Case__c = caseobject.id;
                 createNComOffr.Account__c = caseobject.Account.id;
                 createNComOffr.Case__c = caseobject.id;
                }
                if(strInterestedOrNotSelected!= '' && strInterestedOrNotSelected== 'true' ) {
                    createNComOffr.InterestedStatus__c = strYes;
                } else if(strInterestedOrNotSelected!= '' && strInterestedOrNotSelected== 'false') {
                    createNComOffr.InterestedStatus__c = strNo;
                }
                createNComOffr.Customer_Email_Address__c   = strEmail;
                     
                if(rfCmr.strPropEndDate != ''){                    
                    String strEndDateProp = rfCmr.strPropEndDate ;                  
                    Date dtEndDateProp = setStringToDateFormat(strEndDateProp);          
                    createNComOffr.OfferEndDate__c = dtEndDateProp;                   
                     }
                if(rfCmr.strPropStartDate != ''){
                    String strStartDateProp = rfCmr.strPropStartDate;                
                    Date dtStartDateProp = setStringToDateFormat(strStartDateProp); 
                    createNComOffr.OfferStartDate__c = dtStartDateProp ; 
                    
                }
                if(rfCmr.strValidityDate != ''){
                    String strValidDateProp = rfCmr.strValidityDate;                
                    Date dtValidDateProp = setStringToDateFormat(strValidDateProp); 
                    createNComOffr.Offer_Validity_Date__c = dtValidDateProp;
                    
                }
                if(rfCmr.strDesMkt != ''){                
                    createNComOffr.OfferDescriptionMkt__c = rfCmr.strDesMkt;                
                }                
                if(rfCmr.strDeskEli != ''){                
                    createNComOffr.OfferDescriptionEli__c = rfCmr.strDeskEli;                
                }                
                if(rfCmr.strDeskLeg != ''){                
                    createNComOffr.OfferDescriptionLeg__c = rfCmr.strDeskLeg;                
                }   
                
                // Added as a part of Commercial Offer V5 changes implemented on 27-Jan-2016 by Venkatesh Kumar
                system.debug('### Rforce_CommercialOffers_CTR :  rfCmr.strPropLabel ->' + rfCmr.strPropLabel);
                if(rfCmr.strPropLabel != ''){                
                    createNComOffr.OfferLabel__c = rfCmr.strPropLabel;
                    system.debug('### Rforce_CommercialOffers_CTR : rfCmr.strPropLabel -> ' + rfCmr.strPropLabel);
                }    
                
                if(rfCmr.strPropCode != '' && rfCmr.strPropCode.length()>= 14 ){                
                    createNComOffr.Brand__c=rfCmr.strPropCode.subString(0,3);
                    createNComOffr.Target__c=rfCmr.strPropCode.subString(3,6);
                    createNComOffr.Offer_Level__c=rfCmr.strPropCode.subString(6,9);
                    createNComOffr.MyR_Offers__c=rfCmr.strPropCode.subString(9,12);
                    createNComOffr.OfferType__c = rfCmr.strPropCode.subString(12,14);
                    if(rfCmr.strPropCode.length() == 16){
                        createNComOffr.AS_Offer_Detail__c=rfCmr.strPropCode.subString(14,16);                
                    }
                }
                createNComOffr.Offer_Id__c = rfCmr.strPropOfferID;
                createNComOffr.Image_URL__c = rfCmr.strPictDetail;
                createNComOffr.RequesttoAdobe__c = strNo ;
            }
        }
        try {
            if(createNComOffr != null) {
                insert createNComOffr;
                commOfferId = createNComOffr.id;                             
            }
        } catch(DMLException e) {
          System.debug('Exception in Inserting Commercial Offer'+e);          
        }
      } else {  
            commercialOfferInstance =[select id from Commercial_Offer__c where Offer_Id__c =:strOffersSelected and  Account__c =:strAccountID ];      
            system.debug('### Rforce_CommercialOffers_CTR : commercialOfferInstance--->'+commercialOfferInstance.id);
            if(commercialOfferInstance.id != null) {        
                Commercial_Offer__c cupdateObj = new Commercial_Offer__c();     
                cupdateObj.id = commercialOfferInstance.id;
                if(strInterestedOrNotSelected!= '' && strInterestedOrNotSelected== 'true' ){
                      cupdateObj.InterestedStatus__c = strYes;
                } else if(strInterestedOrNotSelected!= '' && strInterestedOrNotSelected== 'false'){
                        cupdateObj.InterestedStatus__c = strNo;
                }
                System.debug('cupdateObj.InterestedStatus__c===>'+cupdateObj.InterestedStatus__c);
                update cupdateObj;
            }               
       }
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to redirect the Offer record details display in the sub tab.
    */    
    public pagereference redirectOfferDetailpage() {    
        id loggedInUser = userInfo.getUserId();
        integer i = 0;
        id comOffrId;
        String strOfferIdByParam;
        strOfferIdByParam = ApexPages.currentPage().getParameters().get('strOfferId');
        System.debug('### Rforce_CommercialOffers_CTR : Offer Id from Param -> ' + ApexPages.currentPage().getParameters().get('strOfferId'));       
        comOffrId = [select id from Commercial_Offer__c where Offer_Id__c =:strOfferIdByParam order by createddate DESC limit 1].id;
        system.debug('### Rforce_CommercialOffers_CTR : comOffrId -> ' + comOffrId);                
        string offerId=string.valueOf(comOffrId);        
        system.debug('### Rforce_CommercialOffers_CTR : offerId -> ' + offerId);        
        PageReference newComOff = new PageReference('/'+offerId);        
        system.debug('### Rforce_CommercialOffers_CTR : newComOff -> ' + newComOff);
        newComOff.setRedirect(true);
        return newComOff;    
    }
    
    public String getId() {
        return null;
    }

    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to search the offer type
    */    
    public Boolean findTaxonomyByKey(String strSearchText,String strPropCode) {    
        Boolean bResult=false; 
        Integer strOfferCodeLength = strPropCode.length();
        System.debug('### Rforce_CommercialOffers_CTR : Taxonomy -> ' + strPropCode + ' Taxonomy length -> ' + strOfferCodeLength);
        if(strOfferCodeLength == 16) {     
            if(strSearchText.contains(strPropCode.subString(0,3)) || strSearchText.contains(strPropCode.subString(3,6)) ||
             strSearchText.contains(strPropCode.subString(6,9)) || strSearchText.contains(strPropCode.subString(9,12)) ||
             strSearchText.contains(strPropCode.subString(12,14)) || strSearchText.contains(strPropCode.subString(14,16))) {      
                bResult=true;         
            } else {
                bResult=false;
                System.debug('1 No Mtaches Taxonmy');
            }
        }   
        if(strOfferCodeLength == 14){     
            if(strSearchText.contains(strPropCode.subString(0,3)) || strSearchText.contains(strPropCode.subString(3,6)) ||
            strSearchText.contains(strPropCode.subString(6,9)) || strSearchText.contains(strPropCode.subString(9,12)) ||
            strSearchText.contains(strPropCode.subString(12,14))) {      
                bResult=true;         
            } else {
                bResult=false;
                System.debug('2 No Mtaches Taxonmy');
            }
        }   
        return  bResult;
    }
    
    /**
    * @author Rajavel
    * @date 06-05-2015
    * @description This method is used to Search the Offers based on Email and VIN.
    */     
    public String getcheckOffersBasedOnEmailOrVINforSearch() {
        system.debug('### Rforce_CommercialOffers_CTR :  Inside getcheckOffersBasedOnEmailOrVINforSearch() ##');
        String strJSONSerialize='';
        if (rforceCommercialOffers!=null && rforceCommercialOffers.size() > 0) {
            system.debug('### Rforce_CommercialOffers_CTR :  Size of rforceCommercialOffers is..:: ##'+ rforceCommercialOffers.size());
            strJSONSerialize = JSON.serialize(rforceCommercialOffers);
            system.debug('### Rforce_CommercialOffers_CTR :  strJSONSerialize is..::'+ strJSONSerialize);
        } 
        return strJSONSerialize;
    }

    /**
    * @author Venkatesh Kumar
    * @date 05/Jan/2016
    * @description This method is used to convert the date from String to Date (dd-MM-yyyy) format.
    */      
    private Date setStringToDateFormat(String strDateString) {
        String[] strDateSplit = strDateString.split(' ');
        String[] strDate = strDateSplit[0].split('/');
        Integer intDate = integer.valueOf(strDate[0]);
        Integer intMonth = integer.valueOf(strDate[1]);
        Integer intYear = integer.valueOf(strDate[2]);
        String strDateFormated = String.valueOf(intDate)+'/'+String.valueOf(intMonth)+'/'+String.valueOf(intYear);
        Date dtDateValueParsed = Date.parse(strDateFormated);
        return dtDateValueParsed;
    }
}