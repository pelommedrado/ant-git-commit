public class VFC05ArchivageXMLParseEngine{
    
    public VFC05_ArchivesXMLData getXMLData(String toParse)
        {
        VFC05_ArchivesXMLData archivexmlData=new VFC05_ArchivesXMLData();
        try
        {   
            //Need to remove after RC webservice fix
            String str='<SiebelMessage>';
            Integer lengeth=str.length(); 
            Integer pos1=toParse.indexof('<SiebelMessage>');
            Integer pos2=toParse.indexof('</SiebelMessage>');
            pos2=pos2>0?pos2:0;
            
            String parse=toParse.SubString(0,pos2);
            
            CustomXMLDom parser=new CustomXMLDom(parse);
            //CustomXMLDom parser=new CustomXMLDom(toParse);
            CustomXMLDom.Element tempStatus=null;    
            
            archivexmlData.FirstName=getNodeValue(parser,'FirstName');
            archivexmlData.LastName=getNodeValue(parser,'LastName');
            archivexmlData.Status=getNodeValue(parser,'Status2');
                
            archivexmlData.Comment=getNodeValue(parser,'Comment2'); 
            archivexmlData.IdCase=getNodeValue(parser,'SRNumber2');   
            archivexmlData.Description=getNodeValue(parser,'Description');
            archivexmlData.NegociationResult=getNodeValue(parser,'NegociationResult');
            archivexmlData.Area=getNodeValue(parser,'Area');    
            archivexmlData.Created=getNodeValue(parser,'Created'); 
            archivexmlData.QuoteType=getNodeValue(parser,'QuoteType');  
            archivexmlData.FromValue=getNodeValue(parser,'From');
            archivexmlData.RCSRBrand=getNodeValue(parser,'RCSRBrand');
            archivexmlData.CreatedByName=getNodeValue(parser,'CreatedByName');
            archivexmlData.Owner=getNodeValue(parser,'Owner');
            archivexmlData.Source=getNodeValue(parser,'Source');   
            archivexmlData.ClosedDate=getNodeValue(parser,'ClosedDate'); 
            archivexmlData.ClosedBy=getNodeValue(parser,'ClosedBy');   
            archivexmlData.ResolutionDate=getNodeValue(parser,'ResolutionDate'); 
            archivexmlData.ResolutionAuthor=getNodeValue(parser,'ResolutionAuthor');  
            archivexmlData.Mileage=getNodeValue(parser,'Mileage');
            archivexmlData.PreferedLanguage2=getNodeValue(parser,'PreferedLanguage2');
            archivexmlData.InvoiceAmount=getNodeValue(parser,'InvoiceAmount');
            archivexmlData.Owner=getNodeValue(parser,'Owner');
            archivexmlData.Source=getNodeValue(parser,'Source');   
            archivexmlData.ClosedDate=getNodeValue(parser,'ClosedDate'); 
            archivexmlData.ClosedBy=getNodeValue(parser,'ClosedBy');
                
            archivexmlData.SubArea2=getNodeValue(parser,'Sub-Area2');
            archivexmlData.CustomerRefNumber=getNodeValue(parser,'CustomerRefNumber'); 
            archivexmlData.RCDetail=getNodeValue(parser,'RCDetail');
            archivexmlData.AccessPoint=getNodeValue(parser,'AccessPoint');   
            archivexmlData.Severity=getNodeValue(parser,'Severity'); 
            archivexmlData.Decision=getNodeValue(parser,'Decision'); 
            archivexmlData.TestData =getNodeValue(parser,'Action'); 
           
            //Activity History
            List<VFC05_ArchivesXMLData.ActivityList> activityList=new List<VFC05_ArchivesXMLData.ActivityList>();        
                
            VFC05_ArchivesXMLData.ActivityList activity;
           
            //Get ListofAction Element:
            CustomXMLDom.Element listofaction=parser.getElementByTagName('ListOfAction');
            if(listofaction !=null)
            {
                //Get Action List
                List<CustomXMLDom.Element> actionList=listofaction.getElementsByTagName('Action');
                CustomXMLDom.Element element;
                if(actionList!=null)
                for(Integer i=0;i<actionList.size();i++)
                {
                    System.debug('Each list ====>'+i+' '+actionList.get(i).getElementByTagname('Type2').nodevalue);
                    activity=new VFC05_ArchivesXMLData.ActivityList();
                    element=actionList.get(i);
                    activity.Type2=getNodeValue(element,'Type2');
                    activity.OriginDestination=getNodeValue(element,'OriginDestination');
                    activity.Status2=getNodeValue(element,'Status2');
                    activity.Created=getNodeValue(element,'Created');
                    activity.Done=getNodeValue(element,'Done');
                    activity.Description2=getNodeValue(element,'Description2');
                    activity.Comment2=getNodeValue(element,'Comment2');
                    activity.SerialNumber2=getNodeValue(element,'SerialNumber2');
                    activity.OwnedBy=getNodeValue(element,'OwnedBy');
                    activity.ContactLastName=getNodeValue(element,'ContactLastName');
                    activity.ContactFirstName=getNodeValue(element,'ContactFirstName');
                    activity.AccountName=getNodeValue(element,'AccountName');
                    activity.RCFleetName=getNodeValue(element,'RCFleetName');
                    activity.AccountLocation=getNodeValue(element,'AccountLocation');
                    activity.SRNumber2=getNodeValue(element,'SRNumber2');
                    
                    //Attachments
                    List<VFC05_ArchivesXMLData.Attachment> attachmentsList=new List<VFC05_ArchivesXMLData.Attachment>();        
                
                    VFC05_ArchivesXMLData.Attachment Attachments;
           
                    //Get ListofAttachments Element:
                    CustomXMLDom.Element listOfActionAttachment=actionList.get(i).getElementByTagName('ListOfActionAttachment'); 
                    if(listOfActionAttachment!=null)
                    {    
                        //Get Attachment List
                        List<CustomXMLDom.Element> attachmentElements=listOfActionAttachment.getElementsByTagName('ActionAttachment');
               
                        if(attachmentElements!=null)
                        for(Integer a=0;a<attachmentElements.size();a++)
                        {
                            Attachments=new VFC05_ArchivesXMLData.Attachment();
                            element=attachmentElements.get(a);
                            Attachments.ActivityFileName=getNodeValue(element,'ActivityFileName');
                            Attachments.ActivityFileExt=getNodeValue(element,'ActivityFileExt');
                            Attachments.CreatedByName2=getNodeValue(element,'CreatedByName2');
                            Attachments.ActivityFileDate=getNodeValue(element,'ActivityFileDate');
                            Attachments.Comment3=getNodeValue(element,'Comment3');
                            Attachments.ActivityFileSAFName=getNodeValue(element,'ActivityFileSAFName');
                            attachmentsList.add(Attachments);
                        }
                    }
                    activity.AttachmentsList=attachmentsList;
                    //archivexmlData.TestData=''+attachmentsList.get(0).ActivityFileName;
                    activityList.add(activity);
                }
            }
            archivexmlData.ActivitiesLists=activityList;
            //archivexmlData.TestData=''+archivexmlData.ActivitiesLists.size();
            
            //Contact
            List<VFC05_ArchivesXMLData.ContactList> contactList=new List<VFC05_ArchivesXMLData.ContactList>();        
                
            VFC05_ArchivesXMLData.ContactList contact;
           
            //Get ListofContact Element:
            CustomXMLDom.Element listOfContact=parser.getElementByTagName('ListOfContact');
            if(listOfContact!=null)
            {
                //Get Contact List
                List<CustomXMLDom.Element> contactElements=listOfContact.getElementsByTagName('Contact');
                System.debug('Contact===>'+listOfContact);
                CustomXMLDom.Element element;
                if(contactElements!=null)
                for(Integer i=0;i<contactElements.size();i++)
                {
                    System.debug('Contact list ====>'+i+' '+contactElements.get(i).getElementByTagname('TitleSIC').nodevalue);
                    contact=new VFC05_ArchivesXMLData.ContactList();
                    element=contactElements.get(i);
                    contact.TitleSIC=getNodeValue(element,'TitleSIC');
                    contact.Title=getNodeValue(element,'Title');
                    contact.LastName=getNodeValue(element,'LastName');
                    contact.FirstName=getNodeValue(element,'FirstName');
                    contact.NoClient=getNodeValue(element,'NoClient');
                    contact.CustomerIdentificationType=getNodeValue(element,'CustomerIdentificationType');
                    contact.CustomerIdentificationNr2=getNodeValue(element,'CustomerIdentificationNr2');
                    contact.SICId=getNodeValue(element,'SICId');
                    contact.PreferedLanguage2=getNodeValue(element,'PreferedLanguage2');
                    contact.Account=getNodeValue(element,'Account');
                    contact.PreferedMethodContact=getNodeValue(element,'PreferedMethodContact');
                    contact.EmailAddress=getNodeValue(element,'EmailAddress');
                    
                    
                    //To get contact personal
                    List<VFC05_ArchivesXMLData.Contact_PersonalAddress> contactPersonalList=new List<VFC05_ArchivesXMLData.Contact_PersonalAddress>();        
                
                    VFC05_ArchivesXMLData.Contact_PersonalAddress contactPersonalAdd;
                    CustomXMLDom.Element e=contactElements.get(i).getElementByTagname('ListOfContact_PersonalAddress');
                    if(e!=null)
                    {
                        List<CustomXMLDom.Element> contactPersonalElements=e.getElementsByTagName('Contact_PersonalAddress');
                        System.debug('Contact===>'+listOfContact);
                        if(contactPersonalElements !=null)
                        for(Integer j=0;j<contactPersonalElements.size();j++)
                        {
                            System.debug('Contact list ====>'+i+' '+contactPersonalElements.get(j).getElementByTagname('PersonalCity').nodevalue);
                            contactPersonalAdd=new VFC05_ArchivesXMLData.Contact_PersonalAddress();
                            element=contactPersonalElements.get(j);
                            contactPersonalAdd.PersonalCompleteStreetAddress=getNodeValue(element,'PersonalCompleteStreetAddress');
                            contactPersonalAdd.PersonalAdditionalStreetAddress=getNodeValue(element,'PersonalAdditionalStreetAddress');
                            contactPersonalAdd.PersonalStreetNumber=getNodeValue(element,'PersonalStreetNumber');
                            contactPersonalAdd.PersonalStreetType=getNodeValue(element,'PersonalStreetType');
                            contactPersonalAdd.PersonalStreetName=getNodeValue(element,'PersonalStreetName');
                            contactPersonalAdd.PersonalCity=getNodeValue(element,'PersonalCity');
                            contactPersonalAdd.PersonalPostalCode2=getNodeValue(element,'PersonalPostalCode2');
                            contactPersonalAdd.PersonalMailBox=getNodeValue(element,'PersonalMailBox');
                            contactPersonalAdd.PersonalCountry2=getNodeValue(element,'PersonalCountry2');
                            contactPersonalAdd.PersonalCellularPhone=getNodeValue(element,'PersonalCellularPhone');
                            contactPersonalAdd.PersonalWorkPhone=getNodeValue(element,'PersonalWorkPhone');
                            contactPersonalAdd.PersonalHomePhone=getNodeValue(element,'PersonalHomePhone');
                            contactPersonalList.add(contactPersonalAdd);
                        }
                    contact.PersonalAddressList=contactPersonalList;
                    }
                    contactList.add(contact);
                }
            }	
            archivexmlData.ContactList=contactList;
            
            //Concerned
            //Get ListofConcerned Element:
            CustomXMLDom.Element listOfconcerned=parser.getElementByTagName('ListOfRenaultRcDealers');
            if(listOfconcerned!=null)
            {
                //Get Concerned List
                CustomXMLDom.Element concernedElements=listOfconcerned.getElementByTagName('RenaultRcDealers');
                System.debug('Concerned===>'+listOfconcerned);
                VFC05_ArchivesXMLData.Concerned concerned=new VFC05_ArchivesXMLData.Concerned();
                 CustomXMLDom.Element element;
                if(concernedElements!=null)
                {
                    
                    concerned.DealerNumber=getNodeValue(concernedElements,'DealerNumber');
                    concerned.Name22=getNodeValue(concernedElements,'Name22');
                    concerned.CorporateShortName=getNodeValue(concernedElements,'CorporateShortName');
                    concerned.TradeRegistration=getNodeValue(concernedElements,'TradeRegistration');
                    concerned.ParentDivisionName=getNodeValue(concernedElements,'ParentDivisionName');
                    concerned.RCDealerBrand=getNodeValue(concernedElements,'RCDealerBrand');
                    concerned.CorporateStatus=getNodeValue(concernedElements,'CorporateStatus');
                    concerned.Type=getNodeValue(concernedElements,'Type');
                    concerned.Zone=getNodeValue(concernedElements,'Zone');
                    concerned.MainPhoneNumber=getNodeValue(concernedElements,'MainPhoneNumber');
                    concerned.MainFaxNumber=getNodeValue(concernedElements,'MainFaxNumber');
                    
                    //To get concerned Address
                    List<VFC05_ArchivesXMLData.Concerned_BusinessAddress> concernedBusinessAddressList=new List<VFC05_ArchivesXMLData.Concerned_BusinessAddress>();        
                    
                    VFC05_ArchivesXMLData.Concerned_BusinessAddress concernedBusinessAddress;
                    CustomXMLDom.Element e=concernedElements.getElementByTagname('ListOfRenaultRCDealers_BusinessAddress');
                    if(e!=null)
                    {
                        List<CustomXMLDom.Element> concernedAddressElements=e.getElementsByTagName('RenaultRCDealers_BusinessAddress');
                        //CustomXMLDom.Element element;
                        if(concernedAddressElements!=null)
                        for(Integer j=0;j<concernedAddressElements.size();j++)
                        {
                            System.debug('concernedAddressElements list ====>'+j+' '+concernedAddressElements.get(j).getElementByTagname('CompleteAddress').nodevalue);
                            concernedBusinessAddress=new VFC05_ArchivesXMLData.Concerned_BusinessAddress();
                            element=concernedAddressElements.get(j);
                            concernedBusinessAddress.CompleteAddress=getNodeValue(element,'CompleteAddress');
                            concernedBusinessAddress.AdditionalAddress=getNodeValue(element,'AdditionalAddress');
                            concernedBusinessAddress.StreetNumber=getNodeValue(element,'StreetNumber');
                            concernedBusinessAddress.StreetType=getNodeValue(element,'StreetType');
                            concernedBusinessAddress.StreetName=getNodeValue(element,'StreetName');
                            concernedBusinessAddress.City=getNodeValue(element,'City');
                            concernedBusinessAddress.PostalCode=getNodeValue(element,'PostalCode');
                            concernedBusinessAddress.MailBox=getNodeValue(element,'MailBox');
                            
                            concernedBusinessAddressList.add(concernedBusinessAddress);
                        }
                    }
                    concerned.ConcernedBusinessAddressList=concernedBusinessAddressList;
                }
            
            archivexmlData.Concerned=concerned;
            }
            //Concerned Employee
            List<VFC05_ArchivesXMLData.Employee> employeeList=new List<VFC05_ArchivesXMLData.Employee>();        
            
            VFC05_ArchivesXMLData.Employee employee;
            CustomXMLDom.Element ele=parser.getElementByTagname('ListOfEmployee');
            if(ele!=null)
            {
                List<CustomXMLDom.Element> employeeElements=ele.getElementsByTagName('Employee');
                if(employeeElements!=null)
                for(Integer j=0;j<employeeElements.size();j++)
                {
                    System.debug('concernedAddressElements list ====>'+j+' '+employeeElements.get(j).getElementByTagname('FirstName').nodevalue);
                    employee=new VFC05_ArchivesXMLData.Employee();
                    CustomXMLDom.Element element=employeeElements.get(j);
                    employee.FirstName=getNodeValue(element,'FirstName');
                    employee.JobTitle=getNodeValue(element,'JobTitle');
                    employee.LastName=getNodeValue(element,'LastName');
                    employee.ActivePhone=getNodeValue(element,'ActivePhone');
                    employee.Description3=getNodeValue(element,'Description3');
                    employee.Position=getNodeValue(element,'Name3');
                    
                    employeeList.add(employee);
                }
            }
            archivexmlData.EmployeeList=employeeList;
           
            
           
            //Vehicle
            CustomXMLDom.Element listOfVehicle=parser.getElementByTagName('ListOfAssetMgmt-AssetArchivage');
            if(listOfVehicle !=null)
            {
                //Get Vehicle List
                CustomXMLDom.Element vehicleElement=listOfVehicle.getElementByTagName('AssetMgmt-AssetArchivage');
                VFC05_ArchivesXMLData.Vehicle vehicle=new VFC05_ArchivesXMLData.Vehicle();
                if(vehicleElement!=null)
                {
                    vehicle.Build=getNodeValue(vehicleElement,'Build');
                    vehicle.SerialNumber2=getNodeValue(vehicleElement,'SerialNumber2');
                    vehicle.ProductModel=getNodeValue(vehicleElement,'ProductModel');
                    vehicle.Origin=getNodeValue(vehicleElement,'Origin');
                    vehicle.FactoryNumber2=getNodeValue(vehicleElement,'FactoryNumber2');
                    vehicle.DealerName2=getNodeValue(vehicleElement,'DealerName2');
                    vehicle.FactoryNumber2=getNodeValue(vehicleElement,'FactoryNumber2');
                    vehicle.ProductVersionSIC=getNodeValue(vehicleElement,'ProductVersionSIC');
                    vehicle.UsedNew=getNodeValue(vehicleElement,'UsedNew');
                    vehicle.AssetSICId=getNodeValue(vehicleElement,'AssetSICId');
                    vehicle.DealerNumber2=getNodeValue(vehicleElement,'DealerNumber2');
                    vehicle.InstallDate=getNodeValue(vehicleElement,'InstallDate');
                    vehicle.UsedCarPurchaseDate=getNodeValue(vehicleElement,'UsedCarPurchaseDate');
                    vehicle.RegisteredDate=getNodeValue(vehicleElement,'RegisteredDate');
                }
                archivexmlData.Vehicle=vehicle;
            }
            //Paying 
            //Get ListofPaying Element:
            CustomXMLDom.Element listOfPaying=parser.getElementByTagName('ListOfRenaultRcDealers-Compensateur');
            if(listOfPaying!=null)
            {
                //Get Concerned List
                CustomXMLDom.Element payingElements=listOfPaying.getElementByTagName('RenaultRcDealers-Compensateur');
                System.debug('Concerned===>'+listOfPaying);
                VFC05_ArchivesXMLData.Paying paying=new VFC05_ArchivesXMLData.Paying();
                if(payingElements !=null)
                {
                    paying.DealerNumber=getNodeValue(payingElements,'DealerNumber');
                    paying.Name22=getNodeValue(payingElements,'Name22');
                    paying.CorporateShortName=getNodeValue(payingElements,'CorporateShortName');
                    paying.TradeRegistration=getNodeValue(payingElements,'TradeRegistration');
                    paying.ParentDivisionName=getNodeValue(payingElements,'ParentDivisionName');
                    paying.RCDealerBrand=getNodeValue(payingElements,'RCDealerBrand');
                    paying.CorporateStatus=getNodeValue(payingElements,'CorporateStatus');
                    paying.Type=getNodeValue(payingElements,'Type');
                    paying.Zone=getNodeValue(payingElements,'Zone');
                    paying.MainPhoneNumber=getNodeValue(payingElements,'MainPhoneNumber');
                    paying.MainFaxNumber=getNodeValue(payingElements,'MainFaxNumber');
                    
                    //To get Paying Address
                    List<VFC05_ArchivesXMLData.Paying_BusinessAddress> payingBusinessAddressList=new List<VFC05_ArchivesXMLData.Paying_BusinessAddress>();        
                    
                    VFC05_ArchivesXMLData.Paying_BusinessAddress payingBusinessAddress;
                    CustomXMLDom.Element payingElement=payingElements.getElementByTagname('ListOfRenaultRCDealers-Compensateur_BusinessAddress');
                    if(payingElement!=null)
                    {
                        List<CustomXMLDom.Element> payingAddressElements=payingElement.getElementsByTagName('RenaultRCDealers-Compensateur_BusinessAddress');
                        
                        for(Integer j=0;j<payingAddressElements.size();j++)
                        {
                            System.debug('concernedAddressElements list ====>'+j+' '+payingAddressElements.get(j).getElementByTagname('CompleteAddress').nodevalue);
                            payingBusinessAddress=new VFC05_ArchivesXMLData.Paying_BusinessAddress();
                            CustomXMLDom.Element element=payingAddressElements.get(j);
                            payingBusinessAddress.CompleteAddress=getNodeValue(element,'CompleteAddress');
                            payingBusinessAddress.AdditionalAddress=getNodeValue(element,'AdditionalAddress');
                            payingBusinessAddress.StreetNumber=getNodeValue(element,'StreetNumber');
                            payingBusinessAddress.StreetType=getNodeValue(element,'StreetType');
                            payingBusinessAddress.StreetName=getNodeValue(element,'StreetName');
                            payingBusinessAddress.City=getNodeValue(element,'City');
                            payingBusinessAddress.PostalCode=getNodeValue(element,'PostalCode');
                            payingBusinessAddress.MailBox=getNodeValue(element,'MailBox');
                            
                            payingBusinessAddressList.add(payingBusinessAddress);
                        }
                        paying.PayingBusinessAddressList=payingBusinessAddressList;
                    }
                }
                archivexmlData.Paying=paying;
            }
            
            //Owner Organisation
            
            //Get ListofOwner Element:
           
            CustomXMLDom.Element listOfOwner=parser.getElementByTagName('ListOfAccount2');
            if(listOfOwner!=null)
            {
                //Get Concerned List
                CustomXMLDom.Element ownerElements=listOfOwner.getElementByTagName('Account2');
                System.debug('Owner===>'+listOfOwner);
                VFC05_ArchivesXMLData.OwnerOrganization ownerOrganization=new VFC05_ArchivesXMLData.OwnerOrganization();
                if(ownerElements!=null)
                {
                    ownerOrganization.Name2=getNodeValue(ownerElements,'Name2');
                    ownerOrganization.OrgIdentificationNr2=getNodeValue(ownerElements,'OrgIdentificationNr2');
                    ownerOrganization.Type=getNodeValue(ownerElements,'Type');
                    ownerOrganization.ParentAccountName=getNodeValue(ownerElements,'ParentAccountName');
                    ownerOrganization.Preferedlanguage=getNodeValue(ownerElements,'Preferedlanguage');
                    ownerOrganization.OrgIdentificationType=getNodeValue(ownerElements,'OrgIdentificationType');
                    ownerOrganization.SICId=getNodeValue(ownerElements,'SICId');
                    ownerOrganization.OrgIdentificationNr1=getNodeValue(ownerElements,'OrgIdentificationNr1');
                    ownerOrganization.Siret=getNodeValue(ownerElements,'Siret');
                    ownerOrganization.Location2=getNodeValue(ownerElements,'Location2');
                    ownerOrganization.MainPhoneNumber=getNodeValue(ownerElements,'MainPhoneNumber');
                    ownerOrganization.MainFaxNumber=getNodeValue(ownerElements,'MainFaxNumber');
                    
                    //To get owner Address
                    List<VFC05_ArchivesXMLData.BusinessAddress> businessAddressList=new List<VFC05_ArchivesXMLData.BusinessAddress>();        
                    
                    VFC05_ArchivesXMLData.BusinessAddress businessAddress;
                    CustomXMLDom.Element bussElement=ownerElements.getElementByTagname('ListOfAccount_BusinessAddress');
                    if(bussElement!=null)
                    {
                        List<CustomXMLDom.Element> bussAddressElements=bussElement.getElementsByTagName('Account_BusinessAddress');
                        if(bussAddressElements!=null)
                        for(Integer j=0;j<bussAddressElements.size();j++)
                        {
                            System.debug(' list ====>'+j+' '+bussAddressElements.get(j).getElementByTagname('CompleteAddress').nodevalue);
                            businessAddress=new VFC05_ArchivesXMLData.BusinessAddress();
                            CustomXMLDom.Element element=bussAddressElements.get(j);
                            businessAddress.CompleteAddress=getNodeValue(element,'CompleteAddress');
                            businessAddress.AdditionalAddress=getNodeValue(element,'AdditionalAddress');
                            businessAddress.StreetNumber=getNodeValue(element,'StreetNumber');
                            businessAddress.StreetType=getNodeValue(element,'StreetType');
                            businessAddress.StreetName=getNodeValue(element,'StreetName');
                            businessAddress.City=getNodeValue(element,'City');
                            businessAddress.PostalCode=getNodeValue(element,'PostalCode');
                            businessAddress.MailBox=getNodeValue(element,'MailBox');
                            
                            businessAddressList.add(businessAddress);
                        }
                    }
                    ownerOrganization.BusinessAddressList=businessAddressList;
                }  
            
                archivexmlData.OwnerOrganization=ownerOrganization;
            }
            
            //Fleet Organisation
            
            //Get ListofFleet Element:
            CustomXMLDom.Element listOfFleet=parser.getElementByTagName('ListOfAccount2');
            if(listOfFleet!=null)
            {
                //Get Concerned List
                CustomXMLDom.Element fleetElements=listOfFleet.getElementByTagName('Account2');
                System.debug('Owner===>'+listOfFleet);
                VFC05_ArchivesXMLData.FleetOrganization fleetOrganization=new VFC05_ArchivesXMLData.FleetOrganization();
                if(fleetElements !=null)
                {
                    fleetOrganization.Name2=getNodeValue(fleetElements,'Name2');
                    fleetOrganization.OrgIdentificationNr2=getNodeValue(fleetElements,'OrgIdentificationNr2');
                    fleetOrganization.Type=getNodeValue(fleetElements,'Type');
                    fleetOrganization.ParentAccountName=getNodeValue(fleetElements,'ParentAccountName');
                    fleetOrganization.Preferedlanguage=getNodeValue(fleetElements,'Preferedlanguage');
                    fleetOrganization.OrgIdentificationType=getNodeValue(fleetElements,'OrgIdentificationType');
                    fleetOrganization.SICId=getNodeValue(fleetElements,'SICId');
                    fleetOrganization.OrgIdentificationNr1=getNodeValue(fleetElements,'OrgIdentificationNr1');
                    fleetOrganization.Siret=getNodeValue(fleetElements,'Siret');
                    fleetOrganization.Location2=getNodeValue(fleetElements,'Location2');
                    fleetOrganization.MainPhoneNumber=getNodeValue(fleetElements,'MainPhoneNumber');
                    fleetOrganization.MainFaxNumber=getNodeValue(fleetElements,'MainFaxNumber');
                    
                    //To get owner Address
                    List<VFC05_ArchivesXMLData.BusinessAddress> fleetBusinessAddressList=new List<VFC05_ArchivesXMLData.BusinessAddress>();        
                    
                    VFC05_ArchivesXMLData.BusinessAddress businessAddress;
                    CustomXMLDom.Element fbussElement=fleetElements.getElementByTagname('ListOfAccount_BusinessAddress');
                    List<CustomXMLDom.Element> fbussAddressElements=fbussElement.getElementsByTagName('Account_BusinessAddress');
                    if(fbussElement!=null)
                    for(Integer j=0;j<fbussAddressElements.size();j++)
                    {
                        System.debug('list ====>'+j+' '+fbussAddressElements.get(j).getElementByTagname('CompleteAddress').nodevalue);
                        businessAddress=new VFC05_ArchivesXMLData.BusinessAddress();
                        CustomXMLDom.Element element=fbussAddressElements.get(j);
                        businessAddress.CompleteAddress=getNodeValue(element,'CompleteAddress');
                        businessAddress.AdditionalAddress=getNodeValue(element,'AdditionalAddress');
                        businessAddress.StreetNumber=getNodeValue(element,'StreetNumber');
                        businessAddress.StreetType=getNodeValue(element,'StreetType');
                        businessAddress.StreetName=getNodeValue(element,'StreetName');
                        businessAddress.City=getNodeValue(element,'City');
                        businessAddress.PostalCode=getNodeValue(element,'PostalCode');
                        businessAddress.MailBox=getNodeValue(element,'MailBox');
                        
                        fleetBusinessAddressList.add(businessAddress);
                    }
                    fleetOrganization.BusinessAddressList=fleetBusinessAddressList;
                }   
                archivexmlData.FleetOrganization=fleetOrganization;
            }
            
            //EffectPerception
            VFC05_ArchivesXMLData.EffectPerception effectPerception=new VFC05_ArchivesXMLData.EffectPerception();
            effectPerception.ClientView=getNodeValue(parser,'ClientView');
            effectPerception.ClientDemand=getNodeValue(parser,'ClientDemand');
            effectPerception.ServiceCause=getNodeValue(parser,'ServiceCause');
            effectPerception.Risk=getNodeValue(parser,'Risk');
            effectPerception.CommercialCause=getNodeValue(parser,'CommercialCause');
            effectPerception.ProductCauseFunction=getNodeValue(parser,'ProductCauseFunction');
            effectPerception.NITGCode=getNodeValue(parser,'NITGCode');
            effectPerception.ProductCause=getNodeValue(parser,'ProductCause');
          
            archivexmlData.EffectPerception=effectPerception;
            
            //CustomerCompensation
            VFC05_ArchivesXMLData.CustomerCompensation customerCompensation=new VFC05_ArchivesXMLData.CustomerCompensation();
            customerCompensation.Mileage=getNodeValue(parser,'Mileage');
            customerCompensation.InvoiceAmount=getNodeValue(parser,'InvoiceAmount');
            customerCompensation.ClaimAmount=getNodeValue(parser,'ClaimAmount');
            customerCompensation.ClientAmount=getNodeValue(parser,'ClientAmount');
            
            archivexmlData.CustomerCompensation=customerCompensation;
            
            
        }
        catch (System.XMLException e) {  
            System.debug(e.getMessage());
        }
    
        return archivexmlData;
    }
    //To get Node value based on tag name from parser
    public String getNodeValue(CustomXMLDom parser,String tagName)
    {
        if(parser!=null && tagname!=null)
        {
            CustomXMLDom.Element element=parser.getElementByTagName(tagName);
            if(element!=null)
                return element.nodeValue;
        }
        return '';
    }
    
    //To get Node value based on tag name from Element
    public String getNodeValue(CustomXMLDom.Element parser,String tagName)
    {
        if(parser!=null && tagname!=null)
        {
            CustomXMLDom.Element element=parser.getElementByTagName(tagName);
            if(element!=null)
                return element.nodeValue;
        }
        return '';
    }

}