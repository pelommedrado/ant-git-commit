global class PV_FinancingConditionsController {
    
    public PV_FinancingConditionsController(){
        csvFileLines = new String[]{};
    }
    

	public Blob csvFileBody { get;set; }
    public string csvAsString { get;set; }
    public String[] csvFileLines { get;set; }
    public Document docLayoutLeadCsv {
        get{
            return [SELECT Id FROM Document where DeveloperName  = 'Layout_Upload_FinancingConditions'];
        } 
    }
    
   public List<PV_FinancingConditions__c> lsFinancingConditions {
        get{
        	return [select id,Coefficient__c,Flag__c,Month__c,Status__c,Tax__c,TypePerson__c,CreatedDate from PV_FinancingConditions__c where Status__c = 'ACTIVE' order by Month__c,TypePerson__c,Tax__c]; 
    	}
   }
    
    public void importCSVFile(){
        Savepoint sp = Database.setSavepoint();
        try {
           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 
            
            System.debug('csvFileBody:' + csvFileBody.toString());
            System.debug('split:' + csvAsString.split('\n'));
			System.debug('csvFileLines:' + csvFileLines.size());
            
            List<PV_FinancingConditions__c> lsFinancingConditions = new List<PV_FinancingConditions__c>();
            lsFinancingConditions.addAll(inactiveOldFinancingConditions());//inativar Registros Antigos
            
                      
            for(Integer i=1;i<csvFileLines.size();i++){
            
                String[] csvRecordData = csvFileLines[i].split(';');
                
                PV_FinancingConditions__c financingConditions = new PV_FinancingConditions__c();
                financingConditions.Month__c = Integer.valueof(csvRecordData[0]);
                financingConditions.Tax__c = Decimal.valueof(csvRecordData[1].replace(',','.'));
                financingConditions.Coefficient__c = Decimal.valueof(csvRecordData[2].replace(',','.'));
                financingConditions.TypePerson__c = csvRecordData[3];
                financingConditions.Flag__c = csvRecordData[4];
                financingConditions.Status__c = 'ACTIVE';
                lsFinancingConditions.add(financingConditions);
               
            }
            system.debug('**** lsFinancingConditions '+lsFinancingConditions);
            Database.upsert( lsFinancingConditions );
            System.debug('###@@@ lsFinancingConditions '+lsFinancingConditions);
                        
            ApexPages.Message successMessage = 
                new ApexPages.Message(ApexPages.severity.CONFIRM, 
                                     (csvFileLines.size()-1) + ' Coeficiente(s) importado(s) com sucesso!');
            ApexPages.addMessage(successMessage);
            
        } catch (Exception e) {
            Database.rollback( sp );
            System.debug('##### Error '+e);
            ApexPages.Message errorMessage = 
                new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage());
            ApexPages.addMessage(errorMessage);
        } 
    }
    
    public List<PV_FinancingConditions__c> inactiveOldFinancingConditions(){
        List<PV_FinancingConditions__c>lsfinancing = new List<PV_FinancingConditions__c>();
        for(PV_FinancingConditions__c financingConditions:[select id from PV_FinancingConditions__c where Status__c = 'ACTIVE']){
        	financingConditions.Status__c='INACTIVE';
            lsfinancing.add(financingConditions);
        }
        return lsfinancing;
    }
    
    @RemoteAction
    global static String inactiveFinancingConditions(String value){
        System.debug('#### res Value '+value);
        if(value!=null)
            if(value.length()>0){
                PV_FinancingConditions__c financingConditions = new PV_FinancingConditions__c();
                financingConditions.Id = value;
                financingConditions.Status__c='INACTIVE';
                try{
                    update financingConditions;
                    return 'true';
                }catch(Exception e){
                     return 'false';
                }
            }
        return 'false';
    }
}