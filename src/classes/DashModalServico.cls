public class DashModalServico {

    public void configModalLead(ModalDetalhe modal, Lead lead) {
        modal.Nome 			= lead.FirstName + ' ' + lead.LastName;
        modal.email 	 	= lead.Email;
        modal.telefone 	 	= lead.HomePhone__c;
        modal.celular 	 	= lead.MobilePhone;
        modal.dataCriacao	= lead.CreatedDate;
        modal.veiculo		= lead.VehicleOfInterest__c;
        modal.cpf			= lead.CPF_CNPJ__c;
        modal.lead			= lead;
        modal.isTask		= true;
        modal.isPreTask		= false;
    }

    public void configModalEvent(ModalDetalhe modal, Event event) {
        modal.taskId		 = event.Id;
        modal.actv 			 = DashboardUtil.traduzirSubject(event.Subject);
        modal.subject		= event.Subject;
        modal.dataActv 		 = DataUtils.getUserDatetime(event.StartDateTime);
        modal.comentarioActv = event.Description;
        modal.statusActv 	 = event.Status__c;
        modal.isTask		 = true;
        modal.isPreTask		 = false;
    }

    public void configModalTask(ModalDetalhe modal, Task task) {
        modal.taskId		 = task.Id;
        modal.actv 			 = DashboardUtil.traduzirSubject(task.Subject);
        modal.subject		= task.Subject;
        modal.dataActv 		 = DataUtils.getUserDatetime(task.ActivityDatetime__c);
        modal.comentarioActv = task.Description;
        modal.statusActv 	 = task.Status;
        modal.isTask		 = true;
        modal.isPreTask		 = false;
    }

    public void configModalBDC(ModalDetalhe modal, Opportunity opp) {
        modal.statusActv     = 'Pendente';
        modal.isTask         = true;

        modal.nome           = opp.Account.Name;
        modal.email          = opp.Account.PersEmailAddress__c;
        modal.telefone       = opp.Account.PersLandline__c;
        modal.celular        = opp.Account.PersMobPhone__c;
        modal.cpf			 = opp.Account.CustomerIdentificationNbr__c;
        modal.dataCriacao    = opp.CreatedDate;
        modal.veiculo        = opp.VehicleInterest__c;
        modal.opp            = opp;
        modal.isTask         = true;
        modal.isPreTask		 = false;

        modal.stageList      = createStages(opp);
    }

    public void configModalOpp(ModalDetalhe modal, Opportunity opp) {
        modal.nome 		  = opp.Account.Name;
        modal.email 	  = opp.Account.PersEmailAddress__c;
        modal.telefone 	  = opp.Account.PersLandline__c;
        modal.celular 	  = opp.Account.PersMobPhone__c;
        modal.cpf		  = opp.Account.CustomerIdentificationNbr__c;
        modal.dataCriacao = DataUtils.getUserDatetime(opp.CreatedDate);
        modal.veiculo	  = opp.VehicleInterest__c;
        modal.opp		  = opp;
        modal.isTask	  = true;

        modal.stageList = createStages(opp);
        modal.tasklist = new DashLembreteServico().obterLembrete(opp);
    }

    public Opportunity obterOpp(Id oppId) {
        List<Opportunity> opL = [
            SELECT Name, toLabel(StageName), temperature__c, Dealer__c,
            AccountId, Account.Name,
            Account.CustomerIdentificationNbr__c,
            Account.PersMobPhone__c, Account.PersEmailAddress__c,
            Account.PersLandline__c, CreatedDate , VehicleInterest__c
            FROM Opportunity WHERE Id =: oppId
        ];
        return opL.isEmpty() ? null : opL.get(0);
    }

    public Lead obterLead(Id leadId) {
        List<Lead> leadL = [
            SELECT Id, OwnerId, Owner.Name, MobilePhone, HomePhone__c,
            Email, FirstName, LastName, CPF_CNPJ__c, CreatedDate, LastModifiedDate,
            Description, VehicleOfInterest__c
            FROM Lead WHERE Id =: leadId
        ];
        return leadL.isEmpty() ? null : leadL.get(0);
    }

    public Task obterTask(Id taskId) {
        List<Task> taskL = [
            SELECT Id, toLabel(Status), OwnerId, Owner.Name, Account.Name,
            toLabel(Subject), Description, ActivityDatetime__c, WhatId, WhoId
            FROM Task WHERE Id =: taskId
        ];
        return taskL.isEmpty() ? null : taskL.get(0);
    }

    public Event obterEvent(Id eventId) {
        List<Event> eventL = [
            SELECT Id, Account.Name, OwnerId, Owner.Name, toLabel(Subject), Description,
            toLabel(Status__c), StartDateTime, WhatId, WhoId
            FROM Event WHERE Id =: eventId
        ];
        return eventL.isEmpty() ? null : eventL.get(0);
    }

    public List<Stages> createStages(Opportunity opp) {

        List<Stages> modalStageList = new List<Stages>();
        String oppRelStage = null;

        String stageNames = Label.sfaStageNames;

        for(Integer i = 0; i < 7; i++) {
            Stages st = new Stages();
            st.stageName = stageNames.split(',')[i];

            if(stageNames.split(',')[i] == opp.StageName){
                oppRelStage = stageNames.split(',')[i];
                st.stageImage = 'sfaOppStages/active-' + ( i > 0 && i < 6  ? 'middle' : 'edge' ) + '.png';
                st.showName = 'layout';

            } else if(stageNames.split(',')[i] != opp.StageName && oppRelStage != null) {
                st.stageImage = 'sfaOppStages/next-' + ( i > 0 && i < 6   ? 'middle' : 'edge' ) + '.png';
                st.showName = 'none';

            } else if(stageNames.split(',')[i] != opp.StageName && oppRelStage == null){
                st.stageImage = 'sfaOppStages/pass-' + ( i > 0 && i < 6   ? 'middle' : 'edge' ) + '.png';
                st.showName = 'none';
            }

            st.stageClass = (i == 0) ? 'rotate' : 'none';

            modalStageList.add(st);
        }
        return modalStageList;
    }

    public class Stages {
        public String showName 		{ get;set; }
        public String stageName 	{ get;set; }
        public String stageClass 	{ get;set; }
        public String stageImage 	{ get;set; }
    }

    public class ModalDetalhe {
        public String nome 				{ get;set; }
        public String telefone 			{ get;set; }
        public String celular 			{ get;set; }
        public String cpf 				{ get;set; }
        public String email 			{ get;set; }
        public Datetime dataCriacao 	{ get;set; }
        public Opportunity opp 			{ get;set; }
        public Lead lead 				{ get;set; }
        public String veiculo 			{ get;set; }
        public Id taskId				{ get;set; }
        public Boolean isTask 			{ get;set; }
        public Boolean isPreTask		{ get;set; }
        public String actv 			 	{ get;set; }
        public String subject 			{ get;set; }
        public Datetime dataActv 	 	{ get;set; }
        public String comentarioActv 	{ get;set; }
        public String statusActv	 	{ get;set; }
        public String modo				{ get;set; }
        public List<Stages> stageList 	{ get;set; }
        public List<DashLembreteServico.Lembrete> tasklist { get;set; }
    }
}