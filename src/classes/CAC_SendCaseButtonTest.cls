@isTest
public class CAC_SendCaseButtonTest {
    
    
    static Case caseTest;
        
   
    static testMethod void updateUserParamNull(){
        Test.startTest();
       		system.assert(CAC_SendCaseButton.updateUser(null)== null);
        Test.stopTest();
    }
    static testMethod void updateUserSucess(){
    	caseTest = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        CAC_SendCaseButtonTest.createCustonSet();
        Test.startTest();
        	CAC_SendCaseButton.updateUser(caseTest.Id);
        Test.stopTest();
    }
    
    static testMethod void verifyCommentFailure(){
        caseTest = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        Test.startTest();
        system.assert(CAC_SendCaseButton.verifyComment(caseTest.Id).equals('false'));
        Test.stopTest();
     }
    static testMethod void verifyCommentAccept(){
        caseTest = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        CaseComment comentario =  returnComentarioCaso(caseTest.Id);
		Test.startTest();
        for(integer i=0; i<5;i++){
        	comentario =  returnComentarioCaso(caseTest.Id);
        	comentario =  returnComentarioCaso(caseTest.Id);
            comentario =  returnComentarioCaso(caseTest.Id);
        }
        
        Test.stopTest();
    }
    
    
    static testMethod void verifyCommentAnalystError2(){
       
        
        User usuario = createUser();
        
        caseTest = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        System.assert(caseTest.Id!=null);
        caseTest.OwnerId = usuario.Id;
        Database.update(caseTest);
        CaseComment comentario =  returnComentarioCaso(caseTest.Id);
		Test.startTest();
        for(integer i=0; i<5;i++){
        	comentario =  returnComentarioCaso(caseTest.Id);
        	comentario =  returnComentarioCaso(caseTest.Id);
            comentario =  returnComentarioCaso(caseTest.Id);
        }
        CAC_SendCaseButton.verifyCommentAnalyst(caseTest.Id);

        
        Test.stopTest();
        
        
    }
    
    static testMethod void verifyCommentAnalystSucess(){
        Test.startTest();
        caseTest = returnNewCase(Utils.getRecordTypeId('Case', 'Cacweb_PartsWarehouse'));
        
        CaseComment comentario =  returnComentarioCaso(caseTest.Id);
        //System.runAs(createUser()){
        //	CaseComment comentario =  returnComentarioCaso(caseTest.Id);
        //}
        
        System.assertEquals('false2', CAC_SendCaseButton.verifyCommentAnalyst(caseTest.Id));
        Test.stopTest();
    }
    
   
    private static void createCustonSet(){
        cacweb_Mapping__c custonSet = new cacweb_Mapping__c(
        	name = 'Cacweb_PartsWarehouse',
            Department__c = 'Peças',
            Departamento__c = 'Armazém de Peças',
            DirectionInEnglish__c = 'Parts',
            Name_Queue__c= 'Cacweb_PartsWarehouse',
            Record_Type_Name__c = 'Cacweb_PartsWarehouse'
        );
        Database.insert(custonSet);
    }
    
    
    
    private static CaseComment returnComentarioCaso(Id caseId){
        CaseComment comenta = new CaseComment(
        	CommentBody = 'asdasdadadsadadsadasdasdasd',
            IsPublished = true,
            ParentId = caseId
        );
        Database.insert(comenta,true);
        return comenta;
    }
    
     private static Case returnNewCase(Id recordType){
         
             Case cas = new Case(
                Status = 'new',
                Subject__c = 'Diferido (Previsão de Peças)',
                Description = 'teste',
                Dealer_Contact_Name__c = 'teste',
                Contact_Phone__c  = '1122334455',
                Reference__c = 'teste',
                Order_Number__c = '1234567',
                RecordTypeId = recordType
            );
       		Database.insert(cas,true);
            return cas;
         
    }
    private static User createUser(){
         User manager = new User(
             FirstName = 'Test',
             LastName = 'User',
             Email = 'test@org.com',
             Username = 'test@org1.com',
             Alias = 'tes',
             EmailEncodingKey='UTF-8',
             LanguageLocaleKey='en_US',
             LocaleSidKey='en_US',
             TimeZoneSidKey='America/Los_Angeles',
             CommunityNickname = 'testing',
             ProfileId = [select Id from Profile where Name = 'CAC - Full'].Id,
             BIR__c ='123ABC123',
             BypassVR__c = true
         );
        Database.insert( manager );
        return manager;
    }
    
    
    
}