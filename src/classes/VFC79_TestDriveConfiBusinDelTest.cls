@isTest
private class VFC79_TestDriveConfiBusinDelTest {
    
    static testMethod void test() { 
        
        Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [
            SELECT Id, SobjectType, DeveloperName, Name 
            FROM RecordType 
            WHERE (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
                recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
            }

        Account account1 = new Account(
            Name = 'Account Test 01',
            RecordTypeId = recTypeIds.get('Network_Site_Acc'),
            IDBIR__c = '1000');
        insert account1;
        
        Opportunity opp = new Opportunity(
            Name = 'Opportunity Test 01',
            StageName = 'Identified',
            CloseDate = Date.today() + 30,
            OpportunitySource__c = 'NETWORK',
            OpportunitySubSource__c = 'THROUGH',
            AccountId = account1.Id);
        insert opp;

        VEH_Veh__c vehicle1 = new VEH_Veh__c(
            Name = '12111111111111119',
            VehicleRegistrNbr__c = '1000ZB',
            KmCheckDate__c = Date.today(),
            DeliveryDate__c = Date.today() + 30,
            VehicleBrand__c= 'Active',
            KmCheck__c = 100,
            Tech_VINExternalID__c = '9000000009');
        insert vehicle1;

        TDV_TestDriveVehicle__c testDriveVehicle1 = new TDV_TestDriveVehicle__c(
            Account__c = account1.Id,
            AgendaOpeningDate__c = system.today(),
            AgendaClosingDate__c = system.today() + 30,
            Available__c = true,
            Vehicle__c = vehicle1.Id);
        insert testDriveVehicle1;
        
        DateTime dateOfBooking = Datetime.newInstanceGmt( 
            System.now().yearGmt(), 
            System.now().monthGmt(), 
            System.now().dayGmt(), 
            System.now().hourGmt() + 1, 0,0);
        
        TDV_TestDrive__c testDrive1 = new TDV_TestDrive__c(
            DateBooking__c = dateOfBooking,
            Dealer__c = account1.Id,
            TestDriveVehicle__c = testDriveVehicle1.Id,
            Opportunity__c = opp.Id,
            Status__c = 'Scheduled');
        insert testDrive1;
        
        VFC77_TestDriveConfirmationVO testDriveVO = 
            VFC79_TestDriveConfirmBusinessDelegate.getInstance().getTestDriveByOpportunity(opp.Id);
        
        testDriveVO.selectedTime = '10:00';
        testDriveVO.status = 'Confirmed';
        testDriveVO.dateBookingNavigation = Date.today();
        testDriveVO.lstAgendaVehicle = new List<List<VFC64_ScheduleTestDriveVO>>();
        
        VFC79_TestDriveConfirmBusinessDelegate.getInstance().updateTestDrive(testDriveVO);
        
        VFC79_TestDriveConfirmBusinessDelegate.getInstance().getVehicleDetails(testDriveVO);
        VFC79_TestDriveConfirmBusinessDelegate.getInstance().getAgendaVehicle(testDriveVO);
        VFC79_TestDriveConfirmBusinessDelegate.getInstance().getVehiclesAvailable(testDriveVO);
    }
}