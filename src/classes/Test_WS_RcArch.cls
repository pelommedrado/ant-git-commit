@isTest(seeAllData=true)
private class Test_WS_RcArch {
  
  static testMethod void myRCArchTests() {
    myRCArchWebServiceTests(); 
  
  }

   static private void myRCArchWebServiceTests() {
        
        //test cases (see Test_WS_RcArchHttpCalloutMock):
        
        //Prepare the fake response from the WebServic
        
        
        Test.setMock(WebServiceMock.class, new Test_WS_RcArch_WebServiceMock());
        
        
        WS_RCArch_UtilsV2 RCArchUtils = new WS_RCArch_UtilsV2();
        
        /* Create a vehicle standard controller */
       
        /* Prepare the vehicule */
        VEH_Veh__c v = new VEH_Veh__c();
        v.Name = 'VF1BGRG0633285766';
        v.CountryOfDelivery__c='Brazil';
          insert v;
      
   		VFC05_VehicleAttributes vehController = new VFC05_VehicleAttributes(new ApexPages.StandardController(v));
        
        
        /* Prepare the case */
        Case c = new Case();
        
        /* Prepare the attachment */
        Attachment a = new Attachment();
        a.Name = 'test';
       
        /* DO NOT ISERT IT in database : salesforce forgot callout calls with an opened transaction. */
          
        try {
            WebserviceRcArchivageV2.caseId[] caseListRespController = RCArchUtils.getCaseIdList(v.CountryOfDelivery__c,v.Name);

        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }
                
        try {
            WebserviceRcArchivageV2.caseDetails caseDetailsRespController = RCArchUtils.getCaseDetails(v.CountryOfDelivery__c, c.Id);
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }
                 
        try {
            WebserviceRcArchivageV2.caseId[] caseListResponse = RCArchUtils.getCaseIdList(v.CountryOfDelivery__c, v.Name);
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }
                 
        try {
            WebserviceRcArchivageV2.caseDetails caseDetailsReponse = RCArchUtils.getCaseDetails(v.CountryOfDelivery__c, c.Id);
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }
        
        try {
            WebserviceRcArchivageV2.attachmentDetailsList[] attachmentListResp = RCArchUtils.getAttachmentDetailsList(v.CountryOfDelivery__c, c.Id);
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }   
        
        try {
        	
        	String attachContentResp1 = RCArchUtils.getAttachmentContent('test', 'test', 'test');
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }   
   
          try {
        	
        	//String attachContentResp1 = RCArchUtils.getXmlDetails('test', 'test');
        	
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }   
        
        
        
           
        
        /*
        
        try {
            WS_RCArch_UtilsV2.AllCaseInformation AllInformation = new WS_RCArch_UtilsV2.AllCaseInformation();
            
            WebserviceRcArchivageV2.caseDetails caseDetails = new WebserviceRcArchivageV2.caseDetails();
            WebserviceRcArchivageV2.attachmentDetailsList[] tabAttachments = new WebserviceRcArchivageV2.attachmentDetailsList[0];
            
            AllInformation.caseId = c.Id;
            AllInformation.caseDetails = caseDetails; 
            AllInformation.xmlDetails = '';
            AllInformation.tabAttachments = tabAttachments;
            
            WS_RCArch_UtilsV2.AllCaseInformation[] AllInformationResp = RCArchUtils.getAllCasesInformation(vehController);
        } catch (CallOutException e) {
            system.Debug(e.getMessage()); 
        }
        
        */
        

        
    }
}