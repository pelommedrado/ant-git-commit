/**
* Classe responsável pela manipulação dos dados do objeto VehicleBooking__c.
* @author Felipe Jesus Silva.
*/
public class VFC85_VehicleBookingDAO extends VFC01_SObjectDAO
{
	private static final VFC85_VehicleBookingDAO instance = new VFC85_VehicleBookingDAO();
	
	/**
	* Construtor privado para impedir a criação de instancias dessa classe.
	*/
	private VFC85_VehicleBookingDAO()
	{
		
	}
	
	/**
    * Método responsável por prover a instância dessa classe.
    */  
	public static VFC85_VehicleBookingDAO getInstance()
	{
		return instance;
	}
	
	public List<VehicleBooking__c> findByVehicleAndStatus(Set<String> setVehicleId, String status)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = [SELECT Id,
		                                                        Quote__c,
		                                                        Reason_Cancellation__c,
		                                                        Status__c,
		                                                        Vehicle__c
		                                        		 FROM VehicleBooking__c
		                                        		 WHERE Vehicle__c IN : setVehicleId
		                                        		 AND Status__c =: status];
		
		return lstSObjVehicleBooking;
	}
	
	public List<VehicleBooking__c> findByQuoteAndStatus(Set<String> setQuoteId, String status)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = [SELECT Id,
		                                                        Quote__c,
		                                                        Reason_Cancellation__c,
		                                                        Status__c,
		                                                        Vehicle__c
		                                        		 FROM VehicleBooking__c
		                                        		 WHERE Quote__c IN : setQuoteId
		                                        		 AND Status__c =: status];
		
		return lstSObjVehicleBooking;
	}
	
	public List<VehicleBooking__c> findByQuoteAndStatus(String quoteId, String status)
	{
		List<VehicleBooking__c> lstSObjVehicleBooking = [SELECT Id,
		                                                        Quote__c,
		                                                        Reason_Cancellation__c,
		                                                        Status__c,
		                                                        Vehicle__c,
		                                                        ExpireDate__c
		                                        		 FROM VehicleBooking__c
		                                        		 WHERE Quote__c =: quoteId
		                                        		 AND Status__c =: status];
		
		return lstSObjVehicleBooking;
	}
	
	public String buildVehicleBookingQueryForBatchProcess()
	{
		return ' SELECT   Id,' +
						' Quote__c,'+
						' Reason_Cancellation__c,'+
						' Status__c,Vehicle__c,'+
						' ExpireDate__c '+
			   ' FROM ' +
			   			'VehicleBooking__c '+
			   ' WHERE '+
			   			'Status__c = \'Active\''+
			   			'AND Quote__c != null';
		
	}
	
	
	
}