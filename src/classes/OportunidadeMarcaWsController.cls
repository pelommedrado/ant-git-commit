global class OportunidadeMarcaWsController {
    
    webservice static Boolean oppStageIdentified(Id oppId) {
        
        Opportunity opp = [
            SELECT Id, 
            StageName
            FROM Opportunity
            WHERE Id =: oppId
        ];
        
        if(!opp.StageName.equals('Identified')) {
            return true;
        }
        
        return false;
    }
}