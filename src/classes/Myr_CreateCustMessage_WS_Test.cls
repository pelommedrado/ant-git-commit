/** Apex Class used to test the call of the WebService to create the messages
	The goal is not to test the creation of the messages themselves as it is done
	into Myr_CreateCustMessage_Cls_Test but only the behavior of the webservice itself
	
	@author S. Ducamp
	@date 17.07.2015
	@version 1.0
 */
@isTest
private class Myr_CreateCustMessage_WS_Test {
	
	@testSetup static void setCustomSettings() {
        Myr_Datasets_Test.prepareRequiredCustomSettings();
        //initialize a list of customer message settings and customer message templates for tests
        Myr_Datasets_Test.prepareCustomerMessageSettings();
        //Prepare a list of accounts with or without users, with or without vehicles
        Integer maxAcc = 8;
        List<Account> listAccUsers = Myr_Datasets_Test.insertPersonalAccounts( maxAcc, Myr_Datasets_Test.UserOptions.ACTIVE_USERS );
        List<Account> listAccNoUsers = Myr_Datasets_Test.insertPersonalAccounts( maxAcc, Myr_Datasets_Test.UserOptions.NO_USERS );
        //prepare Renault or Dacia Account
        listAccUsers = [SELECT Id, MyR_Status__c, MyD_Status__c FROM Account WHERE Id IN :listAccUsers];
        listAccNoUsers = [SELECT Id, MyR_Status__c, MyD_Status__c FROM Account WHERE Id IN :listAccNoUsers];
        system.AssertEquals( listAccUsers.size(), listAccNoUsers.size());
        for( Integer i = 0; i < listAccUsers.size(); ++i ) {
        	if( Math.mod(i, 2) == 0) {
        		listAccUsers[i].MyR_Status__c=system.Label.Myr_Status_Activated;
        		listAccNoUsers[i].MyR_Status__c=system.Label.Myr_Status_Created;
        	} else {
        		listAccUsers[i].MyD_Status__c=system.Label.Myd_Status_Activated;
        		listAccNoUsers[i].MyD_Status__c=system.Label.Myd_Status_Created;
        	}
        }
        update listAccUsers;
        update listAccNoUsers;
        //Prepare linked vehicles
        List<VEH_Veh__c> listVehicles = Myr_Datasets_Test.getVehicles('CMSGWSTST0', maxAcc, true);
        List<VRE_VehRel__c> listRelations = new List<VRE_VehRel__c>();
        Integer i = 0; 
        while( i < maxAcc ) {
        	listRelations.add( new VRE_VehRel__c(Account__c=listAccUsers[i].Id, VIN__c=listVehicles[i].Id));
        	++i;
        	listRelations.add( new VRE_VehRel__c(Account__c=listAccNoUsers[i].Id, VIN__c=listVehicles[i].Id));
        	++i;
        }
        insert listRelations;
    }

	//TEST: ALL the case of global failures ....
    static testMethod void test_KO_GlobalFailures() {
    	//get user for run
    	User custMsgUsr = Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', false);
        //get accounts
    	Account acc = [SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c FROM Account LIMIT 1]; //inserted in testsetup
    	//get template information
    	Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
    	system.assertNotEquals(null, template);
    	//Insertion using accountSfdcId
    	Myr_CustMessage_Cls msg = new Myr_CustMessage_Cls();
    	msg.accountSfdcId=acc.Id;
    	msg.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg.typeId = 'DEA_EMPTY';
    	msg.country='France';
    	msg.language='fr';
    	
    	//===> GLOBAL FAILURE INTO SEARCH ACCOUNT
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_ACCOUNT;
    	Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: searching accounts: TEST ERROR SEARCH ACCOUNT', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE INTO SEARCH VEHICLE RELATIONS
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_VEHREL;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: searching vehicle relations: TEST ERROR SEARCH VEHICLE RELATIONS', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE INTO SEARCH USERS
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_USER;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: searching users: TEST ERROR SEARCH USERS', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE INTO SEARCH TEMPLATES
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_TEMPL;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: searching templates: TEST ERROR SEARCH TEMPLATES', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE INTO SEARCH DUPLICATES
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.SEARCH_DUP;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: searching duplicates: TEST ERROR SEARCH DUPLICATES', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE INTO DATABASE UPSERT
  		Test.startTest(); //increase the governor limits to finish the test function ...
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.DB_UPSERT;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: upserting records: TEST ERROR DATABASE UPSERT', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		
  		//===> GLOBAL FAILURE
    	msg.failureSimulation = Myr_CustMessage_Cls.GlobalFailureTest.CR_GLOBAL;
    	response = null;
    	system.runAs( custMsgUsr ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS501', response.info.code);
  		system.assertEquals('Global Failure: unexpected error in Myr_CreateCustMessage_Cls.createMessages: TEST ERROR UNEXPECTED ERROR OUTSIDE FROM TRY-CATCH', response.info.message);
  		system.assertEquals(null, response.messageStatus);
  		Test.stopTest();
    }
    
    //TEST: all the messages are ok
    //Note: this is not the goal to test x messages as it is done into Myr_CreateCustMessage_Cls_Test.test_OK_MassiveCreationSyncStatusCode
    static testMethod void test_OK_AllTheMessagesOK() {
        //get accounts
    	Account acc = [SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c FROM Account LIMIT 1]; //inserted in testsetup
    	//get template information
    	Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
    	system.assertNotEquals(null, template);
    	//Insertion using accountSfdcId
    	Myr_CustMessage_Cls msg = new Myr_CustMessage_Cls();
    	msg.accountSfdcId=acc.Id;
    	msg.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg.typeId = 'DEA_EMPTY';
    	msg.country='France';
    	msg.language='fr';
    	
    	//Launch as technical customer message user to test rights
    	Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = null;
    	system.runAs( Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', false) ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS000', response.info.code);
  		system.assertEquals('OK, check messageStatus list for more details', response.info.message);  	
  		system.assertNotEquals(null, response.messageStatus);
  		system.assertEquals(1, response.messageStatus.size());
  		system.assertEquals('WS06MS000', response.messageStatus[0].code);
  		system.assertEquals('Notification created with success', response.messageStatus[0].message);
    }
    
    //TEST: some messages are ok and others not
    //Note: this is not the goal to test x messages as it is done into Myr_CreateCustMessage_Cls_Test.test_OK_MassiveCreationSyncStatusCode
    static testMethod void test_OK_MessagesWithErrors() {
        //get accounts
    	List<Account> listAcc = [SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c FROM Account WHERE isCustomerPortal=false LIMIT 3]; //inserted in testsetup
    	system.assertEquals(3, listAcc.size());
    	//get template information
    	Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
    	system.assertNotEquals(null, template);
    	//Insertion Acc 1 ==> OK
    	Myr_CustMessage_Cls msg1 = new Myr_CustMessage_Cls();
    	msg1.accountSfdcId=listAcc[0].Id;
    	msg1.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg1.typeId = 'DEA_EMPTY';
    	msg1.country='France';
    	msg1.language='fr';
    	//Insertion Acc 2 ==> missing country
    	Myr_CustMessage_Cls msg2 = new Myr_CustMessage_Cls();
    	msg2.accountSfdcId=listAcc[1].Id;
    	msg2.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg2.typeId = 'DEA_EMPTY';
    	msg2.language='fr';
    	//Insertion Acc 3 ==> bad vin
    	Myr_CustMessage_Cls msg3 = new Myr_CustMessage_Cls();
    	msg3.accountSfdcId=listAcc[2].Id;
    	msg3.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg3.typeId = 'DEA_EMPTY';
    	msg3.language='fr';
    	msg3.vin='XX';
    	
    	//Launch as technical customer message user to test rights
    	Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = null;
    	system.runAs( Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', false) ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg1, msg2, msg3} );
    	}
    	
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS100', response.info.code);
  		system.assertEquals('Failed to insert SOME messages', response.info.message);  	
  		system.assertNotEquals(null, response.messageStatus);
  		system.assertEquals(3, response.messageStatus.size());
  		system.assertEquals('WS06MS000', response.messageStatus[0].code);
  		system.assertEquals('Notification created with success', response.messageStatus[0].message);
  		system.assertEquals('WS06MS502', response.messageStatus[1].code);
  		system.assertEquals('Invalid parameter: country and/or language not given in inputs or not found on the user linked to the account', response.messageStatus[1].message);
  		system.assertEquals('WS06MS502', response.messageStatus[2].code);
  		system.assertEquals('Invalid parameter: vin has not the proper format (17 alphanumeric characters)', response.messageStatus[2].message);
    }
    
    //TEST: All the messages are wrong
    //Note: this is not the goal to test x messages as it is done into Myr_CreateCustMessage_Cls_Test.test_OK_MassiveCreationSyncStatusCode
    static testMethod void test_KO_GlobalFailedWhenInsertingMessages() {
        //get accounts
    	Account acc = [SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c FROM Account LIMIT 1]; //inserted in testsetup
    	//get template information
    	Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('DEA_EMPTY', 'France', 'fr', 'Renault');
    	system.assertNotEquals(null, template);
    	//Insertion using accountSfdcId ==> missing country
    	Myr_CustMessage_Cls msg = new Myr_CustMessage_Cls();
    	msg.accountSfdcId=acc.Id;
    	msg.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg.typeId = 'DEA_EMPTY';
    	msg.language='fr';
    	
    	//Launch as technical customer message user to test rights
    	Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = null;
    	system.runAs( Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', false) ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg} );
    	}
    	
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
  		system.assertEquals('WS06MS502', response.info.code);
  		system.assertEquals('Failed to insert ALL the messages', response.info.message);  	
  		system.assertNotEquals(null, response.messageStatus);
  		system.assertEquals(1, response.messageStatus.size());
  		system.assertEquals('WS06MS502', response.messageStatus[0].code);
  		system.assertEquals('Invalid parameter: country and/or language not given in inputs or not found on the user linked to the account', response.messageStatus[0].message);
    }
    
	//TEST: a message has been updated with success
    //Note: general code should be ok whereas previously it was KO
    static testMethod void test_UATPROB_GeneralCodeWithUpdate() {
        //get accounts
    	List<Account> listAcc = [SELECT Id, Tech_ACCExternalID__c, Bcs_Id__c, Bcs_Id_Dacia__c, PeopleId_CN__c FROM Account WHERE isCustomerPortal=false LIMIT 3]; //inserted in testsetup
    	system.assertEquals(3, listAcc.size());
    	//get template information
    	Customer_Message_Templates__c template = Myr_Datasets_Test.getTemplatesByNameCountryLangBrand('GAR_VEH_ADDED', 'France', 'fr', 'Renault');
    	system.assertNotEquals(null, template);
		system.assertEquals( 'replace', template.MessageType__r.MergeAction__c);
		
		//Insert an existing message
		VRE_VehRel__c vre = [SELECT Id, Account__c, VIN__c, VIN__r.Name FROM VRE_VehRel__c LIMIT 1]; //inserted in testsetup

		Customer_Message__c msg = new Customer_Message__c();
    	msg.Template__c = template.Id;
    	msg.Title__c = 'Existing message title';
    	msg.Body__c = 'Existing message body';
    	msg.Summary__c = 'Existing body body';
    	msg.Status__c = system.Label.Customer_Message_Unread;
    	msg.Account__c = vre.Account__c;
    	msg.Channel__c = 'message';
		msg.vin__c = vre.VIN__c;
		insert msg;

    	//Update Acc 1 ==> OK
    	Myr_CustMessage_Cls msg1 = new Myr_CustMessage_Cls();
    	msg1.accountSfdcId=vre.Account__c;
    	msg1.channels = new List<Myr_CustMessage_Cls.Channel>{Myr_CustMessage_Cls.Channel.email};
    	msg1.typeId = 'GAR_VEH_ADDED';
    	msg1.country='France';
    	msg1.language='fr';
		msg1.vin=vre.VIN__r.Name;
		msg1.input1 = 'CLIO IV';
    	//Launch as technical customer message user to test rights
    	Myr_CreateCustMessage_WS.Myr_CreateCustMessage_WS_Response response = null;
    	system.runAs( Myr_Datasets_Test.getCustMsgUser('Myr', 'CreateCustMsgTst', false) ) {
    		 response = Myr_CreateCustMessage_WS.createMessages( new List<Myr_CustMessage_Cls>{msg1} );
    	}
    	
    	system.assertNotEquals(null, response);
  		system.assertNotEquals(null, response.info);
  		system.assertNotEquals(null, response.info.code);
  		system.assertNotEquals(null, response.info.message);
		//message status
		system.assertEquals(1, response.messageStatus.size());
  		system.assertEquals('WS06MS001', response.messageStatus[0].code);
  		system.assertEquals('Notification replaced with success', response.messageStatus[0].message);
		//general status
  		system.assertEquals('WS06MS000', response.info.code);
  		system.assertEquals(system.Label.Customer_Message_WS06MS000_Msg_WS, response.info.message);  	
    }
}