/**
  * Class Name : Rforce_BcsRbxResponse_Class
  * Descripion : This is a POJO class which will be used to set the values from BCS andSIC Webservices.
  * Date : 10-Apr-2015
  * Author:Vetrivel S
  * HQREQ-01155 
  * Description :BCS WS Mapping
**/


public class Rforce_BcsRbxResponse_Class {

        public String IdClient{get;set;}
        public String LastName{get;set;}
        public String Title{get;set;}
        public String Lang{get;set;}
        public String FirstName{get;set;}
        public String email{get;set;}
        public String typeperson{get;set;}
        public String GlobalCommAgreement{get;set;}
        public String PostCommAgreement{get;set;}
        public String TelCommAgreemen{get;set;}
        public String SMSCommAgreement{get;set;}
        public String FaxCommAgreement{get;set;}
        public String EmailCommAgreement{get;set;}
        public String phoneNum1{get;set;}
        public String phoneNum2{get;set;}
        public String phoneNum3{get;set;}
        public String phoneCode1{get;set;}
        public String phoneCode2{get;set;}
        public String phoneCode3{get;set;}
        public String StrName{get;set;}
        public String Compl1{get;set;}
        public String Compl2{get;set;}
        public String Compl3{get;set;}
        public String CountryCode{get;set;}
        public String Zip{get;set;}
        public String City{get;set;}
        public String vin {get;set;}
        
        // Added for new BCS Mapping 27-Mar-2015
        
        public String middleName{get;set;}
        public String localCustomerID1{get;set;}
        public String localCustomerID2{get;set;}
        public String birthDate{get;set;}
        public String sex{get;set;}
        public String companyID{get;set;}
        public String myRenaultID{get;set;}
        public String customerType{get;set;}
        public String preferredCommunication{get;set;}
        public String areaCode{get;set;}
        public String areaLabel{get;set;}
        public String streetType{get;set;}
        public String strNumber{get;set;}
        
        
        // Ended for new BCS Mapping 27-Mar-2015
        
        public List<Rforce_BcsRbxResponse_Class.Vehicle> vcle {get;set;}

        public class Vehicle{
          public String vin {get;set;}
          public String IdClient{get;set;}
          public String brandCode {get;set;}
          public String modelCode {get;set;}
          public String modelLabel {get;set;}
          public String versionLabel {get;set;}
          public String firstRegistrationDate{get;set;}
          public String registration{get;set;}
          public String possessionBegin{get;set;}
          
          /**
             * Author : Vetrivel Sundararajan
             * Date   : 25-Sept-2014
             * Description: Enanchement to add Vehicle and Vehicle Relations for the Account Search in BCS and SIC WS 
             * SCRUM ID  : 315   
             * RO Number : N/A
             * Release No: R5
             * Sprint ID : 4
           */
          public String vehicleType{get;set;}
          public String possessionEnd{get;set;}
          public String lastRegistrationDate{get;set;}
        
    }
 }