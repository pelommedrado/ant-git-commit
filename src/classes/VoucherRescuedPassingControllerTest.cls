@isTest
public class VoucherRescuedPassingControllerTest {
    
    public static testMethod void teste1(){
        
        MyOwnCreation moc = new MyOwnCreation();
        
        User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        User communityUser;
        Account dealer;
        CampaignMember cm;
        
        //Execução com usuario local para criar usuario da comunidade
        System.runAs(localUser) {
            
            //necessario usuario possuir papel
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
            localUser.UserRoleId = r.Id;
            update localUser;
            
            dealer = moc.criaAccountDealer();
            insert(dealer);
            
            Contact contact = moc.criaContato();
            contact.AccountId = dealer.Id;
            insert(contact);
            
            communityUser = moc.criaUser();
            communityUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            communityUser.ContactId = contact.Id;
            insert(communityUser);  
            
            Lead voucher = moc.CriaLead();
            voucher.Email = 'teste@email.com';
            voucher.CPF_CNPJ__c = '34330404192';
            voucher.RecordTypeId = Utils.getRecordTypeId('Lead', 'Voucher');
            Insert voucher;
            
            Campaign campanha = moc.criaCampanha();
            campanha.DealerShareAllowed__c = true;
            campanha.Billing_Required__c = true;
            Insert campanha;
            
            cm = moc.criaMembroCampanha();
            cm.CampaignId = campanha.Id;
            cm.LeadId = voucher.Id;
            Insert cm;
            
        }
        
        system.runAs(communityUser){
            
            Test.startTest();
            
            // atribui conjunto de permissao BR_Leads_Montadora ao usuario para trigger de conta nao converter o lead
            Id permissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_Leads_Montadora' limit 1].Id;
            PermissionSetAssignment ps = new PermissionSetAssignment();
            ps.PermissionSetId = permissionSetId;
            ps.AssigneeId = UserInfo.getUserId();
            Insert ps;

            PageReference pageRef = new PageReference('/apex/VoucherRescuedPassing?cmId='+cm.Id);
            Test.setCurrentPage(pageRef);
            
            VoucherRescuedPassingController controller = new VoucherRescuedPassingController();
            
            controller.registraPassagem();
            List<Account> lsAcc = [Select Id FROM Account WHERE CustomerIdentificationNbr__c = '34330404192'];
            List<Opportunity> lsOpp = [Select Id FROM Opportunity WHERE Account_CPF__c = '34330404192'];
            
            // deve retornar uma conta e oportunidade do lead convertido
            system.assertEquals(1, lsAcc.size());
            system.assertEquals(1, lsOpp.size());
            
            // metodo get deve retornar mesmo Status
            system.assertEquals('Billed', controller.status = 'Billed');

            // deve retornar o mesmo URL do metodo
            pageReference pg = controller.loadPagePromotionalActions();
            system.debug('pg: '+pg);
            system.assertEquals('/apex/PromotionalActions', pg.getUrl()); 

            Test.stopTest();
            
        }
    }
}