@isTest
public class Rforce_W2CwithAttachmentService_Test{
    static testMethod void createcasewithattachment(){
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        
        List<Rforce_W2CSchema_CLS.CaseAttachments> attach= new List<Rforce_W2CSchema_CLS.CaseAttachments>();
        Rforce_W2CSchema_CLS.CaseAttachments att=new Rforce_W2CSchema_CLS.CaseAttachments();
        att.attachmentBody=Blob.valueof('sdfadfs');
        att.attachmentName='test';
        att.attachmentContentType='doc';
        attach.add(att);
        
        Rforce_W2CSchema_CLS.Casedetails casedetail=new Rforce_W2CSchema_CLS.Casedetails();
        casedetail.Priority='High';
        casedetail.emailOptIn='True';
        casedetail.description='test web service';
        casedetail.licenseNumber='asdfasf';
        casedetail.suppliedPhone='455656';
        casedetail.City='France';
        casedetail.postalCode='605004';
        casedetail.address='test';
        casedetail.lastName='lname';
        casedetail.firstName='fname';
        casedetail.comAgreement='true';
        casedetail.state='Paris';
        casedetail.vin='12345678912345';
        casedetail.dealer='test';
        casedetail.suppliedEmail='test123@gmail.com';
        casedetail.country='France';
        casedetail.language='French';
        casedetail.caseFrom='Customer';
        casedetail.detail='';
        casedetail.subType='';
        casedetail.type='Complaint';
        casedetail.subSource='RENAULT SITE';
        casedetail.caseBrand='Renault';
        casedetail.origin='RENAULT SITE';
        casedetail.subject='test';
        casedetail.cellPhone='3454';
        casedetail.company='dfgdf';
        casedetail.title='Mr.';
        casedetail.attachmentdetails=attach;
        Test.startTest();
        Rforce_W2CwithAttachmentService.createCaseAndAttachment(casedetail);        
        Test.stopTest();
    }
   
    static testMethod void createcasewithattachment2(){
         Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        
        List<Rforce_W2CSchema_CLS.CaseAttachments> attach= new List<Rforce_W2CSchema_CLS.CaseAttachments>();
        Rforce_W2CSchema_CLS.CaseAttachments att=new Rforce_W2CSchema_CLS.CaseAttachments();
        att.attachmentBody=Blob.valueof('sdfadfs');
        att.attachmentName='test';
        att.attachmentContentType='';
        attach.add(att);
        
        Rforce_W2CSchema_CLS.Casedetails casedetail=new Rforce_W2CSchema_CLS.Casedetails();
        casedetail.Priority='High';
        casedetail.emailOptIn='True';
        casedetail.description='test web service';
        casedetail.licenseNumber='asdfasf';
        casedetail.suppliedPhone='455656';
        casedetail.City='France';
        casedetail.postalCode='605004';
        casedetail.address='test';
        casedetail.lastName='lname';
        casedetail.firstName='fname';
        casedetail.comAgreement='true';
        casedetail.state='Paris';
        casedetail.vin='12345678912345';
        casedetail.dealer='test';
        casedetail.suppliedEmail='test123@gmail.com';
        casedetail.country='France';
        casedetail.language='French';
        casedetail.caseFrom='Customer';
        casedetail.detail='';
        casedetail.subType='';
        casedetail.type='Complaint';
        casedetail.subSource='RENAULT SITE';
        casedetail.caseBrand='Renault';
        casedetail.origin='RENAULT SITE';
        casedetail.subject='test';
        casedetail.cellPhone='3454';
        casedetail.company='dfgdf';
        casedetail.title='Mr.';
        casedetail.attachmentdetails=attach;
        Test.startTest();
        Rforce_W2CwithAttachmentService.createCaseAndAttachment(casedetail);        
        Test.stopTest();
    }    
    static testMethod void createcasewithattachmentVOCD(){
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        
        List<Rforce_W2CSchema_CLS.CaseAttachments> attach= new List<Rforce_W2CSchema_CLS.CaseAttachments>();
        Rforce_W2CSchema_CLS.CaseAttachments att=new Rforce_W2CSchema_CLS.CaseAttachments();
        att.attachmentBody=Blob.valueof('sdfadfs');
        att.attachmentName='test';
        att.attachmentContentType='doc';
        attach.add(att);
        
        Rforce_W2CSchema_CLS.Casedetails casedetail=new Rforce_W2CSchema_CLS.Casedetails();
        casedetail.Priority='High';
        casedetail.emailOptIn='True';
        casedetail.description='test vocd';
        casedetail.licenseNumber='asdfasf';
        casedetail.suppliedPhone='455656';
        casedetail.City='France';
        casedetail.postalCode='605004';
        casedetail.address='test';
        casedetail.lastName='lname';
        casedetail.firstName='fname';
        casedetail.comAgreement='true';
        casedetail.state='Paris';
        casedetail.vin='12345678912345';
        casedetail.dealer='test';
        casedetail.suppliedEmail='test123@gmail.com';
        casedetail.country='France';
        casedetail.language='French';
        casedetail.caseFrom='Customer';
        casedetail.detail='VOC Dealer Sales';
        casedetail.subType='VOC Dealer';
        casedetail.type='Complaint';
        casedetail.subSource='VOC Dealer';
        casedetail.caseBrand='Renault';
        casedetail.origin='VOC';
        casedetail.subject='test';
        casedetail.cellPhone='3454';
        casedetail.company='dfgdf';
        casedetail.title='Mr.';
        casedetail.customerExperience='customer';
        casedetail.reasonOfReturn='reason';
        casedetail.problemSolved='solved';
        casedetail.brandRecommendation='recommend';
        casedetail.attachmentdetails=attach;
        Test.startTest();
        Rforce_W2CwithAttachmentService.createCaseAndAttachment(casedetail);        
        Test.stopTest();
    }
      static testMethod void createcasewithattachmentVOCC(){
        Country_Info__c ctr = new Country_Info__c (Name = 'France', Country_Code_2L__c = 'FR', Language__c = 'Français', Case_RecordType__c='FR_Case_RecType');
        insert ctr;
        
        List<Rforce_W2CSchema_CLS.CaseAttachments> attach= new List<Rforce_W2CSchema_CLS.CaseAttachments>();
        Rforce_W2CSchema_CLS.CaseAttachments att=new Rforce_W2CSchema_CLS.CaseAttachments();
        att.attachmentBody=Blob.valueof('sdfadfs');
        att.attachmentName='test';
        att.attachmentContentType='doc';
        attach.add(att);
        
        Rforce_W2CSchema_CLS.Casedetails casedetail=new Rforce_W2CSchema_CLS.Casedetails();
        casedetail.Priority='High';
        casedetail.emailOptIn='True';
        casedetail.description='vocc';
        casedetail.licenseNumber='asdfasf';
        casedetail.suppliedPhone='455656';
        casedetail.City='France';
        casedetail.postalCode='605004';
        casedetail.address='test';
        casedetail.lastName='lname';
        casedetail.firstName='fname';
        casedetail.comAgreement='true';
        casedetail.state='Paris';
        casedetail.vin='12345678912345';
        casedetail.dealer='test';
        casedetail.suppliedEmail='test123@gmail.com';
        casedetail.country='France';
        casedetail.language='French';
        casedetail.caseFrom='Customer';
        casedetail.detail='VOC Call Center Info Request';
        casedetail.subType='VOC Call Center';
        casedetail.type='Complaint';
        casedetail.subSource='VOC Call Center';
        casedetail.caseBrand='Renault';
        casedetail.origin='VOC';
        casedetail.subject='test';
        casedetail.cellPhone='3454';
        casedetail.company='dfgdf';
        casedetail.title='Mr.';
        casedetail.customerExperience='customer';
        casedetail.reasonOfReturn='reason';
        casedetail.problemSolved='solved';
        casedetail.brandRecommendation='recommend';
        Case c=new case(From__c='Customer',Language_Web__c='French',PT_Dealer_Web__c='dealer',VIN_Web__c='235235235235236236');
        insert c;
        Case cs=[Select CaseNumber from Case where Id=:c.Id];  
        casedetail.caseNumber=cs.CaseNumber;
        casedetail.buyAgainIntention='buyy';
        casedetail.attachmentdetails=attach;
        Test.startTest();
        Rforce_W2CwithAttachmentService.createCaseAndAttachment(casedetail);        
        Test.stopTest();
    }
}