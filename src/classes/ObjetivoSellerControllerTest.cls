@isTest
private class ObjetivoSellerControllerTest {
	
	static Monthly_Goal_Group__c goalGroup;
	static Monthly_Goal_Dealer__c goalDealer;
	static Monthly_Goal_Seller__c goalSeller;
	static User userSeller;
	static Account dealer;

	static{

		User localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];

		System.runAs(localUser) {

			dealer = AccountBuild.getInstance().createAccountDealer();
			dealer.Dealer_Matrix__c = '1234';
			insert dealer;

			Contact userCnt = ContactBuild.getInstance().createContact(dealer.Id);
			insert userCnt;

			userSeller = UserBuild.getInstance().createUser(userCnt.Id, [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id);
			insert userSeller;

			goalGroup = MonthlyGoalBuild.getInstance().createGoalGroup('June', 2017, '1234');
			insert goalGroup;

			goalDealer = MonthlyGoalBuild.getInstance().createGoalDealer(goalGroup.Id, dealer.Id);
			insert goalDealer;

			goalSeller = MonthlyGoalBuild.getInstance().createGoalSeller(goalDealer.Id, userSeller.Id);
			insert goalSeller;

		}	

	}
	
	@isTest static void shouldInitManager() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldInitExecutive() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Executive'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldUpdateMdg() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
			objCtrl.updateMgd();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldDistributeValues() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
			objCtrl.distribuirValores();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldUpdateSallerValues() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
			objCtrl.updateValoresSeller();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldSave() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
			objCtrl.salvar();
		}
		

		Test.stopTest();

	}

	@isTest static void shouldAssignDealer() {
		
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.ObjetivoSeller')); 
		System.currentPageReference().getParameters().put('mes', 'June');
		System.currentPageReference().getParameters().put('ano', '2017');

		PermissionSetAssignment pset = new PermissionSetAssignment();
		pset.AssigneeId = userSeller.Id;
		pset.PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'BR_SFA_SalesWay_Manager'].Id;
		insert pset;

		System.runAs(userSeller){
			ObjetivoSellerController objCtrl = new ObjetivoSellerController();
			objCtrl.atribuirDealer();
		}
		

		Test.stopTest();

	}
	
}