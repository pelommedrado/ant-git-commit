public class VFC01_VehicleSearchWebService {
public String VIN {get;set;}
public String CodeLangue {get;set;}
public String ErrorMessage ;

//default constructor
public VFC01_VehicleSearchWebService() {

}

public void Call(String VIN, String CodeLangue){
       Boolean Success ;
       string strDebut = 'Début Trace Réponse WS';
       string strFin = 'Fin Trace Réponse WS';
       //WS04_CustdataCrmBserviceRenault.CrmGetCustData callout = new WS04_CustdataCrmBserviceRenault.CrmGetCustData() ;
       //WS04_CustdataCrmBserviceRenault.CustDataRequest info = new WS04_CustdataCrmBserviceRenault.CustDataRequest();
       //WS04_CustdataCrmBserviceRenault.GetCustDataRequest request = new WS04_CustdataCrmBserviceRenault.GetCustDataRequest();
       //WS04_CustdataCrmBserviceRenault.GetCustDataResponse result = new WS04_CustdataCrmBserviceRenault.GetCustDataResponse();
       WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();               
       WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
       WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
       WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
       
        try{  
               servicePref.bvmsi25SocEmettrice  = System.label.SocieteEmettrice;
               servicePref.bvmsi25Vin = 'VF1BGRG0633285766';
               servicePref.bvmsi25CodePaysLang = 'FRA';
               servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase();    
               request.ServicePreferences = servicePref;
               
               VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
               VehWS.clientCertName_x = System.label.RenaultCertificate;           
               VehWS.timeout_x=40000;
            
               VEH_WS = VehWS.getApvGetDetVehXml(request);     
               WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse rep = VEH_WS;
               system.debug(strDebut);
               system.debug('FTA '+VEH_WS);
               system.debug('FTA2 '+VEH_WS.detVeh.dateLiv);
               system.debug(strFin);
        }            
        catch(exception e){                    
            ErrorMessage = String.valueOf(e);
            system.debug(VEH_WS);
            system.debug('erreur : ' + String.valueOf(e));
        }
          
}
public void submitWS() {

System.debug('#### vin : ' + VIN);
System.debug('#### CodeLangue : ' + CodeLangue);

Call( VIN, CodeLangue);

}
}