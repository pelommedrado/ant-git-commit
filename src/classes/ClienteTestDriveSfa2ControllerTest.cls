@isTest
private class ClienteTestDriveSfa2ControllerTest {

    static testMethod void unitTest01()
    {
        // Prepare Test Data
        Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        Account account1 = new Account(Name = 'Account Test 01',
                                       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                       IDBIR__c = '1000');
        insert account1;
        
        Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
                                                   StageName = 'Identified',
                                                   CloseDate = Date.today() + 30,
                                                   OpportunitySource__c = 'NETWORK',
                                                   OpportunitySubSource__c = 'THROUGH',
                                                   AccountId = account1.Id);
        insert opportunity1;
        
        VEH_Veh__c vehicle1 = new VEH_Veh__c(Name = '12111111111111119',
                                             VehicleRegistrNbr__c = '1000ZB',
                                             KmCheckDate__c = Date.today(),
                                             DeliveryDate__c = Date.today() + 30,
                                             VehicleBrand__c= 'Active',
                                             KmCheck__c = 100,
                                             Tech_VINExternalID__c = '9000000009');
        insert vehicle1;
        
        TDV_TestDriveVehicle__c testDriveVehicle1 = new TDV_TestDriveVehicle__c(Account__c = account1.Id,
                                                                                AgendaOpeningDate__c = system.today(),
                                                                                AgendaClosingDate__c = system.today() + 30,
                                                                                Available__c = true,
                                                                                Vehicle__c = vehicle1.Id);
        insert testDriveVehicle1;
        
        DateTime dateOfBooking = Datetime.newInstanceGmt( system.now().yearGmt(), system.now().monthGmt(), system.now().dayGmt(), system.now().hourGmt() + 1, 0,0);
        TDV_TestDrive__c testDrive1 = new TDV_TestDrive__c(DateBooking__c = dateOfBooking,
                                                           Dealer__c = account1.Id,
                                                           TestDriveVehicle__c = testDriveVehicle1.Id,
                                                           Opportunity__c = opportunity1.Id,
                                                           Status__c = 'Scheduled');
        insert testDrive1;
        
        List<Product2> lstProducts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
        
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        // Start Test
        Test.startTest();
        
        ClienteTestDriveSfa2Controller  controller = new ClienteTestDriveSfa2Controller (stdCont);
        controller.initialize();
        controller.testDriveVO = new VFC54_TestDriveVO();
        controller.testDriveVO.selectedTime = '10:00';
        controller.testDriveVO.status = 'other';
        controller.testDriveVO.dateBookingNavigation = Date.today();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC64_ScheduleTestDriveVO>>();
        controller.testDriveVO.testDriveVehicleId = testDriveVehicle1.Id;
        
        controller.cancelOpportunity();
        controller.completeTestDrive();
        controller.confirmGenerationQuote();
        controller.confirmSchedulingTestDrive();
        controller.generateQuote();
        controller.getAgendaNextDay();
        controller.getAgendaPreviousDay();
        controller.openPopUpCancellation();
        controller.performNewTestDrive();
        //controller.processReasonCancellationChange();
        controller.processReasonCancellationTestDriveChange();
        controller.processStatusChange();
        controller.processVehicleAvailableChange();
        controller.verifyGenerateQuote();
        controller.verifyStatus();
        controller.processVehicleInterestChange();
        
        // Stop Test
        Test.stopTest();
    }
 
    static testMethod void unitTest02()
    {
        // Prepare Test Data
        Map<String,ID> recTypeIds = new Map<String,ID>();
        for (RecordType rectyp : [select Id, SobjectType,DeveloperName, Name from RecordType where (SobjectType = 'Account' and DeveloperName in ('Network_Site_Acc'))]) {
            recTypeIds.put(rectyp.DeveloperName, rectyp.Id);
        }
        Account account1 = new Account(Name = 'Account Test 01',
                                       RecordTypeId = recTypeIds.get('Network_Site_Acc'),
                                       IDBIR__c = '1000');
        insert account1;
        
        Opportunity opportunity1 = new Opportunity(Name = 'Opportunity Test 01',
                                                   StageName = 'Identified',
                                                   CloseDate = Date.today() + 30,
                                                   OpportunitySource__c = 'NETWORK',
                                                   OpportunitySubSource__c = 'THROUGH',
                                                   AccountId = account1.Id);
        insert opportunity1;
        
        VEH_Veh__c vehicle1 = new VEH_Veh__c(Name = '12111111111111119',
                                             VehicleRegistrNbr__c = '1000ZB',
                                             KmCheckDate__c = Date.today(),
                                             DeliveryDate__c = Date.today() + 30,
                                             VehicleBrand__c= 'Active',
                                             KmCheck__c = 100,
                                             Tech_VINExternalID__c = '9000000009');
        insert vehicle1;
        
        TDV_TestDriveVehicle__c testDriveVehicle1 = new TDV_TestDriveVehicle__c(Account__c = account1.Id,
                                                                                AgendaOpeningDate__c = system.today(),
                                                                                AgendaClosingDate__c = system.today() + 30,
                                                                                Available__c = true,
                                                                                Vehicle__c = vehicle1.Id);
        insert testDriveVehicle1;
        
        DateTime dateOfBooking = Datetime.newInstanceGmt( system.now().yearGmt(), system.now().monthGmt(), system.now().dayGmt(), system.now().hourGmt() + 1, 0,0);
        TDV_TestDrive__c testDrive1 = new TDV_TestDrive__c(DateBooking__c = dateOfBooking,
                                                           Dealer__c = account1.Id,
                                                           TestDriveVehicle__c = testDriveVehicle1.Id,
                                                           Opportunity__c = opportunity1.Id,
                                                           Status__c = 'Scheduled');
        insert testDrive1;
        
        List<Product2> lstProducts = VFC03_InsertSObjectsRecordsForTestClass.getInstance().insertRecordsToProductObjects();
        
        Apexpages.currentPage().getParameters().put('id', opportunity1.Id);
        ApexPages.StandardController stdCont = new ApexPages.StandardController(opportunity1);
        
        // Start Test
        Test.startTest();
        
        ClienteTestDriveSfa2Controller  controller = new ClienteTestDriveSfa2Controller (stdCont);
        controller.initialize();
        controller.testDriveVO = new VFC54_TestDriveVO();
        controller.testDriveVO.selectedTime = '10:00';
        controller.testDriveVO.status = 'Not Performed';
        controller.testDriveVO.dateBookingNavigation = Date.today();
        controller.testDriveVO.lstAgendaVehicle = new List<List<VFC64_ScheduleTestDriveVO>>();
        controller.testDriveVO.testDriveVehicleId = testDriveVehicle1.Id;
        
        controller.cancelOpportunity();
        controller.completeTestDrive();
        controller.confirmGenerationQuote();
        controller.confirmSchedulingTestDrive();
        controller.generateQuote();
        controller.getAgendaNextDay();
        controller.getAgendaPreviousDay();
        controller.openPopUpCancellation();
        controller.performNewTestDrive();
        //controller.processReasonCancellationChange();
        controller.processReasonCancellationTestDriveChange();
        controller.processStatusChange();
        controller.processVehicleAvailableChange();
        controller.verifyGenerateQuote();
        controller.verifyStatus();
        
        // Stop Test
        Test.stopTest();
    }
    
}