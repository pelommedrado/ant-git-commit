@isTest
private class ActivityManagerBoTest {
	private static User user;
	private static User userTest;
	private static Account accDealer;
	static {
			User usr = [Select id from User where Id = :UserInfo.getUserId()];

			//necessario usuario possuir papel
			UserRole r = [SELECT Id FROM UserRole WHERE Name='Americas'];
			usr.UserRoleId = r.Id;
			Update usr;

			System.RunAs(usr) {
					//Test.startTest();

					accDealer = MyOwnCreation.getInstance().criaAccountDealer();
					INSERT accDealer;

					Contact ctt = new Contact(
							CPF__c 		=  '44476157114',
							Email 		= 'manager1@org1.com',
							FirstName 	= 'User',
							LastName 	= 'Manager1',
							MobilePhone = '11223344556',
							AccountId 	= accDealer.Id
					);

					INSERT ctt;

					Contact cttTest = new Contact(
							CPF__c 		=  '44476157114',
							Email 		= 'user.test.@test.com',
							FirstName 	= 'User',
							LastName 	= 'Test',
							MobilePhone = '11223344556',
							AccountId 	= accDealer.Id
					);

					INSERT cttTest;

					user = new User(
							ContactId 			= ctt.Id,
							ProfileId 			= Utils.getProfileId('BR - Renault + Cliente Manager'),
							FirstName 			= 'User',
							LastName 			= 'Manager1',
							Username 			= 'manager1@org1.com.teste',
							Email 				= 'manager1@org1.com',
							Alias 				= 'ma1',
							RecordDefaultCountry__c = 'Brazil',
							BIR__c 				= '123456',
							EmailEncodingKey	='UTF-8',
							LanguageLocaleKey	='en_US',
							LocaleSidKey		='en_US',
							TimeZoneSidKey		='America/Los_Angeles'
					);

					INSERT user;

					userTest = new User(
							ContactId 			= cttTest.Id,
							ProfileId 			= Utils.getProfileId('BR - Renault + Cliente Manager'),
							FirstName 			= 'User',
							LastName 			= 'Manager1',
							Username 			= 'test@org1.com.teste',
							Email 				= 'test@org1.com',
							Alias 				= 'test',
							RecordDefaultCountry__c = 'Brazil',
							BIR__c 				= '123456',
							EmailEncodingKey	='UTF-8',
							LanguageLocaleKey	='en_US',
							LocaleSidKey		='en_US',
							TimeZoneSidKey		='America/Los_Angeles'
					);

					INSERT userTest;
			}
	}

	@isTest
	static void deveCriarActivityManagerBoParaInsert() {
		System.RunAs(user) {
			final Opportunity oppNew = createOpp();
			final List<Opportunity> oppNewList = new List<Opportunity> { oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, new Map<Id, Opportunity>());
			System.assertEquals(0, activityManager.proprietarioOppList.size());
		}
	}

	@isTest
	static void deveCriarActivityManagerBoParaUpdate() {
		System.RunAs(user) {
			Opportunity oppOld = createOpp();
			oppOld.Id = '0066E000002cVlCQAU';
			Opportunity oppNew  = oppOld.clone(true, false, false, false);
			oppNew.OwnerId = userTest.Id;

			List<Opportunity> oppOldList = new List<Opportunity>{ oppOld };
			Map<Id, Opportunity> oppOldMap = new Map<Id, Opportunity>(oppOldList);

			final List<Opportunity> oppNewList = new List<Opportunity> { oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, oppOldMap);
			System.assertEquals(1, activityManager.proprietarioOppList.size());
		}
	}

	@isTest
	static void deveCriarAtividadeConfirmVisit() {
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppNew   = createOpp();
			final List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, new Map<Id, Opportunity>());

			List<Event> eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.isEmpty(), 'Não deve retorno resultado');

			activityManager.execInsert();

			eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.size() == 1, 'Atividade Confirm Visit nao foi criada');
		}
	}

	@isTest
	static void deveCriarAtividadeConfirmVisitComDetail() {
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppNew   = createOpp();
			oppNew.Detail__c = 'Nao e Test Drive';
			oppNew.OpportunitySource__c = 'Nao e Dealer';

			final List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, new Map<Id, Opportunity>());

			List<Event> eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.isEmpty(), 'Não deve retorno resultado');

			activityManager.execInsert();

			eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.size() == 1, 'Atividade Confirm Visit nao foi criada');
		}
	}

	@isTest
	static void deveCriarAtividadeConfirmTestDrive() {
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppNew   = createOpp();
			oppNew.Detail__c = 'Test Drive';
			final List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, new Map<Id, Opportunity>());

			List<Event> eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmTestDrive);
			System.assert(eventList.isEmpty(), 'Não deve retorno resultado');

			activityManager.execInsert();

			eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmTestDrive);
			System.assert(eventList.size() == 1, 'Atividade Confirm Test Drive nao foi criada');
		}
	}

	@isTest
	static void deveCriarAtividadeCustomeService() {
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			Opportunity oppNew   = createOpp();
			oppNew.OpportunitySubSource__c = 'Nao e Internet';
			List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, new Map<Id, Opportunity>());

			List<Event> eventList = obterAtividadeEventCriada(user, ActivityManagerBo.custemerService);
			System.assert(eventList.isEmpty(), 'Não deve retorno resultado');

			activityManager.execInsert();

			eventList = obterAtividadeEventCriada(user, ActivityManagerBo.custemerService);
			System.assert(eventList.size() == 1, 'Atividade Custome Service nao foi criada');
		}
	}

	@isTest
	static void deveTransferirAtividadeVendedor() {
		Test.startTest();
		insertPermission(user, ProfileUtils.permissionSeller);
		insertPermission(userTest, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			Opportunity oppOld = createOpp();
			INSERT oppOld;

			List<Event> eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.size() == 1, 'Atividade nao foi criada para vendedor');

			eventList = obterAtividadeEventCriada(userTest, ActivityManagerBo.confirmVisit);
			System.assert(eventList.isEmpty(), 'Não deve tem atividade criada');

			Opportunity oppNew  = oppOld.clone(true, false, false, false);
			oppNew.OwnerId = userTest.Id;

			List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			List<Opportunity> oppOldList = new List<Opportunity>{ oppOld };
			Map<Id, Opportunity> oppOldMap = new Map<Id, Opportunity>(oppOldList);
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, oppOldMap);
			activityManager.execUpdate();

			eventList = obterAtividadeEventCriada(userTest, ActivityManagerBo.confirmVisit);
			System.assert(eventList.size() == 1, 'Atividade nao foi transferida');

			eventList = obterAtividadeEventCriada(user, ActivityManagerBo.confirmVisit);
			System.assert(eventList.isEmpty(), 'Não deve retorno resultado');
		}
		Test.stopTest();
	}

	@isTest
	static void deveCriarAtividadeNegotiation() {
		Test.startTest();
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppOld = createOpp();
			INSERT oppOld;

			Opportunity oppNew  = oppOld.clone(true, false, false, false);
			oppNew.StageName = 'Quote';

			List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			List<Opportunity> oppOldList = new List<Opportunity>{ oppOld };
			Map<Id, Opportunity> oppOldMap = new Map<Id, Opportunity>(oppOldList);
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, oppOldMap);
			activityManager.execUpdate();

			List<Task> taskList = obterAtividadeTaskCriada(user, ActivityManagerBo.negotiation);
			System.assert(taskList.size() == 1, 'Atividade Negotiation nao foi criada');
		}
		Test.stopTest();
	}

	@isTest
	static void deveCriarAtividadePreOrder() {
		Test.startTest();
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppOld = createOpp();
			INSERT oppOld;

			Opportunity oppNew  = oppOld.clone(true, false, false, false);
			oppNew.StageName = 'Order';

			List<Opportunity> oppNewList = new List<Opportunity>{ oppNew };
			List<Opportunity> oppOldList = new List<Opportunity>{ oppOld };
			Map<Id, Opportunity> oppOldMap = new Map<Id, Opportunity>(oppOldList);
			ActivityManagerBo activityManager = new ActivityManagerBo(oppNewList, oppOldMap);
			activityManager.execUpdate();

			List<Task> taskList = obterAtividadeTaskCriada(user, ActivityManagerBo.preOrder);
			System.assert(taskList.size() == 1, 'Atividade Negotiation nao foi criada');
		}
		Test.stopTest();
	}

	@isTest
	static void deveUpdateAtividadeRealizarTestSemEvent() {
		Test.startTest();
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppOld = createOpp();
			INSERT oppOld;

			final Event event = new Event();
			event.Subject = ActivityManagerBo.performTestDrive;
			event.StartDateTime	= Datetime.now();
			event.EndDateTime	= Datetime.now().addMinutes(15);
			event.WhatId = oppOld.Id;
			INSERT event;

			ActivityManagerBo.updateAtividadeRealizarTest(oppOld, Datetime.now());
		}
		Test.stopTest();
	}

	@isTest
	static void deveUpdateAtividadeRealizarTestComEvent() {
		Test.startTest();
		insertPermission(user, ProfileUtils.permissionSeller);
		System.RunAs(user) {
			final Opportunity oppOld = createOpp();
			INSERT oppOld;


			ActivityManagerBo.updateAtividadeRealizarTest(oppOld, Datetime.now());
		}
		Test.stopTest();
	}

	@isTest
	static void deveObterFaseTestDrive() {
		List<String> fases = ActivityManagerBo.obterFaseTestDrive();
		Set<String> faseSet = new Set<String>(fases);
		System.assert(faseSet.contains(ActivityManagerBo.confirmVisit));
		System.assert(faseSet.contains(ActivityManagerBo.confirmTestDrive));
		System.assert(faseSet.contains(ActivityManagerBo.custemerService));
		System.assert(faseSet.contains(ActivityManagerBo.performVisit));
	}

	@isTest
	static void deveObterFasePreOrder() {
		List<String> fases = ActivityManagerBo.obterFasePreOrder();
		Set<String> faseSet = new Set<String>(fases);
		System.assert(faseSet.contains(ActivityManagerBo.performTestDrive));
	}

	static List<Event> obterAtividadeEventCriada(User userOwner, String sujectActv) {
		return [
			SELECT Id FROM Event WHERE OwnerId =: userOwner.Id
				AND Subject =: sujectActv
		];
	}

	static List<Task> obterAtividadeTaskCriada(User userOwner, String sujectActv) {
		return [
			SELECT Id FROM Task WHERE OwnerId =: userOwner.Id
				AND Subject =: sujectActv
		];
	}

	static Opportunity createOpp() {
		Opportunity oldOpp   = MyOwnCreation.getInstance().criaOpportunity();
		oldOpp.AccountId     = accDealer.Id;
		oldOpp.Dealer__c     = accDealer.Id;
		oldOpp.OwnerId		 = user.Id;
		oldOpp.StageName	 = 'In Attendance';
		oldOpp.OpportunitySource__c = 'Dealer';
		oldOpp.OpportunitySubSource__c = 'Internet';
		oldOpp.Expiration_Time__c = Datetime.now();
		return oldOpp;
	}

	static void insertPermission(User u, String namePermission) {
			PermissionSet ps = [ SELECT ID From PermissionSet WHERE Name =: namePermission ];
			PermissionSetAssignment psA =
					new PermissionSetAssignment(AssigneeId = u.id, PermissionSetId = ps.Id );
			INSERT psA;
	}

	/*  static testMethod void test1() {

    	Test.startTest();

    	Account acc = [SELECT Id FROM Account WHERE ShippingStreet = 'Rua X' LIMIT 1];
    	Opportunity opp = [SELECT Id, AccountId, Dealer__c FROM Opportunity WHERE AccountId =: acc.Id LIMIT 1];

			List<Opportunity> lstOpp = new List<Opportunity>();
			lstOpp.add(opp);

			map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
			mapOpp.put(opp.Id, opp);

			ActivityManagerBo cls = new ActivityManagerBo(lstOpp, mapOpp);

			Test.stopTest();
    }

    static testMethod void test2(){

    	Test.startTest();

    	Account acc = [SELECT Id FROM Account WHERE ShippingStreet = 'Rua X' LIMIT 1];

    	Opportunity opp = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		LIMIT 1
		];
    	Opportunity oppOld = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		AND OwnerId != :UserInfo.getUserId()
    		LIMIT 1
		];

		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(opp);

		map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		mapOpp.put(opp.Id, oppOld);

		ActivityManagerBo cls = new ActivityManagerBo(lstOpp, mapOpp);

		List<String> fases = new List<String>();
		fases.add('ANY element');

		cls.execUpdate();
		ActivityManagerBo.gerarPreOrder(opp);
		ActivityManagerBo.updateAtividadeRealizarTest(opp,System.today());
		ActivityManagerBo.fecharAtividade(fases,opp);
		ActivityManagerBo.obterFasePreOrder();
		ActivityManagerBo.obterFaseTestDrive();

		Test.stopTest();

    }

    static testMethod void test3(){

    	Test.startTest();

    	Account acc = [SELECT Id FROM Account WHERE ShippingStreet = 'Rua X' LIMIT 1];

    	Opportunity opp = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		LIMIT 1
		];
		opp.StageName = 'Quote';
		Update opp;

    	Opportunity oppOld = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		AND OwnerId != :UserInfo.getUserId()
    		LIMIT 1
		];

		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(opp);

		map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		mapOpp.put(opp.Id, oppOld);

		ActivityManagerBo cls = new ActivityManagerBo(lstOpp, mapOpp);

		List<String> fases = new List<String>();
		fases.add('ANY element');

		cls.execUpdate();

		Test.stopTest();

    }

    static testMethod void test4(){

    	Test.startTest();

    	Account acc = [SELECT Id FROM Account WHERE ShippingStreet = 'Rua X' LIMIT 1];

    	Opportunity opp = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		LIMIT 1
		];
		opp.StageName = 'Order';
		Update opp;

    	Opportunity oppOld = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		AND OwnerId != :UserInfo.getUserId()
    		LIMIT 1
		];

		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(opp);

		map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		mapOpp.put(opp.Id, oppOld);

		ActivityManagerBo cls = new ActivityManagerBo(lstOpp, mapOpp);

		List<String> fases = new List<String>();
		fases.add('ANY element');

		cls.execUpdate();

		Test.stopTest();

    }

    static testMethod void test5(){

    	Test.startTest();

    	Account acc = [SELECT Id FROM Account WHERE ShippingStreet = 'Rua X' LIMIT 1];

    	Opportunity opp = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		LIMIT 1
		];
		opp.StageName = 'Billed';
		Update opp;

    	Opportunity oppOld = [
    		SELECT Id, AccountId, Dealer__c, OwnerId, StageName
    		FROM Opportunity
    		WHERE AccountId =: acc.Id
    		AND OwnerId != :UserInfo.getUserId()
    		LIMIT 1
		];

		List<Opportunity> lstOpp = new List<Opportunity>();
		lstOpp.add(opp);

		map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		mapOpp.put(opp.Id, oppOld);

		ActivityManagerBo cls = new ActivityManagerBo(lstOpp, mapOpp);

		List<String> fases = new List<String>();
		fases.add('ANY element');

		cls.execUpdate();

		Test.stopTest();

	}*/

}