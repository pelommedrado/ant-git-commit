/**
* Classe responsável por controlar a pagina NewVehicle.
* @author Elvia Serpa.
*/
public with sharing class VFC72_NewVehicleController 
{
    private String quoteId; 
    private String idProduto;
    private string idVehico;
    private Decimal priceVehicle;   
    public VFC80_VehicleVO vehicleVO  {get;set;}    

    /** 
    *  
    */      
    public VFC72_NewVehicleController (ApexPages.StandardController controller)
    {
        this.quoteId = controller.getId();
    }

    /**
    * 
    */
    public PageReference initializeVehicle()
    {
        this.vehicleVO = VFC87_NewVehicleBusinessDelegate.getInstance().getAccountModel(quoteId);
        
        this.buildPicklists();
         
        return null;
    }
    
    private void buildPicklists()
    {
        String modelSelected = '';
        this.vehicleVO.lstProd = new List<SelectOption>();
        this.vehicleVO.lstVers = new List<SelectOption>();
        this.vehicleVO.lstYear = new List<SelectOption>();
        this.vehicleVO.lstOptionColor = new List<SelectOption>();
            
        List<Product2> lstProduct = VFC87_NewVehicleBusinessDelegate.getInstance().selectModel();
        
        for(Product2 sObjProd : lstProduct)
        {
            if(vehicleVO.objsQuote.Opportunity.Account.VehicleInterest_BR__c == sObjProd.Name)
            {
                modelSelected = sObjProd.Id;
                
                break;
            }
        }
        
        /*define o modelo que ficará selecionado (pode ser a opção Selecionar ou algum modelo da lista)*/
        this.vehicleVO.itemModel = modelSelected; 
        
        if(String.isEmpty(this.vehicleVO.itemModel))
        {
            this.vehicleVO.lstProd.add(new SelectOption('', 'Selecionar'));
            this.vehicleVO.lstVers.add(new SelectOption('', 'Selecionar')); 
            this.vehicleVO.lstOptionColor.add(new SelectOption('', 'Selecionar'));          
        }
        else
        {
            this.getVersions();
            this.getColors();
            this.getVehicles();
        }
        
        /*monta o picklist de modelos*/
        for(Product2 sObjProd : lstProduct)
        {
            vehicleVO.lstProd.add(new SelectOption(sObjProd.Id, sObjProd.Name));
        }
        
        this.buildPicklistYears();            
    }
    
    private void getVersions()
    {
        Map<String, Product2> mapVersion = new Map<String, Product2>();
        
       List<Product2> lstVersion = VFC87_NewVehicleBusinessDelegate.getInstance().selectVersion(this.vehicleVO.itemModel);                          
       //Alteração 03/05/2013
       
       // List<Product2> lstVersion = VFC87_NewVehicleBusinessDelegate.getInstance().selectVersion(this.vehicleVO.itemVersion);                        
                                
                                
                                
        for(Product2 auxLstVersion : lstVersion) 
        {
            if(auxLstVersion.Version__c != null)
            {
                mapVersion.put(auxLstVersion.Version__c, auxLstVersion); 
            }                  
        }
        
        this.vehicleVO.lstVers.clear();
        this.vehicleVO.lstVers.add(new SelectOption('', 'Selecionar')); 
             
        for(Product2 versionAux : mapVersion.values())
        {
            this.vehicleVO.lstVers.add(new SelectOption(versionAux.Id, versionAux.Version__c));
            this.idProduto = versionAux.Id;
        }   
    }
    
    private void getColors()
    {
        Map<String, Vehicle_Item__c> mapColor = new Map<String, Vehicle_Item__c>();
        
        List<Vehicle_Item__c> lstColor = VFC87_NewVehicleBusinessDelegate.getInstance().selectColor(this.vehicleVO.itemModel);   
        
        for(Vehicle_Item__c auxLstColor : lstColor) 
        {
            if(auxLstColor.Item__r.Label__c != null)
            {
                mapColor.put(auxLstColor.Item__r.Label__c, auxLstColor);       
            }           
        }
        
        this.vehicleVO.lstOptionColor.clear();
        this.vehicleVO.lstOptionColor.add(new SelectOption('', 'Selecionar')); 
              
        for(Vehicle_Item__c colorAux : mapColor.values())
        {
            this.vehicleVO.lstOptionColor.add(new SelectOption(colorAux.Item__r.Label__c, colorAux.Item__r.Label__c));
        }                          
    }
    
    public void getVehicles()
    {
        this.vehicleVO.lstVehico = VFC87_NewVehicleBusinessDelegate.getInstance().vehiclesAvailableGrid(this.quoteId, this.vehicleVO);  
    }  
    
    /**
    * 
    */  
    private void buildPicklistYears()
    {
        vehicleVO.lstYear.add(new SelectOption('', 'Selecionar'));
        
        vehicleVO.lstYear.add(new SelectOption(String.valueOf(Date.today().year() - 1),String.valueOf(Date.today().year() - 1)));
        vehicleVO.lstYear.add(new SelectOption(String.valueOf(Date.today().year()),String.valueOf(Date.today().year())));
        vehicleVO.lstYear.add(new SelectOption(String.valueOf(Date.today().year() + 1),String.valueOf(Date.today().year() + 1)));
    }
    
    /**
    * Trata a ação do evento onchange do picklist de modelos.
    */  
    public void processModelChange()
    {
        if(String.isEmpty(this.vehicleVO.itemModel))
        {
            this.vehicleVO.lstVers.clear();
            this.vehicleVO.lstVers.add(new SelectOption('', 'Selecionar')); 
            this.vehicleVO.lstOptionColor.clear();
            this.vehicleVO.lstOptionColor.add(new SelectOption('', 'Selecionar'));  
            this.vehicleVO.lstVehico.clear();  
        }
        else
        {
            this.getVersions();
            this.getColors();
            this.getVehicles();
        }                                                   
    }   
   
    /**
    * 
    */  
    public Pagereference includeItemVehicle()
    {
        String strMessage = null;
        ApexPages.Message message = null;       
        boolean controls = false;
        
        if(vehicleVO.lstVehico == null)
        {
            strMessage = 'Selecione um veículo.';    
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);  
            return null;        
        }           
                
        for(VFC94_VehicleItemVO  auxVehicle : vehicleVO.lstVehico)
        {
            if(auxVehicle.isCheckVehico == true)
            {
                idVehico = auxVehicle.idVehico;
                priceVehicle = auxVehicle.priceVehicle;
                controls = true;
            }
        }
        
        if(controls == false)
        {
            strMessage = 'Selecione um veículo.';    
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);  
            return null;        
        }

        try
        {
        	if(!String.isEmpty(vehicleVO.itemVersion)){
        		idProduto = vehicleVO.itemVersion;
        		System.debug('########################################################### VERSAO idPrduto ' + vehicleVO.itemVersion);
        		System.debug('########################################################### VERSAO idPrduto ' + idProduto);
        		
        	}else if(!String.isEmpty(vehicleVO.itemModel)){
          		idProduto = vehicleVO.itemModel;
          		System.debug('########################################################### MODEL idPrduto ' + vehicleVO.itemModel);
          		System.debug('########################################################### MODEL idPrduto ' + idProduto);
          	}
        	
            System.debug('>>>>> idVehico'+idVehico);
            VFC87_NewVehicleBusinessDelegate.getInstance().includeItemVehicle(idProduto, idVehico, this.quoteId, priceVehicle); 
        }
        catch(Exception ex)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(message);
            return null;
        }   
            
        PageReference pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+this.quoteId);
        pageRef.setRedirect(true);
        return pageRef;             
    }
    
    /**
    * 
    */  
    public Pagereference  includeItemGeneric()
    {
        String strMessage = null;
        ApexPages.Message message = null;       
        
        if(String.isEmpty(vehicleVO.itemVersion))
        {
            strMessage = 'Selecione uma versão.';    
            message = new ApexPages.Message(ApexPages.Severity.ERROR, strMessage);
            ApexPages.addMessage(message);  
            return null;        
        }
        
        try
        {
        	
        	if(!String.isEmpty(vehicleVO.itemVersion)){
        		idProduto = vehicleVO.itemVersion;
        		System.debug('########################################################### VERSAO idPrduto ' + vehicleVO.itemVersion);
        		System.debug('########################################################### VERSAO idPrduto ' + idProduto);
        		
        	}else if(!String.isEmpty(vehicleVO.itemModel)){
          		idProduto = vehicleVO.itemModel;
          		System.debug('########################################################### MODEL idPrduto ' + vehicleVO.itemModel);
          		System.debug('########################################################### MODEL idPrduto ' + idProduto);
          	}
        	
            VFC87_NewVehicleBusinessDelegate.getInstance().includeItemGeneric(idProduto, this.quoteId); 
        }
        catch(Exception ex)
        {
            message = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(message);
            return null;
        }
         
        PageReference pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+this.quoteId);
        pageRef.setRedirect(true);
        return pageRef;     
    }       
    
    /**
    * 
    */  
    public Pagereference  cancelVehico()
    {
       PageReference pageRef = new PageReference('/apex/VFP12_QuoteDetails?Id='+this.quoteId);
       pageRef.setRedirect(true);
       return pageRef;      
    }                   
}