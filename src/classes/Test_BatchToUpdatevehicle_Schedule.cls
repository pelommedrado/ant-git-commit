/*****************************************************************************************
    Name    : Test_BatchToUpdatevehicleSchedule
    Desc    : This is a Test class for BatchToUpdatevehicleschedule class
    Approach: 
                
                                                
 Modification Log : 
-----------------------------------------------------------------------------------------
Developer                        Date               Description
----------------------------------------------------------------------------------------
Praveen Ravula                  4/26/2013           Created 

******************************************************************************************/


@isTest
private class Test_BatchToUpdatevehicle_Schedule  {

    static testMethod void myUnitTest() {
        String cron = '0 0 0 * * ? 2020';
    
        Test.startTest();
            String jobId = System.schedule('schedulebatchtest', cron, new BatchToUpdatevehicle_Schedule());
        Test.stopTest();
    }
}