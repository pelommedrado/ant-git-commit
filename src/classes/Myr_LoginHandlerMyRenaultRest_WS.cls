/** Apex REST WebService used to provide a login handler for Helios
    This class is called just after the login to initiate some records
    
    This class has been set because it's impossible to instantiate some things when the
    user connects through the Connected Apps. We try something into authorize function of the plugin but if fails
    due to a bug into the Winter 16 release: 
    
    https://help.salesforce.com/HTViewCase?id=5003000000bpnPAAAY&filterid=1
    
    @author S. Ducamp
    @version 1.0
    @date 28.10.2015
    
    1.0: initial revision, just logs
**/
    
@RestResource(urlMapping='/myrenault/login') 
global without sharing class Myr_LoginHandlerMyRenaultRest_WS { 
    
    //Call sample: https://<instance>.salesforce.com/services/apexrest/myrenault/login
    
    @HttpPost
    global static String loginHandler() {
        Myr_LoginHandlerRest_Cls.loginHandler(Myr_MyRenaultTools.Brand.Renault);
        return 'OK';
    } 

}