global class VFC13_UpdateVehicle{

 public String urlForLink {get;set;}
 public String vehiclePrefix {get;set;}
 public static String countryCode {get;set;}
 
  
VFC13_UpdateVehicle() {
    countryCode ='';
    urlForLink = URL.getSalesforceBaseUrl().toExternalForm();        
    Schema.DescribeSObjectResult descrVehicle = Schema.SObjectType.VEH_Veh__c;
    vehiclePrefix = descrVehicle.getKeyPrefix();
}

webservice static Boolean getVehicleDetails(String VINNO,String VID){
    
    PageReference pageRef;
    WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse rep;
    Boolean status=false;

    if (VINNO != null){
        rep = searchBVM(VINNO);
     }
       if (rep != null && rep.detVeh != null) {
        pageRef = updateVehicleData(rep,VINNO,VID);   
        status=true;    
     }
      else {
     // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Not found');
     // ApexPages.addMessage(myMsg);
        status=false;
    
      }   
   return status;
}

 public static WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse searchBVM(String VINNO)
    {
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest request = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlRequest();               
        WS01_ApvGetDetVehXml.ServicePreferences servicePref = new WS01_ApvGetDetVehXml.ServicePreferences();
        WS01_ApvGetDetVehXml.ApvGetDetVehXml VehWS = new WS01_ApvGetDetVehXml.ApvGetDetVehXml();
        WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse VEH_WS = new WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse();
       
        try
        {  
            servicePref.bvmsi25SocEmettrice  = 'CCB';
            servicePref.bvmsi25Vin = VINNO;
            servicePref.bvmsi25CodePaysLang = countryCode; //FRA 
            servicePref.bvmsi25CodeLang = UserInfo.getLanguage().substring(0, 2).toUpperCase(); //FR
            request.ServicePreferences = servicePref;
              
            VehWS.endpoint_x = System.label.VFP04_VehiculeDataURL; 
            VehWS.clientCertName_x = System.label.clientCertNamex;           
            VehWS.timeout_x=40000;
            System.debug('req***********'+request); 
            System.debug('servicePref ***********'+servicePref); 

            return (WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse)VehWS.getApvGetDetVehXml(request);
        }            
        catch (Exception e)
        {                    
            system.debug(VEH_WS);
            system.debug('erreur*********** : ' + String.valueOf(e));
            return null;
        }
    }
    public static PageReference updateVehicleData(WS01_ApvGetDetVehXml.ApvGetDetVehXmlResponse vehBVM,String VIN,String VID)
    {
        VEH_Veh__c veh = new VEH_Veh__c();
        veh.Name = VIN;
        veh.Id=VID;       
                
        //VersionCode__c p460:version
        if (vehBVM.detVeh.version != null){
            veh.VersionCode__c = vehBVM.detVeh.version;
        }
        
        //AfterSalesType__c p460:tvv
        if (vehBVM.detVeh.tvv != null) {
            veh.AfterSalesType__c = vehBVM.detVeh.tvv;
        }
         
         //EngineType__c p460:typeMot 
        if (vehBVM.detVeh.typeMot != null){
            veh.EngineType__c = vehBVM.detVeh.typeMot;         
        }
        
        //EngineIndex__c p460:indMot
        if (vehBVM.detVeh.indMot != null){
            veh.EngineIndex__c = vehBVM.detVeh.indMot; 
        }
        
         //EngineManuNbr__c p460:NMot
        if (vehBVM.detVeh.NMot != null){
            veh.EngineManuNbr__c = vehBVM.detVeh.NMot;
        }
           
        //GearBoxType__c p460:typeBoi
        if (vehBVM.detVeh.typeBoi != null){
            veh.GearBoxType__c = vehBVM.detVeh.typeBoi;
        } 
 
       //GearBoxIndex__c p460:indBoi 
        if (vehBVM.detVeh.indBoi != null){
            veh.GearBoxIndex__c = vehBVM.detVeh.indBoi; 
        }  
        
      //GearBoxManuNbr__c p460:NBoi
        if (vehBVM.detVeh.NBoi != null){
            veh.GearBoxManuNbr__c = vehBVM.detVeh.NBoi;   
        } 
         
        //DateofManu__c p460:dateTcmFab 
        if (vehBVM.detVeh.dateTcmFab != null && vehBVM.detVeh.dateTcmFab.length() > 7) {
            veh.DateofManu__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateTcmFab.substring(0, 2)));
        }  
      
         // updating the EnergyType from BVM service when the value is null
    
        if (veh.EnergyType__c == null) {
            if (vehBVM.detVeh.criteres != null) {
            for (WS01_ApvGetDetVehXml.DetVehCritere crit : vehBVM.detVeh.criteres)
            {
                //EnergyType__c Objet 019
                if (crit.cdObjOf == '019')
                    veh.EnergyType__c = crit.critereOf;
            }
          }
        }
      
          
        if ( veh.DeliveryDate__c == null) {
            if (vehBVM.detVeh.dateLiv != null && vehBVM.detVeh.dateLiv.length() > 7)
                veh.DeliveryDate__c = date.newinstance(Integer.valueOf(vehBVM.detVeh.dateLiv.substring(6, 10)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(3, 5)), Integer.valueOf(vehBVM.detVeh.dateLiv.substring(0, 2)));
        }
              
        if (veh.Model__c == null) {
             if (vehBVM.detVeh.libModel != null)
             veh.Model__c = vehBVM.detVeh.libModel;
        }
        
        
        if (veh.BodyType__c == null) {
            if (vehBVM.detVeh.libCarrosserie != null){
                veh.BodyType__c = vehBVM.detVeh.libCarrosserie;
           }
       }     
       
       
  
       
    // Update the new Criteria Field   
       
       
     WS01_ApvGetDetVehXml.DetVehCritere[] criteres=vehBVM.detVeh.criteres;

      String ModelRangeValue;
      String ModelCode;
      for(integer i=0;i<criteres.size();i++)
      {

      System.debug('Criteria Label Value>>>>>>>>>>>'+Criteres[i].libCritOf);
      
      System.debug('Criteria Value>>>>>>>>>>>'+Criteres[i].critereOf);
      System.debug('Criteria Value cdCritOf>>>>>>>>>>>'+Criteres[i].cdCritOf);
      System.debug('Criteria Value cdObjOf>>>>>>>>>>>'+Criteres[i].cdObjOf);
     
        if(criteres[i].cdObjOf =='008')
          {
         ModelCode=criteres[i].critereOf;
         
         System.debug('ModelCode>>>>>>>>>>>'+ModelCode);
     }
     
     
       if(criteres[i].libCritOf=='MODELE FICHE GAMME')
          {
         ModelRangeValue=criteres[i].critereOf;
         
         System.debug('ModelRangeValue>>>>>>>>>>>'+ModelRangeValue);
     }
     }     
           if (veh.Model_Range__c == null) {
           if (ModelRangeValue != null){
             veh.Model_Range__c = ModelRangeValue;
                          
           }
       }     
       
       
       
       if (veh.ModelCode__c == null) {
           if (ModelCode != null){
             veh.ModelCode__c = ModelCode;
                          
           }
       }     
       
       veh.BVM_data_Upto_date__c =true;
    
        System.debug('res*********'+vehBVM);
            
                 
        try
        {
            update veh;
            System.debug('Salesorce Identification used in Plugin >>>>>'+Veh);
        }
        catch(DMLException dmle)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.UnableToSaveVehicle);
            ApexPages.addMessage(myMsg);
            return null;
        }
                return new PageReference('/' + veh.Id);
    }

    
  }