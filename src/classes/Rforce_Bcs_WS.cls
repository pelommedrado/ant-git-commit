public class Rforce_Bcs_WS {

public String getAccountInfo(String vin,String Registration,String country,String LastName,String firstName,String brand,String email, String City, String ZipCode, String idClient){
      String strBody;
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(System.label.BcsWsUrl);
      req.setClientCertificateName(System.label.clientcertnamex);
      req.setTimeout(120000); 
      req.setHeader('Content-Type', 'application/soap+xml; charset=utf-8');
      req.setHeader('SOAPAction', '"document/http://siebel.com/CustomUI:getCustData"');
      req.setHeader('Keep-Alive', '1115');
      req.setHeader('Accept', 'text/xml');
      req.setHeader('User-Agent', 'SFDC-Callout/26.0');
      
    String requestStr = ''
    +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://siebel.com/CustomUI" xmlns:cus1="http://custdata.crm.bservice.renault">'
    +'<soapenv:Header/>'
    +'<soapenv:Body>'
    +'<cus:BCS_spcCrmGetCustDataService_getCustData_Input>'
    +'<cus1:getCustData>'
    +'<cus1:request>'
    +'<cus1:servicePrefs>'
    +'</cus1:servicePrefs>'
    +'<cus1:custDataRequest>'
    +'<cus1:mode></cus1:mode>'
    +'<cus1:country>'+country+'</cus1:country>'
    +'<cus1:brand>'+brand+'</cus1:brand>'
    +'<cus1:demander>CCBPROD</cus1:demander>'
    +'<cus1:vin>'+vin+'</cus1:vin>'
    +'<cus1:lastName>'+LastName+'</cus1:lastName>'
    +'<cus1:firstName>'+firstName+'</cus1:firstName>'
    +'<cus1:city>'+City+'</cus1:city>'
    +'<cus1:zip>'+ZipCode+'</cus1:zip>'
    +'<cus1:ident1></cus1:ident1>'
    +'<cus1:idClient>'+idClient+'</cus1:idClient>'
    +'<cus1:idMyr></cus1:idMyr>'
    +'<cus1:firstRegistrationDate></cus1:firstRegistrationDate>'
    +'<cus1:registration>'+Registration+'</cus1:registration>'
    +'<cus1:email>'+email+'</cus1:email>'
    +'<cus1:ownedVehicles></cus1:ownedVehicles>'
    +'<cus1:nbReplies>10</cus1:nbReplies>'
  +'</cus1:custDataRequest>'
    +'</cus1:request>'
    +'</cus1:getCustData>'
    +'</cus:BCS_spcCrmGetCustDataService_getCustData_Input>'
    +'</soapenv:Body>'
    +'</soapenv:Envelope>';
        req.setHeader('Content-Length', String.valueOf(requestStr.length()));
        req.setBody(requestStr);
        Http http = new Http();
        try {
            //Execute web service call here
            System.debug('REQUEST='+req.toString());
            System.debug('REQUEST='+req.getBody());      
            HTTPResponse res = http.send(req);  
            //Helpful debug messages
            System.debug('STATUS='+res.getStatus());
            System.debug('STATUS_CODE='+res.getStatusCode());
           
            strBody = res.getBody();
            System.debug('Response String>>>>>>>>>'+strBody );
            System.debug('STATUS CODE'+res.getStatusCode());

       } catch(CalloutException e) {
            system.debug ('BCS WS, CALLOUT EXCEPTION'+e.getMessage());

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Problem with the Webservice'));
           
       } catch (Exception e) {
            system.debug ('BCS WS, UNKNOWN EXCEPTION'+e.getMessage());

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Problem with the Webservice'));
            
       }
            String finalStr='';
            Integer pos1,pos2;
           If(!String.isBlank(strBody))
           {
  
            pos1=strBody.indexof('<response>');
            pos2=strBody.indexof('</response>');
           }
    else{
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Response is Empty'));
          
    }
           
   
        if(pos1>0&&pos2>0)
       {
          String formattedStr=strBody.substring(pos1,pos2);
           finalStr=formattedStr+'</response>';
       }  
    else
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Response is Empty'));
     
    }
       return finalStr;
}
}