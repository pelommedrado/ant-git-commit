/**
* Classe que representa exceções geradas na criação de orçamento.
* @author Christian Ranha.
*/
public with sharing class VFC82_CreateQuoteException extends Exception {

	public VFC82_CreateQuoteException(Exception ex, String message)
	{
		this.initCause(ex);
		this.setMessage(message);	
	}
}