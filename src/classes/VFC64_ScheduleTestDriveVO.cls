/**
* Classe que representa a agenda do Test Drive.
* @author Felipe Jesus Silva 
*/
public class VFC64_ScheduleTestDriveVO 
{
	public String hour {get;set;}
	public String schedulingStatus {get;set;}
	
	public VFC64_ScheduleTestDriveVO()
	{
		
	}
}