/*TestClass dedicated to purge germany2 records
  @author Donatien Veron
  @version 1.0
  @date 01.04.2016

  @version R16.10 16.08.2016: SDP, F1652_US3455, message templates are private now, so they are not available for all the users.
*/
@isTest
private class Myr_Batch_Germany2_BAT_TEST {

	@testSetup static void setCustomSettings() {
		Myr_Datasets_Test.prepareRequiredCustomSettings();
		Myr_Datasets_Test.prepareCustomerMessageSettings();

		//Prepare technical users
		List<Myr_Datasets_Test.RequestedTechUser> listReqTechUser = new List<Myr_Datasets_Test.RequestedTechUser> ();
		//generic user to avoid the copy of the user.recorddefaultcountry on Account.country__c when working on those accounts.
		listReqTechUser.add(new Myr_Datasets_Test.RequestedTechUser('France', '', true));
		listReqTechUser.add(new Myr_Datasets_Test.RequestedTechUser('Germany2', '', true));
		Myr_Datasets_Test.insertTechnicalUsers(listReqTechUser);

		List<Account> listAccounts = new List<Account>();
		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('France')) {
			listAccounts = Myr_Datasets_Test.insertPersonalAccounts(2, Myr_Datasets_Test.UserOptions.ACTIVE_USERS);
		}
		//List<Account> la = [select Id from Account];
		Account a1 = listAccounts[0];
		Account a2 = listAccounts[1];

		system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
			a1.MYR_Status__c = 'Deleted';
			a1.MYD_Status__c = 'Deleted';
			a1.Country__c = 'Germany2'; //this line is useless on HQ but required on other orgs because the trigger
			//that copies the country doesn ot exist on other orgs
			update a1;
		}

		List<Customer_Message_Templates__c> C_T = [select id from Customer_Message_Templates__c];

		system.runAs(Myr_Datasets_Test.getCustMsgUser('TestUnitAtos','BatchGermanye', false)) {
			a2.Country__c = 'France';
			update a2; //this line is useless on HQ but required on other orgs because the trigger
			//that copies the country doesn ot exist on other orgs

			Customer_Message__c c1 = new Customer_Message__c();
			c1.Account__c = a1.Id;
			c1.Body__c = 'Body';
			c1.Channel__c = 'Channel';
			c1.Template__c = C_T[0].Id;
			c1.Title__c = 'Title';
			insert c1;
			Customer_Message__c c2 = new Customer_Message__c();
			c2.Account__c = a2.Id;
			c2.Body__c = 'Body';
			c2.Channel__c = 'Channel';
			c2.Template__c = C_T[0].Id;
			c2.Title__c = 'Title';
			insert c2;

			Synchro_ATC__c s1 = new Synchro_ATC__c();
			s1.Account__c = a1.Id;
			insert s1;
			Synchro_ATC__c s2 = new Synchro_ATC__c();
			s2.Account__c = a2.Id;
			insert s2;

			VEH_Veh__c v1 = new VEH_Veh__c();
			v1.Name = 'VF1KT1J0639194878';
			insert v1;
			List<VEH_Veh__c> V_1 = [select id from VEH_Veh__c];

			VRE_VehRel__c vr1 = new VRE_VehRel__c();
			vr1.Account__c = a1.Id;
			vr1.Vin__c = V_1[0].Id;
			insert vr1;

			VRE_VehRel__c vr2 = new VRE_VehRel__c();
			vr2.Account__c = a2.Id;
			vr2.Vin__c = V_1[0].Id;
			insert vr2;

			List<Logger__c> Logg = new List<Logger__c> ();
			Logger__c l1 = new Logger__c();
			l1.Account__c = a1.Id;
			l1.Apex_Class__c = 'Test_Purge';
			l1.Function__c = 'Function1';
			l1.Project_Name__c = 'Project_Name';
			Logg.add(l1);

			Logger__c l11 = new Logger__c();
			l11.Account__c = a2.Id;
			l11.Apex_Class__c = 'Test_Purge';
			l11.Function__c = 'Function1';
			l11.Project_Name__c = 'Project_Name';
			Logg.add(l11);

			Logger__c l2 = new Logger__c();
			l2.Vehicle_Relationship__c = vr1.Id;
			l2.Apex_Class__c = 'Test_Purge';
			l2.Function__c = 'Function2';
			l2.Project_Name__c = 'Project_Name';
			Logg.add(l2);

			Logger__c l22 = new Logger__c();
			l22.Vehicle_Relationship__c = vr2.Id;
			l22.Apex_Class__c = 'Test_Purge';
			l22.Function__c = 'Function2';
			l22.Project_Name__c = 'Project_Name';
			Logg.add(l22);
			insert Logg;
		}
	}
	
	static testMethod void test_020_Both_Brand_Deleted_Since_Many_Time() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		a.MYR_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYD_Status_UpdateDate__c = Date.parse('01/01/2011');
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
		
				List<Myr_Batch_Germany2_ToolBox.TB_Answer> Account_Ids = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('');
				System.assertNotEquals(0, Account_Ids.size());

				Database.executeBatch(new Myr_Batch_Germany2_CustomerMessage_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_SynchroAtc_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Logger_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Relation_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_User_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Account_Reset_BAT('Renault'));
				Database.executeBatch(new Myr_Batch_Germany2_Account_Delete_BAT());
			}
		Test.stopTest();
		List<Customer_Message__c> L_C = [select Id, account__c from Customer_Message__c];
		System.assertEquals(1, L_C.size());
		List<Synchro_ATC__c> L_S = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(1, L_S.size());
		List<Logger__c> L_L = [select Id, account__c from Logger__c where Apex_Class__c = 'Test_Purge'];
		System.assertEquals(2, L_L.size());
		List<VRE_VehRel__c> L_R = [select Id, account__c from VRE_VehRel__c];
		System.assertEquals(1, L_R.size());

	}

	static testMethod void test_030_Only_Renault_Deleted_Since_Many_Time() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		a.MYR_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYD_Status__c = null;
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				List<Myr_Batch_Germany2_ToolBox.TB_Answer> Account_Ids = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('');
				Database.executeBatch(new Myr_Batch_Germany2_CustomerMessage_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_SynchroAtc_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Logger_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Relation_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_User_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Account_Reset_BAT('Renault'));
				Database.executeBatch(new Myr_Batch_Germany2_Account_Delete_BAT());
			}
		Test.stopTest();
		List<Customer_Message__c> L_C = [select Id, account__c from Customer_Message__c];
		System.assertEquals(1, L_C.size());
		List<Synchro_ATC__c> L_S = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(1, L_S.size());
		List<Logger__c> L_L = [select Id, account__c from Logger__c where Apex_Class__c = 'Test_Purge'];
		System.assertEquals(2, L_L.size());
		List<VRE_VehRel__c> L_R = [select Id, account__c from VRE_VehRel__c];
		System.assertEquals(1, L_R.size());
	}

	static testMethod void test_040_Only_Dacia_Deleted_Since_Many_Time() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		a.MYD_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYR_Status__c = null;
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				List<Myr_Batch_Germany2_ToolBox.TB_Answer> Account_Ids = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('');
			
				Database.executeBatch(new Myr_Batch_Germany2_CustomerMessage_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_SynchroAtc_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Logger_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Relation_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_User_BAT());
				Database.executeBatch(new Myr_Batch_Germany2_Account_Reset_BAT('Renault'));
				Database.executeBatch(new Myr_Batch_Germany2_Account_Delete_BAT());
			
			}
		Test.stopTest();

		List<Customer_Message__c> L_C = [select Id, account__c from Customer_Message__c];
		System.assertEquals(1, L_C.size());
		List<Synchro_ATC__c> L_S = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(1, L_S.size());
		List<Logger__c> L_L = [select Id, account__c from Logger__c where Apex_Class__c = 'Test_Purge'];
		System.assertEquals(2, L_L.size());
		List<VRE_VehRel__c> L_R = [select Id, account__c from VRE_VehRel__c];
		System.assertEquals(1, L_R.size());
	}


	static testMethod void test_040_Toolbox_Get_German_Account_Ids_Renault() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		List<Myr_Batch_Germany2_ToolBox.TB_Answer> M_L;
		a.MYR_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYD_Status__c = 'Activated';
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				M_L = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('RENAULT');
			}
		Test.stopTest();
		System.assertEquals(1, M_L.size());
	}

	static testMethod void test_050_Toolbox_Get_German_Account_Ids_Dacia() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		List<Myr_Batch_Germany2_ToolBox.TB_Answer> M_L;
		a.MYD_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYR_Status__c = 'Activated';
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				M_L = Myr_Batch_Germany2_ToolBox.Get_German_Account_Ids('DACIA');
			}
		Test.stopTest();
		System.assertEquals(1, M_L.size());
	}


	static testMethod void test_050_Toolbox_Set_Empty_ALL() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		Account M_A;
		a.MYD_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYR_Status__c = 'Activated';
		Test.startTest();
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				M_A = Myr_Batch_Germany2_ToolBox.Set_Empty(a, 'ALL');
			}
		Test.stopTest();
		System.assertEquals(null, M_A.MYR_Status__c);
	}
	
	
	static testMethod void test_060_Only_Renault_Reset() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		a.MYR_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYD_Status__c = 'Activated';
		Test.startTest(); 
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				Database.executeBatch(new Myr_Batch_Germany2_Account_Reset_BAT('RENAULT'));
			}
		Test.stopTest();

		List<Customer_Message__c> L_C = [select Id, account__c from Customer_Message__c];
		System.assertEquals(2, L_C.size());
		List<Synchro_ATC__c> L_S = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(2, L_S.size());
		List<Logger__c> L_L = [select Id, account__c from Logger__c where Apex_Class__c = 'Test_Purge'];
		System.assertEquals(4, L_L.size());
		List<VRE_VehRel__c> L_R = [select Id, account__c from VRE_VehRel__c];
		System.assertEquals(2, L_R.size()); 
	}

	static testMethod void test_070_Only_Dacia_Reset() {
		List<Account> L_A = [select Id from Account where MYR_Status__c = 'Deleted'];
		System.assertEquals(1, L_A.size());
		Account a = L_A[0];
		a.MYD_Status__c='Deleted';
		a.MYD_Status_UpdateDate__c = Date.parse('01/01/2011');
		a.MYR_Status__c = 'Activated';
		
		Test.startTest(); 
			system.runAs(Myr_Datasets_Test.getTechnicalUserWithRole('Germany2')) {
				update a;
				Database.executeBatch(new Myr_Batch_Germany2_Account_Reset_BAT('DACIA'));
			} 
		Test.stopTest(); 

		List<Customer_Message__c> L_C = [select Id, account__c from Customer_Message__c];
		System.assertEquals(2, L_C.size());
		List<Synchro_ATC__c> L_S = [select Id, account__c from Synchro_ATC__c];
		System.assertEquals(2, L_S.size());
		List<Logger__c> L_L = [select Id, account__c from Logger__c where Apex_Class__c = 'Test_Purge'];
		System.assertEquals(4, L_L.size());
		List<VRE_VehRel__c> L_R = [select Id, account__c from VRE_VehRel__c];
		System.assertEquals(2, L_R.size());
	} 
}