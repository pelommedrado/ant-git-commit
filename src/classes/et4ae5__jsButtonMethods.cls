/*
Esse campo é gerado e não é o código fonte real para essa
classe global gerenciada.
Esse arquivo somente leitura mostra os construtores globais de classe,
métodos, variáveis e propriedades.
Para permitir a compilação do código, todos os métodos retornam nulo.
*/
global class jsButtonMethods {
    global jsButtonMethods() {

    }
    webService static String activateTriggeredSend(String trigSendId) {
        return null;
    }
    webService static String getPACId(String acctId) {
        return null;
    }
    webService static Boolean isETIntegrated() {
        return null;
    }
    webService static String namespace() {
        return null;
    }
    webService static String pauseTriggeredSend(String trigSendId) {
        return null;
    }
    webService static String performResub(String subKey, String objectType) {
        return null;
    }
    webService static String performUnsub(String subKey, String objectType) {
        return null;
    }
    webService static void resetConfiguration() {

    }
    webService static String saveRemember(et4ae5.jsButtonMethods.rememberParams params) {
        return null;
    }
global class rememberParams {
    @WebService
    webService String isChecked;
    @WebService
    webService String url;
    @WebService
    webService String userId;
    global rememberParams() {

    }
}
}
