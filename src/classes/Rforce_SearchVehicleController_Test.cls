/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Rforce_SearchVehicleController_Test {

static testMethod void testUpdateVehicle() 
    {     
    
        User usr = new User (LastName='Rotondo', alias='lro',Email='lrotondo@rotondo.com',BypassVR__c=true,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId=Label.PROFILE_SYSTEM_ADMIN,TimeZoneSidKey='America/Los_Angeles', UserName='lrotondo@lrotondo.com');
        System.runAs(usr)
        {
        Test.startTest();

        

      Test.setMock(WebServiceMock.class, new Rforce_BVMComplet_WSMock_Test());

      Rforce_BVMComplet_WS.ApvGetDetVehResponse responseMock2= Rforce_SearchVehicleController.searchBVMComplet('93YLSR1RH8J023030');
       
        VEH_Veh__c veh= new VEH_Veh__c();
        veh.Name='93YLSR1RH8J023030';
        veh.VehicleBrand__c='Renault';
        insert veh; 
       
         Pagereference pageRendered2=Rforce_SearchVehicleController.updateVehicleDataBVM(responseMock2,'93YLSR1RH8J023030',veh.Id);
        
        
        Rforce_SearchVehicleController.getVehicleDetailsBVM(veh.Name, veh.Id);
        Rforce_SearchVehicleController.mapNrj('12');

        Test.stopTest();
        System.debug('TESTS ENDED');

    }
    }
}