@isTest
private class TestTask_BeforeUpdate_Trigger {
    
    static testmethod void testTaskBeforeUpdate() {
        
        Country_Info__c ctr = new Country_Info__c (
            Name = 'Brazil', 
            Country_Code_2L__c = 'BR', 
            Language__c = 'Français',  
            CurrencyCode__c = 'BRL');
        
        insert ctr;
        
        User usr = new User (
            LastName = 'Mehdi', 
            RecordDefaultCountry__c = 'Brazil', 
            alias = 'lro', 
            Email = 'test@taskupdatehandler.com', 
            BypassVR__c = false, 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = Label.PROFILE_SYSTEM_ADMIN, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'test@taskupdatehandler.com');
        
        System.runAs(usr) {
            
            Test.startTest();
            
            Case cse = new Case();
            cse.Status = 'New';
            insert cse;
            
            Task tsk1 = new Task();
            tsk1.type = 'type';
            tsk1.status = 'NotStarted';
            tsk1.WhatId = cse.Id;
            tsk1.Priority = 'Normal';
            tsk1.subject = 'subject';           
            insert tsk1;
            
            Task taskCreated = [Select id from Task where type = 'type'];
            tsk1.Priority = 'High';
            update tsk1;
            
            System.assertNotEquals(taskCreated , null, 'Test 1 object was null and not inserted correctly');
            System.assertEquals(taskCreated.id, taskCreated.id, 'Field1 and Field2 not equals in test 0'); 
            
            Test.stopTest();
        }                       
    }
}