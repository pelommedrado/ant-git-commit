public class OportunidadeMarcaAtendimentoController {
    
    public Opportunity opp { get;set; }
    
    public OportunidadeMarcaAtendimentoController(ApexPages.Standardcontroller ctr) {
        this.opp = [
            SELECT Id, 
            StageName,
            Account.Name, 
            Account_CPF__c,
            PersMobPhone__c,
            PersPhone__c,
            Detail__c,
            Email__c,
            Sub_Detail__c,
            Vehicle_Of_Interest__c,
            LeadSource,
            LeadSubsource__c,
            Description
            
            FROM Opportunity
            WHERE id =: ctr.getId() AND RecordTypeId =: Utils.getRecordTypeId('Opportunity', 'DVR')
        ];
    }
    
    public PageReference marcarAtendimento() {
        
        if(String.isEmpty(opp.Description)) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 'Campo Descrição é obrigatório! Caso o cliente não tenha interesse, favor fechar a oportunidade.'));
            return null;
        }
        
        if(!opp.StageName.equals('Identified')) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 'Esta oportunidade já está em atendimento '));
            return null;
        }
        
        // Para passar sobre a regra de validação que não permite o vendedor alterar o stage da oportunidade fora do SFA
        opp.DateTimeSellerUpdatedStageOpp__c = System.now();

        opp.StageName = 'In Attendance';
        
        try {
            Database.update(opp);
            
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, ex.getMessage()));
            
            return null;
        }
        
        PageReference pageRef = new PageReference('/' + opp.Id);
        return pageRef;
    }
}