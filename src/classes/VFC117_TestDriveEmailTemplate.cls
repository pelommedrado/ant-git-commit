public class VFC117_TestDriveEmailTemplate
{
    private static final VFC117_TestDriveEmailTemplate instance = new VFC117_TestDriveEmailTemplate();

    public String salesforcePrefix  { get; set;}
    public String orgId             { get; set;}


    private VFC117_TestDriveEmailTemplate()
    {
        this.salesforcePrefix = VFC116_Util_Instance.getSalesforcePrefix();
        this.orgId = UserInfo.getOrganizationId();
    }

    public static VFC117_TestDriveEmailTemplate getInstance(){
        return instance;
    }
    


    // Template 04:
    //  Confirmação para o Cliente (Concessionária com Salesforce): Email enviado para o Cliente confirmando o
    //  agendamento de Test Drive com os dados do veículo e concessionária.
    //
    public String buildTemplate04(Account dealerAccount, String name, String phone, String cep, 
        String vehicle, Datetime DateHour)
    {
        system.debug('*** buildTemplate04()');

        String[] formatted = formatDateTime(DateHour);

        String body = ''
        +'<html>'
        +'  <head>'
        +'      <title>Renault - Template Salesforce 4</title>'
        +'  </head>'
        +'  <body>'
        +'      <table align="center" cellpadding="0" cellspacing="0" width="580" style="border: solid 1px #111111; background: #ffffff;">'
        +'          <tbody>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##01a.png##" width="540" height="74" border="0" style="display: block;" alt="" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="30"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <span style="font-size: 14px; font-weight: bold; color: #ed087d;">'+name+'</span>,<br />'
        +'                      <br />'
        +'                      <br />'
        +'                      Obrigado por agendar seu test-drive na Renault.<br />'
        +'                      <br />'
        +'                      Você vai dirigir um <span style="color: #ed087d;">'+vehicle+'</span><br />'
        +'                      Data: <span style="color: #ed087d;">'+formatted[0]+'/'+formatted[1]+'/'+formatted[2]+'</span><br />'
        +'                      Hora: <span style="color: #ed087d;">'+formatted[3]+':'+formatted[4]+'</span><br />'
        +'                      Concessionária: <span style="color: #ed087d;">'+dealerAccount.ShortndName__c+'</span><br />'
        +'                      Endereço: <span style="color: #ed087d;">'+dealerAccount.ShippingStreet+' - '+dealerAccount.ShippingCity+' - '+dealerAccount.ShippingState+'</span>'
        +'                  </td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="10"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td width="200" valign="top" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <br />'
        +'                      <br />'
        +'                      Esperamos por você.<br />'
        +'                      <br />'
        +'                      <br />'
        +'                      <!-- <a href="'+buildFacebookLink(vehicle)+'" target="_blank">'
        +'                          <img src="##bt-facebook.png##" width="187" height="18" border="0" style="display: block;" alt="" />'
        +'                      </a> -->'
        +'                  </td>'
        +'                  <td width="18"></td>'
        +'                  <td width="322">'
        +'                      <!-- programar o carregamento da imagem do carro -->'
        +'                      <img src="##carro.jpg##" width="322" height="204" border="0" style="display: block;" alt="" />'
        +'                  </td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="10"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##02.png##" width="540" height="72" border="0" style="display: block;" alt="" usemap="#mapRodape" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'          </tbody>'
        +'      </table>'
        +'      <map id="mapRodape" name="mapRodape">'
        +'          <area shape="rect" coords="2,27,84,36" href="http://www.renault.com.br/" target="_blank" alt="" />'
        +'          <area shape="rect" coords="63,55,81,72" href="http://twitter.com/renaultbrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="86,55,103,72" href="http://www.facebook.com/RenaultBrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="108,55,125,72" href="http://www.youtube.com/renaultbrasil" target="_blank" alt="" />'
        +'      </map>'
        +'  </body>'
        +'</html>';

        // Images replace
        body = fetchDocumentAndReplaceTag('X01a', '##01a.png##', body);
        body = fetchDocumentAndReplaceTag('bt_facebook', '##bt-facebook.png##', body);
        body = fetchDocumentAndReplaceTag('carro_' + vehicle, '##carro.jpg##', body);
        body = fetchDocumentAndReplaceTag('X02', '##02.png##', body);
        
        return body;
    }

    
    // Template 05:
    // Confirmação para o Cliente (Concessionária sem Salesforce): Email enviado para o Cliente confirmando o 
    // agendamento de Test Drive com os dados do veículo e concessionária. Neste email deve ser informado que
    // deve haver uma confirmação com a Concessionária sobre a data e horário.
    public String buildTemplate05(Account dealerAccount, String name, String phone, String cep, 
        String vehicle, Datetime DateHour)
    {
        system.debug('*** buildTemplate05()');

        String[] formatted = formatDateTime(DateHour);

        String body = ''
        +'<html>'
        +'  <head>'
        +'      <title>Renault - Template Salesforce 5-1</title>'
        +'  </head>'
        +'  <body>'
        +'      <table align="center" cellpadding="0" cellspacing="0" width="580" style="border: solid 1px #111111; background: #ffffff;">'
        +'          <tbody>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##01a.png##" width="540" height="74" border="0" style="display: block;" alt="" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="30"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <span style="font-size: 14px; font-weight: bold; color: #ed087d;">'+name+'</span>,<br />'
        +'                      <br />'
        +'                      <br />'
        +'                      Obrigado por agendar seu test-drive na Renault. A concessionária entrará em contato com você para confirmar a data e o horário solicitados.<br />'
        +'                      <br />'
        +'                      Você vai dirigir um <span style="color: #ed087d;">'+ vehicle+'</span><br />'
        +'                      Data: <span style="color: #ed087d;">'+formatted[0]+'/'+formatted[1]+'/'+formatted[2]+'</span><br />'
        +'                      Hora: <span style="color: #ed087d;">'+formatted[3]+':'+formatted[4]+'</span><br />'
        +'                      Concessionária: <span style="color: #ed087d;">'+dealerAccount.ShortndName__c+'</span><br />'
        +'                      Endereço: <span style="color: #ed087d;">'+dealerAccount.ShippingStreet+' - '+dealerAccount.ShippingCity+' - '+dealerAccount.ShippingState+'</span>'
        +'                  </td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="10"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td width="200" valign="top" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <br />'
        +'                      <br />'
        +'                      Esperamos por você.<br />'
        +'                      <br />'
        +'                      <br />'
        +'                      <!-- <a href="'+buildFacebookLink(vehicle)+'" target="_blank">'
        +'                          <img src="##bt-facebook.png##" width="187" height="18" border="0" style="display: block;" alt="" />'
        +'                      </a> -->'
        +'                  </td>'
        +'                  <td width="18"></td>'
        +'                  <td width="322">'
        +'                      <!-- programar o carregamento da imagem do carro -->'
        +'                      <img src="##carro.jpg##" width="322" height="204" border="0" style="display: block;" alt="" />'
        +'                  </td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="10"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##02.png##" width="540" height="72" border="0" style="display: block;" alt="" usemap="#mapRodape" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'          </tbody>'
        +'      </table>'
        +'      <map id="mapRodape" name="mapRodape">'
        +'          <area shape="rect" coords="2,27,84,36" href="http://www.renault.com.br/" target="_blank" alt="" />'
        +'          <area shape="rect" coords="63,55,81,72" href="http://twitter.com/renaultbrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="86,55,103,72" href="http://www.facebook.com/RenaultBrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="108,55,125,72" href="http://www.youtube.com/renaultbrasil" target="_blank" alt="" />'
        +'      </map>'
        +'  </body>'
        +'</html>';

        // Images replace
        body = fetchDocumentAndReplaceTag('X01a', '##01a.png##', body);
        body = fetchDocumentAndReplaceTag('bt_facebook', '##bt-facebook.png##', body);
        body = fetchDocumentAndReplaceTag('carro_' + vehicle, '##carro.jpg##', body);
        body = fetchDocumentAndReplaceTag('X02', '##02.png##', body);
        
        return body;
    }
    
    
    // Template 06:
    //  Confirmação para a Concessionária (Concessionária com Salesforce):  Email enviado para a Concessionária
    //  confirmando o agendamento de Test Drive com os dados do Cliente, data e horário agendado.
    //
    public String buildTemplate06(Account dealerAccount, String name, String phone, String cep, 
        String vehicle, Datetime DateHour)
    {
        system.debug('*** buildTemplate06()');

        String[] formatted = formatDateTime(DateHour);

        String body = ''
        +'<html>'
        +'  <head>'
        +'      <title>Renault - Template Salesforce 6</title>'
        +'  </head>'
        +'  <body>'
        +'      <table align="center" cellpadding="0" cellspacing="0" width="580" style="border: solid 1px #111111; background: #ffffff;">'
        +'          <tbody>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##01b.png##" width="540" height="74" border="0" style="display: block;" alt="" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="30"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <span style="font-size: 14px; font-weight: bold; color: #ed087d;">'+dealerAccount.ShortndName__c+'</span>,<br />'
        +'                      <br />'
        +'                      <br />'
        +'                      Um test-drive foi agendado na sua concessionária. Seguem os dados do cliente e as informações do agendamento.<br />'
        +'                      <br />'
        +'                      <span style="color: #ed087d;">'+ name +'</span><br />'
        +'                      Telefone: <span style="color: #ed087d;">'+ phone +'</span><br />'
        +'                      Data: <span style="color: #ed087d;">'+formatted[0]+'/'+formatted[1]+'/'+formatted[2]+'</span><br />'
        +'                      Hora: <span style="color: #ed087d;">'+formatted[3]+':'+formatted[4]+'</span><br />'
        +'                      Carro: <span style="color: #ed087d;">'+vehicle+'</span><br />'
        +'                      Concessionária: <span style="color: #ed087d;">'+dealerAccount.ShortndName__c+'</span>'
        +'                  </td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="40"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td width="200" valign="top" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                      <img src="##bt-renault+cliente.png##" width="114" height="30" border="0" style="display: block;" alt="" />'
        +'                  </td>'
        +'                  <td width="18"></td>'
        +'                  <td width="322"></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="40"></td></tr>'
        +'              <tr>'
        +'                  <td width="19"></td>'
        +'                  <td colspan="3"><img src="##02.png##" width="540" height="72" border="0" style="display: block;" alt="" usemap="#mapRodape" /></td>'
        +'                  <td width="19"></td>'
        +'              </tr>'
        +'              <tr><td colspan="5" height="19"></td></tr>'
        +'          </tbody>'
        +'      </table>'
        +'      <map id="mapRodape" name="mapRodape">'
        +'          <area shape="rect" coords="2,27,84,36" href="http://www.renault.com.br/" target="_blank" alt="" />'
        +'          <area shape="rect" coords="63,55,81,72" href="http://twitter.com/renaultbrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="86,55,103,72" href="http://www.facebook.com/RenaultBrasil" target="_blank" alt="" />'
        +'          <area shape="rect" coords="108,55,125,72" href="http://www.youtube.com/renaultbrasil" target="_blank" alt="" />'
        +'      </map>'
        +'  </body>'
        +'</html>';
        
        // Images replace
        body = fetchDocumentAndReplaceTag('X01b', '##01b.png##', body);
        body = fetchDocumentAndReplaceTag('X02', '##02.png##', body);
        body = fetchDocumentAndReplaceTag('bt_renault_cliente', '##bt-renault+cliente.png##', body);

        return body;
    }


    // Template 07:
    // Confirmação para a Concessionária (Concessionária sem Salesforce):  Email enviado para a Concessionária 
    // confirmando o agendamento de Test Drive com os dados do Cliente e Horário sugerido. Neste email deve ser 
    // informado que a concessionária deve ligar para o cliente e confirmar o agendamento do Test Drive.
    //
    public String buildTemplate07(Account dealerAccount, String name, String emailCustomer, String phone, String cep, 
        String vehicle, Datetime DateHour)
    {
        system.debug('*** buildTemplate07()');

        String[] formatted = formatDateTime(DateHour);

        String body = ''
        +'<html>'
        +'<head>'
        +'    <title>Renault - Template Salesforce 7</title>'
        +'</head>'
        +'<body>'
        +'    <table align="center" cellpadding="0" cellspacing="0" width="580" style="border: solid 1px #111111; background: #ffffff;">'
        +'        <tbody>'
        +'            <tr><td colspan="5" height="19"></td></tr>'
        +'          <tr>'
        +'                <td width="19"></td>'
        +'                <td colspan="3"><img src="##01b.png##" width="540" height="74" border="0" style="display: block;" alt="" /></td>'
        +'                <td width="19"></td>'
        +'          </tr>'
        +'            <tr><td colspan="5" height="30"></td></tr>'
        +'            <tr>'
        +'                <td width="19"></td>'
        +'                <td colspan="3" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                    <span style="font-size: 14px; font-weight: bold; color: #ed087d;">'+dealerAccount.ShortndName__c+'</span>,<br />'
        +'                    <br />'
        +'                    <br />'
        +'                    Um test-drive foi agendado na sua concessionária. Entre em contato com o cliente para confirmar a data e o horário solicitados.<br />'
        +'                    <br />'
        +'                    <span style="font-size: 14px; font-weight: bold;">Dados do cliente</span><br />'
        +'                    Nome: <span style="color: #ed087d;">'+name+'</span><br />'
        +'                    Telefone: <span style="color: #ed087d;">'+phone+'</span><br />'
        +'                    E-mail: <span style="color: #ed087d;">'+emailCustomer+'</span><br />'
        +'                    Data: <span style="color: #ed087d;">'+formatted[0]+'/'+formatted[1]+'/'+formatted[2]+'</span><br />'
        +'                    Hora: <span style="color: #ed087d;">'+formatted[3]+':'+formatted[4]+'</span><br />'
        +'                    Carro: <span style="color: #ed087d;">'+vehicle+'</span><br />'
        +'                    Concessionária: <span style="color: #ed087d;">'+dealerAccount.ShortndName__c+'</span>'
        +'                </td>'
        +'                <td width="19"></td>'
        +'            </tr>'
        +'            <tr><td colspan="5" height="40"></td></tr>'
        +'            <tr>'
        +'                <td width="19"></td>'
        +'                <td width="200" valign="top" style="font-family: Tahoma; font-size: 13px; color: #545454;">'
        +'                    <img src="##bt-renault+cliente.png##" width="114" height="30" border="0" style="display: block;" alt="" />'
        +'                </td>'
        +'                <td width="18"></td>'
        +'                <td width="322"></td>'
        +'                <td width="19"></td>'
        +'            </tr>'
        +'            <tr><td colspan="5" height="40"></td></tr>'
        +'            <tr>'
        +'                <td width="19"></td>'
        +'              <td colspan="3"><img src="##02.png##" width="540" height="72" border="0" style="display: block;" alt="" usemap="#mapRodape" /></td>'
        +'                <td width="19"></td>'
        +'          </tr>'
        +'            <tr><td colspan="5" height="19"></td></tr>'
        +'      </tbody>'
        +'  </table>'
        +'    <map id="mapRodape" name="mapRodape">'
        +'        <area shape="rect" coords="2,27,84,36" href="http://www.renault.com.br/" target="_blank" alt="" />'
        +'        <area shape="rect" coords="63,55,81,72" href="http://twitter.com/renaultbrasil" target="_blank" alt="" />'
        +'        <area shape="rect" coords="86,55,103,72" href="http://www.facebook.com/RenaultBrasil" target="_blank" alt="" />'
        +'        <area shape="rect" coords="108,55,125,72" href="http://www.youtube.com/renaultbrasil" target="_blank" alt="" />'
        +'    </map>'
        +'</body>'
        +'</html>';

        // Images replace
        body = fetchDocumentAndReplaceTag('X01b', '##01b.png##', body);
        body = fetchDocumentAndReplaceTag('X02', '##02.png##', body);
        body = fetchDocumentAndReplaceTag('bt_renault_cliente', '##bt-renault+cliente.png##', body);

        return body;
    }


    private String buildFacebookLink(String model)
    {
        String link = '';

        if (model == 'LOGAN')
            link = 'http://www.facebook.com/sharer.php?s=100&p[url]=http://www.renault.com.br/Veiculos/conheca-e-compare-a-gama-renault/logan/logan/&p[images][0]=http://campanhas.renault.com.br/images/thumb-logan-v2.jpg&p[title]=Test drive Renault&p[summary]=Acabei de agendar um test drive na Renault com o Renault Logan. Agende o seu tamb%C3%A9m!';
        else if (model == 'FLUENCE')
            link = 'http://www.facebook.com/sharer.php?s=100&p[url]=http://www.renault.com.br/Veiculos/conheca-e-compare-a-gama-renault/fluence/fluence/&p[images][0]=http://campanhas.renault.com.br/images/thumb-fluence-v2.jpg&p[title]=Test drive Renault&p[summary]=Acabei de agendar um test drive na Renault com o Renault Fluence. Agende o seu tamb%C3%A9m!';
        else if (model == 'SANDERO')
            link = 'http://www.facebook.com/sharer.php?s=100&p[url]=http://www.renault.com.br/Veiculos/conheca-e-compare-a-gama-renault/sandero/sandero/explore/&p[images][0]=http://campanhas.renault.com.br/images/thumb-sandero.jpg&p[title]=Test drive Renault&p[summary]=Acabei de agendar um test drive na Renault com o Renault Sandero. Agende o seu tamb%C3%A9m!';
        else if (model == 'CLIO')
            link = 'http://www.facebook.com/sharer.php?s=100&p[url]=http://www.renault.com.br/Veiculos/conheca-e-compare-a-gama-renault/clio/clio/&p[images][0]=http://campanhas.renault.com.br/images/thumb-clio-v2.jpg&p[title]=Test drive Renault&p[summary]=Acabei de agendar um test drive na Renault com o Renault Clio. Agende o seu tamb%C3%A9m!';
        else if (model == 'DUSTER')
            link = 'http://www.facebook.com/sharer.php?s=100&p[url]=http://www.renault.com.br/Veiculos/conheca-e-compare-a-gama-renault/duster/duster/&p[images][0]=http://campanhas.renault.com.br/images/thumb-duster.jpg&p[title]=Test drive Renault&p[summary]=Acabei de agendar um test drive na Renault com o Renault Duster. Agende o seu tamb%C3%A9m!';
        else 
            link = 'http://www.facebook.com/RenaultBrasil';

        return link;
    }
    

    private String fetchDocumentAndReplaceTag(String devName, String tag, String content)
    {
        try  {
            Document doc = [select id, Name from Document where DeveloperName = :devName limit 1];
            content = content.replace(tag, 'https://'+this.salesforcePrefix+'.salesforce.com/servlet/servlet.ImageServer?id='+doc.Id+'&oid='+this.orgId);
        }
        catch (Exception e) {
            system.debug('*** Image ' + devName + ' not found!');
            content = content.replace(tag, 'not_found');
        }
        return content;
    }
    
    private String[] formatDateTime(Datetime DateHour)
    {
        String dt_Day = String.valueOf(DateHour.day());
        String dt_month;
        if (DateHour.month() < 10) dt_month = '0' + String.valueOf(DateHour.month());
        else dt_month = String.valueOf(DateHour.month());
        String dt_year = String.valueOf(DateHour.Year());
        String dt_hour = String.valueOf(DateHour.hour());
        String dt_minute;
        if (DateHour.minute() < 10) dt_minute = '0' + String.valueOf(DateHour.minute());
        else dt_minute = String.valueOf(DateHour.minute());

        return new String[]{dt_Day, dt_month, dt_year, dt_hour, dt_minute};
    }

}