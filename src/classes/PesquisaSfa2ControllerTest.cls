@isTest
private class PesquisaSfa2ControllerTest {
    //static User 		communityUser 	= null;
    static Campaign 	campaign 		= null;
    //static Account 		persAccount 	= null;
    //static Lead 		lead			= null;
    //static Lead 		leadOld			= null;
    static VEH_Veh__c 	veiculo 		= null;
    static VRE_VehRel__c veiculoRel 	= null;
    static Opportunity 	opp				= null;
    static Account 		accGroupDealer	= null;
    static Account 		accDealer		= null;

    static User userRun;
    static User localUser;

    static {

        localUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.runAs(localUser) {
            accDealer = new Account( Name = 'Dealer',
                RecordTypeId = Utils.getRecordTypeId('Account', 'Network_Site_Acc'),
                IDBIR__c = '123456'
            );
            INSERT accDealer;

            Contact contact = new Contact( CPF__c =  '42616895617',
                Email = 'test@org1.com', FirstName = 'test',
                LastName = 'test', MobilePhone = '11223344556'
            );
            contact.AccountId = accDealer.Id;
            INSERT contact;

            userRun = new User(FirstName = 'Mario', LastName = 'Andrade',
                Username = 'uservendedor@test.com',
                Email = 'test@test.com', Alias = 'tes',
                RecordDefaultCountry__c = 'Brazil',
                BIR__c = '123456', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles'
            );
            userRun.ProfileId = [SELECT Id FROM Profile WHERE Name = 'SFA - Seller'].Id;
            userRun.ContactId = contact.Id;
            INSERT  userRun;

            veiculo = new VEH_Veh__c(
               Name='12345678912458965',
               Model__c = 'Test',
               Status__c = 'Active',
               Is_Available__c = true
            );
            veiculo.Status__c = 'Booking';
            INSERT veiculo;

            veiculoRel = new VRE_VehRel__c(Status__c = 'Active');
            veiculoRel.VIN__c = veiculo.Id;
            veiculoRel.Account__c = accDealer.Id;
            INSERT veiculoRel;
        }
    }

    // static {
    //     Test.startTest();
    //
    //     userRun = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
    //
    //
    //     MyOwnCreation moc = new MyOwnCreation();
    //     communityUser = moc.CriaUsuarioComunidade();
    //     System.runAs(userRun) {
    //
    //         /*campaign = moc.criaCampanha();
    //         INSERT campaign;
    //
    //         persAccount = moc.criaPersAccount();
    //         INSERT persAccount;
    //
    //         lead = moc.CriaLead();
    //         INSERT lead;
    //
    //         leadOld = moc.CriaLead();
    //         leadOld.RecordTypeId = Utils.getRecordTypeId('Lead', 'Vendas_Empresa_PF');
    //         INSERT leadOld;*/
    //         /*accGroupDealer = moc.criaAccountDealer();
    //         accGroupDealer.Name = 'Group Dealer';
    //         INSERT accGroupDealer;
    //         */
    //         accDealer = moc.criaAccountDealer();
    //         accDealer.Name = 'Teste 2';
    //         accDealer.IDBIR__c = '1234567';
    //         //accDealer.ParentId = accGroupDealer.Id;
    //         INSERT accDealer;
    //
    //         veiculo = moc.criaVeiculo();
    //         veiculo.Status__c = 'Booking';
    //         INSERT veiculo;
    //
    //         veiculoRel = moc.criaVeiculoRelacionado();
    //         veiculoRel.VIN__c = veiculo.Id;
    //         veiculoRel.Account__c = accDealer.Id;
    //         INSERT veiculoRel;
    //
    //         opp = moc.criaOpportunity();
    //         //opp.Vehicle_Of_Interest__c = 'Kwid';
    //         //opp.TransactionCode__c = 'code';
    //         opp.SourceMedia__c = '	Indication';
    //         INSERT opp;
    //
    //         Quote quote = moc.criaQuote();
    //         quote.OpportunityId = opp.id;
    //         quote.Status = 'Active';
    //         INSERT quote;
    //
    //         VehicleBooking__c vbk = new VehicleBooking__c();
    //         vbk.Vehicle__c = veiculo.Id;
    //         vbk.Status__c = 'Active';
    //         vbk.Quote__c = quote.id;
    //         INSERT vbk;
    //     }
    //     Test.stopTest();
    // }

    static testMethod void deveCriarControllerVeiculo() {
        Test.startTest();

        System.runAs(localUser) {
            opp = new Opportunity(Name = 'teste',
                StageName = 'test', CloseDate = system.today()
            );
            opp.Vehicle_Of_Interest__c = 'Kwid';
            opp.TransactionCode__c = 'code';
            opp.SourceMedia__c = '	Indication';
            INSERT opp;

            Quote quote = new Quote( Name = 'teste', Status = 'Open');
            quote.OpportunityId = opp.id;
            quote.Status = 'Active';
            INSERT quote;

            VehicleBooking__c vbk = new VehicleBooking__c();
            vbk.Vehicle__c = veiculo.Id;
            vbk.Status__c = 'Active';
            vbk.Quote__c = quote.id;
            INSERT vbk;
        }

        System.runAs(userRun) {
            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            Apexpages.currentPage().getParameters().put('veh', veiculoRel.Id);

            PesquisaSfa2Controller controller = new PesquisaSfa2Controller();
            controller.contaOpp = 'string';
            String nomeUser = controller.nomeUser;
            String fieldMapping = controller.fieldMapping;
            String campaignId = controller.campaignId;
            List<SelectOption> campaignSelectList = controller.campaignSelectList;
            String sellerId = controller.sellerId;
            List<SelectOption> sellerSelectList = controller.sellerSelectList;
            String sellerName = controller.sellerName;
            String preF = controller.accountPrefix;
        }
    }

    static testMethod void deveQueryObject() {
        Test.startTest();
        System.runAs(userRun) {
            PesquisaSfa2Controller.queryObject( 'Lead', 'Opportunity' );
        }
        Test.stopTest();
    }

    static testMethod void deveVerificarRetorno() {
        Test.startTest();
        System.runAs(userRun) {
            PesquisaSfa2Controller.verificarRetornoOpp('73412052612');
        }
        Test.stopTest();
    }

    static testMethod void deveTratarHistoricoOpp() {
        Test.startTest();
        System.runAs(userRun) {
            opp = new Opportunity(Name = 'teste',
                StageName = 'test', CloseDate = system.today()
            );
            opp.Vehicle_Of_Interest__c = 'Kwid';
            opp.TransactionCode__c = 'code';
            opp.SourceMedia__c = '	Indication';
            INSERT opp;

            PesquisaSfa2Controller.OppHistoricoRetorno oppHist =
                new PesquisaSfa2Controller.OppHistoricoRetorno();

            List<Opportunity> lsOpp = new List<Opportunity>();
            lsOpp.add(opp);
            oppHist.tratar(lsOpp);
            oppHist.oportunidadeExpirou();
            oppHist.oportunidadeAceita();
            oppHist.oportunidadeReabrir();
            oppHist.oportunidadeNaoReabre();
            oppHist.oportunidadeNaoReabreFaturada();
        }
        Test.stopTest();
    }

    static testMethod void deve() {
        Test.startTest();

        System.runAs(userRun) {

            Account persAccount = new Account( FirstName = 'test', LastName = 'Sobrenome',
                PersEmailAddress__c = 'test@teste.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = '45214524',
                PersLandline__c = '45214578');
            INSERT persAccount;

            Lead lead = new Lead( FirstName = 'Test', LastName = 'Lead',
                CPF_CNPJ__c = '73412052612', LeadSource = 'Internet',
                SubSource__c = 'Marketing', Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found');
            INSERT lead;

            Lead leadOld = new Lead( FirstName = 'Test', LastName = 'Lead',
                CPF_CNPJ__c = '73412052612', LeadSource = 'Internet',
                SubSource__c = 'Marketing', Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found');
            leadOld.RecordTypeId = Utils.getRecordTypeId('Lead', 'Vendas_Empresa_PF');
            INSERT leadOld;

            PesquisaSfa2Controller controller = new PesquisaSfa2Controller();

            controller.acc.Id = persAccount.Id;
            controller.leadId = lead.Id;

            Quarantine__c q = new Quarantine__c(Lead__c=leadOld.Id);
            INSERT q;
            controller.retailPlataform(leadOld.Id);
            controller.updateLeadFromSFA(leadOld);
            controller.selectAccount();
            controller.selectLead();
        }
        Test.stopTest();
    }

    static testMethod void deveConverteLead() {
        Test.startTest();

        System.runAs(localUser) {
            opp = new Opportunity(Name = 'teste',
                StageName = 'test', CloseDate = system.today()
            );
            opp.Vehicle_Of_Interest__c = 'Kwid';
            opp.TransactionCode__c = 'code';
            opp.SourceMedia__c = '	Indication';
            INSERT opp;

            Quote quote = new Quote( Name = 'teste', Status = 'Open');
            quote.OpportunityId = opp.id;
            quote.Status = 'Active';
            INSERT quote;

            VehicleBooking__c vbk = new VehicleBooking__c();
            vbk.Vehicle__c = veiculo.Id;
            vbk.Status__c = 'Active';
            vbk.Quote__c = quote.id;
            INSERT vbk;
        }

        System.runAs(userRun) {

            Account cliente = new Account( FirstName = 'test', LastName = 'Sobrenome',
                PersEmailAddress__c = 'test@teste.com',
                RecordTypeId = Utils.getRecordTypeId('Account','Personal_Acc'),
                VehicleInterest_BR__c = 'test',
                PersMobPhone__c = '45214524',
                PersLandline__c = '45214578');
            INSERT cliente;

            Lead lead = new Lead( FirstName = 'Test', LastName = 'Lead',
                CPF_CNPJ__c = '73412052612', LeadSource = 'Internet',
                SubSource__c = 'Marketing', Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found');
            lead.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT lead;

            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();
            List<SelectOption> opt = crtl.dealerSelectList;
            crtl.insertOpportunity([SELECT Id FROM Account WHERE IDBIR__c = '123456'].Id);
            crtl.convertLead(cliente.Id, lead);
            crtl.save();

            PesquisaSfa2Controller.verificarRetornoOpp('73412052612');
            Apexpages.currentPage().getParameters().put('oppId', opp.Id);
            crtl.solicitarAcessoOportunidade();
            crtl.opp.Ownership_requested_by_ID__c = 'test';
            crtl.solicitarAcessoOportunidade();
        }
        Test.stopTest();
    }


    static testMethod void deveRetornaErroSave() {
        Test.startTest();

        System.runAs(userRun) {

            Lead lead = new Lead( FirstName = 'Test', LastName = 'Lead',
                CPF_CNPJ__c = '73412052612', LeadSource = 'Internet',
                SubSource__c = 'Marketing', Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found');

            lead.CPF_CNPJ__c = '52064560718';
            lead.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT lead;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();

            crtl.opp.SourceMedia__c = 'Telephone';
            crtl.save();

            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE');
            crtl.save();

            crtl.acc.Type_DVE__c = 'SIM';
            crtl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa');
            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE');
            crtl.save();

            crtl.companyName = 'Minha empresa';
            crtl.save();

            crtl.acc.CustomerIdentificationNbr__c = '0123456789';
            crtl.save();
            crtl.acc.CustomerIdentificationNbr__c = '52064560718';
            crtl.save();

            crtl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa_PF');
            crtl.save();

            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR');
            crtl.save();
        }
        Test.stopTest();
    }

    static testMethod void deveRetornaErroSave2() {

        Test.startTest();


        System.runAs(userRun) {
            opp = new Opportunity(Name = 'teste',
                StageName = 'test', CloseDate = system.today()
            );
            opp.Vehicle_Of_Interest__c = 'Kwid';
            opp.TransactionCode__c = 'code';
            opp.StageName = 'Lost';
            opp.SourceMedia__c = '	Indication';
            INSERT opp;

            Quote quote = new Quote( Name = 'teste', Status = 'Open');
            quote.OpportunityId = opp.id;
            quote.Status = 'Active';
            INSERT quote;

            Lead lead = new Lead( FirstName = 'Test', LastName = 'Lead',
                CPF_CNPJ__c = '73412052612', LeadSource = 'Internet',
                SubSource__c = 'Marketing', Detail__c = 'I am Interested',
                Sub_Detail__c = 'Not Found');

            lead.CPF_CNPJ__c = '52064560718';
            lead.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT lead;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();

            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE');
            crtl.save();

            PesquisaSfa2Controller.OppHistoricoRetorno tr = new
              PesquisaSfa2Controller.OppHistoricoRetorno();
            tr.tratar(new List<Opportunity> { opp });

            opp.Vehicle_Of_Interest__c = 'CLIO';
            update opp;
            tr.tratar(new List<Opportunity> { opp });

            //
            //tr.isDate();
            ///tr.isTransCode();
            //opp.Vehicle_Of_Interest__c = 'CLIO';
            //tr.isPerdidaValido();
            //
            // Opportunity opp1 = moc.criaOpportunity();
            // opp1.StageName = 'Lost';
            // opp1.Vehicle_Of_Interest__c = 'CLIO';
            // opp1.SourceMedia__c = '	Indication';
            // INSERT opp1;
            // tr.tratar(new List<Opportunity> { opp1 });
            // tr.isDate();
            // tr.isTransCode();
        }
        Test.stopTest();
    }

    /*static testMethod void deveRetornaErroSave3() {

        //Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        system.runAs(communityUser){
            Lead le = moc.CriaLead();
            le.CPF_CNPJ__c = '52064560718';
            le.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT le;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();

            crtl.acc.Type_DVE__c = 'SIM';
            crtl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa');
            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE');
            crtl.save();
        }

        //Test.stopTest();

    }

    static testMethod void deveRetornaErroSave4() {

        //Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        system.runAs(communityUser){
            Lead le = moc.CriaLead();
            le.CPF_CNPJ__c = '52064560718';
            le.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT le;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();

            crtl.companyName = 'Minha empresa';
            crtl.save();

        }

        //Test.stopTest();

    }

    static testMethod void deveRetornaErroSave5() {

        //Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        system.runAs(communityUser){
            Lead le = moc.CriaLead();
            le.CPF_CNPJ__c = '52064560718';
            le.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT le;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();

            crtl.acc.CustomerIdentificationNbr__c = '0123456789';
            crtl.save();
            crtl.acc.CustomerIdentificationNbr__c = '52064560718';
            crtl.save();
        }

        //Test.stopTest();

    }

    static testMethod void deveRetornaErroSave6() {

        //Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();
        system.runAs(communityUser){
            Lead le = moc.CriaLead();
            le.CPF_CNPJ__c = '52064560718';
            le.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            INSERT le;

            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersMobPhone__c = '123456789';
            crtl.cnt.FirstName = 'Test';
            crtl.cnt.LastName = 'Account';
            crtl.opp.Vehicle_Of_Interest__c = 'CLIO';
            crtl.opp.SourceMedia__c = 'Indication';
            crtl.save();


            crtl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'Vendas_Empresa_PF');
            crtl.save();

            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVR');
            crtl.save();
        }

        //Test.stopTest();

    }*/


   /*static testMethod void test1() {

       Lead lead;
       MyOwnCreation moc = new MyOwnCreation();
       User communityUser = moc.CriaUsuarioComunidade();

        //Executando como usuario da comunidade - Vendedor
        System.runAs(communityUser){

            test.startTest();

            // Test Controller e variaveis
            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller controller = new PesquisaSfa2Controller();

            controller.contaOpp = 'string';
            String nomeUser = controller.nomeUser;
            String fieldMapping = controller.fieldMapping;
            String campaignId = controller.campaignId;
            list<SelectOption> campaignSelectList = controller.campaignSelectList;
            String sellerId = controller.sellerId;
            list<SelectOption> sellerSelectList = controller.sellerSelectList;

            controller.acc.PersMobPhone__c = '123456789';
            controller.cnt.FirstName = 'Test';
            controller.cnt.LastName = 'Account';
            controller.opp.Vehicle_Of_Interest__c = 'CLIO';
            controller.opp.SourceMedia__c = 'Indication';

            //testar metodo save com CPF invalido
            controller.acc.CustomerIdentificationNbr__c = '1111111111';
            controller.save();

            //testar metodo save sem conta e sem lead antigo
            controller.acc.CustomerIdentificationNbr__c = '20280602634';
            controller.save();

            //testar metodo save sem conta e com Lead antigo
            controller.leadId = leadOld.Id;
            controller.acc.CustomerIdentificationNbr__c = '89254857882';
            controller.save();

            //testar metodo save com conta e com Lead antigo
            controller.leadId = lead.Id;
            controller.acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Personal_Acc');
            controller.acc.CustomerIdentificationNbr__c = '73412052612';
            controller.save();

            //testar metodo save - exception todos os contatos estao nulos
            controller.acc.PersEmailAddress__c = null;
            controller.acc.PersMobPhone__c = null;
            controller.acc.PersLandline__c = null;
            controller.save();

            //testar CPF inválido
            controller.acc.CustomerIdentificationNbr__c = '2325642552452152214';
            controller.save();

            String accountPrefix = controller.accountPrefix;
            controller.leadId = leadOld.Id;

            test.stopTest();

        }
   }*/

    /*static testMethod void test2() {

        Lead lead;
        MyOwnCreation moc = new MyOwnCreation();
        User communityUser = moc.CriaUsuarioComunidade();

        //Executando como usuario da comunidade - Vendedor
        System.runAs(communityUser){

            test.startTest();

            Campaign campaign = moc.criaCampanha();
            insert campaign;

            Account persAccount = moc.criaPersAccount();
            insert persAccount;

            lead = moc.CriaLead();
            insert lead;

            Lead leadOld = moc.CriaLead();
            leadOld.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            insert leadOld;

            VEH_Veh__c veiculo = moc.criaVeiculo();
            insert veiculo;

            Opportunity opp = moc.criaOpportunity();
            opp.Vehicle_Of_Interest__c = veiculo.Id;
            opp.SourceMedia__c = '	Indication';
            insert opp;

            // Test Controller e variaveis
            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller controller = new PesquisaSfa2Controller();

            //testar metodo save com campos obrigatorios nao informados
            controller.acc.CustomerIdentificationNbr__c = '38870932400';
            controller.opp.Vehicle_Of_Interest__c = null;
            controller.save();

            PesquisaSfa2Controller.OppHistoricoRetorno oppHist = new PesquisaSfa2Controller.OppHistoricoRetorno();
            List<Opportunity> lsOpp = new List<Opportunity>();
            lsOpp.add(opp);
            oppHist.tratar(lsOpp);
            oppHist.oportunidadeExpirou();
            oppHist.oportunidadeAceita();
            oppHist.oportunidadeReabrir();
            oppHist.oportunidadeNaoReabre();
            oppHist.oportunidadeNaoReabreFaturada();

            PesquisaSfa2Controller.queryObject( 'Lead', 'Opportunity' );

            test.stopTest();

        }
    }

    static testMethod void test3() {

        Lead lead;
        MyOwnCreation moc = new MyOwnCreation();
        User communityUser = moc.CriaUsuarioComunidade();

        //Executando como usuario da comunidade - Vendedor
        System.runAs(communityUser){

            test.startTest();

            Campaign campaign = moc.criaCampanha();
            insert campaign;

            Account persAccount = moc.criaPersAccount();
            insert persAccount;

            lead = moc.CriaLead();
            insert lead;

            Lead leadOld = moc.CriaLead();
            leadOld.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            insert leadOld;

            VEH_Veh__c veiculo = moc.criaVeiculo();
            insert veiculo;

            Opportunity opp = moc.criaOpportunity();
            opp.Vehicle_Of_Interest__c = veiculo.Id;
            opp.SourceMedia__c = '	Indication';
            insert opp;

            // Test Controller e variaveis
            PageReference pageRef = Page.PesquisaSfa2;
            Test.setCurrentPage(pageRef);
            PesquisaSfa2Controller controller = new PesquisaSfa2Controller();

            controller.acc.Id = persAccount.Id;
            controller.leadId = lead.Id;

            Quarantine__c q = new Quarantine__c(Lead__c=leadOld.Id);
            insert q;
            controller.retailPlataform(leadOld.Id);
            controller.updateLeadFromSFA(leadOld);
            controller.selectAccount();
            controller.selectLead();
            PesquisaSfa2Controller.queryObject( 'Lead', 'Lead' );

            PesquisaSfa2Controller.verificarRetornoOpp('73412052612');

            pageRef.getParameters().put('oppId',opp.Id);
            controller.solicitarAcessoOportunidade();
            controller.opp.Ownership_requested_by_ID__c = 'test';
            controller.solicitarAcessoOportunidade();


            test.stopTest();
        }
    }

    static testMethod void teste4(){

        MyOwnCreation moc = new MyOwnCreation();
        User communityUser = moc.CriaUsuarioComunidade();

        test.startTest();

        Lead lead = moc.CriaLead();
        lead.CPF_CNPJ__c = '60609523562';
        lead.RecordTypeId = Utils.getRecordTypeId('Lead', 'Voucher');
        lead.Status = 'To Rescue';
        Insert lead;

        Campaign campanha = moc.criaCampanha();
        campanha.Billing_Required__c = FALSE;
        campanha.Refundable__c = FALSE;
        campanha.IsActive = TRUE;
        campanha.Status = 'In Progress';
        Insert campanha;

        CampaignMember cm = moc.criaMembroCampanha();
        cm.CampaignId = campanha.Id;
        cm.LeadId = lead.Id;
        Insert cm;

        //Executando como usuario da comunidade - Vendedor
        System.runAs(communityUser){

        PageReference pageRef = new PageReference('/apex/Pesquisafa2');
        Test.setCurrentPage(pageRef);

        PesquisaSfa2Controller controller = new PesquisaSfa2Controller();
        pageRef.getParameters().put('cmId',cm.Id);

        controller.setCustomerInformation();

        test.stopTest();

        }

    }



    static testMethod void test6(){

        Test.startTest();

        MyOwnCreation moc = new MyOwnCreation();

        User seller = moc.CriaUsuarioComunidade();

        system.runAs(seller){

            Account acc = new Account();
            acc.Name = 'Company';
            acc.Email__c = 'company@company.com';
            acc.Phone = '33333333';
            acc.CustomerIdentificationNbr__c = '57575505000100';
            acc.RecordTypeId = Utils.getRecordTypeId('Account', 'Company_Acc');
            Insert acc;

            Account cliente = moc.criaPersAccount();
            acc.CustomerIdentificationNbr__c = '20732982227';
            Insert cliente;

            Contact c = moc.criaContato();
            c.FirstName = 'Nome';
            c.LastName = 'Sobrenome';
            Insert c;

            Lead lead = moc.CriaLead();
            lead.RecordTypeId = Utils.getRecordTypeId('Lead','Vendas_Empresa_PF');
            Insert lead;

            PesquisaSfa2Controller crtl = new PesquisaSfa2Controller();

            crtl.acc.PersEmailAddress__c = 'teste@tste.com';
            crtl.acc.CustomerIdentificationNbr__c = '20732982227';
            crtl.acc.Type_DVE__c = 'SIM';
            crtl.oppRecTypeVendaDireta = Utils.getRecordTypeId('Opportunity', 'DVR');
            crtl.opp.RecordTypeId = Utils.getRecordTypeId('Opportunity', 'DVE');
            crtl.opp.Vehicle_Of_Interest__c = 'SANDERO';
            crtl.opp.SourceMedia__c = 'INTERNET';
            crtl.cnt.FirstName = 'Nome';
            crtl.cnt.LastName = 'Sobrenome';

            crtl.save();

        }

        Test.stopTest();
    }*/
}