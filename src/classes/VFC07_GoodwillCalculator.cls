public with sharing class VFC07_GoodwillCalculator 
{   
    public Goodwill__c goodwill {get;set;}

    public VFC07_GoodwillCalculator(ApexPages.StandardController controller) 
    { 
        Goodwill__c tmp = (Goodwill__c)controller.getRecord(); 
        goodwill = [select id
                    , name
                    , QuotClientRate__c
                    , DealerPartRate__c
                    , ICMPartRate__c
                    , ICMPartAmount__c
                    , SRC_Decided_Rate__c
                    , QuotWarrantyRate__c
                    , SRCPartAmount__c
                    , DealerPartAmount__c
                    , SRCPartRate__c
                    , Country__c
                    , Organ__c
                    , Brand__c
                    , Kilometer__c
                    , VehicleAgeInMonth__c  
                    , SRCRetainedRate__c 
                    , SRCDecidedRate__c
                    , DeviationReasDesc__c
                    , DeviationReason__c
                    from Goodwill__c 
                    where id = :tmp.id];       
    } 
    
    public void Save()
    {   
        Decimal p;
        
        if(goodwill.SRCRetainedRate__c == null) 
            p = getSRCParticipationRate(); 
        else 
            p = goodwill.SRCRetainedRate__c;
        
        if (goodwill.QuotWarrantyRate__c == null) 
            goodwill.SRCPartAmount2__c = 0;
        else
            goodwill.SRCPartAmount2__c = p * goodwill.QuotWarrantyRate__c;
        
        try
        {
            update goodwill;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, System.label.FieldsSaved);
            ApexPages.addMessage(myMsg);
        } 
        catch (Exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.SaveFailed);
            ApexPages.addMessage(myMsg);
        }
    }
    
    public Integer getSRCParticipationRate()
    {
        System.debug('####### getSRCParticipationRate ####### - '+ goodwill.Country__c + ' - ' + goodwill.Brand__c + ' - ' + goodwill.Organ__c + ' - ' + Integer.valueOf(goodwill.VehicleAgeInMonth__c) + ' - ' + Integer.ValueOf(goodwill.Kilometer__c));
        //System.debug('####### Result AP02Goodwill.getGoodwillPercentage ####### '+AP02Goodwill.getGoodwillPercentage(goodwill.Country__c, goodwill.Brand__c, goodwill.Organ__c, Integer.valueOf(goodwill.VehicleAgeInMonth__c), Integer.ValueOf(goodwill.Kilometer__c)));
        return AP02Goodwill.getGoodwillPercentage(goodwill.Country__c, goodwill.Brand__c, goodwill.Organ__c, Integer.valueOf(goodwill.VehicleAgeInMonth__c), Integer.ValueOf(goodwill.Kilometer__c));
    }
    
    /**private Goodwill_Grid_line__c[] getGridRows(){
        Goodwill_Grid_line__c[] gw = [SELECT Row_Number__c, Mileage_Interval__c, Age_interval__c FROM Goodwill_Grid_Line__c WHERE Country__c = :goodwill.Country__c AND VehicleBrand__c = :goodwill.Brand__c AND Organ__c = :goodwill.Organ__c ORDER BY Row_Number__c];
        return gw;
    }
    
    private Integer getNoMileageColumns()
    {
        Integer mileageColumns = 0;
        Goodwill_Grid_line__c[] gw = [SELECT Row_Number__c, Max_Mileage__c, Mileage_interval__c FROM Goodwill_Grid_Line__c WHERE Country__c = :goodwill.Country__c AND VehicleBrand__c = :goodwill.Brand__c AND Organ__c = :goodwill.Organ__c];
        if (gw.size() > 0)
            mileageColumns = Integer.valueOf(gw.get(0).Max_Mileage__c / gw.get(0).Mileage_Interval__c)+1;
        
        return mileageColumns;  
    }
**/
    
    public SelectOption[] getselectListCountries(){
        SelectOption[] s = new SelectOption[]{};
            s.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Country__c.getDescribe().getPicklistValues()){
            s.add(new SelectOption(p.getValue(), p.getValue()));
        }
        
        return s;
    }

    public SelectOption[] getselectListOrgan(){
        SelectOption[] s = new SelectOption[]{};
        s.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry p : Goodwill_Grid_Line__c.Organ__c.getDescribe().getPicklistValues()){
            s.add(new SelectOption(p.getValue(), p.getValue()));
        }
        
        return s;
    }
   
}