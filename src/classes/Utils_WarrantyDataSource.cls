public with sharing class Utils_WarrantyDataSource {
 
    // Boolean variable used to specify if the dummy implementation must be used or the real one
    public Boolean Test;
      
    public WS06_servicesBcsDfr.ListDetWarrantyCheck getWarrantyData(VFC05_VehicleAttributes VehController) {     
         System.debug('#### ==> VehController : ' + VehController);
        // --- PRE TREATMENT ---- 
 
        String vin = VehController.getVin();        
        String countryCode = System.Label.CountryCode2;  //String countryCode = 'fr'   
        String date_x = System.Label.date;
        String myData = System.Label.MyDatePsw;      // String myData = 'AWINFO8:topaze11';
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(myData));
        
        // ---- WEB SERVICE CALLOUT -----    
        WS06_servicesBcsDfr.ApvGetInfoWarrantyCheck WarWS = new WS06_servicesBcsDfr.ApvGetInfoWarrantyCheck();
        WarWS.endpoint_x = System.label.VFP05_WarrantyDataURL;   
        WarWS.clientCertName_x = System.label.RenaultCertificate;      
        WarWS.inputHttpHeaders_x = new Map<String, String>();
        WarWS.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        WarWS.timeout_x=40000;
        
        WS06_servicesBcsDfr.ListDetWarrantyCheck WAR_WS = new WS06_servicesBcsDfr.ListDetWarrantyCheck();    
    
        if (Test==true) {
            WAR_WS = Utils_Stubs.WarrantyStub();   
        } else { 
            //WAR_WS = WarWS.GetInfoWarrantyCheck(vin, countryCode, date_x);
            WAR_WS = WarWS.GetInfoWarrantyCheck(vin, countryCode, date_x);
        }
         return WAR_WS;  
    }
       
}