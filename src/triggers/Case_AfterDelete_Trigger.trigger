/** 
 * @Project: RForce
 * @Name: Case_AfterDelete_Trigger
 * @Description: HQREQ 7929: CASE Sync CMDM - This Trigger is used for the Case_AfterDelete_Trigger. 
 * @Created By: RNTBCI
 * @Created Date: 29/06/2016
 **/
 trigger Case_AfterDelete_Trigger on Case (after delete) {
    Rforce_CaseCMDMSync_CLS.updateAccstopcomRC(Trigger.Old,Trigger.New ,'delete'); 
    System.Debug('### Rforce - Case_AfterInsert : BYPASS  - Run by ' + UserInfo.getName());


}