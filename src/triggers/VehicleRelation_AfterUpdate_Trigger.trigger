/*****************************************************************************************
    Name    : VehicleRelation_AfterInsert_Trigger
    Desc    : Update Account as Old Customer or Customer based on Vehicle Relation Status
    Approach: 
    Author  : Sumanth (RNTBCI)
    Project : Rforce
******************************************************************************************/
trigger VehicleRelation_AfterUpdate_Trigger on VRE_VehRel__c (after Update) {
    if (TriggerAdministration.canTrigger('VehicleRelation.onAfterUpdate')) {   
        //VehicleRelationTriggerHandler.onAfterUpdate(trigger.new, Trigger.isUpdate, trigger.oldMap);
     }
    else {
        System.Debug('### Rforce - VehicleRelation_AfterInsert_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    } 
    
    if (TriggerAdministration.canTrigger('VehicleRelationAdmin.onAfterUpdate')) {   
        Myr_VehRelEndPreviousRelation_Cls.closePreviousUpdate(trigger.new, Trigger.isInsert, trigger.oldMap);    
    }else{
        System.Debug('### Rforce - VehicleRelationAdmin_AfterUpdate_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    }
    
    /* BEGIN Rforce QuickRabbit */
    if (TriggerAdministration.canTrigger('VehicleRelationAdmin.onAfterUpdate')) {   
        VehicleRelationAdminTriggerHandler.onAfterUpdate(trigger.new, Trigger.isInsert, trigger.oldMap);
    } else {
        System.Debug('### Rforce - VehicleRelationAdmin_AfterUpdate_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    } 
    /* END Rforce QuickRabbit */ 
    
    if(CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c && TriggerAdministration.canTrigger('VehicleRelation.SynchroATC')) {   
        Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Update(trigger.new, Trigger.isUpdate, trigger.oldMap); 
    }else{
        System.Debug('### MyRenault - VehicleRelation_AfterUpdate_Trigger : No shall pass ! because of CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c  : <' + CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c  + '>, TriggerAdministration.canTrigger(VehicleRelation.SynchroATC) : <' + TriggerAdministration.canTrigger('VehicleRelation.SynchroATC') + '>');
    }
    //Customer messages "GAR_TRANSFERRED" & "GAR_CAR_AUTO_ADDED"
    Myr_CreateMessages_TriggerHandler_Cls notifier = new Myr_CreateMessages_TriggerHandler_Cls();
    notifier.manage_Garage_Notifications(trigger.new, trigger.oldMap);   
}