trigger GoodWill_Before_Insert_Update on Goodwill__c (before insert, before update) {
 /* Update the goodwill amount in function of the type of backup car and the number of days */

    for (Goodwill__c g : trigger.new) {

    	if(g.RecordTypeId=='012D0000000KBFBIA4')
    	{
    		
    	System.debug('<<<<<Handling CarRental Goodwill>>>>>');
         g.Total_Goodwill_Amount__c = VFC01_Goodwill.getGoodwillAmount(g);

    	}

    	else if(g.RecordTypeId=='012D0000000KBFGIA4' && g.Country__c != null
                && g.Brand__c != null
                && g.Organ__c != null
                && g.Kilometer__c != null
                && g.VehicleAgeInMonth__c != null)
    	{
    	System.debug('<<<<<Handling Technical Goodwill>>>>>');
    	  g.SRCPartRate__c=AP02Goodwill.getGoodwillPercentage(g.Country__c, g.Brand__c, g.Organ__c,Integer.valueOf(g.VehicleAgeInMonth__c), Integer.valueOf(g.Kilometer__c));
          
    	}
    }

}