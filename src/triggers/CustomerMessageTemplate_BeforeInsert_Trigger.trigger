/** Trigger on Customer Message Template
	Object:  Customer_Message_Templates__c
	Event: before insert
	Project: MyRenault
	
	@author S. Ducamp / Atos
	@date 10.07.2015
	@revision 1.0
	
	V1.0: first revision: check templates validity, set the name of the template
**/
trigger CustomerMessageTemplate_BeforeInsert_Trigger on Customer_Message_Templates__c (before insert) {

	//check template validity
	 if (TriggerAdministration.canTrigger('CustomerMessageTemplates.checkValidity')) {
        Myr_CustMessageTriggerHandler_Cls.checkTemplateValidity(trigger.new, null);
    } else {
        System.Debug('### MyRenault - CustomerMessageTemplate_BeforeInsert_Trigger : BYPASS checkValidity - Run by ' + UserInfo.getName());
    }
    
    //Template name
	Myr_CustMessageTriggerHandler_Cls.setNameOfCustomerMessageTemplate(trigger.new);
   	
}