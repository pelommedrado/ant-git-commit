trigger Task_BeforeInsert_Trigger on Task (before insert) {

User u = [Select Id, RecordDefaultCountry__c from User where id = : Userinfo.getUserId()];

    if (u.RecordDefaultCountry__c == 'Brazil'){
        For (Task task : Trigger.New){
            if(String.valueOf(task.WhatId).startsWith('500')){
                task.IsVisibleInSelfService = true;
            }
        }
    }
}