trigger TDE_TestDriveEmail_Trigger on TDE_TestDriveEmail__c (before insert) {
 
    Set<ID> ids = new Set<Id>();
    for (TDE_TestDriveEmail__c tde : trigger.new)
        ids.add(tde.Account__c);
    
    if(trigger.isBefore){
        Map<id, Account> accounts = new Map<id, Account>([select PersonContactId from Account where Id in :ids]);
    
        if(accounts != null && accounts.size() > 0){
            for (TDE_TestDriveEmail__c tde:Trigger.new){
                tde.Contact__c = accounts.get(tde.Account__c).PersonContactId;
            }
        }
    }

}