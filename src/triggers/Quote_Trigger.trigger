/**
* Trigger do objeto Quote.
* @author Felipe Jesus Silva.
*/
trigger Quote_Trigger on Quote (before update, after update, before insert, after insert) 
{
    if(Trigger.isInsert && Trigger.isAfter){
        new MonthlySellerQuoteExecution().createMonthlyGoalSellerRelation(Trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        VFC37_TriggerExecution triggerExecution = new VFC104_QuoteAfterUpdateExecution();
	    triggerExecution.execute(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
        new MonthlySellerQuoteExecution().createMonthlyGoalSellerRelation(Trigger.new);
        VFC81_QuoteBO.getInstance().updateLastStatusField(Trigger.newMap, Trigger.oldMap);
	    InterfaceSAP.execute(trigger.oldMap, trigger.new);
    }

    // adicionado isUpdate porque trigger não tinha before insert
    if(Trigger.isBefore && Trigger.isUpdate){
        VFC81_QuoteBO.getInstance().setExternalId(trigger.oldMap, trigger.new);
    }

    // para correção de erro de 101 querys, desativado workflow e passado para trigger
    if(Trigger.isBefore){
        VFC81_QuoteBO.getInstance().setDateTimePreOrderReceived(Trigger.new);
    }
}