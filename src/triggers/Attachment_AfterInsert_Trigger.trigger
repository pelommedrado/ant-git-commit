/*****************************************************************************************
    Name    : Attachment_AfterInsert_TRG
    Desc    : Attachment after insert 
    

******************************************************************************************/
trigger Attachment_AfterInsert_Trigger on Attachment (after insert) {
   if( TriggerAdministration.canTrigger( 'Attachment.onAfterInsert' ) ) {
         AttachmentTriggerHandler.onAfterInsert( Trigger.New );
   }else {
        System.Debug( '### Attachment_AfterInsert_TRG : BYPASS onBeforeDelete - Run by ' + UserInfo.getName() );
   } 
}