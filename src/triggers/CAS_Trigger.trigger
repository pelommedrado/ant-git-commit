trigger CAS_Trigger on Case (before insert,before update,after Insert,after Update) {

List<Goodwill__c> gwApproveList=new List<Goodwill__c>();
List<Goodwill__c> gwRefuseList=new List<Goodwill__c>();
List<Goodwill__c> goodWillApprove=new  List<Goodwill__c>(); 
List<Goodwill__c> goodWillRefuse =new List<Goodwill__c>();
list<case> ListCase= new list<case>();
String des='';
String finalDes='';
Integer chkIndex=0;
boolean disclaimer=false;
String []str = new String[]{'-- désistement','-- Disclaimer','-- Renúncia'};
List<case> caseUpdate =new List<case>();
List<contact> con=new List<contact>();
List<account> acc=new List<account>();
User usr=new User();
String partnerProfile = [select u.Profile.Name from User u where u.id =:Userinfo.getUserId()].Profile.Name; 
String usercoun=[Select RecordDefaultCountry__c from User where id=:Userinfo.getUserId()].RecordDefaultCountry__c; 
Id CusAccId;
Id CusConId;
Id countAcc;
List<Account> conflictsAcc=new List<Account>();
String cpfnumberInCase;
Account a;
List<case> updateCase=new List<case>();
long lMilliSecsInAHour = 60L * 60L * 1000L; 

if (Trigger.isBefore){ 
  
    if(Trigger.isInsert){
   
       for(Case cas:Trigger.new){
       try{
       
       
        if(cas.CPF_Web__c !=null){
          a=[select Id,Name,PersonContactId from Account where CustomerIdentificationNbr__c=:cas.CPF_Web__c limit 1];       
          System.debug('Account>>>>>>>'+a);
       
       }
       
       if(cas.CPF_Web__c ==null && cas.SuppliedEmail!=null){
           List<Case> cslist= new List<Case>();
           cslist.add(cas);
           Rforce_AccountCascadingSearch_CLS.searchAccount(cslist);
      // a=[select Id,Name,PersonContactId from Account where PersEmailAddress__c=:cas.SuppliedEmail limit 1];
         
       }
       
       if(a.Name!=null&&cas.Origin =='Renault Site'){

       System.debug('<<<<<<Web2Case>>>>>>');
       System.debug('cas.AccountId>>>>'+a.Id);
       System.debug(' cas.contactId>>>>>>'+a.PersonContactId);

        cas.AccountId=a.Id;
        cas.contactId=a.PersonContactId;
        }
                  
        }
        catch(Exception e){System.debug('case Error'+e);}
     }
     
  } // End of Insert

   if((Trigger.isInsert) || (Trigger.isUpdate)) { 
   

    for(Case cs:Trigger.new){
        if(cs.Description!=null)
        {
        des=cs.Description;
        for(Integer i = 0; i < str.size(); i++){
        if(des.indexOf(str[i])> 0 && chkIndex==0)
        {
        chkIndex=des.indexOf(str[i]);
        disclaimer=true;
        }
    }
    
    if(disclaimer==true && chkIndex > 0){  
        finalDes=des.substring(0,chkIndex);
        cs.Description=finalDes;
        ListCase.add(cs);
    }
    }
    //Populate VIN from WebtoCase Form - Reena J -27/05/2013
    
    if(cs.VIN_Web__c!=null){
    try{
    Id VINId= [select Id from VEH_Veh__c where Name =:cs.VIN_Web__c].id;
    cs.VIN__c=VINId; 
    
   
    }
    catch(Exception e){}
    }


    cs.SuppliedName=cs.FirstName_Web__c+' '+cs.LastName_Web__c; 

   }
  
    // This is to update the Dealer section from the partner portal & Community- Reena J
    for(Case caseRec: Trigger.New) { 
        if((partnerProfile=='Service Profile') || (partnerProfile=='CORE - Service Profile') || (partnerProfile=='BR - Network SRC')){    
        try{
        System.debug('UserID>>>>>>>>>>'+userinfo.getuserid());
        usr=[select ContactID,AccountId from User where Id=:userinfo.getuserid()];
        System.debug('AccountId>>>>>>>>>>'+usr.AccountId);

        caseRec.Dealer__c =usr.AccountId;
        }
        catch(Exception e){
            System.debug('Error in update dealer:'+e);
        }
        }          
    }  

     

       
       for(Case cas: Trigger.New) { 
              
        if(cas.SuppliedEmail!=null){
            
        try{
        
        conflictsAcc=[Select Id,Name,PersEmailAddress__c,PersMobPhone__c,Phone,BillingStreet,BillingCity,BillingState From account where id=:cas.AccountId]; 
      
       if(conflictsAcc.size()>0){
        if((conflictsAcc[0].phone==cas.SuppliedPhone) && (conflictsAcc[0].PersEmailAddress__c==cas.SuppliedEmail) && (conflictsAcc[0].BillingStreet==cas.Address_Web__c) && (conflictsAcc[0].BillingCity==cas.City_Web__c) && (conflictsAcc[0].BillingState==cas.State_Web__c)){
            
            cas.AccountConflicts_web__c=false;
                       
        }
        else{
            cas.AccountConflicts_web__c=true;
                     
        }
        }
        
        }
        catch(Exception e){}
        
        }
        }       
        
        
      
  } // End of update
 } // End of isBefore

// ---HQREQ-02316-----

if(Trigger.isBefore){
    if(Trigger.isUpdate){
             for (Case cas : Trigger.New) {
                if(cas.Status=='Resolved'){              
                  Datetime dtCreatedDate = cas.CreatedDate;
                  Integer diff =    dtCreatedDate.date().daysBetween(Date.today());
                  dtCreatedDate = dtCreatedDate.addDays(diff);
                  Decimal hrs = (Datetime.now().getTime() - dtCreatedDate.getTime())/1000/60/60 ;
                  Decimal mts =(Datetime.now().getTime() - dtCreatedDate.getTime())/1000/60;
                  Decimal actmts = mts-(hrs*60);
                  Decimal dtime=(diff*24);  
                  Decimal dmin=mts/60;               
                  Decimal totalhrs=dtime+hrs+dmin;
                  System.debug('tttttttttt'+totalhrs);
                  cas.REP_ResolutionTime__c=totalhrs-17;                          
                }
             }
    }    
}


// fire after Insert -Reena Jeyapal -27/05/2013-----
if (Trigger.isAfter) { 
     if (Trigger.isInsert) { 
        if (PAD.CanTrigger('AP01'))
        {
            list<Case> listeCase = new list<Case>();
            for (Case c : trigger.new)
            {
                if (c.VIN__c != null && c.Kilometer__c != null)
                listeCase.add(c);
            }
            if (listeCase.size() > 0)
                AP01Case.updateVehicle(listeCase);
        }
        //CAS : request REQ-00290
        List<Account> AccountList = new List<Account>();
        List<Account> AccountCase = new List<Account>();

        for (Case cas : Trigger.New) {
            if (cas.Type=='Complaint' && cas.AccountId != null) {
              AccountCase = [Select StopComRCFrom__c,StopComRCTo__c from Account where Id = : cas.AccountId];
              if (AccountCase.size() > 0) {
                for (Account Ac : AccountCase) {
                  Ac.StopComRCFrom__c = cas.CreatedDate;
                  Ac.StopComRCTo__c = null;
                  AccountList.add(Ac);
                }
              }
            }
        }
        update AccountList;

      }

      // fire after Update-Reena J-27/05/2013

      if (Trigger.isUpdate) { 
      
      
        if (PAD.CanTrigger('AP01'))
        {
            list<Case> listeCase = new list<Case>();
            
            for (Case c : trigger.new)
            {
                if (c.VIN__c != null 
                && c.Kilometer__c != null
                && (c.VIN__c != trigger.oldmap.get(c.Id).VIN__c
                || c.Kilometer__c != trigger.oldmap.get(c.Id).Kilometer__c))
                listeCase.add(c);
            }
            if (listeCase.size() > 0)
                AP01Case.updateVehicle(listeCase);
        }
        

          //This is to update Goodwill Status for the case - Reena J -27/05/2013

            RecordType[] recType=[Select Name,Id From RecordType where SobjectType='Goodwill__c'];
            
            
            
            if (!checkRecursive.hasAlreadyUpdated()) {
            for(Case caseRec: Trigger.New) {    
            if(caseRec.GWStatus__c=='Refused'){    
            goodWillRefuse = [select Id,GoodwillStatus__c,DeviationReason__c,SRCDecidedRate__c from Goodwill__c where Case__c=:caseRec.Id and RecordType.Name!='CAR RENTAL'];
            if(goodWillRefuse.size() > 0){
            for(Goodwill__c gwc:goodWillRefuse){
                gwc.GoodwillStatus__c = 'Refused' ;
                System.debug('Goodwill status to be updated to Rejected'+gwc); 
                gwRefuseList.add(gwc);
            }
            }
            System.debug('Size of the List to be Updated>>>>>>>'+gwApproveList.size());
            update gwRefuseList;    
            
            }
            if(caseRec.GWStatus__c=='Approved'){    
                goodWillApprove = [select Id,GoodwillStatus__c,DeviationReason__c,SRCDecidedRate__c from Goodwill__c where Case__c=:caseRec.Id and GoodwillStatus__c!='Refused' and RecordType.Name!='CAR RENTAL'];
                if(goodWillApprove.size() > 0){
                for(Goodwill__c gwc:goodWillApprove){     
                System.debug('Goodwill status to be updated to Approved'+gwc); 
                if((gwc.SRCDecidedRate__c!=null) && (gwc.DeviationReason__c==null)){
                caseRec.addError('You must select the Deviation Reason for all the Goodwill');
            }
            else{             
                gwc.GoodwillStatus__c = 'Approved' ;
                gwApproveList.add(gwc);
            }
            }
            }
                update gwApproveList;  
            }
            }
                checkRecursive.setAlreadyUpdated();
            }
            
            //CAS : request REQ-00290
            List<Account> AccountList = new List<Account>();
            List<Account> AccountCase = new List<Account>();

            for (Case cas : Trigger.New) {
                if (cas.Type=='Complaint' && cas.AccountId != null) {
                  AccountCase = [Select StopComRCFrom__c,StopComRCTo__c from Account where Id = : cas.AccountId];
                  if (AccountCase.size() > 0) {
                    for (Account Ac : AccountCase) {
                      if (cas.status=='Closed'){
                        Ac.StopComRCTo__c = Datetime.now();
                      }
                      else
                      {
                        Ac.StopComRCTo__c = null;
                      }
                      Ac.StopComRCFrom__c = cas.CreatedDate;
                      AccountList.add(Ac);
                    }
                  }
                }               
            }
            update AccountList;   
      }
 } // End of after Event
}