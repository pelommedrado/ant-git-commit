trigger EmailMessage_BeforeInsert_Trigger on EmailMessage (Before insert) {
	
	if (TriggerAdministration.canTrigger('EmailMessage.onBeforeInsert')) {
        EmailMessageTriggerHandler.onBeforeInsert(trigger.new);
    } else {
        System.Debug('### Rforce - EmailMessage_BeforeInsert_Trigger : BYPASS onBeforeInsert - Run by ' + UserInfo.getName());
    }
 }