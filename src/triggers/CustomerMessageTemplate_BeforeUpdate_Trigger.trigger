/** Trigger on Customer Message Template
	Object:  Customer_Message_Templates__c
	Event: before update
	Project: MyRenault
	
	@author S. Ducamp / Atos
	@date 10.07.2015
	@revision 1.0
	
	V1.0: first revision: check templates validity, set the name of the template
**/
trigger CustomerMessageTemplate_BeforeUpdate_Trigger on Customer_Message_Templates__c (before update) {

	//check template validity
	 if (TriggerAdministration.canTrigger('CustomerMessageTemplates.checkValidity')) {
        Myr_CustMessageTriggerHandler_Cls.checkTemplateValidity(trigger.new, trigger.oldMap);
    } else {
        System.Debug('### MyRenault - CustomerMessageTemplate_BeforeUpdate_Trigger : BYPASS checkValidity - Run by ' + UserInfo.getName());
    }
    
    //Set the name of the template
	Myr_CustMessageTriggerHandler_Cls.setNameOfCustomerMessageTemplate(trigger.new);
}