trigger CampaignMember_Trigger on CampaignMember (before insert, before delete)
{
	if (Trigger.isBefore)	
	{
		if (Trigger.isInsert)
		{
			// Check if the user profile are allowed to include campaing members
			VFC127_CampaignMemberBO.getInstance().checkIfProfileCanInsert(Trigger.New);
			
			// Check for a Web2toLead insertion (Lead and CampaignMember), we should convert
			// the Lead if the CM is associated do a Campaign that allows the convertion
			// and the Lead has a dealer associated
			 VFC127_CampaignMemberBO.getInstance().checkIfShouldConvertLeadIntoOpportunity(Trigger.New);
		}
		else if (Trigger.isDelete)
		{
			// Check if the user profile are allowed to delete campaing members
			VFC127_CampaignMemberBO.getInstance().checkIfProfileCanDelete(Trigger.Old);
		}
	}
}