trigger ALR_AttachmentTrigger on Attachment (after insert, after update, before insert, before update, before delete) {

    private final Map< String, String > extensionMap = new Map< String, String >{
        'CT-e' => 'pdf',
        'Caveat' => 'pdf',
        'Shipping Invoice' => 'pdf',
        'Photo Chassi/Marking on glass' => 'jpg;jpeg;png;bmp',
        'Photo distance' => 'jpg;jpeg;png;bmp',  
        'Photo approximate' =>  'jpg;jpeg;png;bmp',
        'Photo zoom flash aves' =>  'jpg;jpeg;png;bmp',
        'Invoice' =>  'pdf' 
    };
    
    if( (Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter ){

        Set<Id> idsDocs = new Set<Id>();
        List<Attachment> lsAttachment = new List<Attachment>();
        for(Attachment att : trigger.new){
            
            if(att.parentId.getSObjectType() == Schema.ALR_Document_Insurance__c.SObjectType){
               idsDocs.add(att.parentId);
               lsAttachment.add(att);
            }
        }
        //Check docs
        if(Trigger.IsInsert){
            
            if(idsDocs != null && !idsDocs.isEmpty()) {
                Utils_ALR_ActivateInsurance.getInstance().checkDocuments(lsAttachment);
                Utils_ALR_ActivateInsurance.getInstance().setIdAttToDamage(lsAttachment);
            }
        }
        
        //valida apenas um attachment por objeto ALR_Document_Insurance__c
    	Map<Id, Integer> countAtt = new Map<Id, Integer>();
        
        for(AggregateResult aggres : [select count(Id) quantidade, parentId from Attachment where parentId in: idsDocs 
                                      group by parentId]){
            countAtt.put((Id)aggres.get('parentId'), (Integer)aggres.get('quantidade'));
        }
        
        for(Attachment att : trigger.new){
            if(idsDocs.contains(att.ParentId) && countAtt.get(att.ParentId) > 1){
                att.addError('Documento já possui anexo.');
            }
        }
    }
    
     if( Trigger.isDelete && Trigger.isBefore ){
         
         
        Set< Id > insDocIdSet = new Set< Id >();
        for( Attachment att : Trigger.old ) 
        	if( att.ParentId.getSObjectType() == ALR_Document_Insurance__c.SObjectType ) insDocIdSet.add( att.ParentId );
       
        if(!insDocIdSet.isEmpty() && insDocIdSet != null)
          Utils_ALR_ActivateInsurance.deleteAttachment(Trigger.old);
     }

    if( (Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore ){
        Set< Id > insuranceDocIdSet = new Set< Id >();
        
        for( Attachment att : Trigger.new ) 
          if( att.ParentId.getSObjectType() == ALR_Document_Insurance__c.SObjectType ) insuranceDocIdSet.add( att.ParentId );
        
        if(insuranceDocIdSet != null && !insuranceDocIdSet.isEmpty()){
            Map< Id, ALR_Document_Insurance__c > insuranceDocMap = new Map< Id, ALR_Document_Insurance__c >( [
                select Id, ALR_Doc_Type__c from ALR_Document_Insurance__c where Id in :insuranceDocIdSet
            ] );
        
            if(insuranceDocMap != null){
                for( Attachment att : Trigger.new ){
                    ALR_Document_Insurance__c insuranceDoc = insuranceDocMap.get( att.ParentId );
                    if(insuranceDoc != null){
                        Set< String > validExtensions = extensionMap.containsKey( insuranceDoc.ALR_Doc_Type__c ) ?
                            new Set< String >( extensionMap.get( insuranceDoc.ALR_Doc_Type__c ).split( ';' ) ) :
                            new Set< String >{ 'pdf' };
            
                        if( !att.Name.contains( '.' ) )
                            att.addError( 'Arquivo sem extensão.' );
                        else if( !validExtensions.contains( att.Name.substringAfterLast( '.' ).toLowerCase() ) )
                            att.addError( 
                                'Extensão inválida. São somente permitidos arquivos do tipo ' + 
                                String.join( new List< String >( validExtensions ), ', ' ) + '.' 
                            );
                    }
                }
            }
        }
    }
}