/** Trigger on Customer Message Settings
	Object:  Customer_Message_Settings__c
	Event: before update
	Project: MyRenault
	
	@author S. Ducamp / Atos
	@date 10.07.2015
	@revision 1.0
	
	V1.0: first revision: check templates validity
**/
trigger CustomerMessageSettings_BeforeUpdate_Trigger on Customer_Message_Settings__c (before update) {

	//check settings validity
	 if (TriggerAdministration.canTrigger('CustomerMessageSettings.checkValidity')) {
        Myr_CustMessageTriggerHandler_Cls.checkSettingsValidity(trigger.new, trigger.oldMap);
    } else {
        System.Debug('### MyRenault - CustomerMessageSettings_BeforeUpdate_Trigger : BYPASS checkValidity - Run by ' + UserInfo.getName());
    }
}