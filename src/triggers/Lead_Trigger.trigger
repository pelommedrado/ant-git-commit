trigger Lead_Trigger on Lead (before insert, after update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        // Using BIR_Dealer_of_Interest__c, update the lookup field DealerOfInterest__c
        VFC131_LeadBO.getInstance().updateLookupDealerOfInterest(Trigger.New);
        
        // Update Molicar
        VFC131_LeadBO.getInstance().updateMolicar(Trigger.New);
        
        // Using VehicleOfInterest__c, update the lookup field VehicleOfInterestLookup__c
        VFC131_LeadBO.getInstance().updateLookupVehicleOfInterest(Trigger.New);
        
        // Using SecondVehicleOfInterest__c, update the lookup field SecondVehicleOfInterestLookup__c
        VFC131_LeadBO.getInstance().updateLookupSecondVehicleOfInterest(Trigger.New);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        
        ActivityUtils.createProspectionTaskForLead(Trigger.new, Trigger.oldMap);
        
    }
    
}