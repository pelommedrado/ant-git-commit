trigger ALR_ActivateInsuranceTrigger on ALR_Activate_Insurance__c (before insert, before update) {
   
     if(Trigger.IsInsert && trigger.IsBefore){
        //Utils_ALR_ActivateInsurance.getInstance().newActivateIncurance(trigger.new);
        //Utils_ALR_ActivateInsurance.getInstance().duplicidadeActivanteInsurance(trigger.new);
    }

    if(Trigger.IsUpdate && trigger.IsBefore){
        Utils_ALR_ActivateInsurance.getInstance().validaEnvioSeguradora(trigger.new, trigger.oldMap);
        
        String IdRoleAmericas = [SELECT Id FROM UserRole where Name = 'Americas'].Id;
        if(!UserInfo.getUserRoleId().trim().equals(IdRoleAmericas)){
            Utils_ALR_ActivateInsurance.getInstance().validaStatus(trigger.new, trigger.oldMap);
        }
    }
 }