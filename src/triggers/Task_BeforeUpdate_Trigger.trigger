trigger Task_BeforeUpdate_Trigger on Task (before update) {

User u = [Select Id, RecordDefaultCountry__c from User where id = : Userinfo.getUserId()];

    if (u.RecordDefaultCountry__c == 'Brazil'){
        For (Task task : Trigger.New){
            if(String.valueOf(task.WhatId).startsWith('500')){
                if (task.OwnerId != u.Id){
                    task.addError(Label.Error_message_Task_Brazil); 
                }
            }
        }
    }
}