/*****************************************************************************************
    Name    : Account_AfterUpdate_Trigger
    Desc    : This is to update Account country and language fields based on user country details
    Approach: Used Custom settings to store country information
    Project : Rforce
    **************************************************************************************************
10 July 2015, Project=myRenault, Author=D. Veron (AtoS) : Synchronizations ATC
27 Oct  2015, Project=myRenault, Author=D. Veron (AtoS) : Customer message "DEALER_ACCOUNT_CREATOR"


******************************************************************************************/


trigger Account_AfterUpdate_Trigger on Account (after update) {

	if (TriggerAdministration.canTrigger('Account.onAfterUpdate')) {
        AccountTriggerHandler.onAfterUpdate(trigger.new, Trigger.isUpdate, trigger.oldMap);
    } else {
        System.Debug('### Rforce - Account_AfterUpdate_Trigger : BYPASS onBeforeUpdate - Run by ' + UserInfo.getName());
    }
	
	if( CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c && TriggerAdministration.canTrigger('Account.SynchroATC')) {
        Myr_Synchro_ATC_TriggerHandler.On_Account_After_Update(trigger.new, Trigger.isUpdate, trigger.oldMap);
    }else{
        System.Debug('### MyRenault - Account_AfterUpdate_Trigger : No shall pass ! because of CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c  : <' + CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c  + '>, TriggerAdministration.canTrigger(Account.SynchroATC) : <' + TriggerAdministration.canTrigger('Account.SynchroATC') + '>, System.isfuture() : <' + !System.isfuture() + '>, User : <' + UserInfo.getName() + '>');
    }

    //Customer message "DEALER_ACCOUNT_CREATOR"
    Myr_CreateMessages_TriggerHandler_Cls notifier = new Myr_CreateMessages_TriggerHandler_Cls();
    notifier.manage_Dealer(trigger.new, trigger.oldMap);

	if( TriggerAdministration.canTrigger('Account.SynchroAC') && Myr_AdobeCampain.Is_Org_AC_Synchronizable() ) {
		Myr_AdobeCampain_TriggerHandler.onAccountAfterUpdate( trigger.new, trigger.oldMap );
	} else {
		system.debug('### MyRenault - Account_AfterUpdate_Trigger: No AdobeCampain synchro because : Org activation=<' + CS04_MYR_Settings__c.getInstance().Myr_AC_Activated__c + '>, Bypass Account.SynchroAC=<' + TriggerAdministration.canTrigger('Account.SynchroAC') + '>');
	}
}