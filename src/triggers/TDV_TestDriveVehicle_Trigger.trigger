/**
*	Trigger	-	TDV_TestDriveVehicle_Trigger
*	Author	-	RameshPrabu
*	Date	-	15/11/2012
*
*	#01 <RameshPrabu> <15/11/2012>
*		This trigger called before insert and update for Test Drive vehicle object
**/
trigger TDV_TestDriveVehicle_Trigger on TDV_TestDriveVehicle__c (before insert, before update) {
	if( Trigger.isBefore ){
		if( Trigger.isInsert ){
			
			// Validate the vehicle can only be inserted (or update) for test drive in a given date interval for only one Account if the check Available is true. 
			VFC31_TestDriveVehicleBO.getInstance().validateNewTestDriveVehicle( Trigger.New );
		}
		
		if( Trigger.isUpdate ){
			
			// Validate the vehicle can only be inserted (or update) for test drive in a given date interval for only one Account if the check Available is true.
			VFC31_TestDriveVehicleBO.getInstance().validateEditTestDriveVehicle( Trigger.New, Trigger.old );
		}
	}
}