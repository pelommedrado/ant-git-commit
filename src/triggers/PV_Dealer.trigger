trigger PV_Dealer on Dealer_Offer__c (before delete, after update) {
  if(Trigger.isBefore)
        if(Trigger.isDelete)
            for(Dealer_Offer__c dealer: Trigger.old){
                VFC154_UpdateDateOffers.DealerUpdateOffer(dealer);
                    break;
                }
  if(Trigger.isAfter)
        if(Trigger.isUpdate)
            for(Dealer_Offer__c dealer: Trigger.new){
                VFC154_UpdateDateOffers.DealerUpdateOffer(dealer);
                 break;
             }
  
}