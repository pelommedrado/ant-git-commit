/**
*  Trigger  -  TDV_TestDrive_Trigger
*  Author  -  RameshPrabu
*  Date  -  15/11/2012
*
*  #01 <RameshPrabu> <15/11/2012>
*    This trigger called before insert and update for Test Drive object
**/
trigger TDV_TestDrive_Trigger on TDV_TestDrive__c (before insert, before update) {

 Set<ID> ids = new Set<Id>();
 for (TDV_TestDrive__c tdv : trigger.new){   
     
  if( Trigger.isBefore ){
    ids.add(tdv.Account__c);
    if( Trigger.isInsert ){
      // Validate  same Test Drive Vehicle at a given time more than one entry and Check if the field Dealer is the same account of the Test Drive Vehicle
      VFC30_TestDriveBO.getInstance().validateNewTestDrive( Trigger.New );
    }
    
    if( Trigger.isUpdate ){
      
      // Validate  same Test Drive Vehicle at a given time more than one entry and Check if the field Dealer is the same account of the Test Drive Vehicle
      VFC30_TestDriveBO.getInstance().validateEditTestDrive( Trigger.New, Trigger.old );
    }
  }
 } 
   if(trigger.isBefore){
        Map<id, Account> accounts = new Map<id, Account>([select PersonContactId from Account where Id in :ids]);
    
        if(accounts != null && accounts.size() > 0){
            for (TDV_TestDrive__c tdv:Trigger.new){
                tdv.Contact__c = accounts.get(tdv.Account__c).PersonContactId;
            }
        }
    }
}