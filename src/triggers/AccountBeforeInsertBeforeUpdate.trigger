/*****************************************************************************************
    Name    : AccountBeforeInsertBeforeUpdate
    Desc    : This is to update Account country and language fields based on user country details
    Approach: Used Custom settings to store country information
                
                                                
 Modification Log : 
---------------------------------------------------------------------------------------
Developer                       Date                Description
---------------------------------------------------------------------------------------
Hugo Medrado                    12/6/2012
Praveen Ravula                  10/17/2013           modified to update language field 

******************************************************************************************/


trigger AccountBeforeInsertBeforeUpdate on Account (before insert) 
{

    
    User u=[Select Id,RecordDefaultCountry__c from User Where Id =: UserInfo.getUserId()];  
    for (Account a : Trigger.new){
        a.Country__c = u.RecordDefaultCountry__c;
       //  this section will update language field of account object --10/17/2013
        try{
            
        //System.debug('Value being Updated in language field is >>>>>>>>>>'+Country_Info__c.getInstance(a.Country__c).Language__c);  
        
        /* 
        * Change done by @Hugo Medrado 
        * prevent access a null object
        */
        if(Country_Info__c.getInstance(a.Country__c) != null){ 
	        a.Language__c= Country_Info__c.getInstance(a.Country__c).Language__c;
	        a.Language__pc= Country_Info__c.getInstance(a.Country__c).Language__c;  
        }  
        
        }
        catch(Exception e ){
        System.debug('NullPointerException'+e.getmessage());
        }
     }
}