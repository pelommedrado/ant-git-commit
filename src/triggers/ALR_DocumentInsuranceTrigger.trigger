trigger ALR_DocumentInsuranceTrigger on ALR_Document_Insurance__c (before insert, before update, before delete) {
    
    if(trigger.IsBefore && trigger.IsInsert)
        Utils_ALR_ActivateInsurance.getInstance().validaDocumentInsurance(trigger.new, null);
    
    if(trigger.IsBefore && trigger.IsUpdate)
        Utils_ALR_ActivateInsurance.getInstance().validaDocumentInsurance(trigger.new, trigger.old);
    
    if(trigger.IsBefore && trigger.IsDelete) {
        Set< Id > parentIdSet = new Set< Id >();
        List<Attachment> attDeleteList = new List<Attachment>();
        
        for(ALR_Document_Insurance__c doc : trigger.old){
            parentIdSet.add(doc.Id);
        }
        
        if(!parentIdSet.isEmpty())
            attDeleteList = [select Id from Attachment where parentId in: parentIdSet];
        try{
            delete attDeleteList;
        }catch (Exception e){
            system.debug('**** ERROR Deletar Anexos do documento: '+e);
        }	
    }
}