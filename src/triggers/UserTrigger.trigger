trigger UserTrigger on User (before insert,after insert,before update,after update) {
    
    /* Procura por grupos publicos de concessionarias 
e cria um caso não exista. Após criar o grupo coloca o papel da
comunidade dentro do grupo - add by @ Edvaldo - [ kolekto ]
*/
    if(Trigger.isAfter){
        List<User> powerPartnerList = new List<User>();
        
        for(User usr: Trigger.new){
            if('PowerPartner'.equalsIgnoreCase(usr.UserType))
                powerPartnerList.add(usr);
        }
        
        if(!powerPartnerList.isEmpty())
            createDealerGroup.create(powerPartnerList);
    }
    
    /*
	 *  CRIA REGISTRO EM OBJETIVO DE VENDEDOR -- by @Lucas Andrade - [ kolekto ] - 08/09/2016
	 * 
	 * 
    
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            
            Set<Id> dealers = new Set<Id>();
            Set<Id> sellers = new Set<Id>();
            Set<Id> profilesId = new Set<Id>();
            
            List<Profile> profiles = [SELECT Id, UserLicense.Name
                                      FROM Profile
                                      WHERE UserLicense.Name = 'Gold Partner'];
            
            for(Profile p : profiles){
                profilesId.add(p.Id);
            }
            
            for(User u : Trigger.new){
                if(profilesId.contains(u.ProfileId)){
                    dealers.add(u.AccountId);
                    sellers.add(u.id);
                }
            }
            
            MonthlyGoalSeller_Controller.createFromUser(dealers, sellers);
        }
    } */
    
}