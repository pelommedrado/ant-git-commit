trigger GoodwillAfterUpdate on Goodwill__c (after update) 
{
	if (PAD.CanTrigger('AP03'))
	{
		list<Id> ListCaseId = new list<Id>();
		
		for (Goodwill__c G : trigger.new)
		{
			if(G.GlobalPartAmount__c!=null)
		    	ListCaseId.add(G.Case__c); 
		}
		
		if(ListCaseId.size()>0) 
			AP03UpdateCase.updateCase(ListCaseId);
	}
}