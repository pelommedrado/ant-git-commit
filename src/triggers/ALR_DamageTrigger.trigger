trigger ALR_DamageTrigger on ALR_Damage__c (before insert, after insert, before update) {
	
    if(Trigger.IsInsert && trigger.IsBefore)
    	Utils_ALR_ActivateInsurance.getInstance().newDamage(trigger.new);
    
    if(Trigger.IsUpdate && trigger.IsBefore)   
    	Utils_ALR_ActivateInsurance.getInstance().validaDamage(trigger.new, trigger.oldMap);
    
}