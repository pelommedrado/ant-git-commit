trigger ContactBeforeInsertBeforeUpdate on Contact (before insert, before update) 
{

 if(StopRecursiveTrigger.hasAlreadyDone())
      return;
 else{
    StopRecursiveTrigger.setAlreadyDone();


  User u=[Select Id,RecordDefaultCountry__c from User Where Id =: UserInfo.getUserId()];  
    

      for (Contact con : Trigger.new){
      
        con.Country__c = u.RecordDefaultCountry__c;
         try{  	
	        /* 
	        * Change done by @Hugo Medrado 
	        * prevent access a null object
	        */
         	if(Country_Info__c.getInstance(con.Country__c) != null){
        		con.Language__c= Country_Info__c.getInstance(con.Country__c).Language__c;
         	}
        }
        catch(Exception e ){
        System.debug('NullPointerException'+e.getmessage());
        }
       
      }
      
      }
    
    if(trigger.isBefore) {
        List<User> dealerUser = [
            select AccountId
            from User
            where id = :UserInfo.getUserId()
            And IsPortalEnabled = true
        ];
        
        for(Contact con2 : Trigger.new) {
            if(!dealerUser.isEmpty() && dealerUser != null && con2.AccountId == null && con2.Available_to_Service__c != null)
                con2.AccountId = dealerUser.get(0).AccountId;
        }
    }
   
}