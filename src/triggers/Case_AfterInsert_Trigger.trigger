trigger Case_AfterInsert_Trigger on Case (after insert) {
    Rforce_CaseTriggerHandler_CLS.onAfterInsert(trigger.new);

    System.Debug('### Rforce - Case_AfterInsert : BYPASS  - Run by ' + UserInfo.getName());


}