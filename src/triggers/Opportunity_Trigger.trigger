/**
* Trigger do objeto Opportunity.
* @author Felipe Jesus Silva.
*/

trigger Opportunity_Trigger on Opportunity (after insert, after update, before insert, before update){

    if( Trigger.isAfter && Trigger.isUpdate ) {
        //lista de oportunidades perdidas
        List<Id> opportunityIdLost = new List<Id>();
        List<Opportunity> opptList = new List<Opportunity>();
        //Map<Id, List<String>> mapOpptFases = new Map<Id, List<String>>();

        for(Opportunity opp: Trigger.new) {
            //a oportunidade foi perdida?
            if(opp.StageName.equalsIgnoreCase('Lost')) {
                opportunityIdLost.add(opp.Id);
                opptList.add(opp);
                //mapOpptFases.put(opp.Id, fases);
            }
        }
        /*List<String> fases = new List<String>();
        fases.add('Confirm Test Drive');
        fases.add('Confirm Visit');
        fases.add('Contact');
        fases.add('Customer Service');
        fases.add('Delivery Confirmation');
        fases.add('Negotiation');
        fases.add('Perform Test Drive');
        fases.add('Perform Visit');
        fases.add('Persecution');
        fases.add('Pre-Order');
        fases.add('Prospection');
        fases.add('Rescue');*/
        if(!opptList.isEmpty()){
         ActivityManagerBo.executeFecharAtividades(opptList);
        }

        if( !opportunityIdLost.isEmpty() ) {
            //obter as cotacoes das oportunidades perdidas
            List<Quote> quoteList = [
                SELECT Id, Status, OpportunityId
                FROM Quote
                WHERE OpportunityId = :opportunityIdLost
            ];

            //cancelar todas as cotacoes
            for(Quote quote : quoteList) {
                quote.Status = 'Canceled';
            }

            update quoteList;
        }
        System.debug('Trigger.oldMap' + Trigger.oldMap);
        System.debug('Trigger.new' + Trigger.new);

        new ActivityManagerBo(Trigger.new, Trigger.oldMap).execUpdate();
        //ActivityUtils.manageActivitiesOpportunityUpdate(Trigger.new, Trigger.oldMap);
    }

    if( Trigger.isAfter || (Trigger.isBefore && Trigger.isInsert) ) {
        VFC37_TriggerExecution triggerExecution = null;
        String usrCountry = [select u.RecordDefaultCountry__c from User u where u.id = :Userinfo.getUserId()].RecordDefaultCountry__c;

        String RecName='';
        Set<ID> ids = new Set<Id>();
        for (Opportunity opp : trigger.new){
            if(opp.AccountId != null)
                ids.add(opp.AccountId);

            if(trigger.isBefore){

                if(usrCountry =='Brazil'){
                    if(trigger.isInsert)
                    {
                        triggerExecution = new VFC38_OpportunityAfterInsertExecution();
                    }
                    /* Se proprietario da opp é Renault do Brasil que é o mesmo proprietário do web-to-opportunity não cria tasks*/
                    else if (!UserInfo.getName().equals('Renault do Brasil') && !UserInfo.getName().equals('Renault do Brasil Rede'))
                    {
                        triggerExecution = new VFC39_OpportunityAfterUpdateExecution();
                    }

                    triggerExecution.execute(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);

                }}
        }

        if(trigger.isInsert){
            if(trigger.isBefore){
                Map<id, Account> accounts = new Map<id, Account >([select PersonContactId,VehicleInterest_BR__c from Account where Id in :ids]);

                if(accounts != null && accounts.size() > 0){
                    for (Opportunity o:Trigger.new){
                        if(o.AccountId != null){
                            Account acc = accounts.get(o.AccountId);
                            if(acc != null && acc.PersonContactId != null)
                                o.Contact__c = acc.PersonContactId;

                            if(acc != null && acc.VehicleInterest_BR__c != null)
                                o.Vehicle_Of_Interest__c = acc.VehicleInterest_BR__c;
                        }
                    }
                }
            }
        }
    }

    /* Nova regra de compartilhamento de Oportunidades por grupo e papeis do portal de concessionarias - @Edvaldo [ kolekto ] */
    if( Trigger.isAfter ) {
        OpportunitySharing.execute(Trigger.new, Trigger.oldMap);
        //Database.executeBatch( new OpportunitySharingBatch(Trigger.new));
    }

    /* Criação de registro TRANSACTION com mudança de fase - @Lucas Andrade [ kolekto ] */
    if( Trigger.isBefore && Trigger.isUpdate ) {
        System.debug('Entrou no before update');
        new OpportunityTransactionBo(Trigger.new, Trigger.oldMap).execUpdate();
        //OpportunityTransaction.create(Trigger.new, Trigger.oldMap);


        for(Opportunity oppt : Trigger.new){
            if('Lost'.equalsIgnoreCase(oppt.StageName))
                oppt.RescueOpportunity__c = true;
        }

    }
    /* Define data de expiração da oportunidade beaseado no business hours 'Sales' - @Lucas Andrade [ kolekto ] */
    if( Trigger.isBefore && Trigger.isInsert ) {
        //get expiration lmimit from custom label
        Long expirationLimit = Long.valueof(Label.OpportunityExpirationTime);

        //transform in mileseconds
        Long expirationHours = expirationLimit*60*60*1000;

        //get business hours
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'Sales'];

        //update records
        for(Opportunity opp : Trigger.new){

            Datetime expirationTime = BusinessHours.add(bh.Id, System.now(), expirationHours);
            opp.Expiration_Time__c = expirationTime;

        }

        ActivityUtils.setChaseTaskTime(Trigger.new);
    }

    if( Trigger.isAfter && Trigger.isInsert ) {
        new OpportunityTransactionBo(Trigger.new, new Map<Id, Opportunity>()).execCreate();
        new ActivityManagerBo(Trigger.new, Trigger.oldMap).execInsert();
        //ActivityUtils.manageActivitiesOpportunityInsert(Trigger.new);
    }

}