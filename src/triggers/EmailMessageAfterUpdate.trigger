trigger EmailMessageAfterUpdate on EmailMessage (after insert) {

    for(EmailMessage e:Trigger.new)
    {
       Case c= [SELECT id,IsClosed,Status, Description,MailSentDate__c FROM Case WHERE Id = :e.ParentId LIMIT 1];
       System.Debug('Case Status is Closed>>>>>>>'+c.IsClosed);
       
       if(c.IsClosed !=true && c.MailSentDate__c== null)
       {
            c.MailSentDate__c = e.LastModifiedDate;
            update c;
       }
      
     }

}