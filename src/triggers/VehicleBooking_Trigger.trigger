/**
* Trigger do objeto VehicleBooking.
* @author Felipe Jesus Silva.
*/
trigger VehicleBooking_Trigger on VehicleBooking__c (before insert, after insert, after update) 
{
	VFC37_TriggerExecution triggerExecution = null;
	
	if(trigger.isBefore)
	{
		triggerExecution = new VFC95_VehicleBookBeforeInsertExecution();
	}
	else
	{
		if(trigger.isInsert)
		{
			triggerExecution = new VFC96_VehicleBookAfterInsertExecution();
		}
		else
		{
			triggerExecution = new VFC101_VehicleBookAfterUpdateExecution();
		}		
	}
	
	triggerExecution.execute(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
}