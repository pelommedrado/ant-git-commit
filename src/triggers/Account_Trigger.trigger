/*
*   Trigger - Account_Trigger
*   #01 <Suresh Babu> <20/10/2012>
*       Account Trigger that calls when insert or update Account.
*/
trigger Account_Trigger on Account (before Insert, before Update, after insert, after update) {
    //List<User> userDataLoaderList = [ SELECT Id FROM User WHERE Name LIKE '%Marcelo Dourado%' LIMIT 1];
    // Para origens LMT -- by @Edvaldo - [ kolekto ]
    if( Trigger.isInsert){
        LMT_OrigensAccount.convertLeadWithAccount(Trigger.new);
        
        //Para criação de objetivos de concessionárias -- by @Lucas Andrade - [ koleko ]
        if(Trigger.isAfter) {
            MonthlyGoalDealer_Controller.createFromAcc(Trigger.new);
        }
    }
    
    final Profile profile = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId()];
    //E o usuario Data Loader ?
    if(AccountDmdReceiveBo.setInterfaceProfiles.contains(profile.Name)) {
        if(Trigger.isInsert && Trigger.isBefore) {
            new AccountDmdReceiveBo().execInsert(Trigger.new);   
        }
        if(Trigger.isUpdate && Trigger.isBefore) {
            new AccountDmdReceiveBo().execUpdate(Trigger.new, Trigger.oldMap);
        }    
    }
    
    if( Trigger.isAfter ){
        if( Trigger.isInsert ){
            
            List<Case> caseRec=new List<Case>();
                        
            // To Attach the account for the WebTocase - Reena Jeyapal 21/08/2013
            
              for(Account ac:Trigger.new){
             
              if(ac.CaseNumber__c!=null){
                           
                  for(case cs:[select Id from case where caseNumber=:ac.CaseNumber__c and status!='Closed']){
                      cs.AccountId=ac.Id;
                      cs.ContactId=ac.PersonContactId;
                      caseRec.add(cs);
                  }
              }
             
             }
             try{
                 update caseRec;           
             }
             catch(Exception e){
                 system.debug(e);
             }
             
            if (TriggerAdministration.canTrigger('Account.SynchroAC') && Myr_AdobeCampain.Is_Org_AC_Synchronizable()){
                System.Debug('### Account_AfterInsert_Trigger.Myr_AdobeCampain_TriggerHandler.AC_Preregistration.trigger.new=' + trigger.new);
                
                List<Account> L_A= new List<Account>();
                for(Account a : trigger.new){
                    If(
                        (a.Myr_Status__c!=null && a.Myr_Status__c.equalsIgnoreCase(system.Label.Myr_Status_Preregistrered))
                        ||
                        (a.Myd_Status__c!=null && a.Myd_Status__c.equalsIgnoreCase(system.Label.Myd_Status_Preregistrered))
                        ){
                        L_A.add(a);
                    }
                }
                If(L_A.size()>0){
                    System.Debug('###Account_AfterUpdate_Trigger.Myr_AdobeCampain_TriggerHandler.AC_Preregistration('+L_A+')');
                    Myr_AdobeCampain_TriggerHandler.AC_Preregistration(L_A, null);
                }
        
            }else{
                System.Debug('### Account_AfterInsert_Trigger - No AdobeCampain synchro because : Org activation=<' + CS04_MYR_Settings__c.getInstance().Myr_AC_Activated__c + '>, Bypass Account.SynchroAC=<' + TriggerAdministration.canTrigger('Account.SynchroAC') + '>');
            }        
           
             
        }
        
        if( Trigger.isUpdate ){
            
            // Calculate update Account using new Account with old value
            //WSC02_CalculateLatAndLongParsing.validateUpdateAccount( Trigger.New, Trigger.old );
        }
    }
}