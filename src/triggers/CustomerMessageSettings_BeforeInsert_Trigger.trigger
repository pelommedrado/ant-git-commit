/** Trigger on Customer Message Settings
	Object:  Customer_Message_Settings__c
	Event: before insert
	Project: MyRenault
	
	@author S. Ducamp / Atos
	@date 10.07.2015
	@revision 1.0
	
	V1.0: first revision: check templates validity
**/
trigger CustomerMessageSettings_BeforeInsert_Trigger on Customer_Message_Settings__c (before insert) {

	//check settings validity
	 if (TriggerAdministration.canTrigger('CustomerMessageSettings.checkValidity')) {
        Myr_CustMessageTriggerHandler_Cls.checkSettingsValidity(trigger.new, null);
    } else {
        System.Debug('### MyRenault - CustomerMessageSettings_BeforeInsert_Trigger : BYPASS checkValidity - Run by ' + UserInfo.getName());
    }
    
}