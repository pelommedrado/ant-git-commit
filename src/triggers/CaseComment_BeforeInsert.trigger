trigger CaseComment_BeforeInsert on CaseComment (before insert) {


      if (StopRecursiveTrigger.hasAlreadyDone())
        return;
    else {
        StopRecursiveTrigger.setAlreadyDone();
        Rforce_CaseCommentTriggerHandler_CLS.onBeforeInsert(trigger.new);
        System.Debug('### Rforce - Case_AfterInsert : BYPASS  - Run by ' + UserInfo.getName());
    }


}