trigger PV_Offer on Offer__c (before insert, before update, after update, after insert) {
   
    if(Trigger.isBefore){
        if(Trigger.isInsert){ 
          for(Offer__c offer: Trigger.new){
               VFC149_OfferUtil.CorrectionFieldWhenOfferEqualsInternet(offer);
              if(offer.Monthly_Tax__c != 0 && offer.Monthly_Tax__c != null){
                offer.OFF_IOF__c = VFC149_OfferUtil.calculoIof(offer);
            }
          }
          
          System.debug('################# before insert');
        }
        if(Trigger.isInsert || Trigger.isUpdate){
            for( Offer__c offer : Trigger.new ){
               //offer.OFF_IOF__c = VFC149_OfferUtil.calculoIof(offer);
                if(offer.Condition__c == 'Financiado'){
                    offer.OFF_IOF__c = VFC149_OfferUtil.calculoIof(offer);
                    double cetAM = /*0.0;*/VFC149_OfferUtil.calculaCETAM(offer); 
                    System.debug('calculaCETAA '+VFC149_OfferUtil.calculaCETAA(offer, (VFC149_OfferUtil.calculaCETAM(offer))));
                    double cetAA = /*0.0;*/VFC149_OfferUtil.calculaCETAA(offer, (VFC149_OfferUtil.calculaCETAM(offer)));
                    offer.CET_AM__c = (Decimal)cetAM;
                    offer.CET_AA__c = (Decimal)cetAA;
                }else{
                    offer.CET_AM__c = 0.0;
                    offer.CET_AA__c = 0.0;
                }
               
            }
        }
    }
    if(Trigger.isAfter){    
        if(Trigger.isUpdate){
            //for(Offer__c offer: Trigger.new){
              //  if(offer.isUpgrade__c!=null){
                    VFC149_OfferUtil ofert = new VFC149_OfferUtil ();
                    //ofert.touchOfertaUpgrade(offer);
                   ofert.touchOfertaUpgradeOferta(Trigger.new);
               // }    
            //}
           
          System.debug('################# after update');
        }
        if(Trigger.isInsert ){ 
           // for(Offer__c oferta : Trigger.new){
               VFC151_OfferUpgrade.mudarId(Trigger.new);
            
            //}
            
            System.debug('################# after insert');
        }     
   }  
}