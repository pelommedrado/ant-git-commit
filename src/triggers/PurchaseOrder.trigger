trigger PurchaseOrder on PassingAndPurchaseOrder__c (before update, after update, after Insert, before Insert) {
    
   /* if(trigger.isBefore){  
        
        String userId = UserInfo.getUserId();
        List<User> dealerUser = [select AccountId from User where id =: userId And IsPortalEnabled = true];
        
        for(PassingAndPurchaseOrder__c po: trigger.new){
            
            /* inicio de workflows 
            
            if(po.Status__c.Equals('Sent')){
                po.Edit_Retroactive__c = false;
                po.Sent__c = true;
                if( po.First_Sent_Date__c == null )
                    po.First_Sent_Date__c = system.now();
            }
            if(po.Status__c.Equals('Draft'))
                po.Sent__c = false;
            if(po.Edit_Retroactive__c)
                po.Status__c = 'Draft';
            
            /* fim de workfows 
            
            if(!dealerUser.isEmpty() && dealerUser != null && po.Account__c == null)
                po.Account__c = dealerUser.get(0).AccountId;
            
            if(po.Feirao__c)
                po.PPO_External_ID__c = po.BIR__c + po.Date__c.year() + po.Date__c.month() + po.Date__c.day() + 'F';
            else
                po.PPO_External_ID__c = po.BIR__c + po.Date__c.year() + po.Date__c.month() + po.Date__c.day();
        }
    }
    
    if(trigger.isAfter){
        
        Map<String,DailyGoals__c> mapDailyGoalsIdAndDailyGoals = new Map<String,DailyGoals__c>();
        Map<String,Id> mapBirMatrizEIdMatriz = new Map<String,Id>();
        Map<String,List<Decimal>> mapMatrixAndSum = new Map<String,List<Decimal>>();
        Set<String> setDailyGoalsId = new Set<String>();
        Set<String> setBirMatriz = new Set<String>();
        Set<String> setPPOExternalId = new Set<String>();
        List<String> modeloVeiculo = new List<String>{'79M','B4M','B4S','B9S','CBM','FLM','KEM','L4M','M79','MTM'};
            
            for(PassingAndPurchaseOrder__c po: trigger.new){
                setBirMatriz.add(po.Matrix_BIR__c);
                for(Integer i=0; i < modeloVeiculo.size(); i++)
                    setDailyGoalsId.add(
                        String.valueOf(po.Date__c.year()) + 
                        String.valueOf(po.Date__c.month()) + 
                        String.valueOf(po.Date__c.day()) + 
                        modeloVeiculo[i] +
                        po.Matrix_BIR__c
                    );
            }
        
        List<Account> lsMatrizIds = [Select Id,IDBIR__c from Account where IDBIR__c in: setBirMatriz];  
        if(!lsMatrizIds.isEmpty()){
            for(Account a: lsMatrizIds)
                mapBirMatrizEIdMatriz.put(a.IDBIR__c,a.Id);
        }
        
        List<PassingAndPurchaseOrder__c> lspp = [Select PO_Duster_SUM__c,PO_Sandero_SUM__c,PO_B4S__c, PO_B9S__c, PO_Clio_SUM__c,
                                                 PO_Kangoo_SUM__c, PO_Fluence_SUM__c, PO_Logan_SUM__c, PO_Oroch__c, PO_Master_SUM__c,
                                                 Matrix_BIR__c from PassingAndPurchaseOrder__c where Matrix_BIR__c in: setBirMatriz];
        
        if(!lspp.isEmpty()){
            for(PassingAndPurchaseOrder__c p: trigger.new){
                
                Decimal Duster,Sandero,B4S,B9S,Clio,Kangoo,Fluence,Logan,Oroch,Master;
                Duster = Sandero = B4S = B9S = Clio = Kangoo = Fluence = Logan = Oroch = Master = 0;
                
                for(PassingAndPurchaseOrder__c lsp: lspp){
                    if(p.Matrix_BIR__c.Equals(lsp.Matrix_BIR__c)){
                        
                        Duster += (lsp.PO_Duster_SUM__c == null) ? 0 : lsp.PO_Duster_SUM__c;
                        Sandero += (lsp.PO_Sandero_SUM__c == null) ? 0 : lsp.PO_Sandero_SUM__c;
                        B4S += (lsp.PO_B4S__c == null) ? 0 : lsp.PO_B4S__c;
                        B9S += (lsp.PO_B9S__c == null) ? 0 : lsp.PO_B9S__c;
                        Clio += (lsp.PO_Clio_SUM__c == null) ? 0 : lsp.PO_Clio_SUM__c;
                        Kangoo += (lsp.PO_Kangoo_SUM__c == null) ? 0 : lsp.PO_Kangoo_SUM__c;
                        Fluence += (lsp.PO_Fluence_SUM__c == null) ? 0 : lsp.PO_Fluence_SUM__c;
                        system.debug('Fluence = '+Fluence);
                        Logan += (lsp.PO_Logan_SUM__c == null) ? 0 : lsp.PO_Logan_SUM__c;
                        Oroch += (lsp.PO_Oroch__c == null) ? 0 : lsp.PO_Oroch__c;
                        Master += (lsp.PO_Master_SUM__c == null) ? 0 : lsp.PO_Master_SUM__c;
                        
                    }
                    List<Decimal> somas = new List<Decimal>{Duster,Sandero,B4S,B9S,Clio,Kangoo,Fluence,Logan,Oroch,Master};
                        mapMatrixAndSum.put(lsp.Matrix_BIR__c,somas);
                }
            }
        }
        
        List<DailyGoals__c> dgList = [Select Id, Purchase_Order__c,Daily_Goals_ID__c
                                      from DailyGoals__c where Daily_Goals_ID__c in: setDailyGoalsId ];
        
        system.debug('dgList = '+dgList);
        
        if(!dgList.isEmpty()){
            for(DailyGoals__c dg: dgList)
                mapDailyGoalsIdAndDailyGoals.put(dg.Daily_Goals_ID__c,dg);
        }     
        
        for(PassingAndPurchaseOrder__c po: trigger.new){         
            
            if(po.Status__c.Equals('Sent')){
                
                for(Integer i=0; i < modeloVeiculo.size(); i++){
                    
                    List<Decimal> purchaseOrder = new List<Decimal>{po.PO_Duster_SUM__c,po.PO_Sandero_SUM__c,po.PO_B4S__c, po.PO_B9S__c, po.PO_Clio_SUM__c,
                        po.PO_Fluence_SUM__c, po.PO_Kangoo_SUM__c, po.PO_Logan_SUM__c, po.PO_Oroch__c, po.PO_Master_SUM__c};
                            
                            String chave = String.valueOf(po.Date__c.year()) + 
                            String.valueOf(po.Date__c.month()) + 
                            String.valueOf(po.Date__c.day()) + 
                            modeloVeiculo[i] +
                            po.Matrix_BIR__c;
                    
                    if(mapDailyGoalsIdAndDailyGoals.isEmpty() || mapDailyGoalsIdAndDailyGoals.get(chave).Id == null){
                        
                        DailyGoals__c dg = new DailyGoals__c(
                            Dealer_Matrix__c = mapBirMatrizEIdMatriz.get(po.Matrix_BIR__c),
                            Goal_Date__c = po.Date__c,
                            BIR_Number__c = po.Matrix_BIR__c,
                            Daily_Goals_ID__c = String.valueOf(po.Date__c.year()) + String.valueOf(po.Date__c.month()) +
                            String.valueOf(po.Date__c.day()) + modeloVeiculo[i] + po.Matrix_BIR__c,
                            Vehicle_Model__c = modeloVeiculo[i],
                            Goals_Number__c = 0,
                            Purchase_Order__c = (purchaseOrder[i] == null) ? 0 : purchaseOrder[i]
                        );
                        Insert dg;
                        
                    }else{
                        
                        system.debug('mapMatrixAndSum = '+mapMatrixAndSum);
                        
                        List<Decimal> sum = mapMatrixAndSum.get(po.Matrix_BIR__c);
                        
                        DailyGoals__c dg = new DailyGoals__c(
                            Id = mapDailyGoalsIdAndDailyGoals.get(chave).Id,
                            Purchase_Order__c = sum[i]
                        );
                        Update dg;
                    }
                }
            }
        }
    }*/
}