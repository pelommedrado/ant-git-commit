/*****************************************************************************************
 Name    : Vehicle_AfterUpdate_Trigger                        Creation date : 10 July 2015
 Desc    : Synchronizations ATC from Salesforce towards CSDB web services 
 Author  : Donatien Veron (AtoS)
 Project : myRenault
******************************************************************************************/
trigger Vehicle_AfterUpdate_Trigger on VEH_Veh__c (after Update) {
     if(CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c && TriggerAdministration.canTrigger('Vehicle.SynchroATC')) {
		Myr_Synchro_ATC_TriggerHandler.On_Vehicle_After_Update(trigger.new, Trigger.isUpdate, trigger.oldMap); 
	 } else {
	    System.Debug('### MyRenault - Vehicle_AfterUpdate_Trigger : BYPASS "ATC_Activated" and Vehicle.SynchroATC - Run by ' + UserInfo.getName());
     }
}