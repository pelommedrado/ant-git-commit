trigger GoodwillBeforeUpdate on Goodwill__c (before update) 
{
    if (PAD.CanTrigger('AP02'))
    {
        list<Goodwill__c> ListGoodwill = new list<Goodwill__c>();
    
        for (Goodwill__c g : trigger.new)
        {
            if (g.Country__c != null
                && g.Brand__c != null
                && g.Organ__c != null
                && g.Kilometer__c != null
                && g.VehicleAgeInMonth__c != null)
            listGoodwill.add(g); 
        }
        
        if (listGoodwill.size() > 0)
            AP02Goodwill.updateGoodwill(listGoodwill);
    }
}