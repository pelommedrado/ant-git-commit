trigger Campaign_Trigger on Campaign (before insert, before update, after update)
{
	if (Trigger.isBefore)	
	{
		if (Trigger.isInsert)
		{
			// This method will check, only for SingleCampaign recordType, some field values
			VFC122_CampaignBO.getInstance().validateSingleCampaign(Trigger.New);
			
			// Enforce that IsActive field is in sync with the Status field
			VFC122_CampaignBO.getInstance().enforceStatusIsActiveRules(Trigger.New);

			// Enforce that all children campaign will inherit some attributes values from their parents
			VFC122_CampaignBO.getInstance().enforceInheritance(Trigger.New);
		}
		else if (Trigger.isUpdate)
		{
			// This method will check, only for SingleCampaign recordType, some field values
			VFC122_CampaignBO.getInstance().validateSingleCampaign(Trigger.new);
			
			// Enforce that IsActive field is in sync with the Status field
			VFC122_CampaignBO.getInstance().enforceStatusIsActiveRules(Trigger.New);

			// Enforce that all children campaign will inherit some attributes values from their parents
			VFC122_CampaignBO.getInstance().enforceInheritance(Trigger.New);
		}
	}
	else if (Trigger.isAfter)	
	{
		if (Trigger.isUpdate)
		{
			// This method will check if a parent campaign was changed and if these changes
			// should be propagated to the children
			VFC122_CampaignBO.getInstance().checkParentCampaignChangesToForward(trigger.new, trigger.oldMap);
		}
	}
}