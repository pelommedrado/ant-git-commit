trigger PV_DealerGroup on Dealer_Group_Offer__c (before delete,after update) {
  if(Trigger.isBefore)
        if(Trigger.isDelete)
            for(Dealer_Group_Offer__c  dealer: Trigger.old)
                VFC154_UpdateDateOffers.DealerGroupUpdateOffer(dealer);
  if(Trigger.isAfter){
      if(Trigger.isUpdate){
          for(Dealer_Group_Offer__c  dealer: Trigger.new)
                VFC154_UpdateDateOffers.DealerGroupUpdateOffer(dealer);
      }
  }              
}