trigger DailyGoalsTrigger on DailyGoals__c (before update, before insert) {
    
    Set<String> setBirs = new Set<String>();
    Map<String,Id> mapAccIdBir = new Map<String,Id>();
    
    for(DailyGoals__c dg: trigger.new)
        setBirs.add(dg.BIR_Number__c);
    
    system.debug('setBirs = '+ setBirs);
    
    List<Account> accIdList = [Select Id, IDBIR__c from Account where IDBIR__c in: setBirs];
    
    system.debug('accIdList = '+ accIdList);
    
    if(!accIdList.isEmpty()){
        for(Account a: accIdList)
            mapAccIdBir.put(a.IDBIR__c,a.Id);
    }
    
    for(DailyGoals__c dg: trigger.new){
        
        dg.Dealer_Matrix__c = mapAccIdBir.get(dg.BIR_Number__c);
        
        String ano = String.valueOf(dg.Goal_Date__c.year());
        String mes = (dg.Goal_Date__c.month() < 10) ? '0' + String.valueOf(dg.Goal_Date__c.month()) : String.valueOf(dg.Goal_Date__c.month());
        String dia = (dg.Goal_Date__c.day() < 10) ? '0' + String.valueOf(dg.Goal_Date__c.day()) : String.valueOf(dg.Goal_Date__c.day());

        dg.Daily_Goals_ID__c = ano + mes + dia + dg.Vehicle_Model__c + dg.BIR_Number__c;
        
    }
}