/*****************************************************************************************
    Name    : CaseComment_AfterInsert_Trigger
    Desc    : This is to replicate the casecomment information in the IKR Specific Object when the comment is not created from ECare and publicly visible
    Approach:
    Project : Rforce
******************************************************************************************/


trigger CaseComment_AfterInsert_Trigger on CaseComment (after insert) {

    if (TriggerAdministration.canTrigger('CaseComment.onAfterInsert')) {
        CasecommentTriggerHandler.onAfterInsert(trigger.new, Trigger.isUpdate, trigger.oldMap);
    } else {
        System.Debug('### Rforce - CaseComment_AfterInsert_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    }
}