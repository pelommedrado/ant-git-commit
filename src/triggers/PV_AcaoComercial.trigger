trigger PV_AcaoComercial on PVCommercial_Action__c (before update,after update) {
    if(Trigger.isbefore){
        if(Trigger.isUpdate){
            List<PVCommercial_Action__c> comercialActionInactiveList = new List<PVCommercial_Action__c>();
            for (PVCommercial_Action__c acao : Trigger.new){
                  if(acao.Status__c.equals('Inactive')){
                      comercialActionInactiveList.add(acao);
                  }
            }
            if(!comercialActionInactiveList.isEmpty()){
                VFC153_InactiveOffers vf= new VFC153_InactiveOffers(comercialActionInactiveList);
                vf.inativarOfertaFactory();
				vf.inativarChamadaDeOfertaFactory();
            }
                 
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            
            VFC155_UpdateDateCallOffer vfcDate = new VFC155_UpdateDateCallOffer(Trigger.new, Trigger.oldMap);
            vfcDate.updateDateCallOffer();
            vfcDate.updateDateOffer();
            
            for(PVCommercial_Action__c  acao : Trigger.new){
                for(PVCommercial_Action__c  acaoAntiga :Trigger.old){

         
                    if(acao.Model__c != acaoAntiga.Model__c){
                        VFC155_UpdateDateCallOffer vfc155 = new VFC155_UpdateDateCallOffer();
                        vfc155.atualizaModelo(acao);
                    } 
                    //atualiza as versões da chamada de oferta
                    if(acao.Model__c != acaoAntiga.Model__c){
                        List<PVCall_Offer__c> listaCh = [select id, name,Model__c from PVCall_Offer__c where Commercial_Action__c =:acao.Id];
                        VFC156_PV_VersionCallOffer_Util version = new VFC156_PV_VersionCallOffer_Util();
                        version.insertVersionCallOffer(listaCh);
                    }
                }   
            }
            
        }
    }     
            
       
}