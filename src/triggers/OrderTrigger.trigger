trigger OrderTrigger on Order__c (before insert) {
    
    Set<String> matrixBirAcc = new Set<String>();
    Set<String> setDailyGoalsKey = new Set<String>();
    Map<String,Id> mapBirId = new Map<String,Id>();
    
    for(Order__c o: trigger.new){
        matrixBirAcc.add(o.Matrix_BIR__c);
        setDailyGoalsKey.add(o.DailyGoalKey__c);
    }
    
    Id dealerRecType = Utils.getRecordTypeId('Account', 'Network_Site_Acc');
    
    List<Account> accList = [Select Id, IDBIR__c from account where IDBIR__c in: matrixBirAcc and RecordTypeId =: dealerRecType limit 45000];
    
    system.debug('accList = '+ accList);
    
    if(!accList.isEmpty()){
        for(Account a: accList)
            mapBirId.put(a.IDBIR__c,a.Id);
    }
    
    system.debug('mapBirId = '+ mapBirId);
    
    List<DailyGoals__c> lsDaily = [Select Id, Daily_Goals_ID__c from DailyGoals__c where Daily_Goals_ID__c in: setDailyGoalsKey];
    
    system.debug('lsDaily = '+ lsDaily);
    
    if(!lsDaily.isEmpty()){
        
        Map<String,Id> mapDailyKey = new Map<String,Id>();
        for(DailyGoals__c dg: lsDaily){
            if(dg.Id != null)
            	mapDailyKey.put(dg.Daily_Goals_ID__c,dg.Id);
        }
        
        map<String,DailyGoals__c> mapDailyGoals = new Map<String,DailyGoals__c>();  
        for(Order__c o: trigger.new){
            
            system.debug('mapDailyKey = '+ mapDailyKey.get(o.DailyGoalKey__c));
            
            DailyGoals__c dg = new DailyGoals__c(
                Dealer_Matrix__c = mapBirId.get(o.Matrix_BIR__c),
                Vehicle_Model__c = o.Model__c,
                Id = mapDailyKey.get(o.DailyGoalKey__c),
                BIR_Number__c = o.Matrix_BIR__c,
                Goal_Date__c = o.Date__c, 
                Goals_Number__c = 0
            );
            mapDailyGoals.put(o.DailyGoalKey__c,dg);

        }
        
        Database.Upsert(mapDailyGoals.values(),false);
        
        system.debug('mapDailyGoals size = '+ mapDailyGoals.size());
        system.debug('mapDailyGoals = '+ mapDailyGoals);
        
        for(Order__c o: trigger.new)
            o.DailyGoals__c = mapDailyGoals.get(o.DailyGoalKey__c).Id;
        
    }else{
    
        map<String,DailyGoals__c> mapDailyGoals = new Map<String,DailyGoals__c>();

        for(Order__c o: trigger.new){
            
            DailyGoals__c dg = new DailyGoals__c(
                Dealer_Matrix__c = mapBirId.get(o.Matrix_BIR__c),
                Vehicle_Model__c = o.Model__c,
                BIR_Number__c = o.Matrix_BIR__c,
                Goal_Date__c = o.Date__c, 
                Goals_Number__c = 0
            );
            mapDailyGoals.put(o.DailyGoalKey__c,dg);
        }
        
        system.debug('mapDailyGoals size = '+ mapDailyGoals.size());
        system.debug('mapDailyGoals = '+ mapDailyGoals);
        
        Database.Insert(mapDailyGoals.values(),false);
        
        for(Order__c o: trigger.new){
            
            o.DailyGoals__c = mapDailyGoals.get(o.DailyGoalKey__c).Id;
            
        }
    }
}