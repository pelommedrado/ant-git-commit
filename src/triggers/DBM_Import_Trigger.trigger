trigger DBM_Import_Trigger on DBM_Import__c (after insert)
{
	if (trigger.isAfter)
	{
		if (trigger.isInsert)
		{
			// Handle data inserted in DBM_Import__c object, classifying it in Lead or Account
	 		// and applying some validations
			VFC106_DBM_Import_BO.getInstance().afterInsertOnDBMImport(trigger.new);
   	    }
	}
}