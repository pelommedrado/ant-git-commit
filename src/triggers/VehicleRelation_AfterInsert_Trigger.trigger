/*****************************************************************************************
    Name    : VehicleRelation_AfterInsert_Trigger
    Desc    : Update Account as Old Customer or Customer based on Vehicle Relation Status
    Approach:
    Author  : Sumanth (RNTBCI)
    Project : Rforce / MYR Helios
    ************************************************************************************************
    10 July 2015, Project=myRenault, Author=D. Veron (AtoS) : Synchronizations ATC
    27 Oct  2015, Project=myRenault, Author=D. Veron (AtoS) : Customer message "GAR_CAR_AUTO_ADDED"

******************************************************************************************/
trigger VehicleRelation_AfterInsert_Trigger on VRE_VehRel__c (after Insert) {
    if (TriggerAdministration.canTrigger('VehicleRelation.onAfterInsert')) {
         //VehicleRelationTriggerHandler.onAfterInsert(trigger.new, Trigger.isInsert, trigger.oldMap);
    }else{
        System.Debug('### Rforce - VehicleRelation_AfterInsert_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    }

    if (TriggerAdministration.canTrigger('VehicleRelationAdmin.onAfterInsert')) {
        Myr_VehRelEndPreviousRelation_Cls.closePrevious(trigger.new, Trigger.isInsert, trigger.oldMap);
    }else{
        System.Debug('### Rforce - VehicleRelationAdmin_AfterInsert_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    }

    if (TriggerAdministration.canTrigger('VehicleRelationAdmin.onAfterInsert')) {
        VehicleRelationAdminTriggerHandler.onAfterInsert(trigger.new, Trigger.isInsert, trigger.oldMap);
    }else{
        System.Debug('### Rforce - VehicleRelationAdmin_AfterInsert_Trigger : BYPASS onAfterUpdate - Run by ' + UserInfo.getName());
    }
    if(CS04_MYR_Settings__c.getInstance().Myr_ATC_Activated__c && TriggerAdministration.canTrigger('VehicleRelation.SynchroATC')) {
        Myr_Synchro_ATC_TriggerHandler.On_VehicleRelation_After_Insert(trigger.new);
    }else{
        System.Debug('### MyRenault - VehicleRelation_AfterInsert_Trigger : BYPASS "ATC_Activated" and VehicleRelation.SynchroATC - Run by ' + UserInfo.getName());
    }

      //GAR_CAR_AUTO_ADDED
    Myr_CreateMessages_TriggerHandler_Cls notifier = new Myr_CreateMessages_TriggerHandler_Cls();
    System.Debug('### MyRenault - VehicleRelation_AfterInsert_Trigger : GAR_CAR_AUTO_ADDED - BEGIN');
    notifier.manage_Garage_Notifications(trigger.new, null);
    System.Debug('### MyRenault - VehicleRelation_AfterInsert_Trigger : GAR_CAR_AUTO_ADDED - END');

//Synchro AdobeCampain
    if(TriggerAdministration.canTrigger('VehicleRelation.SynchroAC') && Myr_AdobeCampain.Is_Org_AC_Synchronizable()) {
        Myr_AdobeCampain_TriggerHandler.AC_NewCar(trigger.new, null);
    }else{
        System.Debug('### VehicleRelation_AfterInsert_Trigger - No AdobeCampain synchro because : Org activation=<' + Myr_AdobeCampain.Is_Org_AC_Synchronizable() + '>, Bypass Account.SynchroAC=<' + TriggerAdministration.canTrigger('Account.SynchroAC') + '>');
    }


//Synchro AdobeCampain
    if(TriggerAdministration.canTrigger('VehicleRelation.SynchroAC') && Myr_AdobeCampain.Is_Org_AC_Synchronizable()) {
        Myr_AdobeCampain_TriggerHandler.AC_NewCar(trigger.new, trigger.oldMap);
    }else{
        System.Debug('### VehicleRelation_AfterInsert_Trigger - No AdobeCampain synchro because : Org activation=<' + CS04_MYR_Settings__c.getInstance().Myr_AC_Activated__c + '>, Bypass Account.SynchroAC=<' + TriggerAdministration.canTrigger('Account.SynchroAC') + '>');
    }

    /* Para projeto integração com Distrinet.
    Quando é criado o relacionamento do veiculo com a concessionaria,
    busca pelo vbk que tenha uma cotação som status de veiculo alocado e
    envia esta cotação para o DMS.
    by Edvaldo [ kolekto ]
    */
    new IntegrationDistrinet(Trigger.new).execute();

}