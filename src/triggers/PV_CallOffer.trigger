trigger PV_CallOffer on PVCall_Offer__c (before delete,before update, before insert,after insert, after update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            System.debug('#### update Trigger.new '+ Trigger.new);
            List<PVCall_Offer__c> callOfferInactiveList = new List<PVCall_Offer__c>();
            
            for(PVCall_Offer__c  chamada : Trigger.new){
                
                if(String.isNotBlank(chamada.Status__c) && chamada.Status__c.equals('Inactive')){
                    callOfferInactiveList.add(chamada);
                }
                
                
                //if(chamada.Coefficient__c!=null && chamada.Coefficient__c>0 && chamada.Status__c.equals('Pending') &&
                 // String.isNotBlank(Trigger.oldMap.get(chamada.id).Status__c) && Trigger.oldMap.get(chamada.id).Status__c.equals('Pending'))
                  //  chamada.Status__c = 'Pending Analyst';
                if(String.isNotBlank(Trigger.oldMap.get(chamada.id).Status__c) && Trigger.oldMap.get(chamada.id).Coefficient__c==null && chamada.Coefficient__c!=null && chamada.Coefficient__c>0 && chamada.Status__c.equals('Pending'))
                    chamada.Status__c = 'Active';
            }
            if(!callOfferInactiveList.isEmpty()){
                VFC153_InactiveOffers vf= new VFC153_InactiveOffers(callOfferInactiveList);
                vf.inativarOfertaFactory();
                
            }
            
        }
        if((Trigger.isUpdate || Trigger.isInsert )&& Trigger.isBefore){
            //busca coeficiente
            PV_UtilsCallOffer callOfferUtils = new PV_UtilsCallOffer(Trigger.new, Trigger.oldMap);
            callOfferUtils.searchFinancingConditions();
        }
        if(Trigger.isInsert ){
            
            Set< Id > commercialActionIdSet = new Set< Id >();
            
            for( PVCall_Offer__c callOffer : Trigger.new )
                commercialActionIdSet.add( callOffer.Commercial_Action__c );
            
            Map< Id, PVCommercial_Action__c > commercialActionMap = new Map< Id, PVCommercial_Action__c >(
                [select Model__c from PVCommercial_Action__c where Id = :commercialActionIdSet]
            );
            
            for(PVCall_Offer__c callOffer : Trigger.new){
                PVCommercial_Action__c commercialAction = commercialActionMap.get( callOffer.Commercial_Action__c );
                if( commercialAction == null ) continue;
                callOffer.Model__c = commercialAction.Model__c;
            }
            
        }
        if(Trigger.isDelete){
            for(PVCall_Offer__c chamada : Trigger.old){
                if([select id from Offer__c where Call_Offer__c =: chamada.Id].size()>0)
                    chamada.addError('A chamada não pode ser excluida pois possui ofertas Relacionadas !');
            }
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            
            VFC147_ChamadaOfertaConcessionaria call = new VFC147_ChamadaOfertaConcessionaria();
            call.relacionarTodasCssForTrigger(Trigger.new);
            
            VFC148_ChamadaOfertaVersao versaoDealer= new VFC148_ChamadaOfertaVersao(); 
            versaoDealer.RelacionarOfertaSemFor(Trigger.new);  
            
            
        }
        
    }
    
    
    
}