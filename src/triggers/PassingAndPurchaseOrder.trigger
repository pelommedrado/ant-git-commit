trigger PassingAndPurchaseOrder on PassingAndPurchaseOrder__c (before Insert, before update, after insert, after update) {
    
    if(trigger.isBefore){  
        
        String userId = UserInfo.getUserId();
        List<User> dealerUser = [select AccountId from User where id =: userId And IsPortalEnabled = true];
        
        List<Account> accList;
        if(!dealerUser.isEmpty() && dealerUser != null)
            accList = [select IDBIR__c, NameZone__c from account where Id =: dealerUser.get(0).AccountId];
        
        for(PassingAndPurchaseOrder__c po: trigger.new){
            
            if(!dealerUser.isEmpty() && dealerUser != null && po.Account__c == null)
                po.Account__c = dealerUser.get(0).AccountId;
            
        }
    }
    
    if(trigger.isAfter){
        
        Set<Id> setPurchaseId = new Set<Id>();
        List<String> modeloVeiculo = new List<String>{'B4M','B4M RS','B4S','B9S','CBM','FLM','KEM','L4M','MTM','M79','79M'};
            
            for(PassingAndPurchaseOrder__c po: trigger.new){
                setPurchaseId.add(po.Id);    
            }
        
        system.debug('setPurchaseId = '+setPurchaseId);
        
        Map<Id,Id> mapPurchaseIdOrderId = new Map<Id,Id>();
        List<Order__c> lsOrder = [Select Id, Model__c, Purchase_Order__c from Order__c where Purchase_Order__c in: setPurchaseId];
        
        system.debug('lsOrder.size() = ' + lsOrder.size());
        
        for(Order__c o: lsOrder){
            mapPurchaseIdOrderId.put(o.Purchase_Order__c,o.Id);
        }
        
        system.debug('mapPurchaseIdOrderId' + mapPurchaseIdOrderId);
        
        if(!lsOrder.isEmpty()){ 
            List<Order__c> updateOrderList = new List<Order__c>();
            for(PassingAndPurchaseOrder__c po: trigger.new){
                if(mapPurchaseIdOrderId.get(po.Id) != null && po.Status__c.Equals('Sent')){                   
                    List<Decimal> amount = new List<Decimal>{po.PO_Sandero_SUM__c, po.PO_B4M_RS__c, po.PO_B4S__c,po.PO_B9S__c, po.PO_Clio_SUM__c, po.PO_Fluence_SUM__c,
                        po.PO_Kangoo_SUM__c,po.PO_Logan_SUM__c,po.PO_Master_SUM__c,po.PO_Oroch_SUM__c,po.PO_Duster_SUM__c};                 
                            for(Order__c o: lsOrder){
                                for(Integer i=0; i < modeloVeiculo.size(); i++){     
                                    if(modeloVeiculo[i].Equals(o.Model__c)){
                                        Order__c order = new Order__c(
                                            Id = o.Id,
                                            Amount__c = amount[i]
                                        );
                                        updateOrderList.add(order);
                                    }
                                }         
                            }
                }
            }
            Database.Update(updateOrderList,false);  
            
        }else{
            
            List<Order__c> insertOrderList = new List<Order__c>();
            for(PassingAndPurchaseOrder__c po: trigger.new){               
                if(po.Status__c.Equals('Sent')){
                    List<Decimal> amount = new List<Decimal>{po.PO_Sandero_SUM__c,po.PO_B4M_RS__c,po.PO_B4S__c,po.PO_B9S__c, po.PO_Clio_SUM__c, po.PO_Fluence_SUM__c,
                        po.PO_Kangoo_SUM__c,po.PO_Logan_SUM__c,po.PO_Master_SUM__c,po.PO_Oroch_SUM__c,po.PO_Duster_SUM__c}; 
                            for(Integer i=0; i < modeloVeiculo.size(); i++){
                                String ano = String.valueOf(po.Date__c.year());
                                String mes = (po.Date__c.month() < 10) ? '0' + String.valueOf(po.Date__c.month()) : String.valueOf(po.Date__c.month());
                                String dia = (po.Date__c.day() < 10) ? '0' + String.valueOf(po.Date__c.day()) : String.valueOf(po.Date__c.day());
                                
                                String orderkey = ano + mes + dia + modeloVeiculo[i] + po.BIR__c;
                                String dailyGoalKey = ano + mes + dia + modeloVeiculo[i] + po.Matrix_BIR__c;
                                
                                Order__c order = new Order__c(
                                    Order_Key__c = orderkey,
                                    Model__c = modeloVeiculo[i],
                                    Amount__c = amount[i],
                                    Matrix_BIR__c = po.Matrix_BIR__c,
                                    Date__c = po.Date__c,
                                    DailyGoalKey__c = dailyGoalKey,
                                    Purchase_Order__c = po.Id
                                );
                                insertOrderList.add(order);
                            }
                }
                
            }
            Database.Insert(insertOrderList,false);
        }
    }
}