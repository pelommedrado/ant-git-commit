trigger Approval on Engagement__c (after insert, after update) {
	
    for(Engagement__c e : trigger.new){
        if(e.Status__c == 'Sent'){
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval');
            req.setObjectId(e.Id);
            
            Approval.ProcessResult callProcess = Approval.Process(req);
        }
    }
    
    
}