trigger MonthlyGoalDelaer_Trigger on Monthly_Goal_Dealer__c (after insert) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            new MonthlyGoalSellerExecution().createFromGoalDealer(Trigger.new);
        }
    }

}