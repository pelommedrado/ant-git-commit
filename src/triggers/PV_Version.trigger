trigger PV_Version on PVVersion__c (before update) {
  for(PVVersion__c versaoAntiga: Trigger.old)
  for(PVVersion__c versaoNova: Trigger.new){
            VFC154_UpdateDateOffers ver = new VFC154_UpdateDateOffers ();
            ver.modelUpdateToOffer(versaoAntiga, versaoNova);
            
  }
}