/*****************************************************************************************
Name    : Case_BeforeInsert_Trigger
Desc    : This is to update contact country and language fields based on user country details
Approach: Used Custom settings to store country information
Project : Rforce

******************************************************************************************/

trigger Case_BeforeInsert_Trigger on Case (before insert) {
    
    Rforce_CaseCMDMSync_CLS.updateAccountStopComRCFromToCaseBeforeInsert(trigger.new);
    
    TriggerHandler_Case.beforeInsert(Trigger.new);  
    
    
    List<Case> listCasos = new List<Case>();
    for(Case casos : Trigger.new){
        for(cacweb_RecordTypeAll__c allRecordType: cacweb_RecordTypeAll__c.getAll().values()){
            if(casos.RecordTypeId != null && casos.RecordTypeId.equals(allRecordType.idRecordType__c)){
                listCasos.add(casos);
                break;
            }
        }
    }
    
    if(!listCasos.isEmpty()){
        
        List<Case> caseWhenRecordTypeIsRenaultRedeList = new List<Case>();
        List<Case> caseWhenRecordTypeIsNotEqualsRenaultRedeList = new List<Case>();
        
        
        Set<Id> cacWeb_RecordType_RenaultRedeIdSet = new Set<Id>();
        for(CacWeb_RecordType_RenaultRede__c custonSet : CacWeb_RecordType_RenaultRede__c.getAll().values()){
            cacWeb_RecordType_RenaultRedeIdSet.add(custonSet.RecordType_ID__c);
        }
        
        for(Case caso: listCasos)
            if(cacWeb_RecordType_RenaultRedeIdSet.contains(caso.RecordTypeId))
            caseWhenRecordTypeIsRenaultRedeList.add(caso);
        else
            caseWhenRecordTypeIsNotEqualsRenaultRedeList.add(caso);
        
        
        
        if(!caseWhenRecordTypeIsNotEqualsRenaultRedeList.isEmpty()){
            CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(caseWhenRecordTypeIsNotEqualsRenaultRedeList,Trigger.oldMap);
            caseUtils.updateField();
        }
        
        if(!caseWhenRecordTypeIsRenaultRedeList.isEmpty()){
            CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils2 = new CAC_CaseUtils_RecordTypeIsRenaultRede(caseWhenRecordTypeIsRenaultRedeList,Trigger.oldMap);
            caseUtils2.updateField();
            CAC_CaseUtils_RecordTypeIsRenaultRede.alterOwner(listCasos);
        }
        
        for(Case casea: listCasos){
            casea.Owner_CAC__c = casea.Owner.Name;
        }
    }
    
}