trigger VehicleTrigger on VEH_Veh__c (Before insert, Before update, After insert, After update) {

      if(Trigger.isBefore) {
        ALR_SharingTracking.replaceBirDealer(Trigger.new);
      }
      if(Trigger.isBefore && Trigger.isInsert) {
      }
      if(Trigger.isBefore && Trigger.isUpdate) {
        VehicleBO.getInstance().actionCancelAll(Trigger.new, Trigger.oldMap);
      }

      if(Trigger.isAfter && Trigger.isInsert) {
        if(VehicleBO.isInterfaceSAP(Trigger.new)) {
          ALR_SharingTracking tracking = new ALR_SharingTracking();
          tracking.insertTracking(Trigger.new);
          tracking.compartilhar();
        }
        VFC112_TestDriveVehicle_Import_BO.getInstance()
            .AfterInsertOn_TestDriveVehicle_Import(Trigger.new);
      }
      if(Trigger.isAfter && Trigger.isUpdate) {
        if(VehicleBO.isInterfaceSAP(Trigger.new)) {
            ALR_SharingTracking tracking = new ALR_SharingTracking();
            tracking.validarExisteTracking(Trigger.new);
            tracking.compartilharCasoConcessionariaTrocada(Trigger.new, Trigger.oldMap, Trigger.newMap);
            ALR_SharingTracking.deleteTracking(Trigger.new);
            ALR_SharingTracking.CaseStatusEqualsStock(Trigger.new, Trigger.oldMap);
            //ALR_SharingTracking.insertTrackingCaseUpdate(Trigger.new,Trigger.oldMap);
        }
      }
}