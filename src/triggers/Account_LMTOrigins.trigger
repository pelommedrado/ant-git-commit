trigger Account_LMTOrigins on Account (before insert,after insert) {
    
    /*

    //get Record Default Country of User
    User u = [Select Id,RecordDefaultCountry__c from User Where Id =: UserInfo.getUserId()];

    if( u.RecordDefaultCountry__c != null && u.RecordDefaultCountry__c.equals('Brazil') ){
        
        // recupera o tipo de registro de conta PF e PJ
        Id accountPF = Utils.getRecordTypeId('Account','Personal_Acc');
        Id accountPJ = Utils.getRecordTypeId('Account','Company_Acc');
        
        // recupera Id do proprietário de webToLead e emailToLead para não converte-los (A conversão destes é pela classe VFC131)
        Id webToLeadOwner = [select Id from User where Name = 'Renault do Brasil'].Id;
        Id emailToLeadOwner = [select Id from User where Name = 'Renault do Brasil Rede'].Id;
        
        //Lista de contas PF, PJ que não são criadas por webToOpportunity, emailServices e que não tenham um lead associado já convertido.
        List<Account> lsAccount = new List<Account>();
        
        for(account conta: trigger.new){
            if((conta.RecordTypeId.equals(accountPF) || conta.RecordTypeId.equals(accountPJ)) && !conta.OwnerId.equals(webToLeadOwner) &&
               !conta.OwnerId.equals(emailToLeadOwner) && conta.LeadConverted__c == false){
                   lsAccount.add(conta);
               }
        }

        //set para obter todos os CPFs da trigger
        Set<String> setCPF = new Set<String>();
        if(!lsAccount.isEmpty())
            setCPF = ObterCPF(lsAccount);
            
        //map para obter todos os leads mais antigos com base nos CPF's
        map<String,Id> mapLeads = new map<String,Id>();
        if(!setCPF.isEmpty())
            mapLeads = ObterLeadsExistentes(setCPF);
        
        //Lead a converter
        Lead lead = new Lead();
        
        //Mapeamento da conta com origens e conversão de lead
        for(account conta: lsAccount){
            
            Id leadId;
            if(!mapLeads.isEmpty())
                leadId = mapLeads.get(conta.CustomerIdentificationNbr__c);
            
            if(leadId != null){
                lead = VFC09_LeadDAO.getInstance().findLeadByLeadId(leadId);
                
                //Preenche as origens da conta com o Lead mais antigo na base.
                if(trigger.isBefore)
                    mappingAccountFromLead(conta,lead);
                
                //Converte o Lead.
                if(trigger.isAfter){
                    if(lead.IsConverted == false && lead.SFAConverted__c == false)
                        convertLead(conta.Id, lead);
                }
            }
        }
    }
    
    
    /************* métodos usados na trigger -- by @Edvaldo - kolekto *********************************
    
    
    private static void mappingAccountFromLead(Account a, Lead l){
        a.Source__c = l.LeadSource;
        a.AccSubSource__c = l.SubSource__c;
        a.Detail__c = l.Detail__c;
        a.Sub_Detail__c = l.Sub_Detail__c;
        if(l.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF'))){
            a.Cliente_DVE__c = true;
            if(l.Type_DVE__c != null && !l.Type_DVE__c.equals(''))
                a.Type_DVE__c = l.Type_DVE__c;
        }
    }
    
    private static void convertLead(Id accountId, Lead l){
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setAccountId(accountId);
        lc.setDoNotCreateOpportunity( true );
        lc.setLeadId(l.Id);
        if(l.RecordTypeId.equals(Utils.getRecordTypeId('Lead','Vendas_Empresa_PF')))
            lc.setConvertedStatus('Qualified');
        else
            lc.setConvertedStatus('Hot Lead');
        
        // if Lead owner is Queue, we need to set the Logged in User as Owner of the Account.
        if (l.Owner.type == 'Queue')
            lc.setOwnerId( Userinfo.getUserId() );
        
        Database.Leadconvertresult successLeadConversion = Database.convertLead(lc);
        Id idConta = successLeadConversion.getAccountId();
    }
    
    private static Map<String, Id> ObterLeadsExistentes(Set<String> CPF){
        Id serviceLead = Utils.getRecordTypeId('Lead', 'Service');
        List<Lead> lsLead = [select Id, CPF_CNPJ__c from lead where CPF_CNPJ__c in :CPF AND RecordTypeId !=: serviceLead order by CreatedDate desc];
        Map<String, Id> mapCpfLead = new Map<String, Id>();
        for(Lead l : lsLead)
            mapCpfLead.put(l.CPF_CNPJ__c, l.Id);
        return mapCpfLead;
    }
    
    private static Set<String> ObterCPF(List<Account> lsConta){
        Set<String> CPF = new Set<String>();
        for(Account c : lsConta) {
            if(c.CustomerIdentificationNbr__c != null)
                CPF.add(c.CustomerIdentificationNbr__c);
        }
        return CPF;
    }
    
    */
}