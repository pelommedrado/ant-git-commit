trigger Attachment_BeforeInsert on Attachment (before insert) {
    AttachmentTriggerHandler.onBeforeInsert(Trigger.New);
}