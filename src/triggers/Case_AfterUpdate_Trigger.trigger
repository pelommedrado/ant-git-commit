trigger Case_AfterUpdate_Trigger on Case (after update) {
    
    List<Case> casestosendmail = new List<Case>();
    List<Case> cases = Trigger.new;
    
    for (Case c : cases) {
        if (c.CountryCase__c == 'Colombia' && c.Dealer__c != null)
        {
            Case cOld = System.Trigger.oldMap.get(c.Id);
            if (c.Dealer__c != cOld.Dealer__c) {
                casestosendmail.add(c);
            }
        }
        
    }
    //if (TriggerAdministration.canTrigger('Case.onAfterUpdate')) {
        System.Debug('### Calling Rforce_CaseCMDMSync_CLS.updateAccstopcomRC  - Run by ' + UserInfo.getName());
        Rforce_CaseCMDMSync_CLS.updateAccstopcomRC(Trigger.Old,Trigger.New ,'update'); 
    //}
    Rforce_CaseTriggerHandler_CLS.onAfterUpdate(casestosendmail);
    System.Debug('### Rforce - Case_AfterUpdate : BYPASS  - Run by ' + UserInfo.getName());

}