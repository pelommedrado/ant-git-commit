trigger MonthlyGoalGroup_Trigger on Monthly_Goal_Group__c (after insert) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            MonthlyGoalGroup_Controller.createDealerSellerGoal(Trigger.new);
        }
    }

}