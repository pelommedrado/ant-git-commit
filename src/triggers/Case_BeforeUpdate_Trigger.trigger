trigger Case_BeforeUpdate_Trigger on Case (before update) {

    /* Rforce - Cria relação de posse com base em caso gerado pelo chat - @Edvaldo {kolekto}  */
    for(Case c: Trigger.new){
        if(c.Status != null && c.Status.equals('New') && 
           c.Origin != null && c.Origin.equals('RENAULT SITE') && 
           c.CaseSubSource__c != null && c.CaseSubSource__c.equals('Chat') && c.AccountId != null){
            newLiveagentcontroller chat = new newLiveagentcontroller();
            chat.createOwnerShipRelation(c);
        }
    }
    
    if(Trigger.isUpdate){
        
        List<Case> listCasos = new List<Case>();
        for(Case casos : Trigger.new){
            for(cacweb_RecordTypeAll__c allRecordType: cacweb_RecordTypeAll__c.getAll().values()){
                if(casos.RecordTypeId.equals(allRecordType.idRecordType__c)){
                    listCasos.add(casos);
                    break;
                }
            }
        }
        
        if(!listCasos.isEmpty()){
            List<Case> caseWhenRecordTypeIsRenaultRedeList = new List<Case>();
            List<Case> caseWhenRecordTypeIsNotEqualsRenaultRedeList = new List<Case>();
            
            
            Set<Id> cacWeb_RecordType_RenaultRedeIdSet = new Set<Id>();
            for(CacWeb_RecordType_RenaultRede__c custonSet : CacWeb_RecordType_RenaultRede__c.getAll().values()){
                cacWeb_RecordType_RenaultRedeIdSet.add(custonSet.RecordType_ID__c);
            }
            
            for(Case caso: listCasos)
                if(cacWeb_RecordType_RenaultRedeIdSet.contains(caso.RecordTypeId))
                caseWhenRecordTypeIsRenaultRedeList.add(caso);
            else
                caseWhenRecordTypeIsNotEqualsRenaultRedeList.add(caso);
            
            
            if(!caseWhenRecordTypeIsNotEqualsRenaultRedeList.isEmpty()){
                CAC_CaseUtilsNormaly caseUtils = new CAC_CaseUtilsNormaly(caseWhenRecordTypeIsNotEqualsRenaultRedeList,Trigger.oldMap);
                caseUtils.blockStatus();
                caseUtils.statusAnswers();
                caseUtils.updateField();
                caseUtils.statusAnswersWaiting();
                CAC_CaseUtils.alterOwner(caseWhenRecordTypeIsNotEqualsRenaultRedeList,Trigger.OldMap);
            }
            if(!caseWhenRecordTypeIsRenaultRedeList.isEmpty()){
                CAC_CaseUtils_RecordTypeIsRenaultRede caseUtils = new CAC_CaseUtils_RecordTypeIsRenaultRede(caseWhenRecordTypeIsRenaultRedeList,Trigger.oldMap);
                caseUtils.blockStatus();
                caseUtils.statusAnswers();
                caseUtils.updateField();
                caseUtils.statusAnswersWaiting();
            }
            
            
        }
    }
}