trigger PV_OfferItem on Offer_Item__c (after update) {

        if(Trigger.isAfter && Trigger.isUpdate) {
            List<Offer__c> offerList = new List<Offer__c>();

            for(Offer__c offer : [select Id, Featured_in_Campaign__c from Offer__c where Featured_in_Campaign__c in :Trigger.new])
                if(String.isNotBlank(offer.Featured_in_Campaign__c)){
                    Offer_Item__c offerItem = Trigger.newMap.get(offer.Featured_in_Campaign__c);
                    if(offerItem.Status__c == 'Active')
                        offer.Featured_in_Campaign_End__c = offerItem.End_Date__c;
                    else
                        offer.Featured_in_Campaign_End__c = null;
                        offer.Featured_in_Campaign__c = null;

                    offerList.add(offer);
                }

            if(!offerList.isEmpty()) Database.update(offerList);
        
        }
}