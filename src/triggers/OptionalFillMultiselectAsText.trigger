trigger OptionalFillMultiselectAsText on Optional__c (before insert, before update) {
  for( Optional__c optional : Trigger.new ){
    optional.Featured_in_Product_as_Text__c = optional.Featured_In_Product__c;
  }
}