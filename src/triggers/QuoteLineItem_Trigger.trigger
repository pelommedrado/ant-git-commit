/**
* Trigger do objeto QuoteLineItem.
* @author Felipe Jesus Silva.
*/
trigger QuoteLineItem_Trigger on QuoteLineItem (before insert, after delete) 
{
    VFC37_TriggerExecution triggerExecution = null;
    
    if(trigger.isBefore)
    {
        triggerExecution = new VFC103_QuoteLItemBeforeInsertExecution();
    }
    else
    {
        triggerExecution = new VFC102_QuoteLItemAfterDeleteExecution();
    }
    
    triggerExecution.execute(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
}