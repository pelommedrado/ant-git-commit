<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Featured_Picture_3_4_Frente</fullName>
        <field>FeaturedPicture__c</field>
        <literalValue>34</literalValue>
        <name>Featured Picture 3_4 Frente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Foto Destaque</fullName>
        <actions>
            <name>Featured_Picture_3_4_Frente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK(Text(FeaturedPicture__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
