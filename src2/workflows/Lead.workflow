<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Casa_Cor_Agradecimento_Simples</fullName>
        <description>LDD_Email_Alert_Casa_Cor_Simples</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Renault/Casa_Cor_Agradecimento_Simples</template>
    </alerts>
    <alerts>
        <fullName>Casa_Cor_Agradecimento_Vendedor</fullName>
        <description>LDD_Email_Alert_Casa_Cor_Vendedor</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Renault/Casa_Cor_Agradecimento_Vendedor</template>
    </alerts>
    <alerts>
        <fullName>Incorrect_CurrencyIsoCode_W2L</fullName>
        <description>Incorrect_CurrencyIsoCode_W2L</description>
        <protected>false</protected>
        <recipients>
            <recipient>BR_LMT</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/Web_to_lead_Incorreto</template>
    </alerts>
    <alerts>
        <fullName>LDD_Contact_unsuccessfully</fullName>
        <description>LDD_LMT_CLI_PLAT_NOT_SUC</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/LMT_Contato_sem_sucesso</template>
    </alerts>
    <alerts>
        <fullName>LDD_LMT_BIR_WEB</fullName>
        <ccEmails>renault.empresas@renault.com</ccEmails>
        <description>LDD_LMT_BIR_WEB</description>
        <protected>false</protected>
        <recipients>
            <recipient>marcelino.cunha-renexter@renault.com.logica</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/LDD_LMT_BIR_WEB</template>
    </alerts>
    <fieldUpdates>
        <fullName>CORE_LDD_FIELD_OWNERUPDATE</fullName>
        <field>OwnerId</field>
        <lookupValue>LEAD_CORE_QUEUE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CORE_LDD_FIELD_OWNERUPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateValueofCreditupdate</fullName>
        <field>Date_of_Value_of_Credit_update__c</field>
        <formula>TODAY()</formula>
        <name>DateValueofCreditupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UPDT_CleanValueOfCredit</fullName>
        <field>ValueOfCredit__c</field>
        <name>LDD_UPDT_CleanValueOfCredit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateAddressDate</fullName>
        <field>AddressDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdateAddressDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateAddressUpdated</fullName>
        <field>AddressUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdateAddressUpdated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateBirthdate</fullName>
        <field>Birthdate__c</field>
        <formula>DATE(
 
VALUE(RIGHT(Birthdate_Web_to_lead__c, 4))
,
 
VALUE(MID(Birthdate_Web_to_lead__c, 
  FIND(&quot;/&quot;,Birthdate_Web_to_lead__c) + 1,
    FIND(&quot;/&quot;,Birthdate_Web_to_lead__c, FIND(&quot;/&quot;,Birthdate_Web_to_lead__c) + 1) -
    FIND(&quot;/&quot;,Birthdate_Web_to_lead__c) - 1
))
,
 
VALUE(LEFT(Birthdate_Web_to_lead__c, 
  FIND(&quot;/&quot;,Birthdate_Web_to_lead__c) - 1))
 
)</formula>
        <name>LDD_UpdateBirthdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateBusinessMobilePhone</fullName>
        <field>BusinessMobilePhoneUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdateBusinessMobilePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateBusinessMobilePhoneDate</fullName>
        <field>BusinessMobilePhoneDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdateBusinessMobilePhoneDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateBusinessPhoneDateUpdate</fullName>
        <field>BusinessPhoneDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdateBusinessPhoneDateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateBusinessPhoneUpdated</fullName>
        <field>BusinessPhoneUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdateBusinessPhoneUpdated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateHomePhoneDate</fullName>
        <field>HomePhoneDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdateHomePhoneDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateHomePhoneUpdated</fullName>
        <field>HomePhoneUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdateHomePhoneUpdated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdatePersonalEmail</fullName>
        <field>PersonalEmailUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdatePersonalEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdatePersonalEmailDate</fullName>
        <field>PersonalEmailDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdatePersonalEmailDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdatePersonalMobilePhone</fullName>
        <field>PersonalMobilePhoneUpdated__c</field>
        <literalValue>Y</literalValue>
        <name>LDD_UpdatePersonalMobilePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdatePersonalMobilePhoneDate</fullName>
        <field>PersonalMobilePhoneDateUpdated__c</field>
        <formula>Today()</formula>
        <name>LDD_UpdatePersonalMobilePhoneDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_WF12_UpdateProfessionalEmail</fullName>
        <description>Updates the Email Profissional field with the Email field content.</description>
        <field>Professional_Email__c</field>
        <formula>Email</formula>
        <name>LDD_WF12_UpdateProfessionalEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_WF13_UpdateBusinessPhone</fullName>
        <field>BusinessPhone__c</field>
        <formula>Phone</formula>
        <name>LDD_WF13_UpdateBusinessPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_WF14_Update_Country</fullName>
        <field>Country__c</field>
        <literalValue>Brazil</literalValue>
        <name>LDD_WF14_Update_Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_WF14_Update_Pais</fullName>
        <field>Country</field>
        <formula>&quot;BR&quot;</formula>
        <name>LDD_WF14_Update_Pais</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_WF17_Update_Dealer_Of_Interest</fullName>
        <field>BIR_Dealer_of_Interest__c</field>
        <formula>DealerOfInterest__r.IDBIR__c</formula>
        <name>LDD_WF17_Update_Dealer_Of_Interest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Update_Home_Phone</fullName>
        <description>Update the field Home Phone based on the field of web to lead</description>
        <field>HomePhone__c</field>
        <formula>Home_Phone_Web__c</formula>
        <name>LMT_Update_Home_Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Update_Mobile_Phone</fullName>
        <description>Update the field MobilePhone based on the field of web to lead</description>
        <field>MobilePhone</field>
        <formula>Mobile_Phone__c</formula>
        <name>LMT_Update_Mobile_Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Populate_Standard_City</fullName>
        <field>City</field>
        <formula>City__c</formula>
        <name>Lead: Populate Standard &apos;City&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Populate_Standard_Country</fullName>
        <field>Country</field>
        <formula>TEXT(Country__c)</formula>
        <name>Lead: Populate Standard &apos;Country&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Populate_Standard_Postal_Code</fullName>
        <field>PostalCode</field>
        <formula>PostalCode__c</formula>
        <name>Lead: Populate Standard &apos;Postal Code&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Populate_Standard_State</fullName>
        <field>State</field>
        <formula>State__c</formula>
        <name>Lead: Populate Standard &apos;State&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Populate_Standard_Street</fullName>
        <field>Street</field>
        <formula>Street__c + 
IF( Number__c &lt;&gt; null, &quot;, &quot; + Number__c, &quot;&quot;) + 
IF( Complement__c &lt;&gt; null, &quot; - &quot; + Complement__c, &quot;&quot;) + 
IF( Neighborhood__c &lt;&gt; null, &quot; - &quot; + Neighborhood__c, &quot;&quot;)</formula>
        <name>Lead: Populate Standard &apos;Street&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recordtype_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LeadCoreRecType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Recordtype Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Last_Not_Interest</fullName>
        <field>Date_Last_Not_Interest__c</field>
        <formula>Today()</formula>
        <name>Update Date Last Not Interest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Value_of_Credit</fullName>
        <field>ValueOfCredit__c</field>
        <name>Update_Value_of_Credit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Zera_telefone</fullName>
        <field>Home_Phone_Web__c</field>
        <formula>&quot;123456789&quot;</formula>
        <name>Zera telefone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>zera_titulo</fullName>
        <field>Title</field>
        <formula>NULL</formula>
        <name>zera titulo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CORE_LDD_CreateTaskForCourier</fullName>
        <actions>
            <name>Send_Brochures</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Creates a Task to Send Brochure by Courrier</description>
        <formula>AND(ISCHANGED(OwnerId),ISPICKVAL(Broucher__c,&quot;By Courier&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CORE_LDD_WF_RECORDTYPE</fullName>
        <actions>
            <name>CORE_LDD_FIELD_OWNERUPDATE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Recordtype_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
            <value>BR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country__c</field>
            <operation>notEqual</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Assigns the Core Record Type to leads coming from Renault Site</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Core LDD CreateTaskForBrouchure</fullName>
        <actions>
            <name>Send_Brochure</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Creates a Task to Send Brochure by Email.</description>
        <formula>AND(ISCHANGED(OwnerId),ISPICKVAL(Broucher__c,&quot;By Email&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Core_LDD_FollowUp With Customer</fullName>
        <actions>
            <name>Contact_Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Creates a Task to contact customer</description>
        <formula>AND(ISCHANGED(OwnerId),(RecordTypeId=&apos;012c00000000Kd7&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_Home_Phone_WebToLead</fullName>
        <actions>
            <name>LMT_Update_Home_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>INTERNET</value>
        </criteriaItems>
        <description>Update the Home Phone field when a web to lead is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_LMT_BIR_WEB</fullName>
        <actions>
            <name>LDD_LMT_BIR_WEB</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
    OR(
       CONTAINS(Text(VehicleOfInterest__c), &quot;MASTER&quot;),
       CONTAINS(Text(VehicleOfInterest__c), &quot;KANGOO&quot;),
       CONTAINS(Text(VehicleOfInterest__c), &quot;Master&quot;),
       CONTAINS(Text(VehicleOfInterest__c), &quot;Kangoo&quot;)
    ),
    Text(LeadSource) = &quot;INTERNET&quot;,
    IsConverted = FALSE
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_LMT_CLI_PLAT_NOT_SUC</fullName>
        <actions>
            <name>LDD_Contact_unsuccessfully</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Brazilian LMT Contact unsuccessfully</description>
        <formula>AND ( ISPICKVAL(Status, &apos;Others&apos;),  Transactions_to_be_confirmed__c = 3)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_Mobile_Phone_WebToLead</fullName>
        <actions>
            <name>LMT_Update_Mobile_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>INTERNET,MARKETING</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>Update the Mobile Phone field when a web to lead is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_UpdateBirthdate</fullName>
        <actions>
            <name>LDD_UpdateBirthdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Birthdate_Web_to_lead__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF01_UpdateHomePhoneDate</fullName>
        <actions>
            <name>LDD_UpdateHomePhoneDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdateHomePhoneUpdated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When home phone  field changed, update Lead HomePhoneDateUpdated and HomePhoneUpdated Field.</description>
        <formula>ISCHANGED( HomePhone__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF02_UpdateEmailDate</fullName>
        <actions>
            <name>LDD_UpdatePersonalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdatePersonalEmailDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Email field changed, update Lead Personal Email Date Updated and Personal Email Updated Field.</description>
        <formula>ISCHANGED( Email )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF03_UpdateBusinessPhone</fullName>
        <actions>
            <name>LDD_UpdateBusinessPhoneDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdateBusinessPhoneUpdated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Business phone field changed, update Lead BusinessPhoneDateUpdated and BusinessPhoneUpdated Field.</description>
        <formula>ISCHANGED( BusinessPhone__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF04_UpdatePersonalMobilePhone</fullName>
        <actions>
            <name>LDD_UpdatePersonalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdatePersonalMobilePhoneDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Mobile phone field changed, update Lead PersonalMobilePhoneDateUpdated and PersonalMobilePhoneUpdated Field.</description>
        <formula>ISCHANGED( MobilePhone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF05_UpdateBusinessMobilePhone</fullName>
        <actions>
            <name>LDD_UpdateBusinessMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdateBusinessMobilePhoneDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Business Mobile phone field changed, update Lead BusinessMobilePhoneDateUpdated and BusinessMobilePhoneUpdated Field.</description>
        <formula>ISCHANGED(BusinessMobilePhone__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF06_UpdateAddressDate</fullName>
        <actions>
            <name>LDD_UpdateAddressDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_UpdateAddressUpdated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Address field changed, update Address Date and Address Updated Field.</description>
        <formula>OR(ISCHANGED( Street ),ISCHANGED(  City ) ,ISCHANGED(  State ) , ISCHANGED(  Country ) , ISCHANGED(  PostalCode ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF10_DateValueofCreditupdate</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.ValueOfCredit__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Date_of_Value_of_Credit_update__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Brasil</value>
        </criteriaItems>
        <description>When the Value of Credit field is changed, updates the field
Date_of_Value_of_Credit_update__c if this field &gt;90 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LDD_UPDT_CleanValueOfCredit</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Date_of_Value_of_Credit_update__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LDD_WF11_Incorrect_CurrencyIsoCode_W2L</fullName>
        <actions>
            <name>Incorrect_CurrencyIsoCode_W2L</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Renault do Brasil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CurrencyIsoCode</field>
            <operation>notEqual</operation>
            <value>BRL</value>
        </criteriaItems>
        <description>Send an email for administrators when a Web-to-lead is created with wrong CurrencyIsoCode.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF12_UpdateProfessionalEmail</fullName>
        <actions>
            <name>LDD_WF12_UpdateProfessionalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <description>Updates the Email Profissional field with the Email field content.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF13_UpdateBusinessPhone</fullName>
        <actions>
            <name>LDD_WF13_UpdateBusinessPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF14_Update_Country</fullName>
        <actions>
            <name>LDD_WF14_Update_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LDD_WF14_Update_Pais</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF15_Email_Casa_Cor</fullName>
        <actions>
            <name>Casa_Cor_Agradecimento_Simples</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
    Campaign.Id = &quot;70157000001R6OU&quot;,
    Text(IntentToPurchaseNewVehicle__c) &lt;&gt; &quot;Within next 6 months&quot;,
    OR(
       State = &quot;GO&quot;,
       State = &quot;SP&quot;,
       State = &quot;RS&quot;,
       State = &quot;PR&quot;,
       State = &quot;SC&quot;,
       State = &quot;MT&quot;,
       State = &quot;MS&quot;,
       State = &quot;MG&quot;,
       State = &quot;DF&quot;,
       State = &quot;BA&quot;,
       State = &quot;ES&quot;,
       State = &quot;CE&quot;,
       State = &quot;RJ&quot;,
       State = &quot;AL&quot;,
       State = &quot;PA&quot;,
       State = &quot;PB&quot;,
       State = &quot;PE&quot;
    )
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF16_Email_CasaCor_Vendedor</fullName>
        <actions>
            <name>Casa_Cor_Agradecimento_Vendedor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
    Campaign.Id = &quot;70157000001R6OU&quot;,
    Text(IntentToPurchaseNewVehicle__c) = &quot;Within next 6 months&quot;,
    OR(
       State = &quot;GO&quot;,
       State = &quot;SP&quot;,
       State = &quot;RS&quot;,
       State = &quot;PR&quot;,
       State = &quot;SC&quot;,
       State = &quot;MT&quot;,
       State = &quot;MS&quot;,
       State = &quot;MG&quot;,
       State = &quot;DF&quot;,
       State = &quot;BA&quot;,
       State = &quot;ES&quot;,
       State = &quot;CE&quot;,
       State = &quot;RJ&quot;,
       State = &quot;AL&quot;,
       State = &quot;PA&quot;,
       State = &quot;PB&quot;,
       State = &quot;PE&quot;
    )
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF17_Update_Dealer_Of_Interest</fullName>
        <actions>
            <name>LDD_WF17_Update_Dealer_Of_Interest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates BIR_Dealer_of_Interest__c when an Opportunity is created by Offers Site</description>
        <formula>AND( ISPICKVAL(SubSource__c, &apos;OFFERS SITE&apos;), NOT( ISBLANK(DealerOfInterest__c) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Populates Date Field Not Interest</fullName>
        <actions>
            <name>Update_Date_Last_Not_Interest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Not Interested</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Populate Standard Address</fullName>
        <actions>
            <name>Lead_Populate_Standard_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Populate_Standard_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Populate_Standard_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Populate_Standard_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Populate_Standard_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( PostalCode__c )   || ISCHANGED( Street__c )       || ISCHANGED( Complement__c )   || ISCHANGED( Number__c )       || ISCHANGED( Neighborhood__c ) || ISCHANGED( City__c )         ||  ISCHANGED( State__c )        || ISCHANGED( Country__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Contact_Customer</fullName>
        <assignedToType>owner</assignedToType>
        <description>Contact Customer to follow-up on Lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contact Customer</subject>
    </tasks>
    <tasks>
        <fullName>Send_Brochure</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please Send the Brochure to the Customer by Email.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Brochure</subject>
    </tasks>
    <tasks>
        <fullName>Send_Brochures</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please Send the Brochure to the Customer by Courier.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Brochure</subject>
    </tasks>
</Workflow>
