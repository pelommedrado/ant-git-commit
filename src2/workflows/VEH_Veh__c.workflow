<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Atualiza_VehicleBrand</fullName>
        <description>Atualiza o campo para &quot;Veículo&quot; para Renault</description>
        <field>VehicleBrand__c</field>
        <literalValue>Renault</literalValue>
        <name>Atualiza VehicleBrand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Captur_Model_Name</fullName>
        <field>Model__c</field>
        <formula>&quot;CAPTUR&quot;</formula>
        <name>Captur Model Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_IsAvailable_False</fullName>
        <field>Is_Available__c</field>
        <literalValue>0</literalValue>
        <name>VEH_IsAvailable_False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_IsAvailable_True</fullName>
        <field>Is_Available__c</field>
        <literalValue>1</literalValue>
        <name>VEH_IsAvailable_True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_Annual_Mileage</fullName>
        <description>&quot;KmPerYear__c&quot; field value moved (updated or created), so update of &quot;Annual_Mileage__c&quot;</description>
        <field>Annual_Mileage__c</field>
        <formula>KmPerYear__c</formula>
        <name>VEH_MigHelios_MYR_FU_Annual_Mileage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_Commercial_Model</fullName>
        <description>&quot;ModelCode__c&quot; field value moved (updated or created), so update of &quot;Commercial_Model_c&quot;</description>
        <field>Commercial_Model__c</field>
        <formula>ModelCode__c</formula>
        <name>VEH_MigHelios_MYR_FU_Commercial_Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_Current_Mileage</fullName>
        <description>&quot;KmCheck__c&quot; field value moved (updated or created), so update of &quot;Current_Mileage__c&quot;</description>
        <field>Current_Mileage__c</field>
        <formula>KmCheck__c</formula>
        <name>VEH_MigHelios_MYR_FU_Current_Mileage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_Current_MileageDate</fullName>
        <description>&quot;KmCheckDate__c&quot; field value moved (updated or created), so update of &quot;Current_Mileage_Date__c&quot;</description>
        <field>Current_Mileage_Date__c</field>
        <formula>KmCheckDate__c</formula>
        <name>VEH_MigHelios_MYR_FU_Current_MileageDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_KmCheck</fullName>
        <description>&quot;Current_Mileage__c&quot; field value moved (updated or created), so update of &quot;KmCheck__c&quot;</description>
        <field>KmCheck__c</field>
        <formula>Current_Mileage__c</formula>
        <name>VEH_MigHelios_MYR_FU_KmCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_KmCheckDate</fullName>
        <description>&quot;Current_Mileage_Date__c&quot; field value moved (updated or created), so update of &quot;KmCheckDate__c&quot;</description>
        <field>KmCheckDate__c</field>
        <formula>Current_Mileage_Date__c</formula>
        <name>VEH_MigHelios_MYR_FU_KmCheckDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_KmPerYear</fullName>
        <description>&quot;Annual_Mileage__c&quot; field value moved (updated or created), so update of &quot;KmPerYear__c&quot;</description>
        <field>KmPerYear__c</field>
        <formula>Annual_Mileage__c</formula>
        <name>VEH_MigHelios_MYR_FU_KmPerYear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_MigHelios_MYR_FU_ModelCode</fullName>
        <description>&quot;Commercial_Model__c&quot; field value moved (updated or created), so update of &quot;ModelCode__c&quot;</description>
        <field>ModelCode__c</field>
        <formula>Commercial_Model__c</formula>
        <name>VEH_MigHelios_MYR_FU_ModelCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_VINExternalIDUpdate</fullName>
        <description>Every time a record is created or VIN number is modfied, it updates VIN External ID value with VIN Value</description>
        <field>Tech_VINExternalID__c</field>
        <formula>Name</formula>
        <name>VEH_VINExternalIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_WF02_Update_Annual_Mileage_Date</fullName>
        <field>Annual_Mileage_Date__c</field>
        <formula>Today()</formula>
        <name>VEH_WF02_Update_Annual_Mileage_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_WF03_Update_Current_Mileage_Date</fullName>
        <field>KmCheckDate__c</field>
        <formula>today()</formula>
        <name>VEH_WF03_Update_Current_Mileage_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_WF04_UPDATE_MODEL_LOGAN</fullName>
        <field>Model__c</field>
        <formula>&quot;LOGAN&quot;</formula>
        <name>VEH_WF04_UPDATE_MODEL_LOGAN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VEH_WF05_UPDATE_MODEL_CLIO</fullName>
        <field>Model__c</field>
        <formula>&quot;CLIO&quot;</formula>
        <name>VEH_WF05_UPDATE_MODEL_CLIO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Atualiza VehicleBrand</fullName>
        <actions>
            <name>Atualiza_VehicleBrand</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VEH_Veh__c.VehicleBrand__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Kaptur Captur</fullName>
        <actions>
            <name>Captur_Model_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(  Model__c , &quot;KAPTUR&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VEH_IsAvailable_False</fullName>
        <actions>
            <name>VEH_IsAvailable_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>VEH_Veh__c.Status__c</field>
            <operation>equals</operation>
            <value>Comercial Blockade,Billed,Quality Blockade,Immobilized</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_IsAvailable_True</fullName>
        <actions>
            <name>VEH_IsAvailable_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>VEH_Veh__c.Status__c</field>
            <operation>equals</operation>
            <value>Stock,In Transit</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_Annual_Mileage</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_Annual_Mileage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If KmPerYear__c is updated, then Annual_Mileage__c is updated</description>
        <formula>AND(  OR(ISNEW() ,ISCHANGED( KmPerYear__c )),  ISBLANK(KmPerYear__c ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_Commercial_Model</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_Commercial_Model</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ModelCode__c is updated, then Commercial_Model_c is updated</description>
        <formula>AND(  OR(ISNEW() ,ISCHANGED( ModelCode__c )),  ISBLANK(ModelCode__c ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_Current_Mileage</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_Current_Mileage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If KmCheck__c is updated, then Current_Mileage__c is updated</description>
        <formula>AND(  OR(ISNEW() ,ISCHANGED( KmCheck__c )),  ISBLANK(KmCheck__c ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_Current_Mileage_Date</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_Current_MileageDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If KmCheckDate__c is updated, then Current_Mileage_Date__c is updated</description>
        <formula>AND( OR(ISNEW() ,ISCHANGED( KmCheckDate__c )),  ISBLANK(KmCheckDate__c ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_KmCheck</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_KmCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Current_Mileage__c is updated, then   KmCheck__c is updated</description>
        <formula>AND( OR(ISNEW() ,ISCHANGED( Current_Mileage__c )), ISBLANK(Current_Mileage__c ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_KmCheckDate</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_KmCheckDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Current_Mileage_Date__c is updated, then  KmCheckDate__c is updated</description>
        <formula>AND( OR(ISNEW() ,ISCHANGED( Current_Mileage_Date__c )), ISBLANK(Current_Mileage_Date__c ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_KmPerYear</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_KmPerYear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Annual_Mileage__c is updated, then  KmPerYear__c is updated</description>
        <formula>AND( OR(ISNEW() ,ISCHANGED( Annual_Mileage__c )), ISBLANK(Annual_Mileage__c ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_MigHelios_MYR_Update_ModelCode</fullName>
        <actions>
            <name>VEH_MigHelios_MYR_FU_ModelCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Commercial_Model__c  is updated, then ModelCode__c  is updated</description>
        <formula>AND( OR(ISNEW() ,ISCHANGED( Commercial_Model__c  )), ISBLANK(Commercial_Model__c  ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_WF01_VINExternalIDUpdate</fullName>
        <actions>
            <name>VEH_VINExternalIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISNEW()  ||   ISCHANGED( Name )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_WF02_Update_Annual_Mileage_Date</fullName>
        <actions>
            <name>VEH_WF02_Update_Annual_Mileage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the value of field « Annual_Mileage » (KmPerYear__c) is updated, the field « Annual Mileage Date » (Annual_Mileage_Date__c ) is updated to date of the day</description>
        <formula>ISCHANGED(KmPerYear__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_WF03_Update_Current_Mileage_Date</fullName>
        <actions>
            <name>VEH_WF03_Update_Current_Mileage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the value of field « Current Mileage» (KmCheck__c) is updated, the field « Current Mileage Date» (KmCheckDate__c ) is updated to date of the day</description>
        <formula>ISCHANGED(KmCheck__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VEH_WF04_UPDATE_MODEL_LOGAN</fullName>
        <actions>
            <name>VEH_WF04_UPDATE_MODEL_LOGAN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VEH_Veh__c.Model__c</field>
            <operation>equals</operation>
            <value>SYMBOL II/LOGAN II</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VEH_WF05_UPDATE_MODEL_CLIO</fullName>
        <actions>
            <name>VEH_WF05_UPDATE_MODEL_CLIO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VEH_Veh__c.Model__c</field>
            <operation>equals</operation>
            <value>CLIO 2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
