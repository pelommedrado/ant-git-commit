<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>E_survey_TestDriveEmail_Anonymous</fullName>
        <description>E-survey_TestDriveEmail_Anonymous</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Send_E_survey_TestDrive_Rsite_Anonymous</template>
    </alerts>
    <alerts>
        <fullName>E_survey_TestDriveEmail_NotAnonymous</fullName>
        <description>E-survey_TestDriveEmail_NotAnonymous</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Send_E_survey_TestDriveEmail_Rsite</template>
    </alerts>
    <alerts>
        <fullName>E_survey_TestDriveEmail_NotAnonymous3</fullName>
        <description>E-survey_TestDriveEmail_NotAnonymous3</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Send_E_survey_TestDriveEmail_Rsite</template>
    </alerts>
    <rules>
        <fullName>E-survey - Test Drive Email Rsite - Anonymous</fullName>
        <active>false</active>
        <criteriaItems>
            <field>TDE_TestDriveEmail__c.CreatedById</field>
            <operation>equals</operation>
            <value>Renault do Brasil</value>
        </criteriaItems>
        <description>Send an email with survey one day after the test drive date at dealers without R + C.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>E_survey_TestDriveEmail_Anonymous</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>TDE_TestDriveEmail__c.DateBooking__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>E-survey - Test Drive Email Rsite - Not anonymous</fullName>
        <active>false</active>
        <criteriaItems>
            <field>TDE_TestDriveEmail__c.CreatedById</field>
            <operation>equals</operation>
            <value>Renault do Brasil</value>
        </criteriaItems>
        <description>Send an email with survey one day after the test drive date at dealers without R + C.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>E_survey_TestDriveEmail_NotAnonymous</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>TDE_TestDriveEmail__c.DateBooking__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
