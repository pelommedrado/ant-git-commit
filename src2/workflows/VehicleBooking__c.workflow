<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>VBK_Cancela_reserva</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>VBK Cancela reserva</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VBK_Cancela_reserva_I</fullName>
        <field>VBK_Verified__c</field>
        <literalValue>1</literalValue>
        <name>VBK Cancela reserva I</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VBK_Clean_Booking_User</fullName>
        <description>Clean the Booking User Field on Vehicle Object.</description>
        <field>Booking_User__c</field>
        <name>VBK_Clean Booking User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>Vehicle__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>VBK Cancela reserva</fullName>
        <actions>
            <name>VBK_Cancela_reserva</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VehicleBooking__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.Quote_Open__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.ExpireDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.Automatic_VBK_Expiration__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.VBK_Verified__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <description>Regra que baseada no tempo da concessionária, cancela a reserva de pre ordens que estão em aberto.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VBK Cancela reserva I</fullName>
        <active>true</active>
        <criteriaItems>
            <field>VehicleBooking__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.Quote_Open__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.ExpireDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.Automatic_VBK_Expiration__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>VehicleBooking__c.VBK_Verified__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>Regra que baseada no tempo da concessionária, cancela a reserva de pre ordens que estão em aberto.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VBK_Cancela_reserva_I</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>VehicleBooking__c.ExpireDate__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VBK_Clean Booking User</fullName>
        <actions>
            <name>VBK_Clean_Booking_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VehicleBooking__c.Status__c</field>
            <operation>equals</operation>
            <value>Expired,Canceled</value>
        </criteriaItems>
        <description>When a Vehicle Booking is Canceled, clean the field Booking User on Vehicle Object.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
