<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SQR_UpdateResponseNbr</fullName>
        <description>R-Force:Release 4
This is the field update is used to get the over all response for a survey</description>
        <field>Response_Nbr_Txt__c</field>
        <formula>IF( 
OR(Response__c=&apos;Completely satisfied&apos;,Response__c=&apos;Agree&apos;,Response__c=&apos;Comfortable&apos;),&apos;3&apos;,
IF(OR(Response__c=&apos;Quite satisfied&apos;,Response__c=&apos;Neutral&apos;,Response__c=&apos;Uncomfortable&apos;),&apos;2&apos;,
IF(OR(Response__c=&apos;Strongly Agree&apos;,Response__c=&apos;Yes&apos;,Response__c=&apos;Very Comfortable&apos;),&apos;4&apos;,
IF(OR(Response__c=&apos;Not very satisfied&apos;,Response__c=&apos;Disagree&apos;,Response__c=&apos;Very Uncomfortable&apos;),&apos;1&apos;,
IF(OR(Response__c=&apos;Not satisfied at all&apos;,Response__c=&apos;Donot Use&apos;,Response__c=&apos;Strongly Disagree&apos;),&apos;0&apos;,
IF(OR(Response__c=Null,Response__c=&apos;Not Applicable&apos;),Null,
IF(OR(Response__c=&apos;0&apos;,Response__c=&apos;1&apos;,Response__c=&apos;2&apos;,Response__c=&apos;3&apos;,Response__c=&apos;4&apos;,Response__c=&apos;5&apos;,Response__c=&apos;6&apos;,Response__c=&apos;7&apos;,Response__c=&apos;8&apos;,Response__c=&apos;9&apos;),Response__c,
Null)))))))</formula>
        <name>SQR_UpdateResponseNbr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SQR_WF01ResponseNbr</fullName>
        <actions>
            <name>SQR_UpdateResponseNbr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>R-Force:Release 4
This is the field update is used to get the over all response for a survey</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
