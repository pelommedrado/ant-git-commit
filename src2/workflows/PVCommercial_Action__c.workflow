<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ACO_PV_AddActionNameAndModel</fullName>
        <field>Name</field>
        <formula>Name &amp; &quot; &quot; &amp; Model__r.Name</formula>
        <name>ACO_PV_AddActionNameAndModel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACO_PV_AddModelNameOfTheAction</fullName>
        <actions>
            <name>ACO_PV_AddActionNameAndModel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Adiciona o nome do modelo junto com a ação comercial</description>
        <formula>Name &lt;&gt; null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
