<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PV_Merc_CodUpdate</fullName>
        <field>CodMercadoria__c</field>
        <formula>Name</formula>
        <name>PV_Merc_CodUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_Merc_UpdateModelVersionOptional</fullName>
        <description>Concatena Model,  Version, Optional, Paint para validar</description>
        <field>model_version_optional_painting__c</field>
        <formula>Model__c &amp;  Version__c &amp;  Optional__c &amp;  Paint__c</formula>
        <name>PV_Merc_UpdateModelVersionOptional</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PV_Merc_Cod</fullName>
        <actions>
            <name>PV_Merc_CodUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mercadoria__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copia os valores da mercadoria para o campo código da mercadoria de verificação</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PV_Merc_UpdateModelVersionOptionalPainting</fullName>
        <actions>
            <name>PV_Merc_UpdateModelVersionOptional</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mercadoria__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Concatena os valores de model, version, optional, painting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
