<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PV_Model_validate</fullName>
        <description>O modelo do veiculo não pode ser repetido 2 vezes</description>
        <field>ModeloValidate__c</field>
        <formula>Name</formula>
        <name>PV_Model_validate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PV_Model_NameUpdate</fullName>
        <actions>
            <name>PV_Model_validate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Model__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>O modelo do veiculo não pode ser repetido 2 vezes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
