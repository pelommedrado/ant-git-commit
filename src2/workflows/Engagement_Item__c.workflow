<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Engagement_Item_Unique_ID_Update</fullName>
        <field>Item_ID__c</field>
        <formula>BIR__c &amp; Month__c &amp; Text(Vehicle_Model__c) &amp; Text(Vehicle_Version__c) &amp; Text(Vehicle_Optional__c)</formula>
        <name>Engagement Item Unique ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Engagement Item Unique ID</fullName>
        <actions>
            <name>Engagement_Item_Unique_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
