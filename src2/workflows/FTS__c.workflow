<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Nova_FTS</fullName>
        <description>Nova FTS</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/ContactFollowUpSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>FTS_RCourtesy_Update_FTS_recordtype</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Core_FTS_New_Dealer_Support_RecType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FTS RCourtesy Update FTS recordtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Core FTS WF04 Core FTS Courtesy Page Layout Assignment</fullName>
        <actions>
            <name>FTS_RCourtesy_Update_FTS_recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>R-Force :Release 8 : RCourtesy
After a new FTS is created by core service profile user a new record type is assigned to get a new page layout</description>
        <formula>AND( ISNEW() ,RecordType.Name =&apos;Core FTS Dealer Support RecType&apos;, NOT( $User.BypassWF__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
