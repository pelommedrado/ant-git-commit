<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AR_EM01_Case_Approved</fullName>
        <description>AR EM01 Case Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Argentina_Templates/AR_TE_01_Goodwill_approval_confirmation</template>
    </alerts>
    <alerts>
        <fullName>AR_EM02_Case_Rejected</fullName>
        <description>AR EM02 Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Argentina_Templates/AR_TE_02_Goodwill_rejected</template>
    </alerts>
    <alerts>
        <fullName>Alert_Unaswerable_cases</fullName>
        <description>Alert Unaswerable cases</description>
        <protected>false</protected>
        <recipients>
            <recipient>CN_DCQS_Director</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>China_Template/CN_Template_Unaswerable_cases</template>
    </alerts>
    <alerts>
        <fullName>Alert_privileged_Customer</fullName>
        <description>Alert privileged Customer</description>
        <protected>false</protected>
        <recipients>
            <recipient>CN_DCQS_Director</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>China_Template/CN_Template_privileged_Customer</template>
    </alerts>
    <alerts>
        <fullName>Beyond_7_days</fullName>
        <description>The case goes beyond 7 days</description>
        <protected>false</protected>
        <recipients>
            <recipient>cristiana.furiotto@mondial-assistance.com.br.renault</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edgard.vasques@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elaine.pires@mondial-assistance.com.br</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>patricia_calenta_desativado@nao.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.silva@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>veronica.campos@mondial-assistance.com.br.renault</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/The_case_goes_beyond_7_days</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Automatic_Closure_Answered</fullName>
        <description>CAC Case Automatic Closure Answered</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CAC_EmailAnalyst__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Automatic_Closure_Answered</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Automatic_Closure_Waiting_Reply</fullName>
        <description>CAC Case Automatic Closure Waiting Reply</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CAC_EmailAnalyst__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Automatic_Closure_Waiting_Reply</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Marketing</fullName>
        <description>CAC Case Expiration After Sales Marketing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Quality_Methods</fullName>
        <description>CAC Case Expiration After Sales Quality Methods</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Receive_Warranty_Parts</fullName>
        <description>CAC Case Expiration After Sales Receive Warranty Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Techline</fullName>
        <description>CAC Case Expiration After Sales Techline</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Technical_Documentation</fullName>
        <description>CAC Case Expiration After Sales Technical Documentation</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_After_Sales_Warranty</fullName>
        <description>CAC Case Expiration After Sales Warranty</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Distribution</fullName>
        <description>CAC Case Expiration Distribution</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Distribution_Scheduling</fullName>
        <description>CAC Case Expiration Distribution Scheduling</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Enterprise_Sales</fullName>
        <description>CAC Case Expiration Enterprise Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Financial_Credit_Billing</fullName>
        <description>CAC Case Expiration Financial Credit Billing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Financial_Tributary</fullName>
        <description>CAC Case Expiration Financial Tributary</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Financial_Tributary_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Financial_Tributary_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Logistics</fullName>
        <description>CAC Case Expiration Logistics</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Logistic_Transport_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Marketing_Commercial_Actions</fullName>
        <description>CAC Case Expiration Marketing Commercial Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Marketing_Publicity</fullName>
        <description>CAC Case Expiration Marketing Advertising</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Network_Sales_Commercial_Coordination</fullName>
        <description>CAC Case Expiration Network Sales Commercial Coordination</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Network_Sales_Employee_Sales</fullName>
        <description>CAC Case Expiration Network Sales Employee Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Network_Sales_Network_Informatic</fullName>
        <description>CAC Case Expiration Network Sales Network Informatic</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Network_Sales_Regionals</fullName>
        <description>CAC Case Expiration Network Sales Regionals</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Parts_Return_Parts</fullName>
        <description>CAC Case Expiration Parts Return Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Parts_Warehouse_Parts</fullName>
        <description>CAC Case Expiration Parts Warehouse Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Warehouse_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Parts_Parts_Warehouse_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Quality</fullName>
        <description>CAC Case Expiration Quality</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Renault_Network_Logistics</fullName>
        <description>CAC Case Expiration Renault Network Logistics</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Renault_Network_Training</fullName>
        <description>CAC Case Expiration Renault Network Training</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Expiration_Training</fullName>
        <description>CAC Case Expiration Training</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>CAC_Training_Network_Training_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Automatic_Closure</fullName>
        <description>CAC Case Near Automatic Closure</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Automatic_Closure</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Marketing</fullName>
        <description>CAC Case Near Expiration After Sales Marketing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Quality_Methods</fullName>
        <description>CAC Case Near Expiration After Sales Quality Methods</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Receive_Warranty_Parts</fullName>
        <description>CAC Case Near Expiration After Sales Receive Warranty Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Techline</fullName>
        <description>CAC Case Near Expiration After Sales Techline</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Technical_Documentation</fullName>
        <description>CAC Case Near Expiration After Sales Technical Documentation</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_After_Sales_Warranty</fullName>
        <description>CAC Case Near Expiration After Sales Warranty</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Enterprise_Sales_Key_Accounts</fullName>
        <description>CAC Case Near Expiration Enterprise Sales Key Accounts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Financial_Credit_Billing</fullName>
        <description>CAC Case Near Expiration Financial Credit Billing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Financial_Tributary</fullName>
        <description>CAC Case Near Expiration Financial Tributary</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Financial_Tributary_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Logistic_Transport</fullName>
        <description>CAC Case Near Expiration Logistic Transport</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Marketing_Advertising</fullName>
        <description>CAC Case Near Expiration Marketing Advertising</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Marketing_Commercial_Actions</fullName>
        <description>CAC Case Near Expiration Marketing Commercial Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Marketing_Distribution</fullName>
        <description>CAC Case Near Expiration Marketing Distribution</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Marketing_Scheduling</fullName>
        <description>CAC Case Near Expiration Marketing Scheduling</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Network_Sales_Commercial_Coordination</fullName>
        <description>CAC Case Near Expiration Network Sales Commercial Coordination</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Network_Sales_Employee_Sales</fullName>
        <description>CAC Case Near Expiration Network Sales Employee Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Network_Sales_Network_Informatic</fullName>
        <description>CAC Case Near Expiration Network Sales Network Informatic</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Network_Sales_Regionals</fullName>
        <description>CAC Case Near Expiration Network Sales Regionals</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Parts_Return_Parts</fullName>
        <description>CAC Case Near Expiration Parts Return Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Parts_Warehouse_Parts</fullName>
        <description>CAC Case Near Expiration Parts Warehouse Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Warehouse_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Quality_Industrial_Quality</fullName>
        <description>CAC Case Near Expiration Quality Industrial Quality</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Near_Expiration_Training_Network_Training</fullName>
        <description>CAC Case Near Expiration Training Network Training</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Status_Change</fullName>
        <description>CAC Case Status Change</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CAC_EmailAnalyst__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Status_Changed_Answered</fullName>
        <description>CAC Case Status Changed to Answered</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_Waiting_Answer</fullName>
        <description>CAC Case Waiting Answer</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_Waiting_Answer</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Marketing</fullName>
        <description>CAC Case queued near expiration After Sales Marketing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Quality_Methods</fullName>
        <description>CAC Case queued near expiration After Sales Quality Methods</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Receive_Warranty_Parts</fullName>
        <description>CAC Case queued near expiration After Sales Receive Warranty Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Techline</fullName>
        <description>CAC Case queued near expiration After Sales Techline</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Technical_Documentation</fullName>
        <description>CAC Case queued near expiration After Sales Technical Documentation</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_After_Sales_Warranty</fullName>
        <description>CAC Case queued near expiration After Sales Warranty</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Enterprise_Sales</fullName>
        <description>CAC Case queued near expiration Enterprise Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_near_expiration_Enterprise_Sales</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Financial_Credit_Billing</fullName>
        <description>CAC Case queued near expiration Financial Credit Billing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Financial</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Financial_Tributary</fullName>
        <description>CAC Case queued near expiration Financial Tributary</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Financial_Tributary_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Financial</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Logistic_Transport</fullName>
        <description>CAC Case queued near expiration Logistic Transport</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Logistics</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Marketing_Advertising</fullName>
        <description>CAC Case queued near expiration Marketing Advertising</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Marketing</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Marketing_Commercial_Actions</fullName>
        <description>CAC Case queued near expiration Marketing Commercial Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Marketing</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Marketing_Distribution</fullName>
        <description>CAC Case queued near expiration Marketing Distribution</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_near_expiration_Distribution</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Marketing_Scheduling</fullName>
        <description>CAC Case queued near expiration Marketing Scheduling</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_near_expiration_Distribution</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Network_Sales_Commercial_Coordination</fullName>
        <description>CAC Case queued near expiration Network Sales Commercial Coordination</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Network_Sales</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Network_Sales_Employee_Sales</fullName>
        <description>CAC Case queued near expiration Network Sales Employee Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Network_Sales</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Network_Sales_Informatic_Network</fullName>
        <description>CAC Case queued near expiration Network Sales Informatic Network</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Network_Sales</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Network_Sales_Regionals</fullName>
        <description>CAC Case queued near expiration Network Sales Regionals</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Network_Sales</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Parts_Return_Parts</fullName>
        <description>CAC Case queued near expiration Parts Return Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Parts</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Parts_Warehouse_Parts</fullName>
        <description>CAC Case queued near expiration Parts Warehouse Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Warehouse_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Parts</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Quality</fullName>
        <description>CAC Case queued near expiration Quality Industrial Quality</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Quality</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_near_expiration_Training</fullName>
        <description>CAC Case queued near expiration Training Networking Training</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_Near_Expiration_Training</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Marketing</fullName>
        <description>CAC Case queued out of time After Sales Marketing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Quality_Methods</fullName>
        <description>CAC Case queued out of time After Sales Quality Methods</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Receive_Warranty_Parts</fullName>
        <description>CAC Case queued out of time After Sales Receive Warranty Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Techline</fullName>
        <description>CAC Case queued out of time After Sales Techline</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Technical_Documentation</fullName>
        <description>CAC Case queued out of time After Sales Technical Documentation</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_After_Sales_Warranty</fullName>
        <description>CAC Case queued out of time After Sales Warranty</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Distribution</fullName>
        <description>CAC Case queued out of time Distribution</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Distribution_Scheduling</fullName>
        <description>CAC Case queued out of time Distribution Scheduling</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Enterprise_Sales</fullName>
        <description>CAC Case queued out of time Enterprise Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Financial_Credit_Billing</fullName>
        <description>CAC Case queued out of time Financial Credit Billing</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Financial_Tributary</fullName>
        <description>CAC Case queued out of time Financial Tributary</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Financial_Tributary_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Logistics</fullName>
        <description>CAC Case queued out of time Logistics</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Marketing_Commercial_Actions</fullName>
        <description>CAC Case queued out of time Marketing Commercial Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Marketing_Publicity</fullName>
        <description>CAC Case queued out of time Marketing Advertising</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Network_Sales_Comercial_Cordination</fullName>
        <description>CAC Case queued out of time Network Sales Comercial Coordination</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Network_Sales_Employee_Sales</fullName>
        <description>CAC Case queued out of time Network Sales Employee Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Network_Sales_Network_Informatic</fullName>
        <description>CAC Case queued out of time Network Sales Network Informatic</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Network_Sales_Regionals</fullName>
        <description>CAC Case queued out of time Network Sales Regionals</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Parts_Return_Parts</fullName>
        <description>CAC Case queued out of time Parts Return Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Parts_Warehouse_Parts</fullName>
        <description>CAC Case queued out of time Parts Warehouse Parts</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Parts_Parts_Warehouse_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Quality</fullName>
        <description>CAC Case queued out of time Quality</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_Case_queued_out_of_time_Training</fullName>
        <description>CAC Case queued out of time Training</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/Case_queued_OutOfTime</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Marketing</fullName>
        <description>CAC New Case After Sales Marketing</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Marketing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Quality_Methods</fullName>
        <description>CAC New Case After Sales Quality Methods</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Quality_Methods_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Receive_Warranty_Parts</fullName>
        <description>CAC New Case After Sales Receive Warranty Parts</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Rec_War_Parts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Techline</fullName>
        <description>CAC New Case After Sales Techline</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Techline_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Technical_Documentation</fullName>
        <description>CAC New Case After Sales Technical Documentation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Tech_Doc_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_After_Sales_Warranty</fullName>
        <description>CAC New Case After Sales Warranty</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_After_Sales_Warranty_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Distribution</fullName>
        <description>CAC New Case Distribution</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Distribution_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Distribution_Scheduling</fullName>
        <description>CAC New Case Distribution Scheduling</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Scheduling_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Enterprise_Sales</fullName>
        <description>CAC New Case Enterprise Sales</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Enterp_Sales_Key_Accounts_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Financial_Credit_Billing</fullName>
        <description>CAC New Case Financial Credit Billing</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Fin_Credit_and_Billing_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Financial_Tributary</fullName>
        <description>CAC New Case Financial Tributary</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Financial_Tributary_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Logistics</fullName>
        <description>CAC New Case Logistics</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Marketing_Commercial_Actions</fullName>
        <description>CAC New Case Marketing Commercial Actions</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_MKT_Com_Actions_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Marketing_Publicity</fullName>
        <description>CAC New Case Marketing Advertising</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Marketing_Advertising_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Network_Sales_Commercial_Coordination</fullName>
        <description>CAC New Case Network Sales Commercial Coordination</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Com_Coord_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Network_Sales_Employee_Sales</fullName>
        <description>CAC New Case Network Sales Employee Sales</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Employee_Sales_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Network_Sales_Network_Informatic</fullName>
        <description>CAC New Case Network Sales Network Informatic</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Net_Sales_Net_Info_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Network_Sales_Regionals</fullName>
        <description>CAC New Case Network Sales Regionals</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Network_Sales_Regionals_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Parts</fullName>
        <description>CAC New Case Parts</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Parts_Parts_Return_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Quality</fullName>
        <description>CAC New Case Quality</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Quality_Industrial_Quality_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Renault_Network_Logistics</fullName>
        <description>CAC New Case Renault Network Logistics</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Logistic_Transport_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Renault_Network_Training</fullName>
        <description>CAC New Case Renault Network Training</description>
        <protected>false</protected>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CAC_New_Case_Training</fullName>
        <description>CAC New Case Training</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>CAC_Training_Network_Training_Analysts</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAC_Brasil/New_Case</template>
    </alerts>
    <alerts>
        <fullName>CN_CaseRejected</fullName>
        <description>CN Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>asmaa.amenzou@renault.com.br</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CN_CaseRejected</template>
    </alerts>
    <alerts>
        <fullName>CN_Case_Approved</fullName>
        <description>CN Case Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>asmaa.amenzou@renault.com.br</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CN_CaseApproved</template>
    </alerts>
    <alerts>
        <fullName>CO_Case_non_closed_in_7_days</fullName>
        <description>CO_Case non closed in 7 days</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Colombie/CO_Case_non_closed_in_7_days</template>
    </alerts>
    <alerts>
        <fullName>CO_EM01_Case_Approved</fullName>
        <description>CO EM01 Case Approved</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Colombie/CO_Goodwill_Approval_Template</template>
    </alerts>
    <alerts>
        <fullName>CO_EM02_Case_Rejected</fullName>
        <description>CO EM02 Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Colombie/CO_Goodwill_Rejected_Template</template>
    </alerts>
    <alerts>
        <fullName>CO_EM_Contact_Client_24h</fullName>
        <description>CO EM Contact Client 24h</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Colombie/CO_Contact_Client_in_12_hours</template>
    </alerts>
    <alerts>
        <fullName>Cas_EM03_Information_or_Service_Request_SurveyForm</fullName>
        <description>Cas EM04 Information or Service Request SurveyForm</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Core_CAS_InformationRequestCaseCustomerSurveyCustom</template>
    </alerts>
    <alerts>
        <fullName>Cas_EM03_SurveyForm</fullName>
        <description>Cas EM03 SurveyForm</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Core_CAS_ComplaintCaseCustomerSurveyCustom</template>
    </alerts>
    <alerts>
        <fullName>CaseApproval</fullName>
        <description>CaseApproval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CaseApproval</template>
    </alerts>
    <alerts>
        <fullName>Case_Approved</fullName>
        <description>Case Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CaseApproved</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_BO_N3</fullName>
        <description>Case BR Contact Client 48h BO N3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N3_Coordenacao</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_R1</fullName>
        <description>Case BR Contact Client 48h R1</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R1</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_R2</fullName>
        <description>Case BR Contact Client 48h R2</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R2</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_R3</fullName>
        <description>Case BR Contact Client 48h R3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R3</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_R4</fullName>
        <description>Case BR Contact Client 48h R4</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R4</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_Contact_Client_48h_R5</fullName>
        <description>Case BR Contact Client 48h R5</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R5</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Contact_Client_48h</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_BO_N3</fullName>
        <description>Case BR FollowUp 48hrs BO N3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N3_Coordenacao</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_R1</fullName>
        <description>Case BR FollowUp 48hrs R1</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R1</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_R2</fullName>
        <description>Case BR FollowUp 48hrs R2</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R2</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_R3</fullName>
        <description>Case BR FollowUp 48hrs R3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R3</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_R4</fullName>
        <description>Case BR FollowUp 48hrs R4</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R4</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_BR_FollowUp_48hrs_R5</fullName>
        <description>Case BR FollowUp 48hrs R5</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R5</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/BR_Case_followup_48H</template>
    </alerts>
    <alerts>
        <fullName>Case_Rejected</fullName>
        <description>Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/SUPPORTCaseescalationnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21daysBOL3</fullName>
        <description>Complaint opened for  21 days BO L3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N3_Coordenacao</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21daysR2</fullName>
        <description>Complaint opened for  21 days R2</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R2</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21daysR3</fullName>
        <description>Complaint opened for  21 days R3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R3</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21daysR4</fullName>
        <description>Complaint opened for  21 days R4</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R4</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21daysR5</fullName>
        <description>Complaint opened for  21 days R5</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R5</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor21days_R1</fullName>
        <description>Complaint opened for  21 days R1</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R1</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaintopenedfor21days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor8daysBOL3</fullName>
        <description>Complaint opened for  8 days BO L3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N3_Coordenacao</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor8daysR2</fullName>
        <description>Complaint opened for  8 days R2</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R2</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor8daysR3</fullName>
        <description>Complaint opened for  8 days R3</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R3</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor8daysR4</fullName>
        <description>Complaint opened for  8 days R4</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R4</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedfor8daysR5</fullName>
        <description>Complaint opened for  8 days R5</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R5</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Complaintopenedor8daysR1</fullName>
        <description>Complaint opened for  8 days R1</description>
        <protected>false</protected>
        <recipients>
            <recipient>BO_N2_Coordenacao_R1</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/complaint_opened_for_8_days</template>
    </alerts>
    <alerts>
        <fullName>Salesforce_Could_Not_Create_This_Lead</fullName>
        <ccEmails>marcelino@kolekto.com.br</ccEmails>
        <ccEmails>marcelo.dourado-renexter@renault.com</ccEmails>
        <description>Salesforce Could Not Create This Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>anacebalho.renault@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bruno.machado@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cybele.guidio-renexter@renault.com.logica</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>felipe.f.guerra-renexter@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>marcos.neves-renexter@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Salesforce_Could_Not_Create_This_Lead</template>
    </alerts>
    <alerts>
        <fullName>casefollowup48hrs</fullName>
        <description>case followup 48hrs</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Casefollowup48H</template>
    </alerts>
    <fieldUpdates>
        <fullName>ASGCaseClosure</fullName>
        <field>Status</field>
        <literalValue>Closed Dealer</literalValue>
        <name>ASGCaseClosure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Answered_Status</fullName>
        <description>Indica se Status é respondido</description>
        <field>Answered_Status__c</field>
        <literalValue>1</literalValue>
        <name>Answered Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_Uncheck_new</fullName>
        <field>Approval__c</field>
        <literalValue>0</literalValue>
        <name>Approval checkbox Uncheck_new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_check</fullName>
        <field>Approval_500_2000__c</field>
        <literalValue>1</literalValue>
        <name>Approval checkbox check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_check2</fullName>
        <field>Approval_2000_5000__c</field>
        <literalValue>1</literalValue>
        <name>Approval checkbox check2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_check3</fullName>
        <field>Approval_5000_10000__c</field>
        <literalValue>1</literalValue>
        <name>Approval checkbox check3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_check4</fullName>
        <field>Approval_10000__c</field>
        <literalValue>1</literalValue>
        <name>Approval checkbox check4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_check_new</fullName>
        <description>Check Case : Approval if workflow process is launch</description>
        <field>Approval__c</field>
        <literalValue>1</literalValue>
        <name>Approval checkbox check_new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_uncheck</fullName>
        <field>Approval_500_2000__c</field>
        <literalValue>0</literalValue>
        <name>Approval checkbox uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_uncheck2</fullName>
        <field>Approval_2000_5000__c</field>
        <literalValue>0</literalValue>
        <name>Approval checkbox uncheck2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_uncheck3</fullName>
        <field>Approval_5000_10000__c</field>
        <literalValue>0</literalValue>
        <name>Approval checkbox uncheck3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_checkbox_uncheck4</fullName>
        <field>Approval_10000__c</field>
        <literalValue>0</literalValue>
        <name>Approval checkbox uncheck4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Atualiza_propriet_rio_para_a_fila</fullName>
        <field>OwnerId</field>
        <lookupValue>Cacweb_Enterprise_Sales_KEY_FLEET</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Atualiza proprietário para a fila</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Automatic_Answered_Status</fullName>
        <description>Alteração de Status automática para Fechado Automaticamente Respondido.</description>
        <field>Status</field>
        <literalValue>Closed Automatically Answered</literalValue>
        <name>Automatic Answered Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Automatic_Waiting_Reply_Status</fullName>
        <description>Alteração de Status automática para Fechado Automaticamente Aguardando Resposta.</description>
        <field>Status</field>
        <literalValue>Closed Automatically Waiting Answer</literalValue>
        <name>Automatic Waiting Reply Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N2_LAUDO_CORRE_O</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N2_LAUDO_CORRE_O</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N2_ LAUDO_CORREÇÃO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_AGREEMENT</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_AGREEMENT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ AGREEMENT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_DUVIDAS_RA</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_DUVIDAS_RA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ DUVIDAS_RA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_LOCA_O_SAC</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_LOCA_O_SAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ LOCAÇÃO_SAC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_LOC_SIMPLIFICADA</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_LOC_SIMPLIFICADA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ LOC_SIMPLIFICADA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_PLAQUETA</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_PLAQUETA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ PLAQUETA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_RCL</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_RCL</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ RCL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_REEMBOLSO</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_REEMBOLSO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ REEMBOLSO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BO_N3_SUBSTITUI_O</fullName>
        <field>OwnerId</field>
        <lookupValue>BO_N3_SUBSTITUICAO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BO_N3_ SUBSTITUIÇÃO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_CAS_ChangeOwner_Dealer</fullName>
        <field>OwnerId</field>
        <lookupValue>Cacweb_AfterSalesWarranty</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CAC_CAS_ChangeOwner_Dealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_CAS_ChangeStatus_Waiting_for_reply</fullName>
        <field>Status</field>
        <literalValue>Under Analysis</literalValue>
        <name>CAC_CAS_ChangeStatus_Waiting_for_reply</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_CAS_UpdateStatusToAnswers</fullName>
        <field>Status</field>
        <literalValue>Answered</literalValue>
        <name>CAC_CAS_UpdateStatusToAnswers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Case_Label_Register</fullName>
        <description>Registro de identificação de Caso do CAC</description>
        <field>CACCase__c</field>
        <literalValue>1</literalValue>
        <name>CAC Case Label Register</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Close</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CAC_Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Closing_Waiting_Answer</fullName>
        <field>Status</field>
        <literalValue>Closed Automatically Waiting Answer</literalValue>
        <name>CAC Closing Waiting Answer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Expiration_Flag</fullName>
        <field>Expired__c</field>
        <literalValue>1</literalValue>
        <name>CAC Expiration Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Flag_Expiration</fullName>
        <field>Expired__c</field>
        <literalValue>1</literalValue>
        <name>CAC Flag Expiration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_Flag_Expiration_Parts</fullName>
        <field>Expired__c</field>
        <literalValue>1</literalValue>
        <name>CAC Flag Expiration Parts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_New_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>CAC New to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAC_UpdateAnalystEmail</fullName>
        <description>Updates the field CAC_EmailAnalyst everytime a case (CAC) is modified by an Analyst.</description>
        <field>CAC_EmailAnalyst__c</field>
        <formula>$User.Email</formula>
        <name>CAC_UpdateAnalystEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_APP18_FromSpam</fullName>
        <field>From__c</field>
        <literalValue>Customer</literalValue>
        <name>CAS_APP18_FromSpam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_APP19_SpamDenial</fullName>
        <description>If it&apos;s a spam, customer is not mandatory : put Customer Denial checkbox to avoid pb on validation rules</description>
        <field>CustGlobalDenial__c</field>
        <literalValue>1</literalValue>
        <name>CAS_APP19_SpamDenial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Add1ToReOpen</fullName>
        <description>Add +1 on the field &quot;Case Re-Open&quot;</description>
        <field>CaseReopeningNumber__c</field>
        <formula>IF( ISBLANK( CaseReopeningNumber__c) , 1,  CaseReopeningNumber__c +1)</formula>
        <name>CAS_Add1ToReOpen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_BR_update_case_owner_PVI</fullName>
        <description>Case Subtype = PVI Request
Queue Name = BR_Pedido_PVI</description>
        <field>OwnerId</field>
        <lookupValue>BR_Pedido_PVI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CAS BR update case owner PVI</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_GWStatusUpdate2</fullName>
        <field>GWStatus__c</field>
        <literalValue>No Approval</literalValue>
        <name>CAS_GoodwillStatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Resolved_Date</fullName>
        <field>StatusResolvedDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Resolved_Date_2</fullName>
        <field>StatusResolvedDate__c</field>
        <formula>ClosedDate</formula>
        <name>CAS Resolved Date 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Resolved_Status</fullName>
        <description>When the case status is resolved the field is made true</description>
        <field>REP_ResolvedStatus__c</field>
        <literalValue>1</literalValue>
        <name>CAS Resolved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_SLA_Closing_Time</fullName>
        <description>The closing time field is made CHECKED</description>
        <field>Closing_Time__c</field>
        <literalValue>1</literalValue>
        <name>CAS SLA Closing Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_SLA_Intial_Response</fullName>
        <description>The Initial Response field is made false</description>
        <field>Initial_Response__c</field>
        <literalValue>1</literalValue>
        <name>CAS SLA Intial Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusAnswerdateUpdate</fullName>
        <description>When Status= Answered
Update Status Answered Date by today&apos;s date</description>
        <field>StatusAnsweredDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusAnswerdateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusClosedWF</fullName>
        <field>StatusClosedWF__c</field>
        <literalValue>1</literalValue>
        <name>CAS_StatusClosedWF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusEscaldateUpdate</fullName>
        <description>When Status=Escalated
Update Status Escalated Date by today&apos;s date</description>
        <field>StatusEscalatedDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusEscaldateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusNewdateUpdate</fullName>
        <description>When Status =New  
Update &quot;Status Open Date&quot; by Today date</description>
        <field>StatusNewDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusNewdateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusOpendateUpdate</fullName>
        <description>When Status=Open
Update Status Open Date by today&apos;s date</description>
        <field>StatusOpenDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusOpendateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusResolveddateUpdate</fullName>
        <description>When case status = Resolved
Update Status Resolved Date by today&apos;s date</description>
        <field>StatusResolvedDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusResolveddateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_StatusWaitingdateUpdate</fullName>
        <description>When Status=Waiting for reply
Update Status New Date by today&apos;s date</description>
        <field>StatusWaitingDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_StatusWaitingdateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateCaseLastAllocationDate</fullName>
        <field>Case_Last_Allocation_Date__c</field>
        <formula>NOW()</formula>
        <name>CAS_UpdateCaseLastAllocationDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateModifedDate</fullName>
        <description>If the case is resolved the case resolution date is case resolved date and if the case is closed with out resolution the case resolution date is closed date</description>
        <field>Case_Modified_Date__c</field>
        <formula>TODAY()</formula>
        <name>CAS_UpdateModifedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateResolvedChekbox</fullName>
        <description>When Status = Resolved, Check the checkbox &quot;Resolved Status&quot;
WF for reporting need</description>
        <field>REP_ResolvedStatus__c</field>
        <literalValue>1</literalValue>
        <name>CAS_UpdateResolvedChekbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateResolvedMonth</fullName>
        <field>REP_ResolvedMonth__c</field>
        <formula>MONTH(Today())</formula>
        <name>CAS_UpdateResolvedMonth</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateResolvedYear</fullName>
        <field>REP_ResolvedYear__c</field>
        <formula>YEAR(Today())</formula>
        <name>CAS_UpdateResolvedYear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateStatus</fullName>
        <description>Update Status &quot;Resolved&quot; to &quot;Closed&quot;</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CAS_UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_UpdateStautToClose</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CAS UpdateStautToClose</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Update_field_DateExpertise</fullName>
        <field>Expertise_Date__c</field>
        <formula>NOW()</formula>
        <name>CAS Update_field_DateExpertise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_Update_field_DateExpertise_Blank</fullName>
        <description>When field is unchecked, the field Expertise Date must be update in blank</description>
        <field>Expertise_Date__c</field>
        <name>CAS Update_field_DateExpertise_Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_update_CountryCase_c</fullName>
        <description>Update CountryCase__c field</description>
        <field>CountryCase__c</field>
        <literalValue>Brazil</literalValue>
        <name>CAS update CountryCase__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CORE_CAS_FU01_e2c_origin_Email_update</fullName>
        <field>Origin</field>
        <literalValue>Email</literalValue>
        <name>CORE CAS FU01 e2c origin Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CORE_CAS_FU02_e2c_from_Cust_update</fullName>
        <field>From__c</field>
        <literalValue>Customer</literalValue>
        <name>CORE CAS FU02 e2c from Cust update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CORE_CAS_FU03_e2c_from_Dealer_update</fullName>
        <field>From__c</field>
        <literalValue>Dealer</literalValue>
        <name>CORE CAS FU03 e2c from Dealer update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campo_Disabilitar_Regra_Valida_o</fullName>
        <field>Disables_validation__c</field>
        <literalValue>1</literalValue>
        <name>Campo Disabilitar Regra Validação</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Detail_Dealer_QSA</fullName>
        <field>Detail__c</field>
        <literalValue>AQSA</literalValue>
        <name>Cas_Update_Detail_Dealer_QSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Detail_Dealer_QVN</fullName>
        <field>Detail__c</field>
        <literalValue>AQVN</literalValue>
        <name>Cas_Update_Detail_Dealer_QVN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Owner_Dealer_QSA</fullName>
        <field>OwnerId</field>
        <lookupValue>BR_QVN_QSO_Contacts</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Cas_Update_Owner_Dealer_QSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Owner_Dealer_QVN</fullName>
        <field>OwnerId</field>
        <lookupValue>BR_QVN_QSO_Contacts</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Cas_Update_Owner_Dealer_QVN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_StatusClosed_Dealer_QSA</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Cas_Update_StatusClosed_Dealer_QSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_StatusClosed_Dealer_QVN</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Cas_Update_StatusClosed_Dealer_QVN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_SubType_Dealer_QSA</fullName>
        <field>SubType__c</field>
        <literalValue>TAFIRECALL</literalValue>
        <name>Cas_Update_SubType_Dealer_QSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_SubType_Dealer_QVN</fullName>
        <field>SubType__c</field>
        <literalValue>TAFIRECALL</literalValue>
        <name>Cas_Update_SubType_Dealer_QVN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Type_Dealer_QSA</fullName>
        <field>Type</field>
        <literalValue>Other</literalValue>
        <name>Cas_Update_Type_Dealer_QSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cas_Update_Type_Dealer_QVN</fullName>
        <field>Type</field>
        <literalValue>Other</literalValue>
        <name>Cas_Update_Type_Dealer_QVN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Answered_Waiting_Reply_Flag</fullName>
        <description>Marcar a Flag de Respondido ou Aguardando Resposta</description>
        <field>Waiting_Reply_Status__c</field>
        <literalValue>1</literalValue>
        <name>Check Answered Waiting Reply Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_CAC_Case_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>suporte.cac@kolekto.com.br</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Closed CAC Case Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date7J_Empty</fullName>
        <field>Date7J__c</field>
        <name>Date7J Empty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Encaminhado_para_consultor</fullName>
        <field>Direcionado_para_Consultor__c</field>
        <literalValue>1</literalValue>
        <name>Encaminhado para consultor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GoodWill_Status_Approved</fullName>
        <field>GWStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>GoodWill Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GoodWill_Status_In_Progress</fullName>
        <description>Update &quot;Goodwill Status&quot; to &quot;In Progress Value</description>
        <field>GWStatus__c</field>
        <literalValue>In Progress</literalValue>
        <name>GoodWill Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GoodWill_Status_Rejected</fullName>
        <field>GWStatus__c</field>
        <literalValue>Refused</literalValue>
        <name>GoodWill Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_case_reopen_number</fullName>
        <field>CaseReopeningNumber__c</field>
        <formula>CaseReopeningNumber__c + 1</formula>
        <name>Increment case reopening number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queu_BR_Concessionarias</fullName>
        <field>OwnerId</field>
        <lookupValue>BR_Proatividade_Rede</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queu_BR_Concessionarias</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queue_CO_Dealer</fullName>
        <field>OwnerId</field>
        <lookupValue>Queue_CO_Dealer_Community</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queue_CO_Dealer</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_owner_to_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>GW_BR_Approval1000_3000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign owner to queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_owner_to_queue2</fullName>
        <field>OwnerId</field>
        <lookupValue>GW_BR_Approval3000_5000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign owner to queue2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_owner_to_queue3</fullName>
        <field>OwnerId</field>
        <lookupValue>GW_BR_Approval5000_50000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign owner to queue3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_owner_to_queue4</fullName>
        <field>OwnerId</field>
        <lookupValue>GW_BR_Approval_50000</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign owner to queue4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Answered_Flag</fullName>
        <description>Desmarcar a caixa de seleção de Status Respondido.</description>
        <field>Answered_Status__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Answered Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Answered_Waiting_Reply_Flag</fullName>
        <description>Desmarcar a caixa de seleção de Status Aguardando Resposta.</description>
        <field>Waiting_Reply_Status__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Answered Waiting Reply Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCaseLastAllocationDate</fullName>
        <field>Case_Last_Allocation_Date__c</field>
        <formula>NOW()</formula>
        <name>UpdateCaseLastAllocationDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusChangedToNew</fullName>
        <field>StatusChangedToNew__c</field>
        <formula>now()</formula>
        <name>UpdateStatusChangedToNew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date7J</fullName>
        <description>Update Date7J with today&apos;s date</description>
        <field>Date7J__c</field>
        <formula>NOW()</formula>
        <name>Update Date7J</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_for_PVI_Request</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Status for PVI Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_TimeBetweenNew_and_Open</fullName>
        <field>Time_Between_New_and_Open__c</field>
        <formula>now()-  StatusChangedToNew__c</formula>
        <name>Update TimeBetweenNew_and_Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_new</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update status new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZA_CAS_FU03_e2c_origin_Email_update</fullName>
        <field>From__c</field>
        <literalValue>Fleet</literalValue>
        <name>ZA - CAS FU03 e2c From Fleet update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZA_CAS_FU04_e2c_type_Other</fullName>
        <description>At case automatic creation, perform the following : 
Update Case origin = email
Update From = Customer
Update Case type = &quot;Other&quot;
Update case sub type = &quot;Spare parts&quot;</description>
        <field>Type</field>
        <literalValue>Other</literalValue>
        <name>ZA - CAS FU04 e2c type Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZA_CAS_FU05_e2c_sub_type_Spare_parts</fullName>
        <description>At case automatic creation, perform the following : 
Update Case origin = email
Update From = Customer
Update Case type = &quot;Other&quot;
Update case sub type = &quot;Spare parts&quot;</description>
        <field>SubType__c</field>
        <literalValue>Spare parts</literalValue>
        <name>ZA - CAS FU05 e2c sub type Spare parts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AR CAS WF01 Email To Case</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CORE_CAS_FU03_e2c_from_Dealer_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Argentina</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_DEALER</value>
        </criteriaItems>
        <description>This Is to update country and from value&apos;s of  the case record  when it is created from email--Argentina Specific</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AR CAS WF02 Email To Case</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CORE_CAS_FU02_e2c_from_Cust_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Argentina</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_CUSTOMER</value>
        </criteriaItems>
        <description>This Is to update country and from value&apos;s of  the case record  when it is created from email--Argentina Specific--customer</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAC Answered Status</fullName>
        <actions>
            <name>Answered_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identificar se o status está como Respondido</description>
        <formula>AND(
   Text(Status) = &quot;Answered&quot;,   
   $User.isCac__c,
   OR(
      TEXT($User.roleInCac__c) = &quot;Dealer&quot;,
      TEXT($User.roleInCac__c) = &quot;Analyst&quot;
   ),
   CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAC Case Automatic Closure Answered</fullName>
        <active>true</active>
        <description>Envio de e-mail em caso de Fechamento Automático Respondido.</description>
        <formula>AND(
	ISPICKVAL(Status , &quot;Answered&quot;),
	$User.isCac__c,
		OR (ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
		ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Automatic_Answered_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CAC_HourExpired24__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Automatic Closure Waiting Reply</fullName>
        <active>true</active>
        <description>Envio de e-mail em caso de Fechamento Automático Aguardando Resposta.</description>
        <formula>AND(     
	ISPICKVAL(Status , &quot;Waiting for reply&quot;),     
	$User.isCac__c,      
	OR(TEXT($User.roleInCac__c) = &quot;Dealer&quot;, TEXT($User.roleInCac__c) = &quot;Analyst&quot;), 
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Automatic_Waiting_Reply_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CAC_HourExpired24__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration</fullName>
        <active>true</active>
        <description>Indicação de expiração de caso (flag)</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	$User.isCac__c,
		OR (
			ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration After Sales Quality Methods</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Pós Venda Qualidade e Métodos</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;After Sales&quot;),
        ISPICKVAL(CAC_Department__c , &quot;Qualidade métodos&quot;),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration After Sales Receive Warranty Parts</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Pós Venda Recepção de Peças em Garantia</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;After Sales&quot;),
        ISPICKVAL(CAC_Department__c , &quot;Recepção de peças em garantia&quot;),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration After Sales Techline</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Pós Venda Techline</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;After Sales&quot;),
        ISPICKVAL(CAC_Department__c , &quot;Tech line&quot;),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration After Sales Technical Documentation</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Pós Venda Documentação Técnica</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;After Sales&quot;),
        ISPICKVAL(CAC_Department__c , &quot;Documentação técnica&quot;),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration After Sales Warranty</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Pós Venda Garantia</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Enterprise Sales Key Accounts</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Vendas Empresa Grandes Contas</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Enterprise Sales&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Grandes Contas (VE)&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Financial Credit Billing</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Finanças Crédito e Cobrança</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Financial&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Crédito e Cobrança&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Financial Tributary</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Finanças Tributário</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Financial&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Tributário&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Logistics Transport</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Logística Transporte</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Logistic&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Transporte&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Marketing Advertising</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Marketing Publicidade</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Marketing&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Publicidade&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Marketing Commercial Actions</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Marketing Ação Comercial</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Marketing&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Bonus rede Acao Comercial&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Marketing Distribution</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Marketing Distribuição</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;Distribution&quot;),
        OR(
           ISPICKVAL(CAC_Department__c , &quot;BIN&quot;),
           ISPICKVAL(CAC_Department__c , &quot;DEFIC&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Comissão&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Diplomatas&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Distribuição Regionais&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Governo&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Grandes Contas&quot;),
           ISPICKVAL(CAC_Department__c , &quot;Táxi&quot;)
          ),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;),
Expired__c=False
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Marketing Scheduling</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Marketing Programação</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	ISPICKVAL(CAC_Direction__c , &quot;Distribution&quot;),
        ISPICKVAL(CAC_Department__c , &quot;Programação&quot;),
	$User.isCac__c,
		OR 
			(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Network Sales Commercial Coordination</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Vendas Rede Coordenação Comercial</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Network Sales&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Coordenação Comercial&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Network Sales Employee Sales</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Vendas Rede Vendas a Colaborador</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Network Sales&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Vendas a Colaborador&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Network Sales Network Informatic</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Vendas Rede Informática Rede</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Network Sales&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Informática Rede&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Network Sales Regionals</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Vendas Rede Regionais</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Network Sales&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Regionais&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration OLD</fullName>
        <active>false</active>
        <description>Indicação de expiração de caso (flag)</description>
        <formula>AND(
	NOT (
		OR (
			ISPICKVAL(Status , &quot;Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;),
			ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;),
			ISPICKVAL(Status , &quot;Closed Dealer&quot;),
			ISPICKVAL(Status , &quot;Waiting for reply&quot;)
		)
	),
	$User.isCac__c,
		OR (
			ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
			ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;)
		),
	CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Parts Return Parts</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Peças Devolução de Peças</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Parts&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Devolução de Peças&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Parts Warehouse Parts</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Peças Armazém de Peças</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Parts&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Armazém de Peças&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Quality Industrial Quality</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Qualidade Qualidade Industrial</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Quality&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Qualidade Industrial&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Renault Network Logistics</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Renault-Rede Logística</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Renault Network&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Logística&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Renault Network Training</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Renault-Rede Treinamento</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Renault Network&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Treinamentos&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Flag_Expiration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Expiration Training Network Training</fullName>
        <active>false</active>
        <description>Envio de e-mail na expiração de caso de Treinamento Treinamento à Rede</description>
        <formula>AND( 
NOT ( 
OR ( 
ISPICKVAL(Status , &quot;Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
ISPICKVAL(Status , &quot;Closed Dealer&quot;), 
ISPICKVAL(Status , &quot;Waiting for reply&quot;) 
) 
), 
ISPICKVAL(CAC_Direction__c , &quot;Training&quot;), 
ISPICKVAL(CAC_Department__c , &quot;Treinamento à rede&quot;), 
$User.isCac__c, 
OR 
(ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;), 
ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
), 
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAC_Expiration_Flag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.beforeExpiredDate__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Case.Expiration_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC Case Label</fullName>
        <actions>
            <name>CAC_Case_Label_Register</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAC Closed Case Owner</fullName>
        <actions>
            <name>Closed_CAC_Case_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 
   OR ( 
      ISPICKVAL(Status , &quot;Closed Automatically Waiting Answer&quot;), 
      ISPICKVAL(Status , &quot;Closed Automatically Answered&quot;), 
      ISPICKVAL(Status , &quot;Closed Dealer&quot;)
   ),
   $User.isCac__c, 
   OR (
      ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
      ISPICKVAL($User.roleInCac__c, &quot;Dealer&quot;) 
   ), 
   CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAC Departamento Frota de Proximidade</fullName>
        <actions>
            <name>Atualiza_propriet_rio_para_a_fila</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Status , &quot;New&quot;),ISPICKVAL(CAC_Department__c , &quot;Frota de Proximidade&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAC NOT Answered Status</fullName>
        <actions>
            <name>Uncheck_Answered_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identificar se o status NÃO está como Respondido</description>
        <formula>AND(    Text(Status) &lt;&gt; &quot;Answered&quot;,    $User.isCac__c ,     OR(        TEXT($User.roleInCac__c) = &quot;Dealer&quot;,        TEXT($User.roleInCac__c) = &quot;Analyst&quot;     )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAC NOT Waiting Reply Status</fullName>
        <actions>
            <name>Uncheck_Answered_Waiting_Reply_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identificar se o status NÃO está como Aguardando Resposta</description>
        <formula>AND(     Text(Status) &lt;&gt; &quot;Waiting for reply&quot;,     $User.isCac__c ,     OR(        TEXT($User.roleInCac__c) = &quot;Dealer&quot;,        TEXT($User.roleInCac__c) = &quot;Analyst&quot;     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAC_CAS_ChangeStatus_Scalated</fullName>
        <actions>
            <name>CAC_CAS_ChangeStatus_Waiting_for_reply</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the case status to Scaleted when the owner is changed.</description>
        <formula>AND(     Owner:User.Username =  $User.Username,     $User.isCac__c ,     TEXT($User.roleInCac__c) = &quot;Analyst&quot;,     AND(        RecordType.DeveloperName  &lt;&gt; &quot;Cacweb_RenaultNetworking_Logistic&quot;,        RecordType.DeveloperName  &lt;&gt; &quot;Cacweb_RenaultNetworking_Trainning&quot;     ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAC_SendEmailAt16hours</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.isCac__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.roleInCac__c</field>
            <operation>equals</operation>
            <value>Analyst,Dealer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.CAC_HourExpired16__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC_SendEmailAt8hours</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.isCac__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.roleInCac__c</field>
            <operation>equals</operation>
            <value>Analyst,Dealer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.CAC_HourExpired8__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAC_UpdateAnalystEmail</fullName>
        <actions>
            <name>CAC_UpdateAnalystEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the field CAC_EmailAnalyst everytime a case (CAC) is modified by an Analyst.</description>
        <formula>AND ($User.isCac__c,  
ISPICKVAL($User.roleInCac__c, &quot;Analyst&quot;),
CONTAINS (RecordType.DeveloperName, &quot;Cacweb&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF23 Complaint %3D 8 days R1</fullName>
        <actions>
            <name>Complaintopenedor8daysR1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group R1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF24 Complaint %3D 8 days R2</fullName>
        <actions>
            <name>Complaintopenedfor8daysR2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group R2</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF25 Complaint %3D 8 days R3</fullName>
        <actions>
            <name>Complaintopenedfor8daysR3</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group R3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF26 Complaint %3D 8 days R4</fullName>
        <actions>
            <name>Complaintopenedfor8daysR4</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group R4</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF27 Complaint %3D 8 days R5</fullName>
        <actions>
            <name>Complaintopenedfor8daysR5</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group R5</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF28 Complaint %3D 8 days BO L3</fullName>
        <actions>
            <name>Complaintopenedfor8daysBOL3</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office Level 3,BR - Quality Manager,BR - SRC Supervisor BO (Renault)</value>
        </criteriaItems>
        <description>When the case is opened for 8 days, send an email alert to the case owner and to the coordinator group BO L3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF29 Complaint %3D 21 days R1</fullName>
        <actions>
            <name>Complaintopenedfor21days_R1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group R1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF30 Complaint %3D 21 days R2</fullName>
        <actions>
            <name>Complaintopenedfor21daysR2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group R2</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF31 Complaint %3D 21 days R3</fullName>
        <actions>
            <name>Complaintopenedfor21daysR3</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group R3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF32 Complaint %3D 21 days R4</fullName>
        <actions>
            <name>Complaintopenedfor21daysR4</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group R4</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF32 PVI Request Update Status</fullName>
        <actions>
            <name>CAS_BR_update_case_owner_PVI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_for_PVI_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Information Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>PVI Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Deal RecType</value>
        </criteriaItems>
        <description>If Sub Type = PVI Request and the dealer click on Submit botton Update Status for Draft to New</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF33 Complaint %3D 21 days R5</fullName>
        <actions>
            <name>Complaintopenedfor21daysR5</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group R5</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF34 Complaint %3D 21 days BO L3</fullName>
        <actions>
            <name>Complaintopenedfor21daysBOL3</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Count__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office Level 3,BR - Quality Manager,BR - SRC Supervisor BO (Renault)</value>
        </criteriaItems>
        <description>When the case is opened for 21 days, send and email alert to the case owner and to the coordinator group BO L3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H BO N3</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_BO_N3</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_BO_N3</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office Level 3,BR - Quality Manager,BR - SRC Supervisor BO (Renault)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H R1</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H R2</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H R3</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R3</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R3</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H R4</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R4</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R4</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35  Case BR Followup 48H R5</fullName>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R5</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_FollowUp_48hrs_R5</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H BO N3</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_BO_N3</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_BO_N3</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office Level 3,BR - Quality Manager,BR - SRC Supervisor BO (Renault)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status 
New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H R1</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_R1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_R1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H R2</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_R2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_R2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status 
New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H R3</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_R3</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_R3</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status 
New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H R4</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_R4</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_R4</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status 
New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case BR Contact Client 48H R5</fullName>
        <actions>
            <name>Case_BR_Contact_Client_48h_R5</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_BR_Contact_Client_48h_R5</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_client_48H__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>BR - Agent Back Office L2 R1,BR - Agent Back Office L2 R2,BR - Agent Back Office L2 R3,BR - Agent Back Office L2 R4,BR - Agent Back Office L2 R5,BR - Agent Back Office Level 2 (Mondial),BR - SRC Supervisor BO (Mondial)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>If Case type COMPLAINT with status 
New and Contact Client 48h was not checked after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF35 Case Followup 48H R1</fullName>
        <actions>
            <name>casefollowup48hrs</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Casefollowup48hrs</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Closed,Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_modification_count__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>Complaint with last activity done older than 48hrs</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF39 Email-to-Case - Country Brazil</fullName>
        <actions>
            <name>CAS_update_CountryCase_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>BR_RecType</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Automatically fill in the field Country the value &quot;Brazil&quot; when its a Brazilian Email2Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF40 Case BR Closed Case QVN</fullName>
        <actions>
            <name>Cas_Update_Detail_Dealer_QVN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_Owner_Dealer_QVN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_StatusClosed_Dealer_QVN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_SubType_Dealer_QVN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_Type_Dealer_QVN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>QVN Recall</value>
        </criteriaItems>
        <description>Automatic closure of cas from dealer with QVN Recall</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF41 Case BR Closed Case QSA</fullName>
        <actions>
            <name>Cas_Update_Detail_Dealer_QSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_Owner_Dealer_QSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_StatusClosed_Dealer_QSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_SubType_Dealer_QSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cas_Update_Type_Dealer_QSA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>QSA Recall</value>
        </criteriaItems>
        <description>Automatic closure of cas from dealer with QSA Recall</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF45 ComplaintCaseCustomerSurvey</fullName>
        <actions>
            <name>Cas_EM03_SurveyForm</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.StopSurvey__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>notEqual</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersEmail__pc</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CaseSubSource__c</field>
            <operation>equals</operation>
            <value>Webform,MyRenault</value>
        </criteriaItems>
        <description>When a case is closed an email is sent with the link to survey form</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS WF46 Information or ServiceRequestCaseCustomerSurvey</fullName>
        <actions>
            <name>Cas_EM03_Information_or_Service_Request_SurveyForm</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.StopSurvey__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Information Request,Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>notEqual</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersEmail__pc</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>When a case is closed an email is sent with the link to survey form</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS _WF03_GoodwillGlobalAmount1</fullName>
        <actions>
            <name>CAS_GWStatusUpdate2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.UserMaxGWGlobAmount__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>Goodwill global Amount &gt; User Max Goodwill global amount</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS _WF11_CasePreferedMediaUpdate</fullName>
        <active>false</active>
        <description>Update Case Prefered Media field with Contact documentation</description>
        <formula>ISCHANGED( CasePreferedMedia__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF01_ CaseReopenCount</fullName>
        <actions>
            <name>CAS_Add1ToReOpen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Count how many time the case is reopen</description>
        <formula>AND( NOT( $User.BypassWF__c ), ISPICKVAL(PRIORVALUE ( Status ), &quot;Closed&quot;), ISPICKVAL(Status,&quot;Reopen&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF01_REP_StatusResolved</fullName>
        <actions>
            <name>CAS_UpdateResolvedChekbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_UpdateResolvedMonth</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_UpdateResolvedYear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reporting Need
When Case Status =&quot;Resolved&quot;, check the checkbox &quot;Resolved Status&quot;</description>
        <formula>REP_ResolvedStatus__c =False &amp;&amp; ISPICKVAL(Status,&quot;Resolved&quot;) &amp;&amp; NOT( $User.BypassWF__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF02_StatusUpdateToClosed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BlockClosureFlag__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>When status =&quot;Resolved&quot; since more than 30 days and &quot;Block closure flag&quot; isn&apos;t checked, Then modify the status value to &quot;Closed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_StatusClosedWF</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>CAS_UpdateStatus</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WF04_StatusNewUpdate</fullName>
        <actions>
            <name>CAS_StatusNewdateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>When Status=New
Update Status New Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF05_StatusOpenUpdate</fullName>
        <actions>
            <name>CAS_StatusOpendateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>When Status=Open
Update Status Open Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF06_WaitingUpdate</fullName>
        <actions>
            <name>CAS_StatusWaitingdateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for reply</value>
        </criteriaItems>
        <description>When Status=Waiting for reply
Update Status New Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF07_EscalatedateUpdate</fullName>
        <actions>
            <name>CAS_StatusEscaldateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <description>When Status= Escalated
Update Status Escalated Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF08_AnswereddateUpdate</fullName>
        <actions>
            <name>CAS_StatusAnswerdateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Answered</value>
        </criteriaItems>
        <description>When Status= Answered
Update Status Answered Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF09_ResolvedDateUpdate</fullName>
        <actions>
            <name>CAS_StatusResolveddateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>When Status= Resolved
Update Status Resolved Date by today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF10_Submit4Approval</fullName>
        <actions>
            <name>SubmitGoodwilltoApproval</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.UserNeedsApproval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Goodwill Amount is greater than agent approval right, an activity is created to notify him he must submit the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF12_UpdateStatutIfSpam</fullName>
        <actions>
            <name>CAS_APP18_FromSpam</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_APP19_SpamDenial</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_UpdateStautToClose</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Spam__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF14_UpdateLastCaseModifiedDate</fullName>
        <actions>
            <name>CAS_UpdateCaseLastAllocationDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateCaseLastAllocationDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF15_Agreement Issue</fullName>
        <actions>
            <name>BO_N3_AGREEMENT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Information Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Agreement Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <description>Correção acordo AGR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF15_Vehicle Replacement</fullName>
        <actions>
            <name>BO_N3_SUBSTITUI_O</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Vehicle Replacement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF15_plus 7 days</fullName>
        <actions>
            <name>Beyond_7_days</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Plus_7_Jours__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <description>Si le traitement de la requête compte dépasser les 7 jours, alors envoyer une alerte par mail au supervisuer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF16_Letter of Report</fullName>
        <actions>
            <name>BO_N2_LAUDO_CORRE_O</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Letter of Report / Correction</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF17_CaseStatus_New_TimeStamp</fullName>
        <actions>
            <name>UpdateStatusChangedToNew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status),ISPICKVAL(Status,&apos;New&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF17_Debit Note</fullName>
        <actions>
            <name>BO_N3_REEMBOLSO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Debit Note</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF18_CalculateTime_Between_NewToOpen</fullName>
        <actions>
            <name>Update_TimeBetweenNew_and_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.StatusChangedToNew__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF18_Oval_Plate</fullName>
        <actions>
            <name>BO_N3_PLAQUETA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Oval Plate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF19_RCL Doubts</fullName>
        <actions>
            <name>BO_N3_RCL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>RCL Doubts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF20_SRC_Car_Rental</fullName>
        <actions>
            <name>BO_N3_LOCA_O_SAC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>SRC Car Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF20_UpdateDate7J</fullName>
        <actions>
            <name>Update_Date7J</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Plus_7_Jours__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Update Date7J with Today&apos;s date when Plus 7 Jours is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF21_Comprehensive_Car_Rental</fullName>
        <actions>
            <name>BO_N3_LOC_SIMPLIFICADA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Comprehensive Car Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF21_UpdateDate7J to Empty</fullName>
        <actions>
            <name>Date7J_Empty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Plus_7_Jours__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Update Date7J with Empty date when Plus 7 Jours is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF22_RA Doubts</fullName>
        <actions>
            <name>BO_N3_DUVIDAS_RA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>RA Doubts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFBR_Concessionárias</fullName>
        <actions>
            <name>Queu_BR_Concessionarias</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_status_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>notEqual</operation>
            <value>PVI Request</value>
        </criteriaItems>
        <description>(Case: Case OriginEQUALSDEALER) AND (Case: StatusEQUALSDraft)  AND (Case: ProfileUser WF FlagEQUALSTrue) AND (Case: Country CaseEQUAL</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFBR_Concessionárias_R2%2FR4%2FR5</fullName>
        <actions>
            <name>Update_status_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>notEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Regional_Number__c</field>
            <operation>notEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <description>(Case: Case OriginEQUALSDEALER) AND (Case: StatusEQUALSDraft) AND (Case: Regional NumberNOT EQUAL TO1) AND (Case: Regional NumberNOT EQUAL TO3) AND (Case: ProfileUser WF FlagEQUALSTrue) AND (Case: Country CaseEQUALSBrazil)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFCO01_Contact Client 7 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CO Case RecType</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CO_Case_non_closed_in_7_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WFCO02_ Contact the client in 24 hours</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CO Case RecType</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CO_EM_Contact_Client_24h</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WFCO03_Update Case Origin %26 From Customer</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CORE_CAS_FU02_e2c_from_Cust_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_CUSTOMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>South Africa</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFCO04_Update Case Origin %26 From Dealer</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CORE_CAS_FU03_e2c_from_Dealer_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>South Africa</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFCO_Dealer Community_Colombia</fullName>
        <actions>
            <name>Queue_CO_Dealer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_status_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>DEALER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Core Community RecType</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ProfileUser_WF_Flag__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <description>Community update the case owner</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFZA01_E2C - Update Case Origin %26 From fleet</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ZA_CAS_FU03_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>South Africa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_ZA_Fleet</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WFZA02_E2C - Update Case Origin %26 SubType Spare parts</fullName>
        <actions>
            <name>CORE_CAS_FU01_e2c_origin_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CORE_CAS_FU02_e2c_from_Cust_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ZA_CAS_FU04_e2c_type_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ZA_CAS_FU05_e2c_sub_type_Spare_parts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>South Africa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>_TECH_ZA_Spare parts</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF_43_UpdateExpertise</fullName>
        <actions>
            <name>CAS_Update_field_DateExpertise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Expertise__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>When field expertise is check, add automatically today date in field Date Expertise</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WF_44_ExpertiseUncheck</fullName>
        <actions>
            <name>CAS_Update_field_DateExpertise_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When field Expertise in uncheck and Expertise is not blank, clear the field Date Expertise</description>
        <formula>AND( NOT(Expertise__c), NOT(ISBLANK(Expertise_Date__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN WF02 Queue Unanswerable cases</fullName>
        <actions>
            <name>Alert_Unaswerable_cases</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Unaswerable_cases</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CN Queue Unaswerable cases</value>
        </criteriaItems>
        <description>Create task and send email to the supervisor if the case owner equal to the queue : Unanswerable cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN WF03 Queue privileged Customer</fullName>
        <actions>
            <name>Alert_privileged_Customer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Privileged_Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CN Queue privileged Customer</value>
        </criteriaItems>
        <description>Create task and send email to the supervisor if the case owner equal to the queue : CN Queue privileged Customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Caso Direcionado para Consultores</fullName>
        <actions>
            <name>Encaminhado_para_consultor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Answer__c</field>
            <operation>contains</operation>
            <value>consultor</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Core CAS WF05 Case Resolved Status</fullName>
        <actions>
            <name>CAS_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_Resolved_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>When case status is resolved the Resolved Status field is made true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Core CAS WF06 Case Resolved Date</fullName>
        <actions>
            <name>CAS_Resolved_Date_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.REP_ResolvedStatus__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>If the case is resolved the case resolution date is case resolved date and if the case is closed with out resolution the case resolution date is closed date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Core CAS WF07 Modified Date</fullName>
        <actions>
            <name>CAS_UpdateModifedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If any of the field values are changed the case modified date is made as today</description>
        <formula>OR(ISCHANGED(Status),ISCHANGED(Priority),ISCHANGED(Type),  ISCHANGED(SubType__c),ISCHANGED(Detail__c),ISCHANGED(Subject),ISCHANGED(Description),ISCHANGED(CasePreferedMedia__c),ISCHANGED(Punctual_follow_up__c),ISCHANGED(Approval_required__c),ISCHANGED(VehicleMaintenance__c),ISCHANGED(MailSentDate__c),ISCHANGED(StopSurvey__c),ISCHANGED(UserNeedsApproval__c),ISCHANGED(Answer__c),ISCHANGED(CustSatisfaction__c),ISCHANGED(Spam__c),ISCHANGED(Dealer__c),ISCHANGED(CustPerception__c),ISCHANGED(CustomerWants__c),ISCHANGED(Risk__c),ISCHANGED(CIRNumber__c),ISCHANGED(PartsAvailabTrouble__c),ISCHANGED(PartRefNotAvail__c),ISCHANGED(PartInventoryStatus__c),ISCHANGED(ProductFault__c),ISCHANGED(SubProductFault__c),ISCHANGED(ServiceProblem__c),ISCHANGED(SalesProblem__c),ISCHANGED(Product_Problem__c),ISCHANGED(CatalogFuncCode__c),ISCHANGED(PartOrderNumber__c),ISCHANGED(RetainedVehicle__c),ISCHANGED(RetainedVehicle__c),ISCHANGED( RetainedVehDate__c),ISCHANGED(FirstrequestDate__c),ISCHANGED(EscalatedTo__c),ISCHANGED(EscalReferBy__c),ISCHANGED(Processing__c),ISCHANGED(CaseReopeningNumber__c),ISCHANGED(ParentId),ISCHANGED(ChronoNumber__c),ISCHANGED(SiebelCaseNbr__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Desativa Regra de Validação CAC_CAS_DevolutionParts_AskReceivedMost</fullName>
        <actions>
            <name>Campo_Disabilitar_Regra_Valida_o</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject__c</field>
            <operation>equals</operation>
            <value>Peça recebida a mais</value>
        </criteriaItems>
        <description>Desativa Regra de Validação CAC_CAS_DevolutionParts_AskReceivedMost quando o caso ja estiver criado.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Documentação ASG</fullName>
        <active>true</active>
        <formula>AND( 
RecordType.DeveloperName = &quot;Cacweb_AfterSalesWarranty&quot;, 
TEXT(Subject__c) = &quot;Documentação Padrão ASG&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ASGCaseClosure</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leads não convertidos</fullName>
        <actions>
            <name>Salesforce_Could_Not_Create_This_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>equals</operation>
            <value>Hervé Colsenet</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Salesforce Could Not Create This Lead</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>AR_TA_01_Goodwill_Approval</fullName>
        <assignedToType>role</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>AR TA 01 Goodwill Approval</subject>
    </tasks>
    <tasks>
        <fullName>CHECK_CASE</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Check Case</subject>
    </tasks>
    <tasks>
        <fullName>CNGWApproved</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CN Goodwill Approved</subject>
    </tasks>
    <tasks>
        <fullName>CNGWRefused</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CN Goodwill Refused</subject>
    </tasks>
    <tasks>
        <fullName>CNGWSubmitApproval</fullName>
        <assignedTo>CN_DCQS_Director</assignedTo>
        <assignedToType>role</assignedToType>
        <description>hotline Dealer 已提交给您以下任务：

主题: Submit Goodwill to Approval</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CN Goodwill Submission for Approval</subject>
    </tasks>
    <tasks>
        <fullName>CN_Check_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>CN Check Case</subject>
    </tasks>
    <tasks>
        <fullName>CN_Goodwill_Recall_for_Approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CN Goodwill Recall for Approval</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_BO_N3</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h BO N3</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_R1</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h R1</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_R2</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h R2</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_R3</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h R3</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_R4</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h R4</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_Contact_Client_48h_R5</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve resposta em 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR Contact Client 48h R5</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_BO_N3</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs BO N3</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_R1</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs R1</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_R2</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs R2</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_R3</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs R3</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_R4</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs R4</subject>
    </tasks>
    <tasks>
        <fullName>Case_BR_FollowUp_48hrs_R5</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case BR FollowUp 48hrs R5</subject>
    </tasks>
    <tasks>
        <fullName>Casefollowup48hrs</fullName>
        <assignedToType>owner</assignedToType>
        <description>Atenção, o caso xxxxx não teve nenhuma movimentação nas últimas 48h</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Case followup 48hrs</subject>
    </tasks>
    <tasks>
        <fullName>GWApproval</fullName>
        <assignedTo>BR_SRCSupervisorBO</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Goodwill Approval</subject>
    </tasks>
    <tasks>
        <fullName>GWApproved</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Goodwill Approved</subject>
    </tasks>
    <tasks>
        <fullName>GWRecall</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Goodwill Recall for Approval</subject>
    </tasks>
    <tasks>
        <fullName>GWRefused</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Goodwill Refused</subject>
    </tasks>
    <tasks>
        <fullName>GWSubmitApproval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Goodwill Submission for Approval</subject>
    </tasks>
    <tasks>
        <fullName>Goodwill_for_approval</fullName>
        <assignedTo>BR_SRCSupervisorBORenault</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Goodwill for approval</subject>
    </tasks>
    <tasks>
        <fullName>Goodwill_for_approvalDCQS</fullName>
        <assignedTo>hugo.medrado@renault.com.logica</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Goodwill for approval + 50 000</subject>
    </tasks>
    <tasks>
        <fullName>Privileged_Customer</fullName>
        <assignedTo>CN_DCQS_Director</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Hello,
Thanks to look into the VIP case, analysis cases and find out solution because I can not handle it .

Thanks in advance,
Best regards</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Privileged Customer : VIP</subject>
    </tasks>
    <tasks>
        <fullName>SubmitGoodwilltoApproval</fullName>
        <assignedToType>owner</assignedToType>
        <description>Goodwill Amount is greater than your approval right.
Please submit this case for approval by clicking on &quot;Submit&quot; button on Approval History Section</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Submit Goodwill to Approval</subject>
    </tasks>
    <tasks>
        <fullName>Unaswerable_cases</fullName>
        <assignedTo>CN_DCQS_Director</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Hello,
Thanks to look into the case, analysis cases and find out solution because I can not handle it 
Thanks in advance,
Best regards</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Unaswerable cases</subject>
    </tasks>
</Workflow>
