<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CM_FU01_StatusUpdateDateNow</fullName>
        <description>Custom_Message__c.StatusUpdateDate__c = now()</description>
        <field>StatusUpdateDate__c</field>
        <formula>now()</formula>
        <name>CM_FU01_StatusUpdateDateNow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CM_WF01_StatusUpdateDateToNow</fullName>
        <actions>
            <name>CM_FU01_StatusUpdateDateNow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MYR - CM: Field Status UpdateDate is set to now() when the record is inserted or when the status field of the record is modified.</description>
        <formula>OR( ISNEW(),  ISCHANGED( Status__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
