<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TestDriveVehicleBIR</fullName>
        <field>ID_BIR__c</field>
        <formula>Account__r.IDBIR__c</formula>
        <name>TestDriveVehicleBIR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TDV_01</fullName>
        <actions>
            <name>TestDriveVehicleBIR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Account__r.IDBIR__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
