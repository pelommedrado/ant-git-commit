<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Brazilian_Network_Site_Account_Creation</fullName>
        <description>Brazilian Network Site Account Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>cybele.guidio-renexter@renault.com.logica</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edson.alves-renexter@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>luiz.simao@renault.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>marcelino.cunha-renexter@renault.com.logica</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pedro.lins-renexter@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Brazilian_Network_Site_Account_Creation</template>
    </alerts>
    <fieldUpdates>
        <fullName>ACC_BillingCountry_BR</fullName>
        <field>BillingCountry</field>
        <formula>&quot;BR&quot;</formula>
        <name>ACC_BillingCountry_BR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_BrazilCountryUpdate</fullName>
        <description>When User &quot;Record Default Country&quot; is Brazil, update &quot;Country&quot; field in Account Object with the same value</description>
        <field>Country__c</field>
        <literalValue>Brazil</literalValue>
        <name>ACC_BrazilCountryUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CommunicationAgrmntNO</fullName>
        <field>ComAgreemt__c</field>
        <literalValue>No</literalValue>
        <name>ACC CommunicationAgrmntNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CommunicationAgrmntPartial</fullName>
        <field>ComAgreemt__c</field>
        <literalValue>Partial</literalValue>
        <name>ACC CommunicationAgrmntPartial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CommunicationAgrmntYes</fullName>
        <description>Update  to yes</description>
        <field>ComAgreemt__c</field>
        <literalValue>Yes</literalValue>
        <name>ACC CommunicationAgrmntYes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CompanyAccExtIDUpdate</fullName>
        <field>Tech_ACCExternalID__c</field>
        <formula>&quot;BR&quot;+ CustomerIdentificationNbr__c</formula>
        <name>ACC_CompanyAccExtIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CompanyAccExtIDUpdate2</fullName>
        <field>Tech_ACCExternalID__c</field>
        <formula>&quot;BR&quot;+CompanyID__c</formula>
        <name>ACC_CompanyAccExtIDUpdate2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CompanyAccExtIDUpdate3</fullName>
        <field>CustomerIdentificationNbr__c</field>
        <formula>CompanyID__c</formula>
        <name>ACC_CompanyAccExtIDUpdate3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_CompanyAccExtIDUpdate4</fullName>
        <field>CompanyID__c</field>
        <formula>CustomerIdentificationNbr__c</formula>
        <name>ACC_CompanyAccExtIDUpdate4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_DealershipLanguage</fullName>
        <field>DealershipLanguage__c</field>
        <formula>&quot;Português&quot;</formula>
        <name>ACC_DealershipLanguage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_EnterpAccExtIDUpdate</fullName>
        <description>When an Enterprise Account is created, Tech External ID field is update by &quot;Intracommunity VAT Number&quot; value. Each time &quot;Intracommunity VAT Number&quot; value is changed, it also updates Tech External ID value.</description>
        <field>Tech_ACCExternalID__c</field>
        <formula>&quot;BR&quot;+IntracomntyVATNbr__c</formula>
        <name>ACC_EnterpAccExtIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_MyD_Status</fullName>
        <field>MyD_Status_UpdateDate__c</field>
        <formula>now()</formula>
        <name>ACC_FU_MyD_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_MyR_Status</fullName>
        <description>&quot;MyR Status&quot; field value moved (updated or created), so update of &quot;MyR Status UpdateDate&quot;</description>
        <field>MYR_Status_UpdateDate__c</field>
        <formula>now()</formula>
        <name>ACC_FU_MyR_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_Tech_Name_LocalCustomerId</fullName>
        <description>MyRenault BackEnd: computes a concatenation of Firstname, Lastname and CustomerIdentificationNbr to optimize account research.
DO NOT FILL THE FIELD IS CUSTIDENTNBR IS BLANK !</description>
        <field>Tech_Name_LocalCustomerId__c</field>
        <formula>IF( ISBLANK(CustomerIdentificationNbr__c), &apos;&apos;,
FirstName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+LastName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+CustomerIdentificationNbr__c)</formula>
        <name>ACC_FU_Tech_Name_LocalCustomerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_Tech_Name_MyDaciaID</fullName>
        <description>MyRenault BackEnd: computes a concatenation of Firstname, Lastname and MyDaciaID tyo optimize account research.
DO NOT FILL THE FIELD IS MYDACIA IS BLANK !</description>
        <field>Tech_Name_MyDaciaId__c</field>
        <formula>IF( ISBLANK( MyDaciaID__c ), &apos;&apos;, FirstName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+LastName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+MyDaciaID__c )</formula>
        <name>ACC_FU_Tech_Name_MyDaciaID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_Tech_Name_MyRenaultID</fullName>
        <description>MyRenault BackEnd: computes a concatenation of Firstname, Lastname and MyRenaultId to optimize account research.
DO NOT FILL THE FIELD IS MYRENAULT IS BLANK !</description>
        <field>Tech_Name_MyRenaultId__c</field>
        <formula>IF( ISBLANK(MyRenaultID__c), &apos;&apos;, 
FirstName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+LastName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+MyRenaultID__c)</formula>
        <name>ACC_FU_Tech_Name_MyRenaultID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU_Tech_Name_PersEmailAddress</fullName>
        <description>MyRenault BackEnd: computes a concatenation of Firstname, Lastname and PersEmailAddress to optimize account research.
DO NOT FILL THE FIELD IS PERSEMAILADDRESSE IS BLANK !</description>
        <field>Tech_Name_PersEmailAddress__c</field>
        <formula>IF( ISBLANK(PersEmailAddress__c), &apos;&apos;, 
FirstName+ SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;)+LastName+SUBSTITUTE(SUBSTITUTE($Setup.CS04_MYR_Settings__c.Myr_Tech_Sosl_Delimiter__c, &apos;(&apos;, &apos;&apos;), &apos;)&apos;,&apos;&apos;) +PersEmailAddress__c)</formula>
        <name>ACC_FU_Tech_Name_PersEmailAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_BillingCity</fullName>
        <description>&quot;ShippingCity&quot; field value moved (updated or created), so update of &quot;BillingCity&quot;</description>
        <field>BillingCity</field>
        <formula>ShippingCity</formula>
        <name>ACC_MigHelios_MYR_FU_BillingCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_BillingCountry</fullName>
        <description>&quot;ShippingCountry&quot; field value moved (updated or created), so update of &quot;BillingCountry&quot;</description>
        <field>BillingCountry</field>
        <formula>ShippingCountry</formula>
        <name>ACC_MigHelios_MYR_FU_BillingCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_BillingPostalCode</fullName>
        <description>If ShippingPostalCode is updated, then BillingPostalCode is updated</description>
        <field>BillingPostalCode</field>
        <formula>ShippingPostalCode</formula>
        <name>ACC_MigHelios_MYR_FU_BillingPostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_BillingState</fullName>
        <description>&quot;ShippingState&quot; field value moved (updated or created), so update of &quot;BillingState&quot;</description>
        <field>BillingState</field>
        <formula>ShippingState</formula>
        <name>ACC_MigHelios_MYR_FU_BillingState</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_BillingStreet</fullName>
        <description>&quot;ShippingStreet&quot; field value moved (updated or created), so update of &quot;BillingStreet&quot;</description>
        <field>BillingStreet</field>
        <formula>ShippingStreet</formula>
        <name>ACC_MigHelios_MYR_FU_BillingStreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_Email</fullName>
        <description>&quot;PersEmailAddress__c&quot; field value moved (updated or created), so update of &quot;Email__c&quot;</description>
        <field>Email__c</field>
        <formula>PersEmailAddress__c</formula>
        <name>ACC_MigHelios_MYR_FU_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MYR_User_State_v1</fullName>
        <description>&quot;MYR_Status__c&quot; field value moved (updated or created) to &quot;created&quot;, so update of &quot;MYR_User_State__c&quot; to value &quot;Created&quot;</description>
        <field>MYR_User_State__c</field>
        <literalValue>Created</literalValue>
        <name>ACC_MigHelios_MYR_FU_MYR_User_State_v1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MYR_User_State_v2</fullName>
        <description>&quot;MYR_Status__c&quot; field value moved (updated or created) to &quot;activated&quot;, so update of &quot;MYR_User_State__c&quot; to value &quot;Active&quot;</description>
        <field>MYR_User_State__c</field>
        <literalValue>Active</literalValue>
        <name>ACC_MigHelios_MYR_FU_MYR_User_State_v2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MYR_User_State_v3</fullName>
        <description>&quot;MYR_Status__c&quot; field value moved (updated or created) to &quot;deleted&quot;, so update of &quot;MYR_User_State__c&quot; to value &quot;Deleted&quot;</description>
        <field>MYR_User_State__c</field>
        <literalValue>Deleted</literalValue>
        <name>ACC_MigHelios_MYR_FU_MYR_User_State_v3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v1</fullName>
        <description>&quot;MYR_User_State__c&quot; field value moved (updated or created) to &quot;Created&quot;, so update of &quot;MYR_Status__c&quot; to value &quot;created&quot;</description>
        <field>MYR_Status__c</field>
        <literalValue>created</literalValue>
        <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v2</fullName>
        <description>&quot;MYR_User_State__c&quot; field value moved (updated or created) to &quot;Active&quot;, so update of &quot;MYR_Status__c&quot; to value &quot;activated&quot;</description>
        <field>MYR_Status__c</field>
        <literalValue>activated</literalValue>
        <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v3</fullName>
        <description>&quot;MYR_User_State__c&quot; field value moved (updated or created) to &quot;Deleted&quot;, so update of &quot;MYR_Status__c&quot; to value &quot;deleted&quot;</description>
        <field>MYR_Status__c</field>
        <literalValue>deleted</literalValue>
        <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_PersEmailAddress</fullName>
        <description>&quot;Email__c&quot; field value moved (updated or created), so update of &quot;PersEmailAddress__c&quot;</description>
        <field>PersEmailAddress__c</field>
        <formula>Email__c</formula>
        <name>ACC_MigHelios_MYR_FU_PersEmailAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_ShippingCity</fullName>
        <description>&quot;BillingCity&quot; field value moved (updated or created), so update of &quot;ShippingCity&quot;</description>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>ACC_MigHelios_MYR_FU_ShippingCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_ShippingCountry</fullName>
        <description>&quot;BillingCountry&quot; field value moved (updated or created), so update of &quot;ShippingCountry&quot;</description>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>ACC_MigHelios_MYR_FU_ShippingCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_ShippingPostalCode</fullName>
        <description>If BillingPostalCode is updated, then ShippingPostalCode is updated</description>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>ACC_MigHelios_MYR_FU_ShippingPostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_ShippingState</fullName>
        <description>&quot;BillingState&quot; field value moved (updated or created), so update of &quot;ShippingState&quot;</description>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>ACC_MigHelios_MYR_FU_ShippingState</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_MigHelios_MYR_FU_ShippingStreet</fullName>
        <description>&quot;BillingStreet&quot; field value moved (updated or created), so update of &quot;ShippingStreet&quot;</description>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>ACC_MigHelios_MYR_FU_ShippingStreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_Myd_Prefered_Dealer</fullName>
        <field>MyD_Dealer_1_BIR__c</field>
        <formula>MyD_Subscriber_Dealer_BIR__c</formula>
        <name>ACC_Myd_Prefered_Dealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_Myr_MyTCLastModificationDate_Now</fullName>
        <description>MyRenault: MyTC Last Modification Date set to now</description>
        <field>MyTC_Last_Modification_Date__c</field>
        <formula>now()</formula>
        <name>ACC_Myr_MyTCLastModificationDate_Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_Myr_MyTCLastStatusDate_Now</fullName>
        <description>MyRenault: set Now() to Last Status Date MyTC_Last_Status_Date__c</description>
        <field>MyTC_Last_Status_Date__c</field>
        <formula>now()</formula>
        <name>ACC_Myr_MyTCLastStatusDate_Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_Myr_Prefered_Dealer</fullName>
        <field>MyR_Dealer_1_BIR__c</field>
        <formula>MyR_Subscriber_Dealer_BIR__c</formula>
        <name>ACC_Myr_Prefered_Dealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_NetwkAccExtIDUpdate</fullName>
        <description>When an Enterprise Account is created, Tech External ID field is update by &quot;ID BIR&quot; value. Each time &quot;ID BIR&quot; value is changed, it also updates Tech External ID value.</description>
        <field>Tech_ACCExternalID__c</field>
        <formula>IDBIR__c</formula>
        <name>ACC_NetwkAccExtIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_PersAccExtIDUpdate</fullName>
        <description>When a Personal Account is created, Tech External ID field is update by NUM CL value. Each time NUM CL value is changed, it also updates Tech External ID value.</description>
        <field>Tech_ACCExternalID__c</field>
        <formula>&quot;BR&quot;+CustomerIdentificationNbr__c</formula>
        <name>ACC_PersAccExtIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_ShippingCountry_BR</fullName>
        <field>ShippingCountry</field>
        <formula>&quot;BR&quot;</formula>
        <name>ACC_ShippingCountry_BR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateAddressNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Address&quot; picklist with &quot;No&quot; Value</description>
        <field>Address__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdateAddressNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateAddressYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Address&quot; field with &quot;Yes&quot; value</description>
        <field>Address__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdateAddressYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateComAgmentDate</fullName>
        <description>Update Communication Agreement date with today&apos;s date</description>
        <field>ComAgreemtDate__c</field>
        <formula>TODAY()</formula>
        <name>ACC_UpdateComAgmentDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateEmailwithCity</fullName>
        <field>Website</field>
        <formula>ShippingCity</formula>
        <name>ACC_UpdateEmailwithCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersoEmailNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Personal Email&quot; picklist with &quot;No&quot; Value</description>
        <field>PersEmail__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdatePersoEmailNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersoEmailYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Personal Email&quot; field with &quot;Yes&quot; value</description>
        <field>PersEmail__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdatePersoEmailYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersoMobiNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Personal Mobile Phone&quot; picklist with &quot;No&quot; Value</description>
        <field>PersMobiPhone__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdatePersoMobiNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersoMobiYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Personal Mobile Phone&quot; field with &quot;Yes&quot; value</description>
        <field>PersMobiPhone__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdatePersoMobiYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersonalPhoneNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Personal Phone&quot; picklist with &quot;No&quot; Value</description>
        <field>PersPhone__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdatePersonalPhoneNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdatePersonalPhoneYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Address&quot; field with &quot;Yes&quot; value</description>
        <field>PersPhone__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdatePersonalPhoneYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProEmailAddress</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Professional Email Address&quot; picklist with &quot;Yes&quot; value</description>
        <field>ProEmailAddress__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdateProEmailAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProEmailAddressNO</fullName>
        <description>When communication agreement =&quot;No&quot;, fill the &quot;Professional Email Address&quot; picklist with &quot;No&quot; value</description>
        <field>ProEmailAddress__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdateProEmailAddressNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProMobiNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Professional Mobile Phone&quot; picklist with &quot;No&quot; Value</description>
        <field>ProfMobiPhone__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdateProMobiNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProMobiYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Professional Mobile Phone&quot; field with &quot;Yes&quot; value</description>
        <field>ProfMobiPhone__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdateProMobiYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProPhoneNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;Professional Phone&quot; picklist with &quot;No&quot; Value</description>
        <field>ProfPhone__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdateProPhoneNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateProPhoneYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;Professional Phone&quot; field with &quot;Yes&quot; value</description>
        <field>ProfPhone__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdateProPhoneYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateSMSNO</fullName>
        <description>When communication agreement picklist =&quot;No&quot;, update &quot;SMS&quot; picklist with &quot;No&quot; Value</description>
        <field>SMS__pc</field>
        <literalValue>No</literalValue>
        <name>ACC_UpdateSMSNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateSMSYES</fullName>
        <description>When communication agreement =&quot;Yes&quot;, fill the &quot;SMS&quot; field with &quot;Yes&quot; value</description>
        <field>SMS__pc</field>
        <literalValue>Yes</literalValue>
        <name>ACC_UpdateSMSYES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_UpdateWebsitewithIDBIR</fullName>
        <field>Website</field>
        <formula>IDBIR__c + &quot;  &quot;+ ShippingCity</formula>
        <name>ACC_UpdateWebsitewithIDBIR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF13_DateValueofCreditupdate</fullName>
        <field>ValueOfCredit__c</field>
        <name>ACC_WF13_DateValueofCreditupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF14_DealerActivationDate_Update</fullName>
        <description>Updates the Dealer_Activation_Date field when a Dealer Account is activated.</description>
        <field>Dealer_activation_date__c</field>
        <formula>TODAY()</formula>
        <name>ACC_WF14_DealerActivationDate_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF15_Source_Update</fullName>
        <description>Updates the source field in account with &quot;Phone&quot; if the Sub Source is DVE - ACTIVE or DVE - RECEPTIVE.</description>
        <field>AccountSource</field>
        <literalValue>PHONE</literalValue>
        <name>ACC_WF15_Source_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF18_UpdateRGField</fullName>
        <field>RGStateRegistration__c</field>
        <formula>VALUE(RgStateTexto__c)</formula>
        <name>ACC_WF18_UpdateRGField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF18_UpdateRGField_II</fullName>
        <field>RGStateRegistration__c</field>
        <formula>0</formula>
        <name>ACC_WF18_UpdateRGField II</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF20_Update_MyR_Prefered_Dealer_Date</fullName>
        <description>If the value of “MyR Prefered Dealer” (MyR_Dealer_1_BIR__c)  is updated, the field « MyR Prefered Dealer Update Date » (MyR_Prefered_Dealer_Update_Date__c) is updated to date of the day</description>
        <field>MyR_Prefered_Dealer_Update_Date__c</field>
        <formula>now()</formula>
        <name>ACC_WF20_Update_MyR_Prefered_Dealer_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF21_Update_MyD_Prefered_Dealer_Date</fullName>
        <description>If the value of “MyD Prefered Dealer” (MyD_Dealer_1_BIR__c) is updated, the field « MyD Prefered Dealer Update Date » (MyD_Prefered_Dealer_Update_Date__c) is updated to date of the day</description>
        <field>MyD_Prefered_Dealer_Update_Date__c</field>
        <formula>now()</formula>
        <name>ACC_WF21_Update_MyD_Prefered_Dealer_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF22_Billing_Address_Update_Date</fullName>
        <description>If the value of “Billing Address” (Account.BillingAddress)  is updated or new (creation), the field « Billing Address Update Date » (Account.Billing_Address_Update_Date__c) is updated to the current time.</description>
        <field>Billing_Address_Update_Date__c</field>
        <formula>now()</formula>
        <name>ACC_WF22_Billing_Address_Update_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF23_Purchase_Intention_Update_Date</fullName>
        <description>It the values of &quot;My Purchase Intention Date&quot; or &quot;My Purchase Intention Model&quot; are inserted or updated, then the field &quot;My Purchase Intention Updated Date&quot; is updated</description>
        <field>My_Purchase_Intention_Update_Date__c</field>
        <formula>now()</formula>
        <name>ACC_WF23_Purchase_Intention_Update_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF26_UpdateBillingPostalCode</fullName>
        <field>BillingPostalCode</field>
        <formula>IF(CONTAINS(BillingPostalCode, &quot;-&quot;), SUBSTITUTE( BillingPostalCode, &quot;-&quot;, &quot;&quot;),  
	IF(CONTAINS(BillingPostalCode, &quot;+&quot;), SUBSTITUTE( BillingPostalCode, &quot;+&quot;, &quot;&quot;),
		IF(CONTAINS(BillingPostalCode, &quot;.&quot;), SUBSTITUTE( BillingPostalCode, &quot;.&quot;, &quot;&quot;),BillingPostalCode
		)
	)
)</formula>
        <name>ACC_WF26_UpdateBillingPostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF26_UpdateShippingPostalCode</fullName>
        <field>ShippingPostalCode</field>
        <formula>IF(CONTAINS(ShippingPostalCode, &quot;-&quot;), SUBSTITUTE( ShippingPostalCode, &quot;-&quot;, &quot;&quot;), 
IF(CONTAINS(ShippingPostalCode, &quot;+&quot;), SUBSTITUTE( ShippingPostalCode, &quot;+&quot;, &quot;&quot;), 
IF(CONTAINS(ShippingPostalCode, &quot;.&quot;), SUBSTITUTE( ShippingPostalCode, &quot;.&quot;, &quot;&quot;),ShippingPostalCode
) 
) 
)</formula>
        <name>ACC_WF26_UpdateShippingPostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF41_Tech_FLP_PersMobPhone</fullName>
        <description>Backup a concatenation of firstname, lastname and landline phone. The storing of this concatenation is dedicated to SOSL queries (like into the matching key FLP)</description>
        <field>Tech_Name_MobilePhone__c</field>
        <formula>MID(FirstName, 0, 108) &amp;  MID(LastName, 0, 107) &amp; PersMobPhone__c</formula>
        <name>ACC_WF41_Tech_FLP_PersMobPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_WF42_Tech_FLP_HomePhone</fullName>
        <description>Backup a concatenation of firstname, lastname and homephone. The storing of this concatenation is dedicated to SOSL queries (like into the matching key FLP)</description>
        <field>Tech_Name_LandlinePhone__c</field>
        <formula>MID(FirstName, 0, 108) &amp;  MID(LastName, 0, 107) &amp; HomePhone__c</formula>
        <name>ACC_WF42_Tech_FLP_HomePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_Update_Emial</fullName>
        <description>R-Force:Release 4
The standard email address field is filed with the personal Email Address</description>
        <field>PersonEmail</field>
        <formula>IF( PersEmailAddress__c!=NULL,PersEmailAddress__c, ProfEmailAddress__c )</formula>
        <name>Acc Update Emial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_BillingCity</fullName>
        <field>BillingCity</field>
        <formula>Billing_City__c</formula>
        <name>Account: Standard &apos;BillingCity&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_BillingCountry</fullName>
        <field>BillingCountry</field>
        <formula>Billing_Country__c</formula>
        <name>Account: Standard &apos;BillingCountry&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_BillingPostalCode</fullName>
        <field>BillingPostalCode</field>
        <formula>Billing_PostalCode__c</formula>
        <name>Account: Standard &apos;BillingPostalCode&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_BillingState</fullName>
        <field>BillingState</field>
        <formula>Billing_State__c</formula>
        <name>Account: Standard &apos;BillingState&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_BillingStreet</fullName>
        <field>BillingStreet</field>
        <formula>Billing_Street__c + 
IF( Billing_Number__c &lt;&gt; null, &quot;, &quot; + Billing_Number__c, &quot;&quot;) + 
IF( Billing_Complement__c &lt;&gt; null, &quot; - &quot; + Billing_Complement__c, &quot;&quot;) + 
IF( Billing_Neighborhood__c &lt;&gt; null, &quot; - &quot; + Billing_Neighborhood__c, &quot;&quot;)</formula>
        <name>Account: Standard &apos;BillingStreet&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_ShippingCity</fullName>
        <field>ShippingCity</field>
        <formula>Shipping_City__c</formula>
        <name>Account: Standard &apos;ShippingCity&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_ShippingCountry</fullName>
        <field>ShippingCountry</field>
        <formula>Shipping_Country__c</formula>
        <name>Account: Standard &apos;ShippingCountry&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_ShippingPostalCode</fullName>
        <field>ShippingPostalCode</field>
        <formula>Shipping_PostalCode__c</formula>
        <name>Account: Standard &apos;ShippingPostalCode&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_ShippingState</fullName>
        <field>ShippingState</field>
        <formula>Shipping_State__c</formula>
        <name>Account: Standard &apos;ShippingState&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Standard_ShippingStreet</fullName>
        <field>ShippingStreet</field>
        <formula>Shipping_Street__c + 
IF( Shipping_Number__c &lt;&gt; null, &quot;, &quot; + Shipping_Number__c, &quot;&quot;) + 
IF( Shipping_Complement__c &lt;&gt; null, &quot; - &quot; + Shipping_Complement__c, &quot;&quot;) + 
IF( Shipping_Neighborhood__c &lt;&gt; null, &quot; - &quot; + Shipping_Neighborhood__c, &quot;&quot;)</formula>
        <name>Account: Standard &apos;ShippingStreet&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AddressDate</fullName>
        <field>AddressDate__pc</field>
        <formula>IF(NOT(ISBLANK(TEXT(Address__pc))),TODAY(),null)</formula>
        <name>AddressDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dealer_Without_Offer_Site_Activities</fullName>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Dealer Without Offer Site Activities</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geolocalization_status_None</fullName>
        <description>Geolocalization status set to &apos;To Be Done&apos;</description>
        <field>Geolocalization_Status__c</field>
        <literalValue>Geolocalization_ToBeDone</literalValue>
        <name>Geolocalization_status_None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Latitude_set_null</fullName>
        <field>Latitude__c</field>
        <name>Latitude_set_null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Longitude_set_null</fullName>
        <field>Longitude__c</field>
        <name>Longitude_set_null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonalEmail</fullName>
        <field>PersEmailDate__pc</field>
        <formula>IF(NOT(ISBLANK(TEXT(PersEmail__pc))),TODAY(),null)</formula>
        <name>PersonalEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonalMobilePhone</fullName>
        <field>PersMobiPhoneDate__pc</field>
        <formula>IF(NOT(ISBLANK(TEXT(PersMobiPhone__pc))),TODAY(),null)</formula>
        <name>PersonalMobilePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonalPhone</fullName>
        <field>PersPhoneDate__pc</field>
        <formula>TODAY()</formula>
        <name>PersonalPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProfessionalEmailAddress</fullName>
        <field>EmailAddDate__pc</field>
        <formula>TODAY()</formula>
        <name>ProfessionalEmailAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProfessionalMobilePhone</fullName>
        <field>ProfMobiPhoneDate__pc</field>
        <formula>TODAY()</formula>
        <name>ProfessionalMobilePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProfessionalPhone</fullName>
        <field>ProPhoneDate__pc</field>
        <formula>TODAY()</formula>
        <name>ProfessionalPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS</fullName>
        <field>SMSDate__pc</field>
        <formula>IF(NOT(ISBLANK(TEXT(SMS__pc))),TODAY(),null)</formula>
        <name>SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Email</fullName>
        <description>Updates the default email field, when a account is created.</description>
        <field>PersonEmail</field>
        <formula>PersEmailAddress__c</formula>
        <name>Update Account Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Email_2</fullName>
        <field>PersonEmail</field>
        <formula>PersEmailAddress__c</formula>
        <name>Update Account Email 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webservice_PV_Always_Active</fullName>
        <field>Active_PV__c</field>
        <literalValue>1</literalValue>
        <name>Webservice PV Always Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACC_Billing_Shipping_Country_BR</fullName>
        <actions>
            <name>ACC_BillingCountry_BR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_ShippingCountry_BR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( BillingCountry = &quot;Brazil&quot;, ShippingCountry = &quot;Brazil&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_DealershipLanguage</fullName>
        <actions>
            <name>ACC_DealershipLanguage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>DealershipLanguage__c = &quot;Portuguese&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MYR_WF01_SetGeoLocalizationInformation</fullName>
        <actions>
            <name>Geolocalization_status_None</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Latitude_set_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Longitude_set_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the geolocalization information to null when the address has been modified.</description>
        <formula>AND(      $RecordType.Name=&quot;Personal Account&quot;,      OR(       ISCHANGED(ShippingCity),      ISCHANGED(ShippingState),      ISCHANGED(ShippingStreet),      ISCHANGED(ShippingPostalCode),      ISCHANGED(ShippingCountry)      ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_BillingCity</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_BillingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ShippingCity is updated, then BillingCity is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( ShippingCity )), ISBLANK(ShippingCity) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_BillingCountry</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_BillingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ShippingCountry is updated, then BillingCountry is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( ShippingCountry )), ISBLANK(ShippingCountry ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_BillingPostalCode</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_BillingPostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ShippingPostalCode is updated, then BillingPostalCode is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED(ShippingPostalCode)),  ISBLANK(ShippingPostalCode) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_BillingState</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_BillingState</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ShippingState is updated, then BillingState is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( ShippingState )),  ISBLANK(ShippingState ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_BillingStreet</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_BillingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If ShippingStreet is updated, then BillingStreet is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( ShippingStreet )),  ISBLANK(ShippingStreet ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_Email</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If PersEmailAddress__c is updated, then Email__c is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( PersEmailAddress__c )),  ISBLANK(PersEmailAddress__c) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_Status_v1</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_User_State__c is updated with value &quot;Created&quot;, then MYR_Status__c is updated with value &quot;created&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_User_State__c)),  $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_User_State__c , &quot;Created&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_Status_v2</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_User_State__c is updated with value &quot;Active&quot;, then MYR_Status__c is updated with value &quot;activated&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_User_State__c)),  $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_User_State__c , &quot;Active&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_Status_v3</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MyR_MYR_Status_v3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_User_State__c is updated with value &quot;Deleted&quot;, then MYR_Status__c is updated with value &quot;deleted&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_User_State__c)), $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_User_State__c , &quot;Deleted&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_User_State_v1</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MYR_User_State_v1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_Status__c is updated with value &quot;created&quot;, then MYR_User_State__c is updated with value &quot;Created&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_Status__c)), $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_Status__c , &quot;created&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_User_State_v2</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MYR_User_State_v2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_Status__c is updated with value &quot;activated&quot;, then MYR_User_State__c is updated with value &quot;Active&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_Status__c)), $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_Status__c , &quot;activated&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_MYR_User_State_v3</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_MYR_User_State_v3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If MYR_Status__c is updated with value &quot;deleted&quot;, then MYR_User_State__c is updated with value &quot;Deleted&quot;</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( MYR_Status__c)), $User.BypassWF__c &lt;&gt; True, (ISPICKVAL(MYR_Status__c , &quot;deleted&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_PersEmailAddress</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_PersEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Email__c is updated, then PersEmailAddress__c  is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( Email__c )),  ISBLANK(Email__c) = False,$User.BypassWF__c &lt;&gt; True  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_ShippingCity</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_ShippingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If BillingCity is updated, then ShippingCity is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( BillingCity )),  ISBLANK(BillingCity ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_ShippingCountry</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_ShippingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If BillingCountry is updated, then ShippingCountry is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( BillingCountry )), ISBLANK(BillingCountry ) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_ShippingPostalCode</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_ShippingPostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If BillingPostalCode is updated, then ShippingPostalCode is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( BillingPostalCode )), ISBLANK(BillingPostalCode) = False,$User.BypassWF__c &lt;&gt; True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_ShippingState</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_ShippingState</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If BillingState is updated, then ShippingState is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( BillingState )),  ISBLANK(BillingState ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_MigHelios_MYR_Update_ShippingStreet</fullName>
        <actions>
            <name>ACC_MigHelios_MYR_FU_ShippingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If BillingStreet updated, then ShippingStreet is updated</description>
        <formula>AND( IsPersonAccount = True , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( BillingStreet )),  ISBLANK(BillingStreet ) = False,$User.BypassWF__c &lt;&gt; True   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF01_ComAgmentDate</fullName>
        <actions>
            <name>ACC_UpdateComAgmentDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Communication Agreement Date with Today&apos;s date when Communication Agreement picklist is filled (‘Central Communication Agreement Rules cover the Agreement date Updates’. Corrected for Regression.and approved by Central Business.)03-02-14</description>
        <formula>OR(ISCHANGED( ComAgreemt__c), ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF02_ComAgmentYES</fullName>
        <actions>
            <name>ACC_UpdateAddressYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersoEmailYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersoMobiYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersonalPhoneYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProMobiYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProPhoneYES</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateSMSYES</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When Communication Agreement =YES fill all picklist in communication section with &quot;Yes&quot; Value</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF02_ComAgmentYES_Part2</fullName>
        <actions>
            <name>AddressDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When Communication Agreement =YES fill all picklist in communication section with &quot;Yes&quot; Value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF03_ComAgmentNO</fullName>
        <actions>
            <name>ACC_UpdateAddressNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersoEmailNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersoMobiNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdatePersonalPhoneNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProEmailAddressNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProMobiNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateProPhoneNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateSMSNO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>When &quot;Communication Agreement&quot;=&quot;No&quot;, all picklist value in communication agreeemnt section =&quot;No&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF03_ComAgmentNO_Part2</fullName>
        <actions>
            <name>AddressDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PersonalPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ProfessionalPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ComAgreemt__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>When &quot;Communication Agreement&quot;=&quot;No&quot;, all picklist value in communication agreeemnt section =&quot;No&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF04_EnterpAccExtIDUpdate</fullName>
        <actions>
            <name>ACC_EnterpAccExtIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an Enterprise Account is created, Tech External ID field is update by &quot;Intracommunity VAT Number&quot; value. Each time &quot;Intracommunity VAT Number&quot; value is changed, it also updates Tech External ID value.</description>
        <formula>AND($RecordType.Name =&quot;Enterprise Account&quot; , (ISPICKVAL(Country__c , &quot;Brazil&quot;)),  OR(ISNEW() ,ISCHANGED( IntracomntyVATNbr__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF05_NetwkAccExtIDUpdate</fullName>
        <actions>
            <name>ACC_NetwkAccExtIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Network Account is created, Tech External ID field is update by &quot;ID BIR&quot; value. Each time &quot;ID Bir&quot; value is changed, it also updates Tech External ID value.</description>
        <formula>AND(OR($RecordType.DeveloperName  =&quot;Network_Acc&quot;, $RecordType.DeveloperName  =&quot;Network_Site_Acc&quot;), OR(ISNEW() ,ISCHANGED( IDBIR__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF06_PersAccExtIDUpdate</fullName>
        <actions>
            <name>ACC_PersAccExtIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Personal Account is created, Tech External ID field is update by Country + &quot;Customer Identification Number&quot; value. Each time &quot;Customer Identification Number&quot; value is changed, it also updates Tech External ID value.</description>
        <formula>AND($RecordType.Name =&quot;Personal Account&quot; , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( CustomerIdentificationNbr__c )),  ISBLANK(CustomerIdentificationNbr__c) = False  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF07_MainCityinWebsite</fullName>
        <actions>
            <name>ACC_UpdateEmailwithCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Website field with main city. For lookup search purpose</description>
        <formula>AND( OR($RecordType.DeveloperName =&quot;Enterprise_Acc&quot;,  $RecordType.DeveloperName =&quot;Personal_Acc&quot;,$RecordType.DeveloperName =&quot;SRC_Acc&quot;,$RecordType.DeveloperName =&quot;Other_Acc&quot;),OR( ISNEW(),  ISCHANGED( ShippingCity ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF08_IDBIRCityinWebsite</fullName>
        <actions>
            <name>ACC_UpdateWebsitewithIDBIR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Website field with ID BIR. For lookup search purpose</description>
        <formula>AND( OR($RecordType.DeveloperName =&quot;Network_Acc&quot;,  $RecordType.DeveloperName =&quot;Network_Site_Acc&quot;),OR( ISNEW(),  ISCHANGED(  IDBIR__c  ),  ISCHANGED(   ShippingCity   ), ISCHANGED(    UsualName__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF09_Communication_Agreement</fullName>
        <actions>
            <name>ACC_CommunicationAgrmntYes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateComAgmentDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When all the global communication agreement fields are made yes the communication agreement is made yes</description>
        <formula>AND(PRIORVALUE(CommAgreementchange__c) !=CommAgreementchange__c ,  CommAgreementchange__c==8)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF10_Communication_Agreement</fullName>
        <actions>
            <name>ACC_CommunicationAgrmntNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateComAgmentDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If global communication agreement fields are made no then communication agreement field is made no</description>
        <formula>AND(PRIORVALUE(CommAgreementchange__c) !=CommAgreementchange__c , CommAgreementchange__c==0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF11_Communication_Agreement</fullName>
        <actions>
            <name>ACC_CommunicationAgrmntPartial</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_UpdateComAgmentDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If any of the global communication agreement field is yes then global communication agreement field is made &apos;Partial&apos;</description>
        <formula>AND(OR(ISPICKVAL(ComAgreemt__c,&apos;Yes&apos;),ISPICKVAL(ComAgreemt__c,&apos;No&apos;)),CommAgreementchange__c==1)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF12_Network Site Account Creation</fullName>
        <actions>
            <name>Brazilian_Network_Site_Account_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IDBIR__c</field>
            <operation>startsWith</operation>
            <value>760</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Network Site Account</value>
        </criteriaItems>
        <description>Send an Email informing the creation of a Brazilian Network Site Account</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF13_DateValueofCreditupdate</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.ValueOfCredit__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Date_of_Value_of_Credit_update__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <description>When the Value of Credit field is changed, updates the field 
Date_of_Value_of_Credit_update__c if this field &gt;90 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ACC_WF13_DateValueofCreditupdate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Date_of_Value_of_Credit_update__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ACC_WF14_DealerActivationDate_Update</fullName>
        <actions>
            <name>ACC_WF14_DealerActivationDate_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.isDealerActive__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <description>Updates the Dealer_Activation_Date field when a Dealer Account is activated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF15_MyD_Status</fullName>
        <actions>
            <name>ACC_FU_MyD_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;MyD Status&quot; field value moved (updated or created), so update of &quot;MyD Status UpdateDate&quot;</description>
        <formula>ISCHANGED(MyD_Status__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF15_SMS_Agreement</fullName>
        <actions>
            <name>SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update SMS agreement date when SMS agreement is changed</description>
        <formula>(ISCHANGED(SMS__pc)||ISNEW()) &amp;&amp; $User.BypassWF__c &lt;&gt; True &amp;&amp; RecordType.Name =&apos;CORE-ACC-Personal Account RecType&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF15_Source_Update</fullName>
        <actions>
            <name>ACC_WF15_Source_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.AccSubSource__c</field>
            <operation>equals</operation>
            <value>DVE - ACTIVE,DVE - RECEPTIVE</value>
        </criteriaItems>
        <description>Updates the source field in account with &quot;Phone&quot; if the Sub Source is DVE - ACTIVE or DVE - RECEPTIVE.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF16_CompanyAccExtIDUpdate</fullName>
        <actions>
            <name>ACC_CompanyAccExtIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_CompanyAccExtIDUpdate4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When a Company Account is created, Tech External ID field is update by Country + &quot;Customer Identification Number&quot; value. Each time &quot;Customer Identification Number&quot; value is changed, it also updates Tech External ID value.</description>
        <formula>AND( RecordType.Id=&quot;012D0000000KAoF&quot; , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( CustomerIdentificationNbr__c )),  ISBLANK(CustomerIdentificationNbr__c) = False  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF16_Mobile_Agreement</fullName>
        <actions>
            <name>PersonalMobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Mobile agreement date when Mobile agreement is changed</description>
        <formula>(ISCHANGED(PersMobiPhone__pc)||ISNEW()) &amp;&amp; $User.BypassWF__c &lt;&gt; True &amp;&amp; RecordType.Name =&apos;CORE-ACC-Personal Account RecType&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF16_MyR_Status</fullName>
        <actions>
            <name>ACC_FU_MyR_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;MyR Status&quot; field value moved (updated or created), so update of &quot;MyR Status UpdateDate&quot;</description>
        <formula>ISCHANGED(MYR_Status__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF17_CompanyAccExtIDUpdate</fullName>
        <actions>
            <name>ACC_CompanyAccExtIDUpdate2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_CompanyAccExtIDUpdate3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When a Company Account is created, Tech External ID field is update by Country + &quot;Company Id&quot; value. Each time &quot;Company Id&quot; value is changed, it also updates Tech External ID value.</description>
        <formula>AND( RecordType.Id=&quot;012D0000000KAoF&quot; , (ISPICKVAL(Country__c , &quot;Brazil&quot;)), OR(ISNEW() ,ISCHANGED( CompanyID__c )),  ISBLANK(CompanyID__c) = False  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF17_Email_Agreement</fullName>
        <actions>
            <name>PersonalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Email agreement date when Email agreement is changed</description>
        <formula>(ISCHANGED(PersEmail__pc)||ISNEW()) &amp;&amp; $User.BypassWF__c &lt;&gt; True &amp;&amp; RecordType.Name =&apos;CORE-ACC-Personal Account RecType&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF18_Address_Agreement</fullName>
        <actions>
            <name>AddressDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Address agreement date when Address agreement is changed</description>
        <formula>(ISCHANGED(Address__pc)||ISNEW()) &amp;&amp; $User.BypassWF__c &lt;&gt; True &amp;&amp; RecordType.Name =&apos;CORE-ACC-Personal Account RecType&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF18_UpdateRGField</fullName>
        <actions>
            <name>ACC_WF18_UpdateRGField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (ISNUMBER(RgStateTexto__c),  ISCHANGED(RgStateTexto__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF18_UpdateRGField II</fullName>
        <actions>
            <name>ACC_WF18_UpdateRGField_II</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (NOT (ISNUMBER(RgStateTexto__c)), ISCHANGED(RgStateTexto__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF20_Update_MyR_Prefered_Dealer_Date</fullName>
        <actions>
            <name>ACC_WF20_Update_MyR_Prefered_Dealer_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the value of “MyR Prefered Dealer” (MyR_Dealer_1_BIR__c)  is updated, the field « MyR Prefered Dealer Update Date » (MyR_Prefered_Dealer_Update_Date__c) is updated to current time</description>
        <formula>ISCHANGED(MyR_Dealer_1_BIR__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF21_Update_MyD_Prefered_Dealer_Date</fullName>
        <actions>
            <name>ACC_WF21_Update_MyD_Prefered_Dealer_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the value of “MyD Prefered Dealer” (MyD_Dealer_1_BIR__c)  is updated, the field « MyD Prefered Dealer Update Date » (MyD_Prefered_Dealer_Update_Date__c) is updated to current time</description>
        <formula>ISCHANGED(MyD_Dealer_1_BIR__c) &amp;&amp; $User.BypassWF__c &lt;&gt; True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF22_Billing_Address_Update_Date</fullName>
        <actions>
            <name>ACC_WF22_Billing_Address_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the value of “Billing Address” (Account.BillingAddress)  is updated or new (creation), the field « Billing Address Update Date » (Account.Billing_Address_Update_Date__c) is updated to the current time.</description>
        <formula>$User.BypassWF__c &lt;&gt; True &amp;&amp; (      ISNEW()   || ISCHANGED(BillingCity)   || ISCHANGED(BillingCountry)   || ISCHANGED(BillingPostalCode)   || ISCHANGED(BillingState)   || ISCHANGED(BillingStreet) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF23_Purchase_Intention_Update_Date</fullName>
        <actions>
            <name>ACC_WF23_Purchase_Intention_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It the values of &quot;My Purchase Intention Date&quot; or &quot;My Purchase Intention Model&quot; are inserted or updated, then the field &quot;My Purchase Intention Updated Date&quot; is updated</description>
        <formula>$User.BypassWF__c &lt;&gt; True &amp;&amp; ( ISNEW() || ISCHANGED(My_Purchase_Intention_Date__c) || ISCHANGED(My_Purchase_Intention_Model__c) || ISCHANGED(My_Purchase_Intention_Brand__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF24_Update_MyR_Prefered_Dealer_Subscriber</fullName>
        <actions>
            <name>ACC_Myr_Prefered_Dealer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MyRenault: the field Myr_Dealer_BIR_1__c is set with the value of MyR_Subscriber_Dealer_BIR__c 
 when the account is inserted or when the field MyR_Subscriber_Dealer_BIR__c is modified whereas it was empty before.</description>
        <formula>OR ( AND(ISNEW(), ISBLANK(MyR_Dealer_1_BIR__c)),       AND(ISCHANGED( MyR_Subscriber_Dealer_BIR__c ),         ISBLANK(PRIORVALUE( MyR_Subscriber_Dealer_BIR__c )), ISBLANK(MyR_Dealer_1_BIR__c) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF25_Update_MyD_Prefered_Dealer_Subscriber</fullName>
        <actions>
            <name>ACC_Myd_Prefered_Dealer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MyRenault: the field MyD_Dealer_BIR_1__c is set with the value of MyD_Subscriber_Dealer_BIR__c when the account is inserted or when the field MyD_Subscriber_Dealer_BIR__c is modified whereas it was empty before.</description>
        <formula>OR (  AND(ISNEW(), ISBLANK(MyD_Dealer_1_BIR__c)), AND(ISCHANGED( MyD_Subscriber_Dealer_BIR__c ), ISBLANK(PRIORVALUE( MyD_Subscriber_Dealer_BIR__c )), ISBLANK( MyD_Dealer_1_BIR__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF26_MyTCAcceptance_LastStatusDate</fullName>
        <actions>
            <name>ACC_Myr_MyTCLastStatusDate_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MyRenault: field MyTC_Last_Status_Date__c is empty at the creation is the value ifs filled with something else than Yes or No, then filled when MyTC_Acceptance__c is filled with Yes Or No</description>
        <formula>AND(      OR( ISNEW(), ISCHANGED(MyTC_Acceptance__c ) ),     OR( ISPICKVAL(MyTC_Acceptance__c, &apos;Yes&apos; ), ISPICKVAL(MyTC_Acceptance__c, &apos;No&apos; ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF27_MyTCAcceptance_LastModificationDate</fullName>
        <actions>
            <name>ACC_Myr_MyTCLastModificationDate_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MyRenault: filled when MyTC Acceptance is changed whatever the value</description>
        <formula>OR( AND(ISNEW(), TEXT(MyTC_Acceptance__c)!=null), ISCHANGED( MyTC_Acceptance__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF40_Tech_AccountResearchFields</fullName>
        <actions>
            <name>ACC_FU_Tech_Name_LocalCustomerId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU_Tech_Name_MyDaciaID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU_Tech_Name_MyRenaultID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU_Tech_Name_PersEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <description>MyRenault BackEnd: computes the research fields (Tech fiuelds) to optimize the account research while creating MyRenault/MyDacia accounts through the webservices.
This workflow should not be bypassed otherwise, Myr won&apos;t be able to match the accounts.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF41_Tech_FLP_PersMobPhone</fullName>
        <actions>
            <name>ACC_WF41_Tech_FLP_PersMobPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Detect any change on fields : firstname or lastname or PersMobPhone__c</description>
        <formula>ISCHANGED(FirstName) || ISCHANGED(LastName) || ISCHANGED(PersMobPhone__c)||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC_WF42_Tech_FLP_HomePhone</fullName>
        <actions>
            <name>ACC_WF42_Tech_FLP_HomePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Detect any change on fields : firstname or lastname or HomePhone__c</description>
        <formula>ISCHANGED(FirstName) || ISCHANGED(LastName) || ISCHANGED(HomePhone__c) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Populate Standard BillingAddress</fullName>
        <actions>
            <name>Account_Standard_BillingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_BillingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_BillingPostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_BillingState</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_BillingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Billing_PostalCode__c ) ||  ISCHANGED( Billing_Street__c ) ||  ISCHANGED( Billing_Complement__c ) ||  ISCHANGED( Billing_Number__c ) ||  ISCHANGED( Billing_Neighborhood__c ) ||  ISCHANGED( Billing_City__c ) ||  ISCHANGED( Billing_State__c ) ||  ISCHANGED( Billing_Country__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Populate Standard ShippingAddress</fullName>
        <actions>
            <name>Account_Standard_ShippingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_ShippingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_ShippingPostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_ShippingState</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Standard_ShippingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Shipping_PostalCode__c ) ||  ISCHANGED( Shipping_Street__c ) ||  ISCHANGED( Shipping_Complement__c ) ||  ISCHANGED( Shipping_Number__c ) ||  ISCHANGED( Shipping_Neighborhood__c ) ||  ISCHANGED( Shipping_City__c ) ||  ISCHANGED( Shipping_State__c ) ||  ISCHANGED( Shipping_Country__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Core Acc WF01 Email</fullName>
        <actions>
            <name>Acc_Update_Emial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <description>R-Force:Release 4
The standard email address field is filed with the personal Email Address</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DVR - Deactivate Offer Site if Dealer Inactive</fullName>
        <actions>
            <name>Dealer_Without_Offer_Site_Activities</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
    RecordType.Id = &quot;012D0000000KAoH&quot;, 
    BEGINS(IDBIR__c, &quot;7600&quot;), 
    TEXT(Country__c) = &quot;Brazil&quot;,
    Text(Dealer_Status__c) &lt;&gt; &quot;Active&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dealer Without Offer Site Activities</fullName>
        <actions>
            <name>Dealer_Without_Offer_Site_Activities</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
    NOT(INCLUDES(Activity__c, &quot;1.1.14&quot;)),
    NOT(INCLUDES(Activity__c, &quot;1.1.15&quot;)),
    RecordType.Id = &quot;012D0000000KAoH&quot;,
    BEGINS(IDBIR__c, &quot;7600&quot;),
    TEXT(Country__c) = &quot;Brazil&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Email</fullName>
        <actions>
            <name>Update_Account_Email_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.PersEmailAddress__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Company Account,Network Account,Network Site Account,Other Account,SRC Account,Company Account CN,CORE-ACC-Company Account RecType,Personal Account CN,CORE-ACC-Personal Account RecType</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Webservice PV Always Active</fullName>
        <actions>
            <name>Webservice_PV_Always_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
    RecordType.Id = &quot;012D0000000KAoH&quot;,
    BEGINS(IDBIR__c, &quot;7600&quot;),
    TEXT(Country__c) = &quot;Brazil&quot;,
    $User.BypassWF__c = FALSE,
    NOT(
        OR(
           Id = &quot;001D000001hACXS&quot;,
           Id = &quot;0015700001r1xEp&quot;
        )
    )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
