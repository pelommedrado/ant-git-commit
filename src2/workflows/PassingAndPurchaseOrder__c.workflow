<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PPO_Update_Daily_Goal</fullName>
        <field>Daily_Goal__c</field>
        <formula>VALUE(
CASE( Regional__c,
&quot;R1&quot;,  $Label.PPODailyGoal_R1, 
&quot;R2&quot;,  $Label.PPODailyGoal_R2, 
&quot;R3&quot;,  $Label.PPODailyGoal_R3, 
&quot;R4&quot;,  $Label.PPODailyGoal_R4, 
&quot;R5&quot;,  $Label.PPODailyGoal_R5, 
$Label.PPODailyGoal_R6)
)</formula>
        <name>PPO - Update Daily Goal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_Edit_Retroactive_Field</fullName>
        <field>Edit_Retroactive__c</field>
        <literalValue>0</literalValue>
        <name>PPO - Update Edit Retroactive Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_PPO_External_ID</fullName>
        <field>PPO_External_ID__c</field>
        <formula>if (Feirao__c,
SUBSTITUTE(Account__r.IDBIR__c+TEXT( Date__c)+ &quot;F&quot;, &quot;-&quot;, &quot;&quot;),
SUBSTITUTE(Account__r.IDBIR__c+TEXT( Date__c), &quot;-&quot;, &quot;&quot;))</formula>
        <name>PPO - Update PPO External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_Sent_Date</fullName>
        <field>First_Sent_Date__c</field>
        <formula>NOW()</formula>
        <name>PPO - Update Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_Sent_Field</fullName>
        <field>Sent__c</field>
        <literalValue>1</literalValue>
        <name>PPO - Update Sent Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_Sent_Field_II</fullName>
        <field>Sent__c</field>
        <literalValue>0</literalValue>
        <name>PPO - Update Sent Field II</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>PPO - Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_VSF_field</fullName>
        <field>VFS__c</field>
        <literalValue>1</literalValue>
        <name>PPO - Update VSF field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPO_Update_VSF_field_II</fullName>
        <field>VFS__c</field>
        <literalValue>0</literalValue>
        <name>PPO - Update VSF field II</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PPO - Update Daily Goal</fullName>
        <actions>
            <name>PPO_Update_Daily_Goal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update Edit Retroactive Field</fullName>
        <actions>
            <name>PPO_Update_Edit_Retroactive_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PassingAndPurchaseOrder__c.Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update PPO External ID</fullName>
        <actions>
            <name>PPO_Update_PPO_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update Sent Date</fullName>
        <actions>
            <name>PPO_Update_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISPICKVAL( Status__c, &quot;Sent&quot;),  TEXT(First_Sent_Date__c) = &quot;&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update Sent Field</fullName>
        <actions>
            <name>PPO_Update_Sent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Status__c , &quot;Sent&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update Sent Field II</fullName>
        <actions>
            <name>PPO_Update_Sent_Field_II</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Status__c , &quot;Draft&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update Status</fullName>
        <actions>
            <name>PPO_Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Edit_Retroactive__c ) , Edit_Retroactive__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update VSF field</fullName>
        <actions>
            <name>PPO_Update_VSF_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the VSF field with true when the day of the week is Friday, Saturday and Sunday.</description>
        <formula>IF(CASE(   MOD(  Date__c - DATE( 1900, 1, 7 ), 7 ),   0, &quot;VSF&quot;,   1, &quot;WEEK&quot;,   2, &quot;WEEK&quot;,   3, &quot;WEEK&quot;,   4, &quot;WEEK&quot;,   5, &quot;VSF&quot;,    &quot;VSF&quot; ) =&quot;VSF&quot;, TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PPO - Update VSF field II</fullName>
        <actions>
            <name>PPO_Update_VSF_field_II</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the VSF field with false when the day of the week is different of Friday, Saturday and Sunday.</description>
        <formula>IF(CASE(   MOD(  Date__c - DATE( 1900, 1, 7 ), 7 ),   0, &quot;VSF&quot;,   1, &quot;WEEK&quot;,   2, &quot;WEEK&quot;,   3, &quot;WEEK&quot;,   4, &quot;WEEK&quot;,   5, &quot;VSF&quot;,    &quot;VSF&quot; ) =&quot;WEEK&quot;, TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
