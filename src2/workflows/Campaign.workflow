<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CPG_IdBir</fullName>
        <description>Atualiza o campo ID BIR para ser utilizado na regra de compartilhamento.</description>
        <field>ID_BIR__c</field>
        <formula>IF( Dealer__c &lt;&gt; null, Dealer__r.IDBIR__c, Text(0))</formula>
        <name>CPG_IdBir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPG_UpdateDBMCampaignCode</fullName>
        <field>DBMCampaignCode__c</field>
        <formula>Parent.DBMCampaignCode__c+&quot;-&quot;+Dealer__r.IDBIR__c</formula>
        <name>CPG_UpdateDBMCampaignCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CPG_WF03_UpdateIDBIR</fullName>
        <actions>
            <name>CPG_IdBir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPG_WF04_GenerationCampaignCode</fullName>
        <actions>
            <name>CPG_UpdateDBMCampaignCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update DBM Campaign Code field for generation code automatic.</description>
        <formula>AND(NOT(ISBLANK(Dealer__c)),NOT(ISBLANK(ParentId)),  RecordType.Name = &apos;Single Campaign&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
