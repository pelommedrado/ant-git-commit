<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PDCT01_OptionalsUpdate</fullName>
        <description>Updates the Optionals field with the Optionals_DVE field content.</description>
        <field>Optionals__c</field>
        <formula>Optionals_DVE__c</formula>
        <name>PDCT01_OptionalsUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PDCT01_OptionalsUpdate</fullName>
        <actions>
            <name>PDCT01_OptionalsUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Optionals_DVE__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>equals</operation>
            <value>Model/Version DVE</value>
        </criteriaItems>
        <description>Updates the Optionals field with the Optionals_DVE field content.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
