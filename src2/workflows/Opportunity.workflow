<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alerta_de_Email_de_Envio_de_Oportunidade_com_Cr_dito_Pr_Aprovado</fullName>
        <description>Alerta de Email de Envio de Oportunidade com Crédito Pré Aprovado</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Renault/Nova_Oportunidade_Cr_dito_Pr_Aprovado</template>
    </alerts>
    <alerts>
        <fullName>Alerta_de_e_mail_quando_oportunidade_criada_pela_Plataforma_de_aquecimento</fullName>
        <description>OPP_LMT_BIR_PLAT_NOT_TDV</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/OPP_LMT_NEW_OPP</template>
    </alerts>
    <alerts>
        <fullName>EnvioEmailConcessionaria</fullName>
        <description>Envio de Email para Concessionária</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Renault/Nova_Oportunidade_V2</template>
    </alerts>
    <alerts>
        <fullName>Incorrect_CurrencyIsoCode_W2O</fullName>
        <description>Incorrect_CurrencyIsoCode_W2O</description>
        <protected>false</protected>
        <recipients>
            <recipient>BR_LMT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Renault/Web_to_opportunity_Incorreto</template>
    </alerts>
    <alerts>
        <fullName>Nova_Oportunidade_Test_Drive</fullName>
        <description>OPP_LMT_NEW_OPP_TD</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/Nova_Oportunidade_TDrive</template>
    </alerts>
    <alerts>
        <fullName>OPP_Hot_Lead_Logan_CC</fullName>
        <description>OPP Hot Lead Logan CC</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Commercial_Consultant2__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/Email_Hot_Lead_Consultor_Comercial</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMTOPP_LMT_NEW_WEBtoOPP_NEW_WEBtoOPP</fullName>
        <description>OPP_LMT_BIR_WEB</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/OPP_LMT_NEW_WEBtoOPP</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_BIR_PLAT_NOT_TDV_KWID</fullName>
        <description>OPP_LMT_BIR_PLAT_NOT_TDV_KWID</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessica.epifaneo@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/OPP_LMT_NEW_OPP_KWID</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_NEW_OPP_AROP</fullName>
        <description>OPP_LMT_NEW_OPP_AROP</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/OPP_LMT_NEW_OPP_AROP</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_NEW_OPP_AROP_OROCH</fullName>
        <description>OPP_LMT_NEW_OPP_AROP_OROCH</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/OPP_LMT_NEW_OPP_AROP_OROCH</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_NEW_OPP_CLIO</fullName>
        <description>OPP_LMT_NEW_OPP_CLIO</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/OPP_LMT_NEW_OPP_CLIO</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_NEW_OPP_OROCH</fullName>
        <description>OPP_LMT_NEW_OPP_OROCH</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/OPP_LMT_NEW_OPP_OROCH</template>
    </alerts>
    <alerts>
        <fullName>OPP_LMT_NEW_OPP_TESTDRIVE</fullName>
        <description>OPP_LMT_NEW_OPP_TESTDRIVE</description>
        <protected>false</protected>
        <recipients>
            <field>EmailDealer__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Renault/OPP_LMT_NEW_OPP_TESTDRIVE</template>
    </alerts>
    <alerts>
        <fullName>SendEmailAnalystsAboutKwidPreSale</fullName>
        <ccEmails>edvaldo@kolekto.com.br</ccEmails>
        <description>Send email to analysts about Kwid pre sale</description>
        <protected>false</protected>
        <recipients>
            <recipient>BRAnalistasLancamento</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Kwid</template>
    </alerts>
    <alerts>
        <fullName>Send_E_survey_Aquecimento</fullName>
        <description>Send E-survey - Aquecimento</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Send_E_survey_Aquecimento</template>
    </alerts>
    <alerts>
        <fullName>Send_E_survey_Web_Form_Varejo</fullName>
        <description>Send E-survey - Web Form Varejo</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Send_E_survey_Web_Form_Varejo</template>
    </alerts>
    <fieldUpdates>
        <fullName>Data_do_Pedido</fullName>
        <field>Data_do_pedido__c</field>
        <formula>NOW()</formula>
        <name>Data do Pedido</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LDD_UpdateEmailAddress</fullName>
        <field>EmailDealer__c</field>
        <formula>Dealer__r.Email__c</formula>
        <name>LDD_UpdateEmailAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_Check_SLA_Dealer_Expired</fullName>
        <field>SLA_Dealer_Expired__c</field>
        <literalValue>1</literalValue>
        <name>OPP: Check SLA Dealer Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_DVE_ClosingDate</fullName>
        <description>&quot;Vendas Empresa&quot; Opportunities always must have the closing date set 30 days after the created date.</description>
        <field>CloseDate</field>
        <formula>Today()+30</formula>
        <name>OPP_DVE_ClosingDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_Email_Commercial_Consultant</fullName>
        <field>Email_Commercial_Consultant2__c</field>
        <formula>Email_Commercial_Consultant__c</formula>
        <name>OPP Email Commercial Consultant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_SFA_CloseOPP_ReasonDealerSFA</fullName>
        <description>Reason of automatic cancelation</description>
        <field>ReasonLossCancellation__c</field>
        <literalValue>Opportunity Expired</literalValue>
        <name>OPP_SFA_CloseOPP_Reason_DealerSFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_SFA_CloseOPP_Reason_DealerNotSFA</fullName>
        <field>ReasonLossCancellation__c</field>
        <literalValue>Opportunity not in SFA</literalValue>
        <name>OPP_SFA_CloseOPP_Reason_DealerNotSFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_SFA_CloseOPP_Stage</fullName>
        <description>Close OPP as</description>
        <field>StageName</field>
        <literalValue>Lost</literalValue>
        <name>OPP_SFA_CloseOPP_Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UPDATE_OPTY_NAME</fullName>
        <description>Update de Opportunity Name</description>
        <field>Name</field>
        <formula>&quot;Oportunidade (&quot;+ Account.Name + Account.FirstName +&quot; &quot;+Account.LastName+&quot;)&quot;</formula>
        <name>OPP UPDATE OPTY NAME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_Internet</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>INTERNET</literalValue>
        <name>OPP_UpdateDVESource_Internet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_Interno</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>INTERNAL</literalValue>
        <name>OPP_UpdateDVESource_Interno</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_Marketing</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>MARKETING</literalValue>
        <name>OPP_UpdateDVESource_Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_Phone</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>PHONE</literalValue>
        <name>OPP_UpdateDVESource_Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_Rede</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>NETWORK</literalValue>
        <name>OPP_UpdateDVESource_Rede</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_UpdateDVESource_RenaultSite</fullName>
        <field>OpportunitySource__c</field>
        <literalValue>RENAULT SITE</literalValue>
        <name>OPP_UpdateDVESource_RenaultSite</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SFA_CopyCPF</fullName>
        <field>Account_CPF__c</field>
        <formula>Account.CustomerIdentificationNbr__c</formula>
        <name>SFA_CopyCPF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Data do Pedido</fullName>
        <actions>
            <name>Data_do_Pedido</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Text( StageName ) = &quot;Order&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>E-survey - Aquecimento</fullName>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17 OR 18 OR 19</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Admin Dealers</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC – Agent BO L2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Agent BO L3 / SRC Correspondant</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Agent Front Office</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Agent Front Office - Lead Tab</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC – Central Business Owner</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Marketing Renault</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Central System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Central System Operator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Knowledge Base Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC – Local Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>MKT - Marketing Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Renault Assistance</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Supervisor</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Supervisor and Admin Dealers</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Supervisor Plus</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>CCBIC - Supervisor QM</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>LMT - PA Ativa</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>LMT - Supervisao</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_E_survey_Aquecimento</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>E-survey - Web Form Varejo</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OpportunitySource__c</field>
            <operation>equals</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OpportunitySubSource__c</field>
            <operation>equals</operation>
            <value>LEADS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>equals</operation>
            <value>Renault do Brasil</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_E_survey_Web_Form_Varejo</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LDD_WF07_SendEmailOpportunityV2</fullName>
        <actions>
            <name>EnvioEmailConcessionaria</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.EmailDealer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.isDealerActive__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Detail__c</field>
            <operation>notEqual</operation>
            <value>TEST DRIVE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF08_UpdateEmailAddress</fullName>
        <actions>
            <name>LDD_UpdateEmailAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>1/1/2000</value>
        </criteriaItems>
        <description>Update opportunity email address.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF09_SendEmailOpportunity</fullName>
        <actions>
            <name>Alerta_de_Email_de_Envio_de_Oportunidade_com_Cr_dito_Pr_Aprovado</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.EmailDealer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.isDealerActive__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Detail__c</field>
            <operation>notEqual</operation>
            <value>TEST DRIVE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <description>Send Email to Dealer when a Opportunity is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LDD_WF09_SendEmailTestDrive</fullName>
        <actions>
            <name>Nova_Oportunidade_Test_Drive</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.EmailDealer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.isDealerActive__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Detail__c</field>
            <operation>equals</operation>
            <value>TEST DRIVE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <description>Send Email to Dealer when a Test Drive is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP Email Commercial Consultant</fullName>
        <actions>
            <name>OPP_Email_Commercial_Consultant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Email_Commercial_Consultant__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copia o email do campo formula para o campo do tipo email do consultor comercial</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP Email Hot Lead Logan Consultor</fullName>
        <actions>
            <name>OPP_Hot_Lead_Logan_CC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND (6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.OpportunitySource__c</field>
            <operation>notEqual</operation>
            <value>NETWORK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VehicleInterest__c</field>
            <operation>equals</operation>
            <value>LOGAN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>1/22/2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Email_Commercial_Consultant2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Bônus Logan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Logan Km Vantagens</value>
        </criteriaItems>
        <description>Origem da Oportunidade # Rede
Veiculo de Interesse = LOGAN
Fase = Identificado
Data da Criação &gt; 22/01/2014
Criado, Editado o e não atendeu previamente a esta regra</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP SLA Dealer Expired</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_Check_SLA_Dealer_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP SLA Dealer Expired 2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_Check_SLA_Dealer_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Expiration_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP UPDATE OPTY NAME</fullName>
        <actions>
            <name>OPP_UPDATE_OPTY_NAME</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DVR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Detail__c</field>
            <operation>notEqual</operation>
            <value>TEST DRIVE</value>
        </criteriaItems>
        <description>Update the opportunity name with the Account Name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_CREATETASK</fullName>
        <actions>
            <name>Uma_nova_oportunidade_foi_atribuida_a_sua_Concession_ria</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OpportunityTransition__c</field>
            <operation>equals</operation>
            <value>Test Drive To Manager</value>
        </criteriaItems>
        <description>Cria uma tarefa para o gerente da concessionária ao encaminhar uma oportunidade para a sua concessionária.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_DVE_ClosingDate</fullName>
        <actions>
            <name>OPP_DVE_ClosingDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <description>&quot;Vendas Empresa&quot; Opportunities always must have the closing date set 30 days after the created date.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_Incorrect_CurrencyIsoCode_W2O</fullName>
        <actions>
            <name>Incorrect_CurrencyIsoCode_W2O</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>equals</operation>
            <value>Renault do Brasil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CurrencyIsoCode</field>
            <operation>notEqual</operation>
            <value>BRL</value>
        </criteriaItems>
        <description>Send an email for administrators when a Web-to-opportunity is created with wrong CurrencyIsoCode.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_BIR_PLAT_NOT_TDV</fullName>
        <actions>
            <name>Alerta_de_e_mail_quando_oportunidade_criada_pela_Plataforma_de_aquecimento</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( EmailDealer__c &lt;&gt; &quot;&quot;, isDealerActive__c = &quot;N&quot;, NOT (ISPICKVAL( Detail__c , &quot;TEST DRIVE&quot; )), $User.ProfileId = &quot;00eD0000001Pj8Z&quot;, Platform_Converted__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_BIR_WEB</fullName>
        <actions>
            <name>OPP_LMTOPP_LMT_NEW_WEBtoOPP_NEW_WEBtoOPP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (  EmailDealer__c &lt;&gt; &quot;&quot;,  isDealerActive__c = &quot;N&quot;,  NOT (ISPICKVAL( Detail__c , &quot;TEST DRIVE&quot; )), NOT($User.ProfileId = &quot;00eD0000001Pj8Z&quot;),  NOT(Platform_Converted__c), NOT (CampaignId = &quot;&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_KWID_ANALIST</fullName>
        <actions>
            <name>OPP_LMT_BIR_PLAT_NOT_TDV_KWID</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISPICKVAL( Detail__c , &quot;PRE RESERVA&quot; ), $User.ProfileId = &quot;00eD0000001PhWS&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_NEW_OPP_AROP</fullName>
        <actions>
            <name>OPP_LMT_NEW_OPP_AROP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND (
 CampaignId = &quot;70157000001KGT3&quot;,
 $User.ProfileId = &quot;00eD0000001Pj8Z&quot;,
 Platform_Converted__c, 
 TODAY() &lt;= DATE(2015,12,31)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_NEW_OPP_AROP_OROCH</fullName>
        <actions>
            <name>OPP_LMT_NEW_OPP_AROP_OROCH</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ( 
CampaignId = &quot;70157000000c2YC&quot;, 
$User.ProfileId = &quot;00eD0000001Pj8Z&quot;, 
Platform_Converted__c,
 TODAY() &lt;= DATE(2015,11,30)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_NEW_OPP_CLIO</fullName>
        <actions>
            <name>OPP_LMT_NEW_OPP_CLIO</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ( 
OR(CampaignId = &quot;70157000001KILY&quot;, CampaignId = &quot;70157000001KILd&quot;), 
$User.ProfileId = &quot;00eD0000001Pj8Z&quot;, 
Platform_Converted__c,
TODAY() &lt;= DATE(2015,10,31) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP_LMT_NEW_OPP_OROCH</fullName>
        <actions>
            <name>OPP_LMT_NEW_OPP_OROCH</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ( 
CampaignId = &quot;70157000001KHeP&quot;, 
$User.ProfileId = &quot;00eD0000001Pj8Z&quot;, 
Platform_Converted__c,
 TODAY() &lt;= DATE(2015,10,31) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP_SFA_CloseOPP_Dealer_NotSFA</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DVE,DVR,Vendas Empresa,Vendas Empresa PF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified,Test Drive,Quote,In Attendance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.isDealerActive__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>notEqual</operation>
            <value>Pre order sent to Distrinet,Waiting for allocation,Vehicle allocated,Order Sedre Created</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Vehicle_Of_Interest__c</field>
            <operation>notEqual</operation>
            <value>KWID</value>
        </criteriaItems>
        <description>WR that trigger an automatic close of an OPP if is one day after close date for dealers out of sfa</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_SFA_CloseOPP_Reason_DealerNotSFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>OPP_SFA_CloseOPP_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP_SFA_CloseOPP_Dealer_SFA</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DVE,DVR,Vendas Empresa,Vendas Empresa PF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified,Test Drive,Quote,In Attendance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.isDealerActive__c</field>
            <operation>equals</operation>
            <value>S</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>notEqual</operation>
            <value>Pre order sent to Distrinet,Waiting for allocation,Vehicle allocated,Order Sedre Created</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Vehicle_Of_Interest__c</field>
            <operation>notEqual</operation>
            <value>KWID</value>
        </criteriaItems>
        <description>WR that trigger an automatic close of an OPP if is one day after close date for dealers active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_SFA_CloseOPP_ReasonDealerSFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>OPP_SFA_CloseOPP_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_Internet</fullName>
        <actions>
            <name>OPP_UpdateDVESource_Internet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>INTERNET</value>
        </criteriaItems>
        <description>Inserts the value INTERNET in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is INTERNET and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_Interno</fullName>
        <actions>
            <name>OPP_UpdateDVESource_Interno</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>INTERNAL</value>
        </criteriaItems>
        <description>Inserts the value INTERNAL in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is INTERNAL and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_Marketing</fullName>
        <actions>
            <name>OPP_UpdateDVESource_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>MARKETING</value>
        </criteriaItems>
        <description>Inserts the value MARKETING in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is MARKETING and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_Phone</fullName>
        <actions>
            <name>OPP_UpdateDVESource_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>PHONE</value>
        </criteriaItems>
        <description>Inserts the value PHONE in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is PHONE and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_Rede</fullName>
        <actions>
            <name>OPP_UpdateDVESource_Rede</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>Dealer</value>
        </criteriaItems>
        <description>Inserts the value REDE in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is REDE and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP_UpdateDVESource_RenaultSite</fullName>
        <actions>
            <name>OPP_UpdateDVESource_RenaultSite</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendas Empresa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>RENAULT SITE</value>
        </criteriaItems>
        <description>Inserts the value RENAULT SITE in the field &quot;Opportunity Source&quot;, when the &quot;Lead Source&quot; is RENAULT SITE and the RecordType is &quot;Vendas Empresa&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SFA_CopyCPF</fullName>
        <actions>
            <name>SFA_CopyCPF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF001_PreOrderBilled</fullName>
        <actions>
            <name>Pre_Ordem_Faturada_pelo_DMS</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Billed</value>
        </criteriaItems>
        <description>Quando uma oportunidade é faturada uma tarefa alertando o vendedor é criada para que ele de continuidade ao atendimento.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Pre_Ordem_Faturada_pelo_DMS</fullName>
        <assignedToType>owner</assignedToType>
        <description>Entre em contato com com o cliente através de um SMS para agradecer.

Não esqueça de apresentar a carta de boas vindas e o vídeo do Renault Site.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Pre Ordem Faturada pelo DMS</subject>
    </tasks>
    <tasks>
        <fullName>Uma_nova_oportunidade_foi_atribuida_a_sua_Concession_ria</fullName>
        <assignedToType>owner</assignedToType>
        <description>Uma oportunidade de um Test Drive foi encaminhada para a sua concessionária.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oportunidade TEST DRIVE</subject>
    </tasks>
</Workflow>
