<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailAlert</fullName>
        <description>EmailAlert</description>
        <protected>false</protected>
        <recipients>
            <recipient>m.salmi75@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>simon.gillet-renexter@renault.com.am</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vijayalakshmi.raju@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/RequestProcessNotification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_CCB</fullName>
        <ccEmails>rvlakshmi25@gmail.com</ccEmails>
        <description>Email Alert to CCB</description>
        <protected>false</protected>
        <recipients>
            <recipient>vijayalakshmi.raju@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Process_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Implementation_Team</fullName>
        <description>Email Alert to Implementation Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>praneel.pidikiti@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Process_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Moderator</fullName>
        <description>Email Alert to Moderator</description>
        <protected>false</protected>
        <recipients>
            <recipient>praneel.pidikiti@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Process_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Queue</fullName>
        <description>Email Alert to Queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>praneel.pidikiti@renault.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Process_Notification</template>
    </alerts>
    <alerts>
        <fullName>REQ_EmailSendToSuportTeam</fullName>
        <description>Permit to inform support Team that a request is validated</description>
        <protected>false</protected>
        <recipients>
            <recipient>m.salmi75@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>moctar.ba-renexter@renault.com.ccb</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Notification_Validation</template>
    </alerts>
    <alerts>
        <fullName>REQ_Send_Email_to_business_to_inform_that_a_request_is_solved</fullName>
        <description>Send Email to business to inform that a request is solved</description>
        <protected>false</protected>
        <recipients>
            <recipient>audrey.bourgois@renault.br.ccbic</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>francois.laruelle@renault.br.ccbic</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/Request_Notification_Resolved</template>
    </alerts>
    <fieldUpdates>
        <fullName>ChangeTeamOwner</fullName>
        <field>OwnerId</field>
        <lookupValue>Change_Implementation_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ChangeTeamOwner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_Assignment_to_LMT</fullName>
        <field>Status__c</field>
        <literalValue>Assigned to LMT Change implementation team</literalValue>
        <name>REQ Assignment to LMT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_BusinessValidationDate</fullName>
        <description>Business validation Date</description>
        <field>Validation_Date__c</field>
        <formula>Today()</formula>
        <name>REQ_BusinessValidationDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_Update_Date_of_Deployment</fullName>
        <description>Date of Deployment is updated to today()</description>
        <field>Date_Of_Deployment__c</field>
        <formula>today()</formula>
        <name>REQ Update Date of Deployment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_Update_Deployment_Status</fullName>
        <description>Update the Deployment status automatically to &apos;Yes&apos;.</description>
        <field>Deployment__c</field>
        <literalValue>Yes</literalValue>
        <name>REQ Update Deployment Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_Update_Resolution_Date</fullName>
        <description>Updates resolution date to &apos;today&apos;</description>
        <field>Resolution_Date__c</field>
        <formula>Today()</formula>
        <name>REQ Update Resolution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_Update_Resolved_Date</fullName>
        <field>Resolved_Date__c</field>
        <name>REQ Update Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REQ_assignment_to_SFA</fullName>
        <field>Status__c</field>
        <literalValue>Assigned to SFA Change implementation Team</literalValue>
        <name>REQ assignment to SFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Under Investigation</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_Admin</fullName>
        <description>Update the status to Assigned to Central Admin</description>
        <field>Status__c</field>
        <literalValue>Assigned To Central Admin</literalValue>
        <name>Status Update - Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StstusUpdate</fullName>
        <field>Status__c</field>
        <literalValue>Accepted by CCB</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStartDate</fullName>
        <field>Start_Date__c</field>
        <formula>Today()</formula>
        <name>UpdateStartDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Req_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Change_Implementation_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Req Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>REQ Assignment to SFA Change Implementation Team</fullName>
        <actions>
            <name>REQ_Assignment_to_LMT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>REQ_assignment_to_SFA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Request__c.OwnerId</field>
            <operation>equals</operation>
            <value>SFA Change Implementation Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Request__c.Project__c</field>
            <operation>equals</operation>
            <value>LMT/SFA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Request__c.Country__c</field>
            <operation>equals</operation>
            <value>Brazil</value>
        </criteriaItems>
        <description>Request is assigned to SFA development team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ Update Date of Deployment</fullName>
        <actions>
            <name>REQ_Update_Date_of_Deployment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>REQ_Update_Deployment_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When the request status is moved to closed, update the Deployment Date automatically</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ Update Resolved Date</fullName>
        <actions>
            <name>REQ_Update_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Resolved_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassVR__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ_Assign_ToCentralAdmin</fullName>
        <actions>
            <name>Email_Alert_to_Queue</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Status_Update_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.OwnerId</field>
            <operation>equals</operation>
            <value>Unresolved Admin Issues</value>
        </criteriaItems>
        <description>unresolved Admin issues assigned to this queue.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REQ_Assign_ToCentralImp</fullName>
        <actions>
            <name>Update_Req_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>End User Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Request__c.Request_Sub_type__c</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <description>Based on type/subtype,assign to Implementation Team</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REQ_ChangeImplTeam</fullName>
        <actions>
            <name>StstusUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.OwnerId</field>
            <operation>equals</operation>
            <value>Change Implementation Team</value>
        </criteriaItems>
        <description>Change the status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ_EmailAlert_Incident</fullName>
        <actions>
            <name>EmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Request_Sub_type__c</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <description>When Request Sub-type=Incident, send an email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ_Moderator Email Alert</fullName>
        <actions>
            <name>Email_Alert_to_Moderator</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Request__c.Status__c</field>
            <operation>contains</operation>
            <value>Rejected,Resolved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REQ_Process_Team</fullName>
        <actions>
            <name>Email_Alert_to_Implementation_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>ChangeTeamOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateStartDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Accepted by CCB</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REQ_SendEmailToBusiness</fullName>
        <actions>
            <name>REQ_Send_Email_to_business_to_inform_that_a_request_is_solved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>Permit to send Email to business to,inform that request is resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REQ_SendEmailToSupportTeam</fullName>
        <actions>
            <name>REQ_EmailSendToSuportTeam</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Validated_by_Business__c</field>
            <operation>equals</operation>
            <value>Verdadeiro</value>
        </criteriaItems>
        <description>Permit to support to receive an email when business valid a Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
