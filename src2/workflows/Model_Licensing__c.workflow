<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Model_Licensing_Field_Update_Status</fullName>
        <field>STATUS__c</field>
        <literalValue>Atribuído</literalValue>
        <name>Model Licensing: Field Update &apos;Status&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Atribuido</fullName>
        <field>STATUS__c</field>
        <literalValue>Atribuído</literalValue>
        <name>Status Atribuído</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Model Licensing%3A Update %27Status%27</fullName>
        <actions>
            <name>Model_Licensing_Field_Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (
LastModifiedDate  &lt;&gt;  CreatedDate,
ISPICKVAL( STATUS__c , &quot;Não Atribuído&quot;),
$User.BypassWF__c = false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
