<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>VCO_PV_Version_Check</fullName>
        <field>ID_Text__c</field>
        <formula>Call_Offer__r.Id &amp;  Version__r.Id</formula>
        <name>VCO_PV_Version_Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>VCO_PV_VersionCheck</fullName>
        <actions>
            <name>VCO_PV_Version_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VersionCallOffer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VSC_PV_ValidateNome</fullName>
        <active>false</active>
        <criteriaItems>
            <field>VersionCallOffer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
