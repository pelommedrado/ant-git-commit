<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_inactive</fullName>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Update Status to inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_ClearDeleteDate</fullName>
        <description>Set the Delete_Date to null</description>
        <field>My_Vehicle_Delete_Date__c</field>
        <name>VRE_ClearDeleteDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_ClearDeleteReason</fullName>
        <description>Set the Delete_Reason to null</description>
        <field>My_Vehicle_Delete_Reason__c</field>
        <name>VRE_ClearDeleteReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_GarageStatusConfirmed</fullName>
        <description>MyRenault Backend: My Garage Status = confirmed</description>
        <field>My_Garage_Status__c</field>
        <literalValue>confirmed</literalValue>
        <name>VRE_GarageStatusConfirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_GarageStatusRejected</fullName>
        <field>My_Garage_Status__c</field>
        <literalValue>taken</literalValue>
        <name>VRE_GarageStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_StartDateRelationUpdate</fullName>
        <description>Fill Start Date Of Relationship by &quot;Last Registration Date&quot; from Vehicle Object</description>
        <field>StartDateRelation__c</field>
        <formula>VIN__r.LastRegistrDate__c</formula>
        <name>VRE_StartDateOfRelationshipUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_StatusUpdate</fullName>
        <description>Set Vehicle Status Relation to Inactive</description>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>VRE_StatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_UnikRelationUpdate</fullName>
        <field>UnikRelation__c</field>
        <formula>VIN__c +  Account__r.Id</formula>
        <name>VRE_UnikRelationUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_WF05_Update_Delete_Date</fullName>
        <description>Update Delete Date</description>
        <field>My_Vehicle_Delete_Date__c</field>
        <formula>Today()</formula>
        <name>VRE_WF05_Update_Delete_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VRE_WF06_Update_Start_Date</fullName>
        <field>StartDateRelation__c</field>
        <formula>today()</formula>
        <name>VRE_WF06_Update_Start_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vehicle_Relation_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>data.loader@renault.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Vehicle Relation Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>VRE_MYR_AutoActivationOfRelationship</fullName>
        <actions>
            <name>VRE_GarageStatusConfirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>GarageStatus is set to &quot;confirmed&quot; if 
status= &quot;Active, type=Owner, enddate empty and (Garage Status is empty or &quot;unconfirmed&quot;) and 
the parent account is not a dealer or German with a Created, Preregistered or Activated status
R16.10 F1854 DEACTIVATED</description>
        <formula>AND(    ISPICKVAL(Status__c, &apos;Active&apos;)   , OR(ISPICKVAL( My_Garage_Status__c , &apos;&apos; ), ISPICKVAL( My_Garage_Status__c , &apos;unconfirmed&apos; ))     , NOT(ISPICKVAL(Account__r.Country__c, &apos;Germany&apos;))     , NOT(Account__r.RecordType.DeveloperName == &apos;Network_Site_Acc&apos;)  , ISNULL(EndDateRelation__c)  , ISPICKVAL(TypeRelation__c,&apos;Owner&apos;)  , OR(ISPICKVAL(Account__r.MYR_Status__c, &apos;Activated&apos;), ISPICKVAL(Account__r.MYR_Status__c, &apos;Created&apos;), ISPICKVAL(Account__r.MYR_Status__c, &apos;Preregistered&apos;))  , NOT($User.BypassWF__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VRE_MYR_AutoDeactivationOfRelationship</fullName>
        <actions>
            <name>VRE_GarageStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>MyRenault Backend: My Garage Status is automatically set to taken if the relationship is Inacative or My Grage status is empty.
(14/10/2015) !</description>
        <formula>AND(  ISPICKVAL(Status__c, &apos;Inactive&apos;), OR( ISPICKVAL( My_Garage_Status__c , &apos;&apos; ),     ISPICKVAL( My_Garage_Status__c , &apos;confirmed&apos; ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VRE_MYR_ClearDeleteDateAndDeleteReasonWhenConfirmed</fullName>
        <actions>
            <name>VRE_ClearDeleteDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>VRE_ClearDeleteReason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>(MyR BackEnd) Delete_Date and Delete_Reason are set to null when the status is updated from &apos;taken&apos; to &apos;confirmed&apos;</description>
        <formula>AND(ISPICKVAL(My_Garage_Status__c,&quot;confirmed&quot;), ISPICKVAL(PRIORVALUE(My_Garage_Status__c),&quot;taken&quot;),NOT($User.BypassWF__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VRE_WF01_StartDateofRelationshipUpdate</fullName>
        <actions>
            <name>VRE_StartDateRelationUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Start Date of Relationship when Last Registration Date is not null on the Vehicle object</description>
        <formula>NOT(  ISNULL(VIN__r.LastRegistrDate__c ) ) &amp;&amp; NOT( $User.BypassWF__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>VRE_WF02_UpdateStatus</fullName>
        <active>false</active>
        <description>When today&apos;s date reach the End date of relationship. Status is updated to &quot;inactive&quot;.</description>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VRE_StatusUpdate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>VRE_VehRel__c.EndDateRelation__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VRE_WF03_UnikRelationUpdate</fullName>
        <actions>
            <name>VRE_UnikRelationUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VRE_WF05_Update_Delete_Date</fullName>
        <actions>
            <name>VRE_WF05_Update_Delete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Delete Reason is updated with a value&lt;&gt;null, then Delete Date is set to now()</description>
        <formula>AND(ISCHANGED( My_Vehicle_Delete_Reason__c ),    NOT(ISBLANK(TEXT(My_Vehicle_Delete_Reason__c))), NOT($User.BypassWF__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VRE_WF06_Update_Start_Date</fullName>
        <actions>
            <name>VRE_WF06_Update_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(My_Garage_Status__c,&apos;confirmed&apos;) &amp;&amp; ISBLANK(StartDateRelation__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Vehicle Relation Record Owner</fullName>
        <actions>
            <name>Vehicle_Relation_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Owner:User.Profile.Id = &quot;00eD0000001Pp65&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
