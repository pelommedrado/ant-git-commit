<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MGOALSELLER_REL_Update_Vehicle</fullName>
        <field>Vehicle_of_Interest__c</field>
        <formula>TEXT(Quote__r.Opportunity.Vehicle_Of_Interest__c)</formula>
        <name>MGOALSELLER_REL - Update Vehicle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MGOALSELLER_REL - Vehicle</fullName>
        <actions>
            <name>MGOALSELLER_REL_Update_Vehicle</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Monthly_Goal_Seller_Relation__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
