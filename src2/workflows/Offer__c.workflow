<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PV_Email_DVR</fullName>
        <description>PV_Email_DVR</description>
        <protected>false</protected>
        <recipients>
            <recipient>BR_RP_DVR</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CaseApproval</template>
    </alerts>
    <alerts>
        <fullName>PV_Email_RC</fullName>
        <description>PV_Email_RCI</description>
        <protected>false</protected>
        <recipients>
            <recipient>BR_RP_RCI</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process/CaseApproval</template>
    </alerts>
    <fieldUpdates>
        <fullName>OFF_PV_AddNumeberOfInstalments</fullName>
        <field>Number_Of_Installments__c</field>
        <formula>Call_Offer__r.Period_in_Months__c</formula>
        <name>OFF_PV_AddNumeberOfInstalments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_Insert_IdCallOffer</fullName>
        <field>ID_Call_Offer__c</field>
        <formula>Call_Offer__r.Id</formula>
        <name>OFF_PV_Insert_IdCallOffer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_Update</fullName>
        <field>Minimum_Input__c</field>
        <formula>Call_Offer__r.Minimum_Entry__c</formula>
        <name>OFF_PV_UpdateMinInput</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_UpdateMouthTax</fullName>
        <description>update da taxa do mês</description>
        <field>Monthly_Tax__c</field>
        <formula>Call_Offer__r.Month_Rate__c</formula>
        <name>OFF_PV_UpdateMouthTax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_UpdateProduct</fullName>
        <description>Atualiza o campo para a formula pegar o valor base</description>
        <field>Featured_Product_Text__c</field>
        <formula>Optional__r.Featured_in_Product_as_Text__c</formula>
        <name>OFF_PV_UpdateProduct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_UpdatePvc</fullName>
        <field>Value_PVC__c</field>
        <formula>Mercadoria__r.PVC__c</formula>
        <name>OFF_PV_UpdatePvc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_UpdatePvr</fullName>
        <field>Value_PVR__c</field>
        <formula>Mercadoria__r.PVR__c</formula>
        <name>OFF_PV_UpdatePvr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_addCoefficient</fullName>
        <field>Coefficient__c</field>
        <formula>Call_Offer__r.Coefficient__c</formula>
        <name>OFF_PV_addCoefficient</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OFF_PV_updateTax</fullName>
        <field>Monthly_Tax__c</field>
        <formula>Call_Offer__r.Month_Rate__c</formula>
        <name>OFF_PV_updateTax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_AboutImput</fullName>
        <field>AboutImput__c</field>
        <formula>(Minimum_Input__c *  ValueTo__c)</formula>
        <name>PV_OFF_AboutImput</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_AtualizacaoFasePendenteDVR</fullName>
        <field>Stage__c</field>
        <literalValue>Pending DVR</literalValue>
        <name>PV_OFF_AtualizacaoFasePendenteDVR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_Fase_Aprovada</fullName>
        <description>A fase é alterada para aprovada</description>
        <field>Stage__c</field>
        <literalValue>Approved</literalValue>
        <name>PV_OFF_Fase_Aprovada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_ModificarParaPendenteRCI</fullName>
        <description>Modifica o campo para pendendente rci</description>
        <field>Stage__c</field>
        <literalValue>Pending RCI</literalValue>
        <name>PV_OFF_ModificarParaPendenteRCI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_ModifyPhase</fullName>
        <description>Modifica o status para pendente rci</description>
        <field>Stage__c</field>
        <literalValue>Pending RCI</literalValue>
        <name>PV_OFF_ModifyPhase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_ModifyPhase_WithMinimalWarning</fullName>
        <field>Stage__c</field>
        <literalValue>Pending DVR</literalValue>
        <name>PV_OFF_ModifyPhase_WithMinimalWarning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_ModifyPhase_WithoutCallOffer</fullName>
        <description>Modifica fase para pendente rci</description>
        <field>Stage__c</field>
        <literalValue>Pending RCI</literalValue>
        <name>PV_OFF_ModifyPhase_WithoutCallOffer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_ModifyPhaselNoCallOffer</fullName>
        <description>Ofertas cooperadas SEM chamada de oferta relacionada (outras) e COM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE DVR.</description>
        <field>Stage__c</field>
        <literalValue>Pending DVR</literalValue>
        <name>PV_OFF_ModifyPhaselNoCallOffer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_Percentage</fullName>
        <field>PercentagemImput__c</field>
        <formula>(((Minimum_Input__c *  ValueTo__c) / ValueTo__c) *100)</formula>
        <name>PV_OFF_Percentage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_Rejeitada</fullName>
        <description>O campo fase muda para rejeitada</description>
        <field>Stage__c</field>
        <literalValue>Rejected</literalValue>
        <name>PV_OFF_Rejeitada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_entradaMaisParcelas</fullName>
        <field>EntradaMaisParcelas__c</field>
        <formula>(Minimum_Input__c * ValueTo__c)+ (Number_Of_Installments__c *(((ValueTo__c + 498 +97.93 -(Minimum_Input__c * ValueTo__c))* Coefficient__c )))</formula>
        <name>PV_OFF_entradaMaisParcelas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_noCoperativeNoPvc_and_Pvr</fullName>
        <field>Stage__c</field>
        <literalValue>Pending RCI</literalValue>
        <name>PV_OFF_noCoperativeNoPvc_and_Pvr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_OFF_noCoperativewithPvc_and_Pvr</fullName>
        <description>fase muda para PENDENTE DVR.</description>
        <field>Stage__c</field>
        <literalValue>Pending DVR</literalValue>
        <name>PV_OFF_noCoperativewithPvc_and_Pvr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_UpdateRate</fullName>
        <field>PV_Rate_AA__c</field>
        <formula>(((((Monthly_Tax__c )+1) ^ 12)-1)* (100))</formula>
        <name>PV_UpdateRate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PV_atualizarValorAvistaDe</fullName>
        <field>Value_From__c</field>
        <formula>Mercadoria__r.PVC__c</formula>
        <name>PV_atualizarValorAvistaDe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OFF_PV_AddPvc</fullName>
        <actions>
            <name>OFF_PV_UpdatePvc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Offer__c.Value_PVC__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Atualiza o valor do pvc caso esteja nulo</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_AddPvr</fullName>
        <actions>
            <name>OFF_PV_UpdatePvr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Offer__c.Value_PVR__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Adiciona o valor de pvr caso o valor esteja nulo</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_Coefficient</fullName>
        <actions>
            <name>OFF_PV_addCoefficient</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ID_Call_Offer__c  &lt;&gt;  Call_Offer__r.Id ||  Coefficient__c = null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_Insert_Id_CallOffer</fullName>
        <actions>
            <name>OFF_PV_Insert_IdCallOffer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Offer__c.Name</field>
            <operation>notEqual</operation>
            <value>-1</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_UpdateFeaturedProduct</fullName>
        <actions>
            <name>OFF_PV_UpdateProduct</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Offer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_UpdateMinimunInput</fullName>
        <actions>
            <name>OFF_PV_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Minimum_Input__c = null || ID_Call_Offer__c  &lt;&gt;  Call_Offer__r.Id</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_UpdateMouthTax</fullName>
        <actions>
            <name>OFF_PV_updateTax</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Busca no objeto chamada de oferta o campo taxa mes caso o a taxa mensal esteja vázia</description>
        <formula>Monthly_Tax__c = null || ID_Call_Offer__c  &lt;&gt;  Call_Offer__r.Id</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_UpdateNumberOfInstaments</fullName>
        <actions>
            <name>OFF_PV_AddNumeberOfInstalments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Atualiza o campo campo prazo meses</description>
        <formula>ID_Call_Offer__c  &lt;&gt;  Call_Offer__r.Id ||  Number_Of_Installments__c = null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OFF_PV_ValueAtSight</fullName>
        <actions>
            <name>PV_atualizarValorAvistaDe</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Offer__c.Value_From__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Atualiza o campo à vista de caso esteja nulo</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_CooperadaComOferta%2FPVC%2FPVR</fullName>
        <actions>
            <name>PV_OFF_ModifyPhase_WithMinimalWarning</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ofertas cooperadas COM chamada de oferta ativa e COM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE DVR.</description>
        <formula>(TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Cooperada&quot; &amp;&amp;  TEXT(Call_Offer__r.Status__c)=&quot;Active&quot; &amp;&amp;  (Value_PVC__c &lt; (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c *(Mercadoria__r.PVC__c /100 ) )) )  || (Value_PVC__c &gt; (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 )))  || (( Value_PVR__c &lt; ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))||(Value_PVR__c &gt; ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100)))) ) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_CooperadaSemOfertaComPVC%2FPVR</fullName>
        <actions>
            <name>PV_OFF_ModifyPhaselNoCallOffer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ofertas cooperadas SEM chamada de oferta relacionada (outras) e COM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE DVR.</description>
        <formula>( TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Cooperada&quot; &amp;&amp;  Call_Offer__c = NULL &amp;&amp;  (Value_PVC__c &lt; (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ) )) )  || (Value_PVC__c &gt; (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ))))  &amp;&amp; (( Value_PVR__c &lt; ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))||(Value_PVR__c &gt; ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100)))) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_CooperadaSemOfertaRelacionada%2FPVC%2FPVR</fullName>
        <actions>
            <name>PV_OFF_ModifyPhase_WithoutCallOffer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ofertas cooperadas SEM chamada de oferta relacionada (outras) e SEM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE RCI.</description>
        <formula>(TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Cooperada&quot; &amp;&amp; Call_Offer__c = NULL &amp;&amp;  (Value_PVC__c &gt;= (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 )))) &amp;&amp; Value_PVC__c &lt;= (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ))))  &amp;&amp; (( Value_PVR__c &gt;= ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))&amp;&amp;(Value_PVR__c &lt;= ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_cooperadaSemAlertaPVC%2FPVR</fullName>
        <actions>
            <name>PV_OFF_ModifyPhase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ofertas cooperadas COM chamada de oferta ativa e SEM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE RCI.</description>
        <formula>(TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Cooperada&quot; &amp;&amp; TEXT(Call_Offer__r.Status__c)= &quot;Active&quot; &amp;&amp;  Value_PVC__c &gt;= (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100))) &amp;&amp; Value_PVC__c &lt;= (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100)))  &amp;&amp; (( Value_PVR__c &gt;= ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))&amp;&amp;(Value_PVR__c &lt;= ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100)))))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_noCooperadaSemOfertaComPVC%2FPVR</fullName>
        <actions>
            <name>PV_OFF_noCoperativewithPvc_and_Pvr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>- Ofertas NÃO cooperadas SEM chamada de oferta relacionada (outras) e COM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE DVR.</description>
        <formula>(TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Não cooperada&quot; &amp;&amp;  Call_Offer__c = NULL &amp;&amp;  (Value_PVC__c &lt; (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ) )) )  || (Value_PVC__c &gt; (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 )))  || (( Value_PVR__c &lt; ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))||(Value_PVR__c &gt; ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100)))) )  ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PV_OFF_noCoperative_noPvc_and_Pvr</fullName>
        <actions>
            <name>PV_OFF_noCoperativeNoPvc_and_Pvr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ofertas NÃO cooperadas SEM chamada de oferta relacionada (outras) e SEM alerta de mínimo PVR/ máximo PVC o status muda para PENDENTE RCI</description>
        <formula>(TEXT(Group_Offer__r.Type_of_Offer__c) = &quot;Não cooperada&quot; &amp;&amp; Call_Offer__c = NULL &amp;&amp;  Call_Offer__c = NULL &amp;&amp;  (Value_PVC__c &gt;= (Mercadoria__r.PVC__c - (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ))) &amp;&amp; Value_PVC__c &lt;= (Mercadoria__r.PVC__c + (Version__r.PVC_Maximo__c * (Mercadoria__r.PVC__c /100 ) )))  &amp;&amp; (( Value_PVR__c &gt;= ( Mercadoria__r.PVR__c - (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100))))&amp;&amp;(Value_PVR__c &lt;= ( Mercadoria__r.PVR__c + (Version__r.PVR_Minimo__c *(Mercadoria__r.PVR__c/100)))) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Pv_OFF_LegalText</fullName>
        <actions>
            <name>PV_OFF_AboutImput</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PV_OFF_Percentage</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PV_OFF_entradaMaisParcelas</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PV_UpdateRate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Offer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Realiza os cálculos necessários para o texto jurídico</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
