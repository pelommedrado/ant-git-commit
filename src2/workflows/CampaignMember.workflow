<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Agradecimento_Feiras</fullName>
        <description>Agradecimento_Feiras</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Agradecimentos_feira_AF4</template>
    </alerts>
    <alerts>
        <fullName>SendEmailThanks_Activation</fullName>
        <description>SendEmailThanks_Activation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Agradecimentos_ativacoes_AF4</template>
    </alerts>
    <alerts>
        <fullName>SendEmailThanks_Activation_CommercialVehicles</fullName>
        <description>SendEmailThanks_Activation_CommercialVehicles</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Agradecimentos_ativacoes_utilitarios_AF4</template>
    </alerts>
    <alerts>
        <fullName>SendEmailThanks_Fair_CommercialVehicles</fullName>
        <description>SendEmailThanks_Fair_CommercialVehicles</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sac.brasil@renault.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Todos/Agradecimentos_feira_utilitarios_AF4</template>
    </alerts>
    <fieldUpdates>
        <fullName>CMM_UpdateSourceDealer</fullName>
        <field>Source__c</field>
        <literalValue>Dealer</literalValue>
        <name>CMM_UpdateSourceDealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMM_UpdateSourceMarketing</fullName>
        <field>Source__c</field>
        <literalValue>Marketing</literalValue>
        <name>CMM_UpdateSourceMarketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CMM_WF01_UpdateSourceMarketing</fullName>
        <actions>
            <name>CMM_UpdateSourceMarketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Source field for Marketing when record created by the marketing profile.</description>
        <formula>IF($Profile.Name = &apos;Marketing Renault&apos;, true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMM_WF02_UpdateSourceDelaer</fullName>
        <actions>
            <name>CMM_UpdateSourceDealer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Source field for Dealer when record created by the Commercial Manager and  Commercial Assistant profile.</description>
        <formula>IF(OR($Profile.Name = &apos;Gerente Comercial&apos; , $Profile.Name = &apos;Assistente Comercial&apos;), true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMM_WF03_SendEmailThanks_Fair</fullName>
        <actions>
            <name>Agradecimento_Feiras</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1035.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1034.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1037.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1090.1</value>
        </criteriaItems>
        <description>Send an Email of thaks for people who have completed the form web-to-lead at fairs.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMM_WF04_SendEmailThanks_Fair_CommercialVehicles</fullName>
        <actions>
            <name>SendEmailThanks_Fair_CommercialVehicles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1036.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1039.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1040.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1041.1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1042.1</value>
        </criteriaItems>
        <description>Send an Email of thaks for people who have completed the form web-to-lead at fairs for commercial vehicles.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMM_WF05_SendEmailThanks_Activation</fullName>
        <actions>
            <name>SendEmailThanks_Activation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.DBMCampaignCode__c</field>
            <operation>equals</operation>
            <value>DCL1038.1</value>
        </criteriaItems>
        <description>Send an Email of thaks for people who have completed the form web-to-lead at renault events.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMM_WF06_SendEmailThanks_Activation_CommercialVehicles</fullName>
        <actions>
            <name>SendEmailThanks_Activation_CommercialVehicles</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CampaignMember.Lead_Campaign_Name__c</field>
            <operation>equals</operation>
            <value>Não tem campanha</value>
        </criteriaItems>
        <description>Send an Email of thaks for people who have completed the form web-to-lead at renault events of Commercial Vehicles.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
