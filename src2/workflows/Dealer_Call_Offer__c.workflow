<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PV_ChCO_ValidateCallOfferofDealer</fullName>
        <field>ValidateCallOfferofDealer__c</field>
        <formula>Call_Offer__c  &amp;  Concessionaria__c</formula>
        <name>PV_ChCO_ValidateCallOfferofDealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PV_ChOC_ValidateDealerOfOffer</fullName>
        <actions>
            <name>PV_ChCO_ValidateCallOfferofDealer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Dealer_Call_Offer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>validar o campo &quot;validade&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
