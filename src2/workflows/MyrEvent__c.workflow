<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MyrEvent_IsActive_Date</fullName>
        <description>Set Now is the field  IsActive Date</description>
        <field>IsActive_Date__c</field>
        <formula>NOW()</formula>
        <name>MyrEvent IsActive Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MyrEvent_Record_Type</fullName>
        <description>Modify the record type of the event.</description>
        <field>RecordTypeId</field>
        <lookupValue>Archived_Event</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MyrEvent Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MyrEvent_State_Date</fullName>
        <description>Filled the state date as soon as the state has been changed.</description>
        <field>State_Date__c</field>
        <formula>NOW()</formula>
        <name>MyrEvent State Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Myr_Event_Last_Publication</fullName>
        <description>Set Last Publication Date to Now()</description>
        <field>Last_Publication__c</field>
        <formula>NOW()</formula>
        <name>Myr Event Last Publication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EVT_WF01_State_Date</fullName>
        <actions>
            <name>MyrEvent_State_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Filled the status date as soon as the status has been modified</description>
        <formula>ISCHANGED( State__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EVT_WF02_Publication_Date</fullName>
        <actions>
            <name>Myr_Event_Last_Publication</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MyrEvent__c.State__c</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <description>Date of the last publication for this event : set as soon as the State field has been set to &apos;Published&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EVT_WF03_IsActive_Date</fullName>
        <actions>
            <name>MyrEvent_IsActive_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Keep the date of the last modification of the field IsActive.</description>
        <formula>ISCHANGED( IsActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EVT_WF04_Archived_Events</fullName>
        <actions>
            <name>MyrEvent_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MyrEvent__c.State__c</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Modify the record type when the event is archived (related timeline elements purged)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
