<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CO_Comment_Alert_Template</fullName>
        <description>CO Comment Alert Template</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Colombie/CO_Comment_Alert_Template</template>
    </alerts>
    <alerts>
        <fullName>Novo_Coment_rio</fullName>
        <description>Novo Comentário</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Todos/ContactFollowUpSAMPLE</template>
    </alerts>
    <rules>
        <fullName>CAS_WFCO05_Case Comment Alerts</fullName>
        <actions>
            <name>CO_Comment_Alert_Template</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CountryCase__c</field>
            <operation>equals</operation>
            <value>Colombia</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>CaseComment.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
