<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CAS_Update_Goodwill_Status</fullName>
        <description>Update the Goodwill Status on the Case Object to &apos;No Approval&apos;, while a new goodwill is created.</description>
        <field>GWStatus__c</field>
        <literalValue>No Approval</literalValue>
        <name>CAS Update Goodwill Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GoodWill_Status_Refused</fullName>
        <field>GoodwillStatus__c</field>
        <literalValue>Refused</literalValue>
        <name>GoodWill Status Refused</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GoodWill_Statut_Approved</fullName>
        <field>GoodwillStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>GoodWill Statut Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusOnCase</fullName>
        <field>GWStatus__c</field>
        <literalValue>In Progress</literalValue>
        <name>UpdateStatusOnCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CAS_WF13_Update GoodwillStatusforAlert</fullName>
        <actions>
            <name>CAS_Update_Goodwill_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Goodwill__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>CAR RENTAL</value>
        </criteriaItems>
        <description>Goodwill Status on the Case Object has to be updated to &apos;No Approval&apos; when a new goodwill is added.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GOO_WF01_GoodwillStatusChangeOnCase</fullName>
        <actions>
            <name>UpdateStatusOnCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Goodwill__c.GoodwillStatus__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GWStatus__c</field>
            <operation>notEqual</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Goodwill__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>CAR RENTAL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Car_Rental_Approval_LEVEL2M</fullName>
        <assignedTo>BR_SRCSupervisorBO</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Agent BO LEVEL 2 MONDIAL &lt;10days</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Car Rental Approval LEVEL2M</subject>
    </tasks>
    <tasks>
        <fullName>Car_Rental_Approval_LEVEL3R</fullName>
        <assignedTo>BR_AgentBackOfficeLevel3</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Agent BO LEVEL 3 RENAULT between 10 and 20 days</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Car Rental Approval LEVEL3R</subject>
    </tasks>
    <tasks>
        <fullName>Car_Rental_Approval_Superv_LEVEL3R</fullName>
        <assignedTo>BR_SRCSupervisorBORenault</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Car Rental &gt; 20 days</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Car Rental Approval Superv LEVEL3R</subject>
    </tasks>
    <tasks>
        <fullName>Check_Car_Rental_Goodwill</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Check Car Rental Goodwill</subject>
    </tasks>
    <tasks>
        <fullName>Rent_A_car_Refused</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>To Do</status>
        <subject>Rent A car Refused</subject>
    </tasks>
</Workflow>
