<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ALR_ChassiNotDuplicate2</fullName>
        <field>ALR_ChassiNotDuplicate__c</field>
        <formula>Name</formula>
        <name>ALR_ChassiNotDuplicate2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusWhenDealerShipArrival</fullName>
        <field>ALR_Vehicle_Position__c</field>
        <literalValue>On dealer stock</literalValue>
        <name>UpdateStatusWhenDealerShipArrival</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ALR_ChassiNotDuplicate</fullName>
        <actions>
            <name>ALR_ChassiNotDuplicate2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ALR_Tracking__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Insere o valor do campo name(Chassi) no campo ALR_ChassiNotDuplicate para verificar se o chassi não é duplicado</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateStatusWhenDealerShipArrival</fullName>
        <actions>
            <name>UpdateStatusWhenDealerShipArrival</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ALR_Tracking__c.ALR_Date_MADP__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
