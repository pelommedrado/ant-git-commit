<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is used to store the Relationship Decision Tree.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>AddressOK__c</fullName>
        <description>The address information is ok.</description>
        <externalId>false</externalId>
        <label>Address OK</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Birthdate__c</fullName>
        <description>Is customer birthday.</description>
        <externalId>false</externalId>
        <label>Birthdate</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EmailOK__c</fullName>
        <description>The email is ok.</description>
        <externalId>false</externalId>
        <label>Email OK</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>InformationUpdatedInTheLast5Years__c</fullName>
        <description>The customer information was updated within last 60 months.</description>
        <externalId>false</externalId>
        <label>Information Updated in the Last 5 Years</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ParticipateOnOPA__c</fullName>
        <description>Customer participated on OPA.</description>
        <externalId>false</externalId>
        <label>Participate on OPA</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>PhoneOK__c</fullName>
        <description>Any phone contact is ok.</description>
        <externalId>false</externalId>
        <label>Phone OK</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Y</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>N</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Recommendation1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The first option of recommendation.</description>
        <externalId>false</externalId>
        <label>Recommendation 1</label>
        <referenceTo>RCM_Recommendation__c</referenceTo>
        <relationshipLabel>Relationship Decision Trees</relationshipLabel>
        <relationshipName>Relationship_Decision_Trees</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recommendation2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The second option of recommendation.</description>
        <externalId>false</externalId>
        <label>Recommendation 2</label>
        <referenceTo>RCM_Recommendation__c</referenceTo>
        <relationshipLabel>Relationship Decision Trees (Recomendation 2)</relationshipLabel>
        <relationshipName>Relationship_Decision_Trees1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recommendation3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The third option of recommendation.</description>
        <externalId>false</externalId>
        <label>Recommendation 3</label>
        <referenceTo>RCM_Recommendation__c</referenceTo>
        <relationshipLabel>Relationship Decision Trees (Recomendation 3)</relationshipLabel>
        <relationshipName>Relationship_Decision_Trees2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recommendation4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The fourth option of recommendation.</description>
        <externalId>false</externalId>
        <label>Recommendation 4</label>
        <referenceTo>RCM_Recommendation__c</referenceTo>
        <relationshipLabel>Relationship Decision Trees (Recomendation 4)</relationshipLabel>
        <relationshipName>Relationship_Decision_Trees3</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Relationship Decision Tree</label>
    <nameField>
        <displayFormat>{0000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Relationship Decision Trees</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
