<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is used to store all type of relation between a given account and a vehicle</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Vehicle Relations</relationshipLabel>
        <relationshipName>Vehicle_Relations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CreationOrigin__c</fullName>
        <externalId>false</externalId>
        <label>CreationOrigin</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DateOfEntryInStock__c</fullName>
        <externalId>false</externalId>
        <label>Date of Entry in Stock</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Days_in_Stock__c</fullName>
        <externalId>false</externalId>
        <formula>TODAY()- DateOfEntryInStock__c</formula>
        <label>Days in Stock</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Dealer_Regional__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.NameZone__c</formula>
        <label>Dealer Regional</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndDateRelation__c</fullName>
        <externalId>false</externalId>
        <label>End Date of relationship</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Favorite__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Favorite vehicle user</description>
        <externalId>false</externalId>
        <inlineHelpText>Favorite vehicle user</inlineHelpText>
        <label>Favorite</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Grupo_da_Conta__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Parent.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Grupo da Conta</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IntegrationDate__c</fullName>
        <description>field used for exchanges between RForce and BCS applications (delta/flowback)</description>
        <externalId>false</externalId>
        <label>IntegrationDate</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>MaintenanceDealer__c</fullName>
        <externalId>false</externalId>
        <formula>VIN__r.MaintenanceDealer__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Maintenance Dealer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Model__c</fullName>
        <externalId>false</externalId>
        <formula>VIN__r.Model__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Model</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>My_Garage_Status__c</fullName>
        <description>Non confirmé/unconfirmed
Confirmé/confirmed
Refusé/rejected</description>
        <externalId>false</externalId>
        <label>My Garage Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>unconfirmed</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>confirmed</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>rejected</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>taken</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>My_Vehicle_Delete_Date__c</fullName>
        <description>Date of relation&apos;s deletion</description>
        <externalId>false</externalId>
        <label>My Vehicle Delete Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>My_Vehicle_Delete_Reason__c</fullName>
        <description>Reason of vehicle deletion in My Garage</description>
        <externalId>false</externalId>
        <label>My Vehicle Delete Reason</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>6</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>7</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Owner_Type__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.RecordTypeId</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Owner Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ReasonEndRelation__c</fullName>
        <externalId>false</externalId>
        <label>Reason of end relation</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>I don&apos;t want to follow this vehicle</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>I no longer have this vehicle</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SellerDealer__c</fullName>
        <externalId>false</externalId>
        <formula>VIN__r.IDBIRSellingDealer__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Seller Dealer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SellingDealer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>SellingDealer</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.Name</field>
                <operation>equals</operation>
                <value>Network Site Account</value>
            </filterItems>
            <filterItems>
                <field>Account.DealershipStatus__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Relations véhicules (SellingDealer)</relationshipLabel>
        <relationshipName>Relations_v_hicules</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>StartDateRelation__c</fullName>
        <externalId>false</externalId>
        <label>Start Date Of relationship</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StatusDate__c</fullName>
        <externalId>false</externalId>
        <label>StatusDate</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Tech_ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <externalId>true</externalId>
        <label>Tech External ID</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>TypeRelation__c</fullName>
        <externalId>false</externalId>
        <label>Type of relationship</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>User</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Leasing User</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>UnikRelation__c</fullName>
        <caseSensitive>true</caseSensitive>
        <externalId>false</externalId>
        <label>UnikRelation</label>
        <length>150</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>UpdatedOrigin__c</fullName>
        <externalId>false</externalId>
        <label>UpdatedOrigin</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VIN__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Rforce Release 3/All Countries
Vehicle Identification Number. Unique identifier provided by the Manufacturer.</description>
        <externalId>false</externalId>
        <label>VIN</label>
        <referenceTo>VEH_Veh__c</referenceTo>
        <relationshipLabel>Relations véhicules</relationshipLabel>
        <relationshipName>Vehicle_Relations</relationshipName>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VNVO__c</fullName>
        <externalId>false</externalId>
        <label>VN/VO</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Second hand</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VehicleBrand__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(VIN__r.VehicleBrand__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Vehicle Brand</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VehicleRegistrNbr__c</fullName>
        <externalId>false</externalId>
        <formula>VIN__r.VehicleRegistrNbr__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Vehicle Registration Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Custom_Name__c</fullName>
        <externalId>false</externalId>
        <label>Vehicle Custom Name</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VersionCode__c</fullName>
        <externalId>false</externalId>
        <formula>VIN__r.VersionCode__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Version Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ZE_Contract_Number__c</fullName>
        <description>Contract number (activation code) for ZE</description>
        <externalId>false</externalId>
        <label>ZE Contract Number</label>
        <length>10</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ZE_Status__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Optin CGU (Conditions Generales d&apos;Utilisation) for ZE</description>
        <externalId>false</externalId>
        <label>ZE Status</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Vehicle Relation</label>
    <nameField>
        <displayFormat>{000000000}</displayFormat>
        <label>Relation Number</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Vehicle Relations</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>VIN__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>VehicleRegistrNbr__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StartDateRelation__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EndDateRelation__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TypeRelation__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>VIN__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>VehicleRegistrNbr__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StartDateRelation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>EndDateRelation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TypeRelation__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>VIN__c</searchFilterFields>
        <searchFilterFields>VehicleRegistrNbr__c</searchFilterFields>
        <searchFilterFields>StartDateRelation__c</searchFilterFields>
        <searchFilterFields>EndDateRelation__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>TypeRelation__c</searchFilterFields>
        <searchFilterFields>Model__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>VRE_VR01_EndDateOfRelationshipValidation</fullName>
        <active>true</active>
        <description>&quot;End Date of relationship&quot; must be &gt; &quot;Start Date of relationship&quot;</description>
        <errorConditionFormula>OR((EndDateRelation__c  &gt;  TODAY() ),(EndDateRelation__c  &lt;  StartDateRelation__c)) &amp;&amp; NOT($User.BypassVR__c)</errorConditionFormula>
        <errorDisplayField>EndDateRelation__c</errorDisplayField>
        <errorMessage>The field must be superior to the field &quot;Start Date of relationship&quot; and cannot be superior to today&apos;s date.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>VRE_VR02_TypeOfRelationship</fullName>
        <active>true</active>
        <description>You have to fill field type of relationship in portal user for Brazil</description>
        <errorConditionFormula>AND ($Profile.Name = &quot;Service Profile&quot;, (ISPICKVAL( TypeRelation__c , &quot;&quot;)), ISPICKVAL($User.RecordDefaultCountry__c,&quot;Brazil&quot; ))</errorConditionFormula>
        <errorMessage>É Obrigatório o preenchimento do Tipo de relacionamento</errorMessage>
    </validationRules>
</CustomObject>
